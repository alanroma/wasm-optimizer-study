; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionDispatch/btGhostObject.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionDispatch/btGhostObject.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btGhostObject = type { %class.btCollisionObject, %class.btAlignedObjectArray }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btVector3 = type { [4 x float] }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%union.anon = type { i8* }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btPairCachingGhostObject = type { %class.btGhostObject, %class.btHashedOverlappingPairCache* }
%class.btHashedOverlappingPairCache = type { %class.btOverlappingPairCache, %class.btAlignedObjectArray.0, %struct.btOverlapFilterCallback*, i8, [3 x i8], %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5, %class.btOverlappingPairCallback* }
%class.btOverlappingPairCache = type { %class.btOverlappingPairCallback }
%class.btOverlappingPairCallback = type { i32 (...)** }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.btBroadphasePair*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btBroadphasePair = type { %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btCollisionAlgorithm*, %union.anon.3 }
%class.btCollisionAlgorithm = type opaque
%union.anon.3 = type { i8* }
%struct.btOverlapFilterCallback = type { i32 (...)** }
%class.btAlignedObjectArray.5 = type <{ %class.btAlignedAllocator.6, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.6 = type { i8 }
%class.btDispatcher = type { i32 (...)** }
%class.btConvexShape = type { %class.btCollisionShape }
%"struct.btCollisionWorld::ConvexResultCallback" = type { i32 (...)**, float, i16, i16 }
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }
%"struct.btCollisionWorld::RayResultCallback" = type { i32 (...)**, float, %class.btCollisionObject*, i16, i16, i32 }
%class.btSerializer = type opaque

$_ZN20btAlignedObjectArrayIP17btCollisionObjectEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectED2Ev = comdat any

$_ZN17btCollisionObjectdlEPv = comdat any

$_ZNK20btAlignedObjectArrayIP17btCollisionObjectE16findLinearSearchERKS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE9push_backERKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE8pop_backEv = comdat any

$_ZN17btCollisionObject19getBroadphaseHandleEv = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN15btTransformUtil17calculateVelocityERK11btTransformS2_fR9btVector3S4_ = comdat any

$_ZN11btTransform11setIdentityEv = comdat any

$_ZN11btTransform11setRotationERK12btQuaternion = comdat any

$_ZNK11btTransform11getRotationEv = comdat any

$_ZNK20btAlignedObjectArrayIP17btCollisionObjectEixEi = comdat any

$_ZN17btCollisionObject17getCollisionShapeEv = comdat any

$_ZN17btCollisionObject17getWorldTransformEv = comdat any

$_Z10AabbExpandR9btVector3S0_RKS_S2_ = comdat any

$_Z9btRayAabbRK9btVector3S1_S1_S1_RfRS_ = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZN11btTransform9setOriginERK9btVector3 = comdat any

$_ZNK17btCollisionObject24checkCollideWithOverrideEPKS_ = comdat any

$_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape = comdat any

$_ZNK17btCollisionObject28calculateSerializeBufferSizeEv = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZdvRK9btVector3RKf = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x37inverseEv = comdat any

$_ZN12btQuaternionC2Ev = comdat any

$_ZNK11btMatrix3x311getRotationER12btQuaternion = comdat any

$_ZN12btQuaternion9normalizeEv = comdat any

$_ZNK12btQuaternion8getAngleEv = comdat any

$_ZNK10btQuadWord1xEv = comdat any

$_ZNK10btQuadWord1yEv = comdat any

$_ZNK10btQuadWord1zEv = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_Z6btSqrtf = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZNK11btMatrix3x35cofacEiiii = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN10btQuadWordC2Ev = comdat any

$_ZN10btQuadWord8setValueERKfS1_S1_S1_ = comdat any

$_ZNK12btQuaternion6lengthEv = comdat any

$_ZN12btQuaterniondVERKf = comdat any

$_ZNK12btQuaternion7length2Ev = comdat any

$_ZNK12btQuaternion3dotERKS_ = comdat any

$_ZN12btQuaternionmLERKf = comdat any

$_Z6btAcosf = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN11btMatrix3x311setIdentityEv = comdat any

$_ZN11btMatrix3x311setRotationERK12btQuaternion = comdat any

$_ZNK10btQuadWord1wEv = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_Z9btOutcodeRK9btVector3S1_ = comdat any

$_Z8btSetMinIfEvRT_RKS0_ = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE4initEv = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE10deallocateEPS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE8allocateEiPPKS1_ = comdat any

@_ZTV13btGhostObject = hidden unnamed_addr constant { [11 x i8*] } { [11 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI13btGhostObject to i8*), i8* bitcast (i1 (%class.btCollisionObject*, %class.btCollisionObject*)* @_ZNK17btCollisionObject24checkCollideWithOverrideEPKS_ to i8*), i8* bitcast (%class.btGhostObject* (%class.btGhostObject*)* @_ZN13btGhostObjectD1Ev to i8*), i8* bitcast (void (%class.btGhostObject*)* @_ZN13btGhostObjectD0Ev to i8*), i8* bitcast (void (%class.btCollisionObject*, %class.btCollisionShape*)* @_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape to i8*), i8* bitcast (i32 (%class.btCollisionObject*)* @_ZNK17btCollisionObject28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)* @_ZNK17btCollisionObject9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionObject*, %class.btSerializer*)* @_ZNK17btCollisionObject21serializeSingleObjectEP12btSerializer to i8*), i8* bitcast (void (%class.btGhostObject*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)* @_ZN13btGhostObject28addOverlappingObjectInternalEP17btBroadphaseProxyS1_ to i8*), i8* bitcast (void (%class.btGhostObject*, %struct.btBroadphaseProxy*, %class.btDispatcher*, %struct.btBroadphaseProxy*)* @_ZN13btGhostObject31removeOverlappingObjectInternalEP17btBroadphaseProxyP12btDispatcherS1_ to i8*)] }, align 4
@_ZTV24btPairCachingGhostObject = hidden unnamed_addr constant { [11 x i8*] } { [11 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI24btPairCachingGhostObject to i8*), i8* bitcast (i1 (%class.btCollisionObject*, %class.btCollisionObject*)* @_ZNK17btCollisionObject24checkCollideWithOverrideEPKS_ to i8*), i8* bitcast (%class.btPairCachingGhostObject* (%class.btPairCachingGhostObject*)* @_ZN24btPairCachingGhostObjectD1Ev to i8*), i8* bitcast (void (%class.btPairCachingGhostObject*)* @_ZN24btPairCachingGhostObjectD0Ev to i8*), i8* bitcast (void (%class.btCollisionObject*, %class.btCollisionShape*)* @_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape to i8*), i8* bitcast (i32 (%class.btCollisionObject*)* @_ZNK17btCollisionObject28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)* @_ZNK17btCollisionObject9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionObject*, %class.btSerializer*)* @_ZNK17btCollisionObject21serializeSingleObjectEP12btSerializer to i8*), i8* bitcast (void (%class.btPairCachingGhostObject*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)* @_ZN24btPairCachingGhostObject28addOverlappingObjectInternalEP17btBroadphaseProxyS1_ to i8*), i8* bitcast (void (%class.btPairCachingGhostObject*, %struct.btBroadphaseProxy*, %class.btDispatcher*, %struct.btBroadphaseProxy*)* @_ZN24btPairCachingGhostObject31removeOverlappingObjectInternalEP17btBroadphaseProxyP12btDispatcherS1_ to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS13btGhostObject = hidden constant [16 x i8] c"13btGhostObject\00", align 1
@_ZTI17btCollisionObject = external constant i8*
@_ZTI13btGhostObject = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([16 x i8], [16 x i8]* @_ZTS13btGhostObject, i32 0, i32 0), i8* bitcast (i8** @_ZTI17btCollisionObject to i8*) }, align 4
@_ZTS24btPairCachingGhostObject = hidden constant [27 x i8] c"24btPairCachingGhostObject\00", align 1
@_ZTI24btPairCachingGhostObject = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([27 x i8], [27 x i8]* @_ZTS24btPairCachingGhostObject, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI13btGhostObject to i8*) }, align 4

@_ZN13btGhostObjectC1Ev = hidden unnamed_addr alias %class.btGhostObject* (%class.btGhostObject*), %class.btGhostObject* (%class.btGhostObject*)* @_ZN13btGhostObjectC2Ev
@_ZN13btGhostObjectD1Ev = hidden unnamed_addr alias %class.btGhostObject* (%class.btGhostObject*), %class.btGhostObject* (%class.btGhostObject*)* @_ZN13btGhostObjectD2Ev
@_ZN24btPairCachingGhostObjectC1Ev = hidden unnamed_addr alias %class.btPairCachingGhostObject* (%class.btPairCachingGhostObject*), %class.btPairCachingGhostObject* (%class.btPairCachingGhostObject*)* @_ZN24btPairCachingGhostObjectC2Ev
@_ZN24btPairCachingGhostObjectD1Ev = hidden unnamed_addr alias %class.btPairCachingGhostObject* (%class.btPairCachingGhostObject*), %class.btPairCachingGhostObject* (%class.btPairCachingGhostObject*)* @_ZN24btPairCachingGhostObjectD2Ev

define hidden %class.btGhostObject* @_ZN13btGhostObjectC2Ev(%class.btGhostObject* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btGhostObject*, align 4
  store %class.btGhostObject* %this, %class.btGhostObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGhostObject*, %class.btGhostObject** %this.addr, align 4
  %0 = bitcast %class.btGhostObject* %this1 to %class.btCollisionObject*
  %call = call %class.btCollisionObject* @_ZN17btCollisionObjectC2Ev(%class.btCollisionObject* %0)
  %1 = bitcast %class.btGhostObject* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [11 x i8*] }, { [11 x i8*] }* @_ZTV13btGhostObject, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_overlappingObjects = getelementptr inbounds %class.btGhostObject, %class.btGhostObject* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP17btCollisionObjectEC2Ev(%class.btAlignedObjectArray* %m_overlappingObjects)
  %2 = bitcast %class.btGhostObject* %this1 to %class.btCollisionObject*
  %m_internalType = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %2, i32 0, i32 20
  store i32 4, i32* %m_internalType, align 4, !tbaa !8
  ret %class.btGhostObject* %this1
}

declare %class.btCollisionObject* @_ZN17btCollisionObjectC2Ev(%class.btCollisionObject* returned) unnamed_addr #1

define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP17btCollisionObjectEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: nounwind
define hidden %class.btGhostObject* @_ZN13btGhostObjectD2Ev(%class.btGhostObject* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btGhostObject*, align 4
  store %class.btGhostObject* %this, %class.btGhostObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGhostObject*, %class.btGhostObject** %this.addr, align 4
  %0 = bitcast %class.btGhostObject* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [11 x i8*] }, { [11 x i8*] }* @_ZTV13btGhostObject, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  %m_overlappingObjects = getelementptr inbounds %class.btGhostObject, %class.btGhostObject* %this1, i32 0, i32 1
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP17btCollisionObjectED2Ev(%class.btAlignedObjectArray* %m_overlappingObjects) #9
  %1 = bitcast %class.btGhostObject* %this1 to %class.btCollisionObject*
  %call2 = call %class.btCollisionObject* @_ZN17btCollisionObjectD2Ev(%class.btCollisionObject* %1) #9
  ret %class.btGhostObject* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP17btCollisionObjectED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: nounwind
declare %class.btCollisionObject* @_ZN17btCollisionObjectD2Ev(%class.btCollisionObject* returned) unnamed_addr #3

; Function Attrs: nounwind
define hidden void @_ZN13btGhostObjectD0Ev(%class.btGhostObject* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btGhostObject*, align 4
  store %class.btGhostObject* %this, %class.btGhostObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGhostObject*, %class.btGhostObject** %this.addr, align 4
  %call = call %class.btGhostObject* @_ZN13btGhostObjectD1Ev(%class.btGhostObject* %this1) #9
  %0 = bitcast %class.btGhostObject* %this1 to i8*
  call void @_ZN17btCollisionObjectdlEPv(i8* %0) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN17btCollisionObjectdlEPv(i8* %ptr) #4 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

define hidden void @_ZN13btGhostObject28addOverlappingObjectInternalEP17btBroadphaseProxyS1_(%class.btGhostObject* %this, %struct.btBroadphaseProxy* %otherProxy, %struct.btBroadphaseProxy* %thisProxy) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btGhostObject*, align 4
  %otherProxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %thisProxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %otherObject = alloca %class.btCollisionObject*, align 4
  %index = alloca i32, align 4
  store %class.btGhostObject* %this, %class.btGhostObject** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %otherProxy, %struct.btBroadphaseProxy** %otherProxy.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %thisProxy, %struct.btBroadphaseProxy** %thisProxy.addr, align 4, !tbaa !2
  %this1 = load %class.btGhostObject*, %class.btGhostObject** %this.addr, align 4
  %0 = bitcast %class.btCollisionObject** %otherObject to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %otherProxy.addr, align 4, !tbaa !2
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %1, i32 0, i32 0
  %2 = load i8*, i8** %m_clientObject, align 4, !tbaa !15
  %3 = bitcast i8* %2 to %class.btCollisionObject*
  store %class.btCollisionObject* %3, %class.btCollisionObject** %otherObject, align 4, !tbaa !2
  %4 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %m_overlappingObjects = getelementptr inbounds %class.btGhostObject, %class.btGhostObject* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE16findLinearSearchERKS1_(%class.btAlignedObjectArray* %m_overlappingObjects, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %otherObject)
  store i32 %call, i32* %index, align 4, !tbaa !18
  %5 = load i32, i32* %index, align 4, !tbaa !18
  %m_overlappingObjects2 = getelementptr inbounds %class.btGhostObject, %class.btGhostObject* %this1, i32 0, i32 1
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_overlappingObjects2)
  %cmp = icmp eq i32 %5, %call3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_overlappingObjects4 = getelementptr inbounds %class.btGhostObject, %class.btGhostObject* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE9push_backERKS1_(%class.btAlignedObjectArray* %m_overlappingObjects4, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %otherObject)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %6 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  %7 = bitcast %class.btCollisionObject** %otherObject to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #5

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE16findLinearSearchERKS1_(%class.btAlignedObjectArray* %this, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %key) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %key.addr = alloca %class.btCollisionObject**, align 4
  %index = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject** %key, %class.btCollisionObject*** %key.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %index, align 4, !tbaa !18
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store i32 0, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !18
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp slt i32 %2, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4, !tbaa !19
  %4 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %3, i32 %4
  %5 = load %class.btCollisionObject*, %class.btCollisionObject** %arrayidx, align 4, !tbaa !2
  %6 = load %class.btCollisionObject**, %class.btCollisionObject*** %key.addr, align 4, !tbaa !2
  %7 = load %class.btCollisionObject*, %class.btCollisionObject** %6, align 4, !tbaa !2
  %cmp3 = icmp eq %class.btCollisionObject* %5, %7
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4, !tbaa !18
  store i32 %8, i32* %index, align 4, !tbaa !18
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %9 = load i32, i32* %i, align 4, !tbaa !18
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %10 = load i32, i32* %index, align 4, !tbaa !18
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  %12 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  ret i32 %10
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !23
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE9push_backERKS1_(%class.btAlignedObjectArray* %this, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %_Val) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %class.btCollisionObject**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject** %_Val, %class.btCollisionObject*** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !18
  %1 = load i32, i32* %sz, align 4, !tbaa !18
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP17btCollisionObjectE9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4, !tbaa !19
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !23
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %2, i32 %3
  %4 = bitcast %class.btCollisionObject** %arrayidx to i8*
  %5 = bitcast i8* %4 to %class.btCollisionObject**
  %6 = load %class.btCollisionObject**, %class.btCollisionObject*** %_Val.addr, align 4, !tbaa !2
  %7 = load %class.btCollisionObject*, %class.btCollisionObject** %6, align 4, !tbaa !2
  store %class.btCollisionObject* %7, %class.btCollisionObject** %5, align 4, !tbaa !2
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size5, align 4, !tbaa !23
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size5, align 4, !tbaa !23
  %9 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #5

define hidden void @_ZN13btGhostObject31removeOverlappingObjectInternalEP17btBroadphaseProxyP12btDispatcherS1_(%class.btGhostObject* %this, %struct.btBroadphaseProxy* %otherProxy, %class.btDispatcher* %dispatcher, %struct.btBroadphaseProxy* %thisProxy) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btGhostObject*, align 4
  %otherProxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %thisProxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %otherObject = alloca %class.btCollisionObject*, align 4
  %index = alloca i32, align 4
  store %class.btGhostObject* %this, %class.btGhostObject** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %otherProxy, %struct.btBroadphaseProxy** %otherProxy.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %thisProxy, %struct.btBroadphaseProxy** %thisProxy.addr, align 4, !tbaa !2
  %this1 = load %class.btGhostObject*, %class.btGhostObject** %this.addr, align 4
  %0 = bitcast %class.btCollisionObject** %otherObject to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %otherProxy.addr, align 4, !tbaa !2
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %1, i32 0, i32 0
  %2 = load i8*, i8** %m_clientObject, align 4, !tbaa !15
  %3 = bitcast i8* %2 to %class.btCollisionObject*
  store %class.btCollisionObject* %3, %class.btCollisionObject** %otherObject, align 4, !tbaa !2
  %4 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %m_overlappingObjects = getelementptr inbounds %class.btGhostObject, %class.btGhostObject* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE16findLinearSearchERKS1_(%class.btAlignedObjectArray* %m_overlappingObjects, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %otherObject)
  store i32 %call, i32* %index, align 4, !tbaa !18
  %5 = load i32, i32* %index, align 4, !tbaa !18
  %m_overlappingObjects2 = getelementptr inbounds %class.btGhostObject, %class.btGhostObject* %this1, i32 0, i32 1
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_overlappingObjects2)
  %cmp = icmp slt i32 %5, %call3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_overlappingObjects4 = getelementptr inbounds %class.btGhostObject, %class.btGhostObject* %this1, i32 0, i32 1
  %m_overlappingObjects5 = getelementptr inbounds %class.btGhostObject, %class.btGhostObject* %this1, i32 0, i32 1
  %call6 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_overlappingObjects5)
  %sub = sub nsw i32 %call6, 1
  %call7 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray* %m_overlappingObjects4, i32 %sub)
  %6 = load %class.btCollisionObject*, %class.btCollisionObject** %call7, align 4, !tbaa !2
  %m_overlappingObjects8 = getelementptr inbounds %class.btGhostObject, %class.btGhostObject* %this1, i32 0, i32 1
  %7 = load i32, i32* %index, align 4, !tbaa !18
  %call9 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray* %m_overlappingObjects8, i32 %7)
  store %class.btCollisionObject* %6, %class.btCollisionObject** %call9, align 4, !tbaa !2
  %m_overlappingObjects10 = getelementptr inbounds %class.btGhostObject, %class.btGhostObject* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE8pop_backEv(%class.btAlignedObjectArray* %m_overlappingObjects10)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %8 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #9
  %9 = bitcast %class.btCollisionObject** %otherObject to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray* %this, i32 %n) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !18
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4, !tbaa !19
  %1 = load i32, i32* %n.addr, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %0, i32 %1
  ret %class.btCollisionObject** %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE8pop_backEv(%class.btAlignedObjectArray* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !23
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4, !tbaa !23
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4, !tbaa !19
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4, !tbaa !23
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %1, i32 %2
  ret void
}

define hidden %class.btPairCachingGhostObject* @_ZN24btPairCachingGhostObjectC2Ev(%class.btPairCachingGhostObject* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btPairCachingGhostObject*, align 4
  store %class.btPairCachingGhostObject* %this, %class.btPairCachingGhostObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %this.addr, align 4
  %0 = bitcast %class.btPairCachingGhostObject* %this1 to %class.btGhostObject*
  %call = call %class.btGhostObject* @_ZN13btGhostObjectC2Ev(%class.btGhostObject* %0)
  %1 = bitcast %class.btPairCachingGhostObject* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [11 x i8*] }, { [11 x i8*] }* @_ZTV24btPairCachingGhostObject, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %call2 = call i8* @_Z22btAlignedAllocInternalmi(i32 76, i32 16)
  %2 = bitcast i8* %call2 to %class.btHashedOverlappingPairCache*
  %call3 = call %class.btHashedOverlappingPairCache* @_ZN28btHashedOverlappingPairCacheC1Ev(%class.btHashedOverlappingPairCache* %2)
  %m_hashPairCache = getelementptr inbounds %class.btPairCachingGhostObject, %class.btPairCachingGhostObject* %this1, i32 0, i32 1
  store %class.btHashedOverlappingPairCache* %2, %class.btHashedOverlappingPairCache** %m_hashPairCache, align 4, !tbaa !24
  ret %class.btPairCachingGhostObject* %this1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #1

declare %class.btHashedOverlappingPairCache* @_ZN28btHashedOverlappingPairCacheC1Ev(%class.btHashedOverlappingPairCache* returned) unnamed_addr #1

; Function Attrs: nounwind
define hidden %class.btPairCachingGhostObject* @_ZN24btPairCachingGhostObjectD2Ev(%class.btPairCachingGhostObject* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btPairCachingGhostObject*, align 4
  store %class.btPairCachingGhostObject* %this, %class.btPairCachingGhostObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %this.addr, align 4
  %0 = bitcast %class.btPairCachingGhostObject* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [11 x i8*] }, { [11 x i8*] }* @_ZTV24btPairCachingGhostObject, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  %m_hashPairCache = getelementptr inbounds %class.btPairCachingGhostObject, %class.btPairCachingGhostObject* %this1, i32 0, i32 1
  %1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %m_hashPairCache, align 4, !tbaa !24
  %2 = bitcast %class.btHashedOverlappingPairCache* %1 to %class.btHashedOverlappingPairCache* (%class.btHashedOverlappingPairCache*)***
  %vtable = load %class.btHashedOverlappingPairCache* (%class.btHashedOverlappingPairCache*)**, %class.btHashedOverlappingPairCache* (%class.btHashedOverlappingPairCache*)*** %2, align 4, !tbaa !6
  %vfn = getelementptr inbounds %class.btHashedOverlappingPairCache* (%class.btHashedOverlappingPairCache*)*, %class.btHashedOverlappingPairCache* (%class.btHashedOverlappingPairCache*)** %vtable, i64 0
  %3 = load %class.btHashedOverlappingPairCache* (%class.btHashedOverlappingPairCache*)*, %class.btHashedOverlappingPairCache* (%class.btHashedOverlappingPairCache*)** %vfn, align 4
  %call = call %class.btHashedOverlappingPairCache* %3(%class.btHashedOverlappingPairCache* %1) #9
  %m_hashPairCache2 = getelementptr inbounds %class.btPairCachingGhostObject, %class.btPairCachingGhostObject* %this1, i32 0, i32 1
  %4 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %m_hashPairCache2, align 4, !tbaa !24
  %5 = bitcast %class.btHashedOverlappingPairCache* %4 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %5)
  %6 = bitcast %class.btPairCachingGhostObject* %this1 to %class.btGhostObject*
  %call3 = call %class.btGhostObject* @_ZN13btGhostObjectD2Ev(%class.btGhostObject* %6) #9
  ret %class.btPairCachingGhostObject* %this1
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

; Function Attrs: nounwind
define hidden void @_ZN24btPairCachingGhostObjectD0Ev(%class.btPairCachingGhostObject* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btPairCachingGhostObject*, align 4
  store %class.btPairCachingGhostObject* %this, %class.btPairCachingGhostObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %this.addr, align 4
  %call = call %class.btPairCachingGhostObject* @_ZN24btPairCachingGhostObjectD1Ev(%class.btPairCachingGhostObject* %this1) #9
  %0 = bitcast %class.btPairCachingGhostObject* %this1 to i8*
  call void @_ZN17btCollisionObjectdlEPv(i8* %0) #9
  ret void
}

define hidden void @_ZN24btPairCachingGhostObject28addOverlappingObjectInternalEP17btBroadphaseProxyS1_(%class.btPairCachingGhostObject* %this, %struct.btBroadphaseProxy* %otherProxy, %struct.btBroadphaseProxy* %thisProxy) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btPairCachingGhostObject*, align 4
  %otherProxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %thisProxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %actualThisProxy = alloca %struct.btBroadphaseProxy*, align 4
  %otherObject = alloca %class.btCollisionObject*, align 4
  %index = alloca i32, align 4
  store %class.btPairCachingGhostObject* %this, %class.btPairCachingGhostObject** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %otherProxy, %struct.btBroadphaseProxy** %otherProxy.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %thisProxy, %struct.btBroadphaseProxy** %thisProxy.addr, align 4, !tbaa !2
  %this1 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %this.addr, align 4
  %0 = bitcast %struct.btBroadphaseProxy** %actualThisProxy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %thisProxy.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.btBroadphaseProxy* %1, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %thisProxy.addr, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = bitcast %class.btPairCachingGhostObject* %this1 to %class.btCollisionObject*
  %call = call %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %3)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.btBroadphaseProxy* [ %2, %cond.true ], [ %call, %cond.false ]
  store %struct.btBroadphaseProxy* %cond, %struct.btBroadphaseProxy** %actualThisProxy, align 4, !tbaa !2
  %4 = bitcast %class.btCollisionObject** %otherObject to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %otherProxy.addr, align 4, !tbaa !2
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %5, i32 0, i32 0
  %6 = load i8*, i8** %m_clientObject, align 4, !tbaa !15
  %7 = bitcast i8* %6 to %class.btCollisionObject*
  store %class.btCollisionObject* %7, %class.btCollisionObject** %otherObject, align 4, !tbaa !2
  %8 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  %9 = bitcast %class.btPairCachingGhostObject* %this1 to %class.btGhostObject*
  %m_overlappingObjects = getelementptr inbounds %class.btGhostObject, %class.btGhostObject* %9, i32 0, i32 1
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE16findLinearSearchERKS1_(%class.btAlignedObjectArray* %m_overlappingObjects, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %otherObject)
  store i32 %call2, i32* %index, align 4, !tbaa !18
  %10 = load i32, i32* %index, align 4, !tbaa !18
  %11 = bitcast %class.btPairCachingGhostObject* %this1 to %class.btGhostObject*
  %m_overlappingObjects3 = getelementptr inbounds %class.btGhostObject, %class.btGhostObject* %11, i32 0, i32 1
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_overlappingObjects3)
  %cmp = icmp eq i32 %10, %call4
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end
  %12 = bitcast %class.btPairCachingGhostObject* %this1 to %class.btGhostObject*
  %m_overlappingObjects5 = getelementptr inbounds %class.btGhostObject, %class.btGhostObject* %12, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE9push_backERKS1_(%class.btAlignedObjectArray* %m_overlappingObjects5, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %otherObject)
  %m_hashPairCache = getelementptr inbounds %class.btPairCachingGhostObject, %class.btPairCachingGhostObject* %this1, i32 0, i32 1
  %13 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %m_hashPairCache, align 4, !tbaa !24
  %14 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %actualThisProxy, align 4, !tbaa !2
  %15 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %otherProxy.addr, align 4, !tbaa !2
  %16 = bitcast %class.btHashedOverlappingPairCache* %13 to %struct.btBroadphasePair* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable = load %struct.btBroadphasePair* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, %struct.btBroadphasePair* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %16, align 4, !tbaa !6
  %vfn = getelementptr inbounds %struct.btBroadphasePair* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable, i64 2
  %17 = load %struct.btBroadphasePair* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call6 = call %struct.btBroadphasePair* %17(%class.btHashedOverlappingPairCache* %13, %struct.btBroadphaseProxy* %14, %struct.btBroadphaseProxy* %15)
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end
  %18 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #9
  %19 = bitcast %class.btCollisionObject** %otherObject to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #9
  %20 = bitcast %struct.btBroadphaseProxy** %actualThisProxy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_broadphaseHandle = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 8
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_broadphaseHandle, align 4, !tbaa !26
  ret %struct.btBroadphaseProxy* %0
}

define hidden void @_ZN24btPairCachingGhostObject31removeOverlappingObjectInternalEP17btBroadphaseProxyP12btDispatcherS1_(%class.btPairCachingGhostObject* %this, %struct.btBroadphaseProxy* %otherProxy, %class.btDispatcher* %dispatcher, %struct.btBroadphaseProxy* %thisProxy1) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btPairCachingGhostObject*, align 4
  %otherProxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %thisProxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  %otherObject = alloca %class.btCollisionObject*, align 4
  %actualThisProxy = alloca %struct.btBroadphaseProxy*, align 4
  %index = alloca i32, align 4
  store %class.btPairCachingGhostObject* %this, %class.btPairCachingGhostObject** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %otherProxy, %struct.btBroadphaseProxy** %otherProxy.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %thisProxy1, %struct.btBroadphaseProxy** %thisProxy1.addr, align 4, !tbaa !2
  %this1 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %this.addr, align 4
  %0 = bitcast %class.btCollisionObject** %otherObject to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %otherProxy.addr, align 4, !tbaa !2
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %1, i32 0, i32 0
  %2 = load i8*, i8** %m_clientObject, align 4, !tbaa !15
  %3 = bitcast i8* %2 to %class.btCollisionObject*
  store %class.btCollisionObject* %3, %class.btCollisionObject** %otherObject, align 4, !tbaa !2
  %4 = bitcast %struct.btBroadphaseProxy** %actualThisProxy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %thisProxy1.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.btBroadphaseProxy* %5, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %6 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %thisProxy1.addr, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %entry
  %7 = bitcast %class.btPairCachingGhostObject* %this1 to %class.btCollisionObject*
  %call = call %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %7)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.btBroadphaseProxy* [ %6, %cond.true ], [ %call, %cond.false ]
  store %struct.btBroadphaseProxy* %cond, %struct.btBroadphaseProxy** %actualThisProxy, align 4, !tbaa !2
  %8 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  %9 = bitcast %class.btPairCachingGhostObject* %this1 to %class.btGhostObject*
  %m_overlappingObjects = getelementptr inbounds %class.btGhostObject, %class.btGhostObject* %9, i32 0, i32 1
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE16findLinearSearchERKS1_(%class.btAlignedObjectArray* %m_overlappingObjects, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %otherObject)
  store i32 %call2, i32* %index, align 4, !tbaa !18
  %10 = load i32, i32* %index, align 4, !tbaa !18
  %11 = bitcast %class.btPairCachingGhostObject* %this1 to %class.btGhostObject*
  %m_overlappingObjects3 = getelementptr inbounds %class.btGhostObject, %class.btGhostObject* %11, i32 0, i32 1
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_overlappingObjects3)
  %cmp = icmp slt i32 %10, %call4
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end
  %12 = bitcast %class.btPairCachingGhostObject* %this1 to %class.btGhostObject*
  %m_overlappingObjects5 = getelementptr inbounds %class.btGhostObject, %class.btGhostObject* %12, i32 0, i32 1
  %13 = bitcast %class.btPairCachingGhostObject* %this1 to %class.btGhostObject*
  %m_overlappingObjects6 = getelementptr inbounds %class.btGhostObject, %class.btGhostObject* %13, i32 0, i32 1
  %call7 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_overlappingObjects6)
  %sub = sub nsw i32 %call7, 1
  %call8 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray* %m_overlappingObjects5, i32 %sub)
  %14 = load %class.btCollisionObject*, %class.btCollisionObject** %call8, align 4, !tbaa !2
  %15 = bitcast %class.btPairCachingGhostObject* %this1 to %class.btGhostObject*
  %m_overlappingObjects9 = getelementptr inbounds %class.btGhostObject, %class.btGhostObject* %15, i32 0, i32 1
  %16 = load i32, i32* %index, align 4, !tbaa !18
  %call10 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray* %m_overlappingObjects9, i32 %16)
  store %class.btCollisionObject* %14, %class.btCollisionObject** %call10, align 4, !tbaa !2
  %17 = bitcast %class.btPairCachingGhostObject* %this1 to %class.btGhostObject*
  %m_overlappingObjects11 = getelementptr inbounds %class.btGhostObject, %class.btGhostObject* %17, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE8pop_backEv(%class.btAlignedObjectArray* %m_overlappingObjects11)
  %m_hashPairCache = getelementptr inbounds %class.btPairCachingGhostObject, %class.btPairCachingGhostObject* %this1, i32 0, i32 1
  %18 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %m_hashPairCache, align 4, !tbaa !24
  %19 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %actualThisProxy, align 4, !tbaa !2
  %20 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %otherProxy.addr, align 4, !tbaa !2
  %21 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %22 = bitcast %class.btHashedOverlappingPairCache* %18 to i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable = load i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %22, align 4, !tbaa !6
  %vfn = getelementptr inbounds i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable, i64 3
  %23 = load i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn, align 4
  %call12 = call i8* %23(%class.btHashedOverlappingPairCache* %18, %struct.btBroadphaseProxy* %19, %struct.btBroadphaseProxy* %20, %class.btDispatcher* %21)
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end
  %24 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #9
  %25 = bitcast %struct.btBroadphaseProxy** %actualThisProxy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #9
  %26 = bitcast %class.btCollisionObject** %otherObject to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #9
  ret void
}

define hidden void @_ZNK13btGhostObject15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RN16btCollisionWorld20ConvexResultCallbackEf(%class.btGhostObject* %this, %class.btConvexShape* %castShape, %class.btTransform* nonnull align 4 dereferenceable(64) %convexFromWorld, %class.btTransform* nonnull align 4 dereferenceable(64) %convexToWorld, %"struct.btCollisionWorld::ConvexResultCallback"* nonnull align 4 dereferenceable(12) %resultCallback, float %allowedCcdPenetration) #0 {
entry:
  %this.addr = alloca %class.btGhostObject*, align 4
  %castShape.addr = alloca %class.btConvexShape*, align 4
  %convexFromWorld.addr = alloca %class.btTransform*, align 4
  %convexToWorld.addr = alloca %class.btTransform*, align 4
  %resultCallback.addr = alloca %"struct.btCollisionWorld::ConvexResultCallback"*, align 4
  %allowedCcdPenetration.addr = alloca float, align 4
  %convexFromTrans = alloca %class.btTransform, align 4
  %convexToTrans = alloca %class.btTransform, align 4
  %castShapeAabbMin = alloca %class.btVector3, align 4
  %castShapeAabbMax = alloca %class.btVector3, align 4
  %linVel = alloca %class.btVector3, align 4
  %angVel = alloca %class.btVector3, align 4
  %R = alloca %class.btTransform, align 4
  %ref.tmp = alloca %class.btQuaternion, align 4
  %i = alloca i32, align 4
  %collisionObject = alloca %class.btCollisionObject*, align 4
  %collisionObjectAabbMin = alloca %class.btVector3, align 4
  %collisionObjectAabbMax = alloca %class.btVector3, align 4
  %hitLambda = alloca float, align 4
  %hitNormal = alloca %class.btVector3, align 4
  store %class.btGhostObject* %this, %class.btGhostObject** %this.addr, align 4, !tbaa !2
  store %class.btConvexShape* %castShape, %class.btConvexShape** %castShape.addr, align 4, !tbaa !2
  store %class.btTransform* %convexFromWorld, %class.btTransform** %convexFromWorld.addr, align 4, !tbaa !2
  store %class.btTransform* %convexToWorld, %class.btTransform** %convexToWorld.addr, align 4, !tbaa !2
  store %"struct.btCollisionWorld::ConvexResultCallback"* %resultCallback, %"struct.btCollisionWorld::ConvexResultCallback"** %resultCallback.addr, align 4, !tbaa !2
  store float %allowedCcdPenetration, float* %allowedCcdPenetration.addr, align 4, !tbaa !27
  %this1 = load %class.btGhostObject*, %class.btGhostObject** %this.addr, align 4
  %0 = bitcast %class.btTransform* %convexFromTrans to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %0) #9
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %convexFromTrans)
  %1 = bitcast %class.btTransform* %convexToTrans to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %1) #9
  %call2 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %convexToTrans)
  %2 = load %class.btTransform*, %class.btTransform** %convexFromWorld.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %convexFromTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %2)
  %3 = load %class.btTransform*, %class.btTransform** %convexToWorld.addr, align 4, !tbaa !2
  %call4 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %convexToTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %3)
  %4 = bitcast %class.btVector3* %castShapeAabbMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #9
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %castShapeAabbMin)
  %5 = bitcast %class.btVector3* %castShapeAabbMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #9
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %castShapeAabbMax)
  %6 = bitcast %class.btVector3* %linVel to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #9
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %linVel)
  %7 = bitcast %class.btVector3* %angVel to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #9
  %call8 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %angVel)
  call void @_ZN15btTransformUtil17calculateVelocityERK11btTransformS2_fR9btVector3S4_(%class.btTransform* nonnull align 4 dereferenceable(64) %convexFromTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %convexToTrans, float 1.000000e+00, %class.btVector3* nonnull align 4 dereferenceable(16) %linVel, %class.btVector3* nonnull align 4 dereferenceable(16) %angVel)
  %8 = bitcast %class.btTransform* %R to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %8) #9
  %call9 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %R)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %R)
  %9 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #9
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp, %class.btTransform* %convexFromTrans)
  call void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %R, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp)
  %10 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %10) #9
  %11 = load %class.btConvexShape*, %class.btConvexShape** %castShape.addr, align 4, !tbaa !2
  %12 = bitcast %class.btConvexShape* %11 to %class.btCollisionShape*
  call void @_ZNK16btCollisionShape21calculateTemporalAabbERK11btTransformRK9btVector3S5_fRS3_S6_(%class.btCollisionShape* %12, %class.btTransform* nonnull align 4 dereferenceable(64) %R, %class.btVector3* nonnull align 4 dereferenceable(16) %linVel, %class.btVector3* nonnull align 4 dereferenceable(16) %angVel, float 1.000000e+00, %class.btVector3* nonnull align 4 dereferenceable(16) %castShapeAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %castShapeAabbMax)
  %13 = bitcast %class.btTransform* %R to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %13) #9
  %14 = bitcast %class.btVector3* %angVel to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %14) #9
  %15 = bitcast %class.btVector3* %linVel to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %15) #9
  %16 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #9
  store i32 0, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %17 = load i32, i32* %i, align 4, !tbaa !18
  %m_overlappingObjects = getelementptr inbounds %class.btGhostObject, %class.btGhostObject* %this1, i32 0, i32 1
  %call10 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_overlappingObjects)
  %cmp = icmp slt i32 %17, %call10
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %18 = bitcast %class.btCollisionObject** %collisionObject to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #9
  %m_overlappingObjects11 = getelementptr inbounds %class.btGhostObject, %class.btGhostObject* %this1, i32 0, i32 1
  %19 = load i32, i32* %i, align 4, !tbaa !18
  %call12 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZNK20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray* %m_overlappingObjects11, i32 %19)
  %20 = load %class.btCollisionObject*, %class.btCollisionObject** %call12, align 4, !tbaa !2
  store %class.btCollisionObject* %20, %class.btCollisionObject** %collisionObject, align 4, !tbaa !2
  %21 = load %"struct.btCollisionWorld::ConvexResultCallback"*, %"struct.btCollisionWorld::ConvexResultCallback"** %resultCallback.addr, align 4, !tbaa !2
  %22 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4, !tbaa !2
  %call13 = call %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %22)
  %23 = bitcast %"struct.btCollisionWorld::ConvexResultCallback"* %21 to i1 (%"struct.btCollisionWorld::ConvexResultCallback"*, %struct.btBroadphaseProxy*)***
  %vtable = load i1 (%"struct.btCollisionWorld::ConvexResultCallback"*, %struct.btBroadphaseProxy*)**, i1 (%"struct.btCollisionWorld::ConvexResultCallback"*, %struct.btBroadphaseProxy*)*** %23, align 4, !tbaa !6
  %vfn = getelementptr inbounds i1 (%"struct.btCollisionWorld::ConvexResultCallback"*, %struct.btBroadphaseProxy*)*, i1 (%"struct.btCollisionWorld::ConvexResultCallback"*, %struct.btBroadphaseProxy*)** %vtable, i64 2
  %24 = load i1 (%"struct.btCollisionWorld::ConvexResultCallback"*, %struct.btBroadphaseProxy*)*, i1 (%"struct.btCollisionWorld::ConvexResultCallback"*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call14 = call zeroext i1 %24(%"struct.btCollisionWorld::ConvexResultCallback"* %21, %struct.btBroadphaseProxy* %call13)
  br i1 %call14, label %if.then, label %if.end28

if.then:                                          ; preds = %for.body
  %25 = bitcast %class.btVector3* %collisionObjectAabbMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %25) #9
  %call15 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %collisionObjectAabbMin)
  %26 = bitcast %class.btVector3* %collisionObjectAabbMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %26) #9
  %call16 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %collisionObjectAabbMax)
  %27 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4, !tbaa !2
  %call17 = call %class.btCollisionShape* @_ZN17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %27)
  %28 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4, !tbaa !2
  %call18 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %28)
  %29 = bitcast %class.btCollisionShape* %call17 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable19 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %29, align 4, !tbaa !6
  %vfn20 = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable19, i64 2
  %30 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn20, align 4
  call void %30(%class.btCollisionShape* %call17, %class.btTransform* nonnull align 4 dereferenceable(64) %call18, %class.btVector3* nonnull align 4 dereferenceable(16) %collisionObjectAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %collisionObjectAabbMax)
  call void @_Z10AabbExpandR9btVector3S0_RKS_S2_(%class.btVector3* nonnull align 4 dereferenceable(16) %collisionObjectAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %collisionObjectAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %castShapeAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %castShapeAabbMax)
  %31 = bitcast float* %hitLambda to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #9
  store float 1.000000e+00, float* %hitLambda, align 4, !tbaa !27
  %32 = bitcast %class.btVector3* %hitNormal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %32) #9
  %call21 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %hitNormal)
  %33 = load %class.btTransform*, %class.btTransform** %convexFromWorld.addr, align 4, !tbaa !2
  %call22 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %33)
  %34 = load %class.btTransform*, %class.btTransform** %convexToWorld.addr, align 4, !tbaa !2
  %call23 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %34)
  %call24 = call zeroext i1 @_Z9btRayAabbRK9btVector3S1_S1_S1_RfRS_(%class.btVector3* nonnull align 4 dereferenceable(16) %call22, %class.btVector3* nonnull align 4 dereferenceable(16) %call23, %class.btVector3* nonnull align 4 dereferenceable(16) %collisionObjectAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %collisionObjectAabbMax, float* nonnull align 4 dereferenceable(4) %hitLambda, %class.btVector3* nonnull align 4 dereferenceable(16) %hitNormal)
  br i1 %call24, label %if.then25, label %if.end

if.then25:                                        ; preds = %if.then
  %35 = load %class.btConvexShape*, %class.btConvexShape** %castShape.addr, align 4, !tbaa !2
  %36 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4, !tbaa !2
  %37 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4, !tbaa !2
  %call26 = call %class.btCollisionShape* @_ZN17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %37)
  %38 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4, !tbaa !2
  %call27 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %38)
  %39 = load %"struct.btCollisionWorld::ConvexResultCallback"*, %"struct.btCollisionWorld::ConvexResultCallback"** %resultCallback.addr, align 4, !tbaa !2
  %40 = load float, float* %allowedCcdPenetration.addr, align 4, !tbaa !27
  call void @_ZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEf(%class.btConvexShape* %35, %class.btTransform* nonnull align 4 dereferenceable(64) %convexFromTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %convexToTrans, %class.btCollisionObject* %36, %class.btCollisionShape* %call26, %class.btTransform* nonnull align 4 dereferenceable(64) %call27, %"struct.btCollisionWorld::ConvexResultCallback"* nonnull align 4 dereferenceable(12) %39, float %40)
  br label %if.end

if.end:                                           ; preds = %if.then25, %if.then
  %41 = bitcast %class.btVector3* %hitNormal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %41) #9
  %42 = bitcast float* %hitLambda to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #9
  %43 = bitcast %class.btVector3* %collisionObjectAabbMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %43) #9
  %44 = bitcast %class.btVector3* %collisionObjectAabbMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %44) #9
  br label %if.end28

if.end28:                                         ; preds = %if.end, %for.body
  %45 = bitcast %class.btCollisionObject** %collisionObject to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #9
  br label %for.inc

for.inc:                                          ; preds = %if.end28
  %46 = load i32, i32* %i, align 4, !tbaa !18
  %inc = add nsw i32 %46, 1
  store i32 %inc, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %47 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #9
  %48 = bitcast %class.btVector3* %castShapeAabbMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %48) #9
  %49 = bitcast %class.btVector3* %castShapeAabbMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %49) #9
  %50 = bitcast %class.btTransform* %convexToTrans to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %50) #9
  %51 = bitcast %class.btTransform* %convexFromTrans to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %51) #9
  ret void
}

define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #6 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !28
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

define linkonce_odr hidden void @_ZN15btTransformUtil17calculateVelocityERK11btTransformS2_fR9btVector3S4_(%class.btTransform* nonnull align 4 dereferenceable(64) %transform0, %class.btTransform* nonnull align 4 dereferenceable(64) %transform1, float %timeStep, %class.btVector3* nonnull align 4 dereferenceable(16) %linVel, %class.btVector3* nonnull align 4 dereferenceable(16) %angVel) #0 comdat {
entry:
  %transform0.addr = alloca %class.btTransform*, align 4
  %transform1.addr = alloca %class.btTransform*, align 4
  %timeStep.addr = alloca float, align 4
  %linVel.addr = alloca %class.btVector3*, align 4
  %angVel.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca %class.btVector3, align 4
  %axis = alloca %class.btVector3, align 4
  %angle = alloca float, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  store %class.btTransform* %transform0, %class.btTransform** %transform0.addr, align 4, !tbaa !2
  store %class.btTransform* %transform1, %class.btTransform** %transform1.addr, align 4, !tbaa !2
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !27
  store %class.btVector3* %linVel, %class.btVector3** %linVel.addr, align 4, !tbaa !2
  store %class.btVector3* %angVel, %class.btVector3** %angVel.addr, align 4, !tbaa !2
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = bitcast %class.btVector3* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #9
  %2 = load %class.btTransform*, %class.btTransform** %transform1.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %2)
  %3 = load %class.btTransform*, %class.btTransform** %transform0.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %3)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp1, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call2)
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %timeStep.addr)
  %4 = load %class.btVector3*, %class.btVector3** %linVel.addr, align 4, !tbaa !2
  %5 = bitcast %class.btVector3* %4 to i8*
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false), !tbaa.struct !28
  %7 = bitcast %class.btVector3* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %7) #9
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #9
  %9 = bitcast %class.btVector3* %axis to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #9
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %axis)
  %10 = bitcast float* %angle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btTransform*, %class.btTransform** %transform0.addr, align 4, !tbaa !2
  %12 = load %class.btTransform*, %class.btTransform** %transform1.addr, align 4, !tbaa !2
  call void @_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf(%class.btTransform* nonnull align 4 dereferenceable(64) %11, %class.btTransform* nonnull align 4 dereferenceable(64) %12, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, float* nonnull align 4 dereferenceable(4) %angle)
  %13 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #9
  %14 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %14) #9
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, float* nonnull align 4 dereferenceable(4) %angle)
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %timeStep.addr)
  %15 = load %class.btVector3*, %class.btVector3** %angVel.addr, align 4, !tbaa !2
  %16 = bitcast %class.btVector3* %15 to i8*
  %17 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false), !tbaa.struct !28
  %18 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #9
  %19 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %19) #9
  %20 = bitcast float* %angle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #9
  %21 = bitcast %class.btVector3* %axis to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %21) #9
  ret void
}

define linkonce_odr hidden void @_ZN11btTransform11setIdentityEv(%class.btTransform* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !27
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !27
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !27
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_origin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #6 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %m_basis, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0)
  ret void
}

define linkonce_odr hidden void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btTransform* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %agg.result)
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %m_basis, %class.btQuaternion* nonnull align 4 dereferenceable(16) %agg.result)
  ret void
}

declare void @_ZNK16btCollisionShape21calculateTemporalAabbERK11btTransformRK9btVector3S5_fRS3_S6_(%class.btCollisionShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), float, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZNK20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray* %this, i32 %n) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !18
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4, !tbaa !19
  %1 = load i32, i32* %n.addr, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %0, i32 %1
  ret %class.btCollisionObject** %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionShape* @_ZN17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 9
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %m_collisionShape, align 4, !tbaa !30
  ret %class.btCollisionShape* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_Z10AabbExpandR9btVector3S0_RKS_S2_(%class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %expansionMin, %class.btVector3* nonnull align 4 dereferenceable(16) %expansionMax) #6 comdat {
entry:
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %expansionMin.addr = alloca %class.btVector3*, align 4
  %expansionMax.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca %class.btVector3, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  store %class.btVector3* %expansionMin, %class.btVector3** %expansionMin.addr, align 4, !tbaa !2
  store %class.btVector3* %expansionMax, %class.btVector3** %expansionMax.addr, align 4, !tbaa !2
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %expansionMin.addr, align 4, !tbaa !2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  %3 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %4 = bitcast %class.btVector3* %3 to i8*
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !28
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %6) #9
  %7 = bitcast %class.btVector3* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #9
  %8 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %9 = load %class.btVector3*, %class.btVector3** %expansionMax.addr, align 4, !tbaa !2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp1, %class.btVector3* nonnull align 4 dereferenceable(16) %8, %class.btVector3* nonnull align 4 dereferenceable(16) %9)
  %10 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %11 = bitcast %class.btVector3* %10 to i8*
  %12 = bitcast %class.btVector3* %ref.tmp1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 16, i1 false), !tbaa.struct !28
  %13 = bitcast %class.btVector3* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %13) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_Z9btRayAabbRK9btVector3S1_S1_S1_RfRS_(%class.btVector3* nonnull align 4 dereferenceable(16) %rayFrom, %class.btVector3* nonnull align 4 dereferenceable(16) %rayTo, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, float* nonnull align 4 dereferenceable(4) %param, %class.btVector3* nonnull align 4 dereferenceable(16) %normal) #6 comdat {
entry:
  %retval = alloca i1, align 1
  %rayFrom.addr = alloca %class.btVector3*, align 4
  %rayTo.addr = alloca %class.btVector3*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %param.addr = alloca float*, align 4
  %normal.addr = alloca %class.btVector3*, align 4
  %aabbHalfExtent = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca float, align 4
  %aabbCenter = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca float, align 4
  %source = alloca %class.btVector3, align 4
  %target = alloca %class.btVector3, align 4
  %sourceOutcode = alloca i32, align 4
  %targetOutcode = alloca i32, align 4
  %lambda_enter = alloca float, align 4
  %lambda_exit = alloca float, align 4
  %r = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  %normSign = alloca float, align 4
  %hitNormal = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %bit = alloca i32, align 4
  %j = alloca i32, align 4
  %lambda = alloca float, align 4
  %ref.tmp22 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp24 = alloca float, align 4
  %lambda30 = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btVector3* %rayFrom, %class.btVector3** %rayFrom.addr, align 4, !tbaa !2
  store %class.btVector3* %rayTo, %class.btVector3** %rayTo.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  store float* %param, float** %param.addr, align 4, !tbaa !2
  store %class.btVector3* %normal, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %0 = bitcast %class.btVector3* %aabbHalfExtent to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #9
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %3 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  %4 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  store float 5.000000e-01, float* %ref.tmp1, align 4, !tbaa !27
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %aabbHalfExtent, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1)
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #9
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %6) #9
  %7 = bitcast %class.btVector3* %aabbCenter to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #9
  %8 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #9
  %9 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %10 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %9, %class.btVector3* nonnull align 4 dereferenceable(16) %10)
  %11 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  store float 5.000000e-01, float* %ref.tmp3, align 4, !tbaa !27
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %aabbCenter, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %12 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  %13 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %13) #9
  %14 = bitcast %class.btVector3* %source to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %14) #9
  %15 = load %class.btVector3*, %class.btVector3** %rayFrom.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %source, %class.btVector3* nonnull align 4 dereferenceable(16) %15, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbCenter)
  %16 = bitcast %class.btVector3* %target to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #9
  %17 = load %class.btVector3*, %class.btVector3** %rayTo.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %target, %class.btVector3* nonnull align 4 dereferenceable(16) %17, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbCenter)
  %18 = bitcast i32* %sourceOutcode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #9
  %call = call i32 @_Z9btOutcodeRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %source, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbHalfExtent)
  store i32 %call, i32* %sourceOutcode, align 4, !tbaa !18
  %19 = bitcast i32* %targetOutcode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #9
  %call4 = call i32 @_Z9btOutcodeRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %target, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbHalfExtent)
  store i32 %call4, i32* %targetOutcode, align 4, !tbaa !18
  %20 = load i32, i32* %sourceOutcode, align 4, !tbaa !18
  %21 = load i32, i32* %targetOutcode, align 4, !tbaa !18
  %and = and i32 %20, %21
  %cmp = icmp eq i32 %and, 0
  br i1 %cmp, label %if.then, label %if.end55

if.then:                                          ; preds = %entry
  %22 = bitcast float* %lambda_enter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #9
  store float 0.000000e+00, float* %lambda_enter, align 4, !tbaa !27
  %23 = bitcast float* %lambda_exit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #9
  %24 = load float*, float** %param.addr, align 4, !tbaa !2
  %25 = load float, float* %24, align 4, !tbaa !27
  store float %25, float* %lambda_exit, align 4, !tbaa !27
  %26 = bitcast %class.btVector3* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %26) #9
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %r, %class.btVector3* nonnull align 4 dereferenceable(16) %target, %class.btVector3* nonnull align 4 dereferenceable(16) %source)
  %27 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #9
  %28 = bitcast float* %normSign to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #9
  store float 1.000000e+00, float* %normSign, align 4, !tbaa !27
  %29 = bitcast %class.btVector3* %hitNormal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %29) #9
  %30 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #9
  store float 0.000000e+00, float* %ref.tmp5, align 4, !tbaa !27
  %31 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #9
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !27
  %32 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #9
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !27
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %hitNormal, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %33 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #9
  %34 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #9
  %35 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #9
  %36 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #9
  store i32 1, i32* %bit, align 4, !tbaa !18
  %37 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #9
  store i32 0, i32* %j, align 4, !tbaa !18
  br label %for.cond

for.cond:                                         ; preds = %for.inc43, %if.then
  %38 = load i32, i32* %j, align 4, !tbaa !18
  %cmp9 = icmp slt i32 %38, 2
  br i1 %cmp9, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %39 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #9
  br label %for.end45

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !18
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc, %for.body
  %40 = load i32, i32* %i, align 4, !tbaa !18
  %cmp11 = icmp ne i32 %40, 3
  br i1 %cmp11, label %for.body12, label %for.end

for.body12:                                       ; preds = %for.cond10
  %41 = load i32, i32* %sourceOutcode, align 4, !tbaa !18
  %42 = load i32, i32* %bit, align 4, !tbaa !18
  %and13 = and i32 %41, %42
  %tobool = icmp ne i32 %and13, 0
  br i1 %tobool, label %if.then14, label %if.else

if.then14:                                        ; preds = %for.body12
  %43 = bitcast float* %lambda to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #9
  %call15 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %source)
  %44 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds float, float* %call15, i32 %44
  %45 = load float, float* %arrayidx, align 4, !tbaa !27
  %fneg = fneg float %45
  %call16 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %aabbHalfExtent)
  %46 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx17 = getelementptr inbounds float, float* %call16, i32 %46
  %47 = load float, float* %arrayidx17, align 4, !tbaa !27
  %48 = load float, float* %normSign, align 4, !tbaa !27
  %mul = fmul float %47, %48
  %sub = fsub float %fneg, %mul
  %call18 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %r)
  %49 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 %49
  %50 = load float, float* %arrayidx19, align 4, !tbaa !27
  %div = fdiv float %sub, %50
  store float %div, float* %lambda, align 4, !tbaa !27
  %51 = load float, float* %lambda_enter, align 4, !tbaa !27
  %52 = load float, float* %lambda, align 4, !tbaa !27
  %cmp20 = fcmp ole float %51, %52
  br i1 %cmp20, label %if.then21, label %if.end

if.then21:                                        ; preds = %if.then14
  %53 = load float, float* %lambda, align 4, !tbaa !27
  store float %53, float* %lambda_enter, align 4, !tbaa !27
  %54 = bitcast float* %ref.tmp22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #9
  store float 0.000000e+00, float* %ref.tmp22, align 4, !tbaa !27
  %55 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #9
  store float 0.000000e+00, float* %ref.tmp23, align 4, !tbaa !27
  %56 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #9
  store float 0.000000e+00, float* %ref.tmp24, align 4, !tbaa !27
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %hitNormal, float* nonnull align 4 dereferenceable(4) %ref.tmp22, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp24)
  %57 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #9
  %58 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #9
  %59 = bitcast float* %ref.tmp22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #9
  %60 = load float, float* %normSign, align 4, !tbaa !27
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %hitNormal)
  %61 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 %61
  store float %60, float* %arrayidx26, align 4, !tbaa !27
  br label %if.end

if.end:                                           ; preds = %if.then21, %if.then14
  %62 = bitcast float* %lambda to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #9
  br label %if.end42

if.else:                                          ; preds = %for.body12
  %63 = load i32, i32* %targetOutcode, align 4, !tbaa !18
  %64 = load i32, i32* %bit, align 4, !tbaa !18
  %and27 = and i32 %63, %64
  %tobool28 = icmp ne i32 %and27, 0
  br i1 %tobool28, label %if.then29, label %if.end41

if.then29:                                        ; preds = %if.else
  %65 = bitcast float* %lambda30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %65) #9
  %call31 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %source)
  %66 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx32 = getelementptr inbounds float, float* %call31, i32 %66
  %67 = load float, float* %arrayidx32, align 4, !tbaa !27
  %fneg33 = fneg float %67
  %call34 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %aabbHalfExtent)
  %68 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 %68
  %69 = load float, float* %arrayidx35, align 4, !tbaa !27
  %70 = load float, float* %normSign, align 4, !tbaa !27
  %mul36 = fmul float %69, %70
  %sub37 = fsub float %fneg33, %mul36
  %call38 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %r)
  %71 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx39 = getelementptr inbounds float, float* %call38, i32 %71
  %72 = load float, float* %arrayidx39, align 4, !tbaa !27
  %div40 = fdiv float %sub37, %72
  store float %div40, float* %lambda30, align 4, !tbaa !27
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %lambda_exit, float* nonnull align 4 dereferenceable(4) %lambda30)
  %73 = bitcast float* %lambda30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #9
  br label %if.end41

if.end41:                                         ; preds = %if.then29, %if.else
  br label %if.end42

if.end42:                                         ; preds = %if.end41, %if.end
  %74 = load i32, i32* %bit, align 4, !tbaa !18
  %shl = shl i32 %74, 1
  store i32 %shl, i32* %bit, align 4, !tbaa !18
  br label %for.inc

for.inc:                                          ; preds = %if.end42
  %75 = load i32, i32* %i, align 4, !tbaa !18
  %inc = add nsw i32 %75, 1
  store i32 %inc, i32* %i, align 4, !tbaa !18
  br label %for.cond10

for.end:                                          ; preds = %for.cond10
  store float -1.000000e+00, float* %normSign, align 4, !tbaa !27
  br label %for.inc43

for.inc43:                                        ; preds = %for.end
  %76 = load i32, i32* %j, align 4, !tbaa !18
  %inc44 = add nsw i32 %76, 1
  store i32 %inc44, i32* %j, align 4, !tbaa !18
  br label %for.cond

for.end45:                                        ; preds = %for.cond.cleanup
  %77 = load float, float* %lambda_enter, align 4, !tbaa !27
  %78 = load float, float* %lambda_exit, align 4, !tbaa !27
  %cmp46 = fcmp ole float %77, %78
  br i1 %cmp46, label %if.then47, label %if.end48

if.then47:                                        ; preds = %for.end45
  %79 = load float, float* %lambda_enter, align 4, !tbaa !27
  %80 = load float*, float** %param.addr, align 4, !tbaa !2
  store float %79, float* %80, align 4, !tbaa !27
  %81 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %82 = bitcast %class.btVector3* %81 to i8*
  %83 = bitcast %class.btVector3* %hitNormal to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %82, i8* align 4 %83, i32 16, i1 false), !tbaa.struct !28
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end48:                                         ; preds = %for.end45
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end48, %if.then47
  %84 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #9
  %85 = bitcast %class.btVector3* %hitNormal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %85) #9
  %86 = bitcast float* %normSign to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #9
  %87 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #9
  %88 = bitcast %class.btVector3* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %88) #9
  %89 = bitcast float* %lambda_exit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #9
  %90 = bitcast float* %lambda_enter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #9
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup56 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end55

if.end55:                                         ; preds = %cleanup.cont, %entry
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup56

cleanup56:                                        ; preds = %if.end55, %cleanup
  %91 = bitcast i32* %targetOutcode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #9
  %92 = bitcast i32* %sourceOutcode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #9
  %93 = bitcast %class.btVector3* %target to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %93) #9
  %94 = bitcast %class.btVector3* %source to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %94) #9
  %95 = bitcast %class.btVector3* %aabbCenter to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %95) #9
  %96 = bitcast %class.btVector3* %aabbHalfExtent to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %96) #9
  %97 = load i1, i1* %retval, align 1
  ret i1 %97
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

declare void @_ZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEf(%class.btConvexShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btTransform* nonnull align 4 dereferenceable(64), %class.btCollisionObject*, %class.btCollisionShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %"struct.btCollisionWorld::ConvexResultCallback"* nonnull align 4 dereferenceable(12), float) #1

define hidden void @_ZNK13btGhostObject7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE(%class.btGhostObject* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %rayFromWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %rayToWorld, %"struct.btCollisionWorld::RayResultCallback"* nonnull align 4 dereferenceable(20) %resultCallback) #0 {
entry:
  %this.addr = alloca %class.btGhostObject*, align 4
  %rayFromWorld.addr = alloca %class.btVector3*, align 4
  %rayToWorld.addr = alloca %class.btVector3*, align 4
  %resultCallback.addr = alloca %"struct.btCollisionWorld::RayResultCallback"*, align 4
  %rayFromTrans = alloca %class.btTransform, align 4
  %rayToTrans = alloca %class.btTransform, align 4
  %i = alloca i32, align 4
  %collisionObject = alloca %class.btCollisionObject*, align 4
  store %class.btGhostObject* %this, %class.btGhostObject** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %rayFromWorld, %class.btVector3** %rayFromWorld.addr, align 4, !tbaa !2
  store %class.btVector3* %rayToWorld, %class.btVector3** %rayToWorld.addr, align 4, !tbaa !2
  store %"struct.btCollisionWorld::RayResultCallback"* %resultCallback, %"struct.btCollisionWorld::RayResultCallback"** %resultCallback.addr, align 4, !tbaa !2
  %this1 = load %class.btGhostObject*, %class.btGhostObject** %this.addr, align 4
  %0 = bitcast %class.btTransform* %rayFromTrans to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %0) #9
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %rayFromTrans)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %rayFromTrans)
  %1 = load %class.btVector3*, %class.btVector3** %rayFromWorld.addr, align 4, !tbaa !2
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %rayFromTrans, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %2 = bitcast %class.btTransform* %rayToTrans to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %2) #9
  %call2 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %rayToTrans)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %rayToTrans)
  %3 = load %class.btVector3*, %class.btVector3** %rayToWorld.addr, align 4, !tbaa !2
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %rayToTrans, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  store i32 0, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %5 = load i32, i32* %i, align 4, !tbaa !18
  %m_overlappingObjects = getelementptr inbounds %class.btGhostObject, %class.btGhostObject* %this1, i32 0, i32 1
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_overlappingObjects)
  %cmp = icmp slt i32 %5, %call3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %6 = bitcast %class.btCollisionObject** %collisionObject to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %m_overlappingObjects4 = getelementptr inbounds %class.btGhostObject, %class.btGhostObject* %this1, i32 0, i32 1
  %7 = load i32, i32* %i, align 4, !tbaa !18
  %call5 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZNK20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray* %m_overlappingObjects4, i32 %7)
  %8 = load %class.btCollisionObject*, %class.btCollisionObject** %call5, align 4, !tbaa !2
  store %class.btCollisionObject* %8, %class.btCollisionObject** %collisionObject, align 4, !tbaa !2
  %9 = load %"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::RayResultCallback"** %resultCallback.addr, align 4, !tbaa !2
  %10 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4, !tbaa !2
  %call6 = call %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %10)
  %11 = bitcast %"struct.btCollisionWorld::RayResultCallback"* %9 to i1 (%"struct.btCollisionWorld::RayResultCallback"*, %struct.btBroadphaseProxy*)***
  %vtable = load i1 (%"struct.btCollisionWorld::RayResultCallback"*, %struct.btBroadphaseProxy*)**, i1 (%"struct.btCollisionWorld::RayResultCallback"*, %struct.btBroadphaseProxy*)*** %11, align 4, !tbaa !6
  %vfn = getelementptr inbounds i1 (%"struct.btCollisionWorld::RayResultCallback"*, %struct.btBroadphaseProxy*)*, i1 (%"struct.btCollisionWorld::RayResultCallback"*, %struct.btBroadphaseProxy*)** %vtable, i64 2
  %12 = load i1 (%"struct.btCollisionWorld::RayResultCallback"*, %struct.btBroadphaseProxy*)*, i1 (%"struct.btCollisionWorld::RayResultCallback"*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call7 = call zeroext i1 %12(%"struct.btCollisionWorld::RayResultCallback"* %9, %struct.btBroadphaseProxy* %call6)
  br i1 %call7, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %13 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4, !tbaa !2
  %14 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4, !tbaa !2
  %call8 = call %class.btCollisionShape* @_ZN17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %14)
  %15 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %15)
  %16 = load %"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::RayResultCallback"** %resultCallback.addr, align 4, !tbaa !2
  call void @_ZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackE(%class.btTransform* nonnull align 4 dereferenceable(64) %rayFromTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %rayToTrans, %class.btCollisionObject* %13, %class.btCollisionShape* %call8, %class.btTransform* nonnull align 4 dereferenceable(64) %call9, %"struct.btCollisionWorld::RayResultCallback"* nonnull align 4 dereferenceable(20) %16)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %17 = bitcast %class.btCollisionObject** %collisionObject to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %18 = load i32, i32* %i, align 4, !tbaa !18
  %inc = add nsw i32 %18, 1
  store i32 %inc, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %19 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #9
  %20 = bitcast %class.btTransform* %rayToTrans to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %20) #9
  %21 = bitcast %class.btTransform* %rayFromTrans to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %21) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %origin) #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %origin.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %origin, %class.btVector3** %origin.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %origin.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_origin to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !28
  ret void
}

declare void @_ZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackE(%class.btTransform* nonnull align 4 dereferenceable(64), %class.btTransform* nonnull align 4 dereferenceable(64), %class.btCollisionObject*, %class.btCollisionShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %"struct.btCollisionWorld::RayResultCallback"* nonnull align 4 dereferenceable(20)) #1

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject24checkCollideWithOverrideEPKS_(%class.btCollisionObject* %this, %class.btCollisionObject* %0) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %0, %class.btCollisionObject** %.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  ret i1 true
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape(%class.btCollisionObject* %this, %class.btCollisionShape* %collisionShape) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %collisionShape.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  store %class.btCollisionShape* %collisionShape, %class.btCollisionShape** %collisionShape.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_updateRevision = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 26
  %0 = load i32, i32* %m_updateRevision, align 4, !tbaa !31
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %m_updateRevision, align 4, !tbaa !31
  %1 = load %class.btCollisionShape*, %class.btCollisionShape** %collisionShape.addr, align 4, !tbaa !2
  %m_collisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 9
  store %class.btCollisionShape* %1, %class.btCollisionShape** %m_collisionShape, align 4, !tbaa !30
  %2 = load %class.btCollisionShape*, %class.btCollisionShape** %collisionShape.addr, align 4, !tbaa !2
  %m_rootCollisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 11
  store %class.btCollisionShape* %2, %class.btCollisionShape** %m_rootCollisionShape, align 4, !tbaa !32
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK17btCollisionObject28calculateSerializeBufferSizeEv(%class.btCollisionObject* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  ret i32 256
}

declare i8* @_ZNK17btCollisionObject9serializeEPvP12btSerializer(%class.btCollisionObject*, i8*, %class.btSerializer*) unnamed_addr #1

declare void @_ZNK17btCollisionObject21serializeSingleObjectEP12btSerializer(%class.btCollisionObject*, %class.btSerializer*) unnamed_addr #1

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !28
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !28
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !28
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #5

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZdvRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #6 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load float*, float** %s.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !27
  %div = fdiv float 1.000000e+00, %3
  store float %div, float* %ref.tmp, align 4, !tbaa !27
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %4 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #6 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !27
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !27
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !27
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !27
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !27
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !27
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !27
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !27
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !27
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

define linkonce_odr hidden void @_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf(%class.btTransform* nonnull align 4 dereferenceable(64) %transform0, %class.btTransform* nonnull align 4 dereferenceable(64) %transform1, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, float* nonnull align 4 dereferenceable(4) %angle) #0 comdat {
entry:
  %transform0.addr = alloca %class.btTransform*, align 4
  %transform1.addr = alloca %class.btTransform*, align 4
  %axis.addr = alloca %class.btVector3*, align 4
  %angle.addr = alloca float*, align 4
  %dmat = alloca %class.btMatrix3x3, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %dorn = alloca %class.btQuaternion, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %len = alloca float, align 4
  %ref.tmp12 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  store %class.btTransform* %transform0, %class.btTransform** %transform0.addr, align 4, !tbaa !2
  store %class.btTransform* %transform1, %class.btTransform** %transform1.addr, align 4, !tbaa !2
  store %class.btVector3* %axis, %class.btVector3** %axis.addr, align 4, !tbaa !2
  store float* %angle, float** %angle.addr, align 4, !tbaa !2
  %0 = bitcast %class.btMatrix3x3* %dmat to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %0) #9
  %1 = load %class.btTransform*, %class.btTransform** %transform1.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %1)
  %2 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %2) #9
  %3 = load %class.btTransform*, %class.btTransform** %transform0.addr, align 4, !tbaa !2
  %call1 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %3)
  call void @_ZNK11btMatrix3x37inverseEv(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* %call1)
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %dmat, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp)
  %4 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %4) #9
  %5 = bitcast %class.btQuaternion* %dorn to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #9
  %call2 = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %dorn)
  call void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %dmat, %class.btQuaternion* nonnull align 4 dereferenceable(16) %dorn)
  %call3 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %dorn)
  %call4 = call float @_ZNK12btQuaternion8getAngleEv(%class.btQuaternion* %dorn)
  %6 = load float*, float** %angle.addr, align 4, !tbaa !2
  store float %call4, float* %6, align 4, !tbaa !27
  %7 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #9
  %8 = bitcast %class.btQuaternion* %dorn to %class.btQuadWord*
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %8)
  %9 = bitcast %class.btQuaternion* %dorn to %class.btQuadWord*
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %9)
  %10 = bitcast %class.btQuaternion* %dorn to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %10)
  %call9 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp5, float* nonnull align 4 dereferenceable(4) %call6, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call8)
  %11 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %12 = bitcast %class.btVector3* %11 to i8*
  %13 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false), !tbaa.struct !28
  %14 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %14) #9
  %15 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %15)
  %arrayidx = getelementptr inbounds float, float* %call10, i32 3
  store float 0.000000e+00, float* %arrayidx, align 4, !tbaa !27
  %16 = bitcast float* %len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #9
  %17 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %call11 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %17)
  store float %call11, float* %len, align 4, !tbaa !27
  %18 = load float, float* %len, align 4, !tbaa !27
  %cmp = fcmp olt float %18, 0x3D10000000000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %19 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %19) #9
  %20 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #9
  store float 1.000000e+00, float* %ref.tmp13, align 4, !tbaa !27
  %21 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #9
  store float 0.000000e+00, float* %ref.tmp14, align 4, !tbaa !27
  %22 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #9
  store float 0.000000e+00, float* %ref.tmp15, align 4, !tbaa !27
  %call16 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp15)
  %23 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %24 = bitcast %class.btVector3* %23 to i8*
  %25 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %24, i8* align 4 %25, i32 16, i1 false), !tbaa.struct !28
  %26 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #9
  %27 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #9
  %28 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #9
  %29 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #9
  br label %if.end

if.else:                                          ; preds = %entry
  %30 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #9
  %31 = load float, float* %len, align 4, !tbaa !27
  %call18 = call float @_Z6btSqrtf(float %31)
  store float %call18, float* %ref.tmp17, align 4, !tbaa !27
  %32 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %call19 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %32, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  %33 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #9
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %34 = bitcast float* %len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #9
  %35 = bitcast %class.btQuaternion* %dorn to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %35) #9
  %36 = bitcast %class.btMatrix3x3* %dmat to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %36) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #6 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !27
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !27
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !27
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !27
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !27
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !27
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !27
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !27
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !27
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !27
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !27
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !27
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !27
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !27
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !27
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !27
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #6 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %3, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4, !tbaa !27
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !27
  %7 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %9, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %8, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !27
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4, !tbaa !27
  %13 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %15, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %14, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4, !tbaa !27
  %16 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #9
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4, !tbaa !27
  %19 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #9
  %20 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %21 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %21, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %20, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4, !tbaa !27
  %22 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #9
  %23 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %24 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %24, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %23, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4, !tbaa !27
  %25 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #9
  %26 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %27 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %27, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %26, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4, !tbaa !27
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  %28 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #9
  %29 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #9
  %30 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #9
  %31 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #9
  %32 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #9
  %33 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #9
  %34 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #9
  %35 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #9
  %36 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x37inverseEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #6 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %co = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %det = alloca float, align 4
  %s = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp26 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %1 = bitcast %class.btVector3* %co to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #9
  %2 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %call = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 1, i32 2, i32 2)
  store float %call, float* %ref.tmp, align 4, !tbaa !27
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %call3 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 2, i32 2, i32 0)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !27
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %call5 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 0, i32 2, i32 1)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !27
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %co, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %5 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #9
  %6 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  %7 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  %8 = bitcast float* %det to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this1, i32 0)
  %call8 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call7, %class.btVector3* nonnull align 4 dereferenceable(16) %co)
  store float %call8, float* %det, align 4, !tbaa !27
  %9 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  %10 = load float, float* %det, align 4, !tbaa !27
  %div = fdiv float 1.000000e+00, %10
  store float %div, float* %s, align 4, !tbaa !27
  %11 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %co)
  %12 = load float, float* %call10, align 4, !tbaa !27
  %13 = load float, float* %s, align 4, !tbaa !27
  %mul = fmul float %12, %13
  store float %mul, float* %ref.tmp9, align 4, !tbaa !27
  %14 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #9
  %call12 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 2, i32 2, i32 1)
  %15 = load float, float* %s, align 4, !tbaa !27
  %mul13 = fmul float %call12, %15
  store float %mul13, float* %ref.tmp11, align 4, !tbaa !27
  %16 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #9
  %call15 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 1, i32 1, i32 2)
  %17 = load float, float* %s, align 4, !tbaa !27
  %mul16 = fmul float %call15, %17
  store float %mul16, float* %ref.tmp14, align 4, !tbaa !27
  %18 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #9
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %co)
  %19 = load float, float* %call18, align 4, !tbaa !27
  %20 = load float, float* %s, align 4, !tbaa !27
  %mul19 = fmul float %19, %20
  store float %mul19, float* %ref.tmp17, align 4, !tbaa !27
  %21 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #9
  %call21 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 0, i32 2, i32 2)
  %22 = load float, float* %s, align 4, !tbaa !27
  %mul22 = fmul float %call21, %22
  store float %mul22, float* %ref.tmp20, align 4, !tbaa !27
  %23 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #9
  %call24 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 2, i32 1, i32 0)
  %24 = load float, float* %s, align 4, !tbaa !27
  %mul25 = fmul float %call24, %24
  store float %mul25, float* %ref.tmp23, align 4, !tbaa !27
  %25 = bitcast float* %ref.tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #9
  %call27 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %co)
  %26 = load float, float* %call27, align 4, !tbaa !27
  %27 = load float, float* %s, align 4, !tbaa !27
  %mul28 = fmul float %26, %27
  store float %mul28, float* %ref.tmp26, align 4, !tbaa !27
  %28 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #9
  %call30 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 1, i32 2, i32 0)
  %29 = load float, float* %s, align 4, !tbaa !27
  %mul31 = fmul float %call30, %29
  store float %mul31, float* %ref.tmp29, align 4, !tbaa !27
  %30 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #9
  %call33 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 0, i32 1, i32 1)
  %31 = load float, float* %s, align 4, !tbaa !27
  %mul34 = fmul float %call33, %31
  store float %mul34, float* %ref.tmp32, align 4, !tbaa !27
  %call35 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp26, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  %32 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #9
  %33 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #9
  %34 = bitcast float* %ref.tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #9
  %35 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #9
  %36 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #9
  %37 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #9
  %38 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #9
  %39 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #9
  %40 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #9
  %41 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #9
  %42 = bitcast float* %det to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #9
  %43 = bitcast %class.btVector3* %co to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %43) #9
  ret void
}

define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* %0)
  ret %class.btQuaternion* %this1
}

define linkonce_odr hidden void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %trace = alloca float, align 4
  %temp = alloca [4 x float], align 16
  %s = alloca float, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %s64 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast float* %trace to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %1 = load float, float* %call, align 4, !tbaa !27
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx3)
  %2 = load float, float* %call4, align 4, !tbaa !27
  %add = fadd float %1, %2
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx6)
  %3 = load float, float* %call7, align 4, !tbaa !27
  %add8 = fadd float %add, %3
  store float %add8, float* %trace, align 4, !tbaa !27
  %4 = bitcast [4 x float]* %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #9
  %5 = load float, float* %trace, align 4, !tbaa !27
  %cmp = fcmp ogt float %5, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %6 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %7 = load float, float* %trace, align 4, !tbaa !27
  %add9 = fadd float %7, 1.000000e+00
  %call10 = call float @_Z6btSqrtf(float %add9)
  store float %call10, float* %s, align 4, !tbaa !27
  %8 = load float, float* %s, align 4, !tbaa !27
  %mul = fmul float %8, 5.000000e-01
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  store float %mul, float* %arrayidx11, align 4, !tbaa !27
  %9 = load float, float* %s, align 4, !tbaa !27
  %div = fdiv float 5.000000e-01, %9
  store float %div, float* %s, align 4, !tbaa !27
  %m_el12 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el12, i32 0, i32 2
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx13)
  %10 = load float, float* %call14, align 4, !tbaa !27
  %m_el15 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el15, i32 0, i32 1
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx16)
  %11 = load float, float* %call17, align 4, !tbaa !27
  %sub = fsub float %10, %11
  %12 = load float, float* %s, align 4, !tbaa !27
  %mul18 = fmul float %sub, %12
  %arrayidx19 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 0
  store float %mul18, float* %arrayidx19, align 16, !tbaa !27
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 0
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %13 = load float, float* %call22, align 4, !tbaa !27
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx24)
  %14 = load float, float* %call25, align 4, !tbaa !27
  %sub26 = fsub float %13, %14
  %15 = load float, float* %s, align 4, !tbaa !27
  %mul27 = fmul float %sub26, %15
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 1
  store float %mul27, float* %arrayidx28, align 4, !tbaa !27
  %m_el29 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el29, i32 0, i32 1
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx30)
  %16 = load float, float* %call31, align 4, !tbaa !27
  %m_el32 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx33 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el32, i32 0, i32 0
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx33)
  %17 = load float, float* %call34, align 4, !tbaa !27
  %sub35 = fsub float %16, %17
  %18 = load float, float* %s, align 4, !tbaa !27
  %mul36 = fmul float %sub35, %18
  %arrayidx37 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 2
  store float %mul36, float* %arrayidx37, align 8, !tbaa !27
  %19 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #9
  br label %if.end

if.else:                                          ; preds = %entry
  %20 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #9
  %m_el38 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx39 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el38, i32 0, i32 0
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx39)
  %21 = load float, float* %call40, align 4, !tbaa !27
  %m_el41 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx42 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el41, i32 0, i32 1
  %call43 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx42)
  %22 = load float, float* %call43, align 4, !tbaa !27
  %cmp44 = fcmp olt float %21, %22
  br i1 %cmp44, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else
  %m_el45 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx46 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el45, i32 0, i32 1
  %call47 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx46)
  %23 = load float, float* %call47, align 4, !tbaa !27
  %m_el48 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx49 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el48, i32 0, i32 2
  %call50 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx49)
  %24 = load float, float* %call50, align 4, !tbaa !27
  %cmp51 = fcmp olt float %23, %24
  %25 = zext i1 %cmp51 to i64
  %cond = select i1 %cmp51, i32 2, i32 1
  br label %cond.end

cond.false:                                       ; preds = %if.else
  %m_el52 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx53 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el52, i32 0, i32 0
  %call54 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx53)
  %26 = load float, float* %call54, align 4, !tbaa !27
  %m_el55 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx56 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el55, i32 0, i32 2
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx56)
  %27 = load float, float* %call57, align 4, !tbaa !27
  %cmp58 = fcmp olt float %26, %27
  %28 = zext i1 %cmp58 to i64
  %cond59 = select i1 %cmp58, i32 2, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond60 = phi i32 [ %cond, %cond.true ], [ %cond59, %cond.false ]
  store i32 %cond60, i32* %i, align 4, !tbaa !18
  %29 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #9
  %30 = load i32, i32* %i, align 4, !tbaa !18
  %add61 = add nsw i32 %30, 1
  %rem = srem i32 %add61, 3
  store i32 %rem, i32* %j, align 4, !tbaa !18
  %31 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #9
  %32 = load i32, i32* %i, align 4, !tbaa !18
  %add62 = add nsw i32 %32, 2
  %rem63 = srem i32 %add62, 3
  store i32 %rem63, i32* %k, align 4, !tbaa !18
  %33 = bitcast float* %s64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #9
  %m_el65 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %34 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx66 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el65, i32 0, i32 %34
  %call67 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx66)
  %35 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx68 = getelementptr inbounds float, float* %call67, i32 %35
  %36 = load float, float* %arrayidx68, align 4, !tbaa !27
  %m_el69 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %37 = load i32, i32* %j, align 4, !tbaa !18
  %arrayidx70 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el69, i32 0, i32 %37
  %call71 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx70)
  %38 = load i32, i32* %j, align 4, !tbaa !18
  %arrayidx72 = getelementptr inbounds float, float* %call71, i32 %38
  %39 = load float, float* %arrayidx72, align 4, !tbaa !27
  %sub73 = fsub float %36, %39
  %m_el74 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %40 = load i32, i32* %k, align 4, !tbaa !18
  %arrayidx75 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el74, i32 0, i32 %40
  %call76 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx75)
  %41 = load i32, i32* %k, align 4, !tbaa !18
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 %41
  %42 = load float, float* %arrayidx77, align 4, !tbaa !27
  %sub78 = fsub float %sub73, %42
  %add79 = fadd float %sub78, 1.000000e+00
  %call80 = call float @_Z6btSqrtf(float %add79)
  store float %call80, float* %s64, align 4, !tbaa !27
  %43 = load float, float* %s64, align 4, !tbaa !27
  %mul81 = fmul float %43, 5.000000e-01
  %44 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx82 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %44
  store float %mul81, float* %arrayidx82, align 4, !tbaa !27
  %45 = load float, float* %s64, align 4, !tbaa !27
  %div83 = fdiv float 5.000000e-01, %45
  store float %div83, float* %s64, align 4, !tbaa !27
  %m_el84 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %46 = load i32, i32* %k, align 4, !tbaa !18
  %arrayidx85 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el84, i32 0, i32 %46
  %call86 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx85)
  %47 = load i32, i32* %j, align 4, !tbaa !18
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 %47
  %48 = load float, float* %arrayidx87, align 4, !tbaa !27
  %m_el88 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %49 = load i32, i32* %j, align 4, !tbaa !18
  %arrayidx89 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el88, i32 0, i32 %49
  %call90 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx89)
  %50 = load i32, i32* %k, align 4, !tbaa !18
  %arrayidx91 = getelementptr inbounds float, float* %call90, i32 %50
  %51 = load float, float* %arrayidx91, align 4, !tbaa !27
  %sub92 = fsub float %48, %51
  %52 = load float, float* %s64, align 4, !tbaa !27
  %mul93 = fmul float %sub92, %52
  %arrayidx94 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  store float %mul93, float* %arrayidx94, align 4, !tbaa !27
  %m_el95 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %53 = load i32, i32* %j, align 4, !tbaa !18
  %arrayidx96 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el95, i32 0, i32 %53
  %call97 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx96)
  %54 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx98 = getelementptr inbounds float, float* %call97, i32 %54
  %55 = load float, float* %arrayidx98, align 4, !tbaa !27
  %m_el99 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %56 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx100 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el99, i32 0, i32 %56
  %call101 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx100)
  %57 = load i32, i32* %j, align 4, !tbaa !18
  %arrayidx102 = getelementptr inbounds float, float* %call101, i32 %57
  %58 = load float, float* %arrayidx102, align 4, !tbaa !27
  %add103 = fadd float %55, %58
  %59 = load float, float* %s64, align 4, !tbaa !27
  %mul104 = fmul float %add103, %59
  %60 = load i32, i32* %j, align 4, !tbaa !18
  %arrayidx105 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %60
  store float %mul104, float* %arrayidx105, align 4, !tbaa !27
  %m_el106 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %61 = load i32, i32* %k, align 4, !tbaa !18
  %arrayidx107 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el106, i32 0, i32 %61
  %call108 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx107)
  %62 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx109 = getelementptr inbounds float, float* %call108, i32 %62
  %63 = load float, float* %arrayidx109, align 4, !tbaa !27
  %m_el110 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %64 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx111 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el110, i32 0, i32 %64
  %call112 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx111)
  %65 = load i32, i32* %k, align 4, !tbaa !18
  %arrayidx113 = getelementptr inbounds float, float* %call112, i32 %65
  %66 = load float, float* %arrayidx113, align 4, !tbaa !27
  %add114 = fadd float %63, %66
  %67 = load float, float* %s64, align 4, !tbaa !27
  %mul115 = fmul float %add114, %67
  %68 = load i32, i32* %k, align 4, !tbaa !18
  %arrayidx116 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %68
  store float %mul115, float* %arrayidx116, align 4, !tbaa !27
  %69 = bitcast float* %s64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #9
  %70 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #9
  %71 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #9
  %72 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #9
  br label %if.end

if.end:                                           ; preds = %cond.end, %if.then
  %73 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %74 = bitcast %class.btQuaternion* %73 to %class.btQuadWord*
  %arrayidx117 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 0
  %arrayidx118 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 1
  %arrayidx119 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 2
  %arrayidx120 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %74, float* nonnull align 4 dereferenceable(4) %arrayidx117, float* nonnull align 4 dereferenceable(4) %arrayidx118, float* nonnull align 4 dereferenceable(4) %arrayidx119, float* nonnull align 4 dereferenceable(4) %arrayidx120)
  %75 = bitcast [4 x float]* %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %75) #9
  %76 = bitcast float* %trace to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #9
  ret void
}

define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call float @_ZNK12btQuaternion6lengthEv(%class.btQuaternion* %this1)
  store float %call, float* %ref.tmp, align 4, !tbaa !27
  %call2 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaterniondVERKf(%class.btQuaternion* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1) #9
  ret %class.btQuaternion* %call2
}

define linkonce_odr hidden float @_ZNK12btQuaternion8getAngleEv(%class.btQuaternion* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %s = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  %2 = load float, float* %arrayidx, align 4, !tbaa !27
  %call = call float @_Z6btAcosf(float %2)
  %mul = fmul float 2.000000e+00, %call
  store float %mul, float* %s, align 4, !tbaa !27
  %3 = load float, float* %s, align 4, !tbaa !27
  %4 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  ret float %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #4 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !27
  %0 = load float, float* %y.addr, align 4, !tbaa !27
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !27
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !27
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  ret %class.btVector3* %call
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #6 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !27
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !27
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !27
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !27
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !27
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !27
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !18
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !27
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !27
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !27
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !27
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !27
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !27
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !27
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !27
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !27
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !27
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !27
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !27
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !27
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !27
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !27
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !27
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !27
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !27
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !27
  ret void
}

define linkonce_odr hidden float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this, i32 %r1, i32 %c1, i32 %r2, i32 %c2) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %r1.addr = alloca i32, align 4
  %c1.addr = alloca i32, align 4
  %r2.addr = alloca i32, align 4
  %c2.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %r1, i32* %r1.addr, align 4, !tbaa !18
  store i32 %c1, i32* %c1.addr, align 4, !tbaa !18
  store i32 %r2, i32* %r2.addr, align 4, !tbaa !18
  store i32 %c2, i32* %c2.addr, align 4, !tbaa !18
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %r1.addr, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx)
  %1 = load i32, i32* %c1.addr, align 4, !tbaa !18
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 %1
  %2 = load float, float* %arrayidx2, align 4, !tbaa !27
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %3 = load i32, i32* %r2.addr, align 4, !tbaa !18
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 %3
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx4)
  %4 = load i32, i32* %c2.addr, align 4, !tbaa !18
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %4
  %5 = load float, float* %arrayidx6, align 4, !tbaa !27
  %mul = fmul float %2, %5
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %6 = load i32, i32* %r1.addr, align 4, !tbaa !18
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 %6
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx8)
  %7 = load i32, i32* %c2.addr, align 4, !tbaa !18
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %7
  %8 = load float, float* %arrayidx10, align 4, !tbaa !27
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %9 = load i32, i32* %r2.addr, align 4, !tbaa !18
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 %9
  %call13 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx12)
  %10 = load i32, i32* %c1.addr, align 4, !tbaa !18
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 %10
  %11 = load float, float* %arrayidx14, align 4, !tbaa !27
  %mul15 = fmul float %8, %11
  %sub = fsub float %mul, %mul15
  ret float %sub
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !27
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !27
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !27
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !27
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !27
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !27
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  ret %class.btQuadWord* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) #4 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  store float* %_w, float** %_w.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !27
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !27
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !27
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !27
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !27
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !27
  %6 = load float*, float** %_w.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !27
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4, !tbaa !27
  ret void
}

define linkonce_odr hidden float @_ZNK12btQuaternion6lengthEv(%class.btQuaternion* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaterniondVERKf(%class.btQuaternion* %this, float* nonnull align 4 dereferenceable(4) %s) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !27
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !27
  %call = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKf(%class.btQuaternion* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  ret %class.btQuaternion* %call
}

define linkonce_odr hidden float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

define linkonce_odr hidden float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !27
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call, align 4, !tbaa !27
  %mul = fmul float %1, %4
  %5 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %5, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %6 = load float, float* %arrayidx3, align 4, !tbaa !27
  %7 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %8 = bitcast %class.btQuaternion* %7 to %class.btQuadWord*
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %8)
  %9 = load float, float* %call4, align 4, !tbaa !27
  %mul5 = fmul float %6, %9
  %add = fadd float %mul, %mul5
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %11 = load float, float* %arrayidx7, align 4, !tbaa !27
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %13)
  %14 = load float, float* %call8, align 4, !tbaa !27
  %mul9 = fmul float %11, %14
  %add10 = fadd float %add, %mul9
  %15 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats11 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %15, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 3
  %16 = load float, float* %arrayidx12, align 4, !tbaa !27
  %17 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %18 = bitcast %class.btQuaternion* %17 to %class.btQuadWord*
  %m_floats13 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %18, i32 0, i32 0
  %arrayidx14 = getelementptr inbounds [4 x float], [4 x float]* %m_floats13, i32 0, i32 3
  %19 = load float, float* %arrayidx14, align 4, !tbaa !27
  %mul15 = fmul float %16, %19
  %add16 = fadd float %add10, %mul15
  ret float %add16
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKf(%class.btQuaternion* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %s.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !27
  %2 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %2, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %3 = load float, float* %arrayidx, align 4, !tbaa !27
  %mul = fmul float %3, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !27
  %4 = load float*, float** %s.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !27
  %6 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !27
  %mul4 = fmul float %7, %5
  store float %mul4, float* %arrayidx3, align 4, !tbaa !27
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !27
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats5 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %11 = load float, float* %arrayidx6, align 4, !tbaa !27
  %mul7 = fmul float %11, %9
  store float %mul7, float* %arrayidx6, align 4, !tbaa !27
  %12 = load float*, float** %s.addr, align 4, !tbaa !2
  %13 = load float, float* %12, align 4, !tbaa !27
  %14 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats8 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %14, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 3
  %15 = load float, float* %arrayidx9, align 4, !tbaa !27
  %mul10 = fmul float %15, %13
  store float %mul10, float* %arrayidx9, align 4, !tbaa !27
  ret %class.btQuaternion* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btAcosf(float %x) #4 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !27
  %0 = load float, float* %x.addr, align 4, !tbaa !27
  %cmp = fcmp olt float %0, -1.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store float -1.000000e+00, float* %x.addr, align 4, !tbaa !27
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %1 = load float, float* %x.addr, align 4, !tbaa !27
  %cmp1 = fcmp ogt float %1, 1.000000e+00
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store float 1.000000e+00, float* %x.addr, align 4, !tbaa !27
  br label %if.end3

if.end3:                                          ; preds = %if.then2, %if.end
  %2 = load float, float* %x.addr, align 4, !tbaa !27
  %call = call float @acosf(float %2) #10
  ret float %call
}

; Function Attrs: nounwind readnone
declare float @acosf(float) #7

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #8

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !27
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !27
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !27
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !27
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !27
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !27
  %6 = load float*, float** %s.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !27
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !27
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !27
  ret %class.btVector3* %this1
}

define linkonce_odr hidden void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !27
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !27
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !27
  %3 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !27
  %4 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  store float 1.000000e+00, float* %ref.tmp5, align 4, !tbaa !27
  %5 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !27
  %6 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !27
  %7 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  store float 0.000000e+00, float* %ref.tmp8, align 4, !tbaa !27
  %8 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  store float 1.000000e+00, float* %ref.tmp9, align 4, !tbaa !27
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %9 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  %11 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  %12 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  %13 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  %14 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #9
  %15 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

define linkonce_odr hidden void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %xs = alloca float, align 4
  %ys = alloca float, align 4
  %zs = alloca float, align 4
  %wx = alloca float, align 4
  %wy = alloca float, align 4
  %wz = alloca float, align 4
  %xx = alloca float, align 4
  %xy = alloca float, align 4
  %xz = alloca float, align 4
  %yy = alloca float, align 4
  %yz = alloca float, align 4
  %zz = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp34 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast float* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %1)
  store float %call, float* %d, align 4, !tbaa !27
  %2 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %3 = load float, float* %d, align 4, !tbaa !27
  %div = fdiv float 2.000000e+00, %3
  store float %div, float* %s, align 4, !tbaa !27
  %4 = bitcast float* %xs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %6 = bitcast %class.btQuaternion* %5 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %6)
  %7 = load float, float* %call2, align 4, !tbaa !27
  %8 = load float, float* %s, align 4, !tbaa !27
  %mul = fmul float %7, %8
  store float %mul, float* %xs, align 4, !tbaa !27
  %9 = bitcast float* %ys to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %11)
  %12 = load float, float* %call3, align 4, !tbaa !27
  %13 = load float, float* %s, align 4, !tbaa !27
  %mul4 = fmul float %12, %13
  store float %mul4, float* %ys, align 4, !tbaa !27
  %14 = bitcast float* %zs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #9
  %15 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %16 = bitcast %class.btQuaternion* %15 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %16)
  %17 = load float, float* %call5, align 4, !tbaa !27
  %18 = load float, float* %s, align 4, !tbaa !27
  %mul6 = fmul float %17, %18
  store float %mul6, float* %zs, align 4, !tbaa !27
  %19 = bitcast float* %wx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #9
  %20 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %21 = bitcast %class.btQuaternion* %20 to %class.btQuadWord*
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %21)
  %22 = load float, float* %call7, align 4, !tbaa !27
  %23 = load float, float* %xs, align 4, !tbaa !27
  %mul8 = fmul float %22, %23
  store float %mul8, float* %wx, align 4, !tbaa !27
  %24 = bitcast float* %wy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #9
  %25 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %26 = bitcast %class.btQuaternion* %25 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %26)
  %27 = load float, float* %call9, align 4, !tbaa !27
  %28 = load float, float* %ys, align 4, !tbaa !27
  %mul10 = fmul float %27, %28
  store float %mul10, float* %wy, align 4, !tbaa !27
  %29 = bitcast float* %wz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #9
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %31)
  %32 = load float, float* %call11, align 4, !tbaa !27
  %33 = load float, float* %zs, align 4, !tbaa !27
  %mul12 = fmul float %32, %33
  store float %mul12, float* %wz, align 4, !tbaa !27
  %34 = bitcast float* %xx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #9
  %35 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %36 = bitcast %class.btQuaternion* %35 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %36)
  %37 = load float, float* %call13, align 4, !tbaa !27
  %38 = load float, float* %xs, align 4, !tbaa !27
  %mul14 = fmul float %37, %38
  store float %mul14, float* %xx, align 4, !tbaa !27
  %39 = bitcast float* %xy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #9
  %40 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %41 = bitcast %class.btQuaternion* %40 to %class.btQuadWord*
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %41)
  %42 = load float, float* %call15, align 4, !tbaa !27
  %43 = load float, float* %ys, align 4, !tbaa !27
  %mul16 = fmul float %42, %43
  store float %mul16, float* %xy, align 4, !tbaa !27
  %44 = bitcast float* %xz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #9
  %45 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %46 = bitcast %class.btQuaternion* %45 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %46)
  %47 = load float, float* %call17, align 4, !tbaa !27
  %48 = load float, float* %zs, align 4, !tbaa !27
  %mul18 = fmul float %47, %48
  store float %mul18, float* %xz, align 4, !tbaa !27
  %49 = bitcast float* %yy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #9
  %50 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %51 = bitcast %class.btQuaternion* %50 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %51)
  %52 = load float, float* %call19, align 4, !tbaa !27
  %53 = load float, float* %ys, align 4, !tbaa !27
  %mul20 = fmul float %52, %53
  store float %mul20, float* %yy, align 4, !tbaa !27
  %54 = bitcast float* %yz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #9
  %55 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %56 = bitcast %class.btQuaternion* %55 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %56)
  %57 = load float, float* %call21, align 4, !tbaa !27
  %58 = load float, float* %zs, align 4, !tbaa !27
  %mul22 = fmul float %57, %58
  store float %mul22, float* %yz, align 4, !tbaa !27
  %59 = bitcast float* %zz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #9
  %60 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %61 = bitcast %class.btQuaternion* %60 to %class.btQuadWord*
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %61)
  %62 = load float, float* %call23, align 4, !tbaa !27
  %63 = load float, float* %zs, align 4, !tbaa !27
  %mul24 = fmul float %62, %63
  store float %mul24, float* %zz, align 4, !tbaa !27
  %64 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #9
  %65 = load float, float* %yy, align 4, !tbaa !27
  %66 = load float, float* %zz, align 4, !tbaa !27
  %add = fadd float %65, %66
  %sub = fsub float 1.000000e+00, %add
  store float %sub, float* %ref.tmp, align 4, !tbaa !27
  %67 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #9
  %68 = load float, float* %xy, align 4, !tbaa !27
  %69 = load float, float* %wz, align 4, !tbaa !27
  %sub26 = fsub float %68, %69
  store float %sub26, float* %ref.tmp25, align 4, !tbaa !27
  %70 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #9
  %71 = load float, float* %xz, align 4, !tbaa !27
  %72 = load float, float* %wy, align 4, !tbaa !27
  %add28 = fadd float %71, %72
  store float %add28, float* %ref.tmp27, align 4, !tbaa !27
  %73 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #9
  %74 = load float, float* %xy, align 4, !tbaa !27
  %75 = load float, float* %wz, align 4, !tbaa !27
  %add30 = fadd float %74, %75
  store float %add30, float* %ref.tmp29, align 4, !tbaa !27
  %76 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #9
  %77 = load float, float* %xx, align 4, !tbaa !27
  %78 = load float, float* %zz, align 4, !tbaa !27
  %add32 = fadd float %77, %78
  %sub33 = fsub float 1.000000e+00, %add32
  store float %sub33, float* %ref.tmp31, align 4, !tbaa !27
  %79 = bitcast float* %ref.tmp34 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #9
  %80 = load float, float* %yz, align 4, !tbaa !27
  %81 = load float, float* %wx, align 4, !tbaa !27
  %sub35 = fsub float %80, %81
  store float %sub35, float* %ref.tmp34, align 4, !tbaa !27
  %82 = bitcast float* %ref.tmp36 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #9
  %83 = load float, float* %xz, align 4, !tbaa !27
  %84 = load float, float* %wy, align 4, !tbaa !27
  %sub37 = fsub float %83, %84
  store float %sub37, float* %ref.tmp36, align 4, !tbaa !27
  %85 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %85) #9
  %86 = load float, float* %yz, align 4, !tbaa !27
  %87 = load float, float* %wx, align 4, !tbaa !27
  %add39 = fadd float %86, %87
  store float %add39, float* %ref.tmp38, align 4, !tbaa !27
  %88 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #9
  %89 = load float, float* %xx, align 4, !tbaa !27
  %90 = load float, float* %yy, align 4, !tbaa !27
  %add41 = fadd float %89, %90
  %sub42 = fsub float 1.000000e+00, %add41
  store float %sub42, float* %ref.tmp40, align 4, !tbaa !27
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp34, float* nonnull align 4 dereferenceable(4) %ref.tmp36, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp40)
  %91 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #9
  %92 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #9
  %93 = bitcast float* %ref.tmp36 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #9
  %94 = bitcast float* %ref.tmp34 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #9
  %95 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #9
  %96 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #9
  %97 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #9
  %98 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #9
  %99 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #9
  %100 = bitcast float* %zz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #9
  %101 = bitcast float* %yz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #9
  %102 = bitcast float* %yy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #9
  %103 = bitcast float* %xz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #9
  %104 = bitcast float* %xy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #9
  %105 = bitcast float* %xx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #9
  %106 = bitcast float* %wz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #9
  %107 = bitcast float* %wy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #9
  %108 = bitcast float* %wx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #9
  %109 = bitcast float* %zs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #9
  %110 = bitcast float* %ys to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #9
  %111 = bitcast float* %xs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #9
  %112 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #9
  %113 = bitcast float* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #6 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !27
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !27
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !27
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !27
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !27
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !27
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !27
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !27
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !27
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden i32 @_Z9btOutcodeRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %p, %class.btVector3* nonnull align 4 dereferenceable(16) %halfExtent) #6 comdat {
entry:
  %p.addr = alloca %class.btVector3*, align 4
  %halfExtent.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %p, %class.btVector3** %p.addr, align 4, !tbaa !2
  store %class.btVector3* %halfExtent, %class.btVector3** %halfExtent.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %0)
  %1 = load float, float* %call, align 4, !tbaa !27
  %2 = load %class.btVector3*, %class.btVector3** %halfExtent.addr, align 4, !tbaa !2
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %2)
  %3 = load float, float* %call1, align 4, !tbaa !27
  %fneg = fneg float %3
  %cmp = fcmp olt float %1, %fneg
  %4 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 1, i32 0
  %5 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %5)
  %6 = load float, float* %call2, align 4, !tbaa !27
  %7 = load %class.btVector3*, %class.btVector3** %halfExtent.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %7)
  %8 = load float, float* %call3, align 4, !tbaa !27
  %cmp4 = fcmp ogt float %6, %8
  %9 = zext i1 %cmp4 to i64
  %cond5 = select i1 %cmp4, i32 8, i32 0
  %or = or i32 %cond, %cond5
  %10 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %10)
  %11 = load float, float* %call6, align 4, !tbaa !27
  %12 = load %class.btVector3*, %class.btVector3** %halfExtent.addr, align 4, !tbaa !2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %12)
  %13 = load float, float* %call7, align 4, !tbaa !27
  %fneg8 = fneg float %13
  %cmp9 = fcmp olt float %11, %fneg8
  %14 = zext i1 %cmp9 to i64
  %cond10 = select i1 %cmp9, i32 2, i32 0
  %or11 = or i32 %or, %cond10
  %15 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %15)
  %16 = load float, float* %call12, align 4, !tbaa !27
  %17 = load %class.btVector3*, %class.btVector3** %halfExtent.addr, align 4, !tbaa !2
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %17)
  %18 = load float, float* %call13, align 4, !tbaa !27
  %cmp14 = fcmp ogt float %16, %18
  %19 = zext i1 %cmp14 to i64
  %cond15 = select i1 %cmp14, i32 16, i32 0
  %or16 = or i32 %or11, %cond15
  %20 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %20)
  %21 = load float, float* %call17, align 4, !tbaa !27
  %22 = load %class.btVector3*, %class.btVector3** %halfExtent.addr, align 4, !tbaa !2
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %22)
  %23 = load float, float* %call18, align 4, !tbaa !27
  %fneg19 = fneg float %23
  %cmp20 = fcmp olt float %21, %fneg19
  %24 = zext i1 %cmp20 to i64
  %cond21 = select i1 %cmp20, i32 4, i32 0
  %or22 = or i32 %or16, %cond21
  %25 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %25)
  %26 = load float, float* %call23, align 4, !tbaa !27
  %27 = load %class.btVector3*, %class.btVector3** %halfExtent.addr, align 4, !tbaa !2
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %27)
  %28 = load float, float* %call24, align 4, !tbaa !27
  %cmp25 = fcmp ogt float %26, %28
  %29 = zext i1 %cmp25 to i64
  %cond26 = select i1 %cmp25, i32 32, i32 0
  %or27 = or i32 %or22, %cond26
  ret i32 %or27
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #4 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store float* %b, float** %b.addr, align 4, !tbaa !2
  %0 = load float*, float** %b.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !27
  %2 = load float*, float** %a.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !27
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !27
  %6 = load float*, float** %a.addr, align 4, !tbaa !2
  store float %5, float* %6, align 4, !tbaa !27
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE4initEv(%class.btAlignedObjectArray* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !33
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btCollisionObject** null, %class.btCollisionObject*** %m_data, align 4, !tbaa !19
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !23
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !34
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE5clearEv(%class.btAlignedObjectArray* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !18
  store i32 %last, i32* %last.addr, align 4, !tbaa !18
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %first.addr, align 4, !tbaa !18
  store i32 %1, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !18
  %3 = load i32, i32* %last.addr, align 4, !tbaa !18
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4, !tbaa !19
  %5 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !18
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv(%class.btAlignedObjectArray* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4, !tbaa !19
  %tobool = icmp ne %class.btCollisionObject** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !33, !range !35
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data4, align 4, !tbaa !19
  call void @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE10deallocateEPS1_(%class.btAlignedAllocator* %m_allocator, %class.btCollisionObject** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btCollisionObject** null, %class.btCollisionObject*** %m_data5, align 4, !tbaa !19
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE10deallocateEPS1_(%class.btAlignedAllocator* %this, %class.btCollisionObject** %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.btCollisionObject**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject** %ptr, %class.btCollisionObject*** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btCollisionObject** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv(%class.btAlignedObjectArray* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !34
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btCollisionObject**, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !18
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !18
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btCollisionObject*** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !18
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP17btCollisionObjectE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btCollisionObject**
  store %class.btCollisionObject** %3, %class.btCollisionObject*** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  %4 = load %class.btCollisionObject**, %class.btCollisionObject*** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4copyEiiPS1_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %class.btCollisionObject** %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !33
  %5 = load %class.btCollisionObject**, %class.btCollisionObject*** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btCollisionObject** %5, %class.btCollisionObject*** %m_data, align 4, !tbaa !19
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !18
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !34
  %7 = bitcast %class.btCollisionObject*** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP17btCollisionObjectE9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !18
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !18
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !18
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP17btCollisionObjectE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #6 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !18
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !18
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !18
  %call = call %class.btCollisionObject** @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator* %m_allocator, i32 %1, %class.btCollisionObject*** null)
  %2 = bitcast %class.btCollisionObject** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4copyEiiPS1_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %class.btCollisionObject** %dest) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btCollisionObject**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !18
  store i32 %end, i32* %end.addr, align 4, !tbaa !18
  store %class.btCollisionObject** %dest, %class.btCollisionObject*** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %start.addr, align 4, !tbaa !18
  store i32 %1, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !18
  %3 = load i32, i32* %end.addr, align 4, !tbaa !18
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btCollisionObject**, %class.btCollisionObject*** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %4, i32 %5
  %6 = bitcast %class.btCollisionObject** %arrayidx to i8*
  %7 = bitcast i8* %6 to %class.btCollisionObject**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4, !tbaa !19
  %9 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx2 = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %8, i32 %9
  %10 = load %class.btCollisionObject*, %class.btCollisionObject** %arrayidx2, align 4, !tbaa !2
  store %class.btCollisionObject* %10, %class.btCollisionObject** %7, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !18
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  ret void
}

define linkonce_odr hidden %class.btCollisionObject** @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator* %this, i32 %n, %class.btCollisionObject*** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btCollisionObject***, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !18
  store %class.btCollisionObject*** %hint, %class.btCollisionObject**** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !18
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btCollisionObject**
  ret %class.btCollisionObject** %1
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { argmemonly nounwind willreturn }
attributes #6 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind readnone speculatable willreturn }
attributes #9 = { nounwind }
attributes #10 = { nounwind readnone }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{!9, !13, i64 236}
!9 = !{!"_ZTS17btCollisionObject", !10, i64 4, !10, i64 68, !12, i64 132, !12, i64 148, !12, i64 164, !13, i64 180, !14, i64 184, !3, i64 188, !3, i64 192, !3, i64 196, !3, i64 200, !13, i64 204, !13, i64 208, !13, i64 212, !13, i64 216, !14, i64 220, !14, i64 224, !14, i64 228, !14, i64 232, !13, i64 236, !4, i64 240, !14, i64 244, !14, i64 248, !14, i64 252, !13, i64 256, !13, i64 260}
!10 = !{!"_ZTS11btTransform", !11, i64 0, !12, i64 48}
!11 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!12 = !{!"_ZTS9btVector3", !4, i64 0}
!13 = !{!"int", !4, i64 0}
!14 = !{!"float", !4, i64 0}
!15 = !{!16, !3, i64 0}
!16 = !{!"_ZTS17btBroadphaseProxy", !3, i64 0, !17, i64 4, !17, i64 6, !3, i64 8, !13, i64 12, !12, i64 16, !12, i64 32}
!17 = !{!"short", !4, i64 0}
!18 = !{!13, !13, i64 0}
!19 = !{!20, !3, i64 12}
!20 = !{!"_ZTS20btAlignedObjectArrayIP17btCollisionObjectE", !21, i64 0, !13, i64 4, !13, i64 8, !3, i64 12, !22, i64 16}
!21 = !{!"_ZTS18btAlignedAllocatorIP17btCollisionObjectLj16EE"}
!22 = !{!"bool", !4, i64 0}
!23 = !{!20, !13, i64 4}
!24 = !{!25, !3, i64 284}
!25 = !{!"_ZTS24btPairCachingGhostObject", !3, i64 284}
!26 = !{!9, !3, i64 188}
!27 = !{!14, !14, i64 0}
!28 = !{i64 0, i64 16, !29}
!29 = !{!4, !4, i64 0}
!30 = !{!9, !3, i64 192}
!31 = !{!9, !13, i64 260}
!32 = !{!9, !3, i64 200}
!33 = !{!20, !22, i64 16}
!34 = !{!20, !13, i64 8}
!35 = !{i8 0, i8 2}
