; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/NarrowPhaseCollision/btGjkPairDetector.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/NarrowPhaseCollision/btGjkPairDetector.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btGjkPairDetector = type { %struct.btDiscreteCollisionDetectorInterface, %class.btVector3, %class.btConvexPenetrationDepthSolver*, %class.btVoronoiSimplexSolver*, %class.btConvexShape*, %class.btConvexShape*, i32, i32, float, float, i8, float, i32, i32, i32, i32, i32 }
%struct.btDiscreteCollisionDetectorInterface = type { i32 (...)** }
%class.btVector3 = type { [4 x float] }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%class.btVoronoiSimplexSolver = type <{ i32, [5 x %class.btVector3], [5 x %class.btVector3], [5 x %class.btVector3], %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, i8, [3 x i8], %struct.btSubSimplexClosestResult, i8, [3 x i8] }>
%struct.btSubSimplexClosestResult = type <{ %class.btVector3, %struct.btUsageBitfield, [2 x i8], [4 x float], i8, [3 x i8] }>
%struct.btUsageBitfield = type { i8, i8 }
%class.btConvexPenetrationDepthSolver = type { i32 (...)** }
%"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput" = type { %class.btTransform, %class.btTransform, float }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }
%class.btIDebugDraw = type opaque

$_ZN36btDiscreteCollisionDetectorInterfaceC2Ev = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZNK16btCollisionShape12getShapeTypeEv = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZN11btTransform9getOriginEv = comdat any

$_ZN9btVector3mIERKS_ = comdat any

$_ZNK16btCollisionShape10isConvex2dEv = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZmlRK9btVector3RK11btMatrix3x3 = comdat any

$_ZngRK9btVector3 = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZNK22btVoronoiSimplexSolver11fullSimplexEv = comdat any

$_Z6btSqrtf = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZN9btVector37setZeroEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZN36btDiscreteCollisionDetectorInterfaceD2Ev = comdat any

$_ZN17btGjkPairDetectorD0Ev = comdat any

$_ZN36btDiscreteCollisionDetectorInterfaceD0Ev = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZN17btBroadphaseProxy10isConvex2dEi = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZTS36btDiscreteCollisionDetectorInterface = comdat any

$_ZTI36btDiscreteCollisionDetectorInterface = comdat any

$_ZTV36btDiscreteCollisionDetectorInterface = comdat any

@gNumDeepPenetrationChecks = hidden global i32 0, align 4
@gNumGjkChecks = hidden global i32 0, align 4
@_ZTV17btGjkPairDetector = hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI17btGjkPairDetector to i8*), i8* bitcast (%struct.btDiscreteCollisionDetectorInterface* (%struct.btDiscreteCollisionDetectorInterface*)* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev to i8*), i8* bitcast (void (%class.btGjkPairDetector*)* @_ZN17btGjkPairDetectorD0Ev to i8*), i8* bitcast (void (%class.btGjkPairDetector*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btIDebugDraw*, i1)* @_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS17btGjkPairDetector = hidden constant [20 x i8] c"17btGjkPairDetector\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS36btDiscreteCollisionDetectorInterface = linkonce_odr hidden constant [39 x i8] c"36btDiscreteCollisionDetectorInterface\00", comdat, align 1
@_ZTI36btDiscreteCollisionDetectorInterface = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([39 x i8], [39 x i8]* @_ZTS36btDiscreteCollisionDetectorInterface, i32 0, i32 0) }, comdat, align 4
@_ZTI17btGjkPairDetector = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([20 x i8], [20 x i8]* @_ZTS17btGjkPairDetector, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI36btDiscreteCollisionDetectorInterface to i8*) }, align 4
@_ZTV36btDiscreteCollisionDetectorInterface = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI36btDiscreteCollisionDetectorInterface to i8*), i8* bitcast (%struct.btDiscreteCollisionDetectorInterface* (%struct.btDiscreteCollisionDetectorInterface*)* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev to i8*), i8* bitcast (void (%struct.btDiscreteCollisionDetectorInterface*)* @_ZN36btDiscreteCollisionDetectorInterfaceD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4

@_ZN17btGjkPairDetectorC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver = hidden unnamed_addr alias %class.btGjkPairDetector* (%class.btGjkPairDetector*, %class.btConvexShape*, %class.btConvexShape*, %class.btVoronoiSimplexSolver*, %class.btConvexPenetrationDepthSolver*), %class.btGjkPairDetector* (%class.btGjkPairDetector*, %class.btConvexShape*, %class.btConvexShape*, %class.btVoronoiSimplexSolver*, %class.btConvexPenetrationDepthSolver*)* @_ZN17btGjkPairDetectorC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
@_ZN17btGjkPairDetectorC1EPK13btConvexShapeS2_iiffP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver = hidden unnamed_addr alias %class.btGjkPairDetector* (%class.btGjkPairDetector*, %class.btConvexShape*, %class.btConvexShape*, i32, i32, float, float, %class.btVoronoiSimplexSolver*, %class.btConvexPenetrationDepthSolver*), %class.btGjkPairDetector* (%class.btGjkPairDetector*, %class.btConvexShape*, %class.btConvexShape*, i32, i32, float, float, %class.btVoronoiSimplexSolver*, %class.btConvexPenetrationDepthSolver*)* @_ZN17btGjkPairDetectorC2EPK13btConvexShapeS2_iiffP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver

define hidden %class.btGjkPairDetector* @_ZN17btGjkPairDetectorC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver(%class.btGjkPairDetector* returned %this, %class.btConvexShape* %objectA, %class.btConvexShape* %objectB, %class.btVoronoiSimplexSolver* %simplexSolver, %class.btConvexPenetrationDepthSolver* %penetrationDepthSolver) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btGjkPairDetector*, align 4
  %objectA.addr = alloca %class.btConvexShape*, align 4
  %objectB.addr = alloca %class.btConvexShape*, align 4
  %simplexSolver.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %penetrationDepthSolver.addr = alloca %class.btConvexPenetrationDepthSolver*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btGjkPairDetector* %this, %class.btGjkPairDetector** %this.addr, align 4, !tbaa !2
  store %class.btConvexShape* %objectA, %class.btConvexShape** %objectA.addr, align 4, !tbaa !2
  store %class.btConvexShape* %objectB, %class.btConvexShape** %objectB.addr, align 4, !tbaa !2
  store %class.btVoronoiSimplexSolver* %simplexSolver, %class.btVoronoiSimplexSolver** %simplexSolver.addr, align 4, !tbaa !2
  store %class.btConvexPenetrationDepthSolver* %penetrationDepthSolver, %class.btConvexPenetrationDepthSolver** %penetrationDepthSolver.addr, align 4, !tbaa !2
  %this1 = load %class.btGjkPairDetector*, %class.btGjkPairDetector** %this.addr, align 4
  %0 = bitcast %class.btGjkPairDetector* %this1 to %struct.btDiscreteCollisionDetectorInterface*
  %call = call %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceC2Ev(%struct.btDiscreteCollisionDetectorInterface* %0) #9
  %1 = bitcast %class.btGjkPairDetector* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV17btGjkPairDetector, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_cachedSeparatingAxis = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %2 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  store float 1.000000e+00, float* %ref.tmp2, align 4, !tbaa !8
  %4 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !8
  %call4 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %m_cachedSeparatingAxis, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #9
  %6 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  %7 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  %m_penetrationDepthSolver = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 2
  %8 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %penetrationDepthSolver.addr, align 4, !tbaa !2
  store %class.btConvexPenetrationDepthSolver* %8, %class.btConvexPenetrationDepthSolver** %m_penetrationDepthSolver, align 4, !tbaa !10
  %m_simplexSolver = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 3
  %9 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %simplexSolver.addr, align 4, !tbaa !2
  store %class.btVoronoiSimplexSolver* %9, %class.btVoronoiSimplexSolver** %m_simplexSolver, align 4, !tbaa !15
  %m_minkowskiA = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 4
  %10 = load %class.btConvexShape*, %class.btConvexShape** %objectA.addr, align 4, !tbaa !2
  store %class.btConvexShape* %10, %class.btConvexShape** %m_minkowskiA, align 4, !tbaa !16
  %m_minkowskiB = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 5
  %11 = load %class.btConvexShape*, %class.btConvexShape** %objectB.addr, align 4, !tbaa !2
  store %class.btConvexShape* %11, %class.btConvexShape** %m_minkowskiB, align 4, !tbaa !17
  %m_shapeTypeA = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 6
  %12 = load %class.btConvexShape*, %class.btConvexShape** %objectA.addr, align 4, !tbaa !2
  %13 = bitcast %class.btConvexShape* %12 to %class.btCollisionShape*
  %call5 = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %13)
  store i32 %call5, i32* %m_shapeTypeA, align 4, !tbaa !18
  %m_shapeTypeB = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 7
  %14 = load %class.btConvexShape*, %class.btConvexShape** %objectB.addr, align 4, !tbaa !2
  %15 = bitcast %class.btConvexShape* %14 to %class.btCollisionShape*
  %call6 = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %15)
  store i32 %call6, i32* %m_shapeTypeB, align 4, !tbaa !19
  %m_marginA = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 8
  %16 = load %class.btConvexShape*, %class.btConvexShape** %objectA.addr, align 4, !tbaa !2
  %17 = bitcast %class.btConvexShape* %16 to float (%class.btConvexShape*)***
  %vtable = load float (%class.btConvexShape*)**, float (%class.btConvexShape*)*** %17, align 4, !tbaa !6
  %vfn = getelementptr inbounds float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vtable, i64 12
  %18 = load float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vfn, align 4
  %call7 = call float %18(%class.btConvexShape* %16)
  store float %call7, float* %m_marginA, align 4, !tbaa !20
  %m_marginB = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 9
  %19 = load %class.btConvexShape*, %class.btConvexShape** %objectB.addr, align 4, !tbaa !2
  %20 = bitcast %class.btConvexShape* %19 to float (%class.btConvexShape*)***
  %vtable8 = load float (%class.btConvexShape*)**, float (%class.btConvexShape*)*** %20, align 4, !tbaa !6
  %vfn9 = getelementptr inbounds float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vtable8, i64 12
  %21 = load float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vfn9, align 4
  %call10 = call float %21(%class.btConvexShape* %19)
  store float %call10, float* %m_marginB, align 4, !tbaa !21
  %m_ignoreMargin = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 10
  store i8 0, i8* %m_ignoreMargin, align 4, !tbaa !22
  %m_lastUsedMethod = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 12
  store i32 -1, i32* %m_lastUsedMethod, align 4, !tbaa !23
  %m_catchDegeneracies = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 15
  store i32 1, i32* %m_catchDegeneracies, align 4, !tbaa !24
  %m_fixContactNormalDirection = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 16
  store i32 1, i32* %m_fixContactNormalDirection, align 4, !tbaa !25
  ret %class.btGjkPairDetector* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceC2Ev(%struct.btDiscreteCollisionDetectorInterface* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btDiscreteCollisionDetectorInterface*, align 4
  store %struct.btDiscreteCollisionDetectorInterface* %this, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btDiscreteCollisionDetectorInterface*, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  %0 = bitcast %struct.btDiscreteCollisionDetectorInterface* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV36btDiscreteCollisionDetectorInterface, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  ret %struct.btDiscreteCollisionDetectorInterface* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !8
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !8
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !8
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !8
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_shapeType, align 4, !tbaa !26
  ret i32 %0
}

define hidden %class.btGjkPairDetector* @_ZN17btGjkPairDetectorC2EPK13btConvexShapeS2_iiffP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver(%class.btGjkPairDetector* returned %this, %class.btConvexShape* %objectA, %class.btConvexShape* %objectB, i32 %shapeTypeA, i32 %shapeTypeB, float %marginA, float %marginB, %class.btVoronoiSimplexSolver* %simplexSolver, %class.btConvexPenetrationDepthSolver* %penetrationDepthSolver) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btGjkPairDetector*, align 4
  %objectA.addr = alloca %class.btConvexShape*, align 4
  %objectB.addr = alloca %class.btConvexShape*, align 4
  %shapeTypeA.addr = alloca i32, align 4
  %shapeTypeB.addr = alloca i32, align 4
  %marginA.addr = alloca float, align 4
  %marginB.addr = alloca float, align 4
  %simplexSolver.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %penetrationDepthSolver.addr = alloca %class.btConvexPenetrationDepthSolver*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btGjkPairDetector* %this, %class.btGjkPairDetector** %this.addr, align 4, !tbaa !2
  store %class.btConvexShape* %objectA, %class.btConvexShape** %objectA.addr, align 4, !tbaa !2
  store %class.btConvexShape* %objectB, %class.btConvexShape** %objectB.addr, align 4, !tbaa !2
  store i32 %shapeTypeA, i32* %shapeTypeA.addr, align 4, !tbaa !28
  store i32 %shapeTypeB, i32* %shapeTypeB.addr, align 4, !tbaa !28
  store float %marginA, float* %marginA.addr, align 4, !tbaa !8
  store float %marginB, float* %marginB.addr, align 4, !tbaa !8
  store %class.btVoronoiSimplexSolver* %simplexSolver, %class.btVoronoiSimplexSolver** %simplexSolver.addr, align 4, !tbaa !2
  store %class.btConvexPenetrationDepthSolver* %penetrationDepthSolver, %class.btConvexPenetrationDepthSolver** %penetrationDepthSolver.addr, align 4, !tbaa !2
  %this1 = load %class.btGjkPairDetector*, %class.btGjkPairDetector** %this.addr, align 4
  %0 = bitcast %class.btGjkPairDetector* %this1 to %struct.btDiscreteCollisionDetectorInterface*
  %call = call %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceC2Ev(%struct.btDiscreteCollisionDetectorInterface* %0) #9
  %1 = bitcast %class.btGjkPairDetector* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV17btGjkPairDetector, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_cachedSeparatingAxis = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %2 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  store float 1.000000e+00, float* %ref.tmp2, align 4, !tbaa !8
  %4 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !8
  %call4 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %m_cachedSeparatingAxis, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #9
  %6 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  %7 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  %m_penetrationDepthSolver = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 2
  %8 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %penetrationDepthSolver.addr, align 4, !tbaa !2
  store %class.btConvexPenetrationDepthSolver* %8, %class.btConvexPenetrationDepthSolver** %m_penetrationDepthSolver, align 4, !tbaa !10
  %m_simplexSolver = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 3
  %9 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %simplexSolver.addr, align 4, !tbaa !2
  store %class.btVoronoiSimplexSolver* %9, %class.btVoronoiSimplexSolver** %m_simplexSolver, align 4, !tbaa !15
  %m_minkowskiA = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 4
  %10 = load %class.btConvexShape*, %class.btConvexShape** %objectA.addr, align 4, !tbaa !2
  store %class.btConvexShape* %10, %class.btConvexShape** %m_minkowskiA, align 4, !tbaa !16
  %m_minkowskiB = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 5
  %11 = load %class.btConvexShape*, %class.btConvexShape** %objectB.addr, align 4, !tbaa !2
  store %class.btConvexShape* %11, %class.btConvexShape** %m_minkowskiB, align 4, !tbaa !17
  %m_shapeTypeA = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 6
  %12 = load i32, i32* %shapeTypeA.addr, align 4, !tbaa !28
  store i32 %12, i32* %m_shapeTypeA, align 4, !tbaa !18
  %m_shapeTypeB = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 7
  %13 = load i32, i32* %shapeTypeB.addr, align 4, !tbaa !28
  store i32 %13, i32* %m_shapeTypeB, align 4, !tbaa !19
  %m_marginA = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 8
  %14 = load float, float* %marginA.addr, align 4, !tbaa !8
  store float %14, float* %m_marginA, align 4, !tbaa !20
  %m_marginB = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 9
  %15 = load float, float* %marginB.addr, align 4, !tbaa !8
  store float %15, float* %m_marginB, align 4, !tbaa !21
  %m_ignoreMargin = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 10
  store i8 0, i8* %m_ignoreMargin, align 4, !tbaa !22
  %m_lastUsedMethod = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 12
  store i32 -1, i32* %m_lastUsedMethod, align 4, !tbaa !23
  %m_catchDegeneracies = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 15
  store i32 1, i32* %m_catchDegeneracies, align 4, !tbaa !24
  %m_fixContactNormalDirection = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 16
  store i32 1, i32* %m_fixContactNormalDirection, align 4, !tbaa !25
  ret %class.btGjkPairDetector* %this1
}

define hidden void @_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb(%class.btGjkPairDetector* %this, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* nonnull align 4 dereferenceable(132) %input, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %output, %class.btIDebugDraw* %debugDraw, i1 zeroext %swapResults) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btGjkPairDetector*, align 4
  %input.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, align 4
  %output.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  %debugDraw.addr = alloca %class.btIDebugDraw*, align 4
  %swapResults.addr = alloca i8, align 1
  store %class.btGjkPairDetector* %this, %class.btGjkPairDetector** %this.addr, align 4, !tbaa !2
  store %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4, !tbaa !2
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %output, %"struct.btDiscreteCollisionDetectorInterface::Result"** %output.addr, align 4, !tbaa !2
  store %class.btIDebugDraw* %debugDraw, %class.btIDebugDraw** %debugDraw.addr, align 4, !tbaa !2
  %frombool = zext i1 %swapResults to i8
  store i8 %frombool, i8* %swapResults.addr, align 1, !tbaa !29
  %this1 = load %class.btGjkPairDetector*, %class.btGjkPairDetector** %this.addr, align 4
  %0 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4, !tbaa !2
  %1 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %output.addr, align 4, !tbaa !2
  %2 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDraw.addr, align 4, !tbaa !2
  call void @_ZN17btGjkPairDetector26getClosestPointsNonVirtualERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDraw(%class.btGjkPairDetector* %this1, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* nonnull align 4 dereferenceable(132) %0, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %1, %class.btIDebugDraw* %2)
  ret void
}

define hidden void @_ZN17btGjkPairDetector26getClosestPointsNonVirtualERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDraw(%class.btGjkPairDetector* %this, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* nonnull align 4 dereferenceable(132) %input, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %output, %class.btIDebugDraw* %debugDraw) #0 {
entry:
  %this.addr = alloca %class.btGjkPairDetector*, align 4
  %input.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, align 4
  %output.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  %debugDraw.addr = alloca %class.btIDebugDraw*, align 4
  %distance = alloca float, align 4
  %normalInB = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %pointOnA = alloca %class.btVector3, align 4
  %pointOnB = alloca %class.btVector3, align 4
  %localTransA = alloca %class.btTransform, align 4
  %localTransB = alloca %class.btTransform, align 4
  %positionOffset = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca float, align 4
  %check2d = alloca i8, align 1
  %marginA = alloca float, align 4
  %marginB = alloca float, align 4
  %gGjkMaxIter = alloca i32, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %isValid = alloca i8, align 1
  %checkSimplex = alloca i8, align 1
  %checkPenetration = alloca i8, align 1
  %squaredDistance = alloca float, align 4
  %delta = alloca float, align 4
  %margin = alloca float, align 4
  %seperatingAxisInA = alloca %class.btVector3, align 4
  %ref.tmp21 = alloca %class.btVector3, align 4
  %seperatingAxisInB = alloca %class.btVector3, align 4
  %pInA = alloca %class.btVector3, align 4
  %qInB = alloca %class.btVector3, align 4
  %pWorld = alloca %class.btVector3, align 4
  %qWorld = alloca %class.btVector3, align 4
  %w = alloca %class.btVector3, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %f0 = alloca float, align 4
  %f1 = alloca float, align 4
  %newCachedSeparatingAxis = alloca %class.btVector3, align 4
  %previousSquaredDistance = alloca float, align 4
  %check = alloca i8, align 1
  %lenSqr = alloca float, align 4
  %rlen = alloca float, align 4
  %s = alloca float, align 4
  %ref.tmp116 = alloca %class.btVector3, align 4
  %ref.tmp118 = alloca float, align 4
  %ref.tmp121 = alloca %class.btVector3, align 4
  %ref.tmp123 = alloca float, align 4
  %catchDegeneratePenetrationCase = alloca i8, align 1
  %tmpPointOnA = alloca %class.btVector3, align 4
  %tmpPointOnB = alloca %class.btVector3, align 4
  %isValid2 = alloca i8, align 1
  %tmpNormalInB = alloca %class.btVector3, align 4
  %lenSqr166 = alloca float, align 4
  %ref.tmp176 = alloca float, align 4
  %distance2 = alloca float, align 4
  %ref.tmp179 = alloca %class.btVector3, align 4
  %distance2197 = alloca float, align 4
  %ref.tmp198 = alloca %class.btVector3, align 4
  %ref.tmp205 = alloca %class.btVector3, align 4
  %ref.tmp208 = alloca %class.btVector3, align 4
  %aabbMin = alloca %class.btVector3, align 4
  %aabbMax = alloca %class.btVector3, align 4
  %posA = alloca %class.btVector3, align 4
  %ref.tmp236 = alloca %class.btVector3, align 4
  %ref.tmp237 = alloca float, align 4
  %posB = alloca %class.btVector3, align 4
  %ref.tmp241 = alloca %class.btVector3, align 4
  %ref.tmp242 = alloca float, align 4
  %diff = alloca %class.btVector3, align 4
  %ref.tmp246 = alloca float, align 4
  %ref.tmp252 = alloca %class.btVector3, align 4
  store %class.btGjkPairDetector* %this, %class.btGjkPairDetector** %this.addr, align 4, !tbaa !2
  store %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4, !tbaa !2
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %output, %"struct.btDiscreteCollisionDetectorInterface::Result"** %output.addr, align 4, !tbaa !2
  store %class.btIDebugDraw* %debugDraw, %class.btIDebugDraw** %debugDraw.addr, align 4, !tbaa !2
  %this1 = load %class.btGjkPairDetector*, %class.btGjkPairDetector** %this.addr, align 4
  %m_cachedSeparatingDistance = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 11
  store float 0.000000e+00, float* %m_cachedSeparatingDistance, align 4, !tbaa !30
  %0 = bitcast float* %distance to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store float 0.000000e+00, float* %distance, align 4, !tbaa !8
  %1 = bitcast %class.btVector3* %normalInB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #9
  %2 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !8
  %4 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %normalInB, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #9
  %6 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  %7 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  %8 = bitcast %class.btVector3* %pointOnA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #9
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pointOnA)
  %9 = bitcast %class.btVector3* %pointOnB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #9
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pointOnB)
  %10 = bitcast %class.btTransform* %localTransA to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %10) #9
  %11 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4, !tbaa !2
  %m_transformA = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %11, i32 0, i32 0
  %call6 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %localTransA, %class.btTransform* nonnull align 4 dereferenceable(64) %m_transformA)
  %12 = bitcast %class.btTransform* %localTransB to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %12) #9
  %13 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4, !tbaa !2
  %m_transformB = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %13, i32 0, i32 1
  %call7 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %localTransB, %class.btTransform* nonnull align 4 dereferenceable(64) %m_transformB)
  %14 = bitcast %class.btVector3* %positionOffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %14) #9
  %15 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %15) #9
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %localTransA)
  %call10 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %localTransB)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp8, %class.btVector3* nonnull align 4 dereferenceable(16) %call9, %class.btVector3* nonnull align 4 dereferenceable(16) %call10)
  %16 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #9
  store float 5.000000e-01, float* %ref.tmp11, align 4, !tbaa !8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %positionOffset, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11)
  %17 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  %18 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #9
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %localTransA)
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %call12, %class.btVector3* nonnull align 4 dereferenceable(16) %positionOffset)
  %call14 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %localTransB)
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %call14, %class.btVector3* nonnull align 4 dereferenceable(16) %positionOffset)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %check2d) #9
  %m_minkowskiA = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 4
  %19 = load %class.btConvexShape*, %class.btConvexShape** %m_minkowskiA, align 4, !tbaa !16
  %20 = bitcast %class.btConvexShape* %19 to %class.btCollisionShape*
  %call16 = call zeroext i1 @_ZNK16btCollisionShape10isConvex2dEv(%class.btCollisionShape* %20)
  br i1 %call16, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %m_minkowskiB = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 5
  %21 = load %class.btConvexShape*, %class.btConvexShape** %m_minkowskiB, align 4, !tbaa !17
  %22 = bitcast %class.btConvexShape* %21 to %class.btCollisionShape*
  %call17 = call zeroext i1 @_ZNK16btCollisionShape10isConvex2dEv(%class.btCollisionShape* %22)
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %23 = phi i1 [ false, %entry ], [ %call17, %land.rhs ]
  %frombool = zext i1 %23 to i8
  store i8 %frombool, i8* %check2d, align 1, !tbaa !29
  %24 = bitcast float* %marginA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #9
  %m_marginA = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 8
  %25 = load float, float* %m_marginA, align 4, !tbaa !20
  store float %25, float* %marginA, align 4, !tbaa !8
  %26 = bitcast float* %marginB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #9
  %m_marginB = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 9
  %27 = load float, float* %m_marginB, align 4, !tbaa !21
  store float %27, float* %marginB, align 4, !tbaa !8
  %28 = load i32, i32* @gNumGjkChecks, align 4, !tbaa !28
  %inc = add nsw i32 %28, 1
  store i32 %inc, i32* @gNumGjkChecks, align 4, !tbaa !28
  %m_ignoreMargin = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 10
  %29 = load i8, i8* %m_ignoreMargin, align 4, !tbaa !22, !range !31
  %tobool = trunc i8 %29 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %land.end
  store float 0.000000e+00, float* %marginA, align 4, !tbaa !8
  store float 0.000000e+00, float* %marginB, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then, %land.end
  %m_curIter = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 13
  store i32 0, i32* %m_curIter, align 4, !tbaa !32
  %30 = bitcast i32* %gGjkMaxIter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #9
  store i32 1000, i32* %gGjkMaxIter, align 4, !tbaa !28
  %m_cachedSeparatingAxis = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %31 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #9
  store float 0.000000e+00, float* %ref.tmp18, align 4, !tbaa !8
  %32 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #9
  store float 1.000000e+00, float* %ref.tmp19, align 4, !tbaa !8
  %33 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #9
  store float 0.000000e+00, float* %ref.tmp20, align 4, !tbaa !8
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_cachedSeparatingAxis, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp19, float* nonnull align 4 dereferenceable(4) %ref.tmp20)
  %34 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #9
  %35 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #9
  %36 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #9
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %isValid) #9
  store i8 0, i8* %isValid, align 1, !tbaa !29
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %checkSimplex) #9
  store i8 0, i8* %checkSimplex, align 1, !tbaa !29
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %checkPenetration) #9
  store i8 1, i8* %checkPenetration, align 1, !tbaa !29
  %m_degenerateSimplex = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 14
  store i32 0, i32* %m_degenerateSimplex, align 4, !tbaa !33
  %m_lastUsedMethod = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 12
  store i32 -1, i32* %m_lastUsedMethod, align 4, !tbaa !23
  %37 = bitcast float* %squaredDistance to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #9
  store float 0x43ABC16D60000000, float* %squaredDistance, align 4, !tbaa !8
  %38 = bitcast float* %delta to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #9
  store float 0.000000e+00, float* %delta, align 4, !tbaa !8
  %39 = bitcast float* %margin to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #9
  %40 = load float, float* %marginA, align 4, !tbaa !8
  %41 = load float, float* %marginB, align 4, !tbaa !8
  %add = fadd float %40, %41
  store float %add, float* %margin, align 4, !tbaa !8
  %m_simplexSolver = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 3
  %42 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %m_simplexSolver, align 4, !tbaa !15
  call void @_ZN22btVoronoiSimplexSolver5resetEv(%class.btVoronoiSimplexSolver* %42)
  br label %for.cond

for.cond:                                         ; preds = %cleanup.cont, %if.end
  %43 = bitcast %class.btVector3* %seperatingAxisInA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %43) #9
  %44 = bitcast %class.btVector3* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %44) #9
  %m_cachedSeparatingAxis22 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp21, %class.btVector3* nonnull align 4 dereferenceable(16) %m_cachedSeparatingAxis22)
  %45 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4, !tbaa !2
  %m_transformA23 = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %45, i32 0, i32 0
  %call24 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %m_transformA23)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %seperatingAxisInA, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp21, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call24)
  %46 = bitcast %class.btVector3* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %46) #9
  %47 = bitcast %class.btVector3* %seperatingAxisInB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %47) #9
  %m_cachedSeparatingAxis25 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %48 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4, !tbaa !2
  %m_transformB26 = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %48, i32 0, i32 1
  %call27 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %m_transformB26)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %seperatingAxisInB, %class.btVector3* nonnull align 4 dereferenceable(16) %m_cachedSeparatingAxis25, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call27)
  %49 = bitcast %class.btVector3* %pInA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %49) #9
  %m_minkowskiA28 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 4
  %50 = load %class.btConvexShape*, %class.btConvexShape** %m_minkowskiA28, align 4, !tbaa !16
  call void @_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3(%class.btVector3* sret align 4 %pInA, %class.btConvexShape* %50, %class.btVector3* nonnull align 4 dereferenceable(16) %seperatingAxisInA)
  %51 = bitcast %class.btVector3* %qInB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %51) #9
  %m_minkowskiB29 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 5
  %52 = load %class.btConvexShape*, %class.btConvexShape** %m_minkowskiB29, align 4, !tbaa !17
  call void @_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3(%class.btVector3* sret align 4 %qInB, %class.btConvexShape* %52, %class.btVector3* nonnull align 4 dereferenceable(16) %seperatingAxisInB)
  %53 = bitcast %class.btVector3* %pWorld to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %53) #9
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %pWorld, %class.btTransform* %localTransA, %class.btVector3* nonnull align 4 dereferenceable(16) %pInA)
  %54 = bitcast %class.btVector3* %qWorld to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %54) #9
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %qWorld, %class.btTransform* %localTransB, %class.btVector3* nonnull align 4 dereferenceable(16) %qInB)
  %55 = load i8, i8* %check2d, align 1, !tbaa !29, !range !31
  %tobool30 = trunc i8 %55 to i1
  br i1 %tobool30, label %if.then31, label %if.end35

if.then31:                                        ; preds = %for.cond
  %call32 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pWorld)
  %arrayidx = getelementptr inbounds float, float* %call32, i32 2
  store float 0.000000e+00, float* %arrayidx, align 4, !tbaa !8
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %qWorld)
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 2
  store float 0.000000e+00, float* %arrayidx34, align 4, !tbaa !8
  br label %if.end35

if.end35:                                         ; preds = %if.then31, %for.cond
  %56 = bitcast %class.btVector3* %w to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %56) #9
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %w, %class.btVector3* nonnull align 4 dereferenceable(16) %pWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %qWorld)
  %m_cachedSeparatingAxis36 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %call37 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_cachedSeparatingAxis36, %class.btVector3* nonnull align 4 dereferenceable(16) %w)
  store float %call37, float* %delta, align 4, !tbaa !8
  %57 = load float, float* %delta, align 4, !tbaa !8
  %cmp = fcmp ogt float %57, 0.000000e+00
  br i1 %cmp, label %land.lhs.true, label %if.end42

land.lhs.true:                                    ; preds = %if.end35
  %58 = load float, float* %delta, align 4, !tbaa !8
  %59 = load float, float* %delta, align 4, !tbaa !8
  %mul = fmul float %58, %59
  %60 = load float, float* %squaredDistance, align 4, !tbaa !8
  %61 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4, !tbaa !2
  %m_maximumDistanceSquared = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %61, i32 0, i32 2
  %62 = load float, float* %m_maximumDistanceSquared, align 4, !tbaa !34
  %mul38 = fmul float %60, %62
  %cmp39 = fcmp ogt float %mul, %mul38
  br i1 %cmp39, label %if.then40, label %if.end42

if.then40:                                        ; preds = %land.lhs.true
  %m_degenerateSimplex41 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 14
  store i32 10, i32* %m_degenerateSimplex41, align 4, !tbaa !33
  store i8 1, i8* %checkSimplex, align 1, !tbaa !29
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup94

if.end42:                                         ; preds = %land.lhs.true, %if.end35
  %m_simplexSolver43 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 3
  %63 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %m_simplexSolver43, align 4, !tbaa !15
  %call44 = call zeroext i1 @_ZN22btVoronoiSimplexSolver9inSimplexERK9btVector3(%class.btVoronoiSimplexSolver* %63, %class.btVector3* nonnull align 4 dereferenceable(16) %w)
  br i1 %call44, label %if.then45, label %if.end47

if.then45:                                        ; preds = %if.end42
  %m_degenerateSimplex46 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 14
  store i32 1, i32* %m_degenerateSimplex46, align 4, !tbaa !33
  store i8 1, i8* %checkSimplex, align 1, !tbaa !29
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup94

if.end47:                                         ; preds = %if.end42
  %64 = bitcast float* %f0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #9
  %65 = load float, float* %squaredDistance, align 4, !tbaa !8
  %66 = load float, float* %delta, align 4, !tbaa !8
  %sub = fsub float %65, %66
  store float %sub, float* %f0, align 4, !tbaa !8
  %67 = bitcast float* %f1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #9
  %68 = load float, float* %squaredDistance, align 4, !tbaa !8
  %mul48 = fmul float %68, 0x3EB0C6F7A0000000
  store float %mul48, float* %f1, align 4, !tbaa !8
  %69 = load float, float* %f0, align 4, !tbaa !8
  %70 = load float, float* %f1, align 4, !tbaa !8
  %cmp49 = fcmp ole float %69, %70
  br i1 %cmp49, label %if.then50, label %if.end56

if.then50:                                        ; preds = %if.end47
  %71 = load float, float* %f0, align 4, !tbaa !8
  %cmp51 = fcmp ole float %71, 0.000000e+00
  br i1 %cmp51, label %if.then52, label %if.else

if.then52:                                        ; preds = %if.then50
  %m_degenerateSimplex53 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 14
  store i32 2, i32* %m_degenerateSimplex53, align 4, !tbaa !33
  br label %if.end55

if.else:                                          ; preds = %if.then50
  %m_degenerateSimplex54 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 14
  store i32 11, i32* %m_degenerateSimplex54, align 4, !tbaa !33
  br label %if.end55

if.end55:                                         ; preds = %if.else, %if.then52
  store i8 1, i8* %checkSimplex, align 1, !tbaa !29
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup92

if.end56:                                         ; preds = %if.end47
  %m_simplexSolver57 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 3
  %72 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %m_simplexSolver57, align 4, !tbaa !15
  call void @_ZN22btVoronoiSimplexSolver9addVertexERK9btVector3S2_S2_(%class.btVoronoiSimplexSolver* %72, %class.btVector3* nonnull align 4 dereferenceable(16) %w, %class.btVector3* nonnull align 4 dereferenceable(16) %pWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %qWorld)
  %73 = bitcast %class.btVector3* %newCachedSeparatingAxis to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %73) #9
  %call58 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %newCachedSeparatingAxis)
  %m_simplexSolver59 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 3
  %74 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %m_simplexSolver59, align 4, !tbaa !15
  %call60 = call zeroext i1 @_ZN22btVoronoiSimplexSolver7closestER9btVector3(%class.btVoronoiSimplexSolver* %74, %class.btVector3* nonnull align 4 dereferenceable(16) %newCachedSeparatingAxis)
  br i1 %call60, label %if.end63, label %if.then61

if.then61:                                        ; preds = %if.end56
  %m_degenerateSimplex62 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 14
  store i32 3, i32* %m_degenerateSimplex62, align 4, !tbaa !33
  store i8 1, i8* %checkSimplex, align 1, !tbaa !29
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup91

if.end63:                                         ; preds = %if.end56
  %call64 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %newCachedSeparatingAxis)
  %cmp65 = fcmp olt float %call64, 0x3EB0C6F7A0000000
  br i1 %cmp65, label %if.then66, label %if.end69

if.then66:                                        ; preds = %if.end63
  %m_cachedSeparatingAxis67 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %75 = bitcast %class.btVector3* %m_cachedSeparatingAxis67 to i8*
  %76 = bitcast %class.btVector3* %newCachedSeparatingAxis to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %75, i8* align 4 %76, i32 16, i1 false), !tbaa.struct !38
  %m_degenerateSimplex68 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 14
  store i32 6, i32* %m_degenerateSimplex68, align 4, !tbaa !33
  store i8 1, i8* %checkSimplex, align 1, !tbaa !29
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup91

if.end69:                                         ; preds = %if.end63
  %77 = bitcast float* %previousSquaredDistance to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %77) #9
  %78 = load float, float* %squaredDistance, align 4, !tbaa !8
  store float %78, float* %previousSquaredDistance, align 4, !tbaa !8
  %call70 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %newCachedSeparatingAxis)
  store float %call70, float* %squaredDistance, align 4, !tbaa !8
  %79 = load float, float* %previousSquaredDistance, align 4, !tbaa !8
  %80 = load float, float* %squaredDistance, align 4, !tbaa !8
  %sub71 = fsub float %79, %80
  %81 = load float, float* %previousSquaredDistance, align 4, !tbaa !8
  %mul72 = fmul float 0x3E80000000000000, %81
  %cmp73 = fcmp ole float %sub71, %mul72
  br i1 %cmp73, label %if.then74, label %if.end76

if.then74:                                        ; preds = %if.end69
  store i8 1, i8* %checkSimplex, align 1, !tbaa !29
  %m_degenerateSimplex75 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 14
  store i32 12, i32* %m_degenerateSimplex75, align 4, !tbaa !33
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup90

if.end76:                                         ; preds = %if.end69
  %m_cachedSeparatingAxis77 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %82 = bitcast %class.btVector3* %m_cachedSeparatingAxis77 to i8*
  %83 = bitcast %class.btVector3* %newCachedSeparatingAxis to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %82, i8* align 4 %83, i32 16, i1 false), !tbaa.struct !38
  %m_curIter78 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 13
  %84 = load i32, i32* %m_curIter78, align 4, !tbaa !32
  %inc79 = add nsw i32 %84, 1
  store i32 %inc79, i32* %m_curIter78, align 4, !tbaa !32
  %85 = load i32, i32* %gGjkMaxIter, align 4, !tbaa !28
  %cmp80 = icmp sgt i32 %84, %85
  br i1 %cmp80, label %if.then81, label %if.end82

if.then81:                                        ; preds = %if.end76
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup90

if.end82:                                         ; preds = %if.end76
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %check) #9
  %m_simplexSolver83 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 3
  %86 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %m_simplexSolver83, align 4, !tbaa !15
  %call84 = call zeroext i1 @_ZNK22btVoronoiSimplexSolver11fullSimplexEv(%class.btVoronoiSimplexSolver* %86)
  %lnot = xor i1 %call84, true
  %frombool85 = zext i1 %lnot to i8
  store i8 %frombool85, i8* %check, align 1, !tbaa !29
  %87 = load i8, i8* %check, align 1, !tbaa !29, !range !31
  %tobool86 = trunc i8 %87 to i1
  br i1 %tobool86, label %if.end89, label %if.then87

if.then87:                                        ; preds = %if.end82
  %m_degenerateSimplex88 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 14
  store i32 13, i32* %m_degenerateSimplex88, align 4, !tbaa !33
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end89:                                         ; preds = %if.end82
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end89, %if.then87
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %check) #9
  br label %cleanup90

cleanup90:                                        ; preds = %cleanup, %if.then81, %if.then74
  %88 = bitcast float* %previousSquaredDistance to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #9
  br label %cleanup91

cleanup91:                                        ; preds = %cleanup90, %if.then66, %if.then61
  %89 = bitcast %class.btVector3* %newCachedSeparatingAxis to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %89) #9
  br label %cleanup92

cleanup92:                                        ; preds = %cleanup91, %if.end55
  %90 = bitcast float* %f1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #9
  %91 = bitcast float* %f0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #9
  br label %cleanup94

cleanup94:                                        ; preds = %cleanup92, %if.then45, %if.then40
  %92 = bitcast %class.btVector3* %w to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %92) #9
  %93 = bitcast %class.btVector3* %qWorld to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %93) #9
  %94 = bitcast %class.btVector3* %pWorld to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %94) #9
  %95 = bitcast %class.btVector3* %qInB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %95) #9
  %96 = bitcast %class.btVector3* %pInA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %96) #9
  %97 = bitcast %class.btVector3* %seperatingAxisInB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %97) #9
  %98 = bitcast %class.btVector3* %seperatingAxisInA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %98) #9
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 2, label %for.end
  ]

cleanup.cont:                                     ; preds = %cleanup94
  br label %for.cond

for.end:                                          ; preds = %cleanup94
  %99 = load i8, i8* %checkSimplex, align 1, !tbaa !29, !range !31
  %tobool101 = trunc i8 %99 to i1
  br i1 %tobool101, label %if.then102, label %if.end132

if.then102:                                       ; preds = %for.end
  %m_simplexSolver103 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 3
  %100 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %m_simplexSolver103, align 4, !tbaa !15
  call void @_ZN22btVoronoiSimplexSolver14compute_pointsER9btVector3S1_(%class.btVoronoiSimplexSolver* %100, %class.btVector3* nonnull align 4 dereferenceable(16) %pointOnA, %class.btVector3* nonnull align 4 dereferenceable(16) %pointOnB)
  %m_cachedSeparatingAxis104 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %101 = bitcast %class.btVector3* %normalInB to i8*
  %102 = bitcast %class.btVector3* %m_cachedSeparatingAxis104 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %101, i8* align 4 %102, i32 16, i1 false), !tbaa.struct !38
  %103 = bitcast float* %lenSqr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %103) #9
  %m_cachedSeparatingAxis105 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %call106 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %m_cachedSeparatingAxis105)
  store float %call106, float* %lenSqr, align 4, !tbaa !8
  %104 = load float, float* %lenSqr, align 4, !tbaa !8
  %conv = fpext float %104 to double
  %cmp107 = fcmp olt double %conv, 1.000000e-04
  br i1 %cmp107, label %if.then108, label %if.end110

if.then108:                                       ; preds = %if.then102
  %m_degenerateSimplex109 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 14
  store i32 5, i32* %m_degenerateSimplex109, align 4, !tbaa !33
  br label %if.end110

if.end110:                                        ; preds = %if.then108, %if.then102
  %105 = load float, float* %lenSqr, align 4, !tbaa !8
  %cmp111 = fcmp ogt float %105, 0x3D10000000000000
  br i1 %cmp111, label %if.then112, label %if.else129

if.then112:                                       ; preds = %if.end110
  %106 = bitcast float* %rlen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %106) #9
  %107 = load float, float* %lenSqr, align 4, !tbaa !8
  %call113 = call float @_Z6btSqrtf(float %107)
  %div = fdiv float 1.000000e+00, %call113
  store float %div, float* %rlen, align 4, !tbaa !8
  %call114 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %normalInB, float* nonnull align 4 dereferenceable(4) %rlen)
  %108 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %108) #9
  %109 = load float, float* %squaredDistance, align 4, !tbaa !8
  %call115 = call float @_Z6btSqrtf(float %109)
  store float %call115, float* %s, align 4, !tbaa !8
  %110 = bitcast %class.btVector3* %ref.tmp116 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %110) #9
  %m_cachedSeparatingAxis117 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %111 = bitcast float* %ref.tmp118 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %111) #9
  %112 = load float, float* %marginA, align 4, !tbaa !8
  %113 = load float, float* %s, align 4, !tbaa !8
  %div119 = fdiv float %112, %113
  store float %div119, float* %ref.tmp118, align 4, !tbaa !8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp116, %class.btVector3* nonnull align 4 dereferenceable(16) %m_cachedSeparatingAxis117, float* nonnull align 4 dereferenceable(4) %ref.tmp118)
  %call120 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %pointOnA, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp116)
  %114 = bitcast float* %ref.tmp118 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #9
  %115 = bitcast %class.btVector3* %ref.tmp116 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %115) #9
  %116 = bitcast %class.btVector3* %ref.tmp121 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %116) #9
  %m_cachedSeparatingAxis122 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %117 = bitcast float* %ref.tmp123 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %117) #9
  %118 = load float, float* %marginB, align 4, !tbaa !8
  %119 = load float, float* %s, align 4, !tbaa !8
  %div124 = fdiv float %118, %119
  store float %div124, float* %ref.tmp123, align 4, !tbaa !8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp121, %class.btVector3* nonnull align 4 dereferenceable(16) %m_cachedSeparatingAxis122, float* nonnull align 4 dereferenceable(4) %ref.tmp123)
  %call125 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %pointOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp121)
  %120 = bitcast float* %ref.tmp123 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #9
  %121 = bitcast %class.btVector3* %ref.tmp121 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %121) #9
  %122 = load float, float* %rlen, align 4, !tbaa !8
  %div126 = fdiv float 1.000000e+00, %122
  %123 = load float, float* %margin, align 4, !tbaa !8
  %sub127 = fsub float %div126, %123
  store float %sub127, float* %distance, align 4, !tbaa !8
  store i8 1, i8* %isValid, align 1, !tbaa !29
  %m_lastUsedMethod128 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 12
  store i32 1, i32* %m_lastUsedMethod128, align 4, !tbaa !23
  %124 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #9
  %125 = bitcast float* %rlen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #9
  br label %if.end131

if.else129:                                       ; preds = %if.end110
  %m_lastUsedMethod130 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 12
  store i32 2, i32* %m_lastUsedMethod130, align 4, !tbaa !23
  br label %if.end131

if.end131:                                        ; preds = %if.else129, %if.then112
  %126 = bitcast float* %lenSqr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #9
  br label %if.end132

if.end132:                                        ; preds = %if.end131, %for.end
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %catchDegeneratePenetrationCase) #9
  %m_catchDegeneracies = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 15
  %127 = load i32, i32* %m_catchDegeneracies, align 4, !tbaa !24
  %tobool133 = icmp ne i32 %127, 0
  br i1 %tobool133, label %land.lhs.true134, label %land.end143

land.lhs.true134:                                 ; preds = %if.end132
  %m_penetrationDepthSolver = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 2
  %128 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %m_penetrationDepthSolver, align 4, !tbaa !10
  %tobool135 = icmp ne %class.btConvexPenetrationDepthSolver* %128, null
  br i1 %tobool135, label %land.lhs.true136, label %land.end143

land.lhs.true136:                                 ; preds = %land.lhs.true134
  %m_degenerateSimplex137 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 14
  %129 = load i32, i32* %m_degenerateSimplex137, align 4, !tbaa !33
  %tobool138 = icmp ne i32 %129, 0
  br i1 %tobool138, label %land.rhs139, label %land.end143

land.rhs139:                                      ; preds = %land.lhs.true136
  %130 = load float, float* %distance, align 4, !tbaa !8
  %131 = load float, float* %margin, align 4, !tbaa !8
  %add140 = fadd float %130, %131
  %conv141 = fpext float %add140 to double
  %cmp142 = fcmp olt double %conv141, 1.000000e-02
  br label %land.end143

land.end143:                                      ; preds = %land.rhs139, %land.lhs.true136, %land.lhs.true134, %if.end132
  %132 = phi i1 [ false, %land.lhs.true136 ], [ false, %land.lhs.true134 ], [ false, %if.end132 ], [ %cmp142, %land.rhs139 ]
  %frombool144 = zext i1 %132 to i8
  store i8 %frombool144, i8* %catchDegeneratePenetrationCase, align 1, !tbaa !29
  %133 = load i8, i8* %checkPenetration, align 1, !tbaa !29, !range !31
  %tobool145 = trunc i8 %133 to i1
  br i1 %tobool145, label %land.lhs.true146, label %if.end220

land.lhs.true146:                                 ; preds = %land.end143
  %134 = load i8, i8* %isValid, align 1, !tbaa !29, !range !31
  %tobool147 = trunc i8 %134 to i1
  br i1 %tobool147, label %lor.lhs.false, label %if.then149

lor.lhs.false:                                    ; preds = %land.lhs.true146
  %135 = load i8, i8* %catchDegeneratePenetrationCase, align 1, !tbaa !29, !range !31
  %tobool148 = trunc i8 %135 to i1
  br i1 %tobool148, label %if.then149, label %if.end220

if.then149:                                       ; preds = %lor.lhs.false, %land.lhs.true146
  %m_penetrationDepthSolver150 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 2
  %136 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %m_penetrationDepthSolver150, align 4, !tbaa !10
  %tobool151 = icmp ne %class.btConvexPenetrationDepthSolver* %136, null
  br i1 %tobool151, label %if.then152, label %if.end219

if.then152:                                       ; preds = %if.then149
  %137 = bitcast %class.btVector3* %tmpPointOnA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %137) #9
  %call153 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %tmpPointOnA)
  %138 = bitcast %class.btVector3* %tmpPointOnB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %138) #9
  %call154 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %tmpPointOnB)
  %139 = load i32, i32* @gNumDeepPenetrationChecks, align 4, !tbaa !28
  %inc155 = add nsw i32 %139, 1
  store i32 %inc155, i32* @gNumDeepPenetrationChecks, align 4, !tbaa !28
  %m_cachedSeparatingAxis156 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  call void @_ZN9btVector37setZeroEv(%class.btVector3* %m_cachedSeparatingAxis156)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %isValid2) #9
  %m_penetrationDepthSolver157 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 2
  %140 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %m_penetrationDepthSolver157, align 4, !tbaa !10
  %m_simplexSolver158 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 3
  %141 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %m_simplexSolver158, align 4, !tbaa !15
  %m_minkowskiA159 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 4
  %142 = load %class.btConvexShape*, %class.btConvexShape** %m_minkowskiA159, align 4, !tbaa !16
  %m_minkowskiB160 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 5
  %143 = load %class.btConvexShape*, %class.btConvexShape** %m_minkowskiB160, align 4, !tbaa !17
  %m_cachedSeparatingAxis161 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %144 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDraw.addr, align 4, !tbaa !2
  %145 = bitcast %class.btConvexPenetrationDepthSolver* %140 to i1 (%class.btConvexPenetrationDepthSolver*, %class.btVoronoiSimplexSolver*, %class.btConvexShape*, %class.btConvexShape*, %class.btTransform*, %class.btTransform*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btIDebugDraw*)***
  %vtable = load i1 (%class.btConvexPenetrationDepthSolver*, %class.btVoronoiSimplexSolver*, %class.btConvexShape*, %class.btConvexShape*, %class.btTransform*, %class.btTransform*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btIDebugDraw*)**, i1 (%class.btConvexPenetrationDepthSolver*, %class.btVoronoiSimplexSolver*, %class.btConvexShape*, %class.btConvexShape*, %class.btTransform*, %class.btTransform*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btIDebugDraw*)*** %145, align 4, !tbaa !6
  %vfn = getelementptr inbounds i1 (%class.btConvexPenetrationDepthSolver*, %class.btVoronoiSimplexSolver*, %class.btConvexShape*, %class.btConvexShape*, %class.btTransform*, %class.btTransform*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btIDebugDraw*)*, i1 (%class.btConvexPenetrationDepthSolver*, %class.btVoronoiSimplexSolver*, %class.btConvexShape*, %class.btConvexShape*, %class.btTransform*, %class.btTransform*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btIDebugDraw*)** %vtable, i64 2
  %146 = load i1 (%class.btConvexPenetrationDepthSolver*, %class.btVoronoiSimplexSolver*, %class.btConvexShape*, %class.btConvexShape*, %class.btTransform*, %class.btTransform*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btIDebugDraw*)*, i1 (%class.btConvexPenetrationDepthSolver*, %class.btVoronoiSimplexSolver*, %class.btConvexShape*, %class.btConvexShape*, %class.btTransform*, %class.btTransform*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btIDebugDraw*)** %vfn, align 4
  %call162 = call zeroext i1 %146(%class.btConvexPenetrationDepthSolver* %140, %class.btVoronoiSimplexSolver* nonnull align 4 dereferenceable(357) %141, %class.btConvexShape* %142, %class.btConvexShape* %143, %class.btTransform* nonnull align 4 dereferenceable(64) %localTransA, %class.btTransform* nonnull align 4 dereferenceable(64) %localTransB, %class.btVector3* nonnull align 4 dereferenceable(16) %m_cachedSeparatingAxis161, %class.btVector3* nonnull align 4 dereferenceable(16) %tmpPointOnA, %class.btVector3* nonnull align 4 dereferenceable(16) %tmpPointOnB, %class.btIDebugDraw* %144)
  %frombool163 = zext i1 %call162 to i8
  store i8 %frombool163, i8* %isValid2, align 1, !tbaa !29
  %147 = load i8, i8* %isValid2, align 1, !tbaa !29, !range !31
  %tobool164 = trunc i8 %147 to i1
  br i1 %tobool164, label %if.then165, label %if.else192

if.then165:                                       ; preds = %if.then152
  %148 = bitcast %class.btVector3* %tmpNormalInB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %148) #9
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %tmpNormalInB, %class.btVector3* nonnull align 4 dereferenceable(16) %tmpPointOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %tmpPointOnA)
  %149 = bitcast float* %lenSqr166 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %149) #9
  %call167 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %tmpNormalInB)
  store float %call167, float* %lenSqr166, align 4, !tbaa !8
  %150 = load float, float* %lenSqr166, align 4, !tbaa !8
  %cmp168 = fcmp ole float %150, 0x3D10000000000000
  br i1 %cmp168, label %if.then169, label %if.end173

if.then169:                                       ; preds = %if.then165
  %m_cachedSeparatingAxis170 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %151 = bitcast %class.btVector3* %tmpNormalInB to i8*
  %152 = bitcast %class.btVector3* %m_cachedSeparatingAxis170 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %151, i8* align 4 %152, i32 16, i1 false), !tbaa.struct !38
  %m_cachedSeparatingAxis171 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %call172 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %m_cachedSeparatingAxis171)
  store float %call172, float* %lenSqr166, align 4, !tbaa !8
  br label %if.end173

if.end173:                                        ; preds = %if.then169, %if.then165
  %153 = load float, float* %lenSqr166, align 4, !tbaa !8
  %cmp174 = fcmp ogt float %153, 0x3D10000000000000
  br i1 %cmp174, label %if.then175, label %if.else189

if.then175:                                       ; preds = %if.end173
  %154 = bitcast float* %ref.tmp176 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %154) #9
  %155 = load float, float* %lenSqr166, align 4, !tbaa !8
  %call177 = call float @_Z6btSqrtf(float %155)
  store float %call177, float* %ref.tmp176, align 4, !tbaa !8
  %call178 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %tmpNormalInB, float* nonnull align 4 dereferenceable(4) %ref.tmp176)
  %156 = bitcast float* %ref.tmp176 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %156) #9
  %157 = bitcast float* %distance2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %157) #9
  %158 = bitcast %class.btVector3* %ref.tmp179 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %158) #9
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp179, %class.btVector3* nonnull align 4 dereferenceable(16) %tmpPointOnA, %class.btVector3* nonnull align 4 dereferenceable(16) %tmpPointOnB)
  %call180 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp179)
  %fneg = fneg float %call180
  %159 = bitcast %class.btVector3* %ref.tmp179 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %159) #9
  store float %fneg, float* %distance2, align 4, !tbaa !8
  %160 = load i8, i8* %isValid, align 1, !tbaa !29, !range !31
  %tobool181 = trunc i8 %160 to i1
  br i1 %tobool181, label %lor.lhs.false182, label %if.then184

lor.lhs.false182:                                 ; preds = %if.then175
  %161 = load float, float* %distance2, align 4, !tbaa !8
  %162 = load float, float* %distance, align 4, !tbaa !8
  %cmp183 = fcmp olt float %161, %162
  br i1 %cmp183, label %if.then184, label %if.else186

if.then184:                                       ; preds = %lor.lhs.false182, %if.then175
  %163 = load float, float* %distance2, align 4, !tbaa !8
  store float %163, float* %distance, align 4, !tbaa !8
  %164 = bitcast %class.btVector3* %pointOnA to i8*
  %165 = bitcast %class.btVector3* %tmpPointOnA to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %164, i8* align 4 %165, i32 16, i1 false), !tbaa.struct !38
  %166 = bitcast %class.btVector3* %pointOnB to i8*
  %167 = bitcast %class.btVector3* %tmpPointOnB to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %166, i8* align 4 %167, i32 16, i1 false), !tbaa.struct !38
  %168 = bitcast %class.btVector3* %normalInB to i8*
  %169 = bitcast %class.btVector3* %tmpNormalInB to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %168, i8* align 4 %169, i32 16, i1 false), !tbaa.struct !38
  store i8 1, i8* %isValid, align 1, !tbaa !29
  %m_lastUsedMethod185 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 12
  store i32 3, i32* %m_lastUsedMethod185, align 4, !tbaa !23
  br label %if.end188

if.else186:                                       ; preds = %lor.lhs.false182
  %m_lastUsedMethod187 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 12
  store i32 8, i32* %m_lastUsedMethod187, align 4, !tbaa !23
  br label %if.end188

if.end188:                                        ; preds = %if.else186, %if.then184
  %170 = bitcast float* %distance2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %170) #9
  br label %if.end191

if.else189:                                       ; preds = %if.end173
  %m_lastUsedMethod190 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 12
  store i32 9, i32* %m_lastUsedMethod190, align 4, !tbaa !23
  br label %if.end191

if.end191:                                        ; preds = %if.else189, %if.end188
  %171 = bitcast float* %lenSqr166 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #9
  %172 = bitcast %class.btVector3* %tmpNormalInB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %172) #9
  br label %if.end218

if.else192:                                       ; preds = %if.then152
  %m_cachedSeparatingAxis193 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %call194 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %m_cachedSeparatingAxis193)
  %cmp195 = fcmp ogt float %call194, 0.000000e+00
  br i1 %cmp195, label %if.then196, label %if.end217

if.then196:                                       ; preds = %if.else192
  %173 = bitcast float* %distance2197 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %173) #9
  %174 = bitcast %class.btVector3* %ref.tmp198 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %174) #9
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp198, %class.btVector3* nonnull align 4 dereferenceable(16) %tmpPointOnA, %class.btVector3* nonnull align 4 dereferenceable(16) %tmpPointOnB)
  %call199 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp198)
  %175 = load float, float* %margin, align 4, !tbaa !8
  %sub200 = fsub float %call199, %175
  %176 = bitcast %class.btVector3* %ref.tmp198 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %176) #9
  store float %sub200, float* %distance2197, align 4, !tbaa !8
  %177 = load i8, i8* %isValid, align 1, !tbaa !29, !range !31
  %tobool201 = trunc i8 %177 to i1
  br i1 %tobool201, label %lor.lhs.false202, label %if.then204

lor.lhs.false202:                                 ; preds = %if.then196
  %178 = load float, float* %distance2197, align 4, !tbaa !8
  %179 = load float, float* %distance, align 4, !tbaa !8
  %cmp203 = fcmp olt float %178, %179
  br i1 %cmp203, label %if.then204, label %if.else214

if.then204:                                       ; preds = %lor.lhs.false202, %if.then196
  %180 = load float, float* %distance2197, align 4, !tbaa !8
  store float %180, float* %distance, align 4, !tbaa !8
  %181 = bitcast %class.btVector3* %pointOnA to i8*
  %182 = bitcast %class.btVector3* %tmpPointOnA to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %181, i8* align 4 %182, i32 16, i1 false), !tbaa.struct !38
  %183 = bitcast %class.btVector3* %pointOnB to i8*
  %184 = bitcast %class.btVector3* %tmpPointOnB to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %183, i8* align 4 %184, i32 16, i1 false), !tbaa.struct !38
  %185 = bitcast %class.btVector3* %ref.tmp205 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %185) #9
  %m_cachedSeparatingAxis206 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp205, %class.btVector3* nonnull align 4 dereferenceable(16) %m_cachedSeparatingAxis206, float* nonnull align 4 dereferenceable(4) %marginA)
  %call207 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %pointOnA, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp205)
  %186 = bitcast %class.btVector3* %ref.tmp205 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %186) #9
  %187 = bitcast %class.btVector3* %ref.tmp208 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %187) #9
  %m_cachedSeparatingAxis209 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp208, %class.btVector3* nonnull align 4 dereferenceable(16) %m_cachedSeparatingAxis209, float* nonnull align 4 dereferenceable(4) %marginB)
  %call210 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %pointOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp208)
  %188 = bitcast %class.btVector3* %ref.tmp208 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %188) #9
  %m_cachedSeparatingAxis211 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %189 = bitcast %class.btVector3* %normalInB to i8*
  %190 = bitcast %class.btVector3* %m_cachedSeparatingAxis211 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %189, i8* align 4 %190, i32 16, i1 false), !tbaa.struct !38
  %call212 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %normalInB)
  store i8 1, i8* %isValid, align 1, !tbaa !29
  %m_lastUsedMethod213 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 12
  store i32 6, i32* %m_lastUsedMethod213, align 4, !tbaa !23
  br label %if.end216

if.else214:                                       ; preds = %lor.lhs.false202
  %m_lastUsedMethod215 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 12
  store i32 5, i32* %m_lastUsedMethod215, align 4, !tbaa !23
  br label %if.end216

if.end216:                                        ; preds = %if.else214, %if.then204
  %191 = bitcast float* %distance2197 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %191) #9
  br label %if.end217

if.end217:                                        ; preds = %if.end216, %if.else192
  br label %if.end218

if.end218:                                        ; preds = %if.end217, %if.end191
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %isValid2) #9
  %192 = bitcast %class.btVector3* %tmpPointOnB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %192) #9
  %193 = bitcast %class.btVector3* %tmpPointOnA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %193) #9
  br label %if.end219

if.end219:                                        ; preds = %if.end218, %if.then149
  br label %if.end220

if.end220:                                        ; preds = %if.end219, %lor.lhs.false, %land.end143
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %catchDegeneratePenetrationCase) #9
  %194 = bitcast float* %margin to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %194) #9
  %195 = bitcast float* %delta to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %195) #9
  %196 = bitcast float* %squaredDistance to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %196) #9
  %197 = load i8, i8* %isValid, align 1, !tbaa !29, !range !31
  %tobool221 = trunc i8 %197 to i1
  br i1 %tobool221, label %land.lhs.true222, label %if.end255

land.lhs.true222:                                 ; preds = %if.end220
  %198 = load float, float* %distance, align 4, !tbaa !8
  %cmp223 = fcmp olt float %198, 0.000000e+00
  br i1 %cmp223, label %if.then228, label %lor.lhs.false224

lor.lhs.false224:                                 ; preds = %land.lhs.true222
  %199 = load float, float* %distance, align 4, !tbaa !8
  %200 = load float, float* %distance, align 4, !tbaa !8
  %mul225 = fmul float %199, %200
  %201 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4, !tbaa !2
  %m_maximumDistanceSquared226 = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %201, i32 0, i32 2
  %202 = load float, float* %m_maximumDistanceSquared226, align 4, !tbaa !34
  %cmp227 = fcmp olt float %mul225, %202
  br i1 %cmp227, label %if.then228, label %if.end255

if.then228:                                       ; preds = %lor.lhs.false224, %land.lhs.true222
  %m_fixContactNormalDirection = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 16
  %203 = load i32, i32* %m_fixContactNormalDirection, align 4, !tbaa !25
  %tobool229 = icmp ne i32 %203, 0
  br i1 %tobool229, label %if.then230, label %if.end249

if.then230:                                       ; preds = %if.then228
  %204 = bitcast %class.btVector3* %aabbMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %204) #9
  %call231 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin)
  %205 = bitcast %class.btVector3* %aabbMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %205) #9
  %call232 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax)
  %m_minkowskiA233 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 4
  %206 = load %class.btConvexShape*, %class.btConvexShape** %m_minkowskiA233, align 4, !tbaa !16
  %207 = bitcast %class.btConvexShape* %206 to void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable234 = load void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %207, align 4, !tbaa !6
  %vfn235 = getelementptr inbounds void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable234, i64 2
  %208 = load void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn235, align 4
  call void %208(%class.btConvexShape* %206, %class.btTransform* nonnull align 4 dereferenceable(64) %localTransA, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax)
  %209 = bitcast %class.btVector3* %posA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %209) #9
  %210 = bitcast %class.btVector3* %ref.tmp236 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %210) #9
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp236, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin)
  %211 = bitcast float* %ref.tmp237 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %211) #9
  store float 5.000000e-01, float* %ref.tmp237, align 4, !tbaa !8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %posA, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp236, float* nonnull align 4 dereferenceable(4) %ref.tmp237)
  %212 = bitcast float* %ref.tmp237 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %212) #9
  %213 = bitcast %class.btVector3* %ref.tmp236 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %213) #9
  %m_minkowskiB238 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 5
  %214 = load %class.btConvexShape*, %class.btConvexShape** %m_minkowskiB238, align 4, !tbaa !17
  %215 = bitcast %class.btConvexShape* %214 to void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable239 = load void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %215, align 4, !tbaa !6
  %vfn240 = getelementptr inbounds void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable239, i64 2
  %216 = load void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn240, align 4
  call void %216(%class.btConvexShape* %214, %class.btTransform* nonnull align 4 dereferenceable(64) %localTransB, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax)
  %217 = bitcast %class.btVector3* %posB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %217) #9
  %218 = bitcast %class.btVector3* %ref.tmp241 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %218) #9
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp241, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax)
  %219 = bitcast float* %ref.tmp242 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %219) #9
  store float 5.000000e-01, float* %ref.tmp242, align 4, !tbaa !8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %posB, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp241, float* nonnull align 4 dereferenceable(4) %ref.tmp242)
  %220 = bitcast float* %ref.tmp242 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %220) #9
  %221 = bitcast %class.btVector3* %ref.tmp241 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %221) #9
  %222 = bitcast %class.btVector3* %diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %222) #9
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %diff, %class.btVector3* nonnull align 4 dereferenceable(16) %posA, %class.btVector3* nonnull align 4 dereferenceable(16) %posB)
  %call243 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %diff, %class.btVector3* nonnull align 4 dereferenceable(16) %normalInB)
  %cmp244 = fcmp olt float %call243, 0.000000e+00
  br i1 %cmp244, label %if.then245, label %if.end248

if.then245:                                       ; preds = %if.then230
  %223 = bitcast float* %ref.tmp246 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %223) #9
  store float -1.000000e+00, float* %ref.tmp246, align 4, !tbaa !8
  %call247 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %normalInB, float* nonnull align 4 dereferenceable(4) %ref.tmp246)
  %224 = bitcast float* %ref.tmp246 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %224) #9
  br label %if.end248

if.end248:                                        ; preds = %if.then245, %if.then230
  %225 = bitcast %class.btVector3* %diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %225) #9
  %226 = bitcast %class.btVector3* %posB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %226) #9
  %227 = bitcast %class.btVector3* %posA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %227) #9
  %228 = bitcast %class.btVector3* %aabbMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %228) #9
  %229 = bitcast %class.btVector3* %aabbMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %229) #9
  br label %if.end249

if.end249:                                        ; preds = %if.end248, %if.then228
  %m_cachedSeparatingAxis250 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %230 = bitcast %class.btVector3* %m_cachedSeparatingAxis250 to i8*
  %231 = bitcast %class.btVector3* %normalInB to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %230, i8* align 4 %231, i32 16, i1 false), !tbaa.struct !38
  %232 = load float, float* %distance, align 4, !tbaa !8
  %m_cachedSeparatingDistance251 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 11
  store float %232, float* %m_cachedSeparatingDistance251, align 4, !tbaa !30
  %233 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %output.addr, align 4, !tbaa !2
  %234 = bitcast %class.btVector3* %ref.tmp252 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %234) #9
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp252, %class.btVector3* nonnull align 4 dereferenceable(16) %pointOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %positionOffset)
  %235 = load float, float* %distance, align 4, !tbaa !8
  %236 = bitcast %"struct.btDiscreteCollisionDetectorInterface::Result"* %233 to void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)***
  %vtable253 = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)**, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*** %236, align 4, !tbaa !6
  %vfn254 = getelementptr inbounds void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vtable253, i64 4
  %237 = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vfn254, align 4
  call void %237(%"struct.btDiscreteCollisionDetectorInterface::Result"* %233, %class.btVector3* nonnull align 4 dereferenceable(16) %normalInB, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp252, float %235)
  %238 = bitcast %class.btVector3* %ref.tmp252 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %238) #9
  br label %if.end255

if.end255:                                        ; preds = %if.end249, %lor.lhs.false224, %if.end220
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %checkPenetration) #9
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %checkSimplex) #9
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %isValid) #9
  %239 = bitcast i32* %gGjkMaxIter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %239) #9
  %240 = bitcast float* %marginB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %240) #9
  %241 = bitcast float* %marginA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %241) #9
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %check2d) #9
  %242 = bitcast %class.btVector3* %positionOffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %242) #9
  %243 = bitcast %class.btTransform* %localTransB to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %243) #9
  %244 = bitcast %class.btTransform* %localTransA to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %244) #9
  %245 = bitcast %class.btVector3* %pointOnB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %245) #9
  %246 = bitcast %class.btVector3* %pointOnA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %246) #9
  %247 = bitcast %class.btVector3* %normalInB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %247) #9
  %248 = bitcast float* %distance to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %248) #9
  ret void

unreachable:                                      ; preds = %cleanup94
  unreachable
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !38
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #4 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !8
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !8
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !8
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !8
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !8
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !8
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !8
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !8
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !8
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !8
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !8
  %sub = fsub float %2, %1
  store float %sub, float* %arrayidx3, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !8
  %sub8 = fsub float %5, %4
  store float %sub8, float* %arrayidx7, align 4, !tbaa !8
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !8
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !8
  %sub13 = fsub float %8, %7
  store float %sub13, float* %arrayidx12, align 4, !tbaa !8
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_ZNK16btCollisionShape10isConvex2dEv(%class.btCollisionShape* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %call = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %this1)
  %call2 = call zeroext i1 @_ZN17btBroadphaseProxy10isConvex2dEi(i32 %call)
  ret i1 %call2
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !8
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !8
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !8
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !8
  ret void
}

declare void @_ZN22btVoronoiSimplexSolver5resetEv(%class.btVoronoiSimplexSolver*) #5

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m) #4 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call, float* %ref.tmp, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call2, float* %ref.tmp1, align 4, !tbaa !8
  %6 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call4, float* %ref.tmp3, align 4, !tbaa !8
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %9 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !8
  %fneg4 = fneg float %5
  store float %fneg4, float* %ref.tmp1, align 4, !tbaa !8
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %8 = load float, float* %arrayidx7, align 4, !tbaa !8
  %fneg8 = fneg float %8
  store float %fneg8, float* %ref.tmp5, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

declare void @_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3(%class.btVector3* sret align 4, %class.btConvexShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) #5

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !8
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !8
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !8
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !8
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !8
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !8
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !8
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !8
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

declare zeroext i1 @_ZN22btVoronoiSimplexSolver9inSimplexERK9btVector3(%class.btVoronoiSimplexSolver*, %class.btVector3* nonnull align 4 dereferenceable(16)) #5

declare void @_ZN22btVoronoiSimplexSolver9addVertexERK9btVector3S2_S2_(%class.btVoronoiSimplexSolver*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) #5

declare zeroext i1 @_ZN22btVoronoiSimplexSolver7closestER9btVector3(%class.btVoronoiSimplexSolver*, %class.btVector3* nonnull align 4 dereferenceable(16)) #5

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZNK22btVoronoiSimplexSolver11fullSimplexEv(%class.btVoronoiSimplexSolver* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %m_numVertices = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 0
  %0 = load i32, i32* %m_numVertices, align 4, !tbaa !40
  %cmp = icmp eq i32 %0, 4
  ret i1 %cmp
}

declare void @_ZN22btVoronoiSimplexSolver14compute_pointsER9btVector3S1_(%class.btVoronoiSimplexSolver*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) #5

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !8
  %0 = load float, float* %y.addr, align 4, !tbaa !8
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !8
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !8
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !8
  %6 = load float*, float** %s.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !8
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !8
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !8
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !8
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !8
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !8
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !8
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !8
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !8
  ret %class.btVector3* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN9btVector37setZeroEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !8
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !8
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !8
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !8
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !8
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  ret %class.btVector3* %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4, !tbaa !8
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1) #9
  ret %class.btVector3* %call2
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev(%struct.btDiscreteCollisionDetectorInterface* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %struct.btDiscreteCollisionDetectorInterface*, align 4
  store %struct.btDiscreteCollisionDetectorInterface* %this, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btDiscreteCollisionDetectorInterface*, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  ret %struct.btDiscreteCollisionDetectorInterface* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN17btGjkPairDetectorD0Ev(%class.btGjkPairDetector* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btGjkPairDetector*, align 4
  store %class.btGjkPairDetector* %this, %class.btGjkPairDetector** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGjkPairDetector*, %class.btGjkPairDetector** %this.addr, align 4
  %call = call %class.btGjkPairDetector* bitcast (%struct.btDiscreteCollisionDetectorInterface* (%struct.btDiscreteCollisionDetectorInterface*)* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev to %class.btGjkPairDetector* (%class.btGjkPairDetector*)*)(%class.btGjkPairDetector* %this1) #9
  %0 = bitcast %class.btGjkPairDetector* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN36btDiscreteCollisionDetectorInterfaceD0Ev(%struct.btDiscreteCollisionDetectorInterface* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %struct.btDiscreteCollisionDetectorInterface*, align 4
  store %struct.btDiscreteCollisionDetectorInterface* %this, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btDiscreteCollisionDetectorInterface*, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  call void @llvm.trap() #11
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #6

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #4 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !38
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !38
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !38
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZN17btBroadphaseProxy10isConvex2dEi(i32 %proxyType) #1 comdat {
entry:
  %proxyType.addr = alloca i32, align 4
  store i32 %proxyType, i32* %proxyType.addr, align 4, !tbaa !28
  %0 = load i32, i32* %proxyType.addr, align 4, !tbaa !28
  %cmp = icmp eq i32 %0, 17
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %1 = load i32, i32* %proxyType.addr, align 4, !tbaa !28
  %cmp1 = icmp eq i32 %1, 18
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %2 = phi i1 [ true, %entry ], [ %cmp1, %lor.rhs ]
  ret i1 %2
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !8
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !8
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !8
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !8
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !8
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !8
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !8
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !8
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !8
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4, !tbaa !8
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !8
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !8
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !28
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #7

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #8

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { cold noreturn nounwind }
attributes #7 = { nounwind readnone speculatable willreturn }
attributes #8 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { nounwind }
attributes #10 = { builtin nounwind }
attributes #11 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"float", !4, i64 0}
!10 = !{!11, !3, i64 20}
!11 = !{!"_ZTS17btGjkPairDetector", !12, i64 4, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !13, i64 36, !13, i64 40, !9, i64 44, !9, i64 48, !14, i64 52, !9, i64 56, !13, i64 60, !13, i64 64, !13, i64 68, !13, i64 72, !13, i64 76}
!12 = !{!"_ZTS9btVector3", !4, i64 0}
!13 = !{!"int", !4, i64 0}
!14 = !{!"bool", !4, i64 0}
!15 = !{!11, !3, i64 24}
!16 = !{!11, !3, i64 28}
!17 = !{!11, !3, i64 32}
!18 = !{!11, !13, i64 36}
!19 = !{!11, !13, i64 40}
!20 = !{!11, !9, i64 44}
!21 = !{!11, !9, i64 48}
!22 = !{!11, !14, i64 52}
!23 = !{!11, !13, i64 60}
!24 = !{!11, !13, i64 72}
!25 = !{!11, !13, i64 76}
!26 = !{!27, !13, i64 4}
!27 = !{!"_ZTS16btCollisionShape", !13, i64 4, !3, i64 8}
!28 = !{!13, !13, i64 0}
!29 = !{!14, !14, i64 0}
!30 = !{!11, !9, i64 56}
!31 = !{i8 0, i8 2}
!32 = !{!11, !13, i64 64}
!33 = !{!11, !13, i64 68}
!34 = !{!35, !9, i64 128}
!35 = !{!"_ZTSN36btDiscreteCollisionDetectorInterface17ClosestPointInputE", !36, i64 0, !36, i64 64, !9, i64 128}
!36 = !{!"_ZTS11btTransform", !37, i64 0, !12, i64 48}
!37 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!38 = !{i64 0, i64 16, !39}
!39 = !{!4, !4, i64 0}
!40 = !{!41, !13, i64 0}
!41 = !{!"_ZTS22btVoronoiSimplexSolver", !13, i64 0, !4, i64 4, !4, i64 84, !4, i64 164, !12, i64 244, !12, i64 260, !12, i64 276, !12, i64 292, !9, i64 308, !14, i64 312, !42, i64 316, !14, i64 356}
!42 = !{!"_ZTS25btSubSimplexClosestResult", !12, i64 0, !43, i64 16, !4, i64 20, !14, i64 36}
!43 = !{!"_ZTS15btUsageBitfield", !44, i64 0, !44, i64 0, !44, i64 0, !44, i64 0, !44, i64 0, !44, i64 0, !44, i64 0, !44, i64 0}
!44 = !{!"short", !4, i64 0}
