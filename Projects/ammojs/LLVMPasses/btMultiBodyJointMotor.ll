; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/Featherstone/btMultiBodyJointMotor.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/Featherstone/btMultiBodyJointMotor.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btMultiBodyJointMotor = type { %class.btMultiBodyConstraint, float }
%class.btMultiBodyConstraint = type { i32 (...)**, %class.btMultiBody*, %class.btMultiBody*, i32, i32, i32, i32, i32, i32, i8, float, %class.btAlignedObjectArray.4 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btMultiBody = type <{ %class.btMultiBodyLinkCollider*, %class.btVector3, %class.btQuaternion, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btAlignedObjectArray, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3, i8, i8, i8, i8, float, i32, float, float, i8, [3 x i8], float, i8, [3 x i8] }>
%class.btMultiBodyLinkCollider = type { %class.btCollisionObject, %class.btMultiBody*, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%struct.btBroadphaseProxy = type opaque
%class.btCollisionShape = type opaque
%union.anon = type { i8* }
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }
%class.btVector3 = type { [4 x float] }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btMultibodyLink*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btMultibodyLink = type { float, float, %class.btVector3, i32, %class.btQuaternion, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i8, %class.btQuaternion, %class.btVector3, %class.btVector3, %class.btVector3, float, %class.btMultiBodyLinkCollider*, i32 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %class.btMultiBodyLinkCollider**, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, %class.btMatrix3x3*, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btAlignedObjectArray.16 = type <{ %class.btAlignedAllocator.17, [3 x i8], i32, i32, %struct.btMultiBodySolverConstraint*, i8, [3 x i8] }>
%class.btAlignedAllocator.17 = type { i8 }
%struct.btMultiBodySolverConstraint = type { i32, %class.btVector3, %class.btVector3, i32, i32, %class.btVector3, %class.btVector3, i32, %class.btVector3, %class.btVector3, float, float, float, float, float, float, float, float, float, %union.anon.19, i32, i32, i32, %class.btMultiBody*, i32, i32, %class.btMultiBody*, i32 }
%union.anon.19 = type { i8* }
%struct.btMultiBodyJacobianData = type { %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.21*, i32 }
%class.btAlignedObjectArray.21 = type opaque
%struct.btContactSolverInfo = type { %struct.btContactSolverInfoData }
%struct.btContactSolverInfoData = type { float, float, float, float, float, i32, float, float, float, float, float, i32, float, float, float, float, i32, i32, i32, float, float }

$_ZN21btMultiBodyConstraint9jacobianAEi = comdat any

$_ZN11btMultiBody15getBaseColliderEv = comdat any

$_ZNK17btCollisionObject12getIslandTagEv = comdat any

$_ZNK11btMultiBody11getNumLinksEv = comdat any

$_ZN11btMultiBody7getLinkEi = comdat any

$_ZNK21btMultiBodyConstraint10getNumRowsEv = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE21expandNonInitializingEv = comdat any

$_ZN21btMultiBodyConstraint9jacobianBEi = comdat any

$_ZN20btAlignedObjectArrayIfEixEi = comdat any

$_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi = comdat any

$_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE8allocateEiPPKS0_ = comdat any

$_ZN27btMultiBodySolverConstraintnwEmPv = comdat any

$_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE10deallocateEPS0_ = comdat any

@_ZTV21btMultiBodyJointMotor = hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI21btMultiBodyJointMotor to i8*), i8* bitcast (%class.btMultiBodyJointMotor* (%class.btMultiBodyJointMotor*)* @_ZN21btMultiBodyJointMotorD1Ev to i8*), i8* bitcast (void (%class.btMultiBodyJointMotor*)* @_ZN21btMultiBodyJointMotorD0Ev to i8*), i8* bitcast (i32 (%class.btMultiBodyJointMotor*)* @_ZNK21btMultiBodyJointMotor12getIslandIdAEv to i8*), i8* bitcast (i32 (%class.btMultiBodyJointMotor*)* @_ZNK21btMultiBodyJointMotor12getIslandIdBEv to i8*), i8* bitcast (void (%class.btMultiBodyJointMotor*, %class.btAlignedObjectArray.16*, %struct.btMultiBodyJacobianData*, %struct.btContactSolverInfo*)* @_ZN21btMultiBodyJointMotor20createConstraintRowsER20btAlignedObjectArrayI27btMultiBodySolverConstraintER23btMultiBodyJacobianDataRK19btContactSolverInfo to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS21btMultiBodyJointMotor = hidden constant [24 x i8] c"21btMultiBodyJointMotor\00", align 1
@_ZTI21btMultiBodyConstraint = external constant i8*
@_ZTI21btMultiBodyJointMotor = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([24 x i8], [24 x i8]* @_ZTS21btMultiBodyJointMotor, i32 0, i32 0), i8* bitcast (i8** @_ZTI21btMultiBodyConstraint to i8*) }, align 4

@_ZN21btMultiBodyJointMotorC1EP11btMultiBodyiff = hidden unnamed_addr alias %class.btMultiBodyJointMotor* (%class.btMultiBodyJointMotor*, %class.btMultiBody*, i32, float, float), %class.btMultiBodyJointMotor* (%class.btMultiBodyJointMotor*, %class.btMultiBody*, i32, float, float)* @_ZN21btMultiBodyJointMotorC2EP11btMultiBodyiff
@_ZN21btMultiBodyJointMotorD1Ev = hidden unnamed_addr alias %class.btMultiBodyJointMotor* (%class.btMultiBodyJointMotor*), %class.btMultiBodyJointMotor* (%class.btMultiBodyJointMotor*)* @_ZN21btMultiBodyJointMotorD2Ev

define hidden %class.btMultiBodyJointMotor* @_ZN21btMultiBodyJointMotorC2EP11btMultiBodyiff(%class.btMultiBodyJointMotor* returned %this, %class.btMultiBody* %body, i32 %link, float %desiredVelocity, float %maxMotorImpulse) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btMultiBodyJointMotor*, align 4
  %body.addr = alloca %class.btMultiBody*, align 4
  %link.addr = alloca i32, align 4
  %desiredVelocity.addr = alloca float, align 4
  %maxMotorImpulse.addr = alloca float, align 4
  store %class.btMultiBodyJointMotor* %this, %class.btMultiBodyJointMotor** %this.addr, align 4, !tbaa !2
  store %class.btMultiBody* %body, %class.btMultiBody** %body.addr, align 4, !tbaa !2
  store i32 %link, i32* %link.addr, align 4, !tbaa !6
  store float %desiredVelocity, float* %desiredVelocity.addr, align 4, !tbaa !8
  store float %maxMotorImpulse, float* %maxMotorImpulse.addr, align 4, !tbaa !8
  %this1 = load %class.btMultiBodyJointMotor*, %class.btMultiBodyJointMotor** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyJointMotor* %this1 to %class.btMultiBodyConstraint*
  %1 = load %class.btMultiBody*, %class.btMultiBody** %body.addr, align 4, !tbaa !2
  %2 = load %class.btMultiBody*, %class.btMultiBody** %body.addr, align 4, !tbaa !2
  %3 = load i32, i32* %link.addr, align 4, !tbaa !6
  %4 = load i32, i32* %link.addr, align 4, !tbaa !6
  %call = call %class.btMultiBodyConstraint* @_ZN21btMultiBodyConstraintC2EP11btMultiBodyS1_iiib(%class.btMultiBodyConstraint* %0, %class.btMultiBody* %1, %class.btMultiBody* %2, i32 %3, i32 %4, i32 1, i1 zeroext true)
  %5 = bitcast %class.btMultiBodyJointMotor* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV21btMultiBodyJointMotor, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %5, align 4, !tbaa !10
  %m_desiredVelocity = getelementptr inbounds %class.btMultiBodyJointMotor, %class.btMultiBodyJointMotor* %this1, i32 0, i32 1
  %6 = load float, float* %desiredVelocity.addr, align 4, !tbaa !8
  store float %6, float* %m_desiredVelocity, align 4, !tbaa !12
  %7 = load float, float* %maxMotorImpulse.addr, align 4, !tbaa !8
  %8 = bitcast %class.btMultiBodyJointMotor* %this1 to %class.btMultiBodyConstraint*
  %m_maxAppliedImpulse = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %8, i32 0, i32 10
  store float %7, float* %m_maxAppliedImpulse, align 4, !tbaa !14
  %9 = bitcast %class.btMultiBodyJointMotor* %this1 to %class.btMultiBodyConstraint*
  %call2 = call float* @_ZN21btMultiBodyConstraint9jacobianAEi(%class.btMultiBodyConstraint* %9, i32 0)
  %10 = load i32, i32* %link.addr, align 4, !tbaa !6
  %add = add nsw i32 6, %10
  %arrayidx = getelementptr inbounds float, float* %call2, i32 %add
  store float 1.000000e+00, float* %arrayidx, align 4, !tbaa !8
  ret %class.btMultiBodyJointMotor* %this1
}

declare %class.btMultiBodyConstraint* @_ZN21btMultiBodyConstraintC2EP11btMultiBodyS1_iiib(%class.btMultiBodyConstraint* returned, %class.btMultiBody*, %class.btMultiBody*, i32, i32, i32, i1 zeroext) unnamed_addr #1

define linkonce_odr hidden float* @_ZN21btMultiBodyConstraint9jacobianAEi(%class.btMultiBodyConstraint* %this, i32 %row) #0 comdat {
entry:
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  %row.addr = alloca i32, align 4
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4, !tbaa !2
  store i32 %row, i32* %row.addr, align 4, !tbaa !6
  %this1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 11
  %m_num_rows = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 5
  %0 = load i32, i32* %m_num_rows, align 4, !tbaa !19
  %1 = load i32, i32* %row.addr, align 4, !tbaa !6
  %m_jac_size_both = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 7
  %2 = load i32, i32* %m_jac_size_both, align 4, !tbaa !20
  %mul = mul nsw i32 %1, %2
  %add = add nsw i32 %0, %mul
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_data, i32 %add)
  ret float* %call
}

; Function Attrs: nounwind
declare %class.btMultiBodyConstraint* @_ZN21btMultiBodyConstraintD2Ev(%class.btMultiBodyConstraint* returned) unnamed_addr #2

; Function Attrs: nounwind
define hidden %class.btMultiBodyJointMotor* @_ZN21btMultiBodyJointMotorD2Ev(%class.btMultiBodyJointMotor* returned %this) unnamed_addr #3 {
entry:
  %this.addr = alloca %class.btMultiBodyJointMotor*, align 4
  store %class.btMultiBodyJointMotor* %this, %class.btMultiBodyJointMotor** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyJointMotor*, %class.btMultiBodyJointMotor** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyJointMotor* %this1 to %class.btMultiBodyConstraint*
  %call = call %class.btMultiBodyConstraint* @_ZN21btMultiBodyConstraintD2Ev(%class.btMultiBodyConstraint* %0) #8
  ret %class.btMultiBodyJointMotor* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN21btMultiBodyJointMotorD0Ev(%class.btMultiBodyJointMotor* %this) unnamed_addr #3 {
entry:
  %this.addr = alloca %class.btMultiBodyJointMotor*, align 4
  store %class.btMultiBodyJointMotor* %this, %class.btMultiBodyJointMotor** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyJointMotor*, %class.btMultiBodyJointMotor** %this.addr, align 4
  %call = call %class.btMultiBodyJointMotor* @_ZN21btMultiBodyJointMotorD1Ev(%class.btMultiBodyJointMotor* %this1) #8
  %0 = bitcast %class.btMultiBodyJointMotor* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #4

define hidden i32 @_ZNK21btMultiBodyJointMotor12getIslandIdAEv(%class.btMultiBodyJointMotor* %this) unnamed_addr #0 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btMultiBodyJointMotor*, align 4
  %col = alloca %class.btMultiBodyLinkCollider*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btMultiBodyJointMotor* %this, %class.btMultiBodyJointMotor** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyJointMotor*, %class.btMultiBodyJointMotor** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyLinkCollider** %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = bitcast %class.btMultiBodyJointMotor* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %1, i32 0, i32 1
  %2 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA, align 4, !tbaa !21
  %call = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %2)
  store %class.btMultiBodyLinkCollider* %call, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %3 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %tobool = icmp ne %class.btMultiBodyLinkCollider* %3, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %5 = bitcast %class.btMultiBodyLinkCollider* %4 to %class.btCollisionObject*
  %call2 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %5)
  store i32 %call2, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup14

if.end:                                           ; preds = %entry
  %6 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %8 = bitcast %class.btMultiBodyJointMotor* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA3 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %8, i32 0, i32 1
  %9 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA3, align 4, !tbaa !21
  %call4 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %9)
  %cmp = icmp slt i32 %7, %call4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body:                                         ; preds = %for.cond
  %10 = bitcast %class.btMultiBodyJointMotor* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA5 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %10, i32 0, i32 1
  %11 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA5, align 4, !tbaa !21
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %call6 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %11, i32 %12)
  %m_collider = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call6, i32 0, i32 15
  %13 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider, align 4, !tbaa !22
  %tobool7 = icmp ne %class.btMultiBodyLinkCollider* %13, null
  br i1 %tobool7, label %if.then8, label %if.end13

if.then8:                                         ; preds = %for.body
  %14 = bitcast %class.btMultiBodyJointMotor* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA9 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %14, i32 0, i32 1
  %15 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA9, align 4, !tbaa !21
  %16 = load i32, i32* %i, align 4, !tbaa !6
  %call10 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %15, i32 %16)
  %m_collider11 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call10, i32 0, i32 15
  %17 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider11, align 4, !tbaa !22
  %18 = bitcast %class.btMultiBodyLinkCollider* %17 to %class.btCollisionObject*
  %call12 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %18)
  store i32 %call12, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end13:                                         ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end13
  %19 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

cleanup:                                          ; preds = %if.then8, %for.cond.cleanup
  %20 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup14 [
    i32 2, label %for.end
  ]

for.end:                                          ; preds = %cleanup
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup14

cleanup14:                                        ; preds = %for.end, %cleanup, %if.then
  %21 = bitcast %class.btMultiBodyLinkCollider** %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  %22 = load i32, i32* %retval, align 4
  ret i32 %22
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #5

; Function Attrs: nounwind
define linkonce_odr hidden %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_baseCollider = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 0
  %0 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_baseCollider, align 4, !tbaa !26
  ret %class.btMultiBodyLinkCollider* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_islandTag1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 13
  %0 = load i32, i32* %m_islandTag1, align 4, !tbaa !37
  ret i32 %0
}

define linkonce_odr hidden i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %call = call i32 @_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv(%class.btAlignedObjectArray* %links)
  ret i32 %call
}

define linkonce_odr hidden nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %this, i32 %index) #0 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %index.addr = alloca i32, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !6
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %0 = load i32, i32* %index.addr, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links, i32 %0)
  ret %struct.btMultibodyLink* %call
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #5

define hidden i32 @_ZNK21btMultiBodyJointMotor12getIslandIdBEv(%class.btMultiBodyJointMotor* %this) unnamed_addr #0 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btMultiBodyJointMotor*, align 4
  %col = alloca %class.btMultiBodyLinkCollider*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btMultiBodyJointMotor* %this, %class.btMultiBodyJointMotor** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyJointMotor*, %class.btMultiBodyJointMotor** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyLinkCollider** %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = bitcast %class.btMultiBodyJointMotor* %this1 to %class.btMultiBodyConstraint*
  %m_bodyB = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %1, i32 0, i32 2
  %2 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB, align 4, !tbaa !40
  %call = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %2)
  store %class.btMultiBodyLinkCollider* %call, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %3 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %tobool = icmp ne %class.btMultiBodyLinkCollider* %3, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %5 = bitcast %class.btMultiBodyLinkCollider* %4 to %class.btCollisionObject*
  %call2 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %5)
  store i32 %call2, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup11

if.end:                                           ; preds = %entry
  %6 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %8 = bitcast %class.btMultiBodyJointMotor* %this1 to %class.btMultiBodyConstraint*
  %m_bodyB3 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %8, i32 0, i32 2
  %9 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB3, align 4, !tbaa !40
  %call4 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %9)
  %cmp = icmp slt i32 %7, %call4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body:                                         ; preds = %for.cond
  %10 = bitcast %class.btMultiBodyJointMotor* %this1 to %class.btMultiBodyConstraint*
  %m_bodyB5 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %10, i32 0, i32 2
  %11 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB5, align 4, !tbaa !40
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %call6 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %11, i32 %12)
  %m_collider = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call6, i32 0, i32 15
  %13 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider, align 4, !tbaa !22
  store %class.btMultiBodyLinkCollider* %13, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %14 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %tobool7 = icmp ne %class.btMultiBodyLinkCollider* %14, null
  br i1 %tobool7, label %if.then8, label %if.end10

if.then8:                                         ; preds = %for.body
  %15 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %16 = bitcast %class.btMultiBodyLinkCollider* %15 to %class.btCollisionObject*
  %call9 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %16)
  store i32 %call9, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end10:                                         ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end10
  %17 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

cleanup:                                          ; preds = %if.then8, %for.cond.cleanup
  %18 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup11 [
    i32 2, label %for.end
  ]

for.end:                                          ; preds = %cleanup
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup11

cleanup11:                                        ; preds = %for.end, %cleanup, %if.then
  %19 = bitcast %class.btMultiBodyLinkCollider** %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  %20 = load i32, i32* %retval, align 4
  ret i32 %20
}

define hidden void @_ZN21btMultiBodyJointMotor20createConstraintRowsER20btAlignedObjectArrayI27btMultiBodySolverConstraintER23btMultiBodyJacobianDataRK19btContactSolverInfo(%class.btMultiBodyJointMotor* %this, %class.btAlignedObjectArray.16* nonnull align 4 dereferenceable(17) %constraintRows, %struct.btMultiBodyJacobianData* nonnull align 4 dereferenceable(128) %data, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %infoGlobal) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btMultiBodyJointMotor*, align 4
  %constraintRows.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %data.addr = alloca %struct.btMultiBodyJacobianData*, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %row = alloca i32, align 4
  %constraintRow = alloca %struct.btMultiBodySolverConstraint*, align 4
  %penetration = alloca float, align 4
  store %class.btMultiBodyJointMotor* %this, %class.btMultiBodyJointMotor** %this.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.16* %constraintRows, %class.btAlignedObjectArray.16** %constraintRows.addr, align 4, !tbaa !2
  store %struct.btMultiBodyJacobianData* %data, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyJointMotor*, %class.btMultiBodyJointMotor** %this.addr, align 4
  %0 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %row, align 4, !tbaa !6
  %2 = bitcast %class.btMultiBodyJointMotor* %this1 to %class.btMultiBodyConstraint*
  %call = call i32 @_ZNK21btMultiBodyConstraint10getNumRowsEv(%class.btMultiBodyConstraint* %2)
  %cmp = icmp slt i32 %1, %call
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = bitcast %struct.btMultiBodySolverConstraint** %constraintRow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %constraintRows.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(184) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE21expandNonInitializingEv(%class.btAlignedObjectArray.16* %5)
  store %struct.btMultiBodySolverConstraint* %call2, %struct.btMultiBodySolverConstraint** %constraintRow, align 4, !tbaa !2
  %6 = bitcast float* %penetration to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  store float 0.000000e+00, float* %penetration, align 4, !tbaa !8
  %7 = bitcast %class.btMultiBodyJointMotor* %this1 to %class.btMultiBodyConstraint*
  %8 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4, !tbaa !2
  %9 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %10 = bitcast %class.btMultiBodyJointMotor* %this1 to %class.btMultiBodyConstraint*
  %11 = load i32, i32* %row, align 4, !tbaa !6
  %call3 = call float* @_ZN21btMultiBodyConstraint9jacobianAEi(%class.btMultiBodyConstraint* %10, i32 %11)
  %12 = bitcast %class.btMultiBodyJointMotor* %this1 to %class.btMultiBodyConstraint*
  %13 = load i32, i32* %row, align 4, !tbaa !6
  %call4 = call float* @_ZN21btMultiBodyConstraint9jacobianBEi(%class.btMultiBodyConstraint* %12, i32 %13)
  %14 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %m_desiredVelocity = getelementptr inbounds %class.btMultiBodyJointMotor, %class.btMultiBodyJointMotor* %this1, i32 0, i32 1
  %15 = load float, float* %m_desiredVelocity, align 4, !tbaa !12
  %16 = bitcast %class.btMultiBodyJointMotor* %this1 to %class.btMultiBodyConstraint*
  %m_maxAppliedImpulse = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %16, i32 0, i32 10
  %17 = load float, float* %m_maxAppliedImpulse, align 4, !tbaa !14
  %fneg = fneg float %17
  %18 = bitcast %class.btMultiBodyJointMotor* %this1 to %class.btMultiBodyConstraint*
  %m_maxAppliedImpulse5 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %18, i32 0, i32 10
  %19 = load float, float* %m_maxAppliedImpulse5, align 4, !tbaa !14
  %call6 = call float @_ZN21btMultiBodyConstraint35fillConstraintRowMultiBodyMultiBodyER27btMultiBodySolverConstraintR23btMultiBodyJacobianDataPfS4_RK19btContactSolverInfofff(%class.btMultiBodyConstraint* %7, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(184) %8, %struct.btMultiBodyJacobianData* nonnull align 4 dereferenceable(128) %9, float* %call3, float* %call4, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %14, float %15, float %fneg, float %19)
  %20 = bitcast float* %penetration to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #8
  %21 = bitcast %struct.btMultiBodySolverConstraint** %constraintRow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %22 = load i32, i32* %row, align 4, !tbaa !6
  %inc = add nsw i32 %22, 1
  store i32 %inc, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK21btMultiBodyConstraint10getNumRowsEv(%class.btMultiBodyConstraint* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  %m_num_rows = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 5
  %0 = load i32, i32* %m_num_rows, align 4, !tbaa !19
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(184) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE21expandNonInitializingEv(%class.btAlignedObjectArray.16* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !6
  %1 = load i32, i32* %sz, align 4, !tbaa !6
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE8capacityEv(%class.btAlignedObjectArray.16* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE9allocSizeEi(%class.btAlignedObjectArray.16* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7reserveEi(%class.btAlignedObjectArray.16* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4, !tbaa !41
  %inc = add nsw i32 %2, 1
  store i32 %inc, i32* %m_size, align 4, !tbaa !41
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %3 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4, !tbaa !44
  %4 = load i32, i32* %sz, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %3, i32 %4
  %5 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  ret %struct.btMultiBodySolverConstraint* %arrayidx
}

declare float @_ZN21btMultiBodyConstraint35fillConstraintRowMultiBodyMultiBodyER27btMultiBodySolverConstraintR23btMultiBodyJacobianDataPfS4_RK19btContactSolverInfofff(%class.btMultiBodyConstraint*, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(184), %struct.btMultiBodyJacobianData* nonnull align 4 dereferenceable(128), float*, float*, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84), float, float, float) #1

; Function Attrs: nounwind
define linkonce_odr hidden float* @_ZN21btMultiBodyConstraint9jacobianBEi(%class.btMultiBodyConstraint* %this, i32 %row) #3 comdat {
entry:
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  %row.addr = alloca i32, align 4
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4, !tbaa !2
  store i32 %row, i32* %row.addr, align 4, !tbaa !6
  %this1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 11
  %m_num_rows = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 5
  %0 = load i32, i32* %m_num_rows, align 4, !tbaa !19
  %1 = load i32, i32* %row.addr, align 4, !tbaa !6
  %m_jac_size_both = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 7
  %2 = load i32, i32* %m_jac_size_both, align 4, !tbaa !20
  %mul = mul nsw i32 %1, %2
  %add = add nsw i32 %0, %mul
  %m_jac_size_A = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 6
  %3 = load i32, i32* %m_jac_size_A, align 4, !tbaa !45
  %add2 = add nsw i32 %add, %3
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_data, i32 %add2)
  ret float* %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %this, i32 %n) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4, !tbaa !46
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv(%class.btAlignedObjectArray* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !47
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %this, i32 %n) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btMultibodyLink*, %struct.btMultibodyLink** %m_data, align 4, !tbaa !48
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %0, i32 %1
  ret %struct.btMultibodyLink* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.16* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !41
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE8capacityEv(%class.btAlignedObjectArray.16* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !49
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7reserveEi(%class.btAlignedObjectArray.16* %this, i32 %_Count) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btMultiBodySolverConstraint*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE8capacityEv(%class.btAlignedObjectArray.16* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btMultiBodySolverConstraint** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE8allocateEi(%class.btAlignedObjectArray.16* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.btMultiBodySolverConstraint*
  store %struct.btMultiBodySolverConstraint* %3, %struct.btMultiBodySolverConstraint** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  %4 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4copyEiiPS0_(%class.btAlignedObjectArray.16* %this1, i32 0, i32 %call3, %struct.btMultiBodySolverConstraint* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7destroyEii(%class.btAlignedObjectArray.16* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE10deallocateEv(%class.btAlignedObjectArray.16* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !50
  %5 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  store %struct.btMultiBodySolverConstraint* %5, %struct.btMultiBodySolverConstraint** %m_data, align 4, !tbaa !44
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !49
  %7 = bitcast %struct.btMultiBodySolverConstraint** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE9allocSizeEi(%class.btAlignedObjectArray.16* %this, i32 %size) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE8allocateEi(%class.btAlignedObjectArray.16* %this, i32 %size) #7 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call %struct.btMultiBodySolverConstraint* @_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.17* %m_allocator, i32 %1, %struct.btMultiBodySolverConstraint** null)
  %2 = bitcast %struct.btMultiBodySolverConstraint* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4copyEiiPS0_(%class.btAlignedObjectArray.16* %this, i32 %start, i32 %end, %struct.btMultiBodySolverConstraint* %dest) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btMultiBodySolverConstraint*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store %struct.btMultiBodySolverConstraint* %dest, %struct.btMultiBodySolverConstraint** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %4, i32 %5
  %6 = bitcast %struct.btMultiBodySolverConstraint* %arrayidx to i8*
  %call = call i8* @_ZN27btMultiBodySolverConstraintnwEmPv(i32 184, i8* %6)
  %7 = bitcast i8* %call to %struct.btMultiBodySolverConstraint*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %8 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4, !tbaa !44
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %8, i32 %9
  %10 = bitcast %struct.btMultiBodySolverConstraint* %7 to i8*
  %11 = bitcast %struct.btMultiBodySolverConstraint* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 184, i1 false), !tbaa.struct !51
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7destroyEii(%class.btAlignedObjectArray.16* %this, i32 %first, i32 %last) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %4 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4, !tbaa !44
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE10deallocateEv(%class.btAlignedObjectArray.16* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %0 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4, !tbaa !44
  %tobool = icmp ne %struct.btMultiBodySolverConstraint* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !50, !range !53
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %2 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data4, align 4, !tbaa !44
  call void @_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE10deallocateEPS0_(%class.btAlignedAllocator.17* %m_allocator, %struct.btMultiBodySolverConstraint* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  store %struct.btMultiBodySolverConstraint* null, %struct.btMultiBodySolverConstraint** %m_data5, align 4, !tbaa !44
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden %struct.btMultiBodySolverConstraint* @_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.17* %this, i32 %n, %struct.btMultiBodySolverConstraint** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.17*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btMultiBodySolverConstraint**, align 4
  store %class.btAlignedAllocator.17* %this, %class.btAlignedAllocator.17** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store %struct.btMultiBodySolverConstraint** %hint, %struct.btMultiBodySolverConstraint*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.17*, %class.btAlignedAllocator.17** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 184, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btMultiBodySolverConstraint*
  ret %struct.btMultiBodySolverConstraint* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN27btMultiBodySolverConstraintnwEmPv(i32 %0, i8* %ptr) #6 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !54
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #5

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE10deallocateEPS0_(%class.btAlignedAllocator.17* %this, %struct.btMultiBodySolverConstraint* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.17*, align 4
  %ptr.addr = alloca %struct.btMultiBodySolverConstraint*, align 4
  store %class.btAlignedAllocator.17* %this, %class.btAlignedAllocator.17** %this.addr, align 4, !tbaa !2
  store %struct.btMultiBodySolverConstraint* %ptr, %struct.btMultiBodySolverConstraint** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.17*, %class.btAlignedAllocator.17** %this.addr, align 4
  %0 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btMultiBodySolverConstraint* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { argmemonly nounwind willreturn }
attributes #6 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind }
attributes #9 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"float", !4, i64 0}
!10 = !{!11, !11, i64 0}
!11 = !{!"vtable pointer", !5, i64 0}
!12 = !{!13, !9, i64 64}
!13 = !{!"_ZTS21btMultiBodyJointMotor", !9, i64 64}
!14 = !{!15, !9, i64 40}
!15 = !{!"_ZTS21btMultiBodyConstraint", !3, i64 4, !3, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !16, i64 36, !9, i64 40, !17, i64 44}
!16 = !{!"bool", !4, i64 0}
!17 = !{!"_ZTS20btAlignedObjectArrayIfE", !18, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !16, i64 16}
!18 = !{!"_ZTS18btAlignedAllocatorIfLj16EE"}
!19 = !{!15, !7, i64 20}
!20 = !{!15, !7, i64 28}
!21 = !{!15, !3, i64 4}
!22 = !{!23, !3, i64 180}
!23 = !{!"_ZTS15btMultibodyLink", !9, i64 0, !9, i64 4, !24, i64 8, !7, i64 24, !25, i64 28, !24, i64 44, !24, i64 60, !24, i64 76, !24, i64 92, !16, i64 108, !25, i64 112, !24, i64 128, !24, i64 144, !24, i64 160, !9, i64 176, !3, i64 180, !7, i64 184}
!24 = !{!"_ZTS9btVector3", !4, i64 0}
!25 = !{!"_ZTS12btQuaternion"}
!26 = !{!27, !3, i64 0}
!27 = !{!"_ZTS11btMultiBody", !3, i64 0, !24, i64 4, !25, i64 20, !9, i64 36, !24, i64 40, !24, i64 56, !24, i64 72, !28, i64 88, !30, i64 108, !17, i64 128, !32, i64 148, !34, i64 168, !36, i64 188, !36, i64 236, !36, i64 284, !36, i64 332, !16, i64 380, !16, i64 381, !16, i64 382, !9, i64 384, !7, i64 388, !9, i64 392, !9, i64 396, !16, i64 400, !9, i64 404, !16, i64 408}
!28 = !{!"_ZTS20btAlignedObjectArrayI15btMultibodyLinkE", !29, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !16, i64 16}
!29 = !{!"_ZTS18btAlignedAllocatorI15btMultibodyLinkLj16EE"}
!30 = !{!"_ZTS20btAlignedObjectArrayIP23btMultiBodyLinkColliderE", !31, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !16, i64 16}
!31 = !{!"_ZTS18btAlignedAllocatorIP23btMultiBodyLinkColliderLj16EE"}
!32 = !{!"_ZTS20btAlignedObjectArrayI9btVector3E", !33, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !16, i64 16}
!33 = !{!"_ZTS18btAlignedAllocatorI9btVector3Lj16EE"}
!34 = !{!"_ZTS20btAlignedObjectArrayI11btMatrix3x3E", !35, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !16, i64 16}
!35 = !{!"_ZTS18btAlignedAllocatorI11btMatrix3x3Lj16EE"}
!36 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!37 = !{!38, !7, i64 208}
!38 = !{!"_ZTS17btCollisionObject", !39, i64 4, !39, i64 68, !24, i64 132, !24, i64 148, !24, i64 164, !7, i64 180, !9, i64 184, !3, i64 188, !3, i64 192, !3, i64 196, !3, i64 200, !7, i64 204, !7, i64 208, !7, i64 212, !7, i64 216, !9, i64 220, !9, i64 224, !9, i64 228, !9, i64 232, !7, i64 236, !4, i64 240, !9, i64 244, !9, i64 248, !9, i64 252, !7, i64 256, !7, i64 260}
!39 = !{!"_ZTS11btTransform", !36, i64 0, !24, i64 48}
!40 = !{!15, !3, i64 8}
!41 = !{!42, !7, i64 4}
!42 = !{!"_ZTS20btAlignedObjectArrayI27btMultiBodySolverConstraintE", !43, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !16, i64 16}
!43 = !{!"_ZTS18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE"}
!44 = !{!42, !3, i64 12}
!45 = !{!15, !7, i64 24}
!46 = !{!17, !3, i64 12}
!47 = !{!28, !7, i64 4}
!48 = !{!28, !3, i64 12}
!49 = !{!42, !7, i64 8}
!50 = !{!42, !16, i64 16}
!51 = !{i64 0, i64 4, !6, i64 4, i64 16, !52, i64 20, i64 16, !52, i64 36, i64 4, !6, i64 40, i64 4, !6, i64 44, i64 16, !52, i64 60, i64 16, !52, i64 76, i64 4, !6, i64 80, i64 16, !52, i64 96, i64 16, !52, i64 112, i64 4, !8, i64 116, i64 4, !8, i64 120, i64 4, !8, i64 124, i64 4, !8, i64 128, i64 4, !8, i64 132, i64 4, !8, i64 136, i64 4, !8, i64 140, i64 4, !8, i64 144, i64 4, !8, i64 148, i64 4, !2, i64 148, i64 4, !8, i64 152, i64 4, !6, i64 156, i64 4, !6, i64 160, i64 4, !6, i64 164, i64 4, !2, i64 168, i64 4, !6, i64 172, i64 4, !6, i64 176, i64 4, !2, i64 180, i64 4, !6}
!52 = !{!4, !4, i64 0}
!53 = !{i8 0, i8 2}
!54 = !{!55, !55, i64 0}
!55 = !{!"long", !4, i64 0}
