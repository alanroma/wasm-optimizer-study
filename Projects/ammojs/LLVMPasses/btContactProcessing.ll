; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/Gimpact/btContactProcessing.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/Gimpact/btContactProcessing.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btContactArray = type { %class.btAlignedObjectArray.base, [3 x i8] }
%class.btAlignedObjectArray.base = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.GIM_CONTACT*, i8 }>
%class.btAlignedAllocator = type { i8 }
%class.GIM_CONTACT = type { %class.btVector3, %class.btVector3, float, float, i32, i32 }
%class.btVector3 = type { [4 x float] }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.CONTACT_KEY_TOKEN*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.CONTACT_KEY_TOKEN = type { i32, i32 }
%class.CONTACT_KEY_TOKEN_COMP = type { i8 }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.GIM_CONTACT*, i8, [3 x i8] }>

$_ZN20btAlignedObjectArrayI11GIM_CONTACTE5clearEv = comdat any

$_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI11GIM_CONTACTE9push_backERKS0_ = comdat any

$_ZNK20btAlignedObjectArrayI11GIM_CONTACTEixEi = comdat any

$_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE9push_backERKS0_ = comdat any

$_ZNK11GIM_CONTACT16calc_key_contactEv = comdat any

$_ZN17CONTACT_KEY_TOKENC2Eji = comdat any

$_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE9quickSortI22CONTACT_KEY_TOKEN_COMPEEvRKT_ = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENEixEi = comdat any

$_ZN20btAlignedObjectArrayI11GIM_CONTACTEixEi = comdat any

$_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4sizeEv = comdat any

$_Z6btFabsf = comdat any

$_ZN11GIM_CONTACT19interpolate_normalsEP9btVector3i = comdat any

$_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENED2Ev = comdat any

$_ZN11GIM_CONTACTC2ERKS_ = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZdvRK9btVector3RKf = comdat any

$_Z6btSqrtf = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZNK20btAlignedObjectArrayI11GIM_CONTACTE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI11GIM_CONTACTE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI11GIM_CONTACTE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayI11GIM_CONTACTE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI11GIM_CONTACTE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI11GIM_CONTACTE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI11GIM_CONTACTLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI11GIM_CONTACTLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI11GIM_CONTACTE4initEv = comdat any

$_ZN18btAlignedAllocatorI17CONTACT_KEY_TOKENLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4initEv = comdat any

$_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI17CONTACT_KEY_TOKENLj16EE10deallocateEPS0_ = comdat any

$_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI17CONTACT_KEY_TOKENLj16EE8allocateEiPPKS0_ = comdat any

$_ZN17CONTACT_KEY_TOKENC2ERKS_ = comdat any

$_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE17quickSortInternalI22CONTACT_KEY_TOKEN_COMPEEvRKT_ii = comdat any

$_ZNK22CONTACT_KEY_TOKEN_COMPclERK17CONTACT_KEY_TOKENS2_ = comdat any

$_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4swapEii = comdat any

$_ZNK17CONTACT_KEY_TOKENltERKS_ = comdat any

define hidden void @_ZN14btContactArray14merge_contactsERKS_b(%class.btContactArray* %this, %class.btContactArray* nonnull align 4 dereferenceable(17) %contacts, i1 zeroext %normal_contact_average) #0 {
entry:
  %this.addr = alloca %class.btContactArray*, align 4
  %contacts.addr = alloca %class.btContactArray*, align 4
  %normal_contact_average.addr = alloca i8, align 1
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %keycontacts = alloca %class.btAlignedObjectArray.0, align 4
  %ref.tmp = alloca %struct.CONTACT_KEY_TOKEN, align 4
  %ref.tmp14 = alloca %class.CONTACT_KEY_TOKEN_COMP, align 1
  %coincident_count = alloca i32, align 4
  %coincident_normals = alloca [8 x %class.btVector3], align 16
  %last_key = alloca i32, align 4
  %key = alloca i32, align 4
  %pcontact = alloca %class.GIM_CONTACT*, align 4
  %scontact = alloca %class.GIM_CONTACT*, align 4
  store %class.btContactArray* %this, %class.btContactArray** %this.addr, align 4, !tbaa !2
  store %class.btContactArray* %contacts, %class.btContactArray** %contacts.addr, align 4, !tbaa !2
  %frombool = zext i1 %normal_contact_average to i8
  store i8 %frombool, i8* %normal_contact_average.addr, align 1, !tbaa !6
  %this1 = load %class.btContactArray*, %class.btContactArray** %this.addr, align 4
  %0 = bitcast %class.btContactArray* %this1 to %class.btAlignedObjectArray*
  call void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE5clearEv(%class.btAlignedObjectArray* %0)
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4, !tbaa !2
  %3 = bitcast %class.btContactArray* %2 to %class.btAlignedObjectArray*
  %call = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv(%class.btAlignedObjectArray* %3)
  %cmp = icmp eq i32 %call, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %4 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4, !tbaa !2
  %5 = bitcast %class.btContactArray* %4 to %class.btAlignedObjectArray*
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv(%class.btAlignedObjectArray* %5)
  %cmp3 = icmp eq i32 %call2, 1
  br i1 %cmp3, label %if.then4, label %if.end6

if.then4:                                         ; preds = %if.end
  %6 = bitcast %class.btContactArray* %this1 to %class.btAlignedObjectArray*
  %7 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4, !tbaa !2
  %8 = bitcast %class.btContactArray* %7 to %class.btAlignedObjectArray*
  %call5 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK20btAlignedObjectArrayI11GIM_CONTACTEixEi(%class.btAlignedObjectArray* %8, i32 0)
  call void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE9push_backERKS0_(%class.btAlignedObjectArray* %6, %class.GIM_CONTACT* nonnull align 4 dereferenceable(48) %call5)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end6:                                          ; preds = %if.end
  %9 = bitcast %class.btAlignedObjectArray.0* %keycontacts to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %9) #7
  %call7 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENEC2Ev(%class.btAlignedObjectArray.0* %keycontacts)
  %10 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4, !tbaa !2
  %11 = bitcast %class.btContactArray* %10 to %class.btAlignedObjectArray*
  %call8 = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv(%class.btAlignedObjectArray* %11)
  call void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE7reserveEi(%class.btAlignedObjectArray.0* %keycontacts, i32 %call8)
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end6
  %12 = load i32, i32* %i, align 4, !tbaa !8
  %13 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4, !tbaa !2
  %14 = bitcast %class.btContactArray* %13 to %class.btAlignedObjectArray*
  %call9 = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv(%class.btAlignedObjectArray* %14)
  %cmp10 = icmp slt i32 %12, %call9
  br i1 %cmp10, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %15 = bitcast %struct.CONTACT_KEY_TOKEN* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %15) #7
  %16 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4, !tbaa !2
  %17 = bitcast %class.btContactArray* %16 to %class.btAlignedObjectArray*
  %18 = load i32, i32* %i, align 4, !tbaa !8
  %call11 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK20btAlignedObjectArrayI11GIM_CONTACTEixEi(%class.btAlignedObjectArray* %17, i32 %18)
  %call12 = call i32 @_ZNK11GIM_CONTACT16calc_key_contactEv(%class.GIM_CONTACT* %call11)
  %19 = load i32, i32* %i, align 4, !tbaa !8
  %call13 = call %struct.CONTACT_KEY_TOKEN* @_ZN17CONTACT_KEY_TOKENC2Eji(%struct.CONTACT_KEY_TOKEN* %ref.tmp, i32 %call12, i32 %19)
  call void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE9push_backERKS0_(%class.btAlignedObjectArray.0* %keycontacts, %struct.CONTACT_KEY_TOKEN* nonnull align 4 dereferenceable(8) %ref.tmp)
  %20 = bitcast %struct.CONTACT_KEY_TOKEN* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %20) #7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %21 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %22 = bitcast %class.CONTACT_KEY_TOKEN_COMP* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %22) #7
  call void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE9quickSortI22CONTACT_KEY_TOKEN_COMPEEvRKT_(%class.btAlignedObjectArray.0* %keycontacts, %class.CONTACT_KEY_TOKEN_COMP* nonnull align 1 dereferenceable(1) %ref.tmp14)
  %23 = bitcast %class.CONTACT_KEY_TOKEN_COMP* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %23) #7
  %24 = bitcast i32* %coincident_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #7
  store i32 0, i32* %coincident_count, align 4, !tbaa !8
  %25 = bitcast [8 x %class.btVector3]* %coincident_normals to i8*
  call void @llvm.lifetime.start.p0i8(i64 128, i8* %25) #7
  %array.begin = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %coincident_normals, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 8
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %for.end
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %for.end ], [ %arrayctor.next, %arrayctor.loop ]
  %call15 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %26 = bitcast i32* %last_key to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #7
  %call16 = call nonnull align 4 dereferenceable(8) %struct.CONTACT_KEY_TOKEN* @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENEixEi(%class.btAlignedObjectArray.0* %keycontacts, i32 0)
  %m_key = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %call16, i32 0, i32 0
  %27 = load i32, i32* %m_key, align 4, !tbaa !10
  store i32 %27, i32* %last_key, align 4, !tbaa !8
  %28 = bitcast i32* %key to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #7
  store i32 0, i32* %key, align 4, !tbaa !8
  %29 = bitcast %class.btContactArray* %this1 to %class.btAlignedObjectArray*
  %30 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4, !tbaa !2
  %31 = bitcast %class.btContactArray* %30 to %class.btAlignedObjectArray*
  %call17 = call nonnull align 4 dereferenceable(8) %struct.CONTACT_KEY_TOKEN* @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENEixEi(%class.btAlignedObjectArray.0* %keycontacts, i32 0)
  %m_value = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %call17, i32 0, i32 1
  %32 = load i32, i32* %m_value, align 4, !tbaa !12
  %call18 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK20btAlignedObjectArrayI11GIM_CONTACTEixEi(%class.btAlignedObjectArray* %31, i32 %32)
  call void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE9push_backERKS0_(%class.btAlignedObjectArray* %29, %class.GIM_CONTACT* nonnull align 4 dereferenceable(48) %call18)
  %33 = bitcast %class.GIM_CONTACT** %pcontact to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #7
  %34 = bitcast %class.btContactArray* %this1 to %class.btAlignedObjectArray*
  %call19 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZN20btAlignedObjectArrayI11GIM_CONTACTEixEi(%class.btAlignedObjectArray* %34, i32 0)
  store %class.GIM_CONTACT* %call19, %class.GIM_CONTACT** %pcontact, align 4, !tbaa !2
  store i32 1, i32* %i, align 4, !tbaa !8
  br label %for.cond20

for.cond20:                                       ; preds = %for.inc57, %arrayctor.cont
  %35 = load i32, i32* %i, align 4, !tbaa !8
  %call21 = call i32 @_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4sizeEv(%class.btAlignedObjectArray.0* %keycontacts)
  %cmp22 = icmp slt i32 %35, %call21
  br i1 %cmp22, label %for.body23, label %for.end59

for.body23:                                       ; preds = %for.cond20
  %36 = load i32, i32* %i, align 4, !tbaa !8
  %call24 = call nonnull align 4 dereferenceable(8) %struct.CONTACT_KEY_TOKEN* @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENEixEi(%class.btAlignedObjectArray.0* %keycontacts, i32 %36)
  %m_key25 = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %call24, i32 0, i32 0
  %37 = load i32, i32* %m_key25, align 4, !tbaa !10
  store i32 %37, i32* %key, align 4, !tbaa !8
  %38 = bitcast %class.GIM_CONTACT** %scontact to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #7
  %39 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4, !tbaa !2
  %40 = bitcast %class.btContactArray* %39 to %class.btAlignedObjectArray*
  %41 = load i32, i32* %i, align 4, !tbaa !8
  %call26 = call nonnull align 4 dereferenceable(8) %struct.CONTACT_KEY_TOKEN* @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENEixEi(%class.btAlignedObjectArray.0* %keycontacts, i32 %41)
  %m_value27 = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %call26, i32 0, i32 1
  %42 = load i32, i32* %m_value27, align 4, !tbaa !12
  %call28 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK20btAlignedObjectArrayI11GIM_CONTACTEixEi(%class.btAlignedObjectArray* %40, i32 %42)
  store %class.GIM_CONTACT* %call28, %class.GIM_CONTACT** %scontact, align 4, !tbaa !2
  %43 = load i32, i32* %last_key, align 4, !tbaa !8
  %44 = load i32, i32* %key, align 4, !tbaa !8
  %cmp29 = icmp eq i32 %43, %44
  br i1 %cmp29, label %if.then30, label %if.else48

if.then30:                                        ; preds = %for.body23
  %45 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %pcontact, align 4, !tbaa !2
  %m_depth = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %45, i32 0, i32 2
  %46 = load float, float* %m_depth, align 4, !tbaa !13
  %sub = fsub float %46, 0x3EE4F8B580000000
  %47 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %scontact, align 4, !tbaa !2
  %m_depth31 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %47, i32 0, i32 2
  %48 = load float, float* %m_depth31, align 4, !tbaa !13
  %cmp32 = fcmp ogt float %sub, %48
  br i1 %cmp32, label %if.then33, label %if.else

if.then33:                                        ; preds = %if.then30
  %49 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %scontact, align 4, !tbaa !2
  %50 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %pcontact, align 4, !tbaa !2
  %51 = bitcast %class.GIM_CONTACT* %50 to i8*
  %52 = bitcast %class.GIM_CONTACT* %49 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %51, i8* align 4 %52, i32 48, i1 false), !tbaa.struct !17
  store i32 0, i32* %coincident_count, align 4, !tbaa !8
  br label %if.end47

if.else:                                          ; preds = %if.then30
  %53 = load i8, i8* %normal_contact_average.addr, align 1, !tbaa !6, !range !20
  %tobool = trunc i8 %53 to i1
  br i1 %tobool, label %if.then34, label %if.end46

if.then34:                                        ; preds = %if.else
  %54 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %pcontact, align 4, !tbaa !2
  %m_depth35 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %54, i32 0, i32 2
  %55 = load float, float* %m_depth35, align 4, !tbaa !13
  %56 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %scontact, align 4, !tbaa !2
  %m_depth36 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %56, i32 0, i32 2
  %57 = load float, float* %m_depth36, align 4, !tbaa !13
  %sub37 = fsub float %55, %57
  %call38 = call float @_Z6btFabsf(float %sub37)
  %cmp39 = fcmp olt float %call38, 0x3EE4F8B580000000
  br i1 %cmp39, label %if.then40, label %if.end45

if.then40:                                        ; preds = %if.then34
  %58 = load i32, i32* %coincident_count, align 4, !tbaa !8
  %cmp41 = icmp slt i32 %58, 8
  br i1 %cmp41, label %if.then42, label %if.end44

if.then42:                                        ; preds = %if.then40
  %59 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %scontact, align 4, !tbaa !2
  %m_normal = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %59, i32 0, i32 1
  %60 = load i32, i32* %coincident_count, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %coincident_normals, i32 0, i32 %60
  %61 = bitcast %class.btVector3* %arrayidx to i8*
  %62 = bitcast %class.btVector3* %m_normal to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %61, i8* align 4 %62, i32 16, i1 false), !tbaa.struct !21
  %63 = load i32, i32* %coincident_count, align 4, !tbaa !8
  %inc43 = add nsw i32 %63, 1
  store i32 %inc43, i32* %coincident_count, align 4, !tbaa !8
  br label %if.end44

if.end44:                                         ; preds = %if.then42, %if.then40
  br label %if.end45

if.end45:                                         ; preds = %if.end44, %if.then34
  br label %if.end46

if.end46:                                         ; preds = %if.end45, %if.else
  br label %if.end47

if.end47:                                         ; preds = %if.end46, %if.then33
  br label %if.end56

if.else48:                                        ; preds = %for.body23
  %64 = load i8, i8* %normal_contact_average.addr, align 1, !tbaa !6, !range !20
  %tobool49 = trunc i8 %64 to i1
  br i1 %tobool49, label %land.lhs.true, label %if.end52

land.lhs.true:                                    ; preds = %if.else48
  %65 = load i32, i32* %coincident_count, align 4, !tbaa !8
  %cmp50 = icmp sgt i32 %65, 0
  br i1 %cmp50, label %if.then51, label %if.end52

if.then51:                                        ; preds = %land.lhs.true
  %66 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %pcontact, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %coincident_normals, i32 0, i32 0
  %67 = load i32, i32* %coincident_count, align 4, !tbaa !8
  call void @_ZN11GIM_CONTACT19interpolate_normalsEP9btVector3i(%class.GIM_CONTACT* %66, %class.btVector3* %arraydecay, i32 %67)
  store i32 0, i32* %coincident_count, align 4, !tbaa !8
  br label %if.end52

if.end52:                                         ; preds = %if.then51, %land.lhs.true, %if.else48
  %68 = bitcast %class.btContactArray* %this1 to %class.btAlignedObjectArray*
  %69 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %scontact, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE9push_backERKS0_(%class.btAlignedObjectArray* %68, %class.GIM_CONTACT* nonnull align 4 dereferenceable(48) %69)
  %70 = bitcast %class.btContactArray* %this1 to %class.btAlignedObjectArray*
  %71 = bitcast %class.btContactArray* %this1 to %class.btAlignedObjectArray*
  %call53 = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv(%class.btAlignedObjectArray* %71)
  %sub54 = sub nsw i32 %call53, 1
  %call55 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZN20btAlignedObjectArrayI11GIM_CONTACTEixEi(%class.btAlignedObjectArray* %70, i32 %sub54)
  store %class.GIM_CONTACT* %call55, %class.GIM_CONTACT** %pcontact, align 4, !tbaa !2
  br label %if.end56

if.end56:                                         ; preds = %if.end52, %if.end47
  %72 = load i32, i32* %key, align 4, !tbaa !8
  store i32 %72, i32* %last_key, align 4, !tbaa !8
  %73 = bitcast %class.GIM_CONTACT** %scontact to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #7
  br label %for.inc57

for.inc57:                                        ; preds = %if.end56
  %74 = load i32, i32* %i, align 4, !tbaa !8
  %inc58 = add nsw i32 %74, 1
  store i32 %inc58, i32* %i, align 4, !tbaa !8
  br label %for.cond20

for.end59:                                        ; preds = %for.cond20
  %75 = bitcast %class.GIM_CONTACT** %pcontact to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #7
  %76 = bitcast i32* %key to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #7
  %77 = bitcast i32* %last_key to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #7
  %78 = bitcast [8 x %class.btVector3]* %coincident_normals to i8*
  call void @llvm.lifetime.end.p0i8(i64 128, i8* %78) #7
  %79 = bitcast i32* %coincident_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #7
  %call60 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENED2Ev(%class.btAlignedObjectArray.0* %keycontacts) #7
  %80 = bitcast %class.btAlignedObjectArray.0* %keycontacts to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %80) #7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end59, %if.then4, %if.then
  %81 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE5clearEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !22
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE9push_backERKS0_(%class.btAlignedObjectArray* %this, %class.GIM_CONTACT* nonnull align 4 dereferenceable(48) %_Val) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %class.GIM_CONTACT*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store %class.GIM_CONTACT* %_Val, %class.GIM_CONTACT** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !8
  %1 = load i32, i32* %sz, align 4, !tbaa !8
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI11GIM_CONTACTE9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %m_data, align 4, !tbaa !25
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !22
  %arrayidx = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %2, i32 %3
  %4 = bitcast %class.GIM_CONTACT* %arrayidx to i8*
  %5 = bitcast i8* %4 to %class.GIM_CONTACT*
  %6 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %_Val.addr, align 4, !tbaa !2
  %call5 = call %class.GIM_CONTACT* @_ZN11GIM_CONTACTC2ERKS_(%class.GIM_CONTACT* %5, %class.GIM_CONTACT* nonnull align 4 dereferenceable(48) %6)
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size6, align 4, !tbaa !22
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size6, align 4, !tbaa !22
  %8 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK20btAlignedObjectArrayI11GIM_CONTACTEixEi(%class.btAlignedObjectArray* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %m_data, align 4, !tbaa !25
  %1 = load i32, i32* %n.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %0, i32 %1
  ret %class.GIM_CONTACT* %arrayidx
}

define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENEC2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorI17CONTACT_KEY_TOKENLj16EEC2Ev(%class.btAlignedAllocator.1* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE7reserveEi(%class.btAlignedObjectArray.0* %this, i32 %_Count) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.CONTACT_KEY_TOKEN*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.CONTACT_KEY_TOKEN** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %call2 = call i8* @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE8allocateEi(%class.btAlignedObjectArray.0* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.CONTACT_KEY_TOKEN*
  store %struct.CONTACT_KEY_TOKEN* %3, %struct.CONTACT_KEY_TOKEN** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %4 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4copyEiiPS0_(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call3, %struct.CONTACT_KEY_TOKEN* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !26
  %5 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.CONTACT_KEY_TOKEN* %5, %struct.CONTACT_KEY_TOKEN** %m_data, align 4, !tbaa !29
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !30
  %7 = bitcast %struct.CONTACT_KEY_TOKEN** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE9push_backERKS0_(%class.btAlignedObjectArray.0* %this, %struct.CONTACT_KEY_TOKEN* nonnull align 4 dereferenceable(8) %_Val) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Val.addr = alloca %struct.CONTACT_KEY_TOKEN*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store %struct.CONTACT_KEY_TOKEN* %_Val, %struct.CONTACT_KEY_TOKEN** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !8
  %1 = load i32, i32* %sz, align 4, !tbaa !8
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE9allocSizeEi(%class.btAlignedObjectArray.0* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %m_data, align 4, !tbaa !29
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !31
  %arrayidx = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %2, i32 %3
  %4 = bitcast %struct.CONTACT_KEY_TOKEN* %arrayidx to i8*
  %5 = bitcast i8* %4 to %struct.CONTACT_KEY_TOKEN*
  %6 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %_Val.addr, align 4, !tbaa !2
  %call5 = call %struct.CONTACT_KEY_TOKEN* @_ZN17CONTACT_KEY_TOKENC2ERKS_(%struct.CONTACT_KEY_TOKEN* %5, %struct.CONTACT_KEY_TOKEN* nonnull align 4 dereferenceable(8) %6)
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size6, align 4, !tbaa !31
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size6, align 4, !tbaa !31
  %8 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden i32 @_ZNK11GIM_CONTACT16calc_key_contactEv(%class.GIM_CONTACT* %this) #1 comdat {
entry:
  %this.addr = alloca %class.GIM_CONTACT*, align 4
  %_coords = alloca [3 x i32], align 4
  %_hash = alloca i32, align 4
  %_uitmp = alloca i32*, align 4
  store %class.GIM_CONTACT* %this, %class.GIM_CONTACT** %this.addr, align 4, !tbaa !2
  %this1 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %this.addr, align 4
  %0 = bitcast [3 x i32]* %_coords to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %0) #7
  %arrayinit.begin = getelementptr inbounds [3 x i32], [3 x i32]* %_coords, i32 0, i32 0
  %m_point = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_point)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !19
  %mul = fmul float %1, 1.000000e+03
  %add = fadd float %mul, 1.000000e+00
  %conv = fptosi float %add to i32
  store i32 %conv, i32* %arrayinit.begin, align 4, !tbaa !8
  %arrayinit.element = getelementptr inbounds i32, i32* %arrayinit.begin, i32 1
  %m_point2 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 0
  %call3 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_point2)
  %arrayidx4 = getelementptr inbounds float, float* %call3, i32 1
  %2 = load float, float* %arrayidx4, align 4, !tbaa !19
  %mul5 = fmul float %2, 1.333000e+03
  %conv6 = fptosi float %mul5 to i32
  store i32 %conv6, i32* %arrayinit.element, align 4, !tbaa !8
  %arrayinit.element7 = getelementptr inbounds i32, i32* %arrayinit.element, i32 1
  %m_point8 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 0
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_point8)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 2
  %3 = load float, float* %arrayidx10, align 4, !tbaa !19
  %mul11 = fmul float %3, 2.133000e+03
  %add12 = fadd float %mul11, 3.000000e+00
  %conv13 = fptosi float %add12 to i32
  store i32 %conv13, i32* %arrayinit.element7, align 4, !tbaa !8
  %4 = bitcast i32* %_hash to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  store i32 0, i32* %_hash, align 4, !tbaa !8
  %5 = bitcast i32** %_uitmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %arrayidx14 = getelementptr inbounds [3 x i32], [3 x i32]* %_coords, i32 0, i32 0
  store i32* %arrayidx14, i32** %_uitmp, align 4, !tbaa !2
  %6 = load i32*, i32** %_uitmp, align 4, !tbaa !2
  %7 = load i32, i32* %6, align 4, !tbaa !8
  store i32 %7, i32* %_hash, align 4, !tbaa !8
  %8 = load i32*, i32** %_uitmp, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i32, i32* %8, i32 1
  store i32* %incdec.ptr, i32** %_uitmp, align 4, !tbaa !2
  %9 = load i32*, i32** %_uitmp, align 4, !tbaa !2
  %10 = load i32, i32* %9, align 4, !tbaa !8
  %shl = shl i32 %10, 4
  %11 = load i32, i32* %_hash, align 4, !tbaa !8
  %add15 = add i32 %11, %shl
  store i32 %add15, i32* %_hash, align 4, !tbaa !8
  %12 = load i32*, i32** %_uitmp, align 4, !tbaa !2
  %incdec.ptr16 = getelementptr inbounds i32, i32* %12, i32 1
  store i32* %incdec.ptr16, i32** %_uitmp, align 4, !tbaa !2
  %13 = load i32*, i32** %_uitmp, align 4, !tbaa !2
  %14 = load i32, i32* %13, align 4, !tbaa !8
  %shl17 = shl i32 %14, 8
  %15 = load i32, i32* %_hash, align 4, !tbaa !8
  %add18 = add i32 %15, %shl17
  store i32 %add18, i32* %_hash, align 4, !tbaa !8
  %16 = load i32, i32* %_hash, align 4, !tbaa !8
  %17 = bitcast i32** %_uitmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  %18 = bitcast i32* %_hash to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #7
  %19 = bitcast [3 x i32]* %_coords to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %19) #7
  ret i32 %16
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.CONTACT_KEY_TOKEN* @_ZN17CONTACT_KEY_TOKENC2Eji(%struct.CONTACT_KEY_TOKEN* returned %this, i32 %key, i32 %token) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %struct.CONTACT_KEY_TOKEN*, align 4
  %key.addr = alloca i32, align 4
  %token.addr = alloca i32, align 4
  store %struct.CONTACT_KEY_TOKEN* %this, %struct.CONTACT_KEY_TOKEN** %this.addr, align 4, !tbaa !2
  store i32 %key, i32* %key.addr, align 4, !tbaa !8
  store i32 %token, i32* %token.addr, align 4, !tbaa !8
  %this1 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %this.addr, align 4
  %0 = load i32, i32* %key.addr, align 4, !tbaa !8
  %m_key = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %this1, i32 0, i32 0
  store i32 %0, i32* %m_key, align 4, !tbaa !10
  %1 = load i32, i32* %token.addr, align 4, !tbaa !8
  %m_value = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %this1, i32 0, i32 1
  store i32 %1, i32* %m_value, align 4, !tbaa !12
  ret %struct.CONTACT_KEY_TOKEN* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE9quickSortI22CONTACT_KEY_TOKEN_COMPEEvRKT_(%class.btAlignedObjectArray.0* %this, %class.CONTACT_KEY_TOKEN_COMP* nonnull align 1 dereferenceable(1) %CompareFunc) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %CompareFunc.addr = alloca %class.CONTACT_KEY_TOKEN_COMP*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store %class.CONTACT_KEY_TOKEN_COMP* %CompareFunc, %class.CONTACT_KEY_TOKEN_COMP** %CompareFunc.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %cmp = icmp sgt i32 %call, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = load %class.CONTACT_KEY_TOKEN_COMP*, %class.CONTACT_KEY_TOKEN_COMP** %CompareFunc.addr, align 4, !tbaa !2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE17quickSortInternalI22CONTACT_KEY_TOKEN_COMPEEvRKT_ii(%class.btAlignedObjectArray.0* %this1, %class.CONTACT_KEY_TOKEN_COMP* nonnull align 1 dereferenceable(1) %0, i32 0, i32 %sub)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %struct.CONTACT_KEY_TOKEN* @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %m_data, align 4, !tbaa !29
  %1 = load i32, i32* %n.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %0, i32 %1
  ret %struct.CONTACT_KEY_TOKEN* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZN20btAlignedObjectArrayI11GIM_CONTACTEixEi(%class.btAlignedObjectArray* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %m_data, align 4, !tbaa !25
  %1 = load i32, i32* %n.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %0, i32 %1
  ret %class.GIM_CONTACT* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4sizeEv(%class.btAlignedObjectArray.0* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !31
  ret i32 %0
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btFabsf(float %x) #3 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !19
  %0 = load float, float* %x.addr, align 4, !tbaa !19
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN11GIM_CONTACT19interpolate_normalsEP9btVector3i(%class.GIM_CONTACT* %this, %class.btVector3* %normals, i32 %normal_count) #1 comdat {
entry:
  %this.addr = alloca %class.GIM_CONTACT*, align 4
  %normals.addr = alloca %class.btVector3*, align 4
  %normal_count.addr = alloca i32, align 4
  %vec_sum = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  %vec_sum_len = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.GIM_CONTACT* %this, %class.GIM_CONTACT** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %normals, %class.btVector3** %normals.addr, align 4, !tbaa !2
  store i32 %normal_count, i32* %normal_count.addr, align 4, !tbaa !8
  %this1 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %this.addr, align 4
  %0 = bitcast %class.btVector3* %vec_sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #7
  %m_normal = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %vec_sum to i8*
  %2 = bitcast %class.btVector3* %m_normal to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !21
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %4 = load i32, i32* %i, align 4, !tbaa !8
  %5 = load i32, i32* %normal_count.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %4, %5
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %6 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load %class.btVector3*, %class.btVector3** %normals.addr, align 4, !tbaa !2
  %8 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 %8
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %vec_sum, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %10 = bitcast float* %vec_sum_len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %call2 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %vec_sum)
  store float %call2, float* %vec_sum_len, align 4, !tbaa !19
  %11 = load float, float* %vec_sum_len, align 4, !tbaa !19
  %cmp3 = fcmp olt float %11, 0x3EE4F8B580000000
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.end
  %12 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %12) #7
  %13 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = load float, float* %vec_sum_len, align 4, !tbaa !19
  %call5 = call float @_Z6btSqrtf(float %14)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !19
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %vec_sum, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %m_normal6 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 1
  %15 = bitcast %class.btVector3* %m_normal6 to i8*
  %16 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 16, i1 false), !tbaa.struct !21
  %17 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  %18 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %19 = bitcast float* %vec_sum_len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #7
  %20 = bitcast %class.btVector3* %vec_sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %20) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENED2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE5clearEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

define hidden void @_ZN14btContactArray21merge_contacts_uniqueERKS_(%class.btContactArray* %this, %class.btContactArray* nonnull align 4 dereferenceable(17) %contacts) #0 {
entry:
  %this.addr = alloca %class.btContactArray*, align 4
  %contacts.addr = alloca %class.btContactArray*, align 4
  %average_contact = alloca %class.GIM_CONTACT, align 4
  %i = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %divide_average = alloca float, align 4
  store %class.btContactArray* %this, %class.btContactArray** %this.addr, align 4, !tbaa !2
  store %class.btContactArray* %contacts, %class.btContactArray** %contacts.addr, align 4, !tbaa !2
  %this1 = load %class.btContactArray*, %class.btContactArray** %this.addr, align 4
  %0 = bitcast %class.btContactArray* %this1 to %class.btAlignedObjectArray*
  call void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE5clearEv(%class.btAlignedObjectArray* %0)
  %1 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4, !tbaa !2
  %2 = bitcast %class.btContactArray* %1 to %class.btAlignedObjectArray*
  %call = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv(%class.btAlignedObjectArray* %2)
  %cmp = icmp eq i32 %call, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %3 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4, !tbaa !2
  %4 = bitcast %class.btContactArray* %3 to %class.btAlignedObjectArray*
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv(%class.btAlignedObjectArray* %4)
  %cmp3 = icmp eq i32 %call2, 1
  br i1 %cmp3, label %if.then4, label %if.end6

if.then4:                                         ; preds = %if.end
  %5 = bitcast %class.btContactArray* %this1 to %class.btAlignedObjectArray*
  %6 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4, !tbaa !2
  %7 = bitcast %class.btContactArray* %6 to %class.btAlignedObjectArray*
  %call5 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK20btAlignedObjectArrayI11GIM_CONTACTEixEi(%class.btAlignedObjectArray* %7, i32 0)
  call void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE9push_backERKS0_(%class.btAlignedObjectArray* %5, %class.GIM_CONTACT* nonnull align 4 dereferenceable(48) %call5)
  br label %return

if.end6:                                          ; preds = %if.end
  %8 = bitcast %class.GIM_CONTACT* %average_contact to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %8) #7
  %9 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4, !tbaa !2
  %10 = bitcast %class.btContactArray* %9 to %class.btAlignedObjectArray*
  %call7 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK20btAlignedObjectArrayI11GIM_CONTACTEixEi(%class.btAlignedObjectArray* %10, i32 0)
  %call8 = call %class.GIM_CONTACT* @_ZN11GIM_CONTACTC2ERKS_(%class.GIM_CONTACT* %average_contact, %class.GIM_CONTACT* nonnull align 4 dereferenceable(48) %call7)
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #7
  store i32 1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end6
  %12 = load i32, i32* %i, align 4, !tbaa !8
  %13 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4, !tbaa !2
  %14 = bitcast %class.btContactArray* %13 to %class.btAlignedObjectArray*
  %call9 = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv(%class.btAlignedObjectArray* %14)
  %cmp10 = icmp slt i32 %12, %call9
  br i1 %cmp10, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %15 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %16 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4, !tbaa !2
  %17 = bitcast %class.btContactArray* %16 to %class.btAlignedObjectArray*
  %18 = load i32, i32* %i, align 4, !tbaa !8
  %call11 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK20btAlignedObjectArrayI11GIM_CONTACTEixEi(%class.btAlignedObjectArray* %17, i32 %18)
  %m_point = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %call11, i32 0, i32 0
  %m_point12 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %average_contact, i32 0, i32 0
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_point12, %class.btVector3* nonnull align 4 dereferenceable(16) %m_point)
  %19 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %19) #7
  %20 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4, !tbaa !2
  %21 = bitcast %class.btContactArray* %20 to %class.btAlignedObjectArray*
  %22 = load i32, i32* %i, align 4, !tbaa !8
  %call14 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK20btAlignedObjectArrayI11GIM_CONTACTEixEi(%class.btAlignedObjectArray* %21, i32 %22)
  %m_normal = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %call14, i32 0, i32 1
  %23 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4, !tbaa !2
  %24 = bitcast %class.btContactArray* %23 to %class.btAlignedObjectArray*
  %25 = load i32, i32* %i, align 4, !tbaa !8
  %call15 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK20btAlignedObjectArrayI11GIM_CONTACTEixEi(%class.btAlignedObjectArray* %24, i32 %25)
  %m_depth = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %call15, i32 0, i32 2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normal, float* nonnull align 4 dereferenceable(4) %m_depth)
  %m_normal16 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %average_contact, i32 0, i32 1
  %call17 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_normal16, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %26 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %26) #7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %27 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %27, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %28 = bitcast float* %divide_average to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #7
  %29 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4, !tbaa !2
  %30 = bitcast %class.btContactArray* %29 to %class.btAlignedObjectArray*
  %call18 = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv(%class.btAlignedObjectArray* %30)
  %conv = sitofp i32 %call18 to float
  %div = fdiv float 1.000000e+00, %conv
  store float %div, float* %divide_average, align 4, !tbaa !19
  %m_point19 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %average_contact, i32 0, i32 0
  %call20 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %m_point19, float* nonnull align 4 dereferenceable(4) %divide_average)
  %m_normal21 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %average_contact, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %m_normal21, float* nonnull align 4 dereferenceable(4) %divide_average)
  %m_normal23 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %average_contact, i32 0, i32 1
  %call24 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %m_normal23)
  %m_depth25 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %average_contact, i32 0, i32 2
  store float %call24, float* %m_depth25, align 4, !tbaa !13
  %m_depth26 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %average_contact, i32 0, i32 2
  %m_normal27 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %average_contact, i32 0, i32 1
  %call28 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %m_normal27, float* nonnull align 4 dereferenceable(4) %m_depth26)
  %31 = bitcast float* %divide_average to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #7
  %32 = bitcast %class.GIM_CONTACT* %average_contact to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %32) #7
  br label %return

return:                                           ; preds = %for.end, %if.then4, %if.then
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.GIM_CONTACT* @_ZN11GIM_CONTACTC2ERKS_(%class.GIM_CONTACT* returned %this, %class.GIM_CONTACT* nonnull align 4 dereferenceable(48) %contact) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.GIM_CONTACT*, align 4
  %contact.addr = alloca %class.GIM_CONTACT*, align 4
  store %class.GIM_CONTACT* %this, %class.GIM_CONTACT** %this.addr, align 4, !tbaa !2
  store %class.GIM_CONTACT* %contact, %class.GIM_CONTACT** %contact.addr, align 4, !tbaa !2
  %this1 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %this.addr, align 4
  %m_point = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 0
  %0 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %contact.addr, align 4, !tbaa !2
  %m_point2 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %0, i32 0, i32 0
  %1 = bitcast %class.btVector3* %m_point to i8*
  %2 = bitcast %class.btVector3* %m_point2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !21
  %m_normal = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 1
  %3 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %contact.addr, align 4, !tbaa !2
  %m_normal3 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %3, i32 0, i32 1
  %4 = bitcast %class.btVector3* %m_normal to i8*
  %5 = bitcast %class.btVector3* %m_normal3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !21
  %m_depth = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 2
  %6 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %contact.addr, align 4, !tbaa !2
  %m_depth4 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %6, i32 0, i32 2
  %7 = load float, float* %m_depth4, align 4, !tbaa !13
  store float %7, float* %m_depth, align 4, !tbaa !13
  %m_feature1 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 4
  %8 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %contact.addr, align 4, !tbaa !2
  %m_feature15 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %8, i32 0, i32 4
  %9 = load i32, i32* %m_feature15, align 4, !tbaa !32
  store i32 %9, i32* %m_feature1, align 4, !tbaa !32
  %m_feature2 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 5
  %10 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %contact.addr, align 4, !tbaa !2
  %m_feature26 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %10, i32 0, i32 5
  %11 = load i32, i32* %m_feature26, align 4, !tbaa !33
  store i32 %11, i32* %m_feature2, align 4, !tbaa !33
  ret %class.GIM_CONTACT* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !19
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !19
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !19
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !19
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !19
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !19
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !19
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !19
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !19
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !19
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !19
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !19
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !19
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !19
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !19
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !19
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !19
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !19
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #7
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #7
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !19
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !19
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !19
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !19
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !19
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !19
  %6 = load float*, float** %s.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !19
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !19
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !19
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !19
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !19
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #7
  ret %class.btVector3* %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #5

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZdvRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load float*, float** %s.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !19
  %div = fdiv float 1.000000e+00, %3
  store float %div, float* %ref.tmp, align 4, !tbaa !19
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %4 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #3 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !19
  %0 = load float, float* %y.addr, align 4, !tbaa !19
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !19
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !19
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !19
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !19
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !19
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !19
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #5

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !19
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !19
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !19
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !19
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !19
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !19
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !19
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE8capacityEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !34
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.GIM_CONTACT*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.GIM_CONTACT** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %call2 = call i8* @_ZN20btAlignedObjectArrayI11GIM_CONTACTE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.GIM_CONTACT*
  store %class.GIM_CONTACT* %3, %class.GIM_CONTACT** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv(%class.btAlignedObjectArray* %this1)
  %4 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %class.GIM_CONTACT* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !35
  %5 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.GIM_CONTACT* %5, %class.GIM_CONTACT** %m_data, align 4, !tbaa !25
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !34
  %7 = bitcast %class.GIM_CONTACT** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI11GIM_CONTACTE9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !8
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI11GIM_CONTACTE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #1 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !8
  %call = call %class.GIM_CONTACT* @_ZN18btAlignedAllocatorI11GIM_CONTACTLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %class.GIM_CONTACT** null)
  %2 = bitcast %class.GIM_CONTACT* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %class.GIM_CONTACT* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.GIM_CONTACT*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !8
  store i32 %end, i32* %end.addr, align 4, !tbaa !8
  store %class.GIM_CONTACT* %dest, %class.GIM_CONTACT** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %start.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %end.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %4, i32 %5
  %6 = bitcast %class.GIM_CONTACT* %arrayidx to i8*
  %7 = bitcast i8* %6 to %class.GIM_CONTACT*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %m_data, align 4, !tbaa !25
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx2 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %8, i32 %9
  %call = call %class.GIM_CONTACT* @_ZN11GIM_CONTACTC2ERKS_(%class.GIM_CONTACT* %7, %class.GIM_CONTACT* nonnull align 4 dereferenceable(48) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !8
  store i32 %last, i32* %last.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %first.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %last.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %m_data, align 4, !tbaa !25
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE10deallocateEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %m_data, align 4, !tbaa !25
  %tobool = icmp ne %class.GIM_CONTACT* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !35, !range !20
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %m_data4, align 4, !tbaa !25
  call void @_ZN18btAlignedAllocatorI11GIM_CONTACTLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %class.GIM_CONTACT* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.GIM_CONTACT* null, %class.GIM_CONTACT** %m_data5, align 4, !tbaa !25
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden %class.GIM_CONTACT* @_ZN18btAlignedAllocatorI11GIM_CONTACTLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %class.GIM_CONTACT** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.GIM_CONTACT**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  store %class.GIM_CONTACT** %hint, %class.GIM_CONTACT*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !8
  %mul = mul i32 48, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.GIM_CONTACT*
  ret %class.GIM_CONTACT* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #6

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI11GIM_CONTACTLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %class.GIM_CONTACT* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.GIM_CONTACT*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store %class.GIM_CONTACT* %ptr, %class.GIM_CONTACT** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.GIM_CONTACT* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #6

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE4initEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !35
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.GIM_CONTACT* null, %class.GIM_CONTACT** %m_data, align 4, !tbaa !25
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !22
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !34
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorI17CONTACT_KEY_TOKENLj16EEC2Ev(%class.btAlignedAllocator.1* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  ret %class.btAlignedAllocator.1* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4initEv(%class.btAlignedObjectArray.0* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !26
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.CONTACT_KEY_TOKEN* null, %struct.CONTACT_KEY_TOKEN** %m_data, align 4, !tbaa !29
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !31
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !30
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE5clearEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !8
  store i32 %last, i32* %last.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %first.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %last.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %4 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %m_data, align 4, !tbaa !29
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE10deallocateEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %m_data, align 4, !tbaa !29
  %tobool = icmp ne %struct.CONTACT_KEY_TOKEN* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !26, !range !20
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %m_data4, align 4, !tbaa !29
  call void @_ZN18btAlignedAllocatorI17CONTACT_KEY_TOKENLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %m_allocator, %struct.CONTACT_KEY_TOKEN* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.CONTACT_KEY_TOKEN* null, %struct.CONTACT_KEY_TOKEN** %m_data5, align 4, !tbaa !29
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI17CONTACT_KEY_TOKENLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %this, %struct.CONTACT_KEY_TOKEN* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca %struct.CONTACT_KEY_TOKEN*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  store %struct.CONTACT_KEY_TOKEN* %ptr, %struct.CONTACT_KEY_TOKEN** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.CONTACT_KEY_TOKEN* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE8capacityEv(%class.btAlignedObjectArray.0* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !30
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE8allocateEi(%class.btAlignedObjectArray.0* %this, i32 %size) #1 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !8
  %call = call %struct.CONTACT_KEY_TOKEN* @_ZN18btAlignedAllocatorI17CONTACT_KEY_TOKENLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %m_allocator, i32 %1, %struct.CONTACT_KEY_TOKEN** null)
  %2 = bitcast %struct.CONTACT_KEY_TOKEN* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4copyEiiPS0_(%class.btAlignedObjectArray.0* %this, i32 %start, i32 %end, %struct.CONTACT_KEY_TOKEN* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.CONTACT_KEY_TOKEN*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !8
  store i32 %end, i32* %end.addr, align 4, !tbaa !8
  store %struct.CONTACT_KEY_TOKEN* %dest, %struct.CONTACT_KEY_TOKEN** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %start.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %end.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %4, i32 %5
  %6 = bitcast %struct.CONTACT_KEY_TOKEN* %arrayidx to i8*
  %7 = bitcast i8* %6 to %struct.CONTACT_KEY_TOKEN*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %8 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %m_data, align 4, !tbaa !29
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx2 = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %8, i32 %9
  %call = call %struct.CONTACT_KEY_TOKEN* @_ZN17CONTACT_KEY_TOKENC2ERKS_(%struct.CONTACT_KEY_TOKEN* %7, %struct.CONTACT_KEY_TOKEN* nonnull align 4 dereferenceable(8) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #7
  ret void
}

define linkonce_odr hidden %struct.CONTACT_KEY_TOKEN* @_ZN18btAlignedAllocatorI17CONTACT_KEY_TOKENLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %this, i32 %n, %struct.CONTACT_KEY_TOKEN** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.CONTACT_KEY_TOKEN**, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  store %struct.CONTACT_KEY_TOKEN** %hint, %struct.CONTACT_KEY_TOKEN*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !8
  %mul = mul i32 8, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.CONTACT_KEY_TOKEN*
  ret %struct.CONTACT_KEY_TOKEN* %1
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.CONTACT_KEY_TOKEN* @_ZN17CONTACT_KEY_TOKENC2ERKS_(%struct.CONTACT_KEY_TOKEN* returned %this, %struct.CONTACT_KEY_TOKEN* nonnull align 4 dereferenceable(8) %rtoken) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %struct.CONTACT_KEY_TOKEN*, align 4
  %rtoken.addr = alloca %struct.CONTACT_KEY_TOKEN*, align 4
  store %struct.CONTACT_KEY_TOKEN* %this, %struct.CONTACT_KEY_TOKEN** %this.addr, align 4, !tbaa !2
  store %struct.CONTACT_KEY_TOKEN* %rtoken, %struct.CONTACT_KEY_TOKEN** %rtoken.addr, align 4, !tbaa !2
  %this1 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %this.addr, align 4
  %0 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %rtoken.addr, align 4, !tbaa !2
  %m_key = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %0, i32 0, i32 0
  %1 = load i32, i32* %m_key, align 4, !tbaa !10
  %m_key2 = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %this1, i32 0, i32 0
  store i32 %1, i32* %m_key2, align 4, !tbaa !10
  %2 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %rtoken.addr, align 4, !tbaa !2
  %m_value = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %2, i32 0, i32 1
  %3 = load i32, i32* %m_value, align 4, !tbaa !12
  %m_value3 = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %this1, i32 0, i32 1
  store i32 %3, i32* %m_value3, align 4, !tbaa !12
  ret %struct.CONTACT_KEY_TOKEN* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE9allocSizeEi(%class.btAlignedObjectArray.0* %this, i32 %size) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !8
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE17quickSortInternalI22CONTACT_KEY_TOKEN_COMPEEvRKT_ii(%class.btAlignedObjectArray.0* %this, %class.CONTACT_KEY_TOKEN_COMP* nonnull align 1 dereferenceable(1) %CompareFunc, i32 %lo, i32 %hi) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %CompareFunc.addr = alloca %class.CONTACT_KEY_TOKEN_COMP*, align 4
  %lo.addr = alloca i32, align 4
  %hi.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %x = alloca %struct.CONTACT_KEY_TOKEN, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store %class.CONTACT_KEY_TOKEN_COMP* %CompareFunc, %class.CONTACT_KEY_TOKEN_COMP** %CompareFunc.addr, align 4, !tbaa !2
  store i32 %lo, i32* %lo.addr, align 4, !tbaa !8
  store i32 %hi, i32* %hi.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %lo.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  %2 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load i32, i32* %hi.addr, align 4, !tbaa !8
  store i32 %3, i32* %j, align 4, !tbaa !8
  %4 = bitcast %struct.CONTACT_KEY_TOKEN* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %4) #7
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %5 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %m_data, align 4, !tbaa !29
  %6 = load i32, i32* %lo.addr, align 4, !tbaa !8
  %7 = load i32, i32* %hi.addr, align 4, !tbaa !8
  %add = add nsw i32 %6, %7
  %div = sdiv i32 %add, 2
  %arrayidx = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %5, i32 %div
  %call = call %struct.CONTACT_KEY_TOKEN* @_ZN17CONTACT_KEY_TOKENC2ERKS_(%struct.CONTACT_KEY_TOKEN* %x, %struct.CONTACT_KEY_TOKEN* nonnull align 4 dereferenceable(8) %arrayidx)
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %do.body
  %8 = load %class.CONTACT_KEY_TOKEN_COMP*, %class.CONTACT_KEY_TOKEN_COMP** %CompareFunc.addr, align 4, !tbaa !2
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %9 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %m_data2, align 4, !tbaa !29
  %10 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx3 = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %9, i32 %10
  %call4 = call zeroext i1 @_ZNK22CONTACT_KEY_TOKEN_COMPclERK17CONTACT_KEY_TOKENS2_(%class.CONTACT_KEY_TOKEN_COMP* %8, %struct.CONTACT_KEY_TOKEN* nonnull align 4 dereferenceable(8) %arrayidx3, %struct.CONTACT_KEY_TOKEN* nonnull align 4 dereferenceable(8) %x)
  br i1 %call4, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %11 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %while.cond5

while.cond5:                                      ; preds = %while.body9, %while.end
  %12 = load %class.CONTACT_KEY_TOKEN_COMP*, %class.CONTACT_KEY_TOKEN_COMP** %CompareFunc.addr, align 4, !tbaa !2
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %13 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %m_data6, align 4, !tbaa !29
  %14 = load i32, i32* %j, align 4, !tbaa !8
  %arrayidx7 = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %13, i32 %14
  %call8 = call zeroext i1 @_ZNK22CONTACT_KEY_TOKEN_COMPclERK17CONTACT_KEY_TOKENS2_(%class.CONTACT_KEY_TOKEN_COMP* %12, %struct.CONTACT_KEY_TOKEN* nonnull align 4 dereferenceable(8) %x, %struct.CONTACT_KEY_TOKEN* nonnull align 4 dereferenceable(8) %arrayidx7)
  br i1 %call8, label %while.body9, label %while.end10

while.body9:                                      ; preds = %while.cond5
  %15 = load i32, i32* %j, align 4, !tbaa !8
  %dec = add nsw i32 %15, -1
  store i32 %dec, i32* %j, align 4, !tbaa !8
  br label %while.cond5

while.end10:                                      ; preds = %while.cond5
  %16 = load i32, i32* %i, align 4, !tbaa !8
  %17 = load i32, i32* %j, align 4, !tbaa !8
  %cmp = icmp sle i32 %16, %17
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %while.end10
  %18 = load i32, i32* %i, align 4, !tbaa !8
  %19 = load i32, i32* %j, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4swapEii(%class.btAlignedObjectArray.0* %this1, i32 %18, i32 %19)
  %20 = load i32, i32* %i, align 4, !tbaa !8
  %inc11 = add nsw i32 %20, 1
  store i32 %inc11, i32* %i, align 4, !tbaa !8
  %21 = load i32, i32* %j, align 4, !tbaa !8
  %dec12 = add nsw i32 %21, -1
  store i32 %dec12, i32* %j, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end10
  br label %do.cond

do.cond:                                          ; preds = %if.end
  %22 = load i32, i32* %i, align 4, !tbaa !8
  %23 = load i32, i32* %j, align 4, !tbaa !8
  %cmp13 = icmp sle i32 %22, %23
  br i1 %cmp13, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %24 = load i32, i32* %lo.addr, align 4, !tbaa !8
  %25 = load i32, i32* %j, align 4, !tbaa !8
  %cmp14 = icmp slt i32 %24, %25
  br i1 %cmp14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %do.end
  %26 = load %class.CONTACT_KEY_TOKEN_COMP*, %class.CONTACT_KEY_TOKEN_COMP** %CompareFunc.addr, align 4, !tbaa !2
  %27 = load i32, i32* %lo.addr, align 4, !tbaa !8
  %28 = load i32, i32* %j, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE17quickSortInternalI22CONTACT_KEY_TOKEN_COMPEEvRKT_ii(%class.btAlignedObjectArray.0* %this1, %class.CONTACT_KEY_TOKEN_COMP* nonnull align 1 dereferenceable(1) %26, i32 %27, i32 %28)
  br label %if.end16

if.end16:                                         ; preds = %if.then15, %do.end
  %29 = load i32, i32* %i, align 4, !tbaa !8
  %30 = load i32, i32* %hi.addr, align 4, !tbaa !8
  %cmp17 = icmp slt i32 %29, %30
  br i1 %cmp17, label %if.then18, label %if.end19

if.then18:                                        ; preds = %if.end16
  %31 = load %class.CONTACT_KEY_TOKEN_COMP*, %class.CONTACT_KEY_TOKEN_COMP** %CompareFunc.addr, align 4, !tbaa !2
  %32 = load i32, i32* %i, align 4, !tbaa !8
  %33 = load i32, i32* %hi.addr, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE17quickSortInternalI22CONTACT_KEY_TOKEN_COMPEEvRKT_ii(%class.btAlignedObjectArray.0* %this1, %class.CONTACT_KEY_TOKEN_COMP* nonnull align 1 dereferenceable(1) %31, i32 %32, i32 %33)
  br label %if.end19

if.end19:                                         ; preds = %if.then18, %if.end16
  %34 = bitcast %struct.CONTACT_KEY_TOKEN* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %34) #7
  %35 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #7
  %36 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #7
  ret void
}

define linkonce_odr hidden zeroext i1 @_ZNK22CONTACT_KEY_TOKEN_COMPclERK17CONTACT_KEY_TOKENS2_(%class.CONTACT_KEY_TOKEN_COMP* %this, %struct.CONTACT_KEY_TOKEN* nonnull align 4 dereferenceable(8) %a, %struct.CONTACT_KEY_TOKEN* nonnull align 4 dereferenceable(8) %b) #0 comdat {
entry:
  %this.addr = alloca %class.CONTACT_KEY_TOKEN_COMP*, align 4
  %a.addr = alloca %struct.CONTACT_KEY_TOKEN*, align 4
  %b.addr = alloca %struct.CONTACT_KEY_TOKEN*, align 4
  store %class.CONTACT_KEY_TOKEN_COMP* %this, %class.CONTACT_KEY_TOKEN_COMP** %this.addr, align 4, !tbaa !2
  store %struct.CONTACT_KEY_TOKEN* %a, %struct.CONTACT_KEY_TOKEN** %a.addr, align 4, !tbaa !2
  store %struct.CONTACT_KEY_TOKEN* %b, %struct.CONTACT_KEY_TOKEN** %b.addr, align 4, !tbaa !2
  %this1 = load %class.CONTACT_KEY_TOKEN_COMP*, %class.CONTACT_KEY_TOKEN_COMP** %this.addr, align 4
  %0 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %a.addr, align 4, !tbaa !2
  %1 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %b.addr, align 4, !tbaa !2
  %call = call zeroext i1 @_ZNK17CONTACT_KEY_TOKENltERKS_(%struct.CONTACT_KEY_TOKEN* %0, %struct.CONTACT_KEY_TOKEN* nonnull align 4 dereferenceable(8) %1)
  ret i1 %call
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4swapEii(%class.btAlignedObjectArray.0* %this, i32 %index0, i32 %index1) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %struct.CONTACT_KEY_TOKEN, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %index0, i32* %index0.addr, align 4, !tbaa !8
  store i32 %index1, i32* %index1.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast %struct.CONTACT_KEY_TOKEN* %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #7
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %1 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %m_data, align 4, !tbaa !29
  %2 = load i32, i32* %index0.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %1, i32 %2
  %call = call %struct.CONTACT_KEY_TOKEN* @_ZN17CONTACT_KEY_TOKENC2ERKS_(%struct.CONTACT_KEY_TOKEN* %temp, %struct.CONTACT_KEY_TOKEN* nonnull align 4 dereferenceable(8) %arrayidx)
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %3 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %m_data2, align 4, !tbaa !29
  %4 = load i32, i32* %index1.addr, align 4, !tbaa !8
  %arrayidx3 = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %3, i32 %4
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %5 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %m_data4, align 4, !tbaa !29
  %6 = load i32, i32* %index0.addr, align 4, !tbaa !8
  %arrayidx5 = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %5, i32 %6
  %7 = bitcast %struct.CONTACT_KEY_TOKEN* %arrayidx5 to i8*
  %8 = bitcast %struct.CONTACT_KEY_TOKEN* %arrayidx3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 8, i1 false), !tbaa.struct !36
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %9 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %m_data6, align 4, !tbaa !29
  %10 = load i32, i32* %index1.addr, align 4, !tbaa !8
  %arrayidx7 = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %9, i32 %10
  %11 = bitcast %struct.CONTACT_KEY_TOKEN* %arrayidx7 to i8*
  %12 = bitcast %struct.CONTACT_KEY_TOKEN* %temp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 8, i1 false), !tbaa.struct !36
  %13 = bitcast %struct.CONTACT_KEY_TOKEN* %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %13) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZNK17CONTACT_KEY_TOKENltERKS_(%struct.CONTACT_KEY_TOKEN* %this, %struct.CONTACT_KEY_TOKEN* nonnull align 4 dereferenceable(8) %other) #3 comdat {
entry:
  %this.addr = alloca %struct.CONTACT_KEY_TOKEN*, align 4
  %other.addr = alloca %struct.CONTACT_KEY_TOKEN*, align 4
  store %struct.CONTACT_KEY_TOKEN* %this, %struct.CONTACT_KEY_TOKEN** %this.addr, align 4, !tbaa !2
  store %struct.CONTACT_KEY_TOKEN* %other, %struct.CONTACT_KEY_TOKEN** %other.addr, align 4, !tbaa !2
  %this1 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %this.addr, align 4
  %m_key = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %this1, i32 0, i32 0
  %0 = load i32, i32* %m_key, align 4, !tbaa !10
  %1 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %other.addr, align 4, !tbaa !2
  %m_key2 = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %1, i32 0, i32 0
  %2 = load i32, i32* %m_key2, align 4, !tbaa !10
  %cmp = icmp ult i32 %0, %2
  ret i1 %cmp
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind readnone speculatable willreturn }
attributes #6 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"bool", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"int", !4, i64 0}
!10 = !{!11, !9, i64 0}
!11 = !{!"_ZTS17CONTACT_KEY_TOKEN", !9, i64 0, !9, i64 4}
!12 = !{!11, !9, i64 4}
!13 = !{!14, !16, i64 32}
!14 = !{!"_ZTS11GIM_CONTACT", !15, i64 0, !15, i64 16, !16, i64 32, !16, i64 36, !9, i64 40, !9, i64 44}
!15 = !{!"_ZTS9btVector3", !4, i64 0}
!16 = !{!"float", !4, i64 0}
!17 = !{i64 0, i64 16, !18, i64 16, i64 16, !18, i64 32, i64 4, !19, i64 36, i64 4, !19, i64 40, i64 4, !8, i64 44, i64 4, !8}
!18 = !{!4, !4, i64 0}
!19 = !{!16, !16, i64 0}
!20 = !{i8 0, i8 2}
!21 = !{i64 0, i64 16, !18}
!22 = !{!23, !9, i64 4}
!23 = !{!"_ZTS20btAlignedObjectArrayI11GIM_CONTACTE", !24, i64 0, !9, i64 4, !9, i64 8, !3, i64 12, !7, i64 16}
!24 = !{!"_ZTS18btAlignedAllocatorI11GIM_CONTACTLj16EE"}
!25 = !{!23, !3, i64 12}
!26 = !{!27, !7, i64 16}
!27 = !{!"_ZTS20btAlignedObjectArrayI17CONTACT_KEY_TOKENE", !28, i64 0, !9, i64 4, !9, i64 8, !3, i64 12, !7, i64 16}
!28 = !{!"_ZTS18btAlignedAllocatorI17CONTACT_KEY_TOKENLj16EE"}
!29 = !{!27, !3, i64 12}
!30 = !{!27, !9, i64 8}
!31 = !{!27, !9, i64 4}
!32 = !{!14, !9, i64 40}
!33 = !{!14, !9, i64 44}
!34 = !{!23, !9, i64 8}
!35 = !{!23, !7, i64 16}
!36 = !{i64 0, i64 4, !8, i64 4, i64 4, !8}
