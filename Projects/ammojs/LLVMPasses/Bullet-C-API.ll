; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/Dynamics/Bullet-C-API.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/Dynamics/Bullet-C-API.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btVoronoiSimplexSolver = type <{ i32, [5 x %class.btVector3], [5 x %class.btVector3], [5 x %class.btVector3], %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, i8, [3 x i8], %struct.btSubSimplexClosestResult, i8, [3 x i8] }>
%class.btVector3 = type { [4 x float] }
%struct.btSubSimplexClosestResult = type <{ %class.btVector3, %struct.btUsageBitfield, [2 x i8], [4 x float], i8, [3 x i8] }>
%struct.btUsageBitfield = type { i8, i8 }
%class.btGjkEpaPenetrationDepthSolver = type { %class.btConvexPenetrationDepthSolver }
%class.btConvexPenetrationDepthSolver = type { i32 (...)** }
%struct.plPhysicsSdkHandle__ = type { i32 }
%struct.btPhysicsSdk = type { %class.btVector3, %class.btVector3 }
%struct.plDynamicsWorldHandle__ = type { i32 }
%class.btDefaultCollisionConfiguration = type { %class.btCollisionConfiguration, i32, %class.btPoolAllocator*, i8, %class.btPoolAllocator*, i8, %class.btVoronoiSimplexSolver*, %class.btConvexPenetrationDepthSolver*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc* }
%class.btCollisionConfiguration = type { i32 (...)** }
%class.btPoolAllocator = type opaque
%struct.btCollisionAlgorithmCreateFunc = type <{ i32 (...)**, i8, [3 x i8] }>
%struct.btDefaultCollisionConstructionInfo = type { %class.btPoolAllocator*, %class.btPoolAllocator*, i32, i32, i32, i32 }
%class.btDispatcher = type { i32 (...)** }
%class.btBroadphaseInterface = type { i32 (...)** }
%class.btConstraintSolver = type { i32 (...)** }
%class.btCollisionDispatcher = type { %class.btDispatcher, i32, %class.btAlignedObjectArray, %class.btManifoldResult, void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)*, %class.btPoolAllocator*, %class.btPoolAllocator*, [36 x [36 x %struct.btCollisionAlgorithmCreateFunc*]], %class.btCollisionConfiguration* }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btPersistentManifold**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, i32, i32, i32, i32, i8*, i8, float, float, float, float, float, float, float, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%union.anon = type { i8* }
%class.btManifoldResult = type { %"struct.btDiscreteCollisionDetectorInterface::Result", %class.btPersistentManifold*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i32, i32, i32, i32 }
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }
%struct.btCollisionObjectWrapper = type { %struct.btCollisionObjectWrapper*, %class.btCollisionShape*, %class.btCollisionObject*, %class.btTransform*, i32, i32 }
%struct.btBroadphasePair = type { %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btCollisionAlgorithm*, %union.anon.0 }
%class.btCollisionAlgorithm = type { i32 (...)**, %class.btDispatcher* }
%union.anon.0 = type { i8* }
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btIDebugDraw = type { i32 (...)** }
%class.btAxisSweep3 = type { %class.btAxisSweep3Internal }
%class.btAxisSweep3Internal = type { %class.btBroadphaseInterface, i16, i16, %class.btVector3, %class.btVector3, %class.btVector3, i16, i16, %"class.btAxisSweep3Internal<unsigned short>::Handle"*, i16, [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x i8*], %class.btOverlappingPairCache*, %class.btOverlappingPairCallback*, i8, i32, %struct.btDbvtBroadphase*, %class.btOverlappingPairCache* }
%"class.btAxisSweep3Internal<unsigned short>::Handle" = type opaque
%"class.btAxisSweep3Internal<unsigned short>::Edge" = type opaque
%class.btOverlappingPairCallback = type { i32 (...)** }
%struct.btDbvtBroadphase = type <{ %class.btBroadphaseInterface, [2 x %struct.btDbvt], [3 x %struct.btDbvtProxy*], %class.btOverlappingPairCache*, float, i32, i32, i32, i32, i32, i32, i32, i32, float, i32, i32, i32, i8, i8, i8, i8 }>
%struct.btDbvt = type { %struct.btDbvtNode*, %struct.btDbvtNode*, i32, i32, i32, %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.6 }
%struct.btDbvtNode = type { %struct.btDbvtAabbMm, %struct.btDbvtNode*, %union.anon.1 }
%struct.btDbvtAabbMm = type { %class.btVector3, %class.btVector3 }
%union.anon.1 = type { [2 x %struct.btDbvtNode*] }
%class.btAlignedObjectArray.2 = type <{ %class.btAlignedAllocator.3, [3 x i8], i32, i32, %"struct.btDbvt::sStkNN"*, i8, [3 x i8] }>
%class.btAlignedAllocator.3 = type { i8 }
%"struct.btDbvt::sStkNN" = type { %struct.btDbvtNode*, %struct.btDbvtNode* }
%class.btAlignedObjectArray.6 = type <{ %class.btAlignedAllocator.7, [3 x i8], i32, i32, %struct.btDbvtNode**, i8, [3 x i8] }>
%class.btAlignedAllocator.7 = type { i8 }
%struct.btDbvtProxy = type { %struct.btBroadphaseProxy, %struct.btDbvtNode*, [2 x %struct.btDbvtProxy*], i32 }
%class.btOverlappingPairCache = type { %class.btOverlappingPairCallback }
%class.btSequentialImpulseConstraintSolver = type { %class.btConstraintSolver, %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.24, %class.btAlignedObjectArray.24, %class.btAlignedObjectArray.24, %class.btAlignedObjectArray.28, i32, i32, i32 }
%class.btAlignedObjectArray.10 = type <{ %class.btAlignedAllocator.11, [3 x i8], i32, i32, %struct.btSolverBody*, i8, [3 x i8] }>
%class.btAlignedAllocator.11 = type { i8 }
%struct.btSolverBody = type { %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btRigidBody* }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.13, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray.13 = type <{ %class.btAlignedAllocator.14, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.14 = type { i8 }
%class.btTypedConstraint = type { i32 (...)**, %struct.btTypedObject, i32, %union.anon.16, float, i8, i8, i32, %class.btRigidBody*, %class.btRigidBody*, float, float, %struct.btJointFeedback* }
%union.anon.16 = type { i32 }
%struct.btJointFeedback = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%class.btAlignedObjectArray.19 = type <{ %class.btAlignedAllocator.20, [3 x i8], i32, i32, %struct.btSolverConstraint*, i8, [3 x i8] }>
%class.btAlignedAllocator.20 = type { i8 }
%struct.btSolverConstraint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, float, float, float, float, %union.anon.22, i32, i32, i32, i32 }
%union.anon.22 = type { i8* }
%class.btAlignedObjectArray.24 = type <{ %class.btAlignedAllocator.25, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.25 = type { i8 }
%class.btAlignedObjectArray.28 = type <{ %class.btAlignedAllocator.29, [3 x i8], i32, i32, %"struct.btTypedConstraint::btConstraintInfo1"*, i8, [3 x i8] }>
%class.btAlignedAllocator.29 = type { i8 }
%"struct.btTypedConstraint::btConstraintInfo1" = type { i32, i32 }
%class.btDiscreteDynamicsWorld = type { %class.btDynamicsWorld, %class.btAlignedObjectArray.13, %struct.InplaceSolverIslandCallback*, %class.btConstraintSolver*, %class.btSimulationIslandManager*, %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.36, %class.btVector3, float, float, i8, i8, i8, i8, %class.btAlignedObjectArray.40, i32, i8, [3 x i8], %class.btAlignedObjectArray }
%class.btDynamicsWorld = type { %class.btCollisionWorld.base, void (%class.btDynamicsWorld*, float)*, void (%class.btDynamicsWorld*, float)*, i8*, %struct.btContactSolverInfo }
%class.btCollisionWorld.base = type <{ i32 (...)**, %class.btAlignedObjectArray.32, %class.btDispatcher*, %struct.btDispatcherInfo, %class.btBroadphaseInterface*, %class.btIDebugDraw*, i8 }>
%class.btAlignedObjectArray.32 = type <{ %class.btAlignedAllocator.33, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator.33 = type { i8 }
%struct.btContactSolverInfo = type { %struct.btContactSolverInfoData }
%struct.btContactSolverInfoData = type { float, float, float, float, float, i32, float, float, float, float, float, i32, float, float, float, float, i32, i32, i32, float, float }
%struct.InplaceSolverIslandCallback = type opaque
%class.btSimulationIslandManager = type opaque
%class.btAlignedObjectArray.36 = type <{ %class.btAlignedAllocator.37, [3 x i8], i32, i32, %class.btRigidBody**, i8, [3 x i8] }>
%class.btAlignedAllocator.37 = type { i8 }
%class.btAlignedObjectArray.40 = type <{ %class.btAlignedAllocator.41, [3 x i8], i32, i32, %class.btActionInterface**, i8, [3 x i8] }>
%class.btAlignedAllocator.41 = type { i8 }
%class.btActionInterface = type { i32 (...)** }
%struct.plRigidBodyHandle__ = type { i32 }
%struct.plCollisionShapeHandle__ = type { i32 }
%"struct.btRigidBody::btRigidBodyConstructionInfo" = type { float, %class.btMotionState*, %class.btTransform, %class.btCollisionShape*, %class.btVector3, float, float, float, float, float, float, float, i8, float, float, float, float }
%class.btSphereShape = type { %class.btConvexInternalShape }
%class.btConvexInternalShape = type { %class.btConvexShape, %class.btVector3, %class.btVector3, float, float }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btBoxShape = type { %class.btPolyhedralConvexShape }
%class.btPolyhedralConvexShape = type { %class.btConvexInternalShape, %class.btConvexPolyhedron* }
%class.btConvexPolyhedron = type opaque
%class.btMultiSphereShape = type { %class.btConvexInternalAabbCachingShape.base, [3 x i8], %class.btAlignedObjectArray.44, %class.btAlignedObjectArray.48 }
%class.btConvexInternalAabbCachingShape.base = type <{ %class.btConvexInternalShape, %class.btVector3, %class.btVector3, i8 }>
%class.btAlignedObjectArray.44 = type <{ %class.btAlignedAllocator.45, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator.45 = type { i8 }
%class.btAlignedObjectArray.48 = type <{ %class.btAlignedAllocator.49, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator.49 = type { i8 }
%class.btConeShape = type { %class.btConvexInternalShape, float, float, float, [3 x i32] }
%class.btCylinderShape = type { %class.btConvexInternalShape, i32 }
%class.btConvexHullShape = type { %class.btPolyhedralConvexAabbCachingShape.base, [3 x i8], %class.btAlignedObjectArray.44 }
%class.btPolyhedralConvexAabbCachingShape.base = type <{ %class.btPolyhedralConvexShape, %class.btVector3, %class.btVector3, i8 }>
%struct.plMeshInterfaceHandle__ = type { i32 }
%class.btCompoundShape = type { %class.btCollisionShape, %class.btAlignedObjectArray.52, %class.btVector3, %class.btVector3, %struct.btDbvt*, i32, float, %class.btVector3 }
%class.btAlignedObjectArray.52 = type <{ %class.btAlignedAllocator.53, [3 x i8], i32, i32, %struct.btCompoundShapeChild*, i8, [3 x i8] }>
%class.btAlignedAllocator.53 = type { i8 }
%struct.btCompoundShapeChild = type { %class.btTransform, %class.btCollisionShape*, i32, float, %struct.btDbvtNode* }
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }
%class.btTriangleShape = type { %class.btPolyhedralConvexShape, [3 x %class.btVector3] }
%class.btGjkPairDetector = type { %struct.btDiscreteCollisionDetectorInterface, %class.btVector3, %class.btConvexPenetrationDepthSolver*, %class.btVoronoiSimplexSolver*, %class.btConvexShape*, %class.btConvexShape*, i32, i32, float, float, i8, float, i32, i32, i32, i32, i32 }
%struct.btDiscreteCollisionDetectorInterface = type { i32 (...)** }
%struct.btPointCollector = type <{ %"struct.btDiscreteCollisionDetectorInterface::Result", %class.btVector3, %class.btVector3, float, i8, [3 x i8] }>
%"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput" = type { %class.btTransform, %class.btTransform, float }
%class.btMinkowskiPenetrationDepthSolver = type { %class.btConvexPenetrationDepthSolver }
%class.btSerializer = type { i32 (...)** }
%struct.btConvexInternalShapeData = type { %struct.btCollisionShapeData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, i32 }
%struct.btCollisionShapeData = type { i8*, i32, [4 x i8] }
%struct.btVector3FloatData = type { [4 x float] }

$_ZN12btPhysicsSdkC2Ev = comdat any

$_ZN34btDefaultCollisionConstructionInfoC2Ev = comdat any

$_ZN20btAxisSweep3InternalItEnwEmPv = comdat any

$_ZN35btSequentialImpulseConstraintSolvernwEmPv = comdat any

$_ZN23btDiscreteDynamicsWorldnwEmPv = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN11btTransform11setIdentityEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN11btRigidBody27btRigidBodyConstructionInfoC2EfP13btMotionStateP16btCollisionShapeRK9btVector3 = comdat any

$_ZN17btCollisionObjectnwEmPv = comdat any

$_ZN17btCollisionObject17setWorldTransformERK11btTransform = comdat any

$_ZN17btCollisionObject14setUserPointerEPv = comdat any

$_ZN13btSphereShapenwEmPv = comdat any

$_ZN13btSphereShapeC2Ef = comdat any

$_ZN10btBoxShapenwEmPv = comdat any

$_ZN18btMultiSphereShapenwEmPv = comdat any

$_ZN11btConeShapenwEmPv = comdat any

$_ZN15btCylinderShapenwEmPv = comdat any

$_ZN17btConvexHullShapenwEmPv = comdat any

$_ZN15btCompoundShapenwEmPv = comdat any

$_ZN11btTransform9setOriginERK9btVector3 = comdat any

$_ZN11btTransform11setRotationERK12btQuaternion = comdat any

$_ZN12btQuaternionC2ERKfS1_S1_S1_ = comdat any

$_ZN12btQuaternionC2Ev = comdat any

$_ZN12btQuaternion8setEulerERKfS1_S1_ = comdat any

$_ZNK10btQuadWord4getXEv = comdat any

$_ZNK10btQuadWord4getYEv = comdat any

$_ZNK10btQuadWord4getZEv = comdat any

$_ZNK12btQuaternion4getWEv = comdat any

$_ZN17btCollisionObject17getWorldTransformEv = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZN11btTransform19setFromOpenGLMatrixEPKf = comdat any

$_ZNK11btTransform15getOpenGLMatrixEPf = comdat any

$_ZN11btTransform9getOriginEv = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZNK11btTransform11getRotationEv = comdat any

$_ZN15btTriangleShapeC2ERK9btVector3S2_S2_ = comdat any

$_ZN21btConvexInternalShape9setMarginEf = comdat any

$_ZN22btVoronoiSimplexSolverC2Ev = comdat any

$_ZN30btGjkEpaPenetrationDepthSolverC2Ev = comdat any

$_ZN30btConvexPenetrationDepthSolverD2Ev = comdat any

$_ZN16btPointCollectorC2Ev = comdat any

$_ZN36btDiscreteCollisionDetectorInterface17ClosestPointInputC2Ev = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN36btDiscreteCollisionDetectorInterfaceD2Ev = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN11btMatrix3x311setIdentityEv = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN9btVector34setXEf = comdat any

$_ZN11btMatrix3x311setRotationERK12btQuaternion = comdat any

$_ZNK12btQuaternion7length2Ev = comdat any

$_ZNK10btQuadWord1xEv = comdat any

$_ZNK10btQuadWord1yEv = comdat any

$_ZNK10btQuadWord1zEv = comdat any

$_ZNK10btQuadWord1wEv = comdat any

$_ZNK12btQuaternion3dotERKS_ = comdat any

$_ZN10btQuadWordC2ERKfS1_S1_S1_ = comdat any

$_ZN10btQuadWordC2Ev = comdat any

$_Z5btCosf = comdat any

$_Z5btSinf = comdat any

$_ZN10btQuadWord8setValueERKfS1_S1_S1_ = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZN11btMatrix3x322setFromOpenGLSubMatrixEPKf = comdat any

$_ZNK11btMatrix3x318getOpenGLSubMatrixEPf = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZNK11btMatrix3x311getRotationER12btQuaternion = comdat any

$_Z6btSqrtf = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN15btTriangleShapeD0Ev = comdat any

$_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_ = comdat any

$_ZNK21btConvexInternalShape15getLocalScalingEv = comdat any

$_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3 = comdat any

$_ZNK15btTriangleShape7getNameEv = comdat any

$_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZNK21btConvexInternalShape9getMarginEv = comdat any

$_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv = comdat any

$_ZNK21btConvexInternalShape9serializeEPvP12btSerializer = comdat any

$_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3 = comdat any

$_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i = comdat any

$_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv = comdat any

$_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3 = comdat any

$_ZNK15btTriangleShape14getNumVerticesEv = comdat any

$_ZNK15btTriangleShape11getNumEdgesEv = comdat any

$_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_ = comdat any

$_ZNK15btTriangleShape9getVertexEiR9btVector3 = comdat any

$_ZNK15btTriangleShape12getNumPlanesEv = comdat any

$_ZNK15btTriangleShape8getPlaneER9btVector3S1_i = comdat any

$_ZNK15btTriangleShape8isInsideERK9btVector3f = comdat any

$_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_ = comdat any

$_ZN15btTriangleShapedlEPv = comdat any

$_ZNK9btVector314serializeFloatER18btVector3FloatData = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZNK9btVector37maxAxisEv = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZNK15btTriangleShape10calcNormalER9btVector3 = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZN25btSubSimplexClosestResultC2Ev = comdat any

$_ZN15btUsageBitfieldC2Ev = comdat any

$_ZN15btUsageBitfield5resetEv = comdat any

$_ZN30btConvexPenetrationDepthSolverC2Ev = comdat any

$_ZN30btConvexPenetrationDepthSolverD0Ev = comdat any

$_ZN36btDiscreteCollisionDetectorInterface6ResultC2Ev = comdat any

$_ZN16btPointCollectorD0Ev = comdat any

$_ZN16btPointCollector20setShapeIdentifiersAEii = comdat any

$_ZN16btPointCollector20setShapeIdentifiersBEii = comdat any

$_ZN16btPointCollector15addContactPointERK9btVector3S2_f = comdat any

$_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev = comdat any

$_ZN36btDiscreteCollisionDetectorInterface6ResultD0Ev = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZTV15btTriangleShape = comdat any

$_ZTS15btTriangleShape = comdat any

$_ZTI15btTriangleShape = comdat any

$_ZTV30btConvexPenetrationDepthSolver = comdat any

$_ZTS30btConvexPenetrationDepthSolver = comdat any

$_ZTI30btConvexPenetrationDepthSolver = comdat any

$_ZTV16btPointCollector = comdat any

$_ZTS16btPointCollector = comdat any

$_ZTSN36btDiscreteCollisionDetectorInterface6ResultE = comdat any

$_ZTIN36btDiscreteCollisionDetectorInterface6ResultE = comdat any

$_ZTI16btPointCollector = comdat any

$_ZTVN36btDiscreteCollisionDetectorInterface6ResultE = comdat any

@_ZZ15plNearestPointsE17sGjkSimplexSolver = internal global %class.btVoronoiSimplexSolver zeroinitializer, align 4
@_ZGVZ15plNearestPointsE17sGjkSimplexSolver = internal global i32 0, align 4
@_ZZ15plNearestPointsE7Solver0 = internal global %class.btGjkEpaPenetrationDepthSolver zeroinitializer, align 4
@_ZGVZ15plNearestPointsE7Solver0 = internal global i32 0, align 4
@__dso_handle = external hidden global i8
@_ZZ15plNearestPointsE7Solver1 = internal global { i8** } { i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV33btMinkowskiPenetrationDepthSolver, i32 0, inrange i32 0, i32 2) }, align 4
@_ZTV33btMinkowskiPenetrationDepthSolver = external unnamed_addr constant { [5 x i8*] }, align 4
@_ZGVZ15plNearestPointsE7Solver1 = internal global i32 0, align 4
@_ZTV13btSphereShape = external unnamed_addr constant { [25 x i8*] }, align 4
@_ZTV15btTriangleShape = linkonce_odr hidden unnamed_addr constant { [34 x i8*] } { [34 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI15btTriangleShape to i8*), i8* bitcast (%class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD2Ev to i8*), i8* bitcast (void (%class.btTriangleShape*)* @_ZN15btTriangleShapeD0Ev to i8*), i8* bitcast (void (%class.btTriangleShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btVector3*)* @_ZN21btConvexInternalShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btTriangleShape*, float, %class.btVector3*)* @_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btTriangleShape*)* @_ZNK15btTriangleShape7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, float)* @_ZN21btConvexInternalShape9setMarginEf to i8*), i8* bitcast (float (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btConvexInternalShape*, i8*, %class.btSerializer*)* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btVector3*, %class.btConvexInternalShape*, %class.btVector3*)* @_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btTriangleShape*, %class.btVector3*)* @_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3 to i8*), i8* bitcast (void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, float*, float*)* @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_ to i8*), i8* bitcast (void (%class.btTriangleShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (i32 (%class.btTriangleShape*)* @_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv to i8*), i8* bitcast (void (%class.btTriangleShape*, i32, %class.btVector3*)* @_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3 to i8*), i8* bitcast (i1 (%class.btPolyhedralConvexShape*, i32)* @_ZN23btPolyhedralConvexShape28initializePolyhedralFeaturesEi to i8*), i8* bitcast (i32 (%class.btTriangleShape*)* @_ZNK15btTriangleShape14getNumVerticesEv to i8*), i8* bitcast (i32 (%class.btTriangleShape*)* @_ZNK15btTriangleShape11getNumEdgesEv to i8*), i8* bitcast (void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)* @_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_ to i8*), i8* bitcast (void (%class.btTriangleShape*, i32, %class.btVector3*)* @_ZNK15btTriangleShape9getVertexEiR9btVector3 to i8*), i8* bitcast (i32 (%class.btTriangleShape*)* @_ZNK15btTriangleShape12getNumPlanesEv to i8*), i8* bitcast (void (%class.btTriangleShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK15btTriangleShape8getPlaneER9btVector3S1_i to i8*), i8* bitcast (i1 (%class.btTriangleShape*, %class.btVector3*, float)* @_ZNK15btTriangleShape8isInsideERK9btVector3f to i8*), i8* bitcast (void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)* @_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_ to i8*)] }, comdat, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS15btTriangleShape = linkonce_odr hidden constant [18 x i8] c"15btTriangleShape\00", comdat, align 1
@_ZTI23btPolyhedralConvexShape = external constant i8*
@_ZTI15btTriangleShape = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([18 x i8], [18 x i8]* @_ZTS15btTriangleShape, i32 0, i32 0), i8* bitcast (i8** @_ZTI23btPolyhedralConvexShape to i8*) }, comdat, align 4
@.str = private unnamed_addr constant [9 x i8] c"Triangle\00", align 1
@.str.2 = private unnamed_addr constant [26 x i8] c"btConvexInternalShapeData\00", align 1
@_ZTV30btGjkEpaPenetrationDepthSolver = external unnamed_addr constant { [5 x i8*] }, align 4
@_ZTV30btConvexPenetrationDepthSolver = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI30btConvexPenetrationDepthSolver to i8*), i8* bitcast (%class.btConvexPenetrationDepthSolver* (%class.btConvexPenetrationDepthSolver*)* @_ZN30btConvexPenetrationDepthSolverD2Ev to i8*), i8* bitcast (void (%class.btConvexPenetrationDepthSolver*)* @_ZN30btConvexPenetrationDepthSolverD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS30btConvexPenetrationDepthSolver = linkonce_odr hidden constant [33 x i8] c"30btConvexPenetrationDepthSolver\00", comdat, align 1
@_ZTI30btConvexPenetrationDepthSolver = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([33 x i8], [33 x i8]* @_ZTS30btConvexPenetrationDepthSolver, i32 0, i32 0) }, comdat, align 4
@_ZTV16btPointCollector = linkonce_odr hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI16btPointCollector to i8*), i8* bitcast (%"struct.btDiscreteCollisionDetectorInterface::Result"* (%"struct.btDiscreteCollisionDetectorInterface::Result"*)* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev to i8*), i8* bitcast (void (%struct.btPointCollector*)* @_ZN16btPointCollectorD0Ev to i8*), i8* bitcast (void (%struct.btPointCollector*, i32, i32)* @_ZN16btPointCollector20setShapeIdentifiersAEii to i8*), i8* bitcast (void (%struct.btPointCollector*, i32, i32)* @_ZN16btPointCollector20setShapeIdentifiersBEii to i8*), i8* bitcast (void (%struct.btPointCollector*, %class.btVector3*, %class.btVector3*, float)* @_ZN16btPointCollector15addContactPointERK9btVector3S2_f to i8*)] }, comdat, align 4
@_ZTS16btPointCollector = linkonce_odr hidden constant [19 x i8] c"16btPointCollector\00", comdat, align 1
@_ZTSN36btDiscreteCollisionDetectorInterface6ResultE = linkonce_odr hidden constant [48 x i8] c"N36btDiscreteCollisionDetectorInterface6ResultE\00", comdat, align 1
@_ZTIN36btDiscreteCollisionDetectorInterface6ResultE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([48 x i8], [48 x i8]* @_ZTSN36btDiscreteCollisionDetectorInterface6ResultE, i32 0, i32 0) }, comdat, align 4
@_ZTI16btPointCollector = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([19 x i8], [19 x i8]* @_ZTS16btPointCollector, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTIN36btDiscreteCollisionDetectorInterface6ResultE to i8*) }, comdat, align 4
@_ZTVN36btDiscreteCollisionDetectorInterface6ResultE = linkonce_odr hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTIN36btDiscreteCollisionDetectorInterface6ResultE to i8*), i8* bitcast (%"struct.btDiscreteCollisionDetectorInterface::Result"* (%"struct.btDiscreteCollisionDetectorInterface::Result"*)* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev to i8*), i8* bitcast (void (%"struct.btDiscreteCollisionDetectorInterface::Result"*)* @_ZN36btDiscreteCollisionDetectorInterface6ResultD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4

define hidden %struct.plPhysicsSdkHandle__* @plNewBulletSdk() #0 {
entry:
  %mem = alloca i8*, align 4
  %0 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 32, i32 16)
  store i8* %call, i8** %mem, align 4, !tbaa !2
  %1 = load i8*, i8** %mem, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.btPhysicsSdk*
  %call1 = call %struct.btPhysicsSdk* @_ZN12btPhysicsSdkC2Ev(%struct.btPhysicsSdk* %2)
  %3 = bitcast %struct.btPhysicsSdk* %2 to %struct.plPhysicsSdkHandle__*
  %4 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #6
  ret %struct.plPhysicsSdkHandle__* %3
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #2

define linkonce_odr hidden %struct.btPhysicsSdk* @_ZN12btPhysicsSdkC2Ev(%struct.btPhysicsSdk* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %struct.btPhysicsSdk*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  store %struct.btPhysicsSdk* %this, %struct.btPhysicsSdk** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btPhysicsSdk*, %struct.btPhysicsSdk** %this.addr, align 4
  %m_worldAabbMin = getelementptr inbounds %struct.btPhysicsSdk, %struct.btPhysicsSdk* %this1, i32 0, i32 0
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store float -1.000000e+03, float* %ref.tmp, align 4, !tbaa !6
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store float -1.000000e+03, float* %ref.tmp2, align 4, !tbaa !6
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  store float -1.000000e+03, float* %ref.tmp3, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %m_worldAabbMin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #6
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #6
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  %m_worldAabbMax = getelementptr inbounds %struct.btPhysicsSdk, %struct.btPhysicsSdk* %this1, i32 0, i32 1
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  store float 1.000000e+03, float* %ref.tmp4, align 4, !tbaa !6
  %7 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  store float 1.000000e+03, float* %ref.tmp5, align 4, !tbaa !6
  %8 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  store float 1.000000e+03, float* %ref.tmp6, align 4, !tbaa !6
  %call7 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %m_worldAabbMax, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %9 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #6
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  %11 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  ret %struct.btPhysicsSdk* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

define hidden void @plDeletePhysicsSdk(%struct.plPhysicsSdkHandle__* %physicsSdk) #0 {
entry:
  %physicsSdk.addr = alloca %struct.plPhysicsSdkHandle__*, align 4
  %phys = alloca %struct.btPhysicsSdk*, align 4
  store %struct.plPhysicsSdkHandle__* %physicsSdk, %struct.plPhysicsSdkHandle__** %physicsSdk.addr, align 4, !tbaa !2
  %0 = bitcast %struct.btPhysicsSdk** %phys to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.plPhysicsSdkHandle__*, %struct.plPhysicsSdkHandle__** %physicsSdk.addr, align 4, !tbaa !2
  %2 = bitcast %struct.plPhysicsSdkHandle__* %1 to %struct.btPhysicsSdk*
  store %struct.btPhysicsSdk* %2, %struct.btPhysicsSdk** %phys, align 4, !tbaa !2
  %3 = load %struct.btPhysicsSdk*, %struct.btPhysicsSdk** %phys, align 4, !tbaa !2
  %4 = bitcast %struct.btPhysicsSdk* %3 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %4)
  %5 = bitcast %struct.btPhysicsSdk** %phys to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #2

define hidden %struct.plDynamicsWorldHandle__* @plCreateDynamicsWorld(%struct.plPhysicsSdkHandle__* %physicsSdkHandle) #0 {
entry:
  %physicsSdkHandle.addr = alloca %struct.plPhysicsSdkHandle__*, align 4
  %physicsSdk = alloca %struct.btPhysicsSdk*, align 4
  %mem = alloca i8*, align 4
  %collisionConfiguration = alloca %class.btDefaultCollisionConfiguration*, align 4
  %ref.tmp = alloca %struct.btDefaultCollisionConstructionInfo, align 4
  %dispatcher = alloca %class.btDispatcher*, align 4
  %pairCache = alloca %class.btBroadphaseInterface*, align 4
  %constraintSolver = alloca %class.btConstraintSolver*, align 4
  store %struct.plPhysicsSdkHandle__* %physicsSdkHandle, %struct.plPhysicsSdkHandle__** %physicsSdkHandle.addr, align 4, !tbaa !2
  %0 = bitcast %struct.btPhysicsSdk** %physicsSdk to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.plPhysicsSdkHandle__*, %struct.plPhysicsSdkHandle__** %physicsSdkHandle.addr, align 4, !tbaa !2
  %2 = bitcast %struct.plPhysicsSdkHandle__* %1 to %struct.btPhysicsSdk*
  store %struct.btPhysicsSdk* %2, %struct.btPhysicsSdk** %physicsSdk, align 4, !tbaa !2
  %3 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 92, i32 16)
  store i8* %call, i8** %mem, align 4, !tbaa !2
  %4 = bitcast %class.btDefaultCollisionConfiguration** %collisionConfiguration to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load i8*, i8** %mem, align 4, !tbaa !2
  %6 = bitcast i8* %5 to %class.btDefaultCollisionConfiguration*
  %7 = bitcast %struct.btDefaultCollisionConstructionInfo* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %7) #6
  %call1 = call %struct.btDefaultCollisionConstructionInfo* @_ZN34btDefaultCollisionConstructionInfoC2Ev(%struct.btDefaultCollisionConstructionInfo* %ref.tmp)
  %call2 = call %class.btDefaultCollisionConfiguration* @_ZN31btDefaultCollisionConfigurationC1ERK34btDefaultCollisionConstructionInfo(%class.btDefaultCollisionConfiguration* %6, %struct.btDefaultCollisionConstructionInfo* nonnull align 4 dereferenceable(24) %ref.tmp)
  %8 = bitcast %struct.btDefaultCollisionConstructionInfo* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %8) #6
  store %class.btDefaultCollisionConfiguration* %6, %class.btDefaultCollisionConfiguration** %collisionConfiguration, align 4, !tbaa !2
  %call3 = call i8* @_Z22btAlignedAllocInternalmi(i32 5260, i32 16)
  store i8* %call3, i8** %mem, align 4, !tbaa !2
  %9 = bitcast %class.btDispatcher** %dispatcher to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %10 = load i8*, i8** %mem, align 4, !tbaa !2
  %11 = bitcast i8* %10 to %class.btCollisionDispatcher*
  %12 = load %class.btDefaultCollisionConfiguration*, %class.btDefaultCollisionConfiguration** %collisionConfiguration, align 4, !tbaa !2
  %13 = bitcast %class.btDefaultCollisionConfiguration* %12 to %class.btCollisionConfiguration*
  %call4 = call %class.btCollisionDispatcher* @_ZN21btCollisionDispatcherC1EP24btCollisionConfiguration(%class.btCollisionDispatcher* %11, %class.btCollisionConfiguration* %13)
  %14 = bitcast %class.btCollisionDispatcher* %11 to %class.btDispatcher*
  store %class.btDispatcher* %14, %class.btDispatcher** %dispatcher, align 4, !tbaa !2
  %call5 = call i8* @_Z22btAlignedAllocInternalmi(i32 116, i32 16)
  store i8* %call5, i8** %mem, align 4, !tbaa !2
  %15 = bitcast %class.btBroadphaseInterface** %pairCache to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #6
  %16 = load i8*, i8** %mem, align 4, !tbaa !2
  %call6 = call i8* @_ZN20btAxisSweep3InternalItEnwEmPv(i32 116, i8* %16)
  %17 = bitcast i8* %call6 to %class.btAxisSweep3*
  %18 = load %struct.btPhysicsSdk*, %struct.btPhysicsSdk** %physicsSdk, align 4, !tbaa !2
  %m_worldAabbMin = getelementptr inbounds %struct.btPhysicsSdk, %struct.btPhysicsSdk* %18, i32 0, i32 0
  %19 = load %struct.btPhysicsSdk*, %struct.btPhysicsSdk** %physicsSdk, align 4, !tbaa !2
  %m_worldAabbMax = getelementptr inbounds %struct.btPhysicsSdk, %struct.btPhysicsSdk* %19, i32 0, i32 1
  %call7 = call %class.btAxisSweep3* @_ZN12btAxisSweep3C1ERK9btVector3S2_tP22btOverlappingPairCacheb(%class.btAxisSweep3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %m_worldAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_worldAabbMax, i16 zeroext 16384, %class.btOverlappingPairCache* null, i1 zeroext false)
  %20 = bitcast %class.btAxisSweep3* %17 to %class.btBroadphaseInterface*
  store %class.btBroadphaseInterface* %20, %class.btBroadphaseInterface** %pairCache, align 4, !tbaa !2
  %call8 = call i8* @_Z22btAlignedAllocInternalmi(i32 196, i32 16)
  store i8* %call8, i8** %mem, align 4, !tbaa !2
  %21 = bitcast %class.btConstraintSolver** %constraintSolver to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #6
  %22 = load i8*, i8** %mem, align 4, !tbaa !2
  %call9 = call i8* @_ZN35btSequentialImpulseConstraintSolvernwEmPv(i32 196, i8* %22)
  %23 = bitcast i8* %call9 to %class.btSequentialImpulseConstraintSolver*
  %call10 = call %class.btSequentialImpulseConstraintSolver* @_ZN35btSequentialImpulseConstraintSolverC1Ev(%class.btSequentialImpulseConstraintSolver* %23)
  %24 = bitcast %class.btSequentialImpulseConstraintSolver* %23 to %class.btConstraintSolver*
  store %class.btConstraintSolver* %24, %class.btConstraintSolver** %constraintSolver, align 4, !tbaa !2
  %call11 = call i8* @_Z22btAlignedAllocInternalmi(i32 324, i32 16)
  store i8* %call11, i8** %mem, align 4, !tbaa !2
  %25 = load i8*, i8** %mem, align 4, !tbaa !2
  %call12 = call i8* @_ZN23btDiscreteDynamicsWorldnwEmPv(i32 324, i8* %25)
  %26 = bitcast i8* %call12 to %class.btDiscreteDynamicsWorld*
  %27 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher, align 4, !tbaa !2
  %28 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %pairCache, align 4, !tbaa !2
  %29 = load %class.btConstraintSolver*, %class.btConstraintSolver** %constraintSolver, align 4, !tbaa !2
  %30 = load %class.btDefaultCollisionConfiguration*, %class.btDefaultCollisionConfiguration** %collisionConfiguration, align 4, !tbaa !2
  %31 = bitcast %class.btDefaultCollisionConfiguration* %30 to %class.btCollisionConfiguration*
  %call13 = call %class.btDiscreteDynamicsWorld* @_ZN23btDiscreteDynamicsWorldC1EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration(%class.btDiscreteDynamicsWorld* %26, %class.btDispatcher* %27, %class.btBroadphaseInterface* %28, %class.btConstraintSolver* %29, %class.btCollisionConfiguration* %31)
  %32 = bitcast %class.btDiscreteDynamicsWorld* %26 to %struct.plDynamicsWorldHandle__*
  %33 = bitcast %class.btConstraintSolver** %constraintSolver to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #6
  %34 = bitcast %class.btBroadphaseInterface** %pairCache to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #6
  %35 = bitcast %class.btDispatcher** %dispatcher to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #6
  %36 = bitcast %class.btDefaultCollisionConfiguration** %collisionConfiguration to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #6
  %37 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #6
  %38 = bitcast %struct.btPhysicsSdk** %physicsSdk to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #6
  ret %struct.plDynamicsWorldHandle__* %32
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btDefaultCollisionConstructionInfo* @_ZN34btDefaultCollisionConstructionInfoC2Ev(%struct.btDefaultCollisionConstructionInfo* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %struct.btDefaultCollisionConstructionInfo*, align 4
  store %struct.btDefaultCollisionConstructionInfo* %this, %struct.btDefaultCollisionConstructionInfo** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btDefaultCollisionConstructionInfo*, %struct.btDefaultCollisionConstructionInfo** %this.addr, align 4
  %m_persistentManifoldPool = getelementptr inbounds %struct.btDefaultCollisionConstructionInfo, %struct.btDefaultCollisionConstructionInfo* %this1, i32 0, i32 0
  store %class.btPoolAllocator* null, %class.btPoolAllocator** %m_persistentManifoldPool, align 4, !tbaa !8
  %m_collisionAlgorithmPool = getelementptr inbounds %struct.btDefaultCollisionConstructionInfo, %struct.btDefaultCollisionConstructionInfo* %this1, i32 0, i32 1
  store %class.btPoolAllocator* null, %class.btPoolAllocator** %m_collisionAlgorithmPool, align 4, !tbaa !11
  %m_defaultMaxPersistentManifoldPoolSize = getelementptr inbounds %struct.btDefaultCollisionConstructionInfo, %struct.btDefaultCollisionConstructionInfo* %this1, i32 0, i32 2
  store i32 4096, i32* %m_defaultMaxPersistentManifoldPoolSize, align 4, !tbaa !12
  %m_defaultMaxCollisionAlgorithmPoolSize = getelementptr inbounds %struct.btDefaultCollisionConstructionInfo, %struct.btDefaultCollisionConstructionInfo* %this1, i32 0, i32 3
  store i32 4096, i32* %m_defaultMaxCollisionAlgorithmPoolSize, align 4, !tbaa !13
  %m_customCollisionAlgorithmMaxElementSize = getelementptr inbounds %struct.btDefaultCollisionConstructionInfo, %struct.btDefaultCollisionConstructionInfo* %this1, i32 0, i32 4
  store i32 0, i32* %m_customCollisionAlgorithmMaxElementSize, align 4, !tbaa !14
  %m_useEpaPenetrationAlgorithm = getelementptr inbounds %struct.btDefaultCollisionConstructionInfo, %struct.btDefaultCollisionConstructionInfo* %this1, i32 0, i32 5
  store i32 1, i32* %m_useEpaPenetrationAlgorithm, align 4, !tbaa !15
  ret %struct.btDefaultCollisionConstructionInfo* %this1
}

declare %class.btDefaultCollisionConfiguration* @_ZN31btDefaultCollisionConfigurationC1ERK34btDefaultCollisionConstructionInfo(%class.btDefaultCollisionConfiguration* returned, %struct.btDefaultCollisionConstructionInfo* nonnull align 4 dereferenceable(24)) unnamed_addr #2

declare %class.btCollisionDispatcher* @_ZN21btCollisionDispatcherC1EP24btCollisionConfiguration(%class.btCollisionDispatcher* returned, %class.btCollisionConfiguration*) unnamed_addr #2

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN20btAxisSweep3InternalItEnwEmPv(i32 %0, i8* %ptr) #4 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !16
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

declare %class.btAxisSweep3* @_ZN12btAxisSweep3C1ERK9btVector3S2_tP22btOverlappingPairCacheb(%class.btAxisSweep3* returned, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), i16 zeroext, %class.btOverlappingPairCache*, i1 zeroext) unnamed_addr #2

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN35btSequentialImpulseConstraintSolvernwEmPv(i32 %0, i8* %ptr) #4 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !16
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

declare %class.btSequentialImpulseConstraintSolver* @_ZN35btSequentialImpulseConstraintSolverC1Ev(%class.btSequentialImpulseConstraintSolver* returned) unnamed_addr #2

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN23btDiscreteDynamicsWorldnwEmPv(i32 %0, i8* %ptr) #4 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !16
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

declare %class.btDiscreteDynamicsWorld* @_ZN23btDiscreteDynamicsWorldC1EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration(%class.btDiscreteDynamicsWorld* returned, %class.btDispatcher*, %class.btBroadphaseInterface*, %class.btConstraintSolver*, %class.btCollisionConfiguration*) unnamed_addr #2

define hidden void @plDeleteDynamicsWorld(%struct.plDynamicsWorldHandle__* %world) #0 {
entry:
  %world.addr = alloca %struct.plDynamicsWorldHandle__*, align 4
  %dynamicsWorld = alloca %class.btDynamicsWorld*, align 4
  store %struct.plDynamicsWorldHandle__* %world, %struct.plDynamicsWorldHandle__** %world.addr, align 4, !tbaa !2
  %0 = bitcast %class.btDynamicsWorld** %dynamicsWorld to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.plDynamicsWorldHandle__*, %struct.plDynamicsWorldHandle__** %world.addr, align 4, !tbaa !2
  %2 = bitcast %struct.plDynamicsWorldHandle__* %1 to %class.btDynamicsWorld*
  store %class.btDynamicsWorld* %2, %class.btDynamicsWorld** %dynamicsWorld, align 4, !tbaa !2
  %3 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %dynamicsWorld, align 4, !tbaa !2
  %4 = bitcast %class.btDynamicsWorld* %3 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %4)
  %5 = bitcast %class.btDynamicsWorld** %dynamicsWorld to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  ret void
}

define hidden void @plStepSimulation(%struct.plDynamicsWorldHandle__* %world, float %timeStep) #0 {
entry:
  %world.addr = alloca %struct.plDynamicsWorldHandle__*, align 4
  %timeStep.addr = alloca float, align 4
  %dynamicsWorld = alloca %class.btDynamicsWorld*, align 4
  store %struct.plDynamicsWorldHandle__* %world, %struct.plDynamicsWorldHandle__** %world.addr, align 4, !tbaa !2
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !6
  %0 = bitcast %class.btDynamicsWorld** %dynamicsWorld to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.plDynamicsWorldHandle__*, %struct.plDynamicsWorldHandle__** %world.addr, align 4, !tbaa !2
  %2 = bitcast %struct.plDynamicsWorldHandle__* %1 to %class.btDynamicsWorld*
  store %class.btDynamicsWorld* %2, %class.btDynamicsWorld** %dynamicsWorld, align 4, !tbaa !2
  %3 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %dynamicsWorld, align 4, !tbaa !2
  %4 = load float, float* %timeStep.addr, align 4, !tbaa !6
  %5 = bitcast %class.btDynamicsWorld* %3 to i32 (%class.btDynamicsWorld*, float, i32, float)***
  %vtable = load i32 (%class.btDynamicsWorld*, float, i32, float)**, i32 (%class.btDynamicsWorld*, float, i32, float)*** %5, align 4, !tbaa !18
  %vfn = getelementptr inbounds i32 (%class.btDynamicsWorld*, float, i32, float)*, i32 (%class.btDynamicsWorld*, float, i32, float)** %vtable, i64 13
  %6 = load i32 (%class.btDynamicsWorld*, float, i32, float)*, i32 (%class.btDynamicsWorld*, float, i32, float)** %vfn, align 4
  %call = call i32 %6(%class.btDynamicsWorld* %3, float %4, i32 1, float 0x3F91111120000000)
  %7 = bitcast %class.btDynamicsWorld** %dynamicsWorld to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  ret void
}

define hidden void @plAddRigidBody(%struct.plDynamicsWorldHandle__* %world, %struct.plRigidBodyHandle__* %object) #0 {
entry:
  %world.addr = alloca %struct.plDynamicsWorldHandle__*, align 4
  %object.addr = alloca %struct.plRigidBodyHandle__*, align 4
  %dynamicsWorld = alloca %class.btDynamicsWorld*, align 4
  %body = alloca %class.btRigidBody*, align 4
  store %struct.plDynamicsWorldHandle__* %world, %struct.plDynamicsWorldHandle__** %world.addr, align 4, !tbaa !2
  store %struct.plRigidBodyHandle__* %object, %struct.plRigidBodyHandle__** %object.addr, align 4, !tbaa !2
  %0 = bitcast %class.btDynamicsWorld** %dynamicsWorld to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.plDynamicsWorldHandle__*, %struct.plDynamicsWorldHandle__** %world.addr, align 4, !tbaa !2
  %2 = bitcast %struct.plDynamicsWorldHandle__* %1 to %class.btDynamicsWorld*
  store %class.btDynamicsWorld* %2, %class.btDynamicsWorld** %dynamicsWorld, align 4, !tbaa !2
  %3 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %struct.plRigidBodyHandle__*, %struct.plRigidBodyHandle__** %object.addr, align 4, !tbaa !2
  %5 = bitcast %struct.plRigidBodyHandle__* %4 to %class.btRigidBody*
  store %class.btRigidBody* %5, %class.btRigidBody** %body, align 4, !tbaa !2
  %6 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %dynamicsWorld, align 4, !tbaa !2
  %7 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %8 = bitcast %class.btDynamicsWorld* %6 to void (%class.btDynamicsWorld*, %class.btRigidBody*)***
  %vtable = load void (%class.btDynamicsWorld*, %class.btRigidBody*)**, void (%class.btDynamicsWorld*, %class.btRigidBody*)*** %8, align 4, !tbaa !18
  %vfn = getelementptr inbounds void (%class.btDynamicsWorld*, %class.btRigidBody*)*, void (%class.btDynamicsWorld*, %class.btRigidBody*)** %vtable, i64 21
  %9 = load void (%class.btDynamicsWorld*, %class.btRigidBody*)*, void (%class.btDynamicsWorld*, %class.btRigidBody*)** %vfn, align 4
  call void %9(%class.btDynamicsWorld* %6, %class.btRigidBody* %7)
  %10 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  %11 = bitcast %class.btDynamicsWorld** %dynamicsWorld to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  ret void
}

define hidden void @plRemoveRigidBody(%struct.plDynamicsWorldHandle__* %world, %struct.plRigidBodyHandle__* %object) #0 {
entry:
  %world.addr = alloca %struct.plDynamicsWorldHandle__*, align 4
  %object.addr = alloca %struct.plRigidBodyHandle__*, align 4
  %dynamicsWorld = alloca %class.btDynamicsWorld*, align 4
  %body = alloca %class.btRigidBody*, align 4
  store %struct.plDynamicsWorldHandle__* %world, %struct.plDynamicsWorldHandle__** %world.addr, align 4, !tbaa !2
  store %struct.plRigidBodyHandle__* %object, %struct.plRigidBodyHandle__** %object.addr, align 4, !tbaa !2
  %0 = bitcast %class.btDynamicsWorld** %dynamicsWorld to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.plDynamicsWorldHandle__*, %struct.plDynamicsWorldHandle__** %world.addr, align 4, !tbaa !2
  %2 = bitcast %struct.plDynamicsWorldHandle__* %1 to %class.btDynamicsWorld*
  store %class.btDynamicsWorld* %2, %class.btDynamicsWorld** %dynamicsWorld, align 4, !tbaa !2
  %3 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %struct.plRigidBodyHandle__*, %struct.plRigidBodyHandle__** %object.addr, align 4, !tbaa !2
  %5 = bitcast %struct.plRigidBodyHandle__* %4 to %class.btRigidBody*
  store %class.btRigidBody* %5, %class.btRigidBody** %body, align 4, !tbaa !2
  %6 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %dynamicsWorld, align 4, !tbaa !2
  %7 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %8 = bitcast %class.btDynamicsWorld* %6 to void (%class.btDynamicsWorld*, %class.btRigidBody*)***
  %vtable = load void (%class.btDynamicsWorld*, %class.btRigidBody*)**, void (%class.btDynamicsWorld*, %class.btRigidBody*)*** %8, align 4, !tbaa !18
  %vfn = getelementptr inbounds void (%class.btDynamicsWorld*, %class.btRigidBody*)*, void (%class.btDynamicsWorld*, %class.btRigidBody*)** %vtable, i64 23
  %9 = load void (%class.btDynamicsWorld*, %class.btRigidBody*)*, void (%class.btDynamicsWorld*, %class.btRigidBody*)** %vfn, align 4
  call void %9(%class.btDynamicsWorld* %6, %class.btRigidBody* %7)
  %10 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  %11 = bitcast %class.btDynamicsWorld** %dynamicsWorld to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  ret void
}

define hidden %struct.plRigidBodyHandle__* @plCreateRigidBody(i8* %user_data, float %mass, %struct.plCollisionShapeHandle__* %cshape) #0 {
entry:
  %user_data.addr = alloca i8*, align 4
  %mass.addr = alloca float, align 4
  %cshape.addr = alloca %struct.plCollisionShapeHandle__*, align 4
  %trans = alloca %class.btTransform, align 4
  %localInertia = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %shape = alloca %class.btCollisionShape*, align 4
  %mem = alloca i8*, align 4
  %rbci = alloca %"struct.btRigidBody::btRigidBodyConstructionInfo", align 4
  %body = alloca %class.btRigidBody*, align 4
  store i8* %user_data, i8** %user_data.addr, align 4, !tbaa !2
  store float %mass, float* %mass.addr, align 4, !tbaa !6
  store %struct.plCollisionShapeHandle__* %cshape, %struct.plCollisionShapeHandle__** %cshape.addr, align 4, !tbaa !2
  %0 = bitcast %class.btTransform* %trans to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %0) #6
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %trans)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %trans)
  %1 = bitcast %class.btVector3* %localInertia to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #6
  %2 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !6
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  store float 0.000000e+00, float* %ref.tmp1, align 4, !tbaa !6
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !6
  %call3 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %localInertia, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  %6 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #6
  %7 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  %8 = bitcast %class.btCollisionShape** %shape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  %9 = load %struct.plCollisionShapeHandle__*, %struct.plCollisionShapeHandle__** %cshape.addr, align 4, !tbaa !2
  %10 = bitcast %struct.plCollisionShapeHandle__* %9 to %class.btCollisionShape*
  store %class.btCollisionShape* %10, %class.btCollisionShape** %shape, align 4, !tbaa !2
  %11 = load float, float* %mass.addr, align 4, !tbaa !6
  %tobool = fcmp une float %11, 0.000000e+00
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %12 = load %class.btCollisionShape*, %class.btCollisionShape** %shape, align 4, !tbaa !2
  %13 = load float, float* %mass.addr, align 4, !tbaa !6
  %14 = bitcast %class.btCollisionShape* %12 to void (%class.btCollisionShape*, float, %class.btVector3*)***
  %vtable = load void (%class.btCollisionShape*, float, %class.btVector3*)**, void (%class.btCollisionShape*, float, %class.btVector3*)*** %14, align 4, !tbaa !18
  %vfn = getelementptr inbounds void (%class.btCollisionShape*, float, %class.btVector3*)*, void (%class.btCollisionShape*, float, %class.btVector3*)** %vtable, i64 8
  %15 = load void (%class.btCollisionShape*, float, %class.btVector3*)*, void (%class.btCollisionShape*, float, %class.btVector3*)** %vfn, align 4
  call void %15(%class.btCollisionShape* %12, float %13, %class.btVector3* nonnull align 4 dereferenceable(16) %localInertia)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %16 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  %call4 = call i8* @_Z22btAlignedAllocInternalmi(i32 616, i32 16)
  store i8* %call4, i8** %mem, align 4, !tbaa !2
  %17 = bitcast %"struct.btRigidBody::btRigidBodyConstructionInfo"* %rbci to i8*
  call void @llvm.lifetime.start.p0i8(i64 140, i8* %17) #6
  %18 = load float, float* %mass.addr, align 4, !tbaa !6
  %19 = load %class.btCollisionShape*, %class.btCollisionShape** %shape, align 4, !tbaa !2
  %call5 = call %"struct.btRigidBody::btRigidBodyConstructionInfo"* @_ZN11btRigidBody27btRigidBodyConstructionInfoC2EfP13btMotionStateP16btCollisionShapeRK9btVector3(%"struct.btRigidBody::btRigidBodyConstructionInfo"* %rbci, float %18, %class.btMotionState* null, %class.btCollisionShape* %19, %class.btVector3* nonnull align 4 dereferenceable(16) %localInertia)
  %20 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #6
  %21 = load i8*, i8** %mem, align 4, !tbaa !2
  %call6 = call i8* @_ZN17btCollisionObjectnwEmPv(i32 616, i8* %21)
  %22 = bitcast i8* %call6 to %class.btRigidBody*
  %call7 = call %class.btRigidBody* @_ZN11btRigidBodyC1ERKNS_27btRigidBodyConstructionInfoE(%class.btRigidBody* %22, %"struct.btRigidBody::btRigidBodyConstructionInfo"* nonnull align 4 dereferenceable(140) %rbci)
  store %class.btRigidBody* %22, %class.btRigidBody** %body, align 4, !tbaa !2
  %23 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %24 = bitcast %class.btRigidBody* %23 to %class.btCollisionObject*
  call void @_ZN17btCollisionObject17setWorldTransformERK11btTransform(%class.btCollisionObject* %24, %class.btTransform* nonnull align 4 dereferenceable(64) %trans)
  %25 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %26 = bitcast %class.btRigidBody* %25 to %class.btCollisionObject*
  %27 = load i8*, i8** %user_data.addr, align 4, !tbaa !2
  call void @_ZN17btCollisionObject14setUserPointerEPv(%class.btCollisionObject* %26, i8* %27)
  %28 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %29 = bitcast %class.btRigidBody* %28 to %struct.plRigidBodyHandle__*
  %30 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #6
  %31 = bitcast %"struct.btRigidBody::btRigidBodyConstructionInfo"* %rbci to i8*
  call void @llvm.lifetime.end.p0i8(i64 140, i8* %31) #6
  %32 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #6
  %33 = bitcast %class.btCollisionShape** %shape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #6
  %34 = bitcast %class.btVector3* %localInertia to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %34) #6
  %35 = bitcast %class.btTransform* %trans to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %35) #6
  ret %struct.plRigidBodyHandle__* %29
}

define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

define linkonce_odr hidden void @_ZN11btTransform11setIdentityEv(%class.btTransform* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !6
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !6
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !6
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_origin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #6
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #6
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !6
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !6
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !6
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !6
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !6
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !6
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !6
  ret %class.btVector3* %this1
}

define linkonce_odr hidden %"struct.btRigidBody::btRigidBodyConstructionInfo"* @_ZN11btRigidBody27btRigidBodyConstructionInfoC2EfP13btMotionStateP16btCollisionShapeRK9btVector3(%"struct.btRigidBody::btRigidBodyConstructionInfo"* returned %this, float %mass, %class.btMotionState* %motionState, %class.btCollisionShape* %collisionShape, %class.btVector3* nonnull align 4 dereferenceable(16) %localInertia) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.btRigidBody::btRigidBodyConstructionInfo"*, align 4
  %mass.addr = alloca float, align 4
  %motionState.addr = alloca %class.btMotionState*, align 4
  %collisionShape.addr = alloca %class.btCollisionShape*, align 4
  %localInertia.addr = alloca %class.btVector3*, align 4
  store %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %this.addr, align 4, !tbaa !2
  store float %mass, float* %mass.addr, align 4, !tbaa !6
  store %class.btMotionState* %motionState, %class.btMotionState** %motionState.addr, align 4, !tbaa !2
  store %class.btCollisionShape* %collisionShape, %class.btCollisionShape** %collisionShape.addr, align 4, !tbaa !2
  store %class.btVector3* %localInertia, %class.btVector3** %localInertia.addr, align 4, !tbaa !2
  %this1 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %this.addr, align 4
  %m_mass = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 0
  %0 = load float, float* %mass.addr, align 4, !tbaa !6
  store float %0, float* %m_mass, align 4, !tbaa !20
  %m_motionState = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 1
  %1 = load %class.btMotionState*, %class.btMotionState** %motionState.addr, align 4, !tbaa !2
  store %class.btMotionState* %1, %class.btMotionState** %m_motionState, align 4, !tbaa !26
  %m_startWorldTransform = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 2
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_startWorldTransform)
  %m_collisionShape = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 3
  %2 = load %class.btCollisionShape*, %class.btCollisionShape** %collisionShape.addr, align 4, !tbaa !2
  store %class.btCollisionShape* %2, %class.btCollisionShape** %m_collisionShape, align 4, !tbaa !27
  %m_localInertia = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 4
  %3 = load %class.btVector3*, %class.btVector3** %localInertia.addr, align 4, !tbaa !2
  %4 = bitcast %class.btVector3* %m_localInertia to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !28
  %m_linearDamping = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 5
  store float 0.000000e+00, float* %m_linearDamping, align 4, !tbaa !30
  %m_angularDamping = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 6
  store float 0.000000e+00, float* %m_angularDamping, align 4, !tbaa !31
  %m_friction = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 7
  store float 5.000000e-01, float* %m_friction, align 4, !tbaa !32
  %m_rollingFriction = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 8
  store float 0.000000e+00, float* %m_rollingFriction, align 4, !tbaa !33
  %m_restitution = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 9
  store float 0.000000e+00, float* %m_restitution, align 4, !tbaa !34
  %m_linearSleepingThreshold = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 10
  store float 0x3FE99999A0000000, float* %m_linearSleepingThreshold, align 4, !tbaa !35
  %m_angularSleepingThreshold = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 11
  store float 1.000000e+00, float* %m_angularSleepingThreshold, align 4, !tbaa !36
  %m_additionalDamping = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 12
  store i8 0, i8* %m_additionalDamping, align 4, !tbaa !37
  %m_additionalDampingFactor = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 13
  store float 0x3F747AE140000000, float* %m_additionalDampingFactor, align 4, !tbaa !38
  %m_additionalLinearDampingThresholdSqr = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 14
  store float 0x3F847AE140000000, float* %m_additionalLinearDampingThresholdSqr, align 4, !tbaa !39
  %m_additionalAngularDampingThresholdSqr = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 15
  store float 0x3F847AE140000000, float* %m_additionalAngularDampingThresholdSqr, align 4, !tbaa !40
  %m_additionalAngularDampingFactor = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 16
  store float 0x3F847AE140000000, float* %m_additionalAngularDampingFactor, align 4, !tbaa !41
  %m_startWorldTransform2 = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 2
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %m_startWorldTransform2)
  ret %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN17btCollisionObjectnwEmPv(i32 %0, i8* %ptr) #4 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !16
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

declare %class.btRigidBody* @_ZN11btRigidBodyC1ERKNS_27btRigidBodyConstructionInfoE(%class.btRigidBody* returned, %"struct.btRigidBody::btRigidBodyConstructionInfo"* nonnull align 4 dereferenceable(140)) unnamed_addr #2

define linkonce_odr hidden void @_ZN17btCollisionObject17setWorldTransformERK11btTransform(%class.btCollisionObject* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %worldTrans) #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %worldTrans.addr = alloca %class.btTransform*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %worldTrans, %class.btTransform** %worldTrans.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_updateRevision = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 26
  %0 = load i32, i32* %m_updateRevision, align 4, !tbaa !42
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %m_updateRevision, align 4, !tbaa !42
  %1 = load %class.btTransform*, %class.btTransform** %worldTrans.addr, align 4, !tbaa !2
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_worldTransform, %class.btTransform* nonnull align 4 dereferenceable(64) %1)
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN17btCollisionObject14setUserPointerEPv(%class.btCollisionObject* %this, i8* %userPointer) #3 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %userPointer.addr = alloca i8*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  store i8* %userPointer, i8** %userPointer.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %0 = load i8*, i8** %userPointer.addr, align 4, !tbaa !2
  %1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 21
  %m_userObjectPointer = bitcast %union.anon* %1 to i8**
  store i8* %0, i8** %m_userObjectPointer, align 4, !tbaa !29
  ret void
}

define hidden void @plDeleteRigidBody(%struct.plRigidBodyHandle__* %cbody) #0 {
entry:
  %cbody.addr = alloca %struct.plRigidBodyHandle__*, align 4
  %body = alloca %class.btRigidBody*, align 4
  store %struct.plRigidBodyHandle__* %cbody, %struct.plRigidBodyHandle__** %cbody.addr, align 4, !tbaa !2
  %0 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.plRigidBodyHandle__*, %struct.plRigidBodyHandle__** %cbody.addr, align 4, !tbaa !2
  %2 = bitcast %struct.plRigidBodyHandle__* %1 to %class.btRigidBody*
  store %class.btRigidBody* %2, %class.btRigidBody** %body, align 4, !tbaa !2
  %3 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %4 = bitcast %class.btRigidBody* %3 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %4)
  %5 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  ret void
}

define hidden %struct.plCollisionShapeHandle__* @plNewSphereShape(float %radius) #0 {
entry:
  %radius.addr = alloca float, align 4
  %mem = alloca i8*, align 4
  store float %radius, float* %radius.addr, align 4, !tbaa !6
  %0 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 52, i32 16)
  store i8* %call, i8** %mem, align 4, !tbaa !2
  %1 = load i8*, i8** %mem, align 4, !tbaa !2
  %call1 = call i8* @_ZN13btSphereShapenwEmPv(i32 52, i8* %1)
  %2 = bitcast i8* %call1 to %class.btSphereShape*
  %3 = load float, float* %radius.addr, align 4, !tbaa !6
  %call2 = call %class.btSphereShape* @_ZN13btSphereShapeC2Ef(%class.btSphereShape* %2, float %3)
  %4 = bitcast %class.btSphereShape* %2 to %struct.plCollisionShapeHandle__*
  %5 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  ret %struct.plCollisionShapeHandle__* %4
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN13btSphereShapenwEmPv(i32 %0, i8* %ptr) #4 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !16
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

define linkonce_odr hidden %class.btSphereShape* @_ZN13btSphereShapeC2Ef(%class.btSphereShape* returned %this, float %radius) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btSphereShape*, align 4
  %radius.addr = alloca float, align 4
  store %class.btSphereShape* %this, %class.btSphereShape** %this.addr, align 4, !tbaa !2
  store float %radius, float* %radius.addr, align 4, !tbaa !6
  %this1 = load %class.btSphereShape*, %class.btSphereShape** %this.addr, align 4
  %0 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %call = call %class.btConvexInternalShape* @_ZN21btConvexInternalShapeC2Ev(%class.btConvexInternalShape* %0)
  %1 = bitcast %class.btSphereShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [25 x i8*] }, { [25 x i8*] }* @_ZTV13btSphereShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !18
  %2 = bitcast %class.btSphereShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %2, i32 0, i32 1
  store i32 8, i32* %m_shapeType, align 4, !tbaa !44
  %3 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %3, i32 0, i32 2
  %4 = load float, float* %radius.addr, align 4, !tbaa !6
  call void @_ZN9btVector34setXEf(%class.btVector3* %m_implicitShapeDimensions, float %4)
  %5 = load float, float* %radius.addr, align 4, !tbaa !6
  %6 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %6, i32 0, i32 3
  store float %5, float* %m_collisionMargin, align 4, !tbaa !46
  ret %class.btSphereShape* %this1
}

define hidden %struct.plCollisionShapeHandle__* @plNewBoxShape(float %x, float %y, float %z) #0 {
entry:
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  %z.addr = alloca float, align 4
  %mem = alloca i8*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store float %x, float* %x.addr, align 4, !tbaa !6
  store float %y, float* %y.addr, align 4, !tbaa !6
  store float %z, float* %z.addr, align 4, !tbaa !6
  %0 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 56, i32 16)
  store i8* %call, i8** %mem, align 4, !tbaa !2
  %1 = load i8*, i8** %mem, align 4, !tbaa !2
  %call1 = call i8* @_ZN10btBoxShapenwEmPv(i32 56, i8* %1)
  %2 = bitcast i8* %call1 to %class.btBoxShape*
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #6
  %call2 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %x.addr, float* nonnull align 4 dereferenceable(4) %y.addr, float* nonnull align 4 dereferenceable(4) %z.addr)
  %call3 = call %class.btBoxShape* @_ZN10btBoxShapeC1ERK9btVector3(%class.btBoxShape* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %4 = bitcast %class.btBoxShape* %2 to %struct.plCollisionShapeHandle__*
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #6
  %6 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #6
  ret %struct.plCollisionShapeHandle__* %4
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN10btBoxShapenwEmPv(i32 %0, i8* %ptr) #4 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !16
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

declare %class.btBoxShape* @_ZN10btBoxShapeC1ERK9btVector3(%class.btBoxShape* returned, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #2

define hidden %struct.plCollisionShapeHandle__* @plNewCapsuleShape(float %radius, float %height) #0 {
entry:
  %radius.addr = alloca float, align 4
  %height.addr = alloca float, align 4
  %numSpheres = alloca i32, align 4
  %positions = alloca [2 x %class.btVector3], align 16
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %radi = alloca [2 x float], align 4
  %mem = alloca i8*, align 4
  store float %radius, float* %radius.addr, align 4, !tbaa !6
  store float %height, float* %height.addr, align 4, !tbaa !6
  %0 = bitcast i32* %numSpheres to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 2, i32* %numSpheres, align 4, !tbaa !48
  %1 = bitcast [2 x %class.btVector3]* %positions to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %1) #6
  %arrayinit.begin = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %positions, i32 0, i32 0
  %2 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !6
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  store float 0.000000e+00, float* %ref.tmp1, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.begin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %height.addr, float* nonnull align 4 dereferenceable(4) %ref.tmp1)
  %arrayinit.element = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.begin, i32 1
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load float, float* %height.addr, align 4, !tbaa !6
  %fneg = fneg float %6
  store float %fneg, float* %ref.tmp3, align 4, !tbaa !6
  %7 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !6
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %8 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #6
  %9 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #6
  %10 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  %11 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  %12 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #6
  %13 = bitcast [2 x float]* %radi to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %13) #6
  %arrayinit.begin6 = getelementptr inbounds [2 x float], [2 x float]* %radi, i32 0, i32 0
  %14 = load float, float* %radius.addr, align 4, !tbaa !6
  store float %14, float* %arrayinit.begin6, align 4, !tbaa !6
  %arrayinit.element7 = getelementptr inbounds float, float* %arrayinit.begin6, i32 1
  %15 = load float, float* %radius.addr, align 4, !tbaa !6
  store float %15, float* %arrayinit.element7, align 4, !tbaa !6
  %16 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  %call8 = call i8* @_Z22btAlignedAllocInternalmi(i32 128, i32 16)
  store i8* %call8, i8** %mem, align 4, !tbaa !2
  %17 = load i8*, i8** %mem, align 4, !tbaa !2
  %call9 = call i8* @_ZN18btMultiSphereShapenwEmPv(i32 128, i8* %17)
  %18 = bitcast i8* %call9 to %class.btMultiSphereShape*
  %arraydecay = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %positions, i32 0, i32 0
  %arraydecay10 = getelementptr inbounds [2 x float], [2 x float]* %radi, i32 0, i32 0
  %call11 = call %class.btMultiSphereShape* @_ZN18btMultiSphereShapeC1EPK9btVector3PKfi(%class.btMultiSphereShape* %18, %class.btVector3* %arraydecay, float* %arraydecay10, i32 2)
  %19 = bitcast %class.btMultiSphereShape* %18 to %struct.plCollisionShapeHandle__*
  %20 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #6
  %21 = bitcast [2 x float]* %radi to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %21) #6
  %22 = bitcast [2 x %class.btVector3]* %positions to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %22) #6
  %23 = bitcast i32* %numSpheres to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #6
  ret %struct.plCollisionShapeHandle__* %19
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN18btMultiSphereShapenwEmPv(i32 %0, i8* %ptr) #4 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !16
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

declare %class.btMultiSphereShape* @_ZN18btMultiSphereShapeC1EPK9btVector3PKfi(%class.btMultiSphereShape* returned, %class.btVector3*, float*, i32) unnamed_addr #2

define hidden %struct.plCollisionShapeHandle__* @plNewConeShape(float %radius, float %height) #0 {
entry:
  %radius.addr = alloca float, align 4
  %height.addr = alloca float, align 4
  %mem = alloca i8*, align 4
  store float %radius, float* %radius.addr, align 4, !tbaa !6
  store float %height, float* %height.addr, align 4, !tbaa !6
  %0 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 76, i32 16)
  store i8* %call, i8** %mem, align 4, !tbaa !2
  %1 = load i8*, i8** %mem, align 4, !tbaa !2
  %call1 = call i8* @_ZN11btConeShapenwEmPv(i32 76, i8* %1)
  %2 = bitcast i8* %call1 to %class.btConeShape*
  %3 = load float, float* %radius.addr, align 4, !tbaa !6
  %4 = load float, float* %height.addr, align 4, !tbaa !6
  %call2 = call %class.btConeShape* @_ZN11btConeShapeC1Eff(%class.btConeShape* %2, float %3, float %4)
  %5 = bitcast %class.btConeShape* %2 to %struct.plCollisionShapeHandle__*
  %6 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #6
  ret %struct.plCollisionShapeHandle__* %5
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN11btConeShapenwEmPv(i32 %0, i8* %ptr) #4 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !16
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

declare %class.btConeShape* @_ZN11btConeShapeC1Eff(%class.btConeShape* returned, float, float) unnamed_addr #2

define hidden %struct.plCollisionShapeHandle__* @plNewCylinderShape(float %radius, float %height) #0 {
entry:
  %radius.addr = alloca float, align 4
  %height.addr = alloca float, align 4
  %mem = alloca i8*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store float %radius, float* %radius.addr, align 4, !tbaa !6
  store float %height, float* %height.addr, align 4, !tbaa !6
  %0 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 56, i32 16)
  store i8* %call, i8** %mem, align 4, !tbaa !2
  %1 = load i8*, i8** %mem, align 4, !tbaa !2
  %call1 = call i8* @_ZN15btCylinderShapenwEmPv(i32 56, i8* %1)
  %2 = bitcast i8* %call1 to %class.btCylinderShape*
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #6
  %call2 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %radius.addr, float* nonnull align 4 dereferenceable(4) %height.addr, float* nonnull align 4 dereferenceable(4) %radius.addr)
  %call3 = call %class.btCylinderShape* @_ZN15btCylinderShapeC1ERK9btVector3(%class.btCylinderShape* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %4 = bitcast %class.btCylinderShape* %2 to %struct.plCollisionShapeHandle__*
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #6
  %6 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #6
  ret %struct.plCollisionShapeHandle__* %4
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN15btCylinderShapenwEmPv(i32 %0, i8* %ptr) #4 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !16
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

declare %class.btCylinderShape* @_ZN15btCylinderShapeC1ERK9btVector3(%class.btCylinderShape* returned, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #2

define hidden %struct.plCollisionShapeHandle__* @plNewConvexHullShape() #0 {
entry:
  %mem = alloca i8*, align 4
  %0 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 112, i32 16)
  store i8* %call, i8** %mem, align 4, !tbaa !2
  %1 = load i8*, i8** %mem, align 4, !tbaa !2
  %call1 = call i8* @_ZN17btConvexHullShapenwEmPv(i32 112, i8* %1)
  %2 = bitcast i8* %call1 to %class.btConvexHullShape*
  %call2 = call %class.btConvexHullShape* @_ZN17btConvexHullShapeC1EPKfii(%class.btConvexHullShape* %2, float* null, i32 0, i32 16)
  %3 = bitcast %class.btConvexHullShape* %2 to %struct.plCollisionShapeHandle__*
  %4 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #6
  ret %struct.plCollisionShapeHandle__* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN17btConvexHullShapenwEmPv(i32 %0, i8* %ptr) #4 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !16
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

declare %class.btConvexHullShape* @_ZN17btConvexHullShapeC1EPKfii(%class.btConvexHullShape* returned, float*, i32, i32) unnamed_addr #2

; Function Attrs: nounwind
define hidden %struct.plMeshInterfaceHandle__* @plNewMeshInterface() #3 {
entry:
  ret %struct.plMeshInterfaceHandle__* null
}

define hidden %struct.plCollisionShapeHandle__* @plNewCompoundShape() #0 {
entry:
  %mem = alloca i8*, align 4
  %0 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 92, i32 16)
  store i8* %call, i8** %mem, align 4, !tbaa !2
  %1 = load i8*, i8** %mem, align 4, !tbaa !2
  %call1 = call i8* @_ZN15btCompoundShapenwEmPv(i32 92, i8* %1)
  %2 = bitcast i8* %call1 to %class.btCompoundShape*
  %call2 = call %class.btCompoundShape* @_ZN15btCompoundShapeC1Eb(%class.btCompoundShape* %2, i1 zeroext true)
  %3 = bitcast %class.btCompoundShape* %2 to %struct.plCollisionShapeHandle__*
  %4 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #6
  ret %struct.plCollisionShapeHandle__* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN15btCompoundShapenwEmPv(i32 %0, i8* %ptr) #4 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !16
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

declare %class.btCompoundShape* @_ZN15btCompoundShapeC1Eb(%class.btCompoundShape* returned, i1 zeroext) unnamed_addr #2

define hidden void @plAddChildShape(%struct.plCollisionShapeHandle__* %compoundShapeHandle, %struct.plCollisionShapeHandle__* %childShapeHandle, float* %childPos, float* %childOrn) #0 {
entry:
  %compoundShapeHandle.addr = alloca %struct.plCollisionShapeHandle__*, align 4
  %childShapeHandle.addr = alloca %struct.plCollisionShapeHandle__*, align 4
  %childPos.addr = alloca float*, align 4
  %childOrn.addr = alloca float*, align 4
  %colShape = alloca %class.btCollisionShape*, align 4
  %compoundShape = alloca %class.btCompoundShape*, align 4
  %childShape = alloca %class.btCollisionShape*, align 4
  %localTrans = alloca %class.btTransform, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btQuaternion, align 4
  store %struct.plCollisionShapeHandle__* %compoundShapeHandle, %struct.plCollisionShapeHandle__** %compoundShapeHandle.addr, align 4, !tbaa !2
  store %struct.plCollisionShapeHandle__* %childShapeHandle, %struct.plCollisionShapeHandle__** %childShapeHandle.addr, align 4, !tbaa !2
  store float* %childPos, float** %childPos.addr, align 4, !tbaa !2
  store float* %childOrn, float** %childOrn.addr, align 4, !tbaa !2
  %0 = bitcast %class.btCollisionShape** %colShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.plCollisionShapeHandle__*, %struct.plCollisionShapeHandle__** %compoundShapeHandle.addr, align 4, !tbaa !2
  %2 = bitcast %struct.plCollisionShapeHandle__* %1 to %class.btCollisionShape*
  store %class.btCollisionShape* %2, %class.btCollisionShape** %colShape, align 4, !tbaa !2
  %3 = bitcast %class.btCompoundShape** %compoundShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %class.btCollisionShape*, %class.btCollisionShape** %colShape, align 4, !tbaa !2
  %5 = bitcast %class.btCollisionShape* %4 to %class.btCompoundShape*
  store %class.btCompoundShape* %5, %class.btCompoundShape** %compoundShape, align 4, !tbaa !2
  %6 = bitcast %class.btCollisionShape** %childShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load %struct.plCollisionShapeHandle__*, %struct.plCollisionShapeHandle__** %childShapeHandle.addr, align 4, !tbaa !2
  %8 = bitcast %struct.plCollisionShapeHandle__* %7 to %class.btCollisionShape*
  store %class.btCollisionShape* %8, %class.btCollisionShape** %childShape, align 4, !tbaa !2
  %9 = bitcast %class.btTransform* %localTrans to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %9) #6
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %localTrans)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %localTrans)
  %10 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #6
  %11 = load float*, float** %childPos.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds float, float* %11, i32 0
  %12 = load float*, float** %childPos.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds float, float* %12, i32 1
  %13 = load float*, float** %childPos.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds float, float* %13, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx1, float* nonnull align 4 dereferenceable(4) %arrayidx2)
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %localTrans, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %14 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %14) #6
  %15 = bitcast %class.btQuaternion* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %15) #6
  %16 = load float*, float** %childOrn.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds float, float* %16, i32 0
  %17 = load float*, float** %childOrn.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds float, float* %17, i32 1
  %18 = load float*, float** %childOrn.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds float, float* %18, i32 2
  %19 = load float*, float** %childOrn.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds float, float* %19, i32 3
  %call9 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %ref.tmp4, float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx6, float* nonnull align 4 dereferenceable(4) %arrayidx7, float* nonnull align 4 dereferenceable(4) %arrayidx8)
  call void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %localTrans, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp4)
  %20 = bitcast %class.btQuaternion* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %20) #6
  %21 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape, align 4, !tbaa !2
  %22 = load %class.btCollisionShape*, %class.btCollisionShape** %childShape, align 4, !tbaa !2
  call void @_ZN15btCompoundShape13addChildShapeERK11btTransformP16btCollisionShape(%class.btCompoundShape* %21, %class.btTransform* nonnull align 4 dereferenceable(64) %localTrans, %class.btCollisionShape* %22)
  %23 = bitcast %class.btTransform* %localTrans to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %23) #6
  %24 = bitcast %class.btCollisionShape** %childShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #6
  %25 = bitcast %class.btCompoundShape** %compoundShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #6
  %26 = bitcast %class.btCollisionShape** %colShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %origin) #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %origin.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %origin, %class.btVector3** %origin.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %origin.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_origin to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !28
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #5 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %m_basis, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0)
  ret void
}

define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  store float* %_w, float** %_w.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = load float*, float** %_x.addr, align 4, !tbaa !2
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float*, float** %_z.addr, align 4, !tbaa !2
  %4 = load float*, float** %_w.addr, align 4, !tbaa !2
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4)
  ret %class.btQuaternion* %this1
}

declare void @_ZN15btCompoundShape13addChildShapeERK11btTransformP16btCollisionShape(%class.btCompoundShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btCollisionShape*) #2

define hidden void @plSetEuler(float %yaw, float %pitch, float %roll, float* %orient) #0 {
entry:
  %yaw.addr = alloca float, align 4
  %pitch.addr = alloca float, align 4
  %roll.addr = alloca float, align 4
  %orient.addr = alloca float*, align 4
  %orn = alloca %class.btQuaternion, align 4
  store float %yaw, float* %yaw.addr, align 4, !tbaa !6
  store float %pitch, float* %pitch.addr, align 4, !tbaa !6
  store float %roll, float* %roll.addr, align 4, !tbaa !6
  store float* %orient, float** %orient.addr, align 4, !tbaa !2
  %0 = bitcast %class.btQuaternion* %orn to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #6
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %orn)
  call void @_ZN12btQuaternion8setEulerERKfS1_S1_(%class.btQuaternion* %orn, float* nonnull align 4 dereferenceable(4) %yaw.addr, float* nonnull align 4 dereferenceable(4) %pitch.addr, float* nonnull align 4 dereferenceable(4) %roll.addr)
  %1 = bitcast %class.btQuaternion* %orn to %class.btQuadWord*
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getXEv(%class.btQuadWord* %1)
  %2 = load float, float* %call1, align 4, !tbaa !6
  %3 = load float*, float** %orient.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds float, float* %3, i32 0
  store float %2, float* %arrayidx, align 4, !tbaa !6
  %4 = bitcast %class.btQuaternion* %orn to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getYEv(%class.btQuadWord* %4)
  %5 = load float, float* %call2, align 4, !tbaa !6
  %6 = load float*, float** %orient.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds float, float* %6, i32 1
  store float %5, float* %arrayidx3, align 4, !tbaa !6
  %7 = bitcast %class.btQuaternion* %orn to %class.btQuadWord*
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getZEv(%class.btQuadWord* %7)
  %8 = load float, float* %call4, align 4, !tbaa !6
  %9 = load float*, float** %orient.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds float, float* %9, i32 2
  store float %8, float* %arrayidx5, align 4, !tbaa !6
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK12btQuaternion4getWEv(%class.btQuaternion* %orn)
  %10 = load float, float* %call6, align 4, !tbaa !6
  %11 = load float*, float** %orient.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds float, float* %11, i32 3
  store float %10, float* %arrayidx7, align 4, !tbaa !6
  %12 = bitcast %class.btQuaternion* %orn to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %12) #6
  ret void
}

define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* %0)
  ret %class.btQuaternion* %this1
}

define linkonce_odr hidden void @_ZN12btQuaternion8setEulerERKfS1_S1_(%class.btQuaternion* %this, float* nonnull align 4 dereferenceable(4) %yaw, float* nonnull align 4 dereferenceable(4) %pitch, float* nonnull align 4 dereferenceable(4) %roll) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %yaw.addr = alloca float*, align 4
  %pitch.addr = alloca float*, align 4
  %roll.addr = alloca float*, align 4
  %halfYaw = alloca float, align 4
  %halfPitch = alloca float, align 4
  %halfRoll = alloca float, align 4
  %cosYaw = alloca float, align 4
  %sinYaw = alloca float, align 4
  %cosPitch = alloca float, align 4
  %sinPitch = alloca float, align 4
  %cosRoll = alloca float, align 4
  %sinRoll = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp24 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store float* %yaw, float** %yaw.addr, align 4, !tbaa !2
  store float* %pitch, float** %pitch.addr, align 4, !tbaa !2
  store float* %roll, float** %roll.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast float* %halfYaw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load float*, float** %yaw.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !6
  %mul = fmul float %2, 5.000000e-01
  store float %mul, float* %halfYaw, align 4, !tbaa !6
  %3 = bitcast float* %halfPitch to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load float*, float** %pitch.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !6
  %mul2 = fmul float %5, 5.000000e-01
  store float %mul2, float* %halfPitch, align 4, !tbaa !6
  %6 = bitcast float* %halfRoll to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load float*, float** %roll.addr, align 4, !tbaa !2
  %8 = load float, float* %7, align 4, !tbaa !6
  %mul3 = fmul float %8, 5.000000e-01
  store float %mul3, float* %halfRoll, align 4, !tbaa !6
  %9 = bitcast float* %cosYaw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %10 = load float, float* %halfYaw, align 4, !tbaa !6
  %call = call float @_Z5btCosf(float %10)
  store float %call, float* %cosYaw, align 4, !tbaa !6
  %11 = bitcast float* %sinYaw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #6
  %12 = load float, float* %halfYaw, align 4, !tbaa !6
  %call4 = call float @_Z5btSinf(float %12)
  store float %call4, float* %sinYaw, align 4, !tbaa !6
  %13 = bitcast float* %cosPitch to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  %14 = load float, float* %halfPitch, align 4, !tbaa !6
  %call5 = call float @_Z5btCosf(float %14)
  store float %call5, float* %cosPitch, align 4, !tbaa !6
  %15 = bitcast float* %sinPitch to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #6
  %16 = load float, float* %halfPitch, align 4, !tbaa !6
  %call6 = call float @_Z5btSinf(float %16)
  store float %call6, float* %sinPitch, align 4, !tbaa !6
  %17 = bitcast float* %cosRoll to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #6
  %18 = load float, float* %halfRoll, align 4, !tbaa !6
  %call7 = call float @_Z5btCosf(float %18)
  store float %call7, float* %cosRoll, align 4, !tbaa !6
  %19 = bitcast float* %sinRoll to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #6
  %20 = load float, float* %halfRoll, align 4, !tbaa !6
  %call8 = call float @_Z5btSinf(float %20)
  store float %call8, float* %sinRoll, align 4, !tbaa !6
  %21 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %22 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #6
  %23 = load float, float* %cosRoll, align 4, !tbaa !6
  %24 = load float, float* %sinPitch, align 4, !tbaa !6
  %mul9 = fmul float %23, %24
  %25 = load float, float* %cosYaw, align 4, !tbaa !6
  %mul10 = fmul float %mul9, %25
  %26 = load float, float* %sinRoll, align 4, !tbaa !6
  %27 = load float, float* %cosPitch, align 4, !tbaa !6
  %mul11 = fmul float %26, %27
  %28 = load float, float* %sinYaw, align 4, !tbaa !6
  %mul12 = fmul float %mul11, %28
  %add = fadd float %mul10, %mul12
  store float %add, float* %ref.tmp, align 4, !tbaa !6
  %29 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #6
  %30 = load float, float* %cosRoll, align 4, !tbaa !6
  %31 = load float, float* %cosPitch, align 4, !tbaa !6
  %mul14 = fmul float %30, %31
  %32 = load float, float* %sinYaw, align 4, !tbaa !6
  %mul15 = fmul float %mul14, %32
  %33 = load float, float* %sinRoll, align 4, !tbaa !6
  %34 = load float, float* %sinPitch, align 4, !tbaa !6
  %mul16 = fmul float %33, %34
  %35 = load float, float* %cosYaw, align 4, !tbaa !6
  %mul17 = fmul float %mul16, %35
  %sub = fsub float %mul15, %mul17
  store float %sub, float* %ref.tmp13, align 4, !tbaa !6
  %36 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #6
  %37 = load float, float* %sinRoll, align 4, !tbaa !6
  %38 = load float, float* %cosPitch, align 4, !tbaa !6
  %mul19 = fmul float %37, %38
  %39 = load float, float* %cosYaw, align 4, !tbaa !6
  %mul20 = fmul float %mul19, %39
  %40 = load float, float* %cosRoll, align 4, !tbaa !6
  %41 = load float, float* %sinPitch, align 4, !tbaa !6
  %mul21 = fmul float %40, %41
  %42 = load float, float* %sinYaw, align 4, !tbaa !6
  %mul22 = fmul float %mul21, %42
  %sub23 = fsub float %mul20, %mul22
  store float %sub23, float* %ref.tmp18, align 4, !tbaa !6
  %43 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #6
  %44 = load float, float* %cosRoll, align 4, !tbaa !6
  %45 = load float, float* %cosPitch, align 4, !tbaa !6
  %mul25 = fmul float %44, %45
  %46 = load float, float* %cosYaw, align 4, !tbaa !6
  %mul26 = fmul float %mul25, %46
  %47 = load float, float* %sinRoll, align 4, !tbaa !6
  %48 = load float, float* %sinPitch, align 4, !tbaa !6
  %mul27 = fmul float %47, %48
  %49 = load float, float* %sinYaw, align 4, !tbaa !6
  %mul28 = fmul float %mul27, %49
  %add29 = fadd float %mul26, %mul28
  store float %add29, float* %ref.tmp24, align 4, !tbaa !6
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %21, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp24)
  %50 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #6
  %51 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #6
  %52 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #6
  %53 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #6
  %54 = bitcast float* %sinRoll to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #6
  %55 = bitcast float* %cosRoll to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #6
  %56 = bitcast float* %sinPitch to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #6
  %57 = bitcast float* %cosPitch to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #6
  %58 = bitcast float* %sinYaw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #6
  %59 = bitcast float* %cosYaw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #6
  %60 = bitcast float* %halfRoll to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #6
  %61 = bitcast float* %halfPitch to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #6
  %62 = bitcast float* %halfYaw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getXEv(%class.btQuadWord* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getYEv(%class.btQuadWord* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getZEv(%class.btQuadWord* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK12btQuaternion4getWEv(%class.btQuaternion* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

define hidden void @plAddVertex(%struct.plCollisionShapeHandle__* %cshape, float %x, float %y, float %z) #0 {
entry:
  %cshape.addr = alloca %struct.plCollisionShapeHandle__*, align 4
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  %z.addr = alloca float, align 4
  %colShape = alloca %class.btCollisionShape*, align 4
  %convexHullShape = alloca %class.btConvexHullShape*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %struct.plCollisionShapeHandle__* %cshape, %struct.plCollisionShapeHandle__** %cshape.addr, align 4, !tbaa !2
  store float %x, float* %x.addr, align 4, !tbaa !6
  store float %y, float* %y.addr, align 4, !tbaa !6
  store float %z, float* %z.addr, align 4, !tbaa !6
  %0 = bitcast %class.btCollisionShape** %colShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.plCollisionShapeHandle__*, %struct.plCollisionShapeHandle__** %cshape.addr, align 4, !tbaa !2
  %2 = bitcast %struct.plCollisionShapeHandle__* %1 to %class.btCollisionShape*
  store %class.btCollisionShape* %2, %class.btCollisionShape** %colShape, align 4, !tbaa !2
  %3 = bitcast %class.btConvexHullShape** %convexHullShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %struct.plCollisionShapeHandle__*, %struct.plCollisionShapeHandle__** %cshape.addr, align 4, !tbaa !2
  %5 = bitcast %struct.plCollisionShapeHandle__* %4 to %class.btConvexHullShape*
  store %class.btConvexHullShape* %5, %class.btConvexHullShape** %convexHullShape, align 4, !tbaa !2
  %6 = load %class.btConvexHullShape*, %class.btConvexHullShape** %convexHullShape, align 4, !tbaa !2
  %7 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %x.addr, float* nonnull align 4 dereferenceable(4) %y.addr, float* nonnull align 4 dereferenceable(4) %z.addr)
  call void @_ZN17btConvexHullShape8addPointERK9btVector3b(%class.btConvexHullShape* %6, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, i1 zeroext true)
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #6
  %9 = bitcast %class.btConvexHullShape** %convexHullShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #6
  %10 = bitcast %class.btCollisionShape** %colShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  ret void
}

declare void @_ZN17btConvexHullShape8addPointERK9btVector3b(%class.btConvexHullShape*, %class.btVector3* nonnull align 4 dereferenceable(16), i1 zeroext) #2

define hidden void @plDeleteShape(%struct.plCollisionShapeHandle__* %cshape) #0 {
entry:
  %cshape.addr = alloca %struct.plCollisionShapeHandle__*, align 4
  %shape = alloca %class.btCollisionShape*, align 4
  store %struct.plCollisionShapeHandle__* %cshape, %struct.plCollisionShapeHandle__** %cshape.addr, align 4, !tbaa !2
  %0 = bitcast %class.btCollisionShape** %shape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.plCollisionShapeHandle__*, %struct.plCollisionShapeHandle__** %cshape.addr, align 4, !tbaa !2
  %2 = bitcast %struct.plCollisionShapeHandle__* %1 to %class.btCollisionShape*
  store %class.btCollisionShape* %2, %class.btCollisionShape** %shape, align 4, !tbaa !2
  %3 = load %class.btCollisionShape*, %class.btCollisionShape** %shape, align 4, !tbaa !2
  %4 = bitcast %class.btCollisionShape* %3 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %4)
  %5 = bitcast %class.btCollisionShape** %shape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  ret void
}

define hidden void @plSetScaling(%struct.plCollisionShapeHandle__* %cshape, float* %cscaling) #0 {
entry:
  %cshape.addr = alloca %struct.plCollisionShapeHandle__*, align 4
  %cscaling.addr = alloca float*, align 4
  %shape = alloca %class.btCollisionShape*, align 4
  %scaling = alloca %class.btVector3, align 4
  store %struct.plCollisionShapeHandle__* %cshape, %struct.plCollisionShapeHandle__** %cshape.addr, align 4, !tbaa !2
  store float* %cscaling, float** %cscaling.addr, align 4, !tbaa !2
  %0 = bitcast %class.btCollisionShape** %shape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.plCollisionShapeHandle__*, %struct.plCollisionShapeHandle__** %cshape.addr, align 4, !tbaa !2
  %2 = bitcast %struct.plCollisionShapeHandle__* %1 to %class.btCollisionShape*
  store %class.btCollisionShape* %2, %class.btCollisionShape** %shape, align 4, !tbaa !2
  %3 = bitcast %class.btVector3* %scaling to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #6
  %4 = load float*, float** %cscaling.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds float, float* %4, i32 0
  %5 = load float*, float** %cscaling.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds float, float* %5, i32 1
  %6 = load float*, float** %cscaling.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds float, float* %6, i32 2
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %scaling, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx1, float* nonnull align 4 dereferenceable(4) %arrayidx2)
  %7 = load %class.btCollisionShape*, %class.btCollisionShape** %shape, align 4, !tbaa !2
  %8 = bitcast %class.btCollisionShape* %7 to void (%class.btCollisionShape*, %class.btVector3*)***
  %vtable = load void (%class.btCollisionShape*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btVector3*)*** %8, align 4, !tbaa !18
  %vfn = getelementptr inbounds void (%class.btCollisionShape*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btVector3*)** %vtable, i64 6
  %9 = load void (%class.btCollisionShape*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btVector3*)** %vfn, align 4
  call void %9(%class.btCollisionShape* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %scaling)
  %10 = bitcast %class.btVector3* %scaling to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %10) #6
  %11 = bitcast %class.btCollisionShape** %shape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  ret void
}

define hidden void @plSetPosition(%struct.plRigidBodyHandle__* %object, float* %position) #0 {
entry:
  %object.addr = alloca %struct.plRigidBodyHandle__*, align 4
  %position.addr = alloca float*, align 4
  %body = alloca %class.btRigidBody*, align 4
  %pos = alloca %class.btVector3, align 4
  %worldTrans = alloca %class.btTransform, align 4
  store %struct.plRigidBodyHandle__* %object, %struct.plRigidBodyHandle__** %object.addr, align 4, !tbaa !2
  store float* %position, float** %position.addr, align 4, !tbaa !2
  %0 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.plRigidBodyHandle__*, %struct.plRigidBodyHandle__** %object.addr, align 4, !tbaa !2
  %2 = bitcast %struct.plRigidBodyHandle__* %1 to %class.btRigidBody*
  store %class.btRigidBody* %2, %class.btRigidBody** %body, align 4, !tbaa !2
  %3 = bitcast %class.btVector3* %pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #6
  %4 = load float*, float** %position.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds float, float* %4, i32 0
  %5 = load float*, float** %position.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds float, float* %5, i32 1
  %6 = load float*, float** %position.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds float, float* %6, i32 2
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %pos, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx1, float* nonnull align 4 dereferenceable(4) %arrayidx2)
  %7 = bitcast %class.btTransform* %worldTrans to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %7) #6
  %8 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %9 = bitcast %class.btRigidBody* %8 to %class.btCollisionObject*
  %call3 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %9)
  %call4 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %worldTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %call3)
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %worldTrans, %class.btVector3* nonnull align 4 dereferenceable(16) %pos)
  %10 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %11 = bitcast %class.btRigidBody* %10 to %class.btCollisionObject*
  call void @_ZN17btCollisionObject17setWorldTransformERK11btTransform(%class.btCollisionObject* %11, %class.btTransform* nonnull align 4 dereferenceable(64) %worldTrans)
  %12 = bitcast %class.btTransform* %worldTrans to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %12) #6
  %13 = bitcast %class.btVector3* %pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %13) #6
  %14 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #6
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !28
  ret %class.btTransform* %this1
}

define hidden void @plSetOrientation(%struct.plRigidBodyHandle__* %object, float* %orientation) #0 {
entry:
  %object.addr = alloca %struct.plRigidBodyHandle__*, align 4
  %orientation.addr = alloca float*, align 4
  %body = alloca %class.btRigidBody*, align 4
  %orn = alloca %class.btQuaternion, align 4
  %worldTrans = alloca %class.btTransform, align 4
  store %struct.plRigidBodyHandle__* %object, %struct.plRigidBodyHandle__** %object.addr, align 4, !tbaa !2
  store float* %orientation, float** %orientation.addr, align 4, !tbaa !2
  %0 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.plRigidBodyHandle__*, %struct.plRigidBodyHandle__** %object.addr, align 4, !tbaa !2
  %2 = bitcast %struct.plRigidBodyHandle__* %1 to %class.btRigidBody*
  store %class.btRigidBody* %2, %class.btRigidBody** %body, align 4, !tbaa !2
  %3 = bitcast %class.btQuaternion* %orn to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #6
  %4 = load float*, float** %orientation.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds float, float* %4, i32 0
  %5 = load float*, float** %orientation.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds float, float* %5, i32 1
  %6 = load float*, float** %orientation.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds float, float* %6, i32 2
  %7 = load float*, float** %orientation.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds float, float* %7, i32 3
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %orn, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx1, float* nonnull align 4 dereferenceable(4) %arrayidx2, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  %8 = bitcast %class.btTransform* %worldTrans to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %8) #6
  %9 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %10 = bitcast %class.btRigidBody* %9 to %class.btCollisionObject*
  %call4 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %10)
  %call5 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %worldTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %call4)
  call void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %worldTrans, %class.btQuaternion* nonnull align 4 dereferenceable(16) %orn)
  %11 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %12 = bitcast %class.btRigidBody* %11 to %class.btCollisionObject*
  call void @_ZN17btCollisionObject17setWorldTransformERK11btTransform(%class.btCollisionObject* %12, %class.btTransform* nonnull align 4 dereferenceable(64) %worldTrans)
  %13 = bitcast %class.btTransform* %worldTrans to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %13) #6
  %14 = bitcast %class.btQuaternion* %orn to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %14) #6
  %15 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  ret void
}

define hidden void @plSetOpenGLMatrix(%struct.plRigidBodyHandle__* %object, float* %matrix) #0 {
entry:
  %object.addr = alloca %struct.plRigidBodyHandle__*, align 4
  %matrix.addr = alloca float*, align 4
  %body = alloca %class.btRigidBody*, align 4
  %worldTrans = alloca %class.btTransform*, align 4
  store %struct.plRigidBodyHandle__* %object, %struct.plRigidBodyHandle__** %object.addr, align 4, !tbaa !2
  store float* %matrix, float** %matrix.addr, align 4, !tbaa !2
  %0 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.plRigidBodyHandle__*, %struct.plRigidBodyHandle__** %object.addr, align 4, !tbaa !2
  %2 = bitcast %struct.plRigidBodyHandle__* %1 to %class.btRigidBody*
  store %class.btRigidBody* %2, %class.btRigidBody** %body, align 4, !tbaa !2
  %3 = bitcast %class.btTransform** %worldTrans to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %5 = bitcast %class.btRigidBody* %4 to %class.btCollisionObject*
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %5)
  store %class.btTransform* %call, %class.btTransform** %worldTrans, align 4, !tbaa !2
  %6 = load %class.btTransform*, %class.btTransform** %worldTrans, align 4, !tbaa !2
  %7 = load float*, float** %matrix.addr, align 4, !tbaa !2
  call void @_ZN11btTransform19setFromOpenGLMatrixEPKf(%class.btTransform* %6, float* %7)
  %8 = bitcast %class.btTransform** %worldTrans to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #6
  %9 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #6
  ret void
}

define linkonce_odr hidden void @_ZN11btTransform19setFromOpenGLMatrixEPKf(%class.btTransform* %this, float* %m) #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %m.addr = alloca float*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store float* %m, float** %m.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load float*, float** %m.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x322setFromOpenGLSubMatrixEPKf(%class.btMatrix3x3* %m_basis, float* %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load float*, float** %m.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds float, float* %1, i32 12
  %2 = load float*, float** %m.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds float, float* %2, i32 13
  %3 = load float*, float** %m.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds float, float* %3, i32 14
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_origin, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx2, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  ret void
}

define hidden void @plGetOpenGLMatrix(%struct.plRigidBodyHandle__* %object, float* %matrix) #0 {
entry:
  %object.addr = alloca %struct.plRigidBodyHandle__*, align 4
  %matrix.addr = alloca float*, align 4
  %body = alloca %class.btRigidBody*, align 4
  store %struct.plRigidBodyHandle__* %object, %struct.plRigidBodyHandle__** %object.addr, align 4, !tbaa !2
  store float* %matrix, float** %matrix.addr, align 4, !tbaa !2
  %0 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.plRigidBodyHandle__*, %struct.plRigidBodyHandle__** %object.addr, align 4, !tbaa !2
  %2 = bitcast %struct.plRigidBodyHandle__* %1 to %class.btRigidBody*
  store %class.btRigidBody* %2, %class.btRigidBody** %body, align 4, !tbaa !2
  %3 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %4 = bitcast %class.btRigidBody* %3 to %class.btCollisionObject*
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %4)
  %5 = load float*, float** %matrix.addr, align 4, !tbaa !2
  call void @_ZNK11btTransform15getOpenGLMatrixEPf(%class.btTransform* %call, float* %5)
  %6 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #6
  ret void
}

define linkonce_odr hidden void @_ZNK11btTransform15getOpenGLMatrixEPf(%class.btTransform* %this, float* %m) #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %m.addr = alloca float*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store float* %m, float** %m.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load float*, float** %m.addr, align 4, !tbaa !2
  call void @_ZNK11btMatrix3x318getOpenGLSubMatrixEPf(%class.btMatrix3x3* %m_basis, float* %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %m_origin)
  %1 = load float, float* %call, align 4, !tbaa !6
  %2 = load float*, float** %m.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds float, float* %2, i32 12
  store float %1, float* %arrayidx, align 4, !tbaa !6
  %m_origin2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %m_origin2)
  %3 = load float, float* %call3, align 4, !tbaa !6
  %4 = load float*, float** %m.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds float, float* %4, i32 13
  store float %3, float* %arrayidx4, align 4, !tbaa !6
  %m_origin5 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %m_origin5)
  %5 = load float, float* %call6, align 4, !tbaa !6
  %6 = load float*, float** %m.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds float, float* %6, i32 14
  store float %5, float* %arrayidx7, align 4, !tbaa !6
  %7 = load float*, float** %m.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds float, float* %7, i32 15
  store float 1.000000e+00, float* %arrayidx8, align 4, !tbaa !6
  ret void
}

define hidden void @plGetPosition(%struct.plRigidBodyHandle__* %object, float* %position) #0 {
entry:
  %object.addr = alloca %struct.plRigidBodyHandle__*, align 4
  %position.addr = alloca float*, align 4
  %body = alloca %class.btRigidBody*, align 4
  %pos = alloca %class.btVector3*, align 4
  store %struct.plRigidBodyHandle__* %object, %struct.plRigidBodyHandle__** %object.addr, align 4, !tbaa !2
  store float* %position, float** %position.addr, align 4, !tbaa !2
  %0 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.plRigidBodyHandle__*, %struct.plRigidBodyHandle__** %object.addr, align 4, !tbaa !2
  %2 = bitcast %struct.plRigidBodyHandle__* %1 to %class.btRigidBody*
  store %class.btRigidBody* %2, %class.btRigidBody** %body, align 4, !tbaa !2
  %3 = bitcast %class.btVector3** %pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %5 = bitcast %class.btRigidBody* %4 to %class.btCollisionObject*
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %5)
  %call1 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %call)
  store %class.btVector3* %call1, %class.btVector3** %pos, align 4, !tbaa !2
  %6 = load %class.btVector3*, %class.btVector3** %pos, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %6)
  %7 = load float, float* %call2, align 4, !tbaa !6
  %8 = load float*, float** %position.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds float, float* %8, i32 0
  store float %7, float* %arrayidx, align 4, !tbaa !6
  %9 = load %class.btVector3*, %class.btVector3** %pos, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %9)
  %10 = load float, float* %call3, align 4, !tbaa !6
  %11 = load float*, float** %position.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds float, float* %11, i32 1
  store float %10, float* %arrayidx4, align 4, !tbaa !6
  %12 = load %class.btVector3*, %class.btVector3** %pos, align 4, !tbaa !2
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %12)
  %13 = load float, float* %call5, align 4, !tbaa !6
  %14 = load float*, float** %position.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds float, float* %14, i32 2
  store float %13, float* %arrayidx6, align 4, !tbaa !6
  %15 = bitcast %class.btVector3** %pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

define hidden void @plGetOrientation(%struct.plRigidBodyHandle__* %object, float* %orientation) #0 {
entry:
  %object.addr = alloca %struct.plRigidBodyHandle__*, align 4
  %orientation.addr = alloca float*, align 4
  %body = alloca %class.btRigidBody*, align 4
  %orn = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca %class.btQuaternion, align 4
  store %struct.plRigidBodyHandle__* %object, %struct.plRigidBodyHandle__** %object.addr, align 4, !tbaa !2
  store float* %orientation, float** %orientation.addr, align 4, !tbaa !2
  %0 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.plRigidBodyHandle__*, %struct.plRigidBodyHandle__** %object.addr, align 4, !tbaa !2
  %2 = bitcast %struct.plRigidBodyHandle__* %1 to %class.btRigidBody*
  store %class.btRigidBody* %2, %class.btRigidBody** %body, align 4, !tbaa !2
  %3 = bitcast %class.btQuaternion** %orn to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #6
  %5 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %6 = bitcast %class.btRigidBody* %5 to %class.btCollisionObject*
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %6)
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp, %class.btTransform* %call)
  store %class.btQuaternion* %ref.tmp, %class.btQuaternion** %orn, align 4, !tbaa !2
  %7 = load %class.btQuaternion*, %class.btQuaternion** %orn, align 4, !tbaa !2
  %8 = bitcast %class.btQuaternion* %7 to %class.btQuadWord*
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getXEv(%class.btQuadWord* %8)
  %9 = load float, float* %call1, align 4, !tbaa !6
  %10 = load float*, float** %orientation.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds float, float* %10, i32 0
  store float %9, float* %arrayidx, align 4, !tbaa !6
  %11 = load %class.btQuaternion*, %class.btQuaternion** %orn, align 4, !tbaa !2
  %12 = bitcast %class.btQuaternion* %11 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getYEv(%class.btQuadWord* %12)
  %13 = load float, float* %call2, align 4, !tbaa !6
  %14 = load float*, float** %orientation.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds float, float* %14, i32 1
  store float %13, float* %arrayidx3, align 4, !tbaa !6
  %15 = load %class.btQuaternion*, %class.btQuaternion** %orn, align 4, !tbaa !2
  %16 = bitcast %class.btQuaternion* %15 to %class.btQuadWord*
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getZEv(%class.btQuadWord* %16)
  %17 = load float, float* %call4, align 4, !tbaa !6
  %18 = load float*, float** %orientation.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds float, float* %18, i32 2
  store float %17, float* %arrayidx5, align 4, !tbaa !6
  %19 = load %class.btQuaternion*, %class.btQuaternion** %orn, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK12btQuaternion4getWEv(%class.btQuaternion* %19)
  %20 = load float, float* %call6, align 4, !tbaa !6
  %21 = load float*, float** %orientation.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds float, float* %21, i32 3
  store float %20, float* %arrayidx7, align 4, !tbaa !6
  %22 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %22) #6
  %23 = bitcast %class.btQuaternion** %orn to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #6
  %24 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #6
  ret void
}

define linkonce_odr hidden void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btTransform* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %agg.result)
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %m_basis, %class.btQuaternion* nonnull align 4 dereferenceable(16) %agg.result)
  ret void
}

define hidden double @plNearestPoints(float* %p1, float* %p2, float* %p3, float* %q1, float* %q2, float* %q3, float* %pa, float* %pb, float* %normal) #0 {
entry:
  %retval = alloca double, align 8
  %p1.addr = alloca float*, align 4
  %p2.addr = alloca float*, align 4
  %p3.addr = alloca float*, align 4
  %q1.addr = alloca float*, align 4
  %q2.addr = alloca float*, align 4
  %q3.addr = alloca float*, align 4
  %pa.addr = alloca float*, align 4
  %pb.addr = alloca float*, align 4
  %normal.addr = alloca float*, align 4
  %vp = alloca %class.btVector3, align 4
  %trishapeA = alloca %class.btTriangleShape, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca %class.btVector3, align 4
  %vq = alloca %class.btVector3, align 4
  %trishapeB = alloca %class.btTriangleShape, align 4
  %ref.tmp17 = alloca %class.btVector3, align 4
  %ref.tmp22 = alloca %class.btVector3, align 4
  %Solver = alloca %class.btConvexPenetrationDepthSolver*, align 4
  %convexConvex = alloca %class.btGjkPairDetector, align 4
  %gjkOutput = alloca %struct.btPointCollector, align 4
  %input = alloca %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", align 4
  %tr = alloca %class.btTransform, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store float* %p1, float** %p1.addr, align 4, !tbaa !2
  store float* %p2, float** %p2.addr, align 4, !tbaa !2
  store float* %p3, float** %p3.addr, align 4, !tbaa !2
  store float* %q1, float** %q1.addr, align 4, !tbaa !2
  store float* %q2, float** %q2.addr, align 4, !tbaa !2
  store float* %q3, float** %q3.addr, align 4, !tbaa !2
  store float* %pa, float** %pa.addr, align 4, !tbaa !2
  store float* %pb, float** %pb.addr, align 4, !tbaa !2
  store float* %normal, float** %normal.addr, align 4, !tbaa !2
  %0 = bitcast %class.btVector3* %vp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #6
  %1 = load float*, float** %p1.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds float, float* %1, i32 0
  %2 = load float*, float** %p1.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds float, float* %2, i32 1
  %3 = load float*, float** %p1.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds float, float* %3, i32 2
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %vp, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx1, float* nonnull align 4 dereferenceable(4) %arrayidx2)
  %4 = bitcast %class.btTriangleShape* %trishapeA to i8*
  call void @llvm.lifetime.start.p0i8(i64 104, i8* %4) #6
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #6
  %6 = load float*, float** %p2.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds float, float* %6, i32 0
  %7 = load float*, float** %p2.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds float, float* %7, i32 1
  %8 = load float*, float** %p2.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds float, float* %8, i32 2
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %arrayidx3, float* nonnull align 4 dereferenceable(4) %arrayidx4, float* nonnull align 4 dereferenceable(4) %arrayidx5)
  %9 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #6
  %10 = load float*, float** %p3.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds float, float* %10, i32 0
  %11 = load float*, float** %p3.addr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds float, float* %11, i32 1
  %12 = load float*, float** %p3.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds float, float* %12, i32 2
  %call11 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp7, float* nonnull align 4 dereferenceable(4) %arrayidx8, float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx10)
  %call12 = call %class.btTriangleShape* @_ZN15btTriangleShapeC2ERK9btVector3S2_S2_(%class.btTriangleShape* %trishapeA, %class.btVector3* nonnull align 4 dereferenceable(16) %vp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp7)
  %13 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %13) #6
  %14 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %14) #6
  %15 = bitcast %class.btTriangleShape* %trishapeA to %class.btConvexInternalShape*
  call void @_ZN21btConvexInternalShape9setMarginEf(%class.btConvexInternalShape* %15, float 0x3EB0C6F7A0000000)
  %16 = bitcast %class.btVector3* %vq to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #6
  %17 = load float*, float** %q1.addr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds float, float* %17, i32 0
  %18 = load float*, float** %q1.addr, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds float, float* %18, i32 1
  %19 = load float*, float** %q1.addr, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds float, float* %19, i32 2
  %call16 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %vq, float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %arrayidx14, float* nonnull align 4 dereferenceable(4) %arrayidx15)
  %20 = bitcast %class.btTriangleShape* %trishapeB to i8*
  call void @llvm.lifetime.start.p0i8(i64 104, i8* %20) #6
  %21 = bitcast %class.btVector3* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %21) #6
  %22 = load float*, float** %q2.addr, align 4, !tbaa !2
  %arrayidx18 = getelementptr inbounds float, float* %22, i32 0
  %23 = load float*, float** %q2.addr, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds float, float* %23, i32 1
  %24 = load float*, float** %q2.addr, align 4, !tbaa !2
  %arrayidx20 = getelementptr inbounds float, float* %24, i32 2
  %call21 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp17, float* nonnull align 4 dereferenceable(4) %arrayidx18, float* nonnull align 4 dereferenceable(4) %arrayidx19, float* nonnull align 4 dereferenceable(4) %arrayidx20)
  %25 = bitcast %class.btVector3* %ref.tmp22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %25) #6
  %26 = load float*, float** %q3.addr, align 4, !tbaa !2
  %arrayidx23 = getelementptr inbounds float, float* %26, i32 0
  %27 = load float*, float** %q3.addr, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds float, float* %27, i32 1
  %28 = load float*, float** %q3.addr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds float, float* %28, i32 2
  %call26 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp22, float* nonnull align 4 dereferenceable(4) %arrayidx23, float* nonnull align 4 dereferenceable(4) %arrayidx24, float* nonnull align 4 dereferenceable(4) %arrayidx25)
  %call27 = call %class.btTriangleShape* @_ZN15btTriangleShapeC2ERK9btVector3S2_S2_(%class.btTriangleShape* %trishapeB, %class.btVector3* nonnull align 4 dereferenceable(16) %vq, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp17, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp22)
  %29 = bitcast %class.btVector3* %ref.tmp22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #6
  %30 = bitcast %class.btVector3* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %30) #6
  %31 = bitcast %class.btTriangleShape* %trishapeB to %class.btConvexInternalShape*
  call void @_ZN21btConvexInternalShape9setMarginEf(%class.btConvexInternalShape* %31, float 0x3EB0C6F7A0000000)
  %32 = load atomic i8, i8* bitcast (i32* @_ZGVZ15plNearestPointsE17sGjkSimplexSolver to i8*) acquire, align 4
  %33 = and i8 %32, 1
  %guard.uninitialized = icmp eq i8 %33, 0
  br i1 %guard.uninitialized, label %init.check, label %init.end, !prof !49

init.check:                                       ; preds = %entry
  %34 = call i32 @__cxa_guard_acquire(i32* @_ZGVZ15plNearestPointsE17sGjkSimplexSolver) #6
  %tobool = icmp ne i32 %34, 0
  br i1 %tobool, label %init, label %init.end

init:                                             ; preds = %init.check
  %call28 = call %class.btVoronoiSimplexSolver* @_ZN22btVoronoiSimplexSolverC2Ev(%class.btVoronoiSimplexSolver* @_ZZ15plNearestPointsE17sGjkSimplexSolver)
  call void @__cxa_guard_release(i32* @_ZGVZ15plNearestPointsE17sGjkSimplexSolver) #6
  br label %init.end

init.end:                                         ; preds = %init, %init.check, %entry
  call void @_ZN22btVoronoiSimplexSolver5resetEv(%class.btVoronoiSimplexSolver* @_ZZ15plNearestPointsE17sGjkSimplexSolver)
  %35 = load atomic i8, i8* bitcast (i32* @_ZGVZ15plNearestPointsE7Solver0 to i8*) acquire, align 4
  %36 = and i8 %35, 1
  %guard.uninitialized29 = icmp eq i8 %36, 0
  br i1 %guard.uninitialized29, label %init.check30, label %init.end34, !prof !49

init.check30:                                     ; preds = %init.end
  %37 = call i32 @__cxa_guard_acquire(i32* @_ZGVZ15plNearestPointsE7Solver0) #6
  %tobool31 = icmp ne i32 %37, 0
  br i1 %tobool31, label %init32, label %init.end34

init32:                                           ; preds = %init.check30
  %call33 = call %class.btGjkEpaPenetrationDepthSolver* @_ZN30btGjkEpaPenetrationDepthSolverC2Ev(%class.btGjkEpaPenetrationDepthSolver* @_ZZ15plNearestPointsE7Solver0)
  %38 = call i32 @__cxa_atexit(void (i8*)* @__cxx_global_array_dtor, i8* null, i8* @__dso_handle) #6
  call void @__cxa_guard_release(i32* @_ZGVZ15plNearestPointsE7Solver0) #6
  br label %init.end34

init.end34:                                       ; preds = %init32, %init.check30, %init.end
  %39 = load atomic i8, i8* bitcast (i32* @_ZGVZ15plNearestPointsE7Solver1 to i8*) acquire, align 4
  %40 = and i8 %39, 1
  %guard.uninitialized35 = icmp eq i8 %40, 0
  br i1 %guard.uninitialized35, label %init.check36, label %init.end39, !prof !49

init.check36:                                     ; preds = %init.end34
  %41 = call i32 @__cxa_guard_acquire(i32* @_ZGVZ15plNearestPointsE7Solver1) #6
  %tobool37 = icmp ne i32 %41, 0
  br i1 %tobool37, label %init38, label %init.end39

init38:                                           ; preds = %init.check36
  %42 = call i32 @__cxa_atexit(void (i8*)* @__cxx_global_array_dtor.1, i8* null, i8* @__dso_handle) #6
  call void @__cxa_guard_release(i32* @_ZGVZ15plNearestPointsE7Solver1) #6
  br label %init.end39

init.end39:                                       ; preds = %init38, %init.check36, %init.end34
  %43 = bitcast %class.btConvexPenetrationDepthSolver** %Solver to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #6
  store %class.btConvexPenetrationDepthSolver* null, %class.btConvexPenetrationDepthSolver** %Solver, align 4, !tbaa !2
  store %class.btConvexPenetrationDepthSolver* bitcast ({ i8** }* @_ZZ15plNearestPointsE7Solver1 to %class.btConvexPenetrationDepthSolver*), %class.btConvexPenetrationDepthSolver** %Solver, align 4, !tbaa !2
  %44 = bitcast %class.btGjkPairDetector* %convexConvex to i8*
  call void @llvm.lifetime.start.p0i8(i64 80, i8* %44) #6
  %45 = bitcast %class.btTriangleShape* %trishapeA to %class.btConvexShape*
  %46 = bitcast %class.btTriangleShape* %trishapeB to %class.btConvexShape*
  %47 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %Solver, align 4, !tbaa !2
  %call40 = call %class.btGjkPairDetector* @_ZN17btGjkPairDetectorC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver(%class.btGjkPairDetector* %convexConvex, %class.btConvexShape* %45, %class.btConvexShape* %46, %class.btVoronoiSimplexSolver* @_ZZ15plNearestPointsE17sGjkSimplexSolver, %class.btConvexPenetrationDepthSolver* %47)
  %m_catchDegeneracies = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %convexConvex, i32 0, i32 15
  store i32 1, i32* %m_catchDegeneracies, align 4, !tbaa !50
  %48 = bitcast %struct.btPointCollector* %gjkOutput to i8*
  call void @llvm.lifetime.start.p0i8(i64 44, i8* %48) #6
  %call41 = call %struct.btPointCollector* @_ZN16btPointCollectorC2Ev(%struct.btPointCollector* %gjkOutput)
  %49 = bitcast %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input to i8*
  call void @llvm.lifetime.start.p0i8(i64 132, i8* %49) #6
  %call42 = call %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* @_ZN36btDiscreteCollisionDetectorInterface17ClosestPointInputC2Ev(%"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input)
  %50 = bitcast %class.btTransform* %tr to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %50) #6
  %call43 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %tr)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %tr)
  %m_transformA = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, i32 0, i32 0
  %call44 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_transformA, %class.btTransform* nonnull align 4 dereferenceable(64) %tr)
  %m_transformB = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, i32 0, i32 1
  %call45 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_transformB, %class.btTransform* nonnull align 4 dereferenceable(64) %tr)
  %51 = bitcast %struct.btPointCollector* %gjkOutput to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  call void @_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb(%class.btGjkPairDetector* %convexConvex, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* nonnull align 4 dereferenceable(132) %input, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %51, %class.btIDebugDraw* null, i1 zeroext false)
  %m_hasResult = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %gjkOutput, i32 0, i32 4
  %52 = load i8, i8* %m_hasResult, align 4, !tbaa !52, !range !54
  %tobool46 = trunc i8 %52 to i1
  br i1 %tobool46, label %if.then, label %if.end

if.then:                                          ; preds = %init.end39
  %m_pointInWorld = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %gjkOutput, i32 0, i32 2
  %call47 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_pointInWorld)
  %arrayidx48 = getelementptr inbounds float, float* %call47, i32 0
  %53 = load float, float* %arrayidx48, align 4, !tbaa !6
  %54 = load float*, float** %pa.addr, align 4, !tbaa !2
  %arrayidx49 = getelementptr inbounds float, float* %54, i32 0
  store float %53, float* %arrayidx49, align 4, !tbaa !6
  %55 = load float*, float** %pb.addr, align 4, !tbaa !2
  %arrayidx50 = getelementptr inbounds float, float* %55, i32 0
  store float %53, float* %arrayidx50, align 4, !tbaa !6
  %m_pointInWorld51 = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %gjkOutput, i32 0, i32 2
  %call52 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_pointInWorld51)
  %arrayidx53 = getelementptr inbounds float, float* %call52, i32 1
  %56 = load float, float* %arrayidx53, align 4, !tbaa !6
  %57 = load float*, float** %pa.addr, align 4, !tbaa !2
  %arrayidx54 = getelementptr inbounds float, float* %57, i32 1
  store float %56, float* %arrayidx54, align 4, !tbaa !6
  %58 = load float*, float** %pb.addr, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds float, float* %58, i32 1
  store float %56, float* %arrayidx55, align 4, !tbaa !6
  %m_pointInWorld56 = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %gjkOutput, i32 0, i32 2
  %call57 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_pointInWorld56)
  %arrayidx58 = getelementptr inbounds float, float* %call57, i32 2
  %59 = load float, float* %arrayidx58, align 4, !tbaa !6
  %60 = load float*, float** %pa.addr, align 4, !tbaa !2
  %arrayidx59 = getelementptr inbounds float, float* %60, i32 2
  store float %59, float* %arrayidx59, align 4, !tbaa !6
  %61 = load float*, float** %pb.addr, align 4, !tbaa !2
  %arrayidx60 = getelementptr inbounds float, float* %61, i32 2
  store float %59, float* %arrayidx60, align 4, !tbaa !6
  %m_normalOnBInWorld = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %gjkOutput, i32 0, i32 1
  %call61 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_normalOnBInWorld)
  %arrayidx62 = getelementptr inbounds float, float* %call61, i32 0
  %62 = load float, float* %arrayidx62, align 4, !tbaa !6
  %m_distance = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %gjkOutput, i32 0, i32 3
  %63 = load float, float* %m_distance, align 4, !tbaa !55
  %mul = fmul float %62, %63
  %64 = load float*, float** %pb.addr, align 4, !tbaa !2
  %arrayidx63 = getelementptr inbounds float, float* %64, i32 0
  %65 = load float, float* %arrayidx63, align 4, !tbaa !6
  %add = fadd float %65, %mul
  store float %add, float* %arrayidx63, align 4, !tbaa !6
  %m_normalOnBInWorld64 = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %gjkOutput, i32 0, i32 1
  %call65 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_normalOnBInWorld64)
  %arrayidx66 = getelementptr inbounds float, float* %call65, i32 1
  %66 = load float, float* %arrayidx66, align 4, !tbaa !6
  %m_distance67 = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %gjkOutput, i32 0, i32 3
  %67 = load float, float* %m_distance67, align 4, !tbaa !55
  %mul68 = fmul float %66, %67
  %68 = load float*, float** %pb.addr, align 4, !tbaa !2
  %arrayidx69 = getelementptr inbounds float, float* %68, i32 1
  %69 = load float, float* %arrayidx69, align 4, !tbaa !6
  %add70 = fadd float %69, %mul68
  store float %add70, float* %arrayidx69, align 4, !tbaa !6
  %m_normalOnBInWorld71 = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %gjkOutput, i32 0, i32 1
  %call72 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_normalOnBInWorld71)
  %arrayidx73 = getelementptr inbounds float, float* %call72, i32 2
  %70 = load float, float* %arrayidx73, align 4, !tbaa !6
  %m_distance74 = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %gjkOutput, i32 0, i32 3
  %71 = load float, float* %m_distance74, align 4, !tbaa !55
  %mul75 = fmul float %70, %71
  %72 = load float*, float** %pb.addr, align 4, !tbaa !2
  %arrayidx76 = getelementptr inbounds float, float* %72, i32 2
  %73 = load float, float* %arrayidx76, align 4, !tbaa !6
  %add77 = fadd float %73, %mul75
  store float %add77, float* %arrayidx76, align 4, !tbaa !6
  %m_normalOnBInWorld78 = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %gjkOutput, i32 0, i32 1
  %call79 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_normalOnBInWorld78)
  %arrayidx80 = getelementptr inbounds float, float* %call79, i32 0
  %74 = load float, float* %arrayidx80, align 4, !tbaa !6
  %75 = load float*, float** %normal.addr, align 4, !tbaa !2
  %arrayidx81 = getelementptr inbounds float, float* %75, i32 0
  store float %74, float* %arrayidx81, align 4, !tbaa !6
  %m_normalOnBInWorld82 = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %gjkOutput, i32 0, i32 1
  %call83 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_normalOnBInWorld82)
  %arrayidx84 = getelementptr inbounds float, float* %call83, i32 1
  %76 = load float, float* %arrayidx84, align 4, !tbaa !6
  %77 = load float*, float** %normal.addr, align 4, !tbaa !2
  %arrayidx85 = getelementptr inbounds float, float* %77, i32 1
  store float %76, float* %arrayidx85, align 4, !tbaa !6
  %m_normalOnBInWorld86 = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %gjkOutput, i32 0, i32 1
  %call87 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_normalOnBInWorld86)
  %arrayidx88 = getelementptr inbounds float, float* %call87, i32 2
  %78 = load float, float* %arrayidx88, align 4, !tbaa !6
  %79 = load float*, float** %normal.addr, align 4, !tbaa !2
  %arrayidx89 = getelementptr inbounds float, float* %79, i32 2
  store float %78, float* %arrayidx89, align 4, !tbaa !6
  %m_distance90 = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %gjkOutput, i32 0, i32 3
  %80 = load float, float* %m_distance90, align 4, !tbaa !55
  %conv = fpext float %80 to double
  store double %conv, double* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %init.end39
  store double -1.000000e+00, double* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %81 = bitcast %class.btTransform* %tr to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %81) #6
  %82 = bitcast %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input to i8*
  call void @llvm.lifetime.end.p0i8(i64 132, i8* %82) #6
  %call93 = call %struct.btPointCollector* bitcast (%"struct.btDiscreteCollisionDetectorInterface::Result"* (%"struct.btDiscreteCollisionDetectorInterface::Result"*)* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev to %struct.btPointCollector* (%struct.btPointCollector*)*)(%struct.btPointCollector* %gjkOutput) #6
  %83 = bitcast %struct.btPointCollector* %gjkOutput to i8*
  call void @llvm.lifetime.end.p0i8(i64 44, i8* %83) #6
  %call96 = call %class.btGjkPairDetector* bitcast (%struct.btDiscreteCollisionDetectorInterface* (%struct.btDiscreteCollisionDetectorInterface*)* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev to %class.btGjkPairDetector* (%class.btGjkPairDetector*)*)(%class.btGjkPairDetector* %convexConvex) #6
  %84 = bitcast %class.btGjkPairDetector* %convexConvex to i8*
  call void @llvm.lifetime.end.p0i8(i64 80, i8* %84) #6
  %85 = bitcast %class.btConvexPenetrationDepthSolver** %Solver to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #6
  %call100 = call %class.btTriangleShape* bitcast (%class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD2Ev to %class.btTriangleShape* (%class.btTriangleShape*)*)(%class.btTriangleShape* %trishapeB) #6
  %86 = bitcast %class.btTriangleShape* %trishapeB to i8*
  call void @llvm.lifetime.end.p0i8(i64 104, i8* %86) #6
  %87 = bitcast %class.btVector3* %vq to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %87) #6
  %call104 = call %class.btTriangleShape* bitcast (%class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD2Ev to %class.btTriangleShape* (%class.btTriangleShape*)*)(%class.btTriangleShape* %trishapeA) #6
  %88 = bitcast %class.btTriangleShape* %trishapeA to i8*
  call void @llvm.lifetime.end.p0i8(i64 104, i8* %88) #6
  %89 = bitcast %class.btVector3* %vp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %89) #6
  %90 = load double, double* %retval, align 8
  ret double %90
}

define linkonce_odr hidden %class.btTriangleShape* @_ZN15btTriangleShapeC2ERK9btVector3S2_S2_(%class.btTriangleShape* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %p0, %class.btVector3* nonnull align 4 dereferenceable(16) %p1, %class.btVector3* nonnull align 4 dereferenceable(16) %p2) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btTriangleShape*, align 4
  %this.addr = alloca %class.btTriangleShape*, align 4
  %p0.addr = alloca %class.btVector3*, align 4
  %p1.addr = alloca %class.btVector3*, align 4
  %p2.addr = alloca %class.btVector3*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %p0, %class.btVector3** %p0.addr, align 4, !tbaa !2
  store %class.btVector3* %p1, %class.btVector3** %p1.addr, align 4, !tbaa !2
  store %class.btVector3* %p2, %class.btVector3** %p2.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  store %class.btTriangleShape* %this1, %class.btTriangleShape** %retval, align 4
  %0 = bitcast %class.btTriangleShape* %this1 to %class.btPolyhedralConvexShape*
  %call = call %class.btPolyhedralConvexShape* @_ZN23btPolyhedralConvexShapeC2Ev(%class.btPolyhedralConvexShape* %0)
  %1 = bitcast %class.btTriangleShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [34 x i8*] }, { [34 x i8*] }* @_ZTV15btTriangleShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !18
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %2 = bitcast %class.btTriangleShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %2, i32 0, i32 1
  store i32 1, i32* %m_shapeType, align 4, !tbaa !44
  %3 = load %class.btVector3*, %class.btVector3** %p0.addr, align 4, !tbaa !2
  %m_vertices13 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices13, i32 0, i32 0
  %4 = bitcast %class.btVector3* %arrayidx to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !28
  %6 = load %class.btVector3*, %class.btVector3** %p1.addr, align 4, !tbaa !2
  %m_vertices14 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices14, i32 0, i32 1
  %7 = bitcast %class.btVector3* %arrayidx5 to i8*
  %8 = bitcast %class.btVector3* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !28
  %9 = load %class.btVector3*, %class.btVector3** %p2.addr, align 4, !tbaa !2
  %m_vertices16 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices16, i32 0, i32 2
  %10 = bitcast %class.btVector3* %arrayidx7 to i8*
  %11 = bitcast %class.btVector3* %9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !28
  %12 = load %class.btTriangleShape*, %class.btTriangleShape** %retval, align 4
  ret %class.btTriangleShape* %12
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN21btConvexInternalShape9setMarginEf(%class.btConvexInternalShape* %this, float %margin) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %margin.addr = alloca float, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  store float %margin, float* %margin.addr, align 4, !tbaa !6
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load float, float* %margin.addr, align 4, !tbaa !6
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  store float %0, float* %m_collisionMargin, align 4, !tbaa !46
  ret void
}

; Function Attrs: nounwind
declare i32 @__cxa_guard_acquire(i32*) #6

define linkonce_odr hidden %class.btVoronoiSimplexSolver* @_ZN22btVoronoiSimplexSolverC2Ev(%class.btVoronoiSimplexSolver* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btVoronoiSimplexSolver*, align 4
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  store %class.btVoronoiSimplexSolver* %this1, %class.btVoronoiSimplexSolver** %retval, align 4
  %m_simplexVectorW = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 5
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_simplexPointsP = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %array.begin2 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP, i32 0, i32 0
  %arrayctor.end3 = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin2, i32 5
  br label %arrayctor.loop4

arrayctor.loop4:                                  ; preds = %arrayctor.loop4, %arrayctor.cont
  %arrayctor.cur5 = phi %class.btVector3* [ %array.begin2, %arrayctor.cont ], [ %arrayctor.next7, %arrayctor.loop4 ]
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur5)
  %arrayctor.next7 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur5, i32 1
  %arrayctor.done8 = icmp eq %class.btVector3* %arrayctor.next7, %arrayctor.end3
  br i1 %arrayctor.done8, label %arrayctor.cont9, label %arrayctor.loop4

arrayctor.cont9:                                  ; preds = %arrayctor.loop4
  %m_simplexPointsQ = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %array.begin10 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ, i32 0, i32 0
  %arrayctor.end11 = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin10, i32 5
  br label %arrayctor.loop12

arrayctor.loop12:                                 ; preds = %arrayctor.loop12, %arrayctor.cont9
  %arrayctor.cur13 = phi %class.btVector3* [ %array.begin10, %arrayctor.cont9 ], [ %arrayctor.next15, %arrayctor.loop12 ]
  %call14 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur13)
  %arrayctor.next15 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur13, i32 1
  %arrayctor.done16 = icmp eq %class.btVector3* %arrayctor.next15, %arrayctor.end11
  br i1 %arrayctor.done16, label %arrayctor.cont17, label %arrayctor.loop12

arrayctor.cont17:                                 ; preds = %arrayctor.loop12
  %m_cachedP1 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 4
  %call18 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_cachedP1)
  %m_cachedP2 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 5
  %call19 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_cachedP2)
  %m_cachedV = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 6
  %call20 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_cachedV)
  %m_lastW = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 7
  %call21 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_lastW)
  %m_equalVertexThreshold = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 8
  store float 0x3F1A36E2E0000000, float* %m_equalVertexThreshold, align 4, !tbaa !56
  %m_cachedBC = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %call22 = call %struct.btSubSimplexClosestResult* @_ZN25btSubSimplexClosestResultC2Ev(%struct.btSubSimplexClosestResult* %m_cachedBC)
  %0 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %retval, align 4
  ret %class.btVoronoiSimplexSolver* %0
}

; Function Attrs: nounwind
declare void @__cxa_guard_release(i32*) #6

declare void @_ZN22btVoronoiSimplexSolver5resetEv(%class.btVoronoiSimplexSolver*) #2

; Function Attrs: nounwind
define linkonce_odr hidden %class.btGjkEpaPenetrationDepthSolver* @_ZN30btGjkEpaPenetrationDepthSolverC2Ev(%class.btGjkEpaPenetrationDepthSolver* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btGjkEpaPenetrationDepthSolver*, align 4
  store %class.btGjkEpaPenetrationDepthSolver* %this, %class.btGjkEpaPenetrationDepthSolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGjkEpaPenetrationDepthSolver*, %class.btGjkEpaPenetrationDepthSolver** %this.addr, align 4
  %0 = bitcast %class.btGjkEpaPenetrationDepthSolver* %this1 to %class.btConvexPenetrationDepthSolver*
  %call = call %class.btConvexPenetrationDepthSolver* @_ZN30btConvexPenetrationDepthSolverC2Ev(%class.btConvexPenetrationDepthSolver* %0) #6
  %1 = bitcast %class.btGjkEpaPenetrationDepthSolver* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV30btGjkEpaPenetrationDepthSolver, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !18
  ret %class.btGjkEpaPenetrationDepthSolver* %this1
}

define internal void @__cxx_global_array_dtor(i8* %0) #0 {
entry:
  %.addr = alloca i8*, align 4
  store i8* %0, i8** %.addr, align 4, !tbaa !2
  %call = call %class.btGjkEpaPenetrationDepthSolver* bitcast (%class.btConvexPenetrationDepthSolver* (%class.btConvexPenetrationDepthSolver*)* @_ZN30btConvexPenetrationDepthSolverD2Ev to %class.btGjkEpaPenetrationDepthSolver* (%class.btGjkEpaPenetrationDepthSolver*)*)(%class.btGjkEpaPenetrationDepthSolver* @_ZZ15plNearestPointsE7Solver0) #6
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btConvexPenetrationDepthSolver* @_ZN30btConvexPenetrationDepthSolverD2Ev(%class.btConvexPenetrationDepthSolver* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btConvexPenetrationDepthSolver*, align 4
  store %class.btConvexPenetrationDepthSolver* %this, %class.btConvexPenetrationDepthSolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %this.addr, align 4
  ret %class.btConvexPenetrationDepthSolver* %this1
}

; Function Attrs: nounwind
declare i32 @__cxa_atexit(void (i8*)*, i8*, i8*) #6

define internal void @__cxx_global_array_dtor.1(i8* %0) #0 {
entry:
  %.addr = alloca i8*, align 4
  store i8* %0, i8** %.addr, align 4, !tbaa !2
  %call = call %class.btMinkowskiPenetrationDepthSolver* bitcast (%class.btConvexPenetrationDepthSolver* (%class.btConvexPenetrationDepthSolver*)* @_ZN30btConvexPenetrationDepthSolverD2Ev to %class.btMinkowskiPenetrationDepthSolver* (%class.btMinkowskiPenetrationDepthSolver*)*)(%class.btMinkowskiPenetrationDepthSolver* bitcast ({ i8** }* @_ZZ15plNearestPointsE7Solver1 to %class.btMinkowskiPenetrationDepthSolver*)) #6
  ret void
}

declare %class.btGjkPairDetector* @_ZN17btGjkPairDetectorC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver(%class.btGjkPairDetector* returned, %class.btConvexShape*, %class.btConvexShape*, %class.btVoronoiSimplexSolver*, %class.btConvexPenetrationDepthSolver*) unnamed_addr #2

define linkonce_odr hidden %struct.btPointCollector* @_ZN16btPointCollectorC2Ev(%struct.btPointCollector* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %struct.btPointCollector*, align 4
  store %struct.btPointCollector* %this, %struct.btPointCollector** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btPointCollector*, %struct.btPointCollector** %this.addr, align 4
  %0 = bitcast %struct.btPointCollector* %this1 to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  %call = call %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultC2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* %0) #6
  %1 = bitcast %struct.btPointCollector* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV16btPointCollector, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !18
  %m_normalOnBInWorld = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_normalOnBInWorld)
  %m_pointInWorld = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_pointInWorld)
  %m_distance = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %this1, i32 0, i32 3
  store float 0x43ABC16D60000000, float* %m_distance, align 4, !tbaa !55
  %m_hasResult = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %this1, i32 0, i32 4
  store i8 0, i8* %m_hasResult, align 4, !tbaa !52
  ret %struct.btPointCollector* %this1
}

define linkonce_odr hidden %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* @_ZN36btDiscreteCollisionDetectorInterface17ClosestPointInputC2Ev(%"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %this.addr, align 4
  %m_transformA = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this1, i32 0, i32 0
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_transformA)
  %m_transformB = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this1, i32 0, i32 1
  %call2 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_transformB)
  %m_maximumDistanceSquared = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this1, i32 0, i32 2
  store float 0x43ABC16D60000000, float* %m_maximumDistanceSquared, align 4, !tbaa !61
  ret %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #5 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !28
  ret %class.btTransform* %this1
}

declare void @_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb(%class.btGjkPairDetector*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* nonnull align 4 dereferenceable(132), %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4), %class.btIDebugDraw*, i1 zeroext) unnamed_addr #2

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev(%struct.btDiscreteCollisionDetectorInterface* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %struct.btDiscreteCollisionDetectorInterface*, align 4
  store %struct.btDiscreteCollisionDetectorInterface* %this, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btDiscreteCollisionDetectorInterface*, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  ret %struct.btDiscreteCollisionDetectorInterface* %this1
}

; Function Attrs: nounwind
declare %class.btPolyhedralConvexShape* @_ZN23btPolyhedralConvexShapeD2Ev(%class.btPolyhedralConvexShape* returned) unnamed_addr #7

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

define linkonce_odr hidden void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !6
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !6
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !6
  %3 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !6
  %4 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  store float 1.000000e+00, float* %ref.tmp5, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !6
  %6 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !6
  %7 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  store float 0.000000e+00, float* %ref.tmp8, align 4, !tbaa !6
  %8 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  store float 1.000000e+00, float* %ref.tmp9, align 4, !tbaa !6
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %9 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #6
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  %11 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  %12 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #6
  %13 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #6
  %14 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #6
  %15 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !6
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !6
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !6
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !6
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !6
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !6
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !6
  ret void
}

define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

declare %class.btConvexInternalShape* @_ZN21btConvexInternalShapeC2Ev(%class.btConvexInternalShape* returned) unnamed_addr #2

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector34setXEf(%class.btVector3* %this, float %_x) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float %_x, float* %_x.addr, align 4, !tbaa !6
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float, float* %_x.addr, align 4, !tbaa !6
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %0, float* %arrayidx, align 4, !tbaa !6
  ret void
}

define linkonce_odr hidden void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %xs = alloca float, align 4
  %ys = alloca float, align 4
  %zs = alloca float, align 4
  %wx = alloca float, align 4
  %wy = alloca float, align 4
  %wz = alloca float, align 4
  %xx = alloca float, align 4
  %xy = alloca float, align 4
  %xz = alloca float, align 4
  %yy = alloca float, align 4
  %yz = alloca float, align 4
  %zz = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp34 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast float* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %1)
  store float %call, float* %d, align 4, !tbaa !6
  %2 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load float, float* %d, align 4, !tbaa !6
  %div = fdiv float 2.000000e+00, %3
  store float %div, float* %s, align 4, !tbaa !6
  %4 = bitcast float* %xs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %6 = bitcast %class.btQuaternion* %5 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %6)
  %7 = load float, float* %call2, align 4, !tbaa !6
  %8 = load float, float* %s, align 4, !tbaa !6
  %mul = fmul float %7, %8
  store float %mul, float* %xs, align 4, !tbaa !6
  %9 = bitcast float* %ys to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %11)
  %12 = load float, float* %call3, align 4, !tbaa !6
  %13 = load float, float* %s, align 4, !tbaa !6
  %mul4 = fmul float %12, %13
  store float %mul4, float* %ys, align 4, !tbaa !6
  %14 = bitcast float* %zs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  %15 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %16 = bitcast %class.btQuaternion* %15 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %16)
  %17 = load float, float* %call5, align 4, !tbaa !6
  %18 = load float, float* %s, align 4, !tbaa !6
  %mul6 = fmul float %17, %18
  store float %mul6, float* %zs, align 4, !tbaa !6
  %19 = bitcast float* %wx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #6
  %20 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %21 = bitcast %class.btQuaternion* %20 to %class.btQuadWord*
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %21)
  %22 = load float, float* %call7, align 4, !tbaa !6
  %23 = load float, float* %xs, align 4, !tbaa !6
  %mul8 = fmul float %22, %23
  store float %mul8, float* %wx, align 4, !tbaa !6
  %24 = bitcast float* %wy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #6
  %25 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %26 = bitcast %class.btQuaternion* %25 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %26)
  %27 = load float, float* %call9, align 4, !tbaa !6
  %28 = load float, float* %ys, align 4, !tbaa !6
  %mul10 = fmul float %27, %28
  store float %mul10, float* %wy, align 4, !tbaa !6
  %29 = bitcast float* %wz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #6
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %31)
  %32 = load float, float* %call11, align 4, !tbaa !6
  %33 = load float, float* %zs, align 4, !tbaa !6
  %mul12 = fmul float %32, %33
  store float %mul12, float* %wz, align 4, !tbaa !6
  %34 = bitcast float* %xx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #6
  %35 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %36 = bitcast %class.btQuaternion* %35 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %36)
  %37 = load float, float* %call13, align 4, !tbaa !6
  %38 = load float, float* %xs, align 4, !tbaa !6
  %mul14 = fmul float %37, %38
  store float %mul14, float* %xx, align 4, !tbaa !6
  %39 = bitcast float* %xy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #6
  %40 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %41 = bitcast %class.btQuaternion* %40 to %class.btQuadWord*
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %41)
  %42 = load float, float* %call15, align 4, !tbaa !6
  %43 = load float, float* %ys, align 4, !tbaa !6
  %mul16 = fmul float %42, %43
  store float %mul16, float* %xy, align 4, !tbaa !6
  %44 = bitcast float* %xz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #6
  %45 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %46 = bitcast %class.btQuaternion* %45 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %46)
  %47 = load float, float* %call17, align 4, !tbaa !6
  %48 = load float, float* %zs, align 4, !tbaa !6
  %mul18 = fmul float %47, %48
  store float %mul18, float* %xz, align 4, !tbaa !6
  %49 = bitcast float* %yy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #6
  %50 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %51 = bitcast %class.btQuaternion* %50 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %51)
  %52 = load float, float* %call19, align 4, !tbaa !6
  %53 = load float, float* %ys, align 4, !tbaa !6
  %mul20 = fmul float %52, %53
  store float %mul20, float* %yy, align 4, !tbaa !6
  %54 = bitcast float* %yz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #6
  %55 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %56 = bitcast %class.btQuaternion* %55 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %56)
  %57 = load float, float* %call21, align 4, !tbaa !6
  %58 = load float, float* %zs, align 4, !tbaa !6
  %mul22 = fmul float %57, %58
  store float %mul22, float* %yz, align 4, !tbaa !6
  %59 = bitcast float* %zz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #6
  %60 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %61 = bitcast %class.btQuaternion* %60 to %class.btQuadWord*
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %61)
  %62 = load float, float* %call23, align 4, !tbaa !6
  %63 = load float, float* %zs, align 4, !tbaa !6
  %mul24 = fmul float %62, %63
  store float %mul24, float* %zz, align 4, !tbaa !6
  %64 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #6
  %65 = load float, float* %yy, align 4, !tbaa !6
  %66 = load float, float* %zz, align 4, !tbaa !6
  %add = fadd float %65, %66
  %sub = fsub float 1.000000e+00, %add
  store float %sub, float* %ref.tmp, align 4, !tbaa !6
  %67 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #6
  %68 = load float, float* %xy, align 4, !tbaa !6
  %69 = load float, float* %wz, align 4, !tbaa !6
  %sub26 = fsub float %68, %69
  store float %sub26, float* %ref.tmp25, align 4, !tbaa !6
  %70 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #6
  %71 = load float, float* %xz, align 4, !tbaa !6
  %72 = load float, float* %wy, align 4, !tbaa !6
  %add28 = fadd float %71, %72
  store float %add28, float* %ref.tmp27, align 4, !tbaa !6
  %73 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #6
  %74 = load float, float* %xy, align 4, !tbaa !6
  %75 = load float, float* %wz, align 4, !tbaa !6
  %add30 = fadd float %74, %75
  store float %add30, float* %ref.tmp29, align 4, !tbaa !6
  %76 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #6
  %77 = load float, float* %xx, align 4, !tbaa !6
  %78 = load float, float* %zz, align 4, !tbaa !6
  %add32 = fadd float %77, %78
  %sub33 = fsub float 1.000000e+00, %add32
  store float %sub33, float* %ref.tmp31, align 4, !tbaa !6
  %79 = bitcast float* %ref.tmp34 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #6
  %80 = load float, float* %yz, align 4, !tbaa !6
  %81 = load float, float* %wx, align 4, !tbaa !6
  %sub35 = fsub float %80, %81
  store float %sub35, float* %ref.tmp34, align 4, !tbaa !6
  %82 = bitcast float* %ref.tmp36 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #6
  %83 = load float, float* %xz, align 4, !tbaa !6
  %84 = load float, float* %wy, align 4, !tbaa !6
  %sub37 = fsub float %83, %84
  store float %sub37, float* %ref.tmp36, align 4, !tbaa !6
  %85 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %85) #6
  %86 = load float, float* %yz, align 4, !tbaa !6
  %87 = load float, float* %wx, align 4, !tbaa !6
  %add39 = fadd float %86, %87
  store float %add39, float* %ref.tmp38, align 4, !tbaa !6
  %88 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #6
  %89 = load float, float* %xx, align 4, !tbaa !6
  %90 = load float, float* %yy, align 4, !tbaa !6
  %add41 = fadd float %89, %90
  %sub42 = fsub float 1.000000e+00, %add41
  store float %sub42, float* %ref.tmp40, align 4, !tbaa !6
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp34, float* nonnull align 4 dereferenceable(4) %ref.tmp36, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp40)
  %91 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #6
  %92 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #6
  %93 = bitcast float* %ref.tmp36 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #6
  %94 = bitcast float* %ref.tmp34 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #6
  %95 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #6
  %96 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #6
  %97 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #6
  %98 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #6
  %99 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #6
  %100 = bitcast float* %zz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #6
  %101 = bitcast float* %yz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #6
  %102 = bitcast float* %yy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #6
  %103 = bitcast float* %xz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #6
  %104 = bitcast float* %xy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #6
  %105 = bitcast float* %xx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #6
  %106 = bitcast float* %wz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #6
  %107 = bitcast float* %wy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #6
  %108 = bitcast float* %wx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #6
  %109 = bitcast float* %zs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #6
  %110 = bitcast float* %ys to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #6
  %111 = bitcast float* %xs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #6
  %112 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #6
  %113 = bitcast float* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #6
  ret void
}

define linkonce_odr hidden float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

define linkonce_odr hidden float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !6
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call, align 4, !tbaa !6
  %mul = fmul float %1, %4
  %5 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %5, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %6 = load float, float* %arrayidx3, align 4, !tbaa !6
  %7 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %8 = bitcast %class.btQuaternion* %7 to %class.btQuadWord*
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %8)
  %9 = load float, float* %call4, align 4, !tbaa !6
  %mul5 = fmul float %6, %9
  %add = fadd float %mul, %mul5
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %11 = load float, float* %arrayidx7, align 4, !tbaa !6
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %13)
  %14 = load float, float* %call8, align 4, !tbaa !6
  %mul9 = fmul float %11, %14
  %add10 = fadd float %add, %mul9
  %15 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats11 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %15, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 3
  %16 = load float, float* %arrayidx12, align 4, !tbaa !6
  %17 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %18 = bitcast %class.btQuaternion* %17 to %class.btQuadWord*
  %m_floats13 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %18, i32 0, i32 0
  %arrayidx14 = getelementptr inbounds [4 x float], [4 x float]* %m_floats13, i32 0, i32 3
  %19 = load float, float* %arrayidx14, align 4, !tbaa !6
  %mul15 = fmul float %16, %19
  %add16 = fadd float %add10, %mul15
  ret float %add16
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  store float* %_w, float** %_w.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !6
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !6
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !6
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !6
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !6
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !6
  %6 = load float*, float** %_w.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !6
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4, !tbaa !6
  ret %class.btQuadWord* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  ret %class.btQuadWord* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z5btCosf(float %x) #4 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !6
  %0 = load float, float* %x.addr, align 4, !tbaa !6
  %1 = call float @llvm.cos.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z5btSinf(float %x) #4 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !6
  %0 = load float, float* %x.addr, align 4, !tbaa !6
  %1 = call float @llvm.sin.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) #4 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  store float* %_w, float** %_w.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !6
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !6
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !6
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !6
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !6
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !6
  %6 = load float*, float** %_w.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !6
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4, !tbaa !6
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.cos.f32(float) #8

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sin.f32(float) #8

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #5 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !28
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !28
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !28
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN11btMatrix3x322setFromOpenGLSubMatrixEPKf(%class.btMatrix3x3* %this, float* %m) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %m.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %m, float** %m.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %m.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds float, float* %0, i32 0
  %1 = load float*, float** %m.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds float, float* %1, i32 4
  %2 = load float*, float** %m.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds float, float* %2, i32 8
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx2, float* nonnull align 4 dereferenceable(4) %arrayidx3, float* nonnull align 4 dereferenceable(4) %arrayidx4)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %3 = load float*, float** %m.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds float, float* %3, i32 1
  %4 = load float*, float** %m.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds float, float* %4, i32 5
  %5 = load float*, float** %m.addr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds float, float* %5, i32 9
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx6, float* nonnull align 4 dereferenceable(4) %arrayidx7, float* nonnull align 4 dereferenceable(4) %arrayidx8, float* nonnull align 4 dereferenceable(4) %arrayidx9)
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %6 = load float*, float** %m.addr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds float, float* %6, i32 2
  %7 = load float*, float** %m.addr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds float, float* %7, i32 6
  %8 = load float*, float** %m.addr, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds float, float* %8, i32 10
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx11, float* nonnull align 4 dereferenceable(4) %arrayidx12, float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %arrayidx14)
  ret void
}

define linkonce_odr hidden void @_ZNK11btMatrix3x318getOpenGLSubMatrixEPf(%class.btMatrix3x3* %this, float* %m) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %m.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %m, float** %m.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !6
  %1 = load float*, float** %m.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds float, float* %1, i32 0
  store float %0, float* %arrayidx2, align 4, !tbaa !6
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %2 = load float, float* %call5, align 4, !tbaa !6
  %3 = load float*, float** %m.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds float, float* %3, i32 1
  store float %2, float* %arrayidx6, align 4, !tbaa !6
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 2
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx8)
  %4 = load float, float* %call9, align 4, !tbaa !6
  %5 = load float*, float** %m.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds float, float* %5, i32 2
  store float %4, float* %arrayidx10, align 4, !tbaa !6
  %6 = load float*, float** %m.addr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds float, float* %6, i32 3
  store float 0.000000e+00, float* %arrayidx11, align 4, !tbaa !6
  %m_el12 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el12, i32 0, i32 0
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx13)
  %7 = load float, float* %call14, align 4, !tbaa !6
  %8 = load float*, float** %m.addr, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds float, float* %8, i32 4
  store float %7, float* %arrayidx15, align 4, !tbaa !6
  %m_el16 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx17 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el16, i32 0, i32 1
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx17)
  %9 = load float, float* %call18, align 4, !tbaa !6
  %10 = load float*, float** %m.addr, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds float, float* %10, i32 5
  store float %9, float* %arrayidx19, align 4, !tbaa !6
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 2
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx21)
  %11 = load float, float* %call22, align 4, !tbaa !6
  %12 = load float*, float** %m.addr, align 4, !tbaa !2
  %arrayidx23 = getelementptr inbounds float, float* %12, i32 6
  store float %11, float* %arrayidx23, align 4, !tbaa !6
  %13 = load float*, float** %m.addr, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds float, float* %13, i32 7
  store float 0.000000e+00, float* %arrayidx24, align 4, !tbaa !6
  %m_el25 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx26 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el25, i32 0, i32 0
  %call27 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx26)
  %14 = load float, float* %call27, align 4, !tbaa !6
  %15 = load float*, float** %m.addr, align 4, !tbaa !2
  %arrayidx28 = getelementptr inbounds float, float* %15, i32 8
  store float %14, float* %arrayidx28, align 4, !tbaa !6
  %m_el29 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el29, i32 0, i32 1
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx30)
  %16 = load float, float* %call31, align 4, !tbaa !6
  %17 = load float*, float** %m.addr, align 4, !tbaa !2
  %arrayidx32 = getelementptr inbounds float, float* %17, i32 9
  store float %16, float* %arrayidx32, align 4, !tbaa !6
  %m_el33 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx34 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el33, i32 0, i32 2
  %call35 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx34)
  %18 = load float, float* %call35, align 4, !tbaa !6
  %19 = load float*, float** %m.addr, align 4, !tbaa !2
  %arrayidx36 = getelementptr inbounds float, float* %19, i32 10
  store float %18, float* %arrayidx36, align 4, !tbaa !6
  %20 = load float*, float** %m.addr, align 4, !tbaa !2
  %arrayidx37 = getelementptr inbounds float, float* %20, i32 11
  store float 0.000000e+00, float* %arrayidx37, align 4, !tbaa !6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

define linkonce_odr hidden void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %trace = alloca float, align 4
  %temp = alloca [4 x float], align 16
  %s = alloca float, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %s64 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast float* %trace to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %1 = load float, float* %call, align 4, !tbaa !6
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx3)
  %2 = load float, float* %call4, align 4, !tbaa !6
  %add = fadd float %1, %2
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx6)
  %3 = load float, float* %call7, align 4, !tbaa !6
  %add8 = fadd float %add, %3
  store float %add8, float* %trace, align 4, !tbaa !6
  %4 = bitcast [4 x float]* %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #6
  %5 = load float, float* %trace, align 4, !tbaa !6
  %cmp = fcmp ogt float %5, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %6 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load float, float* %trace, align 4, !tbaa !6
  %add9 = fadd float %7, 1.000000e+00
  %call10 = call float @_Z6btSqrtf(float %add9)
  store float %call10, float* %s, align 4, !tbaa !6
  %8 = load float, float* %s, align 4, !tbaa !6
  %mul = fmul float %8, 5.000000e-01
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  store float %mul, float* %arrayidx11, align 4, !tbaa !6
  %9 = load float, float* %s, align 4, !tbaa !6
  %div = fdiv float 5.000000e-01, %9
  store float %div, float* %s, align 4, !tbaa !6
  %m_el12 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el12, i32 0, i32 2
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx13)
  %10 = load float, float* %call14, align 4, !tbaa !6
  %m_el15 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el15, i32 0, i32 1
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx16)
  %11 = load float, float* %call17, align 4, !tbaa !6
  %sub = fsub float %10, %11
  %12 = load float, float* %s, align 4, !tbaa !6
  %mul18 = fmul float %sub, %12
  %arrayidx19 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 0
  store float %mul18, float* %arrayidx19, align 16, !tbaa !6
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 0
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %13 = load float, float* %call22, align 4, !tbaa !6
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx24)
  %14 = load float, float* %call25, align 4, !tbaa !6
  %sub26 = fsub float %13, %14
  %15 = load float, float* %s, align 4, !tbaa !6
  %mul27 = fmul float %sub26, %15
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 1
  store float %mul27, float* %arrayidx28, align 4, !tbaa !6
  %m_el29 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el29, i32 0, i32 1
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx30)
  %16 = load float, float* %call31, align 4, !tbaa !6
  %m_el32 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx33 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el32, i32 0, i32 0
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx33)
  %17 = load float, float* %call34, align 4, !tbaa !6
  %sub35 = fsub float %16, %17
  %18 = load float, float* %s, align 4, !tbaa !6
  %mul36 = fmul float %sub35, %18
  %arrayidx37 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 2
  store float %mul36, float* %arrayidx37, align 8, !tbaa !6
  %19 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #6
  br label %if.end

if.else:                                          ; preds = %entry
  %20 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #6
  %m_el38 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx39 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el38, i32 0, i32 0
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx39)
  %21 = load float, float* %call40, align 4, !tbaa !6
  %m_el41 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx42 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el41, i32 0, i32 1
  %call43 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx42)
  %22 = load float, float* %call43, align 4, !tbaa !6
  %cmp44 = fcmp olt float %21, %22
  br i1 %cmp44, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else
  %m_el45 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx46 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el45, i32 0, i32 1
  %call47 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx46)
  %23 = load float, float* %call47, align 4, !tbaa !6
  %m_el48 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx49 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el48, i32 0, i32 2
  %call50 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx49)
  %24 = load float, float* %call50, align 4, !tbaa !6
  %cmp51 = fcmp olt float %23, %24
  %25 = zext i1 %cmp51 to i64
  %cond = select i1 %cmp51, i32 2, i32 1
  br label %cond.end

cond.false:                                       ; preds = %if.else
  %m_el52 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx53 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el52, i32 0, i32 0
  %call54 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx53)
  %26 = load float, float* %call54, align 4, !tbaa !6
  %m_el55 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx56 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el55, i32 0, i32 2
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx56)
  %27 = load float, float* %call57, align 4, !tbaa !6
  %cmp58 = fcmp olt float %26, %27
  %28 = zext i1 %cmp58 to i64
  %cond59 = select i1 %cmp58, i32 2, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond60 = phi i32 [ %cond, %cond.true ], [ %cond59, %cond.false ]
  store i32 %cond60, i32* %i, align 4, !tbaa !48
  %29 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #6
  %30 = load i32, i32* %i, align 4, !tbaa !48
  %add61 = add nsw i32 %30, 1
  %rem = srem i32 %add61, 3
  store i32 %rem, i32* %j, align 4, !tbaa !48
  %31 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #6
  %32 = load i32, i32* %i, align 4, !tbaa !48
  %add62 = add nsw i32 %32, 2
  %rem63 = srem i32 %add62, 3
  store i32 %rem63, i32* %k, align 4, !tbaa !48
  %33 = bitcast float* %s64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #6
  %m_el65 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %34 = load i32, i32* %i, align 4, !tbaa !48
  %arrayidx66 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el65, i32 0, i32 %34
  %call67 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx66)
  %35 = load i32, i32* %i, align 4, !tbaa !48
  %arrayidx68 = getelementptr inbounds float, float* %call67, i32 %35
  %36 = load float, float* %arrayidx68, align 4, !tbaa !6
  %m_el69 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %37 = load i32, i32* %j, align 4, !tbaa !48
  %arrayidx70 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el69, i32 0, i32 %37
  %call71 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx70)
  %38 = load i32, i32* %j, align 4, !tbaa !48
  %arrayidx72 = getelementptr inbounds float, float* %call71, i32 %38
  %39 = load float, float* %arrayidx72, align 4, !tbaa !6
  %sub73 = fsub float %36, %39
  %m_el74 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %40 = load i32, i32* %k, align 4, !tbaa !48
  %arrayidx75 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el74, i32 0, i32 %40
  %call76 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx75)
  %41 = load i32, i32* %k, align 4, !tbaa !48
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 %41
  %42 = load float, float* %arrayidx77, align 4, !tbaa !6
  %sub78 = fsub float %sub73, %42
  %add79 = fadd float %sub78, 1.000000e+00
  %call80 = call float @_Z6btSqrtf(float %add79)
  store float %call80, float* %s64, align 4, !tbaa !6
  %43 = load float, float* %s64, align 4, !tbaa !6
  %mul81 = fmul float %43, 5.000000e-01
  %44 = load i32, i32* %i, align 4, !tbaa !48
  %arrayidx82 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %44
  store float %mul81, float* %arrayidx82, align 4, !tbaa !6
  %45 = load float, float* %s64, align 4, !tbaa !6
  %div83 = fdiv float 5.000000e-01, %45
  store float %div83, float* %s64, align 4, !tbaa !6
  %m_el84 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %46 = load i32, i32* %k, align 4, !tbaa !48
  %arrayidx85 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el84, i32 0, i32 %46
  %call86 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx85)
  %47 = load i32, i32* %j, align 4, !tbaa !48
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 %47
  %48 = load float, float* %arrayidx87, align 4, !tbaa !6
  %m_el88 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %49 = load i32, i32* %j, align 4, !tbaa !48
  %arrayidx89 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el88, i32 0, i32 %49
  %call90 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx89)
  %50 = load i32, i32* %k, align 4, !tbaa !48
  %arrayidx91 = getelementptr inbounds float, float* %call90, i32 %50
  %51 = load float, float* %arrayidx91, align 4, !tbaa !6
  %sub92 = fsub float %48, %51
  %52 = load float, float* %s64, align 4, !tbaa !6
  %mul93 = fmul float %sub92, %52
  %arrayidx94 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  store float %mul93, float* %arrayidx94, align 4, !tbaa !6
  %m_el95 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %53 = load i32, i32* %j, align 4, !tbaa !48
  %arrayidx96 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el95, i32 0, i32 %53
  %call97 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx96)
  %54 = load i32, i32* %i, align 4, !tbaa !48
  %arrayidx98 = getelementptr inbounds float, float* %call97, i32 %54
  %55 = load float, float* %arrayidx98, align 4, !tbaa !6
  %m_el99 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %56 = load i32, i32* %i, align 4, !tbaa !48
  %arrayidx100 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el99, i32 0, i32 %56
  %call101 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx100)
  %57 = load i32, i32* %j, align 4, !tbaa !48
  %arrayidx102 = getelementptr inbounds float, float* %call101, i32 %57
  %58 = load float, float* %arrayidx102, align 4, !tbaa !6
  %add103 = fadd float %55, %58
  %59 = load float, float* %s64, align 4, !tbaa !6
  %mul104 = fmul float %add103, %59
  %60 = load i32, i32* %j, align 4, !tbaa !48
  %arrayidx105 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %60
  store float %mul104, float* %arrayidx105, align 4, !tbaa !6
  %m_el106 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %61 = load i32, i32* %k, align 4, !tbaa !48
  %arrayidx107 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el106, i32 0, i32 %61
  %call108 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx107)
  %62 = load i32, i32* %i, align 4, !tbaa !48
  %arrayidx109 = getelementptr inbounds float, float* %call108, i32 %62
  %63 = load float, float* %arrayidx109, align 4, !tbaa !6
  %m_el110 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %64 = load i32, i32* %i, align 4, !tbaa !48
  %arrayidx111 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el110, i32 0, i32 %64
  %call112 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx111)
  %65 = load i32, i32* %k, align 4, !tbaa !48
  %arrayidx113 = getelementptr inbounds float, float* %call112, i32 %65
  %66 = load float, float* %arrayidx113, align 4, !tbaa !6
  %add114 = fadd float %63, %66
  %67 = load float, float* %s64, align 4, !tbaa !6
  %mul115 = fmul float %add114, %67
  %68 = load i32, i32* %k, align 4, !tbaa !48
  %arrayidx116 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %68
  store float %mul115, float* %arrayidx116, align 4, !tbaa !6
  %69 = bitcast float* %s64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #6
  %70 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #6
  %71 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #6
  %72 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #6
  br label %if.end

if.end:                                           ; preds = %cond.end, %if.then
  %73 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %74 = bitcast %class.btQuaternion* %73 to %class.btQuadWord*
  %arrayidx117 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 0
  %arrayidx118 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 1
  %arrayidx119 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 2
  %arrayidx120 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %74, float* nonnull align 4 dereferenceable(4) %arrayidx117, float* nonnull align 4 dereferenceable(4) %arrayidx118, float* nonnull align 4 dereferenceable(4) %arrayidx119, float* nonnull align 4 dereferenceable(4) %arrayidx120)
  %75 = bitcast [4 x float]* %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %75) #6
  %76 = bitcast float* %trace to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #4 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !6
  %0 = load float, float* %y.addr, align 4, !tbaa !6
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #8

declare %class.btPolyhedralConvexShape* @_ZN23btPolyhedralConvexShapeC2Ev(%class.btPolyhedralConvexShape* returned) unnamed_addr #2

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN15btTriangleShapeD0Ev(%class.btTriangleShape* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %call = call %class.btTriangleShape* bitcast (%class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD2Ev to %class.btTriangleShape* (%class.btTriangleShape*)*)(%class.btTriangleShape* %this1) #6
  %0 = bitcast %class.btTriangleShape* %this1 to i8*
  call void @_ZN15btTriangleShapedlEPv(i8* %0) #6
  ret void
}

define linkonce_odr hidden void @_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_(%class.btTriangleShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = bitcast %class.btTriangleShape* %this1 to %class.btConvexInternalShape*
  %1 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %3 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %4 = bitcast %class.btConvexInternalShape* %0 to void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %4, align 4, !tbaa !18
  %vfn = getelementptr inbounds void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 20
  %5 = load void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %5(%class.btConvexInternalShape* %0, %class.btTransform* nonnull align 4 dereferenceable(64) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  ret void
}

declare void @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf(%class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4)) unnamed_addr #2

declare float @_ZNK16btCollisionShape20getAngularMotionDiscEv(%class.btCollisionShape*) unnamed_addr #2

declare float @_ZNK16btCollisionShape27getContactBreakingThresholdEf(%class.btCollisionShape*, float) unnamed_addr #2

declare void @_ZN21btConvexInternalShape15setLocalScalingERK9btVector3(%class.btConvexInternalShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #2

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btConvexInternalShape15getLocalScalingEv(%class.btConvexInternalShape* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 1
  ret %class.btVector3* %m_localScaling
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3(%class.btTriangleShape* %this, float %mass, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %mass.addr = alloca float, align 4
  %inertia.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store float %mass, float* %mass.addr, align 4, !tbaa !6
  store %class.btVector3* %inertia, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !6
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !6
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !6
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %0, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %4 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #6
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  %6 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #6
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden i8* @_ZNK15btTriangleShape7getNameEv(%class.btTriangleShape* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  ret i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str, i32 0, i32 0)
}

define linkonce_odr hidden void @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btCollisionShape* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !6
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store float 1.000000e+00, float* %ref.tmp2, align 4, !tbaa !6
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  store float 1.000000e+00, float* %ref.tmp3, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #6
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #6
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK21btConvexInternalShape9getMarginEv(%class.btConvexInternalShape* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  %0 = load float, float* %m_collisionMargin, align 4, !tbaa !46
  ret float %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv(%class.btConvexInternalShape* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  ret i32 52
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer(%class.btConvexInternalShape* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %shapeData = alloca %struct.btConvexInternalShapeData*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = bitcast %struct.btConvexInternalShapeData** %shapeData to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i8*, i8** %dataBuffer.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.btConvexInternalShapeData*
  store %struct.btConvexInternalShapeData* %2, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %3 = bitcast %class.btConvexInternalShape* %this1 to %class.btCollisionShape*
  %4 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_collisionShapeData = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %4, i32 0, i32 0
  %5 = bitcast %struct.btCollisionShapeData* %m_collisionShapeData to i8*
  %6 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %call = call i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape* %3, i8* %5, %class.btSerializer* %6)
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 2
  %7 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_implicitShapeDimensions2 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %7, i32 0, i32 2
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_implicitShapeDimensions, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_implicitShapeDimensions2)
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 1
  %8 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_localScaling3 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %8, i32 0, i32 1
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_localScaling, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_localScaling3)
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  %9 = load float, float* %m_collisionMargin, align 4, !tbaa !46
  %10 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_collisionMargin4 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %10, i32 0, i32 3
  store float %9, float* %m_collisionMargin4, align 4, !tbaa !63
  %11 = bitcast %struct.btConvexInternalShapeData** %shapeData to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  ret i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.2, i32 0, i32 0)
}

declare void @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer(%class.btCollisionShape*, %class.btSerializer*) unnamed_addr #2

declare void @_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3(%class.btVector3* sret align 4, %class.btConvexInternalShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #2

define linkonce_odr hidden void @_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTriangleShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %dir) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %dir.addr = alloca %class.btVector3*, align 4
  %dots = alloca %class.btVector3, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %dir, %class.btVector3** %dir.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %dots to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %dir.addr, align 4, !tbaa !2
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  %m_vertices12 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices12, i32 0, i32 1
  %m_vertices14 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices14, i32 0, i32 2
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %dots, %class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx3, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx5)
  %m_vertices16 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %call = call i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %dots)
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices16, i32 0, i32 %call
  %2 = bitcast %class.btVector3* %agg.result to i8*
  %3 = bitcast %class.btVector3* %arrayidx7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !28
  %4 = bitcast %class.btVector3* %dots to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #6
  ret void
}

declare void @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_(%class.btConvexShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4), float* nonnull align 4 dereferenceable(4)) unnamed_addr #2

define linkonce_odr hidden void @_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i(%class.btTriangleShape* %this, %class.btVector3* %vectors, %class.btVector3* %supportVerticesOut, i32 %numVectors) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %vectors.addr = alloca %class.btVector3*, align 4
  %supportVerticesOut.addr = alloca %class.btVector3*, align 4
  %numVectors.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %dir = alloca %class.btVector3*, align 4
  %dots = alloca %class.btVector3, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %vectors, %class.btVector3** %vectors.addr, align 4, !tbaa !2
  store %class.btVector3* %supportVerticesOut, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  store i32 %numVectors, i32* %numVectors.addr, align 4, !tbaa !48
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %i, align 4, !tbaa !48
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !48
  %2 = load i32, i32* %numVectors.addr, align 4, !tbaa !48
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = bitcast %class.btVector3** %dir to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load %class.btVector3*, %class.btVector3** %vectors.addr, align 4, !tbaa !2
  %6 = load i32, i32* %i, align 4, !tbaa !48
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 %6
  store %class.btVector3* %arrayidx, %class.btVector3** %dir, align 4, !tbaa !2
  %7 = bitcast %class.btVector3* %dots to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #6
  %8 = load %class.btVector3*, %class.btVector3** %dir, align 4, !tbaa !2
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx2 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  %m_vertices13 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices13, i32 0, i32 1
  %m_vertices15 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices15, i32 0, i32 2
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %dots, %class.btVector3* %8, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx2, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx4, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx6)
  %m_vertices17 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %call = call i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %dots)
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices17, i32 0, i32 %call
  %9 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  %10 = load i32, i32* %i, align 4, !tbaa !48
  %arrayidx9 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 %10
  %11 = bitcast %class.btVector3* %arrayidx9 to i8*
  %12 = bitcast %class.btVector3* %arrayidx8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 16, i1 false), !tbaa.struct !28
  %13 = bitcast %class.btVector3* %dots to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %13) #6
  %14 = bitcast %class.btVector3** %dir to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %15 = load i32, i32* %i, align 4, !tbaa !48
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %i, align 4, !tbaa !48
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

declare void @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_(%class.btConvexInternalShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #2

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv(%class.btTriangleShape* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  ret i32 2
}

define linkonce_odr hidden void @_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3(%class.btTriangleShape* %this, i32 %index, %class.btVector3* nonnull align 4 dereferenceable(16) %penetrationVector) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %index.addr = alloca i32, align 4
  %penetrationVector.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !48
  store %class.btVector3* %penetrationVector, %class.btVector3** %penetrationVector.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %penetrationVector.addr, align 4, !tbaa !2
  call void @_ZNK15btTriangleShape10calcNormalER9btVector3(%class.btTriangleShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  %1 = load i32, i32* %index.addr, align 4, !tbaa !48
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  store float -1.000000e+00, float* %ref.tmp, align 4, !tbaa !6
  %3 = load %class.btVector3*, %class.btVector3** %penetrationVector.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %3, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %4 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

declare zeroext i1 @_ZN23btPolyhedralConvexShape28initializePolyhedralFeaturesEi(%class.btPolyhedralConvexShape*, i32) unnamed_addr #2

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK15btTriangleShape14getNumVerticesEv(%class.btTriangleShape* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  ret i32 3
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK15btTriangleShape11getNumEdgesEv(%class.btTriangleShape* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  ret i32 3
}

define linkonce_odr hidden void @_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_(%class.btTriangleShape* %this, i32 %i, %class.btVector3* nonnull align 4 dereferenceable(16) %pa, %class.btVector3* nonnull align 4 dereferenceable(16) %pb) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %i.addr = alloca i32, align 4
  %pa.addr = alloca %class.btVector3*, align 4
  %pb.addr = alloca %class.btVector3*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !48
  store %class.btVector3* %pa, %class.btVector3** %pa.addr, align 4, !tbaa !2
  store %class.btVector3* %pb, %class.btVector3** %pb.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = load i32, i32* %i.addr, align 4, !tbaa !48
  %1 = load %class.btVector3*, %class.btVector3** %pa.addr, align 4, !tbaa !2
  %2 = bitcast %class.btTriangleShape* %this1 to void (%class.btTriangleShape*, i32, %class.btVector3*)***
  %vtable = load void (%class.btTriangleShape*, i32, %class.btVector3*)**, void (%class.btTriangleShape*, i32, %class.btVector3*)*** %2, align 4, !tbaa !18
  %vfn = getelementptr inbounds void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vtable, i64 27
  %3 = load void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vfn, align 4
  call void %3(%class.btTriangleShape* %this1, i32 %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %4 = load i32, i32* %i.addr, align 4, !tbaa !48
  %add = add nsw i32 %4, 1
  %rem = srem i32 %add, 3
  %5 = load %class.btVector3*, %class.btVector3** %pb.addr, align 4, !tbaa !2
  %6 = bitcast %class.btTriangleShape* %this1 to void (%class.btTriangleShape*, i32, %class.btVector3*)***
  %vtable2 = load void (%class.btTriangleShape*, i32, %class.btVector3*)**, void (%class.btTriangleShape*, i32, %class.btVector3*)*** %6, align 4, !tbaa !18
  %vfn3 = getelementptr inbounds void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vtable2, i64 27
  %7 = load void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vfn3, align 4
  call void %7(%class.btTriangleShape* %this1, i32 %rem, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZNK15btTriangleShape9getVertexEiR9btVector3(%class.btTriangleShape* %this, i32 %index, %class.btVector3* nonnull align 4 dereferenceable(16) %vert) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %index.addr = alloca i32, align 4
  %vert.addr = alloca %class.btVector3*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !48
  store %class.btVector3* %vert, %class.btVector3** %vert.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %index.addr, align 4, !tbaa !48
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 %0
  %1 = load %class.btVector3*, %class.btVector3** %vert.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %1 to i8*
  %3 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !28
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK15btTriangleShape12getNumPlanesEv(%class.btTriangleShape* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  ret i32 1
}

define linkonce_odr hidden void @_ZNK15btTriangleShape8getPlaneER9btVector3S1_i(%class.btTriangleShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %planeNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %planeSupport, i32 %i) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %planeNormal.addr = alloca %class.btVector3*, align 4
  %planeSupport.addr = alloca %class.btVector3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %planeNormal, %class.btVector3** %planeNormal.addr, align 4, !tbaa !2
  store %class.btVector3* %planeSupport, %class.btVector3** %planeSupport.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !48
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = load i32, i32* %i.addr, align 4, !tbaa !48
  %1 = load %class.btVector3*, %class.btVector3** %planeNormal.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %planeSupport.addr, align 4, !tbaa !2
  %3 = bitcast %class.btTriangleShape* %this1 to void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)**, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*** %3, align 4, !tbaa !18
  %vfn = getelementptr inbounds void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)** %vtable, i64 31
  %4 = load void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %4(%class.btTriangleShape* %this1, i32 %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  ret void
}

define linkonce_odr hidden zeroext i1 @_ZNK15btTriangleShape8isInsideERK9btVector3f(%class.btTriangleShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %pt, float %tolerance) unnamed_addr #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btTriangleShape*, align 4
  %pt.addr = alloca %class.btVector3*, align 4
  %tolerance.addr = alloca float, align 4
  %normal = alloca %class.btVector3, align 4
  %dist = alloca float, align 4
  %planeconst = alloca float, align 4
  %i = alloca i32, align 4
  %pa = alloca %class.btVector3, align 4
  %pb = alloca %class.btVector3, align 4
  %edge = alloca %class.btVector3, align 4
  %edgeNormal = alloca %class.btVector3, align 4
  %dist9 = alloca float, align 4
  %edgeConst = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %pt, %class.btVector3** %pt.addr, align 4, !tbaa !2
  store float %tolerance, float* %tolerance.addr, align 4, !tbaa !6
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %normal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #6
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %normal)
  call void @_ZNK15btTriangleShape10calcNormalER9btVector3(%class.btTriangleShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  %1 = bitcast float* %dist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = load %class.btVector3*, %class.btVector3** %pt.addr, align 4, !tbaa !2
  %call2 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  store float %call2, float* %dist, align 4, !tbaa !6
  %3 = bitcast float* %planeconst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  store float %call3, float* %planeconst, align 4, !tbaa !6
  %4 = load float, float* %planeconst, align 4, !tbaa !6
  %5 = load float, float* %dist, align 4, !tbaa !6
  %sub = fsub float %5, %4
  store float %sub, float* %dist, align 4, !tbaa !6
  %6 = load float, float* %dist, align 4, !tbaa !6
  %7 = load float, float* %tolerance.addr, align 4, !tbaa !6
  %fneg = fneg float %7
  %cmp = fcmp oge float %6, %fneg
  br i1 %cmp, label %land.lhs.true, label %if.end22

land.lhs.true:                                    ; preds = %entry
  %8 = load float, float* %dist, align 4, !tbaa !6
  %9 = load float, float* %tolerance.addr, align 4, !tbaa !6
  %cmp4 = fcmp ole float %8, %9
  br i1 %cmp4, label %if.then, label %if.end22

if.then:                                          ; preds = %land.lhs.true
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  store i32 0, i32* %i, align 4, !tbaa !48
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %11 = load i32, i32* %i, align 4, !tbaa !48
  %cmp5 = icmp slt i32 %11, 3
  br i1 %cmp5, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %12 = bitcast %class.btVector3* %pa to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %12) #6
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pa)
  %13 = bitcast %class.btVector3* %pb to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #6
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pb)
  %14 = load i32, i32* %i, align 4, !tbaa !48
  %15 = bitcast %class.btTriangleShape* %this1 to void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)**, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*** %15, align 4, !tbaa !18
  %vfn = getelementptr inbounds void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)** %vtable, i64 26
  %16 = load void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %16(%class.btTriangleShape* %this1, i32 %14, %class.btVector3* nonnull align 4 dereferenceable(16) %pa, %class.btVector3* nonnull align 4 dereferenceable(16) %pb)
  %17 = bitcast %class.btVector3* %edge to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %17) #6
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %edge, %class.btVector3* nonnull align 4 dereferenceable(16) %pb, %class.btVector3* nonnull align 4 dereferenceable(16) %pa)
  %18 = bitcast %class.btVector3* %edgeNormal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %18) #6
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %edgeNormal, %class.btVector3* %edge, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %edgeNormal)
  %19 = bitcast float* %dist9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #6
  %20 = load %class.btVector3*, %class.btVector3** %pt.addr, align 4, !tbaa !2
  %call10 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %20, %class.btVector3* nonnull align 4 dereferenceable(16) %edgeNormal)
  store float %call10, float* %dist9, align 4, !tbaa !6
  %21 = bitcast float* %edgeConst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #6
  %call11 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %pa, %class.btVector3* nonnull align 4 dereferenceable(16) %edgeNormal)
  store float %call11, float* %edgeConst, align 4, !tbaa !6
  %22 = load float, float* %edgeConst, align 4, !tbaa !6
  %23 = load float, float* %dist9, align 4, !tbaa !6
  %sub12 = fsub float %23, %22
  store float %sub12, float* %dist9, align 4, !tbaa !6
  %24 = load float, float* %dist9, align 4, !tbaa !6
  %25 = load float, float* %tolerance.addr, align 4, !tbaa !6
  %fneg13 = fneg float %25
  %cmp14 = fcmp olt float %24, %fneg13
  br i1 %cmp14, label %if.then15, label %if.end

if.then15:                                        ; preds = %for.body
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then15
  %26 = bitcast float* %edgeConst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #6
  %27 = bitcast float* %dist9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #6
  %28 = bitcast %class.btVector3* %edgeNormal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %28) #6
  %29 = bitcast %class.btVector3* %edge to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #6
  %30 = bitcast %class.btVector3* %pb to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %30) #6
  %31 = bitcast %class.btVector3* %pa to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %31) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup21 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %32 = load i32, i32* %i, align 4, !tbaa !48
  %inc = add nsw i32 %32, 1
  store i32 %inc, i32* %i, align 4, !tbaa !48
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup21

cleanup21:                                        ; preds = %for.end, %cleanup
  %33 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #6
  br label %cleanup23

if.end22:                                         ; preds = %land.lhs.true, %entry
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup23

cleanup23:                                        ; preds = %if.end22, %cleanup21
  %34 = bitcast float* %planeconst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #6
  %35 = bitcast float* %dist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #6
  %36 = bitcast %class.btVector3* %normal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %36) #6
  %37 = load i1, i1* %retval, align 1
  ret i1 %37
}

define linkonce_odr hidden void @_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_(%class.btTriangleShape* %this, i32 %i, %class.btVector3* nonnull align 4 dereferenceable(16) %planeNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %planeSupport) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %i.addr = alloca i32, align 4
  %planeNormal.addr = alloca %class.btVector3*, align 4
  %planeSupport.addr = alloca %class.btVector3*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !48
  store %class.btVector3* %planeNormal, %class.btVector3** %planeNormal.addr, align 4, !tbaa !2
  store %class.btVector3* %planeSupport, %class.btVector3** %planeSupport.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %planeNormal.addr, align 4, !tbaa !2
  call void @_ZNK15btTriangleShape10calcNormalER9btVector3(%class.btTriangleShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  %1 = load %class.btVector3*, %class.btVector3** %planeSupport.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %1 to i8*
  %3 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !28
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN15btTriangleShapedlEPv(i8* %ptr) #4 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

declare i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape*, i8*, %class.btSerializer*) unnamed_addr #2

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %i, align 4, !tbaa !48
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !48
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %3 = load i32, i32* %i, align 4, !tbaa !48
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %3
  %4 = load float, float* %arrayidx, align 4, !tbaa !6
  %5 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %5, i32 0, i32 0
  %6 = load i32, i32* %i, align 4, !tbaa !48
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %6
  store float %4, float* %arrayidx3, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !48
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !48
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4, !tbaa !6
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !6
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !6
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #6
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !6
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %1 = load float, float* %arrayidx3, align 4, !tbaa !6
  %cmp = fcmp olt float %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %2 = load float, float* %arrayidx5, align 4, !tbaa !6
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %3 = load float, float* %arrayidx7, align 4, !tbaa !6
  %cmp8 = fcmp olt float %2, %3
  %4 = zext i1 %cmp8 to i64
  %cond = select i1 %cmp8, i32 2, i32 1
  br label %cond.end

cond.false:                                       ; preds = %entry
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 0
  %5 = load float, float* %arrayidx10, align 4, !tbaa !6
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %6 = load float, float* %arrayidx12, align 4, !tbaa !6
  %cmp13 = fcmp olt float %5, %6
  %7 = zext i1 %cmp13 to i64
  %cond14 = select i1 %cmp13, i32 2, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond15 = phi i32 [ %cond, %cond.true ], [ %cond14, %cond.false ]
  ret i32 %cond15
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !6
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !6
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !6
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !6
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !6
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !6
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

define linkonce_odr hidden void @_ZNK15btTriangleShape10calcNormalER9btVector3(%class.btTriangleShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %normal) #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %normal.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %normal, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #6
  %1 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #6
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 1
  %m_vertices13 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices13, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx4)
  %2 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #6
  %m_vertices16 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices16, i32 0, i32 2
  %m_vertices18 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices18, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx7, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx9)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %3 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %4 = bitcast %class.btVector3* %3 to i8*
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !28
  %6 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %6) #6
  %7 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %7) #6
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #6
  %9 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %9)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !6
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !6
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !6
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !6
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !6
  %6 = load float*, float** %s.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !6
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !6
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !6
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #5 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !6
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !6
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !6
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !6
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !6
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !6
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %1 = load float, float* %arrayidx, align 4, !tbaa !6
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %3 = load float, float* %arrayidx3, align 4, !tbaa !6
  %mul = fmul float %1, %3
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %4 = load float, float* %arrayidx5, align 4, !tbaa !6
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %6 = load float, float* %arrayidx7, align 4, !tbaa !6
  %mul8 = fmul float %4, %6
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4, !tbaa !6
  %7 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %8 = load float, float* %arrayidx11, align 4, !tbaa !6
  %9 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %10 = load float, float* %arrayidx13, align 4, !tbaa !6
  %mul14 = fmul float %8, %10
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %11 = load float, float* %arrayidx16, align 4, !tbaa !6
  %12 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %13 = load float, float* %arrayidx18, align 4, !tbaa !6
  %mul19 = fmul float %11, %13
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4, !tbaa !6
  %14 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %15 = load float, float* %arrayidx23, align 4, !tbaa !6
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %17 = load float, float* %arrayidx25, align 4, !tbaa !6
  %mul26 = fmul float %15, %17
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %18 = load float, float* %arrayidx28, align 4, !tbaa !6
  %19 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %19, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %20 = load float, float* %arrayidx30, align 4, !tbaa !6
  %mul31 = fmul float %18, %20
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %21 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #6
  %22 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #6
  %23 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #6
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4, !tbaa !6
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1) #6
  ret %class.btVector3* %call2
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !6
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #6
  ret %class.btVector3* %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: inlinehint
define linkonce_odr hidden %struct.btSubSimplexClosestResult* @_ZN25btSubSimplexClosestResultC2Ev(%struct.btSubSimplexClosestResult* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %struct.btSubSimplexClosestResult*, align 4
  store %struct.btSubSimplexClosestResult* %this, %struct.btSubSimplexClosestResult** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %this.addr, align 4
  %m_closestPointOnSimplex = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_closestPointOnSimplex)
  %m_usedVertices = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 1
  %call2 = call %struct.btUsageBitfield* @_ZN15btUsageBitfieldC2Ev(%struct.btUsageBitfield* %m_usedVertices)
  ret %struct.btSubSimplexClosestResult* %this1
}

define linkonce_odr hidden %struct.btUsageBitfield* @_ZN15btUsageBitfieldC2Ev(%struct.btUsageBitfield* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %struct.btUsageBitfield*, align 4
  store %struct.btUsageBitfield* %this, %struct.btUsageBitfield** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btUsageBitfield*, %struct.btUsageBitfield** %this.addr, align 4
  call void @_ZN15btUsageBitfield5resetEv(%struct.btUsageBitfield* %this1)
  ret %struct.btUsageBitfield* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN15btUsageBitfield5resetEv(%struct.btUsageBitfield* %this) #3 comdat {
entry:
  %this.addr = alloca %struct.btUsageBitfield*, align 4
  store %struct.btUsageBitfield* %this, %struct.btUsageBitfield** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btUsageBitfield*, %struct.btUsageBitfield** %this.addr, align 4
  %0 = bitcast %struct.btUsageBitfield* %this1 to i8*
  %bf.load = load i8, i8* %0, align 2
  %bf.clear = and i8 %bf.load, -2
  store i8 %bf.clear, i8* %0, align 2
  %1 = bitcast %struct.btUsageBitfield* %this1 to i8*
  %bf.load2 = load i8, i8* %1, align 2
  %bf.clear3 = and i8 %bf.load2, -3
  store i8 %bf.clear3, i8* %1, align 2
  %2 = bitcast %struct.btUsageBitfield* %this1 to i8*
  %bf.load4 = load i8, i8* %2, align 2
  %bf.clear5 = and i8 %bf.load4, -5
  store i8 %bf.clear5, i8* %2, align 2
  %3 = bitcast %struct.btUsageBitfield* %this1 to i8*
  %bf.load6 = load i8, i8* %3, align 2
  %bf.clear7 = and i8 %bf.load6, -9
  store i8 %bf.clear7, i8* %3, align 2
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btConvexPenetrationDepthSolver* @_ZN30btConvexPenetrationDepthSolverC2Ev(%class.btConvexPenetrationDepthSolver* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btConvexPenetrationDepthSolver*, align 4
  store %class.btConvexPenetrationDepthSolver* %this, %class.btConvexPenetrationDepthSolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %this.addr, align 4
  %0 = bitcast %class.btConvexPenetrationDepthSolver* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV30btConvexPenetrationDepthSolver, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !18
  ret %class.btConvexPenetrationDepthSolver* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN30btConvexPenetrationDepthSolverD0Ev(%class.btConvexPenetrationDepthSolver* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btConvexPenetrationDepthSolver*, align 4
  store %class.btConvexPenetrationDepthSolver* %this, %class.btConvexPenetrationDepthSolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %this.addr, align 4
  call void @llvm.trap() #11
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #9

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultC2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %this, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  %0 = bitcast %"struct.btDiscreteCollisionDetectorInterface::Result"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTVN36btDiscreteCollisionDetectorInterface6ResultE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !18
  ret %"struct.btDiscreteCollisionDetectorInterface::Result"* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN16btPointCollectorD0Ev(%struct.btPointCollector* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %struct.btPointCollector*, align 4
  store %struct.btPointCollector* %this, %struct.btPointCollector** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btPointCollector*, %struct.btPointCollector** %this.addr, align 4
  %call = call %struct.btPointCollector* bitcast (%"struct.btDiscreteCollisionDetectorInterface::Result"* (%"struct.btDiscreteCollisionDetectorInterface::Result"*)* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev to %struct.btPointCollector* (%struct.btPointCollector*)*)(%struct.btPointCollector* %this1) #6
  %0 = bitcast %struct.btPointCollector* %this1 to i8*
  call void @_ZdlPv(i8* %0) #12
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN16btPointCollector20setShapeIdentifiersAEii(%struct.btPointCollector* %this, i32 %partId0, i32 %index0) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %struct.btPointCollector*, align 4
  %partId0.addr = alloca i32, align 4
  %index0.addr = alloca i32, align 4
  store %struct.btPointCollector* %this, %struct.btPointCollector** %this.addr, align 4, !tbaa !2
  store i32 %partId0, i32* %partId0.addr, align 4, !tbaa !48
  store i32 %index0, i32* %index0.addr, align 4, !tbaa !48
  %this1 = load %struct.btPointCollector*, %struct.btPointCollector** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN16btPointCollector20setShapeIdentifiersBEii(%struct.btPointCollector* %this, i32 %partId1, i32 %index1) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %struct.btPointCollector*, align 4
  %partId1.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  store %struct.btPointCollector* %this, %struct.btPointCollector** %this.addr, align 4, !tbaa !2
  store i32 %partId1, i32* %partId1.addr, align 4, !tbaa !48
  store i32 %index1, i32* %index1.addr, align 4, !tbaa !48
  %this1 = load %struct.btPointCollector*, %struct.btPointCollector** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN16btPointCollector15addContactPointERK9btVector3S2_f(%struct.btPointCollector* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %normalOnBInWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %pointInWorld, float %depth) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %struct.btPointCollector*, align 4
  %normalOnBInWorld.addr = alloca %class.btVector3*, align 4
  %pointInWorld.addr = alloca %class.btVector3*, align 4
  %depth.addr = alloca float, align 4
  store %struct.btPointCollector* %this, %struct.btPointCollector** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %normalOnBInWorld, %class.btVector3** %normalOnBInWorld.addr, align 4, !tbaa !2
  store %class.btVector3* %pointInWorld, %class.btVector3** %pointInWorld.addr, align 4, !tbaa !2
  store float %depth, float* %depth.addr, align 4, !tbaa !6
  %this1 = load %struct.btPointCollector*, %struct.btPointCollector** %this.addr, align 4
  %0 = load float, float* %depth.addr, align 4, !tbaa !6
  %m_distance = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %this1, i32 0, i32 3
  %1 = load float, float* %m_distance, align 4, !tbaa !55
  %cmp = fcmp olt float %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_hasResult = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %this1, i32 0, i32 4
  store i8 1, i8* %m_hasResult, align 4, !tbaa !52
  %2 = load %class.btVector3*, %class.btVector3** %normalOnBInWorld.addr, align 4, !tbaa !2
  %m_normalOnBInWorld = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %this1, i32 0, i32 1
  %3 = bitcast %class.btVector3* %m_normalOnBInWorld to i8*
  %4 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false), !tbaa.struct !28
  %5 = load %class.btVector3*, %class.btVector3** %pointInWorld.addr, align 4, !tbaa !2
  %m_pointInWorld = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %this1, i32 0, i32 2
  %6 = bitcast %class.btVector3* %m_pointInWorld to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !28
  %8 = load float, float* %depth.addr, align 4, !tbaa !6
  %m_distance2 = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %this1, i32 0, i32 3
  store float %8, float* %m_distance2, align 4, !tbaa !55
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %this, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  ret %"struct.btDiscreteCollisionDetectorInterface::Result"* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN36btDiscreteCollisionDetectorInterface6ResultD0Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %this, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  call void @llvm.trap() #11
  unreachable
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #10

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !28
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !28
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !28
  ret %class.btMatrix3x3* %this1
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }
attributes #7 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind readnone speculatable willreturn }
attributes #9 = { cold noreturn nounwind }
attributes #10 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #11 = { noreturn nounwind }
attributes #12 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"float", !4, i64 0}
!8 = !{!9, !3, i64 0}
!9 = !{!"_ZTS34btDefaultCollisionConstructionInfo", !3, i64 0, !3, i64 4, !10, i64 8, !10, i64 12, !10, i64 16, !10, i64 20}
!10 = !{!"int", !4, i64 0}
!11 = !{!9, !3, i64 4}
!12 = !{!9, !10, i64 8}
!13 = !{!9, !10, i64 12}
!14 = !{!9, !10, i64 16}
!15 = !{!9, !10, i64 20}
!16 = !{!17, !17, i64 0}
!17 = !{!"long", !4, i64 0}
!18 = !{!19, !19, i64 0}
!19 = !{!"vtable pointer", !5, i64 0}
!20 = !{!21, !7, i64 0}
!21 = !{!"_ZTSN11btRigidBody27btRigidBodyConstructionInfoE", !7, i64 0, !3, i64 4, !22, i64 8, !3, i64 72, !24, i64 76, !7, i64 92, !7, i64 96, !7, i64 100, !7, i64 104, !7, i64 108, !7, i64 112, !7, i64 116, !25, i64 120, !7, i64 124, !7, i64 128, !7, i64 132, !7, i64 136}
!22 = !{!"_ZTS11btTransform", !23, i64 0, !24, i64 48}
!23 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!24 = !{!"_ZTS9btVector3", !4, i64 0}
!25 = !{!"bool", !4, i64 0}
!26 = !{!21, !3, i64 4}
!27 = !{!21, !3, i64 72}
!28 = !{i64 0, i64 16, !29}
!29 = !{!4, !4, i64 0}
!30 = !{!21, !7, i64 92}
!31 = !{!21, !7, i64 96}
!32 = !{!21, !7, i64 100}
!33 = !{!21, !7, i64 104}
!34 = !{!21, !7, i64 108}
!35 = !{!21, !7, i64 112}
!36 = !{!21, !7, i64 116}
!37 = !{!21, !25, i64 120}
!38 = !{!21, !7, i64 124}
!39 = !{!21, !7, i64 128}
!40 = !{!21, !7, i64 132}
!41 = !{!21, !7, i64 136}
!42 = !{!43, !10, i64 260}
!43 = !{!"_ZTS17btCollisionObject", !22, i64 4, !22, i64 68, !24, i64 132, !24, i64 148, !24, i64 164, !10, i64 180, !7, i64 184, !3, i64 188, !3, i64 192, !3, i64 196, !3, i64 200, !10, i64 204, !10, i64 208, !10, i64 212, !10, i64 216, !7, i64 220, !7, i64 224, !7, i64 228, !7, i64 232, !10, i64 236, !4, i64 240, !7, i64 244, !7, i64 248, !7, i64 252, !10, i64 256, !10, i64 260}
!44 = !{!45, !10, i64 4}
!45 = !{!"_ZTS16btCollisionShape", !10, i64 4, !3, i64 8}
!46 = !{!47, !7, i64 44}
!47 = !{!"_ZTS21btConvexInternalShape", !24, i64 12, !24, i64 28, !7, i64 44, !7, i64 48}
!48 = !{!10, !10, i64 0}
!49 = !{!"branch_weights", i32 1, i32 1048575}
!50 = !{!51, !10, i64 72}
!51 = !{!"_ZTS17btGjkPairDetector", !24, i64 4, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !10, i64 36, !10, i64 40, !7, i64 44, !7, i64 48, !25, i64 52, !7, i64 56, !10, i64 60, !10, i64 64, !10, i64 68, !10, i64 72, !10, i64 76}
!52 = !{!53, !25, i64 40}
!53 = !{!"_ZTS16btPointCollector", !24, i64 4, !24, i64 20, !7, i64 36, !25, i64 40}
!54 = !{i8 0, i8 2}
!55 = !{!53, !7, i64 36}
!56 = !{!57, !7, i64 308}
!57 = !{!"_ZTS22btVoronoiSimplexSolver", !10, i64 0, !4, i64 4, !4, i64 84, !4, i64 164, !24, i64 244, !24, i64 260, !24, i64 276, !24, i64 292, !7, i64 308, !25, i64 312, !58, i64 316, !25, i64 356}
!58 = !{!"_ZTS25btSubSimplexClosestResult", !24, i64 0, !59, i64 16, !4, i64 20, !25, i64 36}
!59 = !{!"_ZTS15btUsageBitfield", !60, i64 0, !60, i64 0, !60, i64 0, !60, i64 0, !60, i64 0, !60, i64 0, !60, i64 0, !60, i64 0}
!60 = !{!"short", !4, i64 0}
!61 = !{!62, !7, i64 128}
!62 = !{!"_ZTSN36btDiscreteCollisionDetectorInterface17ClosestPointInputE", !22, i64 0, !22, i64 64, !7, i64 128}
!63 = !{!64, !7, i64 44}
!64 = !{!"_ZTS25btConvexInternalShapeData", !65, i64 0, !66, i64 12, !66, i64 28, !7, i64 44, !10, i64 48}
!65 = !{!"_ZTS20btCollisionShapeData", !3, i64 0, !10, i64 4, !4, i64 8}
!66 = !{!"_ZTS18btVector3FloatData", !4, i64 0}
