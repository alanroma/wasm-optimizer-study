; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/ConstraintSolver/btGearConstraint.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/ConstraintSolver/btGearConstraint.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btGearConstraint = type { %class.btTypedConstraint, %class.btVector3, %class.btVector3, i8, float }
%class.btTypedConstraint = type { i32 (...)**, %struct.btTypedObject, i32, %union.anon, float, i8, i8, i32, %class.btRigidBody*, %class.btRigidBody*, float, float, %struct.btJointFeedback* }
%struct.btTypedObject = type { i32 }
%union.anon = type { i32 }
%struct.btJointFeedback = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon.0, float, float, float, i32, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%union.anon.0 = type { i8* }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%"struct.btTypedConstraint::btConstraintInfo1" = type { i32, i32 }
%"struct.btTypedConstraint::btConstraintInfo2" = type { float, float, float*, float*, float*, float*, i32, float*, float*, float*, float*, i32*, i32, float }
%class.btAlignedObjectArray.1 = type opaque
%struct.btSolverBody = type { %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btRigidBody* }
%class.btSerializer = type opaque
%struct.btGearConstraintFloatData = type { %struct.btTypedConstraintFloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, [4 x i8] }
%struct.btTypedConstraintFloatData = type { %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData*, i8*, i32, i32, i32, i32, float, float, i32, i32, float, i32 }
%struct.btRigidBodyFloatData = type { %struct.btCollisionObjectFloatData, %struct.btMatrix3x3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, float, float, float, float, float, float, float, float, i32 }
%struct.btCollisionObjectFloatData = type { i8*, i8*, %struct.btCollisionShapeData*, i8*, %struct.btTransformFloatData, %struct.btTransformFloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, float, float, float, float, float, float, float, i32, i32, i32, i32, i32, i32, i32, [4 x i8] }
%struct.btCollisionShapeData = type opaque
%struct.btTransformFloatData = type { %struct.btMatrix3x3FloatData, %struct.btVector3FloatData }
%struct.btMatrix3x3FloatData = type { [3 x %struct.btVector3FloatData] }
%struct.btVector3FloatData = type { [4 x float] }

$_ZN17btTypedConstraintD2Ev = comdat any

$_ZN17btTypedConstraintdlEPv = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZN17btCollisionObject17getWorldTransformEv = comdat any

$_ZN11btTransform8getBasisEv = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN17btTypedConstraint13buildJacobianEv = comdat any

$_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif = comdat any

$_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f = comdat any

$_ZN16btGearConstraint8setParamEifi = comdat any

$_ZNK16btGearConstraint8getParamEii = comdat any

$_ZNK16btGearConstraint28calculateSerializeBufferSizeEv = comdat any

$_ZNK16btGearConstraint9serializeEPvP12btSerializer = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZNK9btVector39serializeER18btVector3FloatData = comdat any

@_ZTV16btGearConstraint = hidden unnamed_addr constant { [13 x i8*] } { [13 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI16btGearConstraint to i8*), i8* bitcast (%class.btGearConstraint* (%class.btGearConstraint*)* @_ZN16btGearConstraintD1Ev to i8*), i8* bitcast (void (%class.btGearConstraint*)* @_ZN16btGearConstraintD0Ev to i8*), i8* bitcast (void (%class.btTypedConstraint*)* @_ZN17btTypedConstraint13buildJacobianEv to i8*), i8* bitcast (void (%class.btTypedConstraint*, %class.btAlignedObjectArray.1*, i32, i32, float)* @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif to i8*), i8* bitcast (void (%class.btGearConstraint*, %"struct.btTypedConstraint::btConstraintInfo1"*)* @_ZN16btGearConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E to i8*), i8* bitcast (void (%class.btGearConstraint*, %"struct.btTypedConstraint::btConstraintInfo2"*)* @_ZN16btGearConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E to i8*), i8* bitcast (void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)* @_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f to i8*), i8* bitcast (void (%class.btGearConstraint*, i32, float, i32)* @_ZN16btGearConstraint8setParamEifi to i8*), i8* bitcast (float (%class.btGearConstraint*, i32, i32)* @_ZNK16btGearConstraint8getParamEii to i8*), i8* bitcast (i32 (%class.btGearConstraint*)* @_ZNK16btGearConstraint28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btGearConstraint*, i8*, %class.btSerializer*)* @_ZNK16btGearConstraint9serializeEPvP12btSerializer to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS16btGearConstraint = hidden constant [19 x i8] c"16btGearConstraint\00", align 1
@_ZTI17btTypedConstraint = external constant i8*
@_ZTI16btGearConstraint = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([19 x i8], [19 x i8]* @_ZTS16btGearConstraint, i32 0, i32 0), i8* bitcast (i8** @_ZTI17btTypedConstraint to i8*) }, align 4
@_ZTV17btTypedConstraint = external unnamed_addr constant { [13 x i8*] }, align 4
@.str = private unnamed_addr constant [26 x i8] c"btGearConstraintFloatData\00", align 1

@_ZN16btGearConstraintC1ER11btRigidBodyS1_RK9btVector3S4_f = hidden unnamed_addr alias %class.btGearConstraint* (%class.btGearConstraint*, %class.btRigidBody*, %class.btRigidBody*, %class.btVector3*, %class.btVector3*, float), %class.btGearConstraint* (%class.btGearConstraint*, %class.btRigidBody*, %class.btRigidBody*, %class.btVector3*, %class.btVector3*, float)* @_ZN16btGearConstraintC2ER11btRigidBodyS1_RK9btVector3S4_f
@_ZN16btGearConstraintD1Ev = hidden unnamed_addr alias %class.btGearConstraint* (%class.btGearConstraint*), %class.btGearConstraint* (%class.btGearConstraint*)* @_ZN16btGearConstraintD2Ev

define hidden %class.btGearConstraint* @_ZN16btGearConstraintC2ER11btRigidBodyS1_RK9btVector3S4_f(%class.btGearConstraint* returned %this, %class.btRigidBody* nonnull align 4 dereferenceable(616) %rbA, %class.btRigidBody* nonnull align 4 dereferenceable(616) %rbB, %class.btVector3* nonnull align 4 dereferenceable(16) %axisInA, %class.btVector3* nonnull align 4 dereferenceable(16) %axisInB, float %ratio) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btGearConstraint*, align 4
  %rbA.addr = alloca %class.btRigidBody*, align 4
  %rbB.addr = alloca %class.btRigidBody*, align 4
  %axisInA.addr = alloca %class.btVector3*, align 4
  %axisInB.addr = alloca %class.btVector3*, align 4
  %ratio.addr = alloca float, align 4
  store %class.btGearConstraint* %this, %class.btGearConstraint** %this.addr, align 4, !tbaa !2
  store %class.btRigidBody* %rbA, %class.btRigidBody** %rbA.addr, align 4, !tbaa !2
  store %class.btRigidBody* %rbB, %class.btRigidBody** %rbB.addr, align 4, !tbaa !2
  store %class.btVector3* %axisInA, %class.btVector3** %axisInA.addr, align 4, !tbaa !2
  store %class.btVector3* %axisInB, %class.btVector3** %axisInB.addr, align 4, !tbaa !2
  store float %ratio, float* %ratio.addr, align 4, !tbaa !6
  %this1 = load %class.btGearConstraint*, %class.btGearConstraint** %this.addr, align 4
  %0 = bitcast %class.btGearConstraint* %this1 to %class.btTypedConstraint*
  %1 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4, !tbaa !2
  %2 = load %class.btRigidBody*, %class.btRigidBody** %rbB.addr, align 4, !tbaa !2
  %call = call %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_(%class.btTypedConstraint* %0, i32 10, %class.btRigidBody* nonnull align 4 dereferenceable(616) %1, %class.btRigidBody* nonnull align 4 dereferenceable(616) %2)
  %3 = bitcast %class.btGearConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [13 x i8*] }, { [13 x i8*] }* @_ZTV16btGearConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %3, align 4, !tbaa !8
  %m_axisInA = getelementptr inbounds %class.btGearConstraint, %class.btGearConstraint* %this1, i32 0, i32 1
  %4 = load %class.btVector3*, %class.btVector3** %axisInA.addr, align 4, !tbaa !2
  %5 = bitcast %class.btVector3* %m_axisInA to i8*
  %6 = bitcast %class.btVector3* %4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false), !tbaa.struct !10
  %m_axisInB = getelementptr inbounds %class.btGearConstraint, %class.btGearConstraint* %this1, i32 0, i32 2
  %7 = load %class.btVector3*, %class.btVector3** %axisInB.addr, align 4, !tbaa !2
  %8 = bitcast %class.btVector3* %m_axisInB to i8*
  %9 = bitcast %class.btVector3* %7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false), !tbaa.struct !10
  %m_ratio = getelementptr inbounds %class.btGearConstraint, %class.btGearConstraint* %this1, i32 0, i32 4
  %10 = load float, float* %ratio.addr, align 4, !tbaa !6
  store float %10, float* %m_ratio, align 4, !tbaa !12
  ret %class.btGearConstraint* %this1
}

declare %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_(%class.btTypedConstraint* returned, i32, %class.btRigidBody* nonnull align 4 dereferenceable(616), %class.btRigidBody* nonnull align 4 dereferenceable(616)) unnamed_addr #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: nounwind
define linkonce_odr hidden %class.btTypedConstraint* @_ZN17btTypedConstraintD2Ev(%class.btTypedConstraint* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = bitcast %class.btTypedConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [13 x i8*] }, { [13 x i8*] }* @_ZTV17btTypedConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !8
  ret %class.btTypedConstraint* %this1
}

; Function Attrs: nounwind
define hidden %class.btGearConstraint* @_ZN16btGearConstraintD2Ev(%class.btGearConstraint* returned %this) unnamed_addr #3 {
entry:
  %this.addr = alloca %class.btGearConstraint*, align 4
  store %class.btGearConstraint* %this, %class.btGearConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGearConstraint*, %class.btGearConstraint** %this.addr, align 4
  %0 = bitcast %class.btGearConstraint* %this1 to %class.btTypedConstraint*
  %call = call %class.btTypedConstraint* @_ZN17btTypedConstraintD2Ev(%class.btTypedConstraint* %0) #6
  ret %class.btGearConstraint* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN16btGearConstraintD0Ev(%class.btGearConstraint* %this) unnamed_addr #3 {
entry:
  %this.addr = alloca %class.btGearConstraint*, align 4
  store %class.btGearConstraint* %this, %class.btGearConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGearConstraint*, %class.btGearConstraint** %this.addr, align 4
  %call = call %class.btGearConstraint* @_ZN16btGearConstraintD1Ev(%class.btGearConstraint* %this1) #6
  %0 = bitcast %class.btGearConstraint* %this1 to i8*
  call void @_ZN17btTypedConstraintdlEPv(i8* %0) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN17btTypedConstraintdlEPv(i8* %ptr) #4 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: nounwind
define hidden void @_ZN16btGearConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E(%class.btGearConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo1"* %info) unnamed_addr #3 {
entry:
  %this.addr = alloca %class.btGearConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo1"*, align 4
  store %class.btGearConstraint* %this, %class.btGearConstraint** %this.addr, align 4, !tbaa !2
  store %"struct.btTypedConstraint::btConstraintInfo1"* %info, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %this1 = load %class.btGearConstraint*, %class.btGearConstraint** %this.addr, align 4
  %0 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %m_numConstraintRows = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %0, i32 0, i32 0
  store i32 1, i32* %m_numConstraintRows, align 4, !tbaa !16
  %1 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %nub = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %1, i32 0, i32 1
  store i32 1, i32* %nub, align 4, !tbaa !19
  ret void
}

define hidden void @_ZN16btGearConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E(%class.btGearConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo2"* %info) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btGearConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo2"*, align 4
  %globalAxisA = alloca %class.btVector3, align 4
  %globalAxisB = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  store %class.btGearConstraint* %this, %class.btGearConstraint** %this.addr, align 4, !tbaa !2
  store %"struct.btTypedConstraint::btConstraintInfo2"* %info, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %this1 = load %class.btGearConstraint*, %class.btGearConstraint** %this.addr, align 4
  %0 = bitcast %class.btVector3* %globalAxisA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #6
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %globalAxisA)
  %1 = bitcast %class.btVector3* %globalAxisB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #6
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %globalAxisB)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #6
  %3 = bitcast %class.btGearConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %3, i32 0, i32 8
  %4 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4, !tbaa !20
  %5 = bitcast %class.btRigidBody* %4 to %class.btCollisionObject*
  %call3 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %5)
  %call4 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %call3)
  %m_axisInA = getelementptr inbounds %class.btGearConstraint, %class.btGearConstraint* %this1, i32 0, i32 1
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call4, %class.btVector3* nonnull align 4 dereferenceable(16) %m_axisInA)
  %6 = bitcast %class.btVector3* %globalAxisA to i8*
  %7 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !10
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #6
  %9 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #6
  %10 = bitcast %class.btGearConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %10, i32 0, i32 9
  %11 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4, !tbaa !22
  %12 = bitcast %class.btRigidBody* %11 to %class.btCollisionObject*
  %call6 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %12)
  %call7 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %call6)
  %m_axisInB = getelementptr inbounds %class.btGearConstraint, %class.btGearConstraint* %this1, i32 0, i32 2
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp5, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call7, %class.btVector3* nonnull align 4 dereferenceable(16) %m_axisInB)
  %13 = bitcast %class.btVector3* %globalAxisB to i8*
  %14 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 16, i1 false), !tbaa.struct !10
  %15 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %15) #6
  %call8 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %globalAxisA)
  %arrayidx = getelementptr inbounds float, float* %call8, i32 0
  %16 = load float, float* %arrayidx, align 4, !tbaa !6
  %17 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %17, i32 0, i32 3
  %18 = load float*, float** %m_J1angularAxis, align 4, !tbaa !23
  %arrayidx9 = getelementptr inbounds float, float* %18, i32 0
  store float %16, float* %arrayidx9, align 4, !tbaa !6
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %globalAxisA)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 1
  %19 = load float, float* %arrayidx11, align 4, !tbaa !6
  %20 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis12 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %20, i32 0, i32 3
  %21 = load float*, float** %m_J1angularAxis12, align 4, !tbaa !23
  %arrayidx13 = getelementptr inbounds float, float* %21, i32 1
  store float %19, float* %arrayidx13, align 4, !tbaa !6
  %call14 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %globalAxisA)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 2
  %22 = load float, float* %arrayidx15, align 4, !tbaa !6
  %23 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis16 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %23, i32 0, i32 3
  %24 = load float*, float** %m_J1angularAxis16, align 4, !tbaa !23
  %arrayidx17 = getelementptr inbounds float, float* %24, i32 2
  store float %22, float* %arrayidx17, align 4, !tbaa !6
  %m_ratio = getelementptr inbounds %class.btGearConstraint, %class.btGearConstraint* %this1, i32 0, i32 4
  %25 = load float, float* %m_ratio, align 4, !tbaa !12
  %call18 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %globalAxisB)
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 0
  %26 = load float, float* %arrayidx19, align 4, !tbaa !6
  %mul = fmul float %25, %26
  %27 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %27, i32 0, i32 5
  %28 = load float*, float** %m_J2angularAxis, align 4, !tbaa !25
  %arrayidx20 = getelementptr inbounds float, float* %28, i32 0
  store float %mul, float* %arrayidx20, align 4, !tbaa !6
  %m_ratio21 = getelementptr inbounds %class.btGearConstraint, %class.btGearConstraint* %this1, i32 0, i32 4
  %29 = load float, float* %m_ratio21, align 4, !tbaa !12
  %call22 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %globalAxisB)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 1
  %30 = load float, float* %arrayidx23, align 4, !tbaa !6
  %mul24 = fmul float %29, %30
  %31 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis25 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %31, i32 0, i32 5
  %32 = load float*, float** %m_J2angularAxis25, align 4, !tbaa !25
  %arrayidx26 = getelementptr inbounds float, float* %32, i32 1
  store float %mul24, float* %arrayidx26, align 4, !tbaa !6
  %m_ratio27 = getelementptr inbounds %class.btGearConstraint, %class.btGearConstraint* %this1, i32 0, i32 4
  %33 = load float, float* %m_ratio27, align 4, !tbaa !12
  %call28 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %globalAxisB)
  %arrayidx29 = getelementptr inbounds float, float* %call28, i32 2
  %34 = load float, float* %arrayidx29, align 4, !tbaa !6
  %mul30 = fmul float %33, %34
  %35 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis31 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %35, i32 0, i32 5
  %36 = load float*, float** %m_J2angularAxis31, align 4, !tbaa !25
  %arrayidx32 = getelementptr inbounds float, float* %36, i32 2
  store float %mul30, float* %arrayidx32, align 4, !tbaa !6
  %37 = bitcast %class.btVector3* %globalAxisB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %37) #6
  %38 = bitcast %class.btVector3* %globalAxisA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %38) #6
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #5 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %1, i32 0)
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call1, float* %ref.tmp, align 4, !tbaa !6
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 1)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !6
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 2)
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !6
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #6
  %10 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN17btTypedConstraint13buildJacobianEv(%class.btTypedConstraint* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif(%class.btTypedConstraint* %this, %class.btAlignedObjectArray.1* nonnull align 1 %ca, i32 %solverBodyA, i32 %solverBodyB, float %timeStep) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  %ca.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %solverBodyA.addr = alloca i32, align 4
  %solverBodyB.addr = alloca i32, align 4
  %timeStep.addr = alloca float, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.1* %ca, %class.btAlignedObjectArray.1** %ca.addr, align 4, !tbaa !2
  store i32 %solverBodyA, i32* %solverBodyA.addr, align 4, !tbaa !26
  store i32 %solverBodyB, i32* %solverBodyB.addr, align 4, !tbaa !26
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !6
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %ca.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f(%class.btTypedConstraint* %this, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %0, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %1, float %2) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  %.addr = alloca %struct.btSolverBody*, align 4
  %.addr1 = alloca %struct.btSolverBody*, align 4
  %.addr2 = alloca float, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4, !tbaa !2
  store %struct.btSolverBody* %0, %struct.btSolverBody** %.addr, align 4, !tbaa !2
  store %struct.btSolverBody* %1, %struct.btSolverBody** %.addr1, align 4, !tbaa !2
  store float %2, float* %.addr2, align 4, !tbaa !6
  %this3 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN16btGearConstraint8setParamEifi(%class.btGearConstraint* %this, i32 %num, float %value, i32 %axis) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btGearConstraint*, align 4
  %num.addr = alloca i32, align 4
  %value.addr = alloca float, align 4
  %axis.addr = alloca i32, align 4
  store %class.btGearConstraint* %this, %class.btGearConstraint** %this.addr, align 4, !tbaa !2
  store i32 %num, i32* %num.addr, align 4, !tbaa !26
  store float %value, float* %value.addr, align 4, !tbaa !6
  store i32 %axis, i32* %axis.addr, align 4, !tbaa !26
  %this1 = load %class.btGearConstraint*, %class.btGearConstraint** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK16btGearConstraint8getParamEii(%class.btGearConstraint* %this, i32 %num, i32 %axis) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btGearConstraint*, align 4
  %num.addr = alloca i32, align 4
  %axis.addr = alloca i32, align 4
  store %class.btGearConstraint* %this, %class.btGearConstraint** %this.addr, align 4, !tbaa !2
  store i32 %num, i32* %num.addr, align 4, !tbaa !26
  store i32 %axis, i32* %axis.addr, align 4, !tbaa !26
  %this1 = load %class.btGearConstraint*, %class.btGearConstraint** %this.addr, align 4
  ret float 0.000000e+00
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK16btGearConstraint28calculateSerializeBufferSizeEv(%class.btGearConstraint* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btGearConstraint*, align 4
  store %class.btGearConstraint* %this, %class.btGearConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGearConstraint*, %class.btGearConstraint** %this.addr, align 4
  ret i32 92
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZNK16btGearConstraint9serializeEPvP12btSerializer(%class.btGearConstraint* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btGearConstraint*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %gear = alloca %struct.btGearConstraintFloatData*, align 4
  store %class.btGearConstraint* %this, %class.btGearConstraint** %this.addr, align 4, !tbaa !2
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btGearConstraint*, %class.btGearConstraint** %this.addr, align 4
  %0 = bitcast %struct.btGearConstraintFloatData** %gear to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i8*, i8** %dataBuffer.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.btGearConstraintFloatData*
  store %struct.btGearConstraintFloatData* %2, %struct.btGearConstraintFloatData** %gear, align 4, !tbaa !2
  %3 = bitcast %class.btGearConstraint* %this1 to %class.btTypedConstraint*
  %4 = load %struct.btGearConstraintFloatData*, %struct.btGearConstraintFloatData** %gear, align 4, !tbaa !2
  %m_typeConstraintData = getelementptr inbounds %struct.btGearConstraintFloatData, %struct.btGearConstraintFloatData* %4, i32 0, i32 0
  %5 = bitcast %struct.btTypedConstraintFloatData* %m_typeConstraintData to i8*
  %6 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %call = call i8* @_ZNK17btTypedConstraint9serializeEPvP12btSerializer(%class.btTypedConstraint* %3, i8* %5, %class.btSerializer* %6)
  %m_axisInA = getelementptr inbounds %class.btGearConstraint, %class.btGearConstraint* %this1, i32 0, i32 1
  %7 = load %struct.btGearConstraintFloatData*, %struct.btGearConstraintFloatData** %gear, align 4, !tbaa !2
  %m_axisInA2 = getelementptr inbounds %struct.btGearConstraintFloatData, %struct.btGearConstraintFloatData* %7, i32 0, i32 1
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_axisInA, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_axisInA2)
  %m_axisInB = getelementptr inbounds %class.btGearConstraint, %class.btGearConstraint* %this1, i32 0, i32 2
  %8 = load %struct.btGearConstraintFloatData*, %struct.btGearConstraintFloatData** %gear, align 4, !tbaa !2
  %m_axisInB3 = getelementptr inbounds %struct.btGearConstraintFloatData, %struct.btGearConstraintFloatData* %8, i32 0, i32 2
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_axisInB, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_axisInB3)
  %m_ratio = getelementptr inbounds %class.btGearConstraint, %class.btGearConstraint* %this1, i32 0, i32 4
  %9 = load float, float* %m_ratio, align 4, !tbaa !12
  %10 = load %struct.btGearConstraintFloatData*, %struct.btGearConstraintFloatData** %gear, align 4, !tbaa !2
  %m_ratio4 = getelementptr inbounds %struct.btGearConstraintFloatData, %struct.btGearConstraintFloatData* %10, i32 0, i32 3
  store float %9, float* %m_ratio4, align 4, !tbaa !27
  %11 = bitcast %struct.btGearConstraintFloatData** %gear to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  ret i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str, i32 0, i32 0)
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !26
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !26
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !6
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !6
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !6
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !6
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !6
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !6
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !6
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !6
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !6
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !6
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !6
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !6
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !6
  ret %class.btVector3* %this1
}

declare i8* @_ZNK17btTypedConstraint9serializeEPvP12btSerializer(%class.btTypedConstraint*, i8*, %class.btSerializer*) unnamed_addr #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %i, align 4, !tbaa !26
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !26
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %3 = load i32, i32* %i, align 4, !tbaa !26
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %3
  %4 = load float, float* %arrayidx, align 4, !tbaa !6
  %5 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %5, i32 0, i32 0
  %6 = load i32, i32* %i, align 4, !tbaa !26
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %6
  store float %4, float* %arrayidx3, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !26
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !26
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"float", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"vtable pointer", !5, i64 0}
!10 = !{i64 0, i64 16, !11}
!11 = !{!4, !4, i64 0}
!12 = !{!13, !7, i64 84}
!13 = !{!"_ZTS16btGearConstraint", !14, i64 48, !14, i64 64, !15, i64 80, !7, i64 84}
!14 = !{!"_ZTS9btVector3", !4, i64 0}
!15 = !{!"bool", !4, i64 0}
!16 = !{!17, !18, i64 0}
!17 = !{!"_ZTSN17btTypedConstraint17btConstraintInfo1E", !18, i64 0, !18, i64 4}
!18 = !{!"int", !4, i64 0}
!19 = !{!17, !18, i64 4}
!20 = !{!21, !3, i64 28}
!21 = !{!"_ZTS17btTypedConstraint", !18, i64 8, !4, i64 12, !7, i64 16, !15, i64 20, !15, i64 21, !18, i64 24, !3, i64 28, !3, i64 32, !7, i64 36, !7, i64 40, !3, i64 44}
!22 = !{!21, !3, i64 32}
!23 = !{!24, !3, i64 12}
!24 = !{!"_ZTSN17btTypedConstraint17btConstraintInfo2E", !7, i64 0, !7, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !18, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !3, i64 44, !18, i64 48, !7, i64 52}
!25 = !{!24, !3, i64 20}
!26 = !{!18, !18, i64 0}
!27 = !{!28, !7, i64 84}
!28 = !{!"_ZTS25btGearConstraintFloatData", !29, i64 0, !30, i64 52, !30, i64 68, !7, i64 84, !4, i64 88}
!29 = !{!"_ZTS26btTypedConstraintFloatData", !3, i64 0, !3, i64 4, !3, i64 8, !18, i64 12, !18, i64 16, !18, i64 20, !18, i64 24, !7, i64 28, !7, i64 32, !18, i64 36, !18, i64 40, !7, i64 44, !18, i64 48}
!30 = !{!"_ZTS18btVector3FloatData", !4, i64 0}
