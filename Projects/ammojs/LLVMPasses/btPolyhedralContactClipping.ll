; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/NarrowPhaseCollision/btPolyhedralContactClipping.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/NarrowPhaseCollision/btPolyhedralContactClipping.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btVector3 = type { [4 x float] }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btConvexPolyhedron = type { i32 (...)**, %class.btAlignedObjectArray, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.btFace*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btFace = type { %class.btAlignedObjectArray.3, [4 x float] }
%class.btAlignedObjectArray.3 = type <{ %class.btAlignedAllocator.4, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.4 = type { i8 }
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }

$_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3EixEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_ = comdat any

$_ZNK9btVector34lerpERKS_RKf = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN9btVector3C2Ev = comdat any

$_Z10BoxSupportPKfS0_Pf = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZNK11btTransformmlERK9btVector3 = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayI6btFaceEixEi = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_Z12IsAlmostZeroRK9btVector3 = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_Z23btSegmentsClosestPointsR9btVector3S0_S0_RfS1_RKS_S3_fS3_f = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_Z6btSqrtf = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZngRK9btVector3 = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIiE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayIiEixEi = comdat any

$_Z6btSwapIP20btAlignedObjectArrayI9btVector3EEvRT_S5_ = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_ = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E2atEi = comdat any

$_ZN20btAlignedObjectArrayI9btVector3ED2Ev = comdat any

$_ZNK9btVector310normalizedEv = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_Z5btDotRK9btVector3S1_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi = comdat any

$_ZN9btVector3nwEmPv = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E4initEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E5clearEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_ = comdat any

@gExpectedNbTests = hidden global i32 0, align 4
@gActualNbTests = hidden global i32 0, align 4
@gUseInternalObject = hidden global i8 1, align 1
@_ZL19gActualSATPairTests = internal global i32 0, align 4

define hidden void @_ZN27btPolyhedralContactClipping8clipFaceERK20btAlignedObjectArrayI9btVector3ERS2_RKS1_f(%class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %pVtxIn, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %ppVtxOut, %class.btVector3* nonnull align 4 dereferenceable(16) %planeNormalWS, float %planeEqWS) #0 {
entry:
  %pVtxIn.addr = alloca %class.btAlignedObjectArray*, align 4
  %ppVtxOut.addr = alloca %class.btAlignedObjectArray*, align 4
  %planeNormalWS.addr = alloca %class.btVector3*, align 4
  %planeEqWS.addr = alloca float, align 4
  %ve = alloca i32, align 4
  %ds = alloca float, align 4
  %de = alloca float, align 4
  %numVerts = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %firstVertex = alloca %class.btVector3, align 4
  %endVertex = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp19 = alloca %class.btVector3, align 4
  %ref.tmp20 = alloca float, align 4
  store %class.btAlignedObjectArray* %pVtxIn, %class.btAlignedObjectArray** %pVtxIn.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray* %ppVtxOut, %class.btAlignedObjectArray** %ppVtxOut.addr, align 4, !tbaa !2
  store %class.btVector3* %planeNormalWS, %class.btVector3** %planeNormalWS.addr, align 4, !tbaa !2
  store float %planeEqWS, float* %planeEqWS.addr, align 4, !tbaa !6
  %0 = bitcast i32* %ve to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = bitcast float* %ds to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = bitcast float* %de to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = bitcast i32* %numVerts to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pVtxIn.addr, align 4, !tbaa !2
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %4)
  store i32 %call, i32* %numVerts, align 4, !tbaa !8
  %5 = load i32, i32* %numVerts, align 4, !tbaa !8
  %cmp = icmp slt i32 %5, 2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %6 = bitcast %class.btVector3* %firstVertex to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #7
  %7 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pVtxIn.addr, align 4, !tbaa !2
  %8 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pVtxIn.addr, align 4, !tbaa !2
  %call1 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %8)
  %sub = sub nsw i32 %call1, 1
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %7, i32 %sub)
  %9 = bitcast %class.btVector3* %firstVertex to i8*
  %10 = bitcast %class.btVector3* %call2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false), !tbaa.struct !10
  %11 = bitcast %class.btVector3* %endVertex to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #7
  %12 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pVtxIn.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %12, i32 0)
  %13 = bitcast %class.btVector3* %endVertex to i8*
  %14 = bitcast %class.btVector3* %call3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 16, i1 false), !tbaa.struct !10
  %15 = load %class.btVector3*, %class.btVector3** %planeNormalWS.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %15, %class.btVector3* nonnull align 4 dereferenceable(16) %firstVertex)
  %16 = load float, float* %planeEqWS.addr, align 4, !tbaa !6
  %add = fadd float %call4, %16
  store float %add, float* %ds, align 4, !tbaa !6
  store i32 0, i32* %ve, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %17 = load i32, i32* %ve, align 4, !tbaa !8
  %18 = load i32, i32* %numVerts, align 4, !tbaa !8
  %cmp5 = icmp slt i32 %17, %18
  br i1 %cmp5, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %19 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pVtxIn.addr, align 4, !tbaa !2
  %20 = load i32, i32* %ve, align 4, !tbaa !8
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %19, i32 %20)
  %21 = bitcast %class.btVector3* %endVertex to i8*
  %22 = bitcast %class.btVector3* %call6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false), !tbaa.struct !10
  %23 = load %class.btVector3*, %class.btVector3** %planeNormalWS.addr, align 4, !tbaa !2
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %23, %class.btVector3* nonnull align 4 dereferenceable(16) %endVertex)
  %24 = load float, float* %planeEqWS.addr, align 4, !tbaa !6
  %add8 = fadd float %call7, %24
  store float %add8, float* %de, align 4, !tbaa !6
  %25 = load float, float* %ds, align 4, !tbaa !6
  %cmp9 = fcmp olt float %25, 0.000000e+00
  br i1 %cmp9, label %if.then10, label %if.else16

if.then10:                                        ; preds = %for.body
  %26 = load float, float* %de, align 4, !tbaa !6
  %cmp11 = fcmp olt float %26, 0.000000e+00
  br i1 %cmp11, label %if.then12, label %if.else

if.then12:                                        ; preds = %if.then10
  %27 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %ppVtxOut.addr, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray* %27, %class.btVector3* nonnull align 4 dereferenceable(16) %endVertex)
  br label %if.end15

if.else:                                          ; preds = %if.then10
  %28 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %ppVtxOut.addr, align 4, !tbaa !2
  %29 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %29) #7
  %30 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #7
  %31 = load float, float* %ds, align 4, !tbaa !6
  %mul = fmul float %31, 1.000000e+00
  %32 = load float, float* %ds, align 4, !tbaa !6
  %33 = load float, float* %de, align 4, !tbaa !6
  %sub14 = fsub float %32, %33
  %div = fdiv float %mul, %sub14
  store float %div, float* %ref.tmp13, align 4, !tbaa !6
  call void @_ZNK9btVector34lerpERKS_RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %firstVertex, %class.btVector3* nonnull align 4 dereferenceable(16) %endVertex, float* nonnull align 4 dereferenceable(4) %ref.tmp13)
  call void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray* %28, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %34 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #7
  %35 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %35) #7
  br label %if.end15

if.end15:                                         ; preds = %if.else, %if.then12
  br label %if.end25

if.else16:                                        ; preds = %for.body
  %36 = load float, float* %de, align 4, !tbaa !6
  %cmp17 = fcmp olt float %36, 0.000000e+00
  br i1 %cmp17, label %if.then18, label %if.end24

if.then18:                                        ; preds = %if.else16
  %37 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %ppVtxOut.addr, align 4, !tbaa !2
  %38 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %38) #7
  %39 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #7
  %40 = load float, float* %ds, align 4, !tbaa !6
  %mul21 = fmul float %40, 1.000000e+00
  %41 = load float, float* %ds, align 4, !tbaa !6
  %42 = load float, float* %de, align 4, !tbaa !6
  %sub22 = fsub float %41, %42
  %div23 = fdiv float %mul21, %sub22
  store float %div23, float* %ref.tmp20, align 4, !tbaa !6
  call void @_ZNK9btVector34lerpERKS_RKf(%class.btVector3* sret align 4 %ref.tmp19, %class.btVector3* %firstVertex, %class.btVector3* nonnull align 4 dereferenceable(16) %endVertex, float* nonnull align 4 dereferenceable(4) %ref.tmp20)
  call void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray* %37, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp19)
  %43 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #7
  %44 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %44) #7
  %45 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %ppVtxOut.addr, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray* %45, %class.btVector3* nonnull align 4 dereferenceable(16) %endVertex)
  br label %if.end24

if.end24:                                         ; preds = %if.then18, %if.else16
  br label %if.end25

if.end25:                                         ; preds = %if.end24, %if.end15
  %46 = bitcast %class.btVector3* %firstVertex to i8*
  %47 = bitcast %class.btVector3* %endVertex to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %46, i8* align 4 %47, i32 16, i1 false), !tbaa.struct !10
  %48 = load float, float* %de, align 4, !tbaa !6
  store float %48, float* %ds, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %if.end25
  %49 = load i32, i32* %ve, align 4, !tbaa !8
  %inc = add nsw i32 %49, 1
  store i32 %inc, i32* %ve, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %50 = bitcast %class.btVector3* %endVertex to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %50) #7
  %51 = bitcast %class.btVector3* %firstVertex to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %51) #7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then
  %52 = bitcast i32* %numVerts to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #7
  %53 = bitcast float* %de to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #7
  %54 = bitcast float* %ds to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #7
  %55 = bitcast i32* %ve to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !12
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %this, i32 %n) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !16
  %1 = load i32, i32* %n.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %1
  ret %class.btVector3* %arrayidx
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !6
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !6
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !6
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !6
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !6
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !6
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %_Val) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %class.btVector3*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %_Val, %class.btVector3** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !8
  %1 = load i32, i32* %sz, align 4, !tbaa !8
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !16
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !12
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 %3
  %4 = bitcast %class.btVector3* %arrayidx to i8*
  %call5 = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %4)
  %5 = bitcast i8* %call5 to %class.btVector3*
  %6 = load %class.btVector3*, %class.btVector3** %_Val.addr, align 4, !tbaa !2
  %7 = bitcast %class.btVector3* %5 to i8*
  %8 = bitcast %class.btVector3* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !10
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %9 = load i32, i32* %m_size6, align 4, !tbaa !12
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %m_size6, align 4, !tbaa !12
  %10 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector34lerpERKS_RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %t) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %t.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %t, float** %t.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !6
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %3 = load float, float* %arrayidx3, align 4, !tbaa !6
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 0
  %4 = load float, float* %arrayidx5, align 4, !tbaa !6
  %sub = fsub float %3, %4
  %5 = load float*, float** %t.addr, align 4, !tbaa !2
  %6 = load float, float* %5, align 4, !tbaa !6
  %mul = fmul float %sub, %6
  %add = fadd float %1, %mul
  store float %add, float* %ref.tmp, align 4, !tbaa !6
  %7 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %m_floats7 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 1
  %8 = load float, float* %arrayidx8, align 4, !tbaa !6
  %9 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 1
  %10 = load float, float* %arrayidx10, align 4, !tbaa !6
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 1
  %11 = load float, float* %arrayidx12, align 4, !tbaa !6
  %sub13 = fsub float %10, %11
  %12 = load float*, float** %t.addr, align 4, !tbaa !2
  %13 = load float, float* %12, align 4, !tbaa !6
  %mul14 = fmul float %sub13, %13
  %add15 = fadd float %8, %mul14
  store float %add15, float* %ref.tmp6, align 4, !tbaa !6
  %14 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #7
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %15 = load float, float* %arrayidx18, align 4, !tbaa !6
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats19 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [4 x float], [4 x float]* %m_floats19, i32 0, i32 2
  %17 = load float, float* %arrayidx20, align 4, !tbaa !6
  %m_floats21 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx22 = getelementptr inbounds [4 x float], [4 x float]* %m_floats21, i32 0, i32 2
  %18 = load float, float* %arrayidx22, align 4, !tbaa !6
  %sub23 = fsub float %17, %18
  %19 = load float*, float** %t.addr, align 4, !tbaa !2
  %20 = load float, float* %19, align 4, !tbaa !6
  %mul24 = fmul float %sub23, %20
  %add25 = fadd float %15, %mul24
  store float %add25, float* %ref.tmp16, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp16)
  %21 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #7
  %22 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #7
  %23 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #7
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

define hidden void @_Z24InverseTransformPoint3x3R9btVector3RKS_RK11btTransform(%class.btVector3* nonnull align 4 dereferenceable(16) %out, %class.btVector3* nonnull align 4 dereferenceable(16) %in, %class.btTransform* nonnull align 4 dereferenceable(64) %tr) #0 {
entry:
  %out.addr = alloca %class.btVector3*, align 4
  %in.addr = alloca %class.btVector3*, align 4
  %tr.addr = alloca %class.btTransform*, align 4
  %rot = alloca %class.btMatrix3x3*, align 4
  %r0 = alloca %class.btVector3*, align 4
  %r1 = alloca %class.btVector3*, align 4
  %r2 = alloca %class.btVector3*, align 4
  %x = alloca float, align 4
  %y = alloca float, align 4
  %z = alloca float, align 4
  store %class.btVector3* %out, %class.btVector3** %out.addr, align 4, !tbaa !2
  store %class.btVector3* %in, %class.btVector3** %in.addr, align 4, !tbaa !2
  store %class.btTransform* %tr, %class.btTransform** %tr.addr, align 4, !tbaa !2
  %0 = bitcast %class.btMatrix3x3** %rot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btTransform*, %class.btTransform** %tr.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %1)
  store %class.btMatrix3x3* %call, %class.btMatrix3x3** %rot, align 4, !tbaa !2
  %2 = bitcast %class.btVector3** %r0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot, align 4, !tbaa !2
  %call1 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %3, i32 0)
  store %class.btVector3* %call1, %class.btVector3** %r0, align 4, !tbaa !2
  %4 = bitcast %class.btVector3** %r1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %5, i32 1)
  store %class.btVector3* %call2, %class.btVector3** %r1, align 4, !tbaa !2
  %6 = bitcast %class.btVector3** %r2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 2)
  store %class.btVector3* %call3, %class.btVector3** %r2, align 4, !tbaa !2
  %8 = bitcast float* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  %9 = load %class.btVector3*, %class.btVector3** %r0, align 4, !tbaa !2
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %9)
  %10 = load float, float* %call4, align 4, !tbaa !6
  %11 = load %class.btVector3*, %class.btVector3** %in.addr, align 4, !tbaa !2
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %11)
  %12 = load float, float* %call5, align 4, !tbaa !6
  %mul = fmul float %10, %12
  %13 = load %class.btVector3*, %class.btVector3** %r1, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %13)
  %14 = load float, float* %call6, align 4, !tbaa !6
  %15 = load %class.btVector3*, %class.btVector3** %in.addr, align 4, !tbaa !2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %15)
  %16 = load float, float* %call7, align 4, !tbaa !6
  %mul8 = fmul float %14, %16
  %add = fadd float %mul, %mul8
  %17 = load %class.btVector3*, %class.btVector3** %r2, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %17)
  %18 = load float, float* %call9, align 4, !tbaa !6
  %19 = load %class.btVector3*, %class.btVector3** %in.addr, align 4, !tbaa !2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %19)
  %20 = load float, float* %call10, align 4, !tbaa !6
  %mul11 = fmul float %18, %20
  %add12 = fadd float %add, %mul11
  store float %add12, float* %x, align 4, !tbaa !6
  %21 = bitcast float* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #7
  %22 = load %class.btVector3*, %class.btVector3** %r0, align 4, !tbaa !2
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %22)
  %23 = load float, float* %call13, align 4, !tbaa !6
  %24 = load %class.btVector3*, %class.btVector3** %in.addr, align 4, !tbaa !2
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %24)
  %25 = load float, float* %call14, align 4, !tbaa !6
  %mul15 = fmul float %23, %25
  %26 = load %class.btVector3*, %class.btVector3** %r1, align 4, !tbaa !2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %26)
  %27 = load float, float* %call16, align 4, !tbaa !6
  %28 = load %class.btVector3*, %class.btVector3** %in.addr, align 4, !tbaa !2
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %28)
  %29 = load float, float* %call17, align 4, !tbaa !6
  %mul18 = fmul float %27, %29
  %add19 = fadd float %mul15, %mul18
  %30 = load %class.btVector3*, %class.btVector3** %r2, align 4, !tbaa !2
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %30)
  %31 = load float, float* %call20, align 4, !tbaa !6
  %32 = load %class.btVector3*, %class.btVector3** %in.addr, align 4, !tbaa !2
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %32)
  %33 = load float, float* %call21, align 4, !tbaa !6
  %mul22 = fmul float %31, %33
  %add23 = fadd float %add19, %mul22
  store float %add23, float* %y, align 4, !tbaa !6
  %34 = bitcast float* %z to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #7
  %35 = load %class.btVector3*, %class.btVector3** %r0, align 4, !tbaa !2
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %35)
  %36 = load float, float* %call24, align 4, !tbaa !6
  %37 = load %class.btVector3*, %class.btVector3** %in.addr, align 4, !tbaa !2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %37)
  %38 = load float, float* %call25, align 4, !tbaa !6
  %mul26 = fmul float %36, %38
  %39 = load %class.btVector3*, %class.btVector3** %r1, align 4, !tbaa !2
  %call27 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %39)
  %40 = load float, float* %call27, align 4, !tbaa !6
  %41 = load %class.btVector3*, %class.btVector3** %in.addr, align 4, !tbaa !2
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %41)
  %42 = load float, float* %call28, align 4, !tbaa !6
  %mul29 = fmul float %40, %42
  %add30 = fadd float %mul26, %mul29
  %43 = load %class.btVector3*, %class.btVector3** %r2, align 4, !tbaa !2
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %43)
  %44 = load float, float* %call31, align 4, !tbaa !6
  %45 = load %class.btVector3*, %class.btVector3** %in.addr, align 4, !tbaa !2
  %call32 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %45)
  %46 = load float, float* %call32, align 4, !tbaa !6
  %mul33 = fmul float %44, %46
  %add34 = fadd float %add30, %mul33
  store float %add34, float* %z, align 4, !tbaa !6
  %47 = load %class.btVector3*, %class.btVector3** %out.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %47, float* nonnull align 4 dereferenceable(4) %x, float* nonnull align 4 dereferenceable(4) %y, float* nonnull align 4 dereferenceable(4) %z)
  %48 = bitcast float* %z to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #7
  %49 = bitcast float* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #7
  %50 = bitcast float* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #7
  %51 = bitcast %class.btVector3** %r2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #7
  %52 = bitcast %class.btVector3** %r1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #7
  %53 = bitcast %class.btVector3** %r0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #7
  %54 = bitcast %class.btMatrix3x3** %rot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !8
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !6
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !6
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !6
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !6
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !6
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !6
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !6
  ret void
}

define hidden zeroext i1 @_Z19TestInternalObjectsRK11btTransformS1_RK9btVector3S4_RK18btConvexPolyhedronS7_f(%class.btTransform* nonnull align 4 dereferenceable(64) %trans0, %class.btTransform* nonnull align 4 dereferenceable(64) %trans1, %class.btVector3* nonnull align 4 dereferenceable(16) %delta_c, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %convex0, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %convex1, float %dmin) #0 {
entry:
  %retval = alloca i1, align 1
  %trans0.addr = alloca %class.btTransform*, align 4
  %trans1.addr = alloca %class.btTransform*, align 4
  %delta_c.addr = alloca %class.btVector3*, align 4
  %axis.addr = alloca %class.btVector3*, align 4
  %convex0.addr = alloca %class.btConvexPolyhedron*, align 4
  %convex1.addr = alloca %class.btConvexPolyhedron*, align 4
  %dmin.addr = alloca float, align 4
  %dp = alloca float, align 4
  %localAxis0 = alloca %class.btVector3, align 4
  %localAxis1 = alloca %class.btVector3, align 4
  %p0 = alloca [3 x float], align 4
  %p1 = alloca [3 x float], align 4
  %Radius0 = alloca float, align 4
  %Radius1 = alloca float, align 4
  %MinRadius = alloca float, align 4
  %MaxRadius = alloca float, align 4
  %MinMaxRadius = alloca float, align 4
  %d0 = alloca float, align 4
  %d1 = alloca float, align 4
  %depth = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btTransform* %trans0, %class.btTransform** %trans0.addr, align 4, !tbaa !2
  store %class.btTransform* %trans1, %class.btTransform** %trans1.addr, align 4, !tbaa !2
  store %class.btVector3* %delta_c, %class.btVector3** %delta_c.addr, align 4, !tbaa !2
  store %class.btVector3* %axis, %class.btVector3** %axis.addr, align 4, !tbaa !2
  store %class.btConvexPolyhedron* %convex0, %class.btConvexPolyhedron** %convex0.addr, align 4, !tbaa !2
  store %class.btConvexPolyhedron* %convex1, %class.btConvexPolyhedron** %convex1.addr, align 4, !tbaa !2
  store float %dmin, float* %dmin.addr, align 4, !tbaa !6
  %0 = bitcast float* %dp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btVector3*, %class.btVector3** %delta_c.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call, float* %dp, align 4, !tbaa !6
  %3 = bitcast %class.btVector3* %localAxis0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #7
  %call1 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %localAxis0)
  %4 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %5 = load %class.btTransform*, %class.btTransform** %trans0.addr, align 4, !tbaa !2
  call void @_Z24InverseTransformPoint3x3R9btVector3RKS_RK11btTransform(%class.btVector3* nonnull align 4 dereferenceable(16) %localAxis0, %class.btVector3* nonnull align 4 dereferenceable(16) %4, %class.btTransform* nonnull align 4 dereferenceable(64) %5)
  %6 = bitcast %class.btVector3* %localAxis1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #7
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %localAxis1)
  %7 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %8 = load %class.btTransform*, %class.btTransform** %trans1.addr, align 4, !tbaa !2
  call void @_Z24InverseTransformPoint3x3R9btVector3RKS_RK11btTransform(%class.btVector3* nonnull align 4 dereferenceable(16) %localAxis1, %class.btVector3* nonnull align 4 dereferenceable(16) %7, %class.btTransform* nonnull align 4 dereferenceable(64) %8)
  %9 = bitcast [3 x float]* %p0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %9) #7
  %10 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %convex0.addr, align 4, !tbaa !2
  %m_extents = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %10, i32 0, i32 5
  %call3 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents)
  %call4 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAxis0)
  %arraydecay = getelementptr inbounds [3 x float], [3 x float]* %p0, i32 0, i32 0
  call void @_Z10BoxSupportPKfS0_Pf(float* %call3, float* %call4, float* %arraydecay)
  %11 = bitcast [3 x float]* %p1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %11) #7
  %12 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %convex1.addr, align 4, !tbaa !2
  %m_extents5 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %12, i32 0, i32 5
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents5)
  %call7 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAxis1)
  %arraydecay8 = getelementptr inbounds [3 x float], [3 x float]* %p1, i32 0, i32 0
  call void @_Z10BoxSupportPKfS0_Pf(float* %call6, float* %call7, float* %arraydecay8)
  %13 = bitcast float* %Radius0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %arrayidx = getelementptr inbounds [3 x float], [3 x float]* %p0, i32 0, i32 0
  %14 = load float, float* %arrayidx, align 4, !tbaa !6
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %localAxis0)
  %15 = load float, float* %call9, align 4, !tbaa !6
  %mul = fmul float %14, %15
  %arrayidx10 = getelementptr inbounds [3 x float], [3 x float]* %p0, i32 0, i32 1
  %16 = load float, float* %arrayidx10, align 4, !tbaa !6
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %localAxis0)
  %17 = load float, float* %call11, align 4, !tbaa !6
  %mul12 = fmul float %16, %17
  %add = fadd float %mul, %mul12
  %arrayidx13 = getelementptr inbounds [3 x float], [3 x float]* %p0, i32 0, i32 2
  %18 = load float, float* %arrayidx13, align 4, !tbaa !6
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %localAxis0)
  %19 = load float, float* %call14, align 4, !tbaa !6
  %mul15 = fmul float %18, %19
  %add16 = fadd float %add, %mul15
  store float %add16, float* %Radius0, align 4, !tbaa !6
  %20 = bitcast float* %Radius1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #7
  %arrayidx17 = getelementptr inbounds [3 x float], [3 x float]* %p1, i32 0, i32 0
  %21 = load float, float* %arrayidx17, align 4, !tbaa !6
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %localAxis1)
  %22 = load float, float* %call18, align 4, !tbaa !6
  %mul19 = fmul float %21, %22
  %arrayidx20 = getelementptr inbounds [3 x float], [3 x float]* %p1, i32 0, i32 1
  %23 = load float, float* %arrayidx20, align 4, !tbaa !6
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %localAxis1)
  %24 = load float, float* %call21, align 4, !tbaa !6
  %mul22 = fmul float %23, %24
  %add23 = fadd float %mul19, %mul22
  %arrayidx24 = getelementptr inbounds [3 x float], [3 x float]* %p1, i32 0, i32 2
  %25 = load float, float* %arrayidx24, align 4, !tbaa !6
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %localAxis1)
  %26 = load float, float* %call25, align 4, !tbaa !6
  %mul26 = fmul float %25, %26
  %add27 = fadd float %add23, %mul26
  store float %add27, float* %Radius1, align 4, !tbaa !6
  %27 = bitcast float* %MinRadius to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #7
  %28 = load float, float* %Radius0, align 4, !tbaa !6
  %29 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %convex0.addr, align 4, !tbaa !2
  %m_radius = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %29, i32 0, i32 6
  %30 = load float, float* %m_radius, align 4, !tbaa !17
  %cmp = fcmp ogt float %28, %30
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %31 = load float, float* %Radius0, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %entry
  %32 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %convex0.addr, align 4, !tbaa !2
  %m_radius28 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %32, i32 0, i32 6
  %33 = load float, float* %m_radius28, align 4, !tbaa !17
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %31, %cond.true ], [ %33, %cond.false ]
  store float %cond, float* %MinRadius, align 4, !tbaa !6
  %34 = bitcast float* %MaxRadius to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #7
  %35 = load float, float* %Radius1, align 4, !tbaa !6
  %36 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %convex1.addr, align 4, !tbaa !2
  %m_radius29 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %36, i32 0, i32 6
  %37 = load float, float* %m_radius29, align 4, !tbaa !17
  %cmp30 = fcmp ogt float %35, %37
  br i1 %cmp30, label %cond.true31, label %cond.false32

cond.true31:                                      ; preds = %cond.end
  %38 = load float, float* %Radius1, align 4, !tbaa !6
  br label %cond.end34

cond.false32:                                     ; preds = %cond.end
  %39 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %convex1.addr, align 4, !tbaa !2
  %m_radius33 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %39, i32 0, i32 6
  %40 = load float, float* %m_radius33, align 4, !tbaa !17
  br label %cond.end34

cond.end34:                                       ; preds = %cond.false32, %cond.true31
  %cond35 = phi float [ %38, %cond.true31 ], [ %40, %cond.false32 ]
  store float %cond35, float* %MaxRadius, align 4, !tbaa !6
  %41 = bitcast float* %MinMaxRadius to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #7
  %42 = load float, float* %MaxRadius, align 4, !tbaa !6
  %43 = load float, float* %MinRadius, align 4, !tbaa !6
  %add36 = fadd float %42, %43
  store float %add36, float* %MinMaxRadius, align 4, !tbaa !6
  %44 = bitcast float* %d0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #7
  %45 = load float, float* %MinMaxRadius, align 4, !tbaa !6
  %46 = load float, float* %dp, align 4, !tbaa !6
  %add37 = fadd float %45, %46
  store float %add37, float* %d0, align 4, !tbaa !6
  %47 = bitcast float* %d1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #7
  %48 = load float, float* %MinMaxRadius, align 4, !tbaa !6
  %49 = load float, float* %dp, align 4, !tbaa !6
  %sub = fsub float %48, %49
  store float %sub, float* %d1, align 4, !tbaa !6
  %50 = bitcast float* %depth to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #7
  %51 = load float, float* %d0, align 4, !tbaa !6
  %52 = load float, float* %d1, align 4, !tbaa !6
  %cmp38 = fcmp olt float %51, %52
  br i1 %cmp38, label %cond.true39, label %cond.false40

cond.true39:                                      ; preds = %cond.end34
  %53 = load float, float* %d0, align 4, !tbaa !6
  br label %cond.end41

cond.false40:                                     ; preds = %cond.end34
  %54 = load float, float* %d1, align 4, !tbaa !6
  br label %cond.end41

cond.end41:                                       ; preds = %cond.false40, %cond.true39
  %cond42 = phi float [ %53, %cond.true39 ], [ %54, %cond.false40 ]
  store float %cond42, float* %depth, align 4, !tbaa !6
  %55 = load float, float* %depth, align 4, !tbaa !6
  %56 = load float, float* %dmin.addr, align 4, !tbaa !6
  %cmp43 = fcmp ogt float %55, %56
  br i1 %cmp43, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end41
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %cond.end41
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %57 = bitcast float* %depth to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #7
  %58 = bitcast float* %d1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #7
  %59 = bitcast float* %d0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #7
  %60 = bitcast float* %MinMaxRadius to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #7
  %61 = bitcast float* %MaxRadius to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #7
  %62 = bitcast float* %MinRadius to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #7
  %63 = bitcast float* %Radius1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #7
  %64 = bitcast float* %Radius0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #7
  %65 = bitcast [3 x float]* %p1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %65) #7
  %66 = bitcast [3 x float]* %p0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %66) #7
  %67 = bitcast %class.btVector3* %localAxis1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %67) #7
  %68 = bitcast %class.btVector3* %localAxis0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %68) #7
  %69 = bitcast float* %dp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #7
  %70 = load i1, i1* %retval, align 1
  ret i1 %70
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z10BoxSupportPKfS0_Pf(float* %extents, float* %sv, float* %p) #2 comdat {
entry:
  %extents.addr = alloca float*, align 4
  %sv.addr = alloca float*, align 4
  %p.addr = alloca float*, align 4
  store float* %extents, float** %extents.addr, align 4, !tbaa !2
  store float* %sv, float** %sv.addr, align 4, !tbaa !2
  store float* %p, float** %p.addr, align 4, !tbaa !2
  %0 = load float*, float** %sv.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds float, float* %0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !6
  %cmp = fcmp olt float %1, 0.000000e+00
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load float*, float** %extents.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds float, float* %2, i32 0
  %3 = load float, float* %arrayidx1, align 4, !tbaa !6
  %fneg = fneg float %3
  br label %cond.end

cond.false:                                       ; preds = %entry
  %4 = load float*, float** %extents.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds float, float* %4, i32 0
  %5 = load float, float* %arrayidx2, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %fneg, %cond.true ], [ %5, %cond.false ]
  %6 = load float*, float** %p.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds float, float* %6, i32 0
  store float %cond, float* %arrayidx3, align 4, !tbaa !6
  %7 = load float*, float** %sv.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds float, float* %7, i32 1
  %8 = load float, float* %arrayidx4, align 4, !tbaa !6
  %cmp5 = fcmp olt float %8, 0.000000e+00
  br i1 %cmp5, label %cond.true6, label %cond.false9

cond.true6:                                       ; preds = %cond.end
  %9 = load float*, float** %extents.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds float, float* %9, i32 1
  %10 = load float, float* %arrayidx7, align 4, !tbaa !6
  %fneg8 = fneg float %10
  br label %cond.end11

cond.false9:                                      ; preds = %cond.end
  %11 = load float*, float** %extents.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds float, float* %11, i32 1
  %12 = load float, float* %arrayidx10, align 4, !tbaa !6
  br label %cond.end11

cond.end11:                                       ; preds = %cond.false9, %cond.true6
  %cond12 = phi float [ %fneg8, %cond.true6 ], [ %12, %cond.false9 ]
  %13 = load float*, float** %p.addr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds float, float* %13, i32 1
  store float %cond12, float* %arrayidx13, align 4, !tbaa !6
  %14 = load float*, float** %sv.addr, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds float, float* %14, i32 2
  %15 = load float, float* %arrayidx14, align 4, !tbaa !6
  %cmp15 = fcmp olt float %15, 0.000000e+00
  br i1 %cmp15, label %cond.true16, label %cond.false19

cond.true16:                                      ; preds = %cond.end11
  %16 = load float*, float** %extents.addr, align 4, !tbaa !2
  %arrayidx17 = getelementptr inbounds float, float* %16, i32 2
  %17 = load float, float* %arrayidx17, align 4, !tbaa !6
  %fneg18 = fneg float %17
  br label %cond.end21

cond.false19:                                     ; preds = %cond.end11
  %18 = load float*, float** %extents.addr, align 4, !tbaa !2
  %arrayidx20 = getelementptr inbounds float, float* %18, i32 2
  %19 = load float, float* %arrayidx20, align 4, !tbaa !6
  br label %cond.end21

cond.end21:                                       ; preds = %cond.false19, %cond.true16
  %cond22 = phi float [ %fneg18, %cond.true16 ], [ %19, %cond.false19 ]
  %20 = load float*, float** %p.addr, align 4, !tbaa !2
  %arrayidx23 = getelementptr inbounds float, float* %20, i32 2
  store float %cond22, float* %arrayidx23, align 4, !tbaa !6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

define hidden zeroext i1 @_ZN27btPolyhedralContactClipping18findSeparatingAxisERK18btConvexPolyhedronS2_RK11btTransformS5_R9btVector3RN36btDiscreteCollisionDetectorInterface6ResultE(%class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %hullA, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %hullB, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB, %class.btVector3* nonnull align 4 dereferenceable(16) %sep, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %resultOut) #0 {
entry:
  %retval = alloca i1, align 1
  %hullA.addr = alloca %class.btConvexPolyhedron*, align 4
  %hullB.addr = alloca %class.btConvexPolyhedron*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %sep.addr = alloca %class.btVector3*, align 4
  %resultOut.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  %c0 = alloca %class.btVector3, align 4
  %c1 = alloca %class.btVector3, align 4
  %DeltaC2 = alloca %class.btVector3, align 4
  %dmin = alloca float, align 4
  %curPlaneTests = alloca i32, align 4
  %numFacesA = alloca i32, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %Normal = alloca %class.btVector3, align 4
  %faceANormalWS = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %d = alloca float, align 4
  %wA = alloca %class.btVector3, align 4
  %wB = alloca %class.btVector3, align 4
  %numFacesB = alloca i32, align 4
  %i40 = alloca i32, align 4
  %Normal45 = alloca %class.btVector3, align 4
  %WorldNormal = alloca %class.btVector3, align 4
  %ref.tmp63 = alloca float, align 4
  %d74 = alloca float, align 4
  %wA75 = alloca %class.btVector3, align 4
  %wB77 = alloca %class.btVector3, align 4
  %edgeAstart = alloca %class.btVector3, align 4
  %edgeAend = alloca %class.btVector3, align 4
  %edgeBstart = alloca %class.btVector3, align 4
  %edgeBend = alloca %class.btVector3, align 4
  %edgeA = alloca i32, align 4
  %edgeB = alloca i32, align 4
  %worldEdgeA = alloca %class.btVector3, align 4
  %worldEdgeB = alloca %class.btVector3, align 4
  %witnessPointA = alloca %class.btVector3, align 4
  %witnessPointB = alloca %class.btVector3, align 4
  %curEdgeEdge = alloca i32, align 4
  %e0 = alloca i32, align 4
  %edge0 = alloca %class.btVector3, align 4
  %WorldEdge0 = alloca %class.btVector3, align 4
  %e1 = alloca i32, align 4
  %edge1 = alloca %class.btVector3, align 4
  %WorldEdge1 = alloca %class.btVector3, align 4
  %Cross = alloca %class.btVector3, align 4
  %ref.tmp129 = alloca float, align 4
  %dist = alloca float, align 4
  %wA139 = alloca %class.btVector3, align 4
  %wB141 = alloca %class.btVector3, align 4
  %ptsVector = alloca %class.btVector3, align 4
  %offsetA = alloca %class.btVector3, align 4
  %offsetB = alloca %class.btVector3, align 4
  %tA = alloca float, align 4
  %tB = alloca float, align 4
  %translation = alloca %class.btVector3, align 4
  %dirA = alloca %class.btVector3, align 4
  %dirB = alloca %class.btVector3, align 4
  %hlenB = alloca float, align 4
  %hlenA = alloca float, align 4
  %nlSqrt = alloca float, align 4
  %nl = alloca float, align 4
  %ref.tmp185 = alloca float, align 4
  %ref.tmp190 = alloca float, align 4
  %ptOnB = alloca %class.btVector3, align 4
  %distance = alloca float, align 4
  %ref.tmp198 = alloca %class.btVector3, align 4
  store %class.btConvexPolyhedron* %hullA, %class.btConvexPolyhedron** %hullA.addr, align 4, !tbaa !2
  store %class.btConvexPolyhedron* %hullB, %class.btConvexPolyhedron** %hullB.addr, align 4, !tbaa !2
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4, !tbaa !2
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4, !tbaa !2
  store %class.btVector3* %sep, %class.btVector3** %sep.addr, align 4, !tbaa !2
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %resultOut, %"struct.btDiscreteCollisionDetectorInterface::Result"** %resultOut.addr, align 4, !tbaa !2
  %0 = load i32, i32* @_ZL19gActualSATPairTests, align 4, !tbaa !8
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* @_ZL19gActualSATPairTests, align 4, !tbaa !8
  %1 = bitcast %class.btVector3* %c0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #7
  %2 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %3 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4, !tbaa !2
  %m_localCenter = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %3, i32 0, i32 4
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %c0, %class.btTransform* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localCenter)
  %4 = bitcast %class.btVector3* %c1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #7
  %5 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %6 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4, !tbaa !2
  %m_localCenter1 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %6, i32 0, i32 4
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %c1, %class.btTransform* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localCenter1)
  %7 = bitcast %class.btVector3* %DeltaC2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #7
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %DeltaC2, %class.btVector3* nonnull align 4 dereferenceable(16) %c0, %class.btVector3* nonnull align 4 dereferenceable(16) %c1)
  %8 = bitcast float* %dmin to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  store float 0x47EFFFFFE0000000, float* %dmin, align 4, !tbaa !6
  %9 = bitcast i32* %curPlaneTests to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  store i32 0, i32* %curPlaneTests, align 4, !tbaa !8
  %10 = bitcast i32* %numFacesA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4, !tbaa !2
  %m_faces = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %11, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %m_faces)
  store i32 %call, i32* %numFacesA, align 4, !tbaa !8
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #7
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %13 = load i32, i32* %i, align 4, !tbaa !8
  %14 = load i32, i32* %numFacesA, align 4, !tbaa !8
  %cmp = icmp slt i32 %13, %14
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup36

for.body:                                         ; preds = %for.cond
  %15 = bitcast %class.btVector3* %Normal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %15) #7
  %16 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4, !tbaa !2
  %m_faces2 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %16, i32 0, i32 2
  %17 = load i32, i32* %i, align 4, !tbaa !8
  %call3 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces2, i32 %17)
  %m_plane = getelementptr inbounds %struct.btFace, %struct.btFace* %call3, i32 0, i32 1
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_plane, i32 0, i32 0
  %18 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4, !tbaa !2
  %m_faces4 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %18, i32 0, i32 2
  %19 = load i32, i32* %i, align 4, !tbaa !8
  %call5 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces4, i32 %19)
  %m_plane6 = getelementptr inbounds %struct.btFace, %struct.btFace* %call5, i32 0, i32 1
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_plane6, i32 0, i32 1
  %20 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4, !tbaa !2
  %m_faces8 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %20, i32 0, i32 2
  %21 = load i32, i32* %i, align 4, !tbaa !8
  %call9 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces8, i32 %21)
  %m_plane10 = getelementptr inbounds %struct.btFace, %struct.btFace* %call9, i32 0, i32 1
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_plane10, i32 0, i32 2
  %call12 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %Normal, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx7, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %22 = bitcast %class.btVector3* %faceANormalWS to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %22) #7
  %23 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %call13 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %23)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %faceANormalWS, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call13, %class.btVector3* nonnull align 4 dereferenceable(16) %Normal)
  %call14 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %DeltaC2, %class.btVector3* nonnull align 4 dereferenceable(16) %faceANormalWS)
  %cmp15 = fcmp olt float %call14, 0.000000e+00
  br i1 %cmp15, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %24 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #7
  store float -1.000000e+00, float* %ref.tmp, align 4, !tbaa !6
  %call16 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %faceANormalWS, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %25 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %26 = load i32, i32* %curPlaneTests, align 4, !tbaa !8
  %inc17 = add nsw i32 %26, 1
  store i32 %inc17, i32* %curPlaneTests, align 4, !tbaa !8
  %27 = load i32, i32* @gExpectedNbTests, align 4, !tbaa !8
  %inc18 = add nsw i32 %27, 1
  store i32 %inc18, i32* @gExpectedNbTests, align 4, !tbaa !8
  %28 = load i8, i8* @gUseInternalObject, align 1, !tbaa !22, !range !23
  %tobool = trunc i8 %28 to i1
  br i1 %tobool, label %land.lhs.true, label %if.end21

land.lhs.true:                                    ; preds = %if.end
  %29 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %30 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %31 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4, !tbaa !2
  %32 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4, !tbaa !2
  %33 = load float, float* %dmin, align 4, !tbaa !6
  %call19 = call zeroext i1 @_Z19TestInternalObjectsRK11btTransformS1_RK9btVector3S4_RK18btConvexPolyhedronS7_f(%class.btTransform* nonnull align 4 dereferenceable(64) %29, %class.btTransform* nonnull align 4 dereferenceable(64) %30, %class.btVector3* nonnull align 4 dereferenceable(16) %DeltaC2, %class.btVector3* nonnull align 4 dereferenceable(16) %faceANormalWS, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %31, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %32, float %33)
  br i1 %call19, label %if.end21, label %if.then20

if.then20:                                        ; preds = %land.lhs.true
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup33

if.end21:                                         ; preds = %land.lhs.true, %if.end
  %34 = load i32, i32* @gActualNbTests, align 4, !tbaa !8
  %inc22 = add nsw i32 %34, 1
  store i32 %inc22, i32* @gActualNbTests, align 4, !tbaa !8
  %35 = bitcast float* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #7
  %36 = bitcast %class.btVector3* %wA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %36) #7
  %call23 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %wA)
  %37 = bitcast %class.btVector3* %wB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %37) #7
  %call24 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %wB)
  %38 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4, !tbaa !2
  %39 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4, !tbaa !2
  %40 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %41 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %call25 = call zeroext i1 @_ZL11TestSepAxisRK18btConvexPolyhedronS1_RK11btTransformS4_RK9btVector3RfRS5_S9_(%class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %38, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %39, %class.btTransform* nonnull align 4 dereferenceable(64) %40, %class.btTransform* nonnull align 4 dereferenceable(64) %41, %class.btVector3* nonnull align 4 dereferenceable(16) %faceANormalWS, float* nonnull align 4 dereferenceable(4) %d, %class.btVector3* nonnull align 4 dereferenceable(16) %wA, %class.btVector3* nonnull align 4 dereferenceable(16) %wB)
  br i1 %call25, label %if.end27, label %if.then26

if.then26:                                        ; preds = %if.end21
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end27:                                         ; preds = %if.end21
  %42 = load float, float* %d, align 4, !tbaa !6
  %43 = load float, float* %dmin, align 4, !tbaa !6
  %cmp28 = fcmp olt float %42, %43
  br i1 %cmp28, label %if.then29, label %if.end30

if.then29:                                        ; preds = %if.end27
  %44 = load float, float* %d, align 4, !tbaa !6
  store float %44, float* %dmin, align 4, !tbaa !6
  %45 = load %class.btVector3*, %class.btVector3** %sep.addr, align 4, !tbaa !2
  %46 = bitcast %class.btVector3* %45 to i8*
  %47 = bitcast %class.btVector3* %faceANormalWS to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %46, i8* align 4 %47, i32 16, i1 false), !tbaa.struct !10
  br label %if.end30

if.end30:                                         ; preds = %if.then29, %if.end27
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end30, %if.then26
  %48 = bitcast %class.btVector3* %wB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %48) #7
  %49 = bitcast %class.btVector3* %wA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %49) #7
  %50 = bitcast float* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #7
  br label %cleanup33

cleanup33:                                        ; preds = %cleanup, %if.then20
  %51 = bitcast %class.btVector3* %faceANormalWS to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %51) #7
  %52 = bitcast %class.btVector3* %Normal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %52) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup36 [
    i32 0, label %cleanup.cont
    i32 4, label %for.inc
  ]

cleanup.cont:                                     ; preds = %cleanup33
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont, %cleanup33
  %53 = load i32, i32* %i, align 4, !tbaa !8
  %inc35 = add nsw i32 %53, 1
  store i32 %inc35, i32* %i, align 4, !tbaa !8
  br label %for.cond

cleanup36:                                        ; preds = %cleanup33, %for.cond.cleanup
  %54 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #7
  %cleanup.dest37 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest37, label %cleanup212 [
    i32 2, label %for.end
  ]

for.end:                                          ; preds = %cleanup36
  %55 = bitcast i32* %numFacesB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #7
  %56 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4, !tbaa !2
  %m_faces38 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %56, i32 0, i32 2
  %call39 = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %m_faces38)
  store i32 %call39, i32* %numFacesB, align 4, !tbaa !8
  %57 = bitcast i32* %i40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %57) #7
  store i32 0, i32* %i40, align 4, !tbaa !8
  br label %for.cond41

for.cond41:                                       ; preds = %for.inc92, %for.end
  %58 = load i32, i32* %i40, align 4, !tbaa !8
  %59 = load i32, i32* %numFacesB, align 4, !tbaa !8
  %cmp42 = icmp slt i32 %58, %59
  br i1 %cmp42, label %for.body44, label %for.cond.cleanup43

for.cond.cleanup43:                               ; preds = %for.cond41
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup94

for.body44:                                       ; preds = %for.cond41
  %60 = bitcast %class.btVector3* %Normal45 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %60) #7
  %61 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4, !tbaa !2
  %m_faces46 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %61, i32 0, i32 2
  %62 = load i32, i32* %i40, align 4, !tbaa !8
  %call47 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces46, i32 %62)
  %m_plane48 = getelementptr inbounds %struct.btFace, %struct.btFace* %call47, i32 0, i32 1
  %arrayidx49 = getelementptr inbounds [4 x float], [4 x float]* %m_plane48, i32 0, i32 0
  %63 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4, !tbaa !2
  %m_faces50 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %63, i32 0, i32 2
  %64 = load i32, i32* %i40, align 4, !tbaa !8
  %call51 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces50, i32 %64)
  %m_plane52 = getelementptr inbounds %struct.btFace, %struct.btFace* %call51, i32 0, i32 1
  %arrayidx53 = getelementptr inbounds [4 x float], [4 x float]* %m_plane52, i32 0, i32 1
  %65 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4, !tbaa !2
  %m_faces54 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %65, i32 0, i32 2
  %66 = load i32, i32* %i40, align 4, !tbaa !8
  %call55 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces54, i32 %66)
  %m_plane56 = getelementptr inbounds %struct.btFace, %struct.btFace* %call55, i32 0, i32 1
  %arrayidx57 = getelementptr inbounds [4 x float], [4 x float]* %m_plane56, i32 0, i32 2
  %call58 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %Normal45, float* nonnull align 4 dereferenceable(4) %arrayidx49, float* nonnull align 4 dereferenceable(4) %arrayidx53, float* nonnull align 4 dereferenceable(4) %arrayidx57)
  %67 = bitcast %class.btVector3* %WorldNormal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %67) #7
  %68 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %call59 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %68)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %WorldNormal, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call59, %class.btVector3* nonnull align 4 dereferenceable(16) %Normal45)
  %call60 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %DeltaC2, %class.btVector3* nonnull align 4 dereferenceable(16) %WorldNormal)
  %cmp61 = fcmp olt float %call60, 0.000000e+00
  br i1 %cmp61, label %if.then62, label %if.end65

if.then62:                                        ; preds = %for.body44
  %69 = bitcast float* %ref.tmp63 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #7
  store float -1.000000e+00, float* %ref.tmp63, align 4, !tbaa !6
  %call64 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %WorldNormal, float* nonnull align 4 dereferenceable(4) %ref.tmp63)
  %70 = bitcast float* %ref.tmp63 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #7
  br label %if.end65

if.end65:                                         ; preds = %if.then62, %for.body44
  %71 = load i32, i32* %curPlaneTests, align 4, !tbaa !8
  %inc66 = add nsw i32 %71, 1
  store i32 %inc66, i32* %curPlaneTests, align 4, !tbaa !8
  %72 = load i32, i32* @gExpectedNbTests, align 4, !tbaa !8
  %inc67 = add nsw i32 %72, 1
  store i32 %inc67, i32* @gExpectedNbTests, align 4, !tbaa !8
  %73 = load i8, i8* @gUseInternalObject, align 1, !tbaa !22, !range !23
  %tobool68 = trunc i8 %73 to i1
  br i1 %tobool68, label %land.lhs.true69, label %if.end72

land.lhs.true69:                                  ; preds = %if.end65
  %74 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %75 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %76 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4, !tbaa !2
  %77 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4, !tbaa !2
  %78 = load float, float* %dmin, align 4, !tbaa !6
  %call70 = call zeroext i1 @_Z19TestInternalObjectsRK11btTransformS1_RK9btVector3S4_RK18btConvexPolyhedronS7_f(%class.btTransform* nonnull align 4 dereferenceable(64) %74, %class.btTransform* nonnull align 4 dereferenceable(64) %75, %class.btVector3* nonnull align 4 dereferenceable(16) %DeltaC2, %class.btVector3* nonnull align 4 dereferenceable(16) %WorldNormal, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %76, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %77, float %78)
  br i1 %call70, label %if.end72, label %if.then71

if.then71:                                        ; preds = %land.lhs.true69
  store i32 7, i32* %cleanup.dest.slot, align 4
  br label %cleanup88

if.end72:                                         ; preds = %land.lhs.true69, %if.end65
  %79 = load i32, i32* @gActualNbTests, align 4, !tbaa !8
  %inc73 = add nsw i32 %79, 1
  store i32 %inc73, i32* @gActualNbTests, align 4, !tbaa !8
  %80 = bitcast float* %d74 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #7
  %81 = bitcast %class.btVector3* %wA75 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %81) #7
  %call76 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %wA75)
  %82 = bitcast %class.btVector3* %wB77 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %82) #7
  %call78 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %wB77)
  %83 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4, !tbaa !2
  %84 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4, !tbaa !2
  %85 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %86 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %call79 = call zeroext i1 @_ZL11TestSepAxisRK18btConvexPolyhedronS1_RK11btTransformS4_RK9btVector3RfRS5_S9_(%class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %83, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %84, %class.btTransform* nonnull align 4 dereferenceable(64) %85, %class.btTransform* nonnull align 4 dereferenceable(64) %86, %class.btVector3* nonnull align 4 dereferenceable(16) %WorldNormal, float* nonnull align 4 dereferenceable(4) %d74, %class.btVector3* nonnull align 4 dereferenceable(16) %wA75, %class.btVector3* nonnull align 4 dereferenceable(16) %wB77)
  br i1 %call79, label %if.end81, label %if.then80

if.then80:                                        ; preds = %if.end72
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup85

if.end81:                                         ; preds = %if.end72
  %87 = load float, float* %d74, align 4, !tbaa !6
  %88 = load float, float* %dmin, align 4, !tbaa !6
  %cmp82 = fcmp olt float %87, %88
  br i1 %cmp82, label %if.then83, label %if.end84

if.then83:                                        ; preds = %if.end81
  %89 = load float, float* %d74, align 4, !tbaa !6
  store float %89, float* %dmin, align 4, !tbaa !6
  %90 = load %class.btVector3*, %class.btVector3** %sep.addr, align 4, !tbaa !2
  %91 = bitcast %class.btVector3* %90 to i8*
  %92 = bitcast %class.btVector3* %WorldNormal to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %91, i8* align 4 %92, i32 16, i1 false), !tbaa.struct !10
  br label %if.end84

if.end84:                                         ; preds = %if.then83, %if.end81
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup85

cleanup85:                                        ; preds = %if.end84, %if.then80
  %93 = bitcast %class.btVector3* %wB77 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %93) #7
  %94 = bitcast %class.btVector3* %wA75 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %94) #7
  %95 = bitcast float* %d74 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #7
  br label %cleanup88

cleanup88:                                        ; preds = %cleanup85, %if.then71
  %96 = bitcast %class.btVector3* %WorldNormal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %96) #7
  %97 = bitcast %class.btVector3* %Normal45 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %97) #7
  %cleanup.dest90 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest90, label %cleanup94 [
    i32 0, label %cleanup.cont91
    i32 7, label %for.inc92
  ]

cleanup.cont91:                                   ; preds = %cleanup88
  br label %for.inc92

for.inc92:                                        ; preds = %cleanup.cont91, %cleanup88
  %98 = load i32, i32* %i40, align 4, !tbaa !8
  %inc93 = add nsw i32 %98, 1
  store i32 %inc93, i32* %i40, align 4, !tbaa !8
  br label %for.cond41

cleanup94:                                        ; preds = %cleanup88, %for.cond.cleanup43
  %99 = bitcast i32* %i40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #7
  %cleanup.dest95 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest95, label %cleanup211 [
    i32 5, label %for.end96
  ]

for.end96:                                        ; preds = %cleanup94
  %100 = bitcast %class.btVector3* %edgeAstart to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %100) #7
  %call97 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %edgeAstart)
  %101 = bitcast %class.btVector3* %edgeAend to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %101) #7
  %call98 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %edgeAend)
  %102 = bitcast %class.btVector3* %edgeBstart to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %102) #7
  %call99 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %edgeBstart)
  %103 = bitcast %class.btVector3* %edgeBend to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %103) #7
  %call100 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %edgeBend)
  %104 = bitcast i32* %edgeA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %104) #7
  store i32 -1, i32* %edgeA, align 4, !tbaa !8
  %105 = bitcast i32* %edgeB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %105) #7
  store i32 -1, i32* %edgeB, align 4, !tbaa !8
  %106 = bitcast %class.btVector3* %worldEdgeA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %106) #7
  %call101 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %worldEdgeA)
  %107 = bitcast %class.btVector3* %worldEdgeB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %107) #7
  %call102 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %worldEdgeB)
  %108 = bitcast %class.btVector3* %witnessPointA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %108) #7
  %call103 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %witnessPointA)
  %109 = bitcast %class.btVector3* %witnessPointB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %109) #7
  %call104 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %witnessPointB)
  %110 = bitcast i32* %curEdgeEdge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %110) #7
  store i32 0, i32* %curEdgeEdge, align 4, !tbaa !8
  %111 = bitcast i32* %e0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %111) #7
  store i32 0, i32* %e0, align 4, !tbaa !8
  br label %for.cond105

for.cond105:                                      ; preds = %for.inc169, %for.end96
  %112 = load i32, i32* %e0, align 4, !tbaa !8
  %113 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4, !tbaa !2
  %m_uniqueEdges = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %113, i32 0, i32 3
  %call106 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %m_uniqueEdges)
  %cmp107 = icmp slt i32 %112, %call106
  br i1 %cmp107, label %for.body109, label %for.cond.cleanup108

for.cond.cleanup108:                              ; preds = %for.cond105
  store i32 8, i32* %cleanup.dest.slot, align 4
  br label %cleanup171

for.body109:                                      ; preds = %for.cond105
  %114 = bitcast %class.btVector3* %edge0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %114) #7
  %115 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4, !tbaa !2
  %m_uniqueEdges110 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %115, i32 0, i32 3
  %116 = load i32, i32* %e0, align 4, !tbaa !8
  %call111 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_uniqueEdges110, i32 %116)
  %117 = bitcast %class.btVector3* %edge0 to i8*
  %118 = bitcast %class.btVector3* %call111 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %117, i8* align 4 %118, i32 16, i1 false), !tbaa.struct !10
  %119 = bitcast %class.btVector3* %WorldEdge0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %119) #7
  %120 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %call112 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %120)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %WorldEdge0, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call112, %class.btVector3* nonnull align 4 dereferenceable(16) %edge0)
  %121 = bitcast i32* %e1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %121) #7
  store i32 0, i32* %e1, align 4, !tbaa !8
  br label %for.cond113

for.cond113:                                      ; preds = %for.inc160, %for.body109
  %122 = load i32, i32* %e1, align 4, !tbaa !8
  %123 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4, !tbaa !2
  %m_uniqueEdges114 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %123, i32 0, i32 3
  %call115 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %m_uniqueEdges114)
  %cmp116 = icmp slt i32 %122, %call115
  br i1 %cmp116, label %for.body118, label %for.cond.cleanup117

for.cond.cleanup117:                              ; preds = %for.cond113
  store i32 11, i32* %cleanup.dest.slot, align 4
  br label %cleanup162

for.body118:                                      ; preds = %for.cond113
  %124 = bitcast %class.btVector3* %edge1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %124) #7
  %125 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4, !tbaa !2
  %m_uniqueEdges119 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %125, i32 0, i32 3
  %126 = load i32, i32* %e1, align 4, !tbaa !8
  %call120 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_uniqueEdges119, i32 %126)
  %127 = bitcast %class.btVector3* %edge1 to i8*
  %128 = bitcast %class.btVector3* %call120 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %127, i8* align 4 %128, i32 16, i1 false), !tbaa.struct !10
  %129 = bitcast %class.btVector3* %WorldEdge1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %129) #7
  %130 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %call121 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %130)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %WorldEdge1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call121, %class.btVector3* nonnull align 4 dereferenceable(16) %edge1)
  %131 = bitcast %class.btVector3* %Cross to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %131) #7
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %Cross, %class.btVector3* %WorldEdge0, %class.btVector3* nonnull align 4 dereferenceable(16) %WorldEdge1)
  %132 = load i32, i32* %curEdgeEdge, align 4, !tbaa !8
  %inc122 = add nsw i32 %132, 1
  store i32 %inc122, i32* %curEdgeEdge, align 4, !tbaa !8
  %call123 = call zeroext i1 @_Z12IsAlmostZeroRK9btVector3(%class.btVector3* nonnull align 4 dereferenceable(16) %Cross)
  br i1 %call123, label %if.end154, label %if.then124

if.then124:                                       ; preds = %for.body118
  %call125 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %Cross)
  %133 = bitcast %class.btVector3* %Cross to i8*
  %134 = bitcast %class.btVector3* %call125 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %133, i8* align 4 %134, i32 16, i1 false), !tbaa.struct !10
  %call126 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %DeltaC2, %class.btVector3* nonnull align 4 dereferenceable(16) %Cross)
  %cmp127 = fcmp olt float %call126, 0.000000e+00
  br i1 %cmp127, label %if.then128, label %if.end131

if.then128:                                       ; preds = %if.then124
  %135 = bitcast float* %ref.tmp129 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %135) #7
  store float -1.000000e+00, float* %ref.tmp129, align 4, !tbaa !6
  %call130 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %Cross, float* nonnull align 4 dereferenceable(4) %ref.tmp129)
  %136 = bitcast float* %ref.tmp129 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %136) #7
  br label %if.end131

if.end131:                                        ; preds = %if.then128, %if.then124
  %137 = load i32, i32* @gExpectedNbTests, align 4, !tbaa !8
  %inc132 = add nsw i32 %137, 1
  store i32 %inc132, i32* @gExpectedNbTests, align 4, !tbaa !8
  %138 = load i8, i8* @gUseInternalObject, align 1, !tbaa !22, !range !23
  %tobool133 = trunc i8 %138 to i1
  br i1 %tobool133, label %land.lhs.true134, label %if.end137

land.lhs.true134:                                 ; preds = %if.end131
  %139 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %140 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %141 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4, !tbaa !2
  %142 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4, !tbaa !2
  %143 = load float, float* %dmin, align 4, !tbaa !6
  %call135 = call zeroext i1 @_Z19TestInternalObjectsRK11btTransformS1_RK9btVector3S4_RK18btConvexPolyhedronS7_f(%class.btTransform* nonnull align 4 dereferenceable(64) %139, %class.btTransform* nonnull align 4 dereferenceable(64) %140, %class.btVector3* nonnull align 4 dereferenceable(16) %DeltaC2, %class.btVector3* nonnull align 4 dereferenceable(16) %Cross, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %141, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %142, float %143)
  br i1 %call135, label %if.end137, label %if.then136

if.then136:                                       ; preds = %land.lhs.true134
  store i32 13, i32* %cleanup.dest.slot, align 4
  br label %cleanup155

if.end137:                                        ; preds = %land.lhs.true134, %if.end131
  %144 = load i32, i32* @gActualNbTests, align 4, !tbaa !8
  %inc138 = add nsw i32 %144, 1
  store i32 %inc138, i32* @gActualNbTests, align 4, !tbaa !8
  %145 = bitcast float* %dist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %145) #7
  %146 = bitcast %class.btVector3* %wA139 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %146) #7
  %call140 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %wA139)
  %147 = bitcast %class.btVector3* %wB141 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %147) #7
  %call142 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %wB141)
  %148 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4, !tbaa !2
  %149 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4, !tbaa !2
  %150 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %151 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %call143 = call zeroext i1 @_ZL11TestSepAxisRK18btConvexPolyhedronS1_RK11btTransformS4_RK9btVector3RfRS5_S9_(%class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %148, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %149, %class.btTransform* nonnull align 4 dereferenceable(64) %150, %class.btTransform* nonnull align 4 dereferenceable(64) %151, %class.btVector3* nonnull align 4 dereferenceable(16) %Cross, float* nonnull align 4 dereferenceable(4) %dist, %class.btVector3* nonnull align 4 dereferenceable(16) %wA139, %class.btVector3* nonnull align 4 dereferenceable(16) %wB141)
  br i1 %call143, label %if.end145, label %if.then144

if.then144:                                       ; preds = %if.end137
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup149

if.end145:                                        ; preds = %if.end137
  %152 = load float, float* %dist, align 4, !tbaa !6
  %153 = load float, float* %dmin, align 4, !tbaa !6
  %cmp146 = fcmp olt float %152, %153
  br i1 %cmp146, label %if.then147, label %if.end148

if.then147:                                       ; preds = %if.end145
  %154 = load float, float* %dist, align 4, !tbaa !6
  store float %154, float* %dmin, align 4, !tbaa !6
  %155 = load %class.btVector3*, %class.btVector3** %sep.addr, align 4, !tbaa !2
  %156 = bitcast %class.btVector3* %155 to i8*
  %157 = bitcast %class.btVector3* %Cross to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %156, i8* align 4 %157, i32 16, i1 false), !tbaa.struct !10
  %158 = load i32, i32* %e0, align 4, !tbaa !8
  store i32 %158, i32* %edgeA, align 4, !tbaa !8
  %159 = load i32, i32* %e1, align 4, !tbaa !8
  store i32 %159, i32* %edgeB, align 4, !tbaa !8
  %160 = bitcast %class.btVector3* %worldEdgeA to i8*
  %161 = bitcast %class.btVector3* %WorldEdge0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %160, i8* align 4 %161, i32 16, i1 false), !tbaa.struct !10
  %162 = bitcast %class.btVector3* %worldEdgeB to i8*
  %163 = bitcast %class.btVector3* %WorldEdge1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %162, i8* align 4 %163, i32 16, i1 false), !tbaa.struct !10
  %164 = bitcast %class.btVector3* %witnessPointA to i8*
  %165 = bitcast %class.btVector3* %wA139 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %164, i8* align 4 %165, i32 16, i1 false), !tbaa.struct !10
  %166 = bitcast %class.btVector3* %witnessPointB to i8*
  %167 = bitcast %class.btVector3* %wB141 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %166, i8* align 4 %167, i32 16, i1 false), !tbaa.struct !10
  br label %if.end148

if.end148:                                        ; preds = %if.then147, %if.end145
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup149

cleanup149:                                       ; preds = %if.end148, %if.then144
  %168 = bitcast %class.btVector3* %wB141 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %168) #7
  %169 = bitcast %class.btVector3* %wA139 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %169) #7
  %170 = bitcast float* %dist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %170) #7
  %cleanup.dest152 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest152, label %cleanup155 [
    i32 0, label %cleanup.cont153
  ]

cleanup.cont153:                                  ; preds = %cleanup149
  br label %if.end154

if.end154:                                        ; preds = %cleanup.cont153, %for.body118
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup155

cleanup155:                                       ; preds = %if.end154, %cleanup149, %if.then136
  %171 = bitcast %class.btVector3* %Cross to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %171) #7
  %172 = bitcast %class.btVector3* %WorldEdge1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %172) #7
  %173 = bitcast %class.btVector3* %edge1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %173) #7
  %cleanup.dest158 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest158, label %cleanup162 [
    i32 0, label %cleanup.cont159
    i32 13, label %for.inc160
  ]

cleanup.cont159:                                  ; preds = %cleanup155
  br label %for.inc160

for.inc160:                                       ; preds = %cleanup.cont159, %cleanup155
  %174 = load i32, i32* %e1, align 4, !tbaa !8
  %inc161 = add nsw i32 %174, 1
  store i32 %inc161, i32* %e1, align 4, !tbaa !8
  br label %for.cond113

cleanup162:                                       ; preds = %cleanup155, %for.cond.cleanup117
  %175 = bitcast i32* %e1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %175) #7
  %cleanup.dest163 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest163, label %cleanup165 [
    i32 11, label %for.end164
  ]

for.end164:                                       ; preds = %cleanup162
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup165

cleanup165:                                       ; preds = %for.end164, %cleanup162
  %176 = bitcast %class.btVector3* %WorldEdge0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %176) #7
  %177 = bitcast %class.btVector3* %edge0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %177) #7
  %cleanup.dest167 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest167, label %cleanup171 [
    i32 0, label %cleanup.cont168
  ]

cleanup.cont168:                                  ; preds = %cleanup165
  br label %for.inc169

for.inc169:                                       ; preds = %cleanup.cont168
  %178 = load i32, i32* %e0, align 4, !tbaa !8
  %inc170 = add nsw i32 %178, 1
  store i32 %inc170, i32* %e0, align 4, !tbaa !8
  br label %for.cond105

cleanup171:                                       ; preds = %cleanup165, %for.cond.cleanup108
  %179 = bitcast i32* %e0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %179) #7
  %cleanup.dest172 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest172, label %cleanup200 [
    i32 8, label %for.end173
  ]

for.end173:                                       ; preds = %cleanup171
  %180 = load i32, i32* %edgeA, align 4, !tbaa !8
  %cmp174 = icmp sge i32 %180, 0
  br i1 %cmp174, label %land.lhs.true175, label %if.end194

land.lhs.true175:                                 ; preds = %for.end173
  %181 = load i32, i32* %edgeB, align 4, !tbaa !8
  %cmp176 = icmp sge i32 %181, 0
  br i1 %cmp176, label %if.then177, label %if.end194

if.then177:                                       ; preds = %land.lhs.true175
  %182 = bitcast %class.btVector3* %ptsVector to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %182) #7
  %call178 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ptsVector)
  %183 = bitcast %class.btVector3* %offsetA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %183) #7
  %call179 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %offsetA)
  %184 = bitcast %class.btVector3* %offsetB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %184) #7
  %call180 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %offsetB)
  %185 = bitcast float* %tA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %185) #7
  %186 = bitcast float* %tB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %186) #7
  %187 = bitcast %class.btVector3* %translation to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %187) #7
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %translation, %class.btVector3* nonnull align 4 dereferenceable(16) %witnessPointB, %class.btVector3* nonnull align 4 dereferenceable(16) %witnessPointA)
  %188 = bitcast %class.btVector3* %dirA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %188) #7
  %189 = bitcast %class.btVector3* %dirA to i8*
  %190 = bitcast %class.btVector3* %worldEdgeA to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %189, i8* align 4 %190, i32 16, i1 false), !tbaa.struct !10
  %191 = bitcast %class.btVector3* %dirB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %191) #7
  %192 = bitcast %class.btVector3* %dirB to i8*
  %193 = bitcast %class.btVector3* %worldEdgeB to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %192, i8* align 4 %193, i32 16, i1 false), !tbaa.struct !10
  %194 = bitcast float* %hlenB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %194) #7
  store float 0x46293E5940000000, float* %hlenB, align 4, !tbaa !6
  %195 = bitcast float* %hlenA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %195) #7
  store float 0x46293E5940000000, float* %hlenA, align 4, !tbaa !6
  %196 = load float, float* %hlenA, align 4, !tbaa !6
  %197 = load float, float* %hlenB, align 4, !tbaa !6
  call void @_Z23btSegmentsClosestPointsR9btVector3S0_S0_RfS1_RKS_S3_fS3_f(%class.btVector3* nonnull align 4 dereferenceable(16) %ptsVector, %class.btVector3* nonnull align 4 dereferenceable(16) %offsetA, %class.btVector3* nonnull align 4 dereferenceable(16) %offsetB, float* nonnull align 4 dereferenceable(4) %tA, float* nonnull align 4 dereferenceable(4) %tB, %class.btVector3* nonnull align 4 dereferenceable(16) %translation, %class.btVector3* nonnull align 4 dereferenceable(16) %dirA, float %196, %class.btVector3* nonnull align 4 dereferenceable(16) %dirB, float %197)
  %198 = bitcast float* %nlSqrt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %198) #7
  %call181 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ptsVector)
  store float %call181, float* %nlSqrt, align 4, !tbaa !6
  %199 = load float, float* %nlSqrt, align 4, !tbaa !6
  %cmp182 = fcmp ogt float %199, 0x3E80000000000000
  br i1 %cmp182, label %if.then183, label %if.end193

if.then183:                                       ; preds = %if.then177
  %200 = bitcast float* %nl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %200) #7
  %201 = load float, float* %nlSqrt, align 4, !tbaa !6
  %call184 = call float @_Z6btSqrtf(float %201)
  store float %call184, float* %nl, align 4, !tbaa !6
  %202 = bitcast float* %ref.tmp185 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %202) #7
  %203 = load float, float* %nl, align 4, !tbaa !6
  %div = fdiv float 1.000000e+00, %203
  store float %div, float* %ref.tmp185, align 4, !tbaa !6
  %call186 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %ptsVector, float* nonnull align 4 dereferenceable(4) %ref.tmp185)
  %204 = bitcast float* %ref.tmp185 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %204) #7
  %call187 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ptsVector, %class.btVector3* nonnull align 4 dereferenceable(16) %DeltaC2)
  %cmp188 = fcmp olt float %call187, 0.000000e+00
  br i1 %cmp188, label %if.then189, label %if.end192

if.then189:                                       ; preds = %if.then183
  %205 = bitcast float* %ref.tmp190 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %205) #7
  store float -1.000000e+00, float* %ref.tmp190, align 4, !tbaa !6
  %call191 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %ptsVector, float* nonnull align 4 dereferenceable(4) %ref.tmp190)
  %206 = bitcast float* %ref.tmp190 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %206) #7
  br label %if.end192

if.end192:                                        ; preds = %if.then189, %if.then183
  %207 = bitcast %class.btVector3* %ptOnB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %207) #7
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ptOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %witnessPointB, %class.btVector3* nonnull align 4 dereferenceable(16) %offsetB)
  %208 = bitcast float* %distance to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %208) #7
  %209 = load float, float* %nl, align 4, !tbaa !6
  store float %209, float* %distance, align 4, !tbaa !6
  %210 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %resultOut.addr, align 4, !tbaa !2
  %211 = load float, float* %distance, align 4, !tbaa !6
  %fneg = fneg float %211
  %212 = bitcast %"struct.btDiscreteCollisionDetectorInterface::Result"* %210 to void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)***
  %vtable = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)**, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*** %212, align 4, !tbaa !24
  %vfn = getelementptr inbounds void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vtable, i64 4
  %213 = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vfn, align 4
  call void %213(%"struct.btDiscreteCollisionDetectorInterface::Result"* %210, %class.btVector3* nonnull align 4 dereferenceable(16) %ptsVector, %class.btVector3* nonnull align 4 dereferenceable(16) %ptOnB, float %fneg)
  %214 = bitcast float* %distance to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %214) #7
  %215 = bitcast %class.btVector3* %ptOnB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %215) #7
  %216 = bitcast float* %nl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %216) #7
  br label %if.end193

if.end193:                                        ; preds = %if.end192, %if.then177
  %217 = bitcast float* %nlSqrt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %217) #7
  %218 = bitcast float* %hlenA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %218) #7
  %219 = bitcast float* %hlenB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %219) #7
  %220 = bitcast %class.btVector3* %dirB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %220) #7
  %221 = bitcast %class.btVector3* %dirA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %221) #7
  %222 = bitcast %class.btVector3* %translation to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %222) #7
  %223 = bitcast float* %tB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %223) #7
  %224 = bitcast float* %tA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %224) #7
  %225 = bitcast %class.btVector3* %offsetB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %225) #7
  %226 = bitcast %class.btVector3* %offsetA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %226) #7
  %227 = bitcast %class.btVector3* %ptsVector to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %227) #7
  br label %if.end194

if.end194:                                        ; preds = %if.end193, %land.lhs.true175, %for.end173
  %228 = load %class.btVector3*, %class.btVector3** %sep.addr, align 4, !tbaa !2
  %call195 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %DeltaC2, %class.btVector3* nonnull align 4 dereferenceable(16) %228)
  %cmp196 = fcmp olt float %call195, 0.000000e+00
  br i1 %cmp196, label %if.then197, label %if.end199

if.then197:                                       ; preds = %if.end194
  %229 = bitcast %class.btVector3* %ref.tmp198 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %229) #7
  %230 = load %class.btVector3*, %class.btVector3** %sep.addr, align 4, !tbaa !2
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp198, %class.btVector3* nonnull align 4 dereferenceable(16) %230)
  %231 = load %class.btVector3*, %class.btVector3** %sep.addr, align 4, !tbaa !2
  %232 = bitcast %class.btVector3* %231 to i8*
  %233 = bitcast %class.btVector3* %ref.tmp198 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %232, i8* align 4 %233, i32 16, i1 false), !tbaa.struct !10
  %234 = bitcast %class.btVector3* %ref.tmp198 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %234) #7
  br label %if.end199

if.end199:                                        ; preds = %if.then197, %if.end194
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup200

cleanup200:                                       ; preds = %if.end199, %cleanup171
  %235 = bitcast i32* %curEdgeEdge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %235) #7
  %236 = bitcast %class.btVector3* %witnessPointB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %236) #7
  %237 = bitcast %class.btVector3* %witnessPointA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %237) #7
  %238 = bitcast %class.btVector3* %worldEdgeB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %238) #7
  %239 = bitcast %class.btVector3* %worldEdgeA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %239) #7
  %240 = bitcast i32* %edgeB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %240) #7
  %241 = bitcast i32* %edgeA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %241) #7
  %242 = bitcast %class.btVector3* %edgeBend to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %242) #7
  %243 = bitcast %class.btVector3* %edgeBstart to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %243) #7
  %244 = bitcast %class.btVector3* %edgeAend to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %244) #7
  %245 = bitcast %class.btVector3* %edgeAstart to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %245) #7
  br label %cleanup211

cleanup211:                                       ; preds = %cleanup200, %cleanup94
  %246 = bitcast i32* %numFacesB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %246) #7
  br label %cleanup212

cleanup212:                                       ; preds = %cleanup211, %cleanup36
  %247 = bitcast i32* %numFacesA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %247) #7
  %248 = bitcast i32* %curPlaneTests to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %248) #7
  %249 = bitcast float* %dmin to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %249) #7
  %250 = bitcast %class.btVector3* %DeltaC2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %250) #7
  %251 = bitcast %class.btVector3* %c1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %251) #7
  %252 = bitcast %class.btVector3* %c0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %252) #7
  %253 = load i1, i1* %retval, align 1
  ret i1 %253
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #3 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !6
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !6
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !6
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !6
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !6
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !6
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #7
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #7
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !26
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btFace*, %struct.btFace** %m_data, align 4, !tbaa !27
  %1 = load i32, i32* %n.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %struct.btFace, %struct.btFace* %0, i32 %1
  ret %struct.btFace* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !6
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !6
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !6
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !6
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !6
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !6
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !6
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %1, i32 0)
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call1, float* %ref.tmp, align 4, !tbaa !6
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 1)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !6
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 2)
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !6
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #7
  %10 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #7
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !6
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !6
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !6
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !6
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !6
  %6 = load float*, float** %s.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !6
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !6
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !6
  ret %class.btVector3* %this1
}

define internal zeroext i1 @_ZL11TestSepAxisRK18btConvexPolyhedronS1_RK11btTransformS4_RK9btVector3RfRS5_S9_(%class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %hullA, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %hullB, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB, %class.btVector3* nonnull align 4 dereferenceable(16) %sep_axis, float* nonnull align 4 dereferenceable(4) %depth, %class.btVector3* nonnull align 4 dereferenceable(16) %witnessPointA, %class.btVector3* nonnull align 4 dereferenceable(16) %witnessPointB) #0 {
entry:
  %retval = alloca i1, align 1
  %hullA.addr = alloca %class.btConvexPolyhedron*, align 4
  %hullB.addr = alloca %class.btConvexPolyhedron*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %sep_axis.addr = alloca %class.btVector3*, align 4
  %depth.addr = alloca float*, align 4
  %witnessPointA.addr = alloca %class.btVector3*, align 4
  %witnessPointB.addr = alloca %class.btVector3*, align 4
  %Min0 = alloca float, align 4
  %Max0 = alloca float, align 4
  %Min1 = alloca float, align 4
  %Max1 = alloca float, align 4
  %witnesPtMinA = alloca %class.btVector3, align 4
  %witnesPtMaxA = alloca %class.btVector3, align 4
  %witnesPtMinB = alloca %class.btVector3, align 4
  %witnesPtMaxB = alloca %class.btVector3, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %d0 = alloca float, align 4
  %d1 = alloca float, align 4
  store %class.btConvexPolyhedron* %hullA, %class.btConvexPolyhedron** %hullA.addr, align 4, !tbaa !2
  store %class.btConvexPolyhedron* %hullB, %class.btConvexPolyhedron** %hullB.addr, align 4, !tbaa !2
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4, !tbaa !2
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4, !tbaa !2
  store %class.btVector3* %sep_axis, %class.btVector3** %sep_axis.addr, align 4, !tbaa !2
  store float* %depth, float** %depth.addr, align 4, !tbaa !2
  store %class.btVector3* %witnessPointA, %class.btVector3** %witnessPointA.addr, align 4, !tbaa !2
  store %class.btVector3* %witnessPointB, %class.btVector3** %witnessPointB.addr, align 4, !tbaa !2
  %0 = bitcast float* %Min0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = bitcast float* %Max0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = bitcast float* %Min1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = bitcast float* %Max1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = bitcast %class.btVector3* %witnesPtMinA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #7
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %witnesPtMinA)
  %5 = bitcast %class.btVector3* %witnesPtMaxA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #7
  %call1 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %witnesPtMaxA)
  %6 = bitcast %class.btVector3* %witnesPtMinB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #7
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %witnesPtMinB)
  %7 = bitcast %class.btVector3* %witnesPtMaxB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #7
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %witnesPtMaxB)
  %8 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4, !tbaa !2
  %9 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %10 = load %class.btVector3*, %class.btVector3** %sep_axis.addr, align 4, !tbaa !2
  call void @_ZNK18btConvexPolyhedron7projectERK11btTransformRK9btVector3RfS6_RS3_S7_(%class.btConvexPolyhedron* %8, %class.btTransform* nonnull align 4 dereferenceable(64) %9, %class.btVector3* nonnull align 4 dereferenceable(16) %10, float* nonnull align 4 dereferenceable(4) %Min0, float* nonnull align 4 dereferenceable(4) %Max0, %class.btVector3* nonnull align 4 dereferenceable(16) %witnesPtMinA, %class.btVector3* nonnull align 4 dereferenceable(16) %witnesPtMaxA)
  %11 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4, !tbaa !2
  %12 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %13 = load %class.btVector3*, %class.btVector3** %sep_axis.addr, align 4, !tbaa !2
  call void @_ZNK18btConvexPolyhedron7projectERK11btTransformRK9btVector3RfS6_RS3_S7_(%class.btConvexPolyhedron* %11, %class.btTransform* nonnull align 4 dereferenceable(64) %12, %class.btVector3* nonnull align 4 dereferenceable(16) %13, float* nonnull align 4 dereferenceable(4) %Min1, float* nonnull align 4 dereferenceable(4) %Max1, %class.btVector3* nonnull align 4 dereferenceable(16) %witnesPtMinB, %class.btVector3* nonnull align 4 dereferenceable(16) %witnesPtMaxB)
  %14 = load float, float* %Max0, align 4, !tbaa !6
  %15 = load float, float* %Min1, align 4, !tbaa !6
  %cmp = fcmp olt float %14, %15
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %16 = load float, float* %Max1, align 4, !tbaa !6
  %17 = load float, float* %Min0, align 4, !tbaa !6
  %cmp4 = fcmp olt float %16, %17
  br i1 %cmp4, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %lor.lhs.false
  %18 = bitcast float* %d0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #7
  %19 = load float, float* %Max0, align 4, !tbaa !6
  %20 = load float, float* %Min1, align 4, !tbaa !6
  %sub = fsub float %19, %20
  store float %sub, float* %d0, align 4, !tbaa !6
  %21 = bitcast float* %d1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #7
  %22 = load float, float* %Max1, align 4, !tbaa !6
  %23 = load float, float* %Min0, align 4, !tbaa !6
  %sub5 = fsub float %22, %23
  store float %sub5, float* %d1, align 4, !tbaa !6
  %24 = load float, float* %d0, align 4, !tbaa !6
  %25 = load float, float* %d1, align 4, !tbaa !6
  %cmp6 = fcmp olt float %24, %25
  br i1 %cmp6, label %if.then7, label %if.else

if.then7:                                         ; preds = %if.end
  %26 = load float, float* %d0, align 4, !tbaa !6
  %27 = load float*, float** %depth.addr, align 4, !tbaa !2
  store float %26, float* %27, align 4, !tbaa !6
  %28 = load %class.btVector3*, %class.btVector3** %witnessPointA.addr, align 4, !tbaa !2
  %29 = bitcast %class.btVector3* %28 to i8*
  %30 = bitcast %class.btVector3* %witnesPtMaxA to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %29, i8* align 4 %30, i32 16, i1 false), !tbaa.struct !10
  %31 = load %class.btVector3*, %class.btVector3** %witnessPointB.addr, align 4, !tbaa !2
  %32 = bitcast %class.btVector3* %31 to i8*
  %33 = bitcast %class.btVector3* %witnesPtMinB to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %32, i8* align 4 %33, i32 16, i1 false), !tbaa.struct !10
  br label %if.end8

if.else:                                          ; preds = %if.end
  %34 = load float, float* %d1, align 4, !tbaa !6
  %35 = load float*, float** %depth.addr, align 4, !tbaa !2
  store float %34, float* %35, align 4, !tbaa !6
  %36 = load %class.btVector3*, %class.btVector3** %witnessPointA.addr, align 4, !tbaa !2
  %37 = bitcast %class.btVector3* %36 to i8*
  %38 = bitcast %class.btVector3* %witnesPtMinA to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %37, i8* align 4 %38, i32 16, i1 false), !tbaa.struct !10
  %39 = load %class.btVector3*, %class.btVector3** %witnessPointB.addr, align 4, !tbaa !2
  %40 = bitcast %class.btVector3* %39 to i8*
  %41 = bitcast %class.btVector3* %witnesPtMaxB to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %40, i8* align 4 %41, i32 16, i1 false), !tbaa.struct !10
  br label %if.end8

if.end8:                                          ; preds = %if.else, %if.then7
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  %42 = bitcast float* %d1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #7
  %43 = bitcast float* %d0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #7
  br label %cleanup

cleanup:                                          ; preds = %if.end8, %if.then
  %44 = bitcast %class.btVector3* %witnesPtMaxB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %44) #7
  %45 = bitcast %class.btVector3* %witnesPtMinB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %45) #7
  %46 = bitcast %class.btVector3* %witnesPtMaxA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %46) #7
  %47 = bitcast %class.btVector3* %witnesPtMinA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %47) #7
  %48 = bitcast float* %Max1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #7
  %49 = bitcast float* %Min1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #7
  %50 = bitcast float* %Max0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #7
  %51 = bitcast float* %Min0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #7
  %52 = load i1, i1* %retval, align 1
  ret i1 %52
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %1 = load float, float* %arrayidx, align 4, !tbaa !6
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %3 = load float, float* %arrayidx3, align 4, !tbaa !6
  %mul = fmul float %1, %3
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %4 = load float, float* %arrayidx5, align 4, !tbaa !6
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %6 = load float, float* %arrayidx7, align 4, !tbaa !6
  %mul8 = fmul float %4, %6
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4, !tbaa !6
  %7 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %8 = load float, float* %arrayidx11, align 4, !tbaa !6
  %9 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %10 = load float, float* %arrayidx13, align 4, !tbaa !6
  %mul14 = fmul float %8, %10
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %11 = load float, float* %arrayidx16, align 4, !tbaa !6
  %12 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %13 = load float, float* %arrayidx18, align 4, !tbaa !6
  %mul19 = fmul float %11, %13
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4, !tbaa !6
  %14 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #7
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %15 = load float, float* %arrayidx23, align 4, !tbaa !6
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %17 = load float, float* %arrayidx25, align 4, !tbaa !6
  %mul26 = fmul float %15, %17
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %18 = load float, float* %arrayidx28, align 4, !tbaa !6
  %19 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %19, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %20 = load float, float* %arrayidx30, align 4, !tbaa !6
  %mul31 = fmul float %18, %20
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %21 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #7
  %22 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #7
  %23 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_Z12IsAlmostZeroRK9btVector3(%class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %retval = alloca i1, align 1
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %0)
  %1 = load float, float* %call, align 4, !tbaa !6
  %2 = call float @llvm.fabs.f32(float %1)
  %conv = fpext float %2 to double
  %cmp = fcmp ogt double %conv, 0x3EB0C6F7A0B5ED8D
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %3)
  %4 = load float, float* %call1, align 4, !tbaa !6
  %5 = call float @llvm.fabs.f32(float %4)
  %conv2 = fpext float %5 to double
  %cmp3 = fcmp ogt double %conv2, 0x3EB0C6F7A0B5ED8D
  br i1 %cmp3, label %if.then, label %lor.lhs.false4

lor.lhs.false4:                                   ; preds = %lor.lhs.false
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %6)
  %7 = load float, float* %call5, align 4, !tbaa !6
  %8 = call float @llvm.fabs.f32(float %7)
  %conv6 = fpext float %8 to double
  %cmp7 = fcmp ogt double %conv6, 0x3EB0C6F7A0B5ED8D
  br i1 %cmp7, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false4, %lor.lhs.false, %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %lor.lhs.false4
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %9 = load i1, i1* %retval, align 1
  ret i1 %9
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4, !tbaa !6
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1) #7
  ret %class.btVector3* %call2
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_Z23btSegmentsClosestPointsR9btVector3S0_S0_RfS1_RKS_S3_fS3_f(%class.btVector3* nonnull align 4 dereferenceable(16) %ptsVector, %class.btVector3* nonnull align 4 dereferenceable(16) %offsetA, %class.btVector3* nonnull align 4 dereferenceable(16) %offsetB, float* nonnull align 4 dereferenceable(4) %tA, float* nonnull align 4 dereferenceable(4) %tB, %class.btVector3* nonnull align 4 dereferenceable(16) %translation, %class.btVector3* nonnull align 4 dereferenceable(16) %dirA, float %hlenA, %class.btVector3* nonnull align 4 dereferenceable(16) %dirB, float %hlenB) #3 comdat {
entry:
  %ptsVector.addr = alloca %class.btVector3*, align 4
  %offsetA.addr = alloca %class.btVector3*, align 4
  %offsetB.addr = alloca %class.btVector3*, align 4
  %tA.addr = alloca float*, align 4
  %tB.addr = alloca float*, align 4
  %translation.addr = alloca %class.btVector3*, align 4
  %dirA.addr = alloca %class.btVector3*, align 4
  %hlenA.addr = alloca float, align 4
  %dirB.addr = alloca %class.btVector3*, align 4
  %hlenB.addr = alloca float, align 4
  %dirA_dot_dirB = alloca float, align 4
  %dirA_dot_trans = alloca float, align 4
  %dirB_dot_trans = alloca float, align 4
  %denom = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp45 = alloca %class.btVector3, align 4
  %ref.tmp46 = alloca %class.btVector3, align 4
  %ref.tmp47 = alloca %class.btVector3, align 4
  store %class.btVector3* %ptsVector, %class.btVector3** %ptsVector.addr, align 4, !tbaa !2
  store %class.btVector3* %offsetA, %class.btVector3** %offsetA.addr, align 4, !tbaa !2
  store %class.btVector3* %offsetB, %class.btVector3** %offsetB.addr, align 4, !tbaa !2
  store float* %tA, float** %tA.addr, align 4, !tbaa !2
  store float* %tB, float** %tB.addr, align 4, !tbaa !2
  store %class.btVector3* %translation, %class.btVector3** %translation.addr, align 4, !tbaa !2
  store %class.btVector3* %dirA, %class.btVector3** %dirA.addr, align 4, !tbaa !2
  store float %hlenA, float* %hlenA.addr, align 4, !tbaa !6
  store %class.btVector3* %dirB, %class.btVector3** %dirB.addr, align 4, !tbaa !2
  store float %hlenB, float* %hlenB.addr, align 4, !tbaa !6
  %0 = bitcast float* %dirA_dot_dirB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btVector3*, %class.btVector3** %dirA.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %dirB.addr, align 4, !tbaa !2
  %call = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call, float* %dirA_dot_dirB, align 4, !tbaa !6
  %3 = bitcast float* %dirA_dot_trans to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load %class.btVector3*, %class.btVector3** %dirA.addr, align 4, !tbaa !2
  %5 = load %class.btVector3*, %class.btVector3** %translation.addr, align 4, !tbaa !2
  %call1 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call1, float* %dirA_dot_trans, align 4, !tbaa !6
  %6 = bitcast float* %dirB_dot_trans to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = load %class.btVector3*, %class.btVector3** %dirB.addr, align 4, !tbaa !2
  %8 = load %class.btVector3*, %class.btVector3** %translation.addr, align 4, !tbaa !2
  %call2 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %7, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call2, float* %dirB_dot_trans, align 4, !tbaa !6
  %9 = bitcast float* %denom to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %10 = load float, float* %dirA_dot_dirB, align 4, !tbaa !6
  %11 = load float, float* %dirA_dot_dirB, align 4, !tbaa !6
  %mul = fmul float %10, %11
  %sub = fsub float 1.000000e+00, %mul
  store float %sub, float* %denom, align 4, !tbaa !6
  %12 = load float, float* %denom, align 4, !tbaa !6
  %cmp = fcmp oeq float %12, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %13 = load float*, float** %tA.addr, align 4, !tbaa !2
  store float 0.000000e+00, float* %13, align 4, !tbaa !6
  br label %if.end12

if.else:                                          ; preds = %entry
  %14 = load float, float* %dirA_dot_trans, align 4, !tbaa !6
  %15 = load float, float* %dirB_dot_trans, align 4, !tbaa !6
  %16 = load float, float* %dirA_dot_dirB, align 4, !tbaa !6
  %mul3 = fmul float %15, %16
  %sub4 = fsub float %14, %mul3
  %17 = load float, float* %denom, align 4, !tbaa !6
  %div = fdiv float %sub4, %17
  %18 = load float*, float** %tA.addr, align 4, !tbaa !2
  store float %div, float* %18, align 4, !tbaa !6
  %19 = load float*, float** %tA.addr, align 4, !tbaa !2
  %20 = load float, float* %19, align 4, !tbaa !6
  %21 = load float, float* %hlenA.addr, align 4, !tbaa !6
  %fneg = fneg float %21
  %cmp5 = fcmp olt float %20, %fneg
  br i1 %cmp5, label %if.then6, label %if.else8

if.then6:                                         ; preds = %if.else
  %22 = load float, float* %hlenA.addr, align 4, !tbaa !6
  %fneg7 = fneg float %22
  %23 = load float*, float** %tA.addr, align 4, !tbaa !2
  store float %fneg7, float* %23, align 4, !tbaa !6
  br label %if.end11

if.else8:                                         ; preds = %if.else
  %24 = load float*, float** %tA.addr, align 4, !tbaa !2
  %25 = load float, float* %24, align 4, !tbaa !6
  %26 = load float, float* %hlenA.addr, align 4, !tbaa !6
  %cmp9 = fcmp ogt float %25, %26
  br i1 %cmp9, label %if.then10, label %if.end

if.then10:                                        ; preds = %if.else8
  %27 = load float, float* %hlenA.addr, align 4, !tbaa !6
  %28 = load float*, float** %tA.addr, align 4, !tbaa !2
  store float %27, float* %28, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then10, %if.else8
  br label %if.end11

if.end11:                                         ; preds = %if.end, %if.then6
  br label %if.end12

if.end12:                                         ; preds = %if.end11, %if.then
  %29 = load float*, float** %tA.addr, align 4, !tbaa !2
  %30 = load float, float* %29, align 4, !tbaa !6
  %31 = load float, float* %dirA_dot_dirB, align 4, !tbaa !6
  %mul13 = fmul float %30, %31
  %32 = load float, float* %dirB_dot_trans, align 4, !tbaa !6
  %sub14 = fsub float %mul13, %32
  %33 = load float*, float** %tB.addr, align 4, !tbaa !2
  store float %sub14, float* %33, align 4, !tbaa !6
  %34 = load float*, float** %tB.addr, align 4, !tbaa !2
  %35 = load float, float* %34, align 4, !tbaa !6
  %36 = load float, float* %hlenB.addr, align 4, !tbaa !6
  %fneg15 = fneg float %36
  %cmp16 = fcmp olt float %35, %fneg15
  br i1 %cmp16, label %if.then17, label %if.else29

if.then17:                                        ; preds = %if.end12
  %37 = load float, float* %hlenB.addr, align 4, !tbaa !6
  %fneg18 = fneg float %37
  %38 = load float*, float** %tB.addr, align 4, !tbaa !2
  store float %fneg18, float* %38, align 4, !tbaa !6
  %39 = load float*, float** %tB.addr, align 4, !tbaa !2
  %40 = load float, float* %39, align 4, !tbaa !6
  %41 = load float, float* %dirA_dot_dirB, align 4, !tbaa !6
  %mul19 = fmul float %40, %41
  %42 = load float, float* %dirA_dot_trans, align 4, !tbaa !6
  %add = fadd float %mul19, %42
  %43 = load float*, float** %tA.addr, align 4, !tbaa !2
  store float %add, float* %43, align 4, !tbaa !6
  %44 = load float*, float** %tA.addr, align 4, !tbaa !2
  %45 = load float, float* %44, align 4, !tbaa !6
  %46 = load float, float* %hlenA.addr, align 4, !tbaa !6
  %fneg20 = fneg float %46
  %cmp21 = fcmp olt float %45, %fneg20
  br i1 %cmp21, label %if.then22, label %if.else24

if.then22:                                        ; preds = %if.then17
  %47 = load float, float* %hlenA.addr, align 4, !tbaa !6
  %fneg23 = fneg float %47
  %48 = load float*, float** %tA.addr, align 4, !tbaa !2
  store float %fneg23, float* %48, align 4, !tbaa !6
  br label %if.end28

if.else24:                                        ; preds = %if.then17
  %49 = load float*, float** %tA.addr, align 4, !tbaa !2
  %50 = load float, float* %49, align 4, !tbaa !6
  %51 = load float, float* %hlenA.addr, align 4, !tbaa !6
  %cmp25 = fcmp ogt float %50, %51
  br i1 %cmp25, label %if.then26, label %if.end27

if.then26:                                        ; preds = %if.else24
  %52 = load float, float* %hlenA.addr, align 4, !tbaa !6
  %53 = load float*, float** %tA.addr, align 4, !tbaa !2
  store float %52, float* %53, align 4, !tbaa !6
  br label %if.end27

if.end27:                                         ; preds = %if.then26, %if.else24
  br label %if.end28

if.end28:                                         ; preds = %if.end27, %if.then22
  br label %if.end44

if.else29:                                        ; preds = %if.end12
  %54 = load float*, float** %tB.addr, align 4, !tbaa !2
  %55 = load float, float* %54, align 4, !tbaa !6
  %56 = load float, float* %hlenB.addr, align 4, !tbaa !6
  %cmp30 = fcmp ogt float %55, %56
  br i1 %cmp30, label %if.then31, label %if.end43

if.then31:                                        ; preds = %if.else29
  %57 = load float, float* %hlenB.addr, align 4, !tbaa !6
  %58 = load float*, float** %tB.addr, align 4, !tbaa !2
  store float %57, float* %58, align 4, !tbaa !6
  %59 = load float*, float** %tB.addr, align 4, !tbaa !2
  %60 = load float, float* %59, align 4, !tbaa !6
  %61 = load float, float* %dirA_dot_dirB, align 4, !tbaa !6
  %mul32 = fmul float %60, %61
  %62 = load float, float* %dirA_dot_trans, align 4, !tbaa !6
  %add33 = fadd float %mul32, %62
  %63 = load float*, float** %tA.addr, align 4, !tbaa !2
  store float %add33, float* %63, align 4, !tbaa !6
  %64 = load float*, float** %tA.addr, align 4, !tbaa !2
  %65 = load float, float* %64, align 4, !tbaa !6
  %66 = load float, float* %hlenA.addr, align 4, !tbaa !6
  %fneg34 = fneg float %66
  %cmp35 = fcmp olt float %65, %fneg34
  br i1 %cmp35, label %if.then36, label %if.else38

if.then36:                                        ; preds = %if.then31
  %67 = load float, float* %hlenA.addr, align 4, !tbaa !6
  %fneg37 = fneg float %67
  %68 = load float*, float** %tA.addr, align 4, !tbaa !2
  store float %fneg37, float* %68, align 4, !tbaa !6
  br label %if.end42

if.else38:                                        ; preds = %if.then31
  %69 = load float*, float** %tA.addr, align 4, !tbaa !2
  %70 = load float, float* %69, align 4, !tbaa !6
  %71 = load float, float* %hlenA.addr, align 4, !tbaa !6
  %cmp39 = fcmp ogt float %70, %71
  br i1 %cmp39, label %if.then40, label %if.end41

if.then40:                                        ; preds = %if.else38
  %72 = load float, float* %hlenA.addr, align 4, !tbaa !6
  %73 = load float*, float** %tA.addr, align 4, !tbaa !2
  store float %72, float* %73, align 4, !tbaa !6
  br label %if.end41

if.end41:                                         ; preds = %if.then40, %if.else38
  br label %if.end42

if.end42:                                         ; preds = %if.end41, %if.then36
  br label %if.end43

if.end43:                                         ; preds = %if.end42, %if.else29
  br label %if.end44

if.end44:                                         ; preds = %if.end43, %if.end28
  %74 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %74) #7
  %75 = load %class.btVector3*, %class.btVector3** %dirA.addr, align 4, !tbaa !2
  %76 = load float*, float** %tA.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %75, float* nonnull align 4 dereferenceable(4) %76)
  %77 = load %class.btVector3*, %class.btVector3** %offsetA.addr, align 4, !tbaa !2
  %78 = bitcast %class.btVector3* %77 to i8*
  %79 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %78, i8* align 4 %79, i32 16, i1 false), !tbaa.struct !10
  %80 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %80) #7
  %81 = bitcast %class.btVector3* %ref.tmp45 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %81) #7
  %82 = load %class.btVector3*, %class.btVector3** %dirB.addr, align 4, !tbaa !2
  %83 = load float*, float** %tB.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp45, %class.btVector3* nonnull align 4 dereferenceable(16) %82, float* nonnull align 4 dereferenceable(4) %83)
  %84 = load %class.btVector3*, %class.btVector3** %offsetB.addr, align 4, !tbaa !2
  %85 = bitcast %class.btVector3* %84 to i8*
  %86 = bitcast %class.btVector3* %ref.tmp45 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %85, i8* align 4 %86, i32 16, i1 false), !tbaa.struct !10
  %87 = bitcast %class.btVector3* %ref.tmp45 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %87) #7
  %88 = bitcast %class.btVector3* %ref.tmp46 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %88) #7
  %89 = bitcast %class.btVector3* %ref.tmp47 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %89) #7
  %90 = load %class.btVector3*, %class.btVector3** %translation.addr, align 4, !tbaa !2
  %91 = load %class.btVector3*, %class.btVector3** %offsetA.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp47, %class.btVector3* nonnull align 4 dereferenceable(16) %90, %class.btVector3* nonnull align 4 dereferenceable(16) %91)
  %92 = load %class.btVector3*, %class.btVector3** %offsetB.addr, align 4, !tbaa !2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp46, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp47, %class.btVector3* nonnull align 4 dereferenceable(16) %92)
  %93 = load %class.btVector3*, %class.btVector3** %ptsVector.addr, align 4, !tbaa !2
  %94 = bitcast %class.btVector3* %93 to i8*
  %95 = bitcast %class.btVector3* %ref.tmp46 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %94, i8* align 4 %95, i32 16, i1 false), !tbaa.struct !10
  %96 = bitcast %class.btVector3* %ref.tmp47 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %96) #7
  %97 = bitcast %class.btVector3* %ref.tmp46 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %97) #7
  %98 = bitcast float* %denom to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #7
  %99 = bitcast float* %dirB_dot_trans to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #7
  %100 = bitcast float* %dirA_dot_trans to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #7
  %101 = bitcast float* %dirA_dot_dirB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #2 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !6
  %0 = load float, float* %y.addr, align 4, !tbaa !6
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #3 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !6
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !6
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !6
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !6
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !6
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !6
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #7
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #7
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp, align 4, !tbaa !6
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !6
  %fneg4 = fneg float %5
  store float %fneg4, float* %ref.tmp1, align 4, !tbaa !6
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %8 = load float, float* %arrayidx7, align 4, !tbaa !6
  %fneg8 = fneg float %8
  store float %fneg8, float* %ref.tmp5, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #7
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #7
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #7
  ret void
}

define hidden void @_ZN27btPolyhedralContactClipping19clipFaceAgainstHullERK9btVector3RK18btConvexPolyhedronRK11btTransformR20btAlignedObjectArrayIS0_EffRN36btDiscreteCollisionDetectorInterface6ResultE(%class.btVector3* nonnull align 4 dereferenceable(16) %separatingNormal, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %hullA, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %worldVertsB1, float %minDist, float %maxDist, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %resultOut) #0 {
entry:
  %separatingNormal.addr = alloca %class.btVector3*, align 4
  %hullA.addr = alloca %class.btConvexPolyhedron*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %worldVertsB1.addr = alloca %class.btAlignedObjectArray*, align 4
  %minDist.addr = alloca float, align 4
  %maxDist.addr = alloca float, align 4
  %resultOut.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  %worldVertsB2 = alloca %class.btAlignedObjectArray, align 4
  %pVtxIn = alloca %class.btAlignedObjectArray*, align 4
  %pVtxOut = alloca %class.btAlignedObjectArray*, align 4
  %closestFaceA = alloca i32, align 4
  %dmin = alloca float, align 4
  %face = alloca i32, align 4
  %Normal = alloca %class.btVector3, align 4
  %faceANormalWS = alloca %class.btVector3, align 4
  %d = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %polyA = alloca %struct.btFace*, align 4
  %numVerticesA = alloca i32, align 4
  %e0 = alloca i32, align 4
  %a = alloca %class.btVector3*, align 4
  %b = alloca %class.btVector3*, align 4
  %edge0 = alloca %class.btVector3, align 4
  %WorldEdge0 = alloca %class.btVector3, align 4
  %worldPlaneAnormal1 = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %planeNormalWS1 = alloca %class.btVector3, align 4
  %ref.tmp43 = alloca %class.btVector3, align 4
  %worldA1 = alloca %class.btVector3, align 4
  %planeEqWS1 = alloca float, align 4
  %planeNormalWS = alloca %class.btVector3, align 4
  %planeEqWS = alloca float, align 4
  %ref.tmp45 = alloca %class.btVector3, align 4
  %point = alloca %class.btVector3, align 4
  %localPlaneNormal = alloca %class.btVector3, align 4
  %localPlaneEq = alloca float, align 4
  %planeNormalWS60 = alloca %class.btVector3, align 4
  %planeEqWS62 = alloca float, align 4
  %i = alloca i32, align 4
  %vtx = alloca %class.btVector3, align 4
  %depth = alloca float, align 4
  %point78 = alloca %class.btVector3, align 4
  store %class.btVector3* %separatingNormal, %class.btVector3** %separatingNormal.addr, align 4, !tbaa !2
  store %class.btConvexPolyhedron* %hullA, %class.btConvexPolyhedron** %hullA.addr, align 4, !tbaa !2
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray* %worldVertsB1, %class.btAlignedObjectArray** %worldVertsB1.addr, align 4, !tbaa !2
  store float %minDist, float* %minDist.addr, align 4, !tbaa !6
  store float %maxDist, float* %maxDist.addr, align 4, !tbaa !6
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %resultOut, %"struct.btDiscreteCollisionDetectorInterface::Result"** %resultOut.addr, align 4, !tbaa !2
  %0 = bitcast %class.btAlignedObjectArray* %worldVertsB2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %0) #7
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* %worldVertsB2)
  %1 = bitcast %class.btAlignedObjectArray** %pVtxIn to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %worldVertsB1.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray* %2, %class.btAlignedObjectArray** %pVtxIn, align 4, !tbaa !2
  %3 = bitcast %class.btAlignedObjectArray** %pVtxOut to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  store %class.btAlignedObjectArray* %worldVertsB2, %class.btAlignedObjectArray** %pVtxOut, align 4, !tbaa !2
  %4 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pVtxOut, align 4, !tbaa !2
  %5 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pVtxIn, align 4, !tbaa !2
  %call1 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %5)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %4, i32 %call1)
  %6 = bitcast i32* %closestFaceA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  store i32 -1, i32* %closestFaceA, align 4, !tbaa !8
  %7 = bitcast float* %dmin to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  store float 0x47EFFFFFE0000000, float* %dmin, align 4, !tbaa !6
  %8 = bitcast i32* %face to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  store i32 0, i32* %face, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %9 = load i32, i32* %face, align 4, !tbaa !8
  %10 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4, !tbaa !2
  %m_faces = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %10, i32 0, i32 2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %m_faces)
  %cmp = icmp slt i32 %9, %call2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %11 = bitcast i32* %face to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %12 = bitcast %class.btVector3* %Normal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %12) #7
  %13 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4, !tbaa !2
  %m_faces3 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %13, i32 0, i32 2
  %14 = load i32, i32* %face, align 4, !tbaa !8
  %call4 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces3, i32 %14)
  %m_plane = getelementptr inbounds %struct.btFace, %struct.btFace* %call4, i32 0, i32 1
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_plane, i32 0, i32 0
  %15 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4, !tbaa !2
  %m_faces5 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %15, i32 0, i32 2
  %16 = load i32, i32* %face, align 4, !tbaa !8
  %call6 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces5, i32 %16)
  %m_plane7 = getelementptr inbounds %struct.btFace, %struct.btFace* %call6, i32 0, i32 1
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_plane7, i32 0, i32 1
  %17 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4, !tbaa !2
  %m_faces9 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %17, i32 0, i32 2
  %18 = load i32, i32* %face, align 4, !tbaa !8
  %call10 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces9, i32 %18)
  %m_plane11 = getelementptr inbounds %struct.btFace, %struct.btFace* %call10, i32 0, i32 1
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_plane11, i32 0, i32 2
  %call13 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %Normal, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx8, float* nonnull align 4 dereferenceable(4) %arrayidx12)
  %19 = bitcast %class.btVector3* %faceANormalWS to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %19) #7
  %20 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %call14 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %20)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %faceANormalWS, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call14, %class.btVector3* nonnull align 4 dereferenceable(16) %Normal)
  %21 = bitcast float* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #7
  %22 = load %class.btVector3*, %class.btVector3** %separatingNormal.addr, align 4, !tbaa !2
  %call15 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %faceANormalWS, %class.btVector3* nonnull align 4 dereferenceable(16) %22)
  store float %call15, float* %d, align 4, !tbaa !6
  %23 = load float, float* %d, align 4, !tbaa !6
  %24 = load float, float* %dmin, align 4, !tbaa !6
  %cmp16 = fcmp olt float %23, %24
  br i1 %cmp16, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %25 = load float, float* %d, align 4, !tbaa !6
  store float %25, float* %dmin, align 4, !tbaa !6
  %26 = load i32, i32* %face, align 4, !tbaa !8
  store i32 %26, i32* %closestFaceA, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %27 = bitcast float* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #7
  %28 = bitcast %class.btVector3* %faceANormalWS to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %28) #7
  %29 = bitcast %class.btVector3* %Normal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #7
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %30 = load i32, i32* %face, align 4, !tbaa !8
  %inc = add nsw i32 %30, 1
  store i32 %inc, i32* %face, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %31 = bitcast float* %dmin to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #7
  %32 = load i32, i32* %closestFaceA, align 4, !tbaa !8
  %cmp17 = icmp slt i32 %32, 0
  br i1 %cmp17, label %if.then18, label %if.end19

if.then18:                                        ; preds = %for.end
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end19:                                         ; preds = %for.end
  %33 = bitcast %struct.btFace** %polyA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #7
  %34 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4, !tbaa !2
  %m_faces20 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %34, i32 0, i32 2
  %35 = load i32, i32* %closestFaceA, align 4, !tbaa !8
  %call21 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces20, i32 %35)
  store %struct.btFace* %call21, %struct.btFace** %polyA, align 4, !tbaa !2
  %36 = bitcast i32* %numVerticesA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #7
  %37 = load %struct.btFace*, %struct.btFace** %polyA, align 4, !tbaa !2
  %m_indices = getelementptr inbounds %struct.btFace, %struct.btFace* %37, i32 0, i32 0
  %call22 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %m_indices)
  store i32 %call22, i32* %numVerticesA, align 4, !tbaa !8
  %38 = bitcast i32* %e0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #7
  store i32 0, i32* %e0, align 4, !tbaa !8
  br label %for.cond23

for.cond23:                                       ; preds = %for.inc47, %if.end19
  %39 = load i32, i32* %e0, align 4, !tbaa !8
  %40 = load i32, i32* %numVerticesA, align 4, !tbaa !8
  %cmp24 = icmp slt i32 %39, %40
  br i1 %cmp24, label %for.body26, label %for.cond.cleanup25

for.cond.cleanup25:                               ; preds = %for.cond23
  store i32 5, i32* %cleanup.dest.slot, align 4
  %41 = bitcast i32* %e0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #7
  br label %for.end49

for.body26:                                       ; preds = %for.cond23
  %42 = bitcast %class.btVector3** %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #7
  %43 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4, !tbaa !2
  %m_vertices = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %43, i32 0, i32 1
  %44 = load %struct.btFace*, %struct.btFace** %polyA, align 4, !tbaa !2
  %m_indices27 = getelementptr inbounds %struct.btFace, %struct.btFace* %44, i32 0, i32 0
  %45 = load i32, i32* %e0, align 4, !tbaa !8
  %call28 = call nonnull align 4 dereferenceable(4) i32* @_ZNK20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_indices27, i32 %45)
  %46 = load i32, i32* %call28, align 4, !tbaa !8
  %call29 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_vertices, i32 %46)
  store %class.btVector3* %call29, %class.btVector3** %a, align 4, !tbaa !2
  %47 = bitcast %class.btVector3** %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #7
  %48 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4, !tbaa !2
  %m_vertices30 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %48, i32 0, i32 1
  %49 = load %struct.btFace*, %struct.btFace** %polyA, align 4, !tbaa !2
  %m_indices31 = getelementptr inbounds %struct.btFace, %struct.btFace* %49, i32 0, i32 0
  %50 = load i32, i32* %e0, align 4, !tbaa !8
  %add = add nsw i32 %50, 1
  %51 = load i32, i32* %numVerticesA, align 4, !tbaa !8
  %rem = srem i32 %add, %51
  %call32 = call nonnull align 4 dereferenceable(4) i32* @_ZNK20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_indices31, i32 %rem)
  %52 = load i32, i32* %call32, align 4, !tbaa !8
  %call33 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_vertices30, i32 %52)
  store %class.btVector3* %call33, %class.btVector3** %b, align 4, !tbaa !2
  %53 = bitcast %class.btVector3* %edge0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %53) #7
  %54 = load %class.btVector3*, %class.btVector3** %a, align 4, !tbaa !2
  %55 = load %class.btVector3*, %class.btVector3** %b, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %edge0, %class.btVector3* nonnull align 4 dereferenceable(16) %54, %class.btVector3* nonnull align 4 dereferenceable(16) %55)
  %56 = bitcast %class.btVector3* %WorldEdge0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %56) #7
  %57 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %call34 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %57)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %WorldEdge0, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call34, %class.btVector3* nonnull align 4 dereferenceable(16) %edge0)
  %58 = bitcast %class.btVector3* %worldPlaneAnormal1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %58) #7
  %59 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %call35 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %59)
  %60 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %60) #7
  %61 = load %struct.btFace*, %struct.btFace** %polyA, align 4, !tbaa !2
  %m_plane36 = getelementptr inbounds %struct.btFace, %struct.btFace* %61, i32 0, i32 1
  %arrayidx37 = getelementptr inbounds [4 x float], [4 x float]* %m_plane36, i32 0, i32 0
  %62 = load %struct.btFace*, %struct.btFace** %polyA, align 4, !tbaa !2
  %m_plane38 = getelementptr inbounds %struct.btFace, %struct.btFace* %62, i32 0, i32 1
  %arrayidx39 = getelementptr inbounds [4 x float], [4 x float]* %m_plane38, i32 0, i32 1
  %63 = load %struct.btFace*, %struct.btFace** %polyA, align 4, !tbaa !2
  %m_plane40 = getelementptr inbounds %struct.btFace, %struct.btFace* %63, i32 0, i32 1
  %arrayidx41 = getelementptr inbounds [4 x float], [4 x float]* %m_plane40, i32 0, i32 2
  %call42 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %arrayidx37, float* nonnull align 4 dereferenceable(4) %arrayidx39, float* nonnull align 4 dereferenceable(4) %arrayidx41)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %worldPlaneAnormal1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call35, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %64 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %64) #7
  %65 = bitcast %class.btVector3* %planeNormalWS1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %65) #7
  %66 = bitcast %class.btVector3* %ref.tmp43 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %66) #7
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp43, %class.btVector3* %WorldEdge0, %class.btVector3* nonnull align 4 dereferenceable(16) %worldPlaneAnormal1)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %planeNormalWS1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp43)
  %67 = bitcast %class.btVector3* %ref.tmp43 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %67) #7
  %68 = bitcast %class.btVector3* %worldA1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %68) #7
  %69 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %70 = load %class.btVector3*, %class.btVector3** %a, align 4, !tbaa !2
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %worldA1, %class.btTransform* %69, %class.btVector3* nonnull align 4 dereferenceable(16) %70)
  %71 = bitcast float* %planeEqWS1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #7
  %call44 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %worldA1, %class.btVector3* nonnull align 4 dereferenceable(16) %planeNormalWS1)
  %fneg = fneg float %call44
  store float %fneg, float* %planeEqWS1, align 4, !tbaa !6
  %72 = bitcast %class.btVector3* %planeNormalWS to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %72) #7
  %73 = bitcast %class.btVector3* %planeNormalWS to i8*
  %74 = bitcast %class.btVector3* %planeNormalWS1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %73, i8* align 4 %74, i32 16, i1 false), !tbaa.struct !10
  %75 = bitcast float* %planeEqWS to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #7
  %76 = load float, float* %planeEqWS1, align 4, !tbaa !6
  store float %76, float* %planeEqWS, align 4, !tbaa !6
  %77 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pVtxIn, align 4, !tbaa !2
  %78 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pVtxOut, align 4, !tbaa !2
  %79 = load float, float* %planeEqWS, align 4, !tbaa !6
  call void @_ZN27btPolyhedralContactClipping8clipFaceERK20btAlignedObjectArrayI9btVector3ERS2_RKS1_f(%class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %77, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %78, %class.btVector3* nonnull align 4 dereferenceable(16) %planeNormalWS, float %79)
  call void @_Z6btSwapIP20btAlignedObjectArrayI9btVector3EEvRT_S5_(%class.btAlignedObjectArray** nonnull align 4 dereferenceable(4) %pVtxIn, %class.btAlignedObjectArray** nonnull align 4 dereferenceable(4) %pVtxOut)
  %80 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pVtxOut, align 4, !tbaa !2
  %81 = bitcast %class.btVector3* %ref.tmp45 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %81) #7
  %call46 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp45)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray* %80, i32 0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp45)
  %82 = bitcast %class.btVector3* %ref.tmp45 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %82) #7
  %83 = bitcast float* %planeEqWS to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #7
  %84 = bitcast %class.btVector3* %planeNormalWS to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %84) #7
  %85 = bitcast float* %planeEqWS1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #7
  %86 = bitcast %class.btVector3* %worldA1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %86) #7
  %87 = bitcast %class.btVector3* %planeNormalWS1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %87) #7
  %88 = bitcast %class.btVector3* %worldPlaneAnormal1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %88) #7
  %89 = bitcast %class.btVector3* %WorldEdge0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %89) #7
  %90 = bitcast %class.btVector3* %edge0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %90) #7
  %91 = bitcast %class.btVector3** %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #7
  %92 = bitcast %class.btVector3** %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #7
  br label %for.inc47

for.inc47:                                        ; preds = %for.body26
  %93 = load i32, i32* %e0, align 4, !tbaa !8
  %inc48 = add nsw i32 %93, 1
  store i32 %inc48, i32* %e0, align 4, !tbaa !8
  br label %for.cond23

for.end49:                                        ; preds = %for.cond.cleanup25
  %94 = bitcast %class.btVector3* %point to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %94) #7
  %call50 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %point)
  %95 = bitcast %class.btVector3* %localPlaneNormal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %95) #7
  %96 = load %struct.btFace*, %struct.btFace** %polyA, align 4, !tbaa !2
  %m_plane51 = getelementptr inbounds %struct.btFace, %struct.btFace* %96, i32 0, i32 1
  %arrayidx52 = getelementptr inbounds [4 x float], [4 x float]* %m_plane51, i32 0, i32 0
  %97 = load %struct.btFace*, %struct.btFace** %polyA, align 4, !tbaa !2
  %m_plane53 = getelementptr inbounds %struct.btFace, %struct.btFace* %97, i32 0, i32 1
  %arrayidx54 = getelementptr inbounds [4 x float], [4 x float]* %m_plane53, i32 0, i32 1
  %98 = load %struct.btFace*, %struct.btFace** %polyA, align 4, !tbaa !2
  %m_plane55 = getelementptr inbounds %struct.btFace, %struct.btFace* %98, i32 0, i32 1
  %arrayidx56 = getelementptr inbounds [4 x float], [4 x float]* %m_plane55, i32 0, i32 2
  %call57 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %localPlaneNormal, float* nonnull align 4 dereferenceable(4) %arrayidx52, float* nonnull align 4 dereferenceable(4) %arrayidx54, float* nonnull align 4 dereferenceable(4) %arrayidx56)
  %99 = bitcast float* %localPlaneEq to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %99) #7
  %100 = load %struct.btFace*, %struct.btFace** %polyA, align 4, !tbaa !2
  %m_plane58 = getelementptr inbounds %struct.btFace, %struct.btFace* %100, i32 0, i32 1
  %arrayidx59 = getelementptr inbounds [4 x float], [4 x float]* %m_plane58, i32 0, i32 3
  %101 = load float, float* %arrayidx59, align 4, !tbaa !6
  store float %101, float* %localPlaneEq, align 4, !tbaa !6
  %102 = bitcast %class.btVector3* %planeNormalWS60 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %102) #7
  %103 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %call61 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %103)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %planeNormalWS60, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call61, %class.btVector3* nonnull align 4 dereferenceable(16) %localPlaneNormal)
  %104 = bitcast float* %planeEqWS62 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %104) #7
  %105 = load float, float* %localPlaneEq, align 4, !tbaa !6
  %106 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %call63 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %106)
  %call64 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %planeNormalWS60, %class.btVector3* nonnull align 4 dereferenceable(16) %call63)
  %sub = fsub float %105, %call64
  store float %sub, float* %planeEqWS62, align 4, !tbaa !6
  %107 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %107) #7
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond65

for.cond65:                                       ; preds = %for.inc81, %for.end49
  %108 = load i32, i32* %i, align 4, !tbaa !8
  %109 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pVtxIn, align 4, !tbaa !2
  %call66 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %109)
  %cmp67 = icmp slt i32 %108, %call66
  br i1 %cmp67, label %for.body69, label %for.cond.cleanup68

for.cond.cleanup68:                               ; preds = %for.cond65
  store i32 8, i32* %cleanup.dest.slot, align 4
  %110 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #7
  br label %for.end83

for.body69:                                       ; preds = %for.cond65
  %111 = bitcast %class.btVector3* %vtx to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %111) #7
  %112 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pVtxIn, align 4, !tbaa !2
  %113 = load i32, i32* %i, align 4, !tbaa !8
  %call70 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3E2atEi(%class.btAlignedObjectArray* %112, i32 %113)
  %114 = bitcast %class.btVector3* %vtx to i8*
  %115 = bitcast %class.btVector3* %call70 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %114, i8* align 4 %115, i32 16, i1 false), !tbaa.struct !10
  %116 = bitcast float* %depth to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %116) #7
  %call71 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %planeNormalWS60, %class.btVector3* nonnull align 4 dereferenceable(16) %vtx)
  %117 = load float, float* %planeEqWS62, align 4, !tbaa !6
  %add72 = fadd float %call71, %117
  store float %add72, float* %depth, align 4, !tbaa !6
  %118 = load float, float* %depth, align 4, !tbaa !6
  %119 = load float, float* %minDist.addr, align 4, !tbaa !6
  %cmp73 = fcmp ole float %118, %119
  br i1 %cmp73, label %if.then74, label %if.end75

if.then74:                                        ; preds = %for.body69
  %120 = load float, float* %minDist.addr, align 4, !tbaa !6
  store float %120, float* %depth, align 4, !tbaa !6
  br label %if.end75

if.end75:                                         ; preds = %if.then74, %for.body69
  %121 = load float, float* %depth, align 4, !tbaa !6
  %122 = load float, float* %maxDist.addr, align 4, !tbaa !6
  %cmp76 = fcmp ole float %121, %122
  br i1 %cmp76, label %if.then77, label %if.end80

if.then77:                                        ; preds = %if.end75
  %123 = bitcast %class.btVector3* %point78 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %123) #7
  %124 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pVtxIn, align 4, !tbaa !2
  %125 = load i32, i32* %i, align 4, !tbaa !8
  %call79 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3E2atEi(%class.btAlignedObjectArray* %124, i32 %125)
  %126 = bitcast %class.btVector3* %point78 to i8*
  %127 = bitcast %class.btVector3* %call79 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %126, i8* align 4 %127, i32 16, i1 false), !tbaa.struct !10
  %128 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %resultOut.addr, align 4, !tbaa !2
  %129 = load %class.btVector3*, %class.btVector3** %separatingNormal.addr, align 4, !tbaa !2
  %130 = load float, float* %depth, align 4, !tbaa !6
  %131 = bitcast %"struct.btDiscreteCollisionDetectorInterface::Result"* %128 to void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)***
  %vtable = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)**, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*** %131, align 4, !tbaa !24
  %vfn = getelementptr inbounds void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vtable, i64 4
  %132 = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vfn, align 4
  call void %132(%"struct.btDiscreteCollisionDetectorInterface::Result"* %128, %class.btVector3* nonnull align 4 dereferenceable(16) %129, %class.btVector3* nonnull align 4 dereferenceable(16) %point78, float %130)
  %133 = bitcast %class.btVector3* %point78 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %133) #7
  br label %if.end80

if.end80:                                         ; preds = %if.then77, %if.end75
  %134 = bitcast float* %depth to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %134) #7
  %135 = bitcast %class.btVector3* %vtx to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %135) #7
  br label %for.inc81

for.inc81:                                        ; preds = %if.end80
  %136 = load i32, i32* %i, align 4, !tbaa !8
  %inc82 = add nsw i32 %136, 1
  store i32 %inc82, i32* %i, align 4, !tbaa !8
  br label %for.cond65

for.end83:                                        ; preds = %for.cond.cleanup68
  %137 = bitcast float* %planeEqWS62 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %137) #7
  %138 = bitcast %class.btVector3* %planeNormalWS60 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %138) #7
  %139 = bitcast float* %localPlaneEq to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #7
  %140 = bitcast %class.btVector3* %localPlaneNormal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %140) #7
  %141 = bitcast %class.btVector3* %point to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %141) #7
  %142 = bitcast i32* %numVerticesA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #7
  %143 = bitcast %struct.btFace** %polyA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end83, %if.then18
  %144 = bitcast i32* %closestFaceA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #7
  %145 = bitcast %class.btAlignedObjectArray** %pVtxOut to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #7
  %146 = bitcast %class.btAlignedObjectArray** %pVtxIn to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #7
  %call87 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* %worldVertsB2) #7
  %147 = bitcast %class.btAlignedObjectArray* %worldVertsB2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %147) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btVector3*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btVector3** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %call2 = call i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btVector3*
  store %class.btVector3* %3, %class.btVector3** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  %4 = load %class.btVector3*, %class.btVector3** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %class.btVector3* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !28
  %5 = load %class.btVector3*, %class.btVector3** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* %5, %class.btVector3** %m_data, align 4, !tbaa !16
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !29
  %7 = bitcast %class.btVector3** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !30
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNK20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %this, i32 %n) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !33
  %1 = load i32, i32* %n.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z6btSwapIP20btAlignedObjectArrayI9btVector3EEvRT_S5_(%class.btAlignedObjectArray** nonnull align 4 dereferenceable(4) %a, %class.btAlignedObjectArray** nonnull align 4 dereferenceable(4) %b) #2 comdat {
entry:
  %a.addr = alloca %class.btAlignedObjectArray**, align 4
  %b.addr = alloca %class.btAlignedObjectArray**, align 4
  %tmp = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray** %a, %class.btAlignedObjectArray*** %a.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray** %b, %class.btAlignedObjectArray*** %b.addr, align 4, !tbaa !2
  %0 = bitcast %class.btAlignedObjectArray** %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btAlignedObjectArray**, %class.btAlignedObjectArray*** %a.addr, align 4, !tbaa !2
  %2 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %1, align 4, !tbaa !2
  store %class.btAlignedObjectArray* %2, %class.btAlignedObjectArray** %tmp, align 4, !tbaa !2
  %3 = load %class.btAlignedObjectArray**, %class.btAlignedObjectArray*** %b.addr, align 4, !tbaa !2
  %4 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %3, align 4, !tbaa !2
  %5 = load %class.btAlignedObjectArray**, %class.btAlignedObjectArray*** %a.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray* %4, %class.btAlignedObjectArray** %5, align 4, !tbaa !2
  %6 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %tmp, align 4, !tbaa !2
  %7 = load %class.btAlignedObjectArray**, %class.btAlignedObjectArray*** %b.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray* %6, %class.btAlignedObjectArray** %7, align 4, !tbaa !2
  %8 = bitcast %class.btAlignedObjectArray** %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray* %this, i32 %newsize, %class.btVector3* nonnull align 4 dereferenceable(16) %fillData) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btVector3*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !8
  store %class.btVector3* %fillData, %class.btVector3** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !8
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %2 = load i32, i32* %curSize, align 4, !tbaa !8
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  store i32 %4, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %6 = load i32, i32* %curSize, align 4, !tbaa !8
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !16
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end17

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = load i32, i32* %curSize, align 4, !tbaa !8
  store i32 %14, i32* %i6, align 4, !tbaa !8
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc14, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !8
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  br label %for.end16

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %18 = load %class.btVector3*, %class.btVector3** %m_data11, align 4, !tbaa !16
  %19 = load i32, i32* %i6, align 4, !tbaa !8
  %arrayidx12 = getelementptr inbounds %class.btVector3, %class.btVector3* %18, i32 %19
  %20 = bitcast %class.btVector3* %arrayidx12 to i8*
  %call13 = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %20)
  %21 = bitcast i8* %call13 to %class.btVector3*
  %22 = load %class.btVector3*, %class.btVector3** %fillData.addr, align 4, !tbaa !2
  %23 = bitcast %class.btVector3* %21 to i8*
  %24 = bitcast %class.btVector3* %22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false), !tbaa.struct !10
  br label %for.inc14

for.inc14:                                        ; preds = %for.body10
  %25 = load i32, i32* %i6, align 4, !tbaa !8
  %inc15 = add nsw i32 %25, 1
  store i32 %inc15, i32* %i6, align 4, !tbaa !8
  br label %for.cond7

for.end16:                                        ; preds = %for.cond.cleanup9
  br label %if.end17

if.end17:                                         ; preds = %for.end16, %for.end
  %26 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %26, i32* %m_size, align 4, !tbaa !12
  %27 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3E2atEi(%class.btAlignedObjectArray* %this, i32 %n) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !16
  %1 = load i32, i32* %n.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %1
  ret %class.btVector3* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

define hidden void @_ZN27btPolyhedralContactClipping19clipHullAgainstHullERK9btVector3RK18btConvexPolyhedronS5_RK11btTransformS8_ffRN36btDiscreteCollisionDetectorInterface6ResultE(%class.btVector3* nonnull align 4 dereferenceable(16) %separatingNormal1, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %hullA, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %hullB, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB, float %minDist, float %maxDist, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %resultOut) #0 {
entry:
  %separatingNormal1.addr = alloca %class.btVector3*, align 4
  %hullA.addr = alloca %class.btConvexPolyhedron*, align 4
  %hullB.addr = alloca %class.btConvexPolyhedron*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %minDist.addr = alloca float, align 4
  %maxDist.addr = alloca float, align 4
  %resultOut.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  %separatingNormal = alloca %class.btVector3, align 4
  %closestFaceB = alloca i32, align 4
  %dmax = alloca float, align 4
  %face = alloca i32, align 4
  %Normal = alloca %class.btVector3, align 4
  %WorldNormal = alloca %class.btVector3, align 4
  %d = alloca float, align 4
  %worldVertsB1 = alloca %class.btAlignedObjectArray, align 4
  %polyB = alloca %struct.btFace*, align 4
  %numVertices = alloca i32, align 4
  %e0 = alloca i32, align 4
  %b = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btVector3* %separatingNormal1, %class.btVector3** %separatingNormal1.addr, align 4, !tbaa !2
  store %class.btConvexPolyhedron* %hullA, %class.btConvexPolyhedron** %hullA.addr, align 4, !tbaa !2
  store %class.btConvexPolyhedron* %hullB, %class.btConvexPolyhedron** %hullB.addr, align 4, !tbaa !2
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4, !tbaa !2
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4, !tbaa !2
  store float %minDist, float* %minDist.addr, align 4, !tbaa !6
  store float %maxDist, float* %maxDist.addr, align 4, !tbaa !6
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %resultOut, %"struct.btDiscreteCollisionDetectorInterface::Result"** %resultOut.addr, align 4, !tbaa !2
  %0 = bitcast %class.btVector3* %separatingNormal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #7
  %1 = load %class.btVector3*, %class.btVector3** %separatingNormal1.addr, align 4, !tbaa !2
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %separatingNormal, %class.btVector3* %1)
  %2 = bitcast i32* %closestFaceB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  store i32 -1, i32* %closestFaceB, align 4, !tbaa !8
  %3 = bitcast float* %dmax to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  store float 0xC7EFFFFFE0000000, float* %dmax, align 4, !tbaa !6
  %4 = bitcast i32* %face to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  store i32 0, i32* %face, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %5 = load i32, i32* %face, align 4, !tbaa !8
  %6 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4, !tbaa !2
  %m_faces = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %6, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %m_faces)
  %cmp = icmp slt i32 %5, %call
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %face to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %8 = bitcast %class.btVector3* %Normal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #7
  %9 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4, !tbaa !2
  %m_faces1 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %9, i32 0, i32 2
  %10 = load i32, i32* %face, align 4, !tbaa !8
  %call2 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces1, i32 %10)
  %m_plane = getelementptr inbounds %struct.btFace, %struct.btFace* %call2, i32 0, i32 1
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_plane, i32 0, i32 0
  %11 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4, !tbaa !2
  %m_faces3 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %11, i32 0, i32 2
  %12 = load i32, i32* %face, align 4, !tbaa !8
  %call4 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces3, i32 %12)
  %m_plane5 = getelementptr inbounds %struct.btFace, %struct.btFace* %call4, i32 0, i32 1
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_plane5, i32 0, i32 1
  %13 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4, !tbaa !2
  %m_faces7 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %13, i32 0, i32 2
  %14 = load i32, i32* %face, align 4, !tbaa !8
  %call8 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces7, i32 %14)
  %m_plane9 = getelementptr inbounds %struct.btFace, %struct.btFace* %call8, i32 0, i32 1
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_plane9, i32 0, i32 2
  %call11 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %Normal, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx6, float* nonnull align 4 dereferenceable(4) %arrayidx10)
  %15 = bitcast %class.btVector3* %WorldNormal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %15) #7
  %16 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %call12 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %16)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %WorldNormal, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call12, %class.btVector3* nonnull align 4 dereferenceable(16) %Normal)
  %17 = bitcast float* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #7
  %call13 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %WorldNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %separatingNormal)
  store float %call13, float* %d, align 4, !tbaa !6
  %18 = load float, float* %d, align 4, !tbaa !6
  %19 = load float, float* %dmax, align 4, !tbaa !6
  %cmp14 = fcmp ogt float %18, %19
  br i1 %cmp14, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %20 = load float, float* %d, align 4, !tbaa !6
  store float %20, float* %dmax, align 4, !tbaa !6
  %21 = load i32, i32* %face, align 4, !tbaa !8
  store i32 %21, i32* %closestFaceB, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %22 = bitcast float* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #7
  %23 = bitcast %class.btVector3* %WorldNormal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %23) #7
  %24 = bitcast %class.btVector3* %Normal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %24) #7
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %25 = load i32, i32* %face, align 4, !tbaa !8
  %inc = add nsw i32 %25, 1
  store i32 %inc, i32* %face, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %26 = bitcast %class.btAlignedObjectArray* %worldVertsB1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %26) #7
  %call15 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* %worldVertsB1)
  %27 = bitcast %struct.btFace** %polyB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #7
  %28 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4, !tbaa !2
  %m_faces16 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %28, i32 0, i32 2
  %29 = load i32, i32* %closestFaceB, align 4, !tbaa !8
  %call17 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces16, i32 %29)
  store %struct.btFace* %call17, %struct.btFace** %polyB, align 4, !tbaa !2
  %30 = bitcast i32* %numVertices to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #7
  %31 = load %struct.btFace*, %struct.btFace** %polyB, align 4, !tbaa !2
  %m_indices = getelementptr inbounds %struct.btFace, %struct.btFace* %31, i32 0, i32 0
  %call18 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %m_indices)
  store i32 %call18, i32* %numVertices, align 4, !tbaa !8
  %32 = bitcast i32* %e0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #7
  store i32 0, i32* %e0, align 4, !tbaa !8
  br label %for.cond19

for.cond19:                                       ; preds = %for.inc26, %for.end
  %33 = load i32, i32* %e0, align 4, !tbaa !8
  %34 = load i32, i32* %numVertices, align 4, !tbaa !8
  %cmp20 = icmp slt i32 %33, %34
  br i1 %cmp20, label %for.body22, label %for.cond.cleanup21

for.cond.cleanup21:                               ; preds = %for.cond19
  %35 = bitcast i32* %e0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #7
  br label %for.end28

for.body22:                                       ; preds = %for.cond19
  %36 = bitcast %class.btVector3** %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #7
  %37 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4, !tbaa !2
  %m_vertices = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %37, i32 0, i32 1
  %38 = load %struct.btFace*, %struct.btFace** %polyB, align 4, !tbaa !2
  %m_indices23 = getelementptr inbounds %struct.btFace, %struct.btFace* %38, i32 0, i32 0
  %39 = load i32, i32* %e0, align 4, !tbaa !8
  %call24 = call nonnull align 4 dereferenceable(4) i32* @_ZNK20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_indices23, i32 %39)
  %40 = load i32, i32* %call24, align 4, !tbaa !8
  %call25 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_vertices, i32 %40)
  store %class.btVector3* %call25, %class.btVector3** %b, align 4, !tbaa !2
  %41 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %41) #7
  %42 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %43 = load %class.btVector3*, %class.btVector3** %b, align 4, !tbaa !2
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btTransform* %42, %class.btVector3* nonnull align 4 dereferenceable(16) %43)
  call void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray* %worldVertsB1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %44 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %44) #7
  %45 = bitcast %class.btVector3** %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #7
  br label %for.inc26

for.inc26:                                        ; preds = %for.body22
  %46 = load i32, i32* %e0, align 4, !tbaa !8
  %inc27 = add nsw i32 %46, 1
  store i32 %inc27, i32* %e0, align 4, !tbaa !8
  br label %for.cond19

for.end28:                                        ; preds = %for.cond.cleanup21
  %47 = bitcast i32* %numVertices to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #7
  %48 = bitcast %struct.btFace** %polyB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #7
  %49 = load i32, i32* %closestFaceB, align 4, !tbaa !8
  %cmp29 = icmp sge i32 %49, 0
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %for.end28
  %50 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4, !tbaa !2
  %51 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %52 = load float, float* %minDist.addr, align 4, !tbaa !6
  %53 = load float, float* %maxDist.addr, align 4, !tbaa !6
  %54 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %resultOut.addr, align 4, !tbaa !2
  call void @_ZN27btPolyhedralContactClipping19clipFaceAgainstHullERK9btVector3RK18btConvexPolyhedronRK11btTransformR20btAlignedObjectArrayIS0_EffRN36btDiscreteCollisionDetectorInterface6ResultE(%class.btVector3* nonnull align 4 dereferenceable(16) %separatingNormal, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %50, %class.btTransform* nonnull align 4 dereferenceable(64) %51, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %worldVertsB1, float %52, float %53, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %54)
  br label %if.end31

if.end31:                                         ; preds = %if.then30, %for.end28
  %call32 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* %worldVertsB1) #7
  %55 = bitcast %class.btAlignedObjectArray* %worldVertsB1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %55) #7
  %56 = bitcast float* %dmax to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #7
  %57 = bitcast i32* %closestFaceB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #7
  %58 = bitcast %class.btVector3* %separatingNormal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %58) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector310normalizedEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %norm = alloca %class.btVector3, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast %class.btVector3* %norm to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #7
  %1 = bitcast %class.btVector3* %norm to i8*
  %2 = bitcast %class.btVector3* %this1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !10
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %norm)
  %3 = bitcast %class.btVector3* %agg.result to i8*
  %4 = bitcast %class.btVector3* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false), !tbaa.struct !10
  %5 = bitcast %class.btVector3* %norm to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #7
  %1 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4, !tbaa !6
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !6
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !6
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #7
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #7
  ret void
}

declare void @_ZNK18btConvexPolyhedron7projectERK11btTransformRK9btVector3RfS6_RS3_S7_(%class.btConvexPolyhedron*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4), float* nonnull align 4 dereferenceable(4), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) #5

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #6

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !6
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #7
  ret %class.btVector3* %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %1 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  ret float %call
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #3 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !6
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !6
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !6
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !6
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !6
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !6
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #7
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #7
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #6

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !29
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !8
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN9btVector3nwEmPv(i32 %0, i8* %ptr) #2 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !34
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !28
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data, align 4, !tbaa !16
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !12
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !29
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !8
  store i32 %last, i32* %last.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %first.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %last.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !16
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !16
  %tobool = icmp ne %class.btVector3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !28, !range !23
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btVector3*, %class.btVector3** %m_data4, align 4, !tbaa !16
  call void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %class.btVector3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data5, align 4, !tbaa !16
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %class.btVector3* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.btVector3*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %ptr, %class.btVector3** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVector3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #5

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #3 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %class.btVector3** null)
  %2 = bitcast %class.btVector3* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %class.btVector3* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !8
  store i32 %end, i32* %end.addr, align 4, !tbaa !8
  store %class.btVector3* %dest, %class.btVector3** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %start.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %end.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btVector3*, %class.btVector3** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 %5
  %6 = bitcast %class.btVector3* %arrayidx to i8*
  %call = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %6)
  %7 = bitcast i8* %call to %class.btVector3*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !16
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 %9
  %10 = bitcast %class.btVector3* %7 to i8*
  %11 = bitcast %class.btVector3* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !10
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #7
  ret void
}

define linkonce_odr hidden %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %class.btVector3** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btVector3**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  store %class.btVector3** %hint, %class.btVector3*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !8
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btVector3*
  ret %class.btVector3* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #5

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind readnone speculatable willreturn }
attributes #7 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"float", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"int", !4, i64 0}
!10 = !{i64 0, i64 16, !11}
!11 = !{!4, !4, i64 0}
!12 = !{!13, !9, i64 4}
!13 = !{!"_ZTS20btAlignedObjectArrayI9btVector3E", !14, i64 0, !9, i64 4, !9, i64 8, !3, i64 12, !15, i64 16}
!14 = !{!"_ZTS18btAlignedAllocatorI9btVector3Lj16EE"}
!15 = !{!"bool", !4, i64 0}
!16 = !{!13, !3, i64 12}
!17 = !{!18, !7, i64 96}
!18 = !{!"_ZTS18btConvexPolyhedron", !13, i64 4, !19, i64 24, !13, i64 44, !21, i64 64, !21, i64 80, !7, i64 96, !21, i64 100, !21, i64 116}
!19 = !{!"_ZTS20btAlignedObjectArrayI6btFaceE", !20, i64 0, !9, i64 4, !9, i64 8, !3, i64 12, !15, i64 16}
!20 = !{!"_ZTS18btAlignedAllocatorI6btFaceLj16EE"}
!21 = !{!"_ZTS9btVector3", !4, i64 0}
!22 = !{!15, !15, i64 0}
!23 = !{i8 0, i8 2}
!24 = !{!25, !25, i64 0}
!25 = !{!"vtable pointer", !5, i64 0}
!26 = !{!19, !9, i64 4}
!27 = !{!19, !3, i64 12}
!28 = !{!13, !15, i64 16}
!29 = !{!13, !9, i64 8}
!30 = !{!31, !9, i64 4}
!31 = !{!"_ZTS20btAlignedObjectArrayIiE", !32, i64 0, !9, i64 4, !9, i64 8, !3, i64 12, !15, i64 16}
!32 = !{!"_ZTS18btAlignedAllocatorIiLj16EE"}
!33 = !{!31, !3, i64 12}
!34 = !{!35, !35, i64 0}
!35 = !{!"long", !4, i64 0}
