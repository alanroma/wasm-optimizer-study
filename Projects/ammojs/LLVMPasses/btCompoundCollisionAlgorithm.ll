; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionDispatch/btCompoundCollisionAlgorithm.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionDispatch/btCompoundCollisionAlgorithm.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%class.btCompoundCollisionAlgorithm = type { %class.btActivatingCollisionAlgorithm, %class.btAlignedObjectArray, i8, %class.btPersistentManifold*, i8, i32 }
%class.btActivatingCollisionAlgorithm = type { %class.btCollisionAlgorithm }
%class.btCollisionAlgorithm = type { i32 (...)**, %class.btDispatcher* }
%class.btDispatcher = type { i32 (...)** }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionAlgorithm**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, i32, i32, i32, i32, i8*, i8, float, float, float, float, float, float, float, i32, %class.btVector3, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%union.anon = type { i8* }
%struct.btCollisionAlgorithmConstructionInfo = type { %class.btDispatcher*, %class.btPersistentManifold* }
%struct.btCollisionObjectWrapper = type { %struct.btCollisionObjectWrapper*, %class.btCollisionShape*, %class.btCollisionObject*, %class.btTransform*, i32, i32 }
%class.btCompoundShape = type { %class.btCollisionShape, %class.btAlignedObjectArray.0, %class.btVector3, %class.btVector3, %struct.btDbvt*, i32, float, %class.btVector3 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.btCompoundShapeChild*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btCompoundShapeChild = type { %class.btTransform, %class.btCollisionShape*, i32, float, %struct.btDbvtNode* }
%struct.btDbvtNode = type { %struct.btDbvtAabbMm, %struct.btDbvtNode*, %union.anon.3 }
%struct.btDbvtAabbMm = type { %class.btVector3, %class.btVector3 }
%union.anon.3 = type { [2 x %struct.btDbvtNode*] }
%struct.btDbvt = type { %struct.btDbvtNode*, %struct.btDbvtNode*, i32, i32, i32, %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.9 }
%class.btAlignedObjectArray.5 = type <{ %class.btAlignedAllocator.6, [3 x i8], i32, i32, %"struct.btDbvt::sStkNN"*, i8, [3 x i8] }>
%class.btAlignedAllocator.6 = type { i8 }
%"struct.btDbvt::sStkNN" = type { %struct.btDbvtNode*, %struct.btDbvtNode* }
%class.btAlignedObjectArray.9 = type <{ %class.btAlignedAllocator.10, [3 x i8], i32, i32, %struct.btDbvtNode**, i8, [3 x i8] }>
%class.btAlignedAllocator.10 = type { i8 }
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btIDebugDraw = type { i32 (...)** }
%class.btManifoldResult = type { %"struct.btDiscreteCollisionDetectorInterface::Result", %class.btPersistentManifold*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i32, i32, i32, i32 }
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }
%struct.btCompoundLeafCallback = type { %"struct.btDbvt::ICollide", %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btDispatcher*, %struct.btDispatcherInfo*, %class.btManifoldResult*, %class.btCollisionAlgorithm**, %class.btPersistentManifold* }
%"struct.btDbvt::ICollide" = type { i32 (...)** }
%class.btAlignedObjectArray.13 = type <{ %class.btAlignedAllocator.14, [3 x i8], i32, i32, %class.btPersistentManifold**, i8, [3 x i8] }>
%class.btAlignedAllocator.14 = type { i8 }

$_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEC2Ev = comdat any

$_ZNK24btCollisionObjectWrapper17getCollisionShapeEv = comdat any

$_ZNK15btCompoundShape17getUpdateRevisionEv = comdat any

$_ZNK15btCompoundShape17getNumChildShapesEv = comdat any

$_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE6resizeEiRKS1_ = comdat any

$_ZNK15btCompoundShape18getDynamicAabbTreeEv = comdat any

$_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi = comdat any

$_ZNK15btCompoundShape13getChildShapeEi = comdat any

$_ZNK24btCollisionObjectWrapper18getCollisionObjectEv = comdat any

$_ZNK24btCollisionObjectWrapper17getWorldTransformEv = comdat any

$_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii = comdat any

$_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmED2Ev = comdat any

$_ZN22btCompoundLeafCallbackC2EPK24btCollisionObjectWrapperS2_P12btDispatcherRK16btDispatcherInfoP16btManifoldResultPP20btCollisionAlgorithmP20btPersistentManifold = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi = comdat any

$_ZNK20btPersistentManifold14getNumContactsEv = comdat any

$_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold = comdat any

$_ZN16btManifoldResult20refreshContactPointsEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZNK11btTransform7inverseEv = comdat any

$_ZNK11btTransformmlERKS_ = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZN12btDbvtAabbMm6FromMMERK9btVector3S2_ = comdat any

$_ZNK6btDbvt9collideTVEPK10btDbvtNodeRK12btDbvtAabbMmRNS_8ICollideE = comdat any

$_ZN22btCompoundLeafCallback17ProcessChildShapeEPK16btCollisionShapei = comdat any

$_ZNK15btCompoundShape17getChildTransformEi = comdat any

$_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_ = comdat any

$_ZN6btDbvt8ICollideD2Ev = comdat any

$_ZN17btCollisionObject17getCollisionShapeEv = comdat any

$_ZN17btCollisionObject17getWorldTransformEv = comdat any

$_ZN15btCompoundShape17getChildTransformEi = comdat any

$_ZN17btCollisionObject17setWorldTransformERK11btTransform = comdat any

$_ZN28btCompoundCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE = comdat any

$_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi = comdat any

$_ZN6btDbvt8ICollideC2Ev = comdat any

$_ZN22btCompoundLeafCallbackD0Ev = comdat any

$_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_ = comdat any

$_ZN22btCompoundLeafCallback7ProcessEPK10btDbvtNode = comdat any

$_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef = comdat any

$_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode = comdat any

$_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode = comdat any

$_ZN6btDbvt8ICollideD0Ev = comdat any

$_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNode = comdat any

$_ZNK20btPersistentManifold8getBody0Ev = comdat any

$_ZNK17btCollisionObject17getWorldTransformEv = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZngRK9btVector3 = comdat any

$_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZN12btDbvtAabbMmC2Ev = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE6resizeEiRKS2_ = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9push_backERKS2_ = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeEixEi = comdat any

$_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE8pop_backEv = comdat any

$_Z9IntersectRK12btDbvtAabbMmS1_ = comdat any

$_ZNK10btDbvtNode10isinternalEv = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeED2Ev = comdat any

$_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE4initEv = comdat any

$_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4copyEiiPS2_ = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EE8allocateEiPPKS2_ = comdat any

$_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EE10deallocateEPS2_ = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9allocSizeEi = comdat any

$_ZNK10btDbvtNode6isleafEv = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE5clearEv = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZNK16btManifoldResult16getBody0InternalEv = comdat any

$_ZNK16btManifoldResult12getBody0WrapEv = comdat any

$_ZN16btManifoldResult12setBody0WrapEPK24btCollisionObjectWrapper = comdat any

$_ZNK16btManifoldResult12getBody1WrapEv = comdat any

$_ZN16btManifoldResult12setBody1WrapEPK24btCollisionObjectWrapper = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi = comdat any

$_ZN18btAlignedAllocatorIP20btCollisionAlgorithmLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE4initEv = comdat any

$_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP20btCollisionAlgorithmLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP20btCollisionAlgorithmLj16EE8allocateEiPPKS1_ = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_ = comdat any

$_ZTV22btCompoundLeafCallback = comdat any

$_ZTS22btCompoundLeafCallback = comdat any

$_ZTSN6btDbvt8ICollideE = comdat any

$_ZTIN6btDbvt8ICollideE = comdat any

$_ZTI22btCompoundLeafCallback = comdat any

$_ZTVN6btDbvt8ICollideE = comdat any

@gCompoundChildShapePairCallback = hidden global i1 (%class.btCollisionShape*, %class.btCollisionShape*)* null, align 4
@_ZTV28btCompoundCollisionAlgorithm = hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI28btCompoundCollisionAlgorithm to i8*), i8* bitcast (%class.btCompoundCollisionAlgorithm* (%class.btCompoundCollisionAlgorithm*)* @_ZN28btCompoundCollisionAlgorithmD1Ev to i8*), i8* bitcast (void (%class.btCompoundCollisionAlgorithm*)* @_ZN28btCompoundCollisionAlgorithmD0Ev to i8*), i8* bitcast (void (%class.btCompoundCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)* @_ZN28btCompoundCollisionAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResult to i8*), i8* bitcast (float (%class.btCompoundCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)* @_ZN28btCompoundCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult to i8*), i8* bitcast (void (%class.btCompoundCollisionAlgorithm*, %class.btAlignedObjectArray.13*)* @_ZN28btCompoundCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS28btCompoundCollisionAlgorithm = hidden constant [31 x i8] c"28btCompoundCollisionAlgorithm\00", align 1
@_ZTI30btActivatingCollisionAlgorithm = external constant i8*
@_ZTI28btCompoundCollisionAlgorithm = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([31 x i8], [31 x i8]* @_ZTS28btCompoundCollisionAlgorithm, i32 0, i32 0), i8* bitcast (i8** @_ZTI30btActivatingCollisionAlgorithm to i8*) }, align 4
@_ZTV22btCompoundLeafCallback = linkonce_odr hidden unnamed_addr constant { [9 x i8*] } { [9 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI22btCompoundLeafCallback to i8*), i8* bitcast (%"struct.btDbvt::ICollide"* (%"struct.btDbvt::ICollide"*)* @_ZN6btDbvt8ICollideD2Ev to i8*), i8* bitcast (void (%struct.btCompoundLeafCallback*)* @_ZN22btCompoundLeafCallbackD0Ev to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_ to i8*), i8* bitcast (void (%struct.btCompoundLeafCallback*, %struct.btDbvtNode*)* @_ZN22btCompoundLeafCallback7ProcessEPK10btDbvtNode to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, float)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef to i8*), i8* bitcast (i1 (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode to i8*), i8* bitcast (i1 (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode to i8*)] }, comdat, align 4
@_ZTS22btCompoundLeafCallback = linkonce_odr hidden constant [25 x i8] c"22btCompoundLeafCallback\00", comdat, align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTSN6btDbvt8ICollideE = linkonce_odr hidden constant [19 x i8] c"N6btDbvt8ICollideE\00", comdat, align 1
@_ZTIN6btDbvt8ICollideE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([19 x i8], [19 x i8]* @_ZTSN6btDbvt8ICollideE, i32 0, i32 0) }, comdat, align 4
@_ZTI22btCompoundLeafCallback = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([25 x i8], [25 x i8]* @_ZTS22btCompoundLeafCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTIN6btDbvt8ICollideE to i8*) }, comdat, align 4
@_ZTVN6btDbvt8ICollideE = linkonce_odr hidden unnamed_addr constant { [9 x i8*] } { [9 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTIN6btDbvt8ICollideE to i8*), i8* bitcast (%"struct.btDbvt::ICollide"* (%"struct.btDbvt::ICollide"*)* @_ZN6btDbvt8ICollideD2Ev to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*)* @_ZN6btDbvt8ICollideD0Ev to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_ to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNode to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, float)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef to i8*), i8* bitcast (i1 (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode to i8*), i8* bitcast (i1 (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode to i8*)] }, comdat, align 4

@_ZN28btCompoundCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b = hidden unnamed_addr alias %class.btCompoundCollisionAlgorithm* (%class.btCompoundCollisionAlgorithm*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1), %class.btCompoundCollisionAlgorithm* (%class.btCompoundCollisionAlgorithm*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1)* @_ZN28btCompoundCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b
@_ZN28btCompoundCollisionAlgorithmD1Ev = hidden unnamed_addr alias %class.btCompoundCollisionAlgorithm* (%class.btCompoundCollisionAlgorithm*), %class.btCompoundCollisionAlgorithm* (%class.btCompoundCollisionAlgorithm*)* @_ZN28btCompoundCollisionAlgorithmD2Ev

define hidden %class.btCompoundCollisionAlgorithm* @_ZN28btCompoundCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b(%class.btCompoundCollisionAlgorithm* returned %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap, i1 zeroext %isSwapped) unnamed_addr #0 {
entry:
  %retval = alloca %class.btCompoundCollisionAlgorithm*, align 4
  %this.addr = alloca %class.btCompoundCollisionAlgorithm*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %isSwapped.addr = alloca i8, align 1
  %colObjWrap = alloca %struct.btCollisionObjectWrapper*, align 4
  %compoundShape = alloca %class.btCompoundShape*, align 4
  store %class.btCompoundCollisionAlgorithm* %this, %class.btCompoundCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %frombool = zext i1 %isSwapped to i8
  store i8 %frombool, i8* %isSwapped.addr, align 1, !tbaa !6
  %this1 = load %class.btCompoundCollisionAlgorithm*, %class.btCompoundCollisionAlgorithm** %this.addr, align 4
  store %class.btCompoundCollisionAlgorithm* %this1, %class.btCompoundCollisionAlgorithm** %retval, align 4
  %0 = bitcast %class.btCompoundCollisionAlgorithm* %this1 to %class.btActivatingCollisionAlgorithm*
  %1 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %3 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call = call %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%class.btActivatingCollisionAlgorithm* %0, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %1, %struct.btCollisionObjectWrapper* %2, %struct.btCollisionObjectWrapper* %3)
  %4 = bitcast %class.btCompoundCollisionAlgorithm* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV28btCompoundCollisionAlgorithm, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %4, align 4, !tbaa !8
  %m_childCollisionAlgorithms = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEC2Ev(%class.btAlignedObjectArray* %m_childCollisionAlgorithms)
  %m_isSwapped = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  %5 = load i8, i8* %isSwapped.addr, align 1, !tbaa !6, !range !10
  %tobool = trunc i8 %5 to i1
  %frombool3 = zext i1 %tobool to i8
  store i8 %frombool3, i8* %m_isSwapped, align 4, !tbaa !11
  %m_sharedManifold = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %6 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %m_manifold = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %6, i32 0, i32 1
  %7 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifold, align 4, !tbaa !16
  store %class.btPersistentManifold* %7, %class.btPersistentManifold** %m_sharedManifold, align 4, !tbaa !18
  %m_ownsManifold = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 4
  store i8 0, i8* %m_ownsManifold, align 4, !tbaa !19
  %8 = bitcast %struct.btCollisionObjectWrapper** %colObjWrap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  %m_isSwapped4 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  %9 = load i8, i8* %m_isSwapped4, align 4, !tbaa !11, !range !10
  %tobool5 = trunc i8 %9 to i1
  br i1 %tobool5, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %10 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %entry
  %11 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.btCollisionObjectWrapper* [ %10, %cond.true ], [ %11, %cond.false ]
  store %struct.btCollisionObjectWrapper* %cond, %struct.btCollisionObjectWrapper** %colObjWrap, align 4, !tbaa !2
  %12 = bitcast %class.btCompoundShape** %compoundShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  %13 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObjWrap, align 4, !tbaa !2
  %call6 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %13)
  %14 = bitcast %class.btCollisionShape* %call6 to %class.btCompoundShape*
  store %class.btCompoundShape* %14, %class.btCompoundShape** %compoundShape, align 4, !tbaa !2
  %15 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape, align 4, !tbaa !2
  %call7 = call i32 @_ZNK15btCompoundShape17getUpdateRevisionEv(%class.btCompoundShape* %15)
  %m_compoundShapeRevision = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 5
  store i32 %call7, i32* %m_compoundShapeRevision, align 4, !tbaa !20
  %16 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %17 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  call void @_ZN28btCompoundCollisionAlgorithm26preallocateChildAlgorithmsEPK24btCollisionObjectWrapperS2_(%class.btCompoundCollisionAlgorithm* %this1, %struct.btCollisionObjectWrapper* %16, %struct.btCollisionObjectWrapper* %17)
  %18 = bitcast %class.btCompoundShape** %compoundShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  %19 = bitcast %struct.btCollisionObjectWrapper** %colObjWrap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  %20 = load %class.btCompoundCollisionAlgorithm*, %class.btCompoundCollisionAlgorithm** %retval, align 4
  ret %class.btCompoundCollisionAlgorithm* %20
}

declare %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%class.btActivatingCollisionAlgorithm* returned, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8), %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*) unnamed_addr #1

define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIP20btCollisionAlgorithmLj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %this) #3 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_shape = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 1
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %m_shape, align 4, !tbaa !21
  ret %class.btCollisionShape* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK15btCompoundShape17getUpdateRevisionEv(%class.btCompoundShape* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_updateRevision = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 5
  %0 = load i32, i32* %m_updateRevision, align 4, !tbaa !23
  ret i32 %0
}

define hidden void @_ZN28btCompoundCollisionAlgorithm26preallocateChildAlgorithmsEPK24btCollisionObjectWrapperS2_(%class.btCompoundCollisionAlgorithm* %this, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) #0 {
entry:
  %this.addr = alloca %class.btCompoundCollisionAlgorithm*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %colObjWrap = alloca %struct.btCollisionObjectWrapper*, align 4
  %otherObjWrap = alloca %struct.btCollisionObjectWrapper*, align 4
  %compoundShape = alloca %class.btCompoundShape*, align 4
  %numChildren = alloca i32, align 4
  %i = alloca i32, align 4
  %ref.tmp = alloca %class.btCollisionAlgorithm*, align 4
  %childShape = alloca %class.btCollisionShape*, align 4
  %childWrap = alloca %struct.btCollisionObjectWrapper, align 4
  store %class.btCompoundCollisionAlgorithm* %this, %class.btCompoundCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundCollisionAlgorithm*, %class.btCompoundCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast %struct.btCollisionObjectWrapper** %colObjWrap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %m_isSwapped = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  %1 = load i8, i8* %m_isSwapped, align 4, !tbaa !11, !range !10
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.btCollisionObjectWrapper* [ %2, %cond.true ], [ %3, %cond.false ]
  store %struct.btCollisionObjectWrapper* %cond, %struct.btCollisionObjectWrapper** %colObjWrap, align 4, !tbaa !2
  %4 = bitcast %struct.btCollisionObjectWrapper** %otherObjWrap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %m_isSwapped2 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  %5 = load i8, i8* %m_isSwapped2, align 4, !tbaa !11, !range !10
  %tobool3 = trunc i8 %5 to i1
  br i1 %tobool3, label %cond.true4, label %cond.false5

cond.true4:                                       ; preds = %cond.end
  %6 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  br label %cond.end6

cond.false5:                                      ; preds = %cond.end
  %7 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  br label %cond.end6

cond.end6:                                        ; preds = %cond.false5, %cond.true4
  %cond7 = phi %struct.btCollisionObjectWrapper* [ %6, %cond.true4 ], [ %7, %cond.false5 ]
  store %struct.btCollisionObjectWrapper* %cond7, %struct.btCollisionObjectWrapper** %otherObjWrap, align 4, !tbaa !2
  %8 = bitcast %class.btCompoundShape** %compoundShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObjWrap, align 4, !tbaa !2
  %call = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %9)
  %10 = bitcast %class.btCollisionShape* %call to %class.btCompoundShape*
  store %class.btCompoundShape* %10, %class.btCompoundShape** %compoundShape, align 4, !tbaa !2
  %11 = bitcast i32* %numChildren to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  %12 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape, align 4, !tbaa !2
  %call8 = call i32 @_ZNK15btCompoundShape17getNumChildShapesEv(%class.btCompoundShape* %12)
  store i32 %call8, i32* %numChildren, align 4, !tbaa !29
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %m_childCollisionAlgorithms = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %14 = load i32, i32* %numChildren, align 4, !tbaa !29
  %15 = bitcast %class.btCollisionAlgorithm** %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %ref.tmp, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE6resizeEiRKS1_(%class.btAlignedObjectArray* %m_childCollisionAlgorithms, i32 %14, %class.btCollisionAlgorithm** nonnull align 4 dereferenceable(4) %ref.tmp)
  %16 = bitcast %class.btCollisionAlgorithm** %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  store i32 0, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end6
  %17 = load i32, i32* %i, align 4, !tbaa !29
  %18 = load i32, i32* %numChildren, align 4, !tbaa !29
  %cmp = icmp slt i32 %17, %18
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %19 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape, align 4, !tbaa !2
  %call9 = call %struct.btDbvt* @_ZNK15btCompoundShape18getDynamicAabbTreeEv(%class.btCompoundShape* %19)
  %tobool10 = icmp ne %struct.btDbvt* %call9, null
  br i1 %tobool10, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %m_childCollisionAlgorithms11 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %20 = load i32, i32* %i, align 4, !tbaa !29
  %call12 = call nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray* %m_childCollisionAlgorithms11, i32 %20)
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %call12, align 4, !tbaa !2
  br label %if.end

if.else:                                          ; preds = %for.body
  %21 = bitcast %class.btCollisionShape** %childShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #8
  %22 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape, align 4, !tbaa !2
  %23 = load i32, i32* %i, align 4, !tbaa !29
  %call13 = call %class.btCollisionShape* @_ZNK15btCompoundShape13getChildShapeEi(%class.btCompoundShape* %22, i32 %23)
  store %class.btCollisionShape* %call13, %class.btCollisionShape** %childShape, align 4, !tbaa !2
  %24 = bitcast %struct.btCollisionObjectWrapper* %childWrap to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %24) #8
  %25 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObjWrap, align 4, !tbaa !2
  %26 = load %class.btCollisionShape*, %class.btCollisionShape** %childShape, align 4, !tbaa !2
  %27 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObjWrap, align 4, !tbaa !2
  %call14 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %27)
  %28 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObjWrap, align 4, !tbaa !2
  %call15 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %28)
  %29 = load i32, i32* %i, align 4, !tbaa !29
  %call16 = call %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* %childWrap, %struct.btCollisionObjectWrapper* %25, %class.btCollisionShape* %26, %class.btCollisionObject* %call14, %class.btTransform* nonnull align 4 dereferenceable(64) %call15, i32 -1, i32 %29)
  %30 = bitcast %class.btCompoundCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %m_dispatcher = getelementptr inbounds %class.btCollisionAlgorithm, %class.btCollisionAlgorithm* %30, i32 0, i32 1
  %31 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4, !tbaa !30
  %32 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %otherObjWrap, align 4, !tbaa !2
  %m_sharedManifold = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %33 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_sharedManifold, align 4, !tbaa !18
  %34 = bitcast %class.btDispatcher* %31 to %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)***
  %vtable = load %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)**, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)*** %34, align 4, !tbaa !8
  %vfn = getelementptr inbounds %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)*, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)** %vtable, i64 2
  %35 = load %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)*, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)** %vfn, align 4
  %call17 = call %class.btCollisionAlgorithm* %35(%class.btDispatcher* %31, %struct.btCollisionObjectWrapper* %childWrap, %struct.btCollisionObjectWrapper* %32, %class.btPersistentManifold* %33)
  %m_childCollisionAlgorithms18 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %36 = load i32, i32* %i, align 4, !tbaa !29
  %call19 = call nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray* %m_childCollisionAlgorithms18, i32 %36)
  store %class.btCollisionAlgorithm* %call17, %class.btCollisionAlgorithm** %call19, align 4, !tbaa !2
  %37 = bitcast %struct.btCollisionObjectWrapper* %childWrap to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %37) #8
  %38 = bitcast %class.btCollisionShape** %childShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %39 = load i32, i32* %i, align 4, !tbaa !29
  %inc = add nsw i32 %39, 1
  store i32 %inc, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %40 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #8
  %41 = bitcast i32* %numChildren to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #8
  %42 = bitcast %class.btCompoundShape** %compoundShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #8
  %43 = bitcast %struct.btCollisionObjectWrapper** %otherObjWrap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #8
  %44 = bitcast %struct.btCollisionObjectWrapper** %colObjWrap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #8
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

define linkonce_odr hidden i32 @_ZNK15btCompoundShape17getNumChildShapesEv(%class.btCompoundShape* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray.0* %m_children)
  ret i32 %call
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE6resizeEiRKS1_(%class.btAlignedObjectArray* %this, i32 %newsize, %class.btCollisionAlgorithm** nonnull align 4 dereferenceable(4) %fillData) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btCollisionAlgorithm**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !29
  store %class.btCollisionAlgorithm** %fillData, %class.btCollisionAlgorithm*** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !29
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !29
  %2 = load i32, i32* %curSize, align 4, !tbaa !29
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !29
  store i32 %4, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !29
  %6 = load i32, i32* %curSize, align 4, !tbaa !29
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %m_data, align 4, !tbaa !32
  %9 = load i32, i32* %i, align 4, !tbaa !29
  %arrayidx = getelementptr inbounds %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !29
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !29
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4sizeEv(%class.btAlignedObjectArray* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !29
  call void @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load i32, i32* %curSize, align 4, !tbaa !29
  store i32 %14, i32* %i6, align 4, !tbaa !29
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !29
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !29
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %18 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %m_data11, align 4, !tbaa !32
  %19 = load i32, i32* %i6, align 4, !tbaa !29
  %arrayidx12 = getelementptr inbounds %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %18, i32 %19
  %20 = bitcast %class.btCollisionAlgorithm** %arrayidx12 to i8*
  %21 = bitcast i8* %20 to %class.btCollisionAlgorithm**
  %22 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %fillData.addr, align 4, !tbaa !2
  %23 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %22, align 4, !tbaa !2
  store %class.btCollisionAlgorithm* %23, %class.btCollisionAlgorithm** %21, align 4, !tbaa !2
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %24 = load i32, i32* %i6, align 4, !tbaa !29
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !29
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %25 = load i32, i32* %newsize.addr, align 4, !tbaa !29
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %25, i32* %m_size, align 4, !tbaa !33
  %26 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btDbvt* @_ZNK15btCompoundShape18getDynamicAabbTreeEv(%class.btCompoundShape* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_dynamicAabbTree = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  %0 = load %struct.btDbvt*, %struct.btDbvt** %m_dynamicAabbTree, align 4, !tbaa !34
  ret %struct.btDbvt* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !29
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %m_data, align 4, !tbaa !32
  %1 = load i32, i32* %n.addr, align 4, !tbaa !29
  %arrayidx = getelementptr inbounds %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %0, i32 %1
  ret %class.btCollisionAlgorithm** %arrayidx
}

define linkonce_odr hidden %class.btCollisionShape* @_ZNK15btCompoundShape13getChildShapeEi(%class.btCompoundShape* %this, i32 %index) #0 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %index.addr = alloca i32, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !29
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %index.addr, align 4, !tbaa !29
  %call = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray.0* %m_children, i32 %0)
  %m_childShape = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call, i32 0, i32 1
  %1 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape, align 4, !tbaa !35
  ret %class.btCollisionShape* %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %this) #3 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_collisionObject = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_collisionObject, align 4, !tbaa !39
  ret %class.btCollisionObject* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %this) #3 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 3
  %0 = load %class.btTransform*, %class.btTransform** %m_worldTransform, align 4, !tbaa !40
  ret %class.btTransform* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* returned %this, %struct.btCollisionObjectWrapper* %parent, %class.btCollisionShape* %shape, %class.btCollisionObject* %collisionObject, %class.btTransform* nonnull align 4 dereferenceable(64) %worldTransform, i32 %partId, i32 %index) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %parent.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %shape.addr = alloca %class.btCollisionShape*, align 4
  %collisionObject.addr = alloca %class.btCollisionObject*, align 4
  %worldTransform.addr = alloca %class.btTransform*, align 4
  %partId.addr = alloca i32, align 4
  %index.addr = alloca i32, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %parent, %struct.btCollisionObjectWrapper** %parent.addr, align 4, !tbaa !2
  store %class.btCollisionShape* %shape, %class.btCollisionShape** %shape.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %collisionObject, %class.btCollisionObject** %collisionObject.addr, align 4, !tbaa !2
  store %class.btTransform* %worldTransform, %class.btTransform** %worldTransform.addr, align 4, !tbaa !2
  store i32 %partId, i32* %partId.addr, align 4, !tbaa !29
  store i32 %index, i32* %index.addr, align 4, !tbaa !29
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_parent = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 0
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %parent.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %0, %struct.btCollisionObjectWrapper** %m_parent, align 4, !tbaa !41
  %m_shape = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 1
  %1 = load %class.btCollisionShape*, %class.btCollisionShape** %shape.addr, align 4, !tbaa !2
  store %class.btCollisionShape* %1, %class.btCollisionShape** %m_shape, align 4, !tbaa !21
  %m_collisionObject = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 2
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %2, %class.btCollisionObject** %m_collisionObject, align 4, !tbaa !39
  %m_worldTransform = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 3
  %3 = load %class.btTransform*, %class.btTransform** %worldTransform.addr, align 4, !tbaa !2
  store %class.btTransform* %3, %class.btTransform** %m_worldTransform, align 4, !tbaa !2
  %m_partId = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 4
  %4 = load i32, i32* %partId.addr, align 4, !tbaa !29
  store i32 %4, i32* %m_partId, align 4, !tbaa !42
  %m_index = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 5
  %5 = load i32, i32* %index.addr, align 4, !tbaa !29
  store i32 %5, i32* %m_index, align 4, !tbaa !43
  ret %struct.btCollisionObjectWrapper* %this1
}

define hidden void @_ZN28btCompoundCollisionAlgorithm21removeChildAlgorithmsEv(%class.btCompoundCollisionAlgorithm* %this) #0 {
entry:
  %this.addr = alloca %class.btCompoundCollisionAlgorithm*, align 4
  %numChildren = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btCompoundCollisionAlgorithm* %this, %class.btCompoundCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundCollisionAlgorithm*, %class.btCompoundCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast i32* %numChildren to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %m_childCollisionAlgorithms = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4sizeEv(%class.btAlignedObjectArray* %m_childCollisionAlgorithms)
  store i32 %call, i32* %numChildren, align 4, !tbaa !29
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store i32 0, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !29
  %3 = load i32, i32* %numChildren, align 4, !tbaa !29
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_childCollisionAlgorithms2 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %4 = load i32, i32* %i, align 4, !tbaa !29
  %call3 = call nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray* %m_childCollisionAlgorithms2, i32 %4)
  %5 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %call3, align 4, !tbaa !2
  %tobool = icmp ne %class.btCollisionAlgorithm* %5, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %m_childCollisionAlgorithms4 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %6 = load i32, i32* %i, align 4, !tbaa !29
  %call5 = call nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray* %m_childCollisionAlgorithms4, i32 %6)
  %7 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %call5, align 4, !tbaa !2
  %8 = bitcast %class.btCollisionAlgorithm* %7 to %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)***
  %vtable = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)**, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*** %8, align 4, !tbaa !8
  %vfn = getelementptr inbounds %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vtable, i64 0
  %9 = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vfn, align 4
  %call6 = call %class.btCollisionAlgorithm* %9(%class.btCollisionAlgorithm* %7) #8
  %10 = bitcast %class.btCompoundCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %m_dispatcher = getelementptr inbounds %class.btCollisionAlgorithm, %class.btCollisionAlgorithm* %10, i32 0, i32 1
  %11 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4, !tbaa !30
  %m_childCollisionAlgorithms7 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %12 = load i32, i32* %i, align 4, !tbaa !29
  %call8 = call nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray* %m_childCollisionAlgorithms7, i32 %12)
  %13 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %call8, align 4, !tbaa !2
  %14 = bitcast %class.btCollisionAlgorithm* %13 to i8*
  %15 = bitcast %class.btDispatcher* %11 to void (%class.btDispatcher*, i8*)***
  %vtable9 = load void (%class.btDispatcher*, i8*)**, void (%class.btDispatcher*, i8*)*** %15, align 4, !tbaa !8
  %vfn10 = getelementptr inbounds void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vtable9, i64 15
  %16 = load void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vfn10, align 4
  call void %16(%class.btDispatcher* %11, i8* %14)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %17 = load i32, i32* %i, align 4, !tbaa !29
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %18 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  %19 = bitcast i32* %numChildren to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4sizeEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !33
  ret i32 %0
}

; Function Attrs: nounwind
define hidden %class.btCompoundCollisionAlgorithm* @_ZN28btCompoundCollisionAlgorithmD2Ev(%class.btCompoundCollisionAlgorithm* returned %this) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btCompoundCollisionAlgorithm*, align 4
  store %class.btCompoundCollisionAlgorithm* %this, %class.btCompoundCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundCollisionAlgorithm*, %class.btCompoundCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast %class.btCompoundCollisionAlgorithm* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV28btCompoundCollisionAlgorithm, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !8
  call void @_ZN28btCompoundCollisionAlgorithm21removeChildAlgorithmsEv(%class.btCompoundCollisionAlgorithm* %this1)
  %m_childCollisionAlgorithms = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmED2Ev(%class.btAlignedObjectArray* %m_childCollisionAlgorithms) #8
  %1 = bitcast %class.btCompoundCollisionAlgorithm* %this1 to %class.btActivatingCollisionAlgorithm*
  %call2 = call %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmD2Ev(%class.btActivatingCollisionAlgorithm* %1) #8
  ret %class.btCompoundCollisionAlgorithm* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: nounwind
declare %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmD2Ev(%class.btActivatingCollisionAlgorithm* returned) unnamed_addr #6

; Function Attrs: nounwind
define hidden void @_ZN28btCompoundCollisionAlgorithmD0Ev(%class.btCompoundCollisionAlgorithm* %this) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btCompoundCollisionAlgorithm*, align 4
  store %class.btCompoundCollisionAlgorithm* %this, %class.btCompoundCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundCollisionAlgorithm*, %class.btCompoundCollisionAlgorithm** %this.addr, align 4
  %call = call %class.btCompoundCollisionAlgorithm* @_ZN28btCompoundCollisionAlgorithmD1Ev(%class.btCompoundCollisionAlgorithm* %this1) #8
  %0 = bitcast %class.btCompoundCollisionAlgorithm* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #7

define hidden void @_ZN28btCompoundCollisionAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResult(%class.btCompoundCollisionAlgorithm* %this, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo, %class.btManifoldResult* %resultOut) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btCompoundCollisionAlgorithm*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %resultOut.addr = alloca %class.btManifoldResult*, align 4
  %colObjWrap = alloca %struct.btCollisionObjectWrapper*, align 4
  %otherObjWrap = alloca %struct.btCollisionObjectWrapper*, align 4
  %compoundShape = alloca %class.btCompoundShape*, align 4
  %tree = alloca %struct.btDbvt*, align 4
  %callback = alloca %struct.btCompoundLeafCallback, align 4
  %i = alloca i32, align 4
  %manifoldArray = alloca %class.btAlignedObjectArray.13, align 4
  %m = alloca i32, align 4
  %ref.tmp = alloca %class.btPersistentManifold*, align 4
  %localAabbMin = alloca %class.btVector3, align 4
  %localAabbMax = alloca %class.btVector3, align 4
  %otherInCompoundSpace = alloca %class.btTransform, align 4
  %ref.tmp42 = alloca %class.btTransform, align 4
  %ref.tmp43 = alloca %class.btTransform, align 4
  %bounds = alloca %struct.btDbvtAabbMm, align 4
  %numChildren = alloca i32, align 4
  %i52 = alloca i32, align 4
  %numChildren61 = alloca i32, align 4
  %i64 = alloca i32, align 4
  %manifoldArray65 = alloca %class.btAlignedObjectArray.13, align 4
  %childShape = alloca %class.btCollisionShape*, align 4
  %orgTrans = alloca %class.btTransform, align 4
  %orgInterpolationTrans = alloca %class.btTransform, align 4
  %newChildWorldTrans = alloca %class.btTransform, align 4
  %aabbMin0 = alloca %class.btVector3, align 4
  %aabbMax0 = alloca %class.btVector3, align 4
  %aabbMin1 = alloca %class.btVector3, align 4
  %aabbMax1 = alloca %class.btVector3, align 4
  %childTrans = alloca %class.btTransform*, align 4
  %ref.tmp87 = alloca %class.btTransform, align 4
  store %class.btCompoundCollisionAlgorithm* %this, %class.btCompoundCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  store %class.btManifoldResult* %resultOut, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundCollisionAlgorithm*, %class.btCompoundCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast %struct.btCollisionObjectWrapper** %colObjWrap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %m_isSwapped = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  %1 = load i8, i8* %m_isSwapped, align 4, !tbaa !11, !range !10
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.btCollisionObjectWrapper* [ %2, %cond.true ], [ %3, %cond.false ]
  store %struct.btCollisionObjectWrapper* %cond, %struct.btCollisionObjectWrapper** %colObjWrap, align 4, !tbaa !2
  %4 = bitcast %struct.btCollisionObjectWrapper** %otherObjWrap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %m_isSwapped2 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  %5 = load i8, i8* %m_isSwapped2, align 4, !tbaa !11, !range !10
  %tobool3 = trunc i8 %5 to i1
  br i1 %tobool3, label %cond.true4, label %cond.false5

cond.true4:                                       ; preds = %cond.end
  %6 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  br label %cond.end6

cond.false5:                                      ; preds = %cond.end
  %7 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  br label %cond.end6

cond.end6:                                        ; preds = %cond.false5, %cond.true4
  %cond7 = phi %struct.btCollisionObjectWrapper* [ %6, %cond.true4 ], [ %7, %cond.false5 ]
  store %struct.btCollisionObjectWrapper* %cond7, %struct.btCollisionObjectWrapper** %otherObjWrap, align 4, !tbaa !2
  %8 = bitcast %class.btCompoundShape** %compoundShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObjWrap, align 4, !tbaa !2
  %call = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %9)
  %10 = bitcast %class.btCollisionShape* %call to %class.btCompoundShape*
  store %class.btCompoundShape* %10, %class.btCompoundShape** %compoundShape, align 4, !tbaa !2
  %11 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape, align 4, !tbaa !2
  %call8 = call i32 @_ZNK15btCompoundShape17getUpdateRevisionEv(%class.btCompoundShape* %11)
  %m_compoundShapeRevision = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 5
  %12 = load i32, i32* %m_compoundShapeRevision, align 4, !tbaa !20
  %cmp = icmp ne i32 %call8, %12
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end6
  call void @_ZN28btCompoundCollisionAlgorithm21removeChildAlgorithmsEv(%class.btCompoundCollisionAlgorithm* %this1)
  %13 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %14 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  call void @_ZN28btCompoundCollisionAlgorithm26preallocateChildAlgorithmsEPK24btCollisionObjectWrapperS2_(%class.btCompoundCollisionAlgorithm* %this1, %struct.btCollisionObjectWrapper* %13, %struct.btCollisionObjectWrapper* %14)
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end6
  %15 = bitcast %struct.btDbvt** %tree to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  %16 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape, align 4, !tbaa !2
  %call9 = call %struct.btDbvt* @_ZNK15btCompoundShape18getDynamicAabbTreeEv(%class.btCompoundShape* %16)
  store %struct.btDbvt* %call9, %struct.btDbvt** %tree, align 4, !tbaa !2
  %17 = bitcast %struct.btCompoundLeafCallback* %callback to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %17) #8
  %18 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObjWrap, align 4, !tbaa !2
  %19 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %otherObjWrap, align 4, !tbaa !2
  %20 = bitcast %class.btCompoundCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %m_dispatcher = getelementptr inbounds %class.btCollisionAlgorithm, %class.btCollisionAlgorithm* %20, i32 0, i32 1
  %21 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4, !tbaa !30
  %22 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  %23 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  %m_childCollisionAlgorithms = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %call10 = call nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray* %m_childCollisionAlgorithms, i32 0)
  %m_sharedManifold = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %24 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_sharedManifold, align 4, !tbaa !18
  %call11 = call %struct.btCompoundLeafCallback* @_ZN22btCompoundLeafCallbackC2EPK24btCollisionObjectWrapperS2_P12btDispatcherRK16btDispatcherInfoP16btManifoldResultPP20btCollisionAlgorithmP20btPersistentManifold(%struct.btCompoundLeafCallback* %callback, %struct.btCollisionObjectWrapper* %18, %struct.btCollisionObjectWrapper* %19, %class.btDispatcher* %21, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %22, %class.btManifoldResult* %23, %class.btCollisionAlgorithm** %call10, %class.btPersistentManifold* %24)
  %25 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  %26 = bitcast %class.btAlignedObjectArray.13* %manifoldArray to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %26) #8
  %call12 = call %class.btAlignedObjectArray.13* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray.13* %manifoldArray)
  store i32 0, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.cond:                                         ; preds = %for.inc33, %if.end
  %27 = load i32, i32* %i, align 4, !tbaa !29
  %m_childCollisionAlgorithms13 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %call14 = call i32 @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4sizeEv(%class.btAlignedObjectArray* %m_childCollisionAlgorithms13)
  %cmp15 = icmp slt i32 %27, %call14
  br i1 %cmp15, label %for.body, label %for.end35

for.body:                                         ; preds = %for.cond
  %m_childCollisionAlgorithms16 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %28 = load i32, i32* %i, align 4, !tbaa !29
  %call17 = call nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray* %m_childCollisionAlgorithms16, i32 %28)
  %29 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %call17, align 4, !tbaa !2
  %tobool18 = icmp ne %class.btCollisionAlgorithm* %29, null
  br i1 %tobool18, label %if.then19, label %if.end32

if.then19:                                        ; preds = %for.body
  %m_childCollisionAlgorithms20 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %30 = load i32, i32* %i, align 4, !tbaa !29
  %call21 = call nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray* %m_childCollisionAlgorithms20, i32 %30)
  %31 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %call21, align 4, !tbaa !2
  %32 = bitcast %class.btCollisionAlgorithm* %31 to void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.13*)***
  %vtable = load void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.13*)**, void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.13*)*** %32, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.13*)*, void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.13*)** %vtable, i64 4
  %33 = load void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.13*)*, void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.13*)** %vfn, align 4
  call void %33(%class.btCollisionAlgorithm* %31, %class.btAlignedObjectArray.13* nonnull align 4 dereferenceable(17) %manifoldArray)
  %34 = bitcast i32* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #8
  store i32 0, i32* %m, align 4, !tbaa !29
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc, %if.then19
  %35 = load i32, i32* %m, align 4, !tbaa !29
  %call23 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.13* %manifoldArray)
  %cmp24 = icmp slt i32 %35, %call23
  br i1 %cmp24, label %for.body25, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond22
  %36 = bitcast i32* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #8
  br label %for.end

for.body25:                                       ; preds = %for.cond22
  %37 = load i32, i32* %m, align 4, !tbaa !29
  %call26 = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.13* %manifoldArray, i32 %37)
  %38 = load %class.btPersistentManifold*, %class.btPersistentManifold** %call26, align 4, !tbaa !2
  %call27 = call i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %38)
  %tobool28 = icmp ne i32 %call27, 0
  br i1 %tobool28, label %if.then29, label %if.end31

if.then29:                                        ; preds = %for.body25
  %39 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  %40 = load i32, i32* %m, align 4, !tbaa !29
  %call30 = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.13* %manifoldArray, i32 %40)
  %41 = load %class.btPersistentManifold*, %class.btPersistentManifold** %call30, align 4, !tbaa !2
  call void @_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold(%class.btManifoldResult* %39, %class.btPersistentManifold* %41)
  %42 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  call void @_ZN16btManifoldResult20refreshContactPointsEv(%class.btManifoldResult* %42)
  %43 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  call void @_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold(%class.btManifoldResult* %43, %class.btPersistentManifold* null)
  br label %if.end31

if.end31:                                         ; preds = %if.then29, %for.body25
  br label %for.inc

for.inc:                                          ; preds = %if.end31
  %44 = load i32, i32* %m, align 4, !tbaa !29
  %inc = add nsw i32 %44, 1
  store i32 %inc, i32* %m, align 4, !tbaa !29
  br label %for.cond22

for.end:                                          ; preds = %for.cond.cleanup
  %45 = bitcast %class.btPersistentManifold** %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #8
  store %class.btPersistentManifold* null, %class.btPersistentManifold** %ref.tmp, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_(%class.btAlignedObjectArray.13* %manifoldArray, i32 0, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %ref.tmp)
  %46 = bitcast %class.btPersistentManifold** %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #8
  br label %if.end32

if.end32:                                         ; preds = %for.end, %for.body
  br label %for.inc33

for.inc33:                                        ; preds = %if.end32
  %47 = load i32, i32* %i, align 4, !tbaa !29
  %inc34 = add nsw i32 %47, 1
  store i32 %inc34, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.end35:                                        ; preds = %for.cond
  %call36 = call %class.btAlignedObjectArray.13* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray.13* %manifoldArray) #8
  %48 = bitcast %class.btAlignedObjectArray.13* %manifoldArray to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %48) #8
  %49 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #8
  %50 = load %struct.btDbvt*, %struct.btDbvt** %tree, align 4, !tbaa !2
  %tobool37 = icmp ne %struct.btDbvt* %50, null
  br i1 %tobool37, label %if.then38, label %if.else

if.then38:                                        ; preds = %for.end35
  %51 = bitcast %class.btVector3* %localAabbMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %51) #8
  %call39 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %localAabbMin)
  %52 = bitcast %class.btVector3* %localAabbMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %52) #8
  %call40 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %localAabbMax)
  %53 = bitcast %class.btTransform* %otherInCompoundSpace to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %53) #8
  %call41 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %otherInCompoundSpace)
  %54 = bitcast %class.btTransform* %ref.tmp42 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %54) #8
  %55 = bitcast %class.btTransform* %ref.tmp43 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %55) #8
  %56 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObjWrap, align 4, !tbaa !2
  %call44 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %56)
  call void @_ZNK11btTransform7inverseEv(%class.btTransform* sret align 4 %ref.tmp43, %class.btTransform* %call44)
  %57 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %otherObjWrap, align 4, !tbaa !2
  %call45 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %57)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp42, %class.btTransform* %ref.tmp43, %class.btTransform* nonnull align 4 dereferenceable(64) %call45)
  %call46 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %otherInCompoundSpace, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp42)
  %58 = bitcast %class.btTransform* %ref.tmp43 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %58) #8
  %59 = bitcast %class.btTransform* %ref.tmp42 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %59) #8
  %60 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %otherObjWrap, align 4, !tbaa !2
  %call47 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %60)
  %61 = bitcast %class.btCollisionShape* %call47 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable48 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %61, align 4, !tbaa !8
  %vfn49 = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable48, i64 2
  %62 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn49, align 4
  call void %62(%class.btCollisionShape* %call47, %class.btTransform* nonnull align 4 dereferenceable(64) %otherInCompoundSpace, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax)
  %63 = bitcast %struct.btDbvtAabbMm* %bounds to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %63) #8
  call void @_ZN12btDbvtAabbMm6FromMMERK9btVector3S2_(%struct.btDbvtAabbMm* sret align 4 %bounds, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax)
  %64 = load %struct.btDbvt*, %struct.btDbvt** %tree, align 4, !tbaa !2
  %65 = load %struct.btDbvt*, %struct.btDbvt** %tree, align 4, !tbaa !2
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %65, i32 0, i32 0
  %66 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root, align 4, !tbaa !44
  %67 = bitcast %struct.btCompoundLeafCallback* %callback to %"struct.btDbvt::ICollide"*
  call void @_ZNK6btDbvt9collideTVEPK10btDbvtNodeRK12btDbvtAabbMmRNS_8ICollideE(%struct.btDbvt* %64, %struct.btDbvtNode* %66, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %bounds, %"struct.btDbvt::ICollide"* nonnull align 4 dereferenceable(4) %67)
  %68 = bitcast %struct.btDbvtAabbMm* %bounds to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %68) #8
  %69 = bitcast %class.btTransform* %otherInCompoundSpace to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %69) #8
  %70 = bitcast %class.btVector3* %localAabbMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %70) #8
  %71 = bitcast %class.btVector3* %localAabbMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %71) #8
  br label %if.end60

if.else:                                          ; preds = %for.end35
  %72 = bitcast i32* %numChildren to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %72) #8
  %m_childCollisionAlgorithms50 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %call51 = call i32 @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4sizeEv(%class.btAlignedObjectArray* %m_childCollisionAlgorithms50)
  store i32 %call51, i32* %numChildren, align 4, !tbaa !29
  %73 = bitcast i32* %i52 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #8
  store i32 0, i32* %i52, align 4, !tbaa !29
  br label %for.cond53

for.cond53:                                       ; preds = %for.inc57, %if.else
  %74 = load i32, i32* %i52, align 4, !tbaa !29
  %75 = load i32, i32* %numChildren, align 4, !tbaa !29
  %cmp54 = icmp slt i32 %74, %75
  br i1 %cmp54, label %for.body55, label %for.end59

for.body55:                                       ; preds = %for.cond53
  %76 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape, align 4, !tbaa !2
  %77 = load i32, i32* %i52, align 4, !tbaa !29
  %call56 = call %class.btCollisionShape* @_ZNK15btCompoundShape13getChildShapeEi(%class.btCompoundShape* %76, i32 %77)
  %78 = load i32, i32* %i52, align 4, !tbaa !29
  call void @_ZN22btCompoundLeafCallback17ProcessChildShapeEPK16btCollisionShapei(%struct.btCompoundLeafCallback* %callback, %class.btCollisionShape* %call56, i32 %78)
  br label %for.inc57

for.inc57:                                        ; preds = %for.body55
  %79 = load i32, i32* %i52, align 4, !tbaa !29
  %inc58 = add nsw i32 %79, 1
  store i32 %inc58, i32* %i52, align 4, !tbaa !29
  br label %for.cond53

for.end59:                                        ; preds = %for.cond53
  %80 = bitcast i32* %i52 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #8
  %81 = bitcast i32* %numChildren to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #8
  br label %if.end60

if.end60:                                         ; preds = %for.end59, %if.then38
  %82 = bitcast i32* %numChildren61 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #8
  %m_childCollisionAlgorithms62 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %call63 = call i32 @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4sizeEv(%class.btAlignedObjectArray* %m_childCollisionAlgorithms62)
  store i32 %call63, i32* %numChildren61, align 4, !tbaa !29
  %83 = bitcast i32* %i64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %83) #8
  %84 = bitcast %class.btAlignedObjectArray.13* %manifoldArray65 to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %84) #8
  %call66 = call %class.btAlignedObjectArray.13* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray.13* %manifoldArray65)
  %85 = bitcast %class.btCollisionShape** %childShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %85) #8
  store %class.btCollisionShape* null, %class.btCollisionShape** %childShape, align 4, !tbaa !2
  %86 = bitcast %class.btTransform* %orgTrans to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %86) #8
  %call67 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %orgTrans)
  %87 = bitcast %class.btTransform* %orgInterpolationTrans to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %87) #8
  %call68 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %orgInterpolationTrans)
  %88 = bitcast %class.btTransform* %newChildWorldTrans to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %88) #8
  %call69 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %newChildWorldTrans)
  %89 = bitcast %class.btVector3* %aabbMin0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %89) #8
  %call70 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin0)
  %90 = bitcast %class.btVector3* %aabbMax0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %90) #8
  %call71 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax0)
  %91 = bitcast %class.btVector3* %aabbMin1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %91) #8
  %call72 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin1)
  %92 = bitcast %class.btVector3* %aabbMax1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %92) #8
  %call73 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax1)
  store i32 0, i32* %i64, align 4, !tbaa !29
  br label %for.cond74

for.cond74:                                       ; preds = %for.inc111, %if.end60
  %93 = load i32, i32* %i64, align 4, !tbaa !29
  %94 = load i32, i32* %numChildren61, align 4, !tbaa !29
  %cmp75 = icmp slt i32 %93, %94
  br i1 %cmp75, label %for.body76, label %for.end113

for.body76:                                       ; preds = %for.cond74
  %m_childCollisionAlgorithms77 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %95 = load i32, i32* %i64, align 4, !tbaa !29
  %call78 = call nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray* %m_childCollisionAlgorithms77, i32 %95)
  %96 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %call78, align 4, !tbaa !2
  %tobool79 = icmp ne %class.btCollisionAlgorithm* %96, null
  br i1 %tobool79, label %if.then80, label %if.end110

if.then80:                                        ; preds = %for.body76
  %97 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape, align 4, !tbaa !2
  %98 = load i32, i32* %i64, align 4, !tbaa !29
  %call81 = call %class.btCollisionShape* @_ZNK15btCompoundShape13getChildShapeEi(%class.btCompoundShape* %97, i32 %98)
  store %class.btCollisionShape* %call81, %class.btCollisionShape** %childShape, align 4, !tbaa !2
  %99 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObjWrap, align 4, !tbaa !2
  %call82 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %99)
  %call83 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %orgTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %call82)
  %100 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObjWrap, align 4, !tbaa !2
  %call84 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %100)
  %call85 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %orgInterpolationTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %call84)
  %101 = bitcast %class.btTransform** %childTrans to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %101) #8
  %102 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape, align 4, !tbaa !2
  %103 = load i32, i32* %i64, align 4, !tbaa !29
  %call86 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK15btCompoundShape17getChildTransformEi(%class.btCompoundShape* %102, i32 %103)
  store %class.btTransform* %call86, %class.btTransform** %childTrans, align 4, !tbaa !2
  %104 = bitcast %class.btTransform* %ref.tmp87 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %104) #8
  %105 = load %class.btTransform*, %class.btTransform** %childTrans, align 4, !tbaa !2
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp87, %class.btTransform* %orgTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %105)
  %call88 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %newChildWorldTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp87)
  %106 = bitcast %class.btTransform* %ref.tmp87 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %106) #8
  %107 = load %class.btCollisionShape*, %class.btCollisionShape** %childShape, align 4, !tbaa !2
  %108 = bitcast %class.btCollisionShape* %107 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable89 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %108, align 4, !tbaa !8
  %vfn90 = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable89, i64 2
  %109 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn90, align 4
  call void %109(%class.btCollisionShape* %107, %class.btTransform* nonnull align 4 dereferenceable(64) %newChildWorldTrans, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin0, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax0)
  %110 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %otherObjWrap, align 4, !tbaa !2
  %call91 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %110)
  %111 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %otherObjWrap, align 4, !tbaa !2
  %call92 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %111)
  %112 = bitcast %class.btCollisionShape* %call91 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable93 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %112, align 4, !tbaa !8
  %vfn94 = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable93, i64 2
  %113 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn94, align 4
  call void %113(%class.btCollisionShape* %call91, %class.btTransform* nonnull align 4 dereferenceable(64) %call92, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax1)
  %call95 = call zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin0, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax0, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax1)
  br i1 %call95, label %if.end109, label %if.then96

if.then96:                                        ; preds = %if.then80
  %m_childCollisionAlgorithms97 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %114 = load i32, i32* %i64, align 4, !tbaa !29
  %call98 = call nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray* %m_childCollisionAlgorithms97, i32 %114)
  %115 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %call98, align 4, !tbaa !2
  %116 = bitcast %class.btCollisionAlgorithm* %115 to %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)***
  %vtable99 = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)**, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*** %116, align 4, !tbaa !8
  %vfn100 = getelementptr inbounds %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vtable99, i64 0
  %117 = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vfn100, align 4
  %call101 = call %class.btCollisionAlgorithm* %117(%class.btCollisionAlgorithm* %115) #8
  %118 = bitcast %class.btCompoundCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %m_dispatcher102 = getelementptr inbounds %class.btCollisionAlgorithm, %class.btCollisionAlgorithm* %118, i32 0, i32 1
  %119 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher102, align 4, !tbaa !30
  %m_childCollisionAlgorithms103 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %120 = load i32, i32* %i64, align 4, !tbaa !29
  %call104 = call nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray* %m_childCollisionAlgorithms103, i32 %120)
  %121 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %call104, align 4, !tbaa !2
  %122 = bitcast %class.btCollisionAlgorithm* %121 to i8*
  %123 = bitcast %class.btDispatcher* %119 to void (%class.btDispatcher*, i8*)***
  %vtable105 = load void (%class.btDispatcher*, i8*)**, void (%class.btDispatcher*, i8*)*** %123, align 4, !tbaa !8
  %vfn106 = getelementptr inbounds void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vtable105, i64 15
  %124 = load void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vfn106, align 4
  call void %124(%class.btDispatcher* %119, i8* %122)
  %m_childCollisionAlgorithms107 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %125 = load i32, i32* %i64, align 4, !tbaa !29
  %call108 = call nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray* %m_childCollisionAlgorithms107, i32 %125)
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %call108, align 4, !tbaa !2
  br label %if.end109

if.end109:                                        ; preds = %if.then96, %if.then80
  %126 = bitcast %class.btTransform** %childTrans to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #8
  br label %if.end110

if.end110:                                        ; preds = %if.end109, %for.body76
  br label %for.inc111

for.inc111:                                       ; preds = %if.end110
  %127 = load i32, i32* %i64, align 4, !tbaa !29
  %inc112 = add nsw i32 %127, 1
  store i32 %inc112, i32* %i64, align 4, !tbaa !29
  br label %for.cond74

for.end113:                                       ; preds = %for.cond74
  %128 = bitcast %class.btVector3* %aabbMax1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %128) #8
  %129 = bitcast %class.btVector3* %aabbMin1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %129) #8
  %130 = bitcast %class.btVector3* %aabbMax0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %130) #8
  %131 = bitcast %class.btVector3* %aabbMin0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %131) #8
  %132 = bitcast %class.btTransform* %newChildWorldTrans to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %132) #8
  %133 = bitcast %class.btTransform* %orgInterpolationTrans to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %133) #8
  %134 = bitcast %class.btTransform* %orgTrans to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %134) #8
  %135 = bitcast %class.btCollisionShape** %childShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %135) #8
  %call114 = call %class.btAlignedObjectArray.13* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray.13* %manifoldArray65) #8
  %136 = bitcast %class.btAlignedObjectArray.13* %manifoldArray65 to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %136) #8
  %137 = bitcast i32* %i64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %137) #8
  %138 = bitcast i32* %numChildren61 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %138) #8
  %call115 = call %struct.btCompoundLeafCallback* bitcast (%"struct.btDbvt::ICollide"* (%"struct.btDbvt::ICollide"*)* @_ZN6btDbvt8ICollideD2Ev to %struct.btCompoundLeafCallback* (%struct.btCompoundLeafCallback*)*)(%struct.btCompoundLeafCallback* %callback) #8
  %139 = bitcast %struct.btCompoundLeafCallback* %callback to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %139) #8
  %140 = bitcast %struct.btDbvt** %tree to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #8
  %141 = bitcast %class.btCompoundShape** %compoundShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #8
  %142 = bitcast %struct.btCollisionObjectWrapper** %otherObjWrap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #8
  %143 = bitcast %struct.btCollisionObjectWrapper** %colObjWrap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btCompoundLeafCallback* @_ZN22btCompoundLeafCallbackC2EPK24btCollisionObjectWrapperS2_P12btDispatcherRK16btDispatcherInfoP16btManifoldResultPP20btCollisionAlgorithmP20btPersistentManifold(%struct.btCompoundLeafCallback* returned %this, %struct.btCollisionObjectWrapper* %compoundObjWrap, %struct.btCollisionObjectWrapper* %otherObjWrap, %class.btDispatcher* %dispatcher, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo, %class.btManifoldResult* %resultOut, %class.btCollisionAlgorithm** %childCollisionAlgorithms, %class.btPersistentManifold* %sharedManifold) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %struct.btCompoundLeafCallback*, align 4
  %compoundObjWrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %otherObjWrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %resultOut.addr = alloca %class.btManifoldResult*, align 4
  %childCollisionAlgorithms.addr = alloca %class.btCollisionAlgorithm**, align 4
  %sharedManifold.addr = alloca %class.btPersistentManifold*, align 4
  store %struct.btCompoundLeafCallback* %this, %struct.btCompoundLeafCallback** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %compoundObjWrap, %struct.btCollisionObjectWrapper** %compoundObjWrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %otherObjWrap, %struct.btCollisionObjectWrapper** %otherObjWrap.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  store %class.btManifoldResult* %resultOut, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  store %class.btCollisionAlgorithm** %childCollisionAlgorithms, %class.btCollisionAlgorithm*** %childCollisionAlgorithms.addr, align 4, !tbaa !2
  store %class.btPersistentManifold* %sharedManifold, %class.btPersistentManifold** %sharedManifold.addr, align 4, !tbaa !2
  %this1 = load %struct.btCompoundLeafCallback*, %struct.btCompoundLeafCallback** %this.addr, align 4
  %0 = bitcast %struct.btCompoundLeafCallback* %this1 to %"struct.btDbvt::ICollide"*
  %call = call %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideC2Ev(%"struct.btDbvt::ICollide"* %0) #8
  %1 = bitcast %struct.btCompoundLeafCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV22btCompoundLeafCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !8
  %m_compoundColObjWrap = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 1
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %compoundObjWrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %2, %struct.btCollisionObjectWrapper** %m_compoundColObjWrap, align 4, !tbaa !50
  %m_otherObjWrap = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 2
  %3 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %otherObjWrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %3, %struct.btCollisionObjectWrapper** %m_otherObjWrap, align 4, !tbaa !52
  %m_dispatcher = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 3
  %4 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  store %class.btDispatcher* %4, %class.btDispatcher** %m_dispatcher, align 4, !tbaa !53
  %m_dispatchInfo = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 4
  %5 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  store %struct.btDispatcherInfo* %5, %struct.btDispatcherInfo** %m_dispatchInfo, align 4, !tbaa !2
  %m_resultOut = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 5
  %6 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  store %class.btManifoldResult* %6, %class.btManifoldResult** %m_resultOut, align 4, !tbaa !54
  %m_childCollisionAlgorithms = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 6
  %7 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %childCollisionAlgorithms.addr, align 4, !tbaa !2
  store %class.btCollisionAlgorithm** %7, %class.btCollisionAlgorithm*** %m_childCollisionAlgorithms, align 4, !tbaa !55
  %m_sharedManifold = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 7
  %8 = load %class.btPersistentManifold*, %class.btPersistentManifold** %sharedManifold.addr, align 4, !tbaa !2
  store %class.btPersistentManifold* %8, %class.btPersistentManifold** %m_sharedManifold, align 4, !tbaa !56
  ret %struct.btCompoundLeafCallback* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.13* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray.13* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.14* @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev(%class.btAlignedAllocator.14* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.13* %this1)
  ret %class.btAlignedObjectArray.13* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.13* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !57
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.13* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !29
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !60
  %1 = load i32, i32* %n.addr, align 4, !tbaa !29
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %0, i32 %1
  ret %class.btPersistentManifold** %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_cachedPoints = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 4
  %0 = load i32, i32* %m_cachedPoints, align 4, !tbaa !61
  ret i32 %0
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold(%class.btManifoldResult* %this, %class.btPersistentManifold* %manifoldPtr) #4 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %manifoldPtr.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4, !tbaa !2
  store %class.btPersistentManifold* %manifoldPtr, %class.btPersistentManifold** %manifoldPtr.addr, align 4, !tbaa !2
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifoldPtr.addr, align 4, !tbaa !2
  %m_manifoldPtr = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  store %class.btPersistentManifold* %0, %class.btPersistentManifold** %m_manifoldPtr, align 4, !tbaa !63
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN16btManifoldResult20refreshContactPointsEv(%class.btManifoldResult* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %isSwapped = alloca i8, align 1
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %m_manifoldPtr = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr, align 4, !tbaa !63
  %call = call i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %isSwapped) #8
  %m_manifoldPtr2 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr2, align 4, !tbaa !63
  %call3 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %1)
  %m_body0Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap, align 4, !tbaa !65
  %call4 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %2)
  %cmp = icmp ne %class.btCollisionObject* %call3, %call4
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %isSwapped, align 1, !tbaa !6
  %3 = load i8, i8* %isSwapped, align 1, !tbaa !6, !range !10
  %tobool5 = trunc i8 %3 to i1
  br i1 %tobool5, label %if.then6, label %if.else

if.then6:                                         ; preds = %if.end
  %m_manifoldPtr7 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %4 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr7, align 4, !tbaa !63
  %m_body1Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  %5 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body1Wrap, align 4, !tbaa !66
  %call8 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %5)
  %call9 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call8)
  %m_body0Wrap10 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %6 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap10, align 4, !tbaa !65
  %call11 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %6)
  %call12 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call11)
  call void @_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_(%class.btPersistentManifold* %4, %class.btTransform* nonnull align 4 dereferenceable(64) %call9, %class.btTransform* nonnull align 4 dereferenceable(64) %call12)
  br label %if.end20

if.else:                                          ; preds = %if.end
  %m_manifoldPtr13 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %7 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr13, align 4, !tbaa !63
  %m_body0Wrap14 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap14, align 4, !tbaa !65
  %call15 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %8)
  %call16 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call15)
  %m_body1Wrap17 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body1Wrap17, align 4, !tbaa !66
  %call18 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %9)
  %call19 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call18)
  call void @_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_(%class.btPersistentManifold* %7, %class.btTransform* nonnull align 4 dereferenceable(64) %call16, %class.btTransform* nonnull align 4 dereferenceable(64) %call19)
  br label %if.end20

if.end20:                                         ; preds = %if.else, %if.then6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %isSwapped) #8
  br label %return

return:                                           ; preds = %if.end20, %if.then
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_(%class.btAlignedObjectArray.13* %this, i32 %newsize, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %fillData) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btPersistentManifold**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !29
  store %class.btPersistentManifold** %fillData, %class.btPersistentManifold*** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.13* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !29
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !29
  %2 = load i32, i32* %curSize, align 4, !tbaa !29
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !29
  store i32 %4, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !29
  %6 = load i32, i32* %curSize, align 4, !tbaa !29
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  %8 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !60
  %9 = load i32, i32* %i, align 4, !tbaa !29
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !29
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !29
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.13* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !29
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.13* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load i32, i32* %curSize, align 4, !tbaa !29
  store i32 %14, i32* %i6, align 4, !tbaa !29
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !29
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !29
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  %18 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data11, align 4, !tbaa !60
  %19 = load i32, i32* %i6, align 4, !tbaa !29
  %arrayidx12 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %18, i32 %19
  %20 = bitcast %class.btPersistentManifold** %arrayidx12 to i8*
  %21 = bitcast i8* %20 to %class.btPersistentManifold**
  %22 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %fillData.addr, align 4, !tbaa !2
  %23 = load %class.btPersistentManifold*, %class.btPersistentManifold** %22, align 4, !tbaa !2
  store %class.btPersistentManifold* %23, %class.btPersistentManifold** %21, align 4, !tbaa !2
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %24 = load i32, i32* %i6, align 4, !tbaa !29
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !29
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %25 = load i32, i32* %newsize.addr, align 4, !tbaa !29
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 2
  store i32 %25, i32* %m_size, align 4, !tbaa !57
  %26 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.13* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray.13* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv(%class.btAlignedObjectArray.13* %this1)
  ret %class.btAlignedObjectArray.13* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

define linkonce_odr hidden void @_ZNK11btTransform7inverseEv(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %inv = alloca %class.btMatrix3x3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %1 = bitcast %class.btMatrix3x3* %inv to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %1) #8
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %inv, %class.btMatrix3x3* %m_basis)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #8
  %3 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #8
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %4 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #8
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #8
  %6 = bitcast %class.btMatrix3x3* %inv to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %6) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformmlERKS_(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t) #5 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %1 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %1) #8
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %2 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %2, i32 0, i32 0
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %3 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #8
  %4 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %4, i32 0, i32 1
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp3, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  %5 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #8
  %6 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %6) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #5 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !67
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN12btDbvtAabbMm6FromMMERK9btVector3S2_(%struct.btDbvtAabbMm* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %mi, %class.btVector3* nonnull align 4 dereferenceable(16) %mx) #5 comdat {
entry:
  %mi.addr = alloca %class.btVector3*, align 4
  %mx.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %mi, %class.btVector3** %mi.addr, align 4, !tbaa !2
  store %class.btVector3* %mx, %class.btVector3** %mx.addr, align 4, !tbaa !2
  %call = call %struct.btDbvtAabbMm* @_ZN12btDbvtAabbMmC2Ev(%struct.btDbvtAabbMm* %agg.result)
  %0 = load %class.btVector3*, %class.btVector3** %mi.addr, align 4, !tbaa !2
  %mi1 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %agg.result, i32 0, i32 0
  %1 = bitcast %class.btVector3* %mi1 to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !67
  %3 = load %class.btVector3*, %class.btVector3** %mx.addr, align 4, !tbaa !2
  %mx2 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %agg.result, i32 0, i32 1
  %4 = bitcast %class.btVector3* %mx2 to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !67
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK6btDbvt9collideTVEPK10btDbvtNodeRK12btDbvtAabbMmRNS_8ICollideE(%struct.btDbvt* %this, %struct.btDbvtNode* %root, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %vol, %"struct.btDbvt::ICollide"* nonnull align 4 dereferenceable(4) %policy) #5 comdat {
entry:
  %this.addr = alloca %struct.btDbvt*, align 4
  %root.addr = alloca %struct.btDbvtNode*, align 4
  %vol.addr = alloca %struct.btDbvtAabbMm*, align 4
  %policy.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %volume = alloca %struct.btDbvtAabbMm, align 4
  %stack = alloca %class.btAlignedObjectArray.9, align 4
  %ref.tmp = alloca %struct.btDbvtNode*, align 4
  %n = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvt* %this, %struct.btDbvt** %this.addr, align 4, !tbaa !2
  store %struct.btDbvtNode* %root, %struct.btDbvtNode** %root.addr, align 4, !tbaa !2
  store %struct.btDbvtAabbMm* %vol, %struct.btDbvtAabbMm** %vol.addr, align 4, !tbaa !2
  store %"struct.btDbvt::ICollide"* %policy, %"struct.btDbvt::ICollide"** %policy.addr, align 4, !tbaa !2
  %this1 = load %struct.btDbvt*, %struct.btDbvt** %this.addr, align 4
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.btDbvtNode* %0, null
  br i1 %tobool, label %if.then, label %if.end14

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btDbvtAabbMm* %volume to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %1) #8
  %2 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %vol.addr, align 4, !tbaa !2
  %3 = bitcast %struct.btDbvtAabbMm* %volume to i8*
  %4 = bitcast %struct.btDbvtAabbMm* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 32, i1 false), !tbaa.struct !69
  %5 = bitcast %class.btAlignedObjectArray.9* %stack to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %5) #8
  %call = call %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeEC2Ev(%class.btAlignedObjectArray.9* %stack)
  %6 = bitcast %struct.btDbvtNode** %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  store %struct.btDbvtNode* null, %struct.btDbvtNode** %ref.tmp, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE6resizeEiRKS2_(%class.btAlignedObjectArray.9* %stack, i32 0, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %ref.tmp)
  %7 = bitcast %struct.btDbvtNode** %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7reserveEi(%class.btAlignedObjectArray.9* %stack, i32 64)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9push_backERKS2_(%class.btAlignedObjectArray.9* %stack, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %root.addr)
  br label %do.body

do.body:                                          ; preds = %do.cond, %if.then
  %8 = bitcast %struct.btDbvtNode** %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.9* %stack)
  %sub = sub nsw i32 %call2, 1
  %call3 = call nonnull align 4 dereferenceable(4) %struct.btDbvtNode** @_ZN20btAlignedObjectArrayIPK10btDbvtNodeEixEi(%class.btAlignedObjectArray.9* %stack, i32 %sub)
  %9 = load %struct.btDbvtNode*, %struct.btDbvtNode** %call3, align 4, !tbaa !2
  store %struct.btDbvtNode* %9, %struct.btDbvtNode** %n, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE8pop_backEv(%class.btAlignedObjectArray.9* %stack)
  %10 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n, align 4, !tbaa !2
  %volume4 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %10, i32 0, i32 0
  %call5 = call zeroext i1 @_Z9IntersectRK12btDbvtAabbMmS1_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume4, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume)
  br i1 %call5, label %if.then6, label %if.end11

if.then6:                                         ; preds = %do.body
  %11 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n, align 4, !tbaa !2
  %call7 = call zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %11)
  br i1 %call7, label %if.then8, label %if.else

if.then8:                                         ; preds = %if.then6
  %12 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n, align 4, !tbaa !2
  %13 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %12, i32 0, i32 2
  %childs = bitcast %union.anon.3* %13 to [2 x %struct.btDbvtNode*]*
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 0
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9push_backERKS2_(%class.btAlignedObjectArray.9* %stack, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %arrayidx)
  %14 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n, align 4, !tbaa !2
  %15 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %14, i32 0, i32 2
  %childs9 = bitcast %union.anon.3* %15 to [2 x %struct.btDbvtNode*]*
  %arrayidx10 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs9, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9push_backERKS2_(%class.btAlignedObjectArray.9* %stack, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %arrayidx10)
  br label %if.end

if.else:                                          ; preds = %if.then6
  %16 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %policy.addr, align 4, !tbaa !2
  %17 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n, align 4, !tbaa !2
  %18 = bitcast %"struct.btDbvt::ICollide"* %16 to void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)***
  %vtable = load void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)**, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*** %18, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)** %vtable, i64 3
  %19 = load void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)** %vfn, align 4
  call void %19(%"struct.btDbvt::ICollide"* %16, %struct.btDbvtNode* %17)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then8
  br label %if.end11

if.end11:                                         ; preds = %if.end, %do.body
  %20 = bitcast %struct.btDbvtNode** %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #8
  br label %do.cond

do.cond:                                          ; preds = %if.end11
  %call12 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.9* %stack)
  %cmp = icmp sgt i32 %call12, 0
  br i1 %cmp, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %call13 = call %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeED2Ev(%class.btAlignedObjectArray.9* %stack) #8
  %21 = bitcast %class.btAlignedObjectArray.9* %stack to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %21) #8
  %22 = bitcast %struct.btDbvtAabbMm* %volume to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %22) #8
  br label %if.end14

if.end14:                                         ; preds = %do.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN22btCompoundLeafCallback17ProcessChildShapeEPK16btCollisionShapei(%struct.btCompoundLeafCallback* %this, %class.btCollisionShape* %childShape, i32 %index) #0 comdat {
entry:
  %this.addr = alloca %struct.btCompoundLeafCallback*, align 4
  %childShape.addr = alloca %class.btCollisionShape*, align 4
  %index.addr = alloca i32, align 4
  %compoundShape = alloca %class.btCompoundShape*, align 4
  %orgTrans = alloca %class.btTransform, align 4
  %orgInterpolationTrans = alloca %class.btTransform, align 4
  %childTrans = alloca %class.btTransform*, align 4
  %newChildWorldTrans = alloca %class.btTransform, align 4
  %aabbMin0 = alloca %class.btVector3, align 4
  %aabbMax0 = alloca %class.btVector3, align 4
  %aabbMin1 = alloca %class.btVector3, align 4
  %aabbMax1 = alloca %class.btVector3, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %compoundWrap = alloca %struct.btCollisionObjectWrapper, align 4
  %tmpWrap = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCompoundLeafCallback* %this, %struct.btCompoundLeafCallback** %this.addr, align 4, !tbaa !2
  store %class.btCollisionShape* %childShape, %class.btCollisionShape** %childShape.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !29
  %this1 = load %struct.btCompoundLeafCallback*, %struct.btCompoundLeafCallback** %this.addr, align 4
  %0 = bitcast %class.btCompoundShape** %compoundShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %m_compoundColObjWrap = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 1
  %1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compoundColObjWrap, align 4, !tbaa !50
  %call = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %1)
  %2 = bitcast %class.btCollisionShape* %call to %class.btCompoundShape*
  store %class.btCompoundShape* %2, %class.btCompoundShape** %compoundShape, align 4, !tbaa !2
  %3 = bitcast %class.btTransform* %orgTrans to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %3) #8
  %m_compoundColObjWrap2 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 1
  %4 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compoundColObjWrap2, align 4, !tbaa !50
  %call3 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %4)
  %call4 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %orgTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %call3)
  %5 = bitcast %class.btTransform* %orgInterpolationTrans to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %5) #8
  %m_compoundColObjWrap5 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 1
  %6 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compoundColObjWrap5, align 4, !tbaa !50
  %call6 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %6)
  %call7 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %orgInterpolationTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %call6)
  %7 = bitcast %class.btTransform** %childTrans to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %8 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape, align 4, !tbaa !2
  %9 = load i32, i32* %index.addr, align 4, !tbaa !29
  %call8 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK15btCompoundShape17getChildTransformEi(%class.btCompoundShape* %8, i32 %9)
  store %class.btTransform* %call8, %class.btTransform** %childTrans, align 4, !tbaa !2
  %10 = bitcast %class.btTransform* %newChildWorldTrans to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %10) #8
  %11 = load %class.btTransform*, %class.btTransform** %childTrans, align 4, !tbaa !2
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %newChildWorldTrans, %class.btTransform* %orgTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %11)
  %12 = bitcast %class.btVector3* %aabbMin0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %12) #8
  %call9 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin0)
  %13 = bitcast %class.btVector3* %aabbMax0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #8
  %call10 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax0)
  %14 = bitcast %class.btVector3* %aabbMin1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %14) #8
  %call11 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin1)
  %15 = bitcast %class.btVector3* %aabbMax1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %15) #8
  %call12 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax1)
  %16 = load %class.btCollisionShape*, %class.btCollisionShape** %childShape.addr, align 4, !tbaa !2
  %17 = bitcast %class.btCollisionShape* %16 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %17, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %18 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %18(%class.btCollisionShape* %16, %class.btTransform* nonnull align 4 dereferenceable(64) %newChildWorldTrans, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin0, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax0)
  %m_otherObjWrap = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 2
  %19 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_otherObjWrap, align 4, !tbaa !52
  %call13 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %19)
  %m_otherObjWrap14 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 2
  %20 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_otherObjWrap14, align 4, !tbaa !52
  %call15 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %20)
  %21 = bitcast %class.btCollisionShape* %call13 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable16 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %21, align 4, !tbaa !8
  %vfn17 = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable16, i64 2
  %22 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn17, align 4
  call void %22(%class.btCollisionShape* %call13, %class.btTransform* nonnull align 4 dereferenceable(64) %call15, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax1)
  %23 = load i1 (%class.btCollisionShape*, %class.btCollisionShape*)*, i1 (%class.btCollisionShape*, %class.btCollisionShape*)** @gCompoundChildShapePairCallback, align 4, !tbaa !2
  %tobool = icmp ne i1 (%class.btCollisionShape*, %class.btCollisionShape*)* %23, null
  br i1 %tobool, label %if.then, label %if.end22

if.then:                                          ; preds = %entry
  %24 = load i1 (%class.btCollisionShape*, %class.btCollisionShape*)*, i1 (%class.btCollisionShape*, %class.btCollisionShape*)** @gCompoundChildShapePairCallback, align 4, !tbaa !2
  %m_otherObjWrap18 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 2
  %25 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_otherObjWrap18, align 4, !tbaa !52
  %call19 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %25)
  %26 = load %class.btCollisionShape*, %class.btCollisionShape** %childShape.addr, align 4, !tbaa !2
  %call20 = call zeroext i1 %24(%class.btCollisionShape* %call19, %class.btCollisionShape* %26)
  br i1 %call20, label %if.end, label %if.then21

if.then21:                                        ; preds = %if.then
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  br label %if.end22

if.end22:                                         ; preds = %if.end, %entry
  %call23 = call zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin0, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax0, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax1)
  br i1 %call23, label %if.then24, label %if.end71

if.then24:                                        ; preds = %if.end22
  %27 = bitcast %struct.btCollisionObjectWrapper* %compoundWrap to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %27) #8
  %m_compoundColObjWrap25 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 1
  %28 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compoundColObjWrap25, align 4, !tbaa !50
  %29 = load %class.btCollisionShape*, %class.btCollisionShape** %childShape.addr, align 4, !tbaa !2
  %m_compoundColObjWrap26 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 1
  %30 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compoundColObjWrap26, align 4, !tbaa !50
  %call27 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %30)
  %31 = load i32, i32* %index.addr, align 4, !tbaa !29
  %call28 = call %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* %compoundWrap, %struct.btCollisionObjectWrapper* %28, %class.btCollisionShape* %29, %class.btCollisionObject* %call27, %class.btTransform* nonnull align 4 dereferenceable(64) %newChildWorldTrans, i32 -1, i32 %31)
  %m_childCollisionAlgorithms = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 6
  %32 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %m_childCollisionAlgorithms, align 4, !tbaa !55
  %33 = load i32, i32* %index.addr, align 4, !tbaa !29
  %arrayidx = getelementptr inbounds %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %32, i32 %33
  %34 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %arrayidx, align 4, !tbaa !2
  %tobool29 = icmp ne %class.btCollisionAlgorithm* %34, null
  br i1 %tobool29, label %if.end37, label %if.then30

if.then30:                                        ; preds = %if.then24
  %m_dispatcher = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 3
  %35 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4, !tbaa !53
  %m_otherObjWrap31 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 2
  %36 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_otherObjWrap31, align 4, !tbaa !52
  %m_sharedManifold = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 7
  %37 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_sharedManifold, align 4, !tbaa !56
  %38 = bitcast %class.btDispatcher* %35 to %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)***
  %vtable32 = load %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)**, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)*** %38, align 4, !tbaa !8
  %vfn33 = getelementptr inbounds %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)*, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)** %vtable32, i64 2
  %39 = load %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)*, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)** %vfn33, align 4
  %call34 = call %class.btCollisionAlgorithm* %39(%class.btDispatcher* %35, %struct.btCollisionObjectWrapper* %compoundWrap, %struct.btCollisionObjectWrapper* %36, %class.btPersistentManifold* %37)
  %m_childCollisionAlgorithms35 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 6
  %40 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %m_childCollisionAlgorithms35, align 4, !tbaa !55
  %41 = load i32, i32* %index.addr, align 4, !tbaa !29
  %arrayidx36 = getelementptr inbounds %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %40, i32 %41
  store %class.btCollisionAlgorithm* %call34, %class.btCollisionAlgorithm** %arrayidx36, align 4, !tbaa !2
  br label %if.end37

if.end37:                                         ; preds = %if.then30, %if.then24
  %42 = bitcast %struct.btCollisionObjectWrapper** %tmpWrap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #8
  store %struct.btCollisionObjectWrapper* null, %struct.btCollisionObjectWrapper** %tmpWrap, align 4, !tbaa !2
  %m_resultOut = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 5
  %43 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut, align 4, !tbaa !54
  %call38 = call %class.btCollisionObject* @_ZNK16btManifoldResult16getBody0InternalEv(%class.btManifoldResult* %43)
  %m_compoundColObjWrap39 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 1
  %44 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compoundColObjWrap39, align 4, !tbaa !50
  %call40 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %44)
  %cmp = icmp eq %class.btCollisionObject* %call38, %call40
  br i1 %cmp, label %if.then41, label %if.else

if.then41:                                        ; preds = %if.end37
  %m_resultOut42 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 5
  %45 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut42, align 4, !tbaa !54
  %call43 = call %struct.btCollisionObjectWrapper* @_ZNK16btManifoldResult12getBody0WrapEv(%class.btManifoldResult* %45)
  store %struct.btCollisionObjectWrapper* %call43, %struct.btCollisionObjectWrapper** %tmpWrap, align 4, !tbaa !2
  %m_resultOut44 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 5
  %46 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut44, align 4, !tbaa !54
  call void @_ZN16btManifoldResult12setBody0WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %46, %struct.btCollisionObjectWrapper* %compoundWrap)
  %m_resultOut45 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 5
  %47 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut45, align 4, !tbaa !54
  %48 = load i32, i32* %index.addr, align 4, !tbaa !29
  %49 = bitcast %class.btManifoldResult* %47 to void (%class.btManifoldResult*, i32, i32)***
  %vtable46 = load void (%class.btManifoldResult*, i32, i32)**, void (%class.btManifoldResult*, i32, i32)*** %49, align 4, !tbaa !8
  %vfn47 = getelementptr inbounds void (%class.btManifoldResult*, i32, i32)*, void (%class.btManifoldResult*, i32, i32)** %vtable46, i64 2
  %50 = load void (%class.btManifoldResult*, i32, i32)*, void (%class.btManifoldResult*, i32, i32)** %vfn47, align 4
  call void %50(%class.btManifoldResult* %47, i32 -1, i32 %48)
  br label %if.end54

if.else:                                          ; preds = %if.end37
  %m_resultOut48 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 5
  %51 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut48, align 4, !tbaa !54
  %call49 = call %struct.btCollisionObjectWrapper* @_ZNK16btManifoldResult12getBody1WrapEv(%class.btManifoldResult* %51)
  store %struct.btCollisionObjectWrapper* %call49, %struct.btCollisionObjectWrapper** %tmpWrap, align 4, !tbaa !2
  %m_resultOut50 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 5
  %52 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut50, align 4, !tbaa !54
  call void @_ZN16btManifoldResult12setBody1WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %52, %struct.btCollisionObjectWrapper* %compoundWrap)
  %m_resultOut51 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 5
  %53 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut51, align 4, !tbaa !54
  %54 = load i32, i32* %index.addr, align 4, !tbaa !29
  %55 = bitcast %class.btManifoldResult* %53 to void (%class.btManifoldResult*, i32, i32)***
  %vtable52 = load void (%class.btManifoldResult*, i32, i32)**, void (%class.btManifoldResult*, i32, i32)*** %55, align 4, !tbaa !8
  %vfn53 = getelementptr inbounds void (%class.btManifoldResult*, i32, i32)*, void (%class.btManifoldResult*, i32, i32)** %vtable52, i64 3
  %56 = load void (%class.btManifoldResult*, i32, i32)*, void (%class.btManifoldResult*, i32, i32)** %vfn53, align 4
  call void %56(%class.btManifoldResult* %53, i32 -1, i32 %54)
  br label %if.end54

if.end54:                                         ; preds = %if.else, %if.then41
  %m_childCollisionAlgorithms55 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 6
  %57 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %m_childCollisionAlgorithms55, align 4, !tbaa !55
  %58 = load i32, i32* %index.addr, align 4, !tbaa !29
  %arrayidx56 = getelementptr inbounds %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %57, i32 %58
  %59 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %arrayidx56, align 4, !tbaa !2
  %m_otherObjWrap57 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 2
  %60 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_otherObjWrap57, align 4, !tbaa !52
  %m_dispatchInfo = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 4
  %61 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %m_dispatchInfo, align 4, !tbaa !70
  %m_resultOut58 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 5
  %62 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut58, align 4, !tbaa !54
  %63 = bitcast %class.btCollisionAlgorithm* %59 to void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)***
  %vtable59 = load void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)**, void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*** %63, align 4, !tbaa !8
  %vfn60 = getelementptr inbounds void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*, void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)** %vtable59, i64 2
  %64 = load void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*, void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)** %vfn60, align 4
  call void %64(%class.btCollisionAlgorithm* %59, %struct.btCollisionObjectWrapper* %compoundWrap, %struct.btCollisionObjectWrapper* %60, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %61, %class.btManifoldResult* %62)
  %m_resultOut61 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 5
  %65 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut61, align 4, !tbaa !54
  %call62 = call %class.btCollisionObject* @_ZNK16btManifoldResult16getBody0InternalEv(%class.btManifoldResult* %65)
  %m_compoundColObjWrap63 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 1
  %66 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compoundColObjWrap63, align 4, !tbaa !50
  %call64 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %66)
  %cmp65 = icmp eq %class.btCollisionObject* %call62, %call64
  br i1 %cmp65, label %if.then66, label %if.else68

if.then66:                                        ; preds = %if.end54
  %m_resultOut67 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 5
  %67 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut67, align 4, !tbaa !54
  %68 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %tmpWrap, align 4, !tbaa !2
  call void @_ZN16btManifoldResult12setBody0WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %67, %struct.btCollisionObjectWrapper* %68)
  br label %if.end70

if.else68:                                        ; preds = %if.end54
  %m_resultOut69 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 5
  %69 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut69, align 4, !tbaa !54
  %70 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %tmpWrap, align 4, !tbaa !2
  call void @_ZN16btManifoldResult12setBody1WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %69, %struct.btCollisionObjectWrapper* %70)
  br label %if.end70

if.end70:                                         ; preds = %if.else68, %if.then66
  %71 = bitcast %struct.btCollisionObjectWrapper** %tmpWrap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #8
  %72 = bitcast %struct.btCollisionObjectWrapper* %compoundWrap to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %72) #8
  br label %if.end71

if.end71:                                         ; preds = %if.end70, %if.end22
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end71, %if.then21
  %73 = bitcast %class.btVector3* %aabbMax1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %73) #8
  %74 = bitcast %class.btVector3* %aabbMin1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %74) #8
  %75 = bitcast %class.btVector3* %aabbMax0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %75) #8
  %76 = bitcast %class.btVector3* %aabbMin0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %76) #8
  %77 = bitcast %class.btTransform* %newChildWorldTrans to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %77) #8
  %78 = bitcast %class.btTransform** %childTrans to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #8
  %79 = bitcast %class.btTransform* %orgInterpolationTrans to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %79) #8
  %80 = bitcast %class.btTransform* %orgTrans to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %80) #8
  %81 = bitcast %class.btCompoundShape** %compoundShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK15btCompoundShape17getChildTransformEi(%class.btCompoundShape* %this, i32 %index) #4 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %index.addr = alloca i32, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !29
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %index.addr, align 4, !tbaa !29
  %call = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray.0* %m_children, i32 %0)
  %m_transform = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call, i32 0, i32 0
  ret %class.btTransform* %m_transform
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin2, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax2) #5 comdat {
entry:
  %aabbMin1.addr = alloca %class.btVector3*, align 4
  %aabbMax1.addr = alloca %class.btVector3*, align 4
  %aabbMin2.addr = alloca %class.btVector3*, align 4
  %aabbMax2.addr = alloca %class.btVector3*, align 4
  %overlap = alloca i8, align 1
  store %class.btVector3* %aabbMin1, %class.btVector3** %aabbMin1.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax1, %class.btVector3** %aabbMax1.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin2, %class.btVector3** %aabbMin2.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax2, %class.btVector3** %aabbMax2.addr, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %overlap) #8
  store i8 1, i8* %overlap, align 1, !tbaa !6
  %0 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %0)
  %1 = load float, float* %call, align 4, !tbaa !71
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4, !tbaa !2
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %2)
  %3 = load float, float* %call1, align 4, !tbaa !71
  %cmp = fcmp ogt float %1, %3
  br i1 %cmp, label %cond.true, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %4 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %4)
  %5 = load float, float* %call2, align 4, !tbaa !71
  %6 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %6)
  %7 = load float, float* %call3, align 4, !tbaa !71
  %cmp4 = fcmp olt float %5, %7
  br i1 %cmp4, label %cond.true, label %cond.false

cond.true:                                        ; preds = %lor.lhs.false, %entry
  br label %cond.end

cond.false:                                       ; preds = %lor.lhs.false
  %8 = load i8, i8* %overlap, align 1, !tbaa !6, !range !10
  %tobool = trunc i8 %8 to i1
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i1 [ false, %cond.true ], [ %tobool, %cond.false ]
  %frombool = zext i1 %cond to i8
  store i8 %frombool, i8* %overlap, align 1, !tbaa !6
  %9 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4, !tbaa !2
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %9)
  %10 = load float, float* %call5, align 4, !tbaa !71
  %11 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %11)
  %12 = load float, float* %call6, align 4, !tbaa !71
  %cmp7 = fcmp ogt float %10, %12
  br i1 %cmp7, label %cond.true12, label %lor.lhs.false8

lor.lhs.false8:                                   ; preds = %cond.end
  %13 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %13)
  %14 = load float, float* %call9, align 4, !tbaa !71
  %15 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4, !tbaa !2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %15)
  %16 = load float, float* %call10, align 4, !tbaa !71
  %cmp11 = fcmp olt float %14, %16
  br i1 %cmp11, label %cond.true12, label %cond.false13

cond.true12:                                      ; preds = %lor.lhs.false8, %cond.end
  br label %cond.end15

cond.false13:                                     ; preds = %lor.lhs.false8
  %17 = load i8, i8* %overlap, align 1, !tbaa !6, !range !10
  %tobool14 = trunc i8 %17 to i1
  br label %cond.end15

cond.end15:                                       ; preds = %cond.false13, %cond.true12
  %cond16 = phi i1 [ false, %cond.true12 ], [ %tobool14, %cond.false13 ]
  %frombool17 = zext i1 %cond16 to i8
  store i8 %frombool17, i8* %overlap, align 1, !tbaa !6
  %18 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4, !tbaa !2
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %18)
  %19 = load float, float* %call18, align 4, !tbaa !71
  %20 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4, !tbaa !2
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %20)
  %21 = load float, float* %call19, align 4, !tbaa !71
  %cmp20 = fcmp ogt float %19, %21
  br i1 %cmp20, label %cond.true25, label %lor.lhs.false21

lor.lhs.false21:                                  ; preds = %cond.end15
  %22 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4, !tbaa !2
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %22)
  %23 = load float, float* %call22, align 4, !tbaa !71
  %24 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4, !tbaa !2
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %24)
  %25 = load float, float* %call23, align 4, !tbaa !71
  %cmp24 = fcmp olt float %23, %25
  br i1 %cmp24, label %cond.true25, label %cond.false26

cond.true25:                                      ; preds = %lor.lhs.false21, %cond.end15
  br label %cond.end28

cond.false26:                                     ; preds = %lor.lhs.false21
  %26 = load i8, i8* %overlap, align 1, !tbaa !6, !range !10
  %tobool27 = trunc i8 %26 to i1
  br label %cond.end28

cond.end28:                                       ; preds = %cond.false26, %cond.true25
  %cond29 = phi i1 [ false, %cond.true25 ], [ %tobool27, %cond.false26 ]
  %frombool30 = zext i1 %cond29 to i8
  store i8 %frombool30, i8* %overlap, align 1, !tbaa !6
  %27 = load i8, i8* %overlap, align 1, !tbaa !6, !range !10
  %tobool31 = trunc i8 %27 to i1
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %overlap) #8
  ret i1 %tobool31
}

; Function Attrs: nounwind
define linkonce_odr hidden %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideD2Ev(%"struct.btDbvt::ICollide"* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  ret %"struct.btDbvt::ICollide"* %this1
}

define hidden float @_ZN28btCompoundCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult(%class.btCompoundCollisionAlgorithm* %this, %class.btCollisionObject* %body0, %class.btCollisionObject* %body1, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo, %class.btManifoldResult* %resultOut) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btCompoundCollisionAlgorithm*, align 4
  %body0.addr = alloca %class.btCollisionObject*, align 4
  %body1.addr = alloca %class.btCollisionObject*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %resultOut.addr = alloca %class.btManifoldResult*, align 4
  %colObj = alloca %class.btCollisionObject*, align 4
  %otherObj = alloca %class.btCollisionObject*, align 4
  %compoundShape = alloca %class.btCompoundShape*, align 4
  %hitFraction = alloca float, align 4
  %numChildren = alloca i32, align 4
  %i = alloca i32, align 4
  %orgTrans = alloca %class.btTransform, align 4
  %frac = alloca float, align 4
  %childTrans = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btTransform, align 4
  store %class.btCompoundCollisionAlgorithm* %this, %class.btCompoundCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %body0, %class.btCollisionObject** %body0.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %body1, %class.btCollisionObject** %body1.addr, align 4, !tbaa !2
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  store %class.btManifoldResult* %resultOut, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundCollisionAlgorithm*, %class.btCompoundCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast %class.btCollisionObject** %colObj to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %m_isSwapped = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  %1 = load i8, i8* %m_isSwapped, align 4, !tbaa !11, !range !10
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %body1.addr, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load %class.btCollisionObject*, %class.btCollisionObject** %body0.addr, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %class.btCollisionObject* [ %2, %cond.true ], [ %3, %cond.false ]
  store %class.btCollisionObject* %cond, %class.btCollisionObject** %colObj, align 4, !tbaa !2
  %4 = bitcast %class.btCollisionObject** %otherObj to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %m_isSwapped2 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  %5 = load i8, i8* %m_isSwapped2, align 4, !tbaa !11, !range !10
  %tobool3 = trunc i8 %5 to i1
  br i1 %tobool3, label %cond.true4, label %cond.false5

cond.true4:                                       ; preds = %cond.end
  %6 = load %class.btCollisionObject*, %class.btCollisionObject** %body0.addr, align 4, !tbaa !2
  br label %cond.end6

cond.false5:                                      ; preds = %cond.end
  %7 = load %class.btCollisionObject*, %class.btCollisionObject** %body1.addr, align 4, !tbaa !2
  br label %cond.end6

cond.end6:                                        ; preds = %cond.false5, %cond.true4
  %cond7 = phi %class.btCollisionObject* [ %6, %cond.true4 ], [ %7, %cond.false5 ]
  store %class.btCollisionObject* %cond7, %class.btCollisionObject** %otherObj, align 4, !tbaa !2
  %8 = bitcast %class.btCompoundShape** %compoundShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  %9 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4, !tbaa !2
  %call = call %class.btCollisionShape* @_ZN17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %9)
  %10 = bitcast %class.btCollisionShape* %call to %class.btCompoundShape*
  store %class.btCompoundShape* %10, %class.btCompoundShape** %compoundShape, align 4, !tbaa !2
  %11 = bitcast float* %hitFraction to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  store float 1.000000e+00, float* %hitFraction, align 4, !tbaa !71
  %12 = bitcast i32* %numChildren to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  %m_childCollisionAlgorithms = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %call8 = call i32 @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4sizeEv(%class.btAlignedObjectArray* %m_childCollisionAlgorithms)
  store i32 %call8, i32* %numChildren, align 4, !tbaa !29
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = bitcast %class.btTransform* %orgTrans to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %14) #8
  %call9 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %orgTrans)
  %15 = bitcast float* %frac to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  store i32 0, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end6
  %16 = load i32, i32* %i, align 4, !tbaa !29
  %17 = load i32, i32* %numChildren, align 4, !tbaa !29
  %cmp = icmp slt i32 %16, %17
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %18 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4, !tbaa !2
  %call10 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %18)
  %call11 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %orgTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %call10)
  %19 = bitcast %class.btTransform** %childTrans to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  %20 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape, align 4, !tbaa !2
  %21 = load i32, i32* %i, align 4, !tbaa !29
  %call12 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN15btCompoundShape17getChildTransformEi(%class.btCompoundShape* %20, i32 %21)
  store %class.btTransform* %call12, %class.btTransform** %childTrans, align 4, !tbaa !2
  %22 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4, !tbaa !2
  %23 = bitcast %class.btTransform* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %23) #8
  %24 = load %class.btTransform*, %class.btTransform** %childTrans, align 4, !tbaa !2
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp, %class.btTransform* %orgTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %24)
  call void @_ZN17btCollisionObject17setWorldTransformERK11btTransform(%class.btCollisionObject* %22, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp)
  %25 = bitcast %class.btTransform* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %25) #8
  %m_childCollisionAlgorithms13 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %26 = load i32, i32* %i, align 4, !tbaa !29
  %call14 = call nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray* %m_childCollisionAlgorithms13, i32 %26)
  %27 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %call14, align 4, !tbaa !2
  %28 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4, !tbaa !2
  %29 = load %class.btCollisionObject*, %class.btCollisionObject** %otherObj, align 4, !tbaa !2
  %30 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  %31 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  %32 = bitcast %class.btCollisionAlgorithm* %27 to float (%class.btCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)***
  %vtable = load float (%class.btCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)**, float (%class.btCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*** %32, align 4, !tbaa !8
  %vfn = getelementptr inbounds float (%class.btCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*, float (%class.btCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)** %vtable, i64 3
  %33 = load float (%class.btCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*, float (%class.btCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)** %vfn, align 4
  %call15 = call float %33(%class.btCollisionAlgorithm* %27, %class.btCollisionObject* %28, %class.btCollisionObject* %29, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %30, %class.btManifoldResult* %31)
  store float %call15, float* %frac, align 4, !tbaa !71
  %34 = load float, float* %frac, align 4, !tbaa !71
  %35 = load float, float* %hitFraction, align 4, !tbaa !71
  %cmp16 = fcmp olt float %34, %35
  br i1 %cmp16, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %36 = load float, float* %frac, align 4, !tbaa !71
  store float %36, float* %hitFraction, align 4, !tbaa !71
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %37 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4, !tbaa !2
  call void @_ZN17btCollisionObject17setWorldTransformERK11btTransform(%class.btCollisionObject* %37, %class.btTransform* nonnull align 4 dereferenceable(64) %orgTrans)
  %38 = bitcast %class.btTransform** %childTrans to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #8
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %39 = load i32, i32* %i, align 4, !tbaa !29
  %inc = add nsw i32 %39, 1
  store i32 %inc, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %40 = load float, float* %hitFraction, align 4, !tbaa !71
  %41 = bitcast float* %frac to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #8
  %42 = bitcast %class.btTransform* %orgTrans to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %42) #8
  %43 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #8
  %44 = bitcast i32* %numChildren to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #8
  %45 = bitcast float* %hitFraction to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #8
  %46 = bitcast %class.btCompoundShape** %compoundShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #8
  %47 = bitcast %class.btCollisionObject** %otherObj to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #8
  %48 = bitcast %class.btCollisionObject** %colObj to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #8
  ret float %40
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionShape* @_ZN17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 9
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %m_collisionShape, align 4, !tbaa !72
  ret %class.btCollisionShape* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN15btCompoundShape17getChildTransformEi(%class.btCompoundShape* %this, i32 %index) #0 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %index.addr = alloca i32, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !29
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %index.addr, align 4, !tbaa !29
  %call = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray.0* %m_children, i32 %0)
  %m_transform = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call, i32 0, i32 0
  ret %class.btTransform* %m_transform
}

define linkonce_odr hidden void @_ZN17btCollisionObject17setWorldTransformERK11btTransform(%class.btCollisionObject* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %worldTrans) #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %worldTrans.addr = alloca %class.btTransform*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %worldTrans, %class.btTransform** %worldTrans.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_updateRevision = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 26
  %0 = load i32, i32* %m_updateRevision, align 4, !tbaa !74
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %m_updateRevision, align 4, !tbaa !74
  %1 = load %class.btTransform*, %class.btTransform** %worldTrans.addr, align 4, !tbaa !2
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_worldTransform, %class.btTransform* nonnull align 4 dereferenceable(64) %1)
  ret void
}

define linkonce_odr hidden void @_ZN28btCompoundCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE(%class.btCompoundCollisionAlgorithm* %this, %class.btAlignedObjectArray.13* nonnull align 4 dereferenceable(17) %manifoldArray) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btCompoundCollisionAlgorithm*, align 4
  %manifoldArray.addr = alloca %class.btAlignedObjectArray.13*, align 4
  %i = alloca i32, align 4
  store %class.btCompoundCollisionAlgorithm* %this, %class.btCompoundCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.13* %manifoldArray, %class.btAlignedObjectArray.13** %manifoldArray.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundCollisionAlgorithm*, %class.btCompoundCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !29
  %m_childCollisionAlgorithms = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4sizeEv(%class.btAlignedObjectArray* %m_childCollisionAlgorithms)
  %cmp = icmp slt i32 %1, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_childCollisionAlgorithms2 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %2 = load i32, i32* %i, align 4, !tbaa !29
  %call3 = call nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray* %m_childCollisionAlgorithms2, i32 %2)
  %3 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %call3, align 4, !tbaa !2
  %tobool = icmp ne %class.btCollisionAlgorithm* %3, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %m_childCollisionAlgorithms4 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %4 = load i32, i32* %i, align 4, !tbaa !29
  %call5 = call nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray* %m_childCollisionAlgorithms4, i32 %4)
  %5 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %call5, align 4, !tbaa !2
  %6 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %manifoldArray.addr, align 4, !tbaa !2
  %7 = bitcast %class.btCollisionAlgorithm* %5 to void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.13*)***
  %vtable = load void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.13*)**, void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.13*)*** %7, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.13*)*, void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.13*)** %vtable, i64 4
  %8 = load void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.13*)*, void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.13*)** %vfn, align 4
  call void %8(%class.btCollisionAlgorithm* %5, %class.btAlignedObjectArray.13* nonnull align 4 dereferenceable(17) %6)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %9 = load i32, i32* %i, align 4, !tbaa !29
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray.0* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !75
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !29
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data, align 4, !tbaa !76
  %1 = load i32, i32* %n.addr, align 4, !tbaa !29
  %arrayidx = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %0, i32 %1
  ret %struct.btCompoundShapeChild* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideC2Ev(%"struct.btDbvt::ICollide"* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  %0 = bitcast %"struct.btDbvt::ICollide"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTVN6btDbvt8ICollideE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !8
  ret %"struct.btDbvt::ICollide"* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN22btCompoundLeafCallbackD0Ev(%struct.btCompoundLeafCallback* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %struct.btCompoundLeafCallback*, align 4
  store %struct.btCompoundLeafCallback* %this, %struct.btCompoundLeafCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCompoundLeafCallback*, %struct.btCompoundLeafCallback** %this.addr, align 4
  %call = call %struct.btCompoundLeafCallback* bitcast (%"struct.btDbvt::ICollide"* (%"struct.btDbvt::ICollide"*)* @_ZN6btDbvt8ICollideD2Ev to %struct.btCompoundLeafCallback* (%struct.btCompoundLeafCallback*)*)(%struct.btCompoundLeafCallback* %this1) #8
  %0 = bitcast %struct.btCompoundLeafCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_(%"struct.btDbvt::ICollide"* %this, %struct.btDbvtNode* %0, %struct.btDbvtNode* %1) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %.addr = alloca %struct.btDbvtNode*, align 4
  %.addr1 = alloca %struct.btDbvtNode*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4, !tbaa !2
  store %struct.btDbvtNode* %0, %struct.btDbvtNode** %.addr, align 4, !tbaa !2
  store %struct.btDbvtNode* %1, %struct.btDbvtNode** %.addr1, align 4, !tbaa !2
  %this2 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  ret void
}

define linkonce_odr hidden void @_ZN22btCompoundLeafCallback7ProcessEPK10btDbvtNode(%struct.btCompoundLeafCallback* %this, %struct.btDbvtNode* %leaf) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %struct.btCompoundLeafCallback*, align 4
  %leaf.addr = alloca %struct.btDbvtNode*, align 4
  %index = alloca i32, align 4
  %compoundShape = alloca %class.btCompoundShape*, align 4
  %childShape = alloca %class.btCollisionShape*, align 4
  store %struct.btCompoundLeafCallback* %this, %struct.btCompoundLeafCallback** %this.addr, align 4, !tbaa !2
  store %struct.btDbvtNode* %leaf, %struct.btDbvtNode** %leaf.addr, align 4, !tbaa !2
  %this1 = load %struct.btCompoundLeafCallback*, %struct.btCompoundLeafCallback** %this.addr, align 4
  %0 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf.addr, align 4, !tbaa !2
  %2 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %1, i32 0, i32 2
  %dataAsInt = bitcast %union.anon.3* %2 to i32*
  %3 = load i32, i32* %dataAsInt, align 4, !tbaa !68
  store i32 %3, i32* %index, align 4, !tbaa !29
  %4 = bitcast %class.btCompoundShape** %compoundShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %m_compoundColObjWrap = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 1
  %5 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compoundColObjWrap, align 4, !tbaa !50
  %call = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %5)
  %6 = bitcast %class.btCollisionShape* %call to %class.btCompoundShape*
  store %class.btCompoundShape* %6, %class.btCompoundShape** %compoundShape, align 4, !tbaa !2
  %7 = bitcast %class.btCollisionShape** %childShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %8 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape, align 4, !tbaa !2
  %9 = load i32, i32* %index, align 4, !tbaa !29
  %call2 = call %class.btCollisionShape* @_ZNK15btCompoundShape13getChildShapeEi(%class.btCompoundShape* %8, i32 %9)
  store %class.btCollisionShape* %call2, %class.btCollisionShape** %childShape, align 4, !tbaa !2
  %10 = load %class.btCollisionShape*, %class.btCollisionShape** %childShape, align 4, !tbaa !2
  %11 = load i32, i32* %index, align 4, !tbaa !29
  call void @_ZN22btCompoundLeafCallback17ProcessChildShapeEPK16btCollisionShapei(%struct.btCompoundLeafCallback* %this1, %class.btCollisionShape* %10, i32 %11)
  %12 = bitcast %class.btCollisionShape** %childShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  %13 = bitcast %class.btCompoundShape** %compoundShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  %14 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #8
  ret void
}

define linkonce_odr hidden void @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef(%"struct.btDbvt::ICollide"* %this, %struct.btDbvtNode* %n, float %0) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %n.addr = alloca %struct.btDbvtNode*, align 4
  %.addr = alloca float, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4, !tbaa !2
  store %struct.btDbvtNode* %n, %struct.btDbvtNode** %n.addr, align 4, !tbaa !2
  store float %0, float* %.addr, align 4, !tbaa !71
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n.addr, align 4, !tbaa !2
  %2 = bitcast %"struct.btDbvt::ICollide"* %this1 to void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)***
  %vtable = load void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)**, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*** %2, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)** %vtable, i64 3
  %3 = load void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)** %vfn, align 4
  call void %3(%"struct.btDbvt::ICollide"* %this1, %struct.btDbvtNode* %1)
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode(%"struct.btDbvt::ICollide"* %this, %struct.btDbvtNode* %0) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %.addr = alloca %struct.btDbvtNode*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4, !tbaa !2
  store %struct.btDbvtNode* %0, %struct.btDbvtNode** %.addr, align 4, !tbaa !2
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  ret i1 true
}

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode(%"struct.btDbvt::ICollide"* %this, %struct.btDbvtNode* %0) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %.addr = alloca %struct.btDbvtNode*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4, !tbaa !2
  store %struct.btDbvtNode* %0, %struct.btDbvtNode** %.addr, align 4, !tbaa !2
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  ret i1 true
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN6btDbvt8ICollideD0Ev(%"struct.btDbvt::ICollide"* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  %call = call %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideD2Ev(%"struct.btDbvt::ICollide"* %this1) #8
  %0 = bitcast %"struct.btDbvt::ICollide"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNode(%"struct.btDbvt::ICollide"* %this, %struct.btDbvtNode* %0) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %.addr = alloca %struct.btDbvtNode*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4, !tbaa !2
  store %struct.btDbvtNode* %0, %struct.btDbvtNode** %.addr, align 4, !tbaa !2
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_body0 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body0, align 4, !tbaa !77
  ret %class.btCollisionObject* %0
}

declare void @_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_(%class.btPersistentManifold*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btTransform* nonnull align 4 dereferenceable(64)) #1

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #5 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #5 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %1, i32 0)
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call1, float* %ref.tmp, align 4, !tbaa !71
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 1)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !71
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 2)
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !71
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #5 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !71
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp, align 4, !tbaa !71
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !71
  %fneg4 = fneg float %5
  store float %fneg4, float* %ref.tmp1, align 4, !tbaa !71
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %8 = load float, float* %arrayidx7, align 4, !tbaa !71
  %fneg8 = fneg float %8
  store float %fneg8, float* %ref.tmp5, align 4, !tbaa !71
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %b.addr = alloca %class.btMatrix3x3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %b, %class.btMatrix3x3** %b.addr, align 4, !tbaa !2
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %b.addr, align 4, !tbaa !2
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !67
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !71
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !71
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !71
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !71
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !71
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !71
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !71
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !29
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !29
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !71
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !71
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !71
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !71
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !71
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !71
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !71
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !71
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !71
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !71
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !71
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !71
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !71
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #5 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !67
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !67
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !67
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #5 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %3, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4, !tbaa !71
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !71
  %7 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %9, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %8, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !71
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4, !tbaa !71
  %13 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %15, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %14, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4, !tbaa !71
  %16 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4, !tbaa !71
  %19 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  %20 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %21 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %21, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %20, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4, !tbaa !71
  %22 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #8
  %23 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %24 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %24, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %23, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4, !tbaa !71
  %25 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  %26 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %27 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %27, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %26, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4, !tbaa !71
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  %28 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #8
  %29 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #8
  %30 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #8
  %31 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #8
  %32 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #8
  %33 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #8
  %34 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #8
  %35 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  %36 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #5 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !71
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !71
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !71
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !71
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !71
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !71
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !71
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !71
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !71
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !71
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !71
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !71
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !71
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !71
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !71
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !71
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !71
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !71
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #5 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !71
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !71
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !71
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !71
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !71
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !71
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !71
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !71
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !71
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4, !tbaa !71
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !71
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !71
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !67
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !67
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !67
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden %struct.btDbvtAabbMm* @_ZN12btDbvtAabbMmC2Ev(%struct.btDbvtAabbMm* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %struct.btDbvtAabbMm*, align 4
  store %struct.btDbvtAabbMm* %this, %struct.btDbvtAabbMm** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %this.addr, align 4
  %mi = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %mi)
  %mx = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %mx)
  ret %struct.btDbvtAabbMm* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeEC2Ev(%class.btAlignedObjectArray.9* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.10* @_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EEC2Ev(%class.btAlignedAllocator.10* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE4initEv(%class.btAlignedObjectArray.9* %this1)
  ret %class.btAlignedObjectArray.9* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE6resizeEiRKS2_(%class.btAlignedObjectArray.9* %this, i32 %newsize, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %fillData) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %struct.btDbvtNode**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !29
  store %struct.btDbvtNode** %fillData, %struct.btDbvtNode*** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !29
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !29
  %2 = load i32, i32* %curSize, align 4, !tbaa !29
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !29
  store i32 %4, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !29
  %6 = load i32, i32* %curSize, align 4, !tbaa !29
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %8 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4, !tbaa !78
  %9 = load i32, i32* %i, align 4, !tbaa !29
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !29
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !29
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !29
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7reserveEi(%class.btAlignedObjectArray.9* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load i32, i32* %curSize, align 4, !tbaa !29
  store i32 %14, i32* %i6, align 4, !tbaa !29
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !29
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !29
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %18 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data11, align 4, !tbaa !78
  %19 = load i32, i32* %i6, align 4, !tbaa !29
  %arrayidx12 = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %18, i32 %19
  %20 = bitcast %struct.btDbvtNode** %arrayidx12 to i8*
  %21 = bitcast i8* %20 to %struct.btDbvtNode**
  %22 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %fillData.addr, align 4, !tbaa !2
  %23 = load %struct.btDbvtNode*, %struct.btDbvtNode** %22, align 4, !tbaa !2
  store %struct.btDbvtNode* %23, %struct.btDbvtNode** %21, align 4, !tbaa !2
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %24 = load i32, i32* %i6, align 4, !tbaa !29
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !29
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %25 = load i32, i32* %newsize.addr, align 4, !tbaa !29
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  store i32 %25, i32* %m_size, align 4, !tbaa !79
  %26 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7reserveEi(%class.btAlignedObjectArray.9* %this, i32 %_Count) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btDbvtNode**, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !29
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE8capacityEv(%class.btAlignedObjectArray.9* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !29
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btDbvtNode*** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !29
  %call2 = call i8* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE8allocateEi(%class.btAlignedObjectArray.9* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.btDbvtNode**
  store %struct.btDbvtNode** %3, %struct.btDbvtNode*** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  %4 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4copyEiiPS2_(%class.btAlignedObjectArray.9* %this1, i32 0, i32 %call3, %struct.btDbvtNode** %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7destroyEii(%class.btAlignedObjectArray.9* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE10deallocateEv(%class.btAlignedObjectArray.9* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !80
  %5 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  store %struct.btDbvtNode** %5, %struct.btDbvtNode*** %m_data, align 4, !tbaa !78
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !29
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !81
  %7 = bitcast %struct.btDbvtNode*** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9push_backERKS2_(%class.btAlignedObjectArray.9* %this, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %_Val) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %_Val.addr = alloca %struct.btDbvtNode**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  store %struct.btDbvtNode** %_Val, %struct.btDbvtNode*** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !29
  %1 = load i32, i32* %sz, align 4, !tbaa !29
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE8capacityEv(%class.btAlignedObjectArray.9* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9allocSizeEi(%class.btAlignedObjectArray.9* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7reserveEi(%class.btAlignedObjectArray.9* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %2 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4, !tbaa !78
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !79
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %2, i32 %3
  %4 = bitcast %struct.btDbvtNode** %arrayidx to i8*
  %5 = bitcast i8* %4 to %struct.btDbvtNode**
  %6 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %_Val.addr, align 4, !tbaa !2
  %7 = load %struct.btDbvtNode*, %struct.btDbvtNode** %6, align 4, !tbaa !2
  store %struct.btDbvtNode* %7, %struct.btDbvtNode** %5, align 4, !tbaa !2
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size5, align 4, !tbaa !79
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size5, align 4, !tbaa !79
  %9 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct.btDbvtNode** @_ZN20btAlignedObjectArrayIPK10btDbvtNodeEixEi(%class.btAlignedObjectArray.9* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !29
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %0 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4, !tbaa !78
  %1 = load i32, i32* %n.addr, align 4, !tbaa !29
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %0, i32 %1
  ret %struct.btDbvtNode** %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.9* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !79
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE8pop_backEv(%class.btAlignedObjectArray.9* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !79
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4, !tbaa !79
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %1 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4, !tbaa !78
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4, !tbaa !79
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %1, i32 %2
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_Z9IntersectRK12btDbvtAabbMmS1_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %a, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %b) #3 comdat {
entry:
  %a.addr = alloca %struct.btDbvtAabbMm*, align 4
  %b.addr = alloca %struct.btDbvtAabbMm*, align 4
  store %struct.btDbvtAabbMm* %a, %struct.btDbvtAabbMm** %a.addr, align 4, !tbaa !2
  store %struct.btDbvtAabbMm* %b, %struct.btDbvtAabbMm** %b.addr, align 4, !tbaa !2
  %0 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4, !tbaa !2
  %mi = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %0, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %mi)
  %1 = load float, float* %call, align 4, !tbaa !71
  %2 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4, !tbaa !2
  %mx = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %2, i32 0, i32 1
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %mx)
  %3 = load float, float* %call1, align 4, !tbaa !71
  %cmp = fcmp ole float %1, %3
  br i1 %cmp, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %entry
  %4 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4, !tbaa !2
  %mx2 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %4, i32 0, i32 1
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %mx2)
  %5 = load float, float* %call3, align 4, !tbaa !71
  %6 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4, !tbaa !2
  %mi4 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %6, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %mi4)
  %7 = load float, float* %call5, align 4, !tbaa !71
  %cmp6 = fcmp oge float %5, %7
  br i1 %cmp6, label %land.lhs.true7, label %land.end

land.lhs.true7:                                   ; preds = %land.lhs.true
  %8 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4, !tbaa !2
  %mi8 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %8, i32 0, i32 0
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %mi8)
  %9 = load float, float* %call9, align 4, !tbaa !71
  %10 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4, !tbaa !2
  %mx10 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %10, i32 0, i32 1
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %mx10)
  %11 = load float, float* %call11, align 4, !tbaa !71
  %cmp12 = fcmp ole float %9, %11
  br i1 %cmp12, label %land.lhs.true13, label %land.end

land.lhs.true13:                                  ; preds = %land.lhs.true7
  %12 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4, !tbaa !2
  %mx14 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %12, i32 0, i32 1
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %mx14)
  %13 = load float, float* %call15, align 4, !tbaa !71
  %14 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4, !tbaa !2
  %mi16 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %14, i32 0, i32 0
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %mi16)
  %15 = load float, float* %call17, align 4, !tbaa !71
  %cmp18 = fcmp oge float %13, %15
  br i1 %cmp18, label %land.lhs.true19, label %land.end

land.lhs.true19:                                  ; preds = %land.lhs.true13
  %16 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4, !tbaa !2
  %mi20 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %16, i32 0, i32 0
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %mi20)
  %17 = load float, float* %call21, align 4, !tbaa !71
  %18 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4, !tbaa !2
  %mx22 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %18, i32 0, i32 1
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %mx22)
  %19 = load float, float* %call23, align 4, !tbaa !71
  %cmp24 = fcmp ole float %17, %19
  br i1 %cmp24, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true19
  %20 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4, !tbaa !2
  %mx25 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %20, i32 0, i32 1
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %mx25)
  %21 = load float, float* %call26, align 4, !tbaa !71
  %22 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4, !tbaa !2
  %mi27 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %22, i32 0, i32 0
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %mi27)
  %23 = load float, float* %call28, align 4, !tbaa !71
  %cmp29 = fcmp oge float %21, %23
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true19, %land.lhs.true13, %land.lhs.true7, %land.lhs.true, %entry
  %24 = phi i1 [ false, %land.lhs.true19 ], [ false, %land.lhs.true13 ], [ false, %land.lhs.true7 ], [ false, %land.lhs.true ], [ false, %entry ], [ %cmp29, %land.rhs ]
  ret i1 %24
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %this) #5 comdat {
entry:
  %this.addr = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvtNode* %this, %struct.btDbvtNode** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %this.addr, align 4
  %call = call zeroext i1 @_ZNK10btDbvtNode6isleafEv(%struct.btDbvtNode* %this1)
  %lnot = xor i1 %call, true
  ret i1 %lnot
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeED2Ev(%class.btAlignedObjectArray.9* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE5clearEv(%class.btAlignedObjectArray.9* %this1)
  ret %class.btAlignedObjectArray.9* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.10* @_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EEC2Ev(%class.btAlignedAllocator.10* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.10*, align 4
  store %class.btAlignedAllocator.10* %this, %class.btAlignedAllocator.10** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.10*, %class.btAlignedAllocator.10** %this.addr, align 4
  ret %class.btAlignedAllocator.10* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE4initEv(%class.btAlignedObjectArray.9* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !80
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  store %struct.btDbvtNode** null, %struct.btDbvtNode*** %m_data, align 4, !tbaa !78
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !79
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !81
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE8capacityEv(%class.btAlignedObjectArray.9* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !81
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE8allocateEi(%class.btAlignedObjectArray.9* %this, i32 %size) #5 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !29
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !29
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !29
  %call = call %struct.btDbvtNode** @_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EE8allocateEiPPKS2_(%class.btAlignedAllocator.10* %m_allocator, i32 %1, %struct.btDbvtNode*** null)
  %2 = bitcast %struct.btDbvtNode** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4copyEiiPS2_(%class.btAlignedObjectArray.9* %this, i32 %start, i32 %end, %struct.btDbvtNode** %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btDbvtNode**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !29
  store i32 %end, i32* %end.addr, align 4, !tbaa !29
  store %struct.btDbvtNode** %dest, %struct.btDbvtNode*** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %start.addr, align 4, !tbaa !29
  store i32 %1, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !29
  %3 = load i32, i32* %end.addr, align 4, !tbaa !29
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !29
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %4, i32 %5
  %6 = bitcast %struct.btDbvtNode** %arrayidx to i8*
  %7 = bitcast i8* %6 to %struct.btDbvtNode**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %8 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4, !tbaa !78
  %9 = load i32, i32* %i, align 4, !tbaa !29
  %arrayidx2 = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %8, i32 %9
  %10 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx2, align 4, !tbaa !2
  store %struct.btDbvtNode* %10, %struct.btDbvtNode** %7, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !29
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7destroyEii(%class.btAlignedObjectArray.9* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !29
  store i32 %last, i32* %last.addr, align 4, !tbaa !29
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %first.addr, align 4, !tbaa !29
  store i32 %1, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !29
  %3 = load i32, i32* %last.addr, align 4, !tbaa !29
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %4 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4, !tbaa !78
  %5 = load i32, i32* %i, align 4, !tbaa !29
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !29
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE10deallocateEv(%class.btAlignedObjectArray.9* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %0 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4, !tbaa !78
  %tobool = icmp ne %struct.btDbvtNode** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !80, !range !10
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %2 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data4, align 4, !tbaa !78
  call void @_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EE10deallocateEPS2_(%class.btAlignedAllocator.10* %m_allocator, %struct.btDbvtNode** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  store %struct.btDbvtNode** null, %struct.btDbvtNode*** %m_data5, align 4, !tbaa !78
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden %struct.btDbvtNode** @_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EE8allocateEiPPKS2_(%class.btAlignedAllocator.10* %this, i32 %n, %struct.btDbvtNode*** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.10*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btDbvtNode***, align 4
  store %class.btAlignedAllocator.10* %this, %class.btAlignedAllocator.10** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !29
  store %struct.btDbvtNode*** %hint, %struct.btDbvtNode**** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.10*, %class.btAlignedAllocator.10** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !29
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btDbvtNode**
  ret %struct.btDbvtNode** %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #1

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EE10deallocateEPS2_(%class.btAlignedAllocator.10* %this, %struct.btDbvtNode** %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.10*, align 4
  %ptr.addr = alloca %struct.btDbvtNode**, align 4
  store %class.btAlignedAllocator.10* %this, %class.btAlignedAllocator.10** %this.addr, align 4, !tbaa !2
  store %struct.btDbvtNode** %ptr, %struct.btDbvtNode*** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.10*, %class.btAlignedAllocator.10** %this.addr, align 4
  %0 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btDbvtNode** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9allocSizeEi(%class.btAlignedObjectArray.9* %this, i32 %size) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !29
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !29
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !29
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZNK10btDbvtNode6isleafEv(%struct.btDbvtNode* %this) #3 comdat {
entry:
  %this.addr = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvtNode* %this, %struct.btDbvtNode** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %this1, i32 0, i32 2
  %childs = bitcast %union.anon.3* %0 to [2 x %struct.btDbvtNode*]*
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 1
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx, align 4, !tbaa !68
  %cmp = icmp eq %struct.btDbvtNode* %1, null
  ret i1 %cmp
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE5clearEv(%class.btAlignedObjectArray.9* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7destroyEii(%class.btAlignedObjectArray.9* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE10deallocateEv(%class.btAlignedObjectArray.9* %this1)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE4initEv(%class.btAlignedObjectArray.9* %this1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !67
  ret %class.btTransform* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btCollisionObject* @_ZNK16btManifoldResult16getBody0InternalEv(%class.btManifoldResult* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %m_body0Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap, align 4, !tbaa !65
  %call = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %0)
  ret %class.btCollisionObject* %call
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btCollisionObjectWrapper* @_ZNK16btManifoldResult12getBody0WrapEv(%class.btManifoldResult* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %m_body0Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap, align 4, !tbaa !65
  ret %struct.btCollisionObjectWrapper* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN16btManifoldResult12setBody0WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %this, %struct.btCollisionObjectWrapper* %obj0Wrap) #4 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %obj0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %obj0Wrap, %struct.btCollisionObjectWrapper** %obj0Wrap.addr, align 4, !tbaa !2
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %obj0Wrap.addr, align 4, !tbaa !2
  %m_body0Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  store %struct.btCollisionObjectWrapper* %0, %struct.btCollisionObjectWrapper** %m_body0Wrap, align 4, !tbaa !65
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btCollisionObjectWrapper* @_ZNK16btManifoldResult12getBody1WrapEv(%class.btManifoldResult* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %m_body1Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body1Wrap, align 4, !tbaa !66
  ret %struct.btCollisionObjectWrapper* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN16btManifoldResult12setBody1WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %this, %struct.btCollisionObjectWrapper* %obj1Wrap) #4 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %obj1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %obj1Wrap, %struct.btCollisionObjectWrapper** %obj1Wrap.addr, align 4, !tbaa !2
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %obj1Wrap.addr, align 4, !tbaa !2
  %m_body1Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  store %struct.btCollisionObjectWrapper* %0, %struct.btCollisionObjectWrapper** %m_body1Wrap, align 4, !tbaa !66
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !29
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data, align 4, !tbaa !76
  %1 = load i32, i32* %n.addr, align 4, !tbaa !29
  %arrayidx = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %0, i32 %1
  ret %struct.btCompoundShapeChild* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIP20btCollisionAlgorithmLj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE4initEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !82
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btCollisionAlgorithm** null, %class.btCollisionAlgorithm*** %m_data, align 4, !tbaa !32
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !33
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !83
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE5clearEv(%class.btAlignedObjectArray* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !29
  store i32 %last, i32* %last.addr, align 4, !tbaa !29
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %first.addr, align 4, !tbaa !29
  store i32 %1, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !29
  %3 = load i32, i32* %last.addr, align 4, !tbaa !29
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %m_data, align 4, !tbaa !32
  %5 = load i32, i32* %i, align 4, !tbaa !29
  %arrayidx = getelementptr inbounds %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !29
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE10deallocateEv(%class.btAlignedObjectArray* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %m_data, align 4, !tbaa !32
  %tobool = icmp ne %class.btCollisionAlgorithm** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !82, !range !10
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %m_data4, align 4, !tbaa !32
  call void @_ZN18btAlignedAllocatorIP20btCollisionAlgorithmLj16EE10deallocateEPS1_(%class.btAlignedAllocator* %m_allocator, %class.btCollisionAlgorithm** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btCollisionAlgorithm** null, %class.btCollisionAlgorithm*** %m_data5, align 4, !tbaa !32
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP20btCollisionAlgorithmLj16EE10deallocateEPS1_(%class.btAlignedAllocator* %this, %class.btCollisionAlgorithm** %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.btCollisionAlgorithm**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store %class.btCollisionAlgorithm** %ptr, %class.btCollisionAlgorithm*** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btCollisionAlgorithm** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btCollisionAlgorithm**, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !29
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !29
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btCollisionAlgorithm*** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !29
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btCollisionAlgorithm**
  store %class.btCollisionAlgorithm** %3, %class.btCollisionAlgorithm*** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4sizeEv(%class.btAlignedObjectArray* %this1)
  %4 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4copyEiiPS1_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %class.btCollisionAlgorithm** %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !82
  %5 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btCollisionAlgorithm** %5, %class.btCollisionAlgorithm*** %m_data, align 4, !tbaa !32
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !29
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !83
  %7 = bitcast %class.btCollisionAlgorithm*** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE8capacityEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !83
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #5 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !29
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !29
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !29
  %call = call %class.btCollisionAlgorithm** @_ZN18btAlignedAllocatorIP20btCollisionAlgorithmLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator* %m_allocator, i32 %1, %class.btCollisionAlgorithm*** null)
  %2 = bitcast %class.btCollisionAlgorithm** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4copyEiiPS1_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %class.btCollisionAlgorithm** %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btCollisionAlgorithm**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !29
  store i32 %end, i32* %end.addr, align 4, !tbaa !29
  store %class.btCollisionAlgorithm** %dest, %class.btCollisionAlgorithm*** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %start.addr, align 4, !tbaa !29
  store i32 %1, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !29
  %3 = load i32, i32* %end.addr, align 4, !tbaa !29
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !29
  %arrayidx = getelementptr inbounds %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %4, i32 %5
  %6 = bitcast %class.btCollisionAlgorithm** %arrayidx to i8*
  %7 = bitcast i8* %6 to %class.btCollisionAlgorithm**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %m_data, align 4, !tbaa !32
  %9 = load i32, i32* %i, align 4, !tbaa !29
  %arrayidx2 = getelementptr inbounds %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %8, i32 %9
  %10 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %arrayidx2, align 4, !tbaa !2
  store %class.btCollisionAlgorithm* %10, %class.btCollisionAlgorithm** %7, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !29
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  ret void
}

define linkonce_odr hidden %class.btCollisionAlgorithm** @_ZN18btAlignedAllocatorIP20btCollisionAlgorithmLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator* %this, i32 %n, %class.btCollisionAlgorithm*** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btCollisionAlgorithm***, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !29
  store %class.btCollisionAlgorithm*** %hint, %class.btCollisionAlgorithm**** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !29
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btCollisionAlgorithm**
  ret %class.btCollisionAlgorithm** %1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.14* @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev(%class.btAlignedAllocator.14* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.14*, align 4
  store %class.btAlignedAllocator.14* %this, %class.btAlignedAllocator.14** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.14*, %class.btAlignedAllocator.14** %this.addr, align 4
  ret %class.btAlignedAllocator.14* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.13* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !84
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data, align 4, !tbaa !60
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !57
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !85
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv(%class.btAlignedObjectArray.13* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.13* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.13* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.13* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.13* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.13* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !29
  store i32 %last, i32* %last.addr, align 4, !tbaa !29
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %first.addr, align 4, !tbaa !29
  store i32 %1, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !29
  %3 = load i32, i32* %last.addr, align 4, !tbaa !29
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !60
  %5 = load i32, i32* %i, align 4, !tbaa !29
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !29
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.13* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !60
  %tobool = icmp ne %class.btPersistentManifold** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !84, !range !10
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data4, align 4, !tbaa !60
  call void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.14* %m_allocator, %class.btPersistentManifold** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data5, align 4, !tbaa !60
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.14* %this, %class.btPersistentManifold** %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.14*, align 4
  %ptr.addr = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedAllocator.14* %this, %class.btAlignedAllocator.14** %this.addr, align 4, !tbaa !2
  store %class.btPersistentManifold** %ptr, %class.btPersistentManifold*** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.14*, %class.btAlignedAllocator.14** %this.addr, align 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btPersistentManifold** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.13* %this, i32 %_Count) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !29
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.13* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !29
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btPersistentManifold*** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !29
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.13* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btPersistentManifold**
  store %class.btPersistentManifold** %3, %class.btPersistentManifold*** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.13* %this1)
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.13* %this1, i32 0, i32 %call3, %class.btPersistentManifold** %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.13* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.13* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.13* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !84
  %5 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  store %class.btPersistentManifold** %5, %class.btPersistentManifold*** %m_data, align 4, !tbaa !60
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !29
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !85
  %7 = bitcast %class.btPersistentManifold*** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.13* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !85
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.13* %this, i32 %size) #5 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !29
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !29
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !29
  %call = call %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.14* %m_allocator, i32 %1, %class.btPersistentManifold*** null)
  %2 = bitcast %class.btPersistentManifold** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.13* %this, i32 %start, i32 %end, %class.btPersistentManifold** %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btPersistentManifold**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !29
  store i32 %end, i32* %end.addr, align 4, !tbaa !29
  store %class.btPersistentManifold** %dest, %class.btPersistentManifold*** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %start.addr, align 4, !tbaa !29
  store i32 %1, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !29
  %3 = load i32, i32* %end.addr, align 4, !tbaa !29
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !29
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %4, i32 %5
  %6 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %7 = bitcast i8* %6 to %class.btPersistentManifold**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  %8 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !60
  %9 = load i32, i32* %i, align 4, !tbaa !29
  %arrayidx2 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %8, i32 %9
  %10 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx2, align 4, !tbaa !2
  store %class.btPersistentManifold* %10, %class.btPersistentManifold** %7, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !29
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  ret void
}

define linkonce_odr hidden %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.14* %this, i32 %n, %class.btPersistentManifold*** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.14*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btPersistentManifold***, align 4
  store %class.btAlignedAllocator.14* %this, %class.btAlignedAllocator.14** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !29
  store %class.btPersistentManifold*** %hint, %class.btPersistentManifold**** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.14*, %class.btAlignedAllocator.14** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !29
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btPersistentManifold**
  ret %class.btPersistentManifold** %1
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind }
attributes #9 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"bool", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"vtable pointer", !5, i64 0}
!10 = !{i8 0, i8 2}
!11 = !{!12, !7, i64 28}
!12 = !{!"_ZTS28btCompoundCollisionAlgorithm", !13, i64 8, !7, i64 28, !3, i64 32, !7, i64 36, !15, i64 40}
!13 = !{!"_ZTS20btAlignedObjectArrayIP20btCollisionAlgorithmE", !14, i64 0, !15, i64 4, !15, i64 8, !3, i64 12, !7, i64 16}
!14 = !{!"_ZTS18btAlignedAllocatorIP20btCollisionAlgorithmLj16EE"}
!15 = !{!"int", !4, i64 0}
!16 = !{!17, !3, i64 4}
!17 = !{!"_ZTS36btCollisionAlgorithmConstructionInfo", !3, i64 0, !3, i64 4}
!18 = !{!12, !3, i64 32}
!19 = !{!12, !7, i64 36}
!20 = !{!12, !15, i64 40}
!21 = !{!22, !3, i64 4}
!22 = !{!"_ZTS24btCollisionObjectWrapper", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !15, i64 16, !15, i64 20}
!23 = !{!24, !15, i64 68}
!24 = !{!"_ZTS15btCompoundShape", !25, i64 12, !27, i64 32, !27, i64 48, !3, i64 64, !15, i64 68, !28, i64 72, !27, i64 76}
!25 = !{!"_ZTS20btAlignedObjectArrayI20btCompoundShapeChildE", !26, i64 0, !15, i64 4, !15, i64 8, !3, i64 12, !7, i64 16}
!26 = !{!"_ZTS18btAlignedAllocatorI20btCompoundShapeChildLj16EE"}
!27 = !{!"_ZTS9btVector3", !4, i64 0}
!28 = !{!"float", !4, i64 0}
!29 = !{!15, !15, i64 0}
!30 = !{!31, !3, i64 4}
!31 = !{!"_ZTS20btCollisionAlgorithm", !3, i64 4}
!32 = !{!13, !3, i64 12}
!33 = !{!13, !15, i64 4}
!34 = !{!24, !3, i64 64}
!35 = !{!36, !3, i64 64}
!36 = !{!"_ZTS20btCompoundShapeChild", !37, i64 0, !3, i64 64, !15, i64 68, !28, i64 72, !3, i64 76}
!37 = !{!"_ZTS11btTransform", !38, i64 0, !27, i64 48}
!38 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!39 = !{!22, !3, i64 8}
!40 = !{!22, !3, i64 12}
!41 = !{!22, !3, i64 0}
!42 = !{!22, !15, i64 16}
!43 = !{!22, !15, i64 20}
!44 = !{!45, !3, i64 0}
!45 = !{!"_ZTS6btDbvt", !3, i64 0, !3, i64 4, !15, i64 8, !15, i64 12, !15, i64 16, !46, i64 20, !48, i64 40}
!46 = !{!"_ZTS20btAlignedObjectArrayIN6btDbvt6sStkNNEE", !47, i64 0, !15, i64 4, !15, i64 8, !3, i64 12, !7, i64 16}
!47 = !{!"_ZTS18btAlignedAllocatorIN6btDbvt6sStkNNELj16EE"}
!48 = !{!"_ZTS20btAlignedObjectArrayIPK10btDbvtNodeE", !49, i64 0, !15, i64 4, !15, i64 8, !3, i64 12, !7, i64 16}
!49 = !{!"_ZTS18btAlignedAllocatorIPK10btDbvtNodeLj16EE"}
!50 = !{!51, !3, i64 4}
!51 = !{!"_ZTS22btCompoundLeafCallback", !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28}
!52 = !{!51, !3, i64 8}
!53 = !{!51, !3, i64 12}
!54 = !{!51, !3, i64 20}
!55 = !{!51, !3, i64 24}
!56 = !{!51, !3, i64 28}
!57 = !{!58, !15, i64 4}
!58 = !{!"_ZTS20btAlignedObjectArrayIP20btPersistentManifoldE", !59, i64 0, !15, i64 4, !15, i64 8, !3, i64 12, !7, i64 16}
!59 = !{!"_ZTS18btAlignedAllocatorIP20btPersistentManifoldLj16EE"}
!60 = !{!58, !3, i64 12}
!61 = !{!62, !15, i64 748}
!62 = !{!"_ZTS20btPersistentManifold", !4, i64 4, !3, i64 740, !3, i64 744, !15, i64 748, !28, i64 752, !28, i64 756, !15, i64 760, !15, i64 764, !15, i64 768}
!63 = !{!64, !3, i64 4}
!64 = !{!"_ZTS16btManifoldResult", !3, i64 4, !3, i64 8, !3, i64 12, !15, i64 16, !15, i64 20, !15, i64 24, !15, i64 28}
!65 = !{!64, !3, i64 8}
!66 = !{!64, !3, i64 12}
!67 = !{i64 0, i64 16, !68}
!68 = !{!4, !4, i64 0}
!69 = !{i64 0, i64 16, !68, i64 16, i64 16, !68}
!70 = !{!51, !3, i64 16}
!71 = !{!28, !28, i64 0}
!72 = !{!73, !3, i64 192}
!73 = !{!"_ZTS17btCollisionObject", !37, i64 4, !37, i64 68, !27, i64 132, !27, i64 148, !27, i64 164, !15, i64 180, !28, i64 184, !3, i64 188, !3, i64 192, !3, i64 196, !3, i64 200, !15, i64 204, !15, i64 208, !15, i64 212, !15, i64 216, !28, i64 220, !28, i64 224, !28, i64 228, !28, i64 232, !15, i64 236, !4, i64 240, !28, i64 244, !28, i64 248, !28, i64 252, !15, i64 256, !15, i64 260}
!74 = !{!73, !15, i64 260}
!75 = !{!25, !15, i64 4}
!76 = !{!25, !3, i64 12}
!77 = !{!62, !3, i64 740}
!78 = !{!48, !3, i64 12}
!79 = !{!48, !15, i64 4}
!80 = !{!48, !7, i64 16}
!81 = !{!48, !15, i64 8}
!82 = !{!13, !7, i64 16}
!83 = !{!13, !15, i64 8}
!84 = !{!58, !7, i64 16}
!85 = !{!58, !15, i64 8}
