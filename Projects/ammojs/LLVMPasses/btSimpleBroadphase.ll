; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/BroadphaseCollision/btSimpleBroadphase.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/BroadphaseCollision/btSimpleBroadphase.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btSimpleBroadphase = type { %class.btBroadphaseInterface, i32, i32, i32, %struct.btSimpleBroadphaseProxy*, i8*, i32, %class.btOverlappingPairCache*, i8, i32 }
%class.btBroadphaseInterface = type { i32 (...)** }
%struct.btSimpleBroadphaseProxy = type { %struct.btBroadphaseProxy, i32 }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%class.btOverlappingPairCache = type { %class.btOverlappingPairCallback }
%class.btOverlappingPairCallback = type { i32 (...)** }
%class.btHashedOverlappingPairCache = type { %class.btOverlappingPairCache, %class.btAlignedObjectArray, %struct.btOverlapFilterCallback*, i8, [3 x i8], %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0, %class.btOverlappingPairCallback* }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btBroadphasePair*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btBroadphasePair = type { %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btCollisionAlgorithm*, %union.anon }
%class.btCollisionAlgorithm = type { i32 (...)**, %class.btDispatcher* }
%class.btDispatcher = type { i32 (...)** }
%union.anon = type { i8* }
%struct.btOverlapFilterCallback = type { i32 (...)** }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btBroadphaseRayCallback = type { %struct.btBroadphaseAabbCallback, %class.btVector3, [3 x i32], float }
%struct.btBroadphaseAabbCallback = type { i32 (...)** }
%class.btBroadphasePairSortPredicate = type { i8 }

$_ZN21btBroadphaseInterfaceC2Ev = comdat any

$_ZN17btBroadphaseProxynaEmPv = comdat any

$_ZN23btSimpleBroadphaseProxyC2Ev = comdat any

$_ZN23btSimpleBroadphaseProxy11SetNextFreeEi = comdat any

$_ZN18btSimpleBroadphase11allocHandleEv = comdat any

$_ZN17btBroadphaseProxynwEmPv = comdat any

$_ZN23btSimpleBroadphaseProxyC2ERK9btVector3S2_iPvssS3_ = comdat any

$_ZN18btSimpleBroadphase10freeHandleEP23btSimpleBroadphaseProxy = comdat any

$_ZNK18btSimpleBroadphase23getSimpleProxyFromProxyEP17btBroadphaseProxy = comdat any

$_ZN18btSimpleBroadphase23getSimpleProxyFromProxyEP17btBroadphaseProxy = comdat any

$_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_ = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvRKT_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_ = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv = comdat any

$_ZN16btBroadphasePairC2Ev = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi = comdat any

$_ZeqRK16btBroadphasePairS1_ = comdat any

$_ZN18btSimpleBroadphase23getOverlappingPairCacheEv = comdat any

$_ZNK18btSimpleBroadphase23getOverlappingPairCacheEv = comdat any

$_ZNK18btSimpleBroadphase17getBroadphaseAabbER9btVector3S1_ = comdat any

$_ZN18btSimpleBroadphase10printStatsEv = comdat any

$_ZN21btBroadphaseInterfaceD2Ev = comdat any

$_ZN21btBroadphaseInterfaceD0Ev = comdat any

$_ZN21btBroadphaseInterface9resetPoolEP12btDispatcher = comdat any

$_ZN17btBroadphaseProxyC2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK23btSimpleBroadphaseProxy11GetNextFreeEv = comdat any

$_ZN17btBroadphaseProxyC2ERK9btVector3S2_PvssS3_ = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii = comdat any

$_ZN16btBroadphasePairC2ERKS_ = comdat any

$_ZNK29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE4swapEii = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi = comdat any

$_ZN16btBroadphasePairnwEmPv = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE10deallocateEPS0_ = comdat any

$_ZTS21btBroadphaseInterface = comdat any

$_ZTI21btBroadphaseInterface = comdat any

$_ZTV21btBroadphaseInterface = comdat any

@_ZTV18btSimpleBroadphase = hidden unnamed_addr constant { [16 x i8*] } { [16 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI18btSimpleBroadphase to i8*), i8* bitcast (%class.btSimpleBroadphase* (%class.btSimpleBroadphase*)* @_ZN18btSimpleBroadphaseD1Ev to i8*), i8* bitcast (void (%class.btSimpleBroadphase*)* @_ZN18btSimpleBroadphaseD0Ev to i8*), i8* bitcast (%struct.btBroadphaseProxy* (%class.btSimpleBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i16, i16, %class.btDispatcher*, i8*)* @_ZN18btSimpleBroadphase11createProxyERK9btVector3S2_iPvssP12btDispatcherS3_ to i8*), i8* bitcast (void (%class.btSimpleBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)* @_ZN18btSimpleBroadphase12destroyProxyEP17btBroadphaseProxyP12btDispatcher to i8*), i8* bitcast (void (%class.btSimpleBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)* @_ZN18btSimpleBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcher to i8*), i8* bitcast (void (%class.btSimpleBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*)* @_ZNK18btSimpleBroadphase7getAabbEP17btBroadphaseProxyR9btVector3S3_ to i8*), i8* bitcast (void (%class.btSimpleBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)* @_ZN18btSimpleBroadphase7rayTestERK9btVector3S2_R23btBroadphaseRayCallbackS2_S2_ to i8*), i8* bitcast (void (%class.btSimpleBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)* @_ZN18btSimpleBroadphase8aabbTestERK9btVector3S2_R24btBroadphaseAabbCallback to i8*), i8* bitcast (void (%class.btSimpleBroadphase*, %class.btDispatcher*)* @_ZN18btSimpleBroadphase25calculateOverlappingPairsEP12btDispatcher to i8*), i8* bitcast (%class.btOverlappingPairCache* (%class.btSimpleBroadphase*)* @_ZN18btSimpleBroadphase23getOverlappingPairCacheEv to i8*), i8* bitcast (%class.btOverlappingPairCache* (%class.btSimpleBroadphase*)* @_ZNK18btSimpleBroadphase23getOverlappingPairCacheEv to i8*), i8* bitcast (void (%class.btSimpleBroadphase*, %class.btVector3*, %class.btVector3*)* @_ZNK18btSimpleBroadphase17getBroadphaseAabbER9btVector3S1_ to i8*), i8* bitcast (void (%class.btSimpleBroadphase*, %class.btDispatcher*)* @_ZN18btSimpleBroadphase9resetPoolEP12btDispatcher to i8*), i8* bitcast (void (%class.btSimpleBroadphase*)* @_ZN18btSimpleBroadphase10printStatsEv to i8*)] }, align 4
@gOverlappingPairs = external global i32, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS18btSimpleBroadphase = hidden constant [21 x i8] c"18btSimpleBroadphase\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS21btBroadphaseInterface = linkonce_odr hidden constant [24 x i8] c"21btBroadphaseInterface\00", comdat, align 1
@_ZTI21btBroadphaseInterface = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([24 x i8], [24 x i8]* @_ZTS21btBroadphaseInterface, i32 0, i32 0) }, comdat, align 4
@_ZTI18btSimpleBroadphase = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([21 x i8], [21 x i8]* @_ZTS18btSimpleBroadphase, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI21btBroadphaseInterface to i8*) }, align 4
@_ZTV21btBroadphaseInterface = linkonce_odr hidden unnamed_addr constant { [16 x i8*] } { [16 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI21btBroadphaseInterface to i8*), i8* bitcast (%class.btBroadphaseInterface* (%class.btBroadphaseInterface*)* @_ZN21btBroadphaseInterfaceD2Ev to i8*), i8* bitcast (void (%class.btBroadphaseInterface*)* @_ZN21btBroadphaseInterfaceD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btBroadphaseInterface*, %class.btDispatcher*)* @_ZN21btBroadphaseInterface9resetPoolEP12btDispatcher to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4

@_ZN18btSimpleBroadphaseC1EiP22btOverlappingPairCache = hidden unnamed_addr alias %class.btSimpleBroadphase* (%class.btSimpleBroadphase*, i32, %class.btOverlappingPairCache*), %class.btSimpleBroadphase* (%class.btSimpleBroadphase*, i32, %class.btOverlappingPairCache*)* @_ZN18btSimpleBroadphaseC2EiP22btOverlappingPairCache
@_ZN18btSimpleBroadphaseD1Ev = hidden unnamed_addr alias %class.btSimpleBroadphase* (%class.btSimpleBroadphase*), %class.btSimpleBroadphase* (%class.btSimpleBroadphase*)* @_ZN18btSimpleBroadphaseD2Ev

; Function Attrs: nounwind
define hidden void @_ZN18btSimpleBroadphase8validateEv(%class.btSimpleBroadphase* %this) #0 {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %j = alloca i32, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc7, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %m_numHandles = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 1
  %2 = load i32, i32* %m_numHandles, align 4, !tbaa !8
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  br label %for.end9

for.body:                                         ; preds = %for.cond
  %4 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %add = add nsw i32 %5, 1
  store i32 %add, i32* %j, align 4, !tbaa !6
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc, %for.body
  %6 = load i32, i32* %j, align 4, !tbaa !6
  %m_numHandles3 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 1
  %7 = load i32, i32* %m_numHandles3, align 4, !tbaa !8
  %cmp4 = icmp slt i32 %6, %7
  br i1 %cmp4, label %for.body6, label %for.cond.cleanup5

for.cond.cleanup5:                                ; preds = %for.cond2
  store i32 5, i32* %cleanup.dest.slot, align 4
  %8 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #9
  br label %for.end

for.body6:                                        ; preds = %for.cond2
  br label %for.inc

for.inc:                                          ; preds = %for.body6
  %9 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond2

for.end:                                          ; preds = %for.cond.cleanup5
  br label %for.inc7

for.inc7:                                         ; preds = %for.end
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc8 = add nsw i32 %10, 1
  store i32 %inc8, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end9:                                         ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

define hidden %class.btSimpleBroadphase* @_ZN18btSimpleBroadphaseC2EiP22btOverlappingPairCache(%class.btSimpleBroadphase* returned %this, i32 %maxProxies, %class.btOverlappingPairCache* %overlappingPairCache) unnamed_addr #2 {
entry:
  %retval = alloca %class.btSimpleBroadphase*, align 4
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %maxProxies.addr = alloca i32, align 4
  %overlappingPairCache.addr = alloca %class.btOverlappingPairCache*, align 4
  %mem = alloca i8*, align 4
  %i = alloca i32, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4, !tbaa !2
  store i32 %maxProxies, i32* %maxProxies.addr, align 4, !tbaa !6
  store %class.btOverlappingPairCache* %overlappingPairCache, %class.btOverlappingPairCache** %overlappingPairCache.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  store %class.btSimpleBroadphase* %this1, %class.btSimpleBroadphase** %retval, align 4
  %0 = bitcast %class.btSimpleBroadphase* %this1 to %class.btBroadphaseInterface*
  %call = call %class.btBroadphaseInterface* @_ZN21btBroadphaseInterfaceC2Ev(%class.btBroadphaseInterface* %0) #9
  %1 = bitcast %class.btSimpleBroadphase* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV18btSimpleBroadphase, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !11
  %m_pairCache = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 7
  %2 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %overlappingPairCache.addr, align 4, !tbaa !2
  store %class.btOverlappingPairCache* %2, %class.btOverlappingPairCache** %m_pairCache, align 4, !tbaa !13
  %m_ownsPairCache = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 8
  store i8 0, i8* %m_ownsPairCache, align 4, !tbaa !14
  %m_invalidPair = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 9
  store i32 0, i32* %m_invalidPair, align 4, !tbaa !15
  %3 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %overlappingPairCache.addr, align 4, !tbaa !2
  %tobool = icmp ne %class.btOverlappingPairCache* %3, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %4 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %call2 = call i8* @_Z22btAlignedAllocInternalmi(i32 76, i32 16)
  store i8* %call2, i8** %mem, align 4, !tbaa !2
  %5 = load i8*, i8** %mem, align 4, !tbaa !2
  %6 = bitcast i8* %5 to %class.btHashedOverlappingPairCache*
  %call3 = call %class.btHashedOverlappingPairCache* @_ZN28btHashedOverlappingPairCacheC1Ev(%class.btHashedOverlappingPairCache* %6)
  %7 = bitcast %class.btHashedOverlappingPairCache* %6 to %class.btOverlappingPairCache*
  %m_pairCache4 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 7
  store %class.btOverlappingPairCache* %7, %class.btOverlappingPairCache** %m_pairCache4, align 4, !tbaa !13
  %m_ownsPairCache5 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 8
  store i8 1, i8* %m_ownsPairCache5, align 4, !tbaa !14
  %8 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %9 = load i32, i32* %maxProxies.addr, align 4, !tbaa !6
  %mul = mul i32 52, %9
  %call6 = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %m_pHandlesRawPtr = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 5
  store i8* %call6, i8** %m_pHandlesRawPtr, align 4, !tbaa !16
  %10 = load i32, i32* %maxProxies.addr, align 4, !tbaa !6
  %11 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %10, i32 52)
  %12 = extractvalue { i32, i1 } %11, 1
  %13 = extractvalue { i32, i1 } %11, 0
  %14 = select i1 %12, i32 -1, i32 %13
  %m_pHandlesRawPtr7 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 5
  %15 = load i8*, i8** %m_pHandlesRawPtr7, align 4, !tbaa !16
  %call8 = call i8* @_ZN17btBroadphaseProxynaEmPv(i32 %14, i8* %15)
  %16 = bitcast i8* %call8 to %struct.btSimpleBroadphaseProxy*
  %isempty = icmp eq i32 %10, 0
  br i1 %isempty, label %arrayctor.cont, label %new.ctorloop

new.ctorloop:                                     ; preds = %if.end
  %arrayctor.end = getelementptr inbounds %struct.btSimpleBroadphaseProxy, %struct.btSimpleBroadphaseProxy* %16, i32 %10
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %new.ctorloop
  %arrayctor.cur = phi %struct.btSimpleBroadphaseProxy* [ %16, %new.ctorloop ], [ %arrayctor.next, %arrayctor.loop ]
  %call9 = call %struct.btSimpleBroadphaseProxy* @_ZN23btSimpleBroadphaseProxyC2Ev(%struct.btSimpleBroadphaseProxy* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %struct.btSimpleBroadphaseProxy, %struct.btSimpleBroadphaseProxy* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %struct.btSimpleBroadphaseProxy* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %if.end, %arrayctor.loop
  %m_pHandles = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 4
  store %struct.btSimpleBroadphaseProxy* %16, %struct.btSimpleBroadphaseProxy** %m_pHandles, align 4, !tbaa !17
  %17 = load i32, i32* %maxProxies.addr, align 4, !tbaa !6
  %m_maxHandles = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 2
  store i32 %17, i32* %m_maxHandles, align 4, !tbaa !18
  %m_numHandles = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 1
  store i32 0, i32* %m_numHandles, align 4, !tbaa !8
  %m_firstFreeHandle = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 6
  store i32 0, i32* %m_firstFreeHandle, align 4, !tbaa !19
  %m_LastHandleIndex = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 3
  store i32 -1, i32* %m_LastHandleIndex, align 4, !tbaa !20
  %18 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #9
  %m_firstFreeHandle10 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 6
  %19 = load i32, i32* %m_firstFreeHandle10, align 4, !tbaa !19
  store i32 %19, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %arrayctor.cont
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %21 = load i32, i32* %maxProxies.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %20, %21
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %22 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_pHandles11 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 4
  %23 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %m_pHandles11, align 4, !tbaa !17
  %24 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btSimpleBroadphaseProxy, %struct.btSimpleBroadphaseProxy* %23, i32 %24
  %25 = load i32, i32* %i, align 4, !tbaa !6
  %add = add nsw i32 %25, 1
  call void @_ZN23btSimpleBroadphaseProxy11SetNextFreeEi(%struct.btSimpleBroadphaseProxy* %arrayidx, i32 %add)
  %26 = load i32, i32* %i, align 4, !tbaa !6
  %add12 = add nsw i32 %26, 2
  %m_pHandles13 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 4
  %27 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %m_pHandles13, align 4, !tbaa !17
  %28 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx14 = getelementptr inbounds %struct.btSimpleBroadphaseProxy, %struct.btSimpleBroadphaseProxy* %27, i32 %28
  %29 = bitcast %struct.btSimpleBroadphaseProxy* %arrayidx14 to %struct.btBroadphaseProxy*
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %29, i32 0, i32 4
  store i32 %add12, i32* %m_uniqueId, align 4, !tbaa !21
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %30 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %30, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %m_pHandles15 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 4
  %31 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %m_pHandles15, align 4, !tbaa !17
  %32 = load i32, i32* %maxProxies.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %32, 1
  %arrayidx16 = getelementptr inbounds %struct.btSimpleBroadphaseProxy, %struct.btSimpleBroadphaseProxy* %31, i32 %sub
  call void @_ZN23btSimpleBroadphaseProxy11SetNextFreeEi(%struct.btSimpleBroadphaseProxy* %arrayidx16, i32 0)
  %33 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %retval, align 4
  ret %class.btSimpleBroadphase* %33
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btBroadphaseInterface* @_ZN21btBroadphaseInterfaceC2Ev(%class.btBroadphaseInterface* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btBroadphaseInterface*, align 4
  store %class.btBroadphaseInterface* %this, %class.btBroadphaseInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %this.addr, align 4
  %0 = bitcast %class.btBroadphaseInterface* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV21btBroadphaseInterface, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !11
  ret %class.btBroadphaseInterface* %this1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #4

declare %class.btHashedOverlappingPairCache* @_ZN28btHashedOverlappingPairCacheC1Ev(%class.btHashedOverlappingPairCache* returned) unnamed_addr #4

; Function Attrs: nounwind readnone speculatable willreturn
declare { i32, i1 } @llvm.umul.with.overflow.i32(i32, i32) #5

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN17btBroadphaseProxynaEmPv(i32 %0, i8* %ptr) #3 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !25
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

define linkonce_odr hidden %struct.btSimpleBroadphaseProxy* @_ZN23btSimpleBroadphaseProxyC2Ev(%struct.btSimpleBroadphaseProxy* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btSimpleBroadphaseProxy*, align 4
  store %struct.btSimpleBroadphaseProxy* %this, %struct.btSimpleBroadphaseProxy** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %this.addr, align 4
  %0 = bitcast %struct.btSimpleBroadphaseProxy* %this1 to %struct.btBroadphaseProxy*
  %call = call %struct.btBroadphaseProxy* @_ZN17btBroadphaseProxyC2Ev(%struct.btBroadphaseProxy* %0)
  ret %struct.btSimpleBroadphaseProxy* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN23btSimpleBroadphaseProxy11SetNextFreeEi(%struct.btSimpleBroadphaseProxy* %this, i32 %next) #3 comdat {
entry:
  %this.addr = alloca %struct.btSimpleBroadphaseProxy*, align 4
  %next.addr = alloca i32, align 4
  store %struct.btSimpleBroadphaseProxy* %this, %struct.btSimpleBroadphaseProxy** %this.addr, align 4, !tbaa !2
  store i32 %next, i32* %next.addr, align 4, !tbaa !6
  %this1 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %this.addr, align 4
  %0 = load i32, i32* %next.addr, align 4, !tbaa !6
  %m_nextFree = getelementptr inbounds %struct.btSimpleBroadphaseProxy, %struct.btSimpleBroadphaseProxy* %this1, i32 0, i32 1
  store i32 %0, i32* %m_nextFree, align 4, !tbaa !27
  ret void
}

; Function Attrs: nounwind
define hidden %class.btSimpleBroadphase* @_ZN18btSimpleBroadphaseD2Ev(%class.btSimpleBroadphase* returned %this) unnamed_addr #0 {
entry:
  %retval = alloca %class.btSimpleBroadphase*, align 4
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  store %class.btSimpleBroadphase* %this1, %class.btSimpleBroadphase** %retval, align 4
  %0 = bitcast %class.btSimpleBroadphase* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV18btSimpleBroadphase, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !11
  %m_pHandlesRawPtr = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 5
  %1 = load i8*, i8** %m_pHandlesRawPtr, align 4, !tbaa !16
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  %m_ownsPairCache = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 8
  %2 = load i8, i8* %m_ownsPairCache, align 4, !tbaa !14, !range !29
  %tobool = trunc i8 %2 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_pairCache = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 7
  %3 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4, !tbaa !13
  %4 = bitcast %class.btOverlappingPairCache* %3 to %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)***
  %vtable = load %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)**, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*** %4, align 4, !tbaa !11
  %vfn = getelementptr inbounds %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)** %vtable, i64 0
  %5 = load %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)** %vfn, align 4
  %call = call %class.btOverlappingPairCache* %5(%class.btOverlappingPairCache* %3) #9
  %m_pairCache2 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 7
  %6 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache2, align 4, !tbaa !13
  %7 = bitcast %class.btOverlappingPairCache* %6 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %7)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %8 = bitcast %class.btSimpleBroadphase* %this1 to %class.btBroadphaseInterface*
  %call3 = call %class.btBroadphaseInterface* @_ZN21btBroadphaseInterfaceD2Ev(%class.btBroadphaseInterface* %8) #9
  %9 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %retval, align 4
  ret %class.btSimpleBroadphase* %9
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #4

; Function Attrs: nounwind
define hidden void @_ZN18btSimpleBroadphaseD0Ev(%class.btSimpleBroadphase* %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %call = call %class.btSimpleBroadphase* @_ZN18btSimpleBroadphaseD1Ev(%class.btSimpleBroadphase* %this1) #9
  %0 = bitcast %class.btSimpleBroadphase* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #6

define hidden %struct.btBroadphaseProxy* @_ZN18btSimpleBroadphase11createProxyERK9btVector3S2_iPvssP12btDispatcherS3_(%class.btSimpleBroadphase* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, i32 %shapeType, i8* %userPtr, i16 signext %collisionFilterGroup, i16 signext %collisionFilterMask, %class.btDispatcher* %0, i8* %multiSapProxy) unnamed_addr #2 {
entry:
  %retval = alloca %struct.btBroadphaseProxy*, align 4
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %shapeType.addr = alloca i32, align 4
  %userPtr.addr = alloca i8*, align 4
  %collisionFilterGroup.addr = alloca i16, align 2
  %collisionFilterMask.addr = alloca i16, align 2
  %.addr = alloca %class.btDispatcher*, align 4
  %multiSapProxy.addr = alloca i8*, align 4
  %newHandleIndex = alloca i32, align 4
  %proxy = alloca %struct.btSimpleBroadphaseProxy*, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  store i32 %shapeType, i32* %shapeType.addr, align 4, !tbaa !6
  store i8* %userPtr, i8** %userPtr.addr, align 4, !tbaa !2
  store i16 %collisionFilterGroup, i16* %collisionFilterGroup.addr, align 2, !tbaa !30
  store i16 %collisionFilterMask, i16* %collisionFilterMask.addr, align 2, !tbaa !30
  store %class.btDispatcher* %0, %class.btDispatcher** %.addr, align 4, !tbaa !2
  store i8* %multiSapProxy, i8** %multiSapProxy.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %m_numHandles = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 1
  %1 = load i32, i32* %m_numHandles, align 4, !tbaa !8
  %m_maxHandles = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_maxHandles, align 4, !tbaa !18
  %cmp = icmp sge i32 %1, %2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = bitcast i32* %newHandleIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %call = call i32 @_ZN18btSimpleBroadphase11allocHandleEv(%class.btSimpleBroadphase* %this1)
  store i32 %call, i32* %newHandleIndex, align 4, !tbaa !6
  %4 = bitcast %struct.btSimpleBroadphaseProxy** %proxy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %m_pHandles = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 4
  %5 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %m_pHandles, align 4, !tbaa !17
  %6 = load i32, i32* %newHandleIndex, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btSimpleBroadphaseProxy, %struct.btSimpleBroadphaseProxy* %5, i32 %6
  %7 = bitcast %struct.btSimpleBroadphaseProxy* %arrayidx to i8*
  %call2 = call i8* @_ZN17btBroadphaseProxynwEmPv(i32 52, i8* %7)
  %8 = bitcast i8* %call2 to %struct.btSimpleBroadphaseProxy*
  %9 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %10 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %11 = load i32, i32* %shapeType.addr, align 4, !tbaa !6
  %12 = load i8*, i8** %userPtr.addr, align 4, !tbaa !2
  %13 = load i16, i16* %collisionFilterGroup.addr, align 2, !tbaa !30
  %14 = load i16, i16* %collisionFilterMask.addr, align 2, !tbaa !30
  %15 = load i8*, i8** %multiSapProxy.addr, align 4, !tbaa !2
  %call3 = call %struct.btSimpleBroadphaseProxy* @_ZN23btSimpleBroadphaseProxyC2ERK9btVector3S2_iPvssS3_(%struct.btSimpleBroadphaseProxy* %8, %class.btVector3* nonnull align 4 dereferenceable(16) %9, %class.btVector3* nonnull align 4 dereferenceable(16) %10, i32 %11, i8* %12, i16 signext %13, i16 signext %14, i8* %15)
  store %struct.btSimpleBroadphaseProxy* %8, %struct.btSimpleBroadphaseProxy** %proxy, align 4, !tbaa !2
  %16 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy, align 4, !tbaa !2
  %17 = bitcast %struct.btSimpleBroadphaseProxy* %16 to %struct.btBroadphaseProxy*
  store %struct.btBroadphaseProxy* %17, %struct.btBroadphaseProxy** %retval, align 4
  %18 = bitcast %struct.btSimpleBroadphaseProxy** %proxy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #9
  %19 = bitcast i32* %newHandleIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #9
  br label %return

return:                                           ; preds = %if.end, %if.then
  %20 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %retval, align 4
  ret %struct.btBroadphaseProxy* %20
}

define linkonce_odr hidden i32 @_ZN18btSimpleBroadphase11allocHandleEv(%class.btSimpleBroadphase* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %freeHandle = alloca i32, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %0 = bitcast i32* %freeHandle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %m_firstFreeHandle = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 6
  %1 = load i32, i32* %m_firstFreeHandle, align 4, !tbaa !19
  store i32 %1, i32* %freeHandle, align 4, !tbaa !6
  %m_pHandles = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 4
  %2 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %m_pHandles, align 4, !tbaa !17
  %3 = load i32, i32* %freeHandle, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btSimpleBroadphaseProxy, %struct.btSimpleBroadphaseProxy* %2, i32 %3
  %call = call i32 @_ZNK23btSimpleBroadphaseProxy11GetNextFreeEv(%struct.btSimpleBroadphaseProxy* %arrayidx)
  %m_firstFreeHandle2 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 6
  store i32 %call, i32* %m_firstFreeHandle2, align 4, !tbaa !19
  %m_numHandles = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 1
  %4 = load i32, i32* %m_numHandles, align 4, !tbaa !8
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %m_numHandles, align 4, !tbaa !8
  %5 = load i32, i32* %freeHandle, align 4, !tbaa !6
  %m_LastHandleIndex = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 3
  %6 = load i32, i32* %m_LastHandleIndex, align 4, !tbaa !20
  %cmp = icmp sgt i32 %5, %6
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load i32, i32* %freeHandle, align 4, !tbaa !6
  %m_LastHandleIndex3 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 3
  store i32 %7, i32* %m_LastHandleIndex3, align 4, !tbaa !20
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %8 = load i32, i32* %freeHandle, align 4, !tbaa !6
  %9 = bitcast i32* %freeHandle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  ret i32 %8
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN17btBroadphaseProxynwEmPv(i32 %0, i8* %ptr) #3 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !25
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

define linkonce_odr hidden %struct.btSimpleBroadphaseProxy* @_ZN23btSimpleBroadphaseProxyC2ERK9btVector3S2_iPvssS3_(%struct.btSimpleBroadphaseProxy* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %minpt, %class.btVector3* nonnull align 4 dereferenceable(16) %maxpt, i32 %shapeType, i8* %userPtr, i16 signext %collisionFilterGroup, i16 signext %collisionFilterMask, i8* %multiSapProxy) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btSimpleBroadphaseProxy*, align 4
  %minpt.addr = alloca %class.btVector3*, align 4
  %maxpt.addr = alloca %class.btVector3*, align 4
  %shapeType.addr = alloca i32, align 4
  %userPtr.addr = alloca i8*, align 4
  %collisionFilterGroup.addr = alloca i16, align 2
  %collisionFilterMask.addr = alloca i16, align 2
  %multiSapProxy.addr = alloca i8*, align 4
  store %struct.btSimpleBroadphaseProxy* %this, %struct.btSimpleBroadphaseProxy** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %minpt, %class.btVector3** %minpt.addr, align 4, !tbaa !2
  store %class.btVector3* %maxpt, %class.btVector3** %maxpt.addr, align 4, !tbaa !2
  store i32 %shapeType, i32* %shapeType.addr, align 4, !tbaa !6
  store i8* %userPtr, i8** %userPtr.addr, align 4, !tbaa !2
  store i16 %collisionFilterGroup, i16* %collisionFilterGroup.addr, align 2, !tbaa !30
  store i16 %collisionFilterMask, i16* %collisionFilterMask.addr, align 2, !tbaa !30
  store i8* %multiSapProxy, i8** %multiSapProxy.addr, align 4, !tbaa !2
  %this1 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %this.addr, align 4
  %0 = bitcast %struct.btSimpleBroadphaseProxy* %this1 to %struct.btBroadphaseProxy*
  %1 = load %class.btVector3*, %class.btVector3** %minpt.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %maxpt.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %userPtr.addr, align 4, !tbaa !2
  %4 = load i16, i16* %collisionFilterGroup.addr, align 2, !tbaa !30
  %5 = load i16, i16* %collisionFilterMask.addr, align 2, !tbaa !30
  %6 = load i8*, i8** %multiSapProxy.addr, align 4, !tbaa !2
  %call = call %struct.btBroadphaseProxy* @_ZN17btBroadphaseProxyC2ERK9btVector3S2_PvssS3_(%struct.btBroadphaseProxy* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, i8* %3, i16 signext %4, i16 signext %5, i8* %6)
  ret %struct.btSimpleBroadphaseProxy* %this1
}

define hidden void @_ZN18btSimpleBroadphase12destroyProxyEP17btBroadphaseProxyP12btDispatcher(%class.btSimpleBroadphase* %this, %struct.btBroadphaseProxy* %proxyOrg, %class.btDispatcher* %dispatcher) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %proxyOrg.addr = alloca %struct.btBroadphaseProxy*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %proxy0 = alloca %struct.btSimpleBroadphaseProxy*, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxyOrg, %struct.btBroadphaseProxy** %proxyOrg.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %0 = bitcast %struct.btSimpleBroadphaseProxy** %proxy0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxyOrg.addr, align 4, !tbaa !2
  %2 = bitcast %struct.btBroadphaseProxy* %1 to %struct.btSimpleBroadphaseProxy*
  store %struct.btSimpleBroadphaseProxy* %2, %struct.btSimpleBroadphaseProxy** %proxy0, align 4, !tbaa !2
  %3 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy0, align 4, !tbaa !2
  call void @_ZN18btSimpleBroadphase10freeHandleEP23btSimpleBroadphaseProxy(%class.btSimpleBroadphase* %this1, %struct.btSimpleBroadphaseProxy* %3)
  %m_pairCache = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 7
  %4 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4, !tbaa !13
  %5 = bitcast %class.btOverlappingPairCache* %4 to %class.btOverlappingPairCallback*
  %6 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxyOrg.addr, align 4, !tbaa !2
  %7 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %8 = bitcast %class.btOverlappingPairCallback* %5 to void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable = load void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %8, align 4, !tbaa !11
  %vfn = getelementptr inbounds void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable, i64 4
  %9 = load void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn, align 4
  call void %9(%class.btOverlappingPairCallback* %5, %struct.btBroadphaseProxy* %6, %class.btDispatcher* %7)
  %10 = bitcast %struct.btSimpleBroadphaseProxy** %proxy0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN18btSimpleBroadphase10freeHandleEP23btSimpleBroadphaseProxy(%class.btSimpleBroadphase* %this, %struct.btSimpleBroadphaseProxy* %proxy) #0 comdat {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %proxy.addr = alloca %struct.btSimpleBroadphaseProxy*, align 4
  %handle = alloca i32, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4, !tbaa !2
  store %struct.btSimpleBroadphaseProxy* %proxy, %struct.btSimpleBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %0 = bitcast i32* %handle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  %m_pHandles = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 4
  %2 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %m_pHandles, align 4, !tbaa !17
  %sub.ptr.lhs.cast = ptrtoint %struct.btSimpleBroadphaseProxy* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %struct.btSimpleBroadphaseProxy* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 52
  store i32 %sub.ptr.div, i32* %handle, align 4, !tbaa !6
  %3 = load i32, i32* %handle, align 4, !tbaa !6
  %m_LastHandleIndex = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 3
  %4 = load i32, i32* %m_LastHandleIndex, align 4, !tbaa !20
  %cmp = icmp eq i32 %3, %4
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_LastHandleIndex2 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 3
  %5 = load i32, i32* %m_LastHandleIndex2, align 4, !tbaa !20
  %dec = add nsw i32 %5, -1
  store i32 %dec, i32* %m_LastHandleIndex2, align 4, !tbaa !20
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %6 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  %m_firstFreeHandle = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 6
  %7 = load i32, i32* %m_firstFreeHandle, align 4, !tbaa !19
  call void @_ZN23btSimpleBroadphaseProxy11SetNextFreeEi(%struct.btSimpleBroadphaseProxy* %6, i32 %7)
  %8 = load i32, i32* %handle, align 4, !tbaa !6
  %m_firstFreeHandle3 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 6
  store i32 %8, i32* %m_firstFreeHandle3, align 4, !tbaa !19
  %9 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  %10 = bitcast %struct.btSimpleBroadphaseProxy* %9 to %struct.btBroadphaseProxy*
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %10, i32 0, i32 0
  store i8* null, i8** %m_clientObject, align 4, !tbaa !31
  %m_numHandles = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 1
  %11 = load i32, i32* %m_numHandles, align 4, !tbaa !8
  %dec4 = add nsw i32 %11, -1
  store i32 %dec4, i32* %m_numHandles, align 4, !tbaa !8
  %12 = bitcast i32* %handle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  ret void
}

define hidden void @_ZNK18btSimpleBroadphase7getAabbEP17btBroadphaseProxyR9btVector3S3_(%class.btSimpleBroadphase* %this, %struct.btBroadphaseProxy* %proxy, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %sbp = alloca %struct.btSimpleBroadphaseProxy*, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %0 = bitcast %struct.btSimpleBroadphaseProxy** %sbp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  %call = call %struct.btSimpleBroadphaseProxy* @_ZNK18btSimpleBroadphase23getSimpleProxyFromProxyEP17btBroadphaseProxy(%class.btSimpleBroadphase* %this1, %struct.btBroadphaseProxy* %1)
  store %struct.btSimpleBroadphaseProxy* %call, %struct.btSimpleBroadphaseProxy** %sbp, align 4, !tbaa !2
  %2 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %sbp, align 4, !tbaa !2
  %3 = bitcast %struct.btSimpleBroadphaseProxy* %2 to %struct.btBroadphaseProxy*
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %3, i32 0, i32 5
  %4 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %5 = bitcast %class.btVector3* %4 to i8*
  %6 = bitcast %class.btVector3* %m_aabbMin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false), !tbaa.struct !32
  %7 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %sbp, align 4, !tbaa !2
  %8 = bitcast %struct.btSimpleBroadphaseProxy* %7 to %struct.btBroadphaseProxy*
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %8, i32 0, i32 6
  %9 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %10 = bitcast %class.btVector3* %9 to i8*
  %11 = bitcast %class.btVector3* %m_aabbMax to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !32
  %12 = bitcast %struct.btSimpleBroadphaseProxy** %sbp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %struct.btSimpleBroadphaseProxy* @_ZNK18btSimpleBroadphase23getSimpleProxyFromProxyEP17btBroadphaseProxy(%class.btSimpleBroadphase* %this, %struct.btBroadphaseProxy* %proxy) #3 comdat {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy0 = alloca %struct.btSimpleBroadphaseProxy*, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %0 = bitcast %struct.btSimpleBroadphaseProxy** %proxy0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  %2 = bitcast %struct.btBroadphaseProxy* %1 to %struct.btSimpleBroadphaseProxy*
  store %struct.btSimpleBroadphaseProxy* %2, %struct.btSimpleBroadphaseProxy** %proxy0, align 4, !tbaa !2
  %3 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy0, align 4, !tbaa !2
  %4 = bitcast %struct.btSimpleBroadphaseProxy** %proxy0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  ret %struct.btSimpleBroadphaseProxy* %3
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

define hidden void @_ZN18btSimpleBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcher(%class.btSimpleBroadphase* %this, %struct.btBroadphaseProxy* %proxy, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, %class.btDispatcher* %0) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %.addr = alloca %class.btDispatcher*, align 4
  %sbp = alloca %struct.btSimpleBroadphaseProxy*, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  store %class.btDispatcher* %0, %class.btDispatcher** %.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %1 = bitcast %struct.btSimpleBroadphaseProxy** %sbp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  %call = call %struct.btSimpleBroadphaseProxy* @_ZN18btSimpleBroadphase23getSimpleProxyFromProxyEP17btBroadphaseProxy(%class.btSimpleBroadphase* %this1, %struct.btBroadphaseProxy* %2)
  store %struct.btSimpleBroadphaseProxy* %call, %struct.btSimpleBroadphaseProxy** %sbp, align 4, !tbaa !2
  %3 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %4 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %sbp, align 4, !tbaa !2
  %5 = bitcast %struct.btSimpleBroadphaseProxy* %4 to %struct.btBroadphaseProxy*
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %5, i32 0, i32 5
  %6 = bitcast %class.btVector3* %m_aabbMin to i8*
  %7 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !32
  %8 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %9 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %sbp, align 4, !tbaa !2
  %10 = bitcast %struct.btSimpleBroadphaseProxy* %9 to %struct.btBroadphaseProxy*
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %10, i32 0, i32 6
  %11 = bitcast %class.btVector3* %m_aabbMax to i8*
  %12 = bitcast %class.btVector3* %8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 16, i1 false), !tbaa.struct !32
  %13 = bitcast %struct.btSimpleBroadphaseProxy** %sbp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %struct.btSimpleBroadphaseProxy* @_ZN18btSimpleBroadphase23getSimpleProxyFromProxyEP17btBroadphaseProxy(%class.btSimpleBroadphase* %this, %struct.btBroadphaseProxy* %proxy) #3 comdat {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy0 = alloca %struct.btSimpleBroadphaseProxy*, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %0 = bitcast %struct.btSimpleBroadphaseProxy** %proxy0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  %2 = bitcast %struct.btBroadphaseProxy* %1 to %struct.btSimpleBroadphaseProxy*
  store %struct.btSimpleBroadphaseProxy* %2, %struct.btSimpleBroadphaseProxy** %proxy0, align 4, !tbaa !2
  %3 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy0, align 4, !tbaa !2
  %4 = bitcast %struct.btSimpleBroadphaseProxy** %proxy0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  ret %struct.btSimpleBroadphaseProxy* %3
}

define hidden void @_ZN18btSimpleBroadphase7rayTestERK9btVector3S2_R23btBroadphaseRayCallbackS2_S2_(%class.btSimpleBroadphase* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %rayFrom, %class.btVector3* nonnull align 4 dereferenceable(16) %rayTo, %struct.btBroadphaseRayCallback* nonnull align 4 dereferenceable(36) %rayCallback, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %rayFrom.addr = alloca %class.btVector3*, align 4
  %rayTo.addr = alloca %class.btVector3*, align 4
  %rayCallback.addr = alloca %struct.btBroadphaseRayCallback*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %proxy = alloca %struct.btSimpleBroadphaseProxy*, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %rayFrom, %class.btVector3** %rayFrom.addr, align 4, !tbaa !2
  store %class.btVector3* %rayTo, %class.btVector3** %rayTo.addr, align 4, !tbaa !2
  store %struct.btBroadphaseRayCallback* %rayCallback, %struct.btBroadphaseRayCallback** %rayCallback.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %m_LastHandleIndex = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 3
  %2 = load i32, i32* %m_LastHandleIndex, align 4, !tbaa !20
  %cmp = icmp sle i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = bitcast %struct.btSimpleBroadphaseProxy** %proxy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %m_pHandles = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 4
  %5 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %m_pHandles, align 4, !tbaa !17
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btSimpleBroadphaseProxy, %struct.btSimpleBroadphaseProxy* %5, i32 %6
  store %struct.btSimpleBroadphaseProxy* %arrayidx, %struct.btSimpleBroadphaseProxy** %proxy, align 4, !tbaa !2
  %7 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy, align 4, !tbaa !2
  %8 = bitcast %struct.btSimpleBroadphaseProxy* %7 to %struct.btBroadphaseProxy*
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %8, i32 0, i32 0
  %9 = load i8*, i8** %m_clientObject, align 4, !tbaa !31
  %tobool = icmp ne i8* %9, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %for.body
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body
  %10 = load %struct.btBroadphaseRayCallback*, %struct.btBroadphaseRayCallback** %rayCallback.addr, align 4, !tbaa !2
  %11 = bitcast %struct.btBroadphaseRayCallback* %10 to %struct.btBroadphaseAabbCallback*
  %12 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy, align 4, !tbaa !2
  %13 = bitcast %struct.btSimpleBroadphaseProxy* %12 to %struct.btBroadphaseProxy*
  %14 = bitcast %struct.btBroadphaseAabbCallback* %11 to i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)***
  %vtable = load i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)**, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*** %14, align 4, !tbaa !11
  %vfn = getelementptr inbounds i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)** %vtable, i64 2
  %15 = load i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call = call zeroext i1 %15(%struct.btBroadphaseAabbCallback* %11, %struct.btBroadphaseProxy* %13)
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %16 = bitcast %struct.btSimpleBroadphaseProxy** %proxy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 4, label %for.inc
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont, %cleanup
  %17 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

define hidden void @_ZN18btSimpleBroadphase8aabbTestERK9btVector3S2_R24btBroadphaseAabbCallback(%class.btSimpleBroadphase* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, %struct.btBroadphaseAabbCallback* nonnull align 4 dereferenceable(4) %callback) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %callback.addr = alloca %struct.btBroadphaseAabbCallback*, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %proxy = alloca %struct.btSimpleBroadphaseProxy*, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  store %struct.btBroadphaseAabbCallback* %callback, %struct.btBroadphaseAabbCallback** %callback.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %m_LastHandleIndex = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 3
  %2 = load i32, i32* %m_LastHandleIndex, align 4, !tbaa !20
  %cmp = icmp sle i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = bitcast %struct.btSimpleBroadphaseProxy** %proxy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %m_pHandles = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 4
  %5 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %m_pHandles, align 4, !tbaa !17
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btSimpleBroadphaseProxy, %struct.btSimpleBroadphaseProxy* %5, i32 %6
  store %struct.btSimpleBroadphaseProxy* %arrayidx, %struct.btSimpleBroadphaseProxy** %proxy, align 4, !tbaa !2
  %7 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy, align 4, !tbaa !2
  %8 = bitcast %struct.btSimpleBroadphaseProxy* %7 to %struct.btBroadphaseProxy*
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %8, i32 0, i32 0
  %9 = load i8*, i8** %m_clientObject, align 4, !tbaa !31
  %tobool = icmp ne i8* %9, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %for.body
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body
  %10 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %11 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %12 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy, align 4, !tbaa !2
  %13 = bitcast %struct.btSimpleBroadphaseProxy* %12 to %struct.btBroadphaseProxy*
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %13, i32 0, i32 5
  %14 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy, align 4, !tbaa !2
  %15 = bitcast %struct.btSimpleBroadphaseProxy* %14 to %struct.btBroadphaseProxy*
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %15, i32 0, i32 6
  %call = call zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %10, %class.btVector3* nonnull align 4 dereferenceable(16) %11, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMax)
  br i1 %call, label %if.then2, label %if.end4

if.then2:                                         ; preds = %if.end
  %16 = load %struct.btBroadphaseAabbCallback*, %struct.btBroadphaseAabbCallback** %callback.addr, align 4, !tbaa !2
  %17 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy, align 4, !tbaa !2
  %18 = bitcast %struct.btSimpleBroadphaseProxy* %17 to %struct.btBroadphaseProxy*
  %19 = bitcast %struct.btBroadphaseAabbCallback* %16 to i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)***
  %vtable = load i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)**, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*** %19, align 4, !tbaa !11
  %vfn = getelementptr inbounds i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)** %vtable, i64 2
  %20 = load i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call3 = call zeroext i1 %20(%struct.btBroadphaseAabbCallback* %16, %struct.btBroadphaseProxy* %18)
  br label %if.end4

if.end4:                                          ; preds = %if.then2, %if.end
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end4, %if.then
  %21 = bitcast %struct.btSimpleBroadphaseProxy** %proxy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #9
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 4, label %for.inc
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont, %cleanup
  %22 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %22, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin2, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax2) #7 comdat {
entry:
  %aabbMin1.addr = alloca %class.btVector3*, align 4
  %aabbMax1.addr = alloca %class.btVector3*, align 4
  %aabbMin2.addr = alloca %class.btVector3*, align 4
  %aabbMax2.addr = alloca %class.btVector3*, align 4
  %overlap = alloca i8, align 1
  store %class.btVector3* %aabbMin1, %class.btVector3** %aabbMin1.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax1, %class.btVector3** %aabbMax1.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin2, %class.btVector3** %aabbMin2.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax2, %class.btVector3** %aabbMax2.addr, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %overlap) #9
  store i8 1, i8* %overlap, align 1, !tbaa !34
  %0 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %0)
  %1 = load float, float* %call, align 4, !tbaa !35
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4, !tbaa !2
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %2)
  %3 = load float, float* %call1, align 4, !tbaa !35
  %cmp = fcmp ogt float %1, %3
  br i1 %cmp, label %cond.true, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %4 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %4)
  %5 = load float, float* %call2, align 4, !tbaa !35
  %6 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %6)
  %7 = load float, float* %call3, align 4, !tbaa !35
  %cmp4 = fcmp olt float %5, %7
  br i1 %cmp4, label %cond.true, label %cond.false

cond.true:                                        ; preds = %lor.lhs.false, %entry
  br label %cond.end

cond.false:                                       ; preds = %lor.lhs.false
  %8 = load i8, i8* %overlap, align 1, !tbaa !34, !range !29
  %tobool = trunc i8 %8 to i1
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i1 [ false, %cond.true ], [ %tobool, %cond.false ]
  %frombool = zext i1 %cond to i8
  store i8 %frombool, i8* %overlap, align 1, !tbaa !34
  %9 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4, !tbaa !2
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %9)
  %10 = load float, float* %call5, align 4, !tbaa !35
  %11 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %11)
  %12 = load float, float* %call6, align 4, !tbaa !35
  %cmp7 = fcmp ogt float %10, %12
  br i1 %cmp7, label %cond.true12, label %lor.lhs.false8

lor.lhs.false8:                                   ; preds = %cond.end
  %13 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %13)
  %14 = load float, float* %call9, align 4, !tbaa !35
  %15 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4, !tbaa !2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %15)
  %16 = load float, float* %call10, align 4, !tbaa !35
  %cmp11 = fcmp olt float %14, %16
  br i1 %cmp11, label %cond.true12, label %cond.false13

cond.true12:                                      ; preds = %lor.lhs.false8, %cond.end
  br label %cond.end15

cond.false13:                                     ; preds = %lor.lhs.false8
  %17 = load i8, i8* %overlap, align 1, !tbaa !34, !range !29
  %tobool14 = trunc i8 %17 to i1
  br label %cond.end15

cond.end15:                                       ; preds = %cond.false13, %cond.true12
  %cond16 = phi i1 [ false, %cond.true12 ], [ %tobool14, %cond.false13 ]
  %frombool17 = zext i1 %cond16 to i8
  store i8 %frombool17, i8* %overlap, align 1, !tbaa !34
  %18 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4, !tbaa !2
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %18)
  %19 = load float, float* %call18, align 4, !tbaa !35
  %20 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4, !tbaa !2
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %20)
  %21 = load float, float* %call19, align 4, !tbaa !35
  %cmp20 = fcmp ogt float %19, %21
  br i1 %cmp20, label %cond.true25, label %lor.lhs.false21

lor.lhs.false21:                                  ; preds = %cond.end15
  %22 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4, !tbaa !2
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %22)
  %23 = load float, float* %call22, align 4, !tbaa !35
  %24 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4, !tbaa !2
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %24)
  %25 = load float, float* %call23, align 4, !tbaa !35
  %cmp24 = fcmp olt float %23, %25
  br i1 %cmp24, label %cond.true25, label %cond.false26

cond.true25:                                      ; preds = %lor.lhs.false21, %cond.end15
  br label %cond.end28

cond.false26:                                     ; preds = %lor.lhs.false21
  %26 = load i8, i8* %overlap, align 1, !tbaa !34, !range !29
  %tobool27 = trunc i8 %26 to i1
  br label %cond.end28

cond.end28:                                       ; preds = %cond.false26, %cond.true25
  %cond29 = phi i1 [ false, %cond.true25 ], [ %tobool27, %cond.false26 ]
  %frombool30 = zext i1 %cond29 to i8
  store i8 %frombool30, i8* %overlap, align 1, !tbaa !34
  %27 = load i8, i8* %overlap, align 1, !tbaa !34, !range !29
  %tobool31 = trunc i8 %27 to i1
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %overlap) #9
  ret i1 %tobool31
}

define hidden zeroext i1 @_ZN18btSimpleBroadphase11aabbOverlapEP23btSimpleBroadphaseProxyS1_(%struct.btSimpleBroadphaseProxy* %proxy0, %struct.btSimpleBroadphaseProxy* %proxy1) #2 {
entry:
  %proxy0.addr = alloca %struct.btSimpleBroadphaseProxy*, align 4
  %proxy1.addr = alloca %struct.btSimpleBroadphaseProxy*, align 4
  store %struct.btSimpleBroadphaseProxy* %proxy0, %struct.btSimpleBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  store %struct.btSimpleBroadphaseProxy* %proxy1, %struct.btSimpleBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %0 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btSimpleBroadphaseProxy* %0 to %struct.btBroadphaseProxy*
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %1, i32 0, i32 5
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_aabbMin)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !35
  %3 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %4 = bitcast %struct.btSimpleBroadphaseProxy* %3 to %struct.btBroadphaseProxy*
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %4, i32 0, i32 6
  %call1 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_aabbMax)
  %arrayidx2 = getelementptr inbounds float, float* %call1, i32 0
  %5 = load float, float* %arrayidx2, align 4, !tbaa !35
  %cmp = fcmp ole float %2, %5
  br i1 %cmp, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %entry
  %6 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %7 = bitcast %struct.btSimpleBroadphaseProxy* %6 to %struct.btBroadphaseProxy*
  %m_aabbMin3 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %7, i32 0, i32 5
  %call4 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_aabbMin3)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 0
  %8 = load float, float* %arrayidx5, align 4, !tbaa !35
  %9 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %10 = bitcast %struct.btSimpleBroadphaseProxy* %9 to %struct.btBroadphaseProxy*
  %m_aabbMax6 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %10, i32 0, i32 6
  %call7 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_aabbMax6)
  %arrayidx8 = getelementptr inbounds float, float* %call7, i32 0
  %11 = load float, float* %arrayidx8, align 4, !tbaa !35
  %cmp9 = fcmp ole float %8, %11
  br i1 %cmp9, label %land.lhs.true10, label %land.end

land.lhs.true10:                                  ; preds = %land.lhs.true
  %12 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %13 = bitcast %struct.btSimpleBroadphaseProxy* %12 to %struct.btBroadphaseProxy*
  %m_aabbMin11 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %13, i32 0, i32 5
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_aabbMin11)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 1
  %14 = load float, float* %arrayidx13, align 4, !tbaa !35
  %15 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %16 = bitcast %struct.btSimpleBroadphaseProxy* %15 to %struct.btBroadphaseProxy*
  %m_aabbMax14 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %16, i32 0, i32 6
  %call15 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_aabbMax14)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 1
  %17 = load float, float* %arrayidx16, align 4, !tbaa !35
  %cmp17 = fcmp ole float %14, %17
  br i1 %cmp17, label %land.lhs.true18, label %land.end

land.lhs.true18:                                  ; preds = %land.lhs.true10
  %18 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %19 = bitcast %struct.btSimpleBroadphaseProxy* %18 to %struct.btBroadphaseProxy*
  %m_aabbMin19 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %19, i32 0, i32 5
  %call20 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_aabbMin19)
  %arrayidx21 = getelementptr inbounds float, float* %call20, i32 1
  %20 = load float, float* %arrayidx21, align 4, !tbaa !35
  %21 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %22 = bitcast %struct.btSimpleBroadphaseProxy* %21 to %struct.btBroadphaseProxy*
  %m_aabbMax22 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %22, i32 0, i32 6
  %call23 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_aabbMax22)
  %arrayidx24 = getelementptr inbounds float, float* %call23, i32 1
  %23 = load float, float* %arrayidx24, align 4, !tbaa !35
  %cmp25 = fcmp ole float %20, %23
  br i1 %cmp25, label %land.lhs.true26, label %land.end

land.lhs.true26:                                  ; preds = %land.lhs.true18
  %24 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %25 = bitcast %struct.btSimpleBroadphaseProxy* %24 to %struct.btBroadphaseProxy*
  %m_aabbMin27 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %25, i32 0, i32 5
  %call28 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_aabbMin27)
  %arrayidx29 = getelementptr inbounds float, float* %call28, i32 2
  %26 = load float, float* %arrayidx29, align 4, !tbaa !35
  %27 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %28 = bitcast %struct.btSimpleBroadphaseProxy* %27 to %struct.btBroadphaseProxy*
  %m_aabbMax30 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %28, i32 0, i32 6
  %call31 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_aabbMax30)
  %arrayidx32 = getelementptr inbounds float, float* %call31, i32 2
  %29 = load float, float* %arrayidx32, align 4, !tbaa !35
  %cmp33 = fcmp ole float %26, %29
  br i1 %cmp33, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true26
  %30 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %31 = bitcast %struct.btSimpleBroadphaseProxy* %30 to %struct.btBroadphaseProxy*
  %m_aabbMin34 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %31, i32 0, i32 5
  %call35 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_aabbMin34)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 2
  %32 = load float, float* %arrayidx36, align 4, !tbaa !35
  %33 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %34 = bitcast %struct.btSimpleBroadphaseProxy* %33 to %struct.btBroadphaseProxy*
  %m_aabbMax37 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %34, i32 0, i32 6
  %call38 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_aabbMax37)
  %arrayidx39 = getelementptr inbounds float, float* %call38, i32 2
  %35 = load float, float* %arrayidx39, align 4, !tbaa !35
  %cmp40 = fcmp ole float %32, %35
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true26, %land.lhs.true18, %land.lhs.true10, %land.lhs.true, %entry
  %36 = phi i1 [ false, %land.lhs.true26 ], [ false, %land.lhs.true18 ], [ false, %land.lhs.true10 ], [ false, %land.lhs.true ], [ false, %entry ], [ %cmp40, %land.rhs ]
  ret i1 %36
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

define hidden void @_ZN18btSimpleBroadphase25calculateOverlappingPairsEP12btDispatcher(%class.btSimpleBroadphase* %this, %class.btDispatcher* %dispatcher) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %new_largest_index = alloca i32, align 4
  %proxy0 = alloca %struct.btSimpleBroadphaseProxy*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %proxy1 = alloca %struct.btSimpleBroadphaseProxy*, align 4
  %p0 = alloca %struct.btSimpleBroadphaseProxy*, align 4
  %p1 = alloca %struct.btSimpleBroadphaseProxy*, align 4
  %overlappingPairArray = alloca %class.btAlignedObjectArray*, align 4
  %ref.tmp = alloca %class.btBroadphasePairSortPredicate, align 1
  %ref.tmp61 = alloca %struct.btBroadphasePair, align 4
  %previousPair = alloca %struct.btBroadphasePair, align 4
  %pair = alloca %struct.btBroadphasePair*, align 4
  %isDuplicate = alloca i8, align 1
  %needsRemoval = alloca i8, align 1
  %hasOverlap = alloca i8, align 1
  %ref.tmp96 = alloca %class.btBroadphasePairSortPredicate, align 1
  %ref.tmp100 = alloca %struct.btBroadphasePair, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %m_numHandles = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 1
  %2 = load i32, i32* %m_numHandles, align 4, !tbaa !8
  %cmp = icmp sge i32 %2, 0
  br i1 %cmp, label %if.then, label %if.end104

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %new_largest_index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  store i32 -1, i32* %new_largest_index, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc46, %if.then
  %4 = load i32, i32* %i, align 4, !tbaa !6
  %m_LastHandleIndex = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 3
  %5 = load i32, i32* %m_LastHandleIndex, align 4, !tbaa !20
  %cmp2 = icmp sle i32 %4, %5
  br i1 %cmp2, label %for.body, label %for.end48

for.body:                                         ; preds = %for.cond
  %6 = bitcast %struct.btSimpleBroadphaseProxy** %proxy0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %m_pHandles = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 4
  %7 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %m_pHandles, align 4, !tbaa !17
  %8 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btSimpleBroadphaseProxy, %struct.btSimpleBroadphaseProxy* %7, i32 %8
  store %struct.btSimpleBroadphaseProxy* %arrayidx, %struct.btSimpleBroadphaseProxy** %proxy0, align 4, !tbaa !2
  %9 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy0, align 4, !tbaa !2
  %10 = bitcast %struct.btSimpleBroadphaseProxy* %9 to %struct.btBroadphaseProxy*
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %10, i32 0, i32 0
  %11 = load i8*, i8** %m_clientObject, align 4, !tbaa !31
  %tobool = icmp ne i8* %11, null
  br i1 %tobool, label %if.end, label %if.then3

if.then3:                                         ; preds = %for.body
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup43

if.end:                                           ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !6
  store i32 %12, i32* %new_largest_index, align 4, !tbaa !6
  %13 = load i32, i32* %i, align 4, !tbaa !6
  %add = add nsw i32 %13, 1
  store i32 %add, i32* %j, align 4, !tbaa !6
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc, %if.end
  %14 = load i32, i32* %j, align 4, !tbaa !6
  %m_LastHandleIndex5 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 3
  %15 = load i32, i32* %m_LastHandleIndex5, align 4, !tbaa !20
  %cmp6 = icmp sle i32 %14, %15
  br i1 %cmp6, label %for.body7, label %for.end

for.body7:                                        ; preds = %for.cond4
  %16 = bitcast %struct.btSimpleBroadphaseProxy** %proxy1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #9
  %m_pHandles8 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 4
  %17 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %m_pHandles8, align 4, !tbaa !17
  %18 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx9 = getelementptr inbounds %struct.btSimpleBroadphaseProxy, %struct.btSimpleBroadphaseProxy* %17, i32 %18
  store %struct.btSimpleBroadphaseProxy* %arrayidx9, %struct.btSimpleBroadphaseProxy** %proxy1, align 4, !tbaa !2
  %19 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy1, align 4, !tbaa !2
  %20 = bitcast %struct.btSimpleBroadphaseProxy* %19 to %struct.btBroadphaseProxy*
  %m_clientObject10 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %20, i32 0, i32 0
  %21 = load i8*, i8** %m_clientObject10, align 4, !tbaa !31
  %tobool11 = icmp ne i8* %21, null
  br i1 %tobool11, label %if.end13, label %if.then12

if.then12:                                        ; preds = %for.body7
  store i32 7, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end13:                                         ; preds = %for.body7
  %22 = bitcast %struct.btSimpleBroadphaseProxy** %p0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #9
  %23 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy0, align 4, !tbaa !2
  %24 = bitcast %struct.btSimpleBroadphaseProxy* %23 to %struct.btBroadphaseProxy*
  %call = call %struct.btSimpleBroadphaseProxy* @_ZN18btSimpleBroadphase23getSimpleProxyFromProxyEP17btBroadphaseProxy(%class.btSimpleBroadphase* %this1, %struct.btBroadphaseProxy* %24)
  store %struct.btSimpleBroadphaseProxy* %call, %struct.btSimpleBroadphaseProxy** %p0, align 4, !tbaa !2
  %25 = bitcast %struct.btSimpleBroadphaseProxy** %p1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #9
  %26 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy1, align 4, !tbaa !2
  %27 = bitcast %struct.btSimpleBroadphaseProxy* %26 to %struct.btBroadphaseProxy*
  %call14 = call %struct.btSimpleBroadphaseProxy* @_ZN18btSimpleBroadphase23getSimpleProxyFromProxyEP17btBroadphaseProxy(%class.btSimpleBroadphase* %this1, %struct.btBroadphaseProxy* %27)
  store %struct.btSimpleBroadphaseProxy* %call14, %struct.btSimpleBroadphaseProxy** %p1, align 4, !tbaa !2
  %28 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %p0, align 4, !tbaa !2
  %29 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %p1, align 4, !tbaa !2
  %call15 = call zeroext i1 @_ZN18btSimpleBroadphase11aabbOverlapEP23btSimpleBroadphaseProxyS1_(%struct.btSimpleBroadphaseProxy* %28, %struct.btSimpleBroadphaseProxy* %29)
  br i1 %call15, label %if.then16, label %if.else

if.then16:                                        ; preds = %if.end13
  %m_pairCache = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 7
  %30 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4, !tbaa !13
  %31 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy0, align 4, !tbaa !2
  %32 = bitcast %struct.btSimpleBroadphaseProxy* %31 to %struct.btBroadphaseProxy*
  %33 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy1, align 4, !tbaa !2
  %34 = bitcast %struct.btSimpleBroadphaseProxy* %33 to %struct.btBroadphaseProxy*
  %35 = bitcast %class.btOverlappingPairCache* %30 to %struct.btBroadphasePair* (%class.btOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable = load %struct.btBroadphasePair* (%class.btOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, %struct.btBroadphasePair* (%class.btOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %35, align 4, !tbaa !11
  %vfn = getelementptr inbounds %struct.btBroadphasePair* (%class.btOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable, i64 13
  %36 = load %struct.btBroadphasePair* (%class.btOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call17 = call %struct.btBroadphasePair* %36(%class.btOverlappingPairCache* %30, %struct.btBroadphaseProxy* %32, %struct.btBroadphaseProxy* %34)
  %tobool18 = icmp ne %struct.btBroadphasePair* %call17, null
  br i1 %tobool18, label %if.end24, label %if.then19

if.then19:                                        ; preds = %if.then16
  %m_pairCache20 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 7
  %37 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache20, align 4, !tbaa !13
  %38 = bitcast %class.btOverlappingPairCache* %37 to %class.btOverlappingPairCallback*
  %39 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy0, align 4, !tbaa !2
  %40 = bitcast %struct.btSimpleBroadphaseProxy* %39 to %struct.btBroadphaseProxy*
  %41 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy1, align 4, !tbaa !2
  %42 = bitcast %struct.btSimpleBroadphaseProxy* %41 to %struct.btBroadphaseProxy*
  %43 = bitcast %class.btOverlappingPairCallback* %38 to %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable21 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %43, align 4, !tbaa !11
  %vfn22 = getelementptr inbounds %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable21, i64 2
  %44 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn22, align 4
  %call23 = call %struct.btBroadphasePair* %44(%class.btOverlappingPairCallback* %38, %struct.btBroadphaseProxy* %40, %struct.btBroadphaseProxy* %42)
  br label %if.end24

if.end24:                                         ; preds = %if.then19, %if.then16
  br label %if.end42

if.else:                                          ; preds = %if.end13
  %m_pairCache25 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 7
  %45 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache25, align 4, !tbaa !13
  %46 = bitcast %class.btOverlappingPairCache* %45 to i1 (%class.btOverlappingPairCache*)***
  %vtable26 = load i1 (%class.btOverlappingPairCache*)**, i1 (%class.btOverlappingPairCache*)*** %46, align 4, !tbaa !11
  %vfn27 = getelementptr inbounds i1 (%class.btOverlappingPairCache*)*, i1 (%class.btOverlappingPairCache*)** %vtable26, i64 14
  %47 = load i1 (%class.btOverlappingPairCache*)*, i1 (%class.btOverlappingPairCache*)** %vfn27, align 4
  %call28 = call zeroext i1 %47(%class.btOverlappingPairCache* %45)
  br i1 %call28, label %if.end41, label %if.then29

if.then29:                                        ; preds = %if.else
  %m_pairCache30 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 7
  %48 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache30, align 4, !tbaa !13
  %49 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy0, align 4, !tbaa !2
  %50 = bitcast %struct.btSimpleBroadphaseProxy* %49 to %struct.btBroadphaseProxy*
  %51 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy1, align 4, !tbaa !2
  %52 = bitcast %struct.btSimpleBroadphaseProxy* %51 to %struct.btBroadphaseProxy*
  %53 = bitcast %class.btOverlappingPairCache* %48 to %struct.btBroadphasePair* (%class.btOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable31 = load %struct.btBroadphasePair* (%class.btOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, %struct.btBroadphasePair* (%class.btOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %53, align 4, !tbaa !11
  %vfn32 = getelementptr inbounds %struct.btBroadphasePair* (%class.btOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable31, i64 13
  %54 = load %struct.btBroadphasePair* (%class.btOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn32, align 4
  %call33 = call %struct.btBroadphasePair* %54(%class.btOverlappingPairCache* %48, %struct.btBroadphaseProxy* %50, %struct.btBroadphaseProxy* %52)
  %tobool34 = icmp ne %struct.btBroadphasePair* %call33, null
  br i1 %tobool34, label %if.then35, label %if.end40

if.then35:                                        ; preds = %if.then29
  %m_pairCache36 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 7
  %55 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache36, align 4, !tbaa !13
  %56 = bitcast %class.btOverlappingPairCache* %55 to %class.btOverlappingPairCallback*
  %57 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy0, align 4, !tbaa !2
  %58 = bitcast %struct.btSimpleBroadphaseProxy* %57 to %struct.btBroadphaseProxy*
  %59 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy1, align 4, !tbaa !2
  %60 = bitcast %struct.btSimpleBroadphaseProxy* %59 to %struct.btBroadphaseProxy*
  %61 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %62 = bitcast %class.btOverlappingPairCallback* %56 to i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable37 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %62, align 4, !tbaa !11
  %vfn38 = getelementptr inbounds i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable37, i64 3
  %63 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn38, align 4
  %call39 = call i8* %63(%class.btOverlappingPairCallback* %56, %struct.btBroadphaseProxy* %58, %struct.btBroadphaseProxy* %60, %class.btDispatcher* %61)
  br label %if.end40

if.end40:                                         ; preds = %if.then35, %if.then29
  br label %if.end41

if.end41:                                         ; preds = %if.end40, %if.else
  br label %if.end42

if.end42:                                         ; preds = %if.end41, %if.end24
  %64 = bitcast %struct.btSimpleBroadphaseProxy** %p1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #9
  %65 = bitcast %struct.btSimpleBroadphaseProxy** %p0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #9
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end42, %if.then12
  %66 = bitcast %struct.btSimpleBroadphaseProxy** %proxy1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #9
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 7, label %for.inc
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont, %cleanup
  %67 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %67, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond4

for.end:                                          ; preds = %for.cond4
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup43

cleanup43:                                        ; preds = %for.end, %if.then3
  %68 = bitcast %struct.btSimpleBroadphaseProxy** %proxy0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #9
  %cleanup.dest44 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest44, label %unreachable [
    i32 0, label %cleanup.cont45
    i32 4, label %for.inc46
  ]

cleanup.cont45:                                   ; preds = %cleanup43
  br label %for.inc46

for.inc46:                                        ; preds = %cleanup.cont45, %cleanup43
  %69 = load i32, i32* %i, align 4, !tbaa !6
  %inc47 = add nsw i32 %69, 1
  store i32 %inc47, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end48:                                        ; preds = %for.cond
  %70 = load i32, i32* %new_largest_index, align 4, !tbaa !6
  %m_LastHandleIndex49 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 3
  store i32 %70, i32* %m_LastHandleIndex49, align 4, !tbaa !20
  %m_ownsPairCache = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 8
  %71 = load i8, i8* %m_ownsPairCache, align 4, !tbaa !14, !range !29
  %tobool50 = trunc i8 %71 to i1
  br i1 %tobool50, label %land.lhs.true, label %if.end103

land.lhs.true:                                    ; preds = %for.end48
  %m_pairCache51 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 7
  %72 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache51, align 4, !tbaa !13
  %73 = bitcast %class.btOverlappingPairCache* %72 to i1 (%class.btOverlappingPairCache*)***
  %vtable52 = load i1 (%class.btOverlappingPairCache*)**, i1 (%class.btOverlappingPairCache*)*** %73, align 4, !tbaa !11
  %vfn53 = getelementptr inbounds i1 (%class.btOverlappingPairCache*)*, i1 (%class.btOverlappingPairCache*)** %vtable52, i64 14
  %74 = load i1 (%class.btOverlappingPairCache*)*, i1 (%class.btOverlappingPairCache*)** %vfn53, align 4
  %call54 = call zeroext i1 %74(%class.btOverlappingPairCache* %72)
  br i1 %call54, label %if.then55, label %if.end103

if.then55:                                        ; preds = %land.lhs.true
  %75 = bitcast %class.btAlignedObjectArray** %overlappingPairArray to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #9
  %m_pairCache56 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 7
  %76 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache56, align 4, !tbaa !13
  %77 = bitcast %class.btOverlappingPairCache* %76 to %class.btAlignedObjectArray* (%class.btOverlappingPairCache*)***
  %vtable57 = load %class.btAlignedObjectArray* (%class.btOverlappingPairCache*)**, %class.btAlignedObjectArray* (%class.btOverlappingPairCache*)*** %77, align 4, !tbaa !11
  %vfn58 = getelementptr inbounds %class.btAlignedObjectArray* (%class.btOverlappingPairCache*)*, %class.btAlignedObjectArray* (%class.btOverlappingPairCache*)** %vtable57, i64 7
  %78 = load %class.btAlignedObjectArray* (%class.btOverlappingPairCache*)*, %class.btAlignedObjectArray* (%class.btOverlappingPairCache*)** %vfn58, align 4
  %call59 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray* %78(%class.btOverlappingPairCache* %76)
  store %class.btAlignedObjectArray* %call59, %class.btAlignedObjectArray** %overlappingPairArray, align 4, !tbaa !2
  %79 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %overlappingPairArray, align 4, !tbaa !2
  %80 = bitcast %class.btBroadphasePairSortPredicate* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %80) #9
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvRKT_(%class.btAlignedObjectArray* %79, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %ref.tmp)
  %81 = bitcast %class.btBroadphasePairSortPredicate* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %81) #9
  %82 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %overlappingPairArray, align 4, !tbaa !2
  %83 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %overlappingPairArray, align 4, !tbaa !2
  %call60 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %83)
  %m_invalidPair = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 9
  %84 = load i32, i32* %m_invalidPair, align 4, !tbaa !15
  %sub = sub nsw i32 %call60, %84
  %85 = bitcast %struct.btBroadphasePair* %ref.tmp61 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %85) #9
  %call62 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2Ev(%struct.btBroadphasePair* %ref.tmp61)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_(%class.btAlignedObjectArray* %82, i32 %sub, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %ref.tmp61)
  %86 = bitcast %struct.btBroadphasePair* %ref.tmp61 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %86) #9
  %m_invalidPair63 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 9
  store i32 0, i32* %m_invalidPair63, align 4, !tbaa !15
  %87 = bitcast %struct.btBroadphasePair* %previousPair to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %87) #9
  %call64 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2Ev(%struct.btBroadphasePair* %previousPair)
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %previousPair, i32 0, i32 0
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !37
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %previousPair, i32 0, i32 1
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !39
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %previousPair, i32 0, i32 2
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %m_algorithm, align 4, !tbaa !40
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond65

for.cond65:                                       ; preds = %for.inc93, %if.then55
  %88 = load i32, i32* %i, align 4, !tbaa !6
  %89 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %overlappingPairArray, align 4, !tbaa !2
  %call66 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %89)
  %cmp67 = icmp slt i32 %88, %call66
  br i1 %cmp67, label %for.body68, label %for.end95

for.body68:                                       ; preds = %for.cond65
  %90 = bitcast %struct.btBroadphasePair** %pair to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %90) #9
  %91 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %overlappingPairArray, align 4, !tbaa !2
  %92 = load i32, i32* %i, align 4, !tbaa !6
  %call69 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %91, i32 %92)
  store %struct.btBroadphasePair* %call69, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %isDuplicate) #9
  %93 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %call70 = call zeroext i1 @_ZeqRK16btBroadphasePairS1_(%struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %93, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %previousPair)
  %frombool = zext i1 %call70 to i8
  store i8 %frombool, i8* %isDuplicate, align 1, !tbaa !34
  %94 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %95 = bitcast %struct.btBroadphasePair* %previousPair to i8*
  %96 = bitcast %struct.btBroadphasePair* %94 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %95, i8* align 4 %96, i32 16, i1 false), !tbaa.struct !41
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %needsRemoval) #9
  store i8 0, i8* %needsRemoval, align 1, !tbaa !34
  %97 = load i8, i8* %isDuplicate, align 1, !tbaa !34, !range !29
  %tobool71 = trunc i8 %97 to i1
  br i1 %tobool71, label %if.else81, label %if.then72

if.then72:                                        ; preds = %for.body68
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %hasOverlap) #9
  %98 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %m_pProxy073 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %98, i32 0, i32 0
  %99 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy073, align 4, !tbaa !37
  %100 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %m_pProxy174 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %100, i32 0, i32 1
  %101 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy174, align 4, !tbaa !39
  %call75 = call zeroext i1 @_ZN18btSimpleBroadphase15testAabbOverlapEP17btBroadphaseProxyS1_(%class.btSimpleBroadphase* %this1, %struct.btBroadphaseProxy* %99, %struct.btBroadphaseProxy* %101)
  %frombool76 = zext i1 %call75 to i8
  store i8 %frombool76, i8* %hasOverlap, align 1, !tbaa !34
  %102 = load i8, i8* %hasOverlap, align 1, !tbaa !34, !range !29
  %tobool77 = trunc i8 %102 to i1
  br i1 %tobool77, label %if.then78, label %if.else79

if.then78:                                        ; preds = %if.then72
  store i8 0, i8* %needsRemoval, align 1, !tbaa !34
  br label %if.end80

if.else79:                                        ; preds = %if.then72
  store i8 1, i8* %needsRemoval, align 1, !tbaa !34
  br label %if.end80

if.end80:                                         ; preds = %if.else79, %if.then78
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %hasOverlap) #9
  br label %if.end82

if.else81:                                        ; preds = %for.body68
  store i8 1, i8* %needsRemoval, align 1, !tbaa !34
  br label %if.end82

if.end82:                                         ; preds = %if.else81, %if.end80
  %103 = load i8, i8* %needsRemoval, align 1, !tbaa !34, !range !29
  %tobool83 = trunc i8 %103 to i1
  br i1 %tobool83, label %if.then84, label %if.end92

if.then84:                                        ; preds = %if.end82
  %m_pairCache85 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 7
  %104 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache85, align 4, !tbaa !13
  %105 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %106 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %107 = bitcast %class.btOverlappingPairCache* %104 to void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)***
  %vtable86 = load void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)**, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*** %107, align 4, !tbaa !11
  %vfn87 = getelementptr inbounds void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vtable86, i64 8
  %108 = load void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vfn87, align 4
  call void %108(%class.btOverlappingPairCache* %104, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %105, %class.btDispatcher* %106)
  %109 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %m_pProxy088 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %109, i32 0, i32 0
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy088, align 4, !tbaa !37
  %110 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %m_pProxy189 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %110, i32 0, i32 1
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy189, align 4, !tbaa !39
  %m_invalidPair90 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 9
  %111 = load i32, i32* %m_invalidPair90, align 4, !tbaa !15
  %inc91 = add nsw i32 %111, 1
  store i32 %inc91, i32* %m_invalidPair90, align 4, !tbaa !15
  %112 = load i32, i32* @gOverlappingPairs, align 4, !tbaa !6
  %dec = add nsw i32 %112, -1
  store i32 %dec, i32* @gOverlappingPairs, align 4, !tbaa !6
  br label %if.end92

if.end92:                                         ; preds = %if.then84, %if.end82
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %needsRemoval) #9
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %isDuplicate) #9
  %113 = bitcast %struct.btBroadphasePair** %pair to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #9
  br label %for.inc93

for.inc93:                                        ; preds = %if.end92
  %114 = load i32, i32* %i, align 4, !tbaa !6
  %inc94 = add nsw i32 %114, 1
  store i32 %inc94, i32* %i, align 4, !tbaa !6
  br label %for.cond65

for.end95:                                        ; preds = %for.cond65
  %115 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %overlappingPairArray, align 4, !tbaa !2
  %116 = bitcast %class.btBroadphasePairSortPredicate* %ref.tmp96 to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %116) #9
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvRKT_(%class.btAlignedObjectArray* %115, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %ref.tmp96)
  %117 = bitcast %class.btBroadphasePairSortPredicate* %ref.tmp96 to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %117) #9
  %118 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %overlappingPairArray, align 4, !tbaa !2
  %119 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %overlappingPairArray, align 4, !tbaa !2
  %call97 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %119)
  %m_invalidPair98 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 9
  %120 = load i32, i32* %m_invalidPair98, align 4, !tbaa !15
  %sub99 = sub nsw i32 %call97, %120
  %121 = bitcast %struct.btBroadphasePair* %ref.tmp100 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %121) #9
  %call101 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2Ev(%struct.btBroadphasePair* %ref.tmp100)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_(%class.btAlignedObjectArray* %118, i32 %sub99, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %ref.tmp100)
  %122 = bitcast %struct.btBroadphasePair* %ref.tmp100 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %122) #9
  %m_invalidPair102 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 9
  store i32 0, i32* %m_invalidPair102, align 4, !tbaa !15
  %123 = bitcast %struct.btBroadphasePair* %previousPair to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %123) #9
  %124 = bitcast %class.btAlignedObjectArray** %overlappingPairArray to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #9
  br label %if.end103

if.end103:                                        ; preds = %for.end95, %land.lhs.true, %for.end48
  %125 = bitcast i32* %new_largest_index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #9
  br label %if.end104

if.end104:                                        ; preds = %if.end103, %entry
  %126 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #9
  %127 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #9
  ret void

unreachable:                                      ; preds = %cleanup43, %cleanup
  unreachable
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvRKT_(%class.btAlignedObjectArray* %this, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %CompareFunc) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %CompareFunc.addr = alloca %class.btBroadphasePairSortPredicate*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store %class.btBroadphasePairSortPredicate* %CompareFunc, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp sgt i32 %call, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray* %this1, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %0, i32 0, i32 %sub)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_(%class.btAlignedObjectArray* %this, i32 %newsize, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %fillData) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %struct.btBroadphasePair*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !6
  store %struct.btBroadphasePair* %fillData, %struct.btBroadphasePair** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !6
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %2 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  store i32 %4, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !42
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end18

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %14 = load i32, i32* %curSize, align 4, !tbaa !6
  store i32 %14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc15, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !6
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  br label %for.end17

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %18 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data11, align 4, !tbaa !42
  %19 = load i32, i32* %i6, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %18, i32 %19
  %20 = bitcast %struct.btBroadphasePair* %arrayidx12 to i8*
  %call13 = call i8* @_ZN16btBroadphasePairnwEmPv(i32 16, i8* %20)
  %21 = bitcast i8* %call13 to %struct.btBroadphasePair*
  %22 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %fillData.addr, align 4, !tbaa !2
  %call14 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %21, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %22)
  br label %for.inc15

for.inc15:                                        ; preds = %for.body10
  %23 = load i32, i32* %i6, align 4, !tbaa !6
  %inc16 = add nsw i32 %23, 1
  store i32 %inc16, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.end17:                                        ; preds = %for.cond.cleanup9
  br label %if.end18

if.end18:                                         ; preds = %for.end17, %for.end
  %24 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %24, i32* %m_size, align 4, !tbaa !45
  %25 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !45
  ret i32 %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btBroadphasePair* @_ZN16btBroadphasePairC2Ev(%struct.btBroadphasePair* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %struct.btBroadphasePair*, align 4
  store %struct.btBroadphasePair* %this, %struct.btBroadphasePair** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %this.addr, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 0
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !37
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 1
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !39
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 2
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %m_algorithm, align 4, !tbaa !40
  %0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 3
  %m_internalInfo1 = bitcast %union.anon* %0 to i8**
  store i8* null, i8** %m_internalInfo1, align 4, !tbaa !33
  ret %struct.btBroadphasePair* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !42
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 %1
  ret %struct.btBroadphasePair* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZeqRK16btBroadphasePairS1_(%struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %a, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %b) #3 comdat {
entry:
  %a.addr = alloca %struct.btBroadphasePair*, align 4
  %b.addr = alloca %struct.btBroadphasePair*, align 4
  store %struct.btBroadphasePair* %a, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  store %struct.btBroadphasePair* %b, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 0
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !37
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %m_pProxy01 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 0, i32 0
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy01, align 4, !tbaa !37
  %cmp = icmp eq %struct.btBroadphaseProxy* %1, %3
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %4, i32 0, i32 1
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !39
  %6 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %m_pProxy12 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %6, i32 0, i32 1
  %7 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy12, align 4, !tbaa !39
  %cmp3 = icmp eq %struct.btBroadphaseProxy* %5, %7
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %8 = phi i1 [ false, %entry ], [ %cmp3, %land.rhs ]
  ret i1 %8
}

define hidden zeroext i1 @_ZN18btSimpleBroadphase15testAabbOverlapEP17btBroadphaseProxyS1_(%class.btSimpleBroadphase* %this, %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy* %proxy1) #2 {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  %p0 = alloca %struct.btSimpleBroadphaseProxy*, align 4
  %p1 = alloca %struct.btSimpleBroadphaseProxy*, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy1, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %0 = bitcast %struct.btSimpleBroadphaseProxy** %p0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %call = call %struct.btSimpleBroadphaseProxy* @_ZN18btSimpleBroadphase23getSimpleProxyFromProxyEP17btBroadphaseProxy(%class.btSimpleBroadphase* %this1, %struct.btBroadphaseProxy* %1)
  store %struct.btSimpleBroadphaseProxy* %call, %struct.btSimpleBroadphaseProxy** %p0, align 4, !tbaa !2
  %2 = bitcast %struct.btSimpleBroadphaseProxy** %p1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %call2 = call %struct.btSimpleBroadphaseProxy* @_ZN18btSimpleBroadphase23getSimpleProxyFromProxyEP17btBroadphaseProxy(%class.btSimpleBroadphase* %this1, %struct.btBroadphaseProxy* %3)
  store %struct.btSimpleBroadphaseProxy* %call2, %struct.btSimpleBroadphaseProxy** %p1, align 4, !tbaa !2
  %4 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %p0, align 4, !tbaa !2
  %5 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %p1, align 4, !tbaa !2
  %call3 = call zeroext i1 @_ZN18btSimpleBroadphase11aabbOverlapEP23btSimpleBroadphaseProxyS1_(%struct.btSimpleBroadphaseProxy* %4, %struct.btSimpleBroadphaseProxy* %5)
  %6 = bitcast %struct.btSimpleBroadphaseProxy** %p1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  %7 = bitcast %struct.btSimpleBroadphaseProxy** %p0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret i1 %call3
}

; Function Attrs: nounwind
define hidden void @_ZN18btSimpleBroadphase9resetPoolEP12btDispatcher(%class.btSimpleBroadphase* %this, %class.btDispatcher* %dispatcher) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btOverlappingPairCache* @_ZN18btSimpleBroadphase23getOverlappingPairCacheEv(%class.btSimpleBroadphase* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %m_pairCache = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 7
  %0 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4, !tbaa !13
  ret %class.btOverlappingPairCache* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btOverlappingPairCache* @_ZNK18btSimpleBroadphase23getOverlappingPairCacheEv(%class.btSimpleBroadphase* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %m_pairCache = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 7
  %0 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4, !tbaa !13
  ret %class.btOverlappingPairCache* %0
}

define linkonce_odr hidden void @_ZNK18btSimpleBroadphase17getBroadphaseAabbER9btVector3S1_(%class.btSimpleBroadphase* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store float 0xC3ABC16D60000000, float* %ref.tmp, align 4, !tbaa !35
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 0xC3ABC16D60000000, float* %ref.tmp2, align 4, !tbaa !35
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  store float 0xC3ABC16D60000000, float* %ref.tmp3, align 4, !tbaa !35
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %0, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %4 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #9
  %6 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  %7 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %8 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  store float 0x43ABC16D60000000, float* %ref.tmp4, align 4, !tbaa !35
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  store float 0x43ABC16D60000000, float* %ref.tmp5, align 4, !tbaa !35
  %10 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  store float 0x43ABC16D60000000, float* %ref.tmp6, align 4, !tbaa !35
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %7, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %11 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  %12 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  %13 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN18btSimpleBroadphase10printStatsEv(%class.btSimpleBroadphase* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btBroadphaseInterface* @_ZN21btBroadphaseInterfaceD2Ev(%class.btBroadphaseInterface* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btBroadphaseInterface*, align 4
  store %class.btBroadphaseInterface* %this, %class.btBroadphaseInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %this.addr, align 4
  ret %class.btBroadphaseInterface* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN21btBroadphaseInterfaceD0Ev(%class.btBroadphaseInterface* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btBroadphaseInterface*, align 4
  store %class.btBroadphaseInterface* %this, %class.btBroadphaseInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %this.addr, align 4
  call void @llvm.trap() #11
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN21btBroadphaseInterface9resetPoolEP12btDispatcher(%class.btBroadphaseInterface* %this, %class.btDispatcher* %dispatcher) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btBroadphaseInterface*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %class.btBroadphaseInterface* %this, %class.btBroadphaseInterface** %this.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %this.addr, align 4
  ret void
}

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #8

define linkonce_odr hidden %struct.btBroadphaseProxy* @_ZN17btBroadphaseProxyC2Ev(%struct.btBroadphaseProxy* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btBroadphaseProxy*, align 4
  store %struct.btBroadphaseProxy* %this, %struct.btBroadphaseProxy** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %this.addr, align 4
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 0
  store i8* null, i8** %m_clientObject, align 4, !tbaa !31
  %m_multiSapParentProxy = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 3
  store i8* null, i8** %m_multiSapParentProxy, align 4, !tbaa !46
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 5
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aabbMin)
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 6
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aabbMax)
  ret %struct.btBroadphaseProxy* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK23btSimpleBroadphaseProxy11GetNextFreeEv(%struct.btSimpleBroadphaseProxy* %this) #3 comdat {
entry:
  %this.addr = alloca %struct.btSimpleBroadphaseProxy*, align 4
  store %struct.btSimpleBroadphaseProxy* %this, %struct.btSimpleBroadphaseProxy** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %this.addr, align 4
  %m_nextFree = getelementptr inbounds %struct.btSimpleBroadphaseProxy, %struct.btSimpleBroadphaseProxy* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_nextFree, align 4, !tbaa !27
  ret i32 %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btBroadphaseProxy* @_ZN17btBroadphaseProxyC2ERK9btVector3S2_PvssS3_(%struct.btBroadphaseProxy* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, i8* %userPtr, i16 signext %collisionFilterGroup, i16 signext %collisionFilterMask, i8* %multiSapParentProxy) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %struct.btBroadphaseProxy*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %userPtr.addr = alloca i8*, align 4
  %collisionFilterGroup.addr = alloca i16, align 2
  %collisionFilterMask.addr = alloca i16, align 2
  %multiSapParentProxy.addr = alloca i8*, align 4
  store %struct.btBroadphaseProxy* %this, %struct.btBroadphaseProxy** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  store i8* %userPtr, i8** %userPtr.addr, align 4, !tbaa !2
  store i16 %collisionFilterGroup, i16* %collisionFilterGroup.addr, align 2, !tbaa !30
  store i16 %collisionFilterMask, i16* %collisionFilterMask.addr, align 2, !tbaa !30
  store i8* %multiSapParentProxy, i8** %multiSapParentProxy.addr, align 4, !tbaa !2
  %this1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %this.addr, align 4
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 0
  %0 = load i8*, i8** %userPtr.addr, align 4, !tbaa !2
  store i8* %0, i8** %m_clientObject, align 4, !tbaa !31
  %m_collisionFilterGroup = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 1
  %1 = load i16, i16* %collisionFilterGroup.addr, align 2, !tbaa !30
  store i16 %1, i16* %m_collisionFilterGroup, align 4, !tbaa !47
  %m_collisionFilterMask = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 2
  %2 = load i16, i16* %collisionFilterMask.addr, align 2, !tbaa !30
  store i16 %2, i16* %m_collisionFilterMask, align 2, !tbaa !48
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 5
  %3 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %4 = bitcast %class.btVector3* %m_aabbMin to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !32
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 6
  %6 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %7 = bitcast %class.btVector3* %m_aabbMax to i8*
  %8 = bitcast %class.btVector3* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !32
  %9 = load i8*, i8** %multiSapParentProxy.addr, align 4, !tbaa !2
  %m_multiSapParentProxy = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 3
  store i8* %9, i8** %m_multiSapParentProxy, align 4, !tbaa !46
  ret %struct.btBroadphaseProxy* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !35
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !35
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !35
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !35
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !35
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !35
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !35
  ret void
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray* %this, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %CompareFunc, i32 %lo, i32 %hi) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %CompareFunc.addr = alloca %class.btBroadphasePairSortPredicate*, align 4
  %lo.addr = alloca i32, align 4
  %hi.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %x = alloca %struct.btBroadphasePair, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store %class.btBroadphasePairSortPredicate* %CompareFunc, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  store i32 %lo, i32* %lo.addr, align 4, !tbaa !6
  store i32 %hi, i32* %hi.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %lo.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  %2 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %3 = load i32, i32* %hi.addr, align 4, !tbaa !6
  store i32 %3, i32* %j, align 4, !tbaa !6
  %4 = bitcast %struct.btBroadphasePair* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #9
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %5 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !42
  %6 = load i32, i32* %lo.addr, align 4, !tbaa !6
  %7 = load i32, i32* %hi.addr, align 4, !tbaa !6
  %add = add nsw i32 %6, %7
  %div = sdiv i32 %add, 2
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %5, i32 %div
  %call = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %x, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx)
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %do.body
  %8 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %9 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data2, align 4, !tbaa !42
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %9, i32 %10
  %call4 = call zeroext i1 @_ZNK29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_(%class.btBroadphasePairSortPredicate* %8, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx3, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %x)
  br i1 %call4, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %while.cond5

while.cond5:                                      ; preds = %while.body9, %while.end
  %12 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %13 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data6, align 4, !tbaa !42
  %14 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %13, i32 %14
  %call8 = call zeroext i1 @_ZNK29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_(%class.btBroadphasePairSortPredicate* %12, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %x, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx7)
  br i1 %call8, label %while.body9, label %while.end10

while.body9:                                      ; preds = %while.cond5
  %15 = load i32, i32* %j, align 4, !tbaa !6
  %dec = add nsw i32 %15, -1
  store i32 %dec, i32* %j, align 4, !tbaa !6
  br label %while.cond5

while.end10:                                      ; preds = %while.cond5
  %16 = load i32, i32* %i, align 4, !tbaa !6
  %17 = load i32, i32* %j, align 4, !tbaa !6
  %cmp = icmp sle i32 %16, %17
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %while.end10
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %19 = load i32, i32* %j, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4swapEii(%class.btAlignedObjectArray* %this1, i32 %18, i32 %19)
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %inc11 = add nsw i32 %20, 1
  store i32 %inc11, i32* %i, align 4, !tbaa !6
  %21 = load i32, i32* %j, align 4, !tbaa !6
  %dec12 = add nsw i32 %21, -1
  store i32 %dec12, i32* %j, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end10
  br label %do.cond

do.cond:                                          ; preds = %if.end
  %22 = load i32, i32* %i, align 4, !tbaa !6
  %23 = load i32, i32* %j, align 4, !tbaa !6
  %cmp13 = icmp sle i32 %22, %23
  br i1 %cmp13, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %24 = load i32, i32* %lo.addr, align 4, !tbaa !6
  %25 = load i32, i32* %j, align 4, !tbaa !6
  %cmp14 = icmp slt i32 %24, %25
  br i1 %cmp14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %do.end
  %26 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %27 = load i32, i32* %lo.addr, align 4, !tbaa !6
  %28 = load i32, i32* %j, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray* %this1, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %26, i32 %27, i32 %28)
  br label %if.end16

if.end16:                                         ; preds = %if.then15, %do.end
  %29 = load i32, i32* %i, align 4, !tbaa !6
  %30 = load i32, i32* %hi.addr, align 4, !tbaa !6
  %cmp17 = icmp slt i32 %29, %30
  br i1 %cmp17, label %if.then18, label %if.end19

if.then18:                                        ; preds = %if.end16
  %31 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %32 = load i32, i32* %i, align 4, !tbaa !6
  %33 = load i32, i32* %hi.addr, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray* %this1, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %31, i32 %32, i32 %33)
  br label %if.end19

if.end19:                                         ; preds = %if.then18, %if.end16
  %34 = bitcast %struct.btBroadphasePair* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %34) #9
  %35 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #9
  %36 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* returned %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %other) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %struct.btBroadphasePair*, align 4
  %other.addr = alloca %struct.btBroadphasePair*, align 4
  store %struct.btBroadphasePair* %this, %struct.btBroadphasePair** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphasePair* %other, %struct.btBroadphasePair** %other.addr, align 4, !tbaa !2
  %this1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %this.addr, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 0
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4, !tbaa !2
  %m_pProxy02 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 0
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy02, align 4, !tbaa !37
  store %struct.btBroadphaseProxy* %1, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !37
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 1
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4, !tbaa !2
  %m_pProxy13 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 0, i32 1
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy13, align 4, !tbaa !39
  store %struct.btBroadphaseProxy* %3, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !39
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 2
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4, !tbaa !2
  %m_algorithm4 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %4, i32 0, i32 2
  %5 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm4, align 4, !tbaa !40
  store %class.btCollisionAlgorithm* %5, %class.btCollisionAlgorithm** %m_algorithm, align 4, !tbaa !40
  %6 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 3
  %m_internalInfo1 = bitcast %union.anon* %6 to i8**
  %7 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4, !tbaa !2
  %8 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %7, i32 0, i32 3
  %m_internalInfo15 = bitcast %union.anon* %8 to i8**
  %9 = load i8*, i8** %m_internalInfo15, align 4, !tbaa !33
  store i8* %9, i8** %m_internalInfo1, align 4, !tbaa !33
  ret %struct.btBroadphasePair* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZNK29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_(%class.btBroadphasePairSortPredicate* %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %a, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %b) #0 comdat {
entry:
  %this.addr = alloca %class.btBroadphasePairSortPredicate*, align 4
  %a.addr = alloca %struct.btBroadphasePair*, align 4
  %b.addr = alloca %struct.btBroadphasePair*, align 4
  %uidA0 = alloca i32, align 4
  %uidB0 = alloca i32, align 4
  %uidA1 = alloca i32, align 4
  %uidB1 = alloca i32, align 4
  store %class.btBroadphasePairSortPredicate* %this, %class.btBroadphasePairSortPredicate** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphasePair* %a, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  store %struct.btBroadphasePair* %b, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %this1 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %this.addr, align 4
  %0 = bitcast i32* %uidA0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %1, i32 0, i32 0
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !37
  %tobool = icmp ne %struct.btBroadphaseProxy* %2, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  %m_pProxy02 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %3, i32 0, i32 0
  %4 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy02, align 4, !tbaa !37
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %4, i32 0, i32 4
  %5 = load i32, i32* %m_uniqueId, align 4, !tbaa !21
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %5, %cond.true ], [ -1, %cond.false ]
  store i32 %cond, i32* %uidA0, align 4, !tbaa !6
  %6 = bitcast i32* %uidB0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %7 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %m_pProxy03 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %7, i32 0, i32 0
  %8 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy03, align 4, !tbaa !37
  %tobool4 = icmp ne %struct.btBroadphaseProxy* %8, null
  br i1 %tobool4, label %cond.true5, label %cond.false8

cond.true5:                                       ; preds = %cond.end
  %9 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %m_pProxy06 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %9, i32 0, i32 0
  %10 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy06, align 4, !tbaa !37
  %m_uniqueId7 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %10, i32 0, i32 4
  %11 = load i32, i32* %m_uniqueId7, align 4, !tbaa !21
  br label %cond.end9

cond.false8:                                      ; preds = %cond.end
  br label %cond.end9

cond.end9:                                        ; preds = %cond.false8, %cond.true5
  %cond10 = phi i32 [ %11, %cond.true5 ], [ -1, %cond.false8 ]
  store i32 %cond10, i32* %uidB0, align 4, !tbaa !6
  %12 = bitcast i32* %uidA1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #9
  %13 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %13, i32 0, i32 1
  %14 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !39
  %tobool11 = icmp ne %struct.btBroadphaseProxy* %14, null
  br i1 %tobool11, label %cond.true12, label %cond.false15

cond.true12:                                      ; preds = %cond.end9
  %15 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  %m_pProxy113 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %15, i32 0, i32 1
  %16 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy113, align 4, !tbaa !39
  %m_uniqueId14 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %16, i32 0, i32 4
  %17 = load i32, i32* %m_uniqueId14, align 4, !tbaa !21
  br label %cond.end16

cond.false15:                                     ; preds = %cond.end9
  br label %cond.end16

cond.end16:                                       ; preds = %cond.false15, %cond.true12
  %cond17 = phi i32 [ %17, %cond.true12 ], [ -1, %cond.false15 ]
  store i32 %cond17, i32* %uidA1, align 4, !tbaa !6
  %18 = bitcast i32* %uidB1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #9
  %19 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %m_pProxy118 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %19, i32 0, i32 1
  %20 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy118, align 4, !tbaa !39
  %tobool19 = icmp ne %struct.btBroadphaseProxy* %20, null
  br i1 %tobool19, label %cond.true20, label %cond.false23

cond.true20:                                      ; preds = %cond.end16
  %21 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %m_pProxy121 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %21, i32 0, i32 1
  %22 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy121, align 4, !tbaa !39
  %m_uniqueId22 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %22, i32 0, i32 4
  %23 = load i32, i32* %m_uniqueId22, align 4, !tbaa !21
  br label %cond.end24

cond.false23:                                     ; preds = %cond.end16
  br label %cond.end24

cond.end24:                                       ; preds = %cond.false23, %cond.true20
  %cond25 = phi i32 [ %23, %cond.true20 ], [ -1, %cond.false23 ]
  store i32 %cond25, i32* %uidB1, align 4, !tbaa !6
  %24 = load i32, i32* %uidA0, align 4, !tbaa !6
  %25 = load i32, i32* %uidB0, align 4, !tbaa !6
  %cmp = icmp sgt i32 %24, %25
  br i1 %cmp, label %lor.end, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %cond.end24
  %26 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  %m_pProxy026 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %26, i32 0, i32 0
  %27 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy026, align 4, !tbaa !37
  %28 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %m_pProxy027 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %28, i32 0, i32 0
  %29 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy027, align 4, !tbaa !37
  %cmp28 = icmp eq %struct.btBroadphaseProxy* %27, %29
  br i1 %cmp28, label %land.lhs.true, label %lor.rhs

land.lhs.true:                                    ; preds = %lor.lhs.false
  %30 = load i32, i32* %uidA1, align 4, !tbaa !6
  %31 = load i32, i32* %uidB1, align 4, !tbaa !6
  %cmp29 = icmp sgt i32 %30, %31
  br i1 %cmp29, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %land.lhs.true, %lor.lhs.false
  %32 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  %m_pProxy030 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %32, i32 0, i32 0
  %33 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy030, align 4, !tbaa !37
  %34 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %m_pProxy031 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %34, i32 0, i32 0
  %35 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy031, align 4, !tbaa !37
  %cmp32 = icmp eq %struct.btBroadphaseProxy* %33, %35
  br i1 %cmp32, label %land.lhs.true33, label %land.end

land.lhs.true33:                                  ; preds = %lor.rhs
  %36 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  %m_pProxy134 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %36, i32 0, i32 1
  %37 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy134, align 4, !tbaa !39
  %38 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %m_pProxy135 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %38, i32 0, i32 1
  %39 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy135, align 4, !tbaa !39
  %cmp36 = icmp eq %struct.btBroadphaseProxy* %37, %39
  br i1 %cmp36, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true33
  %40 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %40, i32 0, i32 2
  %41 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm, align 4, !tbaa !40
  %42 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %m_algorithm37 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %42, i32 0, i32 2
  %43 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm37, align 4, !tbaa !40
  %cmp38 = icmp ugt %class.btCollisionAlgorithm* %41, %43
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true33, %lor.rhs
  %44 = phi i1 [ false, %land.lhs.true33 ], [ false, %lor.rhs ], [ %cmp38, %land.rhs ]
  br label %lor.end

lor.end:                                          ; preds = %land.end, %land.lhs.true, %cond.end24
  %45 = phi i1 [ true, %land.lhs.true ], [ true, %cond.end24 ], [ %44, %land.end ]
  %46 = bitcast i32* %uidB1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #9
  %47 = bitcast i32* %uidA1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #9
  %48 = bitcast i32* %uidB0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #9
  %49 = bitcast i32* %uidA0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #9
  ret i1 %45
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4swapEii(%class.btAlignedObjectArray* %this, i32 %index0, i32 %index1) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %struct.btBroadphasePair, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %index0, i32* %index0.addr, align 4, !tbaa !6
  store i32 %index1, i32* %index1.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast %struct.btBroadphasePair* %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !42
  %2 = load i32, i32* %index0.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %1, i32 %2
  %call = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %temp, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx)
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data2, align 4, !tbaa !42
  %4 = load i32, i32* %index1.addr, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %3, i32 %4
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %5 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data4, align 4, !tbaa !42
  %6 = load i32, i32* %index0.addr, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %5, i32 %6
  %7 = bitcast %struct.btBroadphasePair* %arrayidx5 to i8*
  %8 = bitcast %struct.btBroadphasePair* %arrayidx3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !41
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %9 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data6, align 4, !tbaa !42
  %10 = load i32, i32* %index1.addr, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %9, i32 %10
  %11 = bitcast %struct.btBroadphasePair* %arrayidx7 to i8*
  %12 = bitcast %struct.btBroadphasePair* %temp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 16, i1 false), !tbaa.struct !41
  %13 = bitcast %struct.btBroadphasePair* %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %13) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btBroadphasePair*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btBroadphasePair** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayI16btBroadphasePairE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.btBroadphasePair*
  store %struct.btBroadphasePair* %3, %struct.btBroadphasePair** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %struct.btBroadphasePair* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !49
  %5 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btBroadphasePair* %5, %struct.btBroadphasePair** %m_data, align 4, !tbaa !42
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !50
  %7 = bitcast %struct.btBroadphasePair** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN16btBroadphasePairnwEmPv(i32 %0, i8* %ptr) #3 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !25
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !50
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI16btBroadphasePairE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #7 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call %struct.btBroadphasePair* @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %struct.btBroadphasePair** null)
  %2 = bitcast %struct.btBroadphasePair* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %struct.btBroadphasePair* %dest) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btBroadphasePair*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store %struct.btBroadphasePair* %dest, %struct.btBroadphasePair** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %4, i32 %5
  %6 = bitcast %struct.btBroadphasePair* %arrayidx to i8*
  %call = call i8* @_ZN16btBroadphasePairnwEmPv(i32 16, i8* %6)
  %7 = bitcast i8* %call to %struct.btBroadphasePair*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !42
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %8, i32 %9
  %call3 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %7, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !42
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE10deallocateEv(%class.btAlignedObjectArray* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !42
  %tobool = icmp ne %struct.btBroadphasePair* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !49, !range !29
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data4, align 4, !tbaa !42
  call void @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %struct.btBroadphasePair* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btBroadphasePair* null, %struct.btBroadphasePair** %m_data5, align 4, !tbaa !42
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden %struct.btBroadphasePair* @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %struct.btBroadphasePair** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btBroadphasePair**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store %struct.btBroadphasePair** %hint, %struct.btBroadphasePair*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btBroadphasePair*
  ret %struct.btBroadphasePair* %1
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %struct.btBroadphasePair* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %struct.btBroadphasePair*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphasePair* %ptr, %struct.btBroadphasePair** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btBroadphasePair* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind readnone speculatable willreturn }
attributes #6 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { cold noreturn nounwind }
attributes #9 = { nounwind }
attributes #10 = { builtin nounwind }
attributes #11 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !7, i64 4}
!9 = !{!"_ZTS18btSimpleBroadphase", !7, i64 4, !7, i64 8, !7, i64 12, !3, i64 16, !3, i64 20, !7, i64 24, !3, i64 28, !10, i64 32, !7, i64 36}
!10 = !{!"bool", !4, i64 0}
!11 = !{!12, !12, i64 0}
!12 = !{!"vtable pointer", !5, i64 0}
!13 = !{!9, !3, i64 28}
!14 = !{!9, !10, i64 32}
!15 = !{!9, !7, i64 36}
!16 = !{!9, !3, i64 20}
!17 = !{!9, !3, i64 16}
!18 = !{!9, !7, i64 8}
!19 = !{!9, !7, i64 24}
!20 = !{!9, !7, i64 12}
!21 = !{!22, !7, i64 12}
!22 = !{!"_ZTS17btBroadphaseProxy", !3, i64 0, !23, i64 4, !23, i64 6, !3, i64 8, !7, i64 12, !24, i64 16, !24, i64 32}
!23 = !{!"short", !4, i64 0}
!24 = !{!"_ZTS9btVector3", !4, i64 0}
!25 = !{!26, !26, i64 0}
!26 = !{!"long", !4, i64 0}
!27 = !{!28, !7, i64 48}
!28 = !{!"_ZTS23btSimpleBroadphaseProxy", !7, i64 48}
!29 = !{i8 0, i8 2}
!30 = !{!23, !23, i64 0}
!31 = !{!22, !3, i64 0}
!32 = !{i64 0, i64 16, !33}
!33 = !{!4, !4, i64 0}
!34 = !{!10, !10, i64 0}
!35 = !{!36, !36, i64 0}
!36 = !{!"float", !4, i64 0}
!37 = !{!38, !3, i64 0}
!38 = !{!"_ZTS16btBroadphasePair", !3, i64 0, !3, i64 4, !3, i64 8, !4, i64 12}
!39 = !{!38, !3, i64 4}
!40 = !{!38, !3, i64 8}
!41 = !{i64 0, i64 4, !2, i64 4, i64 4, !2, i64 8, i64 4, !2, i64 12, i64 4, !2, i64 12, i64 4, !6}
!42 = !{!43, !3, i64 12}
!43 = !{!"_ZTS20btAlignedObjectArrayI16btBroadphasePairE", !44, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !10, i64 16}
!44 = !{!"_ZTS18btAlignedAllocatorI16btBroadphasePairLj16EE"}
!45 = !{!43, !7, i64 4}
!46 = !{!22, !3, i64 8}
!47 = !{!22, !23, i64 4}
!48 = !{!22, !23, i64 6}
!49 = !{!43, !10, i64 16}
!50 = !{!43, !7, i64 8}
