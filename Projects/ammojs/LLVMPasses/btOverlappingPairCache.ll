; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/BroadphaseCollision/btOverlappingPairCache.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/BroadphaseCollision/btOverlappingPairCache.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btHashedOverlappingPairCache = type { %class.btOverlappingPairCache, %class.btAlignedObjectArray, %struct.btOverlapFilterCallback*, i8, [3 x i8], %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0, %class.btOverlappingPairCallback* }
%class.btOverlappingPairCache = type { %class.btOverlappingPairCallback }
%class.btOverlappingPairCallback = type { i32 (...)** }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btBroadphasePair*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btBroadphasePair = type { %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btCollisionAlgorithm*, %union.anon }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%class.btCollisionAlgorithm = type { i32 (...)**, %class.btDispatcher* }
%class.btDispatcher = type { i32 (...)** }
%union.anon = type { i8* }
%struct.btOverlapFilterCallback = type { i32 (...)** }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btSortedOverlappingPairCache = type { %class.btOverlappingPairCache, %class.btAlignedObjectArray, i8, i8, %struct.btOverlapFilterCallback*, %class.btOverlappingPairCallback* }
%class.CleanPairCallback = type { %struct.btOverlapCallback, %struct.btBroadphaseProxy*, %class.btOverlappingPairCache*, %class.btDispatcher* }
%struct.btOverlapCallback = type { i32 (...)** }
%class.RemovePairCallback = type { %struct.btOverlapCallback, %struct.btBroadphaseProxy* }
%class.btBroadphasePairSortPredicate = type { i8 }
%class.CleanPairCallback.4 = type { %struct.btOverlapCallback, %struct.btBroadphaseProxy*, %class.btOverlappingPairCache*, %class.btDispatcher* }
%class.RemovePairCallback.5 = type { %struct.btOverlapCallback, %struct.btBroadphaseProxy* }

$_ZN22btOverlappingPairCacheC2Ev = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIiED2Ev = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairED2Ev = comdat any

$_ZN17btOverlapCallbackD2Ev = comdat any

$_Z6btSwapIP17btBroadphaseProxyEvRT_S3_ = comdat any

$_ZNK17btBroadphaseProxy6getUidEv = comdat any

$_ZN28btHashedOverlappingPairCache7getHashEjj = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv = comdat any

$_ZNK20btAlignedObjectArrayIiE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIiEixEi = comdat any

$_ZN28btHashedOverlappingPairCache10equalsPairERK16btBroadphasePairii = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi = comdat any

$_ZN20btAlignedObjectArrayIiE6resizeEiRKi = comdat any

$_ZN28btHashedOverlappingPairCache16internalFindPairEP17btBroadphaseProxyS1_i = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE21expandNonInitializingEv = comdat any

$_ZN16btBroadphasePairnwEmPv = comdat any

$_ZN16btBroadphasePairC2ER17btBroadphaseProxyS1_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE8pop_backEv = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE9push_backERKS0_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvRKT_ = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairE16findLinearSearchERKS0_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE4swapEii = comdat any

$_ZNK28btSortedOverlappingPairCache24needsBroadphaseCollisionEP17btBroadphaseProxyS1_ = comdat any

$_ZN28btHashedOverlappingPairCache18addOverlappingPairEP17btBroadphaseProxyS1_ = comdat any

$_ZN28btHashedOverlappingPairCache26getOverlappingPairArrayPtrEv = comdat any

$_ZNK28btHashedOverlappingPairCache26getOverlappingPairArrayPtrEv = comdat any

$_ZN28btHashedOverlappingPairCache23getOverlappingPairArrayEv = comdat any

$_ZNK28btHashedOverlappingPairCache22getNumOverlappingPairsEv = comdat any

$_ZN28btHashedOverlappingPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback = comdat any

$_ZN28btHashedOverlappingPairCache18hasDeferredRemovalEv = comdat any

$_ZN28btHashedOverlappingPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback = comdat any

$_ZN28btSortedOverlappingPairCache26getOverlappingPairArrayPtrEv = comdat any

$_ZNK28btSortedOverlappingPairCache26getOverlappingPairArrayPtrEv = comdat any

$_ZN28btSortedOverlappingPairCache23getOverlappingPairArrayEv = comdat any

$_ZNK28btSortedOverlappingPairCache22getNumOverlappingPairsEv = comdat any

$_ZN28btSortedOverlappingPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback = comdat any

$_ZN28btSortedOverlappingPairCache18hasDeferredRemovalEv = comdat any

$_ZN28btSortedOverlappingPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback = comdat any

$_ZN25btOverlappingPairCallbackC2Ev = comdat any

$_ZN25btOverlappingPairCallbackD2Ev = comdat any

$_ZN22btOverlappingPairCacheD0Ev = comdat any

$_ZN25btOverlappingPairCallbackD0Ev = comdat any

$_ZN17btOverlapCallbackC2Ev = comdat any

$_ZN17btOverlapCallbackD0Ev = comdat any

$_ZNK28btHashedOverlappingPairCache24needsBroadphaseCollisionEP17btBroadphaseProxyS1_ = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairEixEi = comdat any

$_ZN18btAlignedAllocatorI16btBroadphasePairLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE4initEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiE4initEv = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayIiE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIiE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIiE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE8allocateEiPPKS0_ = comdat any

$_ZN16btBroadphasePairC2ERKS_ = comdat any

$_ZN20btAlignedObjectArrayIiE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIiE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIiE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIiE4copyEiiPi = comdat any

$_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii = comdat any

$_ZNK29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_ = comdat any

$_ZeqRK16btBroadphasePairS1_ = comdat any

$_ZTS22btOverlappingPairCache = comdat any

$_ZTS25btOverlappingPairCallback = comdat any

$_ZTI25btOverlappingPairCallback = comdat any

$_ZTI22btOverlappingPairCache = comdat any

$_ZTV22btOverlappingPairCache = comdat any

$_ZTV25btOverlappingPairCallback = comdat any

$_ZTS17btOverlapCallback = comdat any

$_ZTI17btOverlapCallback = comdat any

$_ZTV17btOverlapCallback = comdat any

@gOverlappingPairs = hidden global i32 0, align 4
@gRemovePairs = hidden global i32 0, align 4
@gAddedPairs = hidden global i32 0, align 4
@gFindPairs = hidden global i32 0, align 4
@_ZTV28btHashedOverlappingPairCache = hidden unnamed_addr constant { [19 x i8*] } { [19 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI28btHashedOverlappingPairCache to i8*), i8* bitcast (%class.btHashedOverlappingPairCache* (%class.btHashedOverlappingPairCache*)* @_ZN28btHashedOverlappingPairCacheD1Ev to i8*), i8* bitcast (void (%class.btHashedOverlappingPairCache*)* @_ZN28btHashedOverlappingPairCacheD0Ev to i8*), i8* bitcast (%struct.btBroadphasePair* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)* @_ZN28btHashedOverlappingPairCache18addOverlappingPairEP17btBroadphaseProxyS1_ to i8*), i8* bitcast (i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)* @_ZN28btHashedOverlappingPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher to i8*), i8* bitcast (void (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %class.btDispatcher*)* @_ZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher to i8*), i8* bitcast (%struct.btBroadphasePair* (%class.btHashedOverlappingPairCache*)* @_ZN28btHashedOverlappingPairCache26getOverlappingPairArrayPtrEv to i8*), i8* bitcast (%struct.btBroadphasePair* (%class.btHashedOverlappingPairCache*)* @_ZNK28btHashedOverlappingPairCache26getOverlappingPairArrayPtrEv to i8*), i8* bitcast (%class.btAlignedObjectArray* (%class.btHashedOverlappingPairCache*)* @_ZN28btHashedOverlappingPairCache23getOverlappingPairArrayEv to i8*), i8* bitcast (void (%class.btHashedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)* @_ZN28btHashedOverlappingPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher to i8*), i8* bitcast (i32 (%class.btHashedOverlappingPairCache*)* @_ZNK28btHashedOverlappingPairCache22getNumOverlappingPairsEv to i8*), i8* bitcast (void (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %class.btDispatcher*)* @_ZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher to i8*), i8* bitcast (void (%class.btHashedOverlappingPairCache*, %struct.btOverlapFilterCallback*)* @_ZN28btHashedOverlappingPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback to i8*), i8* bitcast (void (%class.btHashedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)* @_ZN28btHashedOverlappingPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher to i8*), i8* bitcast (%struct.btBroadphasePair* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)* @_ZN28btHashedOverlappingPairCache8findPairEP17btBroadphaseProxyS1_ to i8*), i8* bitcast (i1 (%class.btHashedOverlappingPairCache*)* @_ZN28btHashedOverlappingPairCache18hasDeferredRemovalEv to i8*), i8* bitcast (void (%class.btHashedOverlappingPairCache*, %class.btOverlappingPairCallback*)* @_ZN28btHashedOverlappingPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback to i8*), i8* bitcast (void (%class.btHashedOverlappingPairCache*, %class.btDispatcher*)* @_ZN28btHashedOverlappingPairCache20sortOverlappingPairsEP12btDispatcher to i8*)] }, align 4
@_ZTV28btSortedOverlappingPairCache = hidden unnamed_addr constant { [19 x i8*] } { [19 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI28btSortedOverlappingPairCache to i8*), i8* bitcast (%class.btSortedOverlappingPairCache* (%class.btSortedOverlappingPairCache*)* @_ZN28btSortedOverlappingPairCacheD1Ev to i8*), i8* bitcast (void (%class.btSortedOverlappingPairCache*)* @_ZN28btSortedOverlappingPairCacheD0Ev to i8*), i8* bitcast (%struct.btBroadphasePair* (%class.btSortedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)* @_ZN28btSortedOverlappingPairCache18addOverlappingPairEP17btBroadphaseProxyS1_ to i8*), i8* bitcast (i8* (%class.btSortedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)* @_ZN28btSortedOverlappingPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher to i8*), i8* bitcast (void (%class.btSortedOverlappingPairCache*, %struct.btBroadphaseProxy*, %class.btDispatcher*)* @_ZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher to i8*), i8* bitcast (%struct.btBroadphasePair* (%class.btSortedOverlappingPairCache*)* @_ZN28btSortedOverlappingPairCache26getOverlappingPairArrayPtrEv to i8*), i8* bitcast (%struct.btBroadphasePair* (%class.btSortedOverlappingPairCache*)* @_ZNK28btSortedOverlappingPairCache26getOverlappingPairArrayPtrEv to i8*), i8* bitcast (%class.btAlignedObjectArray* (%class.btSortedOverlappingPairCache*)* @_ZN28btSortedOverlappingPairCache23getOverlappingPairArrayEv to i8*), i8* bitcast (void (%class.btSortedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)* @_ZN28btSortedOverlappingPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher to i8*), i8* bitcast (i32 (%class.btSortedOverlappingPairCache*)* @_ZNK28btSortedOverlappingPairCache22getNumOverlappingPairsEv to i8*), i8* bitcast (void (%class.btSortedOverlappingPairCache*, %struct.btBroadphaseProxy*, %class.btDispatcher*)* @_ZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher to i8*), i8* bitcast (void (%class.btSortedOverlappingPairCache*, %struct.btOverlapFilterCallback*)* @_ZN28btSortedOverlappingPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback to i8*), i8* bitcast (void (%class.btSortedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)* @_ZN28btSortedOverlappingPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher to i8*), i8* bitcast (%struct.btBroadphasePair* (%class.btSortedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)* @_ZN28btSortedOverlappingPairCache8findPairEP17btBroadphaseProxyS1_ to i8*), i8* bitcast (i1 (%class.btSortedOverlappingPairCache*)* @_ZN28btSortedOverlappingPairCache18hasDeferredRemovalEv to i8*), i8* bitcast (void (%class.btSortedOverlappingPairCache*, %class.btOverlappingPairCallback*)* @_ZN28btSortedOverlappingPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback to i8*), i8* bitcast (void (%class.btSortedOverlappingPairCache*, %class.btDispatcher*)* @_ZN28btSortedOverlappingPairCache20sortOverlappingPairsEP12btDispatcher to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS28btHashedOverlappingPairCache = hidden constant [31 x i8] c"28btHashedOverlappingPairCache\00", align 1
@_ZTS22btOverlappingPairCache = linkonce_odr hidden constant [25 x i8] c"22btOverlappingPairCache\00", comdat, align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS25btOverlappingPairCallback = linkonce_odr hidden constant [28 x i8] c"25btOverlappingPairCallback\00", comdat, align 1
@_ZTI25btOverlappingPairCallback = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([28 x i8], [28 x i8]* @_ZTS25btOverlappingPairCallback, i32 0, i32 0) }, comdat, align 4
@_ZTI22btOverlappingPairCache = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([25 x i8], [25 x i8]* @_ZTS22btOverlappingPairCache, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI25btOverlappingPairCallback to i8*) }, comdat, align 4
@_ZTI28btHashedOverlappingPairCache = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([31 x i8], [31 x i8]* @_ZTS28btHashedOverlappingPairCache, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI22btOverlappingPairCache to i8*) }, align 4
@_ZTS28btSortedOverlappingPairCache = hidden constant [31 x i8] c"28btSortedOverlappingPairCache\00", align 1
@_ZTI28btSortedOverlappingPairCache = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([31 x i8], [31 x i8]* @_ZTS28btSortedOverlappingPairCache, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI22btOverlappingPairCache to i8*) }, align 4
@_ZTV22btOverlappingPairCache = linkonce_odr hidden unnamed_addr constant { [19 x i8*] } { [19 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI22btOverlappingPairCache to i8*), i8* bitcast (%class.btOverlappingPairCallback* (%class.btOverlappingPairCallback*)* @_ZN25btOverlappingPairCallbackD2Ev to i8*), i8* bitcast (void (%class.btOverlappingPairCache*)* @_ZN22btOverlappingPairCacheD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTV25btOverlappingPairCallback = linkonce_odr hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI25btOverlappingPairCallback to i8*), i8* bitcast (%class.btOverlappingPairCallback* (%class.btOverlappingPairCallback*)* @_ZN25btOverlappingPairCallbackD2Ev to i8*), i8* bitcast (void (%class.btOverlappingPairCallback*)* @_ZN25btOverlappingPairCallbackD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTVZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback = internal unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback to i8*), i8* bitcast (%struct.btOverlapCallback* (%struct.btOverlapCallback*)* @_ZN17btOverlapCallbackD2Ev to i8*), i8* bitcast (void (%class.CleanPairCallback*)* @_ZZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackD0Ev to i8*), i8* bitcast (i1 (%class.CleanPairCallback*, %struct.btBroadphasePair*)* @_ZZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallback14processOverlapER16btBroadphasePair to i8*)] }, align 4
@_ZTSZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback = internal constant [110 x i8] c"ZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback\00", align 1
@_ZTS17btOverlapCallback = linkonce_odr hidden constant [20 x i8] c"17btOverlapCallback\00", comdat, align 1
@_ZTI17btOverlapCallback = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([20 x i8], [20 x i8]* @_ZTS17btOverlapCallback, i32 0, i32 0) }, comdat, align 4
@_ZTIZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback = internal constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([110 x i8], [110 x i8]* @_ZTSZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI17btOverlapCallback to i8*) }, align 4
@_ZTV17btOverlapCallback = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI17btOverlapCallback to i8*), i8* bitcast (%struct.btOverlapCallback* (%struct.btOverlapCallback*)* @_ZN17btOverlapCallbackD2Ev to i8*), i8* bitcast (void (%struct.btOverlapCallback*)* @_ZN17btOverlapCallbackD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTVZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback = internal unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback to i8*), i8* bitcast (%struct.btOverlapCallback* (%struct.btOverlapCallback*)* @_ZN17btOverlapCallbackD2Ev to i8*), i8* bitcast (void (%class.RemovePairCallback*)* @_ZZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackD0Ev to i8*), i8* bitcast (i1 (%class.RemovePairCallback*, %struct.btBroadphasePair*)* @_ZZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallback14processOverlapER16btBroadphasePair to i8*)] }, align 4
@_ZTSZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback = internal constant [129 x i8] c"ZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback\00", align 1
@_ZTIZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback = internal constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([129 x i8], [129 x i8]* @_ZTSZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI17btOverlapCallback to i8*) }, align 4
@_ZTVZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback = internal unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback to i8*), i8* bitcast (%struct.btOverlapCallback* (%struct.btOverlapCallback*)* @_ZN17btOverlapCallbackD2Ev to i8*), i8* bitcast (void (%class.CleanPairCallback.4*)* @_ZZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackD0Ev to i8*), i8* bitcast (i1 (%class.CleanPairCallback.4*, %struct.btBroadphasePair*)* @_ZZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallback14processOverlapER16btBroadphasePair to i8*)] }, align 4
@_ZTSZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback = internal constant [110 x i8] c"ZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback\00", align 1
@_ZTIZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback = internal constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([110 x i8], [110 x i8]* @_ZTSZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI17btOverlapCallback to i8*) }, align 4
@_ZTVZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback = internal unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback to i8*), i8* bitcast (%struct.btOverlapCallback* (%struct.btOverlapCallback*)* @_ZN17btOverlapCallbackD2Ev to i8*), i8* bitcast (void (%class.RemovePairCallback.5*)* @_ZZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackD0Ev to i8*), i8* bitcast (i1 (%class.RemovePairCallback.5*, %struct.btBroadphasePair*)* @_ZZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallback14processOverlapER16btBroadphasePair to i8*)] }, align 4
@_ZTSZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback = internal constant [129 x i8] c"ZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback\00", align 1
@_ZTIZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback = internal constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([129 x i8], [129 x i8]* @_ZTSZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI17btOverlapCallback to i8*) }, align 4

@_ZN28btHashedOverlappingPairCacheC1Ev = hidden unnamed_addr alias %class.btHashedOverlappingPairCache* (%class.btHashedOverlappingPairCache*), %class.btHashedOverlappingPairCache* (%class.btHashedOverlappingPairCache*)* @_ZN28btHashedOverlappingPairCacheC2Ev
@_ZN28btHashedOverlappingPairCacheD1Ev = hidden unnamed_addr alias %class.btHashedOverlappingPairCache* (%class.btHashedOverlappingPairCache*), %class.btHashedOverlappingPairCache* (%class.btHashedOverlappingPairCache*)* @_ZN28btHashedOverlappingPairCacheD2Ev
@_ZN28btSortedOverlappingPairCacheC1Ev = hidden unnamed_addr alias %class.btSortedOverlappingPairCache* (%class.btSortedOverlappingPairCache*), %class.btSortedOverlappingPairCache* (%class.btSortedOverlappingPairCache*)* @_ZN28btSortedOverlappingPairCacheC2Ev
@_ZN28btSortedOverlappingPairCacheD1Ev = hidden unnamed_addr alias %class.btSortedOverlappingPairCache* (%class.btSortedOverlappingPairCache*), %class.btSortedOverlappingPairCache* (%class.btSortedOverlappingPairCache*)* @_ZN28btSortedOverlappingPairCacheD2Ev

define hidden %class.btHashedOverlappingPairCache* @_ZN28btHashedOverlappingPairCacheC2Ev(%class.btHashedOverlappingPairCache* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %initialAllocatedSize = alloca i32, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = bitcast %class.btHashedOverlappingPairCache* %this1 to %class.btOverlappingPairCache*
  %call = call %class.btOverlappingPairCache* @_ZN22btOverlappingPairCacheC2Ev(%class.btOverlappingPairCache* %0) #8
  %1 = bitcast %class.btHashedOverlappingPairCache* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [19 x i8*] }, { [19 x i8*] }* @_ZTV28btHashedOverlappingPairCache, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI16btBroadphasePairEC2Ev(%class.btAlignedObjectArray* %m_overlappingPairArray)
  %m_overlapFilterCallback = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 2
  store %struct.btOverlapFilterCallback* null, %struct.btOverlapFilterCallback** %m_overlapFilterCallback, align 4, !tbaa !8
  %m_blockedForChanges = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 3
  store i8 0, i8* %m_blockedForChanges, align 4, !tbaa !16
  %m_hashTable = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 5
  %call3 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.0* %m_hashTable)
  %m_next = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 6
  %call4 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.0* %m_next)
  %m_ghostPairCallback = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 7
  store %class.btOverlappingPairCallback* null, %class.btOverlappingPairCallback** %m_ghostPairCallback, align 4, !tbaa !17
  %2 = bitcast i32* %initialAllocatedSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store i32 2, i32* %initialAllocatedSize, align 4, !tbaa !18
  %m_overlappingPairArray5 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %3 = load i32, i32* %initialAllocatedSize, align 4, !tbaa !18
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi(%class.btAlignedObjectArray* %m_overlappingPairArray5, i32 %3)
  call void @_ZN28btHashedOverlappingPairCache10growTablesEv(%class.btHashedOverlappingPairCache* %this1)
  %4 = bitcast i32* %initialAllocatedSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  ret %class.btHashedOverlappingPairCache* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btOverlappingPairCache* @_ZN22btOverlappingPairCacheC2Ev(%class.btOverlappingPairCache* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btOverlappingPairCache*, align 4
  store %class.btOverlappingPairCache* %this, %class.btOverlappingPairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %this.addr, align 4
  %0 = bitcast %class.btOverlappingPairCache* %this1 to %class.btOverlappingPairCallback*
  %call = call %class.btOverlappingPairCallback* @_ZN25btOverlappingPairCallbackC2Ev(%class.btOverlappingPairCallback* %0) #8
  %1 = bitcast %class.btOverlappingPairCache* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [19 x i8*] }, { [19 x i8*] }* @_ZTV22btOverlappingPairCache, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  ret %class.btOverlappingPairCache* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI16btBroadphasePairEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.1* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btBroadphasePair*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !18
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !18
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btBroadphasePair** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !18
  %call2 = call i8* @_ZN20btAlignedObjectArrayI16btBroadphasePairE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.btBroadphasePair*
  store %struct.btBroadphasePair* %3, %struct.btBroadphasePair** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %struct.btBroadphasePair* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !19
  %5 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btBroadphasePair* %5, %struct.btBroadphasePair** %m_data, align 4, !tbaa !20
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !18
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !21
  %7 = bitcast %struct.btBroadphasePair** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

define hidden void @_ZN28btHashedOverlappingPairCache10growTablesEv(%class.btHashedOverlappingPairCache* %this) #0 {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %newCapacity = alloca i32, align 4
  %curHashtableSize = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp6 = alloca i32, align 4
  %i = alloca i32, align 4
  %pair = alloca %struct.btBroadphasePair*, align 4
  %proxyId1 = alloca i32, align 4
  %proxyId2 = alloca i32, align 4
  %hashValue = alloca i32, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = bitcast i32* %newCapacity to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %m_overlappingPairArray)
  store i32 %call, i32* %newCapacity, align 4, !tbaa !18
  %m_hashTable = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 5
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.0* %m_hashTable)
  %1 = load i32, i32* %newCapacity, align 4, !tbaa !18
  %cmp = icmp slt i32 %call2, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast i32* %curHashtableSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %m_hashTable3 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 5
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.0* %m_hashTable3)
  store i32 %call4, i32* %curHashtableSize, align 4, !tbaa !18
  %m_hashTable5 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 5
  %3 = load i32, i32* %newCapacity, align 4, !tbaa !18
  %4 = bitcast i32* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  store i32 0, i32* %ref.tmp, align 4, !tbaa !18
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.0* %m_hashTable5, i32 %3, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  %5 = bitcast i32* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  %m_next = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 6
  %6 = load i32, i32* %newCapacity, align 4, !tbaa !18
  %7 = bitcast i32* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  store i32 0, i32* %ref.tmp6, align 4, !tbaa !18
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.0* %m_next, i32 %6, i32* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %8 = bitcast i32* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  store i32 0, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %10 = load i32, i32* %i, align 4, !tbaa !18
  %11 = load i32, i32* %newCapacity, align 4, !tbaa !18
  %cmp7 = icmp slt i32 %10, %11
  br i1 %cmp7, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_hashTable8 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 5
  %12 = load i32, i32* %i, align 4, !tbaa !18
  %call9 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_hashTable8, i32 %12)
  store i32 -1, i32* %call9, align 4, !tbaa !18
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %13 = load i32, i32* %i, align 4, !tbaa !18
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !18
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc15, %for.end
  %14 = load i32, i32* %i, align 4, !tbaa !18
  %15 = load i32, i32* %newCapacity, align 4, !tbaa !18
  %cmp11 = icmp slt i32 %14, %15
  br i1 %cmp11, label %for.body12, label %for.end17

for.body12:                                       ; preds = %for.cond10
  %m_next13 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 6
  %16 = load i32, i32* %i, align 4, !tbaa !18
  %call14 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_next13, i32 %16)
  store i32 -1, i32* %call14, align 4, !tbaa !18
  br label %for.inc15

for.inc15:                                        ; preds = %for.body12
  %17 = load i32, i32* %i, align 4, !tbaa !18
  %inc16 = add nsw i32 %17, 1
  store i32 %inc16, i32* %i, align 4, !tbaa !18
  br label %for.cond10

for.end17:                                        ; preds = %for.cond10
  store i32 0, i32* %i, align 4, !tbaa !18
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc34, %for.end17
  %18 = load i32, i32* %i, align 4, !tbaa !18
  %19 = load i32, i32* %curHashtableSize, align 4, !tbaa !18
  %cmp19 = icmp slt i32 %18, %19
  br i1 %cmp19, label %for.body20, label %for.end36

for.body20:                                       ; preds = %for.cond18
  %20 = bitcast %struct.btBroadphasePair** %pair to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #8
  %m_overlappingPairArray21 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %21 = load i32, i32* %i, align 4, !tbaa !18
  %call22 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray21, i32 %21)
  store %struct.btBroadphasePair* %call22, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %22 = bitcast i32* %proxyId1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #8
  %23 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %23, i32 0, i32 0
  %24 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !22
  %call23 = call i32 @_ZNK17btBroadphaseProxy6getUidEv(%struct.btBroadphaseProxy* %24)
  store i32 %call23, i32* %proxyId1, align 4, !tbaa !18
  %25 = bitcast i32* %proxyId2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  %26 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %26, i32 0, i32 1
  %27 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !24
  %call24 = call i32 @_ZNK17btBroadphaseProxy6getUidEv(%struct.btBroadphaseProxy* %27)
  store i32 %call24, i32* %proxyId2, align 4, !tbaa !18
  %28 = bitcast i32* %hashValue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #8
  %29 = load i32, i32* %proxyId1, align 4, !tbaa !18
  %30 = load i32, i32* %proxyId2, align 4, !tbaa !18
  %call25 = call i32 @_ZN28btHashedOverlappingPairCache7getHashEjj(%class.btHashedOverlappingPairCache* %this1, i32 %29, i32 %30)
  %m_overlappingPairArray26 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call27 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %m_overlappingPairArray26)
  %sub = sub nsw i32 %call27, 1
  %and = and i32 %call25, %sub
  store i32 %and, i32* %hashValue, align 4, !tbaa !18
  %m_hashTable28 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 5
  %31 = load i32, i32* %hashValue, align 4, !tbaa !18
  %call29 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_hashTable28, i32 %31)
  %32 = load i32, i32* %call29, align 4, !tbaa !18
  %m_next30 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 6
  %33 = load i32, i32* %i, align 4, !tbaa !18
  %call31 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_next30, i32 %33)
  store i32 %32, i32* %call31, align 4, !tbaa !18
  %34 = load i32, i32* %i, align 4, !tbaa !18
  %m_hashTable32 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 5
  %35 = load i32, i32* %hashValue, align 4, !tbaa !18
  %call33 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_hashTable32, i32 %35)
  store i32 %34, i32* %call33, align 4, !tbaa !18
  %36 = bitcast i32* %hashValue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #8
  %37 = bitcast i32* %proxyId2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #8
  %38 = bitcast i32* %proxyId1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #8
  %39 = bitcast %struct.btBroadphasePair** %pair to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #8
  br label %for.inc34

for.inc34:                                        ; preds = %for.body20
  %40 = load i32, i32* %i, align 4, !tbaa !18
  %inc35 = add nsw i32 %40, 1
  store i32 %inc35, i32* %i, align 4, !tbaa !18
  br label %for.cond18

for.end36:                                        ; preds = %for.cond18
  %41 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #8
  %42 = bitcast i32* %curHashtableSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #8
  br label %if.end

if.end:                                           ; preds = %for.end36, %entry
  %43 = bitcast i32* %newCapacity to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #8
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: nounwind
define hidden %class.btHashedOverlappingPairCache* @_ZN28btHashedOverlappingPairCacheD2Ev(%class.btHashedOverlappingPairCache* returned %this) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = bitcast %class.btHashedOverlappingPairCache* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [19 x i8*] }, { [19 x i8*] }* @_ZTV28btHashedOverlappingPairCache, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  %m_next = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 6
  %call = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.0* %m_next) #8
  %m_hashTable = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 5
  %call2 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.0* %m_hashTable) #8
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call3 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI16btBroadphasePairED2Ev(%class.btAlignedObjectArray* %m_overlappingPairArray) #8
  %1 = bitcast %class.btHashedOverlappingPairCache* %this1 to %class.btOverlappingPairCache*
  %call4 = call %class.btOverlappingPairCache* bitcast (%class.btOverlappingPairCallback* (%class.btOverlappingPairCallback*)* @_ZN25btOverlappingPairCallbackD2Ev to %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*)(%class.btOverlappingPairCache* %1) #8
  ret %class.btHashedOverlappingPairCache* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI16btBroadphasePairED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN28btHashedOverlappingPairCacheD0Ev(%class.btHashedOverlappingPairCache* %this) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %call = call %class.btHashedOverlappingPairCache* @_ZN28btHashedOverlappingPairCacheD1Ev(%class.btHashedOverlappingPairCache* %this1) #8
  %0 = bitcast %class.btHashedOverlappingPairCache* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #5

define hidden void @_ZN28btHashedOverlappingPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher(%class.btHashedOverlappingPairCache* %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %pair, %class.btDispatcher* %dispatcher) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %pair.addr = alloca %struct.btBroadphasePair*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphasePair* %pair, %struct.btBroadphasePair** %pair.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4, !tbaa !2
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 2
  %1 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm, align 4, !tbaa !25
  %tobool = icmp ne %class.btCollisionAlgorithm* %1, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %2 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %tobool2 = icmp ne %class.btDispatcher* %2, null
  br i1 %tobool2, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4, !tbaa !2
  %m_algorithm3 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %3, i32 0, i32 2
  %4 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm3, align 4, !tbaa !25
  %5 = bitcast %class.btCollisionAlgorithm* %4 to %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)***
  %vtable = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)**, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*** %5, align 4, !tbaa !6
  %vfn = getelementptr inbounds %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vtable, i64 0
  %6 = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vfn, align 4
  %call = call %class.btCollisionAlgorithm* %6(%class.btCollisionAlgorithm* %4) #8
  %7 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %8 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4, !tbaa !2
  %m_algorithm4 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %8, i32 0, i32 2
  %9 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm4, align 4, !tbaa !25
  %10 = bitcast %class.btCollisionAlgorithm* %9 to i8*
  %11 = bitcast %class.btDispatcher* %7 to void (%class.btDispatcher*, i8*)***
  %vtable5 = load void (%class.btDispatcher*, i8*)**, void (%class.btDispatcher*, i8*)*** %11, align 4, !tbaa !6
  %vfn6 = getelementptr inbounds void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vtable5, i64 15
  %12 = load void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vfn6, align 4
  call void %12(%class.btDispatcher* %7, i8* %10)
  %13 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4, !tbaa !2
  %m_algorithm7 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %13, i32 0, i32 2
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %m_algorithm7, align 4, !tbaa !25
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  ret void
}

define hidden void @_ZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher(%class.btHashedOverlappingPairCache* %this, %struct.btBroadphaseProxy* %proxy, %class.btDispatcher* %dispatcher) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %cleanPairs = alloca %class.CleanPairCallback, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = bitcast %class.CleanPairCallback* %cleanPairs to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  %2 = bitcast %class.btHashedOverlappingPairCache* %this1 to %class.btOverlappingPairCache*
  %3 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %call = call %class.CleanPairCallback* @_ZZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackC2ES1_P22btOverlappingPairCacheS3_(%class.CleanPairCallback* %cleanPairs, %struct.btBroadphaseProxy* %1, %class.btOverlappingPairCache* %2, %class.btDispatcher* %3)
  %4 = bitcast %class.CleanPairCallback* %cleanPairs to %struct.btOverlapCallback*
  %5 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %6 = bitcast %class.btHashedOverlappingPairCache* %this1 to void (%class.btHashedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)***
  %vtable = load void (%class.btHashedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)**, void (%class.btHashedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)*** %6, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btHashedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)*, void (%class.btHashedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)** %vtable, i64 12
  %7 = load void (%class.btHashedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)*, void (%class.btHashedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)** %vfn, align 4
  call void %7(%class.btHashedOverlappingPairCache* %this1, %struct.btOverlapCallback* %4, %class.btDispatcher* %5)
  %call2 = call %class.CleanPairCallback* bitcast (%struct.btOverlapCallback* (%struct.btOverlapCallback*)* @_ZN17btOverlapCallbackD2Ev to %class.CleanPairCallback* (%class.CleanPairCallback*)*)(%class.CleanPairCallback* %cleanPairs) #8
  %8 = bitcast %class.CleanPairCallback* %cleanPairs to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #8
  ret void
}

; Function Attrs: nounwind
define internal %class.CleanPairCallback* @_ZZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackC2ES1_P22btOverlappingPairCacheS3_(%class.CleanPairCallback* returned %this, %struct.btBroadphaseProxy* %cleanProxy, %class.btOverlappingPairCache* %pairCache, %class.btDispatcher* %dispatcher) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.CleanPairCallback*, align 4
  %cleanProxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %pairCache.addr = alloca %class.btOverlappingPairCache*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %class.CleanPairCallback* %this, %class.CleanPairCallback** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %cleanProxy, %struct.btBroadphaseProxy** %cleanProxy.addr, align 4, !tbaa !2
  store %class.btOverlappingPairCache* %pairCache, %class.btOverlappingPairCache** %pairCache.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.CleanPairCallback*, %class.CleanPairCallback** %this.addr, align 4
  %0 = bitcast %class.CleanPairCallback* %this1 to %struct.btOverlapCallback*
  %call = call %struct.btOverlapCallback* @_ZN17btOverlapCallbackC2Ev(%struct.btOverlapCallback* %0) #8
  %1 = bitcast %class.CleanPairCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_cleanProxy = getelementptr inbounds %class.CleanPairCallback, %class.CleanPairCallback* %this1, i32 0, i32 1
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %cleanProxy.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %2, %struct.btBroadphaseProxy** %m_cleanProxy, align 4, !tbaa !26
  %m_pairCache = getelementptr inbounds %class.CleanPairCallback, %class.CleanPairCallback* %this1, i32 0, i32 2
  %3 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %pairCache.addr, align 4, !tbaa !2
  store %class.btOverlappingPairCache* %3, %class.btOverlappingPairCache** %m_pairCache, align 4, !tbaa !28
  %m_dispatcher = getelementptr inbounds %class.CleanPairCallback, %class.CleanPairCallback* %this1, i32 0, i32 3
  %4 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  store %class.btDispatcher* %4, %class.btDispatcher** %m_dispatcher, align 4, !tbaa !29
  ret %class.CleanPairCallback* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btOverlapCallback* @_ZN17btOverlapCallbackD2Ev(%struct.btOverlapCallback* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %struct.btOverlapCallback*, align 4
  store %struct.btOverlapCallback* %this, %struct.btOverlapCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btOverlapCallback*, %struct.btOverlapCallback** %this.addr, align 4
  ret %struct.btOverlapCallback* %this1
}

define hidden void @_ZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher(%class.btHashedOverlappingPairCache* %this, %struct.btBroadphaseProxy* %proxy, %class.btDispatcher* %dispatcher) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %removeCallback = alloca %class.RemovePairCallback, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = bitcast %class.RemovePairCallback* %removeCallback to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #8
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  %call = call %class.RemovePairCallback* @_ZZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackC2ES1_(%class.RemovePairCallback* %removeCallback, %struct.btBroadphaseProxy* %1)
  %2 = bitcast %class.RemovePairCallback* %removeCallback to %struct.btOverlapCallback*
  %3 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %4 = bitcast %class.btHashedOverlappingPairCache* %this1 to void (%class.btHashedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)***
  %vtable = load void (%class.btHashedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)**, void (%class.btHashedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)*** %4, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btHashedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)*, void (%class.btHashedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)** %vtable, i64 12
  %5 = load void (%class.btHashedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)*, void (%class.btHashedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)** %vfn, align 4
  call void %5(%class.btHashedOverlappingPairCache* %this1, %struct.btOverlapCallback* %2, %class.btDispatcher* %3)
  %call2 = call %class.RemovePairCallback* bitcast (%struct.btOverlapCallback* (%struct.btOverlapCallback*)* @_ZN17btOverlapCallbackD2Ev to %class.RemovePairCallback* (%class.RemovePairCallback*)*)(%class.RemovePairCallback* %removeCallback) #8
  %6 = bitcast %class.RemovePairCallback* %removeCallback to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %6) #8
  ret void
}

; Function Attrs: nounwind
define internal %class.RemovePairCallback* @_ZZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackC2ES1_(%class.RemovePairCallback* returned %this, %struct.btBroadphaseProxy* %obsoleteProxy) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.RemovePairCallback*, align 4
  %obsoleteProxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  store %class.RemovePairCallback* %this, %class.RemovePairCallback** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %obsoleteProxy, %struct.btBroadphaseProxy** %obsoleteProxy.addr, align 4, !tbaa !2
  %this1 = load %class.RemovePairCallback*, %class.RemovePairCallback** %this.addr, align 4
  %0 = bitcast %class.RemovePairCallback* %this1 to %struct.btOverlapCallback*
  %call = call %struct.btOverlapCallback* @_ZN17btOverlapCallbackC2Ev(%struct.btOverlapCallback* %0) #8
  %1 = bitcast %class.RemovePairCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_obsoleteProxy = getelementptr inbounds %class.RemovePairCallback, %class.RemovePairCallback* %this1, i32 0, i32 1
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %obsoleteProxy.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %2, %struct.btBroadphaseProxy** %m_obsoleteProxy, align 4, !tbaa !30
  ret %class.RemovePairCallback* %this1
}

define hidden %struct.btBroadphasePair* @_ZN28btHashedOverlappingPairCache8findPairEP17btBroadphaseProxyS1_(%class.btHashedOverlappingPairCache* %this, %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy* %proxy1) unnamed_addr #0 {
entry:
  %retval = alloca %struct.btBroadphasePair*, align 4
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxyId1 = alloca i32, align 4
  %proxyId2 = alloca i32, align 4
  %hash = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %index = alloca i32, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy1, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = load i32, i32* @gFindPairs, align 4, !tbaa !18
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* @gFindPairs, align 4, !tbaa !18
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %1, i32 0, i32 4
  %2 = load i32, i32* %m_uniqueId, align 4, !tbaa !32
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %m_uniqueId2 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %3, i32 0, i32 4
  %4 = load i32, i32* %m_uniqueId2, align 4, !tbaa !32
  %cmp = icmp sgt i32 %2, %4
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_Z6btSwapIP17btBroadphaseProxyEvRT_S3_(%struct.btBroadphaseProxy** nonnull align 4 dereferenceable(4) %proxy0.addr, %struct.btBroadphaseProxy** nonnull align 4 dereferenceable(4) %proxy1.addr)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %5 = bitcast i32* %proxyId1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %call = call i32 @_ZNK17btBroadphaseProxy6getUidEv(%struct.btBroadphaseProxy* %6)
  store i32 %call, i32* %proxyId1, align 4, !tbaa !18
  %7 = bitcast i32* %proxyId2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %8 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %call3 = call i32 @_ZNK17btBroadphaseProxy6getUidEv(%struct.btBroadphaseProxy* %8)
  store i32 %call3, i32* %proxyId2, align 4, !tbaa !18
  %9 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  %10 = load i32, i32* %proxyId1, align 4, !tbaa !18
  %11 = load i32, i32* %proxyId2, align 4, !tbaa !18
  %call4 = call i32 @_ZN28btHashedOverlappingPairCache7getHashEjj(%class.btHashedOverlappingPairCache* %this1, i32 %10, i32 %11)
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call5 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %m_overlappingPairArray)
  %sub = sub nsw i32 %call5, 1
  %and = and i32 %call4, %sub
  store i32 %and, i32* %hash, align 4, !tbaa !18
  %12 = load i32, i32* %hash, align 4, !tbaa !18
  %m_hashTable = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 5
  %call6 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.0* %m_hashTable)
  %cmp7 = icmp sge i32 %12, %call6
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end
  store %struct.btBroadphasePair* null, %struct.btBroadphasePair** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup23

if.end9:                                          ; preds = %if.end
  %13 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %m_hashTable10 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 5
  %14 = load i32, i32* %hash, align 4, !tbaa !18
  %call11 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_hashTable10, i32 %14)
  %15 = load i32, i32* %call11, align 4, !tbaa !18
  store i32 %15, i32* %index, align 4, !tbaa !18
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end9
  %16 = load i32, i32* %index, align 4, !tbaa !18
  %cmp12 = icmp ne i32 %16, -1
  br i1 %cmp12, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %m_overlappingPairArray13 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %17 = load i32, i32* %index, align 4, !tbaa !18
  %call14 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray13, i32 %17)
  %18 = load i32, i32* %proxyId1, align 4, !tbaa !18
  %19 = load i32, i32* %proxyId2, align 4, !tbaa !18
  %call15 = call zeroext i1 @_ZN28btHashedOverlappingPairCache10equalsPairERK16btBroadphasePairii(%class.btHashedOverlappingPairCache* %this1, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %call14, i32 %18, i32 %19)
  %conv = zext i1 %call15 to i32
  %cmp16 = icmp eq i32 %conv, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %20 = phi i1 [ false, %while.cond ], [ %cmp16, %land.rhs ]
  br i1 %20, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %m_next = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 6
  %21 = load i32, i32* %index, align 4, !tbaa !18
  %call17 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_next, i32 %21)
  %22 = load i32, i32* %call17, align 4, !tbaa !18
  store i32 %22, i32* %index, align 4, !tbaa !18
  br label %while.cond

while.end:                                        ; preds = %land.end
  %23 = load i32, i32* %index, align 4, !tbaa !18
  %cmp18 = icmp eq i32 %23, -1
  br i1 %cmp18, label %if.then19, label %if.end20

if.then19:                                        ; preds = %while.end
  store %struct.btBroadphasePair* null, %struct.btBroadphasePair** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end20:                                         ; preds = %while.end
  %m_overlappingPairArray21 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %24 = load i32, i32* %index, align 4, !tbaa !18
  %call22 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray21, i32 %24)
  store %struct.btBroadphasePair* %call22, %struct.btBroadphasePair** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end20, %if.then19
  %25 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #8
  br label %cleanup23

cleanup23:                                        ; preds = %cleanup, %if.then8
  %26 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #8
  %27 = bitcast i32* %proxyId2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #8
  %28 = bitcast i32* %proxyId1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #8
  %29 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %retval, align 4
  ret %struct.btBroadphasePair* %29
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z6btSwapIP17btBroadphaseProxyEvRT_S3_(%struct.btBroadphaseProxy** nonnull align 4 dereferenceable(4) %a, %struct.btBroadphaseProxy** nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca %struct.btBroadphaseProxy**, align 4
  %b.addr = alloca %struct.btBroadphaseProxy**, align 4
  %tmp = alloca %struct.btBroadphaseProxy*, align 4
  store %struct.btBroadphaseProxy** %a, %struct.btBroadphaseProxy*** %a.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy** %b, %struct.btBroadphaseProxy*** %b.addr, align 4, !tbaa !2
  %0 = bitcast %struct.btBroadphaseProxy** %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %struct.btBroadphaseProxy**, %struct.btBroadphaseProxy*** %a.addr, align 4, !tbaa !2
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %1, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %2, %struct.btBroadphaseProxy** %tmp, align 4, !tbaa !2
  %3 = load %struct.btBroadphaseProxy**, %struct.btBroadphaseProxy*** %b.addr, align 4, !tbaa !2
  %4 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %3, align 4, !tbaa !2
  %5 = load %struct.btBroadphaseProxy**, %struct.btBroadphaseProxy*** %a.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %4, %struct.btBroadphaseProxy** %5, align 4, !tbaa !2
  %6 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %tmp, align 4, !tbaa !2
  %7 = load %struct.btBroadphaseProxy**, %struct.btBroadphaseProxy*** %b.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %6, %struct.btBroadphaseProxy** %7, align 4, !tbaa !2
  %8 = bitcast %struct.btBroadphaseProxy** %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK17btBroadphaseProxy6getUidEv(%struct.btBroadphaseProxy* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btBroadphaseProxy*, align 4
  store %struct.btBroadphaseProxy* %this, %struct.btBroadphaseProxy** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %this.addr, align 4
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 4
  %0 = load i32, i32* %m_uniqueId, align 4, !tbaa !32
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN28btHashedOverlappingPairCache7getHashEjj(%class.btHashedOverlappingPairCache* %this, i32 %proxyId1, i32 %proxyId2) #1 comdat {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %proxyId1.addr = alloca i32, align 4
  %proxyId2.addr = alloca i32, align 4
  %key = alloca i32, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  store i32 %proxyId1, i32* %proxyId1.addr, align 4, !tbaa !18
  store i32 %proxyId2, i32* %proxyId2.addr, align 4, !tbaa !18
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = bitcast i32* %key to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %proxyId1.addr, align 4, !tbaa !18
  %2 = load i32, i32* %proxyId2.addr, align 4, !tbaa !18
  %shl = shl i32 %2, 16
  %or = or i32 %1, %shl
  store i32 %or, i32* %key, align 4, !tbaa !18
  %3 = load i32, i32* %key, align 4, !tbaa !18
  %shl2 = shl i32 %3, 15
  %neg = xor i32 %shl2, -1
  %4 = load i32, i32* %key, align 4, !tbaa !18
  %add = add nsw i32 %4, %neg
  store i32 %add, i32* %key, align 4, !tbaa !18
  %5 = load i32, i32* %key, align 4, !tbaa !18
  %shr = ashr i32 %5, 10
  %6 = load i32, i32* %key, align 4, !tbaa !18
  %xor = xor i32 %6, %shr
  store i32 %xor, i32* %key, align 4, !tbaa !18
  %7 = load i32, i32* %key, align 4, !tbaa !18
  %shl3 = shl i32 %7, 3
  %8 = load i32, i32* %key, align 4, !tbaa !18
  %add4 = add nsw i32 %8, %shl3
  store i32 %add4, i32* %key, align 4, !tbaa !18
  %9 = load i32, i32* %key, align 4, !tbaa !18
  %shr5 = ashr i32 %9, 6
  %10 = load i32, i32* %key, align 4, !tbaa !18
  %xor6 = xor i32 %10, %shr5
  store i32 %xor6, i32* %key, align 4, !tbaa !18
  %11 = load i32, i32* %key, align 4, !tbaa !18
  %shl7 = shl i32 %11, 11
  %neg8 = xor i32 %shl7, -1
  %12 = load i32, i32* %key, align 4, !tbaa !18
  %add9 = add nsw i32 %12, %neg8
  store i32 %add9, i32* %key, align 4, !tbaa !18
  %13 = load i32, i32* %key, align 4, !tbaa !18
  %shr10 = ashr i32 %13, 16
  %14 = load i32, i32* %key, align 4, !tbaa !18
  %xor11 = xor i32 %14, %shr10
  store i32 %xor11, i32* %key, align 4, !tbaa !18
  %15 = load i32, i32* %key, align 4, !tbaa !18
  %16 = bitcast i32* %key to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  ret i32 %15
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !21
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !36
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !18
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !37
  %1 = load i32, i32* %n.addr, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZN28btHashedOverlappingPairCache10equalsPairERK16btBroadphasePairii(%class.btHashedOverlappingPairCache* %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %pair, i32 %proxyId1, i32 %proxyId2) #1 comdat {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %pair.addr = alloca %struct.btBroadphasePair*, align 4
  %proxyId1.addr = alloca i32, align 4
  %proxyId2.addr = alloca i32, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphasePair* %pair, %struct.btBroadphasePair** %pair.addr, align 4, !tbaa !2
  store i32 %proxyId1, i32* %proxyId1.addr, align 4, !tbaa !18
  store i32 %proxyId2, i32* %proxyId2.addr, align 4, !tbaa !18
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4, !tbaa !2
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 0
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !22
  %call = call i32 @_ZNK17btBroadphaseProxy6getUidEv(%struct.btBroadphaseProxy* %1)
  %2 = load i32, i32* %proxyId1.addr, align 4, !tbaa !18
  %cmp = icmp eq i32 %call, %2
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4, !tbaa !2
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %3, i32 0, i32 1
  %4 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !24
  %call2 = call i32 @_ZNK17btBroadphaseProxy6getUidEv(%struct.btBroadphaseProxy* %4)
  %5 = load i32, i32* %proxyId2.addr, align 4, !tbaa !18
  %cmp3 = icmp eq i32 %call2, %5
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %6 = phi i1 [ false, %entry ], [ %cmp3, %land.rhs ]
  ret i1 %6
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !18
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !20
  %1 = load i32, i32* %n.addr, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 %1
  ret %struct.btBroadphasePair* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.0* %this, i32 %newsize, i32* nonnull align 4 dereferenceable(4) %fillData) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca i32*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !18
  store i32* %fillData, i32** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !18
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !18
  %2 = load i32, i32* %curSize, align 4, !tbaa !18
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !18
  store i32 %4, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !18
  %6 = load i32, i32* %curSize, align 4, !tbaa !18
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %8 = load i32*, i32** %m_data, align 4, !tbaa !37
  %9 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds i32, i32* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !18
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !18
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !18
  call void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load i32, i32* %curSize, align 4, !tbaa !18
  store i32 %14, i32* %i6, align 4, !tbaa !18
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !18
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !18
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %18 = load i32*, i32** %m_data11, align 4, !tbaa !37
  %19 = load i32, i32* %i6, align 4, !tbaa !18
  %arrayidx12 = getelementptr inbounds i32, i32* %18, i32 %19
  %20 = bitcast i32* %arrayidx12 to i8*
  %21 = bitcast i8* %20 to i32*
  %22 = load i32*, i32** %fillData.addr, align 4, !tbaa !2
  %23 = load i32, i32* %22, align 4, !tbaa !18
  store i32 %23, i32* %21, align 4, !tbaa !18
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %24 = load i32, i32* %i6, align 4, !tbaa !18
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !18
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %25 = load i32, i32* %newsize.addr, align 4, !tbaa !18
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 %25, i32* %m_size, align 4, !tbaa !36
  %26 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #8
  ret void
}

define hidden %struct.btBroadphasePair* @_ZN28btHashedOverlappingPairCache15internalAddPairEP17btBroadphaseProxyS1_(%class.btHashedOverlappingPairCache* %this, %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy* %proxy1) #0 {
entry:
  %retval = alloca %struct.btBroadphasePair*, align 4
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxyId1 = alloca i32, align 4
  %proxyId2 = alloca i32, align 4
  %hash = alloca i32, align 4
  %pair = alloca %struct.btBroadphasePair*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %count = alloca i32, align 4
  %oldCapacity = alloca i32, align 4
  %mem = alloca i8*, align 4
  %newCapacity = alloca i32, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy1, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %0, i32 0, i32 4
  %1 = load i32, i32* %m_uniqueId, align 4, !tbaa !32
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %m_uniqueId2 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %2, i32 0, i32 4
  %3 = load i32, i32* %m_uniqueId2, align 4, !tbaa !32
  %cmp = icmp sgt i32 %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_Z6btSwapIP17btBroadphaseProxyEvRT_S3_(%struct.btBroadphaseProxy** nonnull align 4 dereferenceable(4) %proxy0.addr, %struct.btBroadphaseProxy** nonnull align 4 dereferenceable(4) %proxy1.addr)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %4 = bitcast i32* %proxyId1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %call = call i32 @_ZNK17btBroadphaseProxy6getUidEv(%struct.btBroadphaseProxy* %5)
  store i32 %call, i32* %proxyId1, align 4, !tbaa !18
  %6 = bitcast i32* %proxyId2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %call3 = call i32 @_ZNK17btBroadphaseProxy6getUidEv(%struct.btBroadphaseProxy* %7)
  store i32 %call3, i32* %proxyId2, align 4, !tbaa !18
  %8 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  %9 = load i32, i32* %proxyId1, align 4, !tbaa !18
  %10 = load i32, i32* %proxyId2, align 4, !tbaa !18
  %call4 = call i32 @_ZN28btHashedOverlappingPairCache7getHashEjj(%class.btHashedOverlappingPairCache* %this1, i32 %9, i32 %10)
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call5 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %m_overlappingPairArray)
  %sub = sub nsw i32 %call5, 1
  %and = and i32 %call4, %sub
  store i32 %and, i32* %hash, align 4, !tbaa !18
  %11 = bitcast %struct.btBroadphasePair** %pair to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  %12 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %13 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %14 = load i32, i32* %hash, align 4, !tbaa !18
  %call6 = call %struct.btBroadphasePair* @_ZN28btHashedOverlappingPairCache16internalFindPairEP17btBroadphaseProxyS1_i(%class.btHashedOverlappingPairCache* %this1, %struct.btBroadphaseProxy* %12, %struct.btBroadphaseProxy* %13, i32 %14)
  store %struct.btBroadphasePair* %call6, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %15 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %cmp7 = icmp ne %struct.btBroadphasePair* %15, null
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end
  %16 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  store %struct.btBroadphasePair* %16, %struct.btBroadphasePair** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end9:                                          ; preds = %if.end
  %17 = bitcast i32* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #8
  %m_overlappingPairArray10 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call11 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %m_overlappingPairArray10)
  store i32 %call11, i32* %count, align 4, !tbaa !18
  %18 = bitcast i32* %oldCapacity to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #8
  %m_overlappingPairArray12 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call13 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %m_overlappingPairArray12)
  store i32 %call13, i32* %oldCapacity, align 4, !tbaa !18
  %19 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  %m_overlappingPairArray14 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call15 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairE21expandNonInitializingEv(%class.btAlignedObjectArray* %m_overlappingPairArray14)
  %20 = bitcast %struct.btBroadphasePair* %call15 to i8*
  store i8* %20, i8** %mem, align 4, !tbaa !2
  %m_ghostPairCallback = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 7
  %21 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_ghostPairCallback, align 4, !tbaa !17
  %tobool = icmp ne %class.btOverlappingPairCallback* %21, null
  br i1 %tobool, label %if.then16, label %if.end19

if.then16:                                        ; preds = %if.end9
  %m_ghostPairCallback17 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 7
  %22 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_ghostPairCallback17, align 4, !tbaa !17
  %23 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %24 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %25 = bitcast %class.btOverlappingPairCallback* %22 to %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %25, align 4, !tbaa !6
  %vfn = getelementptr inbounds %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable, i64 2
  %26 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call18 = call %struct.btBroadphasePair* %26(%class.btOverlappingPairCallback* %22, %struct.btBroadphaseProxy* %23, %struct.btBroadphaseProxy* %24)
  br label %if.end19

if.end19:                                         ; preds = %if.then16, %if.end9
  %27 = bitcast i32* %newCapacity to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #8
  %m_overlappingPairArray20 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call21 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %m_overlappingPairArray20)
  store i32 %call21, i32* %newCapacity, align 4, !tbaa !18
  %28 = load i32, i32* %oldCapacity, align 4, !tbaa !18
  %29 = load i32, i32* %newCapacity, align 4, !tbaa !18
  %cmp22 = icmp slt i32 %28, %29
  br i1 %cmp22, label %if.then23, label %if.end29

if.then23:                                        ; preds = %if.end19
  call void @_ZN28btHashedOverlappingPairCache10growTablesEv(%class.btHashedOverlappingPairCache* %this1)
  %30 = load i32, i32* %proxyId1, align 4, !tbaa !18
  %31 = load i32, i32* %proxyId2, align 4, !tbaa !18
  %call24 = call i32 @_ZN28btHashedOverlappingPairCache7getHashEjj(%class.btHashedOverlappingPairCache* %this1, i32 %30, i32 %31)
  %m_overlappingPairArray25 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call26 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %m_overlappingPairArray25)
  %sub27 = sub nsw i32 %call26, 1
  %and28 = and i32 %call24, %sub27
  store i32 %and28, i32* %hash, align 4, !tbaa !18
  br label %if.end29

if.end29:                                         ; preds = %if.then23, %if.end19
  %32 = load i8*, i8** %mem, align 4, !tbaa !2
  %call30 = call i8* @_ZN16btBroadphasePairnwEmPv(i32 16, i8* %32)
  %33 = bitcast i8* %call30 to %struct.btBroadphasePair*
  %34 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %35 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %call31 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ER17btBroadphaseProxyS1_(%struct.btBroadphasePair* %33, %struct.btBroadphaseProxy* nonnull align 4 dereferenceable(48) %34, %struct.btBroadphaseProxy* nonnull align 4 dereferenceable(48) %35)
  store %struct.btBroadphasePair* %33, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %36 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %36, i32 0, i32 2
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %m_algorithm, align 4, !tbaa !25
  %37 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %38 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %37, i32 0, i32 3
  %m_internalTmpValue = bitcast %union.anon* %38 to i32*
  store i32 0, i32* %m_internalTmpValue, align 4, !tbaa !38
  %m_hashTable = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 5
  %39 = load i32, i32* %hash, align 4, !tbaa !18
  %call32 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_hashTable, i32 %39)
  %40 = load i32, i32* %call32, align 4, !tbaa !18
  %m_next = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 6
  %41 = load i32, i32* %count, align 4, !tbaa !18
  %call33 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_next, i32 %41)
  store i32 %40, i32* %call33, align 4, !tbaa !18
  %42 = load i32, i32* %count, align 4, !tbaa !18
  %m_hashTable34 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 5
  %43 = load i32, i32* %hash, align 4, !tbaa !18
  %call35 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_hashTable34, i32 %43)
  store i32 %42, i32* %call35, align 4, !tbaa !18
  %44 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  store %struct.btBroadphasePair* %44, %struct.btBroadphasePair** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %45 = bitcast i32* %newCapacity to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #8
  %46 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #8
  %47 = bitcast i32* %oldCapacity to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #8
  %48 = bitcast i32* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #8
  br label %cleanup

cleanup:                                          ; preds = %if.end29, %if.then8
  %49 = bitcast %struct.btBroadphasePair** %pair to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #8
  %50 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #8
  %51 = bitcast i32* %proxyId2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #8
  %52 = bitcast i32* %proxyId1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #8
  %53 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %retval, align 4
  ret %struct.btBroadphasePair* %53
}

; Function Attrs: inlinehint
define linkonce_odr hidden %struct.btBroadphasePair* @_ZN28btHashedOverlappingPairCache16internalFindPairEP17btBroadphaseProxyS1_i(%class.btHashedOverlappingPairCache* %this, %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy* %proxy1, i32 %hash) #3 comdat {
entry:
  %retval = alloca %struct.btBroadphasePair*, align 4
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  %hash.addr = alloca i32, align 4
  %proxyId1 = alloca i32, align 4
  %proxyId2 = alloca i32, align 4
  %index = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy1, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  store i32 %hash, i32* %hash.addr, align 4, !tbaa !18
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = bitcast i32* %proxyId1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %call = call i32 @_ZNK17btBroadphaseProxy6getUidEv(%struct.btBroadphaseProxy* %1)
  store i32 %call, i32* %proxyId1, align 4, !tbaa !18
  %2 = bitcast i32* %proxyId2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %call2 = call i32 @_ZNK17btBroadphaseProxy6getUidEv(%struct.btBroadphaseProxy* %3)
  store i32 %call2, i32* %proxyId2, align 4, !tbaa !18
  %4 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %m_hashTable = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 5
  %5 = load i32, i32* %hash.addr, align 4, !tbaa !18
  %call3 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_hashTable, i32 %5)
  %6 = load i32, i32* %call3, align 4, !tbaa !18
  store i32 %6, i32* %index, align 4, !tbaa !18
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %7 = load i32, i32* %index, align 4, !tbaa !18
  %cmp = icmp ne i32 %7, -1
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %8 = load i32, i32* %index, align 4, !tbaa !18
  %call4 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray, i32 %8)
  %9 = load i32, i32* %proxyId1, align 4, !tbaa !18
  %10 = load i32, i32* %proxyId2, align 4, !tbaa !18
  %call5 = call zeroext i1 @_ZN28btHashedOverlappingPairCache10equalsPairERK16btBroadphasePairii(%class.btHashedOverlappingPairCache* %this1, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %call4, i32 %9, i32 %10)
  %conv = zext i1 %call5 to i32
  %cmp6 = icmp eq i32 %conv, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %11 = phi i1 [ false, %while.cond ], [ %cmp6, %land.rhs ]
  br i1 %11, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %m_next = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 6
  %12 = load i32, i32* %index, align 4, !tbaa !18
  %call7 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_next, i32 %12)
  %13 = load i32, i32* %call7, align 4, !tbaa !18
  store i32 %13, i32* %index, align 4, !tbaa !18
  br label %while.cond

while.end:                                        ; preds = %land.end
  %14 = load i32, i32* %index, align 4, !tbaa !18
  %cmp8 = icmp eq i32 %14, -1
  br i1 %cmp8, label %if.then, label %if.end

if.then:                                          ; preds = %while.end
  store %struct.btBroadphasePair* null, %struct.btBroadphasePair** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %while.end
  %m_overlappingPairArray9 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %15 = load i32, i32* %index, align 4, !tbaa !18
  %call10 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray9, i32 %15)
  store %struct.btBroadphasePair* %call10, %struct.btBroadphasePair** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %16 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast i32* %proxyId2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  %18 = bitcast i32* %proxyId1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  %19 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %retval, align 4
  ret %struct.btBroadphasePair* %19
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !39
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairE21expandNonInitializingEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !18
  %1 = load i32, i32* %sz, align 4, !tbaa !18
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI16btBroadphasePairE9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4, !tbaa !39
  %inc = add nsw i32 %2, 1
  store i32 %inc, i32* %m_size, align 4, !tbaa !39
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !20
  %4 = load i32, i32* %sz, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %3, i32 %4
  %5 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  ret %struct.btBroadphasePair* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN16btBroadphasePairnwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !40
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ER17btBroadphaseProxyS1_(%struct.btBroadphasePair* returned %this, %struct.btBroadphaseProxy* nonnull align 4 dereferenceable(48) %proxy0, %struct.btBroadphaseProxy* nonnull align 4 dereferenceable(48) %proxy1) unnamed_addr #4 comdat {
entry:
  %retval = alloca %struct.btBroadphasePair*, align 4
  %this.addr = alloca %struct.btBroadphasePair*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  store %struct.btBroadphasePair* %this, %struct.btBroadphasePair** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy1, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %this1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %this.addr, align 4
  store %struct.btBroadphasePair* %this1, %struct.btBroadphasePair** %retval, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %0, i32 0, i32 4
  %1 = load i32, i32* %m_uniqueId, align 4, !tbaa !32
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %m_uniqueId2 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %2, i32 0, i32 4
  %3 = load i32, i32* %m_uniqueId2, align 4, !tbaa !32
  %cmp = icmp slt i32 %1, %3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 0
  store %struct.btBroadphaseProxy* %4, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !22
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 1
  store %struct.btBroadphaseProxy* %5, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !24
  br label %if.end

if.else:                                          ; preds = %entry
  %6 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %m_pProxy03 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 0
  store %struct.btBroadphaseProxy* %6, %struct.btBroadphaseProxy** %m_pProxy03, align 4, !tbaa !22
  %7 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %m_pProxy14 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 1
  store %struct.btBroadphaseProxy* %7, %struct.btBroadphaseProxy** %m_pProxy14, align 4, !tbaa !24
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 2
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %m_algorithm, align 4, !tbaa !25
  %8 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 3
  %m_internalInfo1 = bitcast %union.anon* %8 to i8**
  store i8* null, i8** %m_internalInfo1, align 4, !tbaa !38
  %9 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %retval, align 4
  ret %struct.btBroadphasePair* %9
}

define hidden i8* @_ZN28btHashedOverlappingPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher(%class.btHashedOverlappingPairCache* %this, %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy* %proxy1, %class.btDispatcher* %dispatcher) unnamed_addr #0 {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %proxyId1 = alloca i32, align 4
  %proxyId2 = alloca i32, align 4
  %hash = alloca i32, align 4
  %pair = alloca %struct.btBroadphasePair*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %userData = alloca i8*, align 4
  %pairIndex = alloca i32, align 4
  %index = alloca i32, align 4
  %previous = alloca i32, align 4
  %lastPairIndex = alloca i32, align 4
  %last = alloca %struct.btBroadphasePair*, align 4
  %lastHash = alloca i32, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy1, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = load i32, i32* @gRemovePairs, align 4, !tbaa !18
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* @gRemovePairs, align 4, !tbaa !18
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %1, i32 0, i32 4
  %2 = load i32, i32* %m_uniqueId, align 4, !tbaa !32
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %m_uniqueId2 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %3, i32 0, i32 4
  %4 = load i32, i32* %m_uniqueId2, align 4, !tbaa !32
  %cmp = icmp sgt i32 %2, %4
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_Z6btSwapIP17btBroadphaseProxyEvRT_S3_(%struct.btBroadphaseProxy** nonnull align 4 dereferenceable(4) %proxy0.addr, %struct.btBroadphaseProxy** nonnull align 4 dereferenceable(4) %proxy1.addr)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %5 = bitcast i32* %proxyId1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %call = call i32 @_ZNK17btBroadphaseProxy6getUidEv(%struct.btBroadphaseProxy* %6)
  store i32 %call, i32* %proxyId1, align 4, !tbaa !18
  %7 = bitcast i32* %proxyId2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %8 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %call3 = call i32 @_ZNK17btBroadphaseProxy6getUidEv(%struct.btBroadphaseProxy* %8)
  store i32 %call3, i32* %proxyId2, align 4, !tbaa !18
  %9 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  %10 = load i32, i32* %proxyId1, align 4, !tbaa !18
  %11 = load i32, i32* %proxyId2, align 4, !tbaa !18
  %call4 = call i32 @_ZN28btHashedOverlappingPairCache7getHashEjj(%class.btHashedOverlappingPairCache* %this1, i32 %10, i32 %11)
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call5 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %m_overlappingPairArray)
  %sub = sub nsw i32 %call5, 1
  %and = and i32 %call4, %sub
  store i32 %and, i32* %hash, align 4, !tbaa !18
  %12 = bitcast %struct.btBroadphasePair** %pair to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  %13 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %14 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %15 = load i32, i32* %hash, align 4, !tbaa !18
  %call6 = call %struct.btBroadphasePair* @_ZN28btHashedOverlappingPairCache16internalFindPairEP17btBroadphaseProxyS1_i(%class.btHashedOverlappingPairCache* %this1, %struct.btBroadphaseProxy* %13, %struct.btBroadphaseProxy* %14, i32 %15)
  store %struct.btBroadphasePair* %call6, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %16 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %cmp7 = icmp eq %struct.btBroadphasePair* %16, null
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end
  store i8* null, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup83

if.end9:                                          ; preds = %if.end
  %17 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %18 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %19 = bitcast %class.btHashedOverlappingPairCache* %this1 to void (%class.btHashedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)***
  %vtable = load void (%class.btHashedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)**, void (%class.btHashedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*** %19, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btHashedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btHashedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vtable, i64 8
  %20 = load void (%class.btHashedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btHashedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vfn, align 4
  call void %20(%class.btHashedOverlappingPairCache* %this1, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %17, %class.btDispatcher* %18)
  %21 = bitcast i8** %userData to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #8
  %22 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %23 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %22, i32 0, i32 3
  %m_internalInfo1 = bitcast %union.anon* %23 to i8**
  %24 = load i8*, i8** %m_internalInfo1, align 4, !tbaa !38
  store i8* %24, i8** %userData, align 4, !tbaa !2
  %25 = bitcast i32* %pairIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  %26 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %m_overlappingPairArray10 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call11 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray10, i32 0)
  %sub.ptr.lhs.cast = ptrtoint %struct.btBroadphasePair* %26 to i32
  %sub.ptr.rhs.cast = ptrtoint %struct.btBroadphasePair* %call11 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 16
  store i32 %sub.ptr.div, i32* %pairIndex, align 4, !tbaa !18
  %27 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #8
  %m_hashTable = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 5
  %28 = load i32, i32* %hash, align 4, !tbaa !18
  %call12 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_hashTable, i32 %28)
  %29 = load i32, i32* %call12, align 4, !tbaa !18
  store i32 %29, i32* %index, align 4, !tbaa !18
  %30 = bitcast i32* %previous to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #8
  store i32 -1, i32* %previous, align 4, !tbaa !18
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end9
  %31 = load i32, i32* %index, align 4, !tbaa !18
  %32 = load i32, i32* %pairIndex, align 4, !tbaa !18
  %cmp13 = icmp ne i32 %31, %32
  br i1 %cmp13, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %33 = load i32, i32* %index, align 4, !tbaa !18
  store i32 %33, i32* %previous, align 4, !tbaa !18
  %m_next = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 6
  %34 = load i32, i32* %index, align 4, !tbaa !18
  %call14 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_next, i32 %34)
  %35 = load i32, i32* %call14, align 4, !tbaa !18
  store i32 %35, i32* %index, align 4, !tbaa !18
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %36 = load i32, i32* %previous, align 4, !tbaa !18
  %cmp15 = icmp ne i32 %36, -1
  br i1 %cmp15, label %if.then16, label %if.else

if.then16:                                        ; preds = %while.end
  %m_next17 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 6
  %37 = load i32, i32* %pairIndex, align 4, !tbaa !18
  %call18 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_next17, i32 %37)
  %38 = load i32, i32* %call18, align 4, !tbaa !18
  %m_next19 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 6
  %39 = load i32, i32* %previous, align 4, !tbaa !18
  %call20 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_next19, i32 %39)
  store i32 %38, i32* %call20, align 4, !tbaa !18
  br label %if.end25

if.else:                                          ; preds = %while.end
  %m_next21 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 6
  %40 = load i32, i32* %pairIndex, align 4, !tbaa !18
  %call22 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_next21, i32 %40)
  %41 = load i32, i32* %call22, align 4, !tbaa !18
  %m_hashTable23 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 5
  %42 = load i32, i32* %hash, align 4, !tbaa !18
  %call24 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_hashTable23, i32 %42)
  store i32 %41, i32* %call24, align 4, !tbaa !18
  br label %if.end25

if.end25:                                         ; preds = %if.else, %if.then16
  %43 = bitcast i32* %lastPairIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #8
  %m_overlappingPairArray26 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call27 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %m_overlappingPairArray26)
  %sub28 = sub nsw i32 %call27, 1
  store i32 %sub28, i32* %lastPairIndex, align 4, !tbaa !18
  %m_ghostPairCallback = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 7
  %44 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_ghostPairCallback, align 4, !tbaa !17
  %tobool = icmp ne %class.btOverlappingPairCallback* %44, null
  br i1 %tobool, label %if.then29, label %if.end34

if.then29:                                        ; preds = %if.end25
  %m_ghostPairCallback30 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 7
  %45 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_ghostPairCallback30, align 4, !tbaa !17
  %46 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %47 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %48 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %49 = bitcast %class.btOverlappingPairCallback* %45 to i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable31 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %49, align 4, !tbaa !6
  %vfn32 = getelementptr inbounds i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable31, i64 3
  %50 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn32, align 4
  %call33 = call i8* %50(%class.btOverlappingPairCallback* %45, %struct.btBroadphaseProxy* %46, %struct.btBroadphaseProxy* %47, %class.btDispatcher* %48)
  br label %if.end34

if.end34:                                         ; preds = %if.then29, %if.end25
  %51 = load i32, i32* %lastPairIndex, align 4, !tbaa !18
  %52 = load i32, i32* %pairIndex, align 4, !tbaa !18
  %cmp35 = icmp eq i32 %51, %52
  br i1 %cmp35, label %if.then36, label %if.end38

if.then36:                                        ; preds = %if.end34
  %m_overlappingPairArray37 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE8pop_backEv(%class.btAlignedObjectArray* %m_overlappingPairArray37)
  %53 = load i8*, i8** %userData, align 4, !tbaa !2
  store i8* %53, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end38:                                         ; preds = %if.end34
  %54 = bitcast %struct.btBroadphasePair** %last to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #8
  %m_overlappingPairArray39 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %55 = load i32, i32* %lastPairIndex, align 4, !tbaa !18
  %call40 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray39, i32 %55)
  store %struct.btBroadphasePair* %call40, %struct.btBroadphasePair** %last, align 4, !tbaa !2
  %56 = bitcast i32* %lastHash to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #8
  %57 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %last, align 4, !tbaa !2
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %57, i32 0, i32 0
  %58 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !22
  %call41 = call i32 @_ZNK17btBroadphaseProxy6getUidEv(%struct.btBroadphaseProxy* %58)
  %59 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %last, align 4, !tbaa !2
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %59, i32 0, i32 1
  %60 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !24
  %call42 = call i32 @_ZNK17btBroadphaseProxy6getUidEv(%struct.btBroadphaseProxy* %60)
  %call43 = call i32 @_ZN28btHashedOverlappingPairCache7getHashEjj(%class.btHashedOverlappingPairCache* %this1, i32 %call41, i32 %call42)
  %m_overlappingPairArray44 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call45 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %m_overlappingPairArray44)
  %sub46 = sub nsw i32 %call45, 1
  %and47 = and i32 %call43, %sub46
  store i32 %and47, i32* %lastHash, align 4, !tbaa !18
  %m_hashTable48 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 5
  %61 = load i32, i32* %lastHash, align 4, !tbaa !18
  %call49 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_hashTable48, i32 %61)
  %62 = load i32, i32* %call49, align 4, !tbaa !18
  store i32 %62, i32* %index, align 4, !tbaa !18
  store i32 -1, i32* %previous, align 4, !tbaa !18
  br label %while.cond50

while.cond50:                                     ; preds = %while.body52, %if.end38
  %63 = load i32, i32* %index, align 4, !tbaa !18
  %64 = load i32, i32* %lastPairIndex, align 4, !tbaa !18
  %cmp51 = icmp ne i32 %63, %64
  br i1 %cmp51, label %while.body52, label %while.end55

while.body52:                                     ; preds = %while.cond50
  %65 = load i32, i32* %index, align 4, !tbaa !18
  store i32 %65, i32* %previous, align 4, !tbaa !18
  %m_next53 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 6
  %66 = load i32, i32* %index, align 4, !tbaa !18
  %call54 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_next53, i32 %66)
  %67 = load i32, i32* %call54, align 4, !tbaa !18
  store i32 %67, i32* %index, align 4, !tbaa !18
  br label %while.cond50

while.end55:                                      ; preds = %while.cond50
  %68 = load i32, i32* %previous, align 4, !tbaa !18
  %cmp56 = icmp ne i32 %68, -1
  br i1 %cmp56, label %if.then57, label %if.else62

if.then57:                                        ; preds = %while.end55
  %m_next58 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 6
  %69 = load i32, i32* %lastPairIndex, align 4, !tbaa !18
  %call59 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_next58, i32 %69)
  %70 = load i32, i32* %call59, align 4, !tbaa !18
  %m_next60 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 6
  %71 = load i32, i32* %previous, align 4, !tbaa !18
  %call61 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_next60, i32 %71)
  store i32 %70, i32* %call61, align 4, !tbaa !18
  br label %if.end67

if.else62:                                        ; preds = %while.end55
  %m_next63 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 6
  %72 = load i32, i32* %lastPairIndex, align 4, !tbaa !18
  %call64 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_next63, i32 %72)
  %73 = load i32, i32* %call64, align 4, !tbaa !18
  %m_hashTable65 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 5
  %74 = load i32, i32* %lastHash, align 4, !tbaa !18
  %call66 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_hashTable65, i32 %74)
  store i32 %73, i32* %call66, align 4, !tbaa !18
  br label %if.end67

if.end67:                                         ; preds = %if.else62, %if.then57
  %m_overlappingPairArray68 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %75 = load i32, i32* %lastPairIndex, align 4, !tbaa !18
  %call69 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray68, i32 %75)
  %m_overlappingPairArray70 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %76 = load i32, i32* %pairIndex, align 4, !tbaa !18
  %call71 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray70, i32 %76)
  %77 = bitcast %struct.btBroadphasePair* %call71 to i8*
  %78 = bitcast %struct.btBroadphasePair* %call69 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %77, i8* align 4 %78, i32 16, i1 false), !tbaa.struct !42
  %m_hashTable72 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 5
  %79 = load i32, i32* %lastHash, align 4, !tbaa !18
  %call73 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_hashTable72, i32 %79)
  %80 = load i32, i32* %call73, align 4, !tbaa !18
  %m_next74 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 6
  %81 = load i32, i32* %pairIndex, align 4, !tbaa !18
  %call75 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_next74, i32 %81)
  store i32 %80, i32* %call75, align 4, !tbaa !18
  %82 = load i32, i32* %pairIndex, align 4, !tbaa !18
  %m_hashTable76 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 5
  %83 = load i32, i32* %lastHash, align 4, !tbaa !18
  %call77 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_hashTable76, i32 %83)
  store i32 %82, i32* %call77, align 4, !tbaa !18
  %m_overlappingPairArray78 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE8pop_backEv(%class.btAlignedObjectArray* %m_overlappingPairArray78)
  %84 = load i8*, i8** %userData, align 4, !tbaa !2
  store i8* %84, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %85 = bitcast i32* %lastHash to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #8
  %86 = bitcast %struct.btBroadphasePair** %last to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #8
  br label %cleanup

cleanup:                                          ; preds = %if.end67, %if.then36
  %87 = bitcast i32* %lastPairIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #8
  %88 = bitcast i32* %previous to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #8
  %89 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #8
  %90 = bitcast i32* %pairIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #8
  %91 = bitcast i8** %userData to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #8
  br label %cleanup83

cleanup83:                                        ; preds = %cleanup, %if.then8
  %92 = bitcast %struct.btBroadphasePair** %pair to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #8
  %93 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #8
  %94 = bitcast i32* %proxyId2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #8
  %95 = bitcast i32* %proxyId1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #8
  %96 = load i8*, i8** %retval, align 4
  ret i8* %96
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE8pop_backEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !39
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4, !tbaa !39
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !20
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4, !tbaa !39
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %1, i32 %2
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

define hidden void @_ZN28btHashedOverlappingPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher(%class.btHashedOverlappingPairCache* %this, %struct.btOverlapCallback* %callback, %class.btDispatcher* %dispatcher) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %callback.addr = alloca %struct.btOverlapCallback*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %i = alloca i32, align 4
  %pair = alloca %struct.btBroadphasePair*, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  store %struct.btOverlapCallback* %callback, %struct.btOverlapCallback** %callback.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.cond:                                         ; preds = %if.end, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !18
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %m_overlappingPairArray)
  %cmp = icmp slt i32 %1, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = bitcast %struct.btBroadphasePair** %pair to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %m_overlappingPairArray2 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %3 = load i32, i32* %i, align 4, !tbaa !18
  %call3 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray2, i32 %3)
  store %struct.btBroadphasePair* %call3, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %4 = load %struct.btOverlapCallback*, %struct.btOverlapCallback** %callback.addr, align 4, !tbaa !2
  %5 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %6 = bitcast %struct.btOverlapCallback* %4 to i1 (%struct.btOverlapCallback*, %struct.btBroadphasePair*)***
  %vtable = load i1 (%struct.btOverlapCallback*, %struct.btBroadphasePair*)**, i1 (%struct.btOverlapCallback*, %struct.btBroadphasePair*)*** %6, align 4, !tbaa !6
  %vfn = getelementptr inbounds i1 (%struct.btOverlapCallback*, %struct.btBroadphasePair*)*, i1 (%struct.btOverlapCallback*, %struct.btBroadphasePair*)** %vtable, i64 2
  %7 = load i1 (%struct.btOverlapCallback*, %struct.btBroadphasePair*)*, i1 (%struct.btOverlapCallback*, %struct.btBroadphasePair*)** %vfn, align 4
  %call4 = call zeroext i1 %7(%struct.btOverlapCallback* %4, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %5)
  br i1 %call4, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %8 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %8, i32 0, i32 0
  %9 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !22
  %10 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %10, i32 0, i32 1
  %11 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !24
  %12 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %13 = bitcast %class.btHashedOverlappingPairCache* %this1 to i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable5 = load i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %13, align 4, !tbaa !6
  %vfn6 = getelementptr inbounds i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable5, i64 3
  %14 = load i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn6, align 4
  %call7 = call i8* %14(%class.btHashedOverlappingPairCache* %this1, %struct.btBroadphaseProxy* %9, %struct.btBroadphaseProxy* %11, %class.btDispatcher* %12)
  %15 = load i32, i32* @gOverlappingPairs, align 4, !tbaa !18
  %dec = add nsw i32 %15, -1
  store i32 %dec, i32* @gOverlappingPairs, align 4, !tbaa !18
  br label %if.end

if.else:                                          ; preds = %for.body
  %16 = load i32, i32* %i, align 4, !tbaa !18
  %inc = add nsw i32 %16, 1
  store i32 %inc, i32* %i, align 4, !tbaa !18
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %17 = bitcast %struct.btBroadphasePair** %pair to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %18 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  ret void
}

define hidden void @_ZN28btHashedOverlappingPairCache20sortOverlappingPairsEP12btDispatcher(%class.btHashedOverlappingPairCache* %this, %class.btDispatcher* %dispatcher) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %tmpPairs = alloca %class.btAlignedObjectArray, align 4
  %i = alloca i32, align 4
  %ref.tmp = alloca %class.btBroadphasePairSortPredicate, align 1
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = bitcast %class.btAlignedObjectArray* %tmpPairs to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %0) #8
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI16btBroadphasePairEC2Ev(%class.btAlignedObjectArray* %tmpPairs)
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store i32 0, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !18
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %m_overlappingPairArray)
  %cmp = icmp slt i32 %2, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_overlappingPairArray3 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %3 = load i32, i32* %i, align 4, !tbaa !18
  %call4 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray3, i32 %3)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE9push_backERKS0_(%class.btAlignedObjectArray* %tmpPairs, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %call4)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4, !tbaa !18
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !18
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc12, %for.end
  %5 = load i32, i32* %i, align 4, !tbaa !18
  %call6 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %tmpPairs)
  %cmp7 = icmp slt i32 %5, %call6
  br i1 %cmp7, label %for.body8, label %for.end14

for.body8:                                        ; preds = %for.cond5
  %6 = load i32, i32* %i, align 4, !tbaa !18
  %call9 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %tmpPairs, i32 %6)
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %call9, i32 0, i32 0
  %7 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !22
  %8 = load i32, i32* %i, align 4, !tbaa !18
  %call10 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %tmpPairs, i32 %8)
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %call10, i32 0, i32 1
  %9 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !24
  %10 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %11 = bitcast %class.btHashedOverlappingPairCache* %this1 to i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable = load i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %11, align 4, !tbaa !6
  %vfn = getelementptr inbounds i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable, i64 3
  %12 = load i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn, align 4
  %call11 = call i8* %12(%class.btHashedOverlappingPairCache* %this1, %struct.btBroadphaseProxy* %7, %struct.btBroadphaseProxy* %9, %class.btDispatcher* %10)
  br label %for.inc12

for.inc12:                                        ; preds = %for.body8
  %13 = load i32, i32* %i, align 4, !tbaa !18
  %inc13 = add nsw i32 %13, 1
  store i32 %inc13, i32* %i, align 4, !tbaa !18
  br label %for.cond5

for.end14:                                        ; preds = %for.cond5
  store i32 0, i32* %i, align 4, !tbaa !18
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc21, %for.end14
  %14 = load i32, i32* %i, align 4, !tbaa !18
  %m_next = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 6
  %call16 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.0* %m_next)
  %cmp17 = icmp slt i32 %14, %call16
  br i1 %cmp17, label %for.body18, label %for.end23

for.body18:                                       ; preds = %for.cond15
  %m_next19 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 6
  %15 = load i32, i32* %i, align 4, !tbaa !18
  %call20 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_next19, i32 %15)
  store i32 -1, i32* %call20, align 4, !tbaa !18
  br label %for.inc21

for.inc21:                                        ; preds = %for.body18
  %16 = load i32, i32* %i, align 4, !tbaa !18
  %inc22 = add nsw i32 %16, 1
  store i32 %inc22, i32* %i, align 4, !tbaa !18
  br label %for.cond15

for.end23:                                        ; preds = %for.cond15
  %17 = bitcast %class.btBroadphasePairSortPredicate* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %17) #8
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvRKT_(%class.btAlignedObjectArray* %tmpPairs, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %ref.tmp)
  %18 = bitcast %class.btBroadphasePairSortPredicate* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %18) #8
  store i32 0, i32* %i, align 4, !tbaa !18
  br label %for.cond24

for.cond24:                                       ; preds = %for.inc35, %for.end23
  %19 = load i32, i32* %i, align 4, !tbaa !18
  %call25 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %tmpPairs)
  %cmp26 = icmp slt i32 %19, %call25
  br i1 %cmp26, label %for.body27, label %for.end37

for.body27:                                       ; preds = %for.cond24
  %20 = load i32, i32* %i, align 4, !tbaa !18
  %call28 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %tmpPairs, i32 %20)
  %m_pProxy029 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %call28, i32 0, i32 0
  %21 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy029, align 4, !tbaa !22
  %22 = load i32, i32* %i, align 4, !tbaa !18
  %call30 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %tmpPairs, i32 %22)
  %m_pProxy131 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %call30, i32 0, i32 1
  %23 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy131, align 4, !tbaa !24
  %24 = bitcast %class.btHashedOverlappingPairCache* %this1 to %struct.btBroadphasePair* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable32 = load %struct.btBroadphasePair* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, %struct.btBroadphasePair* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %24, align 4, !tbaa !6
  %vfn33 = getelementptr inbounds %struct.btBroadphasePair* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable32, i64 2
  %25 = load %struct.btBroadphasePair* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn33, align 4
  %call34 = call %struct.btBroadphasePair* %25(%class.btHashedOverlappingPairCache* %this1, %struct.btBroadphaseProxy* %21, %struct.btBroadphaseProxy* %23)
  br label %for.inc35

for.inc35:                                        ; preds = %for.body27
  %26 = load i32, i32* %i, align 4, !tbaa !18
  %inc36 = add nsw i32 %26, 1
  store i32 %inc36, i32* %i, align 4, !tbaa !18
  br label %for.cond24

for.end37:                                        ; preds = %for.cond24
  %27 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #8
  %call38 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI16btBroadphasePairED2Ev(%class.btAlignedObjectArray* %tmpPairs) #8
  %28 = bitcast %class.btAlignedObjectArray* %tmpPairs to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %28) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE9push_backERKS0_(%class.btAlignedObjectArray* %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %_Val) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %struct.btBroadphasePair*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphasePair* %_Val, %struct.btBroadphasePair** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !18
  %1 = load i32, i32* %sz, align 4, !tbaa !18
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI16btBroadphasePairE9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !20
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !39
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 %3
  %4 = bitcast %struct.btBroadphasePair* %arrayidx to i8*
  %call5 = call i8* @_ZN16btBroadphasePairnwEmPv(i32 16, i8* %4)
  %5 = bitcast i8* %call5 to %struct.btBroadphasePair*
  %6 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %_Val.addr, align 4, !tbaa !2
  %call6 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %5, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %6)
  %m_size7 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size7, align 4, !tbaa !39
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size7, align 4, !tbaa !39
  %8 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  ret void
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvRKT_(%class.btAlignedObjectArray* %this, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %CompareFunc) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %CompareFunc.addr = alloca %class.btBroadphasePairSortPredicate*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store %class.btBroadphasePairSortPredicate* %CompareFunc, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp sgt i32 %call, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray* %this1, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %0, i32 0, i32 %sub)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

define hidden i8* @_ZN28btSortedOverlappingPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher(%class.btSortedOverlappingPairCache* %this, %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy* %proxy1, %class.btDispatcher* %dispatcher) unnamed_addr #0 {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %findPair = alloca %struct.btBroadphasePair, align 4
  %findIndex = alloca i32, align 4
  %pair = alloca %struct.btBroadphasePair*, align 4
  %userData = alloca i8*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy1, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %0 = bitcast %class.btSortedOverlappingPairCache* %this1 to i1 (%class.btSortedOverlappingPairCache*)***
  %vtable = load i1 (%class.btSortedOverlappingPairCache*)**, i1 (%class.btSortedOverlappingPairCache*)*** %0, align 4, !tbaa !6
  %vfn = getelementptr inbounds i1 (%class.btSortedOverlappingPairCache*)*, i1 (%class.btSortedOverlappingPairCache*)** %vtable, i64 14
  %1 = load i1 (%class.btSortedOverlappingPairCache*)*, i1 (%class.btSortedOverlappingPairCache*)** %vfn, align 4
  %call = call zeroext i1 %1(%class.btSortedOverlappingPairCache* %this1)
  br i1 %call, label %if.end22, label %if.then

if.then:                                          ; preds = %entry
  %2 = bitcast %struct.btBroadphasePair* %findPair to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #8
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %4 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %call2 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ER17btBroadphaseProxyS1_(%struct.btBroadphasePair* %findPair, %struct.btBroadphaseProxy* nonnull align 4 dereferenceable(48) %3, %struct.btBroadphaseProxy* nonnull align 4 dereferenceable(48) %4)
  %5 = bitcast i32* %findIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %m_overlappingPairArray = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE16findLinearSearchERKS0_(%class.btAlignedObjectArray* %m_overlappingPairArray, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %findPair)
  store i32 %call3, i32* %findIndex, align 4, !tbaa !18
  %6 = load i32, i32* %findIndex, align 4, !tbaa !18
  %m_overlappingPairArray4 = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %call5 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %m_overlappingPairArray4)
  %cmp = icmp slt i32 %6, %call5
  br i1 %cmp, label %if.then6, label %if.end20

if.then6:                                         ; preds = %if.then
  %7 = load i32, i32* @gOverlappingPairs, align 4, !tbaa !18
  %dec = add nsw i32 %7, -1
  store i32 %dec, i32* @gOverlappingPairs, align 4, !tbaa !18
  %8 = bitcast %struct.btBroadphasePair** %pair to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  %m_overlappingPairArray7 = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %9 = load i32, i32* %findIndex, align 4, !tbaa !18
  %call8 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray7, i32 %9)
  store %struct.btBroadphasePair* %call8, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %10 = bitcast i8** %userData to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %12 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %11, i32 0, i32 3
  %m_internalInfo1 = bitcast %union.anon* %12 to i8**
  %13 = load i8*, i8** %m_internalInfo1, align 4, !tbaa !38
  store i8* %13, i8** %userData, align 4, !tbaa !2
  %14 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %15 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %16 = bitcast %class.btSortedOverlappingPairCache* %this1 to void (%class.btSortedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)***
  %vtable9 = load void (%class.btSortedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)**, void (%class.btSortedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*** %16, align 4, !tbaa !6
  %vfn10 = getelementptr inbounds void (%class.btSortedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btSortedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vtable9, i64 8
  %17 = load void (%class.btSortedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btSortedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vfn10, align 4
  call void %17(%class.btSortedOverlappingPairCache* %this1, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %14, %class.btDispatcher* %15)
  %m_ghostPairCallback = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 5
  %18 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_ghostPairCallback, align 4, !tbaa !43
  %tobool = icmp ne %class.btOverlappingPairCallback* %18, null
  br i1 %tobool, label %if.then11, label %if.end

if.then11:                                        ; preds = %if.then6
  %m_ghostPairCallback12 = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 5
  %19 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_ghostPairCallback12, align 4, !tbaa !43
  %20 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %21 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %22 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %23 = bitcast %class.btOverlappingPairCallback* %19 to i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable13 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %23, align 4, !tbaa !6
  %vfn14 = getelementptr inbounds i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable13, i64 3
  %24 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn14, align 4
  %call15 = call i8* %24(%class.btOverlappingPairCallback* %19, %struct.btBroadphaseProxy* %20, %struct.btBroadphaseProxy* %21, %class.btDispatcher* %22)
  br label %if.end

if.end:                                           ; preds = %if.then11, %if.then6
  %m_overlappingPairArray16 = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %25 = load i32, i32* %findIndex, align 4, !tbaa !18
  %m_overlappingPairArray17 = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %call18 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %m_overlappingPairArray17)
  %sub = sub nsw i32 %call18, 1
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4swapEii(%class.btAlignedObjectArray* %m_overlappingPairArray16, i32 %25, i32 %sub)
  %m_overlappingPairArray19 = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE8pop_backEv(%class.btAlignedObjectArray* %m_overlappingPairArray19)
  %26 = load i8*, i8** %userData, align 4, !tbaa !2
  store i8* %26, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %27 = bitcast i8** %userData to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #8
  %28 = bitcast %struct.btBroadphasePair** %pair to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #8
  br label %cleanup

if.end20:                                         ; preds = %if.then
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end20, %if.end
  %29 = bitcast i32* %findIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #8
  %30 = bitcast %struct.btBroadphasePair* %findPair to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %30) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %return
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end22

if.end22:                                         ; preds = %cleanup.cont, %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end22, %cleanup
  %31 = load i8*, i8** %retval, align 4
  ret i8* %31

unreachable:                                      ; preds = %cleanup
  unreachable
}

define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE16findLinearSearchERKS0_(%class.btAlignedObjectArray* %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %key) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %key.addr = alloca %struct.btBroadphasePair*, align 4
  %index = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphasePair* %key, %struct.btBroadphasePair** %key.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %index, align 4, !tbaa !18
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store i32 0, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !18
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp slt i32 %2, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !20
  %4 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %3, i32 %4
  %5 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %key.addr, align 4, !tbaa !2
  %call3 = call zeroext i1 @_ZeqRK16btBroadphasePairS1_(%struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %5)
  br i1 %call3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !18
  store i32 %6, i32* %index, align 4, !tbaa !18
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %7 = load i32, i32* %i, align 4, !tbaa !18
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %8 = load i32, i32* %index, align 4, !tbaa !18
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  ret i32 %8
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4swapEii(%class.btAlignedObjectArray* %this, i32 %index0, i32 %index1) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %struct.btBroadphasePair, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %index0, i32* %index0.addr, align 4, !tbaa !18
  store i32 %index1, i32* %index1.addr, align 4, !tbaa !18
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast %struct.btBroadphasePair* %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !20
  %2 = load i32, i32* %index0.addr, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %1, i32 %2
  %call = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %temp, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx)
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data2, align 4, !tbaa !20
  %4 = load i32, i32* %index1.addr, align 4, !tbaa !18
  %arrayidx3 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %3, i32 %4
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %5 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data4, align 4, !tbaa !20
  %6 = load i32, i32* %index0.addr, align 4, !tbaa !18
  %arrayidx5 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %5, i32 %6
  %7 = bitcast %struct.btBroadphasePair* %arrayidx5 to i8*
  %8 = bitcast %struct.btBroadphasePair* %arrayidx3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !42
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %9 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data6, align 4, !tbaa !20
  %10 = load i32, i32* %index1.addr, align 4, !tbaa !18
  %arrayidx7 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %9, i32 %10
  %11 = bitcast %struct.btBroadphasePair* %arrayidx7 to i8*
  %12 = bitcast %struct.btBroadphasePair* %temp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 16, i1 false), !tbaa.struct !42
  %13 = bitcast %struct.btBroadphasePair* %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %13) #8
  ret void
}

define hidden %struct.btBroadphasePair* @_ZN28btSortedOverlappingPairCache18addOverlappingPairEP17btBroadphaseProxyS1_(%class.btSortedOverlappingPairCache* %this, %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy* %proxy1) unnamed_addr #0 {
entry:
  %retval = alloca %struct.btBroadphasePair*, align 4
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  %mem = alloca i8*, align 4
  %pair = alloca %struct.btBroadphasePair*, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy1, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %call = call zeroext i1 @_ZNK28btSortedOverlappingPairCache24needsBroadphaseCollisionEP17btBroadphaseProxyS1_(%class.btSortedOverlappingPairCache* %this1, %struct.btBroadphaseProxy* %0, %struct.btBroadphaseProxy* %1)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store %struct.btBroadphasePair* null, %struct.btBroadphasePair** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %m_overlappingPairArray = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %call2 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairE21expandNonInitializingEv(%class.btAlignedObjectArray* %m_overlappingPairArray)
  %3 = bitcast %struct.btBroadphasePair* %call2 to i8*
  store i8* %3, i8** %mem, align 4, !tbaa !2
  %4 = bitcast %struct.btBroadphasePair** %pair to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load i8*, i8** %mem, align 4, !tbaa !2
  %call3 = call i8* @_ZN16btBroadphasePairnwEmPv(i32 16, i8* %5)
  %6 = bitcast i8* %call3 to %struct.btBroadphasePair*
  %7 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %8 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %call4 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ER17btBroadphaseProxyS1_(%struct.btBroadphasePair* %6, %struct.btBroadphaseProxy* nonnull align 4 dereferenceable(48) %7, %struct.btBroadphaseProxy* nonnull align 4 dereferenceable(48) %8)
  store %struct.btBroadphasePair* %6, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %9 = load i32, i32* @gOverlappingPairs, align 4, !tbaa !18
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* @gOverlappingPairs, align 4, !tbaa !18
  %10 = load i32, i32* @gAddedPairs, align 4, !tbaa !18
  %inc5 = add nsw i32 %10, 1
  store i32 %inc5, i32* @gAddedPairs, align 4, !tbaa !18
  %m_ghostPairCallback = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 5
  %11 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_ghostPairCallback, align 4, !tbaa !43
  %tobool = icmp ne %class.btOverlappingPairCallback* %11, null
  br i1 %tobool, label %if.then6, label %if.end9

if.then6:                                         ; preds = %if.end
  %m_ghostPairCallback7 = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 5
  %12 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_ghostPairCallback7, align 4, !tbaa !43
  %13 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %14 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %15 = bitcast %class.btOverlappingPairCallback* %12 to %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %15, align 4, !tbaa !6
  %vfn = getelementptr inbounds %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable, i64 2
  %16 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call8 = call %struct.btBroadphasePair* %16(%class.btOverlappingPairCallback* %12, %struct.btBroadphaseProxy* %13, %struct.btBroadphaseProxy* %14)
  br label %if.end9

if.end9:                                          ; preds = %if.then6, %if.end
  %17 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  store %struct.btBroadphasePair* %17, %struct.btBroadphasePair** %retval, align 4
  %18 = bitcast %struct.btBroadphasePair** %pair to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  %19 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  br label %return

return:                                           ; preds = %if.end9, %if.then
  %20 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %retval, align 4
  ret %struct.btBroadphasePair* %20
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_ZNK28btSortedOverlappingPairCache24needsBroadphaseCollisionEP17btBroadphaseProxyS1_(%class.btSortedOverlappingPairCache* %this, %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy* %proxy1) #3 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  %collides = alloca i8, align 1
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy1, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %m_overlapFilterCallback = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 4
  %0 = load %struct.btOverlapFilterCallback*, %struct.btOverlapFilterCallback** %m_overlapFilterCallback, align 4, !tbaa !45
  %tobool = icmp ne %struct.btOverlapFilterCallback* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_overlapFilterCallback2 = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 4
  %1 = load %struct.btOverlapFilterCallback*, %struct.btOverlapFilterCallback** %m_overlapFilterCallback2, align 4, !tbaa !45
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %4 = bitcast %struct.btOverlapFilterCallback* %1 to i1 (%struct.btOverlapFilterCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable = load i1 (%struct.btOverlapFilterCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, i1 (%struct.btOverlapFilterCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %4, align 4, !tbaa !6
  %vfn = getelementptr inbounds i1 (%struct.btOverlapFilterCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, i1 (%struct.btOverlapFilterCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable, i64 2
  %5 = load i1 (%struct.btOverlapFilterCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, i1 (%struct.btOverlapFilterCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call = call zeroext i1 %5(%struct.btOverlapFilterCallback* %1, %struct.btBroadphaseProxy* %2, %struct.btBroadphaseProxy* %3)
  store i1 %call, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %collides) #8
  %6 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %m_collisionFilterGroup = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %6, i32 0, i32 1
  %7 = load i16, i16* %m_collisionFilterGroup, align 4, !tbaa !46
  %conv = sext i16 %7 to i32
  %8 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %m_collisionFilterMask = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %8, i32 0, i32 2
  %9 = load i16, i16* %m_collisionFilterMask, align 2, !tbaa !47
  %conv3 = sext i16 %9 to i32
  %and = and i32 %conv, %conv3
  %cmp = icmp ne i32 %and, 0
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %collides, align 1, !tbaa !48
  %10 = load i8, i8* %collides, align 1, !tbaa !48, !range !49
  %tobool4 = trunc i8 %10 to i1
  br i1 %tobool4, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %if.end
  %11 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %m_collisionFilterGroup5 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %11, i32 0, i32 1
  %12 = load i16, i16* %m_collisionFilterGroup5, align 4, !tbaa !46
  %conv6 = sext i16 %12 to i32
  %13 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %m_collisionFilterMask7 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %13, i32 0, i32 2
  %14 = load i16, i16* %m_collisionFilterMask7, align 2, !tbaa !47
  %conv8 = sext i16 %14 to i32
  %and9 = and i32 %conv6, %conv8
  %tobool10 = icmp ne i32 %and9, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.end
  %15 = phi i1 [ false, %if.end ], [ %tobool10, %land.rhs ]
  %frombool11 = zext i1 %15 to i8
  store i8 %frombool11, i8* %collides, align 1, !tbaa !48
  %16 = load i8, i8* %collides, align 1, !tbaa !48, !range !49
  %tobool12 = trunc i8 %16 to i1
  store i1 %tobool12, i1* %retval, align 1
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %collides) #8
  br label %return

return:                                           ; preds = %land.end, %if.then
  %17 = load i1, i1* %retval, align 1
  ret i1 %17
}

define hidden %struct.btBroadphasePair* @_ZN28btSortedOverlappingPairCache8findPairEP17btBroadphaseProxyS1_(%class.btSortedOverlappingPairCache* %this, %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy* %proxy1) unnamed_addr #0 {
entry:
  %retval = alloca %struct.btBroadphasePair*, align 4
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  %tmpPair = alloca %struct.btBroadphasePair, align 4
  %findIndex = alloca i32, align 4
  %pair = alloca %struct.btBroadphasePair*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy1, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %call = call zeroext i1 @_ZNK28btSortedOverlappingPairCache24needsBroadphaseCollisionEP17btBroadphaseProxyS1_(%class.btSortedOverlappingPairCache* %this1, %struct.btBroadphaseProxy* %0, %struct.btBroadphaseProxy* %1)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store %struct.btBroadphasePair* null, %struct.btBroadphasePair** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = bitcast %struct.btBroadphasePair* %tmpPair to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #8
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %4 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %call2 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ER17btBroadphaseProxyS1_(%struct.btBroadphasePair* %tmpPair, %struct.btBroadphaseProxy* nonnull align 4 dereferenceable(48) %3, %struct.btBroadphaseProxy* nonnull align 4 dereferenceable(48) %4)
  %5 = bitcast i32* %findIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %m_overlappingPairArray = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE16findLinearSearchERKS0_(%class.btAlignedObjectArray* %m_overlappingPairArray, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %tmpPair)
  store i32 %call3, i32* %findIndex, align 4, !tbaa !18
  %6 = load i32, i32* %findIndex, align 4, !tbaa !18
  %m_overlappingPairArray4 = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %call5 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %m_overlappingPairArray4)
  %cmp = icmp slt i32 %6, %call5
  br i1 %cmp, label %if.then6, label %if.end9

if.then6:                                         ; preds = %if.end
  %7 = bitcast %struct.btBroadphasePair** %pair to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %m_overlappingPairArray7 = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %8 = load i32, i32* %findIndex, align 4, !tbaa !18
  %call8 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray7, i32 %8)
  store %struct.btBroadphasePair* %call8, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %9 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  store %struct.btBroadphasePair* %9, %struct.btBroadphasePair** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %10 = bitcast %struct.btBroadphasePair** %pair to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  br label %cleanup

if.end9:                                          ; preds = %if.end
  store %struct.btBroadphasePair* null, %struct.btBroadphasePair** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end9, %if.then6
  %11 = bitcast i32* %findIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  %12 = bitcast %struct.btBroadphasePair* %tmpPair to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %12) #8
  br label %return

return:                                           ; preds = %cleanup, %if.then
  %13 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %retval, align 4
  ret %struct.btBroadphasePair* %13
}

define hidden void @_ZN28btSortedOverlappingPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher(%class.btSortedOverlappingPairCache* %this, %struct.btOverlapCallback* %callback, %class.btDispatcher* %dispatcher) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  %callback.addr = alloca %struct.btOverlapCallback*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %i = alloca i32, align 4
  %pair = alloca %struct.btBroadphasePair*, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  store %struct.btOverlapCallback* %callback, %struct.btOverlapCallback** %callback.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.cond:                                         ; preds = %if.end, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !18
  %m_overlappingPairArray = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %m_overlappingPairArray)
  %cmp = icmp slt i32 %1, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = bitcast %struct.btBroadphasePair** %pair to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %m_overlappingPairArray2 = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %3 = load i32, i32* %i, align 4, !tbaa !18
  %call3 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray2, i32 %3)
  store %struct.btBroadphasePair* %call3, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %4 = load %struct.btOverlapCallback*, %struct.btOverlapCallback** %callback.addr, align 4, !tbaa !2
  %5 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %6 = bitcast %struct.btOverlapCallback* %4 to i1 (%struct.btOverlapCallback*, %struct.btBroadphasePair*)***
  %vtable = load i1 (%struct.btOverlapCallback*, %struct.btBroadphasePair*)**, i1 (%struct.btOverlapCallback*, %struct.btBroadphasePair*)*** %6, align 4, !tbaa !6
  %vfn = getelementptr inbounds i1 (%struct.btOverlapCallback*, %struct.btBroadphasePair*)*, i1 (%struct.btOverlapCallback*, %struct.btBroadphasePair*)** %vtable, i64 2
  %7 = load i1 (%struct.btOverlapCallback*, %struct.btBroadphasePair*)*, i1 (%struct.btOverlapCallback*, %struct.btBroadphasePair*)** %vfn, align 4
  %call4 = call zeroext i1 %7(%struct.btOverlapCallback* %4, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %5)
  br i1 %call4, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %8 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %9 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %10 = bitcast %class.btSortedOverlappingPairCache* %this1 to void (%class.btSortedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)***
  %vtable5 = load void (%class.btSortedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)**, void (%class.btSortedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*** %10, align 4, !tbaa !6
  %vfn6 = getelementptr inbounds void (%class.btSortedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btSortedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vtable5, i64 8
  %11 = load void (%class.btSortedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btSortedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vfn6, align 4
  call void %11(%class.btSortedOverlappingPairCache* %this1, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %8, %class.btDispatcher* %9)
  %12 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %12, i32 0, i32 0
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !22
  %13 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %13, i32 0, i32 1
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !24
  %m_overlappingPairArray7 = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %14 = load i32, i32* %i, align 4, !tbaa !18
  %m_overlappingPairArray8 = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %call9 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %m_overlappingPairArray8)
  %sub = sub nsw i32 %call9, 1
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4swapEii(%class.btAlignedObjectArray* %m_overlappingPairArray7, i32 %14, i32 %sub)
  %m_overlappingPairArray10 = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE8pop_backEv(%class.btAlignedObjectArray* %m_overlappingPairArray10)
  %15 = load i32, i32* @gOverlappingPairs, align 4, !tbaa !18
  %dec = add nsw i32 %15, -1
  store i32 %dec, i32* @gOverlappingPairs, align 4, !tbaa !18
  br label %if.end

if.else:                                          ; preds = %for.body
  %16 = load i32, i32* %i, align 4, !tbaa !18
  %inc = add nsw i32 %16, 1
  store i32 %inc, i32* %i, align 4, !tbaa !18
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %17 = bitcast %struct.btBroadphasePair** %pair to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %18 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  ret void
}

define hidden %class.btSortedOverlappingPairCache* @_ZN28btSortedOverlappingPairCacheC2Ev(%class.btSortedOverlappingPairCache* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  %initialAllocatedSize = alloca i32, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %0 = bitcast %class.btSortedOverlappingPairCache* %this1 to %class.btOverlappingPairCache*
  %call = call %class.btOverlappingPairCache* @_ZN22btOverlappingPairCacheC2Ev(%class.btOverlappingPairCache* %0) #8
  %1 = bitcast %class.btSortedOverlappingPairCache* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [19 x i8*] }, { [19 x i8*] }* @_ZTV28btSortedOverlappingPairCache, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_overlappingPairArray = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI16btBroadphasePairEC2Ev(%class.btAlignedObjectArray* %m_overlappingPairArray)
  %m_blockedForChanges = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 2
  store i8 0, i8* %m_blockedForChanges, align 4, !tbaa !50
  %m_hasDeferredRemoval = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 3
  store i8 1, i8* %m_hasDeferredRemoval, align 1, !tbaa !51
  %m_overlapFilterCallback = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 4
  store %struct.btOverlapFilterCallback* null, %struct.btOverlapFilterCallback** %m_overlapFilterCallback, align 4, !tbaa !45
  %m_ghostPairCallback = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 5
  store %class.btOverlappingPairCallback* null, %class.btOverlappingPairCallback** %m_ghostPairCallback, align 4, !tbaa !43
  %2 = bitcast i32* %initialAllocatedSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store i32 2, i32* %initialAllocatedSize, align 4, !tbaa !18
  %m_overlappingPairArray3 = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %3 = load i32, i32* %initialAllocatedSize, align 4, !tbaa !18
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi(%class.btAlignedObjectArray* %m_overlappingPairArray3, i32 %3)
  %4 = bitcast i32* %initialAllocatedSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  ret %class.btSortedOverlappingPairCache* %this1
}

; Function Attrs: nounwind
define hidden %class.btSortedOverlappingPairCache* @_ZN28btSortedOverlappingPairCacheD2Ev(%class.btSortedOverlappingPairCache* returned %this) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %0 = bitcast %class.btSortedOverlappingPairCache* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [19 x i8*] }, { [19 x i8*] }* @_ZTV28btSortedOverlappingPairCache, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  %m_overlappingPairArray = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI16btBroadphasePairED2Ev(%class.btAlignedObjectArray* %m_overlappingPairArray) #8
  %1 = bitcast %class.btSortedOverlappingPairCache* %this1 to %class.btOverlappingPairCache*
  %call2 = call %class.btOverlappingPairCache* bitcast (%class.btOverlappingPairCallback* (%class.btOverlappingPairCallback*)* @_ZN25btOverlappingPairCallbackD2Ev to %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*)(%class.btOverlappingPairCache* %1) #8
  ret %class.btSortedOverlappingPairCache* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN28btSortedOverlappingPairCacheD0Ev(%class.btSortedOverlappingPairCache* %this) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %call = call %class.btSortedOverlappingPairCache* @_ZN28btSortedOverlappingPairCacheD1Ev(%class.btSortedOverlappingPairCache* %this1) #8
  %0 = bitcast %class.btSortedOverlappingPairCache* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

define hidden void @_ZN28btSortedOverlappingPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher(%class.btSortedOverlappingPairCache* %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %pair, %class.btDispatcher* %dispatcher) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  %pair.addr = alloca %struct.btBroadphasePair*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphasePair* %pair, %struct.btBroadphasePair** %pair.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4, !tbaa !2
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 2
  %1 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm, align 4, !tbaa !25
  %tobool = icmp ne %class.btCollisionAlgorithm* %1, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4, !tbaa !2
  %m_algorithm2 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 0, i32 2
  %3 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm2, align 4, !tbaa !25
  %4 = bitcast %class.btCollisionAlgorithm* %3 to %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)***
  %vtable = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)**, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*** %4, align 4, !tbaa !6
  %vfn = getelementptr inbounds %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vtable, i64 0
  %5 = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vfn, align 4
  %call = call %class.btCollisionAlgorithm* %5(%class.btCollisionAlgorithm* %3) #8
  %6 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %7 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4, !tbaa !2
  %m_algorithm3 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %7, i32 0, i32 2
  %8 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm3, align 4, !tbaa !25
  %9 = bitcast %class.btCollisionAlgorithm* %8 to i8*
  %10 = bitcast %class.btDispatcher* %6 to void (%class.btDispatcher*, i8*)***
  %vtable4 = load void (%class.btDispatcher*, i8*)**, void (%class.btDispatcher*, i8*)*** %10, align 4, !tbaa !6
  %vfn5 = getelementptr inbounds void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vtable4, i64 15
  %11 = load void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vfn5, align 4
  call void %11(%class.btDispatcher* %6, i8* %9)
  %12 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4, !tbaa !2
  %m_algorithm6 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %12, i32 0, i32 2
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %m_algorithm6, align 4, !tbaa !25
  %13 = load i32, i32* @gRemovePairs, align 4, !tbaa !18
  %dec = add nsw i32 %13, -1
  store i32 %dec, i32* @gRemovePairs, align 4, !tbaa !18
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

define hidden void @_ZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher(%class.btSortedOverlappingPairCache* %this, %struct.btBroadphaseProxy* %proxy, %class.btDispatcher* %dispatcher) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %cleanPairs = alloca %class.CleanPairCallback.4, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %0 = bitcast %class.CleanPairCallback.4* %cleanPairs to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  %2 = bitcast %class.btSortedOverlappingPairCache* %this1 to %class.btOverlappingPairCache*
  %3 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %call = call %class.CleanPairCallback.4* @_ZZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackC2ES1_P22btOverlappingPairCacheS3_(%class.CleanPairCallback.4* %cleanPairs, %struct.btBroadphaseProxy* %1, %class.btOverlappingPairCache* %2, %class.btDispatcher* %3)
  %4 = bitcast %class.CleanPairCallback.4* %cleanPairs to %struct.btOverlapCallback*
  %5 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %6 = bitcast %class.btSortedOverlappingPairCache* %this1 to void (%class.btSortedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)***
  %vtable = load void (%class.btSortedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)**, void (%class.btSortedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)*** %6, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btSortedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)*, void (%class.btSortedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)** %vtable, i64 12
  %7 = load void (%class.btSortedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)*, void (%class.btSortedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)** %vfn, align 4
  call void %7(%class.btSortedOverlappingPairCache* %this1, %struct.btOverlapCallback* %4, %class.btDispatcher* %5)
  %call2 = call %class.CleanPairCallback.4* bitcast (%struct.btOverlapCallback* (%struct.btOverlapCallback*)* @_ZN17btOverlapCallbackD2Ev to %class.CleanPairCallback.4* (%class.CleanPairCallback.4*)*)(%class.CleanPairCallback.4* %cleanPairs) #8
  %8 = bitcast %class.CleanPairCallback.4* %cleanPairs to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #8
  ret void
}

; Function Attrs: nounwind
define internal %class.CleanPairCallback.4* @_ZZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackC2ES1_P22btOverlappingPairCacheS3_(%class.CleanPairCallback.4* returned %this, %struct.btBroadphaseProxy* %cleanProxy, %class.btOverlappingPairCache* %pairCache, %class.btDispatcher* %dispatcher) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.CleanPairCallback.4*, align 4
  %cleanProxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %pairCache.addr = alloca %class.btOverlappingPairCache*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %class.CleanPairCallback.4* %this, %class.CleanPairCallback.4** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %cleanProxy, %struct.btBroadphaseProxy** %cleanProxy.addr, align 4, !tbaa !2
  store %class.btOverlappingPairCache* %pairCache, %class.btOverlappingPairCache** %pairCache.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.CleanPairCallback.4*, %class.CleanPairCallback.4** %this.addr, align 4
  %0 = bitcast %class.CleanPairCallback.4* %this1 to %struct.btOverlapCallback*
  %call = call %struct.btOverlapCallback* @_ZN17btOverlapCallbackC2Ev(%struct.btOverlapCallback* %0) #8
  %1 = bitcast %class.CleanPairCallback.4* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_cleanProxy = getelementptr inbounds %class.CleanPairCallback.4, %class.CleanPairCallback.4* %this1, i32 0, i32 1
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %cleanProxy.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %2, %struct.btBroadphaseProxy** %m_cleanProxy, align 4, !tbaa !52
  %m_pairCache = getelementptr inbounds %class.CleanPairCallback.4, %class.CleanPairCallback.4* %this1, i32 0, i32 2
  %3 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %pairCache.addr, align 4, !tbaa !2
  store %class.btOverlappingPairCache* %3, %class.btOverlappingPairCache** %m_pairCache, align 4, !tbaa !54
  %m_dispatcher = getelementptr inbounds %class.CleanPairCallback.4, %class.CleanPairCallback.4* %this1, i32 0, i32 3
  %4 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  store %class.btDispatcher* %4, %class.btDispatcher** %m_dispatcher, align 4, !tbaa !55
  ret %class.CleanPairCallback.4* %this1
}

define hidden void @_ZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher(%class.btSortedOverlappingPairCache* %this, %struct.btBroadphaseProxy* %proxy, %class.btDispatcher* %dispatcher) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %removeCallback = alloca %class.RemovePairCallback.5, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %0 = bitcast %class.RemovePairCallback.5* %removeCallback to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #8
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  %call = call %class.RemovePairCallback.5* @_ZZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackC2ES1_(%class.RemovePairCallback.5* %removeCallback, %struct.btBroadphaseProxy* %1)
  %2 = bitcast %class.RemovePairCallback.5* %removeCallback to %struct.btOverlapCallback*
  %3 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %4 = bitcast %class.btSortedOverlappingPairCache* %this1 to void (%class.btSortedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)***
  %vtable = load void (%class.btSortedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)**, void (%class.btSortedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)*** %4, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btSortedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)*, void (%class.btSortedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)** %vtable, i64 12
  %5 = load void (%class.btSortedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)*, void (%class.btSortedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)** %vfn, align 4
  call void %5(%class.btSortedOverlappingPairCache* %this1, %struct.btOverlapCallback* %2, %class.btDispatcher* %3)
  %call2 = call %class.RemovePairCallback.5* bitcast (%struct.btOverlapCallback* (%struct.btOverlapCallback*)* @_ZN17btOverlapCallbackD2Ev to %class.RemovePairCallback.5* (%class.RemovePairCallback.5*)*)(%class.RemovePairCallback.5* %removeCallback) #8
  %6 = bitcast %class.RemovePairCallback.5* %removeCallback to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %6) #8
  ret void
}

; Function Attrs: nounwind
define internal %class.RemovePairCallback.5* @_ZZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackC2ES1_(%class.RemovePairCallback.5* returned %this, %struct.btBroadphaseProxy* %obsoleteProxy) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.RemovePairCallback.5*, align 4
  %obsoleteProxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  store %class.RemovePairCallback.5* %this, %class.RemovePairCallback.5** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %obsoleteProxy, %struct.btBroadphaseProxy** %obsoleteProxy.addr, align 4, !tbaa !2
  %this1 = load %class.RemovePairCallback.5*, %class.RemovePairCallback.5** %this.addr, align 4
  %0 = bitcast %class.RemovePairCallback.5* %this1 to %struct.btOverlapCallback*
  %call = call %struct.btOverlapCallback* @_ZN17btOverlapCallbackC2Ev(%struct.btOverlapCallback* %0) #8
  %1 = bitcast %class.RemovePairCallback.5* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_obsoleteProxy = getelementptr inbounds %class.RemovePairCallback.5, %class.RemovePairCallback.5* %this1, i32 0, i32 1
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %obsoleteProxy.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %2, %struct.btBroadphaseProxy** %m_obsoleteProxy, align 4, !tbaa !56
  ret %class.RemovePairCallback.5* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN28btSortedOverlappingPairCache20sortOverlappingPairsEP12btDispatcher(%class.btSortedOverlappingPairCache* %this, %class.btDispatcher* %dispatcher) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  ret void
}

define linkonce_odr hidden %struct.btBroadphasePair* @_ZN28btHashedOverlappingPairCache18addOverlappingPairEP17btBroadphaseProxyS1_(%class.btHashedOverlappingPairCache* %this, %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy* %proxy1) unnamed_addr #0 comdat {
entry:
  %retval = alloca %struct.btBroadphasePair*, align 4
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy1, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = load i32, i32* @gAddedPairs, align 4, !tbaa !18
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* @gAddedPairs, align 4, !tbaa !18
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %call = call zeroext i1 @_ZNK28btHashedOverlappingPairCache24needsBroadphaseCollisionEP17btBroadphaseProxyS1_(%class.btHashedOverlappingPairCache* %this1, %struct.btBroadphaseProxy* %1, %struct.btBroadphaseProxy* %2)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store %struct.btBroadphasePair* null, %struct.btBroadphasePair** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %4 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %call2 = call %struct.btBroadphasePair* @_ZN28btHashedOverlappingPairCache15internalAddPairEP17btBroadphaseProxyS1_(%class.btHashedOverlappingPairCache* %this1, %struct.btBroadphaseProxy* %3, %struct.btBroadphaseProxy* %4)
  store %struct.btBroadphasePair* %call2, %struct.btBroadphasePair** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %retval, align 4
  ret %struct.btBroadphasePair* %5
}

define linkonce_odr hidden %struct.btBroadphasePair* @_ZN28btHashedOverlappingPairCache26getOverlappingPairArrayPtrEv(%class.btHashedOverlappingPairCache* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray, i32 0)
  ret %struct.btBroadphasePair* %call
}

define linkonce_odr hidden %struct.btBroadphasePair* @_ZNK28btHashedOverlappingPairCache26getOverlappingPairArrayPtrEv(%class.btHashedOverlappingPairCache* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZNK20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray, i32 0)
  ret %struct.btBroadphasePair* %call
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray* @_ZN28btHashedOverlappingPairCache23getOverlappingPairArrayEv(%class.btHashedOverlappingPairCache* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  ret %class.btAlignedObjectArray* %m_overlappingPairArray
}

define linkonce_odr hidden i32 @_ZNK28btHashedOverlappingPairCache22getNumOverlappingPairsEv(%class.btHashedOverlappingPairCache* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %m_overlappingPairArray)
  ret i32 %call
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN28btHashedOverlappingPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback(%class.btHashedOverlappingPairCache* %this, %struct.btOverlapFilterCallback* %callback) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %callback.addr = alloca %struct.btOverlapFilterCallback*, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  store %struct.btOverlapFilterCallback* %callback, %struct.btOverlapFilterCallback** %callback.addr, align 4, !tbaa !2
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = load %struct.btOverlapFilterCallback*, %struct.btOverlapFilterCallback** %callback.addr, align 4, !tbaa !2
  %m_overlapFilterCallback = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 2
  store %struct.btOverlapFilterCallback* %0, %struct.btOverlapFilterCallback** %m_overlapFilterCallback, align 4, !tbaa !8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZN28btHashedOverlappingPairCache18hasDeferredRemovalEv(%class.btHashedOverlappingPairCache* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  ret i1 false
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN28btHashedOverlappingPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback(%class.btHashedOverlappingPairCache* %this, %class.btOverlappingPairCallback* %ghostPairCallback) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %ghostPairCallback.addr = alloca %class.btOverlappingPairCallback*, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  store %class.btOverlappingPairCallback* %ghostPairCallback, %class.btOverlappingPairCallback** %ghostPairCallback.addr, align 4, !tbaa !2
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %ghostPairCallback.addr, align 4, !tbaa !2
  %m_ghostPairCallback = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 7
  store %class.btOverlappingPairCallback* %0, %class.btOverlappingPairCallback** %m_ghostPairCallback, align 4, !tbaa !17
  ret void
}

define linkonce_odr hidden %struct.btBroadphasePair* @_ZN28btSortedOverlappingPairCache26getOverlappingPairArrayPtrEv(%class.btSortedOverlappingPairCache* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray, i32 0)
  ret %struct.btBroadphasePair* %call
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btBroadphasePair* @_ZNK28btSortedOverlappingPairCache26getOverlappingPairArrayPtrEv(%class.btSortedOverlappingPairCache* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZNK20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray, i32 0)
  ret %struct.btBroadphasePair* %call
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray* @_ZN28btSortedOverlappingPairCache23getOverlappingPairArrayEv(%class.btSortedOverlappingPairCache* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  ret %class.btAlignedObjectArray* %m_overlappingPairArray
}

define linkonce_odr hidden i32 @_ZNK28btSortedOverlappingPairCache22getNumOverlappingPairsEv(%class.btSortedOverlappingPairCache* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %m_overlappingPairArray)
  ret i32 %call
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN28btSortedOverlappingPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback(%class.btSortedOverlappingPairCache* %this, %struct.btOverlapFilterCallback* %callback) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  %callback.addr = alloca %struct.btOverlapFilterCallback*, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  store %struct.btOverlapFilterCallback* %callback, %struct.btOverlapFilterCallback** %callback.addr, align 4, !tbaa !2
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %0 = load %struct.btOverlapFilterCallback*, %struct.btOverlapFilterCallback** %callback.addr, align 4, !tbaa !2
  %m_overlapFilterCallback = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 4
  store %struct.btOverlapFilterCallback* %0, %struct.btOverlapFilterCallback** %m_overlapFilterCallback, align 4, !tbaa !45
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZN28btSortedOverlappingPairCache18hasDeferredRemovalEv(%class.btSortedOverlappingPairCache* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %m_hasDeferredRemoval = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 3
  %0 = load i8, i8* %m_hasDeferredRemoval, align 1, !tbaa !51, !range !49
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN28btSortedOverlappingPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback(%class.btSortedOverlappingPairCache* %this, %class.btOverlappingPairCallback* %ghostPairCallback) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  %ghostPairCallback.addr = alloca %class.btOverlappingPairCallback*, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  store %class.btOverlappingPairCallback* %ghostPairCallback, %class.btOverlappingPairCallback** %ghostPairCallback.addr, align 4, !tbaa !2
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %0 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %ghostPairCallback.addr, align 4, !tbaa !2
  %m_ghostPairCallback = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 5
  store %class.btOverlappingPairCallback* %0, %class.btOverlappingPairCallback** %m_ghostPairCallback, align 4, !tbaa !43
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btOverlappingPairCallback* @_ZN25btOverlappingPairCallbackC2Ev(%class.btOverlappingPairCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btOverlappingPairCallback*, align 4
  store %class.btOverlappingPairCallback* %this, %class.btOverlappingPairCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %this.addr, align 4
  %0 = bitcast %class.btOverlappingPairCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV25btOverlappingPairCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  ret %class.btOverlappingPairCallback* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btOverlappingPairCallback* @_ZN25btOverlappingPairCallbackD2Ev(%class.btOverlappingPairCallback* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btOverlappingPairCallback*, align 4
  store %class.btOverlappingPairCallback* %this, %class.btOverlappingPairCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %this.addr, align 4
  ret %class.btOverlappingPairCallback* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN22btOverlappingPairCacheD0Ev(%class.btOverlappingPairCache* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btOverlappingPairCache*, align 4
  store %class.btOverlappingPairCache* %this, %class.btOverlappingPairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN25btOverlappingPairCallbackD0Ev(%class.btOverlappingPairCallback* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btOverlappingPairCallback*, align 4
  store %class.btOverlappingPairCallback* %this, %class.btOverlappingPairCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #6

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %struct.btOverlapCallback* @_ZN17btOverlapCallbackC2Ev(%struct.btOverlapCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btOverlapCallback*, align 4
  store %struct.btOverlapCallback* %this, %struct.btOverlapCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btOverlapCallback*, %struct.btOverlapCallback** %this.addr, align 4
  %0 = bitcast %struct.btOverlapCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV17btOverlapCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  ret %struct.btOverlapCallback* %this1
}

; Function Attrs: inlinehint nounwind
define internal void @_ZZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackD0Ev(%class.CleanPairCallback* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.CleanPairCallback*, align 4
  store %class.CleanPairCallback* %this, %class.CleanPairCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.CleanPairCallback*, %class.CleanPairCallback** %this.addr, align 4
  %call = call %class.CleanPairCallback* bitcast (%struct.btOverlapCallback* (%struct.btOverlapCallback*)* @_ZN17btOverlapCallbackD2Ev to %class.CleanPairCallback* (%class.CleanPairCallback*)*)(%class.CleanPairCallback* %this1) #8
  %0 = bitcast %class.CleanPairCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

define internal zeroext i1 @_ZZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallback14processOverlapER16btBroadphasePair(%class.CleanPairCallback* %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %pair) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.CleanPairCallback*, align 4
  %pair.addr = alloca %struct.btBroadphasePair*, align 4
  store %class.CleanPairCallback* %this, %class.CleanPairCallback** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphasePair* %pair, %struct.btBroadphasePair** %pair.addr, align 4, !tbaa !2
  %this1 = load %class.CleanPairCallback*, %class.CleanPairCallback** %this.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4, !tbaa !2
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 0
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !22
  %m_cleanProxy = getelementptr inbounds %class.CleanPairCallback, %class.CleanPairCallback* %this1, i32 0, i32 1
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_cleanProxy, align 4, !tbaa !26
  %cmp = icmp eq %struct.btBroadphaseProxy* %1, %2
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4, !tbaa !2
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %3, i32 0, i32 1
  %4 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !24
  %m_cleanProxy2 = getelementptr inbounds %class.CleanPairCallback, %class.CleanPairCallback* %this1, i32 0, i32 1
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_cleanProxy2, align 4, !tbaa !26
  %cmp3 = icmp eq %struct.btBroadphaseProxy* %4, %5
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  %m_pairCache = getelementptr inbounds %class.CleanPairCallback, %class.CleanPairCallback* %this1, i32 0, i32 2
  %6 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4, !tbaa !28
  %7 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4, !tbaa !2
  %m_dispatcher = getelementptr inbounds %class.CleanPairCallback, %class.CleanPairCallback* %this1, i32 0, i32 3
  %8 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4, !tbaa !29
  %9 = bitcast %class.btOverlappingPairCache* %6 to void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)***
  %vtable = load void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)**, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*** %9, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vtable, i64 8
  %10 = load void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vfn, align 4
  call void %10(%class.btOverlappingPairCache* %6, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %7, %class.btDispatcher* %8)
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.lhs.false
  ret i1 false
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN17btOverlapCallbackD0Ev(%struct.btOverlapCallback* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %struct.btOverlapCallback*, align 4
  store %struct.btOverlapCallback* %this, %struct.btOverlapCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btOverlapCallback*, %struct.btOverlapCallback** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

; Function Attrs: inlinehint nounwind
define internal void @_ZZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackD0Ev(%class.RemovePairCallback* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.RemovePairCallback*, align 4
  store %class.RemovePairCallback* %this, %class.RemovePairCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.RemovePairCallback*, %class.RemovePairCallback** %this.addr, align 4
  %call = call %class.RemovePairCallback* bitcast (%struct.btOverlapCallback* (%struct.btOverlapCallback*)* @_ZN17btOverlapCallbackD2Ev to %class.RemovePairCallback* (%class.RemovePairCallback*)*)(%class.RemovePairCallback* %this1) #8
  %0 = bitcast %class.RemovePairCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: nounwind
define internal zeroext i1 @_ZZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallback14processOverlapER16btBroadphasePair(%class.RemovePairCallback* %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %pair) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.RemovePairCallback*, align 4
  %pair.addr = alloca %struct.btBroadphasePair*, align 4
  store %class.RemovePairCallback* %this, %class.RemovePairCallback** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphasePair* %pair, %struct.btBroadphasePair** %pair.addr, align 4, !tbaa !2
  %this1 = load %class.RemovePairCallback*, %class.RemovePairCallback** %this.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4, !tbaa !2
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 0
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !22
  %m_obsoleteProxy = getelementptr inbounds %class.RemovePairCallback, %class.RemovePairCallback* %this1, i32 0, i32 1
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_obsoleteProxy, align 4, !tbaa !30
  %cmp = icmp eq %struct.btBroadphaseProxy* %1, %2
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4, !tbaa !2
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %3, i32 0, i32 1
  %4 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !24
  %m_obsoleteProxy2 = getelementptr inbounds %class.RemovePairCallback, %class.RemovePairCallback* %this1, i32 0, i32 1
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_obsoleteProxy2, align 4, !tbaa !30
  %cmp3 = icmp eq %struct.btBroadphaseProxy* %4, %5
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %6 = phi i1 [ true, %entry ], [ %cmp3, %lor.rhs ]
  ret i1 %6
}

; Function Attrs: inlinehint nounwind
define internal void @_ZZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackD0Ev(%class.CleanPairCallback.4* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.CleanPairCallback.4*, align 4
  store %class.CleanPairCallback.4* %this, %class.CleanPairCallback.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.CleanPairCallback.4*, %class.CleanPairCallback.4** %this.addr, align 4
  %call = call %class.CleanPairCallback.4* bitcast (%struct.btOverlapCallback* (%struct.btOverlapCallback*)* @_ZN17btOverlapCallbackD2Ev to %class.CleanPairCallback.4* (%class.CleanPairCallback.4*)*)(%class.CleanPairCallback.4* %this1) #8
  %0 = bitcast %class.CleanPairCallback.4* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

define internal zeroext i1 @_ZZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallback14processOverlapER16btBroadphasePair(%class.CleanPairCallback.4* %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %pair) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.CleanPairCallback.4*, align 4
  %pair.addr = alloca %struct.btBroadphasePair*, align 4
  store %class.CleanPairCallback.4* %this, %class.CleanPairCallback.4** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphasePair* %pair, %struct.btBroadphasePair** %pair.addr, align 4, !tbaa !2
  %this1 = load %class.CleanPairCallback.4*, %class.CleanPairCallback.4** %this.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4, !tbaa !2
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 0
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !22
  %m_cleanProxy = getelementptr inbounds %class.CleanPairCallback.4, %class.CleanPairCallback.4* %this1, i32 0, i32 1
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_cleanProxy, align 4, !tbaa !52
  %cmp = icmp eq %struct.btBroadphaseProxy* %1, %2
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4, !tbaa !2
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %3, i32 0, i32 1
  %4 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !24
  %m_cleanProxy2 = getelementptr inbounds %class.CleanPairCallback.4, %class.CleanPairCallback.4* %this1, i32 0, i32 1
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_cleanProxy2, align 4, !tbaa !52
  %cmp3 = icmp eq %struct.btBroadphaseProxy* %4, %5
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  %m_pairCache = getelementptr inbounds %class.CleanPairCallback.4, %class.CleanPairCallback.4* %this1, i32 0, i32 2
  %6 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4, !tbaa !54
  %7 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4, !tbaa !2
  %m_dispatcher = getelementptr inbounds %class.CleanPairCallback.4, %class.CleanPairCallback.4* %this1, i32 0, i32 3
  %8 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4, !tbaa !55
  %9 = bitcast %class.btOverlappingPairCache* %6 to void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)***
  %vtable = load void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)**, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*** %9, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vtable, i64 8
  %10 = load void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vfn, align 4
  call void %10(%class.btOverlappingPairCache* %6, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %7, %class.btDispatcher* %8)
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.lhs.false
  ret i1 false
}

; Function Attrs: inlinehint nounwind
define internal void @_ZZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackD0Ev(%class.RemovePairCallback.5* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.RemovePairCallback.5*, align 4
  store %class.RemovePairCallback.5* %this, %class.RemovePairCallback.5** %this.addr, align 4, !tbaa !2
  %this1 = load %class.RemovePairCallback.5*, %class.RemovePairCallback.5** %this.addr, align 4
  %call = call %class.RemovePairCallback.5* bitcast (%struct.btOverlapCallback* (%struct.btOverlapCallback*)* @_ZN17btOverlapCallbackD2Ev to %class.RemovePairCallback.5* (%class.RemovePairCallback.5*)*)(%class.RemovePairCallback.5* %this1) #8
  %0 = bitcast %class.RemovePairCallback.5* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: nounwind
define internal zeroext i1 @_ZZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallback14processOverlapER16btBroadphasePair(%class.RemovePairCallback.5* %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %pair) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.RemovePairCallback.5*, align 4
  %pair.addr = alloca %struct.btBroadphasePair*, align 4
  store %class.RemovePairCallback.5* %this, %class.RemovePairCallback.5** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphasePair* %pair, %struct.btBroadphasePair** %pair.addr, align 4, !tbaa !2
  %this1 = load %class.RemovePairCallback.5*, %class.RemovePairCallback.5** %this.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4, !tbaa !2
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 0
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !22
  %m_obsoleteProxy = getelementptr inbounds %class.RemovePairCallback.5, %class.RemovePairCallback.5* %this1, i32 0, i32 1
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_obsoleteProxy, align 4, !tbaa !56
  %cmp = icmp eq %struct.btBroadphaseProxy* %1, %2
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4, !tbaa !2
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %3, i32 0, i32 1
  %4 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !24
  %m_obsoleteProxy2 = getelementptr inbounds %class.RemovePairCallback.5, %class.RemovePairCallback.5* %this1, i32 0, i32 1
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_obsoleteProxy2, align 4, !tbaa !56
  %cmp3 = icmp eq %struct.btBroadphaseProxy* %4, %5
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %6 = phi i1 [ true, %entry ], [ %cmp3, %lor.rhs ]
  ret i1 %6
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_ZNK28btHashedOverlappingPairCache24needsBroadphaseCollisionEP17btBroadphaseProxyS1_(%class.btHashedOverlappingPairCache* %this, %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy* %proxy1) #3 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  %collides = alloca i8, align 1
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy1, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %m_overlapFilterCallback = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 2
  %0 = load %struct.btOverlapFilterCallback*, %struct.btOverlapFilterCallback** %m_overlapFilterCallback, align 4, !tbaa !8
  %tobool = icmp ne %struct.btOverlapFilterCallback* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_overlapFilterCallback2 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 2
  %1 = load %struct.btOverlapFilterCallback*, %struct.btOverlapFilterCallback** %m_overlapFilterCallback2, align 4, !tbaa !8
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %4 = bitcast %struct.btOverlapFilterCallback* %1 to i1 (%struct.btOverlapFilterCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable = load i1 (%struct.btOverlapFilterCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, i1 (%struct.btOverlapFilterCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %4, align 4, !tbaa !6
  %vfn = getelementptr inbounds i1 (%struct.btOverlapFilterCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, i1 (%struct.btOverlapFilterCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable, i64 2
  %5 = load i1 (%struct.btOverlapFilterCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, i1 (%struct.btOverlapFilterCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call = call zeroext i1 %5(%struct.btOverlapFilterCallback* %1, %struct.btBroadphaseProxy* %2, %struct.btBroadphaseProxy* %3)
  store i1 %call, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %collides) #8
  %6 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %m_collisionFilterGroup = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %6, i32 0, i32 1
  %7 = load i16, i16* %m_collisionFilterGroup, align 4, !tbaa !46
  %conv = sext i16 %7 to i32
  %8 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %m_collisionFilterMask = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %8, i32 0, i32 2
  %9 = load i16, i16* %m_collisionFilterMask, align 2, !tbaa !47
  %conv3 = sext i16 %9 to i32
  %and = and i32 %conv, %conv3
  %cmp = icmp ne i32 %and, 0
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %collides, align 1, !tbaa !48
  %10 = load i8, i8* %collides, align 1, !tbaa !48, !range !49
  %tobool4 = trunc i8 %10 to i1
  br i1 %tobool4, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %if.end
  %11 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %m_collisionFilterGroup5 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %11, i32 0, i32 1
  %12 = load i16, i16* %m_collisionFilterGroup5, align 4, !tbaa !46
  %conv6 = sext i16 %12 to i32
  %13 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %m_collisionFilterMask7 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %13, i32 0, i32 2
  %14 = load i16, i16* %m_collisionFilterMask7, align 2, !tbaa !47
  %conv8 = sext i16 %14 to i32
  %and9 = and i32 %conv6, %conv8
  %tobool10 = icmp ne i32 %and9, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.end
  %15 = phi i1 [ false, %if.end ], [ %tobool10, %land.rhs ]
  %frombool11 = zext i1 %15 to i8
  store i8 %frombool11, i8* %collides, align 1, !tbaa !48
  %16 = load i8, i8* %collides, align 1, !tbaa !48, !range !49
  %tobool12 = trunc i8 %16 to i1
  store i1 %tobool12, i1* %retval, align 1
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %collides) #8
  br label %return

return:                                           ; preds = %land.end, %if.then
  %17 = load i1, i1* %retval, align 1
  ret i1 %17
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZNK20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !18
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !20
  %1 = load i32, i32* %n.addr, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 %1
  ret %struct.btBroadphasePair* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4initEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !19
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btBroadphasePair* null, %struct.btBroadphasePair** %m_data, align 4, !tbaa !20
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !39
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !21
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.1* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  ret %class.btAlignedAllocator.1* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !58
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store i32* null, i32** %m_data, align 4, !tbaa !37
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !36
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !59
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE5clearEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !18
  store i32 %last, i32* %last.addr, align 4, !tbaa !18
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %first.addr, align 4, !tbaa !18
  store i32 %1, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !18
  %3 = load i32, i32* %last.addr, align 4, !tbaa !18
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !20
  %5 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !18
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE10deallocateEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !20
  %tobool = icmp ne %struct.btBroadphasePair* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !19, !range !49
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data4, align 4, !tbaa !20
  call void @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %struct.btBroadphasePair* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btBroadphasePair* null, %struct.btBroadphasePair** %m_data5, align 4, !tbaa !20
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %struct.btBroadphasePair* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %struct.btBroadphasePair*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphasePair* %ptr, %struct.btBroadphasePair** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btBroadphasePair* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #7

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.0* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !18
  store i32 %last, i32* %last.addr, align 4, !tbaa !18
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %first.addr, align 4, !tbaa !18
  store i32 %1, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !18
  %3 = load i32, i32* %last.addr, align 4, !tbaa !18
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %4 = load i32*, i32** %m_data, align 4, !tbaa !37
  %5 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !18
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.0* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !37
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !58, !range !49
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4, !tbaa !37
  call void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.1* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4, !tbaa !37
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.1* %this, i32* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  store i32* %ptr, i32** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI16btBroadphasePairE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #3 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !18
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !18
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !18
  %call = call %struct.btBroadphasePair* @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %struct.btBroadphasePair** null)
  %2 = bitcast %struct.btBroadphasePair* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %struct.btBroadphasePair* %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btBroadphasePair*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !18
  store i32 %end, i32* %end.addr, align 4, !tbaa !18
  store %struct.btBroadphasePair* %dest, %struct.btBroadphasePair** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %start.addr, align 4, !tbaa !18
  store i32 %1, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !18
  %3 = load i32, i32* %end.addr, align 4, !tbaa !18
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %4, i32 %5
  %6 = bitcast %struct.btBroadphasePair* %arrayidx to i8*
  %call = call i8* @_ZN16btBroadphasePairnwEmPv(i32 16, i8* %6)
  %7 = bitcast i8* %call to %struct.btBroadphasePair*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !20
  %9 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx2 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %8, i32 %9
  %call3 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %7, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !18
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret void
}

define linkonce_odr hidden %struct.btBroadphasePair* @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %struct.btBroadphasePair** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btBroadphasePair**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !18
  store %struct.btBroadphasePair** %hint, %struct.btBroadphasePair*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !18
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btBroadphasePair*
  ret %struct.btBroadphasePair* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #7

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* returned %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %other) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %struct.btBroadphasePair*, align 4
  %other.addr = alloca %struct.btBroadphasePair*, align 4
  store %struct.btBroadphasePair* %this, %struct.btBroadphasePair** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphasePair* %other, %struct.btBroadphasePair** %other.addr, align 4, !tbaa !2
  %this1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %this.addr, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 0
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4, !tbaa !2
  %m_pProxy02 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 0
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy02, align 4, !tbaa !22
  store %struct.btBroadphaseProxy* %1, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !22
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 1
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4, !tbaa !2
  %m_pProxy13 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 0, i32 1
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy13, align 4, !tbaa !24
  store %struct.btBroadphaseProxy* %3, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !24
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 2
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4, !tbaa !2
  %m_algorithm4 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %4, i32 0, i32 2
  %5 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm4, align 4, !tbaa !25
  store %class.btCollisionAlgorithm* %5, %class.btCollisionAlgorithm** %m_algorithm, align 4, !tbaa !25
  %6 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 3
  %m_internalInfo1 = bitcast %union.anon* %6 to i8**
  %7 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4, !tbaa !2
  %8 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %7, i32 0, i32 3
  %m_internalInfo15 = bitcast %union.anon* %8 to i8**
  %9 = load i8*, i8** %m_internalInfo15, align 4, !tbaa !38
  store i8* %9, i8** %m_internalInfo1, align 4, !tbaa !38
  ret %struct.btBroadphasePair* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.0* %this, i32 %_Count) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca i32*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !18
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !18
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast i32** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !18
  %call2 = call i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.0* %this1, i32 %2)
  %3 = bitcast i8* %call2 to i32*
  store i32* %3, i32** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %4 = load i32*, i32** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call3, i32* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !58
  %5 = load i32*, i32** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store i32* %5, i32** %m_data, align 4, !tbaa !37
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !18
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !59
  %7 = bitcast i32** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !59
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.0* %this, i32 %size) #3 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !18
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !18
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !18
  %call = call i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.1* %m_allocator, i32 %1, i32** null)
  %2 = bitcast i32* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.0* %this, i32 %start, i32 %end, i32* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !18
  store i32 %end, i32* %end.addr, align 4, !tbaa !18
  store i32* %dest, i32** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %start.addr, align 4, !tbaa !18
  store i32 %1, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !18
  %3 = load i32, i32* %end.addr, align 4, !tbaa !18
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i32*, i32** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  %6 = bitcast i32* %arrayidx to i8*
  %7 = bitcast i8* %6 to i32*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %8 = load i32*, i32** %m_data, align 4, !tbaa !37
  %9 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx2 = getelementptr inbounds i32, i32* %8, i32 %9
  %10 = load i32, i32* %arrayidx2, align 4, !tbaa !18
  store i32 %10, i32* %7, align 4, !tbaa !18
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !18
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  ret void
}

define linkonce_odr hidden i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.1* %this, i32 %n, i32** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca i32**, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !18
  store i32** %hint, i32*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !18
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to i32*
  ret i32* %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI16btBroadphasePairE9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !18
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !18
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !18
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray* %this, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %CompareFunc, i32 %lo, i32 %hi) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %CompareFunc.addr = alloca %class.btBroadphasePairSortPredicate*, align 4
  %lo.addr = alloca i32, align 4
  %hi.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %x = alloca %struct.btBroadphasePair, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store %class.btBroadphasePairSortPredicate* %CompareFunc, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  store i32 %lo, i32* %lo.addr, align 4, !tbaa !18
  store i32 %hi, i32* %hi.addr, align 4, !tbaa !18
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %lo.addr, align 4, !tbaa !18
  store i32 %1, i32* %i, align 4, !tbaa !18
  %2 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load i32, i32* %hi.addr, align 4, !tbaa !18
  store i32 %3, i32* %j, align 4, !tbaa !18
  %4 = bitcast %struct.btBroadphasePair* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #8
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %5 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !20
  %6 = load i32, i32* %lo.addr, align 4, !tbaa !18
  %7 = load i32, i32* %hi.addr, align 4, !tbaa !18
  %add = add nsw i32 %6, %7
  %div = sdiv i32 %add, 2
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %5, i32 %div
  %call = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %x, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx)
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %do.body
  %8 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %9 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data2, align 4, !tbaa !20
  %10 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx3 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %9, i32 %10
  %call4 = call zeroext i1 @_ZNK29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_(%class.btBroadphasePairSortPredicate* %8, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx3, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %x)
  br i1 %call4, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %11 = load i32, i32* %i, align 4, !tbaa !18
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !18
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %while.cond5

while.cond5:                                      ; preds = %while.body9, %while.end
  %12 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %13 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data6, align 4, !tbaa !20
  %14 = load i32, i32* %j, align 4, !tbaa !18
  %arrayidx7 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %13, i32 %14
  %call8 = call zeroext i1 @_ZNK29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_(%class.btBroadphasePairSortPredicate* %12, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %x, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx7)
  br i1 %call8, label %while.body9, label %while.end10

while.body9:                                      ; preds = %while.cond5
  %15 = load i32, i32* %j, align 4, !tbaa !18
  %dec = add nsw i32 %15, -1
  store i32 %dec, i32* %j, align 4, !tbaa !18
  br label %while.cond5

while.end10:                                      ; preds = %while.cond5
  %16 = load i32, i32* %i, align 4, !tbaa !18
  %17 = load i32, i32* %j, align 4, !tbaa !18
  %cmp = icmp sle i32 %16, %17
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %while.end10
  %18 = load i32, i32* %i, align 4, !tbaa !18
  %19 = load i32, i32* %j, align 4, !tbaa !18
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4swapEii(%class.btAlignedObjectArray* %this1, i32 %18, i32 %19)
  %20 = load i32, i32* %i, align 4, !tbaa !18
  %inc11 = add nsw i32 %20, 1
  store i32 %inc11, i32* %i, align 4, !tbaa !18
  %21 = load i32, i32* %j, align 4, !tbaa !18
  %dec12 = add nsw i32 %21, -1
  store i32 %dec12, i32* %j, align 4, !tbaa !18
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end10
  br label %do.cond

do.cond:                                          ; preds = %if.end
  %22 = load i32, i32* %i, align 4, !tbaa !18
  %23 = load i32, i32* %j, align 4, !tbaa !18
  %cmp13 = icmp sle i32 %22, %23
  br i1 %cmp13, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %24 = load i32, i32* %lo.addr, align 4, !tbaa !18
  %25 = load i32, i32* %j, align 4, !tbaa !18
  %cmp14 = icmp slt i32 %24, %25
  br i1 %cmp14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %do.end
  %26 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %27 = load i32, i32* %lo.addr, align 4, !tbaa !18
  %28 = load i32, i32* %j, align 4, !tbaa !18
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray* %this1, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %26, i32 %27, i32 %28)
  br label %if.end16

if.end16:                                         ; preds = %if.then15, %do.end
  %29 = load i32, i32* %i, align 4, !tbaa !18
  %30 = load i32, i32* %hi.addr, align 4, !tbaa !18
  %cmp17 = icmp slt i32 %29, %30
  br i1 %cmp17, label %if.then18, label %if.end19

if.then18:                                        ; preds = %if.end16
  %31 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %32 = load i32, i32* %i, align 4, !tbaa !18
  %33 = load i32, i32* %hi.addr, align 4, !tbaa !18
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray* %this1, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %31, i32 %32, i32 %33)
  br label %if.end19

if.end19:                                         ; preds = %if.then18, %if.end16
  %34 = bitcast %struct.btBroadphasePair* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %34) #8
  %35 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  %36 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZNK29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_(%class.btBroadphasePairSortPredicate* %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %a, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %b) #4 comdat {
entry:
  %this.addr = alloca %class.btBroadphasePairSortPredicate*, align 4
  %a.addr = alloca %struct.btBroadphasePair*, align 4
  %b.addr = alloca %struct.btBroadphasePair*, align 4
  %uidA0 = alloca i32, align 4
  %uidB0 = alloca i32, align 4
  %uidA1 = alloca i32, align 4
  %uidB1 = alloca i32, align 4
  store %class.btBroadphasePairSortPredicate* %this, %class.btBroadphasePairSortPredicate** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphasePair* %a, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  store %struct.btBroadphasePair* %b, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %this1 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %this.addr, align 4
  %0 = bitcast i32* %uidA0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %1, i32 0, i32 0
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !22
  %tobool = icmp ne %struct.btBroadphaseProxy* %2, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  %m_pProxy02 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %3, i32 0, i32 0
  %4 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy02, align 4, !tbaa !22
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %4, i32 0, i32 4
  %5 = load i32, i32* %m_uniqueId, align 4, !tbaa !32
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %5, %cond.true ], [ -1, %cond.false ]
  store i32 %cond, i32* %uidA0, align 4, !tbaa !18
  %6 = bitcast i32* %uidB0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %m_pProxy03 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %7, i32 0, i32 0
  %8 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy03, align 4, !tbaa !22
  %tobool4 = icmp ne %struct.btBroadphaseProxy* %8, null
  br i1 %tobool4, label %cond.true5, label %cond.false8

cond.true5:                                       ; preds = %cond.end
  %9 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %m_pProxy06 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %9, i32 0, i32 0
  %10 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy06, align 4, !tbaa !22
  %m_uniqueId7 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %10, i32 0, i32 4
  %11 = load i32, i32* %m_uniqueId7, align 4, !tbaa !32
  br label %cond.end9

cond.false8:                                      ; preds = %cond.end
  br label %cond.end9

cond.end9:                                        ; preds = %cond.false8, %cond.true5
  %cond10 = phi i32 [ %11, %cond.true5 ], [ -1, %cond.false8 ]
  store i32 %cond10, i32* %uidB0, align 4, !tbaa !18
  %12 = bitcast i32* %uidA1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  %13 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %13, i32 0, i32 1
  %14 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !24
  %tobool11 = icmp ne %struct.btBroadphaseProxy* %14, null
  br i1 %tobool11, label %cond.true12, label %cond.false15

cond.true12:                                      ; preds = %cond.end9
  %15 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  %m_pProxy113 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %15, i32 0, i32 1
  %16 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy113, align 4, !tbaa !24
  %m_uniqueId14 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %16, i32 0, i32 4
  %17 = load i32, i32* %m_uniqueId14, align 4, !tbaa !32
  br label %cond.end16

cond.false15:                                     ; preds = %cond.end9
  br label %cond.end16

cond.end16:                                       ; preds = %cond.false15, %cond.true12
  %cond17 = phi i32 [ %17, %cond.true12 ], [ -1, %cond.false15 ]
  store i32 %cond17, i32* %uidA1, align 4, !tbaa !18
  %18 = bitcast i32* %uidB1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #8
  %19 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %m_pProxy118 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %19, i32 0, i32 1
  %20 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy118, align 4, !tbaa !24
  %tobool19 = icmp ne %struct.btBroadphaseProxy* %20, null
  br i1 %tobool19, label %cond.true20, label %cond.false23

cond.true20:                                      ; preds = %cond.end16
  %21 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %m_pProxy121 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %21, i32 0, i32 1
  %22 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy121, align 4, !tbaa !24
  %m_uniqueId22 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %22, i32 0, i32 4
  %23 = load i32, i32* %m_uniqueId22, align 4, !tbaa !32
  br label %cond.end24

cond.false23:                                     ; preds = %cond.end16
  br label %cond.end24

cond.end24:                                       ; preds = %cond.false23, %cond.true20
  %cond25 = phi i32 [ %23, %cond.true20 ], [ -1, %cond.false23 ]
  store i32 %cond25, i32* %uidB1, align 4, !tbaa !18
  %24 = load i32, i32* %uidA0, align 4, !tbaa !18
  %25 = load i32, i32* %uidB0, align 4, !tbaa !18
  %cmp = icmp sgt i32 %24, %25
  br i1 %cmp, label %lor.end, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %cond.end24
  %26 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  %m_pProxy026 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %26, i32 0, i32 0
  %27 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy026, align 4, !tbaa !22
  %28 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %m_pProxy027 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %28, i32 0, i32 0
  %29 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy027, align 4, !tbaa !22
  %cmp28 = icmp eq %struct.btBroadphaseProxy* %27, %29
  br i1 %cmp28, label %land.lhs.true, label %lor.rhs

land.lhs.true:                                    ; preds = %lor.lhs.false
  %30 = load i32, i32* %uidA1, align 4, !tbaa !18
  %31 = load i32, i32* %uidB1, align 4, !tbaa !18
  %cmp29 = icmp sgt i32 %30, %31
  br i1 %cmp29, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %land.lhs.true, %lor.lhs.false
  %32 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  %m_pProxy030 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %32, i32 0, i32 0
  %33 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy030, align 4, !tbaa !22
  %34 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %m_pProxy031 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %34, i32 0, i32 0
  %35 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy031, align 4, !tbaa !22
  %cmp32 = icmp eq %struct.btBroadphaseProxy* %33, %35
  br i1 %cmp32, label %land.lhs.true33, label %land.end

land.lhs.true33:                                  ; preds = %lor.rhs
  %36 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  %m_pProxy134 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %36, i32 0, i32 1
  %37 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy134, align 4, !tbaa !24
  %38 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %m_pProxy135 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %38, i32 0, i32 1
  %39 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy135, align 4, !tbaa !24
  %cmp36 = icmp eq %struct.btBroadphaseProxy* %37, %39
  br i1 %cmp36, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true33
  %40 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %40, i32 0, i32 2
  %41 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm, align 4, !tbaa !25
  %42 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %m_algorithm37 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %42, i32 0, i32 2
  %43 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm37, align 4, !tbaa !25
  %cmp38 = icmp ugt %class.btCollisionAlgorithm* %41, %43
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true33, %lor.rhs
  %44 = phi i1 [ false, %land.lhs.true33 ], [ false, %lor.rhs ], [ %cmp38, %land.rhs ]
  br label %lor.end

lor.end:                                          ; preds = %land.end, %land.lhs.true, %cond.end24
  %45 = phi i1 [ true, %land.lhs.true ], [ true, %cond.end24 ], [ %44, %land.end ]
  %46 = bitcast i32* %uidB1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #8
  %47 = bitcast i32* %uidA1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #8
  %48 = bitcast i32* %uidB0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #8
  %49 = bitcast i32* %uidA0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #8
  ret i1 %45
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZeqRK16btBroadphasePairS1_(%struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %a, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %b) #1 comdat {
entry:
  %a.addr = alloca %struct.btBroadphasePair*, align 4
  %b.addr = alloca %struct.btBroadphasePair*, align 4
  store %struct.btBroadphasePair* %a, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  store %struct.btBroadphasePair* %b, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 0
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !22
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %m_pProxy01 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 0, i32 0
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy01, align 4, !tbaa !22
  %cmp = icmp eq %struct.btBroadphaseProxy* %1, %3
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %4, i32 0, i32 1
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !24
  %6 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %m_pProxy12 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %6, i32 0, i32 1
  %7 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy12, align 4, !tbaa !24
  %cmp3 = icmp eq %struct.btBroadphaseProxy* %5, %7
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %8 = phi i1 [ false, %entry ], [ %cmp3, %land.rhs ]
  ret i1 %8
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { cold noreturn nounwind }
attributes #7 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind }
attributes #9 = { builtin nounwind }
attributes #10 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{!9, !3, i64 24}
!9 = !{!"_ZTS28btHashedOverlappingPairCache", !10, i64 4, !3, i64 24, !13, i64 28, !14, i64 32, !14, i64 52, !3, i64 72}
!10 = !{!"_ZTS20btAlignedObjectArrayI16btBroadphasePairE", !11, i64 0, !12, i64 4, !12, i64 8, !3, i64 12, !13, i64 16}
!11 = !{!"_ZTS18btAlignedAllocatorI16btBroadphasePairLj16EE"}
!12 = !{!"int", !4, i64 0}
!13 = !{!"bool", !4, i64 0}
!14 = !{!"_ZTS20btAlignedObjectArrayIiE", !15, i64 0, !12, i64 4, !12, i64 8, !3, i64 12, !13, i64 16}
!15 = !{!"_ZTS18btAlignedAllocatorIiLj16EE"}
!16 = !{!9, !13, i64 28}
!17 = !{!9, !3, i64 72}
!18 = !{!12, !12, i64 0}
!19 = !{!10, !13, i64 16}
!20 = !{!10, !3, i64 12}
!21 = !{!10, !12, i64 8}
!22 = !{!23, !3, i64 0}
!23 = !{!"_ZTS16btBroadphasePair", !3, i64 0, !3, i64 4, !3, i64 8, !4, i64 12}
!24 = !{!23, !3, i64 4}
!25 = !{!23, !3, i64 8}
!26 = !{!27, !3, i64 4}
!27 = !{!"_ZTSZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback", !3, i64 4, !3, i64 8, !3, i64 12}
!28 = !{!27, !3, i64 8}
!29 = !{!27, !3, i64 12}
!30 = !{!31, !3, i64 4}
!31 = !{!"_ZTSZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback", !3, i64 4}
!32 = !{!33, !12, i64 12}
!33 = !{!"_ZTS17btBroadphaseProxy", !3, i64 0, !34, i64 4, !34, i64 6, !3, i64 8, !12, i64 12, !35, i64 16, !35, i64 32}
!34 = !{!"short", !4, i64 0}
!35 = !{!"_ZTS9btVector3", !4, i64 0}
!36 = !{!14, !12, i64 4}
!37 = !{!14, !3, i64 12}
!38 = !{!4, !4, i64 0}
!39 = !{!10, !12, i64 4}
!40 = !{!41, !41, i64 0}
!41 = !{!"long", !4, i64 0}
!42 = !{i64 0, i64 4, !2, i64 4, i64 4, !2, i64 8, i64 4, !2, i64 12, i64 4, !2, i64 12, i64 4, !18}
!43 = !{!44, !3, i64 32}
!44 = !{!"_ZTS28btSortedOverlappingPairCache", !10, i64 4, !13, i64 24, !13, i64 25, !3, i64 28, !3, i64 32}
!45 = !{!44, !3, i64 28}
!46 = !{!33, !34, i64 4}
!47 = !{!33, !34, i64 6}
!48 = !{!13, !13, i64 0}
!49 = !{i8 0, i8 2}
!50 = !{!44, !13, i64 24}
!51 = !{!44, !13, i64 25}
!52 = !{!53, !3, i64 4}
!53 = !{!"_ZTSZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback", !3, i64 4, !3, i64 8, !3, i64 12}
!54 = !{!53, !3, i64 8}
!55 = !{!53, !3, i64 12}
!56 = !{!57, !3, i64 4}
!57 = !{!"_ZTSZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback", !3, i64 4}
!58 = !{!14, !13, i64 16}
!59 = !{!14, !12, i64 8}
