; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/NarrowPhaseCollision/btMinkowskiPenetrationDepthSolver.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/NarrowPhaseCollision/btMinkowskiPenetrationDepthSolver.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btVector3 = type { [4 x float] }
%class.btMinkowskiPenetrationDepthSolver = type { %class.btConvexPenetrationDepthSolver }
%class.btConvexPenetrationDepthSolver = type { i32 (...)** }
%class.btVoronoiSimplexSolver = type <{ i32, [5 x %class.btVector3], [5 x %class.btVector3], [5 x %class.btVector3], %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, i8, [3 x i8], %struct.btSubSimplexClosestResult, i8, [3 x i8] }>
%struct.btSubSimplexClosestResult = type <{ %class.btVector3, %struct.btUsageBitfield, [2 x i8], [4 x float], i8, [3 x i8] }>
%struct.btUsageBitfield = type { i8, i8 }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btIDebugDraw = type { i32 (...)** }
%class.btGjkPairDetector = type { %struct.btDiscreteCollisionDetectorInterface, %class.btVector3, %class.btConvexPenetrationDepthSolver*, %class.btVoronoiSimplexSolver*, %class.btConvexShape*, %class.btConvexShape*, i32, i32, float, float, i8, float, i32, i32, i32, i32, i32 }
%struct.btDiscreteCollisionDetectorInterface = type { i32 (...)** }
%"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput" = type { %class.btTransform, %class.btTransform, float }
%struct.btIntermediateResult = type <{ %"struct.btDiscreteCollisionDetectorInterface::Result", %class.btVector3, %class.btVector3, float, i8, [3 x i8] }>
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }

$_ZNK16btCollisionShape10isConvex2dEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZmlRK9btVector3RK11btMatrix3x3 = comdat any

$_ZngRK9btVector3 = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZN9btVector3mIERKS_ = comdat any

$_ZN36btDiscreteCollisionDetectorInterface17ClosestPointInputC2Ev = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZN11btTransform9setOriginERK9btVector3 = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZN17btGjkPairDetector23setCachedSeperatingAxisERK9btVector3 = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN36btDiscreteCollisionDetectorInterfaceD2Ev = comdat any

$_ZN30btConvexPenetrationDepthSolverD2Ev = comdat any

$_ZN33btMinkowskiPenetrationDepthSolverD0Ev = comdat any

$_ZN17btBroadphaseProxy10isConvex2dEi = comdat any

$_ZNK16btCollisionShape12getShapeTypeEv = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZN36btDiscreteCollisionDetectorInterface6ResultC2Ev = comdat any

$_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev = comdat any

$_ZN36btDiscreteCollisionDetectorInterface6ResultD0Ev = comdat any

$_ZTS30btConvexPenetrationDepthSolver = comdat any

$_ZTI30btConvexPenetrationDepthSolver = comdat any

$_ZTSN36btDiscreteCollisionDetectorInterface6ResultE = comdat any

$_ZTIN36btDiscreteCollisionDetectorInterface6ResultE = comdat any

$_ZTVN36btDiscreteCollisionDetectorInterface6ResultE = comdat any

@_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections = internal global [62 x %class.btVector3] zeroinitializer, align 16
@_ZGVZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections = internal global i32 0, align 4
@_ZTV33btMinkowskiPenetrationDepthSolver = hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI33btMinkowskiPenetrationDepthSolver to i8*), i8* bitcast (%class.btConvexPenetrationDepthSolver* (%class.btConvexPenetrationDepthSolver*)* @_ZN30btConvexPenetrationDepthSolverD2Ev to i8*), i8* bitcast (void (%class.btMinkowskiPenetrationDepthSolver*)* @_ZN33btMinkowskiPenetrationDepthSolverD0Ev to i8*), i8* bitcast (i1 (%class.btMinkowskiPenetrationDepthSolver*, %class.btVoronoiSimplexSolver*, %class.btConvexShape*, %class.btConvexShape*, %class.btTransform*, %class.btTransform*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btIDebugDraw*)* @_ZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDraw to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS33btMinkowskiPenetrationDepthSolver = hidden constant [36 x i8] c"33btMinkowskiPenetrationDepthSolver\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS30btConvexPenetrationDepthSolver = linkonce_odr hidden constant [33 x i8] c"30btConvexPenetrationDepthSolver\00", comdat, align 1
@_ZTI30btConvexPenetrationDepthSolver = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([33 x i8], [33 x i8]* @_ZTS30btConvexPenetrationDepthSolver, i32 0, i32 0) }, comdat, align 4
@_ZTI33btMinkowskiPenetrationDepthSolver = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([36 x i8], [36 x i8]* @_ZTS33btMinkowskiPenetrationDepthSolver, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btConvexPenetrationDepthSolver to i8*) }, align 4
@_ZTVZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawE20btIntermediateResult = internal unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawE20btIntermediateResult to i8*), i8* bitcast (%"struct.btDiscreteCollisionDetectorInterface::Result"* (%"struct.btDiscreteCollisionDetectorInterface::Result"*)* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev to i8*), i8* bitcast (void (%struct.btIntermediateResult*)* @_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawEN20btIntermediateResultD0Ev to i8*), i8* bitcast (void (%struct.btIntermediateResult*, i32, i32)* @_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawEN20btIntermediateResult20setShapeIdentifiersAEii to i8*), i8* bitcast (void (%struct.btIntermediateResult*, i32, i32)* @_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawEN20btIntermediateResult20setShapeIdentifiersBEii to i8*), i8* bitcast (void (%struct.btIntermediateResult*, %class.btVector3*, %class.btVector3*, float)* @_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawEN20btIntermediateResult15addContactPointERKS8_SE_f to i8*)] }, align 4
@_ZTSZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawE20btIntermediateResult = internal constant [171 x i8] c"ZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawE20btIntermediateResult\00", align 1
@_ZTSN36btDiscreteCollisionDetectorInterface6ResultE = linkonce_odr hidden constant [48 x i8] c"N36btDiscreteCollisionDetectorInterface6ResultE\00", comdat, align 1
@_ZTIN36btDiscreteCollisionDetectorInterface6ResultE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([48 x i8], [48 x i8]* @_ZTSN36btDiscreteCollisionDetectorInterface6ResultE, i32 0, i32 0) }, comdat, align 4
@_ZTIZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawE20btIntermediateResult = internal constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([171 x i8], [171 x i8]* @_ZTSZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawE20btIntermediateResult, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTIN36btDiscreteCollisionDetectorInterface6ResultE to i8*) }, align 4
@_ZTVN36btDiscreteCollisionDetectorInterface6ResultE = linkonce_odr hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTIN36btDiscreteCollisionDetectorInterface6ResultE to i8*), i8* bitcast (%"struct.btDiscreteCollisionDetectorInterface::Result"* (%"struct.btDiscreteCollisionDetectorInterface::Result"*)* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev to i8*), i8* bitcast (void (%"struct.btDiscreteCollisionDetectorInterface::Result"*)* @_ZN36btDiscreteCollisionDetectorInterface6ResultD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4

define hidden zeroext i1 @_ZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDraw(%class.btMinkowskiPenetrationDepthSolver* %this, %class.btVoronoiSimplexSolver* nonnull align 4 dereferenceable(357) %simplexSolver, %class.btConvexShape* %convexA, %class.btConvexShape* %convexB, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btVector3* nonnull align 4 dereferenceable(16) %pa, %class.btVector3* nonnull align 4 dereferenceable(16) %pb, %class.btIDebugDraw* %debugDraw) unnamed_addr #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btMinkowskiPenetrationDepthSolver*, align 4
  %simplexSolver.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %convexA.addr = alloca %class.btConvexShape*, align 4
  %convexB.addr = alloca %class.btConvexShape*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %pa.addr = alloca %class.btVector3*, align 4
  %pb.addr = alloca %class.btVector3*, align 4
  %debugDraw.addr = alloca %class.btIDebugDraw*, align 4
  %check2d = alloca i8, align 1
  %minProj = alloca float, align 4
  %minNorm = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %minA = alloca %class.btVector3, align 4
  %minB = alloca %class.btVector3, align 4
  %seperatingAxisInA = alloca %class.btVector3, align 4
  %seperatingAxisInB = alloca %class.btVector3, align 4
  %pInA = alloca %class.btVector3, align 4
  %qInB = alloca %class.btVector3, align 4
  %pWorld = alloca %class.btVector3, align 4
  %qWorld = alloca %class.btVector3, align 4
  %w = alloca %class.btVector3, align 4
  %supportVerticesABatch = alloca [62 x %class.btVector3], align 16
  %supportVerticesBBatch = alloca [62 x %class.btVector3], align 16
  %seperatingAxisInABatch = alloca [62 x %class.btVector3], align 16
  %seperatingAxisInBBatch = alloca [62 x %class.btVector3], align 16
  %i = alloca i32, align 4
  %numSampleDirections = alloca i32, align 4
  %norm = alloca %class.btVector3, align 4
  %ref.tmp41 = alloca %class.btVector3, align 4
  %ref.tmp42 = alloca %class.btVector3, align 4
  %ref.tmp45 = alloca %class.btVector3, align 4
  %numPDA = alloca i32, align 4
  %i49 = alloca i32, align 4
  %norm53 = alloca %class.btVector3, align 4
  %ref.tmp57 = alloca %class.btVector3, align 4
  %ref.tmp61 = alloca %class.btVector3, align 4
  %ref.tmp62 = alloca %class.btVector3, align 4
  %ref.tmp65 = alloca %class.btVector3, align 4
  %numPDB = alloca i32, align 4
  %i77 = alloca i32, align 4
  %norm82 = alloca %class.btVector3, align 4
  %ref.tmp86 = alloca %class.btVector3, align 4
  %ref.tmp90 = alloca %class.btVector3, align 4
  %ref.tmp91 = alloca %class.btVector3, align 4
  %ref.tmp94 = alloca %class.btVector3, align 4
  %norm112 = alloca %class.btVector3, align 4
  %ref.tmp127 = alloca %class.btVector3, align 4
  %ref.tmp128 = alloca %class.btVector3, align 4
  %ref.tmp136 = alloca %class.btVector3, align 4
  %delta = alloca float, align 4
  %ref.tmp145 = alloca %class.btVector3, align 4
  %ref.tmp146 = alloca float, align 4
  %ref.tmp149 = alloca %class.btVector3, align 4
  %ref.tmp150 = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %extraSeparation = alloca float, align 4
  %gjkdet = alloca %class.btGjkPairDetector, align 4
  %offsetDist = alloca float, align 4
  %offset = alloca %class.btVector3, align 4
  %input = alloca %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", align 4
  %newOrg = alloca %class.btVector3, align 4
  %displacedTrans = alloca %class.btTransform, align 4
  %res = alloca %struct.btIntermediateResult, align 4
  %ref.tmp167 = alloca %class.btVector3, align 4
  %correctedMinNorm = alloca float, align 4
  %penetration_relaxation = alloca float, align 4
  %ref.tmp171 = alloca %class.btVector3, align 4
  %ref.tmp172 = alloca %class.btVector3, align 4
  store %class.btMinkowskiPenetrationDepthSolver* %this, %class.btMinkowskiPenetrationDepthSolver** %this.addr, align 4, !tbaa !2
  store %class.btVoronoiSimplexSolver* %simplexSolver, %class.btVoronoiSimplexSolver** %simplexSolver.addr, align 4, !tbaa !2
  store %class.btConvexShape* %convexA, %class.btConvexShape** %convexA.addr, align 4, !tbaa !2
  store %class.btConvexShape* %convexB, %class.btConvexShape** %convexB.addr, align 4, !tbaa !2
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4, !tbaa !2
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store %class.btVector3* %pa, %class.btVector3** %pa.addr, align 4, !tbaa !2
  store %class.btVector3* %pb, %class.btVector3** %pb.addr, align 4, !tbaa !2
  store %class.btIDebugDraw* %debugDraw, %class.btIDebugDraw** %debugDraw.addr, align 4, !tbaa !2
  %this1 = load %class.btMinkowskiPenetrationDepthSolver*, %class.btMinkowskiPenetrationDepthSolver** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %check2d) #6
  %1 = load %class.btConvexShape*, %class.btConvexShape** %convexA.addr, align 4, !tbaa !2
  %2 = bitcast %class.btConvexShape* %1 to %class.btCollisionShape*
  %call = call zeroext i1 @_ZNK16btCollisionShape10isConvex2dEv(%class.btCollisionShape* %2)
  br i1 %call, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %3 = load %class.btConvexShape*, %class.btConvexShape** %convexB.addr, align 4, !tbaa !2
  %4 = bitcast %class.btConvexShape* %3 to %class.btCollisionShape*
  %call2 = call zeroext i1 @_ZNK16btCollisionShape10isConvex2dEv(%class.btCollisionShape* %4)
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %5 = phi i1 [ false, %entry ], [ %call2, %land.rhs ]
  %frombool = zext i1 %5 to i8
  store i8 %frombool, i8* %check2d, align 1, !tbaa !6
  %6 = bitcast float* %minProj to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  store float 0x43ABC16D60000000, float* %minProj, align 4, !tbaa !8
  %7 = bitcast %class.btVector3* %minNorm to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #6
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !8
  %9 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !8
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %minNorm, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %11 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  %12 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #6
  %13 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #6
  %14 = bitcast %class.btVector3* %minA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %14) #6
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %minA)
  %15 = bitcast %class.btVector3* %minB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %15) #6
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %minB)
  %16 = bitcast %class.btVector3* %seperatingAxisInA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #6
  %call8 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %seperatingAxisInA)
  %17 = bitcast %class.btVector3* %seperatingAxisInB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %17) #6
  %call9 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %seperatingAxisInB)
  %18 = bitcast %class.btVector3* %pInA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %18) #6
  %call10 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pInA)
  %19 = bitcast %class.btVector3* %qInB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %19) #6
  %call11 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %qInB)
  %20 = bitcast %class.btVector3* %pWorld to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %20) #6
  %call12 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pWorld)
  %21 = bitcast %class.btVector3* %qWorld to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %21) #6
  %call13 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %qWorld)
  %22 = bitcast %class.btVector3* %w to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %22) #6
  %call14 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %w)
  %23 = bitcast [62 x %class.btVector3]* %supportVerticesABatch to i8*
  call void @llvm.lifetime.start.p0i8(i64 992, i8* %23) #6
  %array.begin = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %supportVerticesABatch, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 62
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %land.end
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %land.end ], [ %arrayctor.next, %arrayctor.loop ]
  %call15 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %24 = bitcast [62 x %class.btVector3]* %supportVerticesBBatch to i8*
  call void @llvm.lifetime.start.p0i8(i64 992, i8* %24) #6
  %array.begin16 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %supportVerticesBBatch, i32 0, i32 0
  %arrayctor.end17 = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin16, i32 62
  br label %arrayctor.loop18

arrayctor.loop18:                                 ; preds = %arrayctor.loop18, %arrayctor.cont
  %arrayctor.cur19 = phi %class.btVector3* [ %array.begin16, %arrayctor.cont ], [ %arrayctor.next21, %arrayctor.loop18 ]
  %call20 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur19)
  %arrayctor.next21 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur19, i32 1
  %arrayctor.done22 = icmp eq %class.btVector3* %arrayctor.next21, %arrayctor.end17
  br i1 %arrayctor.done22, label %arrayctor.cont23, label %arrayctor.loop18

arrayctor.cont23:                                 ; preds = %arrayctor.loop18
  %25 = bitcast [62 x %class.btVector3]* %seperatingAxisInABatch to i8*
  call void @llvm.lifetime.start.p0i8(i64 992, i8* %25) #6
  %array.begin24 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %seperatingAxisInABatch, i32 0, i32 0
  %arrayctor.end25 = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin24, i32 62
  br label %arrayctor.loop26

arrayctor.loop26:                                 ; preds = %arrayctor.loop26, %arrayctor.cont23
  %arrayctor.cur27 = phi %class.btVector3* [ %array.begin24, %arrayctor.cont23 ], [ %arrayctor.next29, %arrayctor.loop26 ]
  %call28 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur27)
  %arrayctor.next29 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur27, i32 1
  %arrayctor.done30 = icmp eq %class.btVector3* %arrayctor.next29, %arrayctor.end25
  br i1 %arrayctor.done30, label %arrayctor.cont31, label %arrayctor.loop26

arrayctor.cont31:                                 ; preds = %arrayctor.loop26
  %26 = bitcast [62 x %class.btVector3]* %seperatingAxisInBBatch to i8*
  call void @llvm.lifetime.start.p0i8(i64 992, i8* %26) #6
  %array.begin32 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %seperatingAxisInBBatch, i32 0, i32 0
  %arrayctor.end33 = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin32, i32 62
  br label %arrayctor.loop34

arrayctor.loop34:                                 ; preds = %arrayctor.loop34, %arrayctor.cont31
  %arrayctor.cur35 = phi %class.btVector3* [ %array.begin32, %arrayctor.cont31 ], [ %arrayctor.next37, %arrayctor.loop34 ]
  %call36 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur35)
  %arrayctor.next37 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur35, i32 1
  %arrayctor.done38 = icmp eq %class.btVector3* %arrayctor.next37, %arrayctor.end33
  br i1 %arrayctor.done38, label %arrayctor.cont39, label %arrayctor.loop34

arrayctor.cont39:                                 ; preds = %arrayctor.loop34
  %27 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #6
  %28 = bitcast i32* %numSampleDirections to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #6
  store i32 42, i32* %numSampleDirections, align 4, !tbaa !10
  store i32 0, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %arrayctor.cont39
  %29 = load i32, i32* %i, align 4, !tbaa !10
  %30 = load i32, i32* %numSampleDirections, align 4, !tbaa !10
  %cmp = icmp slt i32 %29, %30
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %31 = bitcast %class.btVector3* %norm to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %31) #6
  %call40 = call %class.btVector3* @_ZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEv()
  %32 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %call40, i32 %32
  %33 = bitcast %class.btVector3* %norm to i8*
  %34 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %33, i8* align 4 %34, i32 16, i1 false), !tbaa.struct !12
  %35 = bitcast %class.btVector3* %ref.tmp41 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %35) #6
  %36 = bitcast %class.btVector3* %ref.tmp42 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %36) #6
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp42, %class.btVector3* nonnull align 4 dereferenceable(16) %norm)
  %37 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %call43 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %37)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp41, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp42, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call43)
  %38 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx44 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %seperatingAxisInABatch, i32 0, i32 %38
  %39 = bitcast %class.btVector3* %arrayidx44 to i8*
  %40 = bitcast %class.btVector3* %ref.tmp41 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %39, i8* align 4 %40, i32 16, i1 false), !tbaa.struct !12
  %41 = bitcast %class.btVector3* %ref.tmp42 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %41) #6
  %42 = bitcast %class.btVector3* %ref.tmp41 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %42) #6
  %43 = bitcast %class.btVector3* %ref.tmp45 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %43) #6
  %44 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %call46 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %44)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp45, %class.btVector3* nonnull align 4 dereferenceable(16) %norm, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call46)
  %45 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx47 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %seperatingAxisInBBatch, i32 0, i32 %45
  %46 = bitcast %class.btVector3* %arrayidx47 to i8*
  %47 = bitcast %class.btVector3* %ref.tmp45 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %46, i8* align 4 %47, i32 16, i1 false), !tbaa.struct !12
  %48 = bitcast %class.btVector3* %ref.tmp45 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %48) #6
  %49 = bitcast %class.btVector3* %norm to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %49) #6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %50 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add nsw i32 %50, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %51 = bitcast i32* %numPDA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #6
  %52 = load %class.btConvexShape*, %class.btConvexShape** %convexA.addr, align 4, !tbaa !2
  %53 = bitcast %class.btConvexShape* %52 to i32 (%class.btConvexShape*)***
  %vtable = load i32 (%class.btConvexShape*)**, i32 (%class.btConvexShape*)*** %53, align 4, !tbaa !14
  %vfn = getelementptr inbounds i32 (%class.btConvexShape*)*, i32 (%class.btConvexShape*)** %vtable, i64 21
  %54 = load i32 (%class.btConvexShape*)*, i32 (%class.btConvexShape*)** %vfn, align 4
  %call48 = call i32 %54(%class.btConvexShape* %52)
  store i32 %call48, i32* %numPDA, align 4, !tbaa !10
  %55 = load i32, i32* %numPDA, align 4, !tbaa !10
  %tobool = icmp ne i32 %55, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %56 = bitcast i32* %i49 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #6
  store i32 0, i32* %i49, align 4, !tbaa !10
  br label %for.cond50

for.cond50:                                       ; preds = %for.inc69, %if.then
  %57 = load i32, i32* %i49, align 4, !tbaa !10
  %58 = load i32, i32* %numPDA, align 4, !tbaa !10
  %cmp51 = icmp slt i32 %57, %58
  br i1 %cmp51, label %for.body52, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond50
  %59 = bitcast i32* %i49 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #6
  br label %for.end71

for.body52:                                       ; preds = %for.cond50
  %60 = bitcast %class.btVector3* %norm53 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %60) #6
  %call54 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %norm53)
  %61 = load %class.btConvexShape*, %class.btConvexShape** %convexA.addr, align 4, !tbaa !2
  %62 = load i32, i32* %i49, align 4, !tbaa !10
  %63 = bitcast %class.btConvexShape* %61 to void (%class.btConvexShape*, i32, %class.btVector3*)***
  %vtable55 = load void (%class.btConvexShape*, i32, %class.btVector3*)**, void (%class.btConvexShape*, i32, %class.btVector3*)*** %63, align 4, !tbaa !14
  %vfn56 = getelementptr inbounds void (%class.btConvexShape*, i32, %class.btVector3*)*, void (%class.btConvexShape*, i32, %class.btVector3*)** %vtable55, i64 22
  %64 = load void (%class.btConvexShape*, i32, %class.btVector3*)*, void (%class.btConvexShape*, i32, %class.btVector3*)** %vfn56, align 4
  call void %64(%class.btConvexShape* %61, i32 %62, %class.btVector3* nonnull align 4 dereferenceable(16) %norm53)
  %65 = bitcast %class.btVector3* %ref.tmp57 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %65) #6
  %66 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %call58 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %66)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp57, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call58, %class.btVector3* nonnull align 4 dereferenceable(16) %norm53)
  %67 = bitcast %class.btVector3* %norm53 to i8*
  %68 = bitcast %class.btVector3* %ref.tmp57 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %67, i8* align 4 %68, i32 16, i1 false), !tbaa.struct !12
  %69 = bitcast %class.btVector3* %ref.tmp57 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %69) #6
  %call59 = call %class.btVector3* @_ZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEv()
  %70 = load i32, i32* %numSampleDirections, align 4, !tbaa !10
  %arrayidx60 = getelementptr inbounds %class.btVector3, %class.btVector3* %call59, i32 %70
  %71 = bitcast %class.btVector3* %arrayidx60 to i8*
  %72 = bitcast %class.btVector3* %norm53 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %71, i8* align 4 %72, i32 16, i1 false), !tbaa.struct !12
  %73 = bitcast %class.btVector3* %ref.tmp61 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %73) #6
  %74 = bitcast %class.btVector3* %ref.tmp62 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %74) #6
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp62, %class.btVector3* nonnull align 4 dereferenceable(16) %norm53)
  %75 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %call63 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %75)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp61, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp62, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call63)
  %76 = load i32, i32* %numSampleDirections, align 4, !tbaa !10
  %arrayidx64 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %seperatingAxisInABatch, i32 0, i32 %76
  %77 = bitcast %class.btVector3* %arrayidx64 to i8*
  %78 = bitcast %class.btVector3* %ref.tmp61 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %77, i8* align 4 %78, i32 16, i1 false), !tbaa.struct !12
  %79 = bitcast %class.btVector3* %ref.tmp62 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %79) #6
  %80 = bitcast %class.btVector3* %ref.tmp61 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %80) #6
  %81 = bitcast %class.btVector3* %ref.tmp65 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %81) #6
  %82 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %call66 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %82)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp65, %class.btVector3* nonnull align 4 dereferenceable(16) %norm53, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call66)
  %83 = load i32, i32* %numSampleDirections, align 4, !tbaa !10
  %arrayidx67 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %seperatingAxisInBBatch, i32 0, i32 %83
  %84 = bitcast %class.btVector3* %arrayidx67 to i8*
  %85 = bitcast %class.btVector3* %ref.tmp65 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %84, i8* align 4 %85, i32 16, i1 false), !tbaa.struct !12
  %86 = bitcast %class.btVector3* %ref.tmp65 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %86) #6
  %87 = load i32, i32* %numSampleDirections, align 4, !tbaa !10
  %inc68 = add nsw i32 %87, 1
  store i32 %inc68, i32* %numSampleDirections, align 4, !tbaa !10
  %88 = bitcast %class.btVector3* %norm53 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %88) #6
  br label %for.inc69

for.inc69:                                        ; preds = %for.body52
  %89 = load i32, i32* %i49, align 4, !tbaa !10
  %inc70 = add nsw i32 %89, 1
  store i32 %inc70, i32* %i49, align 4, !tbaa !10
  br label %for.cond50

for.end71:                                        ; preds = %for.cond.cleanup
  br label %if.end

if.end:                                           ; preds = %for.end71, %for.end
  %90 = bitcast i32* %numPDA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #6
  %91 = bitcast i32* %numPDB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %91) #6
  %92 = load %class.btConvexShape*, %class.btConvexShape** %convexB.addr, align 4, !tbaa !2
  %93 = bitcast %class.btConvexShape* %92 to i32 (%class.btConvexShape*)***
  %vtable72 = load i32 (%class.btConvexShape*)**, i32 (%class.btConvexShape*)*** %93, align 4, !tbaa !14
  %vfn73 = getelementptr inbounds i32 (%class.btConvexShape*)*, i32 (%class.btConvexShape*)** %vtable72, i64 21
  %94 = load i32 (%class.btConvexShape*)*, i32 (%class.btConvexShape*)** %vfn73, align 4
  %call74 = call i32 %94(%class.btConvexShape* %92)
  store i32 %call74, i32* %numPDB, align 4, !tbaa !10
  %95 = load i32, i32* %numPDB, align 4, !tbaa !10
  %tobool75 = icmp ne i32 %95, 0
  br i1 %tobool75, label %if.then76, label %if.end101

if.then76:                                        ; preds = %if.end
  %96 = bitcast i32* %i77 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %96) #6
  store i32 0, i32* %i77, align 4, !tbaa !10
  br label %for.cond78

for.cond78:                                       ; preds = %for.inc98, %if.then76
  %97 = load i32, i32* %i77, align 4, !tbaa !10
  %98 = load i32, i32* %numPDB, align 4, !tbaa !10
  %cmp79 = icmp slt i32 %97, %98
  br i1 %cmp79, label %for.body81, label %for.cond.cleanup80

for.cond.cleanup80:                               ; preds = %for.cond78
  %99 = bitcast i32* %i77 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #6
  br label %for.end100

for.body81:                                       ; preds = %for.cond78
  %100 = bitcast %class.btVector3* %norm82 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %100) #6
  %call83 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %norm82)
  %101 = load %class.btConvexShape*, %class.btConvexShape** %convexB.addr, align 4, !tbaa !2
  %102 = load i32, i32* %i77, align 4, !tbaa !10
  %103 = bitcast %class.btConvexShape* %101 to void (%class.btConvexShape*, i32, %class.btVector3*)***
  %vtable84 = load void (%class.btConvexShape*, i32, %class.btVector3*)**, void (%class.btConvexShape*, i32, %class.btVector3*)*** %103, align 4, !tbaa !14
  %vfn85 = getelementptr inbounds void (%class.btConvexShape*, i32, %class.btVector3*)*, void (%class.btConvexShape*, i32, %class.btVector3*)** %vtable84, i64 22
  %104 = load void (%class.btConvexShape*, i32, %class.btVector3*)*, void (%class.btConvexShape*, i32, %class.btVector3*)** %vfn85, align 4
  call void %104(%class.btConvexShape* %101, i32 %102, %class.btVector3* nonnull align 4 dereferenceable(16) %norm82)
  %105 = bitcast %class.btVector3* %ref.tmp86 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %105) #6
  %106 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %call87 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %106)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp86, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call87, %class.btVector3* nonnull align 4 dereferenceable(16) %norm82)
  %107 = bitcast %class.btVector3* %norm82 to i8*
  %108 = bitcast %class.btVector3* %ref.tmp86 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %107, i8* align 4 %108, i32 16, i1 false), !tbaa.struct !12
  %109 = bitcast %class.btVector3* %ref.tmp86 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %109) #6
  %call88 = call %class.btVector3* @_ZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEv()
  %110 = load i32, i32* %numSampleDirections, align 4, !tbaa !10
  %arrayidx89 = getelementptr inbounds %class.btVector3, %class.btVector3* %call88, i32 %110
  %111 = bitcast %class.btVector3* %arrayidx89 to i8*
  %112 = bitcast %class.btVector3* %norm82 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %111, i8* align 4 %112, i32 16, i1 false), !tbaa.struct !12
  %113 = bitcast %class.btVector3* %ref.tmp90 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %113) #6
  %114 = bitcast %class.btVector3* %ref.tmp91 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %114) #6
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp91, %class.btVector3* nonnull align 4 dereferenceable(16) %norm82)
  %115 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %call92 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %115)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp90, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp91, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call92)
  %116 = load i32, i32* %numSampleDirections, align 4, !tbaa !10
  %arrayidx93 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %seperatingAxisInABatch, i32 0, i32 %116
  %117 = bitcast %class.btVector3* %arrayidx93 to i8*
  %118 = bitcast %class.btVector3* %ref.tmp90 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %117, i8* align 4 %118, i32 16, i1 false), !tbaa.struct !12
  %119 = bitcast %class.btVector3* %ref.tmp91 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %119) #6
  %120 = bitcast %class.btVector3* %ref.tmp90 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %120) #6
  %121 = bitcast %class.btVector3* %ref.tmp94 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %121) #6
  %122 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %call95 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %122)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp94, %class.btVector3* nonnull align 4 dereferenceable(16) %norm82, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call95)
  %123 = load i32, i32* %numSampleDirections, align 4, !tbaa !10
  %arrayidx96 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %seperatingAxisInBBatch, i32 0, i32 %123
  %124 = bitcast %class.btVector3* %arrayidx96 to i8*
  %125 = bitcast %class.btVector3* %ref.tmp94 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %124, i8* align 4 %125, i32 16, i1 false), !tbaa.struct !12
  %126 = bitcast %class.btVector3* %ref.tmp94 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %126) #6
  %127 = load i32, i32* %numSampleDirections, align 4, !tbaa !10
  %inc97 = add nsw i32 %127, 1
  store i32 %inc97, i32* %numSampleDirections, align 4, !tbaa !10
  %128 = bitcast %class.btVector3* %norm82 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %128) #6
  br label %for.inc98

for.inc98:                                        ; preds = %for.body81
  %129 = load i32, i32* %i77, align 4, !tbaa !10
  %inc99 = add nsw i32 %129, 1
  store i32 %inc99, i32* %i77, align 4, !tbaa !10
  br label %for.cond78

for.end100:                                       ; preds = %for.cond.cleanup80
  br label %if.end101

if.end101:                                        ; preds = %for.end100, %if.end
  %130 = bitcast i32* %numPDB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #6
  %131 = load %class.btConvexShape*, %class.btConvexShape** %convexA.addr, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %seperatingAxisInABatch, i32 0, i32 0
  %arraydecay102 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %supportVerticesABatch, i32 0, i32 0
  %132 = load i32, i32* %numSampleDirections, align 4, !tbaa !10
  %133 = bitcast %class.btConvexShape* %131 to void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)***
  %vtable103 = load void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)**, void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)*** %133, align 4, !tbaa !14
  %vfn104 = getelementptr inbounds void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)*, void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)** %vtable103, i64 19
  %134 = load void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)*, void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)** %vfn104, align 4
  call void %134(%class.btConvexShape* %131, %class.btVector3* %arraydecay, %class.btVector3* %arraydecay102, i32 %132)
  %135 = load %class.btConvexShape*, %class.btConvexShape** %convexB.addr, align 4, !tbaa !2
  %arraydecay105 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %seperatingAxisInBBatch, i32 0, i32 0
  %arraydecay106 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %supportVerticesBBatch, i32 0, i32 0
  %136 = load i32, i32* %numSampleDirections, align 4, !tbaa !10
  %137 = bitcast %class.btConvexShape* %135 to void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)***
  %vtable107 = load void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)**, void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)*** %137, align 4, !tbaa !14
  %vfn108 = getelementptr inbounds void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)*, void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)** %vtable107, i64 19
  %138 = load void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)*, void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)** %vfn108, align 4
  call void %138(%class.btConvexShape* %135, %class.btVector3* %arraydecay105, %class.btVector3* %arraydecay106, i32 %136)
  store i32 0, i32* %i, align 4, !tbaa !10
  br label %for.cond109

for.cond109:                                      ; preds = %for.inc142, %if.end101
  %139 = load i32, i32* %i, align 4, !tbaa !10
  %140 = load i32, i32* %numSampleDirections, align 4, !tbaa !10
  %cmp110 = icmp slt i32 %139, %140
  br i1 %cmp110, label %for.body111, label %for.end144

for.body111:                                      ; preds = %for.cond109
  %141 = bitcast %class.btVector3* %norm112 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %141) #6
  %call113 = call %class.btVector3* @_ZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEv()
  %142 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx114 = getelementptr inbounds %class.btVector3, %class.btVector3* %call113, i32 %142
  %143 = bitcast %class.btVector3* %norm112 to i8*
  %144 = bitcast %class.btVector3* %arrayidx114 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %143, i8* align 4 %144, i32 16, i1 false), !tbaa.struct !12
  %145 = load i8, i8* %check2d, align 1, !tbaa !6, !range !16
  %tobool115 = trunc i8 %145 to i1
  br i1 %tobool115, label %if.then116, label %if.end119

if.then116:                                       ; preds = %for.body111
  %call117 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %norm112)
  %arrayidx118 = getelementptr inbounds float, float* %call117, i32 2
  store float 0.000000e+00, float* %arrayidx118, align 4, !tbaa !8
  br label %if.end119

if.end119:                                        ; preds = %if.then116, %for.body111
  %call120 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %norm112)
  %conv = fpext float %call120 to double
  %cmp121 = fcmp ogt double %conv, 1.000000e-02
  br i1 %cmp121, label %if.then122, label %if.end141

if.then122:                                       ; preds = %if.end119
  %146 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx123 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %seperatingAxisInABatch, i32 0, i32 %146
  %147 = bitcast %class.btVector3* %seperatingAxisInA to i8*
  %148 = bitcast %class.btVector3* %arrayidx123 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %147, i8* align 16 %148, i32 16, i1 false), !tbaa.struct !12
  %149 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx124 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %seperatingAxisInBBatch, i32 0, i32 %149
  %150 = bitcast %class.btVector3* %seperatingAxisInB to i8*
  %151 = bitcast %class.btVector3* %arrayidx124 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %150, i8* align 16 %151, i32 16, i1 false), !tbaa.struct !12
  %152 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx125 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %supportVerticesABatch, i32 0, i32 %152
  %153 = bitcast %class.btVector3* %pInA to i8*
  %154 = bitcast %class.btVector3* %arrayidx125 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %153, i8* align 16 %154, i32 16, i1 false), !tbaa.struct !12
  %155 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx126 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %supportVerticesBBatch, i32 0, i32 %155
  %156 = bitcast %class.btVector3* %qInB to i8*
  %157 = bitcast %class.btVector3* %arrayidx126 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %156, i8* align 16 %157, i32 16, i1 false), !tbaa.struct !12
  %158 = bitcast %class.btVector3* %ref.tmp127 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %158) #6
  %159 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp127, %class.btTransform* %159, %class.btVector3* nonnull align 4 dereferenceable(16) %pInA)
  %160 = bitcast %class.btVector3* %pWorld to i8*
  %161 = bitcast %class.btVector3* %ref.tmp127 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %160, i8* align 4 %161, i32 16, i1 false), !tbaa.struct !12
  %162 = bitcast %class.btVector3* %ref.tmp127 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %162) #6
  %163 = bitcast %class.btVector3* %ref.tmp128 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %163) #6
  %164 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp128, %class.btTransform* %164, %class.btVector3* nonnull align 4 dereferenceable(16) %qInB)
  %165 = bitcast %class.btVector3* %qWorld to i8*
  %166 = bitcast %class.btVector3* %ref.tmp128 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %165, i8* align 4 %166, i32 16, i1 false), !tbaa.struct !12
  %167 = bitcast %class.btVector3* %ref.tmp128 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %167) #6
  %168 = load i8, i8* %check2d, align 1, !tbaa !6, !range !16
  %tobool129 = trunc i8 %168 to i1
  br i1 %tobool129, label %if.then130, label %if.end135

if.then130:                                       ; preds = %if.then122
  %call131 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pWorld)
  %arrayidx132 = getelementptr inbounds float, float* %call131, i32 2
  store float 0.000000e+00, float* %arrayidx132, align 4, !tbaa !8
  %call133 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %qWorld)
  %arrayidx134 = getelementptr inbounds float, float* %call133, i32 2
  store float 0.000000e+00, float* %arrayidx134, align 4, !tbaa !8
  br label %if.end135

if.end135:                                        ; preds = %if.then130, %if.then122
  %169 = bitcast %class.btVector3* %ref.tmp136 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %169) #6
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp136, %class.btVector3* nonnull align 4 dereferenceable(16) %qWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %pWorld)
  %170 = bitcast %class.btVector3* %w to i8*
  %171 = bitcast %class.btVector3* %ref.tmp136 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %170, i8* align 4 %171, i32 16, i1 false), !tbaa.struct !12
  %172 = bitcast %class.btVector3* %ref.tmp136 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %172) #6
  %173 = bitcast float* %delta to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %173) #6
  %call137 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %norm112, %class.btVector3* nonnull align 4 dereferenceable(16) %w)
  store float %call137, float* %delta, align 4, !tbaa !8
  %174 = load float, float* %delta, align 4, !tbaa !8
  %175 = load float, float* %minProj, align 4, !tbaa !8
  %cmp138 = fcmp olt float %174, %175
  br i1 %cmp138, label %if.then139, label %if.end140

if.then139:                                       ; preds = %if.end135
  %176 = load float, float* %delta, align 4, !tbaa !8
  store float %176, float* %minProj, align 4, !tbaa !8
  %177 = bitcast %class.btVector3* %minNorm to i8*
  %178 = bitcast %class.btVector3* %norm112 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %177, i8* align 4 %178, i32 16, i1 false), !tbaa.struct !12
  %179 = bitcast %class.btVector3* %minA to i8*
  %180 = bitcast %class.btVector3* %pWorld to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %179, i8* align 4 %180, i32 16, i1 false), !tbaa.struct !12
  %181 = bitcast %class.btVector3* %minB to i8*
  %182 = bitcast %class.btVector3* %qWorld to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %181, i8* align 4 %182, i32 16, i1 false), !tbaa.struct !12
  br label %if.end140

if.end140:                                        ; preds = %if.then139, %if.end135
  %183 = bitcast float* %delta to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %183) #6
  br label %if.end141

if.end141:                                        ; preds = %if.end140, %if.end119
  %184 = bitcast %class.btVector3* %norm112 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %184) #6
  br label %for.inc142

for.inc142:                                       ; preds = %if.end141
  %185 = load i32, i32* %i, align 4, !tbaa !10
  %inc143 = add nsw i32 %185, 1
  store i32 %inc143, i32* %i, align 4, !tbaa !10
  br label %for.cond109

for.end144:                                       ; preds = %for.cond109
  %186 = bitcast %class.btVector3* %ref.tmp145 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %186) #6
  %187 = bitcast float* %ref.tmp146 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %187) #6
  %188 = load %class.btConvexShape*, %class.btConvexShape** %convexA.addr, align 4, !tbaa !2
  %call147 = call float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape* %188)
  store float %call147, float* %ref.tmp146, align 4, !tbaa !8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp145, %class.btVector3* nonnull align 4 dereferenceable(16) %minNorm, float* nonnull align 4 dereferenceable(4) %ref.tmp146)
  %call148 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %minA, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp145)
  %189 = bitcast float* %ref.tmp146 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %189) #6
  %190 = bitcast %class.btVector3* %ref.tmp145 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %190) #6
  %191 = bitcast %class.btVector3* %ref.tmp149 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %191) #6
  %192 = bitcast float* %ref.tmp150 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %192) #6
  %193 = load %class.btConvexShape*, %class.btConvexShape** %convexB.addr, align 4, !tbaa !2
  %call151 = call float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape* %193)
  store float %call151, float* %ref.tmp150, align 4, !tbaa !8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp149, %class.btVector3* nonnull align 4 dereferenceable(16) %minNorm, float* nonnull align 4 dereferenceable(4) %ref.tmp150)
  %call152 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %minB, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp149)
  %194 = bitcast float* %ref.tmp150 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %194) #6
  %195 = bitcast %class.btVector3* %ref.tmp149 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %195) #6
  %196 = load float, float* %minProj, align 4, !tbaa !8
  %cmp153 = fcmp olt float %196, 0.000000e+00
  br i1 %cmp153, label %if.then154, label %if.end155

if.then154:                                       ; preds = %for.end144
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end155:                                        ; preds = %for.end144
  %197 = bitcast float* %extraSeparation to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %197) #6
  store float 5.000000e-01, float* %extraSeparation, align 4, !tbaa !8
  %198 = load float, float* %extraSeparation, align 4, !tbaa !8
  %199 = load %class.btConvexShape*, %class.btConvexShape** %convexA.addr, align 4, !tbaa !2
  %call156 = call float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape* %199)
  %200 = load %class.btConvexShape*, %class.btConvexShape** %convexB.addr, align 4, !tbaa !2
  %call157 = call float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape* %200)
  %add = fadd float %call156, %call157
  %add158 = fadd float %198, %add
  %201 = load float, float* %minProj, align 4, !tbaa !8
  %add159 = fadd float %201, %add158
  store float %add159, float* %minProj, align 4, !tbaa !8
  %202 = bitcast %class.btGjkPairDetector* %gjkdet to i8*
  call void @llvm.lifetime.start.p0i8(i64 80, i8* %202) #6
  %203 = load %class.btConvexShape*, %class.btConvexShape** %convexA.addr, align 4, !tbaa !2
  %204 = load %class.btConvexShape*, %class.btConvexShape** %convexB.addr, align 4, !tbaa !2
  %205 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %simplexSolver.addr, align 4, !tbaa !2
  %call160 = call %class.btGjkPairDetector* @_ZN17btGjkPairDetectorC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver(%class.btGjkPairDetector* %gjkdet, %class.btConvexShape* %203, %class.btConvexShape* %204, %class.btVoronoiSimplexSolver* %205, %class.btConvexPenetrationDepthSolver* null)
  %206 = bitcast float* %offsetDist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %206) #6
  %207 = load float, float* %minProj, align 4, !tbaa !8
  store float %207, float* %offsetDist, align 4, !tbaa !8
  %208 = bitcast %class.btVector3* %offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %208) #6
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %offset, %class.btVector3* nonnull align 4 dereferenceable(16) %minNorm, float* nonnull align 4 dereferenceable(4) %offsetDist)
  %209 = bitcast %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input to i8*
  call void @llvm.lifetime.start.p0i8(i64 132, i8* %209) #6
  %call161 = call %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* @_ZN36btDiscreteCollisionDetectorInterface17ClosestPointInputC2Ev(%"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input)
  %210 = bitcast %class.btVector3* %newOrg to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %210) #6
  %211 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %call162 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %211)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %newOrg, %class.btVector3* nonnull align 4 dereferenceable(16) %call162, %class.btVector3* nonnull align 4 dereferenceable(16) %offset)
  %212 = bitcast %class.btTransform* %displacedTrans to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %212) #6
  %213 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %call163 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %displacedTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %213)
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %displacedTrans, %class.btVector3* nonnull align 4 dereferenceable(16) %newOrg)
  %m_transformA = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, i32 0, i32 0
  %call164 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_transformA, %class.btTransform* nonnull align 4 dereferenceable(64) %displacedTrans)
  %214 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %m_transformB = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, i32 0, i32 1
  %call165 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_transformB, %class.btTransform* nonnull align 4 dereferenceable(64) %214)
  %m_maximumDistanceSquared = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, i32 0, i32 2
  store float 0x43ABC16D60000000, float* %m_maximumDistanceSquared, align 4, !tbaa !17
  %215 = bitcast %struct.btIntermediateResult* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 44, i8* %215) #6
  %call166 = call %struct.btIntermediateResult* @_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawEN20btIntermediateResultC2Ev(%struct.btIntermediateResult* %res)
  %216 = bitcast %class.btVector3* %ref.tmp167 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %216) #6
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp167, %class.btVector3* nonnull align 4 dereferenceable(16) %minNorm)
  call void @_ZN17btGjkPairDetector23setCachedSeperatingAxisERK9btVector3(%class.btGjkPairDetector* %gjkdet, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp167)
  %217 = bitcast %class.btVector3* %ref.tmp167 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %217) #6
  %218 = bitcast %struct.btIntermediateResult* %res to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  %219 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDraw.addr, align 4, !tbaa !2
  call void @_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb(%class.btGjkPairDetector* %gjkdet, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* nonnull align 4 dereferenceable(132) %input, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %218, %class.btIDebugDraw* %219, i1 zeroext false)
  %220 = bitcast float* %correctedMinNorm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %220) #6
  %221 = load float, float* %minProj, align 4, !tbaa !8
  %m_depth = getelementptr inbounds %struct.btIntermediateResult, %struct.btIntermediateResult* %res, i32 0, i32 3
  %222 = load float, float* %m_depth, align 4, !tbaa !22
  %sub = fsub float %221, %222
  store float %sub, float* %correctedMinNorm, align 4, !tbaa !8
  %223 = bitcast float* %penetration_relaxation to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %223) #6
  store float 1.000000e+00, float* %penetration_relaxation, align 4, !tbaa !8
  %call168 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %minNorm, float* nonnull align 4 dereferenceable(4) %penetration_relaxation)
  %m_hasResult = getelementptr inbounds %struct.btIntermediateResult, %struct.btIntermediateResult* %res, i32 0, i32 4
  %224 = load i8, i8* %m_hasResult, align 4, !tbaa !24, !range !16
  %tobool169 = trunc i8 %224 to i1
  br i1 %tobool169, label %if.then170, label %if.end174

if.then170:                                       ; preds = %if.end155
  %225 = bitcast %class.btVector3* %ref.tmp171 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %225) #6
  %m_pointInWorld = getelementptr inbounds %struct.btIntermediateResult, %struct.btIntermediateResult* %res, i32 0, i32 2
  %226 = bitcast %class.btVector3* %ref.tmp172 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %226) #6
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp172, %class.btVector3* nonnull align 4 dereferenceable(16) %minNorm, float* nonnull align 4 dereferenceable(4) %correctedMinNorm)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp171, %class.btVector3* nonnull align 4 dereferenceable(16) %m_pointInWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp172)
  %227 = load %class.btVector3*, %class.btVector3** %pa.addr, align 4, !tbaa !2
  %228 = bitcast %class.btVector3* %227 to i8*
  %229 = bitcast %class.btVector3* %ref.tmp171 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %228, i8* align 4 %229, i32 16, i1 false), !tbaa.struct !12
  %230 = bitcast %class.btVector3* %ref.tmp172 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %230) #6
  %231 = bitcast %class.btVector3* %ref.tmp171 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %231) #6
  %m_pointInWorld173 = getelementptr inbounds %struct.btIntermediateResult, %struct.btIntermediateResult* %res, i32 0, i32 2
  %232 = load %class.btVector3*, %class.btVector3** %pb.addr, align 4, !tbaa !2
  %233 = bitcast %class.btVector3* %232 to i8*
  %234 = bitcast %class.btVector3* %m_pointInWorld173 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %233, i8* align 4 %234, i32 16, i1 false), !tbaa.struct !12
  %235 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %236 = bitcast %class.btVector3* %235 to i8*
  %237 = bitcast %class.btVector3* %minNorm to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %236, i8* align 4 %237, i32 16, i1 false), !tbaa.struct !12
  br label %if.end174

if.end174:                                        ; preds = %if.then170, %if.end155
  %m_hasResult175 = getelementptr inbounds %struct.btIntermediateResult, %struct.btIntermediateResult* %res, i32 0, i32 4
  %238 = load i8, i8* %m_hasResult175, align 4, !tbaa !24, !range !16
  %tobool176 = trunc i8 %238 to i1
  store i1 %tobool176, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  %239 = bitcast float* %penetration_relaxation to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %239) #6
  %240 = bitcast float* %correctedMinNorm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %240) #6
  %call177 = call %struct.btIntermediateResult* bitcast (%"struct.btDiscreteCollisionDetectorInterface::Result"* (%"struct.btDiscreteCollisionDetectorInterface::Result"*)* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev to %struct.btIntermediateResult* (%struct.btIntermediateResult*)*)(%struct.btIntermediateResult* %res) #6
  %241 = bitcast %struct.btIntermediateResult* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 44, i8* %241) #6
  %242 = bitcast %class.btTransform* %displacedTrans to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %242) #6
  %243 = bitcast %class.btVector3* %newOrg to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %243) #6
  %244 = bitcast %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input to i8*
  call void @llvm.lifetime.end.p0i8(i64 132, i8* %244) #6
  %245 = bitcast %class.btVector3* %offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %245) #6
  %246 = bitcast float* %offsetDist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %246) #6
  %call178 = call %class.btGjkPairDetector* bitcast (%struct.btDiscreteCollisionDetectorInterface* (%struct.btDiscreteCollisionDetectorInterface*)* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev to %class.btGjkPairDetector* (%class.btGjkPairDetector*)*)(%class.btGjkPairDetector* %gjkdet) #6
  %247 = bitcast %class.btGjkPairDetector* %gjkdet to i8*
  call void @llvm.lifetime.end.p0i8(i64 80, i8* %247) #6
  %248 = bitcast float* %extraSeparation to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %248) #6
  br label %cleanup

cleanup:                                          ; preds = %if.end174, %if.then154
  %249 = bitcast i32* %numSampleDirections to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %249) #6
  %250 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %250) #6
  %251 = bitcast [62 x %class.btVector3]* %seperatingAxisInBBatch to i8*
  call void @llvm.lifetime.end.p0i8(i64 992, i8* %251) #6
  %252 = bitcast [62 x %class.btVector3]* %seperatingAxisInABatch to i8*
  call void @llvm.lifetime.end.p0i8(i64 992, i8* %252) #6
  %253 = bitcast [62 x %class.btVector3]* %supportVerticesBBatch to i8*
  call void @llvm.lifetime.end.p0i8(i64 992, i8* %253) #6
  %254 = bitcast [62 x %class.btVector3]* %supportVerticesABatch to i8*
  call void @llvm.lifetime.end.p0i8(i64 992, i8* %254) #6
  %255 = bitcast %class.btVector3* %w to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %255) #6
  %256 = bitcast %class.btVector3* %qWorld to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %256) #6
  %257 = bitcast %class.btVector3* %pWorld to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %257) #6
  %258 = bitcast %class.btVector3* %qInB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %258) #6
  %259 = bitcast %class.btVector3* %pInA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %259) #6
  %260 = bitcast %class.btVector3* %seperatingAxisInB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %260) #6
  %261 = bitcast %class.btVector3* %seperatingAxisInA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %261) #6
  %262 = bitcast %class.btVector3* %minB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %262) #6
  %263 = bitcast %class.btVector3* %minA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %263) #6
  %264 = bitcast %class.btVector3* %minNorm to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %264) #6
  %265 = bitcast float* %minProj to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %265) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %check2d) #6
  %266 = load i1, i1* %retval, align 1
  ret i1 %266
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_ZNK16btCollisionShape10isConvex2dEv(%class.btCollisionShape* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %call = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %this1)
  %call2 = call zeroext i1 @_ZN17btBroadphaseProxy10isConvex2dEi(i32 %call)
  ret i1 %call2
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !8
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !8
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !8
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !8
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

define hidden %class.btVector3* @_ZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEv() #0 {
entry:
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp24 = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp35 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp39 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  %ref.tmp41 = alloca float, align 4
  %ref.tmp43 = alloca float, align 4
  %ref.tmp44 = alloca float, align 4
  %ref.tmp45 = alloca float, align 4
  %ref.tmp47 = alloca float, align 4
  %ref.tmp48 = alloca float, align 4
  %ref.tmp49 = alloca float, align 4
  %ref.tmp51 = alloca float, align 4
  %ref.tmp52 = alloca float, align 4
  %ref.tmp53 = alloca float, align 4
  %ref.tmp55 = alloca float, align 4
  %ref.tmp56 = alloca float, align 4
  %ref.tmp57 = alloca float, align 4
  %ref.tmp59 = alloca float, align 4
  %ref.tmp60 = alloca float, align 4
  %ref.tmp61 = alloca float, align 4
  %ref.tmp63 = alloca float, align 4
  %ref.tmp64 = alloca float, align 4
  %ref.tmp65 = alloca float, align 4
  %ref.tmp67 = alloca float, align 4
  %ref.tmp68 = alloca float, align 4
  %ref.tmp69 = alloca float, align 4
  %ref.tmp71 = alloca float, align 4
  %ref.tmp72 = alloca float, align 4
  %ref.tmp73 = alloca float, align 4
  %ref.tmp75 = alloca float, align 4
  %ref.tmp76 = alloca float, align 4
  %ref.tmp77 = alloca float, align 4
  %ref.tmp79 = alloca float, align 4
  %ref.tmp80 = alloca float, align 4
  %ref.tmp81 = alloca float, align 4
  %ref.tmp83 = alloca float, align 4
  %ref.tmp84 = alloca float, align 4
  %ref.tmp85 = alloca float, align 4
  %ref.tmp87 = alloca float, align 4
  %ref.tmp88 = alloca float, align 4
  %ref.tmp89 = alloca float, align 4
  %ref.tmp91 = alloca float, align 4
  %ref.tmp92 = alloca float, align 4
  %ref.tmp93 = alloca float, align 4
  %ref.tmp95 = alloca float, align 4
  %ref.tmp96 = alloca float, align 4
  %ref.tmp97 = alloca float, align 4
  %ref.tmp99 = alloca float, align 4
  %ref.tmp100 = alloca float, align 4
  %ref.tmp101 = alloca float, align 4
  %ref.tmp103 = alloca float, align 4
  %ref.tmp104 = alloca float, align 4
  %ref.tmp105 = alloca float, align 4
  %ref.tmp107 = alloca float, align 4
  %ref.tmp108 = alloca float, align 4
  %ref.tmp109 = alloca float, align 4
  %ref.tmp111 = alloca float, align 4
  %ref.tmp112 = alloca float, align 4
  %ref.tmp113 = alloca float, align 4
  %ref.tmp115 = alloca float, align 4
  %ref.tmp116 = alloca float, align 4
  %ref.tmp117 = alloca float, align 4
  %ref.tmp119 = alloca float, align 4
  %ref.tmp120 = alloca float, align 4
  %ref.tmp121 = alloca float, align 4
  %ref.tmp123 = alloca float, align 4
  %ref.tmp124 = alloca float, align 4
  %ref.tmp125 = alloca float, align 4
  %ref.tmp127 = alloca float, align 4
  %ref.tmp128 = alloca float, align 4
  %ref.tmp129 = alloca float, align 4
  %ref.tmp131 = alloca float, align 4
  %ref.tmp132 = alloca float, align 4
  %ref.tmp133 = alloca float, align 4
  %ref.tmp135 = alloca float, align 4
  %ref.tmp136 = alloca float, align 4
  %ref.tmp137 = alloca float, align 4
  %ref.tmp139 = alloca float, align 4
  %ref.tmp140 = alloca float, align 4
  %ref.tmp141 = alloca float, align 4
  %ref.tmp143 = alloca float, align 4
  %ref.tmp144 = alloca float, align 4
  %ref.tmp145 = alloca float, align 4
  %ref.tmp147 = alloca float, align 4
  %ref.tmp148 = alloca float, align 4
  %ref.tmp149 = alloca float, align 4
  %ref.tmp151 = alloca float, align 4
  %ref.tmp152 = alloca float, align 4
  %ref.tmp153 = alloca float, align 4
  %ref.tmp155 = alloca float, align 4
  %ref.tmp156 = alloca float, align 4
  %ref.tmp157 = alloca float, align 4
  %ref.tmp159 = alloca float, align 4
  %ref.tmp160 = alloca float, align 4
  %ref.tmp161 = alloca float, align 4
  %ref.tmp163 = alloca float, align 4
  %ref.tmp164 = alloca float, align 4
  %ref.tmp165 = alloca float, align 4
  %0 = load atomic i8, i8* bitcast (i32* @_ZGVZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections to i8*) acquire, align 4
  %1 = and i8 %0, 1
  %guard.uninitialized = icmp eq i8 %1, 0
  br i1 %guard.uninitialized, label %init.check, label %init.end, !prof !25

init.check:                                       ; preds = %entry
  %2 = call i32 @__cxa_guard_acquire(i32* @_ZGVZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections) #6
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %init, label %init.end

init:                                             ; preds = %init.check
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !8
  %4 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  store float -0.000000e+00, float* %ref.tmp1, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  store float -1.000000e+00, float* %ref.tmp2, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 0), float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  %6 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  store float 0x3FE727CC00000000, float* %ref.tmp3, align 4, !tbaa !8
  %7 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  store float 0xBFE0D2BD40000000, float* %ref.tmp4, align 4, !tbaa !8
  %8 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  store float 0xBFDC9F3C80000000, float* %ref.tmp5, align 4, !tbaa !8
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 1), float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  store float 0xBFD1B05740000000, float* %ref.tmp7, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  store float 0xBFEB388440000000, float* %ref.tmp8, align 4, !tbaa !8
  %11 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #6
  store float 0xBFDC9F3C80000000, float* %ref.tmp9, align 4, !tbaa !8
  %call10 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 2), float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %12 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  store float 0xBFEC9F2340000000, float* %ref.tmp11, align 4, !tbaa !8
  %13 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  store float -0.000000e+00, float* %ref.tmp12, align 4, !tbaa !8
  %14 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  store float 0xBFDC9F2FE0000000, float* %ref.tmp13, align 4, !tbaa !8
  %call14 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 3), float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp13)
  %15 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #6
  store float 0xBFD1B05740000000, float* %ref.tmp15, align 4, !tbaa !8
  %16 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  store float 0x3FEB388440000000, float* %ref.tmp16, align 4, !tbaa !8
  %17 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #6
  store float 0xBFDC9F40A0000000, float* %ref.tmp17, align 4, !tbaa !8
  %call18 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 4), float* nonnull align 4 dereferenceable(4) %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  %18 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #6
  store float 0x3FE727CC00000000, float* %ref.tmp19, align 4, !tbaa !8
  %19 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #6
  store float 0x3FE0D2BD40000000, float* %ref.tmp20, align 4, !tbaa !8
  %20 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #6
  store float 0xBFDC9F3C80000000, float* %ref.tmp21, align 4, !tbaa !8
  %call22 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 5), float* nonnull align 4 dereferenceable(4) %ref.tmp19, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %21 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #6
  store float 0x3FD1B05740000000, float* %ref.tmp23, align 4, !tbaa !8
  %22 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #6
  store float 0xBFEB388440000000, float* %ref.tmp24, align 4, !tbaa !8
  %23 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #6
  store float 0x3FDC9F40A0000000, float* %ref.tmp25, align 4, !tbaa !8
  %call26 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 6), float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp24, float* nonnull align 4 dereferenceable(4) %ref.tmp25)
  %24 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #6
  store float 0xBFE727CC00000000, float* %ref.tmp27, align 4, !tbaa !8
  %25 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #6
  store float 0xBFE0D2BD40000000, float* %ref.tmp28, align 4, !tbaa !8
  %26 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #6
  store float 0x3FDC9F3C80000000, float* %ref.tmp29, align 4, !tbaa !8
  %call30 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 7), float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp29)
  %27 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #6
  store float 0xBFE727CC00000000, float* %ref.tmp31, align 4, !tbaa !8
  %28 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #6
  store float 0x3FE0D2BD40000000, float* %ref.tmp32, align 4, !tbaa !8
  %29 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #6
  store float 0x3FDC9F3C80000000, float* %ref.tmp33, align 4, !tbaa !8
  %call34 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 8), float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp32, float* nonnull align 4 dereferenceable(4) %ref.tmp33)
  %30 = bitcast float* %ref.tmp35 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #6
  store float 0x3FD1B05740000000, float* %ref.tmp35, align 4, !tbaa !8
  %31 = bitcast float* %ref.tmp36 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #6
  store float 0x3FEB388440000000, float* %ref.tmp36, align 4, !tbaa !8
  %32 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #6
  store float 0x3FDC9F3C80000000, float* %ref.tmp37, align 4, !tbaa !8
  %call38 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 9), float* nonnull align 4 dereferenceable(4) %ref.tmp35, float* nonnull align 4 dereferenceable(4) %ref.tmp36, float* nonnull align 4 dereferenceable(4) %ref.tmp37)
  %33 = bitcast float* %ref.tmp39 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #6
  store float 0x3FEC9F2340000000, float* %ref.tmp39, align 4, !tbaa !8
  %34 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #6
  store float 0.000000e+00, float* %ref.tmp40, align 4, !tbaa !8
  %35 = bitcast float* %ref.tmp41 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #6
  store float 0x3FDC9F2FE0000000, float* %ref.tmp41, align 4, !tbaa !8
  %call42 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 10), float* nonnull align 4 dereferenceable(4) %ref.tmp39, float* nonnull align 4 dereferenceable(4) %ref.tmp40, float* nonnull align 4 dereferenceable(4) %ref.tmp41)
  %36 = bitcast float* %ref.tmp43 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #6
  store float -0.000000e+00, float* %ref.tmp43, align 4, !tbaa !8
  %37 = bitcast float* %ref.tmp44 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #6
  store float 0.000000e+00, float* %ref.tmp44, align 4, !tbaa !8
  %38 = bitcast float* %ref.tmp45 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #6
  store float 1.000000e+00, float* %ref.tmp45, align 4, !tbaa !8
  %call46 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 11), float* nonnull align 4 dereferenceable(4) %ref.tmp43, float* nonnull align 4 dereferenceable(4) %ref.tmp44, float* nonnull align 4 dereferenceable(4) %ref.tmp45)
  %39 = bitcast float* %ref.tmp47 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #6
  store float 0x3FDB387E00000000, float* %ref.tmp47, align 4, !tbaa !8
  %40 = bitcast float* %ref.tmp48 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #6
  store float 0xBFD3C6D620000000, float* %ref.tmp48, align 4, !tbaa !8
  %41 = bitcast float* %ref.tmp49 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #6
  store float 0xBFEB388EC0000000, float* %ref.tmp49, align 4, !tbaa !8
  %call50 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 12), float* nonnull align 4 dereferenceable(4) %ref.tmp47, float* nonnull align 4 dereferenceable(4) %ref.tmp48, float* nonnull align 4 dereferenceable(4) %ref.tmp49)
  %42 = bitcast float* %ref.tmp51 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #6
  store float 0xBFC4CB5BC0000000, float* %ref.tmp51, align 4, !tbaa !8
  %43 = bitcast float* %ref.tmp52 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #6
  store float 0xBFDFFFEB00000000, float* %ref.tmp52, align 4, !tbaa !8
  %44 = bitcast float* %ref.tmp53 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #6
  store float 0xBFEB388EC0000000, float* %ref.tmp53, align 4, !tbaa !8
  %call54 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 13), float* nonnull align 4 dereferenceable(4) %ref.tmp51, float* nonnull align 4 dereferenceable(4) %ref.tmp52, float* nonnull align 4 dereferenceable(4) %ref.tmp53)
  %45 = bitcast float* %ref.tmp55 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #6
  store float 0x3FD0D2D880000000, float* %ref.tmp55, align 4, !tbaa !8
  %46 = bitcast float* %ref.tmp56 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #6
  store float 0xBFE9E36D20000000, float* %ref.tmp56, align 4, !tbaa !8
  %47 = bitcast float* %ref.tmp57 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #6
  store float 0xBFE0D2D880000000, float* %ref.tmp57, align 4, !tbaa !8
  %call58 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 14), float* nonnull align 4 dereferenceable(4) %ref.tmp55, float* nonnull align 4 dereferenceable(4) %ref.tmp56, float* nonnull align 4 dereferenceable(4) %ref.tmp57)
  %48 = bitcast float* %ref.tmp59 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #6
  store float 0x3FDB387E00000000, float* %ref.tmp59, align 4, !tbaa !8
  %49 = bitcast float* %ref.tmp60 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #6
  store float 0x3FD3C6D620000000, float* %ref.tmp60, align 4, !tbaa !8
  %50 = bitcast float* %ref.tmp61 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #6
  store float 0xBFEB388EC0000000, float* %ref.tmp61, align 4, !tbaa !8
  %call62 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 15), float* nonnull align 4 dereferenceable(4) %ref.tmp59, float* nonnull align 4 dereferenceable(4) %ref.tmp60, float* nonnull align 4 dereferenceable(4) %ref.tmp61)
  %51 = bitcast float* %ref.tmp63 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #6
  store float 0x3FEB388220000000, float* %ref.tmp63, align 4, !tbaa !8
  %52 = bitcast float* %ref.tmp64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #6
  store float -0.000000e+00, float* %ref.tmp64, align 4, !tbaa !8
  %53 = bitcast float* %ref.tmp65 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #6
  store float 0xBFE0D2D440000000, float* %ref.tmp65, align 4, !tbaa !8
  %call66 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 16), float* nonnull align 4 dereferenceable(4) %ref.tmp63, float* nonnull align 4 dereferenceable(4) %ref.tmp64, float* nonnull align 4 dereferenceable(4) %ref.tmp65)
  %54 = bitcast float* %ref.tmp67 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #6
  store float 0xBFE0D2C7C0000000, float* %ref.tmp67, align 4, !tbaa !8
  %55 = bitcast float* %ref.tmp68 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #6
  store float -0.000000e+00, float* %ref.tmp68, align 4, !tbaa !8
  %56 = bitcast float* %ref.tmp69 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #6
  store float 0xBFEB388A80000000, float* %ref.tmp69, align 4, !tbaa !8
  %call70 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 17), float* nonnull align 4 dereferenceable(4) %ref.tmp67, float* nonnull align 4 dereferenceable(4) %ref.tmp68, float* nonnull align 4 dereferenceable(4) %ref.tmp69)
  %57 = bitcast float* %ref.tmp71 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %57) #6
  store float 0xBFE605A700000000, float* %ref.tmp71, align 4, !tbaa !8
  %58 = bitcast float* %ref.tmp72 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #6
  store float 0xBFDFFFF360000000, float* %ref.tmp72, align 4, !tbaa !8
  %59 = bitcast float* %ref.tmp73 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #6
  store float 0xBFE0D2D440000000, float* %ref.tmp73, align 4, !tbaa !8
  %call74 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 18), float* nonnull align 4 dereferenceable(4) %ref.tmp71, float* nonnull align 4 dereferenceable(4) %ref.tmp72, float* nonnull align 4 dereferenceable(4) %ref.tmp73)
  %60 = bitcast float* %ref.tmp75 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #6
  store float 0xBFC4CB5BC0000000, float* %ref.tmp75, align 4, !tbaa !8
  %61 = bitcast float* %ref.tmp76 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %61) #6
  store float 0x3FDFFFEB00000000, float* %ref.tmp76, align 4, !tbaa !8
  %62 = bitcast float* %ref.tmp77 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #6
  store float 0xBFEB388EC0000000, float* %ref.tmp77, align 4, !tbaa !8
  %call78 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 19), float* nonnull align 4 dereferenceable(4) %ref.tmp75, float* nonnull align 4 dereferenceable(4) %ref.tmp76, float* nonnull align 4 dereferenceable(4) %ref.tmp77)
  %63 = bitcast float* %ref.tmp79 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #6
  store float 0xBFE605A700000000, float* %ref.tmp79, align 4, !tbaa !8
  %64 = bitcast float* %ref.tmp80 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #6
  store float 0x3FDFFFF360000000, float* %ref.tmp80, align 4, !tbaa !8
  %65 = bitcast float* %ref.tmp81 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %65) #6
  store float 0xBFE0D2D440000000, float* %ref.tmp81, align 4, !tbaa !8
  %call82 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 20), float* nonnull align 4 dereferenceable(4) %ref.tmp79, float* nonnull align 4 dereferenceable(4) %ref.tmp80, float* nonnull align 4 dereferenceable(4) %ref.tmp81)
  %66 = bitcast float* %ref.tmp83 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #6
  store float 0x3FD0D2D880000000, float* %ref.tmp83, align 4, !tbaa !8
  %67 = bitcast float* %ref.tmp84 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #6
  store float 0x3FE9E36D20000000, float* %ref.tmp84, align 4, !tbaa !8
  %68 = bitcast float* %ref.tmp85 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #6
  store float 0xBFE0D2D880000000, float* %ref.tmp85, align 4, !tbaa !8
  %call86 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 21), float* nonnull align 4 dereferenceable(4) %ref.tmp83, float* nonnull align 4 dereferenceable(4) %ref.tmp84, float* nonnull align 4 dereferenceable(4) %ref.tmp85)
  %69 = bitcast float* %ref.tmp87 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #6
  store float 0x3FEE6F1120000000, float* %ref.tmp87, align 4, !tbaa !8
  %70 = bitcast float* %ref.tmp88 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #6
  store float 0x3FD3C6DE80000000, float* %ref.tmp88, align 4, !tbaa !8
  %71 = bitcast float* %ref.tmp89 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #6
  store float 0.000000e+00, float* %ref.tmp89, align 4, !tbaa !8
  %call90 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 22), float* nonnull align 4 dereferenceable(4) %ref.tmp87, float* nonnull align 4 dereferenceable(4) %ref.tmp88, float* nonnull align 4 dereferenceable(4) %ref.tmp89)
  %72 = bitcast float* %ref.tmp91 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %72) #6
  store float 0x3FEE6F1120000000, float* %ref.tmp91, align 4, !tbaa !8
  %73 = bitcast float* %ref.tmp92 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #6
  store float 0xBFD3C6DE80000000, float* %ref.tmp92, align 4, !tbaa !8
  %74 = bitcast float* %ref.tmp93 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %74) #6
  store float 0.000000e+00, float* %ref.tmp93, align 4, !tbaa !8
  %call94 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 23), float* nonnull align 4 dereferenceable(4) %ref.tmp91, float* nonnull align 4 dereferenceable(4) %ref.tmp92, float* nonnull align 4 dereferenceable(4) %ref.tmp93)
  %75 = bitcast float* %ref.tmp95 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #6
  store float 0x3FE2CF24A0000000, float* %ref.tmp95, align 4, !tbaa !8
  %76 = bitcast float* %ref.tmp96 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #6
  store float 0xBFE9E377A0000000, float* %ref.tmp96, align 4, !tbaa !8
  %77 = bitcast float* %ref.tmp97 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %77) #6
  store float 0.000000e+00, float* %ref.tmp97, align 4, !tbaa !8
  %call98 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 24), float* nonnull align 4 dereferenceable(4) %ref.tmp95, float* nonnull align 4 dereferenceable(4) %ref.tmp96, float* nonnull align 4 dereferenceable(4) %ref.tmp97)
  %78 = bitcast float* %ref.tmp99 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %78) #6
  store float 0.000000e+00, float* %ref.tmp99, align 4, !tbaa !8
  %79 = bitcast float* %ref.tmp100 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #6
  store float -1.000000e+00, float* %ref.tmp100, align 4, !tbaa !8
  %80 = bitcast float* %ref.tmp101 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #6
  store float 0.000000e+00, float* %ref.tmp101, align 4, !tbaa !8
  %call102 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 25), float* nonnull align 4 dereferenceable(4) %ref.tmp99, float* nonnull align 4 dereferenceable(4) %ref.tmp100, float* nonnull align 4 dereferenceable(4) %ref.tmp101)
  %81 = bitcast float* %ref.tmp103 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %81) #6
  store float 0xBFE2CF24A0000000, float* %ref.tmp103, align 4, !tbaa !8
  %82 = bitcast float* %ref.tmp104 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #6
  store float 0xBFE9E377A0000000, float* %ref.tmp104, align 4, !tbaa !8
  %83 = bitcast float* %ref.tmp105 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %83) #6
  store float 0.000000e+00, float* %ref.tmp105, align 4, !tbaa !8
  %call106 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 26), float* nonnull align 4 dereferenceable(4) %ref.tmp103, float* nonnull align 4 dereferenceable(4) %ref.tmp104, float* nonnull align 4 dereferenceable(4) %ref.tmp105)
  %84 = bitcast float* %ref.tmp107 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %84) #6
  store float 0xBFEE6F1120000000, float* %ref.tmp107, align 4, !tbaa !8
  %85 = bitcast float* %ref.tmp108 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %85) #6
  store float 0xBFD3C6DE80000000, float* %ref.tmp108, align 4, !tbaa !8
  %86 = bitcast float* %ref.tmp109 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %86) #6
  store float -0.000000e+00, float* %ref.tmp109, align 4, !tbaa !8
  %call110 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 27), float* nonnull align 4 dereferenceable(4) %ref.tmp107, float* nonnull align 4 dereferenceable(4) %ref.tmp108, float* nonnull align 4 dereferenceable(4) %ref.tmp109)
  %87 = bitcast float* %ref.tmp111 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %87) #6
  store float 0xBFEE6F1120000000, float* %ref.tmp111, align 4, !tbaa !8
  %88 = bitcast float* %ref.tmp112 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #6
  store float 0x3FD3C6DE80000000, float* %ref.tmp112, align 4, !tbaa !8
  %89 = bitcast float* %ref.tmp113 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %89) #6
  store float -0.000000e+00, float* %ref.tmp113, align 4, !tbaa !8
  %call114 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 28), float* nonnull align 4 dereferenceable(4) %ref.tmp111, float* nonnull align 4 dereferenceable(4) %ref.tmp112, float* nonnull align 4 dereferenceable(4) %ref.tmp113)
  %90 = bitcast float* %ref.tmp115 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %90) #6
  store float 0xBFE2CF24A0000000, float* %ref.tmp115, align 4, !tbaa !8
  %91 = bitcast float* %ref.tmp116 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %91) #6
  store float 0x3FE9E377A0000000, float* %ref.tmp116, align 4, !tbaa !8
  %92 = bitcast float* %ref.tmp117 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %92) #6
  store float -0.000000e+00, float* %ref.tmp117, align 4, !tbaa !8
  %call118 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 29), float* nonnull align 4 dereferenceable(4) %ref.tmp115, float* nonnull align 4 dereferenceable(4) %ref.tmp116, float* nonnull align 4 dereferenceable(4) %ref.tmp117)
  %93 = bitcast float* %ref.tmp119 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %93) #6
  store float -0.000000e+00, float* %ref.tmp119, align 4, !tbaa !8
  %94 = bitcast float* %ref.tmp120 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %94) #6
  store float 1.000000e+00, float* %ref.tmp120, align 4, !tbaa !8
  %95 = bitcast float* %ref.tmp121 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %95) #6
  store float -0.000000e+00, float* %ref.tmp121, align 4, !tbaa !8
  %call122 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 30), float* nonnull align 4 dereferenceable(4) %ref.tmp119, float* nonnull align 4 dereferenceable(4) %ref.tmp120, float* nonnull align 4 dereferenceable(4) %ref.tmp121)
  %96 = bitcast float* %ref.tmp123 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %96) #6
  store float 0x3FE2CF24A0000000, float* %ref.tmp123, align 4, !tbaa !8
  %97 = bitcast float* %ref.tmp124 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %97) #6
  store float 0x3FE9E377A0000000, float* %ref.tmp124, align 4, !tbaa !8
  %98 = bitcast float* %ref.tmp125 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %98) #6
  store float -0.000000e+00, float* %ref.tmp125, align 4, !tbaa !8
  %call126 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 31), float* nonnull align 4 dereferenceable(4) %ref.tmp123, float* nonnull align 4 dereferenceable(4) %ref.tmp124, float* nonnull align 4 dereferenceable(4) %ref.tmp125)
  %99 = bitcast float* %ref.tmp127 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %99) #6
  store float 0x3FE605A700000000, float* %ref.tmp127, align 4, !tbaa !8
  %100 = bitcast float* %ref.tmp128 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %100) #6
  store float 0xBFDFFFF360000000, float* %ref.tmp128, align 4, !tbaa !8
  %101 = bitcast float* %ref.tmp129 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %101) #6
  store float 0x3FE0D2D440000000, float* %ref.tmp129, align 4, !tbaa !8
  %call130 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 32), float* nonnull align 4 dereferenceable(4) %ref.tmp127, float* nonnull align 4 dereferenceable(4) %ref.tmp128, float* nonnull align 4 dereferenceable(4) %ref.tmp129)
  %102 = bitcast float* %ref.tmp131 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %102) #6
  store float 0xBFD0D2D880000000, float* %ref.tmp131, align 4, !tbaa !8
  %103 = bitcast float* %ref.tmp132 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %103) #6
  store float 0xBFE9E36D20000000, float* %ref.tmp132, align 4, !tbaa !8
  %104 = bitcast float* %ref.tmp133 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %104) #6
  store float 0x3FE0D2D880000000, float* %ref.tmp133, align 4, !tbaa !8
  %call134 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 33), float* nonnull align 4 dereferenceable(4) %ref.tmp131, float* nonnull align 4 dereferenceable(4) %ref.tmp132, float* nonnull align 4 dereferenceable(4) %ref.tmp133)
  %105 = bitcast float* %ref.tmp135 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %105) #6
  store float 0xBFEB388220000000, float* %ref.tmp135, align 4, !tbaa !8
  %106 = bitcast float* %ref.tmp136 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %106) #6
  store float 0.000000e+00, float* %ref.tmp136, align 4, !tbaa !8
  %107 = bitcast float* %ref.tmp137 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %107) #6
  store float 0x3FE0D2D440000000, float* %ref.tmp137, align 4, !tbaa !8
  %call138 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 34), float* nonnull align 4 dereferenceable(4) %ref.tmp135, float* nonnull align 4 dereferenceable(4) %ref.tmp136, float* nonnull align 4 dereferenceable(4) %ref.tmp137)
  %108 = bitcast float* %ref.tmp139 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %108) #6
  store float 0xBFD0D2D880000000, float* %ref.tmp139, align 4, !tbaa !8
  %109 = bitcast float* %ref.tmp140 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %109) #6
  store float 0x3FE9E36D20000000, float* %ref.tmp140, align 4, !tbaa !8
  %110 = bitcast float* %ref.tmp141 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %110) #6
  store float 0x3FE0D2D880000000, float* %ref.tmp141, align 4, !tbaa !8
  %call142 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 35), float* nonnull align 4 dereferenceable(4) %ref.tmp139, float* nonnull align 4 dereferenceable(4) %ref.tmp140, float* nonnull align 4 dereferenceable(4) %ref.tmp141)
  %111 = bitcast float* %ref.tmp143 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %111) #6
  store float 0x3FE605A700000000, float* %ref.tmp143, align 4, !tbaa !8
  %112 = bitcast float* %ref.tmp144 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %112) #6
  store float 0x3FDFFFF360000000, float* %ref.tmp144, align 4, !tbaa !8
  %113 = bitcast float* %ref.tmp145 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %113) #6
  store float 0x3FE0D2D440000000, float* %ref.tmp145, align 4, !tbaa !8
  %call146 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 36), float* nonnull align 4 dereferenceable(4) %ref.tmp143, float* nonnull align 4 dereferenceable(4) %ref.tmp144, float* nonnull align 4 dereferenceable(4) %ref.tmp145)
  %114 = bitcast float* %ref.tmp147 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %114) #6
  store float 0x3FE0D2C7C0000000, float* %ref.tmp147, align 4, !tbaa !8
  %115 = bitcast float* %ref.tmp148 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %115) #6
  store float 0.000000e+00, float* %ref.tmp148, align 4, !tbaa !8
  %116 = bitcast float* %ref.tmp149 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %116) #6
  store float 0x3FEB388A80000000, float* %ref.tmp149, align 4, !tbaa !8
  %call150 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 37), float* nonnull align 4 dereferenceable(4) %ref.tmp147, float* nonnull align 4 dereferenceable(4) %ref.tmp148, float* nonnull align 4 dereferenceable(4) %ref.tmp149)
  %117 = bitcast float* %ref.tmp151 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %117) #6
  store float 0x3FC4CB5BC0000000, float* %ref.tmp151, align 4, !tbaa !8
  %118 = bitcast float* %ref.tmp152 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %118) #6
  store float 0xBFDFFFEB00000000, float* %ref.tmp152, align 4, !tbaa !8
  %119 = bitcast float* %ref.tmp153 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %119) #6
  store float 0x3FEB388EC0000000, float* %ref.tmp153, align 4, !tbaa !8
  %call154 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 38), float* nonnull align 4 dereferenceable(4) %ref.tmp151, float* nonnull align 4 dereferenceable(4) %ref.tmp152, float* nonnull align 4 dereferenceable(4) %ref.tmp153)
  %120 = bitcast float* %ref.tmp155 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %120) #6
  store float 0xBFDB387E00000000, float* %ref.tmp155, align 4, !tbaa !8
  %121 = bitcast float* %ref.tmp156 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %121) #6
  store float 0xBFD3C6D620000000, float* %ref.tmp156, align 4, !tbaa !8
  %122 = bitcast float* %ref.tmp157 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %122) #6
  store float 0x3FEB388EC0000000, float* %ref.tmp157, align 4, !tbaa !8
  %call158 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 39), float* nonnull align 4 dereferenceable(4) %ref.tmp155, float* nonnull align 4 dereferenceable(4) %ref.tmp156, float* nonnull align 4 dereferenceable(4) %ref.tmp157)
  %123 = bitcast float* %ref.tmp159 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %123) #6
  store float 0xBFDB387E00000000, float* %ref.tmp159, align 4, !tbaa !8
  %124 = bitcast float* %ref.tmp160 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %124) #6
  store float 0x3FD3C6D620000000, float* %ref.tmp160, align 4, !tbaa !8
  %125 = bitcast float* %ref.tmp161 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %125) #6
  store float 0x3FEB388EC0000000, float* %ref.tmp161, align 4, !tbaa !8
  %call162 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 40), float* nonnull align 4 dereferenceable(4) %ref.tmp159, float* nonnull align 4 dereferenceable(4) %ref.tmp160, float* nonnull align 4 dereferenceable(4) %ref.tmp161)
  %126 = bitcast float* %ref.tmp163 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %126) #6
  store float 0x3FC4CB5BC0000000, float* %ref.tmp163, align 4, !tbaa !8
  %127 = bitcast float* %ref.tmp164 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %127) #6
  store float 0x3FDFFFEB00000000, float* %ref.tmp164, align 4, !tbaa !8
  %128 = bitcast float* %ref.tmp165 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %128) #6
  store float 0x3FEB388EC0000000, float* %ref.tmp165, align 4, !tbaa !8
  %call166 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 41), float* nonnull align 4 dereferenceable(4) %ref.tmp163, float* nonnull align 4 dereferenceable(4) %ref.tmp164, float* nonnull align 4 dereferenceable(4) %ref.tmp165)
  br label %arrayinit.body

arrayinit.body:                                   ; preds = %arrayinit.body, %init
  %arrayinit.cur = phi %class.btVector3* [ getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 42), %init ], [ %arrayinit.next, %arrayinit.body ]
  %call167 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayinit.cur)
  %arrayinit.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.cur, i32 1
  %arrayinit.done = icmp eq %class.btVector3* %arrayinit.next, getelementptr inbounds (%class.btVector3, %class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 0), i32 62)
  br i1 %arrayinit.done, label %arrayinit.end, label %arrayinit.body

arrayinit.end:                                    ; preds = %arrayinit.body
  %129 = bitcast float* %ref.tmp165 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #6
  %130 = bitcast float* %ref.tmp164 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #6
  %131 = bitcast float* %ref.tmp163 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %131) #6
  %132 = bitcast float* %ref.tmp161 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #6
  %133 = bitcast float* %ref.tmp160 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %133) #6
  %134 = bitcast float* %ref.tmp159 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %134) #6
  %135 = bitcast float* %ref.tmp157 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %135) #6
  %136 = bitcast float* %ref.tmp156 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %136) #6
  %137 = bitcast float* %ref.tmp155 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %137) #6
  %138 = bitcast float* %ref.tmp153 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %138) #6
  %139 = bitcast float* %ref.tmp152 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #6
  %140 = bitcast float* %ref.tmp151 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #6
  %141 = bitcast float* %ref.tmp149 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #6
  %142 = bitcast float* %ref.tmp148 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #6
  %143 = bitcast float* %ref.tmp147 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #6
  %144 = bitcast float* %ref.tmp145 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #6
  %145 = bitcast float* %ref.tmp144 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #6
  %146 = bitcast float* %ref.tmp143 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #6
  %147 = bitcast float* %ref.tmp141 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #6
  %148 = bitcast float* %ref.tmp140 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #6
  %149 = bitcast float* %ref.tmp139 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #6
  %150 = bitcast float* %ref.tmp137 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #6
  %151 = bitcast float* %ref.tmp136 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #6
  %152 = bitcast float* %ref.tmp135 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %152) #6
  %153 = bitcast float* %ref.tmp133 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #6
  %154 = bitcast float* %ref.tmp132 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %154) #6
  %155 = bitcast float* %ref.tmp131 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %155) #6
  %156 = bitcast float* %ref.tmp129 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %156) #6
  %157 = bitcast float* %ref.tmp128 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #6
  %158 = bitcast float* %ref.tmp127 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %158) #6
  %159 = bitcast float* %ref.tmp125 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %159) #6
  %160 = bitcast float* %ref.tmp124 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %160) #6
  %161 = bitcast float* %ref.tmp123 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %161) #6
  %162 = bitcast float* %ref.tmp121 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %162) #6
  %163 = bitcast float* %ref.tmp120 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %163) #6
  %164 = bitcast float* %ref.tmp119 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #6
  %165 = bitcast float* %ref.tmp117 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %165) #6
  %166 = bitcast float* %ref.tmp116 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #6
  %167 = bitcast float* %ref.tmp115 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %167) #6
  %168 = bitcast float* %ref.tmp113 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %168) #6
  %169 = bitcast float* %ref.tmp112 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #6
  %170 = bitcast float* %ref.tmp111 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %170) #6
  %171 = bitcast float* %ref.tmp109 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #6
  %172 = bitcast float* %ref.tmp108 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %172) #6
  %173 = bitcast float* %ref.tmp107 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %173) #6
  %174 = bitcast float* %ref.tmp105 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %174) #6
  %175 = bitcast float* %ref.tmp104 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %175) #6
  %176 = bitcast float* %ref.tmp103 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %176) #6
  %177 = bitcast float* %ref.tmp101 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %177) #6
  %178 = bitcast float* %ref.tmp100 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %178) #6
  %179 = bitcast float* %ref.tmp99 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %179) #6
  %180 = bitcast float* %ref.tmp97 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %180) #6
  %181 = bitcast float* %ref.tmp96 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %181) #6
  %182 = bitcast float* %ref.tmp95 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %182) #6
  %183 = bitcast float* %ref.tmp93 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %183) #6
  %184 = bitcast float* %ref.tmp92 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %184) #6
  %185 = bitcast float* %ref.tmp91 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %185) #6
  %186 = bitcast float* %ref.tmp89 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %186) #6
  %187 = bitcast float* %ref.tmp88 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %187) #6
  %188 = bitcast float* %ref.tmp87 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %188) #6
  %189 = bitcast float* %ref.tmp85 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %189) #6
  %190 = bitcast float* %ref.tmp84 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %190) #6
  %191 = bitcast float* %ref.tmp83 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %191) #6
  %192 = bitcast float* %ref.tmp81 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %192) #6
  %193 = bitcast float* %ref.tmp80 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %193) #6
  %194 = bitcast float* %ref.tmp79 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %194) #6
  %195 = bitcast float* %ref.tmp77 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %195) #6
  %196 = bitcast float* %ref.tmp76 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %196) #6
  %197 = bitcast float* %ref.tmp75 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %197) #6
  %198 = bitcast float* %ref.tmp73 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %198) #6
  %199 = bitcast float* %ref.tmp72 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %199) #6
  %200 = bitcast float* %ref.tmp71 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %200) #6
  %201 = bitcast float* %ref.tmp69 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %201) #6
  %202 = bitcast float* %ref.tmp68 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %202) #6
  %203 = bitcast float* %ref.tmp67 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %203) #6
  %204 = bitcast float* %ref.tmp65 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %204) #6
  %205 = bitcast float* %ref.tmp64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %205) #6
  %206 = bitcast float* %ref.tmp63 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %206) #6
  %207 = bitcast float* %ref.tmp61 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %207) #6
  %208 = bitcast float* %ref.tmp60 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %208) #6
  %209 = bitcast float* %ref.tmp59 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %209) #6
  %210 = bitcast float* %ref.tmp57 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %210) #6
  %211 = bitcast float* %ref.tmp56 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %211) #6
  %212 = bitcast float* %ref.tmp55 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %212) #6
  %213 = bitcast float* %ref.tmp53 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %213) #6
  %214 = bitcast float* %ref.tmp52 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %214) #6
  %215 = bitcast float* %ref.tmp51 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %215) #6
  %216 = bitcast float* %ref.tmp49 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %216) #6
  %217 = bitcast float* %ref.tmp48 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %217) #6
  %218 = bitcast float* %ref.tmp47 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %218) #6
  %219 = bitcast float* %ref.tmp45 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %219) #6
  %220 = bitcast float* %ref.tmp44 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %220) #6
  %221 = bitcast float* %ref.tmp43 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %221) #6
  %222 = bitcast float* %ref.tmp41 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %222) #6
  %223 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %223) #6
  %224 = bitcast float* %ref.tmp39 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %224) #6
  %225 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %225) #6
  %226 = bitcast float* %ref.tmp36 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %226) #6
  %227 = bitcast float* %ref.tmp35 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %227) #6
  %228 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %228) #6
  %229 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %229) #6
  %230 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %230) #6
  %231 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %231) #6
  %232 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %232) #6
  %233 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %233) #6
  %234 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %234) #6
  %235 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %235) #6
  %236 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %236) #6
  %237 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %237) #6
  %238 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %238) #6
  %239 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %239) #6
  %240 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %240) #6
  %241 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %241) #6
  %242 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %242) #6
  %243 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %243) #6
  %244 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %244) #6
  %245 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %245) #6
  %246 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %246) #6
  %247 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %247) #6
  %248 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %248) #6
  %249 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %249) #6
  %250 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %250) #6
  %251 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %251) #6
  %252 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %252) #6
  %253 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %253) #6
  %254 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %254) #6
  call void @__cxa_guard_release(i32* @_ZGVZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections) #6
  br label %init.end

init.end:                                         ; preds = %arrayinit.end, %init.check, %entry
  ret %class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 0)
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call, float* %ref.tmp, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call2, float* %ref.tmp1, align 4, !tbaa !8
  %6 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call4, float* %ref.tmp3, align 4, !tbaa !8
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %9 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #6
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !8
  %fneg4 = fneg float %5
  store float %fneg4, float* %ref.tmp1, align 4, !tbaa !8
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %8 = load float, float* %arrayidx7, align 4, !tbaa !8
  %fneg8 = fneg float %8
  store float %fneg8, float* %ref.tmp5, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #6
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %1, i32 0)
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call1, float* %ref.tmp, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 1)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !8
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 2)
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !8
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #6
  %10 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #6
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !8
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !8
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !8
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !8
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !8
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !8
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !8
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !8
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !8
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !8
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !8
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !8
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !8
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  ret void
}

declare float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape*) #4

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !8
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !8
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !8
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !8
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !8
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !8
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !8
  %sub = fsub float %2, %1
  store float %sub, float* %arrayidx3, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !8
  %sub8 = fsub float %5, %4
  store float %sub8, float* %arrayidx7, align 4, !tbaa !8
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !8
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !8
  %sub13 = fsub float %8, %7
  store float %sub13, float* %arrayidx12, align 4, !tbaa !8
  ret %class.btVector3* %this1
}

declare %class.btGjkPairDetector* @_ZN17btGjkPairDetectorC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver(%class.btGjkPairDetector* returned, %class.btConvexShape*, %class.btConvexShape*, %class.btVoronoiSimplexSolver*, %class.btConvexPenetrationDepthSolver*) unnamed_addr #4

define linkonce_odr hidden %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* @_ZN36btDiscreteCollisionDetectorInterface17ClosestPointInputC2Ev(%"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %this.addr, align 4
  %m_transformA = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this1, i32 0, i32 0
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_transformA)
  %m_transformB = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this1, i32 0, i32 1
  %call2 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_transformB)
  %m_maximumDistanceSquared = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this1, i32 0, i32 2
  store float 0x43ABC16D60000000, float* %m_maximumDistanceSquared, align 4, !tbaa !17
  ret %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !8
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !8
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !8
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !8
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !8
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !12
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %origin) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %origin.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %origin, %class.btVector3** %origin.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %origin.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_origin to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !12
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !12
  ret %class.btTransform* %this1
}

define internal %struct.btIntermediateResult* @_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawEN20btIntermediateResultC2Ev(%struct.btIntermediateResult* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %struct.btIntermediateResult*, align 4
  store %struct.btIntermediateResult* %this, %struct.btIntermediateResult** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btIntermediateResult*, %struct.btIntermediateResult** %this.addr, align 4
  %0 = bitcast %struct.btIntermediateResult* %this1 to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  %call = call %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultC2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* %0) #6
  %1 = bitcast %struct.btIntermediateResult* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTVZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawE20btIntermediateResult, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !14
  %m_normalOnBInWorld = getelementptr inbounds %struct.btIntermediateResult, %struct.btIntermediateResult* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_normalOnBInWorld)
  %m_pointInWorld = getelementptr inbounds %struct.btIntermediateResult, %struct.btIntermediateResult* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_pointInWorld)
  %m_hasResult = getelementptr inbounds %struct.btIntermediateResult, %struct.btIntermediateResult* %this1, i32 0, i32 4
  store i8 0, i8* %m_hasResult, align 4, !tbaa !24
  ret %struct.btIntermediateResult* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN17btGjkPairDetector23setCachedSeperatingAxisERK9btVector3(%class.btGjkPairDetector* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %seperatingAxis) #5 comdat {
entry:
  %this.addr = alloca %class.btGjkPairDetector*, align 4
  %seperatingAxis.addr = alloca %class.btVector3*, align 4
  store %class.btGjkPairDetector* %this, %class.btGjkPairDetector** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %seperatingAxis, %class.btVector3** %seperatingAxis.addr, align 4, !tbaa !2
  %this1 = load %class.btGjkPairDetector*, %class.btGjkPairDetector** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %seperatingAxis.addr, align 4, !tbaa !2
  %m_cachedSeparatingAxis = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_cachedSeparatingAxis to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !12
  ret void
}

declare void @_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb(%class.btGjkPairDetector*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* nonnull align 4 dereferenceable(132), %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4), %class.btIDebugDraw*, i1 zeroext) unnamed_addr #4

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !8
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !8
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !8
  %6 = load float*, float** %s.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !8
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !8
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !8
  ret %class.btVector3* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev(%struct.btDiscreteCollisionDetectorInterface* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %struct.btDiscreteCollisionDetectorInterface*, align 4
  store %struct.btDiscreteCollisionDetectorInterface* %this, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btDiscreteCollisionDetectorInterface*, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  ret %struct.btDiscreteCollisionDetectorInterface* %this1
}

; Function Attrs: nounwind
declare i32 @__cxa_guard_acquire(i32*) #6

; Function Attrs: nounwind
declare void @__cxa_guard_release(i32*) #6

; Function Attrs: nounwind
define linkonce_odr hidden %class.btConvexPenetrationDepthSolver* @_ZN30btConvexPenetrationDepthSolverD2Ev(%class.btConvexPenetrationDepthSolver* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btConvexPenetrationDepthSolver*, align 4
  store %class.btConvexPenetrationDepthSolver* %this, %class.btConvexPenetrationDepthSolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %this.addr, align 4
  ret %class.btConvexPenetrationDepthSolver* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN33btMinkowskiPenetrationDepthSolverD0Ev(%class.btMinkowskiPenetrationDepthSolver* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btMinkowskiPenetrationDepthSolver*, align 4
  store %class.btMinkowskiPenetrationDepthSolver* %this, %class.btMinkowskiPenetrationDepthSolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMinkowskiPenetrationDepthSolver*, %class.btMinkowskiPenetrationDepthSolver** %this.addr, align 4
  %call = call %class.btMinkowskiPenetrationDepthSolver* bitcast (%class.btConvexPenetrationDepthSolver* (%class.btConvexPenetrationDepthSolver*)* @_ZN30btConvexPenetrationDepthSolverD2Ev to %class.btMinkowskiPenetrationDepthSolver* (%class.btMinkowskiPenetrationDepthSolver*)*)(%class.btMinkowskiPenetrationDepthSolver* %this1) #6
  %0 = bitcast %class.btMinkowskiPenetrationDepthSolver* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZN17btBroadphaseProxy10isConvex2dEi(i32 %proxyType) #3 comdat {
entry:
  %proxyType.addr = alloca i32, align 4
  store i32 %proxyType, i32* %proxyType.addr, align 4, !tbaa !10
  %0 = load i32, i32* %proxyType.addr, align 4, !tbaa !10
  %cmp = icmp eq i32 %0, 17
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %1 = load i32, i32* %proxyType.addr, align 4, !tbaa !10
  %cmp1 = icmp eq i32 %1, 18
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %2 = phi i1 [ true, %entry ], [ %cmp1, %lor.rhs ]
  ret i1 %2
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_shapeType, align 4, !tbaa !26
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !8
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !8
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !8
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !8
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !8
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !8
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !8
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !8
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !8
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !10
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4, !tbaa !8
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !8
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !8
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #6
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #6
  ret void
}

define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !12
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !12
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !12
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !12
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !12
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !12
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultC2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %this, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  %0 = bitcast %"struct.btDiscreteCollisionDetectorInterface::Result"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTVN36btDiscreteCollisionDetectorInterface6ResultE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !14
  ret %"struct.btDiscreteCollisionDetectorInterface::Result"* %this1
}

; Function Attrs: inlinehint nounwind
define internal void @_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawEN20btIntermediateResultD0Ev(%struct.btIntermediateResult* %this) unnamed_addr #3 {
entry:
  %this.addr = alloca %struct.btIntermediateResult*, align 4
  store %struct.btIntermediateResult* %this, %struct.btIntermediateResult** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btIntermediateResult*, %struct.btIntermediateResult** %this.addr, align 4
  %call = call %struct.btIntermediateResult* bitcast (%"struct.btDiscreteCollisionDetectorInterface::Result"* (%"struct.btDiscreteCollisionDetectorInterface::Result"*)* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev to %struct.btIntermediateResult* (%struct.btIntermediateResult*)*)(%struct.btIntermediateResult* %this1) #6
  %0 = bitcast %struct.btIntermediateResult* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: nounwind
define internal void @_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawEN20btIntermediateResult20setShapeIdentifiersAEii(%struct.btIntermediateResult* %this, i32 %partId0, i32 %index0) unnamed_addr #5 {
entry:
  %this.addr = alloca %struct.btIntermediateResult*, align 4
  %partId0.addr = alloca i32, align 4
  %index0.addr = alloca i32, align 4
  store %struct.btIntermediateResult* %this, %struct.btIntermediateResult** %this.addr, align 4, !tbaa !2
  store i32 %partId0, i32* %partId0.addr, align 4, !tbaa !10
  store i32 %index0, i32* %index0.addr, align 4, !tbaa !10
  %this1 = load %struct.btIntermediateResult*, %struct.btIntermediateResult** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define internal void @_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawEN20btIntermediateResult20setShapeIdentifiersBEii(%struct.btIntermediateResult* %this, i32 %partId1, i32 %index1) unnamed_addr #5 {
entry:
  %this.addr = alloca %struct.btIntermediateResult*, align 4
  %partId1.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  store %struct.btIntermediateResult* %this, %struct.btIntermediateResult** %this.addr, align 4, !tbaa !2
  store i32 %partId1, i32* %partId1.addr, align 4, !tbaa !10
  store i32 %index1, i32* %index1.addr, align 4, !tbaa !10
  %this1 = load %struct.btIntermediateResult*, %struct.btIntermediateResult** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define internal void @_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawEN20btIntermediateResult15addContactPointERKS8_SE_f(%struct.btIntermediateResult* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %normalOnBInWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %pointInWorld, float %depth) unnamed_addr #5 {
entry:
  %this.addr = alloca %struct.btIntermediateResult*, align 4
  %normalOnBInWorld.addr = alloca %class.btVector3*, align 4
  %pointInWorld.addr = alloca %class.btVector3*, align 4
  %depth.addr = alloca float, align 4
  store %struct.btIntermediateResult* %this, %struct.btIntermediateResult** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %normalOnBInWorld, %class.btVector3** %normalOnBInWorld.addr, align 4, !tbaa !2
  store %class.btVector3* %pointInWorld, %class.btVector3** %pointInWorld.addr, align 4, !tbaa !2
  store float %depth, float* %depth.addr, align 4, !tbaa !8
  %this1 = load %struct.btIntermediateResult*, %struct.btIntermediateResult** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %normalOnBInWorld.addr, align 4, !tbaa !2
  %m_normalOnBInWorld = getelementptr inbounds %struct.btIntermediateResult, %struct.btIntermediateResult* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_normalOnBInWorld to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !12
  %3 = load %class.btVector3*, %class.btVector3** %pointInWorld.addr, align 4, !tbaa !2
  %m_pointInWorld = getelementptr inbounds %struct.btIntermediateResult, %struct.btIntermediateResult* %this1, i32 0, i32 2
  %4 = bitcast %class.btVector3* %m_pointInWorld to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !12
  %6 = load float, float* %depth.addr, align 4, !tbaa !8
  %m_depth = getelementptr inbounds %struct.btIntermediateResult, %struct.btIntermediateResult* %this1, i32 0, i32 3
  store float %6, float* %m_depth, align 4, !tbaa !22
  %m_hasResult = getelementptr inbounds %struct.btIntermediateResult, %struct.btIntermediateResult* %this1, i32 0, i32 4
  store i8 1, i8* %m_hasResult, align 4, !tbaa !24
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %this, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  ret %"struct.btDiscreteCollisionDetectorInterface::Result"* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN36btDiscreteCollisionDetectorInterface6ResultD0Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %this, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #7

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #8

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }
attributes #7 = { cold noreturn nounwind }
attributes #8 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { builtin nounwind }
attributes #10 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"bool", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"float", !4, i64 0}
!10 = !{!11, !11, i64 0}
!11 = !{!"int", !4, i64 0}
!12 = !{i64 0, i64 16, !13}
!13 = !{!4, !4, i64 0}
!14 = !{!15, !15, i64 0}
!15 = !{!"vtable pointer", !5, i64 0}
!16 = !{i8 0, i8 2}
!17 = !{!18, !9, i64 128}
!18 = !{!"_ZTSN36btDiscreteCollisionDetectorInterface17ClosestPointInputE", !19, i64 0, !19, i64 64, !9, i64 128}
!19 = !{!"_ZTS11btTransform", !20, i64 0, !21, i64 48}
!20 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!21 = !{!"_ZTS9btVector3", !4, i64 0}
!22 = !{!23, !9, i64 36}
!23 = !{!"_ZTSZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawE20btIntermediateResult", !21, i64 4, !21, i64 20, !9, i64 36, !7, i64 40}
!24 = !{!23, !7, i64 40}
!25 = !{!"branch_weights", i32 1, i32 1048575}
!26 = !{!27, !11, i64 4}
!27 = !{!"_ZTS16btCollisionShape", !11, i64 4, !3, i64 8}
