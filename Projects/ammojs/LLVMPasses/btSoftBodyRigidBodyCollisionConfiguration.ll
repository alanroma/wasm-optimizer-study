; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletSoftBody/btSoftBodyRigidBodyCollisionConfiguration.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletSoftBody/btSoftBodyRigidBodyCollisionConfiguration.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btSoftBodyRigidBodyCollisionConfiguration = type { %class.btDefaultCollisionConfiguration, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc* }
%class.btDefaultCollisionConfiguration = type { %class.btCollisionConfiguration, i32, %class.btPoolAllocator*, i8, %class.btPoolAllocator*, i8, %class.btVoronoiSimplexSolver*, %class.btConvexPenetrationDepthSolver*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc* }
%class.btCollisionConfiguration = type { i32 (...)** }
%class.btPoolAllocator = type { i32, i32, i32, i8*, i8* }
%class.btVoronoiSimplexSolver = type opaque
%class.btConvexPenetrationDepthSolver = type opaque
%struct.btCollisionAlgorithmCreateFunc = type <{ i32 (...)**, i8, [3 x i8] }>
%struct.btDefaultCollisionConstructionInfo = type { %class.btPoolAllocator*, %class.btPoolAllocator*, i32, i32, i32, i32 }
%"struct.btSoftSoftCollisionAlgorithm::CreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, [3 x i8] }
%struct.btCollisionAlgorithmCreateFunc.base = type <{ i32 (...)**, i8 }>
%"struct.btSoftRigidCollisionAlgorithm::CreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, [3 x i8] }
%"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, [3 x i8] }
%"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, [3 x i8] }
%class.btCollisionAlgorithm = type { i32 (...)**, %class.btDispatcher* }
%class.btDispatcher = type { i32 (...)** }
%struct.btCollisionAlgorithmConstructionInfo = type { %class.btDispatcher*, %class.btPersistentManifold* }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, i32, i32, i32, i32, i8*, i8, float, float, float, float, float, float, float, i32, %class.btVector3, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%union.anon = type { i8* }
%struct.btCollisionObjectWrapper = type { %struct.btCollisionObjectWrapper*, %class.btCollisionShape*, %class.btCollisionObject*, %class.btTransform*, i32, i32 }
%class.btSoftSoftCollisionAlgorithm = type { %class.btCollisionAlgorithm, i8, %class.btPersistentManifold*, %class.btSoftBody*, %class.btSoftBody* }
%class.btSoftBody = type opaque
%class.btSoftRigidCollisionAlgorithm = type <{ %class.btCollisionAlgorithm, %class.btSoftBody*, %class.btCollisionObject*, i8, [3 x i8] }>
%class.btSoftBodyConcaveCollisionAlgorithm = type { %class.btCollisionAlgorithm, i8, %class.btSoftBodyTriangleCallback }
%class.btSoftBodyTriangleCallback = type { %class.btTriangleCallback, %class.btSoftBody*, %class.btCollisionObject*, %class.btVector3, %class.btVector3, %class.btManifoldResult*, %class.btDispatcher*, %struct.btDispatcherInfo*, float, %class.btHashMap, i32 }
%class.btTriangleCallback = type { i32 (...)** }
%class.btManifoldResult = type { %"struct.btDiscreteCollisionDetectorInterface::Result", %class.btPersistentManifold*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i32, i32, i32, i32 }
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btIDebugDraw = type opaque
%class.btHashMap = type { %class.btAlignedObjectArray, %class.btAlignedObjectArray, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.4 }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.btTriIndex*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btTriIndex = type { i32, %class.btCollisionShape* }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %class.btHashKey*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btHashKey = type opaque

$_ZN28btSoftSoftCollisionAlgorithm10CreateFuncC2Ev = comdat any

$_ZN29btSoftRigidCollisionAlgorithm10CreateFuncC2Ev = comdat any

$_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncC2Ev = comdat any

$_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncC2Ev = comdat any

$_ZNK15btPoolAllocator14getElementSizeEv = comdat any

$_Z5btMaxIiERKT_S2_S2_ = comdat any

$_ZN15btPoolAllocatorD2Ev = comdat any

$_ZN15btPoolAllocatorC2Eii = comdat any

$_ZN17btBroadphaseProxy8isConvexEi = comdat any

$_ZN17btBroadphaseProxy9isConcaveEi = comdat any

$_ZN31btDefaultCollisionConfiguration25getPersistentManifoldPoolEv = comdat any

$_ZN31btDefaultCollisionConfiguration25getCollisionAlgorithmPoolEv = comdat any

$_ZN31btDefaultCollisionConfiguration16getSimplexSolverEv = comdat any

$_ZN30btCollisionAlgorithmCreateFuncC2Ev = comdat any

$_ZN28btSoftSoftCollisionAlgorithm10CreateFuncD0Ev = comdat any

$_ZN28btSoftSoftCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = comdat any

$_ZN30btCollisionAlgorithmCreateFuncD0Ev = comdat any

$_ZN30btCollisionAlgorithmCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS4_ = comdat any

$_ZN30btCollisionAlgorithmCreateFuncD2Ev = comdat any

$_ZN29btSoftRigidCollisionAlgorithm10CreateFuncD0Ev = comdat any

$_ZN29btSoftRigidCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = comdat any

$_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncD0Ev = comdat any

$_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = comdat any

$_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncD0Ev = comdat any

$_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = comdat any

$_ZTVN28btSoftSoftCollisionAlgorithm10CreateFuncE = comdat any

$_ZTSN28btSoftSoftCollisionAlgorithm10CreateFuncE = comdat any

$_ZTS30btCollisionAlgorithmCreateFunc = comdat any

$_ZTI30btCollisionAlgorithmCreateFunc = comdat any

$_ZTIN28btSoftSoftCollisionAlgorithm10CreateFuncE = comdat any

$_ZTV30btCollisionAlgorithmCreateFunc = comdat any

$_ZTVN29btSoftRigidCollisionAlgorithm10CreateFuncE = comdat any

$_ZTSN29btSoftRigidCollisionAlgorithm10CreateFuncE = comdat any

$_ZTIN29btSoftRigidCollisionAlgorithm10CreateFuncE = comdat any

$_ZTVN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE = comdat any

$_ZTSN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE = comdat any

$_ZTIN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE = comdat any

$_ZTVN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE = comdat any

$_ZTSN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE = comdat any

$_ZTIN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE = comdat any

@_ZTV41btSoftBodyRigidBodyCollisionConfiguration = hidden unnamed_addr constant { [8 x i8*] } { [8 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI41btSoftBodyRigidBodyCollisionConfiguration to i8*), i8* bitcast (%class.btSoftBodyRigidBodyCollisionConfiguration* (%class.btSoftBodyRigidBodyCollisionConfiguration*)* @_ZN41btSoftBodyRigidBodyCollisionConfigurationD1Ev to i8*), i8* bitcast (void (%class.btSoftBodyRigidBodyCollisionConfiguration*)* @_ZN41btSoftBodyRigidBodyCollisionConfigurationD0Ev to i8*), i8* bitcast (%class.btPoolAllocator* (%class.btDefaultCollisionConfiguration*)* @_ZN31btDefaultCollisionConfiguration25getPersistentManifoldPoolEv to i8*), i8* bitcast (%class.btPoolAllocator* (%class.btDefaultCollisionConfiguration*)* @_ZN31btDefaultCollisionConfiguration25getCollisionAlgorithmPoolEv to i8*), i8* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%class.btSoftBodyRigidBodyCollisionConfiguration*, i32, i32)* @_ZN41btSoftBodyRigidBodyCollisionConfiguration31getCollisionAlgorithmCreateFuncEii to i8*), i8* bitcast (%class.btVoronoiSimplexSolver* (%class.btDefaultCollisionConfiguration*)* @_ZN31btDefaultCollisionConfiguration16getSimplexSolverEv to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS41btSoftBodyRigidBodyCollisionConfiguration = hidden constant [44 x i8] c"41btSoftBodyRigidBodyCollisionConfiguration\00", align 1
@_ZTI31btDefaultCollisionConfiguration = external constant i8*
@_ZTI41btSoftBodyRigidBodyCollisionConfiguration = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([44 x i8], [44 x i8]* @_ZTS41btSoftBodyRigidBodyCollisionConfiguration, i32 0, i32 0), i8* bitcast (i8** @_ZTI31btDefaultCollisionConfiguration to i8*) }, align 4
@_ZTVN28btSoftSoftCollisionAlgorithm10CreateFuncE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN28btSoftSoftCollisionAlgorithm10CreateFuncE to i8*), i8* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to i8*), i8* bitcast (void (%"struct.btSoftSoftCollisionAlgorithm::CreateFunc"*)* @_ZN28btSoftSoftCollisionAlgorithm10CreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%"struct.btSoftSoftCollisionAlgorithm::CreateFunc"*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN28btSoftSoftCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ to i8*)] }, comdat, align 4
@_ZTSN28btSoftSoftCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant [45 x i8] c"N28btSoftSoftCollisionAlgorithm10CreateFuncE\00", comdat, align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS30btCollisionAlgorithmCreateFunc = linkonce_odr hidden constant [33 x i8] c"30btCollisionAlgorithmCreateFunc\00", comdat, align 1
@_ZTI30btCollisionAlgorithmCreateFunc = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([33 x i8], [33 x i8]* @_ZTS30btCollisionAlgorithmCreateFunc, i32 0, i32 0) }, comdat, align 4
@_ZTIN28btSoftSoftCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([45 x i8], [45 x i8]* @_ZTSN28btSoftSoftCollisionAlgorithm10CreateFuncE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*) }, comdat, align 4
@_ZTV30btCollisionAlgorithmCreateFunc = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*), i8* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to i8*), i8* bitcast (void (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN30btCollisionAlgorithmCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS4_ to i8*)] }, comdat, align 4
@_ZTVN29btSoftRigidCollisionAlgorithm10CreateFuncE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN29btSoftRigidCollisionAlgorithm10CreateFuncE to i8*), i8* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to i8*), i8* bitcast (void (%"struct.btSoftRigidCollisionAlgorithm::CreateFunc"*)* @_ZN29btSoftRigidCollisionAlgorithm10CreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%"struct.btSoftRigidCollisionAlgorithm::CreateFunc"*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN29btSoftRigidCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ to i8*)] }, comdat, align 4
@_ZTSN29btSoftRigidCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant [46 x i8] c"N29btSoftRigidCollisionAlgorithm10CreateFuncE\00", comdat, align 1
@_ZTIN29btSoftRigidCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([46 x i8], [46 x i8]* @_ZTSN29btSoftRigidCollisionAlgorithm10CreateFuncE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*) }, comdat, align 4
@_ZTVN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE to i8*), i8* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to i8*), i8* bitcast (void (%"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"*)* @_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ to i8*)] }, comdat, align 4
@_ZTSN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant [52 x i8] c"N35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE\00", comdat, align 1
@_ZTIN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([52 x i8], [52 x i8]* @_ZTSN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*) }, comdat, align 4
@_ZTVN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE to i8*), i8* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to i8*), i8* bitcast (void (%"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"*)* @_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ to i8*)] }, comdat, align 4
@_ZTSN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE = linkonce_odr hidden constant [59 x i8] c"N35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE\00", comdat, align 1
@_ZTIN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([59 x i8], [59 x i8]* @_ZTSN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*) }, comdat, align 4

@_ZN41btSoftBodyRigidBodyCollisionConfigurationC1ERK34btDefaultCollisionConstructionInfo = hidden unnamed_addr alias %class.btSoftBodyRigidBodyCollisionConfiguration* (%class.btSoftBodyRigidBodyCollisionConfiguration*, %struct.btDefaultCollisionConstructionInfo*), %class.btSoftBodyRigidBodyCollisionConfiguration* (%class.btSoftBodyRigidBodyCollisionConfiguration*, %struct.btDefaultCollisionConstructionInfo*)* @_ZN41btSoftBodyRigidBodyCollisionConfigurationC2ERK34btDefaultCollisionConstructionInfo
@_ZN41btSoftBodyRigidBodyCollisionConfigurationD1Ev = hidden unnamed_addr alias %class.btSoftBodyRigidBodyCollisionConfiguration* (%class.btSoftBodyRigidBodyCollisionConfiguration*), %class.btSoftBodyRigidBodyCollisionConfiguration* (%class.btSoftBodyRigidBodyCollisionConfiguration*)* @_ZN41btSoftBodyRigidBodyCollisionConfigurationD2Ev

define hidden %class.btSoftBodyRigidBodyCollisionConfiguration* @_ZN41btSoftBodyRigidBodyCollisionConfigurationC2ERK34btDefaultCollisionConstructionInfo(%class.btSoftBodyRigidBodyCollisionConfiguration* returned %this, %struct.btDefaultCollisionConstructionInfo* nonnull align 4 dereferenceable(24) %constructionInfo) unnamed_addr #0 {
entry:
  %retval = alloca %class.btSoftBodyRigidBodyCollisionConfiguration*, align 4
  %this.addr = alloca %class.btSoftBodyRigidBodyCollisionConfiguration*, align 4
  %constructionInfo.addr = alloca %struct.btDefaultCollisionConstructionInfo*, align 4
  %mem = alloca i8*, align 4
  %curElemSize = alloca i32, align 4
  %maxSize0 = alloca i32, align 4
  %maxSize1 = alloca i32, align 4
  %maxSize2 = alloca i32, align 4
  %collisionAlgorithmMaxElementSize = alloca i32, align 4
  %mem24 = alloca i8*, align 4
  store %class.btSoftBodyRigidBodyCollisionConfiguration* %this, %class.btSoftBodyRigidBodyCollisionConfiguration** %this.addr, align 4, !tbaa !2
  store %struct.btDefaultCollisionConstructionInfo* %constructionInfo, %struct.btDefaultCollisionConstructionInfo** %constructionInfo.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftBodyRigidBodyCollisionConfiguration*, %class.btSoftBodyRigidBodyCollisionConfiguration** %this.addr, align 4
  store %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, %class.btSoftBodyRigidBodyCollisionConfiguration** %retval, align 4
  %0 = bitcast %class.btSoftBodyRigidBodyCollisionConfiguration* %this1 to %class.btDefaultCollisionConfiguration*
  %1 = load %struct.btDefaultCollisionConstructionInfo*, %struct.btDefaultCollisionConstructionInfo** %constructionInfo.addr, align 4, !tbaa !2
  %call = call %class.btDefaultCollisionConfiguration* @_ZN31btDefaultCollisionConfigurationC2ERK34btDefaultCollisionConstructionInfo(%class.btDefaultCollisionConfiguration* %0, %struct.btDefaultCollisionConstructionInfo* nonnull align 4 dereferenceable(24) %1)
  %2 = bitcast %class.btSoftBodyRigidBodyCollisionConfiguration* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [8 x i8*] }, { [8 x i8*] }* @_ZTV41btSoftBodyRigidBodyCollisionConfiguration, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %2, align 4, !tbaa !6
  %3 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %call2 = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call2, i8** %mem, align 4, !tbaa !2
  %4 = load i8*, i8** %mem, align 4, !tbaa !2
  %5 = bitcast i8* %4 to %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"*
  %call3 = call %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* @_ZN28btSoftSoftCollisionAlgorithm10CreateFuncC2Ev(%"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* %5)
  %6 = bitcast %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* %5 to %struct.btCollisionAlgorithmCreateFunc*
  %m_softSoftCreateFunc = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 1
  store %struct.btCollisionAlgorithmCreateFunc* %6, %struct.btCollisionAlgorithmCreateFunc** %m_softSoftCreateFunc, align 4, !tbaa !8
  %call4 = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call4, i8** %mem, align 4, !tbaa !2
  %7 = load i8*, i8** %mem, align 4, !tbaa !2
  %8 = bitcast i8* %7 to %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"*
  %call5 = call %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* @_ZN29btSoftRigidCollisionAlgorithm10CreateFuncC2Ev(%"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %8)
  %9 = bitcast %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %8 to %struct.btCollisionAlgorithmCreateFunc*
  %m_softRigidConvexCreateFunc = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 2
  store %struct.btCollisionAlgorithmCreateFunc* %9, %struct.btCollisionAlgorithmCreateFunc** %m_softRigidConvexCreateFunc, align 4, !tbaa !10
  %call6 = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call6, i8** %mem, align 4, !tbaa !2
  %10 = load i8*, i8** %mem, align 4, !tbaa !2
  %11 = bitcast i8* %10 to %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"*
  %call7 = call %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* @_ZN29btSoftRigidCollisionAlgorithm10CreateFuncC2Ev(%"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %11)
  %12 = bitcast %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %11 to %struct.btCollisionAlgorithmCreateFunc*
  %m_swappedSoftRigidConvexCreateFunc = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 3
  store %struct.btCollisionAlgorithmCreateFunc* %12, %struct.btCollisionAlgorithmCreateFunc** %m_swappedSoftRigidConvexCreateFunc, align 4, !tbaa !11
  %m_swappedSoftRigidConvexCreateFunc8 = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 3
  %13 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_swappedSoftRigidConvexCreateFunc8, align 4, !tbaa !11
  %m_swapped = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc, %struct.btCollisionAlgorithmCreateFunc* %13, i32 0, i32 1
  store i8 1, i8* %m_swapped, align 4, !tbaa !12
  %call9 = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call9, i8** %mem, align 4, !tbaa !2
  %14 = load i8*, i8** %mem, align 4, !tbaa !2
  %15 = bitcast i8* %14 to %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"*
  %call10 = call %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* @_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncC2Ev(%"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* %15)
  %16 = bitcast %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* %15 to %struct.btCollisionAlgorithmCreateFunc*
  %m_softRigidConcaveCreateFunc = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 4
  store %struct.btCollisionAlgorithmCreateFunc* %16, %struct.btCollisionAlgorithmCreateFunc** %m_softRigidConcaveCreateFunc, align 4, !tbaa !15
  %call11 = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call11, i8** %mem, align 4, !tbaa !2
  %17 = load i8*, i8** %mem, align 4, !tbaa !2
  %18 = bitcast i8* %17 to %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"*
  %call12 = call %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* @_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncC2Ev(%"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* %18)
  %19 = bitcast %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* %18 to %struct.btCollisionAlgorithmCreateFunc*
  %m_swappedSoftRigidConcaveCreateFunc = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 5
  store %struct.btCollisionAlgorithmCreateFunc* %19, %struct.btCollisionAlgorithmCreateFunc** %m_swappedSoftRigidConcaveCreateFunc, align 4, !tbaa !16
  %m_swappedSoftRigidConcaveCreateFunc13 = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 5
  %20 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_swappedSoftRigidConcaveCreateFunc13, align 4, !tbaa !16
  %m_swapped14 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc, %struct.btCollisionAlgorithmCreateFunc* %20, i32 0, i32 1
  store i8 1, i8* %m_swapped14, align 4, !tbaa !12
  %21 = bitcast %class.btSoftBodyRigidBodyCollisionConfiguration* %this1 to %class.btDefaultCollisionConfiguration*
  %m_ownsCollisionAlgorithmPool = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %21, i32 0, i32 5
  %22 = load i8, i8* %m_ownsCollisionAlgorithmPool, align 4, !tbaa !17, !range !20
  %tobool = trunc i8 %22 to i1
  br i1 %tobool, label %land.lhs.true, label %if.end28

land.lhs.true:                                    ; preds = %entry
  %23 = bitcast %class.btSoftBodyRigidBodyCollisionConfiguration* %this1 to %class.btDefaultCollisionConfiguration*
  %m_collisionAlgorithmPool = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %23, i32 0, i32 4
  %24 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_collisionAlgorithmPool, align 4, !tbaa !21
  %tobool15 = icmp ne %class.btPoolAllocator* %24, null
  br i1 %tobool15, label %if.then, label %if.end28

if.then:                                          ; preds = %land.lhs.true
  %25 = bitcast i32* %curElemSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  %26 = bitcast %class.btSoftBodyRigidBodyCollisionConfiguration* %this1 to %class.btDefaultCollisionConfiguration*
  %m_collisionAlgorithmPool16 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %26, i32 0, i32 4
  %27 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_collisionAlgorithmPool16, align 4, !tbaa !21
  %call17 = call i32 @_ZNK15btPoolAllocator14getElementSizeEv(%class.btPoolAllocator* %27)
  store i32 %call17, i32* %curElemSize, align 4, !tbaa !22
  %28 = bitcast i32* %maxSize0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #8
  store i32 24, i32* %maxSize0, align 4, !tbaa !22
  %29 = bitcast i32* %maxSize1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #8
  store i32 20, i32* %maxSize1, align 4, !tbaa !22
  %30 = bitcast i32* %maxSize2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #8
  store i32 156, i32* %maxSize2, align 4, !tbaa !22
  %31 = bitcast i32* %collisionAlgorithmMaxElementSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #8
  %call18 = call nonnull align 4 dereferenceable(4) i32* @_Z5btMaxIiERKT_S2_S2_(i32* nonnull align 4 dereferenceable(4) %maxSize0, i32* nonnull align 4 dereferenceable(4) %maxSize1)
  %32 = load i32, i32* %call18, align 4, !tbaa !22
  store i32 %32, i32* %collisionAlgorithmMaxElementSize, align 4, !tbaa !22
  %call19 = call nonnull align 4 dereferenceable(4) i32* @_Z5btMaxIiERKT_S2_S2_(i32* nonnull align 4 dereferenceable(4) %collisionAlgorithmMaxElementSize, i32* nonnull align 4 dereferenceable(4) %maxSize2)
  %33 = load i32, i32* %call19, align 4, !tbaa !22
  store i32 %33, i32* %collisionAlgorithmMaxElementSize, align 4, !tbaa !22
  %34 = load i32, i32* %collisionAlgorithmMaxElementSize, align 4, !tbaa !22
  %35 = load i32, i32* %curElemSize, align 4, !tbaa !22
  %cmp = icmp sgt i32 %34, %35
  br i1 %cmp, label %if.then20, label %if.end

if.then20:                                        ; preds = %if.then
  %36 = bitcast %class.btSoftBodyRigidBodyCollisionConfiguration* %this1 to %class.btDefaultCollisionConfiguration*
  %m_collisionAlgorithmPool21 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %36, i32 0, i32 4
  %37 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_collisionAlgorithmPool21, align 4, !tbaa !21
  %call22 = call %class.btPoolAllocator* @_ZN15btPoolAllocatorD2Ev(%class.btPoolAllocator* %37) #8
  %38 = bitcast %class.btSoftBodyRigidBodyCollisionConfiguration* %this1 to %class.btDefaultCollisionConfiguration*
  %m_collisionAlgorithmPool23 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %38, i32 0, i32 4
  %39 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_collisionAlgorithmPool23, align 4, !tbaa !21
  %40 = bitcast %class.btPoolAllocator* %39 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %40)
  %41 = bitcast i8** %mem24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #8
  %call25 = call i8* @_Z22btAlignedAllocInternalmi(i32 20, i32 16)
  store i8* %call25, i8** %mem24, align 4, !tbaa !2
  %42 = load i8*, i8** %mem24, align 4, !tbaa !2
  %43 = bitcast i8* %42 to %class.btPoolAllocator*
  %44 = load i32, i32* %collisionAlgorithmMaxElementSize, align 4, !tbaa !22
  %45 = load %struct.btDefaultCollisionConstructionInfo*, %struct.btDefaultCollisionConstructionInfo** %constructionInfo.addr, align 4, !tbaa !2
  %m_defaultMaxCollisionAlgorithmPoolSize = getelementptr inbounds %struct.btDefaultCollisionConstructionInfo, %struct.btDefaultCollisionConstructionInfo* %45, i32 0, i32 3
  %46 = load i32, i32* %m_defaultMaxCollisionAlgorithmPoolSize, align 4, !tbaa !23
  %call26 = call %class.btPoolAllocator* @_ZN15btPoolAllocatorC2Eii(%class.btPoolAllocator* %43, i32 %44, i32 %46)
  %47 = bitcast %class.btSoftBodyRigidBodyCollisionConfiguration* %this1 to %class.btDefaultCollisionConfiguration*
  %m_collisionAlgorithmPool27 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %47, i32 0, i32 4
  store %class.btPoolAllocator* %43, %class.btPoolAllocator** %m_collisionAlgorithmPool27, align 4, !tbaa !21
  %48 = bitcast i8** %mem24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #8
  br label %if.end

if.end:                                           ; preds = %if.then20, %if.then
  %49 = bitcast i32* %collisionAlgorithmMaxElementSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #8
  %50 = bitcast i32* %maxSize2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #8
  %51 = bitcast i32* %maxSize1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #8
  %52 = bitcast i32* %maxSize0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #8
  %53 = bitcast i32* %curElemSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #8
  br label %if.end28

if.end28:                                         ; preds = %if.end, %land.lhs.true, %entry
  %54 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #8
  %55 = load %class.btSoftBodyRigidBodyCollisionConfiguration*, %class.btSoftBodyRigidBodyCollisionConfiguration** %retval, align 4
  ret %class.btSoftBodyRigidBodyCollisionConfiguration* %55
}

declare %class.btDefaultCollisionConfiguration* @_ZN31btDefaultCollisionConfigurationC2ERK34btDefaultCollisionConstructionInfo(%class.btDefaultCollisionConfiguration* returned, %struct.btDefaultCollisionConstructionInfo* nonnull align 4 dereferenceable(24)) unnamed_addr #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #1

; Function Attrs: inlinehint
define linkonce_odr hidden %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* @_ZN28btSoftSoftCollisionAlgorithm10CreateFuncC2Ev(%"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* %this, %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"*, %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* %0)
  %1 = bitcast %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN28btSoftSoftCollisionAlgorithm10CreateFuncE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  ret %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* @_ZN29btSoftRigidCollisionAlgorithm10CreateFuncC2Ev(%"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %this, %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"*, %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* %0)
  %1 = bitcast %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN29btSoftRigidCollisionAlgorithm10CreateFuncE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  ret %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* @_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncC2Ev(%"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* %this, %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"*, %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* %0)
  %1 = bitcast %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  ret %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* @_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncC2Ev(%"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"*, align 4
  store %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* %this, %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"*, %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* %0)
  %1 = bitcast %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  ret %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK15btPoolAllocator14getElementSizeEv(%class.btPoolAllocator* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btPoolAllocator*, align 4
  store %class.btPoolAllocator* %this, %class.btPoolAllocator** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPoolAllocator*, %class.btPoolAllocator** %this.addr, align 4
  %m_elemSize = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 0
  %0 = load i32, i32* %m_elemSize, align 4, !tbaa !25
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_Z5btMaxIiERKT_S2_S2_(i32* nonnull align 4 dereferenceable(4) %a, i32* nonnull align 4 dereferenceable(4) %b) #4 comdat {
entry:
  %a.addr = alloca i32*, align 4
  %b.addr = alloca i32*, align 4
  store i32* %a, i32** %a.addr, align 4, !tbaa !2
  store i32* %b, i32** %b.addr, align 4, !tbaa !2
  %0 = load i32*, i32** %a.addr, align 4, !tbaa !2
  %1 = load i32, i32* %0, align 4, !tbaa !22
  %2 = load i32*, i32** %b.addr, align 4, !tbaa !2
  %3 = load i32, i32* %2, align 4, !tbaa !22
  %cmp = icmp sgt i32 %1, %3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load i32*, i32** %a.addr, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %entry
  %5 = load i32*, i32** %b.addr, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %4, %cond.true ], [ %5, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btPoolAllocator* @_ZN15btPoolAllocatorD2Ev(%class.btPoolAllocator* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btPoolAllocator*, align 4
  store %class.btPoolAllocator* %this, %class.btPoolAllocator** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPoolAllocator*, %class.btPoolAllocator** %this.addr, align 4
  %m_pool = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 4
  %0 = load i8*, i8** %m_pool, align 4, !tbaa !27
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret %class.btPoolAllocator* %this1
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

define linkonce_odr hidden %class.btPoolAllocator* @_ZN15btPoolAllocatorC2Eii(%class.btPoolAllocator* returned %this, i32 %elemSize, i32 %maxElements) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btPoolAllocator*, align 4
  %this.addr = alloca %class.btPoolAllocator*, align 4
  %elemSize.addr = alloca i32, align 4
  %maxElements.addr = alloca i32, align 4
  %p = alloca i8*, align 4
  %count = alloca i32, align 4
  store %class.btPoolAllocator* %this, %class.btPoolAllocator** %this.addr, align 4, !tbaa !2
  store i32 %elemSize, i32* %elemSize.addr, align 4, !tbaa !22
  store i32 %maxElements, i32* %maxElements.addr, align 4, !tbaa !22
  %this1 = load %class.btPoolAllocator*, %class.btPoolAllocator** %this.addr, align 4
  store %class.btPoolAllocator* %this1, %class.btPoolAllocator** %retval, align 4
  %m_elemSize = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 0
  %0 = load i32, i32* %elemSize.addr, align 4, !tbaa !22
  store i32 %0, i32* %m_elemSize, align 4, !tbaa !25
  %m_maxElements = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 1
  %1 = load i32, i32* %maxElements.addr, align 4, !tbaa !22
  store i32 %1, i32* %m_maxElements, align 4, !tbaa !28
  %m_elemSize2 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 0
  %2 = load i32, i32* %m_elemSize2, align 4, !tbaa !25
  %m_maxElements3 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 1
  %3 = load i32, i32* %m_maxElements3, align 4, !tbaa !28
  %mul = mul nsw i32 %2, %3
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %m_pool = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 4
  store i8* %call, i8** %m_pool, align 4, !tbaa !27
  %4 = bitcast i8** %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %m_pool4 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 4
  %5 = load i8*, i8** %m_pool4, align 4, !tbaa !27
  store i8* %5, i8** %p, align 4, !tbaa !2
  %6 = load i8*, i8** %p, align 4, !tbaa !2
  %m_firstFree = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 3
  store i8* %6, i8** %m_firstFree, align 4, !tbaa !29
  %m_maxElements5 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 1
  %7 = load i32, i32* %m_maxElements5, align 4, !tbaa !28
  %m_freeCount = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 2
  store i32 %7, i32* %m_freeCount, align 4, !tbaa !30
  %8 = bitcast i32* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  %m_maxElements6 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 1
  %9 = load i32, i32* %m_maxElements6, align 4, !tbaa !28
  store i32 %9, i32* %count, align 4, !tbaa !22
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %10 = load i32, i32* %count, align 4, !tbaa !22
  %dec = add nsw i32 %10, -1
  store i32 %dec, i32* %count, align 4, !tbaa !22
  %tobool = icmp ne i32 %dec, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %11 = load i8*, i8** %p, align 4, !tbaa !2
  %m_elemSize7 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 0
  %12 = load i32, i32* %m_elemSize7, align 4, !tbaa !25
  %add.ptr = getelementptr inbounds i8, i8* %11, i32 %12
  %13 = load i8*, i8** %p, align 4, !tbaa !2
  %14 = bitcast i8* %13 to i8**
  store i8* %add.ptr, i8** %14, align 4, !tbaa !2
  %m_elemSize8 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 0
  %15 = load i32, i32* %m_elemSize8, align 4, !tbaa !25
  %16 = load i8*, i8** %p, align 4, !tbaa !2
  %add.ptr9 = getelementptr inbounds i8, i8* %16, i32 %15
  store i8* %add.ptr9, i8** %p, align 4, !tbaa !2
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %17 = load i8*, i8** %p, align 4, !tbaa !2
  %18 = bitcast i8* %17 to i8**
  store i8* null, i8** %18, align 4, !tbaa !2
  %19 = bitcast i32* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  %20 = bitcast i8** %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #8
  %21 = load %class.btPoolAllocator*, %class.btPoolAllocator** %retval, align 4
  ret %class.btPoolAllocator* %21
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: nounwind
define hidden %class.btSoftBodyRigidBodyCollisionConfiguration* @_ZN41btSoftBodyRigidBodyCollisionConfigurationD2Ev(%class.btSoftBodyRigidBodyCollisionConfiguration* returned %this) unnamed_addr #5 {
entry:
  %this.addr = alloca %class.btSoftBodyRigidBodyCollisionConfiguration*, align 4
  store %class.btSoftBodyRigidBodyCollisionConfiguration* %this, %class.btSoftBodyRigidBodyCollisionConfiguration** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftBodyRigidBodyCollisionConfiguration*, %class.btSoftBodyRigidBodyCollisionConfiguration** %this.addr, align 4
  %0 = bitcast %class.btSoftBodyRigidBodyCollisionConfiguration* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [8 x i8*] }, { [8 x i8*] }* @_ZTV41btSoftBodyRigidBodyCollisionConfiguration, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  %m_softSoftCreateFunc = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 1
  %1 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_softSoftCreateFunc, align 4, !tbaa !8
  %2 = bitcast %struct.btCollisionAlgorithmCreateFunc* %1 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %2, align 4, !tbaa !6
  %vfn = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable, i64 0
  %3 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn, align 4
  %call = call %struct.btCollisionAlgorithmCreateFunc* %3(%struct.btCollisionAlgorithmCreateFunc* %1) #8
  %m_softSoftCreateFunc2 = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 1
  %4 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_softSoftCreateFunc2, align 4, !tbaa !8
  %5 = bitcast %struct.btCollisionAlgorithmCreateFunc* %4 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %5)
  %m_softRigidConvexCreateFunc = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 2
  %6 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_softRigidConvexCreateFunc, align 4, !tbaa !10
  %7 = bitcast %struct.btCollisionAlgorithmCreateFunc* %6 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable3 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %7, align 4, !tbaa !6
  %vfn4 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable3, i64 0
  %8 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn4, align 4
  %call5 = call %struct.btCollisionAlgorithmCreateFunc* %8(%struct.btCollisionAlgorithmCreateFunc* %6) #8
  %m_softRigidConvexCreateFunc6 = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 2
  %9 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_softRigidConvexCreateFunc6, align 4, !tbaa !10
  %10 = bitcast %struct.btCollisionAlgorithmCreateFunc* %9 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %10)
  %m_swappedSoftRigidConvexCreateFunc = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 3
  %11 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_swappedSoftRigidConvexCreateFunc, align 4, !tbaa !11
  %12 = bitcast %struct.btCollisionAlgorithmCreateFunc* %11 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable7 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %12, align 4, !tbaa !6
  %vfn8 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable7, i64 0
  %13 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn8, align 4
  %call9 = call %struct.btCollisionAlgorithmCreateFunc* %13(%struct.btCollisionAlgorithmCreateFunc* %11) #8
  %m_swappedSoftRigidConvexCreateFunc10 = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 3
  %14 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_swappedSoftRigidConvexCreateFunc10, align 4, !tbaa !11
  %15 = bitcast %struct.btCollisionAlgorithmCreateFunc* %14 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %15)
  %m_softRigidConcaveCreateFunc = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 4
  %16 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_softRigidConcaveCreateFunc, align 4, !tbaa !15
  %17 = bitcast %struct.btCollisionAlgorithmCreateFunc* %16 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable11 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %17, align 4, !tbaa !6
  %vfn12 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable11, i64 0
  %18 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn12, align 4
  %call13 = call %struct.btCollisionAlgorithmCreateFunc* %18(%struct.btCollisionAlgorithmCreateFunc* %16) #8
  %m_softRigidConcaveCreateFunc14 = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 4
  %19 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_softRigidConcaveCreateFunc14, align 4, !tbaa !15
  %20 = bitcast %struct.btCollisionAlgorithmCreateFunc* %19 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %20)
  %m_swappedSoftRigidConcaveCreateFunc = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 5
  %21 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_swappedSoftRigidConcaveCreateFunc, align 4, !tbaa !16
  %22 = bitcast %struct.btCollisionAlgorithmCreateFunc* %21 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable15 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %22, align 4, !tbaa !6
  %vfn16 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable15, i64 0
  %23 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn16, align 4
  %call17 = call %struct.btCollisionAlgorithmCreateFunc* %23(%struct.btCollisionAlgorithmCreateFunc* %21) #8
  %m_swappedSoftRigidConcaveCreateFunc18 = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 5
  %24 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_swappedSoftRigidConcaveCreateFunc18, align 4, !tbaa !16
  %25 = bitcast %struct.btCollisionAlgorithmCreateFunc* %24 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %25)
  %26 = bitcast %class.btSoftBodyRigidBodyCollisionConfiguration* %this1 to %class.btDefaultCollisionConfiguration*
  %call19 = call %class.btDefaultCollisionConfiguration* @_ZN31btDefaultCollisionConfigurationD2Ev(%class.btDefaultCollisionConfiguration* %26) #8
  ret %class.btSoftBodyRigidBodyCollisionConfiguration* %this1
}

; Function Attrs: nounwind
declare %class.btDefaultCollisionConfiguration* @_ZN31btDefaultCollisionConfigurationD2Ev(%class.btDefaultCollisionConfiguration* returned) unnamed_addr #6

; Function Attrs: nounwind
define hidden void @_ZN41btSoftBodyRigidBodyCollisionConfigurationD0Ev(%class.btSoftBodyRigidBodyCollisionConfiguration* %this) unnamed_addr #5 {
entry:
  %this.addr = alloca %class.btSoftBodyRigidBodyCollisionConfiguration*, align 4
  store %class.btSoftBodyRigidBodyCollisionConfiguration* %this, %class.btSoftBodyRigidBodyCollisionConfiguration** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftBodyRigidBodyCollisionConfiguration*, %class.btSoftBodyRigidBodyCollisionConfiguration** %this.addr, align 4
  %call = call %class.btSoftBodyRigidBodyCollisionConfiguration* @_ZN41btSoftBodyRigidBodyCollisionConfigurationD1Ev(%class.btSoftBodyRigidBodyCollisionConfiguration* %this1) #8
  %0 = bitcast %class.btSoftBodyRigidBodyCollisionConfiguration* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #7

define hidden %struct.btCollisionAlgorithmCreateFunc* @_ZN41btSoftBodyRigidBodyCollisionConfiguration31getCollisionAlgorithmCreateFuncEii(%class.btSoftBodyRigidBodyCollisionConfiguration* %this, i32 %proxyType0, i32 %proxyType1) unnamed_addr #0 {
entry:
  %retval = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  %this.addr = alloca %class.btSoftBodyRigidBodyCollisionConfiguration*, align 4
  %proxyType0.addr = alloca i32, align 4
  %proxyType1.addr = alloca i32, align 4
  store %class.btSoftBodyRigidBodyCollisionConfiguration* %this, %class.btSoftBodyRigidBodyCollisionConfiguration** %this.addr, align 4, !tbaa !2
  store i32 %proxyType0, i32* %proxyType0.addr, align 4, !tbaa !22
  store i32 %proxyType1, i32* %proxyType1.addr, align 4, !tbaa !22
  %this1 = load %class.btSoftBodyRigidBodyCollisionConfiguration*, %class.btSoftBodyRigidBodyCollisionConfiguration** %this.addr, align 4
  %0 = load i32, i32* %proxyType0.addr, align 4, !tbaa !22
  %cmp = icmp eq i32 %0, 32
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load i32, i32* %proxyType1.addr, align 4, !tbaa !22
  %cmp2 = icmp eq i32 %1, 32
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %m_softSoftCreateFunc = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 1
  %2 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_softSoftCreateFunc, align 4, !tbaa !8
  store %struct.btCollisionAlgorithmCreateFunc* %2, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  %3 = load i32, i32* %proxyType0.addr, align 4, !tbaa !22
  %cmp3 = icmp eq i32 %3, 32
  br i1 %cmp3, label %land.lhs.true4, label %if.end6

land.lhs.true4:                                   ; preds = %if.end
  %4 = load i32, i32* %proxyType1.addr, align 4, !tbaa !22
  %call = call zeroext i1 @_ZN17btBroadphaseProxy8isConvexEi(i32 %4)
  br i1 %call, label %if.then5, label %if.end6

if.then5:                                         ; preds = %land.lhs.true4
  %m_softRigidConvexCreateFunc = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 2
  %5 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_softRigidConvexCreateFunc, align 4, !tbaa !10
  store %struct.btCollisionAlgorithmCreateFunc* %5, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end6:                                          ; preds = %land.lhs.true4, %if.end
  %6 = load i32, i32* %proxyType0.addr, align 4, !tbaa !22
  %call7 = call zeroext i1 @_ZN17btBroadphaseProxy8isConvexEi(i32 %6)
  br i1 %call7, label %land.lhs.true8, label %if.end11

land.lhs.true8:                                   ; preds = %if.end6
  %7 = load i32, i32* %proxyType1.addr, align 4, !tbaa !22
  %cmp9 = icmp eq i32 %7, 32
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %land.lhs.true8
  %m_swappedSoftRigidConvexCreateFunc = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 3
  %8 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_swappedSoftRigidConvexCreateFunc, align 4, !tbaa !11
  store %struct.btCollisionAlgorithmCreateFunc* %8, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end11:                                         ; preds = %land.lhs.true8, %if.end6
  %9 = load i32, i32* %proxyType0.addr, align 4, !tbaa !22
  %cmp12 = icmp eq i32 %9, 32
  br i1 %cmp12, label %land.lhs.true13, label %if.end16

land.lhs.true13:                                  ; preds = %if.end11
  %10 = load i32, i32* %proxyType1.addr, align 4, !tbaa !22
  %call14 = call zeroext i1 @_ZN17btBroadphaseProxy9isConcaveEi(i32 %10)
  br i1 %call14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %land.lhs.true13
  %m_softRigidConcaveCreateFunc = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 4
  %11 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_softRigidConcaveCreateFunc, align 4, !tbaa !15
  store %struct.btCollisionAlgorithmCreateFunc* %11, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end16:                                         ; preds = %land.lhs.true13, %if.end11
  %12 = load i32, i32* %proxyType0.addr, align 4, !tbaa !22
  %call17 = call zeroext i1 @_ZN17btBroadphaseProxy9isConcaveEi(i32 %12)
  br i1 %call17, label %land.lhs.true18, label %if.end21

land.lhs.true18:                                  ; preds = %if.end16
  %13 = load i32, i32* %proxyType1.addr, align 4, !tbaa !22
  %cmp19 = icmp eq i32 %13, 32
  br i1 %cmp19, label %if.then20, label %if.end21

if.then20:                                        ; preds = %land.lhs.true18
  %m_swappedSoftRigidConcaveCreateFunc = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 5
  %14 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_swappedSoftRigidConcaveCreateFunc, align 4, !tbaa !16
  store %struct.btCollisionAlgorithmCreateFunc* %14, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end21:                                         ; preds = %land.lhs.true18, %if.end16
  %15 = bitcast %class.btSoftBodyRigidBodyCollisionConfiguration* %this1 to %class.btDefaultCollisionConfiguration*
  %16 = load i32, i32* %proxyType0.addr, align 4, !tbaa !22
  %17 = load i32, i32* %proxyType1.addr, align 4, !tbaa !22
  %call22 = call %struct.btCollisionAlgorithmCreateFunc* @_ZN31btDefaultCollisionConfiguration31getCollisionAlgorithmCreateFuncEii(%class.btDefaultCollisionConfiguration* %15, i32 %16, i32 %17)
  store %struct.btCollisionAlgorithmCreateFunc* %call22, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

return:                                           ; preds = %if.end21, %if.then20, %if.then15, %if.then10, %if.then5, %if.then
  %18 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  ret %struct.btCollisionAlgorithmCreateFunc* %18
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZN17btBroadphaseProxy8isConvexEi(i32 %proxyType) #4 comdat {
entry:
  %proxyType.addr = alloca i32, align 4
  store i32 %proxyType, i32* %proxyType.addr, align 4, !tbaa !22
  %0 = load i32, i32* %proxyType.addr, align 4, !tbaa !22
  %cmp = icmp slt i32 %0, 20
  ret i1 %cmp
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZN17btBroadphaseProxy9isConcaveEi(i32 %proxyType) #4 comdat {
entry:
  %proxyType.addr = alloca i32, align 4
  store i32 %proxyType, i32* %proxyType.addr, align 4, !tbaa !22
  %0 = load i32, i32* %proxyType.addr, align 4, !tbaa !22
  %cmp = icmp sgt i32 %0, 20
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %1 = load i32, i32* %proxyType.addr, align 4, !tbaa !22
  %cmp1 = icmp slt i32 %1, 30
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %2 = phi i1 [ false, %entry ], [ %cmp1, %land.rhs ]
  ret i1 %2
}

declare %struct.btCollisionAlgorithmCreateFunc* @_ZN31btDefaultCollisionConfiguration31getCollisionAlgorithmCreateFuncEii(%class.btDefaultCollisionConfiguration*, i32, i32) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden %class.btPoolAllocator* @_ZN31btDefaultCollisionConfiguration25getPersistentManifoldPoolEv(%class.btDefaultCollisionConfiguration* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btDefaultCollisionConfiguration*, align 4
  store %class.btDefaultCollisionConfiguration* %this, %class.btDefaultCollisionConfiguration** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btDefaultCollisionConfiguration*, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  %m_persistentManifoldPool = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 2
  %0 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_persistentManifoldPool, align 4, !tbaa !31
  ret %class.btPoolAllocator* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btPoolAllocator* @_ZN31btDefaultCollisionConfiguration25getCollisionAlgorithmPoolEv(%class.btDefaultCollisionConfiguration* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btDefaultCollisionConfiguration*, align 4
  store %class.btDefaultCollisionConfiguration* %this, %class.btDefaultCollisionConfiguration** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btDefaultCollisionConfiguration*, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  %m_collisionAlgorithmPool = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 4
  %0 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_collisionAlgorithmPool, align 4, !tbaa !21
  ret %class.btPoolAllocator* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btVoronoiSimplexSolver* @_ZN31btDefaultCollisionConfiguration16getSimplexSolverEv(%class.btDefaultCollisionConfiguration* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btDefaultCollisionConfiguration*, align 4
  store %class.btDefaultCollisionConfiguration* %this, %class.btDefaultCollisionConfiguration** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btDefaultCollisionConfiguration*, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  %m_simplexSolver = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 6
  %0 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %m_simplexSolver, align 4, !tbaa !32
  ret %class.btVoronoiSimplexSolver* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %this, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  %0 = bitcast %struct.btCollisionAlgorithmCreateFunc* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV30btCollisionAlgorithmCreateFunc, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  %m_swapped = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc, %struct.btCollisionAlgorithmCreateFunc* %this1, i32 0, i32 1
  store i8 0, i8* %m_swapped, align 4, !tbaa !12
  ret %struct.btCollisionAlgorithmCreateFunc* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN28btSoftSoftCollisionAlgorithm10CreateFuncD0Ev(%"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* %this, %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"*, %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %call = call %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* (%"struct.btSoftSoftCollisionAlgorithm::CreateFunc"*)*)(%"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* %this1) #8
  %0 = bitcast %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN28btSoftSoftCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %bbsize = alloca i32, align 4
  %ptr = alloca i8*, align 4
  store %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* %this, %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %this1 = load %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"*, %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast i32* %bbsize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 24, i32* %bbsize, align 4, !tbaa !22
  %1 = bitcast i8** %ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %2, i32 0, i32 0
  %3 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4, !tbaa !33
  %4 = load i32, i32* %bbsize, align 4, !tbaa !22
  %5 = bitcast %class.btDispatcher* %3 to i8* (%class.btDispatcher*, i32)***
  %vtable = load i8* (%class.btDispatcher*, i32)**, i8* (%class.btDispatcher*, i32)*** %5, align 4, !tbaa !6
  %vfn = getelementptr inbounds i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vtable, i64 14
  %6 = load i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vfn, align 4
  %call = call i8* %6(%class.btDispatcher* %3, i32 %4)
  store i8* %call, i8** %ptr, align 4, !tbaa !2
  %7 = load i8*, i8** %ptr, align 4, !tbaa !2
  %8 = bitcast i8* %7 to %class.btSoftSoftCollisionAlgorithm*
  %9 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %10 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %11 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call2 = call %class.btSoftSoftCollisionAlgorithm* @_ZN28btSoftSoftCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_(%class.btSoftSoftCollisionAlgorithm* %8, %class.btPersistentManifold* null, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %9, %struct.btCollisionObjectWrapper* %10, %struct.btCollisionObjectWrapper* %11)
  %12 = bitcast %class.btSoftSoftCollisionAlgorithm* %8 to %class.btCollisionAlgorithm*
  %13 = bitcast i8** %ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  %14 = bitcast i32* %bbsize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #8
  ret %class.btCollisionAlgorithm* %12
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN30btCollisionAlgorithmCreateFuncD0Ev(%struct.btCollisionAlgorithmCreateFunc* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %this, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncD2Ev(%struct.btCollisionAlgorithmCreateFunc* %this1) #8
  %0 = bitcast %struct.btCollisionAlgorithmCreateFunc* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN30btCollisionAlgorithmCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS4_(%struct.btCollisionAlgorithmCreateFunc* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %0, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  %.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %this, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionAlgorithmConstructionInfo* %0, %struct.btCollisionAlgorithmConstructionInfo** %.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  ret %class.btCollisionAlgorithm* null
}

declare %class.btSoftSoftCollisionAlgorithm* @_ZN28btSoftSoftCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_(%class.btSoftSoftCollisionAlgorithm* returned, %class.btPersistentManifold*, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8), %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncD2Ev(%struct.btCollisionAlgorithmCreateFunc* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %this, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  ret %struct.btCollisionAlgorithmCreateFunc* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN29btSoftRigidCollisionAlgorithm10CreateFuncD0Ev(%"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %this, %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"*, %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %call = call %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* (%"struct.btSoftRigidCollisionAlgorithm::CreateFunc"*)*)(%"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %this1) #8
  %0 = bitcast %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN29btSoftRigidCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btCollisionAlgorithm*, align 4
  %this.addr = alloca %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %mem = alloca i8*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %this, %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %this1 = load %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"*, %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %1, i32 0, i32 0
  %2 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4, !tbaa !33
  %3 = bitcast %class.btDispatcher* %2 to i8* (%class.btDispatcher*, i32)***
  %vtable = load i8* (%class.btDispatcher*, i32)**, i8* (%class.btDispatcher*, i32)*** %3, align 4, !tbaa !6
  %vfn = getelementptr inbounds i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vtable, i64 14
  %4 = load i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vfn, align 4
  %call = call i8* %4(%class.btDispatcher* %2, i32 20)
  store i8* %call, i8** %mem, align 4, !tbaa !2
  %5 = bitcast %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %m_swapped = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc, %struct.btCollisionAlgorithmCreateFunc* %5, i32 0, i32 1
  %6 = load i8, i8* %m_swapped, align 4, !tbaa !12, !range !20
  %tobool = trunc i8 %6 to i1
  br i1 %tobool, label %if.else, label %if.then

if.then:                                          ; preds = %entry
  %7 = load i8*, i8** %mem, align 4, !tbaa !2
  %8 = bitcast i8* %7 to %class.btSoftRigidCollisionAlgorithm*
  %9 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %10 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %11 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call2 = call %class.btSoftRigidCollisionAlgorithm* @_ZN29btSoftRigidCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_b(%class.btSoftRigidCollisionAlgorithm* %8, %class.btPersistentManifold* null, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %9, %struct.btCollisionObjectWrapper* %10, %struct.btCollisionObjectWrapper* %11, i1 zeroext false)
  %12 = bitcast %class.btSoftRigidCollisionAlgorithm* %8 to %class.btCollisionAlgorithm*
  store %class.btCollisionAlgorithm* %12, %class.btCollisionAlgorithm** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %entry
  %13 = load i8*, i8** %mem, align 4, !tbaa !2
  %14 = bitcast i8* %13 to %class.btSoftRigidCollisionAlgorithm*
  %15 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %16 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %17 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call3 = call %class.btSoftRigidCollisionAlgorithm* @_ZN29btSoftRigidCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_b(%class.btSoftRigidCollisionAlgorithm* %14, %class.btPersistentManifold* null, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %15, %struct.btCollisionObjectWrapper* %16, %struct.btCollisionObjectWrapper* %17, i1 zeroext true)
  %18 = bitcast %class.btSoftRigidCollisionAlgorithm* %14 to %class.btCollisionAlgorithm*
  store %class.btCollisionAlgorithm* %18, %class.btCollisionAlgorithm** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else, %if.then
  %19 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  %20 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %retval, align 4
  ret %class.btCollisionAlgorithm* %20
}

declare %class.btSoftRigidCollisionAlgorithm* @_ZN29btSoftRigidCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_b(%class.btSoftRigidCollisionAlgorithm* returned, %class.btPersistentManifold*, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8), %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1 zeroext) unnamed_addr #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncD0Ev(%"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* %this, %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"*, %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %call = call %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* (%"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"*)*)(%"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* %this1) #8
  %0 = bitcast %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %mem = alloca i8*, align 4
  store %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* %this, %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %this1 = load %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"*, %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %1, i32 0, i32 0
  %2 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4, !tbaa !33
  %3 = bitcast %class.btDispatcher* %2 to i8* (%class.btDispatcher*, i32)***
  %vtable = load i8* (%class.btDispatcher*, i32)**, i8* (%class.btDispatcher*, i32)*** %3, align 4, !tbaa !6
  %vfn = getelementptr inbounds i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vtable, i64 14
  %4 = load i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vfn, align 4
  %call = call i8* %4(%class.btDispatcher* %2, i32 156)
  store i8* %call, i8** %mem, align 4, !tbaa !2
  %5 = load i8*, i8** %mem, align 4, !tbaa !2
  %6 = bitcast i8* %5 to %class.btSoftBodyConcaveCollisionAlgorithm*
  %7 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call2 = call %class.btSoftBodyConcaveCollisionAlgorithm* @_ZN35btSoftBodyConcaveCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b(%class.btSoftBodyConcaveCollisionAlgorithm* %6, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %7, %struct.btCollisionObjectWrapper* %8, %struct.btCollisionObjectWrapper* %9, i1 zeroext false)
  %10 = bitcast %class.btSoftBodyConcaveCollisionAlgorithm* %6 to %class.btCollisionAlgorithm*
  %11 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret %class.btCollisionAlgorithm* %10
}

declare %class.btSoftBodyConcaveCollisionAlgorithm* @_ZN35btSoftBodyConcaveCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b(%class.btSoftBodyConcaveCollisionAlgorithm* returned, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8), %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1 zeroext) unnamed_addr #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncD0Ev(%"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"*, align 4
  store %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* %this, %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"*, %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  %call = call %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* (%"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"*)*)(%"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* %this1) #8
  %0 = bitcast %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %mem = alloca i8*, align 4
  store %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* %this, %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %this1 = load %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"*, %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  %0 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %1, i32 0, i32 0
  %2 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4, !tbaa !33
  %3 = bitcast %class.btDispatcher* %2 to i8* (%class.btDispatcher*, i32)***
  %vtable = load i8* (%class.btDispatcher*, i32)**, i8* (%class.btDispatcher*, i32)*** %3, align 4, !tbaa !6
  %vfn = getelementptr inbounds i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vtable, i64 14
  %4 = load i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vfn, align 4
  %call = call i8* %4(%class.btDispatcher* %2, i32 156)
  store i8* %call, i8** %mem, align 4, !tbaa !2
  %5 = load i8*, i8** %mem, align 4, !tbaa !2
  %6 = bitcast i8* %5 to %class.btSoftBodyConcaveCollisionAlgorithm*
  %7 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call2 = call %class.btSoftBodyConcaveCollisionAlgorithm* @_ZN35btSoftBodyConcaveCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b(%class.btSoftBodyConcaveCollisionAlgorithm* %6, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %7, %struct.btCollisionObjectWrapper* %8, %struct.btCollisionObjectWrapper* %9, i1 zeroext true)
  %10 = bitcast %class.btSoftBodyConcaveCollisionAlgorithm* %6 to %class.btCollisionAlgorithm*
  %11 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret %class.btCollisionAlgorithm* %10
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind }
attributes #9 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{!9, !3, i64 92}
!9 = !{!"_ZTS41btSoftBodyRigidBodyCollisionConfiguration", !3, i64 92, !3, i64 96, !3, i64 100, !3, i64 104, !3, i64 108}
!10 = !{!9, !3, i64 96}
!11 = !{!9, !3, i64 100}
!12 = !{!13, !14, i64 4}
!13 = !{!"_ZTS30btCollisionAlgorithmCreateFunc", !14, i64 4}
!14 = !{!"bool", !4, i64 0}
!15 = !{!9, !3, i64 104}
!16 = !{!9, !3, i64 108}
!17 = !{!18, !14, i64 20}
!18 = !{!"_ZTS31btDefaultCollisionConfiguration", !19, i64 4, !3, i64 8, !14, i64 12, !3, i64 16, !14, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !3, i64 44, !3, i64 48, !3, i64 52, !3, i64 56, !3, i64 60, !3, i64 64, !3, i64 68, !3, i64 72, !3, i64 76, !3, i64 80, !3, i64 84, !3, i64 88}
!19 = !{!"int", !4, i64 0}
!20 = !{i8 0, i8 2}
!21 = !{!18, !3, i64 16}
!22 = !{!19, !19, i64 0}
!23 = !{!24, !19, i64 12}
!24 = !{!"_ZTS34btDefaultCollisionConstructionInfo", !3, i64 0, !3, i64 4, !19, i64 8, !19, i64 12, !19, i64 16, !19, i64 20}
!25 = !{!26, !19, i64 0}
!26 = !{!"_ZTS15btPoolAllocator", !19, i64 0, !19, i64 4, !19, i64 8, !3, i64 12, !3, i64 16}
!27 = !{!26, !3, i64 16}
!28 = !{!26, !19, i64 4}
!29 = !{!26, !3, i64 12}
!30 = !{!26, !19, i64 8}
!31 = !{!18, !3, i64 8}
!32 = !{!18, !3, i64 24}
!33 = !{!34, !3, i64 0}
!34 = !{!"_ZTS36btCollisionAlgorithmConstructionInfo", !3, i64 0, !3, i64 4}
