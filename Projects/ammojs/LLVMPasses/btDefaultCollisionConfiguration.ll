; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionDispatch/btDefaultCollisionConfiguration.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionDispatch/btDefaultCollisionConfiguration.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btDefaultCollisionConfiguration = type { %class.btCollisionConfiguration, i32, %class.btPoolAllocator*, i8, %class.btPoolAllocator*, i8, %class.btVoronoiSimplexSolver*, %class.btConvexPenetrationDepthSolver*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc* }
%class.btCollisionConfiguration = type { i32 (...)** }
%class.btPoolAllocator = type { i32, i32, i32, i8*, i8* }
%class.btVoronoiSimplexSolver = type <{ i32, [5 x %class.btVector3], [5 x %class.btVector3], [5 x %class.btVector3], %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, i8, [3 x i8], %struct.btSubSimplexClosestResult, i8, [3 x i8] }>
%class.btVector3 = type { [4 x float] }
%struct.btSubSimplexClosestResult = type <{ %class.btVector3, %struct.btUsageBitfield, [2 x i8], [4 x float], i8, [3 x i8] }>
%struct.btUsageBitfield = type { i8, i8 }
%class.btConvexPenetrationDepthSolver = type { i32 (...)** }
%struct.btCollisionAlgorithmCreateFunc = type <{ i32 (...)**, i8, [3 x i8] }>
%struct.btDefaultCollisionConstructionInfo = type { %class.btPoolAllocator*, %class.btPoolAllocator*, i32, i32, i32, i32 }
%class.btGjkEpaPenetrationDepthSolver = type { %class.btConvexPenetrationDepthSolver }
%class.btMinkowskiPenetrationDepthSolver = type { %class.btConvexPenetrationDepthSolver }
%"struct.btConvexConvexAlgorithm::CreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, %class.btConvexPenetrationDepthSolver*, %class.btVoronoiSimplexSolver*, i32, i32 }
%struct.btCollisionAlgorithmCreateFunc.base = type <{ i32 (...)**, i8 }>
%"struct.btConvexConcaveCollisionAlgorithm::CreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, [3 x i8] }
%"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, [3 x i8] }
%"struct.btCompoundCollisionAlgorithm::CreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, [3 x i8] }
%"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, [3 x i8] }
%"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, [3 x i8] }
%"struct.btEmptyAlgorithm::CreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, [3 x i8] }
%"struct.btSphereSphereCollisionAlgorithm::CreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, [3 x i8] }
%"struct.btSphereTriangleCollisionAlgorithm::CreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, [3 x i8] }
%"struct.btBoxBoxCollisionAlgorithm::CreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, [3 x i8] }
%"struct.btConvexPlaneCollisionAlgorithm::CreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, i32, i32 }
%class.btCollisionAlgorithm = type { i32 (...)**, %class.btDispatcher* }
%class.btDispatcher = type { i32 (...)** }
%struct.btCollisionAlgorithmConstructionInfo = type { %class.btDispatcher*, %class.btPersistentManifold* }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, i32, i32, i32, i32, i8*, i8, float, float, float, float, float, float, float, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%union.anon = type { i8* }
%struct.btCollisionObjectWrapper = type { %struct.btCollisionObjectWrapper*, %class.btCollisionShape*, %class.btCollisionObject*, %class.btTransform*, i32, i32 }
%class.btConvexConcaveCollisionAlgorithm = type { %class.btActivatingCollisionAlgorithm, i8, %class.btConvexTriangleCallback }
%class.btActivatingCollisionAlgorithm = type { %class.btCollisionAlgorithm }
%class.btConvexTriangleCallback = type { %class.btTriangleCallback, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btVector3, %class.btVector3, %class.btManifoldResult*, %class.btDispatcher*, %struct.btDispatcherInfo*, float, i32, %class.btPersistentManifold* }
%class.btTriangleCallback = type { i32 (...)** }
%class.btManifoldResult = type { %"struct.btDiscreteCollisionDetectorInterface::Result", %class.btPersistentManifold*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i32, i32, i32, i32 }
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btIDebugDraw = type opaque
%class.btCompoundCollisionAlgorithm = type { %class.btActivatingCollisionAlgorithm, %class.btAlignedObjectArray, i8, %class.btPersistentManifold*, i8, i32 }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionAlgorithm**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btCompoundCompoundCollisionAlgorithm = type { %class.btActivatingCollisionAlgorithm, %class.btHashedSimplePairCache*, %class.btAlignedObjectArray.0, %class.btPersistentManifold*, i8, i32, i32 }
%class.btHashedSimplePairCache = type { i32 (...)**, %class.btAlignedObjectArray.0, i8, [3 x i8], %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5 }
%class.btAlignedObjectArray.5 = type <{ %class.btAlignedAllocator.6, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.6 = type { i8 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.btSimplePair*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btSimplePair = type { i32, i32, %union.anon.3 }
%union.anon.3 = type { i8* }
%class.btEmptyAlgorithm = type { %class.btCollisionAlgorithm }
%class.btSphereSphereCollisionAlgorithm = type { %class.btActivatingCollisionAlgorithm, i8, %class.btPersistentManifold* }
%class.btSphereTriangleCollisionAlgorithm = type <{ %class.btActivatingCollisionAlgorithm, i8, [3 x i8], %class.btPersistentManifold*, i8, [3 x i8] }>
%class.btBoxBoxCollisionAlgorithm = type { %class.btActivatingCollisionAlgorithm, i8, %class.btPersistentManifold* }
%class.btConvexPlaneCollisionAlgorithm = type { %class.btCollisionAlgorithm, i8, %class.btPersistentManifold*, i8, i32, i32 }

$_ZN24btCollisionConfigurationC2Ev = comdat any

$_ZN22btVoronoiSimplexSolvernwEmPv = comdat any

$_ZN22btVoronoiSimplexSolverC2Ev = comdat any

$_ZN30btGjkEpaPenetrationDepthSolverC2Ev = comdat any

$_ZN33btMinkowskiPenetrationDepthSolverC2Ev = comdat any

$_ZN33btConvexConcaveCollisionAlgorithm10CreateFuncC2Ev = comdat any

$_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncC2Ev = comdat any

$_ZN28btCompoundCollisionAlgorithm10CreateFuncC2Ev = comdat any

$_ZN36btCompoundCompoundCollisionAlgorithm10CreateFuncC2Ev = comdat any

$_ZN28btCompoundCollisionAlgorithm17SwappedCreateFuncC2Ev = comdat any

$_ZN16btEmptyAlgorithm10CreateFuncC2Ev = comdat any

$_ZN32btSphereSphereCollisionAlgorithm10CreateFuncC2Ev = comdat any

$_ZN34btSphereTriangleCollisionAlgorithm10CreateFuncC2Ev = comdat any

$_ZN26btBoxBoxCollisionAlgorithm10CreateFuncC2Ev = comdat any

$_ZN31btConvexPlaneCollisionAlgorithm10CreateFuncC2Ev = comdat any

$_Z5btMaxIiERKT_S2_S2_ = comdat any

$_ZN15btPoolAllocatorC2Eii = comdat any

$_ZN15btPoolAllocatorD2Ev = comdat any

$_ZN17btBroadphaseProxy8isConvexEi = comdat any

$_ZN17btBroadphaseProxy9isConcaveEi = comdat any

$_ZN17btBroadphaseProxy10isCompoundEi = comdat any

$_ZN31btDefaultCollisionConfiguration25getPersistentManifoldPoolEv = comdat any

$_ZN31btDefaultCollisionConfiguration25getCollisionAlgorithmPoolEv = comdat any

$_ZN31btDefaultCollisionConfiguration16getSimplexSolverEv = comdat any

$_ZN24btCollisionConfigurationD2Ev = comdat any

$_ZN24btCollisionConfigurationD0Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN25btSubSimplexClosestResultC2Ev = comdat any

$_ZN15btUsageBitfieldC2Ev = comdat any

$_ZN15btUsageBitfield5resetEv = comdat any

$_ZN30btConvexPenetrationDepthSolverC2Ev = comdat any

$_ZN30btConvexPenetrationDepthSolverD2Ev = comdat any

$_ZN30btConvexPenetrationDepthSolverD0Ev = comdat any

$_ZN30btCollisionAlgorithmCreateFuncC2Ev = comdat any

$_ZN30btCollisionAlgorithmCreateFuncD2Ev = comdat any

$_ZN33btConvexConcaveCollisionAlgorithm10CreateFuncD0Ev = comdat any

$_ZN33btConvexConcaveCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = comdat any

$_ZN30btCollisionAlgorithmCreateFuncD0Ev = comdat any

$_ZN30btCollisionAlgorithmCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS4_ = comdat any

$_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncD0Ev = comdat any

$_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = comdat any

$_ZN28btCompoundCollisionAlgorithm10CreateFuncD0Ev = comdat any

$_ZN28btCompoundCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = comdat any

$_ZN36btCompoundCompoundCollisionAlgorithm10CreateFuncD0Ev = comdat any

$_ZN36btCompoundCompoundCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = comdat any

$_ZN28btCompoundCollisionAlgorithm17SwappedCreateFuncD0Ev = comdat any

$_ZN28btCompoundCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = comdat any

$_ZN16btEmptyAlgorithm10CreateFuncD0Ev = comdat any

$_ZN16btEmptyAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = comdat any

$_ZN32btSphereSphereCollisionAlgorithm10CreateFuncD0Ev = comdat any

$_ZN32btSphereSphereCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = comdat any

$_ZN34btSphereTriangleCollisionAlgorithm10CreateFuncD0Ev = comdat any

$_ZN34btSphereTriangleCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = comdat any

$_ZN26btBoxBoxCollisionAlgorithm10CreateFuncD0Ev = comdat any

$_ZN26btBoxBoxCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = comdat any

$_ZN31btConvexPlaneCollisionAlgorithm10CreateFuncD0Ev = comdat any

$_ZN31btConvexPlaneCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = comdat any

$_ZTS24btCollisionConfiguration = comdat any

$_ZTI24btCollisionConfiguration = comdat any

$_ZTV24btCollisionConfiguration = comdat any

$_ZTV30btConvexPenetrationDepthSolver = comdat any

$_ZTS30btConvexPenetrationDepthSolver = comdat any

$_ZTI30btConvexPenetrationDepthSolver = comdat any

$_ZTVN33btConvexConcaveCollisionAlgorithm10CreateFuncE = comdat any

$_ZTSN33btConvexConcaveCollisionAlgorithm10CreateFuncE = comdat any

$_ZTS30btCollisionAlgorithmCreateFunc = comdat any

$_ZTI30btCollisionAlgorithmCreateFunc = comdat any

$_ZTIN33btConvexConcaveCollisionAlgorithm10CreateFuncE = comdat any

$_ZTV30btCollisionAlgorithmCreateFunc = comdat any

$_ZTVN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE = comdat any

$_ZTSN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE = comdat any

$_ZTIN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE = comdat any

$_ZTVN28btCompoundCollisionAlgorithm10CreateFuncE = comdat any

$_ZTSN28btCompoundCollisionAlgorithm10CreateFuncE = comdat any

$_ZTIN28btCompoundCollisionAlgorithm10CreateFuncE = comdat any

$_ZTVN36btCompoundCompoundCollisionAlgorithm10CreateFuncE = comdat any

$_ZTSN36btCompoundCompoundCollisionAlgorithm10CreateFuncE = comdat any

$_ZTIN36btCompoundCompoundCollisionAlgorithm10CreateFuncE = comdat any

$_ZTVN28btCompoundCollisionAlgorithm17SwappedCreateFuncE = comdat any

$_ZTSN28btCompoundCollisionAlgorithm17SwappedCreateFuncE = comdat any

$_ZTIN28btCompoundCollisionAlgorithm17SwappedCreateFuncE = comdat any

$_ZTVN16btEmptyAlgorithm10CreateFuncE = comdat any

$_ZTSN16btEmptyAlgorithm10CreateFuncE = comdat any

$_ZTIN16btEmptyAlgorithm10CreateFuncE = comdat any

$_ZTVN32btSphereSphereCollisionAlgorithm10CreateFuncE = comdat any

$_ZTSN32btSphereSphereCollisionAlgorithm10CreateFuncE = comdat any

$_ZTIN32btSphereSphereCollisionAlgorithm10CreateFuncE = comdat any

$_ZTVN34btSphereTriangleCollisionAlgorithm10CreateFuncE = comdat any

$_ZTSN34btSphereTriangleCollisionAlgorithm10CreateFuncE = comdat any

$_ZTIN34btSphereTriangleCollisionAlgorithm10CreateFuncE = comdat any

$_ZTVN26btBoxBoxCollisionAlgorithm10CreateFuncE = comdat any

$_ZTSN26btBoxBoxCollisionAlgorithm10CreateFuncE = comdat any

$_ZTIN26btBoxBoxCollisionAlgorithm10CreateFuncE = comdat any

$_ZTVN31btConvexPlaneCollisionAlgorithm10CreateFuncE = comdat any

$_ZTSN31btConvexPlaneCollisionAlgorithm10CreateFuncE = comdat any

$_ZTIN31btConvexPlaneCollisionAlgorithm10CreateFuncE = comdat any

@_ZTV31btDefaultCollisionConfiguration = hidden unnamed_addr constant { [8 x i8*] } { [8 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI31btDefaultCollisionConfiguration to i8*), i8* bitcast (%class.btDefaultCollisionConfiguration* (%class.btDefaultCollisionConfiguration*)* @_ZN31btDefaultCollisionConfigurationD1Ev to i8*), i8* bitcast (void (%class.btDefaultCollisionConfiguration*)* @_ZN31btDefaultCollisionConfigurationD0Ev to i8*), i8* bitcast (%class.btPoolAllocator* (%class.btDefaultCollisionConfiguration*)* @_ZN31btDefaultCollisionConfiguration25getPersistentManifoldPoolEv to i8*), i8* bitcast (%class.btPoolAllocator* (%class.btDefaultCollisionConfiguration*)* @_ZN31btDefaultCollisionConfiguration25getCollisionAlgorithmPoolEv to i8*), i8* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%class.btDefaultCollisionConfiguration*, i32, i32)* @_ZN31btDefaultCollisionConfiguration31getCollisionAlgorithmCreateFuncEii to i8*), i8* bitcast (%class.btVoronoiSimplexSolver* (%class.btDefaultCollisionConfiguration*)* @_ZN31btDefaultCollisionConfiguration16getSimplexSolverEv to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS31btDefaultCollisionConfiguration = hidden constant [34 x i8] c"31btDefaultCollisionConfiguration\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS24btCollisionConfiguration = linkonce_odr hidden constant [27 x i8] c"24btCollisionConfiguration\00", comdat, align 1
@_ZTI24btCollisionConfiguration = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([27 x i8], [27 x i8]* @_ZTS24btCollisionConfiguration, i32 0, i32 0) }, comdat, align 4
@_ZTI31btDefaultCollisionConfiguration = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([34 x i8], [34 x i8]* @_ZTS31btDefaultCollisionConfiguration, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI24btCollisionConfiguration to i8*) }, align 4
@_ZTV24btCollisionConfiguration = linkonce_odr hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI24btCollisionConfiguration to i8*), i8* bitcast (%class.btCollisionConfiguration* (%class.btCollisionConfiguration*)* @_ZN24btCollisionConfigurationD2Ev to i8*), i8* bitcast (void (%class.btCollisionConfiguration*)* @_ZN24btCollisionConfigurationD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTV30btGjkEpaPenetrationDepthSolver = external unnamed_addr constant { [5 x i8*] }, align 4
@_ZTV30btConvexPenetrationDepthSolver = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI30btConvexPenetrationDepthSolver to i8*), i8* bitcast (%class.btConvexPenetrationDepthSolver* (%class.btConvexPenetrationDepthSolver*)* @_ZN30btConvexPenetrationDepthSolverD2Ev to i8*), i8* bitcast (void (%class.btConvexPenetrationDepthSolver*)* @_ZN30btConvexPenetrationDepthSolverD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTS30btConvexPenetrationDepthSolver = linkonce_odr hidden constant [33 x i8] c"30btConvexPenetrationDepthSolver\00", comdat, align 1
@_ZTI30btConvexPenetrationDepthSolver = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([33 x i8], [33 x i8]* @_ZTS30btConvexPenetrationDepthSolver, i32 0, i32 0) }, comdat, align 4
@_ZTV33btMinkowskiPenetrationDepthSolver = external unnamed_addr constant { [5 x i8*] }, align 4
@_ZTVN33btConvexConcaveCollisionAlgorithm10CreateFuncE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN33btConvexConcaveCollisionAlgorithm10CreateFuncE to i8*), i8* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to i8*), i8* bitcast (void (%"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"*)* @_ZN33btConvexConcaveCollisionAlgorithm10CreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN33btConvexConcaveCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ to i8*)] }, comdat, align 4
@_ZTSN33btConvexConcaveCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant [50 x i8] c"N33btConvexConcaveCollisionAlgorithm10CreateFuncE\00", comdat, align 1
@_ZTS30btCollisionAlgorithmCreateFunc = linkonce_odr hidden constant [33 x i8] c"30btCollisionAlgorithmCreateFunc\00", comdat, align 1
@_ZTI30btCollisionAlgorithmCreateFunc = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([33 x i8], [33 x i8]* @_ZTS30btCollisionAlgorithmCreateFunc, i32 0, i32 0) }, comdat, align 4
@_ZTIN33btConvexConcaveCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([50 x i8], [50 x i8]* @_ZTSN33btConvexConcaveCollisionAlgorithm10CreateFuncE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*) }, comdat, align 4
@_ZTV30btCollisionAlgorithmCreateFunc = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*), i8* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to i8*), i8* bitcast (void (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN30btCollisionAlgorithmCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS4_ to i8*)] }, comdat, align 4
@_ZTVN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE to i8*), i8* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to i8*), i8* bitcast (void (%"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"*)* @_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ to i8*)] }, comdat, align 4
@_ZTSN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE = linkonce_odr hidden constant [57 x i8] c"N33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE\00", comdat, align 1
@_ZTIN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([57 x i8], [57 x i8]* @_ZTSN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*) }, comdat, align 4
@_ZTVN28btCompoundCollisionAlgorithm10CreateFuncE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN28btCompoundCollisionAlgorithm10CreateFuncE to i8*), i8* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to i8*), i8* bitcast (void (%"struct.btCompoundCollisionAlgorithm::CreateFunc"*)* @_ZN28btCompoundCollisionAlgorithm10CreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%"struct.btCompoundCollisionAlgorithm::CreateFunc"*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN28btCompoundCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ to i8*)] }, comdat, align 4
@_ZTSN28btCompoundCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant [45 x i8] c"N28btCompoundCollisionAlgorithm10CreateFuncE\00", comdat, align 1
@_ZTIN28btCompoundCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([45 x i8], [45 x i8]* @_ZTSN28btCompoundCollisionAlgorithm10CreateFuncE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*) }, comdat, align 4
@_ZTVN36btCompoundCompoundCollisionAlgorithm10CreateFuncE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN36btCompoundCompoundCollisionAlgorithm10CreateFuncE to i8*), i8* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to i8*), i8* bitcast (void (%"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"*)* @_ZN36btCompoundCompoundCollisionAlgorithm10CreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN36btCompoundCompoundCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ to i8*)] }, comdat, align 4
@_ZTSN36btCompoundCompoundCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant [53 x i8] c"N36btCompoundCompoundCollisionAlgorithm10CreateFuncE\00", comdat, align 1
@_ZTIN36btCompoundCompoundCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([53 x i8], [53 x i8]* @_ZTSN36btCompoundCompoundCollisionAlgorithm10CreateFuncE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*) }, comdat, align 4
@_ZTVN28btCompoundCollisionAlgorithm17SwappedCreateFuncE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN28btCompoundCollisionAlgorithm17SwappedCreateFuncE to i8*), i8* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to i8*), i8* bitcast (void (%"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"*)* @_ZN28btCompoundCollisionAlgorithm17SwappedCreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN28btCompoundCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ to i8*)] }, comdat, align 4
@_ZTSN28btCompoundCollisionAlgorithm17SwappedCreateFuncE = linkonce_odr hidden constant [52 x i8] c"N28btCompoundCollisionAlgorithm17SwappedCreateFuncE\00", comdat, align 1
@_ZTIN28btCompoundCollisionAlgorithm17SwappedCreateFuncE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([52 x i8], [52 x i8]* @_ZTSN28btCompoundCollisionAlgorithm17SwappedCreateFuncE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*) }, comdat, align 4
@_ZTVN16btEmptyAlgorithm10CreateFuncE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN16btEmptyAlgorithm10CreateFuncE to i8*), i8* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to i8*), i8* bitcast (void (%"struct.btEmptyAlgorithm::CreateFunc"*)* @_ZN16btEmptyAlgorithm10CreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%"struct.btEmptyAlgorithm::CreateFunc"*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN16btEmptyAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ to i8*)] }, comdat, align 4
@_ZTSN16btEmptyAlgorithm10CreateFuncE = linkonce_odr hidden constant [33 x i8] c"N16btEmptyAlgorithm10CreateFuncE\00", comdat, align 1
@_ZTIN16btEmptyAlgorithm10CreateFuncE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([33 x i8], [33 x i8]* @_ZTSN16btEmptyAlgorithm10CreateFuncE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*) }, comdat, align 4
@_ZTVN32btSphereSphereCollisionAlgorithm10CreateFuncE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN32btSphereSphereCollisionAlgorithm10CreateFuncE to i8*), i8* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to i8*), i8* bitcast (void (%"struct.btSphereSphereCollisionAlgorithm::CreateFunc"*)* @_ZN32btSphereSphereCollisionAlgorithm10CreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%"struct.btSphereSphereCollisionAlgorithm::CreateFunc"*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN32btSphereSphereCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ to i8*)] }, comdat, align 4
@_ZTSN32btSphereSphereCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant [49 x i8] c"N32btSphereSphereCollisionAlgorithm10CreateFuncE\00", comdat, align 1
@_ZTIN32btSphereSphereCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([49 x i8], [49 x i8]* @_ZTSN32btSphereSphereCollisionAlgorithm10CreateFuncE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*) }, comdat, align 4
@_ZTVN34btSphereTriangleCollisionAlgorithm10CreateFuncE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN34btSphereTriangleCollisionAlgorithm10CreateFuncE to i8*), i8* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to i8*), i8* bitcast (void (%"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"*)* @_ZN34btSphereTriangleCollisionAlgorithm10CreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN34btSphereTriangleCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ to i8*)] }, comdat, align 4
@_ZTSN34btSphereTriangleCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant [51 x i8] c"N34btSphereTriangleCollisionAlgorithm10CreateFuncE\00", comdat, align 1
@_ZTIN34btSphereTriangleCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([51 x i8], [51 x i8]* @_ZTSN34btSphereTriangleCollisionAlgorithm10CreateFuncE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*) }, comdat, align 4
@_ZTVN26btBoxBoxCollisionAlgorithm10CreateFuncE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN26btBoxBoxCollisionAlgorithm10CreateFuncE to i8*), i8* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to i8*), i8* bitcast (void (%"struct.btBoxBoxCollisionAlgorithm::CreateFunc"*)* @_ZN26btBoxBoxCollisionAlgorithm10CreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%"struct.btBoxBoxCollisionAlgorithm::CreateFunc"*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN26btBoxBoxCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ to i8*)] }, comdat, align 4
@_ZTSN26btBoxBoxCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant [43 x i8] c"N26btBoxBoxCollisionAlgorithm10CreateFuncE\00", comdat, align 1
@_ZTIN26btBoxBoxCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([43 x i8], [43 x i8]* @_ZTSN26btBoxBoxCollisionAlgorithm10CreateFuncE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*) }, comdat, align 4
@_ZTVN31btConvexPlaneCollisionAlgorithm10CreateFuncE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN31btConvexPlaneCollisionAlgorithm10CreateFuncE to i8*), i8* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to i8*), i8* bitcast (void (%"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*)* @_ZN31btConvexPlaneCollisionAlgorithm10CreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN31btConvexPlaneCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ to i8*)] }, comdat, align 4
@_ZTSN31btConvexPlaneCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant [48 x i8] c"N31btConvexPlaneCollisionAlgorithm10CreateFuncE\00", comdat, align 1
@_ZTIN31btConvexPlaneCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([48 x i8], [48 x i8]* @_ZTSN31btConvexPlaneCollisionAlgorithm10CreateFuncE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*) }, comdat, align 4

@_ZN31btDefaultCollisionConfigurationC1ERK34btDefaultCollisionConstructionInfo = hidden unnamed_addr alias %class.btDefaultCollisionConfiguration* (%class.btDefaultCollisionConfiguration*, %struct.btDefaultCollisionConstructionInfo*), %class.btDefaultCollisionConfiguration* (%class.btDefaultCollisionConfiguration*, %struct.btDefaultCollisionConstructionInfo*)* @_ZN31btDefaultCollisionConfigurationC2ERK34btDefaultCollisionConstructionInfo
@_ZN31btDefaultCollisionConfigurationD1Ev = hidden unnamed_addr alias %class.btDefaultCollisionConfiguration* (%class.btDefaultCollisionConfiguration*), %class.btDefaultCollisionConfiguration* (%class.btDefaultCollisionConfiguration*)* @_ZN31btDefaultCollisionConfigurationD2Ev

define hidden %class.btDefaultCollisionConfiguration* @_ZN31btDefaultCollisionConfigurationC2ERK34btDefaultCollisionConstructionInfo(%class.btDefaultCollisionConfiguration* returned %this, %struct.btDefaultCollisionConstructionInfo* nonnull align 4 dereferenceable(24) %constructionInfo) unnamed_addr #0 {
entry:
  %retval = alloca %class.btDefaultCollisionConfiguration*, align 4
  %this.addr = alloca %class.btDefaultCollisionConfiguration*, align 4
  %constructionInfo.addr = alloca %struct.btDefaultCollisionConstructionInfo*, align 4
  %mem = alloca i8*, align 4
  %maxSize = alloca i32, align 4
  %maxSize2 = alloca i32, align 4
  %maxSize3 = alloca i32, align 4
  %sl = alloca i32, align 4
  %collisionAlgorithmMaxElementSize = alloca i32, align 4
  %mem50 = alloca i8*, align 4
  %mem61 = alloca i8*, align 4
  store %class.btDefaultCollisionConfiguration* %this, %class.btDefaultCollisionConfiguration** %this.addr, align 4, !tbaa !2
  store %struct.btDefaultCollisionConstructionInfo* %constructionInfo, %struct.btDefaultCollisionConstructionInfo** %constructionInfo.addr, align 4, !tbaa !2
  %this1 = load %class.btDefaultCollisionConfiguration*, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  store %class.btDefaultCollisionConfiguration* %this1, %class.btDefaultCollisionConfiguration** %retval, align 4
  %0 = bitcast %class.btDefaultCollisionConfiguration* %this1 to %class.btCollisionConfiguration*
  %call = call %class.btCollisionConfiguration* @_ZN24btCollisionConfigurationC2Ev(%class.btCollisionConfiguration* %0) #8
  %1 = bitcast %class.btDefaultCollisionConfiguration* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [8 x i8*] }, { [8 x i8*] }* @_ZTV31btDefaultCollisionConfiguration, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %2 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %call2 = call i8* @_Z22btAlignedAllocInternalmi(i32 360, i32 16)
  store i8* %call2, i8** %mem, align 4, !tbaa !2
  %3 = load i8*, i8** %mem, align 4, !tbaa !2
  %call3 = call i8* @_ZN22btVoronoiSimplexSolvernwEmPv(i32 360, i8* %3)
  %4 = bitcast i8* %call3 to %class.btVoronoiSimplexSolver*
  %call4 = call %class.btVoronoiSimplexSolver* @_ZN22btVoronoiSimplexSolverC2Ev(%class.btVoronoiSimplexSolver* %4)
  %m_simplexSolver = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 6
  store %class.btVoronoiSimplexSolver* %4, %class.btVoronoiSimplexSolver** %m_simplexSolver, align 4, !tbaa !8
  %5 = load %struct.btDefaultCollisionConstructionInfo*, %struct.btDefaultCollisionConstructionInfo** %constructionInfo.addr, align 4, !tbaa !2
  %m_useEpaPenetrationAlgorithm = getelementptr inbounds %struct.btDefaultCollisionConstructionInfo, %struct.btDefaultCollisionConstructionInfo* %5, i32 0, i32 5
  %6 = load i32, i32* %m_useEpaPenetrationAlgorithm, align 4, !tbaa !12
  %tobool = icmp ne i32 %6, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %call5 = call i8* @_Z22btAlignedAllocInternalmi(i32 4, i32 16)
  store i8* %call5, i8** %mem, align 4, !tbaa !2
  %7 = load i8*, i8** %mem, align 4, !tbaa !2
  %8 = bitcast i8* %7 to %class.btGjkEpaPenetrationDepthSolver*
  %call6 = call %class.btGjkEpaPenetrationDepthSolver* @_ZN30btGjkEpaPenetrationDepthSolverC2Ev(%class.btGjkEpaPenetrationDepthSolver* %8)
  %9 = bitcast %class.btGjkEpaPenetrationDepthSolver* %8 to %class.btConvexPenetrationDepthSolver*
  %m_pdSolver = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 7
  store %class.btConvexPenetrationDepthSolver* %9, %class.btConvexPenetrationDepthSolver** %m_pdSolver, align 4, !tbaa !14
  br label %if.end

if.else:                                          ; preds = %entry
  %call7 = call i8* @_Z22btAlignedAllocInternalmi(i32 4, i32 16)
  store i8* %call7, i8** %mem, align 4, !tbaa !2
  %10 = load i8*, i8** %mem, align 4, !tbaa !2
  %11 = bitcast i8* %10 to %class.btMinkowskiPenetrationDepthSolver*
  %call8 = call %class.btMinkowskiPenetrationDepthSolver* @_ZN33btMinkowskiPenetrationDepthSolverC2Ev(%class.btMinkowskiPenetrationDepthSolver* %11) #8
  %12 = bitcast %class.btMinkowskiPenetrationDepthSolver* %11 to %class.btConvexPenetrationDepthSolver*
  %m_pdSolver9 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 7
  store %class.btConvexPenetrationDepthSolver* %12, %class.btConvexPenetrationDepthSolver** %m_pdSolver9, align 4, !tbaa !14
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %call10 = call i8* @_Z22btAlignedAllocInternalmi(i32 24, i32 16)
  store i8* %call10, i8** %mem, align 4, !tbaa !2
  %13 = load i8*, i8** %mem, align 4, !tbaa !2
  %14 = bitcast i8* %13 to %"struct.btConvexConvexAlgorithm::CreateFunc"*
  %m_simplexSolver11 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 6
  %15 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %m_simplexSolver11, align 4, !tbaa !8
  %m_pdSolver12 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 7
  %16 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %m_pdSolver12, align 4, !tbaa !14
  %call13 = call %"struct.btConvexConvexAlgorithm::CreateFunc"* @_ZN23btConvexConvexAlgorithm10CreateFuncC1EP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver(%"struct.btConvexConvexAlgorithm::CreateFunc"* %14, %class.btVoronoiSimplexSolver* %15, %class.btConvexPenetrationDepthSolver* %16)
  %17 = bitcast %"struct.btConvexConvexAlgorithm::CreateFunc"* %14 to %struct.btCollisionAlgorithmCreateFunc*
  %m_convexConvexCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 8
  store %struct.btCollisionAlgorithmCreateFunc* %17, %struct.btCollisionAlgorithmCreateFunc** %m_convexConvexCreateFunc, align 4, !tbaa !15
  %call14 = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call14, i8** %mem, align 4, !tbaa !2
  %18 = load i8*, i8** %mem, align 4, !tbaa !2
  %19 = bitcast i8* %18 to %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"*
  %call15 = call %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* @_ZN33btConvexConcaveCollisionAlgorithm10CreateFuncC2Ev(%"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* %19)
  %20 = bitcast %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* %19 to %struct.btCollisionAlgorithmCreateFunc*
  %m_convexConcaveCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 9
  store %struct.btCollisionAlgorithmCreateFunc* %20, %struct.btCollisionAlgorithmCreateFunc** %m_convexConcaveCreateFunc, align 4, !tbaa !16
  %call16 = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call16, i8** %mem, align 4, !tbaa !2
  %21 = load i8*, i8** %mem, align 4, !tbaa !2
  %22 = bitcast i8* %21 to %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"*
  %call17 = call %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* @_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncC2Ev(%"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* %22)
  %23 = bitcast %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* %22 to %struct.btCollisionAlgorithmCreateFunc*
  %m_swappedConvexConcaveCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 10
  store %struct.btCollisionAlgorithmCreateFunc* %23, %struct.btCollisionAlgorithmCreateFunc** %m_swappedConvexConcaveCreateFunc, align 4, !tbaa !17
  %call18 = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call18, i8** %mem, align 4, !tbaa !2
  %24 = load i8*, i8** %mem, align 4, !tbaa !2
  %25 = bitcast i8* %24 to %"struct.btCompoundCollisionAlgorithm::CreateFunc"*
  %call19 = call %"struct.btCompoundCollisionAlgorithm::CreateFunc"* @_ZN28btCompoundCollisionAlgorithm10CreateFuncC2Ev(%"struct.btCompoundCollisionAlgorithm::CreateFunc"* %25)
  %26 = bitcast %"struct.btCompoundCollisionAlgorithm::CreateFunc"* %25 to %struct.btCollisionAlgorithmCreateFunc*
  %m_compoundCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 11
  store %struct.btCollisionAlgorithmCreateFunc* %26, %struct.btCollisionAlgorithmCreateFunc** %m_compoundCreateFunc, align 4, !tbaa !18
  %call20 = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call20, i8** %mem, align 4, !tbaa !2
  %27 = load i8*, i8** %mem, align 4, !tbaa !2
  %28 = bitcast i8* %27 to %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"*
  %call21 = call %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* @_ZN36btCompoundCompoundCollisionAlgorithm10CreateFuncC2Ev(%"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* %28)
  %29 = bitcast %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* %28 to %struct.btCollisionAlgorithmCreateFunc*
  %m_compoundCompoundCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 12
  store %struct.btCollisionAlgorithmCreateFunc* %29, %struct.btCollisionAlgorithmCreateFunc** %m_compoundCompoundCreateFunc, align 4, !tbaa !19
  %call22 = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call22, i8** %mem, align 4, !tbaa !2
  %30 = load i8*, i8** %mem, align 4, !tbaa !2
  %31 = bitcast i8* %30 to %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"*
  %call23 = call %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* @_ZN28btCompoundCollisionAlgorithm17SwappedCreateFuncC2Ev(%"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* %31)
  %32 = bitcast %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* %31 to %struct.btCollisionAlgorithmCreateFunc*
  %m_swappedCompoundCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 13
  store %struct.btCollisionAlgorithmCreateFunc* %32, %struct.btCollisionAlgorithmCreateFunc** %m_swappedCompoundCreateFunc, align 4, !tbaa !20
  %call24 = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call24, i8** %mem, align 4, !tbaa !2
  %33 = load i8*, i8** %mem, align 4, !tbaa !2
  %34 = bitcast i8* %33 to %"struct.btEmptyAlgorithm::CreateFunc"*
  %call25 = call %"struct.btEmptyAlgorithm::CreateFunc"* @_ZN16btEmptyAlgorithm10CreateFuncC2Ev(%"struct.btEmptyAlgorithm::CreateFunc"* %34)
  %35 = bitcast %"struct.btEmptyAlgorithm::CreateFunc"* %34 to %struct.btCollisionAlgorithmCreateFunc*
  %m_emptyCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 14
  store %struct.btCollisionAlgorithmCreateFunc* %35, %struct.btCollisionAlgorithmCreateFunc** %m_emptyCreateFunc, align 4, !tbaa !21
  %call26 = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call26, i8** %mem, align 4, !tbaa !2
  %36 = load i8*, i8** %mem, align 4, !tbaa !2
  %37 = bitcast i8* %36 to %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"*
  %call27 = call %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* @_ZN32btSphereSphereCollisionAlgorithm10CreateFuncC2Ev(%"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* %37)
  %38 = bitcast %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* %37 to %struct.btCollisionAlgorithmCreateFunc*
  %m_sphereSphereCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 15
  store %struct.btCollisionAlgorithmCreateFunc* %38, %struct.btCollisionAlgorithmCreateFunc** %m_sphereSphereCF, align 4, !tbaa !22
  %call28 = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call28, i8** %mem, align 4, !tbaa !2
  %39 = load i8*, i8** %mem, align 4, !tbaa !2
  %40 = bitcast i8* %39 to %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"*
  %call29 = call %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* @_ZN34btSphereTriangleCollisionAlgorithm10CreateFuncC2Ev(%"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %40)
  %41 = bitcast %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %40 to %struct.btCollisionAlgorithmCreateFunc*
  %m_sphereTriangleCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 19
  store %struct.btCollisionAlgorithmCreateFunc* %41, %struct.btCollisionAlgorithmCreateFunc** %m_sphereTriangleCF, align 4, !tbaa !23
  %call30 = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call30, i8** %mem, align 4, !tbaa !2
  %42 = load i8*, i8** %mem, align 4, !tbaa !2
  %43 = bitcast i8* %42 to %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"*
  %call31 = call %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* @_ZN34btSphereTriangleCollisionAlgorithm10CreateFuncC2Ev(%"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %43)
  %44 = bitcast %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %43 to %struct.btCollisionAlgorithmCreateFunc*
  %m_triangleSphereCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 20
  store %struct.btCollisionAlgorithmCreateFunc* %44, %struct.btCollisionAlgorithmCreateFunc** %m_triangleSphereCF, align 4, !tbaa !24
  %m_triangleSphereCF32 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 20
  %45 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_triangleSphereCF32, align 4, !tbaa !24
  %m_swapped = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc, %struct.btCollisionAlgorithmCreateFunc* %45, i32 0, i32 1
  store i8 1, i8* %m_swapped, align 4, !tbaa !25
  %call33 = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call33, i8** %mem, align 4, !tbaa !2
  %46 = load i8*, i8** %mem, align 4, !tbaa !2
  %47 = bitcast i8* %46 to %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"*
  %call34 = call %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* @_ZN26btBoxBoxCollisionAlgorithm10CreateFuncC2Ev(%"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* %47)
  %48 = bitcast %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* %47 to %struct.btCollisionAlgorithmCreateFunc*
  %m_boxBoxCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 18
  store %struct.btCollisionAlgorithmCreateFunc* %48, %struct.btCollisionAlgorithmCreateFunc** %m_boxBoxCF, align 4, !tbaa !27
  %call35 = call i8* @_Z22btAlignedAllocInternalmi(i32 16, i32 16)
  store i8* %call35, i8** %mem, align 4, !tbaa !2
  %49 = load i8*, i8** %mem, align 4, !tbaa !2
  %50 = bitcast i8* %49 to %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*
  %call36 = call %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* @_ZN31btConvexPlaneCollisionAlgorithm10CreateFuncC2Ev(%"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %50)
  %51 = bitcast %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %50 to %struct.btCollisionAlgorithmCreateFunc*
  %m_convexPlaneCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 22
  store %struct.btCollisionAlgorithmCreateFunc* %51, %struct.btCollisionAlgorithmCreateFunc** %m_convexPlaneCF, align 4, !tbaa !28
  %call37 = call i8* @_Z22btAlignedAllocInternalmi(i32 16, i32 16)
  store i8* %call37, i8** %mem, align 4, !tbaa !2
  %52 = load i8*, i8** %mem, align 4, !tbaa !2
  %53 = bitcast i8* %52 to %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*
  %call38 = call %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* @_ZN31btConvexPlaneCollisionAlgorithm10CreateFuncC2Ev(%"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %53)
  %54 = bitcast %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %53 to %struct.btCollisionAlgorithmCreateFunc*
  %m_planeConvexCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 21
  store %struct.btCollisionAlgorithmCreateFunc* %54, %struct.btCollisionAlgorithmCreateFunc** %m_planeConvexCF, align 4, !tbaa !29
  %m_planeConvexCF39 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 21
  %55 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_planeConvexCF39, align 4, !tbaa !29
  %m_swapped40 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc, %struct.btCollisionAlgorithmCreateFunc* %55, i32 0, i32 1
  store i8 1, i8* %m_swapped40, align 4, !tbaa !25
  %56 = bitcast i32* %maxSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #8
  store i32 36, i32* %maxSize, align 4, !tbaa !30
  %57 = bitcast i32* %maxSize2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %57) #8
  store i32 80, i32* %maxSize2, align 4, !tbaa !30
  %58 = bitcast i32* %maxSize3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #8
  store i32 44, i32* %maxSize3, align 4, !tbaa !30
  %59 = bitcast i32* %sl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #8
  store i32 92, i32* %sl, align 4, !tbaa !30
  store i32 80, i32* %sl, align 4, !tbaa !30
  %60 = bitcast i32* %collisionAlgorithmMaxElementSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #8
  %61 = load %struct.btDefaultCollisionConstructionInfo*, %struct.btDefaultCollisionConstructionInfo** %constructionInfo.addr, align 4, !tbaa !2
  %m_customCollisionAlgorithmMaxElementSize = getelementptr inbounds %struct.btDefaultCollisionConstructionInfo, %struct.btDefaultCollisionConstructionInfo* %61, i32 0, i32 4
  %call41 = call nonnull align 4 dereferenceable(4) i32* @_Z5btMaxIiERKT_S2_S2_(i32* nonnull align 4 dereferenceable(4) %maxSize, i32* nonnull align 4 dereferenceable(4) %m_customCollisionAlgorithmMaxElementSize)
  %62 = load i32, i32* %call41, align 4, !tbaa !30
  store i32 %62, i32* %collisionAlgorithmMaxElementSize, align 4, !tbaa !30
  %call42 = call nonnull align 4 dereferenceable(4) i32* @_Z5btMaxIiERKT_S2_S2_(i32* nonnull align 4 dereferenceable(4) %collisionAlgorithmMaxElementSize, i32* nonnull align 4 dereferenceable(4) %maxSize2)
  %63 = load i32, i32* %call42, align 4, !tbaa !30
  store i32 %63, i32* %collisionAlgorithmMaxElementSize, align 4, !tbaa !30
  %call43 = call nonnull align 4 dereferenceable(4) i32* @_Z5btMaxIiERKT_S2_S2_(i32* nonnull align 4 dereferenceable(4) %collisionAlgorithmMaxElementSize, i32* nonnull align 4 dereferenceable(4) %maxSize3)
  %64 = load i32, i32* %call43, align 4, !tbaa !30
  store i32 %64, i32* %collisionAlgorithmMaxElementSize, align 4, !tbaa !30
  %65 = load %struct.btDefaultCollisionConstructionInfo*, %struct.btDefaultCollisionConstructionInfo** %constructionInfo.addr, align 4, !tbaa !2
  %m_persistentManifoldPool = getelementptr inbounds %struct.btDefaultCollisionConstructionInfo, %struct.btDefaultCollisionConstructionInfo* %65, i32 0, i32 0
  %66 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_persistentManifoldPool, align 4, !tbaa !31
  %tobool44 = icmp ne %class.btPoolAllocator* %66, null
  br i1 %tobool44, label %if.then45, label %if.else48

if.then45:                                        ; preds = %if.end
  %m_ownsPersistentManifoldPool = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 3
  store i8 0, i8* %m_ownsPersistentManifoldPool, align 4, !tbaa !32
  %67 = load %struct.btDefaultCollisionConstructionInfo*, %struct.btDefaultCollisionConstructionInfo** %constructionInfo.addr, align 4, !tbaa !2
  %m_persistentManifoldPool46 = getelementptr inbounds %struct.btDefaultCollisionConstructionInfo, %struct.btDefaultCollisionConstructionInfo* %67, i32 0, i32 0
  %68 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_persistentManifoldPool46, align 4, !tbaa !31
  %m_persistentManifoldPool47 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 2
  store %class.btPoolAllocator* %68, %class.btPoolAllocator** %m_persistentManifoldPool47, align 4, !tbaa !33
  br label %if.end54

if.else48:                                        ; preds = %if.end
  %m_ownsPersistentManifoldPool49 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 3
  store i8 1, i8* %m_ownsPersistentManifoldPool49, align 4, !tbaa !32
  %69 = bitcast i8** %mem50 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #8
  %call51 = call i8* @_Z22btAlignedAllocInternalmi(i32 20, i32 16)
  store i8* %call51, i8** %mem50, align 4, !tbaa !2
  %70 = load i8*, i8** %mem50, align 4, !tbaa !2
  %71 = bitcast i8* %70 to %class.btPoolAllocator*
  %72 = load %struct.btDefaultCollisionConstructionInfo*, %struct.btDefaultCollisionConstructionInfo** %constructionInfo.addr, align 4, !tbaa !2
  %m_defaultMaxPersistentManifoldPoolSize = getelementptr inbounds %struct.btDefaultCollisionConstructionInfo, %struct.btDefaultCollisionConstructionInfo* %72, i32 0, i32 2
  %73 = load i32, i32* %m_defaultMaxPersistentManifoldPoolSize, align 4, !tbaa !34
  %call52 = call %class.btPoolAllocator* @_ZN15btPoolAllocatorC2Eii(%class.btPoolAllocator* %71, i32 772, i32 %73)
  %m_persistentManifoldPool53 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 2
  store %class.btPoolAllocator* %71, %class.btPoolAllocator** %m_persistentManifoldPool53, align 4, !tbaa !33
  %74 = bitcast i8** %mem50 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #8
  br label %if.end54

if.end54:                                         ; preds = %if.else48, %if.then45
  %75 = load %struct.btDefaultCollisionConstructionInfo*, %struct.btDefaultCollisionConstructionInfo** %constructionInfo.addr, align 4, !tbaa !2
  %m_collisionAlgorithmPool = getelementptr inbounds %struct.btDefaultCollisionConstructionInfo, %struct.btDefaultCollisionConstructionInfo* %75, i32 0, i32 1
  %76 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_collisionAlgorithmPool, align 4, !tbaa !35
  %tobool55 = icmp ne %class.btPoolAllocator* %76, null
  br i1 %tobool55, label %if.then56, label %if.else59

if.then56:                                        ; preds = %if.end54
  %m_ownsCollisionAlgorithmPool = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 5
  store i8 0, i8* %m_ownsCollisionAlgorithmPool, align 4, !tbaa !36
  %77 = load %struct.btDefaultCollisionConstructionInfo*, %struct.btDefaultCollisionConstructionInfo** %constructionInfo.addr, align 4, !tbaa !2
  %m_collisionAlgorithmPool57 = getelementptr inbounds %struct.btDefaultCollisionConstructionInfo, %struct.btDefaultCollisionConstructionInfo* %77, i32 0, i32 1
  %78 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_collisionAlgorithmPool57, align 4, !tbaa !35
  %m_collisionAlgorithmPool58 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 4
  store %class.btPoolAllocator* %78, %class.btPoolAllocator** %m_collisionAlgorithmPool58, align 4, !tbaa !37
  br label %if.end65

if.else59:                                        ; preds = %if.end54
  %m_ownsCollisionAlgorithmPool60 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsCollisionAlgorithmPool60, align 4, !tbaa !36
  %79 = bitcast i8** %mem61 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #8
  %call62 = call i8* @_Z22btAlignedAllocInternalmi(i32 20, i32 16)
  store i8* %call62, i8** %mem61, align 4, !tbaa !2
  %80 = load i8*, i8** %mem61, align 4, !tbaa !2
  %81 = bitcast i8* %80 to %class.btPoolAllocator*
  %82 = load i32, i32* %collisionAlgorithmMaxElementSize, align 4, !tbaa !30
  %83 = load %struct.btDefaultCollisionConstructionInfo*, %struct.btDefaultCollisionConstructionInfo** %constructionInfo.addr, align 4, !tbaa !2
  %m_defaultMaxCollisionAlgorithmPoolSize = getelementptr inbounds %struct.btDefaultCollisionConstructionInfo, %struct.btDefaultCollisionConstructionInfo* %83, i32 0, i32 3
  %84 = load i32, i32* %m_defaultMaxCollisionAlgorithmPoolSize, align 4, !tbaa !38
  %call63 = call %class.btPoolAllocator* @_ZN15btPoolAllocatorC2Eii(%class.btPoolAllocator* %81, i32 %82, i32 %84)
  %m_collisionAlgorithmPool64 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 4
  store %class.btPoolAllocator* %81, %class.btPoolAllocator** %m_collisionAlgorithmPool64, align 4, !tbaa !37
  %85 = bitcast i8** %mem61 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #8
  br label %if.end65

if.end65:                                         ; preds = %if.else59, %if.then56
  %86 = bitcast i32* %collisionAlgorithmMaxElementSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #8
  %87 = bitcast i32* %sl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #8
  %88 = bitcast i32* %maxSize3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #8
  %89 = bitcast i32* %maxSize2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #8
  %90 = bitcast i32* %maxSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #8
  %91 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #8
  %92 = load %class.btDefaultCollisionConfiguration*, %class.btDefaultCollisionConfiguration** %retval, align 4
  ret %class.btDefaultCollisionConfiguration* %92
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionConfiguration* @_ZN24btCollisionConfigurationC2Ev(%class.btCollisionConfiguration* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionConfiguration*, align 4
  store %class.btCollisionConfiguration* %this, %class.btCollisionConfiguration** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionConfiguration*, %class.btCollisionConfiguration** %this.addr, align 4
  %0 = bitcast %class.btCollisionConfiguration* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV24btCollisionConfiguration, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  ret %class.btCollisionConfiguration* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN22btVoronoiSimplexSolvernwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !39
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

define linkonce_odr hidden %class.btVoronoiSimplexSolver* @_ZN22btVoronoiSimplexSolverC2Ev(%class.btVoronoiSimplexSolver* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btVoronoiSimplexSolver*, align 4
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  store %class.btVoronoiSimplexSolver* %this1, %class.btVoronoiSimplexSolver** %retval, align 4
  %m_simplexVectorW = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 5
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_simplexPointsP = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %array.begin2 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP, i32 0, i32 0
  %arrayctor.end3 = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin2, i32 5
  br label %arrayctor.loop4

arrayctor.loop4:                                  ; preds = %arrayctor.loop4, %arrayctor.cont
  %arrayctor.cur5 = phi %class.btVector3* [ %array.begin2, %arrayctor.cont ], [ %arrayctor.next7, %arrayctor.loop4 ]
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur5)
  %arrayctor.next7 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur5, i32 1
  %arrayctor.done8 = icmp eq %class.btVector3* %arrayctor.next7, %arrayctor.end3
  br i1 %arrayctor.done8, label %arrayctor.cont9, label %arrayctor.loop4

arrayctor.cont9:                                  ; preds = %arrayctor.loop4
  %m_simplexPointsQ = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %array.begin10 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ, i32 0, i32 0
  %arrayctor.end11 = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin10, i32 5
  br label %arrayctor.loop12

arrayctor.loop12:                                 ; preds = %arrayctor.loop12, %arrayctor.cont9
  %arrayctor.cur13 = phi %class.btVector3* [ %array.begin10, %arrayctor.cont9 ], [ %arrayctor.next15, %arrayctor.loop12 ]
  %call14 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur13)
  %arrayctor.next15 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur13, i32 1
  %arrayctor.done16 = icmp eq %class.btVector3* %arrayctor.next15, %arrayctor.end11
  br i1 %arrayctor.done16, label %arrayctor.cont17, label %arrayctor.loop12

arrayctor.cont17:                                 ; preds = %arrayctor.loop12
  %m_cachedP1 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 4
  %call18 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_cachedP1)
  %m_cachedP2 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 5
  %call19 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_cachedP2)
  %m_cachedV = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 6
  %call20 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_cachedV)
  %m_lastW = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 7
  %call21 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_lastW)
  %m_equalVertexThreshold = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 8
  store float 0x3F1A36E2E0000000, float* %m_equalVertexThreshold, align 4, !tbaa !41
  %m_cachedBC = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %call22 = call %struct.btSubSimplexClosestResult* @_ZN25btSubSimplexClosestResultC2Ev(%struct.btSubSimplexClosestResult* %m_cachedBC)
  %0 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %retval, align 4
  ret %class.btVoronoiSimplexSolver* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btGjkEpaPenetrationDepthSolver* @_ZN30btGjkEpaPenetrationDepthSolverC2Ev(%class.btGjkEpaPenetrationDepthSolver* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btGjkEpaPenetrationDepthSolver*, align 4
  store %class.btGjkEpaPenetrationDepthSolver* %this, %class.btGjkEpaPenetrationDepthSolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGjkEpaPenetrationDepthSolver*, %class.btGjkEpaPenetrationDepthSolver** %this.addr, align 4
  %0 = bitcast %class.btGjkEpaPenetrationDepthSolver* %this1 to %class.btConvexPenetrationDepthSolver*
  %call = call %class.btConvexPenetrationDepthSolver* @_ZN30btConvexPenetrationDepthSolverC2Ev(%class.btConvexPenetrationDepthSolver* %0) #8
  %1 = bitcast %class.btGjkEpaPenetrationDepthSolver* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV30btGjkEpaPenetrationDepthSolver, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  ret %class.btGjkEpaPenetrationDepthSolver* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btMinkowskiPenetrationDepthSolver* @_ZN33btMinkowskiPenetrationDepthSolverC2Ev(%class.btMinkowskiPenetrationDepthSolver* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btMinkowskiPenetrationDepthSolver*, align 4
  store %class.btMinkowskiPenetrationDepthSolver* %this, %class.btMinkowskiPenetrationDepthSolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMinkowskiPenetrationDepthSolver*, %class.btMinkowskiPenetrationDepthSolver** %this.addr, align 4
  %0 = bitcast %class.btMinkowskiPenetrationDepthSolver* %this1 to %class.btConvexPenetrationDepthSolver*
  %call = call %class.btConvexPenetrationDepthSolver* @_ZN30btConvexPenetrationDepthSolverC2Ev(%class.btConvexPenetrationDepthSolver* %0) #8
  %1 = bitcast %class.btMinkowskiPenetrationDepthSolver* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV33btMinkowskiPenetrationDepthSolver, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  ret %class.btMinkowskiPenetrationDepthSolver* %this1
}

declare %"struct.btConvexConvexAlgorithm::CreateFunc"* @_ZN23btConvexConvexAlgorithm10CreateFuncC1EP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver(%"struct.btConvexConvexAlgorithm::CreateFunc"* returned, %class.btVoronoiSimplexSolver*, %class.btConvexPenetrationDepthSolver*) unnamed_addr #3

; Function Attrs: inlinehint
define linkonce_odr hidden %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* @_ZN33btConvexConcaveCollisionAlgorithm10CreateFuncC2Ev(%"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* %this, %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"*, %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* %0)
  %1 = bitcast %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN33btConvexConcaveCollisionAlgorithm10CreateFuncE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  ret %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* @_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncC2Ev(%"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"*, align 4
  store %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* %this, %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"*, %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* %0)
  %1 = bitcast %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  ret %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %"struct.btCompoundCollisionAlgorithm::CreateFunc"* @_ZN28btCompoundCollisionAlgorithm10CreateFuncC2Ev(%"struct.btCompoundCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCompoundCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btCompoundCollisionAlgorithm::CreateFunc"* %this, %"struct.btCompoundCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btCompoundCollisionAlgorithm::CreateFunc"*, %"struct.btCompoundCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btCompoundCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* %0)
  %1 = bitcast %"struct.btCompoundCollisionAlgorithm::CreateFunc"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN28btCompoundCollisionAlgorithm10CreateFuncE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  ret %"struct.btCompoundCollisionAlgorithm::CreateFunc"* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* @_ZN36btCompoundCompoundCollisionAlgorithm10CreateFuncC2Ev(%"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* %this, %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"*, %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* %0)
  %1 = bitcast %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN36btCompoundCompoundCollisionAlgorithm10CreateFuncE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  ret %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* @_ZN28btCompoundCollisionAlgorithm17SwappedCreateFuncC2Ev(%"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"*, align 4
  store %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* %this, %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"*, %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* %0)
  %1 = bitcast %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN28btCompoundCollisionAlgorithm17SwappedCreateFuncE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  ret %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %"struct.btEmptyAlgorithm::CreateFunc"* @_ZN16btEmptyAlgorithm10CreateFuncC2Ev(%"struct.btEmptyAlgorithm::CreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btEmptyAlgorithm::CreateFunc"*, align 4
  store %"struct.btEmptyAlgorithm::CreateFunc"* %this, %"struct.btEmptyAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btEmptyAlgorithm::CreateFunc"*, %"struct.btEmptyAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btEmptyAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* %0)
  %1 = bitcast %"struct.btEmptyAlgorithm::CreateFunc"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN16btEmptyAlgorithm10CreateFuncE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  ret %"struct.btEmptyAlgorithm::CreateFunc"* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* @_ZN32btSphereSphereCollisionAlgorithm10CreateFuncC2Ev(%"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* %this, %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"*, %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* %0)
  %1 = bitcast %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN32btSphereSphereCollisionAlgorithm10CreateFuncE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  ret %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* @_ZN34btSphereTriangleCollisionAlgorithm10CreateFuncC2Ev(%"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %this, %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"*, %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* %0)
  %1 = bitcast %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN34btSphereTriangleCollisionAlgorithm10CreateFuncE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  ret %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* @_ZN26btBoxBoxCollisionAlgorithm10CreateFuncC2Ev(%"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* %this, %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"*, %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* %0)
  %1 = bitcast %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN26btBoxBoxCollisionAlgorithm10CreateFuncE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  ret %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* @_ZN31btConvexPlaneCollisionAlgorithm10CreateFuncC2Ev(%"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this, %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*, %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* %0)
  %1 = bitcast %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN31btConvexPlaneCollisionAlgorithm10CreateFuncE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_numPerturbationIterations = getelementptr inbounds %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc", %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this1, i32 0, i32 1
  store i32 1, i32* %m_numPerturbationIterations, align 4, !tbaa !48
  %m_minimumPointsPerturbationThreshold = getelementptr inbounds %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc", %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this1, i32 0, i32 2
  store i32 0, i32* %m_minimumPointsPerturbationThreshold, align 4, !tbaa !50
  ret %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_Z5btMaxIiERKT_S2_S2_(i32* nonnull align 4 dereferenceable(4) %a, i32* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca i32*, align 4
  %b.addr = alloca i32*, align 4
  store i32* %a, i32** %a.addr, align 4, !tbaa !2
  store i32* %b, i32** %b.addr, align 4, !tbaa !2
  %0 = load i32*, i32** %a.addr, align 4, !tbaa !2
  %1 = load i32, i32* %0, align 4, !tbaa !30
  %2 = load i32*, i32** %b.addr, align 4, !tbaa !2
  %3 = load i32, i32* %2, align 4, !tbaa !30
  %cmp = icmp sgt i32 %1, %3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load i32*, i32** %a.addr, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %entry
  %5 = load i32*, i32** %b.addr, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %4, %cond.true ], [ %5, %cond.false ]
  ret i32* %cond-lvalue
}

define linkonce_odr hidden %class.btPoolAllocator* @_ZN15btPoolAllocatorC2Eii(%class.btPoolAllocator* returned %this, i32 %elemSize, i32 %maxElements) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btPoolAllocator*, align 4
  %this.addr = alloca %class.btPoolAllocator*, align 4
  %elemSize.addr = alloca i32, align 4
  %maxElements.addr = alloca i32, align 4
  %p = alloca i8*, align 4
  %count = alloca i32, align 4
  store %class.btPoolAllocator* %this, %class.btPoolAllocator** %this.addr, align 4, !tbaa !2
  store i32 %elemSize, i32* %elemSize.addr, align 4, !tbaa !30
  store i32 %maxElements, i32* %maxElements.addr, align 4, !tbaa !30
  %this1 = load %class.btPoolAllocator*, %class.btPoolAllocator** %this.addr, align 4
  store %class.btPoolAllocator* %this1, %class.btPoolAllocator** %retval, align 4
  %m_elemSize = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 0
  %0 = load i32, i32* %elemSize.addr, align 4, !tbaa !30
  store i32 %0, i32* %m_elemSize, align 4, !tbaa !51
  %m_maxElements = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 1
  %1 = load i32, i32* %maxElements.addr, align 4, !tbaa !30
  store i32 %1, i32* %m_maxElements, align 4, !tbaa !53
  %m_elemSize2 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 0
  %2 = load i32, i32* %m_elemSize2, align 4, !tbaa !51
  %m_maxElements3 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 1
  %3 = load i32, i32* %m_maxElements3, align 4, !tbaa !53
  %mul = mul nsw i32 %2, %3
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %m_pool = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 4
  store i8* %call, i8** %m_pool, align 4, !tbaa !54
  %4 = bitcast i8** %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %m_pool4 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 4
  %5 = load i8*, i8** %m_pool4, align 4, !tbaa !54
  store i8* %5, i8** %p, align 4, !tbaa !2
  %6 = load i8*, i8** %p, align 4, !tbaa !2
  %m_firstFree = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 3
  store i8* %6, i8** %m_firstFree, align 4, !tbaa !55
  %m_maxElements5 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 1
  %7 = load i32, i32* %m_maxElements5, align 4, !tbaa !53
  %m_freeCount = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 2
  store i32 %7, i32* %m_freeCount, align 4, !tbaa !56
  %8 = bitcast i32* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  %m_maxElements6 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 1
  %9 = load i32, i32* %m_maxElements6, align 4, !tbaa !53
  store i32 %9, i32* %count, align 4, !tbaa !30
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %10 = load i32, i32* %count, align 4, !tbaa !30
  %dec = add nsw i32 %10, -1
  store i32 %dec, i32* %count, align 4, !tbaa !30
  %tobool = icmp ne i32 %dec, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %11 = load i8*, i8** %p, align 4, !tbaa !2
  %m_elemSize7 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 0
  %12 = load i32, i32* %m_elemSize7, align 4, !tbaa !51
  %add.ptr = getelementptr inbounds i8, i8* %11, i32 %12
  %13 = load i8*, i8** %p, align 4, !tbaa !2
  %14 = bitcast i8* %13 to i8**
  store i8* %add.ptr, i8** %14, align 4, !tbaa !2
  %m_elemSize8 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 0
  %15 = load i32, i32* %m_elemSize8, align 4, !tbaa !51
  %16 = load i8*, i8** %p, align 4, !tbaa !2
  %add.ptr9 = getelementptr inbounds i8, i8* %16, i32 %15
  store i8* %add.ptr9, i8** %p, align 4, !tbaa !2
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %17 = load i8*, i8** %p, align 4, !tbaa !2
  %18 = bitcast i8* %17 to i8**
  store i8* null, i8** %18, align 4, !tbaa !2
  %19 = bitcast i32* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  %20 = bitcast i8** %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #8
  %21 = load %class.btPoolAllocator*, %class.btPoolAllocator** %retval, align 4
  ret %class.btPoolAllocator* %21
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: nounwind
define hidden %class.btDefaultCollisionConfiguration* @_ZN31btDefaultCollisionConfigurationD2Ev(%class.btDefaultCollisionConfiguration* returned %this) unnamed_addr #4 {
entry:
  %retval = alloca %class.btDefaultCollisionConfiguration*, align 4
  %this.addr = alloca %class.btDefaultCollisionConfiguration*, align 4
  store %class.btDefaultCollisionConfiguration* %this, %class.btDefaultCollisionConfiguration** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btDefaultCollisionConfiguration*, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  store %class.btDefaultCollisionConfiguration* %this1, %class.btDefaultCollisionConfiguration** %retval, align 4
  %0 = bitcast %class.btDefaultCollisionConfiguration* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [8 x i8*] }, { [8 x i8*] }* @_ZTV31btDefaultCollisionConfiguration, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  %m_ownsCollisionAlgorithmPool = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsCollisionAlgorithmPool, align 4, !tbaa !36, !range !57
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_collisionAlgorithmPool = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 4
  %2 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_collisionAlgorithmPool, align 4, !tbaa !37
  %call = call %class.btPoolAllocator* @_ZN15btPoolAllocatorD2Ev(%class.btPoolAllocator* %2) #8
  %m_collisionAlgorithmPool2 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 4
  %3 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_collisionAlgorithmPool2, align 4, !tbaa !37
  %4 = bitcast %class.btPoolAllocator* %3 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_ownsPersistentManifoldPool = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 3
  %5 = load i8, i8* %m_ownsPersistentManifoldPool, align 4, !tbaa !32, !range !57
  %tobool3 = trunc i8 %5 to i1
  br i1 %tobool3, label %if.then4, label %if.end7

if.then4:                                         ; preds = %if.end
  %m_persistentManifoldPool = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 2
  %6 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_persistentManifoldPool, align 4, !tbaa !33
  %call5 = call %class.btPoolAllocator* @_ZN15btPoolAllocatorD2Ev(%class.btPoolAllocator* %6) #8
  %m_persistentManifoldPool6 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 2
  %7 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_persistentManifoldPool6, align 4, !tbaa !33
  %8 = bitcast %class.btPoolAllocator* %7 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %8)
  br label %if.end7

if.end7:                                          ; preds = %if.then4, %if.end
  %m_convexConvexCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 8
  %9 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_convexConvexCreateFunc, align 4, !tbaa !15
  %10 = bitcast %struct.btCollisionAlgorithmCreateFunc* %9 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %10, align 4, !tbaa !6
  %vfn = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable, i64 0
  %11 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn, align 4
  %call8 = call %struct.btCollisionAlgorithmCreateFunc* %11(%struct.btCollisionAlgorithmCreateFunc* %9) #8
  %m_convexConvexCreateFunc9 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 8
  %12 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_convexConvexCreateFunc9, align 4, !tbaa !15
  %13 = bitcast %struct.btCollisionAlgorithmCreateFunc* %12 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %13)
  %m_convexConcaveCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 9
  %14 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_convexConcaveCreateFunc, align 4, !tbaa !16
  %15 = bitcast %struct.btCollisionAlgorithmCreateFunc* %14 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable10 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %15, align 4, !tbaa !6
  %vfn11 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable10, i64 0
  %16 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn11, align 4
  %call12 = call %struct.btCollisionAlgorithmCreateFunc* %16(%struct.btCollisionAlgorithmCreateFunc* %14) #8
  %m_convexConcaveCreateFunc13 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 9
  %17 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_convexConcaveCreateFunc13, align 4, !tbaa !16
  %18 = bitcast %struct.btCollisionAlgorithmCreateFunc* %17 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %18)
  %m_swappedConvexConcaveCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 10
  %19 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_swappedConvexConcaveCreateFunc, align 4, !tbaa !17
  %20 = bitcast %struct.btCollisionAlgorithmCreateFunc* %19 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable14 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %20, align 4, !tbaa !6
  %vfn15 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable14, i64 0
  %21 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn15, align 4
  %call16 = call %struct.btCollisionAlgorithmCreateFunc* %21(%struct.btCollisionAlgorithmCreateFunc* %19) #8
  %m_swappedConvexConcaveCreateFunc17 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 10
  %22 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_swappedConvexConcaveCreateFunc17, align 4, !tbaa !17
  %23 = bitcast %struct.btCollisionAlgorithmCreateFunc* %22 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %23)
  %m_compoundCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 11
  %24 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_compoundCreateFunc, align 4, !tbaa !18
  %25 = bitcast %struct.btCollisionAlgorithmCreateFunc* %24 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable18 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %25, align 4, !tbaa !6
  %vfn19 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable18, i64 0
  %26 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn19, align 4
  %call20 = call %struct.btCollisionAlgorithmCreateFunc* %26(%struct.btCollisionAlgorithmCreateFunc* %24) #8
  %m_compoundCreateFunc21 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 11
  %27 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_compoundCreateFunc21, align 4, !tbaa !18
  %28 = bitcast %struct.btCollisionAlgorithmCreateFunc* %27 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %28)
  %m_compoundCompoundCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 12
  %29 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_compoundCompoundCreateFunc, align 4, !tbaa !19
  %30 = bitcast %struct.btCollisionAlgorithmCreateFunc* %29 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable22 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %30, align 4, !tbaa !6
  %vfn23 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable22, i64 0
  %31 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn23, align 4
  %call24 = call %struct.btCollisionAlgorithmCreateFunc* %31(%struct.btCollisionAlgorithmCreateFunc* %29) #8
  %m_compoundCompoundCreateFunc25 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 12
  %32 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_compoundCompoundCreateFunc25, align 4, !tbaa !19
  %33 = bitcast %struct.btCollisionAlgorithmCreateFunc* %32 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %33)
  %m_swappedCompoundCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 13
  %34 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_swappedCompoundCreateFunc, align 4, !tbaa !20
  %35 = bitcast %struct.btCollisionAlgorithmCreateFunc* %34 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable26 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %35, align 4, !tbaa !6
  %vfn27 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable26, i64 0
  %36 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn27, align 4
  %call28 = call %struct.btCollisionAlgorithmCreateFunc* %36(%struct.btCollisionAlgorithmCreateFunc* %34) #8
  %m_swappedCompoundCreateFunc29 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 13
  %37 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_swappedCompoundCreateFunc29, align 4, !tbaa !20
  %38 = bitcast %struct.btCollisionAlgorithmCreateFunc* %37 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %38)
  %m_emptyCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 14
  %39 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_emptyCreateFunc, align 4, !tbaa !21
  %40 = bitcast %struct.btCollisionAlgorithmCreateFunc* %39 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable30 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %40, align 4, !tbaa !6
  %vfn31 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable30, i64 0
  %41 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn31, align 4
  %call32 = call %struct.btCollisionAlgorithmCreateFunc* %41(%struct.btCollisionAlgorithmCreateFunc* %39) #8
  %m_emptyCreateFunc33 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 14
  %42 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_emptyCreateFunc33, align 4, !tbaa !21
  %43 = bitcast %struct.btCollisionAlgorithmCreateFunc* %42 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %43)
  %m_sphereSphereCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 15
  %44 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_sphereSphereCF, align 4, !tbaa !22
  %45 = bitcast %struct.btCollisionAlgorithmCreateFunc* %44 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable34 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %45, align 4, !tbaa !6
  %vfn35 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable34, i64 0
  %46 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn35, align 4
  %call36 = call %struct.btCollisionAlgorithmCreateFunc* %46(%struct.btCollisionAlgorithmCreateFunc* %44) #8
  %m_sphereSphereCF37 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 15
  %47 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_sphereSphereCF37, align 4, !tbaa !22
  %48 = bitcast %struct.btCollisionAlgorithmCreateFunc* %47 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %48)
  %m_sphereTriangleCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 19
  %49 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_sphereTriangleCF, align 4, !tbaa !23
  %50 = bitcast %struct.btCollisionAlgorithmCreateFunc* %49 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable38 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %50, align 4, !tbaa !6
  %vfn39 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable38, i64 0
  %51 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn39, align 4
  %call40 = call %struct.btCollisionAlgorithmCreateFunc* %51(%struct.btCollisionAlgorithmCreateFunc* %49) #8
  %m_sphereTriangleCF41 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 19
  %52 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_sphereTriangleCF41, align 4, !tbaa !23
  %53 = bitcast %struct.btCollisionAlgorithmCreateFunc* %52 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %53)
  %m_triangleSphereCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 20
  %54 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_triangleSphereCF, align 4, !tbaa !24
  %55 = bitcast %struct.btCollisionAlgorithmCreateFunc* %54 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable42 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %55, align 4, !tbaa !6
  %vfn43 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable42, i64 0
  %56 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn43, align 4
  %call44 = call %struct.btCollisionAlgorithmCreateFunc* %56(%struct.btCollisionAlgorithmCreateFunc* %54) #8
  %m_triangleSphereCF45 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 20
  %57 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_triangleSphereCF45, align 4, !tbaa !24
  %58 = bitcast %struct.btCollisionAlgorithmCreateFunc* %57 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %58)
  %m_boxBoxCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 18
  %59 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_boxBoxCF, align 4, !tbaa !27
  %60 = bitcast %struct.btCollisionAlgorithmCreateFunc* %59 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable46 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %60, align 4, !tbaa !6
  %vfn47 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable46, i64 0
  %61 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn47, align 4
  %call48 = call %struct.btCollisionAlgorithmCreateFunc* %61(%struct.btCollisionAlgorithmCreateFunc* %59) #8
  %m_boxBoxCF49 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 18
  %62 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_boxBoxCF49, align 4, !tbaa !27
  %63 = bitcast %struct.btCollisionAlgorithmCreateFunc* %62 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %63)
  %m_convexPlaneCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 22
  %64 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_convexPlaneCF, align 4, !tbaa !28
  %65 = bitcast %struct.btCollisionAlgorithmCreateFunc* %64 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable50 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %65, align 4, !tbaa !6
  %vfn51 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable50, i64 0
  %66 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn51, align 4
  %call52 = call %struct.btCollisionAlgorithmCreateFunc* %66(%struct.btCollisionAlgorithmCreateFunc* %64) #8
  %m_convexPlaneCF53 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 22
  %67 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_convexPlaneCF53, align 4, !tbaa !28
  %68 = bitcast %struct.btCollisionAlgorithmCreateFunc* %67 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %68)
  %m_planeConvexCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 21
  %69 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_planeConvexCF, align 4, !tbaa !29
  %70 = bitcast %struct.btCollisionAlgorithmCreateFunc* %69 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable54 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %70, align 4, !tbaa !6
  %vfn55 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable54, i64 0
  %71 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn55, align 4
  %call56 = call %struct.btCollisionAlgorithmCreateFunc* %71(%struct.btCollisionAlgorithmCreateFunc* %69) #8
  %m_planeConvexCF57 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 21
  %72 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_planeConvexCF57, align 4, !tbaa !29
  %73 = bitcast %struct.btCollisionAlgorithmCreateFunc* %72 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %73)
  %m_simplexSolver = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 6
  %74 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %m_simplexSolver, align 4, !tbaa !8
  %m_simplexSolver58 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 6
  %75 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %m_simplexSolver58, align 4, !tbaa !8
  %76 = bitcast %class.btVoronoiSimplexSolver* %75 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %76)
  %m_pdSolver = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 7
  %77 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %m_pdSolver, align 4, !tbaa !14
  %78 = bitcast %class.btConvexPenetrationDepthSolver* %77 to %class.btConvexPenetrationDepthSolver* (%class.btConvexPenetrationDepthSolver*)***
  %vtable59 = load %class.btConvexPenetrationDepthSolver* (%class.btConvexPenetrationDepthSolver*)**, %class.btConvexPenetrationDepthSolver* (%class.btConvexPenetrationDepthSolver*)*** %78, align 4, !tbaa !6
  %vfn60 = getelementptr inbounds %class.btConvexPenetrationDepthSolver* (%class.btConvexPenetrationDepthSolver*)*, %class.btConvexPenetrationDepthSolver* (%class.btConvexPenetrationDepthSolver*)** %vtable59, i64 0
  %79 = load %class.btConvexPenetrationDepthSolver* (%class.btConvexPenetrationDepthSolver*)*, %class.btConvexPenetrationDepthSolver* (%class.btConvexPenetrationDepthSolver*)** %vfn60, align 4
  %call61 = call %class.btConvexPenetrationDepthSolver* %79(%class.btConvexPenetrationDepthSolver* %77) #8
  %m_pdSolver62 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 7
  %80 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %m_pdSolver62, align 4, !tbaa !14
  %81 = bitcast %class.btConvexPenetrationDepthSolver* %80 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %81)
  %82 = bitcast %class.btDefaultCollisionConfiguration* %this1 to %class.btCollisionConfiguration*
  %call63 = call %class.btCollisionConfiguration* @_ZN24btCollisionConfigurationD2Ev(%class.btCollisionConfiguration* %82) #8
  %83 = load %class.btDefaultCollisionConfiguration*, %class.btDefaultCollisionConfiguration** %retval, align 4
  ret %class.btDefaultCollisionConfiguration* %83
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btPoolAllocator* @_ZN15btPoolAllocatorD2Ev(%class.btPoolAllocator* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btPoolAllocator*, align 4
  store %class.btPoolAllocator* %this, %class.btPoolAllocator** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPoolAllocator*, %class.btPoolAllocator** %this.addr, align 4
  %m_pool = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 4
  %0 = load i8*, i8** %m_pool, align 4, !tbaa !54
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret %class.btPoolAllocator* %this1
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: nounwind
define hidden void @_ZN31btDefaultCollisionConfigurationD0Ev(%class.btDefaultCollisionConfiguration* %this) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btDefaultCollisionConfiguration*, align 4
  store %class.btDefaultCollisionConfiguration* %this, %class.btDefaultCollisionConfiguration** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btDefaultCollisionConfiguration*, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  %call = call %class.btDefaultCollisionConfiguration* @_ZN31btDefaultCollisionConfigurationD1Ev(%class.btDefaultCollisionConfiguration* %this1) #8
  %0 = bitcast %class.btDefaultCollisionConfiguration* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #6

define hidden %struct.btCollisionAlgorithmCreateFunc* @_ZN31btDefaultCollisionConfiguration31getCollisionAlgorithmCreateFuncEii(%class.btDefaultCollisionConfiguration* %this, i32 %proxyType0, i32 %proxyType1) unnamed_addr #0 {
entry:
  %retval = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  %this.addr = alloca %class.btDefaultCollisionConfiguration*, align 4
  %proxyType0.addr = alloca i32, align 4
  %proxyType1.addr = alloca i32, align 4
  store %class.btDefaultCollisionConfiguration* %this, %class.btDefaultCollisionConfiguration** %this.addr, align 4, !tbaa !2
  store i32 %proxyType0, i32* %proxyType0.addr, align 4, !tbaa !30
  store i32 %proxyType1, i32* %proxyType1.addr, align 4, !tbaa !30
  %this1 = load %class.btDefaultCollisionConfiguration*, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  %0 = load i32, i32* %proxyType0.addr, align 4, !tbaa !30
  %cmp = icmp eq i32 %0, 8
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load i32, i32* %proxyType1.addr, align 4, !tbaa !30
  %cmp2 = icmp eq i32 %1, 8
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %m_sphereSphereCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 15
  %2 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_sphereSphereCF, align 4, !tbaa !22
  store %struct.btCollisionAlgorithmCreateFunc* %2, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  %3 = load i32, i32* %proxyType0.addr, align 4, !tbaa !30
  %cmp3 = icmp eq i32 %3, 8
  br i1 %cmp3, label %land.lhs.true4, label %if.end7

land.lhs.true4:                                   ; preds = %if.end
  %4 = load i32, i32* %proxyType1.addr, align 4, !tbaa !30
  %cmp5 = icmp eq i32 %4, 1
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %land.lhs.true4
  %m_sphereTriangleCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 19
  %5 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_sphereTriangleCF, align 4, !tbaa !23
  store %struct.btCollisionAlgorithmCreateFunc* %5, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end7:                                          ; preds = %land.lhs.true4, %if.end
  %6 = load i32, i32* %proxyType0.addr, align 4, !tbaa !30
  %cmp8 = icmp eq i32 %6, 1
  br i1 %cmp8, label %land.lhs.true9, label %if.end12

land.lhs.true9:                                   ; preds = %if.end7
  %7 = load i32, i32* %proxyType1.addr, align 4, !tbaa !30
  %cmp10 = icmp eq i32 %7, 8
  br i1 %cmp10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %land.lhs.true9
  %m_triangleSphereCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 20
  %8 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_triangleSphereCF, align 4, !tbaa !24
  store %struct.btCollisionAlgorithmCreateFunc* %8, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end12:                                         ; preds = %land.lhs.true9, %if.end7
  %9 = load i32, i32* %proxyType0.addr, align 4, !tbaa !30
  %cmp13 = icmp eq i32 %9, 0
  br i1 %cmp13, label %land.lhs.true14, label %if.end17

land.lhs.true14:                                  ; preds = %if.end12
  %10 = load i32, i32* %proxyType1.addr, align 4, !tbaa !30
  %cmp15 = icmp eq i32 %10, 0
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %land.lhs.true14
  %m_boxBoxCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 18
  %11 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_boxBoxCF, align 4, !tbaa !27
  store %struct.btCollisionAlgorithmCreateFunc* %11, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end17:                                         ; preds = %land.lhs.true14, %if.end12
  %12 = load i32, i32* %proxyType0.addr, align 4, !tbaa !30
  %call = call zeroext i1 @_ZN17btBroadphaseProxy8isConvexEi(i32 %12)
  br i1 %call, label %land.lhs.true18, label %if.end21

land.lhs.true18:                                  ; preds = %if.end17
  %13 = load i32, i32* %proxyType1.addr, align 4, !tbaa !30
  %cmp19 = icmp eq i32 %13, 28
  br i1 %cmp19, label %if.then20, label %if.end21

if.then20:                                        ; preds = %land.lhs.true18
  %m_convexPlaneCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 22
  %14 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_convexPlaneCF, align 4, !tbaa !28
  store %struct.btCollisionAlgorithmCreateFunc* %14, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end21:                                         ; preds = %land.lhs.true18, %if.end17
  %15 = load i32, i32* %proxyType1.addr, align 4, !tbaa !30
  %call22 = call zeroext i1 @_ZN17btBroadphaseProxy8isConvexEi(i32 %15)
  br i1 %call22, label %land.lhs.true23, label %if.end26

land.lhs.true23:                                  ; preds = %if.end21
  %16 = load i32, i32* %proxyType0.addr, align 4, !tbaa !30
  %cmp24 = icmp eq i32 %16, 28
  br i1 %cmp24, label %if.then25, label %if.end26

if.then25:                                        ; preds = %land.lhs.true23
  %m_planeConvexCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 21
  %17 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_planeConvexCF, align 4, !tbaa !29
  store %struct.btCollisionAlgorithmCreateFunc* %17, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end26:                                         ; preds = %land.lhs.true23, %if.end21
  %18 = load i32, i32* %proxyType0.addr, align 4, !tbaa !30
  %call27 = call zeroext i1 @_ZN17btBroadphaseProxy8isConvexEi(i32 %18)
  br i1 %call27, label %land.lhs.true28, label %if.end31

land.lhs.true28:                                  ; preds = %if.end26
  %19 = load i32, i32* %proxyType1.addr, align 4, !tbaa !30
  %call29 = call zeroext i1 @_ZN17btBroadphaseProxy8isConvexEi(i32 %19)
  br i1 %call29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %land.lhs.true28
  %m_convexConvexCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 8
  %20 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_convexConvexCreateFunc, align 4, !tbaa !15
  store %struct.btCollisionAlgorithmCreateFunc* %20, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end31:                                         ; preds = %land.lhs.true28, %if.end26
  %21 = load i32, i32* %proxyType0.addr, align 4, !tbaa !30
  %call32 = call zeroext i1 @_ZN17btBroadphaseProxy8isConvexEi(i32 %21)
  br i1 %call32, label %land.lhs.true33, label %if.end36

land.lhs.true33:                                  ; preds = %if.end31
  %22 = load i32, i32* %proxyType1.addr, align 4, !tbaa !30
  %call34 = call zeroext i1 @_ZN17btBroadphaseProxy9isConcaveEi(i32 %22)
  br i1 %call34, label %if.then35, label %if.end36

if.then35:                                        ; preds = %land.lhs.true33
  %m_convexConcaveCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 9
  %23 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_convexConcaveCreateFunc, align 4, !tbaa !16
  store %struct.btCollisionAlgorithmCreateFunc* %23, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end36:                                         ; preds = %land.lhs.true33, %if.end31
  %24 = load i32, i32* %proxyType1.addr, align 4, !tbaa !30
  %call37 = call zeroext i1 @_ZN17btBroadphaseProxy8isConvexEi(i32 %24)
  br i1 %call37, label %land.lhs.true38, label %if.end41

land.lhs.true38:                                  ; preds = %if.end36
  %25 = load i32, i32* %proxyType0.addr, align 4, !tbaa !30
  %call39 = call zeroext i1 @_ZN17btBroadphaseProxy9isConcaveEi(i32 %25)
  br i1 %call39, label %if.then40, label %if.end41

if.then40:                                        ; preds = %land.lhs.true38
  %m_swappedConvexConcaveCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 10
  %26 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_swappedConvexConcaveCreateFunc, align 4, !tbaa !17
  store %struct.btCollisionAlgorithmCreateFunc* %26, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end41:                                         ; preds = %land.lhs.true38, %if.end36
  %27 = load i32, i32* %proxyType0.addr, align 4, !tbaa !30
  %call42 = call zeroext i1 @_ZN17btBroadphaseProxy10isCompoundEi(i32 %27)
  br i1 %call42, label %land.lhs.true43, label %if.end46

land.lhs.true43:                                  ; preds = %if.end41
  %28 = load i32, i32* %proxyType1.addr, align 4, !tbaa !30
  %call44 = call zeroext i1 @_ZN17btBroadphaseProxy10isCompoundEi(i32 %28)
  br i1 %call44, label %if.then45, label %if.end46

if.then45:                                        ; preds = %land.lhs.true43
  %m_compoundCompoundCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 12
  %29 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_compoundCompoundCreateFunc, align 4, !tbaa !19
  store %struct.btCollisionAlgorithmCreateFunc* %29, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end46:                                         ; preds = %land.lhs.true43, %if.end41
  %30 = load i32, i32* %proxyType0.addr, align 4, !tbaa !30
  %call47 = call zeroext i1 @_ZN17btBroadphaseProxy10isCompoundEi(i32 %30)
  br i1 %call47, label %if.then48, label %if.else

if.then48:                                        ; preds = %if.end46
  %m_compoundCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 11
  %31 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_compoundCreateFunc, align 4, !tbaa !18
  store %struct.btCollisionAlgorithmCreateFunc* %31, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.else:                                          ; preds = %if.end46
  %32 = load i32, i32* %proxyType1.addr, align 4, !tbaa !30
  %call49 = call zeroext i1 @_ZN17btBroadphaseProxy10isCompoundEi(i32 %32)
  br i1 %call49, label %if.then50, label %if.end51

if.then50:                                        ; preds = %if.else
  %m_swappedCompoundCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 13
  %33 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_swappedCompoundCreateFunc, align 4, !tbaa !20
  store %struct.btCollisionAlgorithmCreateFunc* %33, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end51:                                         ; preds = %if.else
  br label %if.end52

if.end52:                                         ; preds = %if.end51
  %m_emptyCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 14
  %34 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_emptyCreateFunc, align 4, !tbaa !21
  store %struct.btCollisionAlgorithmCreateFunc* %34, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

return:                                           ; preds = %if.end52, %if.then50, %if.then48, %if.then45, %if.then40, %if.then35, %if.then30, %if.then25, %if.then20, %if.then16, %if.then11, %if.then6, %if.then
  %35 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  ret %struct.btCollisionAlgorithmCreateFunc* %35
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZN17btBroadphaseProxy8isConvexEi(i32 %proxyType) #1 comdat {
entry:
  %proxyType.addr = alloca i32, align 4
  store i32 %proxyType, i32* %proxyType.addr, align 4, !tbaa !30
  %0 = load i32, i32* %proxyType.addr, align 4, !tbaa !30
  %cmp = icmp slt i32 %0, 20
  ret i1 %cmp
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZN17btBroadphaseProxy9isConcaveEi(i32 %proxyType) #1 comdat {
entry:
  %proxyType.addr = alloca i32, align 4
  store i32 %proxyType, i32* %proxyType.addr, align 4, !tbaa !30
  %0 = load i32, i32* %proxyType.addr, align 4, !tbaa !30
  %cmp = icmp sgt i32 %0, 20
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %1 = load i32, i32* %proxyType.addr, align 4, !tbaa !30
  %cmp1 = icmp slt i32 %1, 30
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %2 = phi i1 [ false, %entry ], [ %cmp1, %land.rhs ]
  ret i1 %2
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZN17btBroadphaseProxy10isCompoundEi(i32 %proxyType) #1 comdat {
entry:
  %proxyType.addr = alloca i32, align 4
  store i32 %proxyType, i32* %proxyType.addr, align 4, !tbaa !30
  %0 = load i32, i32* %proxyType.addr, align 4, !tbaa !30
  %cmp = icmp eq i32 %0, 31
  ret i1 %cmp
}

; Function Attrs: nounwind
define hidden void @_ZN31btDefaultCollisionConfiguration35setConvexConvexMultipointIterationsEii(%class.btDefaultCollisionConfiguration* %this, i32 %numPerturbationIterations, i32 %minimumPointsPerturbationThreshold) #4 {
entry:
  %this.addr = alloca %class.btDefaultCollisionConfiguration*, align 4
  %numPerturbationIterations.addr = alloca i32, align 4
  %minimumPointsPerturbationThreshold.addr = alloca i32, align 4
  %convexConvex = alloca %"struct.btConvexConvexAlgorithm::CreateFunc"*, align 4
  store %class.btDefaultCollisionConfiguration* %this, %class.btDefaultCollisionConfiguration** %this.addr, align 4, !tbaa !2
  store i32 %numPerturbationIterations, i32* %numPerturbationIterations.addr, align 4, !tbaa !30
  store i32 %minimumPointsPerturbationThreshold, i32* %minimumPointsPerturbationThreshold.addr, align 4, !tbaa !30
  %this1 = load %class.btDefaultCollisionConfiguration*, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  %0 = bitcast %"struct.btConvexConvexAlgorithm::CreateFunc"** %convexConvex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %m_convexConvexCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 8
  %1 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_convexConvexCreateFunc, align 4, !tbaa !15
  %2 = bitcast %struct.btCollisionAlgorithmCreateFunc* %1 to %"struct.btConvexConvexAlgorithm::CreateFunc"*
  store %"struct.btConvexConvexAlgorithm::CreateFunc"* %2, %"struct.btConvexConvexAlgorithm::CreateFunc"** %convexConvex, align 4, !tbaa !2
  %3 = load i32, i32* %numPerturbationIterations.addr, align 4, !tbaa !30
  %4 = load %"struct.btConvexConvexAlgorithm::CreateFunc"*, %"struct.btConvexConvexAlgorithm::CreateFunc"** %convexConvex, align 4, !tbaa !2
  %m_numPerturbationIterations = getelementptr inbounds %"struct.btConvexConvexAlgorithm::CreateFunc", %"struct.btConvexConvexAlgorithm::CreateFunc"* %4, i32 0, i32 3
  store i32 %3, i32* %m_numPerturbationIterations, align 4, !tbaa !58
  %5 = load i32, i32* %minimumPointsPerturbationThreshold.addr, align 4, !tbaa !30
  %6 = load %"struct.btConvexConvexAlgorithm::CreateFunc"*, %"struct.btConvexConvexAlgorithm::CreateFunc"** %convexConvex, align 4, !tbaa !2
  %m_minimumPointsPerturbationThreshold = getelementptr inbounds %"struct.btConvexConvexAlgorithm::CreateFunc", %"struct.btConvexConvexAlgorithm::CreateFunc"* %6, i32 0, i32 4
  store i32 %5, i32* %m_minimumPointsPerturbationThreshold, align 4, !tbaa !60
  %7 = bitcast %"struct.btConvexConvexAlgorithm::CreateFunc"** %convexConvex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: nounwind
define hidden void @_ZN31btDefaultCollisionConfiguration34setPlaneConvexMultipointIterationsEii(%class.btDefaultCollisionConfiguration* %this, i32 %numPerturbationIterations, i32 %minimumPointsPerturbationThreshold) #4 {
entry:
  %this.addr = alloca %class.btDefaultCollisionConfiguration*, align 4
  %numPerturbationIterations.addr = alloca i32, align 4
  %minimumPointsPerturbationThreshold.addr = alloca i32, align 4
  %cpCF = alloca %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*, align 4
  %pcCF = alloca %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*, align 4
  store %class.btDefaultCollisionConfiguration* %this, %class.btDefaultCollisionConfiguration** %this.addr, align 4, !tbaa !2
  store i32 %numPerturbationIterations, i32* %numPerturbationIterations.addr, align 4, !tbaa !30
  store i32 %minimumPointsPerturbationThreshold, i32* %minimumPointsPerturbationThreshold.addr, align 4, !tbaa !30
  %this1 = load %class.btDefaultCollisionConfiguration*, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  %0 = bitcast %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"** %cpCF to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %m_convexPlaneCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 22
  %1 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_convexPlaneCF, align 4, !tbaa !28
  %2 = bitcast %struct.btCollisionAlgorithmCreateFunc* %1 to %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*
  store %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %2, %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"** %cpCF, align 4, !tbaa !2
  %3 = load i32, i32* %numPerturbationIterations.addr, align 4, !tbaa !30
  %4 = load %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*, %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"** %cpCF, align 4, !tbaa !2
  %m_numPerturbationIterations = getelementptr inbounds %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc", %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %4, i32 0, i32 1
  store i32 %3, i32* %m_numPerturbationIterations, align 4, !tbaa !48
  %5 = load i32, i32* %minimumPointsPerturbationThreshold.addr, align 4, !tbaa !30
  %6 = load %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*, %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"** %cpCF, align 4, !tbaa !2
  %m_minimumPointsPerturbationThreshold = getelementptr inbounds %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc", %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %6, i32 0, i32 2
  store i32 %5, i32* %m_minimumPointsPerturbationThreshold, align 4, !tbaa !50
  %7 = bitcast %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"** %pcCF to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %m_planeConvexCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 21
  %8 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_planeConvexCF, align 4, !tbaa !29
  %9 = bitcast %struct.btCollisionAlgorithmCreateFunc* %8 to %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*
  store %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %9, %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"** %pcCF, align 4, !tbaa !2
  %10 = load i32, i32* %numPerturbationIterations.addr, align 4, !tbaa !30
  %11 = load %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*, %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"** %pcCF, align 4, !tbaa !2
  %m_numPerturbationIterations2 = getelementptr inbounds %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc", %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %11, i32 0, i32 1
  store i32 %10, i32* %m_numPerturbationIterations2, align 4, !tbaa !48
  %12 = load i32, i32* %minimumPointsPerturbationThreshold.addr, align 4, !tbaa !30
  %13 = load %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*, %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"** %pcCF, align 4, !tbaa !2
  %m_minimumPointsPerturbationThreshold3 = getelementptr inbounds %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc", %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %13, i32 0, i32 2
  store i32 %12, i32* %m_minimumPointsPerturbationThreshold3, align 4, !tbaa !50
  %14 = bitcast %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"** %pcCF to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #8
  %15 = bitcast %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"** %cpCF to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btPoolAllocator* @_ZN31btDefaultCollisionConfiguration25getPersistentManifoldPoolEv(%class.btDefaultCollisionConfiguration* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btDefaultCollisionConfiguration*, align 4
  store %class.btDefaultCollisionConfiguration* %this, %class.btDefaultCollisionConfiguration** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btDefaultCollisionConfiguration*, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  %m_persistentManifoldPool = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 2
  %0 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_persistentManifoldPool, align 4, !tbaa !33
  ret %class.btPoolAllocator* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btPoolAllocator* @_ZN31btDefaultCollisionConfiguration25getCollisionAlgorithmPoolEv(%class.btDefaultCollisionConfiguration* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btDefaultCollisionConfiguration*, align 4
  store %class.btDefaultCollisionConfiguration* %this, %class.btDefaultCollisionConfiguration** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btDefaultCollisionConfiguration*, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  %m_collisionAlgorithmPool = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 4
  %0 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_collisionAlgorithmPool, align 4, !tbaa !37
  ret %class.btPoolAllocator* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btVoronoiSimplexSolver* @_ZN31btDefaultCollisionConfiguration16getSimplexSolverEv(%class.btDefaultCollisionConfiguration* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btDefaultCollisionConfiguration*, align 4
  store %class.btDefaultCollisionConfiguration* %this, %class.btDefaultCollisionConfiguration** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btDefaultCollisionConfiguration*, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  %m_simplexSolver = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 6
  %0 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %m_simplexSolver, align 4, !tbaa !8
  ret %class.btVoronoiSimplexSolver* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btCollisionConfiguration* @_ZN24btCollisionConfigurationD2Ev(%class.btCollisionConfiguration* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btCollisionConfiguration*, align 4
  store %class.btCollisionConfiguration* %this, %class.btCollisionConfiguration** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionConfiguration*, %class.btCollisionConfiguration** %this.addr, align 4
  ret %class.btCollisionConfiguration* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN24btCollisionConfigurationD0Ev(%class.btCollisionConfiguration* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btCollisionConfiguration*, align 4
  store %class.btCollisionConfiguration* %this, %class.btCollisionConfiguration** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionConfiguration*, %class.btCollisionConfiguration** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #7

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden %struct.btSubSimplexClosestResult* @_ZN25btSubSimplexClosestResultC2Ev(%struct.btSubSimplexClosestResult* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %struct.btSubSimplexClosestResult*, align 4
  store %struct.btSubSimplexClosestResult* %this, %struct.btSubSimplexClosestResult** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %this.addr, align 4
  %m_closestPointOnSimplex = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_closestPointOnSimplex)
  %m_usedVertices = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 1
  %call2 = call %struct.btUsageBitfield* @_ZN15btUsageBitfieldC2Ev(%struct.btUsageBitfield* %m_usedVertices)
  ret %struct.btSubSimplexClosestResult* %this1
}

define linkonce_odr hidden %struct.btUsageBitfield* @_ZN15btUsageBitfieldC2Ev(%struct.btUsageBitfield* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %struct.btUsageBitfield*, align 4
  store %struct.btUsageBitfield* %this, %struct.btUsageBitfield** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btUsageBitfield*, %struct.btUsageBitfield** %this.addr, align 4
  call void @_ZN15btUsageBitfield5resetEv(%struct.btUsageBitfield* %this1)
  ret %struct.btUsageBitfield* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN15btUsageBitfield5resetEv(%struct.btUsageBitfield* %this) #4 comdat {
entry:
  %this.addr = alloca %struct.btUsageBitfield*, align 4
  store %struct.btUsageBitfield* %this, %struct.btUsageBitfield** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btUsageBitfield*, %struct.btUsageBitfield** %this.addr, align 4
  %0 = bitcast %struct.btUsageBitfield* %this1 to i8*
  %bf.load = load i8, i8* %0, align 2
  %bf.clear = and i8 %bf.load, -2
  store i8 %bf.clear, i8* %0, align 2
  %1 = bitcast %struct.btUsageBitfield* %this1 to i8*
  %bf.load2 = load i8, i8* %1, align 2
  %bf.clear3 = and i8 %bf.load2, -3
  store i8 %bf.clear3, i8* %1, align 2
  %2 = bitcast %struct.btUsageBitfield* %this1 to i8*
  %bf.load4 = load i8, i8* %2, align 2
  %bf.clear5 = and i8 %bf.load4, -5
  store i8 %bf.clear5, i8* %2, align 2
  %3 = bitcast %struct.btUsageBitfield* %this1 to i8*
  %bf.load6 = load i8, i8* %3, align 2
  %bf.clear7 = and i8 %bf.load6, -9
  store i8 %bf.clear7, i8* %3, align 2
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btConvexPenetrationDepthSolver* @_ZN30btConvexPenetrationDepthSolverC2Ev(%class.btConvexPenetrationDepthSolver* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexPenetrationDepthSolver*, align 4
  store %class.btConvexPenetrationDepthSolver* %this, %class.btConvexPenetrationDepthSolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %this.addr, align 4
  %0 = bitcast %class.btConvexPenetrationDepthSolver* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV30btConvexPenetrationDepthSolver, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  ret %class.btConvexPenetrationDepthSolver* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btConvexPenetrationDepthSolver* @_ZN30btConvexPenetrationDepthSolverD2Ev(%class.btConvexPenetrationDepthSolver* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btConvexPenetrationDepthSolver*, align 4
  store %class.btConvexPenetrationDepthSolver* %this, %class.btConvexPenetrationDepthSolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %this.addr, align 4
  ret %class.btConvexPenetrationDepthSolver* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN30btConvexPenetrationDepthSolverD0Ev(%class.btConvexPenetrationDepthSolver* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btConvexPenetrationDepthSolver*, align 4
  store %class.btConvexPenetrationDepthSolver* %this, %class.btConvexPenetrationDepthSolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %this, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  %0 = bitcast %struct.btCollisionAlgorithmCreateFunc* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV30btCollisionAlgorithmCreateFunc, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  %m_swapped = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc, %struct.btCollisionAlgorithmCreateFunc* %this1, i32 0, i32 1
  store i8 0, i8* %m_swapped, align 4, !tbaa !25
  ret %struct.btCollisionAlgorithmCreateFunc* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncD2Ev(%struct.btCollisionAlgorithmCreateFunc* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %this, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  ret %struct.btCollisionAlgorithmCreateFunc* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN33btConvexConcaveCollisionAlgorithm10CreateFuncD0Ev(%"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* %this, %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"*, %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %call = call %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* (%"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"*)*)(%"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* %this1) #8
  %0 = bitcast %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN33btConvexConcaveCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %mem = alloca i8*, align 4
  store %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* %this, %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %this1 = load %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"*, %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %1, i32 0, i32 0
  %2 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4, !tbaa !61
  %3 = bitcast %class.btDispatcher* %2 to i8* (%class.btDispatcher*, i32)***
  %vtable = load i8* (%class.btDispatcher*, i32)**, i8* (%class.btDispatcher*, i32)*** %3, align 4, !tbaa !6
  %vfn = getelementptr inbounds i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vtable, i64 14
  %4 = load i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vfn, align 4
  %call = call i8* %4(%class.btDispatcher* %2, i32 80)
  store i8* %call, i8** %mem, align 4, !tbaa !2
  %5 = load i8*, i8** %mem, align 4, !tbaa !2
  %6 = bitcast i8* %5 to %class.btConvexConcaveCollisionAlgorithm*
  %7 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call2 = call %class.btConvexConcaveCollisionAlgorithm* @_ZN33btConvexConcaveCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b(%class.btConvexConcaveCollisionAlgorithm* %6, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %7, %struct.btCollisionObjectWrapper* %8, %struct.btCollisionObjectWrapper* %9, i1 zeroext false)
  %10 = bitcast %class.btConvexConcaveCollisionAlgorithm* %6 to %class.btCollisionAlgorithm*
  %11 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret %class.btCollisionAlgorithm* %10
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN30btCollisionAlgorithmCreateFuncD0Ev(%struct.btCollisionAlgorithmCreateFunc* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %this, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncD2Ev(%struct.btCollisionAlgorithmCreateFunc* %this1) #8
  %0 = bitcast %struct.btCollisionAlgorithmCreateFunc* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN30btCollisionAlgorithmCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS4_(%struct.btCollisionAlgorithmCreateFunc* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %0, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  %.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %this, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionAlgorithmConstructionInfo* %0, %struct.btCollisionAlgorithmConstructionInfo** %.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  ret %class.btCollisionAlgorithm* null
}

declare %class.btConvexConcaveCollisionAlgorithm* @_ZN33btConvexConcaveCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b(%class.btConvexConcaveCollisionAlgorithm* returned, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8), %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1 zeroext) unnamed_addr #3

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncD0Ev(%"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"*, align 4
  store %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* %this, %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"*, %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  %call = call %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* (%"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"*)*)(%"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* %this1) #8
  %0 = bitcast %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %mem = alloca i8*, align 4
  store %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* %this, %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %this1 = load %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"*, %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  %0 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %1, i32 0, i32 0
  %2 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4, !tbaa !61
  %3 = bitcast %class.btDispatcher* %2 to i8* (%class.btDispatcher*, i32)***
  %vtable = load i8* (%class.btDispatcher*, i32)**, i8* (%class.btDispatcher*, i32)*** %3, align 4, !tbaa !6
  %vfn = getelementptr inbounds i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vtable, i64 14
  %4 = load i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vfn, align 4
  %call = call i8* %4(%class.btDispatcher* %2, i32 80)
  store i8* %call, i8** %mem, align 4, !tbaa !2
  %5 = load i8*, i8** %mem, align 4, !tbaa !2
  %6 = bitcast i8* %5 to %class.btConvexConcaveCollisionAlgorithm*
  %7 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call2 = call %class.btConvexConcaveCollisionAlgorithm* @_ZN33btConvexConcaveCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b(%class.btConvexConcaveCollisionAlgorithm* %6, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %7, %struct.btCollisionObjectWrapper* %8, %struct.btCollisionObjectWrapper* %9, i1 zeroext true)
  %10 = bitcast %class.btConvexConcaveCollisionAlgorithm* %6 to %class.btCollisionAlgorithm*
  %11 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret %class.btCollisionAlgorithm* %10
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN28btCompoundCollisionAlgorithm10CreateFuncD0Ev(%"struct.btCompoundCollisionAlgorithm::CreateFunc"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCompoundCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btCompoundCollisionAlgorithm::CreateFunc"* %this, %"struct.btCompoundCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btCompoundCollisionAlgorithm::CreateFunc"*, %"struct.btCompoundCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %call = call %"struct.btCompoundCollisionAlgorithm::CreateFunc"* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to %"struct.btCompoundCollisionAlgorithm::CreateFunc"* (%"struct.btCompoundCollisionAlgorithm::CreateFunc"*)*)(%"struct.btCompoundCollisionAlgorithm::CreateFunc"* %this1) #8
  %0 = bitcast %"struct.btCompoundCollisionAlgorithm::CreateFunc"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN28btCompoundCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%"struct.btCompoundCollisionAlgorithm::CreateFunc"* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.btCompoundCollisionAlgorithm::CreateFunc"*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %mem = alloca i8*, align 4
  store %"struct.btCompoundCollisionAlgorithm::CreateFunc"* %this, %"struct.btCompoundCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %this1 = load %"struct.btCompoundCollisionAlgorithm::CreateFunc"*, %"struct.btCompoundCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %1, i32 0, i32 0
  %2 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4, !tbaa !61
  %3 = bitcast %class.btDispatcher* %2 to i8* (%class.btDispatcher*, i32)***
  %vtable = load i8* (%class.btDispatcher*, i32)**, i8* (%class.btDispatcher*, i32)*** %3, align 4, !tbaa !6
  %vfn = getelementptr inbounds i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vtable, i64 14
  %4 = load i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vfn, align 4
  %call = call i8* %4(%class.btDispatcher* %2, i32 44)
  store i8* %call, i8** %mem, align 4, !tbaa !2
  %5 = load i8*, i8** %mem, align 4, !tbaa !2
  %6 = bitcast i8* %5 to %class.btCompoundCollisionAlgorithm*
  %7 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call2 = call %class.btCompoundCollisionAlgorithm* @_ZN28btCompoundCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b(%class.btCompoundCollisionAlgorithm* %6, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %7, %struct.btCollisionObjectWrapper* %8, %struct.btCollisionObjectWrapper* %9, i1 zeroext false)
  %10 = bitcast %class.btCompoundCollisionAlgorithm* %6 to %class.btCollisionAlgorithm*
  %11 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret %class.btCollisionAlgorithm* %10
}

declare %class.btCompoundCollisionAlgorithm* @_ZN28btCompoundCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b(%class.btCompoundCollisionAlgorithm* returned, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8), %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1 zeroext) unnamed_addr #3

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN36btCompoundCompoundCollisionAlgorithm10CreateFuncD0Ev(%"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* %this, %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"*, %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %call = call %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* (%"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"*)*)(%"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* %this1) #8
  %0 = bitcast %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN36btCompoundCompoundCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %mem = alloca i8*, align 4
  store %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* %this, %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %this1 = load %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"*, %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %1, i32 0, i32 0
  %2 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4, !tbaa !61
  %3 = bitcast %class.btDispatcher* %2 to i8* (%class.btDispatcher*, i32)***
  %vtable = load i8* (%class.btDispatcher*, i32)**, i8* (%class.btDispatcher*, i32)*** %3, align 4, !tbaa !6
  %vfn = getelementptr inbounds i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vtable, i64 14
  %4 = load i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vfn, align 4
  %call = call i8* %4(%class.btDispatcher* %2, i32 48)
  store i8* %call, i8** %mem, align 4, !tbaa !2
  %5 = load i8*, i8** %mem, align 4, !tbaa !2
  %6 = bitcast i8* %5 to %class.btCompoundCompoundCollisionAlgorithm*
  %7 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call2 = call %class.btCompoundCompoundCollisionAlgorithm* @_ZN36btCompoundCompoundCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b(%class.btCompoundCompoundCollisionAlgorithm* %6, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %7, %struct.btCollisionObjectWrapper* %8, %struct.btCollisionObjectWrapper* %9, i1 zeroext false)
  %10 = bitcast %class.btCompoundCompoundCollisionAlgorithm* %6 to %class.btCollisionAlgorithm*
  %11 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret %class.btCollisionAlgorithm* %10
}

declare %class.btCompoundCompoundCollisionAlgorithm* @_ZN36btCompoundCompoundCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b(%class.btCompoundCompoundCollisionAlgorithm* returned, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8), %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1 zeroext) unnamed_addr #3

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN28btCompoundCollisionAlgorithm17SwappedCreateFuncD0Ev(%"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"*, align 4
  store %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* %this, %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"*, %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  %call = call %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* (%"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"*)*)(%"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* %this1) #8
  %0 = bitcast %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN28btCompoundCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %mem = alloca i8*, align 4
  store %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* %this, %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %this1 = load %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"*, %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  %0 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %1, i32 0, i32 0
  %2 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4, !tbaa !61
  %3 = bitcast %class.btDispatcher* %2 to i8* (%class.btDispatcher*, i32)***
  %vtable = load i8* (%class.btDispatcher*, i32)**, i8* (%class.btDispatcher*, i32)*** %3, align 4, !tbaa !6
  %vfn = getelementptr inbounds i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vtable, i64 14
  %4 = load i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vfn, align 4
  %call = call i8* %4(%class.btDispatcher* %2, i32 44)
  store i8* %call, i8** %mem, align 4, !tbaa !2
  %5 = load i8*, i8** %mem, align 4, !tbaa !2
  %6 = bitcast i8* %5 to %class.btCompoundCollisionAlgorithm*
  %7 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call2 = call %class.btCompoundCollisionAlgorithm* @_ZN28btCompoundCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b(%class.btCompoundCollisionAlgorithm* %6, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %7, %struct.btCollisionObjectWrapper* %8, %struct.btCollisionObjectWrapper* %9, i1 zeroext true)
  %10 = bitcast %class.btCompoundCollisionAlgorithm* %6 to %class.btCollisionAlgorithm*
  %11 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret %class.btCollisionAlgorithm* %10
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN16btEmptyAlgorithm10CreateFuncD0Ev(%"struct.btEmptyAlgorithm::CreateFunc"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btEmptyAlgorithm::CreateFunc"*, align 4
  store %"struct.btEmptyAlgorithm::CreateFunc"* %this, %"struct.btEmptyAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btEmptyAlgorithm::CreateFunc"*, %"struct.btEmptyAlgorithm::CreateFunc"** %this.addr, align 4
  %call = call %"struct.btEmptyAlgorithm::CreateFunc"* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to %"struct.btEmptyAlgorithm::CreateFunc"* (%"struct.btEmptyAlgorithm::CreateFunc"*)*)(%"struct.btEmptyAlgorithm::CreateFunc"* %this1) #8
  %0 = bitcast %"struct.btEmptyAlgorithm::CreateFunc"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN16btEmptyAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%"struct.btEmptyAlgorithm::CreateFunc"* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.btEmptyAlgorithm::CreateFunc"*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %mem = alloca i8*, align 4
  store %"struct.btEmptyAlgorithm::CreateFunc"* %this, %"struct.btEmptyAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %this1 = load %"struct.btEmptyAlgorithm::CreateFunc"*, %"struct.btEmptyAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %1, i32 0, i32 0
  %2 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4, !tbaa !61
  %3 = bitcast %class.btDispatcher* %2 to i8* (%class.btDispatcher*, i32)***
  %vtable = load i8* (%class.btDispatcher*, i32)**, i8* (%class.btDispatcher*, i32)*** %3, align 4, !tbaa !6
  %vfn = getelementptr inbounds i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vtable, i64 14
  %4 = load i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vfn, align 4
  %call = call i8* %4(%class.btDispatcher* %2, i32 8)
  store i8* %call, i8** %mem, align 4, !tbaa !2
  %5 = load i8*, i8** %mem, align 4, !tbaa !2
  %6 = bitcast i8* %5 to %class.btEmptyAlgorithm*
  %7 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %call2 = call %class.btEmptyAlgorithm* @_ZN16btEmptyAlgorithmC1ERK36btCollisionAlgorithmConstructionInfo(%class.btEmptyAlgorithm* %6, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %7)
  %8 = bitcast %class.btEmptyAlgorithm* %6 to %class.btCollisionAlgorithm*
  %9 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  ret %class.btCollisionAlgorithm* %8
}

declare %class.btEmptyAlgorithm* @_ZN16btEmptyAlgorithmC1ERK36btCollisionAlgorithmConstructionInfo(%class.btEmptyAlgorithm* returned, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8)) unnamed_addr #3

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN32btSphereSphereCollisionAlgorithm10CreateFuncD0Ev(%"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* %this, %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"*, %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %call = call %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* (%"struct.btSphereSphereCollisionAlgorithm::CreateFunc"*)*)(%"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* %this1) #8
  %0 = bitcast %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN32btSphereSphereCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %col0Wrap, %struct.btCollisionObjectWrapper* %col1Wrap) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %col0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %col1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %mem = alloca i8*, align 4
  store %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* %this, %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %col0Wrap, %struct.btCollisionObjectWrapper** %col0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %col1Wrap, %struct.btCollisionObjectWrapper** %col1Wrap.addr, align 4, !tbaa !2
  %this1 = load %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"*, %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %1, i32 0, i32 0
  %2 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4, !tbaa !61
  %3 = bitcast %class.btDispatcher* %2 to i8* (%class.btDispatcher*, i32)***
  %vtable = load i8* (%class.btDispatcher*, i32)**, i8* (%class.btDispatcher*, i32)*** %3, align 4, !tbaa !6
  %vfn = getelementptr inbounds i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vtable, i64 14
  %4 = load i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vfn, align 4
  %call = call i8* %4(%class.btDispatcher* %2, i32 16)
  store i8* %call, i8** %mem, align 4, !tbaa !2
  %5 = load i8*, i8** %mem, align 4, !tbaa !2
  %6 = bitcast i8* %5 to %class.btSphereSphereCollisionAlgorithm*
  %7 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col0Wrap.addr, align 4, !tbaa !2
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col1Wrap.addr, align 4, !tbaa !2
  %call2 = call %class.btSphereSphereCollisionAlgorithm* @_ZN32btSphereSphereCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_(%class.btSphereSphereCollisionAlgorithm* %6, %class.btPersistentManifold* null, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %7, %struct.btCollisionObjectWrapper* %8, %struct.btCollisionObjectWrapper* %9)
  %10 = bitcast %class.btSphereSphereCollisionAlgorithm* %6 to %class.btCollisionAlgorithm*
  %11 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret %class.btCollisionAlgorithm* %10
}

declare %class.btSphereSphereCollisionAlgorithm* @_ZN32btSphereSphereCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_(%class.btSphereSphereCollisionAlgorithm* returned, %class.btPersistentManifold*, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8), %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*) unnamed_addr #3

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN34btSphereTriangleCollisionAlgorithm10CreateFuncD0Ev(%"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %this, %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"*, %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %call = call %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* (%"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"*)*)(%"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %this1) #8
  %0 = bitcast %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN34btSphereTriangleCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %mem = alloca i8*, align 4
  store %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %this, %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %this1 = load %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"*, %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %1, i32 0, i32 0
  %2 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4, !tbaa !61
  %3 = bitcast %class.btDispatcher* %2 to i8* (%class.btDispatcher*, i32)***
  %vtable = load i8* (%class.btDispatcher*, i32)**, i8* (%class.btDispatcher*, i32)*** %3, align 4, !tbaa !6
  %vfn = getelementptr inbounds i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vtable, i64 14
  %4 = load i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vfn, align 4
  %call = call i8* %4(%class.btDispatcher* %2, i32 20)
  store i8* %call, i8** %mem, align 4, !tbaa !2
  %5 = load i8*, i8** %mem, align 4, !tbaa !2
  %6 = bitcast i8* %5 to %class.btSphereTriangleCollisionAlgorithm*
  %7 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %m_manifold = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %7, i32 0, i32 1
  %8 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifold, align 4, !tbaa !63
  %9 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %10 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %11 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %12 = bitcast %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %m_swapped = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc, %struct.btCollisionAlgorithmCreateFunc* %12, i32 0, i32 1
  %13 = load i8, i8* %m_swapped, align 4, !tbaa !25, !range !57
  %tobool = trunc i8 %13 to i1
  %call2 = call %class.btSphereTriangleCollisionAlgorithm* @_ZN34btSphereTriangleCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_b(%class.btSphereTriangleCollisionAlgorithm* %6, %class.btPersistentManifold* %8, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %9, %struct.btCollisionObjectWrapper* %10, %struct.btCollisionObjectWrapper* %11, i1 zeroext %tobool)
  %14 = bitcast %class.btSphereTriangleCollisionAlgorithm* %6 to %class.btCollisionAlgorithm*
  %15 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  ret %class.btCollisionAlgorithm* %14
}

declare %class.btSphereTriangleCollisionAlgorithm* @_ZN34btSphereTriangleCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_b(%class.btSphereTriangleCollisionAlgorithm* returned, %class.btPersistentManifold*, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8), %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1 zeroext) unnamed_addr #3

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN26btBoxBoxCollisionAlgorithm10CreateFuncD0Ev(%"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* %this, %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"*, %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %call = call %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* (%"struct.btBoxBoxCollisionAlgorithm::CreateFunc"*)*)(%"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* %this1) #8
  %0 = bitcast %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN26btBoxBoxCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %bbsize = alloca i32, align 4
  %ptr = alloca i8*, align 4
  store %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* %this, %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %this1 = load %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"*, %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast i32* %bbsize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 16, i32* %bbsize, align 4, !tbaa !30
  %1 = bitcast i8** %ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %2, i32 0, i32 0
  %3 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4, !tbaa !61
  %4 = load i32, i32* %bbsize, align 4, !tbaa !30
  %5 = bitcast %class.btDispatcher* %3 to i8* (%class.btDispatcher*, i32)***
  %vtable = load i8* (%class.btDispatcher*, i32)**, i8* (%class.btDispatcher*, i32)*** %5, align 4, !tbaa !6
  %vfn = getelementptr inbounds i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vtable, i64 14
  %6 = load i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vfn, align 4
  %call = call i8* %6(%class.btDispatcher* %3, i32 %4)
  store i8* %call, i8** %ptr, align 4, !tbaa !2
  %7 = load i8*, i8** %ptr, align 4, !tbaa !2
  %8 = bitcast i8* %7 to %class.btBoxBoxCollisionAlgorithm*
  %9 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %10 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %11 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call2 = call %class.btBoxBoxCollisionAlgorithm* @_ZN26btBoxBoxCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_(%class.btBoxBoxCollisionAlgorithm* %8, %class.btPersistentManifold* null, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %9, %struct.btCollisionObjectWrapper* %10, %struct.btCollisionObjectWrapper* %11)
  %12 = bitcast %class.btBoxBoxCollisionAlgorithm* %8 to %class.btCollisionAlgorithm*
  %13 = bitcast i8** %ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  %14 = bitcast i32* %bbsize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #8
  ret %class.btCollisionAlgorithm* %12
}

declare %class.btBoxBoxCollisionAlgorithm* @_ZN26btBoxBoxCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_(%class.btBoxBoxCollisionAlgorithm* returned, %class.btPersistentManifold*, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8), %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*) unnamed_addr #3

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN31btConvexPlaneCollisionAlgorithm10CreateFuncD0Ev(%"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this, %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*, %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %call = call %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* (%"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*)*)(%"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this1) #8
  %0 = bitcast %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN31btConvexPlaneCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btCollisionAlgorithm*, align 4
  %this.addr = alloca %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %mem = alloca i8*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this, %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %this1 = load %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*, %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %1, i32 0, i32 0
  %2 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4, !tbaa !61
  %3 = bitcast %class.btDispatcher* %2 to i8* (%class.btDispatcher*, i32)***
  %vtable = load i8* (%class.btDispatcher*, i32)**, i8* (%class.btDispatcher*, i32)*** %3, align 4, !tbaa !6
  %vfn = getelementptr inbounds i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vtable, i64 14
  %4 = load i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vfn, align 4
  %call = call i8* %4(%class.btDispatcher* %2, i32 28)
  store i8* %call, i8** %mem, align 4, !tbaa !2
  %5 = bitcast %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %m_swapped = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc, %struct.btCollisionAlgorithmCreateFunc* %5, i32 0, i32 1
  %6 = load i8, i8* %m_swapped, align 4, !tbaa !25, !range !57
  %tobool = trunc i8 %6 to i1
  br i1 %tobool, label %if.else, label %if.then

if.then:                                          ; preds = %entry
  %7 = load i8*, i8** %mem, align 4, !tbaa !2
  %8 = bitcast i8* %7 to %class.btConvexPlaneCollisionAlgorithm*
  %9 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %10 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %11 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %m_numPerturbationIterations = getelementptr inbounds %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc", %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this1, i32 0, i32 1
  %12 = load i32, i32* %m_numPerturbationIterations, align 4, !tbaa !48
  %m_minimumPointsPerturbationThreshold = getelementptr inbounds %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc", %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this1, i32 0, i32 2
  %13 = load i32, i32* %m_minimumPointsPerturbationThreshold, align 4, !tbaa !50
  %call2 = call %class.btConvexPlaneCollisionAlgorithm* @_ZN31btConvexPlaneCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_bii(%class.btConvexPlaneCollisionAlgorithm* %8, %class.btPersistentManifold* null, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %9, %struct.btCollisionObjectWrapper* %10, %struct.btCollisionObjectWrapper* %11, i1 zeroext false, i32 %12, i32 %13)
  %14 = bitcast %class.btConvexPlaneCollisionAlgorithm* %8 to %class.btCollisionAlgorithm*
  store %class.btCollisionAlgorithm* %14, %class.btCollisionAlgorithm** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %entry
  %15 = load i8*, i8** %mem, align 4, !tbaa !2
  %16 = bitcast i8* %15 to %class.btConvexPlaneCollisionAlgorithm*
  %17 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %18 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %19 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %m_numPerturbationIterations3 = getelementptr inbounds %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc", %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this1, i32 0, i32 1
  %20 = load i32, i32* %m_numPerturbationIterations3, align 4, !tbaa !48
  %m_minimumPointsPerturbationThreshold4 = getelementptr inbounds %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc", %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this1, i32 0, i32 2
  %21 = load i32, i32* %m_minimumPointsPerturbationThreshold4, align 4, !tbaa !50
  %call5 = call %class.btConvexPlaneCollisionAlgorithm* @_ZN31btConvexPlaneCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_bii(%class.btConvexPlaneCollisionAlgorithm* %16, %class.btPersistentManifold* null, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %17, %struct.btCollisionObjectWrapper* %18, %struct.btCollisionObjectWrapper* %19, i1 zeroext true, i32 %20, i32 %21)
  %22 = bitcast %class.btConvexPlaneCollisionAlgorithm* %16 to %class.btCollisionAlgorithm*
  store %class.btCollisionAlgorithm* %22, %class.btCollisionAlgorithm** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else, %if.then
  %23 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #8
  %24 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %retval, align 4
  ret %class.btCollisionAlgorithm* %24
}

declare %class.btConvexPlaneCollisionAlgorithm* @_ZN31btConvexPlaneCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_bii(%class.btConvexPlaneCollisionAlgorithm* returned, %class.btPersistentManifold*, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8), %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1 zeroext, i32, i32) unnamed_addr #3

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { cold noreturn nounwind }
attributes #8 = { nounwind }
attributes #9 = { builtin nounwind }
attributes #10 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{!9, !3, i64 24}
!9 = !{!"_ZTS31btDefaultCollisionConfiguration", !10, i64 4, !3, i64 8, !11, i64 12, !3, i64 16, !11, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !3, i64 44, !3, i64 48, !3, i64 52, !3, i64 56, !3, i64 60, !3, i64 64, !3, i64 68, !3, i64 72, !3, i64 76, !3, i64 80, !3, i64 84, !3, i64 88}
!10 = !{!"int", !4, i64 0}
!11 = !{!"bool", !4, i64 0}
!12 = !{!13, !10, i64 20}
!13 = !{!"_ZTS34btDefaultCollisionConstructionInfo", !3, i64 0, !3, i64 4, !10, i64 8, !10, i64 12, !10, i64 16, !10, i64 20}
!14 = !{!9, !3, i64 28}
!15 = !{!9, !3, i64 32}
!16 = !{!9, !3, i64 36}
!17 = !{!9, !3, i64 40}
!18 = !{!9, !3, i64 44}
!19 = !{!9, !3, i64 48}
!20 = !{!9, !3, i64 52}
!21 = !{!9, !3, i64 56}
!22 = !{!9, !3, i64 60}
!23 = !{!9, !3, i64 76}
!24 = !{!9, !3, i64 80}
!25 = !{!26, !11, i64 4}
!26 = !{!"_ZTS30btCollisionAlgorithmCreateFunc", !11, i64 4}
!27 = !{!9, !3, i64 72}
!28 = !{!9, !3, i64 88}
!29 = !{!9, !3, i64 84}
!30 = !{!10, !10, i64 0}
!31 = !{!13, !3, i64 0}
!32 = !{!9, !11, i64 12}
!33 = !{!9, !3, i64 8}
!34 = !{!13, !10, i64 8}
!35 = !{!13, !3, i64 4}
!36 = !{!9, !11, i64 20}
!37 = !{!9, !3, i64 16}
!38 = !{!13, !10, i64 12}
!39 = !{!40, !40, i64 0}
!40 = !{!"long", !4, i64 0}
!41 = !{!42, !44, i64 308}
!42 = !{!"_ZTS22btVoronoiSimplexSolver", !10, i64 0, !4, i64 4, !4, i64 84, !4, i64 164, !43, i64 244, !43, i64 260, !43, i64 276, !43, i64 292, !44, i64 308, !11, i64 312, !45, i64 316, !11, i64 356}
!43 = !{!"_ZTS9btVector3", !4, i64 0}
!44 = !{!"float", !4, i64 0}
!45 = !{!"_ZTS25btSubSimplexClosestResult", !43, i64 0, !46, i64 16, !4, i64 20, !11, i64 36}
!46 = !{!"_ZTS15btUsageBitfield", !47, i64 0, !47, i64 0, !47, i64 0, !47, i64 0, !47, i64 0, !47, i64 0, !47, i64 0, !47, i64 0}
!47 = !{!"short", !4, i64 0}
!48 = !{!49, !10, i64 8}
!49 = !{!"_ZTSN31btConvexPlaneCollisionAlgorithm10CreateFuncE", !10, i64 8, !10, i64 12}
!50 = !{!49, !10, i64 12}
!51 = !{!52, !10, i64 0}
!52 = !{!"_ZTS15btPoolAllocator", !10, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !3, i64 16}
!53 = !{!52, !10, i64 4}
!54 = !{!52, !3, i64 16}
!55 = !{!52, !3, i64 12}
!56 = !{!52, !10, i64 8}
!57 = !{i8 0, i8 2}
!58 = !{!59, !10, i64 16}
!59 = !{!"_ZTSN23btConvexConvexAlgorithm10CreateFuncE", !3, i64 8, !3, i64 12, !10, i64 16, !10, i64 20}
!60 = !{!59, !10, i64 20}
!61 = !{!62, !3, i64 0}
!62 = !{!"_ZTS36btCollisionAlgorithmConstructionInfo", !3, i64 0, !3, i64 4}
!63 = !{!62, !3, i64 4}
