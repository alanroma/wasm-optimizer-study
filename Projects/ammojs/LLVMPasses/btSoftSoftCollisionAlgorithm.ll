; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletSoftBody/btSoftSoftCollisionAlgorithm.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletSoftBody/btSoftSoftCollisionAlgorithm.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btSoftSoftCollisionAlgorithm = type { %class.btCollisionAlgorithm, i8, %class.btPersistentManifold*, %class.btSoftBody*, %class.btSoftBody* }
%class.btCollisionAlgorithm = type { i32 (...)**, %class.btDispatcher* }
%class.btDispatcher = type { i32 (...)** }
%class.btSoftBody = type { %class.btCollisionObject, %class.btAlignedObjectArray, %class.btSoftBodySolver*, %"struct.btSoftBody::Config", %"struct.btSoftBody::SolverState", %"struct.btSoftBody::Pose", i8*, %struct.btSoftBodyWorldInfo*, %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.25, %class.btAlignedObjectArray.29, %class.btAlignedObjectArray.33, %class.btAlignedObjectArray.37, %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.49, %class.btAlignedObjectArray.53, %class.btAlignedObjectArray.57, %class.btAlignedObjectArray.65, float, [2 x %class.btVector3], i8, %struct.btDbvt, %struct.btDbvt, %struct.btDbvt, %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.81, %class.btTransform, %class.btVector3, float, %class.btAlignedObjectArray.85 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%union.anon = type { i8* }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btSoftBodySolver = type { i32 (...)**, i32, i32, float }
%"struct.btSoftBody::Config" = type { i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, i32, i32, i32, i32, i32, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%"struct.btSoftBody::SolverState" = type { float, float, float, float, float }
%"struct.btSoftBody::Pose" = type { i8, i8, float, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12, %class.btVector3, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btSoftBodyWorldInfo = type { float, float, float, float, %class.btVector3, %class.btBroadphaseInterface*, %class.btDispatcher*, %class.btVector3, %struct.btSparseSdf }
%class.btBroadphaseInterface = type opaque
%struct.btSparseSdf = type { %class.btAlignedObjectArray.16, float, i32, i32, i32, i32, i32 }
%class.btAlignedObjectArray.16 = type <{ %class.btAlignedAllocator.17, [3 x i8], i32, i32, %"struct.btSparseSdf<3>::Cell"**, i8, [3 x i8] }>
%class.btAlignedAllocator.17 = type { i8 }
%"struct.btSparseSdf<3>::Cell" = type opaque
%class.btAlignedObjectArray.20 = type <{ %class.btAlignedAllocator.21, [3 x i8], i32, i32, %"struct.btSoftBody::Note"*, i8, [3 x i8] }>
%class.btAlignedAllocator.21 = type { i8 }
%"struct.btSoftBody::Note" = type { %"struct.btSoftBody::Element", i8*, %class.btVector3, i32, [4 x %"struct.btSoftBody::Node"*], [4 x float] }
%"struct.btSoftBody::Element" = type { i8* }
%"struct.btSoftBody::Node" = type <{ %"struct.btSoftBody::Feature", %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, %struct.btDbvtNode*, i8, [3 x i8] }>
%"struct.btSoftBody::Feature" = type { %"struct.btSoftBody::Element", %"struct.btSoftBody::Material"* }
%"struct.btSoftBody::Material" = type { %"struct.btSoftBody::Element", float, float, float, i32 }
%struct.btDbvtNode = type { %struct.btDbvtAabbMm, %struct.btDbvtNode*, %union.anon.23 }
%struct.btDbvtAabbMm = type { %class.btVector3, %class.btVector3 }
%union.anon.23 = type { [2 x %struct.btDbvtNode*] }
%class.btAlignedObjectArray.25 = type <{ %class.btAlignedAllocator.26, [3 x i8], i32, i32, %"struct.btSoftBody::Node"*, i8, [3 x i8] }>
%class.btAlignedAllocator.26 = type { i8 }
%class.btAlignedObjectArray.29 = type <{ %class.btAlignedAllocator.30, [3 x i8], i32, i32, %"struct.btSoftBody::Link"*, i8, [3 x i8] }>
%class.btAlignedAllocator.30 = type { i8 }
%"struct.btSoftBody::Link" = type { %"struct.btSoftBody::Feature", [2 x %"struct.btSoftBody::Node"*], float, i8, float, float, float, %class.btVector3 }
%class.btAlignedObjectArray.33 = type <{ %class.btAlignedAllocator.34, [3 x i8], i32, i32, %"struct.btSoftBody::Face"*, i8, [3 x i8] }>
%class.btAlignedAllocator.34 = type { i8 }
%"struct.btSoftBody::Face" = type { %"struct.btSoftBody::Feature", [3 x %"struct.btSoftBody::Node"*], %class.btVector3, float, %struct.btDbvtNode* }
%class.btAlignedObjectArray.37 = type <{ %class.btAlignedAllocator.38, [3 x i8], i32, i32, %"struct.btSoftBody::Tetra"*, i8, [3 x i8] }>
%class.btAlignedAllocator.38 = type { i8 }
%"struct.btSoftBody::Tetra" = type { %"struct.btSoftBody::Feature", [4 x %"struct.btSoftBody::Node"*], float, %struct.btDbvtNode*, [4 x %class.btVector3], float, float }
%class.btAlignedObjectArray.41 = type <{ %class.btAlignedAllocator.42, [3 x i8], i32, i32, %"struct.btSoftBody::Anchor"*, i8, [3 x i8] }>
%class.btAlignedAllocator.42 = type { i8 }
%"struct.btSoftBody::Anchor" = type { %"struct.btSoftBody::Node"*, %class.btVector3, %class.btRigidBody*, float, %class.btMatrix3x3, %class.btVector3, float }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.44, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray.44 = type <{ %class.btAlignedAllocator.45, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.45 = type { i8 }
%class.btTypedConstraint = type opaque
%class.btAlignedObjectArray.49 = type <{ %class.btAlignedAllocator.50, [3 x i8], i32, i32, %"struct.btSoftBody::RContact"*, i8, [3 x i8] }>
%class.btAlignedAllocator.50 = type { i8 }
%"struct.btSoftBody::RContact" = type { %"struct.btSoftBody::sCti", %"struct.btSoftBody::Node"*, %class.btMatrix3x3, %class.btVector3, float, float, float }
%"struct.btSoftBody::sCti" = type { %class.btCollisionObject*, %class.btVector3, float }
%class.btAlignedObjectArray.53 = type <{ %class.btAlignedAllocator.54, [3 x i8], i32, i32, %"struct.btSoftBody::SContact"*, i8, [3 x i8] }>
%class.btAlignedAllocator.54 = type { i8 }
%"struct.btSoftBody::SContact" = type { %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Face"*, %class.btVector3, %class.btVector3, float, float, [2 x float] }
%class.btAlignedObjectArray.57 = type <{ %class.btAlignedAllocator.58, [3 x i8], i32, i32, %"struct.btSoftBody::Joint"**, i8, [3 x i8] }>
%class.btAlignedAllocator.58 = type { i8 }
%"struct.btSoftBody::Joint" = type <{ i32 (...)**, [2 x %"struct.btSoftBody::Body"], [2 x %class.btVector3], float, float, float, %class.btVector3, %class.btVector3, %class.btMatrix3x3, i8, [3 x i8] }>
%"struct.btSoftBody::Body" = type { %"struct.btSoftBody::Cluster"*, %class.btRigidBody*, %class.btCollisionObject* }
%"struct.btSoftBody::Cluster" = type { %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.60, %class.btAlignedObjectArray.8, %class.btTransform, float, float, %class.btMatrix3x3, %class.btMatrix3x3, %class.btVector3, [2 x %class.btVector3], [2 x %class.btVector3], i32, i32, %class.btVector3, %class.btVector3, %struct.btDbvtNode*, float, float, float, float, float, float, i8, i8, i32 }
%class.btAlignedObjectArray.60 = type <{ %class.btAlignedAllocator.61, [3 x i8], i32, i32, %"struct.btSoftBody::Node"**, i8, [3 x i8] }>
%class.btAlignedAllocator.61 = type { i8 }
%class.btAlignedObjectArray.65 = type <{ %class.btAlignedAllocator.66, [3 x i8], i32, i32, %"struct.btSoftBody::Material"**, i8, [3 x i8] }>
%class.btAlignedAllocator.66 = type { i8 }
%struct.btDbvt = type { %struct.btDbvtNode*, %struct.btDbvtNode*, i32, i32, i32, %class.btAlignedObjectArray.69, %class.btAlignedObjectArray.73 }
%class.btAlignedObjectArray.69 = type <{ %class.btAlignedAllocator.70, [3 x i8], i32, i32, %"struct.btDbvt::sStkNN"*, i8, [3 x i8] }>
%class.btAlignedAllocator.70 = type { i8 }
%"struct.btDbvt::sStkNN" = type { %struct.btDbvtNode*, %struct.btDbvtNode* }
%class.btAlignedObjectArray.73 = type <{ %class.btAlignedAllocator.74, [3 x i8], i32, i32, %struct.btDbvtNode**, i8, [3 x i8] }>
%class.btAlignedAllocator.74 = type { i8 }
%class.btAlignedObjectArray.77 = type <{ %class.btAlignedAllocator.78, [3 x i8], i32, i32, %"struct.btSoftBody::Cluster"**, i8, [3 x i8] }>
%class.btAlignedAllocator.78 = type { i8 }
%class.btAlignedObjectArray.81 = type <{ %class.btAlignedAllocator.82, [3 x i8], i32, i32, i8*, i8, [3 x i8] }>
%class.btAlignedAllocator.82 = type { i8 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%class.btAlignedObjectArray.85 = type <{ %class.btAlignedAllocator.86, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.86 = type { i8 }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, i32, i32, i32, i32, i8*, i8, float, float, float, float, float, float, float, i32, %class.btVector3, %class.btVector3 }
%struct.btCollisionAlgorithmConstructionInfo = type { %class.btDispatcher*, %class.btPersistentManifold* }
%struct.btCollisionObjectWrapper = type { %struct.btCollisionObjectWrapper*, %class.btCollisionShape*, %class.btCollisionObject*, %class.btTransform*, i32, i32 }
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btIDebugDraw = type { i32 (...)** }
%class.btManifoldResult = type { %"struct.btDiscreteCollisionDetectorInterface::Result", %class.btPersistentManifold*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i32, i32, i32, i32 }
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }
%class.btAlignedObjectArray.89 = type <{ %class.btAlignedAllocator.90, [3 x i8], i32, i32, %class.btPersistentManifold**, i8, [3 x i8] }>
%class.btAlignedAllocator.90 = type { i8 }

$_ZN20btCollisionAlgorithmD2Ev = comdat any

$_ZNK24btCollisionObjectWrapper18getCollisionObjectEv = comdat any

$_ZN10btSoftBody17getSoftBodySolverEv = comdat any

$_ZN28btSoftSoftCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_ = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_ = comdat any

$_ZTS20btCollisionAlgorithm = comdat any

$_ZTI20btCollisionAlgorithm = comdat any

@_ZTV28btSoftSoftCollisionAlgorithm = hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI28btSoftSoftCollisionAlgorithm to i8*), i8* bitcast (%class.btSoftSoftCollisionAlgorithm* (%class.btSoftSoftCollisionAlgorithm*)* @_ZN28btSoftSoftCollisionAlgorithmD1Ev to i8*), i8* bitcast (void (%class.btSoftSoftCollisionAlgorithm*)* @_ZN28btSoftSoftCollisionAlgorithmD0Ev to i8*), i8* bitcast (void (%class.btSoftSoftCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)* @_ZN28btSoftSoftCollisionAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResult to i8*), i8* bitcast (float (%class.btSoftSoftCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)* @_ZN28btSoftSoftCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult to i8*), i8* bitcast (void (%class.btSoftSoftCollisionAlgorithm*, %class.btAlignedObjectArray.89*)* @_ZN28btSoftSoftCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS28btSoftSoftCollisionAlgorithm = hidden constant [31 x i8] c"28btSoftSoftCollisionAlgorithm\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS20btCollisionAlgorithm = linkonce_odr hidden constant [23 x i8] c"20btCollisionAlgorithm\00", comdat, align 1
@_ZTI20btCollisionAlgorithm = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([23 x i8], [23 x i8]* @_ZTS20btCollisionAlgorithm, i32 0, i32 0) }, comdat, align 4
@_ZTI28btSoftSoftCollisionAlgorithm = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([31 x i8], [31 x i8]* @_ZTS28btSoftSoftCollisionAlgorithm, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI20btCollisionAlgorithm to i8*) }, align 4

@_ZN28btSoftSoftCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_ = hidden unnamed_addr alias %class.btSoftSoftCollisionAlgorithm* (%class.btSoftSoftCollisionAlgorithm*, %class.btPersistentManifold*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*), %class.btSoftSoftCollisionAlgorithm* (%class.btSoftSoftCollisionAlgorithm*, %class.btPersistentManifold*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN28btSoftSoftCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_
@_ZN28btSoftSoftCollisionAlgorithmD1Ev = hidden unnamed_addr alias %class.btSoftSoftCollisionAlgorithm* (%class.btSoftSoftCollisionAlgorithm*), %class.btSoftSoftCollisionAlgorithm* (%class.btSoftSoftCollisionAlgorithm*)* @_ZN28btSoftSoftCollisionAlgorithmD2Ev

define hidden %class.btSoftSoftCollisionAlgorithm* @_ZN28btSoftSoftCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_(%class.btSoftSoftCollisionAlgorithm* returned %this, %class.btPersistentManifold* %0, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %1, %struct.btCollisionObjectWrapper* %2) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSoftSoftCollisionAlgorithm*, align 4
  %.addr = alloca %class.btPersistentManifold*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %.addr1 = alloca %struct.btCollisionObjectWrapper*, align 4
  %.addr2 = alloca %struct.btCollisionObjectWrapper*, align 4
  store %class.btSoftSoftCollisionAlgorithm* %this, %class.btSoftSoftCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %class.btPersistentManifold* %0, %class.btPersistentManifold** %.addr, align 4, !tbaa !2
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %1, %struct.btCollisionObjectWrapper** %.addr1, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %2, %struct.btCollisionObjectWrapper** %.addr2, align 4, !tbaa !2
  %this3 = load %class.btSoftSoftCollisionAlgorithm*, %class.btSoftSoftCollisionAlgorithm** %this.addr, align 4
  %3 = bitcast %class.btSoftSoftCollisionAlgorithm* %this3 to %class.btCollisionAlgorithm*
  %4 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %call = call %class.btCollisionAlgorithm* @_ZN20btCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo(%class.btCollisionAlgorithm* %3, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %4)
  %5 = bitcast %class.btSoftSoftCollisionAlgorithm* %this3 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV28btSoftSoftCollisionAlgorithm, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %5, align 4, !tbaa !6
  ret %class.btSoftSoftCollisionAlgorithm* %this3
}

declare %class.btCollisionAlgorithm* @_ZN20btCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo(%class.btCollisionAlgorithm* returned, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8)) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN20btCollisionAlgorithmD2Ev(%class.btCollisionAlgorithm* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionAlgorithm*, align 4
  store %class.btCollisionAlgorithm* %this, %class.btCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %this.addr, align 4
  ret %class.btCollisionAlgorithm* %this1
}

; Function Attrs: nounwind
define hidden %class.btSoftSoftCollisionAlgorithm* @_ZN28btSoftSoftCollisionAlgorithmD2Ev(%class.btSoftSoftCollisionAlgorithm* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSoftSoftCollisionAlgorithm*, align 4
  store %class.btSoftSoftCollisionAlgorithm* %this, %class.btSoftSoftCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftSoftCollisionAlgorithm*, %class.btSoftSoftCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast %class.btSoftSoftCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %call = call %class.btCollisionAlgorithm* @_ZN20btCollisionAlgorithmD2Ev(%class.btCollisionAlgorithm* %0) #7
  ret %class.btSoftSoftCollisionAlgorithm* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN28btSoftSoftCollisionAlgorithmD0Ev(%class.btSoftSoftCollisionAlgorithm* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSoftSoftCollisionAlgorithm*, align 4
  store %class.btSoftSoftCollisionAlgorithm* %this, %class.btSoftSoftCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftSoftCollisionAlgorithm*, %class.btSoftSoftCollisionAlgorithm** %this.addr, align 4
  %call = call %class.btSoftSoftCollisionAlgorithm* @_ZN28btSoftSoftCollisionAlgorithmD1Ev(%class.btSoftSoftCollisionAlgorithm* %this1) #7
  %0 = bitcast %class.btSoftSoftCollisionAlgorithm* %this1 to i8*
  call void @_ZdlPv(i8* %0) #8
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #3

define hidden void @_ZN28btSoftSoftCollisionAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResult(%class.btSoftSoftCollisionAlgorithm* %this, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %0, %class.btManifoldResult* %1) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSoftSoftCollisionAlgorithm*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %.addr = alloca %struct.btDispatcherInfo*, align 4
  %.addr1 = alloca %class.btManifoldResult*, align 4
  %soft0 = alloca %class.btSoftBody*, align 4
  %soft1 = alloca %class.btSoftBody*, align 4
  store %class.btSoftSoftCollisionAlgorithm* %this, %class.btSoftSoftCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  store %struct.btDispatcherInfo* %0, %struct.btDispatcherInfo** %.addr, align 4, !tbaa !2
  store %class.btManifoldResult* %1, %class.btManifoldResult** %.addr1, align 4, !tbaa !2
  %this2 = load %class.btSoftSoftCollisionAlgorithm*, %class.btSoftSoftCollisionAlgorithm** %this.addr, align 4
  %2 = bitcast %class.btSoftBody** %soft0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %call = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %3)
  %4 = bitcast %class.btCollisionObject* %call to %class.btSoftBody*
  store %class.btSoftBody* %4, %class.btSoftBody** %soft0, align 4, !tbaa !2
  %5 = bitcast %class.btSoftBody** %soft1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call3 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %6)
  %7 = bitcast %class.btCollisionObject* %call3 to %class.btSoftBody*
  store %class.btSoftBody* %7, %class.btSoftBody** %soft1, align 4, !tbaa !2
  %8 = load %class.btSoftBody*, %class.btSoftBody** %soft0, align 4, !tbaa !2
  %call4 = call %class.btSoftBodySolver* @_ZN10btSoftBody17getSoftBodySolverEv(%class.btSoftBody* %8)
  %9 = load %class.btSoftBody*, %class.btSoftBody** %soft0, align 4, !tbaa !2
  %10 = load %class.btSoftBody*, %class.btSoftBody** %soft1, align 4, !tbaa !2
  %11 = bitcast %class.btSoftBodySolver* %call4 to void (%class.btSoftBodySolver*, %class.btSoftBody*, %class.btSoftBody*)***
  %vtable = load void (%class.btSoftBodySolver*, %class.btSoftBody*, %class.btSoftBody*)**, void (%class.btSoftBodySolver*, %class.btSoftBody*, %class.btSoftBody*)*** %11, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btSoftBodySolver*, %class.btSoftBody*, %class.btSoftBody*)*, void (%class.btSoftBodySolver*, %class.btSoftBody*, %class.btSoftBody*)** %vtable, i64 10
  %12 = load void (%class.btSoftBodySolver*, %class.btSoftBody*, %class.btSoftBody*)*, void (%class.btSoftBodySolver*, %class.btSoftBody*, %class.btSoftBody*)** %vfn, align 4
  call void %12(%class.btSoftBodySolver* %call4, %class.btSoftBody* %9, %class.btSoftBody* %10)
  %13 = bitcast %class.btSoftBody** %soft1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #7
  %14 = bitcast %class.btSoftBody** %soft0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #7
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #4

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %this) #5 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_collisionObject = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_collisionObject, align 4, !tbaa !8
  ret %class.btCollisionObject* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btSoftBodySolver* @_ZN10btSoftBody17getSoftBodySolverEv(%class.btSoftBody* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btSoftBody*, align 4
  store %class.btSoftBody* %this, %class.btSoftBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftBody*, %class.btSoftBody** %this.addr, align 4
  %m_softBodySolver = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %this1, i32 0, i32 2
  %0 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %m_softBodySolver, align 4, !tbaa !11
  ret %class.btSoftBodySolver* %0
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #4

; Function Attrs: nounwind
define hidden float @_ZN28btSoftSoftCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult(%class.btSoftSoftCollisionAlgorithm* %this, %class.btCollisionObject* %0, %class.btCollisionObject* %1, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %2, %class.btManifoldResult* %3) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSoftSoftCollisionAlgorithm*, align 4
  %.addr = alloca %class.btCollisionObject*, align 4
  %.addr1 = alloca %class.btCollisionObject*, align 4
  %.addr2 = alloca %struct.btDispatcherInfo*, align 4
  %.addr3 = alloca %class.btManifoldResult*, align 4
  store %class.btSoftSoftCollisionAlgorithm* %this, %class.btSoftSoftCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %0, %class.btCollisionObject** %.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %1, %class.btCollisionObject** %.addr1, align 4, !tbaa !2
  store %struct.btDispatcherInfo* %2, %struct.btDispatcherInfo** %.addr2, align 4, !tbaa !2
  store %class.btManifoldResult* %3, %class.btManifoldResult** %.addr3, align 4, !tbaa !2
  %this4 = load %class.btSoftSoftCollisionAlgorithm*, %class.btSoftSoftCollisionAlgorithm** %this.addr, align 4
  ret float 1.000000e+00
}

define linkonce_odr hidden void @_ZN28btSoftSoftCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE(%class.btSoftSoftCollisionAlgorithm* %this, %class.btAlignedObjectArray.89* nonnull align 4 dereferenceable(17) %manifoldArray) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btSoftSoftCollisionAlgorithm*, align 4
  %manifoldArray.addr = alloca %class.btAlignedObjectArray.89*, align 4
  store %class.btSoftSoftCollisionAlgorithm* %this, %class.btSoftSoftCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.89* %manifoldArray, %class.btAlignedObjectArray.89** %manifoldArray.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftSoftCollisionAlgorithm*, %class.btSoftSoftCollisionAlgorithm** %this.addr, align 4
  %m_manifoldPtr = getelementptr inbounds %class.btSoftSoftCollisionAlgorithm, %class.btSoftSoftCollisionAlgorithm* %this1, i32 0, i32 2
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr, align 4, !tbaa !63
  %tobool = icmp ne %class.btPersistentManifold* %0, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %m_ownManifold = getelementptr inbounds %class.btSoftSoftCollisionAlgorithm, %class.btSoftSoftCollisionAlgorithm* %this1, i32 0, i32 1
  %1 = load i8, i8* %m_ownManifold, align 4, !tbaa !65, !range !66
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %2 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %manifoldArray.addr, align 4, !tbaa !2
  %m_manifoldPtr3 = getelementptr inbounds %class.btSoftSoftCollisionAlgorithm, %class.btSoftSoftCollisionAlgorithm* %this1, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_(%class.btAlignedObjectArray.89* %2, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %m_manifoldPtr3)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_(%class.btAlignedObjectArray.89* %this, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %_Val) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  %_Val.addr = alloca %class.btPersistentManifold**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  store %class.btPersistentManifold** %_Val, %class.btPersistentManifold*** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.89* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !67
  %1 = load i32, i32* %sz, align 4, !tbaa !67
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.89* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.89* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi(%class.btAlignedObjectArray.89* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.89* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 4
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !68
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !71
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %2, i32 %3
  %4 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %5 = bitcast i8* %4 to %class.btPersistentManifold**
  %6 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %_Val.addr, align 4, !tbaa !2
  %7 = load %class.btPersistentManifold*, %class.btPersistentManifold** %6, align 4, !tbaa !2
  store %class.btPersistentManifold* %7, %class.btPersistentManifold** %5, align 4, !tbaa !2
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size5, align 4, !tbaa !71
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size5, align 4, !tbaa !71
  %9 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.89* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !71
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.89* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !72
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.89* %this, i32 %_Count) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !67
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.89* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !67
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btPersistentManifold*** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !67
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.89* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btPersistentManifold**
  store %class.btPersistentManifold** %3, %class.btPersistentManifold*** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.89* %this1)
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.89* %this1, i32 0, i32 %call3, %class.btPersistentManifold** %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.89* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.89* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.89* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !73
  %5 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 4
  store %class.btPersistentManifold** %5, %class.btPersistentManifold*** %m_data, align 4, !tbaa !68
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !67
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !72
  %7 = bitcast %class.btPersistentManifold*** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi(%class.btAlignedObjectArray.89* %this, i32 %size) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !67
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !67
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !67
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.89* %this, i32 %size) #6 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !67
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !67
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !67
  %call = call %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.90* %m_allocator, i32 %1, %class.btPersistentManifold*** null)
  %2 = bitcast %class.btPersistentManifold** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.89* %this, i32 %start, i32 %end, %class.btPersistentManifold** %dest) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btPersistentManifold**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !67
  store i32 %end, i32* %end.addr, align 4, !tbaa !67
  store %class.btPersistentManifold** %dest, %class.btPersistentManifold*** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %start.addr, align 4, !tbaa !67
  store i32 %1, i32* %i, align 4, !tbaa !67
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !67
  %3 = load i32, i32* %end.addr, align 4, !tbaa !67
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !67
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %4, i32 %5
  %6 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %7 = bitcast i8* %6 to %class.btPersistentManifold**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 4
  %8 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !68
  %9 = load i32, i32* %i, align 4, !tbaa !67
  %arrayidx2 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %8, i32 %9
  %10 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx2, align 4, !tbaa !2
  store %class.btPersistentManifold* %10, %class.btPersistentManifold** %7, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !67
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !67
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.89* %this, i32 %first, i32 %last) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !67
  store i32 %last, i32* %last.addr, align 4, !tbaa !67
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %first.addr, align 4, !tbaa !67
  store i32 %1, i32* %i, align 4, !tbaa !67
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !67
  %3 = load i32, i32* %last.addr, align 4, !tbaa !67
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 4
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !68
  %5 = load i32, i32* %i, align 4, !tbaa !67
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !67
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !67
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.89* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !68
  %tobool = icmp ne %class.btPersistentManifold** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !73, !range !66
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 4
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data4, align 4, !tbaa !68
  call void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.90* %m_allocator, %class.btPersistentManifold** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data5, align 4, !tbaa !68
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.90* %this, i32 %n, %class.btPersistentManifold*** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.90*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btPersistentManifold***, align 4
  store %class.btAlignedAllocator.90* %this, %class.btAlignedAllocator.90** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !67
  store %class.btPersistentManifold*** %hint, %class.btPersistentManifold**** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.90*, %class.btAlignedAllocator.90** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !67
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btPersistentManifold**
  ret %class.btPersistentManifold** %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #1

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.90* %this, %class.btPersistentManifold** %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.90*, align 4
  %ptr.addr = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedAllocator.90* %this, %class.btAlignedAllocator.90** %this.addr, align 4, !tbaa !2
  store %class.btPersistentManifold** %ptr, %class.btPersistentManifold*** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.90*, %class.btAlignedAllocator.90** %this.addr, align 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btPersistentManifold** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind }
attributes #8 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{!9, !3, i64 8}
!9 = !{!"_ZTS24btCollisionObjectWrapper", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !10, i64 16, !10, i64 20}
!10 = !{!"int", !4, i64 0}
!11 = !{!12, !3, i64 284}
!12 = !{!"_ZTS10btSoftBody", !13, i64 264, !3, i64 284, !16, i64 288, !23, i64 452, !24, i64 472, !3, i64 680, !3, i64 684, !31, i64 688, !33, i64 708, !35, i64 728, !37, i64 748, !39, i64 768, !41, i64 788, !43, i64 808, !45, i64 828, !47, i64 848, !49, i64 868, !18, i64 888, !4, i64 892, !15, i64 924, !51, i64 928, !51, i64 988, !51, i64 1048, !56, i64 1108, !58, i64 1128, !60, i64 1148, !29, i64 1212, !18, i64 1228, !61, i64 1232}
!13 = !{!"_ZTS20btAlignedObjectArrayIPK17btCollisionObjectE", !14, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !15, i64 16}
!14 = !{!"_ZTS18btAlignedAllocatorIPK17btCollisionObjectLj16EE"}
!15 = !{!"bool", !4, i64 0}
!16 = !{!"_ZTSN10btSoftBody6ConfigE", !17, i64 0, !18, i64 4, !18, i64 8, !18, i64 12, !18, i64 16, !18, i64 20, !18, i64 24, !18, i64 28, !18, i64 32, !18, i64 36, !18, i64 40, !18, i64 44, !18, i64 48, !18, i64 52, !18, i64 56, !18, i64 60, !18, i64 64, !18, i64 68, !18, i64 72, !18, i64 76, !18, i64 80, !10, i64 84, !10, i64 88, !10, i64 92, !10, i64 96, !10, i64 100, !19, i64 104, !21, i64 124, !21, i64 144}
!17 = !{!"_ZTSN10btSoftBody10eAeroModel1_E", !4, i64 0}
!18 = !{!"float", !4, i64 0}
!19 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody8eVSolver1_EE", !20, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !15, i64 16}
!20 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody8eVSolver1_ELj16EE"}
!21 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody8ePSolver1_EE", !22, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !15, i64 16}
!22 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody8ePSolver1_ELj16EE"}
!23 = !{!"_ZTSN10btSoftBody11SolverStateE", !18, i64 0, !18, i64 4, !18, i64 8, !18, i64 12, !18, i64 16}
!24 = !{!"_ZTSN10btSoftBody4PoseE", !15, i64 0, !15, i64 1, !18, i64 4, !25, i64 8, !27, i64 28, !29, i64 48, !30, i64 64, !30, i64 112, !30, i64 160}
!25 = !{!"_ZTS20btAlignedObjectArrayI9btVector3E", !26, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !15, i64 16}
!26 = !{!"_ZTS18btAlignedAllocatorI9btVector3Lj16EE"}
!27 = !{!"_ZTS20btAlignedObjectArrayIfE", !28, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !15, i64 16}
!28 = !{!"_ZTS18btAlignedAllocatorIfLj16EE"}
!29 = !{!"_ZTS9btVector3", !4, i64 0}
!30 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!31 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody4NoteEE", !32, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !15, i64 16}
!32 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody4NoteELj16EE"}
!33 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody4NodeEE", !34, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !15, i64 16}
!34 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody4NodeELj16EE"}
!35 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody4LinkEE", !36, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !15, i64 16}
!36 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody4LinkELj16EE"}
!37 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody4FaceEE", !38, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !15, i64 16}
!38 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody4FaceELj16EE"}
!39 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody5TetraEE", !40, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !15, i64 16}
!40 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody5TetraELj16EE"}
!41 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody6AnchorEE", !42, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !15, i64 16}
!42 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody6AnchorELj16EE"}
!43 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody8RContactEE", !44, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !15, i64 16}
!44 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody8RContactELj16EE"}
!45 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody8SContactEE", !46, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !15, i64 16}
!46 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody8SContactELj16EE"}
!47 = !{!"_ZTS20btAlignedObjectArrayIPN10btSoftBody5JointEE", !48, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !15, i64 16}
!48 = !{!"_ZTS18btAlignedAllocatorIPN10btSoftBody5JointELj16EE"}
!49 = !{!"_ZTS20btAlignedObjectArrayIPN10btSoftBody8MaterialEE", !50, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !15, i64 16}
!50 = !{!"_ZTS18btAlignedAllocatorIPN10btSoftBody8MaterialELj16EE"}
!51 = !{!"_ZTS6btDbvt", !3, i64 0, !3, i64 4, !10, i64 8, !10, i64 12, !10, i64 16, !52, i64 20, !54, i64 40}
!52 = !{!"_ZTS20btAlignedObjectArrayIN6btDbvt6sStkNNEE", !53, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !15, i64 16}
!53 = !{!"_ZTS18btAlignedAllocatorIN6btDbvt6sStkNNELj16EE"}
!54 = !{!"_ZTS20btAlignedObjectArrayIPK10btDbvtNodeE", !55, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !15, i64 16}
!55 = !{!"_ZTS18btAlignedAllocatorIPK10btDbvtNodeLj16EE"}
!56 = !{!"_ZTS20btAlignedObjectArrayIPN10btSoftBody7ClusterEE", !57, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !15, i64 16}
!57 = !{!"_ZTS18btAlignedAllocatorIPN10btSoftBody7ClusterELj16EE"}
!58 = !{!"_ZTS20btAlignedObjectArrayIbE", !59, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !15, i64 16}
!59 = !{!"_ZTS18btAlignedAllocatorIbLj16EE"}
!60 = !{!"_ZTS11btTransform", !30, i64 0, !29, i64 48}
!61 = !{!"_ZTS20btAlignedObjectArrayIiE", !62, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !15, i64 16}
!62 = !{!"_ZTS18btAlignedAllocatorIiLj16EE"}
!63 = !{!64, !3, i64 12}
!64 = !{!"_ZTS28btSoftSoftCollisionAlgorithm", !15, i64 8, !3, i64 12, !3, i64 16, !3, i64 20}
!65 = !{!64, !15, i64 8}
!66 = !{i8 0, i8 2}
!67 = !{!10, !10, i64 0}
!68 = !{!69, !3, i64 12}
!69 = !{!"_ZTS20btAlignedObjectArrayIP20btPersistentManifoldE", !70, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !15, i64 16}
!70 = !{!"_ZTS18btAlignedAllocatorIP20btPersistentManifoldLj16EE"}
!71 = !{!69, !10, i64 4}
!72 = !{!69, !10, i64 8}
!73 = !{!69, !15, i64 16}
