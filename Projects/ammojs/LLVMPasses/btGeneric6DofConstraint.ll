; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/ConstraintSolver/btGeneric6DofConstraint.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/ConstraintSolver/btGeneric6DofConstraint.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btGeneric6DofConstraint = type <{ %class.btTypedConstraint, %class.btTransform, %class.btTransform, [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry], %class.btTranslationalLimitMotor, [3 x %class.btRotationalLimitMotor], float, %class.btTransform, %class.btTransform, %class.btVector3, [3 x %class.btVector3], %class.btVector3, float, float, i8, [3 x i8], %class.btVector3, i8, i8, [2 x i8], i32, i8, [3 x i8] }>
%class.btTypedConstraint = type { i32 (...)**, %struct.btTypedObject, i32, %union.anon, float, i8, i8, i32, %class.btRigidBody*, %class.btRigidBody*, float, float, %struct.btJointFeedback* }
%struct.btTypedObject = type { i32 }
%union.anon = type { i32 }
%struct.btJointFeedback = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%class.btJacobianEntry = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float }
%class.btTranslationalLimitMotor = type { %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, %class.btVector3, %class.btVector3, %class.btVector3, [3 x i8], %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, [3 x i32] }
%class.btRotationalLimitMotor = type { float, float, float, float, float, float, float, float, float, float, float, i8, float, float, i32, float }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btVector3 = type { [4 x float] }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon.0, float, float, float, i32, i32 }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%union.anon.0 = type { i8* }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%"struct.btTypedConstraint::btConstraintInfo1" = type { i32, i32 }
%"struct.btTypedConstraint::btConstraintInfo2" = type { float, float, float*, float*, float*, float*, i32, float*, float*, float*, float*, i32*, i32, float }
%class.btAlignedObjectArray.1 = type opaque
%struct.btSolverBody = type { %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btRigidBody* }
%class.btSerializer = type opaque
%struct.btGeneric6DofConstraintData = type { %struct.btTypedConstraintData, %struct.btTransformFloatData, %struct.btTransformFloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, i32, i32 }
%struct.btTypedConstraintData = type { %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData*, i8*, i32, i32, i32, i32, float, float, i32, i32, float, i32 }
%struct.btRigidBodyFloatData = type { %struct.btCollisionObjectFloatData, %struct.btMatrix3x3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, float, float, float, float, float, float, float, float, i32 }
%struct.btCollisionObjectFloatData = type { i8*, i8*, %struct.btCollisionShapeData*, i8*, %struct.btTransformFloatData, %struct.btTransformFloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, float, float, float, float, float, float, float, i32, i32, i32, i32, i32, i32, i32, [4 x i8] }
%struct.btCollisionShapeData = type opaque
%struct.btMatrix3x3FloatData = type { [3 x %struct.btVector3FloatData] }
%struct.btTransformFloatData = type { %struct.btMatrix3x3FloatData, %struct.btVector3FloatData }
%struct.btVector3FloatData = type { [4 x float] }

$_ZN11btTransformC2ERKS_ = comdat any

$_ZN15btJacobianEntryC2Ev = comdat any

$_ZN25btTranslationalLimitMotorC2Ev = comdat any

$_ZN22btRotationalLimitMotorC2Ev = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK11btRigidBody24getCenterOfMassTransformEv = comdat any

$_ZNK11btTransformmlERKS_ = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_Z7btAtan2ff = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_Z6btAsinf = comdat any

$_ZN22btRotationalLimitMotor16needApplyTorquesEv = comdat any

$_ZNK11btRigidBody18getAngularVelocityEv = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZN11btRigidBody18applyTorqueImpulseERK9btVector3 = comdat any

$_ZngRK9btVector3 = comdat any

$_ZNK11btRigidBody23getCenterOfMassPositionEv = comdat any

$_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3 = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZN11btRigidBody12applyImpulseERK9btVector3S2_ = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZN11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x37inverseEv = comdat any

$_ZNK11btMatrix3x39getColumnEi = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZN17btTypedConstraint13getRigidBodyAEv = comdat any

$_ZNK11btRigidBody10getInvMassEv = comdat any

$_ZN17btTypedConstraint13getRigidBodyBEv = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_ZNK11btRigidBody22getInvInertiaDiagLocalEv = comdat any

$_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f = comdat any

$_ZN15btJacobianEntryC2ERK9btVector3RK11btMatrix3x3S5_S2_S2_ = comdat any

$_Z21btAdjustAngleToLimitsfff = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN25btTranslationalLimitMotor9isLimitedEi = comdat any

$_ZN25btTranslationalLimitMotor14needApplyForceEi = comdat any

$_ZNK11btRigidBody17getLinearVelocityEv = comdat any

$_ZN23btGeneric6DofConstraint23getRotationalLimitMotorEi = comdat any

$_ZN11btTransform9getOriginEv = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZNK9btVector310normalizedEv = comdat any

$_ZN11btTransform11setIdentityEv = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK11btTransform7inverseEv = comdat any

$_ZN17btTypedConstraintD2Ev = comdat any

$_ZN23btGeneric6DofConstraintD0Ev = comdat any

$_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif = comdat any

$_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f = comdat any

$_ZNK23btGeneric6DofConstraint28calculateSerializeBufferSizeEv = comdat any

$_ZNK23btGeneric6DofConstraint9serializeEPvP12btSerializer = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZN11btRigidBody19applyCentralImpulseERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZNK11btMatrix3x35cofacEiiii = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_Z6btSqrtf = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_Z6btFabsf = comdat any

$_Z16btNormalizeAnglef = comdat any

$_Z6btFmodff = comdat any

$_ZN11btMatrix3x311setIdentityEv = comdat any

$_ZN23btGeneric6DofConstraintdlEPv = comdat any

$_ZNK11btTransform9serializeER20btTransformFloatData = comdat any

$_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData = comdat any

$_ZNK9btVector39serializeER18btVector3FloatData = comdat any

@_ZTV23btGeneric6DofConstraint = hidden unnamed_addr constant { [14 x i8*] } { [14 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI23btGeneric6DofConstraint to i8*), i8* bitcast (%class.btTypedConstraint* (%class.btTypedConstraint*)* @_ZN17btTypedConstraintD2Ev to i8*), i8* bitcast (void (%class.btGeneric6DofConstraint*)* @_ZN23btGeneric6DofConstraintD0Ev to i8*), i8* bitcast (void (%class.btGeneric6DofConstraint*)* @_ZN23btGeneric6DofConstraint13buildJacobianEv to i8*), i8* bitcast (void (%class.btTypedConstraint*, %class.btAlignedObjectArray.1*, i32, i32, float)* @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif to i8*), i8* bitcast (void (%class.btGeneric6DofConstraint*, %"struct.btTypedConstraint::btConstraintInfo1"*)* @_ZN23btGeneric6DofConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E to i8*), i8* bitcast (void (%class.btGeneric6DofConstraint*, %"struct.btTypedConstraint::btConstraintInfo2"*)* @_ZN23btGeneric6DofConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E to i8*), i8* bitcast (void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)* @_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f to i8*), i8* bitcast (void (%class.btGeneric6DofConstraint*, i32, float, i32)* @_ZN23btGeneric6DofConstraint8setParamEifi to i8*), i8* bitcast (float (%class.btGeneric6DofConstraint*, i32, i32)* @_ZNK23btGeneric6DofConstraint8getParamEii to i8*), i8* bitcast (i32 (%class.btGeneric6DofConstraint*)* @_ZNK23btGeneric6DofConstraint28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btGeneric6DofConstraint*, i8*, %class.btSerializer*)* @_ZNK23btGeneric6DofConstraint9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btGeneric6DofConstraint*)* @_ZN23btGeneric6DofConstraint13calcAnchorPosEv to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS23btGeneric6DofConstraint = hidden constant [26 x i8] c"23btGeneric6DofConstraint\00", align 1
@_ZTI17btTypedConstraint = external constant i8*
@_ZTI23btGeneric6DofConstraint = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([26 x i8], [26 x i8]* @_ZTS23btGeneric6DofConstraint, i32 0, i32 0), i8* bitcast (i8** @_ZTI17btTypedConstraint to i8*) }, align 4
@_ZTV17btTypedConstraint = external unnamed_addr constant { [13 x i8*] }, align 4
@.str = private unnamed_addr constant [28 x i8] c"btGeneric6DofConstraintData\00", align 1

@_ZN23btGeneric6DofConstraintC1ER11btRigidBodyS1_RK11btTransformS4_b = hidden unnamed_addr alias %class.btGeneric6DofConstraint* (%class.btGeneric6DofConstraint*, %class.btRigidBody*, %class.btRigidBody*, %class.btTransform*, %class.btTransform*, i1), %class.btGeneric6DofConstraint* (%class.btGeneric6DofConstraint*, %class.btRigidBody*, %class.btRigidBody*, %class.btTransform*, %class.btTransform*, i1)* @_ZN23btGeneric6DofConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b
@_ZN23btGeneric6DofConstraintC1ER11btRigidBodyRK11btTransformb = hidden unnamed_addr alias %class.btGeneric6DofConstraint* (%class.btGeneric6DofConstraint*, %class.btRigidBody*, %class.btTransform*, i1), %class.btGeneric6DofConstraint* (%class.btGeneric6DofConstraint*, %class.btRigidBody*, %class.btTransform*, i1)* @_ZN23btGeneric6DofConstraintC2ER11btRigidBodyRK11btTransformb

define hidden %class.btGeneric6DofConstraint* @_ZN23btGeneric6DofConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b(%class.btGeneric6DofConstraint* returned %this, %class.btRigidBody* nonnull align 4 dereferenceable(616) %rbA, %class.btRigidBody* nonnull align 4 dereferenceable(616) %rbB, %class.btTransform* nonnull align 4 dereferenceable(64) %frameInA, %class.btTransform* nonnull align 4 dereferenceable(64) %frameInB, i1 zeroext %useLinearReferenceFrameA) unnamed_addr #0 {
entry:
  %retval = alloca %class.btGeneric6DofConstraint*, align 4
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  %rbA.addr = alloca %class.btRigidBody*, align 4
  %rbB.addr = alloca %class.btRigidBody*, align 4
  %frameInA.addr = alloca %class.btTransform*, align 4
  %frameInB.addr = alloca %class.btTransform*, align 4
  %useLinearReferenceFrameA.addr = alloca i8, align 1
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4, !tbaa !2
  store %class.btRigidBody* %rbA, %class.btRigidBody** %rbA.addr, align 4, !tbaa !2
  store %class.btRigidBody* %rbB, %class.btRigidBody** %rbB.addr, align 4, !tbaa !2
  store %class.btTransform* %frameInA, %class.btTransform** %frameInA.addr, align 4, !tbaa !2
  store %class.btTransform* %frameInB, %class.btTransform** %frameInB.addr, align 4, !tbaa !2
  %frombool = zext i1 %useLinearReferenceFrameA to i8
  store i8 %frombool, i8* %useLinearReferenceFrameA.addr, align 1, !tbaa !6
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  store %class.btGeneric6DofConstraint* %this1, %class.btGeneric6DofConstraint** %retval, align 4
  %0 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %1 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4, !tbaa !2
  %2 = load %class.btRigidBody*, %class.btRigidBody** %rbB.addr, align 4, !tbaa !2
  %call = call %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_(%class.btTypedConstraint* %0, i32 6, %class.btRigidBody* nonnull align 4 dereferenceable(616) %1, %class.btRigidBody* nonnull align 4 dereferenceable(616) %2)
  %3 = bitcast %class.btGeneric6DofConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [14 x i8*] }, { [14 x i8*] }* @_ZTV23btGeneric6DofConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %3, align 4, !tbaa !8
  %m_frameInA = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 1
  %4 = load %class.btTransform*, %class.btTransform** %frameInA.addr, align 4, !tbaa !2
  %call2 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_frameInA, %class.btTransform* nonnull align 4 dereferenceable(64) %4)
  %m_frameInB = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 2
  %5 = load %class.btTransform*, %class.btTransform** %frameInB.addr, align 4, !tbaa !2
  %call3 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_frameInB, %class.btTransform* nonnull align 4 dereferenceable(64) %5)
  %m_jacLinear = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 3
  %array.begin = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jacLinear, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btJacobianEntry* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call4 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btJacobianEntry* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_jacAng = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 4
  %array.begin5 = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jacAng, i32 0, i32 0
  %arrayctor.end6 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin5, i32 3
  br label %arrayctor.loop7

arrayctor.loop7:                                  ; preds = %arrayctor.loop7, %arrayctor.cont
  %arrayctor.cur8 = phi %class.btJacobianEntry* [ %array.begin5, %arrayctor.cont ], [ %arrayctor.next10, %arrayctor.loop7 ]
  %call9 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur8)
  %arrayctor.next10 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur8, i32 1
  %arrayctor.done11 = icmp eq %class.btJacobianEntry* %arrayctor.next10, %arrayctor.end6
  br i1 %arrayctor.done11, label %arrayctor.cont12, label %arrayctor.loop7

arrayctor.cont12:                                 ; preds = %arrayctor.loop7
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %call13 = call %class.btTranslationalLimitMotor* @_ZN25btTranslationalLimitMotorC2Ev(%class.btTranslationalLimitMotor* %m_linearLimits)
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 6
  %array.begin14 = getelementptr inbounds [3 x %class.btRotationalLimitMotor], [3 x %class.btRotationalLimitMotor]* %m_angularLimits, i32 0, i32 0
  %arrayctor.end15 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %array.begin14, i32 3
  br label %arrayctor.loop16

arrayctor.loop16:                                 ; preds = %arrayctor.loop16, %arrayctor.cont12
  %arrayctor.cur17 = phi %class.btRotationalLimitMotor* [ %array.begin14, %arrayctor.cont12 ], [ %arrayctor.next19, %arrayctor.loop16 ]
  %call18 = call %class.btRotationalLimitMotor* @_ZN22btRotationalLimitMotorC2Ev(%class.btRotationalLimitMotor* %arrayctor.cur17)
  %arrayctor.next19 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %arrayctor.cur17, i32 1
  %arrayctor.done20 = icmp eq %class.btRotationalLimitMotor* %arrayctor.next19, %arrayctor.end15
  br i1 %arrayctor.done20, label %arrayctor.cont21, label %arrayctor.loop16

arrayctor.cont21:                                 ; preds = %arrayctor.loop16
  %m_calculatedTransformA = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 8
  %call22 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_calculatedTransformA)
  %m_calculatedTransformB = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 9
  %call23 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_calculatedTransformB)
  %m_calculatedAxisAngleDiff = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 10
  %call24 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_calculatedAxisAngleDiff)
  %m_calculatedAxis = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 11
  %array.begin25 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis, i32 0, i32 0
  %arrayctor.end26 = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin25, i32 3
  br label %arrayctor.loop27

arrayctor.loop27:                                 ; preds = %arrayctor.loop27, %arrayctor.cont21
  %arrayctor.cur28 = phi %class.btVector3* [ %array.begin25, %arrayctor.cont21 ], [ %arrayctor.next30, %arrayctor.loop27 ]
  %call29 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur28)
  %arrayctor.next30 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur28, i32 1
  %arrayctor.done31 = icmp eq %class.btVector3* %arrayctor.next30, %arrayctor.end26
  br i1 %arrayctor.done31, label %arrayctor.cont32, label %arrayctor.loop27

arrayctor.cont32:                                 ; preds = %arrayctor.loop27
  %m_calculatedLinearDiff = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 12
  %call33 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_calculatedLinearDiff)
  %m_AnchorPos = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 17
  %call34 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_AnchorPos)
  %m_useLinearReferenceFrameA = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 18
  %6 = load i8, i8* %useLinearReferenceFrameA.addr, align 1, !tbaa !6, !range !10
  %tobool = trunc i8 %6 to i1
  %frombool35 = zext i1 %tobool to i8
  store i8 %frombool35, i8* %m_useLinearReferenceFrameA, align 4, !tbaa !11
  %m_useOffsetForConstraintFrame = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 19
  store i8 1, i8* %m_useOffsetForConstraintFrame, align 1, !tbaa !19
  %m_flags = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 21
  store i32 0, i32* %m_flags, align 4, !tbaa !20
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 22
  store i8 0, i8* %m_useSolveConstraintObsolete, align 4, !tbaa !21
  call void @_ZN23btGeneric6DofConstraint19calculateTransformsEv(%class.btGeneric6DofConstraint* %this1)
  %7 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %retval, align 4
  ret %class.btGeneric6DofConstraint* %7
}

declare %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_(%class.btTypedConstraint* returned, i32, %class.btRigidBody* nonnull align 4 dereferenceable(616), %class.btRigidBody* nonnull align 4 dereferenceable(616)) unnamed_addr #1

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !22
  ret %class.btTransform* %this1
}

define linkonce_odr hidden %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btJacobianEntry*, align 4
  store %class.btJacobianEntry* %this, %class.btJacobianEntry** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btJacobianEntry*, %class.btJacobianEntry** %this.addr, align 4
  %m_linearJointAxis = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_linearJointAxis)
  %m_aJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aJ)
  %m_bJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_bJ)
  %m_0MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_0MinvJt)
  %m_1MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_1MinvJt)
  ret %class.btJacobianEntry* %this1
}

define linkonce_odr hidden %class.btTranslationalLimitMotor* @_ZN25btTranslationalLimitMotorC2Ev(%class.btTranslationalLimitMotor* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btTranslationalLimitMotor*, align 4
  %this.addr = alloca %class.btTranslationalLimitMotor*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp24 = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %i = alloca i32, align 4
  store %class.btTranslationalLimitMotor* %this, %class.btTranslationalLimitMotor** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTranslationalLimitMotor*, %class.btTranslationalLimitMotor** %this.addr, align 4
  store %class.btTranslationalLimitMotor* %this1, %class.btTranslationalLimitMotor** %retval, align 4
  %m_lowerLimit = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_lowerLimit)
  %m_upperLimit = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_upperLimit)
  %m_accumulatedImpulse = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_accumulatedImpulse)
  %m_normalCFM = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 6
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_normalCFM)
  %m_stopERP = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 7
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_stopERP)
  %m_stopCFM = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 8
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_stopCFM)
  %m_targetVelocity = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 10
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_targetVelocity)
  %m_maxMotorForce = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 11
  %call8 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_maxMotorForce)
  %m_currentLimitError = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 12
  %call9 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_currentLimitError)
  %m_currentLinearDiff = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 13
  %call10 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_currentLinearDiff)
  %m_lowerLimit11 = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 0
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !24
  %1 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 0.000000e+00, float* %ref.tmp12, align 4, !tbaa !24
  %2 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 0.000000e+00, float* %ref.tmp13, align 4, !tbaa !24
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_lowerLimit11, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp13)
  %3 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  %4 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  %m_upperLimit14 = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 1
  %6 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  store float 0.000000e+00, float* %ref.tmp15, align 4, !tbaa !24
  %7 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  store float 0.000000e+00, float* %ref.tmp16, align 4, !tbaa !24
  %8 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  store float 0.000000e+00, float* %ref.tmp17, align 4, !tbaa !24
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_upperLimit14, float* nonnull align 4 dereferenceable(4) %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  %9 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  %m_accumulatedImpulse18 = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 2
  %12 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  store float 0.000000e+00, float* %ref.tmp19, align 4, !tbaa !24
  %13 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  store float 0.000000e+00, float* %ref.tmp20, align 4, !tbaa !24
  %14 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #8
  store float 0.000000e+00, float* %ref.tmp21, align 4, !tbaa !24
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_accumulatedImpulse18, float* nonnull align 4 dereferenceable(4) %ref.tmp19, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %15 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  %m_normalCFM22 = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 6
  %18 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #8
  store float 0.000000e+00, float* %ref.tmp23, align 4, !tbaa !24
  %19 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  store float 0.000000e+00, float* %ref.tmp24, align 4, !tbaa !24
  %20 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #8
  store float 0.000000e+00, float* %ref.tmp25, align 4, !tbaa !24
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_normalCFM22, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp24, float* nonnull align 4 dereferenceable(4) %ref.tmp25)
  %21 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  %22 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #8
  %23 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #8
  %m_stopERP26 = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 7
  %24 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #8
  store float 0x3FC99999A0000000, float* %ref.tmp27, align 4, !tbaa !24
  %25 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  store float 0x3FC99999A0000000, float* %ref.tmp28, align 4, !tbaa !24
  %26 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #8
  store float 0x3FC99999A0000000, float* %ref.tmp29, align 4, !tbaa !24
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_stopERP26, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp29)
  %27 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #8
  %28 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #8
  %29 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #8
  %m_stopCFM30 = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 8
  %30 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #8
  store float 0.000000e+00, float* %ref.tmp31, align 4, !tbaa !24
  %31 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #8
  store float 0.000000e+00, float* %ref.tmp32, align 4, !tbaa !24
  %32 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #8
  store float 0.000000e+00, float* %ref.tmp33, align 4, !tbaa !24
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_stopCFM30, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp32, float* nonnull align 4 dereferenceable(4) %ref.tmp33)
  %33 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #8
  %34 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #8
  %35 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  %m_limitSoftness = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 3
  store float 0x3FE6666660000000, float* %m_limitSoftness, align 4, !tbaa !25
  %m_damping = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 4
  store float 1.000000e+00, float* %m_damping, align 4, !tbaa !26
  %m_restitution = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 5
  store float 5.000000e-01, float* %m_restitution, align 4, !tbaa !27
  %36 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #8
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %37 = load i32, i32* %i, align 4, !tbaa !28
  %cmp = icmp slt i32 %37, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %38 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_enableMotor = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 9
  %39 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds [3 x i8], [3 x i8]* %m_enableMotor, i32 0, i32 %39
  store i8 0, i8* %arrayidx, align 1, !tbaa !6
  %m_targetVelocity34 = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 10
  %call35 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_targetVelocity34)
  %40 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 %40
  store float 0.000000e+00, float* %arrayidx36, align 4, !tbaa !24
  %m_maxMotorForce37 = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 11
  %call38 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_maxMotorForce37)
  %41 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx39 = getelementptr inbounds float, float* %call38, i32 %41
  store float 0.000000e+00, float* %arrayidx39, align 4, !tbaa !24
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %42 = load i32, i32* %i, align 4, !tbaa !28
  %inc = add nsw i32 %42, 1
  store i32 %inc, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %43 = load %class.btTranslationalLimitMotor*, %class.btTranslationalLimitMotor** %retval, align 4
  ret %class.btTranslationalLimitMotor* %43
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btRotationalLimitMotor* @_ZN22btRotationalLimitMotorC2Ev(%class.btRotationalLimitMotor* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btRotationalLimitMotor*, align 4
  store %class.btRotationalLimitMotor* %this, %class.btRotationalLimitMotor** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRotationalLimitMotor*, %class.btRotationalLimitMotor** %this.addr, align 4
  %m_accumulatedImpulse = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 15
  store float 0.000000e+00, float* %m_accumulatedImpulse, align 4, !tbaa !29
  %m_targetVelocity = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 2
  store float 0.000000e+00, float* %m_targetVelocity, align 4, !tbaa !31
  %m_maxMotorForce = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 3
  store float 0x3FB99999A0000000, float* %m_maxMotorForce, align 4, !tbaa !32
  %m_maxLimitForce = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 4
  store float 3.000000e+02, float* %m_maxLimitForce, align 4, !tbaa !33
  %m_loLimit = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 0
  store float 1.000000e+00, float* %m_loLimit, align 4, !tbaa !34
  %m_hiLimit = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 1
  store float -1.000000e+00, float* %m_hiLimit, align 4, !tbaa !35
  %m_normalCFM = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 7
  store float 0.000000e+00, float* %m_normalCFM, align 4, !tbaa !36
  %m_stopERP = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 8
  store float 0x3FC99999A0000000, float* %m_stopERP, align 4, !tbaa !37
  %m_stopCFM = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 9
  store float 0.000000e+00, float* %m_stopCFM, align 4, !tbaa !38
  %m_bounce = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 10
  store float 0.000000e+00, float* %m_bounce, align 4, !tbaa !39
  %m_damping = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 5
  store float 1.000000e+00, float* %m_damping, align 4, !tbaa !40
  %m_limitSoftness = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 6
  store float 5.000000e-01, float* %m_limitSoftness, align 4, !tbaa !41
  %m_currentLimit = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 14
  store i32 0, i32* %m_currentLimit, align 4, !tbaa !42
  %m_currentLimitError = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 12
  store float 0.000000e+00, float* %m_currentLimitError, align 4, !tbaa !43
  %m_enableMotor = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 11
  store i8 0, i8* %m_enableMotor, align 4, !tbaa !44
  ret %class.btRotationalLimitMotor* %this1
}

define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

define hidden void @_ZN23btGeneric6DofConstraint19calculateTransformsEv(%class.btGeneric6DofConstraint* %this) #0 {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %0 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %0, i32 0, i32 8
  %1 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4, !tbaa !45
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %1)
  %2 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %2, i32 0, i32 9
  %3 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4, !tbaa !47
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %3)
  call void @_ZN23btGeneric6DofConstraint19calculateTransformsERK11btTransformS2_(%class.btGeneric6DofConstraint* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %call, %class.btTransform* nonnull align 4 dereferenceable(64) %call2)
  ret void
}

define hidden %class.btGeneric6DofConstraint* @_ZN23btGeneric6DofConstraintC2ER11btRigidBodyRK11btTransformb(%class.btGeneric6DofConstraint* returned %this, %class.btRigidBody* nonnull align 4 dereferenceable(616) %rbB, %class.btTransform* nonnull align 4 dereferenceable(64) %frameInB, i1 zeroext %useLinearReferenceFrameB) unnamed_addr #0 {
entry:
  %retval = alloca %class.btGeneric6DofConstraint*, align 4
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  %rbB.addr = alloca %class.btRigidBody*, align 4
  %frameInB.addr = alloca %class.btTransform*, align 4
  %useLinearReferenceFrameB.addr = alloca i8, align 1
  %ref.tmp = alloca %class.btTransform, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4, !tbaa !2
  store %class.btRigidBody* %rbB, %class.btRigidBody** %rbB.addr, align 4, !tbaa !2
  store %class.btTransform* %frameInB, %class.btTransform** %frameInB.addr, align 4, !tbaa !2
  %frombool = zext i1 %useLinearReferenceFrameB to i8
  store i8 %frombool, i8* %useLinearReferenceFrameB.addr, align 1, !tbaa !6
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  store %class.btGeneric6DofConstraint* %this1, %class.btGeneric6DofConstraint** %retval, align 4
  %0 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %call = call nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZN17btTypedConstraint12getFixedBodyEv()
  %1 = load %class.btRigidBody*, %class.btRigidBody** %rbB.addr, align 4, !tbaa !2
  %call2 = call %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_(%class.btTypedConstraint* %0, i32 6, %class.btRigidBody* nonnull align 4 dereferenceable(616) %call, %class.btRigidBody* nonnull align 4 dereferenceable(616) %1)
  %2 = bitcast %class.btGeneric6DofConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [14 x i8*] }, { [14 x i8*] }* @_ZTV23btGeneric6DofConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %2, align 4, !tbaa !8
  %m_frameInA = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 1
  %call3 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_frameInA)
  %m_frameInB = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 2
  %3 = load %class.btTransform*, %class.btTransform** %frameInB.addr, align 4, !tbaa !2
  %call4 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_frameInB, %class.btTransform* nonnull align 4 dereferenceable(64) %3)
  %m_jacLinear = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 3
  %array.begin = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jacLinear, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btJacobianEntry* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call5 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btJacobianEntry* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_jacAng = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 4
  %array.begin6 = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jacAng, i32 0, i32 0
  %arrayctor.end7 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin6, i32 3
  br label %arrayctor.loop8

arrayctor.loop8:                                  ; preds = %arrayctor.loop8, %arrayctor.cont
  %arrayctor.cur9 = phi %class.btJacobianEntry* [ %array.begin6, %arrayctor.cont ], [ %arrayctor.next11, %arrayctor.loop8 ]
  %call10 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur9)
  %arrayctor.next11 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur9, i32 1
  %arrayctor.done12 = icmp eq %class.btJacobianEntry* %arrayctor.next11, %arrayctor.end7
  br i1 %arrayctor.done12, label %arrayctor.cont13, label %arrayctor.loop8

arrayctor.cont13:                                 ; preds = %arrayctor.loop8
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %call14 = call %class.btTranslationalLimitMotor* @_ZN25btTranslationalLimitMotorC2Ev(%class.btTranslationalLimitMotor* %m_linearLimits)
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 6
  %array.begin15 = getelementptr inbounds [3 x %class.btRotationalLimitMotor], [3 x %class.btRotationalLimitMotor]* %m_angularLimits, i32 0, i32 0
  %arrayctor.end16 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %array.begin15, i32 3
  br label %arrayctor.loop17

arrayctor.loop17:                                 ; preds = %arrayctor.loop17, %arrayctor.cont13
  %arrayctor.cur18 = phi %class.btRotationalLimitMotor* [ %array.begin15, %arrayctor.cont13 ], [ %arrayctor.next20, %arrayctor.loop17 ]
  %call19 = call %class.btRotationalLimitMotor* @_ZN22btRotationalLimitMotorC2Ev(%class.btRotationalLimitMotor* %arrayctor.cur18)
  %arrayctor.next20 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %arrayctor.cur18, i32 1
  %arrayctor.done21 = icmp eq %class.btRotationalLimitMotor* %arrayctor.next20, %arrayctor.end16
  br i1 %arrayctor.done21, label %arrayctor.cont22, label %arrayctor.loop17

arrayctor.cont22:                                 ; preds = %arrayctor.loop17
  %m_calculatedTransformA = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 8
  %call23 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_calculatedTransformA)
  %m_calculatedTransformB = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 9
  %call24 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_calculatedTransformB)
  %m_calculatedAxisAngleDiff = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 10
  %call25 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_calculatedAxisAngleDiff)
  %m_calculatedAxis = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 11
  %array.begin26 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis, i32 0, i32 0
  %arrayctor.end27 = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin26, i32 3
  br label %arrayctor.loop28

arrayctor.loop28:                                 ; preds = %arrayctor.loop28, %arrayctor.cont22
  %arrayctor.cur29 = phi %class.btVector3* [ %array.begin26, %arrayctor.cont22 ], [ %arrayctor.next31, %arrayctor.loop28 ]
  %call30 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur29)
  %arrayctor.next31 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur29, i32 1
  %arrayctor.done32 = icmp eq %class.btVector3* %arrayctor.next31, %arrayctor.end27
  br i1 %arrayctor.done32, label %arrayctor.cont33, label %arrayctor.loop28

arrayctor.cont33:                                 ; preds = %arrayctor.loop28
  %m_calculatedLinearDiff = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 12
  %call34 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_calculatedLinearDiff)
  %m_AnchorPos = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 17
  %call35 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_AnchorPos)
  %m_useLinearReferenceFrameA = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 18
  %4 = load i8, i8* %useLinearReferenceFrameB.addr, align 1, !tbaa !6, !range !10
  %tobool = trunc i8 %4 to i1
  %frombool36 = zext i1 %tobool to i8
  store i8 %frombool36, i8* %m_useLinearReferenceFrameA, align 4, !tbaa !11
  %m_useOffsetForConstraintFrame = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 19
  store i8 1, i8* %m_useOffsetForConstraintFrame, align 1, !tbaa !19
  %m_flags = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 21
  store i32 0, i32* %m_flags, align 4, !tbaa !20
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 22
  store i8 0, i8* %m_useSolveConstraintObsolete, align 4, !tbaa !21
  %5 = bitcast %class.btTransform* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %5) #8
  %6 = load %class.btRigidBody*, %class.btRigidBody** %rbB.addr, align 4, !tbaa !2
  %call37 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %6)
  %m_frameInB38 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 2
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp, %class.btTransform* %call37, %class.btTransform* nonnull align 4 dereferenceable(64) %m_frameInB38)
  %m_frameInA39 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 1
  %call40 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_frameInA39, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp)
  %7 = bitcast %class.btTransform* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %7) #8
  call void @_ZN23btGeneric6DofConstraint19calculateTransformsEv(%class.btGeneric6DofConstraint* %this1)
  %8 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %retval, align 4
  ret %class.btGeneric6DofConstraint* %8
}

declare nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZN17btTypedConstraint12getFixedBodyEv() #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #5

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformmlERKS_(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %1 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %1) #8
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %2 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %2, i32 0, i32 0
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %3 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #8
  %4 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %4, i32 0, i32 1
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp3, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  %5 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #8
  %6 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %6) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !22
  ret %class.btTransform* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #5

define hidden float @_Z15btGetMatrixElemRK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %mat, i32 %index) #0 {
entry:
  %mat.addr = alloca %class.btMatrix3x3*, align 4
  %index.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store %class.btMatrix3x3* %mat, %class.btMatrix3x3** %mat.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !28
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %index.addr, align 4, !tbaa !28
  %rem = srem i32 %1, 3
  store i32 %rem, i32* %i, align 4, !tbaa !28
  %2 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load i32, i32* %index.addr, align 4, !tbaa !28
  %div = sdiv i32 %3, 3
  store i32 %div, i32* %j, align 4, !tbaa !28
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !28
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 %5)
  %call1 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call)
  %6 = load i32, i32* %j, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds float, float* %call1, i32 %6
  %7 = load float, float* %arrayidx, align 4, !tbaa !24
  %8 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  ret float %7
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !28
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

define hidden zeroext i1 @_Z16matrixToEulerXYZRK11btMatrix3x3R9btVector3(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %mat, %class.btVector3* nonnull align 4 dereferenceable(16) %xyz) #0 {
entry:
  %retval = alloca i1, align 1
  %mat.addr = alloca %class.btMatrix3x3*, align 4
  %xyz.addr = alloca %class.btVector3*, align 4
  %fi = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btMatrix3x3* %mat, %class.btMatrix3x3** %mat.addr, align 4, !tbaa !2
  store %class.btVector3* %xyz, %class.btVector3** %xyz.addr, align 4, !tbaa !2
  %0 = bitcast float* %fi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4, !tbaa !2
  %call = call float @_Z15btGetMatrixElemRK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %1, i32 2)
  store float %call, float* %fi, align 4, !tbaa !24
  %2 = load float, float* %fi, align 4, !tbaa !24
  %cmp = fcmp olt float %2, 1.000000e+00
  br i1 %cmp, label %if.then, label %if.else27

if.then:                                          ; preds = %entry
  %3 = load float, float* %fi, align 4, !tbaa !24
  %cmp1 = fcmp ogt float %3, -1.000000e+00
  br i1 %cmp1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4, !tbaa !2
  %call3 = call float @_Z15btGetMatrixElemRK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %4, i32 5)
  %fneg = fneg float %call3
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4, !tbaa !2
  %call4 = call float @_Z15btGetMatrixElemRK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %5, i32 8)
  %call5 = call float @_Z7btAtan2ff(float %fneg, float %call4)
  %6 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4, !tbaa !2
  %call6 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %6)
  %arrayidx = getelementptr inbounds float, float* %call6, i32 0
  store float %call5, float* %arrayidx, align 4, !tbaa !24
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4, !tbaa !2
  %call7 = call float @_Z15btGetMatrixElemRK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %7, i32 2)
  %call8 = call float @_Z6btAsinf(float %call7)
  %8 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4, !tbaa !2
  %call9 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %8)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 1
  store float %call8, float* %arrayidx10, align 4, !tbaa !24
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4, !tbaa !2
  %call11 = call float @_Z15btGetMatrixElemRK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %9, i32 1)
  %fneg12 = fneg float %call11
  %10 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4, !tbaa !2
  %call13 = call float @_Z15btGetMatrixElemRK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %10, i32 0)
  %call14 = call float @_Z7btAtan2ff(float %fneg12, float %call13)
  %11 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4, !tbaa !2
  %call15 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %11)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 2
  store float %call14, float* %arrayidx16, align 4, !tbaa !24
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %if.then
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4, !tbaa !2
  %call17 = call float @_Z15btGetMatrixElemRK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %12, i32 3)
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4, !tbaa !2
  %call18 = call float @_Z15btGetMatrixElemRK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %13, i32 4)
  %call19 = call float @_Z7btAtan2ff(float %call17, float %call18)
  %fneg20 = fneg float %call19
  %14 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4, !tbaa !2
  %call21 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %14)
  %arrayidx22 = getelementptr inbounds float, float* %call21, i32 0
  store float %fneg20, float* %arrayidx22, align 4, !tbaa !24
  %15 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4, !tbaa !2
  %call23 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %15)
  %arrayidx24 = getelementptr inbounds float, float* %call23, i32 1
  store float 0xBFF921FB60000000, float* %arrayidx24, align 4, !tbaa !24
  %16 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4, !tbaa !2
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %16)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 2
  store float 0.000000e+00, float* %arrayidx26, align 4, !tbaa !24
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else27:                                        ; preds = %entry
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4, !tbaa !2
  %call28 = call float @_Z15btGetMatrixElemRK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %17, i32 3)
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4, !tbaa !2
  %call29 = call float @_Z15btGetMatrixElemRK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %18, i32 4)
  %call30 = call float @_Z7btAtan2ff(float %call28, float %call29)
  %19 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4, !tbaa !2
  %call31 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %19)
  %arrayidx32 = getelementptr inbounds float, float* %call31, i32 0
  store float %call30, float* %arrayidx32, align 4, !tbaa !24
  %20 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4, !tbaa !2
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %20)
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 1
  store float 0x3FF921FB60000000, float* %arrayidx34, align 4, !tbaa !24
  %21 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4, !tbaa !2
  %call35 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %21)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 2
  store float 0.000000e+00, float* %arrayidx36, align 4, !tbaa !24
  br label %if.end

if.end:                                           ; preds = %if.else27
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.else, %if.then2
  %22 = bitcast float* %fi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #8
  %23 = load i1, i1* %retval, align 1
  ret i1 %23
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z7btAtan2ff(float %x, float %y) #4 comdat {
entry:
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !24
  store float %y, float* %y.addr, align 4, !tbaa !24
  %0 = load float, float* %x.addr, align 4, !tbaa !24
  %1 = load float, float* %y.addr, align 4, !tbaa !24
  %call = call float @atan2f(float %0, float %1) #9
  ret float %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btAsinf(float %x) #4 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !24
  %0 = load float, float* %x.addr, align 4, !tbaa !24
  %cmp = fcmp olt float %0, -1.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store float -1.000000e+00, float* %x.addr, align 4, !tbaa !24
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %1 = load float, float* %x.addr, align 4, !tbaa !24
  %cmp1 = fcmp ogt float %1, 1.000000e+00
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store float 1.000000e+00, float* %x.addr, align 4, !tbaa !24
  br label %if.end3

if.end3:                                          ; preds = %if.then2, %if.end
  %2 = load float, float* %x.addr, align 4, !tbaa !24
  %call = call float @asinf(float %2) #9
  ret float %call
}

; Function Attrs: nounwind
define hidden i32 @_ZN22btRotationalLimitMotor14testLimitValueEf(%class.btRotationalLimitMotor* %this, float %test_value) #3 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btRotationalLimitMotor*, align 4
  %test_value.addr = alloca float, align 4
  store %class.btRotationalLimitMotor* %this, %class.btRotationalLimitMotor** %this.addr, align 4, !tbaa !2
  store float %test_value, float* %test_value.addr, align 4, !tbaa !24
  %this1 = load %class.btRotationalLimitMotor*, %class.btRotationalLimitMotor** %this.addr, align 4
  %m_loLimit = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 0
  %0 = load float, float* %m_loLimit, align 4, !tbaa !34
  %m_hiLimit = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 1
  %1 = load float, float* %m_hiLimit, align 4, !tbaa !35
  %cmp = fcmp ogt float %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_currentLimit = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 14
  store i32 0, i32* %m_currentLimit, align 4, !tbaa !42
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load float, float* %test_value.addr, align 4, !tbaa !24
  %m_loLimit2 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 0
  %3 = load float, float* %m_loLimit2, align 4, !tbaa !34
  %cmp3 = fcmp olt float %2, %3
  br i1 %cmp3, label %if.then4, label %if.else18

if.then4:                                         ; preds = %if.end
  %m_currentLimit5 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 14
  store i32 1, i32* %m_currentLimit5, align 4, !tbaa !42
  %4 = load float, float* %test_value.addr, align 4, !tbaa !24
  %m_loLimit6 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 0
  %5 = load float, float* %m_loLimit6, align 4, !tbaa !34
  %sub = fsub float %4, %5
  %m_currentLimitError = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 12
  store float %sub, float* %m_currentLimitError, align 4, !tbaa !43
  %m_currentLimitError7 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 12
  %6 = load float, float* %m_currentLimitError7, align 4, !tbaa !43
  %cmp8 = fcmp ogt float %6, 0x400921FB60000000
  br i1 %cmp8, label %if.then9, label %if.else

if.then9:                                         ; preds = %if.then4
  %m_currentLimitError10 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 12
  %7 = load float, float* %m_currentLimitError10, align 4, !tbaa !43
  %sub11 = fsub float %7, 0x401921FB60000000
  store float %sub11, float* %m_currentLimitError10, align 4, !tbaa !43
  br label %if.end17

if.else:                                          ; preds = %if.then4
  %m_currentLimitError12 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 12
  %8 = load float, float* %m_currentLimitError12, align 4, !tbaa !43
  %cmp13 = fcmp olt float %8, 0xC00921FB60000000
  br i1 %cmp13, label %if.then14, label %if.end16

if.then14:                                        ; preds = %if.else
  %m_currentLimitError15 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 12
  %9 = load float, float* %m_currentLimitError15, align 4, !tbaa !43
  %add = fadd float %9, 0x401921FB60000000
  store float %add, float* %m_currentLimitError15, align 4, !tbaa !43
  br label %if.end16

if.end16:                                         ; preds = %if.then14, %if.else
  br label %if.end17

if.end17:                                         ; preds = %if.end16, %if.then9
  store i32 1, i32* %retval, align 4
  br label %return

if.else18:                                        ; preds = %if.end
  %10 = load float, float* %test_value.addr, align 4, !tbaa !24
  %m_hiLimit19 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 1
  %11 = load float, float* %m_hiLimit19, align 4, !tbaa !35
  %cmp20 = fcmp ogt float %10, %11
  br i1 %cmp20, label %if.then21, label %if.end39

if.then21:                                        ; preds = %if.else18
  %m_currentLimit22 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 14
  store i32 2, i32* %m_currentLimit22, align 4, !tbaa !42
  %12 = load float, float* %test_value.addr, align 4, !tbaa !24
  %m_hiLimit23 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 1
  %13 = load float, float* %m_hiLimit23, align 4, !tbaa !35
  %sub24 = fsub float %12, %13
  %m_currentLimitError25 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 12
  store float %sub24, float* %m_currentLimitError25, align 4, !tbaa !43
  %m_currentLimitError26 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 12
  %14 = load float, float* %m_currentLimitError26, align 4, !tbaa !43
  %cmp27 = fcmp ogt float %14, 0x400921FB60000000
  br i1 %cmp27, label %if.then28, label %if.else31

if.then28:                                        ; preds = %if.then21
  %m_currentLimitError29 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 12
  %15 = load float, float* %m_currentLimitError29, align 4, !tbaa !43
  %sub30 = fsub float %15, 0x401921FB60000000
  store float %sub30, float* %m_currentLimitError29, align 4, !tbaa !43
  br label %if.end38

if.else31:                                        ; preds = %if.then21
  %m_currentLimitError32 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 12
  %16 = load float, float* %m_currentLimitError32, align 4, !tbaa !43
  %cmp33 = fcmp olt float %16, 0xC00921FB60000000
  br i1 %cmp33, label %if.then34, label %if.end37

if.then34:                                        ; preds = %if.else31
  %m_currentLimitError35 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 12
  %17 = load float, float* %m_currentLimitError35, align 4, !tbaa !43
  %add36 = fadd float %17, 0x401921FB60000000
  store float %add36, float* %m_currentLimitError35, align 4, !tbaa !43
  br label %if.end37

if.end37:                                         ; preds = %if.then34, %if.else31
  br label %if.end38

if.end38:                                         ; preds = %if.end37, %if.then28
  store i32 2, i32* %retval, align 4
  br label %return

if.end39:                                         ; preds = %if.else18
  br label %if.end40

if.end40:                                         ; preds = %if.end39
  %m_currentLimit41 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 14
  store i32 0, i32* %m_currentLimit41, align 4, !tbaa !42
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end40, %if.end38, %if.end17, %if.then
  %18 = load i32, i32* %retval, align 4
  ret i32 %18
}

define hidden float @_ZN22btRotationalLimitMotor18solveAngularLimitsEfR9btVector3fP11btRigidBodyS3_(%class.btRotationalLimitMotor* %this, float %timeStep, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, float %jacDiagABInv, %class.btRigidBody* %body0, %class.btRigidBody* %body1) #0 {
entry:
  %retval = alloca float, align 4
  %this.addr = alloca %class.btRotationalLimitMotor*, align 4
  %timeStep.addr = alloca float, align 4
  %axis.addr = alloca %class.btVector3*, align 4
  %jacDiagABInv.addr = alloca float, align 4
  %body0.addr = alloca %class.btRigidBody*, align 4
  %body1.addr = alloca %class.btRigidBody*, align 4
  %target_velocity = alloca float, align 4
  %maxMotorForce = alloca float, align 4
  %angVelA = alloca %class.btVector3, align 4
  %angVelB = alloca %class.btVector3, align 4
  %vel_diff = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %rel_vel = alloca float, align 4
  %motor_relvel = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %unclippedMotorImpulse = alloca float, align 4
  %clippedMotorImpulse = alloca float, align 4
  %lo = alloca float, align 4
  %hi = alloca float, align 4
  %oldaccumImpulse = alloca float, align 4
  %sum = alloca float, align 4
  %motorImp = alloca %class.btVector3, align 4
  %ref.tmp43 = alloca %class.btVector3, align 4
  store %class.btRotationalLimitMotor* %this, %class.btRotationalLimitMotor** %this.addr, align 4, !tbaa !2
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !24
  store %class.btVector3* %axis, %class.btVector3** %axis.addr, align 4, !tbaa !2
  store float %jacDiagABInv, float* %jacDiagABInv.addr, align 4, !tbaa !24
  store %class.btRigidBody* %body0, %class.btRigidBody** %body0.addr, align 4, !tbaa !2
  store %class.btRigidBody* %body1, %class.btRigidBody** %body1.addr, align 4, !tbaa !2
  %this1 = load %class.btRotationalLimitMotor*, %class.btRotationalLimitMotor** %this.addr, align 4
  %call = call zeroext i1 @_ZN22btRotationalLimitMotor16needApplyTorquesEv(%class.btRotationalLimitMotor* %this1)
  %conv = zext i1 %call to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store float 0.000000e+00, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %0 = bitcast float* %target_velocity to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %m_targetVelocity = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 2
  %1 = load float, float* %m_targetVelocity, align 4, !tbaa !31
  store float %1, float* %target_velocity, align 4, !tbaa !24
  %2 = bitcast float* %maxMotorForce to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %m_maxMotorForce = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 3
  %3 = load float, float* %m_maxMotorForce, align 4, !tbaa !32
  store float %3, float* %maxMotorForce, align 4, !tbaa !24
  %m_currentLimit = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 14
  %4 = load i32, i32* %m_currentLimit, align 4, !tbaa !42
  %cmp2 = icmp ne i32 %4, 0
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  %m_stopERP = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 8
  %5 = load float, float* %m_stopERP, align 4, !tbaa !37
  %fneg = fneg float %5
  %m_currentLimitError = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 12
  %6 = load float, float* %m_currentLimitError, align 4, !tbaa !43
  %mul = fmul float %fneg, %6
  %7 = load float, float* %timeStep.addr, align 4, !tbaa !24
  %div = fdiv float %mul, %7
  store float %div, float* %target_velocity, align 4, !tbaa !24
  %m_maxLimitForce = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 4
  %8 = load float, float* %m_maxLimitForce, align 4, !tbaa !33
  store float %8, float* %maxMotorForce, align 4, !tbaa !24
  br label %if.end4

if.end4:                                          ; preds = %if.then3, %if.end
  %9 = load float, float* %timeStep.addr, align 4, !tbaa !24
  %10 = load float, float* %maxMotorForce, align 4, !tbaa !24
  %mul5 = fmul float %10, %9
  store float %mul5, float* %maxMotorForce, align 4, !tbaa !24
  %11 = bitcast %class.btVector3* %angVelA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #8
  %12 = load %class.btRigidBody*, %class.btRigidBody** %body0.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %12)
  %13 = bitcast %class.btVector3* %angVelA to i8*
  %14 = bitcast %class.btVector3* %call6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 16, i1 false), !tbaa.struct !22
  %15 = bitcast %class.btVector3* %angVelB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %15) #8
  %16 = load %class.btRigidBody*, %class.btRigidBody** %body1.addr, align 4, !tbaa !2
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %16)
  %17 = bitcast %class.btVector3* %angVelB to i8*
  %18 = bitcast %class.btVector3* %call7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %17, i8* align 4 %18, i32 16, i1 false), !tbaa.struct !22
  %19 = bitcast %class.btVector3* %vel_diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %19) #8
  %call8 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vel_diff)
  %20 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %20) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelA, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelB)
  %21 = bitcast %class.btVector3* %vel_diff to i8*
  %22 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false), !tbaa.struct !22
  %23 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %23) #8
  %24 = bitcast float* %rel_vel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #8
  %25 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %call9 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %25, %class.btVector3* nonnull align 4 dereferenceable(16) %vel_diff)
  store float %call9, float* %rel_vel, align 4, !tbaa !24
  %26 = bitcast float* %motor_relvel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #8
  %m_limitSoftness = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 6
  %27 = load float, float* %m_limitSoftness, align 4, !tbaa !41
  %28 = load float, float* %target_velocity, align 4, !tbaa !24
  %m_damping = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 5
  %29 = load float, float* %m_damping, align 4, !tbaa !40
  %30 = load float, float* %rel_vel, align 4, !tbaa !24
  %mul10 = fmul float %29, %30
  %sub = fsub float %28, %mul10
  %mul11 = fmul float %27, %sub
  store float %mul11, float* %motor_relvel, align 4, !tbaa !24
  %31 = load float, float* %motor_relvel, align 4, !tbaa !24
  %cmp12 = fcmp olt float %31, 0x3E80000000000000
  br i1 %cmp12, label %land.lhs.true, label %if.end15

land.lhs.true:                                    ; preds = %if.end4
  %32 = load float, float* %motor_relvel, align 4, !tbaa !24
  %cmp13 = fcmp ogt float %32, 0xBE80000000000000
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %land.lhs.true
  store float 0.000000e+00, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end15:                                         ; preds = %land.lhs.true, %if.end4
  %33 = bitcast float* %unclippedMotorImpulse to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #8
  %m_bounce = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 10
  %34 = load float, float* %m_bounce, align 4, !tbaa !39
  %add = fadd float 1.000000e+00, %34
  %35 = load float, float* %motor_relvel, align 4, !tbaa !24
  %mul16 = fmul float %add, %35
  %36 = load float, float* %jacDiagABInv.addr, align 4, !tbaa !24
  %mul17 = fmul float %mul16, %36
  store float %mul17, float* %unclippedMotorImpulse, align 4, !tbaa !24
  %37 = bitcast float* %clippedMotorImpulse to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #8
  %38 = load float, float* %unclippedMotorImpulse, align 4, !tbaa !24
  %cmp18 = fcmp ogt float %38, 0.000000e+00
  br i1 %cmp18, label %if.then19, label %if.else

if.then19:                                        ; preds = %if.end15
  %39 = load float, float* %unclippedMotorImpulse, align 4, !tbaa !24
  %40 = load float, float* %maxMotorForce, align 4, !tbaa !24
  %cmp20 = fcmp ogt float %39, %40
  br i1 %cmp20, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then19
  %41 = load float, float* %maxMotorForce, align 4, !tbaa !24
  br label %cond.end

cond.false:                                       ; preds = %if.then19
  %42 = load float, float* %unclippedMotorImpulse, align 4, !tbaa !24
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %41, %cond.true ], [ %42, %cond.false ]
  store float %cond, float* %clippedMotorImpulse, align 4, !tbaa !24
  br label %if.end28

if.else:                                          ; preds = %if.end15
  %43 = load float, float* %unclippedMotorImpulse, align 4, !tbaa !24
  %44 = load float, float* %maxMotorForce, align 4, !tbaa !24
  %fneg21 = fneg float %44
  %cmp22 = fcmp olt float %43, %fneg21
  br i1 %cmp22, label %cond.true23, label %cond.false25

cond.true23:                                      ; preds = %if.else
  %45 = load float, float* %maxMotorForce, align 4, !tbaa !24
  %fneg24 = fneg float %45
  br label %cond.end26

cond.false25:                                     ; preds = %if.else
  %46 = load float, float* %unclippedMotorImpulse, align 4, !tbaa !24
  br label %cond.end26

cond.end26:                                       ; preds = %cond.false25, %cond.true23
  %cond27 = phi float [ %fneg24, %cond.true23 ], [ %46, %cond.false25 ]
  store float %cond27, float* %clippedMotorImpulse, align 4, !tbaa !24
  br label %if.end28

if.end28:                                         ; preds = %cond.end26, %cond.end
  %47 = bitcast float* %lo to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #8
  store float 0xC3ABC16D60000000, float* %lo, align 4, !tbaa !24
  %48 = bitcast float* %hi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #8
  store float 0x43ABC16D60000000, float* %hi, align 4, !tbaa !24
  %49 = bitcast float* %oldaccumImpulse to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #8
  %m_accumulatedImpulse = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 15
  %50 = load float, float* %m_accumulatedImpulse, align 4, !tbaa !29
  store float %50, float* %oldaccumImpulse, align 4, !tbaa !24
  %51 = bitcast float* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #8
  %52 = load float, float* %oldaccumImpulse, align 4, !tbaa !24
  %53 = load float, float* %clippedMotorImpulse, align 4, !tbaa !24
  %add29 = fadd float %52, %53
  store float %add29, float* %sum, align 4, !tbaa !24
  %54 = load float, float* %sum, align 4, !tbaa !24
  %55 = load float, float* %hi, align 4, !tbaa !24
  %cmp30 = fcmp ogt float %54, %55
  br i1 %cmp30, label %cond.true31, label %cond.false32

cond.true31:                                      ; preds = %if.end28
  br label %cond.end38

cond.false32:                                     ; preds = %if.end28
  %56 = load float, float* %sum, align 4, !tbaa !24
  %57 = load float, float* %lo, align 4, !tbaa !24
  %cmp33 = fcmp olt float %56, %57
  br i1 %cmp33, label %cond.true34, label %cond.false35

cond.true34:                                      ; preds = %cond.false32
  br label %cond.end36

cond.false35:                                     ; preds = %cond.false32
  %58 = load float, float* %sum, align 4, !tbaa !24
  br label %cond.end36

cond.end36:                                       ; preds = %cond.false35, %cond.true34
  %cond37 = phi float [ 0.000000e+00, %cond.true34 ], [ %58, %cond.false35 ]
  br label %cond.end38

cond.end38:                                       ; preds = %cond.end36, %cond.true31
  %cond39 = phi float [ 0.000000e+00, %cond.true31 ], [ %cond37, %cond.end36 ]
  %m_accumulatedImpulse40 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 15
  store float %cond39, float* %m_accumulatedImpulse40, align 4, !tbaa !29
  %m_accumulatedImpulse41 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 15
  %59 = load float, float* %m_accumulatedImpulse41, align 4, !tbaa !29
  %60 = load float, float* %oldaccumImpulse, align 4, !tbaa !24
  %sub42 = fsub float %59, %60
  store float %sub42, float* %clippedMotorImpulse, align 4, !tbaa !24
  %61 = bitcast %class.btVector3* %motorImp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %61) #8
  %62 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %motorImp, float* nonnull align 4 dereferenceable(4) %clippedMotorImpulse, %class.btVector3* nonnull align 4 dereferenceable(16) %62)
  %63 = load %class.btRigidBody*, %class.btRigidBody** %body0.addr, align 4, !tbaa !2
  call void @_ZN11btRigidBody18applyTorqueImpulseERK9btVector3(%class.btRigidBody* %63, %class.btVector3* nonnull align 4 dereferenceable(16) %motorImp)
  %64 = load %class.btRigidBody*, %class.btRigidBody** %body1.addr, align 4, !tbaa !2
  %65 = bitcast %class.btVector3* %ref.tmp43 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %65) #8
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp43, %class.btVector3* nonnull align 4 dereferenceable(16) %motorImp)
  call void @_ZN11btRigidBody18applyTorqueImpulseERK9btVector3(%class.btRigidBody* %64, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp43)
  %66 = bitcast %class.btVector3* %ref.tmp43 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %66) #8
  %67 = load float, float* %clippedMotorImpulse, align 4, !tbaa !24
  store float %67, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %68 = bitcast %class.btVector3* %motorImp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %68) #8
  %69 = bitcast float* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #8
  %70 = bitcast float* %oldaccumImpulse to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #8
  %71 = bitcast float* %hi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #8
  %72 = bitcast float* %lo to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #8
  %73 = bitcast float* %clippedMotorImpulse to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #8
  %74 = bitcast float* %unclippedMotorImpulse to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #8
  br label %cleanup

cleanup:                                          ; preds = %cond.end38, %if.then14
  %75 = bitcast float* %motor_relvel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #8
  %76 = bitcast float* %rel_vel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #8
  %77 = bitcast %class.btVector3* %vel_diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %77) #8
  %78 = bitcast %class.btVector3* %angVelB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %78) #8
  %79 = bitcast %class.btVector3* %angVelA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %79) #8
  %80 = bitcast float* %maxMotorForce to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #8
  %81 = bitcast float* %target_velocity to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #8
  br label %return

return:                                           ; preds = %cleanup, %if.then
  %82 = load float, float* %retval, align 4
  ret float %82
}

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZN22btRotationalLimitMotor16needApplyTorquesEv(%class.btRotationalLimitMotor* %this) #3 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btRotationalLimitMotor*, align 4
  store %class.btRotationalLimitMotor* %this, %class.btRotationalLimitMotor** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRotationalLimitMotor*, %class.btRotationalLimitMotor** %this.addr, align 4
  %m_currentLimit = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 14
  %0 = load i32, i32* %m_currentLimit, align 4, !tbaa !42
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %m_enableMotor = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %this1, i32 0, i32 11
  %1 = load i8, i8* %m_enableMotor, align 4, !tbaa !44, !range !10
  %tobool = trunc i8 %1 to i1
  %conv = zext i1 %tobool to i32
  %cmp2 = icmp eq i32 %conv, 0
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i1, i1* %retval, align 1
  ret i1 %2
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  ret %class.btVector3* %m_angularVelocity
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #5

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !24
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !24
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !24
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !24
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !24
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !24
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !24
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !24
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !24
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !24
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !24
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !24
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !24
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !24
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !24
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

define linkonce_odr hidden void @_ZN11btRigidBody18applyTorqueImpulseERK9btVector3(%class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %torque) #0 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %torque.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %torque, %class.btVector3** %torque.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #8
  %m_invInertiaTensorWorld = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 1
  %2 = load %class.btVector3*, %class.btVector3** %torque.addr, align 4, !tbaa !2
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_invInertiaTensorWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  %m_angularFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 26
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularFactor)
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_angularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %3 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %3) #8
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !24
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp, align 4, !tbaa !24
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !24
  %fneg4 = fneg float %5
  store float %fneg4, float* %ref.tmp1, align 4, !tbaa !24
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %8 = load float, float* %arrayidx7, align 4, !tbaa !24
  %fneg8 = fneg float %8
  store float %fneg8, float* %ref.tmp5, align 4, !tbaa !24
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret void
}

define hidden i32 @_ZN25btTranslationalLimitMotor14testLimitValueEif(%class.btTranslationalLimitMotor* %this, i32 %limitIndex, float %test_value) #0 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btTranslationalLimitMotor*, align 4
  %limitIndex.addr = alloca i32, align 4
  %test_value.addr = alloca float, align 4
  %loLimit = alloca float, align 4
  %hiLimit = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btTranslationalLimitMotor* %this, %class.btTranslationalLimitMotor** %this.addr, align 4, !tbaa !2
  store i32 %limitIndex, i32* %limitIndex.addr, align 4, !tbaa !28
  store float %test_value, float* %test_value.addr, align 4, !tbaa !24
  %this1 = load %class.btTranslationalLimitMotor*, %class.btTranslationalLimitMotor** %this.addr, align 4
  %0 = bitcast float* %loLimit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %m_lowerLimit = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 0
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_lowerLimit)
  %1 = load i32, i32* %limitIndex.addr, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds float, float* %call, i32 %1
  %2 = load float, float* %arrayidx, align 4, !tbaa !24
  store float %2, float* %loLimit, align 4, !tbaa !24
  %3 = bitcast float* %hiLimit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %m_upperLimit = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 1
  %call2 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_upperLimit)
  %4 = load i32, i32* %limitIndex.addr, align 4, !tbaa !28
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 %4
  %5 = load float, float* %arrayidx3, align 4, !tbaa !24
  store float %5, float* %hiLimit, align 4, !tbaa !24
  %6 = load float, float* %loLimit, align 4, !tbaa !24
  %7 = load float, float* %hiLimit, align 4, !tbaa !24
  %cmp = fcmp ogt float %6, %7
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_currentLimit = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 14
  %8 = load i32, i32* %limitIndex.addr, align 4, !tbaa !28
  %arrayidx4 = getelementptr inbounds [3 x i32], [3 x i32]* %m_currentLimit, i32 0, i32 %8
  store i32 0, i32* %arrayidx4, align 4, !tbaa !28
  %m_currentLimitError = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 12
  %call5 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_currentLimitError)
  %9 = load i32, i32* %limitIndex.addr, align 4, !tbaa !28
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %9
  store float 0.000000e+00, float* %arrayidx6, align 4, !tbaa !24
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %10 = load float, float* %test_value.addr, align 4, !tbaa !24
  %11 = load float, float* %loLimit, align 4, !tbaa !24
  %cmp7 = fcmp olt float %10, %11
  br i1 %cmp7, label %if.then8, label %if.else

if.then8:                                         ; preds = %if.end
  %m_currentLimit9 = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 14
  %12 = load i32, i32* %limitIndex.addr, align 4, !tbaa !28
  %arrayidx10 = getelementptr inbounds [3 x i32], [3 x i32]* %m_currentLimit9, i32 0, i32 %12
  store i32 2, i32* %arrayidx10, align 4, !tbaa !28
  %13 = load float, float* %test_value.addr, align 4, !tbaa !24
  %14 = load float, float* %loLimit, align 4, !tbaa !24
  %sub = fsub float %13, %14
  %m_currentLimitError11 = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 12
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_currentLimitError11)
  %15 = load i32, i32* %limitIndex.addr, align 4, !tbaa !28
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 %15
  store float %sub, float* %arrayidx13, align 4, !tbaa !24
  store i32 2, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %if.end
  %16 = load float, float* %test_value.addr, align 4, !tbaa !24
  %17 = load float, float* %hiLimit, align 4, !tbaa !24
  %cmp14 = fcmp ogt float %16, %17
  br i1 %cmp14, label %if.then15, label %if.end22

if.then15:                                        ; preds = %if.else
  %m_currentLimit16 = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 14
  %18 = load i32, i32* %limitIndex.addr, align 4, !tbaa !28
  %arrayidx17 = getelementptr inbounds [3 x i32], [3 x i32]* %m_currentLimit16, i32 0, i32 %18
  store i32 1, i32* %arrayidx17, align 4, !tbaa !28
  %19 = load float, float* %test_value.addr, align 4, !tbaa !24
  %20 = load float, float* %hiLimit, align 4, !tbaa !24
  %sub18 = fsub float %19, %20
  %m_currentLimitError19 = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 12
  %call20 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_currentLimitError19)
  %21 = load i32, i32* %limitIndex.addr, align 4, !tbaa !28
  %arrayidx21 = getelementptr inbounds float, float* %call20, i32 %21
  store float %sub18, float* %arrayidx21, align 4, !tbaa !24
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end22:                                         ; preds = %if.else
  br label %if.end23

if.end23:                                         ; preds = %if.end22
  %m_currentLimit24 = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 14
  %22 = load i32, i32* %limitIndex.addr, align 4, !tbaa !28
  %arrayidx25 = getelementptr inbounds [3 x i32], [3 x i32]* %m_currentLimit24, i32 0, i32 %22
  store i32 0, i32* %arrayidx25, align 4, !tbaa !28
  %m_currentLimitError26 = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 12
  %call27 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_currentLimitError26)
  %23 = load i32, i32* %limitIndex.addr, align 4, !tbaa !28
  %arrayidx28 = getelementptr inbounds float, float* %call27, i32 %23
  store float 0.000000e+00, float* %arrayidx28, align 4, !tbaa !24
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end23, %if.then15, %if.then8, %if.then
  %24 = bitcast float* %hiLimit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #8
  %25 = bitcast float* %loLimit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #8
  %26 = load i32, i32* %retval, align 4
  ret i32 %26
}

define hidden float @_ZN25btTranslationalLimitMotor15solveLinearAxisEffR11btRigidBodyRK9btVector3S1_S4_iS4_S4_(%class.btTranslationalLimitMotor* %this, float %timeStep, float %jacDiagABInv, %class.btRigidBody* nonnull align 4 dereferenceable(616) %body1, %class.btVector3* nonnull align 4 dereferenceable(16) %pointInA, %class.btRigidBody* nonnull align 4 dereferenceable(616) %body2, %class.btVector3* nonnull align 4 dereferenceable(16) %pointInB, i32 %limit_index, %class.btVector3* nonnull align 4 dereferenceable(16) %axis_normal_on_a, %class.btVector3* nonnull align 4 dereferenceable(16) %anchorPos) #0 {
entry:
  %retval = alloca float, align 4
  %this.addr = alloca %class.btTranslationalLimitMotor*, align 4
  %timeStep.addr = alloca float, align 4
  %jacDiagABInv.addr = alloca float, align 4
  %body1.addr = alloca %class.btRigidBody*, align 4
  %pointInA.addr = alloca %class.btVector3*, align 4
  %body2.addr = alloca %class.btRigidBody*, align 4
  %pointInB.addr = alloca %class.btVector3*, align 4
  %limit_index.addr = alloca i32, align 4
  %axis_normal_on_a.addr = alloca %class.btVector3*, align 4
  %anchorPos.addr = alloca %class.btVector3*, align 4
  %rel_pos1 = alloca %class.btVector3, align 4
  %rel_pos2 = alloca %class.btVector3, align 4
  %vel1 = alloca %class.btVector3, align 4
  %vel2 = alloca %class.btVector3, align 4
  %vel = alloca %class.btVector3, align 4
  %rel_vel = alloca float, align 4
  %depth = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %lo = alloca float, align 4
  %hi = alloca float, align 4
  %minLimit = alloca float, align 4
  %maxLimit = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %normalImpulse = alloca float, align 4
  %oldNormalImpulse = alloca float, align 4
  %sum = alloca float, align 4
  %impulse_vector = alloca %class.btVector3, align 4
  %ref.tmp35 = alloca %class.btVector3, align 4
  store %class.btTranslationalLimitMotor* %this, %class.btTranslationalLimitMotor** %this.addr, align 4, !tbaa !2
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !24
  store float %jacDiagABInv, float* %jacDiagABInv.addr, align 4, !tbaa !24
  store %class.btRigidBody* %body1, %class.btRigidBody** %body1.addr, align 4, !tbaa !2
  store %class.btVector3* %pointInA, %class.btVector3** %pointInA.addr, align 4, !tbaa !2
  store %class.btRigidBody* %body2, %class.btRigidBody** %body2.addr, align 4, !tbaa !2
  store %class.btVector3* %pointInB, %class.btVector3** %pointInB.addr, align 4, !tbaa !2
  store i32 %limit_index, i32* %limit_index.addr, align 4, !tbaa !28
  store %class.btVector3* %axis_normal_on_a, %class.btVector3** %axis_normal_on_a.addr, align 4, !tbaa !2
  store %class.btVector3* %anchorPos, %class.btVector3** %anchorPos.addr, align 4, !tbaa !2
  %this1 = load %class.btTranslationalLimitMotor*, %class.btTranslationalLimitMotor** %this.addr, align 4
  %0 = bitcast %class.btVector3* %rel_pos1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %anchorPos.addr, align 4, !tbaa !2
  %2 = load %class.btRigidBody*, %class.btRigidBody** %body1.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %2)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  %3 = bitcast %class.btVector3* %rel_pos2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #8
  %4 = load %class.btVector3*, %class.btVector3** %anchorPos.addr, align 4, !tbaa !2
  %5 = load %class.btRigidBody*, %class.btRigidBody** %body2.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %5)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %rel_pos2, %class.btVector3* nonnull align 4 dereferenceable(16) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %call2)
  %6 = bitcast %class.btVector3* %vel1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #8
  %7 = load %class.btRigidBody*, %class.btRigidBody** %body1.addr, align 4, !tbaa !2
  call void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* sret align 4 %vel1, %class.btRigidBody* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1)
  %8 = bitcast %class.btVector3* %vel2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #8
  %9 = load %class.btRigidBody*, %class.btRigidBody** %body2.addr, align 4, !tbaa !2
  call void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* sret align 4 %vel2, %class.btRigidBody* %9, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2)
  %10 = bitcast %class.btVector3* %vel to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %vel, %class.btVector3* nonnull align 4 dereferenceable(16) %vel1, %class.btVector3* nonnull align 4 dereferenceable(16) %vel2)
  %11 = bitcast float* %rel_vel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  %12 = load %class.btVector3*, %class.btVector3** %axis_normal_on_a.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %12, %class.btVector3* nonnull align 4 dereferenceable(16) %vel)
  store float %call3, float* %rel_vel, align 4, !tbaa !24
  %13 = bitcast float* %depth to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %14) #8
  %15 = load %class.btVector3*, %class.btVector3** %pointInA.addr, align 4, !tbaa !2
  %16 = load %class.btVector3*, %class.btVector3** %pointInB.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %15, %class.btVector3* nonnull align 4 dereferenceable(16) %16)
  %17 = load %class.btVector3*, %class.btVector3** %axis_normal_on_a.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %17)
  %fneg = fneg float %call4
  %18 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #8
  store float %fneg, float* %depth, align 4, !tbaa !24
  %19 = bitcast float* %lo to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  store float 0xC3ABC16D60000000, float* %lo, align 4, !tbaa !24
  %20 = bitcast float* %hi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #8
  store float 0x43ABC16D60000000, float* %hi, align 4, !tbaa !24
  %21 = bitcast float* %minLimit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #8
  %m_lowerLimit = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 0
  %call5 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_lowerLimit)
  %22 = load i32, i32* %limit_index.addr, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds float, float* %call5, i32 %22
  %23 = load float, float* %arrayidx, align 4, !tbaa !24
  store float %23, float* %minLimit, align 4, !tbaa !24
  %24 = bitcast float* %maxLimit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #8
  %m_upperLimit = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 1
  %call6 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_upperLimit)
  %25 = load i32, i32* %limit_index.addr, align 4, !tbaa !28
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 %25
  %26 = load float, float* %arrayidx7, align 4, !tbaa !24
  store float %26, float* %maxLimit, align 4, !tbaa !24
  %27 = load float, float* %minLimit, align 4, !tbaa !24
  %28 = load float, float* %maxLimit, align 4, !tbaa !24
  %cmp = fcmp olt float %27, %28
  br i1 %cmp, label %if.then, label %if.end15

if.then:                                          ; preds = %entry
  %29 = load float, float* %depth, align 4, !tbaa !24
  %30 = load float, float* %maxLimit, align 4, !tbaa !24
  %cmp8 = fcmp ogt float %29, %30
  br i1 %cmp8, label %if.then9, label %if.else

if.then9:                                         ; preds = %if.then
  %31 = load float, float* %maxLimit, align 4, !tbaa !24
  %32 = load float, float* %depth, align 4, !tbaa !24
  %sub = fsub float %32, %31
  store float %sub, float* %depth, align 4, !tbaa !24
  store float 0.000000e+00, float* %lo, align 4, !tbaa !24
  br label %if.end14

if.else:                                          ; preds = %if.then
  %33 = load float, float* %depth, align 4, !tbaa !24
  %34 = load float, float* %minLimit, align 4, !tbaa !24
  %cmp10 = fcmp olt float %33, %34
  br i1 %cmp10, label %if.then11, label %if.else13

if.then11:                                        ; preds = %if.else
  %35 = load float, float* %minLimit, align 4, !tbaa !24
  %36 = load float, float* %depth, align 4, !tbaa !24
  %sub12 = fsub float %36, %35
  store float %sub12, float* %depth, align 4, !tbaa !24
  store float 0.000000e+00, float* %hi, align 4, !tbaa !24
  br label %if.end

if.else13:                                        ; preds = %if.else
  store float 0.000000e+00, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then11
  br label %if.end14

if.end14:                                         ; preds = %if.end, %if.then9
  br label %if.end15

if.end15:                                         ; preds = %if.end14, %entry
  %37 = bitcast float* %normalImpulse to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #8
  %m_limitSoftness = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 3
  %38 = load float, float* %m_limitSoftness, align 4, !tbaa !25
  %m_restitution = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 5
  %39 = load float, float* %m_restitution, align 4, !tbaa !27
  %40 = load float, float* %depth, align 4, !tbaa !24
  %mul = fmul float %39, %40
  %41 = load float, float* %timeStep.addr, align 4, !tbaa !24
  %div = fdiv float %mul, %41
  %m_damping = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 4
  %42 = load float, float* %m_damping, align 4, !tbaa !26
  %43 = load float, float* %rel_vel, align 4, !tbaa !24
  %mul16 = fmul float %42, %43
  %sub17 = fsub float %div, %mul16
  %mul18 = fmul float %38, %sub17
  %44 = load float, float* %jacDiagABInv.addr, align 4, !tbaa !24
  %mul19 = fmul float %mul18, %44
  store float %mul19, float* %normalImpulse, align 4, !tbaa !24
  %45 = bitcast float* %oldNormalImpulse to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #8
  %m_accumulatedImpulse = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 2
  %call20 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_accumulatedImpulse)
  %46 = load i32, i32* %limit_index.addr, align 4, !tbaa !28
  %arrayidx21 = getelementptr inbounds float, float* %call20, i32 %46
  %47 = load float, float* %arrayidx21, align 4, !tbaa !24
  store float %47, float* %oldNormalImpulse, align 4, !tbaa !24
  %48 = bitcast float* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #8
  %49 = load float, float* %oldNormalImpulse, align 4, !tbaa !24
  %50 = load float, float* %normalImpulse, align 4, !tbaa !24
  %add = fadd float %49, %50
  store float %add, float* %sum, align 4, !tbaa !24
  %51 = load float, float* %sum, align 4, !tbaa !24
  %52 = load float, float* %hi, align 4, !tbaa !24
  %cmp22 = fcmp ogt float %51, %52
  br i1 %cmp22, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end15
  br label %cond.end26

cond.false:                                       ; preds = %if.end15
  %53 = load float, float* %sum, align 4, !tbaa !24
  %54 = load float, float* %lo, align 4, !tbaa !24
  %cmp23 = fcmp olt float %53, %54
  br i1 %cmp23, label %cond.true24, label %cond.false25

cond.true24:                                      ; preds = %cond.false
  br label %cond.end

cond.false25:                                     ; preds = %cond.false
  %55 = load float, float* %sum, align 4, !tbaa !24
  br label %cond.end

cond.end:                                         ; preds = %cond.false25, %cond.true24
  %cond = phi float [ 0.000000e+00, %cond.true24 ], [ %55, %cond.false25 ]
  br label %cond.end26

cond.end26:                                       ; preds = %cond.end, %cond.true
  %cond27 = phi float [ 0.000000e+00, %cond.true ], [ %cond, %cond.end ]
  %m_accumulatedImpulse28 = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 2
  %call29 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_accumulatedImpulse28)
  %56 = load i32, i32* %limit_index.addr, align 4, !tbaa !28
  %arrayidx30 = getelementptr inbounds float, float* %call29, i32 %56
  store float %cond27, float* %arrayidx30, align 4, !tbaa !24
  %m_accumulatedImpulse31 = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 2
  %call32 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_accumulatedImpulse31)
  %57 = load i32, i32* %limit_index.addr, align 4, !tbaa !28
  %arrayidx33 = getelementptr inbounds float, float* %call32, i32 %57
  %58 = load float, float* %arrayidx33, align 4, !tbaa !24
  %59 = load float, float* %oldNormalImpulse, align 4, !tbaa !24
  %sub34 = fsub float %58, %59
  store float %sub34, float* %normalImpulse, align 4, !tbaa !24
  %60 = bitcast %class.btVector3* %impulse_vector to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %60) #8
  %61 = load %class.btVector3*, %class.btVector3** %axis_normal_on_a.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %impulse_vector, %class.btVector3* nonnull align 4 dereferenceable(16) %61, float* nonnull align 4 dereferenceable(4) %normalImpulse)
  %62 = load %class.btRigidBody*, %class.btRigidBody** %body1.addr, align 4, !tbaa !2
  call void @_ZN11btRigidBody12applyImpulseERK9btVector3S2_(%class.btRigidBody* %62, %class.btVector3* nonnull align 4 dereferenceable(16) %impulse_vector, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1)
  %63 = load %class.btRigidBody*, %class.btRigidBody** %body2.addr, align 4, !tbaa !2
  %64 = bitcast %class.btVector3* %ref.tmp35 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %64) #8
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp35, %class.btVector3* nonnull align 4 dereferenceable(16) %impulse_vector)
  call void @_ZN11btRigidBody12applyImpulseERK9btVector3S2_(%class.btRigidBody* %63, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp35, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2)
  %65 = bitcast %class.btVector3* %ref.tmp35 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %65) #8
  %66 = load float, float* %normalImpulse, align 4, !tbaa !24
  store float %66, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %67 = bitcast %class.btVector3* %impulse_vector to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %67) #8
  %68 = bitcast float* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #8
  %69 = bitcast float* %oldNormalImpulse to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #8
  %70 = bitcast float* %normalImpulse to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #8
  br label %cleanup

cleanup:                                          ; preds = %cond.end26, %if.else13
  %71 = bitcast float* %maxLimit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #8
  %72 = bitcast float* %minLimit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #8
  %73 = bitcast float* %hi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #8
  %74 = bitcast float* %lo to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #8
  %75 = bitcast float* %depth to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #8
  %76 = bitcast float* %rel_vel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #8
  %77 = bitcast %class.btVector3* %vel to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %77) #8
  %78 = bitcast %class.btVector3* %vel2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %78) #8
  %79 = bitcast %class.btVector3* %vel1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %79) #8
  %80 = bitcast %class.btVector3* %rel_pos2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %80) #8
  %81 = bitcast %class.btVector3* %rel_pos1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %81) #8
  %82 = load float, float* %retval, align 4
  ret float %82
}

define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %m_worldTransform)
  ret %class.btVector3* %call
}

define linkonce_odr hidden void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos) #0 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %rel_pos.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %rel_pos, %class.btVector3** %rel_pos.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %1 = load %class.btVector3*, %class.btVector3** %rel_pos.addr, align 4, !tbaa !2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %m_angularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !24
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !24
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !24
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !24
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !24
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !24
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !24
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !24
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !24
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

define linkonce_odr hidden void @_ZN11btRigidBody12applyImpulseERK9btVector3S2_(%class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %impulse, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos) #0 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %impulse.addr = alloca %class.btVector3*, align 4
  %rel_pos.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %impulse, %class.btVector3** %impulse.addr, align 4, !tbaa !2
  store %class.btVector3* %rel_pos, %class.btVector3** %rel_pos.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %0 = load float, float* %m_inverseMass, align 4, !tbaa !48
  %cmp = fcmp une float %0, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load %class.btVector3*, %class.btVector3** %impulse.addr, align 4, !tbaa !2
  call void @_ZN11btRigidBody19applyCentralImpulseERK9btVector3(%class.btRigidBody* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %m_angularFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 26
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_angularFactor)
  %tobool = icmp ne float* %call, null
  br i1 %tobool, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #8
  %3 = load %class.btVector3*, %class.btVector3** %rel_pos.addr, align 4, !tbaa !2
  %4 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #8
  %5 = load %class.btVector3*, %class.btVector3** %impulse.addr, align 4, !tbaa !2
  %m_linearFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 5
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %5, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearFactor)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  call void @_ZN11btRigidBody18applyTorqueImpulseERK9btVector3(%class.btRigidBody* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %6 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %6) #8
  %7 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then2, %if.then
  br label %if.end4

if.end4:                                          ; preds = %if.end, %entry
  ret void
}

define hidden void @_ZN23btGeneric6DofConstraint18calculateAngleInfoEv(%class.btGeneric6DofConstraint* %this) #0 {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  %relative_frame = alloca %class.btMatrix3x3, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %axis0 = alloca %class.btVector3, align 4
  %axis2 = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp14 = alloca %class.btVector3, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %0 = bitcast %class.btMatrix3x3* %relative_frame to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %0) #8
  %1 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %1) #8
  %m_calculatedTransformA = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 8
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_calculatedTransformA)
  call void @_ZNK11btMatrix3x37inverseEv(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* %call)
  %m_calculatedTransformB = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 9
  %call2 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_calculatedTransformB)
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %relative_frame, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call2)
  %2 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %2) #8
  %m_calculatedAxisAngleDiff = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 10
  %call3 = call zeroext i1 @_Z16matrixToEulerXYZRK11btMatrix3x3R9btVector3(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %relative_frame, %class.btVector3* nonnull align 4 dereferenceable(16) %m_calculatedAxisAngleDiff)
  %3 = bitcast %class.btVector3* %axis0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #8
  %m_calculatedTransformB4 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 9
  %call5 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_calculatedTransformB4)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %axis0, %class.btMatrix3x3* %call5, i32 0)
  %4 = bitcast %class.btVector3* %axis2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #8
  %m_calculatedTransformA6 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 8
  %call7 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_calculatedTransformA6)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %axis2, %class.btMatrix3x3* %call7, i32 2)
  %5 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #8
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp8, %class.btVector3* %axis2, %class.btVector3* nonnull align 4 dereferenceable(16) %axis0)
  %m_calculatedAxis = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 11
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis, i32 0, i32 1
  %6 = bitcast %class.btVector3* %arrayidx to i8*
  %7 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !22
  %8 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #8
  %9 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #8
  %m_calculatedAxis10 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 11
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis10, i32 0, i32 1
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* %arrayidx11, %class.btVector3* nonnull align 4 dereferenceable(16) %axis2)
  %m_calculatedAxis12 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 11
  %arrayidx13 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis12, i32 0, i32 0
  %10 = bitcast %class.btVector3* %arrayidx13 to i8*
  %11 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !22
  %12 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %12) #8
  %13 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #8
  %m_calculatedAxis15 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 11
  %arrayidx16 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis15, i32 0, i32 1
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp14, %class.btVector3* %axis0, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx16)
  %m_calculatedAxis17 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 11
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis17, i32 0, i32 2
  %14 = bitcast %class.btVector3* %arrayidx18 to i8*
  %15 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %15, i32 16, i1 false), !tbaa.struct !22
  %16 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %16) #8
  %m_calculatedAxis19 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 11
  %arrayidx20 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis19, i32 0, i32 0
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %arrayidx20)
  %m_calculatedAxis22 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 11
  %arrayidx23 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis22, i32 0, i32 1
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %arrayidx23)
  %m_calculatedAxis25 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 11
  %arrayidx26 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis25, i32 0, i32 2
  %call27 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %arrayidx26)
  %17 = bitcast %class.btVector3* %axis2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %17) #8
  %18 = bitcast %class.btVector3* %axis0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #8
  %19 = bitcast %class.btMatrix3x3* %relative_frame to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %19) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %3, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4, !tbaa !24
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !24
  %7 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %9, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %8, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !24
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4, !tbaa !24
  %13 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %15, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %14, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4, !tbaa !24
  %16 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4, !tbaa !24
  %19 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  %20 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %21 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %21, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %20, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4, !tbaa !24
  %22 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #8
  %23 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %24 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %24, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %23, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4, !tbaa !24
  %25 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  %26 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %27 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %27, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %26, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4, !tbaa !24
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  %28 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #8
  %29 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #8
  %30 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #8
  %31 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #8
  %32 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #8
  %33 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #8
  %34 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #8
  %35 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  %36 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x37inverseEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %co = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %det = alloca float, align 4
  %s = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp26 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %1 = bitcast %class.btVector3* %co to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #8
  %2 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %call = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 1, i32 2, i32 2)
  store float %call, float* %ref.tmp, align 4, !tbaa !24
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %call3 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 2, i32 2, i32 0)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !24
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %call5 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 0, i32 2, i32 1)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !24
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %co, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %5 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  %6 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %7 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  %8 = bitcast float* %det to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this1, i32 0)
  %call8 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call7, %class.btVector3* nonnull align 4 dereferenceable(16) %co)
  store float %call8, float* %det, align 4, !tbaa !24
  %9 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  %10 = load float, float* %det, align 4, !tbaa !24
  %div = fdiv float 1.000000e+00, %10
  store float %div, float* %s, align 4, !tbaa !24
  %11 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %co)
  %12 = load float, float* %call10, align 4, !tbaa !24
  %13 = load float, float* %s, align 4, !tbaa !24
  %mul = fmul float %12, %13
  store float %mul, float* %ref.tmp9, align 4, !tbaa !24
  %14 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #8
  %call12 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 2, i32 2, i32 1)
  %15 = load float, float* %s, align 4, !tbaa !24
  %mul13 = fmul float %call12, %15
  store float %mul13, float* %ref.tmp11, align 4, !tbaa !24
  %16 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  %call15 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 1, i32 1, i32 2)
  %17 = load float, float* %s, align 4, !tbaa !24
  %mul16 = fmul float %call15, %17
  store float %mul16, float* %ref.tmp14, align 4, !tbaa !24
  %18 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #8
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %co)
  %19 = load float, float* %call18, align 4, !tbaa !24
  %20 = load float, float* %s, align 4, !tbaa !24
  %mul19 = fmul float %19, %20
  store float %mul19, float* %ref.tmp17, align 4, !tbaa !24
  %21 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #8
  %call21 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 0, i32 2, i32 2)
  %22 = load float, float* %s, align 4, !tbaa !24
  %mul22 = fmul float %call21, %22
  store float %mul22, float* %ref.tmp20, align 4, !tbaa !24
  %23 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #8
  %call24 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 2, i32 1, i32 0)
  %24 = load float, float* %s, align 4, !tbaa !24
  %mul25 = fmul float %call24, %24
  store float %mul25, float* %ref.tmp23, align 4, !tbaa !24
  %25 = bitcast float* %ref.tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  %call27 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %co)
  %26 = load float, float* %call27, align 4, !tbaa !24
  %27 = load float, float* %s, align 4, !tbaa !24
  %mul28 = fmul float %26, %27
  store float %mul28, float* %ref.tmp26, align 4, !tbaa !24
  %28 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #8
  %call30 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 1, i32 2, i32 0)
  %29 = load float, float* %s, align 4, !tbaa !24
  %mul31 = fmul float %call30, %29
  store float %mul31, float* %ref.tmp29, align 4, !tbaa !24
  %30 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #8
  %call33 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 0, i32 1, i32 1)
  %31 = load float, float* %s, align 4, !tbaa !24
  %mul34 = fmul float %call33, %31
  store float %mul34, float* %ref.tmp32, align 4, !tbaa !24
  %call35 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp26, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  %32 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #8
  %33 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #8
  %34 = bitcast float* %ref.tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #8
  %35 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  %36 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #8
  %37 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #8
  %38 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #8
  %39 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #8
  %40 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #8
  %41 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #8
  %42 = bitcast float* %det to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #8
  %43 = bitcast %class.btVector3* %co to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %43) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this, i32 %i) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !28
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx)
  %0 = load i32, i32* %i.addr, align 4, !tbaa !28
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 %0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx4)
  %1 = load i32, i32* %i.addr, align 4, !tbaa !28
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 2
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx8)
  %2 = load i32, i32* %i.addr, align 4, !tbaa !28
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %2
  %call11 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %arrayidx2, float* nonnull align 4 dereferenceable(4) %arrayidx6, float* nonnull align 4 dereferenceable(4) %arrayidx10)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %1 = load float, float* %arrayidx, align 4, !tbaa !24
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %3 = load float, float* %arrayidx3, align 4, !tbaa !24
  %mul = fmul float %1, %3
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %4 = load float, float* %arrayidx5, align 4, !tbaa !24
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %6 = load float, float* %arrayidx7, align 4, !tbaa !24
  %mul8 = fmul float %4, %6
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4, !tbaa !24
  %7 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %8 = load float, float* %arrayidx11, align 4, !tbaa !24
  %9 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %10 = load float, float* %arrayidx13, align 4, !tbaa !24
  %mul14 = fmul float %8, %10
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %11 = load float, float* %arrayidx16, align 4, !tbaa !24
  %12 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %13 = load float, float* %arrayidx18, align 4, !tbaa !24
  %mul19 = fmul float %11, %13
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4, !tbaa !24
  %14 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #8
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %15 = load float, float* %arrayidx23, align 4, !tbaa !24
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %17 = load float, float* %arrayidx25, align 4, !tbaa !24
  %mul26 = fmul float %15, %17
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %18 = load float, float* %arrayidx28, align 4, !tbaa !24
  %19 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %19, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %20 = load float, float* %arrayidx30, align 4, !tbaa !24
  %mul31 = fmul float %18, %20
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4, !tbaa !24
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %21 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  %22 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #8
  %23 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4, !tbaa !24
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1) #8
  ret %class.btVector3* %call2
}

define hidden void @_ZN23btGeneric6DofConstraint19calculateTransformsERK11btTransformS2_(%class.btGeneric6DofConstraint* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB) #0 {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btTransform, align 4
  %ref.tmp2 = alloca %class.btTransform, align 4
  %miA = alloca float, align 4
  %miB = alloca float, align 4
  %miS = alloca float, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4, !tbaa !2
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %0 = bitcast %class.btTransform* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %0) #8
  %1 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %m_frameInA = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 1
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp, %class.btTransform* %1, %class.btTransform* nonnull align 4 dereferenceable(64) %m_frameInA)
  %m_calculatedTransformA = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 8
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_calculatedTransformA, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp)
  %2 = bitcast %class.btTransform* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %2) #8
  %3 = bitcast %class.btTransform* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %3) #8
  %4 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %m_frameInB = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 2
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp2, %class.btTransform* %4, %class.btTransform* nonnull align 4 dereferenceable(64) %m_frameInB)
  %m_calculatedTransformB = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 9
  %call3 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_calculatedTransformB, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp2)
  %5 = bitcast %class.btTransform* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %5) #8
  call void @_ZN23btGeneric6DofConstraint19calculateLinearInfoEv(%class.btGeneric6DofConstraint* %this1)
  call void @_ZN23btGeneric6DofConstraint18calculateAngleInfoEv(%class.btGeneric6DofConstraint* %this1)
  %m_useOffsetForConstraintFrame = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 19
  %6 = load i8, i8* %m_useOffsetForConstraintFrame, align 1, !tbaa !19, !range !10
  %tobool = trunc i8 %6 to i1
  br i1 %tobool, label %if.then, label %if.end13

if.then:                                          ; preds = %entry
  %7 = bitcast float* %miA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %8 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %call4 = call nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %8)
  %call5 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %call4)
  store float %call5, float* %miA, align 4, !tbaa !24
  %9 = bitcast float* %miB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  %10 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %call6 = call nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %10)
  %call7 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %call6)
  store float %call7, float* %miB, align 4, !tbaa !24
  %11 = load float, float* %miA, align 4, !tbaa !24
  %cmp = fcmp olt float %11, 0x3E80000000000000
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %if.then
  %12 = load float, float* %miB, align 4, !tbaa !24
  %cmp8 = fcmp olt float %12, 0x3E80000000000000
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %if.then
  %13 = phi i1 [ true, %if.then ], [ %cmp8, %lor.rhs ]
  %m_hasStaticBody = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 15
  %frombool = zext i1 %13 to i8
  store i8 %frombool, i8* %m_hasStaticBody, align 4, !tbaa !52
  %14 = bitcast float* %miS to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #8
  %15 = load float, float* %miA, align 4, !tbaa !24
  %16 = load float, float* %miB, align 4, !tbaa !24
  %add = fadd float %15, %16
  store float %add, float* %miS, align 4, !tbaa !24
  %17 = load float, float* %miS, align 4, !tbaa !24
  %cmp9 = fcmp ogt float %17, 0.000000e+00
  br i1 %cmp9, label %if.then10, label %if.else

if.then10:                                        ; preds = %lor.end
  %18 = load float, float* %miB, align 4, !tbaa !24
  %19 = load float, float* %miS, align 4, !tbaa !24
  %div = fdiv float %18, %19
  %m_factA = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 13
  store float %div, float* %m_factA, align 4, !tbaa !53
  br label %if.end

if.else:                                          ; preds = %lor.end
  %m_factA11 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 13
  store float 5.000000e-01, float* %m_factA11, align 4, !tbaa !53
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then10
  %m_factA12 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 13
  %20 = load float, float* %m_factA12, align 4, !tbaa !53
  %sub = fsub float 1.000000e+00, %20
  %m_factB = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 14
  store float %sub, float* %m_factB, align 4, !tbaa !54
  %21 = bitcast float* %miS to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  %22 = bitcast float* %miB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #8
  %23 = bitcast float* %miA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #8
  br label %if.end13

if.end13:                                         ; preds = %if.end, %entry
  ret void
}

define hidden void @_ZN23btGeneric6DofConstraint19calculateLinearInfoEv(%class.btGeneric6DofConstraint* %this) #0 {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btMatrix3x3, align 4
  %i = alloca i32, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %m_calculatedTransformB = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 9
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_calculatedTransformB)
  %m_calculatedTransformA = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 8
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_calculatedTransformA)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call2)
  %m_calculatedLinearDiff = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 12
  %1 = bitcast %class.btVector3* %m_calculatedLinearDiff to i8*
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !22
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %3) #8
  %4 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #8
  %5 = bitcast %class.btMatrix3x3* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %5) #8
  %m_calculatedTransformA5 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 8
  %call6 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_calculatedTransformA5)
  call void @_ZNK11btMatrix3x37inverseEv(%class.btMatrix3x3* sret align 4 %ref.tmp4, %class.btMatrix3x3* %call6)
  %m_calculatedLinearDiff7 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 12
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp3, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %m_calculatedLinearDiff7)
  %m_calculatedLinearDiff8 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 12
  %6 = bitcast %class.btVector3* %m_calculatedLinearDiff8 to i8*
  %7 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !22
  %8 = bitcast %class.btMatrix3x3* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %8) #8
  %9 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #8
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %11 = load i32, i32* %i, align 4, !tbaa !28
  %cmp = icmp slt i32 %11, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_calculatedLinearDiff9 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 12
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_calculatedLinearDiff9)
  %13 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds float, float* %call10, i32 %13
  %14 = load float, float* %arrayidx, align 4, !tbaa !24
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %m_currentLinearDiff = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %m_linearLimits, i32 0, i32 13
  %call11 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_currentLinearDiff)
  %15 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx12 = getelementptr inbounds float, float* %call11, i32 %15
  store float %14, float* %arrayidx12, align 4, !tbaa !24
  %m_linearLimits13 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %16 = load i32, i32* %i, align 4, !tbaa !28
  %m_calculatedLinearDiff14 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 12
  %call15 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_calculatedLinearDiff14)
  %17 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 %17
  %18 = load float, float* %arrayidx16, align 4, !tbaa !24
  %call17 = call i32 @_ZN25btTranslationalLimitMotor14testLimitValueEif(%class.btTranslationalLimitMotor* %m_linearLimits13, i32 %16, float %18)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %19 = load i32, i32* %i, align 4, !tbaa !28
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 8
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4, !tbaa !45
  ret %class.btRigidBody* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %0 = load float, float* %m_inverseMass, align 4, !tbaa !48
  ret float %0
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 9
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4, !tbaa !47
  ret %class.btRigidBody* %0
}

define hidden void @_ZN23btGeneric6DofConstraint19buildLinearJacobianER15btJacobianEntryRK9btVector3S4_S4_(%class.btGeneric6DofConstraint* %this, %class.btJacobianEntry* nonnull align 4 dereferenceable(84) %jacLinear, %class.btVector3* nonnull align 4 dereferenceable(16) %normalWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotAInW, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotBInW) #0 {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  %jacLinear.addr = alloca %class.btJacobianEntry*, align 4
  %normalWorld.addr = alloca %class.btVector3*, align 4
  %pivotAInW.addr = alloca %class.btVector3*, align 4
  %pivotBInW.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp3 = alloca %class.btMatrix3x3, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4, !tbaa !2
  store %class.btJacobianEntry* %jacLinear, %class.btJacobianEntry** %jacLinear.addr, align 4, !tbaa !2
  store %class.btVector3* %normalWorld, %class.btVector3** %normalWorld.addr, align 4, !tbaa !2
  store %class.btVector3* %pivotAInW, %class.btVector3** %pivotAInW.addr, align 4, !tbaa !2
  store %class.btVector3* %pivotBInW, %class.btVector3** %pivotBInW.addr, align 4, !tbaa !2
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %0 = load %class.btJacobianEntry*, %class.btJacobianEntry** %jacLinear.addr, align 4, !tbaa !2
  %1 = bitcast %class.btJacobianEntry* %0 to i8*
  %2 = bitcast i8* %1 to %class.btJacobianEntry*
  %3 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %3) #8
  %4 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %4, i32 0, i32 8
  %5 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4, !tbaa !45
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %5)
  %call2 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* %call2)
  %6 = bitcast %class.btMatrix3x3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %6) #8
  %7 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %7, i32 0, i32 9
  %8 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4, !tbaa !47
  %call4 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %8)
  %call5 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call4)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp3, %class.btMatrix3x3* %call5)
  %9 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #8
  %10 = load %class.btVector3*, %class.btVector3** %pivotAInW.addr, align 4, !tbaa !2
  %11 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA7 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %11, i32 0, i32 8
  %12 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA7, align 4, !tbaa !45
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %12)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %10, %class.btVector3* nonnull align 4 dereferenceable(16) %call8)
  %13 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #8
  %14 = load %class.btVector3*, %class.btVector3** %pivotBInW.addr, align 4, !tbaa !2
  %15 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB10 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %15, i32 0, i32 9
  %16 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB10, align 4, !tbaa !47
  %call11 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %16)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %14, %class.btVector3* nonnull align 4 dereferenceable(16) %call11)
  %17 = load %class.btVector3*, %class.btVector3** %normalWorld.addr, align 4, !tbaa !2
  %18 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA12 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %18, i32 0, i32 8
  %19 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA12, align 4, !tbaa !45
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %19)
  %20 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA14 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %20, i32 0, i32 8
  %21 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA14, align 4, !tbaa !45
  %call15 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %21)
  %22 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB16 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %22, i32 0, i32 9
  %23 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB16, align 4, !tbaa !47
  %call17 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %23)
  %24 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB18 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %24, i32 0, i32 9
  %25 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB18, align 4, !tbaa !47
  %call19 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %25)
  %call20 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f(%class.btJacobianEntry* %2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call13, float %call15, %class.btVector3* nonnull align 4 dereferenceable(16) %call17, float %call19)
  %26 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %26) #8
  %27 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %27) #8
  %28 = bitcast %class.btMatrix3x3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %28) #8
  %29 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %29) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_invInertiaLocal = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 8
  ret %class.btVector3* %m_invInertiaLocal
}

define linkonce_odr hidden %class.btJacobianEntry* @_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f(%class.btJacobianEntry* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %world2A, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %world2B, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2, %class.btVector3* nonnull align 4 dereferenceable(16) %jointAxis, %class.btVector3* nonnull align 4 dereferenceable(16) %inertiaInvA, float %massInvA, %class.btVector3* nonnull align 4 dereferenceable(16) %inertiaInvB, float %massInvB) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btJacobianEntry*, align 4
  %world2A.addr = alloca %class.btMatrix3x3*, align 4
  %world2B.addr = alloca %class.btMatrix3x3*, align 4
  %rel_pos1.addr = alloca %class.btVector3*, align 4
  %rel_pos2.addr = alloca %class.btVector3*, align 4
  %jointAxis.addr = alloca %class.btVector3*, align 4
  %inertiaInvA.addr = alloca %class.btVector3*, align 4
  %massInvA.addr = alloca float, align 4
  %inertiaInvB.addr = alloca %class.btVector3*, align 4
  %massInvB.addr = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca %class.btVector3, align 4
  store %class.btJacobianEntry* %this, %class.btJacobianEntry** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %world2A, %class.btMatrix3x3** %world2A.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %world2B, %class.btMatrix3x3** %world2B.addr, align 4, !tbaa !2
  store %class.btVector3* %rel_pos1, %class.btVector3** %rel_pos1.addr, align 4, !tbaa !2
  store %class.btVector3* %rel_pos2, %class.btVector3** %rel_pos2.addr, align 4, !tbaa !2
  store %class.btVector3* %jointAxis, %class.btVector3** %jointAxis.addr, align 4, !tbaa !2
  store %class.btVector3* %inertiaInvA, %class.btVector3** %inertiaInvA.addr, align 4, !tbaa !2
  store float %massInvA, float* %massInvA.addr, align 4, !tbaa !24
  store %class.btVector3* %inertiaInvB, %class.btVector3** %inertiaInvB.addr, align 4, !tbaa !2
  store float %massInvB, float* %massInvB.addr, align 4, !tbaa !24
  %this1 = load %class.btJacobianEntry*, %class.btJacobianEntry** %this.addr, align 4
  %m_linearJointAxis = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %jointAxis.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVector3* %m_linearJointAxis to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !22
  %m_aJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aJ)
  %m_bJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_bJ)
  %m_0MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_0MinvJt)
  %m_1MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_1MinvJt)
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #8
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %world2A.addr, align 4, !tbaa !2
  %5 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %rel_pos1.addr, align 4, !tbaa !2
  %m_linearJointAxis6 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* %6, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearJointAxis6)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %m_aJ7 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %7 = bitcast %class.btVector3* %m_aJ7 to i8*
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !22
  %9 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #8
  %10 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %10) #8
  %11 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #8
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %world2B.addr, align 4, !tbaa !2
  %13 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #8
  %14 = load %class.btVector3*, %class.btVector3** %rel_pos2.addr, align 4, !tbaa !2
  %15 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %15) #8
  %m_linearJointAxis11 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp10, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearJointAxis11)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* %14, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp10)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp8, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %12, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9)
  %m_bJ12 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %16 = bitcast %class.btVector3* %m_bJ12 to i8*
  %17 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false), !tbaa.struct !22
  %18 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #8
  %19 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %19) #8
  %20 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %20) #8
  %21 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %21) #8
  %22 = load %class.btVector3*, %class.btVector3** %inertiaInvA.addr, align 4, !tbaa !2
  %m_aJ14 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp13, %class.btVector3* nonnull align 4 dereferenceable(16) %22, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aJ14)
  %m_0MinvJt15 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %23 = bitcast %class.btVector3* %m_0MinvJt15 to i8*
  %24 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false), !tbaa.struct !22
  %25 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %25) #8
  %26 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %26) #8
  %27 = load %class.btVector3*, %class.btVector3** %inertiaInvB.addr, align 4, !tbaa !2
  %m_bJ17 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp16, %class.btVector3* nonnull align 4 dereferenceable(16) %27, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bJ17)
  %m_1MinvJt18 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %28 = bitcast %class.btVector3* %m_1MinvJt18 to i8*
  %29 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %29, i32 16, i1 false), !tbaa.struct !22
  %30 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %30) #8
  %31 = load float, float* %massInvA.addr, align 4, !tbaa !24
  %m_0MinvJt19 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %m_aJ20 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %call21 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_0MinvJt19, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aJ20)
  %add = fadd float %31, %call21
  %32 = load float, float* %massInvB.addr, align 4, !tbaa !24
  %add22 = fadd float %add, %32
  %m_1MinvJt23 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %m_bJ24 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %call25 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_1MinvJt23, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bJ24)
  %add26 = fadd float %add22, %call25
  %m_Adiag = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 5
  store float %add26, float* %m_Adiag, align 4, !tbaa !55
  ret %class.btJacobianEntry* %this1
}

define hidden void @_ZN23btGeneric6DofConstraint20buildAngularJacobianER15btJacobianEntryRK9btVector3(%class.btGeneric6DofConstraint* %this, %class.btJacobianEntry* nonnull align 4 dereferenceable(84) %jacAngular, %class.btVector3* nonnull align 4 dereferenceable(16) %jointAxisW) #0 {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  %jacAngular.addr = alloca %class.btJacobianEntry*, align 4
  %jointAxisW.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp3 = alloca %class.btMatrix3x3, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4, !tbaa !2
  store %class.btJacobianEntry* %jacAngular, %class.btJacobianEntry** %jacAngular.addr, align 4, !tbaa !2
  store %class.btVector3* %jointAxisW, %class.btVector3** %jointAxisW.addr, align 4, !tbaa !2
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %0 = load %class.btJacobianEntry*, %class.btJacobianEntry** %jacAngular.addr, align 4, !tbaa !2
  %1 = bitcast %class.btJacobianEntry* %0 to i8*
  %2 = bitcast i8* %1 to %class.btJacobianEntry*
  %3 = load %class.btVector3*, %class.btVector3** %jointAxisW.addr, align 4, !tbaa !2
  %4 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %4) #8
  %5 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %5, i32 0, i32 8
  %6 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4, !tbaa !45
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %6)
  %call2 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* %call2)
  %7 = bitcast %class.btMatrix3x3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %7) #8
  %8 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %8, i32 0, i32 9
  %9 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4, !tbaa !47
  %call4 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %9)
  %call5 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call4)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp3, %class.btMatrix3x3* %call5)
  %10 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA6 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %10, i32 0, i32 8
  %11 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA6, align 4, !tbaa !45
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %11)
  %12 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB8 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %12, i32 0, i32 9
  %13 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB8, align 4, !tbaa !47
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %13)
  %call10 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2ERK9btVector3RK11btMatrix3x3S5_S2_S2_(%class.btJacobianEntry* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %call7, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  %14 = bitcast %class.btMatrix3x3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %14) #8
  %15 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %15) #8
  ret void
}

define linkonce_odr hidden %class.btJacobianEntry* @_ZN15btJacobianEntryC2ERK9btVector3RK11btMatrix3x3S5_S2_S2_(%class.btJacobianEntry* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %jointAxis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %world2A, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %world2B, %class.btVector3* nonnull align 4 dereferenceable(16) %inertiaInvA, %class.btVector3* nonnull align 4 dereferenceable(16) %inertiaInvB) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btJacobianEntry*, align 4
  %jointAxis.addr = alloca %class.btVector3*, align 4
  %world2A.addr = alloca %class.btMatrix3x3*, align 4
  %world2B.addr = alloca %class.btMatrix3x3*, align 4
  %inertiaInvA.addr = alloca %class.btVector3*, align 4
  %inertiaInvB.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca %class.btVector3, align 4
  store %class.btJacobianEntry* %this, %class.btJacobianEntry** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %jointAxis, %class.btVector3** %jointAxis.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %world2A, %class.btMatrix3x3** %world2A.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %world2B, %class.btMatrix3x3** %world2B.addr, align 4, !tbaa !2
  store %class.btVector3* %inertiaInvA, %class.btVector3** %inertiaInvA.addr, align 4, !tbaa !2
  store %class.btVector3* %inertiaInvB, %class.btVector3** %inertiaInvB.addr, align 4, !tbaa !2
  %this1 = load %class.btJacobianEntry*, %class.btJacobianEntry** %this.addr, align 4
  %m_linearJointAxis = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !24
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !24
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !24
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %m_linearJointAxis, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  %m_aJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aJ)
  %m_bJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_bJ)
  %m_0MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_0MinvJt)
  %m_1MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_1MinvJt)
  %6 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #8
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %world2A.addr, align 4, !tbaa !2
  %8 = load %class.btVector3*, %class.btVector3** %jointAxis.addr, align 4, !tbaa !2
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp8, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %7, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  %m_aJ9 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %9 = bitcast %class.btVector3* %m_aJ9 to i8*
  %10 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false), !tbaa.struct !22
  %11 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %11) #8
  %12 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %12) #8
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %world2B.addr, align 4, !tbaa !2
  %14 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %14) #8
  %15 = load %class.btVector3*, %class.btVector3** %jointAxis.addr, align 4, !tbaa !2
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp11, %class.btVector3* nonnull align 4 dereferenceable(16) %15)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp10, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %13, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp11)
  %m_bJ12 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %16 = bitcast %class.btVector3* %m_bJ12 to i8*
  %17 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false), !tbaa.struct !22
  %18 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #8
  %19 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %19) #8
  %20 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %20) #8
  %21 = load %class.btVector3*, %class.btVector3** %inertiaInvA.addr, align 4, !tbaa !2
  %m_aJ14 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp13, %class.btVector3* nonnull align 4 dereferenceable(16) %21, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aJ14)
  %m_0MinvJt15 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %22 = bitcast %class.btVector3* %m_0MinvJt15 to i8*
  %23 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %22, i8* align 4 %23, i32 16, i1 false), !tbaa.struct !22
  %24 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %24) #8
  %25 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %25) #8
  %26 = load %class.btVector3*, %class.btVector3** %inertiaInvB.addr, align 4, !tbaa !2
  %m_bJ17 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp16, %class.btVector3* nonnull align 4 dereferenceable(16) %26, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bJ17)
  %m_1MinvJt18 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %27 = bitcast %class.btVector3* %m_1MinvJt18 to i8*
  %28 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %27, i8* align 4 %28, i32 16, i1 false), !tbaa.struct !22
  %29 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #8
  %m_0MinvJt19 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %m_aJ20 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %call21 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_0MinvJt19, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aJ20)
  %m_1MinvJt22 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %m_bJ23 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %call24 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_1MinvJt22, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bJ23)
  %add = fadd float %call21, %call24
  %m_Adiag = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 5
  store float %add, float* %m_Adiag, align 4, !tbaa !55
  ret %class.btJacobianEntry* %this1
}

define hidden zeroext i1 @_ZN23btGeneric6DofConstraint21testAngularLimitMotorEi(%class.btGeneric6DofConstraint* %this, i32 %axis_index) #0 {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  %axis_index.addr = alloca i32, align 4
  %angle = alloca float, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4, !tbaa !2
  store i32 %axis_index, i32* %axis_index.addr, align 4, !tbaa !28
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %0 = bitcast float* %angle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %m_calculatedAxisAngleDiff = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 10
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_calculatedAxisAngleDiff)
  %1 = load i32, i32* %axis_index.addr, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds float, float* %call, i32 %1
  %2 = load float, float* %arrayidx, align 4, !tbaa !24
  store float %2, float* %angle, align 4, !tbaa !24
  %3 = load float, float* %angle, align 4, !tbaa !24
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 6
  %4 = load i32, i32* %axis_index.addr, align 4, !tbaa !28
  %arrayidx2 = getelementptr inbounds [3 x %class.btRotationalLimitMotor], [3 x %class.btRotationalLimitMotor]* %m_angularLimits, i32 0, i32 %4
  %m_loLimit = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %arrayidx2, i32 0, i32 0
  %5 = load float, float* %m_loLimit, align 4, !tbaa !34
  %m_angularLimits3 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 6
  %6 = load i32, i32* %axis_index.addr, align 4, !tbaa !28
  %arrayidx4 = getelementptr inbounds [3 x %class.btRotationalLimitMotor], [3 x %class.btRotationalLimitMotor]* %m_angularLimits3, i32 0, i32 %6
  %m_hiLimit = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %arrayidx4, i32 0, i32 1
  %7 = load float, float* %m_hiLimit, align 4, !tbaa !35
  %call5 = call float @_Z21btAdjustAngleToLimitsfff(float %3, float %5, float %7)
  store float %call5, float* %angle, align 4, !tbaa !24
  %8 = load float, float* %angle, align 4, !tbaa !24
  %m_angularLimits6 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 6
  %9 = load i32, i32* %axis_index.addr, align 4, !tbaa !28
  %arrayidx7 = getelementptr inbounds [3 x %class.btRotationalLimitMotor], [3 x %class.btRotationalLimitMotor]* %m_angularLimits6, i32 0, i32 %9
  %m_currentPosition = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %arrayidx7, i32 0, i32 13
  store float %8, float* %m_currentPosition, align 4, !tbaa !57
  %m_angularLimits8 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 6
  %10 = load i32, i32* %axis_index.addr, align 4, !tbaa !28
  %arrayidx9 = getelementptr inbounds [3 x %class.btRotationalLimitMotor], [3 x %class.btRotationalLimitMotor]* %m_angularLimits8, i32 0, i32 %10
  %11 = load float, float* %angle, align 4, !tbaa !24
  %call10 = call i32 @_ZN22btRotationalLimitMotor14testLimitValueEf(%class.btRotationalLimitMotor* %arrayidx9, float %11)
  %m_angularLimits11 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 6
  %12 = load i32, i32* %axis_index.addr, align 4, !tbaa !28
  %arrayidx12 = getelementptr inbounds [3 x %class.btRotationalLimitMotor], [3 x %class.btRotationalLimitMotor]* %m_angularLimits11, i32 0, i32 %12
  %call13 = call zeroext i1 @_ZN22btRotationalLimitMotor16needApplyTorquesEv(%class.btRotationalLimitMotor* %arrayidx12)
  %13 = bitcast float* %angle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  ret i1 %call13
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_Z21btAdjustAngleToLimitsfff(float %angleInRadians, float %angleLowerLimitInRadians, float %angleUpperLimitInRadians) #2 comdat {
entry:
  %retval = alloca float, align 4
  %angleInRadians.addr = alloca float, align 4
  %angleLowerLimitInRadians.addr = alloca float, align 4
  %angleUpperLimitInRadians.addr = alloca float, align 4
  %diffLo = alloca float, align 4
  %diffHi = alloca float, align 4
  %diffHi11 = alloca float, align 4
  %diffLo15 = alloca float, align 4
  store float %angleInRadians, float* %angleInRadians.addr, align 4, !tbaa !24
  store float %angleLowerLimitInRadians, float* %angleLowerLimitInRadians.addr, align 4, !tbaa !24
  store float %angleUpperLimitInRadians, float* %angleUpperLimitInRadians.addr, align 4, !tbaa !24
  %0 = load float, float* %angleLowerLimitInRadians.addr, align 4, !tbaa !24
  %1 = load float, float* %angleUpperLimitInRadians.addr, align 4, !tbaa !24
  %cmp = fcmp oge float %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load float, float* %angleInRadians.addr, align 4, !tbaa !24
  store float %2, float* %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %3 = load float, float* %angleInRadians.addr, align 4, !tbaa !24
  %4 = load float, float* %angleLowerLimitInRadians.addr, align 4, !tbaa !24
  %cmp1 = fcmp olt float %3, %4
  br i1 %cmp1, label %if.then2, label %if.else8

if.then2:                                         ; preds = %if.else
  %5 = bitcast float* %diffLo to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load float, float* %angleLowerLimitInRadians.addr, align 4, !tbaa !24
  %7 = load float, float* %angleInRadians.addr, align 4, !tbaa !24
  %sub = fsub float %6, %7
  %call = call float @_Z16btNormalizeAnglef(float %sub)
  %call3 = call float @_Z6btFabsf(float %call)
  store float %call3, float* %diffLo, align 4, !tbaa !24
  %8 = bitcast float* %diffHi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  %9 = load float, float* %angleUpperLimitInRadians.addr, align 4, !tbaa !24
  %10 = load float, float* %angleInRadians.addr, align 4, !tbaa !24
  %sub4 = fsub float %9, %10
  %call5 = call float @_Z16btNormalizeAnglef(float %sub4)
  %call6 = call float @_Z6btFabsf(float %call5)
  store float %call6, float* %diffHi, align 4, !tbaa !24
  %11 = load float, float* %diffLo, align 4, !tbaa !24
  %12 = load float, float* %diffHi, align 4, !tbaa !24
  %cmp7 = fcmp olt float %11, %12
  br i1 %cmp7, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then2
  %13 = load float, float* %angleInRadians.addr, align 4, !tbaa !24
  br label %cond.end

cond.false:                                       ; preds = %if.then2
  %14 = load float, float* %angleInRadians.addr, align 4, !tbaa !24
  %add = fadd float %14, 0x401921FB60000000
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %13, %cond.true ], [ %add, %cond.false ]
  store float %cond, float* %retval, align 4
  %15 = bitcast float* %diffHi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %diffLo to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  br label %return

if.else8:                                         ; preds = %if.else
  %17 = load float, float* %angleInRadians.addr, align 4, !tbaa !24
  %18 = load float, float* %angleUpperLimitInRadians.addr, align 4, !tbaa !24
  %cmp9 = fcmp ogt float %17, %18
  br i1 %cmp9, label %if.then10, label %if.else25

if.then10:                                        ; preds = %if.else8
  %19 = bitcast float* %diffHi11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  %20 = load float, float* %angleInRadians.addr, align 4, !tbaa !24
  %21 = load float, float* %angleUpperLimitInRadians.addr, align 4, !tbaa !24
  %sub12 = fsub float %20, %21
  %call13 = call float @_Z16btNormalizeAnglef(float %sub12)
  %call14 = call float @_Z6btFabsf(float %call13)
  store float %call14, float* %diffHi11, align 4, !tbaa !24
  %22 = bitcast float* %diffLo15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #8
  %23 = load float, float* %angleInRadians.addr, align 4, !tbaa !24
  %24 = load float, float* %angleLowerLimitInRadians.addr, align 4, !tbaa !24
  %sub16 = fsub float %23, %24
  %call17 = call float @_Z16btNormalizeAnglef(float %sub16)
  %call18 = call float @_Z6btFabsf(float %call17)
  store float %call18, float* %diffLo15, align 4, !tbaa !24
  %25 = load float, float* %diffLo15, align 4, !tbaa !24
  %26 = load float, float* %diffHi11, align 4, !tbaa !24
  %cmp19 = fcmp olt float %25, %26
  br i1 %cmp19, label %cond.true20, label %cond.false22

cond.true20:                                      ; preds = %if.then10
  %27 = load float, float* %angleInRadians.addr, align 4, !tbaa !24
  %sub21 = fsub float %27, 0x401921FB60000000
  br label %cond.end23

cond.false22:                                     ; preds = %if.then10
  %28 = load float, float* %angleInRadians.addr, align 4, !tbaa !24
  br label %cond.end23

cond.end23:                                       ; preds = %cond.false22, %cond.true20
  %cond24 = phi float [ %sub21, %cond.true20 ], [ %28, %cond.false22 ]
  store float %cond24, float* %retval, align 4
  %29 = bitcast float* %diffLo15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #8
  %30 = bitcast float* %diffHi11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #8
  br label %return

if.else25:                                        ; preds = %if.else8
  %31 = load float, float* %angleInRadians.addr, align 4, !tbaa !24
  store float %31, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.else25, %cond.end23, %cond.end, %if.then
  %32 = load float, float* %retval, align 4
  ret float %32
}

define hidden void @_ZN23btGeneric6DofConstraint13buildJacobianEv(%class.btGeneric6DofConstraint* %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %i = alloca i32, align 4
  %pivotAInW = alloca %class.btVector3, align 4
  %pivotBInW = alloca %class.btVector3, align 4
  %normalWorld = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca %class.btVector3, align 4
  %ref.tmp18 = alloca %class.btVector3, align 4
  %ref.tmp30 = alloca %class.btVector3, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 22
  %0 = load i8, i8* %m_useSolveConstraintObsolete, align 4, !tbaa !21, !range !10
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end36

if.then:                                          ; preds = %entry
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %m_accumulatedImpulse = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %m_linearLimits, i32 0, i32 2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !24
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !24
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !24
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_accumulatedImpulse, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %4 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  %6 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %8 = load i32, i32* %i, align 4, !tbaa !28
  %cmp = icmp slt i32 %8, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 6
  %9 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds [3 x %class.btRotationalLimitMotor], [3 x %class.btRotationalLimitMotor]* %m_angularLimits, i32 0, i32 %9
  %m_accumulatedImpulse4 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %arrayidx, i32 0, i32 15
  store float 0.000000e+00, float* %m_accumulatedImpulse4, align 4, !tbaa !29
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !28
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %11 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %11, i32 0, i32 8
  %12 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4, !tbaa !45
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %12)
  %13 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %13, i32 0, i32 9
  %14 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4, !tbaa !47
  %call5 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %14)
  call void @_ZN23btGeneric6DofConstraint19calculateTransformsERK11btTransformS2_(%class.btGeneric6DofConstraint* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %call, %class.btTransform* nonnull align 4 dereferenceable(64) %call5)
  %15 = bitcast %class.btGeneric6DofConstraint* %this1 to void (%class.btGeneric6DofConstraint*)***
  %vtable = load void (%class.btGeneric6DofConstraint*)**, void (%class.btGeneric6DofConstraint*)*** %15, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btGeneric6DofConstraint*)*, void (%class.btGeneric6DofConstraint*)** %vtable, i64 11
  %16 = load void (%class.btGeneric6DofConstraint*)*, void (%class.btGeneric6DofConstraint*)** %vfn, align 4
  call void %16(%class.btGeneric6DofConstraint* %this1)
  %17 = bitcast %class.btVector3* %pivotAInW to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %17) #8
  %m_AnchorPos = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 17
  %18 = bitcast %class.btVector3* %pivotAInW to i8*
  %19 = bitcast %class.btVector3* %m_AnchorPos to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %18, i8* align 4 %19, i32 16, i1 false), !tbaa.struct !22
  %20 = bitcast %class.btVector3* %pivotBInW to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %20) #8
  %m_AnchorPos6 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 17
  %21 = bitcast %class.btVector3* %pivotBInW to i8*
  %22 = bitcast %class.btVector3* %m_AnchorPos6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false), !tbaa.struct !22
  %23 = bitcast %class.btVector3* %normalWorld to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %23) #8
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %normalWorld)
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc22, %for.end
  %24 = load i32, i32* %i, align 4, !tbaa !28
  %cmp9 = icmp slt i32 %24, 3
  br i1 %cmp9, label %for.body10, label %for.end24

for.body10:                                       ; preds = %for.cond8
  %m_linearLimits11 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %25 = load i32, i32* %i, align 4, !tbaa !28
  %call12 = call zeroext i1 @_ZN25btTranslationalLimitMotor9isLimitedEi(%class.btTranslationalLimitMotor* %m_linearLimits11, i32 %25)
  br i1 %call12, label %if.then13, label %if.end21

if.then13:                                        ; preds = %for.body10
  %m_useLinearReferenceFrameA = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 18
  %26 = load i8, i8* %m_useLinearReferenceFrameA, align 4, !tbaa !11, !range !10
  %tobool14 = trunc i8 %26 to i1
  br i1 %tobool14, label %if.then15, label %if.else

if.then15:                                        ; preds = %if.then13
  %27 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %27) #8
  %m_calculatedTransformA = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 8
  %call17 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_calculatedTransformA)
  %28 = load i32, i32* %i, align 4, !tbaa !28
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp16, %class.btMatrix3x3* %call17, i32 %28)
  %29 = bitcast %class.btVector3* %normalWorld to i8*
  %30 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %29, i8* align 4 %30, i32 16, i1 false), !tbaa.struct !22
  %31 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %31) #8
  br label %if.end

if.else:                                          ; preds = %if.then13
  %32 = bitcast %class.btVector3* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %32) #8
  %m_calculatedTransformB = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 9
  %call19 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_calculatedTransformB)
  %33 = load i32, i32* %i, align 4, !tbaa !28
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp18, %class.btMatrix3x3* %call19, i32 %33)
  %34 = bitcast %class.btVector3* %normalWorld to i8*
  %35 = bitcast %class.btVector3* %ref.tmp18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %34, i8* align 4 %35, i32 16, i1 false), !tbaa.struct !22
  %36 = bitcast %class.btVector3* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %36) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then15
  %m_jacLinear = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 3
  %37 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx20 = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jacLinear, i32 0, i32 %37
  call void @_ZN23btGeneric6DofConstraint19buildLinearJacobianER15btJacobianEntryRK9btVector3S4_S4_(%class.btGeneric6DofConstraint* %this1, %class.btJacobianEntry* nonnull align 4 dereferenceable(84) %arrayidx20, %class.btVector3* nonnull align 4 dereferenceable(16) %normalWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotAInW, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotBInW)
  br label %if.end21

if.end21:                                         ; preds = %if.end, %for.body10
  br label %for.inc22

for.inc22:                                        ; preds = %if.end21
  %38 = load i32, i32* %i, align 4, !tbaa !28
  %inc23 = add nsw i32 %38, 1
  store i32 %inc23, i32* %i, align 4, !tbaa !28
  br label %for.cond8

for.end24:                                        ; preds = %for.cond8
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond25

for.cond25:                                       ; preds = %for.inc33, %for.end24
  %39 = load i32, i32* %i, align 4, !tbaa !28
  %cmp26 = icmp slt i32 %39, 3
  br i1 %cmp26, label %for.body27, label %for.end35

for.body27:                                       ; preds = %for.cond25
  %40 = load i32, i32* %i, align 4, !tbaa !28
  %call28 = call zeroext i1 @_ZN23btGeneric6DofConstraint21testAngularLimitMotorEi(%class.btGeneric6DofConstraint* %this1, i32 %40)
  br i1 %call28, label %if.then29, label %if.end32

if.then29:                                        ; preds = %for.body27
  %41 = bitcast %class.btVector3* %ref.tmp30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %41) #8
  %42 = load i32, i32* %i, align 4, !tbaa !28
  call void @_ZNK23btGeneric6DofConstraint7getAxisEi(%class.btVector3* sret align 4 %ref.tmp30, %class.btGeneric6DofConstraint* %this1, i32 %42)
  %43 = bitcast %class.btVector3* %normalWorld to i8*
  %44 = bitcast %class.btVector3* %ref.tmp30 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %43, i8* align 4 %44, i32 16, i1 false), !tbaa.struct !22
  %45 = bitcast %class.btVector3* %ref.tmp30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %45) #8
  %m_jacAng = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 4
  %46 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx31 = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jacAng, i32 0, i32 %46
  call void @_ZN23btGeneric6DofConstraint20buildAngularJacobianER15btJacobianEntryRK9btVector3(%class.btGeneric6DofConstraint* %this1, %class.btJacobianEntry* nonnull align 4 dereferenceable(84) %arrayidx31, %class.btVector3* nonnull align 4 dereferenceable(16) %normalWorld)
  br label %if.end32

if.end32:                                         ; preds = %if.then29, %for.body27
  br label %for.inc33

for.inc33:                                        ; preds = %if.end32
  %47 = load i32, i32* %i, align 4, !tbaa !28
  %inc34 = add nsw i32 %47, 1
  store i32 %inc34, i32* %i, align 4, !tbaa !28
  br label %for.cond25

for.end35:                                        ; preds = %for.cond25
  %48 = bitcast %class.btVector3* %normalWorld to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %48) #8
  %49 = bitcast %class.btVector3* %pivotBInW to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %49) #8
  %50 = bitcast %class.btVector3* %pivotAInW to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %50) #8
  %51 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #8
  br label %if.end36

if.end36:                                         ; preds = %for.end35, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !24
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !24
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !24
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !24
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !24
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !24
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !24
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZN25btTranslationalLimitMotor9isLimitedEi(%class.btTranslationalLimitMotor* %this, i32 %limitIndex) #4 comdat {
entry:
  %this.addr = alloca %class.btTranslationalLimitMotor*, align 4
  %limitIndex.addr = alloca i32, align 4
  store %class.btTranslationalLimitMotor* %this, %class.btTranslationalLimitMotor** %this.addr, align 4, !tbaa !2
  store i32 %limitIndex, i32* %limitIndex.addr, align 4, !tbaa !28
  %this1 = load %class.btTranslationalLimitMotor*, %class.btTranslationalLimitMotor** %this.addr, align 4
  %m_upperLimit = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 1
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_upperLimit)
  %0 = load i32, i32* %limitIndex.addr, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds float, float* %call, i32 %0
  %1 = load float, float* %arrayidx, align 4, !tbaa !24
  %m_lowerLimit = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 0
  %call2 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_lowerLimit)
  %2 = load i32, i32* %limitIndex.addr, align 4, !tbaa !28
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 %2
  %3 = load float, float* %arrayidx3, align 4, !tbaa !24
  %cmp = fcmp oge float %1, %3
  ret i1 %cmp
}

; Function Attrs: nounwind
define hidden void @_ZNK23btGeneric6DofConstraint7getAxisEi(%class.btVector3* noalias sret align 4 %agg.result, %class.btGeneric6DofConstraint* %this, i32 %axis_index) #3 {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  %axis_index.addr = alloca i32, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4, !tbaa !2
  store i32 %axis_index, i32* %axis_index.addr, align 4, !tbaa !28
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %m_calculatedAxis = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 11
  %0 = load i32, i32* %axis_index.addr, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis, i32 0, i32 %0
  %1 = bitcast %class.btVector3* %agg.result to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !22
  ret void
}

define hidden void @_ZN23btGeneric6DofConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E(%class.btGeneric6DofConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo1"* %info) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo1"*, align 4
  %i = alloca i32, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4, !tbaa !2
  store %"struct.btTypedConstraint::btConstraintInfo1"* %info, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 22
  %0 = load i8, i8* %m_useSolveConstraintObsolete, align 4, !tbaa !21, !range !10
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %m_numConstraintRows = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %1, i32 0, i32 0
  store i32 0, i32* %m_numConstraintRows, align 4, !tbaa !58
  %2 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %nub = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %2, i32 0, i32 1
  store i32 0, i32* %nub, align 4, !tbaa !60
  br label %if.end23

if.else:                                          ; preds = %entry
  %3 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %3, i32 0, i32 8
  %4 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4, !tbaa !45
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %4)
  %5 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %5, i32 0, i32 9
  %6 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4, !tbaa !47
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %6)
  call void @_ZN23btGeneric6DofConstraint19calculateTransformsERK11btTransformS2_(%class.btGeneric6DofConstraint* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %call, %class.btTransform* nonnull align 4 dereferenceable(64) %call2)
  %7 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %m_numConstraintRows3 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %7, i32 0, i32 0
  store i32 0, i32* %m_numConstraintRows3, align 4, !tbaa !58
  %8 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %nub4 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %8, i32 0, i32 1
  store i32 6, i32* %nub4, align 4, !tbaa !60
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else
  %10 = load i32, i32* %i, align 4, !tbaa !28
  %cmp = icmp slt i32 %10, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %11 = load i32, i32* %i, align 4, !tbaa !28
  %call5 = call zeroext i1 @_ZN25btTranslationalLimitMotor14needApplyForceEi(%class.btTranslationalLimitMotor* %m_linearLimits, i32 %11)
  br i1 %call5, label %if.then6, label %if.end

if.then6:                                         ; preds = %for.body
  %12 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %m_numConstraintRows7 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %12, i32 0, i32 0
  %13 = load i32, i32* %m_numConstraintRows7, align 4, !tbaa !58
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %m_numConstraintRows7, align 4, !tbaa !58
  %14 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %nub8 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %14, i32 0, i32 1
  %15 = load i32, i32* %nub8, align 4, !tbaa !60
  %dec = add nsw i32 %15, -1
  store i32 %dec, i32* %nub8, align 4, !tbaa !60
  br label %if.end

if.end:                                           ; preds = %if.then6, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %16 = load i32, i32* %i, align 4, !tbaa !28
  %inc9 = add nsw i32 %16, 1
  store i32 %inc9, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc20, %for.end
  %17 = load i32, i32* %i, align 4, !tbaa !28
  %cmp11 = icmp slt i32 %17, 3
  br i1 %cmp11, label %for.body12, label %for.end22

for.body12:                                       ; preds = %for.cond10
  %18 = load i32, i32* %i, align 4, !tbaa !28
  %call13 = call zeroext i1 @_ZN23btGeneric6DofConstraint21testAngularLimitMotorEi(%class.btGeneric6DofConstraint* %this1, i32 %18)
  br i1 %call13, label %if.then14, label %if.end19

if.then14:                                        ; preds = %for.body12
  %19 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %m_numConstraintRows15 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %19, i32 0, i32 0
  %20 = load i32, i32* %m_numConstraintRows15, align 4, !tbaa !58
  %inc16 = add nsw i32 %20, 1
  store i32 %inc16, i32* %m_numConstraintRows15, align 4, !tbaa !58
  %21 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %nub17 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %21, i32 0, i32 1
  %22 = load i32, i32* %nub17, align 4, !tbaa !60
  %dec18 = add nsw i32 %22, -1
  store i32 %dec18, i32* %nub17, align 4, !tbaa !60
  br label %if.end19

if.end19:                                         ; preds = %if.then14, %for.body12
  br label %for.inc20

for.inc20:                                        ; preds = %if.end19
  %23 = load i32, i32* %i, align 4, !tbaa !28
  %inc21 = add nsw i32 %23, 1
  store i32 %inc21, i32* %i, align 4, !tbaa !28
  br label %for.cond10

for.end22:                                        ; preds = %for.cond10
  %24 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #8
  br label %if.end23

if.end23:                                         ; preds = %for.end22, %if.then
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZN25btTranslationalLimitMotor14needApplyForceEi(%class.btTranslationalLimitMotor* %this, i32 %limitIndex) #4 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btTranslationalLimitMotor*, align 4
  %limitIndex.addr = alloca i32, align 4
  store %class.btTranslationalLimitMotor* %this, %class.btTranslationalLimitMotor** %this.addr, align 4, !tbaa !2
  store i32 %limitIndex, i32* %limitIndex.addr, align 4, !tbaa !28
  %this1 = load %class.btTranslationalLimitMotor*, %class.btTranslationalLimitMotor** %this.addr, align 4
  %m_currentLimit = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 14
  %0 = load i32, i32* %limitIndex.addr, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %m_currentLimit, i32 0, i32 %0
  %1 = load i32, i32* %arrayidx, align 4, !tbaa !28
  %cmp = icmp eq i32 %1, 0
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %m_enableMotor = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %this1, i32 0, i32 9
  %2 = load i32, i32* %limitIndex.addr, align 4, !tbaa !28
  %arrayidx2 = getelementptr inbounds [3 x i8], [3 x i8]* %m_enableMotor, i32 0, i32 %2
  %3 = load i8, i8* %arrayidx2, align 1, !tbaa !6, !range !10
  %tobool = trunc i8 %3 to i1
  %conv = zext i1 %tobool to i32
  %cmp3 = icmp eq i32 %conv, 0
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load i1, i1* %retval, align 1
  ret i1 %4
}

; Function Attrs: nounwind
define hidden void @_ZN23btGeneric6DofConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E(%class.btGeneric6DofConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo1"* %info) #3 {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo1"*, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4, !tbaa !2
  store %"struct.btTypedConstraint::btConstraintInfo1"* %info, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 22
  %0 = load i8, i8* %m_useSolveConstraintObsolete, align 4, !tbaa !21, !range !10
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %m_numConstraintRows = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %1, i32 0, i32 0
  store i32 0, i32* %m_numConstraintRows, align 4, !tbaa !58
  %2 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %nub = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %2, i32 0, i32 1
  store i32 0, i32* %nub, align 4, !tbaa !60
  br label %if.end

if.else:                                          ; preds = %entry
  %3 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %m_numConstraintRows2 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %3, i32 0, i32 0
  store i32 6, i32* %m_numConstraintRows2, align 4, !tbaa !58
  %4 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %nub3 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %4, i32 0, i32 1
  store i32 0, i32* %nub3, align 4, !tbaa !60
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

define hidden void @_ZN23btGeneric6DofConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E(%class.btGeneric6DofConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo2"* %info) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo2"*, align 4
  %transA = alloca %class.btTransform*, align 4
  %transB = alloca %class.btTransform*, align 4
  %linVelA = alloca %class.btVector3*, align 4
  %linVelB = alloca %class.btVector3*, align 4
  %angVelA = alloca %class.btVector3*, align 4
  %angVelB = alloca %class.btVector3*, align 4
  %row = alloca i32, align 4
  %row13 = alloca i32, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4, !tbaa !2
  store %"struct.btTypedConstraint::btConstraintInfo2"* %info, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %0 = bitcast %class.btTransform** %transA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %1, i32 0, i32 8
  %2 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4, !tbaa !45
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %2)
  store %class.btTransform* %call, %class.btTransform** %transA, align 4, !tbaa !2
  %3 = bitcast %class.btTransform** %transB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %4, i32 0, i32 9
  %5 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4, !tbaa !47
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %5)
  store %class.btTransform* %call2, %class.btTransform** %transB, align 4, !tbaa !2
  %6 = bitcast %class.btVector3** %linVelA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA3 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %7, i32 0, i32 8
  %8 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA3, align 4, !tbaa !45
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody17getLinearVelocityEv(%class.btRigidBody* %8)
  store %class.btVector3* %call4, %class.btVector3** %linVelA, align 4, !tbaa !2
  %9 = bitcast %class.btVector3** %linVelB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  %10 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB5 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %10, i32 0, i32 9
  %11 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB5, align 4, !tbaa !47
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody17getLinearVelocityEv(%class.btRigidBody* %11)
  store %class.btVector3* %call6, %class.btVector3** %linVelB, align 4, !tbaa !2
  %12 = bitcast %class.btVector3** %angVelA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  %13 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA7 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %13, i32 0, i32 8
  %14 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA7, align 4, !tbaa !45
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %14)
  store %class.btVector3* %call8, %class.btVector3** %angVelA, align 4, !tbaa !2
  %15 = bitcast %class.btVector3** %angVelB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  %16 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB9 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %16, i32 0, i32 9
  %17 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB9, align 4, !tbaa !47
  %call10 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %17)
  store %class.btVector3* %call10, %class.btVector3** %angVelB, align 4, !tbaa !2
  %m_useOffsetForConstraintFrame = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 19
  %18 = load i8, i8* %m_useOffsetForConstraintFrame, align 1, !tbaa !19, !range !10
  %tobool = trunc i8 %18 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %19 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  %20 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %21 = load %class.btTransform*, %class.btTransform** %transA, align 4, !tbaa !2
  %22 = load %class.btTransform*, %class.btTransform** %transB, align 4, !tbaa !2
  %23 = load %class.btVector3*, %class.btVector3** %linVelA, align 4, !tbaa !2
  %24 = load %class.btVector3*, %class.btVector3** %linVelB, align 4, !tbaa !2
  %25 = load %class.btVector3*, %class.btVector3** %angVelA, align 4, !tbaa !2
  %26 = load %class.btVector3*, %class.btVector3** %angVelB, align 4, !tbaa !2
  %call11 = call i32 @_ZN23btGeneric6DofConstraint16setAngularLimitsEPN17btTypedConstraint17btConstraintInfo2EiRK11btTransformS5_RK9btVector3S8_S8_S8_(%class.btGeneric6DofConstraint* %this1, %"struct.btTypedConstraint::btConstraintInfo2"* %20, i32 0, %class.btTransform* nonnull align 4 dereferenceable(64) %21, %class.btTransform* nonnull align 4 dereferenceable(64) %22, %class.btVector3* nonnull align 4 dereferenceable(16) %23, %class.btVector3* nonnull align 4 dereferenceable(16) %24, %class.btVector3* nonnull align 4 dereferenceable(16) %25, %class.btVector3* nonnull align 4 dereferenceable(16) %26)
  store i32 %call11, i32* %row, align 4, !tbaa !28
  %27 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %28 = load i32, i32* %row, align 4, !tbaa !28
  %29 = load %class.btTransform*, %class.btTransform** %transA, align 4, !tbaa !2
  %30 = load %class.btTransform*, %class.btTransform** %transB, align 4, !tbaa !2
  %31 = load %class.btVector3*, %class.btVector3** %linVelA, align 4, !tbaa !2
  %32 = load %class.btVector3*, %class.btVector3** %linVelB, align 4, !tbaa !2
  %33 = load %class.btVector3*, %class.btVector3** %angVelA, align 4, !tbaa !2
  %34 = load %class.btVector3*, %class.btVector3** %angVelB, align 4, !tbaa !2
  %call12 = call i32 @_ZN23btGeneric6DofConstraint15setLinearLimitsEPN17btTypedConstraint17btConstraintInfo2EiRK11btTransformS5_RK9btVector3S8_S8_S8_(%class.btGeneric6DofConstraint* %this1, %"struct.btTypedConstraint::btConstraintInfo2"* %27, i32 %28, %class.btTransform* nonnull align 4 dereferenceable(64) %29, %class.btTransform* nonnull align 4 dereferenceable(64) %30, %class.btVector3* nonnull align 4 dereferenceable(16) %31, %class.btVector3* nonnull align 4 dereferenceable(16) %32, %class.btVector3* nonnull align 4 dereferenceable(16) %33, %class.btVector3* nonnull align 4 dereferenceable(16) %34)
  %35 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  br label %if.end

if.else:                                          ; preds = %entry
  %36 = bitcast i32* %row13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #8
  %37 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %38 = load %class.btTransform*, %class.btTransform** %transA, align 4, !tbaa !2
  %39 = load %class.btTransform*, %class.btTransform** %transB, align 4, !tbaa !2
  %40 = load %class.btVector3*, %class.btVector3** %linVelA, align 4, !tbaa !2
  %41 = load %class.btVector3*, %class.btVector3** %linVelB, align 4, !tbaa !2
  %42 = load %class.btVector3*, %class.btVector3** %angVelA, align 4, !tbaa !2
  %43 = load %class.btVector3*, %class.btVector3** %angVelB, align 4, !tbaa !2
  %call14 = call i32 @_ZN23btGeneric6DofConstraint15setLinearLimitsEPN17btTypedConstraint17btConstraintInfo2EiRK11btTransformS5_RK9btVector3S8_S8_S8_(%class.btGeneric6DofConstraint* %this1, %"struct.btTypedConstraint::btConstraintInfo2"* %37, i32 0, %class.btTransform* nonnull align 4 dereferenceable(64) %38, %class.btTransform* nonnull align 4 dereferenceable(64) %39, %class.btVector3* nonnull align 4 dereferenceable(16) %40, %class.btVector3* nonnull align 4 dereferenceable(16) %41, %class.btVector3* nonnull align 4 dereferenceable(16) %42, %class.btVector3* nonnull align 4 dereferenceable(16) %43)
  store i32 %call14, i32* %row13, align 4, !tbaa !28
  %44 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %45 = load i32, i32* %row13, align 4, !tbaa !28
  %46 = load %class.btTransform*, %class.btTransform** %transA, align 4, !tbaa !2
  %47 = load %class.btTransform*, %class.btTransform** %transB, align 4, !tbaa !2
  %48 = load %class.btVector3*, %class.btVector3** %linVelA, align 4, !tbaa !2
  %49 = load %class.btVector3*, %class.btVector3** %linVelB, align 4, !tbaa !2
  %50 = load %class.btVector3*, %class.btVector3** %angVelA, align 4, !tbaa !2
  %51 = load %class.btVector3*, %class.btVector3** %angVelB, align 4, !tbaa !2
  %call15 = call i32 @_ZN23btGeneric6DofConstraint16setAngularLimitsEPN17btTypedConstraint17btConstraintInfo2EiRK11btTransformS5_RK9btVector3S8_S8_S8_(%class.btGeneric6DofConstraint* %this1, %"struct.btTypedConstraint::btConstraintInfo2"* %44, i32 %45, %class.btTransform* nonnull align 4 dereferenceable(64) %46, %class.btTransform* nonnull align 4 dereferenceable(64) %47, %class.btVector3* nonnull align 4 dereferenceable(16) %48, %class.btVector3* nonnull align 4 dereferenceable(16) %49, %class.btVector3* nonnull align 4 dereferenceable(16) %50, %class.btVector3* nonnull align 4 dereferenceable(16) %51)
  %52 = bitcast i32* %row13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %53 = bitcast %class.btVector3** %angVelB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #8
  %54 = bitcast %class.btVector3** %angVelA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #8
  %55 = bitcast %class.btVector3** %linVelB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #8
  %56 = bitcast %class.btVector3** %linVelA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #8
  %57 = bitcast %class.btTransform** %transB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #8
  %58 = bitcast %class.btTransform** %transA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody17getLinearVelocityEv(%class.btRigidBody* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  ret %class.btVector3* %m_linearVelocity
}

define hidden i32 @_ZN23btGeneric6DofConstraint16setAngularLimitsEPN17btTypedConstraint17btConstraintInfo2EiRK11btTransformS5_RK9btVector3S8_S8_S8_(%class.btGeneric6DofConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo2"* %info, i32 %row_offset, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB, %class.btVector3* nonnull align 4 dereferenceable(16) %linVelA, %class.btVector3* nonnull align 4 dereferenceable(16) %linVelB, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelA, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelB) #0 {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo2"*, align 4
  %row_offset.addr = alloca i32, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %linVelA.addr = alloca %class.btVector3*, align 4
  %linVelB.addr = alloca %class.btVector3*, align 4
  %angVelA.addr = alloca %class.btVector3*, align 4
  %angVelB.addr = alloca %class.btVector3*, align 4
  %d6constraint = alloca %class.btGeneric6DofConstraint*, align 4
  %row = alloca i32, align 4
  %i = alloca i32, align 4
  %axis = alloca %class.btVector3, align 4
  %flags = alloca i32, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4, !tbaa !2
  store %"struct.btTypedConstraint::btConstraintInfo2"* %info, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  store i32 %row_offset, i32* %row_offset.addr, align 4, !tbaa !28
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4, !tbaa !2
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4, !tbaa !2
  store %class.btVector3* %linVelA, %class.btVector3** %linVelA.addr, align 4, !tbaa !2
  store %class.btVector3* %linVelB, %class.btVector3** %linVelB.addr, align 4, !tbaa !2
  store %class.btVector3* %angVelA, %class.btVector3** %angVelA.addr, align 4, !tbaa !2
  store %class.btVector3* %angVelB, %class.btVector3** %angVelB.addr, align 4, !tbaa !2
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %0 = bitcast %class.btGeneric6DofConstraint** %d6constraint to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store %class.btGeneric6DofConstraint* %this1, %class.btGeneric6DofConstraint** %d6constraint, align 4, !tbaa !2
  %1 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %row_offset.addr, align 4, !tbaa !28
  store i32 %2, i32* %row, align 4, !tbaa !28
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %4 = load i32, i32* %i, align 4, !tbaa !28
  %cmp = icmp slt i32 %4, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %6 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %d6constraint, align 4, !tbaa !2
  %7 = load i32, i32* %i, align 4, !tbaa !28
  %call = call %class.btRotationalLimitMotor* @_ZN23btGeneric6DofConstraint23getRotationalLimitMotorEi(%class.btGeneric6DofConstraint* %6, i32 %7)
  %call2 = call zeroext i1 @_ZN22btRotationalLimitMotor16needApplyTorquesEv(%class.btRotationalLimitMotor* %call)
  br i1 %call2, label %if.then, label %if.end22

if.then:                                          ; preds = %for.body
  %8 = bitcast %class.btVector3* %axis to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #8
  %9 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %d6constraint, align 4, !tbaa !2
  %10 = load i32, i32* %i, align 4, !tbaa !28
  call void @_ZNK23btGeneric6DofConstraint7getAxisEi(%class.btVector3* sret align 4 %axis, %class.btGeneric6DofConstraint* %9, i32 %10)
  %11 = bitcast i32* %flags to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  %m_flags = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 21
  %12 = load i32, i32* %m_flags, align 4, !tbaa !20
  %13 = load i32, i32* %i, align 4, !tbaa !28
  %add = add nsw i32 %13, 3
  %mul = mul nsw i32 %add, 3
  %shr = ashr i32 %12, %mul
  store i32 %shr, i32* %flags, align 4, !tbaa !28
  %14 = load i32, i32* %flags, align 4, !tbaa !28
  %and = and i32 %14, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.end, label %if.then3

if.then3:                                         ; preds = %if.then
  %15 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %cfm = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %15, i32 0, i32 8
  %16 = load float*, float** %cfm, align 4, !tbaa !61
  %arrayidx = getelementptr inbounds float, float* %16, i32 0
  %17 = load float, float* %arrayidx, align 4, !tbaa !24
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 6
  %18 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx4 = getelementptr inbounds [3 x %class.btRotationalLimitMotor], [3 x %class.btRotationalLimitMotor]* %m_angularLimits, i32 0, i32 %18
  %m_normalCFM = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %arrayidx4, i32 0, i32 7
  store float %17, float* %m_normalCFM, align 4, !tbaa !36
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %19 = load i32, i32* %flags, align 4, !tbaa !28
  %and5 = and i32 %19, 2
  %tobool6 = icmp ne i32 %and5, 0
  br i1 %tobool6, label %if.end12, label %if.then7

if.then7:                                         ; preds = %if.end
  %20 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %cfm8 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %20, i32 0, i32 8
  %21 = load float*, float** %cfm8, align 4, !tbaa !61
  %arrayidx9 = getelementptr inbounds float, float* %21, i32 0
  %22 = load float, float* %arrayidx9, align 4, !tbaa !24
  %m_angularLimits10 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 6
  %23 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx11 = getelementptr inbounds [3 x %class.btRotationalLimitMotor], [3 x %class.btRotationalLimitMotor]* %m_angularLimits10, i32 0, i32 %23
  %m_stopCFM = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %arrayidx11, i32 0, i32 9
  store float %22, float* %m_stopCFM, align 4, !tbaa !38
  br label %if.end12

if.end12:                                         ; preds = %if.then7, %if.end
  %24 = load i32, i32* %flags, align 4, !tbaa !28
  %and13 = and i32 %24, 4
  %tobool14 = icmp ne i32 %and13, 0
  br i1 %tobool14, label %if.end18, label %if.then15

if.then15:                                        ; preds = %if.end12
  %25 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %erp = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %25, i32 0, i32 1
  %26 = load float, float* %erp, align 4, !tbaa !63
  %m_angularLimits16 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 6
  %27 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx17 = getelementptr inbounds [3 x %class.btRotationalLimitMotor], [3 x %class.btRotationalLimitMotor]* %m_angularLimits16, i32 0, i32 %27
  %m_stopERP = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %arrayidx17, i32 0, i32 8
  store float %26, float* %m_stopERP, align 4, !tbaa !37
  br label %if.end18

if.end18:                                         ; preds = %if.then15, %if.end12
  %28 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %d6constraint, align 4, !tbaa !2
  %29 = load i32, i32* %i, align 4, !tbaa !28
  %call19 = call %class.btRotationalLimitMotor* @_ZN23btGeneric6DofConstraint23getRotationalLimitMotorEi(%class.btGeneric6DofConstraint* %28, i32 %29)
  %30 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %31 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %32 = load %class.btVector3*, %class.btVector3** %linVelA.addr, align 4, !tbaa !2
  %33 = load %class.btVector3*, %class.btVector3** %linVelB.addr, align 4, !tbaa !2
  %34 = load %class.btVector3*, %class.btVector3** %angVelA.addr, align 4, !tbaa !2
  %35 = load %class.btVector3*, %class.btVector3** %angVelB.addr, align 4, !tbaa !2
  %36 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %37 = load i32, i32* %row, align 4, !tbaa !28
  %call20 = call i32 @_ZN23btGeneric6DofConstraint21get_limit_motor_info2EP22btRotationalLimitMotorRK11btTransformS4_RK9btVector3S7_S7_S7_PN17btTypedConstraint17btConstraintInfo2EiRS5_ii(%class.btGeneric6DofConstraint* %this1, %class.btRotationalLimitMotor* %call19, %class.btTransform* nonnull align 4 dereferenceable(64) %30, %class.btTransform* nonnull align 4 dereferenceable(64) %31, %class.btVector3* nonnull align 4 dereferenceable(16) %32, %class.btVector3* nonnull align 4 dereferenceable(16) %33, %class.btVector3* nonnull align 4 dereferenceable(16) %34, %class.btVector3* nonnull align 4 dereferenceable(16) %35, %"struct.btTypedConstraint::btConstraintInfo2"* %36, i32 %37, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, i32 1, i32 0)
  %38 = load i32, i32* %row, align 4, !tbaa !28
  %add21 = add nsw i32 %38, %call20
  store i32 %add21, i32* %row, align 4, !tbaa !28
  %39 = bitcast i32* %flags to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #8
  %40 = bitcast %class.btVector3* %axis to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %40) #8
  br label %if.end22

if.end22:                                         ; preds = %if.end18, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end22
  %41 = load i32, i32* %i, align 4, !tbaa !28
  %inc = add nsw i32 %41, 1
  store i32 %inc, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %42 = load i32, i32* %row, align 4, !tbaa !28
  %43 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #8
  %44 = bitcast %class.btGeneric6DofConstraint** %d6constraint to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #8
  ret i32 %42
}

define hidden i32 @_ZN23btGeneric6DofConstraint15setLinearLimitsEPN17btTypedConstraint17btConstraintInfo2EiRK11btTransformS5_RK9btVector3S8_S8_S8_(%class.btGeneric6DofConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo2"* %info, i32 %row, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB, %class.btVector3* nonnull align 4 dereferenceable(16) %linVelA, %class.btVector3* nonnull align 4 dereferenceable(16) %linVelB, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelA, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelB) #0 {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo2"*, align 4
  %row.addr = alloca i32, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %linVelA.addr = alloca %class.btVector3*, align 4
  %linVelB.addr = alloca %class.btVector3*, align 4
  %angVelA.addr = alloca %class.btVector3*, align 4
  %angVelB.addr = alloca %class.btVector3*, align 4
  %limot = alloca %class.btRotationalLimitMotor, align 4
  %i = alloca i32, align 4
  %axis = alloca %class.btVector3, align 4
  %flags = alloca i32, align 4
  %indx1 = alloca i32, align 4
  %indx2 = alloca i32, align 4
  %rotAllowed = alloca i32, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4, !tbaa !2
  store %"struct.btTypedConstraint::btConstraintInfo2"* %info, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  store i32 %row, i32* %row.addr, align 4, !tbaa !28
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4, !tbaa !2
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4, !tbaa !2
  store %class.btVector3* %linVelA, %class.btVector3** %linVelA.addr, align 4, !tbaa !2
  store %class.btVector3* %linVelB, %class.btVector3** %linVelB.addr, align 4, !tbaa !2
  store %class.btVector3* %angVelA, %class.btVector3** %angVelA.addr, align 4, !tbaa !2
  store %class.btVector3* %angVelB, %class.btVector3** %angVelB.addr, align 4, !tbaa !2
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %0 = bitcast %class.btRotationalLimitMotor* %limot to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %0) #8
  %call = call %class.btRotationalLimitMotor* @_ZN22btRotationalLimitMotorC2Ev(%class.btRotationalLimitMotor* %limot)
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !28
  %cmp = icmp slt i32 %2, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %4 = load i32, i32* %i, align 4, !tbaa !28
  %call2 = call zeroext i1 @_ZN25btTranslationalLimitMotor14needApplyForceEi(%class.btTranslationalLimitMotor* %m_linearLimits, i32 %4)
  br i1 %call2, label %if.then, label %if.end79

if.then:                                          ; preds = %for.body
  %m_bounce = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %limot, i32 0, i32 10
  store float 0.000000e+00, float* %m_bounce, align 4, !tbaa !39
  %m_linearLimits3 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %m_currentLimit = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %m_linearLimits3, i32 0, i32 14
  %5 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %m_currentLimit, i32 0, i32 %5
  %6 = load i32, i32* %arrayidx, align 4, !tbaa !28
  %m_currentLimit4 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %limot, i32 0, i32 14
  store i32 %6, i32* %m_currentLimit4, align 4, !tbaa !42
  %m_linearLimits5 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %m_currentLinearDiff = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %m_linearLimits5, i32 0, i32 13
  %call6 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_currentLinearDiff)
  %7 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 %7
  %8 = load float, float* %arrayidx7, align 4, !tbaa !24
  %m_currentPosition = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %limot, i32 0, i32 13
  store float %8, float* %m_currentPosition, align 4, !tbaa !57
  %m_linearLimits8 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %m_currentLimitError = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %m_linearLimits8, i32 0, i32 12
  %call9 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_currentLimitError)
  %9 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %9
  %10 = load float, float* %arrayidx10, align 4, !tbaa !24
  %m_currentLimitError11 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %limot, i32 0, i32 12
  store float %10, float* %m_currentLimitError11, align 4, !tbaa !43
  %m_linearLimits12 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %m_damping = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %m_linearLimits12, i32 0, i32 4
  %11 = load float, float* %m_damping, align 4, !tbaa !64
  %m_damping13 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %limot, i32 0, i32 5
  store float %11, float* %m_damping13, align 4, !tbaa !40
  %m_linearLimits14 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %m_enableMotor = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %m_linearLimits14, i32 0, i32 9
  %12 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx15 = getelementptr inbounds [3 x i8], [3 x i8]* %m_enableMotor, i32 0, i32 %12
  %13 = load i8, i8* %arrayidx15, align 1, !tbaa !6, !range !10
  %tobool = trunc i8 %13 to i1
  %m_enableMotor16 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %limot, i32 0, i32 11
  %frombool = zext i1 %tobool to i8
  store i8 %frombool, i8* %m_enableMotor16, align 4, !tbaa !44
  %m_linearLimits17 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %m_upperLimit = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %m_linearLimits17, i32 0, i32 1
  %call18 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_upperLimit)
  %14 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 %14
  %15 = load float, float* %arrayidx19, align 4, !tbaa !24
  %m_hiLimit = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %limot, i32 0, i32 1
  store float %15, float* %m_hiLimit, align 4, !tbaa !35
  %m_linearLimits20 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %m_limitSoftness = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %m_linearLimits20, i32 0, i32 3
  %16 = load float, float* %m_limitSoftness, align 4, !tbaa !65
  %m_limitSoftness21 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %limot, i32 0, i32 6
  store float %16, float* %m_limitSoftness21, align 4, !tbaa !41
  %m_linearLimits22 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %m_lowerLimit = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %m_linearLimits22, i32 0, i32 0
  %call23 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_lowerLimit)
  %17 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx24 = getelementptr inbounds float, float* %call23, i32 %17
  %18 = load float, float* %arrayidx24, align 4, !tbaa !24
  %m_loLimit = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %limot, i32 0, i32 0
  store float %18, float* %m_loLimit, align 4, !tbaa !34
  %m_maxLimitForce = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %limot, i32 0, i32 4
  store float 0.000000e+00, float* %m_maxLimitForce, align 4, !tbaa !33
  %m_linearLimits25 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %m_maxMotorForce = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %m_linearLimits25, i32 0, i32 11
  %call26 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_maxMotorForce)
  %19 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx27 = getelementptr inbounds float, float* %call26, i32 %19
  %20 = load float, float* %arrayidx27, align 4, !tbaa !24
  %m_maxMotorForce28 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %limot, i32 0, i32 3
  store float %20, float* %m_maxMotorForce28, align 4, !tbaa !32
  %m_linearLimits29 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %m_targetVelocity = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %m_linearLimits29, i32 0, i32 10
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_targetVelocity)
  %21 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 %21
  %22 = load float, float* %arrayidx31, align 4, !tbaa !24
  %m_targetVelocity32 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %limot, i32 0, i32 2
  store float %22, float* %m_targetVelocity32, align 4, !tbaa !31
  %23 = bitcast %class.btVector3* %axis to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %23) #8
  %m_calculatedTransformA = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 8
  %call33 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_calculatedTransformA)
  %24 = load i32, i32* %i, align 4, !tbaa !28
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %axis, %class.btMatrix3x3* %call33, i32 %24)
  %25 = bitcast i32* %flags to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  %m_flags = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 21
  %26 = load i32, i32* %m_flags, align 4, !tbaa !20
  %27 = load i32, i32* %i, align 4, !tbaa !28
  %mul = mul nsw i32 %27, 3
  %shr = ashr i32 %26, %mul
  store i32 %shr, i32* %flags, align 4, !tbaa !28
  %28 = load i32, i32* %flags, align 4, !tbaa !28
  %and = and i32 %28, 1
  %tobool34 = icmp ne i32 %and, 0
  br i1 %tobool34, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %m_linearLimits35 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %m_normalCFM = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %m_linearLimits35, i32 0, i32 6
  %call36 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_normalCFM)
  %29 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx37 = getelementptr inbounds float, float* %call36, i32 %29
  %30 = load float, float* %arrayidx37, align 4, !tbaa !24
  br label %cond.end

cond.false:                                       ; preds = %if.then
  %31 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %cfm = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %31, i32 0, i32 8
  %32 = load float*, float** %cfm, align 4, !tbaa !61
  %arrayidx38 = getelementptr inbounds float, float* %32, i32 0
  %33 = load float, float* %arrayidx38, align 4, !tbaa !24
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %30, %cond.true ], [ %33, %cond.false ]
  %m_normalCFM39 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %limot, i32 0, i32 7
  store float %cond, float* %m_normalCFM39, align 4, !tbaa !36
  %34 = load i32, i32* %flags, align 4, !tbaa !28
  %and40 = and i32 %34, 2
  %tobool41 = icmp ne i32 %and40, 0
  br i1 %tobool41, label %cond.true42, label %cond.false46

cond.true42:                                      ; preds = %cond.end
  %m_linearLimits43 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %m_stopCFM = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %m_linearLimits43, i32 0, i32 8
  %call44 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_stopCFM)
  %35 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx45 = getelementptr inbounds float, float* %call44, i32 %35
  %36 = load float, float* %arrayidx45, align 4, !tbaa !24
  br label %cond.end49

cond.false46:                                     ; preds = %cond.end
  %37 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %cfm47 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %37, i32 0, i32 8
  %38 = load float*, float** %cfm47, align 4, !tbaa !61
  %arrayidx48 = getelementptr inbounds float, float* %38, i32 0
  %39 = load float, float* %arrayidx48, align 4, !tbaa !24
  br label %cond.end49

cond.end49:                                       ; preds = %cond.false46, %cond.true42
  %cond50 = phi float [ %36, %cond.true42 ], [ %39, %cond.false46 ]
  %m_stopCFM51 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %limot, i32 0, i32 9
  store float %cond50, float* %m_stopCFM51, align 4, !tbaa !38
  %40 = load i32, i32* %flags, align 4, !tbaa !28
  %and52 = and i32 %40, 4
  %tobool53 = icmp ne i32 %and52, 0
  br i1 %tobool53, label %cond.true54, label %cond.false58

cond.true54:                                      ; preds = %cond.end49
  %m_linearLimits55 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %m_stopERP = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %m_linearLimits55, i32 0, i32 7
  %call56 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_stopERP)
  %41 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx57 = getelementptr inbounds float, float* %call56, i32 %41
  %42 = load float, float* %arrayidx57, align 4, !tbaa !24
  br label %cond.end59

cond.false58:                                     ; preds = %cond.end49
  %43 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %erp = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %43, i32 0, i32 1
  %44 = load float, float* %erp, align 4, !tbaa !63
  br label %cond.end59

cond.end59:                                       ; preds = %cond.false58, %cond.true54
  %cond60 = phi float [ %42, %cond.true54 ], [ %44, %cond.false58 ]
  %m_stopERP61 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %limot, i32 0, i32 8
  store float %cond60, float* %m_stopERP61, align 4, !tbaa !37
  %m_useOffsetForConstraintFrame = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 19
  %45 = load i8, i8* %m_useOffsetForConstraintFrame, align 1, !tbaa !19, !range !10
  %tobool62 = trunc i8 %45 to i1
  br i1 %tobool62, label %if.then63, label %if.else

if.then63:                                        ; preds = %cond.end59
  %46 = bitcast i32* %indx1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #8
  %47 = load i32, i32* %i, align 4, !tbaa !28
  %add = add nsw i32 %47, 1
  %rem = srem i32 %add, 3
  store i32 %rem, i32* %indx1, align 4, !tbaa !28
  %48 = bitcast i32* %indx2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #8
  %49 = load i32, i32* %i, align 4, !tbaa !28
  %add64 = add nsw i32 %49, 2
  %rem65 = srem i32 %add64, 3
  store i32 %rem65, i32* %indx2, align 4, !tbaa !28
  %50 = bitcast i32* %rotAllowed to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #8
  store i32 1, i32* %rotAllowed, align 4, !tbaa !28
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 6
  %51 = load i32, i32* %indx1, align 4, !tbaa !28
  %arrayidx66 = getelementptr inbounds [3 x %class.btRotationalLimitMotor], [3 x %class.btRotationalLimitMotor]* %m_angularLimits, i32 0, i32 %51
  %m_currentLimit67 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %arrayidx66, i32 0, i32 14
  %52 = load i32, i32* %m_currentLimit67, align 4, !tbaa !42
  %tobool68 = icmp ne i32 %52, 0
  br i1 %tobool68, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then63
  %m_angularLimits69 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 6
  %53 = load i32, i32* %indx2, align 4, !tbaa !28
  %arrayidx70 = getelementptr inbounds [3 x %class.btRotationalLimitMotor], [3 x %class.btRotationalLimitMotor]* %m_angularLimits69, i32 0, i32 %53
  %m_currentLimit71 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %arrayidx70, i32 0, i32 14
  %54 = load i32, i32* %m_currentLimit71, align 4, !tbaa !42
  %tobool72 = icmp ne i32 %54, 0
  br i1 %tobool72, label %if.then73, label %if.end

if.then73:                                        ; preds = %land.lhs.true
  store i32 0, i32* %rotAllowed, align 4, !tbaa !28
  br label %if.end

if.end:                                           ; preds = %if.then73, %land.lhs.true, %if.then63
  %55 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %56 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %57 = load %class.btVector3*, %class.btVector3** %linVelA.addr, align 4, !tbaa !2
  %58 = load %class.btVector3*, %class.btVector3** %linVelB.addr, align 4, !tbaa !2
  %59 = load %class.btVector3*, %class.btVector3** %angVelA.addr, align 4, !tbaa !2
  %60 = load %class.btVector3*, %class.btVector3** %angVelB.addr, align 4, !tbaa !2
  %61 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %62 = load i32, i32* %row.addr, align 4, !tbaa !28
  %63 = load i32, i32* %rotAllowed, align 4, !tbaa !28
  %call74 = call i32 @_ZN23btGeneric6DofConstraint21get_limit_motor_info2EP22btRotationalLimitMotorRK11btTransformS4_RK9btVector3S7_S7_S7_PN17btTypedConstraint17btConstraintInfo2EiRS5_ii(%class.btGeneric6DofConstraint* %this1, %class.btRotationalLimitMotor* %limot, %class.btTransform* nonnull align 4 dereferenceable(64) %55, %class.btTransform* nonnull align 4 dereferenceable(64) %56, %class.btVector3* nonnull align 4 dereferenceable(16) %57, %class.btVector3* nonnull align 4 dereferenceable(16) %58, %class.btVector3* nonnull align 4 dereferenceable(16) %59, %class.btVector3* nonnull align 4 dereferenceable(16) %60, %"struct.btTypedConstraint::btConstraintInfo2"* %61, i32 %62, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, i32 0, i32 %63)
  %64 = load i32, i32* %row.addr, align 4, !tbaa !28
  %add75 = add nsw i32 %64, %call74
  store i32 %add75, i32* %row.addr, align 4, !tbaa !28
  %65 = bitcast i32* %rotAllowed to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #8
  %66 = bitcast i32* %indx2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #8
  %67 = bitcast i32* %indx1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #8
  br label %if.end78

if.else:                                          ; preds = %cond.end59
  %68 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %69 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %70 = load %class.btVector3*, %class.btVector3** %linVelA.addr, align 4, !tbaa !2
  %71 = load %class.btVector3*, %class.btVector3** %linVelB.addr, align 4, !tbaa !2
  %72 = load %class.btVector3*, %class.btVector3** %angVelA.addr, align 4, !tbaa !2
  %73 = load %class.btVector3*, %class.btVector3** %angVelB.addr, align 4, !tbaa !2
  %74 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %75 = load i32, i32* %row.addr, align 4, !tbaa !28
  %call76 = call i32 @_ZN23btGeneric6DofConstraint21get_limit_motor_info2EP22btRotationalLimitMotorRK11btTransformS4_RK9btVector3S7_S7_S7_PN17btTypedConstraint17btConstraintInfo2EiRS5_ii(%class.btGeneric6DofConstraint* %this1, %class.btRotationalLimitMotor* %limot, %class.btTransform* nonnull align 4 dereferenceable(64) %68, %class.btTransform* nonnull align 4 dereferenceable(64) %69, %class.btVector3* nonnull align 4 dereferenceable(16) %70, %class.btVector3* nonnull align 4 dereferenceable(16) %71, %class.btVector3* nonnull align 4 dereferenceable(16) %72, %class.btVector3* nonnull align 4 dereferenceable(16) %73, %"struct.btTypedConstraint::btConstraintInfo2"* %74, i32 %75, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, i32 0, i32 0)
  %76 = load i32, i32* %row.addr, align 4, !tbaa !28
  %add77 = add nsw i32 %76, %call76
  store i32 %add77, i32* %row.addr, align 4, !tbaa !28
  br label %if.end78

if.end78:                                         ; preds = %if.else, %if.end
  %77 = bitcast i32* %flags to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #8
  %78 = bitcast %class.btVector3* %axis to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %78) #8
  br label %if.end79

if.end79:                                         ; preds = %if.end78, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end79
  %79 = load i32, i32* %i, align 4, !tbaa !28
  %inc = add nsw i32 %79, 1
  store i32 %inc, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %80 = load i32, i32* %row.addr, align 4, !tbaa !28
  %81 = bitcast %class.btRotationalLimitMotor* %limot to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %81) #8
  ret i32 %80
}

define hidden void @_ZN23btGeneric6DofConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_S8_S8_(%class.btGeneric6DofConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo2"* %info, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB, %class.btVector3* nonnull align 4 dereferenceable(16) %linVelA, %class.btVector3* nonnull align 4 dereferenceable(16) %linVelB, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelA, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelB) #0 {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo2"*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %linVelA.addr = alloca %class.btVector3*, align 4
  %linVelB.addr = alloca %class.btVector3*, align 4
  %angVelA.addr = alloca %class.btVector3*, align 4
  %angVelB.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  %row = alloca i32, align 4
  %row4 = alloca i32, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4, !tbaa !2
  store %"struct.btTypedConstraint::btConstraintInfo2"* %info, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4, !tbaa !2
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4, !tbaa !2
  store %class.btVector3* %linVelA, %class.btVector3** %linVelA.addr, align 4, !tbaa !2
  store %class.btVector3* %linVelB, %class.btVector3** %linVelB.addr, align 4, !tbaa !2
  store %class.btVector3* %angVelA, %class.btVector3** %angVelA.addr, align 4, !tbaa !2
  store %class.btVector3* %angVelB, %class.btVector3** %angVelB.addr, align 4, !tbaa !2
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %1 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  call void @_ZN23btGeneric6DofConstraint19calculateTransformsERK11btTransformS2_(%class.btGeneric6DofConstraint* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %0, %class.btTransform* nonnull align 4 dereferenceable(64) %1)
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4, !tbaa !28
  %cmp = icmp slt i32 %3, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i32, i32* %i, align 4, !tbaa !28
  %call = call zeroext i1 @_ZN23btGeneric6DofConstraint21testAngularLimitMotorEi(%class.btGeneric6DofConstraint* %this1, i32 %4)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4, !tbaa !28
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_useOffsetForConstraintFrame = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 19
  %6 = load i8, i8* %m_useOffsetForConstraintFrame, align 1, !tbaa !19, !range !10
  %tobool = trunc i8 %6 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %for.end
  %7 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %8 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %9 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %10 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %11 = load %class.btVector3*, %class.btVector3** %linVelA.addr, align 4, !tbaa !2
  %12 = load %class.btVector3*, %class.btVector3** %linVelB.addr, align 4, !tbaa !2
  %13 = load %class.btVector3*, %class.btVector3** %angVelA.addr, align 4, !tbaa !2
  %14 = load %class.btVector3*, %class.btVector3** %angVelB.addr, align 4, !tbaa !2
  %call2 = call i32 @_ZN23btGeneric6DofConstraint16setAngularLimitsEPN17btTypedConstraint17btConstraintInfo2EiRK11btTransformS5_RK9btVector3S8_S8_S8_(%class.btGeneric6DofConstraint* %this1, %"struct.btTypedConstraint::btConstraintInfo2"* %8, i32 0, %class.btTransform* nonnull align 4 dereferenceable(64) %9, %class.btTransform* nonnull align 4 dereferenceable(64) %10, %class.btVector3* nonnull align 4 dereferenceable(16) %11, %class.btVector3* nonnull align 4 dereferenceable(16) %12, %class.btVector3* nonnull align 4 dereferenceable(16) %13, %class.btVector3* nonnull align 4 dereferenceable(16) %14)
  store i32 %call2, i32* %row, align 4, !tbaa !28
  %15 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %16 = load i32, i32* %row, align 4, !tbaa !28
  %17 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %18 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %19 = load %class.btVector3*, %class.btVector3** %linVelA.addr, align 4, !tbaa !2
  %20 = load %class.btVector3*, %class.btVector3** %linVelB.addr, align 4, !tbaa !2
  %21 = load %class.btVector3*, %class.btVector3** %angVelA.addr, align 4, !tbaa !2
  %22 = load %class.btVector3*, %class.btVector3** %angVelB.addr, align 4, !tbaa !2
  %call3 = call i32 @_ZN23btGeneric6DofConstraint15setLinearLimitsEPN17btTypedConstraint17btConstraintInfo2EiRK11btTransformS5_RK9btVector3S8_S8_S8_(%class.btGeneric6DofConstraint* %this1, %"struct.btTypedConstraint::btConstraintInfo2"* %15, i32 %16, %class.btTransform* nonnull align 4 dereferenceable(64) %17, %class.btTransform* nonnull align 4 dereferenceable(64) %18, %class.btVector3* nonnull align 4 dereferenceable(16) %19, %class.btVector3* nonnull align 4 dereferenceable(16) %20, %class.btVector3* nonnull align 4 dereferenceable(16) %21, %class.btVector3* nonnull align 4 dereferenceable(16) %22)
  %23 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #8
  br label %if.end

if.else:                                          ; preds = %for.end
  %24 = bitcast i32* %row4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #8
  %25 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %26 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %27 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %28 = load %class.btVector3*, %class.btVector3** %linVelA.addr, align 4, !tbaa !2
  %29 = load %class.btVector3*, %class.btVector3** %linVelB.addr, align 4, !tbaa !2
  %30 = load %class.btVector3*, %class.btVector3** %angVelA.addr, align 4, !tbaa !2
  %31 = load %class.btVector3*, %class.btVector3** %angVelB.addr, align 4, !tbaa !2
  %call5 = call i32 @_ZN23btGeneric6DofConstraint15setLinearLimitsEPN17btTypedConstraint17btConstraintInfo2EiRK11btTransformS5_RK9btVector3S8_S8_S8_(%class.btGeneric6DofConstraint* %this1, %"struct.btTypedConstraint::btConstraintInfo2"* %25, i32 0, %class.btTransform* nonnull align 4 dereferenceable(64) %26, %class.btTransform* nonnull align 4 dereferenceable(64) %27, %class.btVector3* nonnull align 4 dereferenceable(16) %28, %class.btVector3* nonnull align 4 dereferenceable(16) %29, %class.btVector3* nonnull align 4 dereferenceable(16) %30, %class.btVector3* nonnull align 4 dereferenceable(16) %31)
  store i32 %call5, i32* %row4, align 4, !tbaa !28
  %32 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %33 = load i32, i32* %row4, align 4, !tbaa !28
  %34 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %35 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %36 = load %class.btVector3*, %class.btVector3** %linVelA.addr, align 4, !tbaa !2
  %37 = load %class.btVector3*, %class.btVector3** %linVelB.addr, align 4, !tbaa !2
  %38 = load %class.btVector3*, %class.btVector3** %angVelA.addr, align 4, !tbaa !2
  %39 = load %class.btVector3*, %class.btVector3** %angVelB.addr, align 4, !tbaa !2
  %call6 = call i32 @_ZN23btGeneric6DofConstraint16setAngularLimitsEPN17btTypedConstraint17btConstraintInfo2EiRK11btTransformS5_RK9btVector3S8_S8_S8_(%class.btGeneric6DofConstraint* %this1, %"struct.btTypedConstraint::btConstraintInfo2"* %32, i32 %33, %class.btTransform* nonnull align 4 dereferenceable(64) %34, %class.btTransform* nonnull align 4 dereferenceable(64) %35, %class.btVector3* nonnull align 4 dereferenceable(16) %36, %class.btVector3* nonnull align 4 dereferenceable(16) %37, %class.btVector3* nonnull align 4 dereferenceable(16) %38, %class.btVector3* nonnull align 4 dereferenceable(16) %39)
  %40 = bitcast i32* %row4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %41 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #8
  ret void
}

define hidden i32 @_ZN23btGeneric6DofConstraint21get_limit_motor_info2EP22btRotationalLimitMotorRK11btTransformS4_RK9btVector3S7_S7_S7_PN17btTypedConstraint17btConstraintInfo2EiRS5_ii(%class.btGeneric6DofConstraint* %this, %class.btRotationalLimitMotor* %limot, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB, %class.btVector3* nonnull align 4 dereferenceable(16) %linVelA, %class.btVector3* nonnull align 4 dereferenceable(16) %linVelB, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelA, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelB, %"struct.btTypedConstraint::btConstraintInfo2"* %info, i32 %row, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1, i32 %rotational, i32 %rotAllowed) #0 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  %limot.addr = alloca %class.btRotationalLimitMotor*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %linVelA.addr = alloca %class.btVector3*, align 4
  %linVelB.addr = alloca %class.btVector3*, align 4
  %angVelA.addr = alloca %class.btVector3*, align 4
  %angVelB.addr = alloca %class.btVector3*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo2"*, align 4
  %row.addr = alloca i32, align 4
  %ax1.addr = alloca %class.btVector3*, align 4
  %rotational.addr = alloca i32, align 4
  %rotAllowed.addr = alloca i32, align 4
  %srow = alloca i32, align 4
  %powered = alloca i32, align 4
  %limit = alloca i32, align 4
  %J1 = alloca float*, align 4
  %J2 = alloca float*, align 4
  %tmpA = alloca %class.btVector3, align 4
  %tmpB = alloca %class.btVector3, align 4
  %relA = alloca %class.btVector3, align 4
  %relB = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %projB = alloca %class.btVector3, align 4
  %ref.tmp43 = alloca float, align 4
  %orthoB = alloca %class.btVector3, align 4
  %ref.tmp45 = alloca %class.btVector3, align 4
  %projA = alloca %class.btVector3, align 4
  %ref.tmp48 = alloca float, align 4
  %orthoA = alloca %class.btVector3, align 4
  %desiredOffs = alloca float, align 4
  %totalDist = alloca %class.btVector3, align 4
  %ref.tmp50 = alloca %class.btVector3, align 4
  %ref.tmp51 = alloca %class.btVector3, align 4
  %ref.tmp52 = alloca %class.btVector3, align 4
  %ref.tmp53 = alloca %class.btVector3, align 4
  %ref.tmp54 = alloca %class.btVector3, align 4
  %ref.tmp55 = alloca %class.btVector3, align 4
  %ref.tmp56 = alloca %class.btVector3, align 4
  %ref.tmp57 = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  %ltd = alloca %class.btVector3, align 4
  %c = alloca %class.btVector3, align 4
  %ref.tmp86 = alloca %class.btVector3, align 4
  %ref.tmp102 = alloca %class.btVector3, align 4
  %ref.tmp106 = alloca %class.btVector3, align 4
  %ref.tmp107 = alloca %class.btVector3, align 4
  %tag_vel = alloca float, align 4
  %mot_fact = alloca float, align 4
  %k = alloca float, align 4
  %vel = alloca float, align 4
  %newc = alloca float, align 4
  %newc232 = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4, !tbaa !2
  store %class.btRotationalLimitMotor* %limot, %class.btRotationalLimitMotor** %limot.addr, align 4, !tbaa !2
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4, !tbaa !2
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4, !tbaa !2
  store %class.btVector3* %linVelA, %class.btVector3** %linVelA.addr, align 4, !tbaa !2
  store %class.btVector3* %linVelB, %class.btVector3** %linVelB.addr, align 4, !tbaa !2
  store %class.btVector3* %angVelA, %class.btVector3** %angVelA.addr, align 4, !tbaa !2
  store %class.btVector3* %angVelB, %class.btVector3** %angVelB.addr, align 4, !tbaa !2
  store %"struct.btTypedConstraint::btConstraintInfo2"* %info, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  store i32 %row, i32* %row.addr, align 4, !tbaa !28
  store %class.btVector3* %ax1, %class.btVector3** %ax1.addr, align 4, !tbaa !2
  store i32 %rotational, i32* %rotational.addr, align 4, !tbaa !28
  store i32 %rotAllowed, i32* %rotAllowed.addr, align 4, !tbaa !28
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %0 = bitcast i32* %srow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %row.addr, align 4, !tbaa !28
  %2 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %2, i32 0, i32 6
  %3 = load i32, i32* %rowskip, align 4, !tbaa !66
  %mul = mul nsw i32 %1, %3
  store i32 %mul, i32* %srow, align 4, !tbaa !28
  %4 = bitcast i32* %powered to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btRotationalLimitMotor*, %class.btRotationalLimitMotor** %limot.addr, align 4, !tbaa !2
  %m_enableMotor = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %5, i32 0, i32 11
  %6 = load i8, i8* %m_enableMotor, align 4, !tbaa !44, !range !10
  %tobool = trunc i8 %6 to i1
  %conv = zext i1 %tobool to i32
  store i32 %conv, i32* %powered, align 4, !tbaa !28
  %7 = bitcast i32* %limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %8 = load %class.btRotationalLimitMotor*, %class.btRotationalLimitMotor** %limot.addr, align 4, !tbaa !2
  %m_currentLimit = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %8, i32 0, i32 14
  %9 = load i32, i32* %m_currentLimit, align 4, !tbaa !42
  store i32 %9, i32* %limit, align 4, !tbaa !28
  %10 = load i32, i32* %powered, align 4, !tbaa !28
  %tobool2 = icmp ne i32 %10, 0
  br i1 %tobool2, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %11 = load i32, i32* %limit, align 4, !tbaa !28
  %tobool3 = icmp ne i32 %11, 0
  br i1 %tobool3, label %if.then, label %if.else248

if.then:                                          ; preds = %lor.lhs.false, %entry
  %12 = bitcast float** %J1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  %13 = load i32, i32* %rotational.addr, align 4, !tbaa !28
  %tobool4 = icmp ne i32 %13, 0
  br i1 %tobool4, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %14 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %14, i32 0, i32 3
  %15 = load float*, float** %m_J1angularAxis, align 4, !tbaa !67
  br label %cond.end

cond.false:                                       ; preds = %if.then
  %16 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1linearAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %16, i32 0, i32 2
  %17 = load float*, float** %m_J1linearAxis, align 4, !tbaa !68
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float* [ %15, %cond.true ], [ %17, %cond.false ]
  store float* %cond, float** %J1, align 4, !tbaa !2
  %18 = bitcast float** %J2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #8
  %19 = load i32, i32* %rotational.addr, align 4, !tbaa !28
  %tobool5 = icmp ne i32 %19, 0
  br i1 %tobool5, label %cond.true6, label %cond.false7

cond.true6:                                       ; preds = %cond.end
  %20 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %20, i32 0, i32 5
  %21 = load float*, float** %m_J2angularAxis, align 4, !tbaa !69
  br label %cond.end8

cond.false7:                                      ; preds = %cond.end
  %22 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2linearAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %22, i32 0, i32 4
  %23 = load float*, float** %m_J2linearAxis, align 4, !tbaa !70
  br label %cond.end8

cond.end8:                                        ; preds = %cond.false7, %cond.true6
  %cond9 = phi float* [ %21, %cond.true6 ], [ %23, %cond.false7 ]
  store float* %cond9, float** %J2, align 4, !tbaa !2
  %24 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4, !tbaa !2
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %24)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %25 = load float, float* %arrayidx, align 4, !tbaa !24
  %26 = load float*, float** %J1, align 4, !tbaa !2
  %27 = load i32, i32* %srow, align 4, !tbaa !28
  %add = add nsw i32 %27, 0
  %arrayidx10 = getelementptr inbounds float, float* %26, i32 %add
  store float %25, float* %arrayidx10, align 4, !tbaa !24
  %28 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4, !tbaa !2
  %call11 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %28)
  %arrayidx12 = getelementptr inbounds float, float* %call11, i32 1
  %29 = load float, float* %arrayidx12, align 4, !tbaa !24
  %30 = load float*, float** %J1, align 4, !tbaa !2
  %31 = load i32, i32* %srow, align 4, !tbaa !28
  %add13 = add nsw i32 %31, 1
  %arrayidx14 = getelementptr inbounds float, float* %30, i32 %add13
  store float %29, float* %arrayidx14, align 4, !tbaa !24
  %32 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4, !tbaa !2
  %call15 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %32)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 2
  %33 = load float, float* %arrayidx16, align 4, !tbaa !24
  %34 = load float*, float** %J1, align 4, !tbaa !2
  %35 = load i32, i32* %srow, align 4, !tbaa !28
  %add17 = add nsw i32 %35, 2
  %arrayidx18 = getelementptr inbounds float, float* %34, i32 %add17
  store float %33, float* %arrayidx18, align 4, !tbaa !24
  %36 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4, !tbaa !2
  %call19 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %36)
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 0
  %37 = load float, float* %arrayidx20, align 4, !tbaa !24
  %fneg = fneg float %37
  %38 = load float*, float** %J2, align 4, !tbaa !2
  %39 = load i32, i32* %srow, align 4, !tbaa !28
  %add21 = add nsw i32 %39, 0
  %arrayidx22 = getelementptr inbounds float, float* %38, i32 %add21
  store float %fneg, float* %arrayidx22, align 4, !tbaa !24
  %40 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4, !tbaa !2
  %call23 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %40)
  %arrayidx24 = getelementptr inbounds float, float* %call23, i32 1
  %41 = load float, float* %arrayidx24, align 4, !tbaa !24
  %fneg25 = fneg float %41
  %42 = load float*, float** %J2, align 4, !tbaa !2
  %43 = load i32, i32* %srow, align 4, !tbaa !28
  %add26 = add nsw i32 %43, 1
  %arrayidx27 = getelementptr inbounds float, float* %42, i32 %add26
  store float %fneg25, float* %arrayidx27, align 4, !tbaa !24
  %44 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4, !tbaa !2
  %call28 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %44)
  %arrayidx29 = getelementptr inbounds float, float* %call28, i32 2
  %45 = load float, float* %arrayidx29, align 4, !tbaa !24
  %fneg30 = fneg float %45
  %46 = load float*, float** %J2, align 4, !tbaa !2
  %47 = load i32, i32* %srow, align 4, !tbaa !28
  %add31 = add nsw i32 %47, 2
  %arrayidx32 = getelementptr inbounds float, float* %46, i32 %add31
  store float %fneg30, float* %arrayidx32, align 4, !tbaa !24
  %48 = load i32, i32* %rotational.addr, align 4, !tbaa !28
  %tobool33 = icmp ne i32 %48, 0
  br i1 %tobool33, label %if.end124, label %if.then34

if.then34:                                        ; preds = %cond.end8
  %m_useOffsetForConstraintFrame = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 19
  %49 = load i8, i8* %m_useOffsetForConstraintFrame, align 1, !tbaa !19, !range !10
  %tobool35 = trunc i8 %49 to i1
  br i1 %tobool35, label %if.then36, label %if.else

if.then36:                                        ; preds = %if.then34
  %50 = bitcast %class.btVector3* %tmpA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %50) #8
  %call37 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %tmpA)
  %51 = bitcast %class.btVector3* %tmpB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %51) #8
  %call38 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %tmpB)
  %52 = bitcast %class.btVector3* %relA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %52) #8
  %call39 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %relA)
  %53 = bitcast %class.btVector3* %relB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %53) #8
  %call40 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %relB)
  %54 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %54) #8
  %m_calculatedTransformB = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 9
  %call41 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_calculatedTransformB)
  %55 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %call42 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %55)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %call41, %class.btVector3* nonnull align 4 dereferenceable(16) %call42)
  %56 = bitcast %class.btVector3* %relB to i8*
  %57 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %56, i8* align 4 %57, i32 16, i1 false), !tbaa.struct !22
  %58 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %58) #8
  %59 = bitcast %class.btVector3* %projB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %59) #8
  %60 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4, !tbaa !2
  %61 = bitcast float* %ref.tmp43 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %61) #8
  %62 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4, !tbaa !2
  %call44 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %relB, %class.btVector3* nonnull align 4 dereferenceable(16) %62)
  store float %call44, float* %ref.tmp43, align 4, !tbaa !24
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %projB, %class.btVector3* nonnull align 4 dereferenceable(16) %60, float* nonnull align 4 dereferenceable(4) %ref.tmp43)
  %63 = bitcast float* %ref.tmp43 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #8
  %64 = bitcast %class.btVector3* %orthoB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %64) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %orthoB, %class.btVector3* nonnull align 4 dereferenceable(16) %relB, %class.btVector3* nonnull align 4 dereferenceable(16) %projB)
  %65 = bitcast %class.btVector3* %ref.tmp45 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %65) #8
  %m_calculatedTransformA = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 8
  %call46 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_calculatedTransformA)
  %66 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %call47 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %66)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp45, %class.btVector3* nonnull align 4 dereferenceable(16) %call46, %class.btVector3* nonnull align 4 dereferenceable(16) %call47)
  %67 = bitcast %class.btVector3* %relA to i8*
  %68 = bitcast %class.btVector3* %ref.tmp45 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %67, i8* align 4 %68, i32 16, i1 false), !tbaa.struct !22
  %69 = bitcast %class.btVector3* %ref.tmp45 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %69) #8
  %70 = bitcast %class.btVector3* %projA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %70) #8
  %71 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4, !tbaa !2
  %72 = bitcast float* %ref.tmp48 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %72) #8
  %73 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4, !tbaa !2
  %call49 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %relA, %class.btVector3* nonnull align 4 dereferenceable(16) %73)
  store float %call49, float* %ref.tmp48, align 4, !tbaa !24
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %projA, %class.btVector3* nonnull align 4 dereferenceable(16) %71, float* nonnull align 4 dereferenceable(4) %ref.tmp48)
  %74 = bitcast float* %ref.tmp48 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #8
  %75 = bitcast %class.btVector3* %orthoA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %75) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %orthoA, %class.btVector3* nonnull align 4 dereferenceable(16) %relA, %class.btVector3* nonnull align 4 dereferenceable(16) %projA)
  %76 = bitcast float* %desiredOffs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #8
  %77 = load %class.btRotationalLimitMotor*, %class.btRotationalLimitMotor** %limot.addr, align 4, !tbaa !2
  %m_currentPosition = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %77, i32 0, i32 13
  %78 = load float, float* %m_currentPosition, align 4, !tbaa !57
  %79 = load %class.btRotationalLimitMotor*, %class.btRotationalLimitMotor** %limot.addr, align 4, !tbaa !2
  %m_currentLimitError = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %79, i32 0, i32 12
  %80 = load float, float* %m_currentLimitError, align 4, !tbaa !43
  %sub = fsub float %78, %80
  store float %sub, float* %desiredOffs, align 4, !tbaa !24
  %81 = bitcast %class.btVector3* %totalDist to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %81) #8
  %82 = bitcast %class.btVector3* %ref.tmp50 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %82) #8
  %83 = bitcast %class.btVector3* %ref.tmp51 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %83) #8
  %84 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp51, %class.btVector3* nonnull align 4 dereferenceable(16) %84, float* nonnull align 4 dereferenceable(4) %desiredOffs)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp50, %class.btVector3* nonnull align 4 dereferenceable(16) %projA, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp51)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %totalDist, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp50, %class.btVector3* nonnull align 4 dereferenceable(16) %projB)
  %85 = bitcast %class.btVector3* %ref.tmp51 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %85) #8
  %86 = bitcast %class.btVector3* %ref.tmp50 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %86) #8
  %87 = bitcast %class.btVector3* %ref.tmp52 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %87) #8
  %88 = bitcast %class.btVector3* %ref.tmp53 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %88) #8
  %m_factA = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 13
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp53, %class.btVector3* nonnull align 4 dereferenceable(16) %totalDist, float* nonnull align 4 dereferenceable(4) %m_factA)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp52, %class.btVector3* nonnull align 4 dereferenceable(16) %orthoA, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp53)
  %89 = bitcast %class.btVector3* %relA to i8*
  %90 = bitcast %class.btVector3* %ref.tmp52 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %89, i8* align 4 %90, i32 16, i1 false), !tbaa.struct !22
  %91 = bitcast %class.btVector3* %ref.tmp53 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %91) #8
  %92 = bitcast %class.btVector3* %ref.tmp52 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %92) #8
  %93 = bitcast %class.btVector3* %ref.tmp54 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %93) #8
  %94 = bitcast %class.btVector3* %ref.tmp55 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %94) #8
  %m_factB = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 14
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp55, %class.btVector3* nonnull align 4 dereferenceable(16) %totalDist, float* nonnull align 4 dereferenceable(4) %m_factB)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp54, %class.btVector3* nonnull align 4 dereferenceable(16) %orthoB, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp55)
  %95 = bitcast %class.btVector3* %relB to i8*
  %96 = bitcast %class.btVector3* %ref.tmp54 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %95, i8* align 4 %96, i32 16, i1 false), !tbaa.struct !22
  %97 = bitcast %class.btVector3* %ref.tmp55 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %97) #8
  %98 = bitcast %class.btVector3* %ref.tmp54 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %98) #8
  %99 = bitcast %class.btVector3* %ref.tmp56 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %99) #8
  %100 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4, !tbaa !2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp56, %class.btVector3* %relA, %class.btVector3* nonnull align 4 dereferenceable(16) %100)
  %101 = bitcast %class.btVector3* %tmpA to i8*
  %102 = bitcast %class.btVector3* %ref.tmp56 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %101, i8* align 4 %102, i32 16, i1 false), !tbaa.struct !22
  %103 = bitcast %class.btVector3* %ref.tmp56 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %103) #8
  %104 = bitcast %class.btVector3* %ref.tmp57 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %104) #8
  %105 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4, !tbaa !2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp57, %class.btVector3* %relB, %class.btVector3* nonnull align 4 dereferenceable(16) %105)
  %106 = bitcast %class.btVector3* %tmpB to i8*
  %107 = bitcast %class.btVector3* %ref.tmp57 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %106, i8* align 4 %107, i32 16, i1 false), !tbaa.struct !22
  %108 = bitcast %class.btVector3* %ref.tmp57 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %108) #8
  %m_hasStaticBody = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 15
  %109 = load i8, i8* %m_hasStaticBody, align 4, !tbaa !52, !range !10
  %tobool58 = trunc i8 %109 to i1
  br i1 %tobool58, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then36
  %110 = load i32, i32* %rotAllowed.addr, align 4, !tbaa !28
  %tobool59 = icmp ne i32 %110, 0
  br i1 %tobool59, label %if.end, label %if.then60

if.then60:                                        ; preds = %land.lhs.true
  %m_factA61 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 13
  %call62 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %tmpA, float* nonnull align 4 dereferenceable(4) %m_factA61)
  %m_factB63 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 14
  %call64 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %tmpB, float* nonnull align 4 dereferenceable(4) %m_factB63)
  br label %if.end

if.end:                                           ; preds = %if.then60, %land.lhs.true, %if.then36
  %111 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %111) #8
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %112 = load i32, i32* %i, align 4, !tbaa !28
  %cmp = icmp slt i32 %112, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call65 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpA)
  %113 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx66 = getelementptr inbounds float, float* %call65, i32 %113
  %114 = load float, float* %arrayidx66, align 4, !tbaa !24
  %115 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis67 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %115, i32 0, i32 3
  %116 = load float*, float** %m_J1angularAxis67, align 4, !tbaa !67
  %117 = load i32, i32* %srow, align 4, !tbaa !28
  %118 = load i32, i32* %i, align 4, !tbaa !28
  %add68 = add nsw i32 %117, %118
  %arrayidx69 = getelementptr inbounds float, float* %116, i32 %add68
  store float %114, float* %arrayidx69, align 4, !tbaa !24
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %119 = load i32, i32* %i, align 4, !tbaa !28
  %inc = add nsw i32 %119, 1
  store i32 %inc, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond70

for.cond70:                                       ; preds = %for.inc79, %for.end
  %120 = load i32, i32* %i, align 4, !tbaa !28
  %cmp71 = icmp slt i32 %120, 3
  br i1 %cmp71, label %for.body72, label %for.end81

for.body72:                                       ; preds = %for.cond70
  %call73 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpB)
  %121 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx74 = getelementptr inbounds float, float* %call73, i32 %121
  %122 = load float, float* %arrayidx74, align 4, !tbaa !24
  %fneg75 = fneg float %122
  %123 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis76 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %123, i32 0, i32 5
  %124 = load float*, float** %m_J2angularAxis76, align 4, !tbaa !69
  %125 = load i32, i32* %srow, align 4, !tbaa !28
  %126 = load i32, i32* %i, align 4, !tbaa !28
  %add77 = add nsw i32 %125, %126
  %arrayidx78 = getelementptr inbounds float, float* %124, i32 %add77
  store float %fneg75, float* %arrayidx78, align 4, !tbaa !24
  br label %for.inc79

for.inc79:                                        ; preds = %for.body72
  %127 = load i32, i32* %i, align 4, !tbaa !28
  %inc80 = add nsw i32 %127, 1
  store i32 %inc80, i32* %i, align 4, !tbaa !28
  br label %for.cond70

for.end81:                                        ; preds = %for.cond70
  %128 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #8
  %129 = bitcast %class.btVector3* %totalDist to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %129) #8
  %130 = bitcast float* %desiredOffs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #8
  %131 = bitcast %class.btVector3* %orthoA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %131) #8
  %132 = bitcast %class.btVector3* %projA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %132) #8
  %133 = bitcast %class.btVector3* %orthoB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %133) #8
  %134 = bitcast %class.btVector3* %projB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %134) #8
  %135 = bitcast %class.btVector3* %relB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %135) #8
  %136 = bitcast %class.btVector3* %relA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %136) #8
  %137 = bitcast %class.btVector3* %tmpB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %137) #8
  %138 = bitcast %class.btVector3* %tmpA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %138) #8
  br label %if.end123

if.else:                                          ; preds = %if.then34
  %139 = bitcast %class.btVector3* %ltd to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %139) #8
  %call82 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ltd)
  %140 = bitcast %class.btVector3* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %140) #8
  %m_calculatedTransformB83 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 9
  %call84 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_calculatedTransformB83)
  %141 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %call85 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %141)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %c, %class.btVector3* nonnull align 4 dereferenceable(16) %call84, %class.btVector3* nonnull align 4 dereferenceable(16) %call85)
  %142 = bitcast %class.btVector3* %ref.tmp86 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %142) #8
  %143 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4, !tbaa !2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp86, %class.btVector3* %c, %class.btVector3* nonnull align 4 dereferenceable(16) %143)
  %144 = bitcast %class.btVector3* %ltd to i8*
  %145 = bitcast %class.btVector3* %ref.tmp86 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %144, i8* align 4 %145, i32 16, i1 false), !tbaa.struct !22
  %146 = bitcast %class.btVector3* %ref.tmp86 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %146) #8
  %call87 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ltd)
  %arrayidx88 = getelementptr inbounds float, float* %call87, i32 0
  %147 = load float, float* %arrayidx88, align 4, !tbaa !24
  %148 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis89 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %148, i32 0, i32 3
  %149 = load float*, float** %m_J1angularAxis89, align 4, !tbaa !67
  %150 = load i32, i32* %srow, align 4, !tbaa !28
  %add90 = add nsw i32 %150, 0
  %arrayidx91 = getelementptr inbounds float, float* %149, i32 %add90
  store float %147, float* %arrayidx91, align 4, !tbaa !24
  %call92 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ltd)
  %arrayidx93 = getelementptr inbounds float, float* %call92, i32 1
  %151 = load float, float* %arrayidx93, align 4, !tbaa !24
  %152 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis94 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %152, i32 0, i32 3
  %153 = load float*, float** %m_J1angularAxis94, align 4, !tbaa !67
  %154 = load i32, i32* %srow, align 4, !tbaa !28
  %add95 = add nsw i32 %154, 1
  %arrayidx96 = getelementptr inbounds float, float* %153, i32 %add95
  store float %151, float* %arrayidx96, align 4, !tbaa !24
  %call97 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ltd)
  %arrayidx98 = getelementptr inbounds float, float* %call97, i32 2
  %155 = load float, float* %arrayidx98, align 4, !tbaa !24
  %156 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis99 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %156, i32 0, i32 3
  %157 = load float*, float** %m_J1angularAxis99, align 4, !tbaa !67
  %158 = load i32, i32* %srow, align 4, !tbaa !28
  %add100 = add nsw i32 %158, 2
  %arrayidx101 = getelementptr inbounds float, float* %157, i32 %add100
  store float %155, float* %arrayidx101, align 4, !tbaa !24
  %159 = bitcast %class.btVector3* %ref.tmp102 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %159) #8
  %m_calculatedTransformB103 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 9
  %call104 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_calculatedTransformB103)
  %160 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %call105 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %160)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp102, %class.btVector3* nonnull align 4 dereferenceable(16) %call104, %class.btVector3* nonnull align 4 dereferenceable(16) %call105)
  %161 = bitcast %class.btVector3* %c to i8*
  %162 = bitcast %class.btVector3* %ref.tmp102 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %161, i8* align 4 %162, i32 16, i1 false), !tbaa.struct !22
  %163 = bitcast %class.btVector3* %ref.tmp102 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %163) #8
  %164 = bitcast %class.btVector3* %ref.tmp106 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %164) #8
  %165 = bitcast %class.btVector3* %ref.tmp107 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %165) #8
  %166 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4, !tbaa !2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp107, %class.btVector3* %c, %class.btVector3* nonnull align 4 dereferenceable(16) %166)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp106, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp107)
  %167 = bitcast %class.btVector3* %ltd to i8*
  %168 = bitcast %class.btVector3* %ref.tmp106 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %167, i8* align 4 %168, i32 16, i1 false), !tbaa.struct !22
  %169 = bitcast %class.btVector3* %ref.tmp107 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %169) #8
  %170 = bitcast %class.btVector3* %ref.tmp106 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %170) #8
  %call108 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ltd)
  %arrayidx109 = getelementptr inbounds float, float* %call108, i32 0
  %171 = load float, float* %arrayidx109, align 4, !tbaa !24
  %172 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis110 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %172, i32 0, i32 5
  %173 = load float*, float** %m_J2angularAxis110, align 4, !tbaa !69
  %174 = load i32, i32* %srow, align 4, !tbaa !28
  %add111 = add nsw i32 %174, 0
  %arrayidx112 = getelementptr inbounds float, float* %173, i32 %add111
  store float %171, float* %arrayidx112, align 4, !tbaa !24
  %call113 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ltd)
  %arrayidx114 = getelementptr inbounds float, float* %call113, i32 1
  %175 = load float, float* %arrayidx114, align 4, !tbaa !24
  %176 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis115 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %176, i32 0, i32 5
  %177 = load float*, float** %m_J2angularAxis115, align 4, !tbaa !69
  %178 = load i32, i32* %srow, align 4, !tbaa !28
  %add116 = add nsw i32 %178, 1
  %arrayidx117 = getelementptr inbounds float, float* %177, i32 %add116
  store float %175, float* %arrayidx117, align 4, !tbaa !24
  %call118 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ltd)
  %arrayidx119 = getelementptr inbounds float, float* %call118, i32 2
  %179 = load float, float* %arrayidx119, align 4, !tbaa !24
  %180 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis120 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %180, i32 0, i32 5
  %181 = load float*, float** %m_J2angularAxis120, align 4, !tbaa !69
  %182 = load i32, i32* %srow, align 4, !tbaa !28
  %add121 = add nsw i32 %182, 2
  %arrayidx122 = getelementptr inbounds float, float* %181, i32 %add121
  store float %179, float* %arrayidx122, align 4, !tbaa !24
  %183 = bitcast %class.btVector3* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %183) #8
  %184 = bitcast %class.btVector3* %ltd to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %184) #8
  br label %if.end123

if.end123:                                        ; preds = %if.else, %for.end81
  br label %if.end124

if.end124:                                        ; preds = %if.end123, %cond.end8
  %185 = load i32, i32* %limit, align 4, !tbaa !28
  %tobool125 = icmp ne i32 %185, 0
  br i1 %tobool125, label %land.lhs.true126, label %if.end129

land.lhs.true126:                                 ; preds = %if.end124
  %186 = load %class.btRotationalLimitMotor*, %class.btRotationalLimitMotor** %limot.addr, align 4, !tbaa !2
  %m_loLimit = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %186, i32 0, i32 0
  %187 = load float, float* %m_loLimit, align 4, !tbaa !34
  %188 = load %class.btRotationalLimitMotor*, %class.btRotationalLimitMotor** %limot.addr, align 4, !tbaa !2
  %m_hiLimit = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %188, i32 0, i32 1
  %189 = load float, float* %m_hiLimit, align 4, !tbaa !35
  %cmp127 = fcmp oeq float %187, %189
  br i1 %cmp127, label %if.then128, label %if.end129

if.then128:                                       ; preds = %land.lhs.true126
  store i32 0, i32* %powered, align 4, !tbaa !28
  br label %if.end129

if.end129:                                        ; preds = %if.then128, %land.lhs.true126, %if.end124
  %190 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %190, i32 0, i32 7
  %191 = load float*, float** %m_constraintError, align 4, !tbaa !71
  %192 = load i32, i32* %srow, align 4, !tbaa !28
  %arrayidx130 = getelementptr inbounds float, float* %191, i32 %192
  store float 0.000000e+00, float* %arrayidx130, align 4, !tbaa !24
  %193 = load i32, i32* %powered, align 4, !tbaa !28
  %tobool131 = icmp ne i32 %193, 0
  br i1 %tobool131, label %if.then132, label %if.end158

if.then132:                                       ; preds = %if.end129
  %194 = load %class.btRotationalLimitMotor*, %class.btRotationalLimitMotor** %limot.addr, align 4, !tbaa !2
  %m_normalCFM = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %194, i32 0, i32 7
  %195 = load float, float* %m_normalCFM, align 4, !tbaa !36
  %196 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %cfm = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %196, i32 0, i32 8
  %197 = load float*, float** %cfm, align 4, !tbaa !61
  %198 = load i32, i32* %srow, align 4, !tbaa !28
  %arrayidx133 = getelementptr inbounds float, float* %197, i32 %198
  store float %195, float* %arrayidx133, align 4, !tbaa !24
  %199 = load i32, i32* %limit, align 4, !tbaa !28
  %tobool134 = icmp ne i32 %199, 0
  br i1 %tobool134, label %if.end157, label %if.then135

if.then135:                                       ; preds = %if.then132
  %200 = bitcast float* %tag_vel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %200) #8
  %201 = load i32, i32* %rotational.addr, align 4, !tbaa !28
  %tobool136 = icmp ne i32 %201, 0
  br i1 %tobool136, label %cond.true137, label %cond.false138

cond.true137:                                     ; preds = %if.then135
  %202 = load %class.btRotationalLimitMotor*, %class.btRotationalLimitMotor** %limot.addr, align 4, !tbaa !2
  %m_targetVelocity = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %202, i32 0, i32 2
  %203 = load float, float* %m_targetVelocity, align 4, !tbaa !31
  br label %cond.end141

cond.false138:                                    ; preds = %if.then135
  %204 = load %class.btRotationalLimitMotor*, %class.btRotationalLimitMotor** %limot.addr, align 4, !tbaa !2
  %m_targetVelocity139 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %204, i32 0, i32 2
  %205 = load float, float* %m_targetVelocity139, align 4, !tbaa !31
  %fneg140 = fneg float %205
  br label %cond.end141

cond.end141:                                      ; preds = %cond.false138, %cond.true137
  %cond142 = phi float [ %203, %cond.true137 ], [ %fneg140, %cond.false138 ]
  store float %cond142, float* %tag_vel, align 4, !tbaa !24
  %206 = bitcast float* %mot_fact to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %206) #8
  %207 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %208 = load %class.btRotationalLimitMotor*, %class.btRotationalLimitMotor** %limot.addr, align 4, !tbaa !2
  %m_currentPosition143 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %208, i32 0, i32 13
  %209 = load float, float* %m_currentPosition143, align 4, !tbaa !57
  %210 = load %class.btRotationalLimitMotor*, %class.btRotationalLimitMotor** %limot.addr, align 4, !tbaa !2
  %m_loLimit144 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %210, i32 0, i32 0
  %211 = load float, float* %m_loLimit144, align 4, !tbaa !34
  %212 = load %class.btRotationalLimitMotor*, %class.btRotationalLimitMotor** %limot.addr, align 4, !tbaa !2
  %m_hiLimit145 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %212, i32 0, i32 1
  %213 = load float, float* %m_hiLimit145, align 4, !tbaa !35
  %214 = load float, float* %tag_vel, align 4, !tbaa !24
  %215 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %fps = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %215, i32 0, i32 0
  %216 = load float, float* %fps, align 4, !tbaa !72
  %217 = load %class.btRotationalLimitMotor*, %class.btRotationalLimitMotor** %limot.addr, align 4, !tbaa !2
  %m_stopERP = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %217, i32 0, i32 8
  %218 = load float, float* %m_stopERP, align 4, !tbaa !37
  %mul146 = fmul float %216, %218
  %call147 = call float @_ZN17btTypedConstraint14getMotorFactorEfffff(%class.btTypedConstraint* %207, float %209, float %211, float %213, float %214, float %mul146)
  store float %call147, float* %mot_fact, align 4, !tbaa !24
  %219 = load float, float* %mot_fact, align 4, !tbaa !24
  %220 = load %class.btRotationalLimitMotor*, %class.btRotationalLimitMotor** %limot.addr, align 4, !tbaa !2
  %m_targetVelocity148 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %220, i32 0, i32 2
  %221 = load float, float* %m_targetVelocity148, align 4, !tbaa !31
  %mul149 = fmul float %219, %221
  %222 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError150 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %222, i32 0, i32 7
  %223 = load float*, float** %m_constraintError150, align 4, !tbaa !71
  %224 = load i32, i32* %srow, align 4, !tbaa !28
  %arrayidx151 = getelementptr inbounds float, float* %223, i32 %224
  %225 = load float, float* %arrayidx151, align 4, !tbaa !24
  %add152 = fadd float %225, %mul149
  store float %add152, float* %arrayidx151, align 4, !tbaa !24
  %226 = load %class.btRotationalLimitMotor*, %class.btRotationalLimitMotor** %limot.addr, align 4, !tbaa !2
  %m_maxMotorForce = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %226, i32 0, i32 3
  %227 = load float, float* %m_maxMotorForce, align 4, !tbaa !32
  %fneg153 = fneg float %227
  %228 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_lowerLimit = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %228, i32 0, i32 9
  %229 = load float*, float** %m_lowerLimit, align 4, !tbaa !73
  %230 = load i32, i32* %srow, align 4, !tbaa !28
  %arrayidx154 = getelementptr inbounds float, float* %229, i32 %230
  store float %fneg153, float* %arrayidx154, align 4, !tbaa !24
  %231 = load %class.btRotationalLimitMotor*, %class.btRotationalLimitMotor** %limot.addr, align 4, !tbaa !2
  %m_maxMotorForce155 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %231, i32 0, i32 3
  %232 = load float, float* %m_maxMotorForce155, align 4, !tbaa !32
  %233 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_upperLimit = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %233, i32 0, i32 10
  %234 = load float*, float** %m_upperLimit, align 4, !tbaa !74
  %235 = load i32, i32* %srow, align 4, !tbaa !28
  %arrayidx156 = getelementptr inbounds float, float* %234, i32 %235
  store float %232, float* %arrayidx156, align 4, !tbaa !24
  %236 = bitcast float* %mot_fact to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %236) #8
  %237 = bitcast float* %tag_vel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %237) #8
  br label %if.end157

if.end157:                                        ; preds = %cond.end141, %if.then132
  br label %if.end158

if.end158:                                        ; preds = %if.end157, %if.end129
  %238 = load i32, i32* %limit, align 4, !tbaa !28
  %tobool159 = icmp ne i32 %238, 0
  br i1 %tobool159, label %if.then160, label %if.end247

if.then160:                                       ; preds = %if.end158
  %239 = bitcast float* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %239) #8
  %240 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %fps161 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %240, i32 0, i32 0
  %241 = load float, float* %fps161, align 4, !tbaa !72
  %242 = load %class.btRotationalLimitMotor*, %class.btRotationalLimitMotor** %limot.addr, align 4, !tbaa !2
  %m_stopERP162 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %242, i32 0, i32 8
  %243 = load float, float* %m_stopERP162, align 4, !tbaa !37
  %mul163 = fmul float %241, %243
  store float %mul163, float* %k, align 4, !tbaa !24
  %244 = load i32, i32* %rotational.addr, align 4, !tbaa !28
  %tobool164 = icmp ne i32 %244, 0
  br i1 %tobool164, label %if.else171, label %if.then165

if.then165:                                       ; preds = %if.then160
  %245 = load float, float* %k, align 4, !tbaa !24
  %246 = load %class.btRotationalLimitMotor*, %class.btRotationalLimitMotor** %limot.addr, align 4, !tbaa !2
  %m_currentLimitError166 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %246, i32 0, i32 12
  %247 = load float, float* %m_currentLimitError166, align 4, !tbaa !43
  %mul167 = fmul float %245, %247
  %248 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError168 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %248, i32 0, i32 7
  %249 = load float*, float** %m_constraintError168, align 4, !tbaa !71
  %250 = load i32, i32* %srow, align 4, !tbaa !28
  %arrayidx169 = getelementptr inbounds float, float* %249, i32 %250
  %251 = load float, float* %arrayidx169, align 4, !tbaa !24
  %add170 = fadd float %251, %mul167
  store float %add170, float* %arrayidx169, align 4, !tbaa !24
  br label %if.end178

if.else171:                                       ; preds = %if.then160
  %252 = load float, float* %k, align 4, !tbaa !24
  %fneg172 = fneg float %252
  %253 = load %class.btRotationalLimitMotor*, %class.btRotationalLimitMotor** %limot.addr, align 4, !tbaa !2
  %m_currentLimitError173 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %253, i32 0, i32 12
  %254 = load float, float* %m_currentLimitError173, align 4, !tbaa !43
  %mul174 = fmul float %fneg172, %254
  %255 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError175 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %255, i32 0, i32 7
  %256 = load float*, float** %m_constraintError175, align 4, !tbaa !71
  %257 = load i32, i32* %srow, align 4, !tbaa !28
  %arrayidx176 = getelementptr inbounds float, float* %256, i32 %257
  %258 = load float, float* %arrayidx176, align 4, !tbaa !24
  %add177 = fadd float %258, %mul174
  store float %add177, float* %arrayidx176, align 4, !tbaa !24
  br label %if.end178

if.end178:                                        ; preds = %if.else171, %if.then165
  %259 = load %class.btRotationalLimitMotor*, %class.btRotationalLimitMotor** %limot.addr, align 4, !tbaa !2
  %m_stopCFM = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %259, i32 0, i32 9
  %260 = load float, float* %m_stopCFM, align 4, !tbaa !38
  %261 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %cfm179 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %261, i32 0, i32 8
  %262 = load float*, float** %cfm179, align 4, !tbaa !61
  %263 = load i32, i32* %srow, align 4, !tbaa !28
  %arrayidx180 = getelementptr inbounds float, float* %262, i32 %263
  store float %260, float* %arrayidx180, align 4, !tbaa !24
  %264 = load %class.btRotationalLimitMotor*, %class.btRotationalLimitMotor** %limot.addr, align 4, !tbaa !2
  %m_loLimit181 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %264, i32 0, i32 0
  %265 = load float, float* %m_loLimit181, align 4, !tbaa !34
  %266 = load %class.btRotationalLimitMotor*, %class.btRotationalLimitMotor** %limot.addr, align 4, !tbaa !2
  %m_hiLimit182 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %266, i32 0, i32 1
  %267 = load float, float* %m_hiLimit182, align 4, !tbaa !35
  %cmp183 = fcmp oeq float %265, %267
  br i1 %cmp183, label %if.then184, label %if.else189

if.then184:                                       ; preds = %if.end178
  %268 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_lowerLimit185 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %268, i32 0, i32 9
  %269 = load float*, float** %m_lowerLimit185, align 4, !tbaa !73
  %270 = load i32, i32* %srow, align 4, !tbaa !28
  %arrayidx186 = getelementptr inbounds float, float* %269, i32 %270
  store float 0xC7EFFFFFE0000000, float* %arrayidx186, align 4, !tbaa !24
  %271 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_upperLimit187 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %271, i32 0, i32 10
  %272 = load float*, float** %m_upperLimit187, align 4, !tbaa !74
  %273 = load i32, i32* %srow, align 4, !tbaa !28
  %arrayidx188 = getelementptr inbounds float, float* %272, i32 %273
  store float 0x47EFFFFFE0000000, float* %arrayidx188, align 4, !tbaa !24
  br label %if.end246

if.else189:                                       ; preds = %if.end178
  %274 = load i32, i32* %limit, align 4, !tbaa !28
  %cmp190 = icmp eq i32 %274, 1
  br i1 %cmp190, label %if.then191, label %if.else196

if.then191:                                       ; preds = %if.else189
  %275 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_lowerLimit192 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %275, i32 0, i32 9
  %276 = load float*, float** %m_lowerLimit192, align 4, !tbaa !73
  %277 = load i32, i32* %srow, align 4, !tbaa !28
  %arrayidx193 = getelementptr inbounds float, float* %276, i32 %277
  store float 0.000000e+00, float* %arrayidx193, align 4, !tbaa !24
  %278 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_upperLimit194 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %278, i32 0, i32 10
  %279 = load float*, float** %m_upperLimit194, align 4, !tbaa !74
  %280 = load i32, i32* %srow, align 4, !tbaa !28
  %arrayidx195 = getelementptr inbounds float, float* %279, i32 %280
  store float 0x47EFFFFFE0000000, float* %arrayidx195, align 4, !tbaa !24
  br label %if.end201

if.else196:                                       ; preds = %if.else189
  %281 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_lowerLimit197 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %281, i32 0, i32 9
  %282 = load float*, float** %m_lowerLimit197, align 4, !tbaa !73
  %283 = load i32, i32* %srow, align 4, !tbaa !28
  %arrayidx198 = getelementptr inbounds float, float* %282, i32 %283
  store float 0xC7EFFFFFE0000000, float* %arrayidx198, align 4, !tbaa !24
  %284 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_upperLimit199 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %284, i32 0, i32 10
  %285 = load float*, float** %m_upperLimit199, align 4, !tbaa !74
  %286 = load i32, i32* %srow, align 4, !tbaa !28
  %arrayidx200 = getelementptr inbounds float, float* %285, i32 %286
  store float 0.000000e+00, float* %arrayidx200, align 4, !tbaa !24
  br label %if.end201

if.end201:                                        ; preds = %if.else196, %if.then191
  %287 = load %class.btRotationalLimitMotor*, %class.btRotationalLimitMotor** %limot.addr, align 4, !tbaa !2
  %m_bounce = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %287, i32 0, i32 10
  %288 = load float, float* %m_bounce, align 4, !tbaa !39
  %cmp202 = fcmp ogt float %288, 0.000000e+00
  br i1 %cmp202, label %if.then203, label %if.end245

if.then203:                                       ; preds = %if.end201
  %289 = bitcast float* %vel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %289) #8
  %290 = load i32, i32* %rotational.addr, align 4, !tbaa !28
  %tobool204 = icmp ne i32 %290, 0
  br i1 %tobool204, label %if.then205, label %if.else209

if.then205:                                       ; preds = %if.then203
  %291 = load %class.btVector3*, %class.btVector3** %angVelA.addr, align 4, !tbaa !2
  %292 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4, !tbaa !2
  %call206 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %291, %class.btVector3* nonnull align 4 dereferenceable(16) %292)
  store float %call206, float* %vel, align 4, !tbaa !24
  %293 = load %class.btVector3*, %class.btVector3** %angVelB.addr, align 4, !tbaa !2
  %294 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4, !tbaa !2
  %call207 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %293, %class.btVector3* nonnull align 4 dereferenceable(16) %294)
  %295 = load float, float* %vel, align 4, !tbaa !24
  %sub208 = fsub float %295, %call207
  store float %sub208, float* %vel, align 4, !tbaa !24
  br label %if.end213

if.else209:                                       ; preds = %if.then203
  %296 = load %class.btVector3*, %class.btVector3** %linVelA.addr, align 4, !tbaa !2
  %297 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4, !tbaa !2
  %call210 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %296, %class.btVector3* nonnull align 4 dereferenceable(16) %297)
  store float %call210, float* %vel, align 4, !tbaa !24
  %298 = load %class.btVector3*, %class.btVector3** %linVelB.addr, align 4, !tbaa !2
  %299 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4, !tbaa !2
  %call211 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %298, %class.btVector3* nonnull align 4 dereferenceable(16) %299)
  %300 = load float, float* %vel, align 4, !tbaa !24
  %sub212 = fsub float %300, %call211
  store float %sub212, float* %vel, align 4, !tbaa !24
  br label %if.end213

if.end213:                                        ; preds = %if.else209, %if.then205
  %301 = load i32, i32* %limit, align 4, !tbaa !28
  %cmp214 = icmp eq i32 %301, 1
  br i1 %cmp214, label %if.then215, label %if.else229

if.then215:                                       ; preds = %if.end213
  %302 = load float, float* %vel, align 4, !tbaa !24
  %cmp216 = fcmp olt float %302, 0.000000e+00
  br i1 %cmp216, label %if.then217, label %if.end228

if.then217:                                       ; preds = %if.then215
  %303 = bitcast float* %newc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %303) #8
  %304 = load %class.btRotationalLimitMotor*, %class.btRotationalLimitMotor** %limot.addr, align 4, !tbaa !2
  %m_bounce218 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %304, i32 0, i32 10
  %305 = load float, float* %m_bounce218, align 4, !tbaa !39
  %fneg219 = fneg float %305
  %306 = load float, float* %vel, align 4, !tbaa !24
  %mul220 = fmul float %fneg219, %306
  store float %mul220, float* %newc, align 4, !tbaa !24
  %307 = load float, float* %newc, align 4, !tbaa !24
  %308 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError221 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %308, i32 0, i32 7
  %309 = load float*, float** %m_constraintError221, align 4, !tbaa !71
  %310 = load i32, i32* %srow, align 4, !tbaa !28
  %arrayidx222 = getelementptr inbounds float, float* %309, i32 %310
  %311 = load float, float* %arrayidx222, align 4, !tbaa !24
  %cmp223 = fcmp ogt float %307, %311
  br i1 %cmp223, label %if.then224, label %if.end227

if.then224:                                       ; preds = %if.then217
  %312 = load float, float* %newc, align 4, !tbaa !24
  %313 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError225 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %313, i32 0, i32 7
  %314 = load float*, float** %m_constraintError225, align 4, !tbaa !71
  %315 = load i32, i32* %srow, align 4, !tbaa !28
  %arrayidx226 = getelementptr inbounds float, float* %314, i32 %315
  store float %312, float* %arrayidx226, align 4, !tbaa !24
  br label %if.end227

if.end227:                                        ; preds = %if.then224, %if.then217
  %316 = bitcast float* %newc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %316) #8
  br label %if.end228

if.end228:                                        ; preds = %if.end227, %if.then215
  br label %if.end244

if.else229:                                       ; preds = %if.end213
  %317 = load float, float* %vel, align 4, !tbaa !24
  %cmp230 = fcmp ogt float %317, 0.000000e+00
  br i1 %cmp230, label %if.then231, label %if.end243

if.then231:                                       ; preds = %if.else229
  %318 = bitcast float* %newc232 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %318) #8
  %319 = load %class.btRotationalLimitMotor*, %class.btRotationalLimitMotor** %limot.addr, align 4, !tbaa !2
  %m_bounce233 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %319, i32 0, i32 10
  %320 = load float, float* %m_bounce233, align 4, !tbaa !39
  %fneg234 = fneg float %320
  %321 = load float, float* %vel, align 4, !tbaa !24
  %mul235 = fmul float %fneg234, %321
  store float %mul235, float* %newc232, align 4, !tbaa !24
  %322 = load float, float* %newc232, align 4, !tbaa !24
  %323 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError236 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %323, i32 0, i32 7
  %324 = load float*, float** %m_constraintError236, align 4, !tbaa !71
  %325 = load i32, i32* %srow, align 4, !tbaa !28
  %arrayidx237 = getelementptr inbounds float, float* %324, i32 %325
  %326 = load float, float* %arrayidx237, align 4, !tbaa !24
  %cmp238 = fcmp olt float %322, %326
  br i1 %cmp238, label %if.then239, label %if.end242

if.then239:                                       ; preds = %if.then231
  %327 = load float, float* %newc232, align 4, !tbaa !24
  %328 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError240 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %328, i32 0, i32 7
  %329 = load float*, float** %m_constraintError240, align 4, !tbaa !71
  %330 = load i32, i32* %srow, align 4, !tbaa !28
  %arrayidx241 = getelementptr inbounds float, float* %329, i32 %330
  store float %327, float* %arrayidx241, align 4, !tbaa !24
  br label %if.end242

if.end242:                                        ; preds = %if.then239, %if.then231
  %331 = bitcast float* %newc232 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %331) #8
  br label %if.end243

if.end243:                                        ; preds = %if.end242, %if.else229
  br label %if.end244

if.end244:                                        ; preds = %if.end243, %if.end228
  %332 = bitcast float* %vel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %332) #8
  br label %if.end245

if.end245:                                        ; preds = %if.end244, %if.end201
  br label %if.end246

if.end246:                                        ; preds = %if.end245, %if.then184
  %333 = bitcast float* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %333) #8
  br label %if.end247

if.end247:                                        ; preds = %if.end246, %if.end158
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %334 = bitcast float** %J2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %334) #8
  %335 = bitcast float** %J1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %335) #8
  br label %cleanup

if.else248:                                       ; preds = %lor.lhs.false
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else248, %if.end247
  %336 = bitcast i32* %limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %336) #8
  %337 = bitcast i32* %powered to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %337) #8
  %338 = bitcast i32* %srow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %338) #8
  %339 = load i32, i32* %retval, align 4
  ret i32 %339
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btRotationalLimitMotor* @_ZN23btGeneric6DofConstraint23getRotationalLimitMotorEi(%class.btGeneric6DofConstraint* %this, i32 %index) #3 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  %index.addr = alloca i32, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !28
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 6
  %0 = load i32, i32* %index.addr, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds [3 x %class.btRotationalLimitMotor], [3 x %class.btRotationalLimitMotor]* %m_angularLimits, i32 0, i32 %0
  ret %class.btRotationalLimitMotor* %arrayidx
}

; Function Attrs: nounwind
define hidden void @_ZN23btGeneric6DofConstraint9updateRHSEf(%class.btGeneric6DofConstraint* %this, float %timeStep) #3 {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  %timeStep.addr = alloca float, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4, !tbaa !2
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !24
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  ret void
}

define hidden void @_ZN23btGeneric6DofConstraint9setFramesERK11btTransformS2_(%class.btGeneric6DofConstraint* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %frameA, %class.btTransform* nonnull align 4 dereferenceable(64) %frameB) #0 {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  %frameA.addr = alloca %class.btTransform*, align 4
  %frameB.addr = alloca %class.btTransform*, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %frameA, %class.btTransform** %frameA.addr, align 4, !tbaa !2
  store %class.btTransform* %frameB, %class.btTransform** %frameB.addr, align 4, !tbaa !2
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %frameA.addr, align 4, !tbaa !2
  %m_frameInA = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_frameInA, %class.btTransform* nonnull align 4 dereferenceable(64) %0)
  %1 = load %class.btTransform*, %class.btTransform** %frameB.addr, align 4, !tbaa !2
  %m_frameInB = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 2
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_frameInB, %class.btTransform* nonnull align 4 dereferenceable(64) %1)
  %2 = bitcast %class.btGeneric6DofConstraint* %this1 to void (%class.btGeneric6DofConstraint*)***
  %vtable = load void (%class.btGeneric6DofConstraint*)**, void (%class.btGeneric6DofConstraint*)*** %2, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btGeneric6DofConstraint*)*, void (%class.btGeneric6DofConstraint*)** %vtable, i64 2
  %3 = load void (%class.btGeneric6DofConstraint*)*, void (%class.btGeneric6DofConstraint*)** %vfn, align 4
  call void %3(%class.btGeneric6DofConstraint* %this1)
  call void @_ZN23btGeneric6DofConstraint19calculateTransformsEv(%class.btGeneric6DofConstraint* %this1)
  ret void
}

define hidden float @_ZNK23btGeneric6DofConstraint24getRelativePivotPositionEi(%class.btGeneric6DofConstraint* %this, i32 %axisIndex) #0 {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  %axisIndex.addr = alloca i32, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4, !tbaa !2
  store i32 %axisIndex, i32* %axisIndex.addr, align 4, !tbaa !28
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %m_calculatedLinearDiff = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 12
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_calculatedLinearDiff)
  %0 = load i32, i32* %axisIndex.addr, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds float, float* %call, i32 %0
  %1 = load float, float* %arrayidx, align 4, !tbaa !24
  ret float %1
}

define hidden float @_ZNK23btGeneric6DofConstraint8getAngleEi(%class.btGeneric6DofConstraint* %this, i32 %axisIndex) #0 {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  %axisIndex.addr = alloca i32, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4, !tbaa !2
  store i32 %axisIndex, i32* %axisIndex.addr, align 4, !tbaa !28
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %m_calculatedAxisAngleDiff = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 10
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_calculatedAxisAngleDiff)
  %0 = load i32, i32* %axisIndex.addr, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds float, float* %call, i32 %0
  %1 = load float, float* %arrayidx, align 4, !tbaa !24
  ret float %1
}

define hidden void @_ZN23btGeneric6DofConstraint13calcAnchorPosEv(%class.btGeneric6DofConstraint* %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  %imA = alloca float, align 4
  %imB = alloca float, align 4
  %weight = alloca float, align 4
  %pA = alloca %class.btVector3*, align 4
  %pB = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca float, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %0 = bitcast float* %imA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %1, i32 0, i32 8
  %2 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4, !tbaa !45
  %call = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %2)
  store float %call, float* %imA, align 4, !tbaa !24
  %3 = bitcast float* %imB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %4, i32 0, i32 9
  %5 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4, !tbaa !47
  %call2 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %5)
  store float %call2, float* %imB, align 4, !tbaa !24
  %6 = bitcast float* %weight to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load float, float* %imB, align 4, !tbaa !24
  %cmp = fcmp oeq float %7, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store float 1.000000e+00, float* %weight, align 4, !tbaa !24
  br label %if.end

if.else:                                          ; preds = %entry
  %8 = load float, float* %imA, align 4, !tbaa !24
  %9 = load float, float* %imA, align 4, !tbaa !24
  %10 = load float, float* %imB, align 4, !tbaa !24
  %add = fadd float %9, %10
  %div = fdiv float %8, %add
  store float %div, float* %weight, align 4, !tbaa !24
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %11 = bitcast %class.btVector3** %pA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  %m_calculatedTransformA = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 8
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_calculatedTransformA)
  store %class.btVector3* %call3, %class.btVector3** %pA, align 4, !tbaa !2
  %12 = bitcast %class.btVector3** %pB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  %m_calculatedTransformB = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 9
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_calculatedTransformB)
  store %class.btVector3* %call4, %class.btVector3** %pB, align 4, !tbaa !2
  %13 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #8
  %14 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %14) #8
  %15 = load %class.btVector3*, %class.btVector3** %pA, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %15, float* nonnull align 4 dereferenceable(4) %weight)
  %16 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #8
  %17 = load %class.btVector3*, %class.btVector3** %pB, align 4, !tbaa !2
  %18 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #8
  %19 = load float, float* %weight, align 4, !tbaa !24
  %sub = fsub float 1.000000e+00, %19
  store float %sub, float* %ref.tmp7, align 4, !tbaa !24
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %17, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp6)
  %m_AnchorPos = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 17
  %20 = bitcast %class.btVector3* %m_AnchorPos to i8*
  %21 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %20, i8* align 4 %21, i32 16, i1 false), !tbaa.struct !22
  %22 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #8
  %23 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %23) #8
  %24 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %24) #8
  %25 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %25) #8
  %26 = bitcast %class.btVector3** %pB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #8
  %27 = bitcast %class.btVector3** %pA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #8
  %28 = bitcast float* %weight to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #8
  %29 = bitcast float* %imB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #8
  %30 = bitcast float* %imA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !24
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !24
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !24
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !24
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !24
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !24
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !24
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !24
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !24
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %1, i32 0)
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call1, float* %ref.tmp, align 4, !tbaa !24
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 1)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !24
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 2)
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !24
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !24
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !24
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !24
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !24
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !24
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !24
  %6 = load float*, float** %s.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !24
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !24
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !24
  ret %class.btVector3* %this1
}

declare float @_ZN17btTypedConstraint14getMotorFactorEfffff(%class.btTypedConstraint*, float, float, float, float, float) #1

define hidden void @_ZN23btGeneric6DofConstraint8setParamEifi(%class.btGeneric6DofConstraint* %this, i32 %num, float %value, i32 %axis) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  %num.addr = alloca i32, align 4
  %value.addr = alloca float, align 4
  %axis.addr = alloca i32, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4, !tbaa !2
  store i32 %num, i32* %num.addr, align 4, !tbaa !28
  store float %value, float* %value.addr, align 4, !tbaa !24
  store i32 %axis, i32* %axis.addr, align 4, !tbaa !28
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %0 = load i32, i32* %axis.addr, align 4, !tbaa !28
  %cmp = icmp sge i32 %0, 0
  br i1 %cmp, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %1 = load i32, i32* %axis.addr, align 4, !tbaa !28
  %cmp2 = icmp slt i32 %1, 3
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %2 = load i32, i32* %num.addr, align 4, !tbaa !28
  switch i32 %2, label %sw.default [
    i32 2, label %sw.bb
    i32 4, label %sw.bb3
    i32 3, label %sw.bb11
  ]

sw.bb:                                            ; preds = %if.then
  %3 = load float, float* %value.addr, align 4, !tbaa !24
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %m_stopERP = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %m_linearLimits, i32 0, i32 7
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_stopERP)
  %4 = load i32, i32* %axis.addr, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds float, float* %call, i32 %4
  store float %3, float* %arrayidx, align 4, !tbaa !24
  %5 = load i32, i32* %axis.addr, align 4, !tbaa !28
  %mul = mul nsw i32 %5, 3
  %shl = shl i32 4, %mul
  %m_flags = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 21
  %6 = load i32, i32* %m_flags, align 4, !tbaa !20
  %or = or i32 %6, %shl
  store i32 %or, i32* %m_flags, align 4, !tbaa !20
  br label %sw.epilog

sw.bb3:                                           ; preds = %if.then
  %7 = load float, float* %value.addr, align 4, !tbaa !24
  %m_linearLimits4 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %m_stopCFM = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %m_linearLimits4, i32 0, i32 8
  %call5 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_stopCFM)
  %8 = load i32, i32* %axis.addr, align 4, !tbaa !28
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %8
  store float %7, float* %arrayidx6, align 4, !tbaa !24
  %9 = load i32, i32* %axis.addr, align 4, !tbaa !28
  %mul7 = mul nsw i32 %9, 3
  %shl8 = shl i32 2, %mul7
  %m_flags9 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 21
  %10 = load i32, i32* %m_flags9, align 4, !tbaa !20
  %or10 = or i32 %10, %shl8
  store i32 %or10, i32* %m_flags9, align 4, !tbaa !20
  br label %sw.epilog

sw.bb11:                                          ; preds = %if.then
  %11 = load float, float* %value.addr, align 4, !tbaa !24
  %m_linearLimits12 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %m_normalCFM = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %m_linearLimits12, i32 0, i32 6
  %call13 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_normalCFM)
  %12 = load i32, i32* %axis.addr, align 4, !tbaa !28
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 %12
  store float %11, float* %arrayidx14, align 4, !tbaa !24
  %13 = load i32, i32* %axis.addr, align 4, !tbaa !28
  %mul15 = mul nsw i32 %13, 3
  %shl16 = shl i32 1, %mul15
  %m_flags17 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 21
  %14 = load i32, i32* %m_flags17, align 4, !tbaa !20
  %or18 = or i32 %14, %shl16
  store i32 %or18, i32* %m_flags17, align 4, !tbaa !20
  br label %sw.epilog

sw.default:                                       ; preds = %if.then
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb11, %sw.bb3, %sw.bb
  br label %if.end51

if.else:                                          ; preds = %land.lhs.true, %entry
  %15 = load i32, i32* %axis.addr, align 4, !tbaa !28
  %cmp19 = icmp sge i32 %15, 3
  br i1 %cmp19, label %land.lhs.true20, label %if.else50

land.lhs.true20:                                  ; preds = %if.else
  %16 = load i32, i32* %axis.addr, align 4, !tbaa !28
  %cmp21 = icmp slt i32 %16, 6
  br i1 %cmp21, label %if.then22, label %if.else50

if.then22:                                        ; preds = %land.lhs.true20
  %17 = load i32, i32* %num.addr, align 4, !tbaa !28
  switch i32 %17, label %sw.default48 [
    i32 2, label %sw.bb23
    i32 4, label %sw.bb30
    i32 3, label %sw.bb39
  ]

sw.bb23:                                          ; preds = %if.then22
  %18 = load float, float* %value.addr, align 4, !tbaa !24
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 6
  %19 = load i32, i32* %axis.addr, align 4, !tbaa !28
  %sub = sub nsw i32 %19, 3
  %arrayidx24 = getelementptr inbounds [3 x %class.btRotationalLimitMotor], [3 x %class.btRotationalLimitMotor]* %m_angularLimits, i32 0, i32 %sub
  %m_stopERP25 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %arrayidx24, i32 0, i32 8
  store float %18, float* %m_stopERP25, align 4, !tbaa !37
  %20 = load i32, i32* %axis.addr, align 4, !tbaa !28
  %mul26 = mul nsw i32 %20, 3
  %shl27 = shl i32 4, %mul26
  %m_flags28 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 21
  %21 = load i32, i32* %m_flags28, align 4, !tbaa !20
  %or29 = or i32 %21, %shl27
  store i32 %or29, i32* %m_flags28, align 4, !tbaa !20
  br label %sw.epilog49

sw.bb30:                                          ; preds = %if.then22
  %22 = load float, float* %value.addr, align 4, !tbaa !24
  %m_angularLimits31 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 6
  %23 = load i32, i32* %axis.addr, align 4, !tbaa !28
  %sub32 = sub nsw i32 %23, 3
  %arrayidx33 = getelementptr inbounds [3 x %class.btRotationalLimitMotor], [3 x %class.btRotationalLimitMotor]* %m_angularLimits31, i32 0, i32 %sub32
  %m_stopCFM34 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %arrayidx33, i32 0, i32 9
  store float %22, float* %m_stopCFM34, align 4, !tbaa !38
  %24 = load i32, i32* %axis.addr, align 4, !tbaa !28
  %mul35 = mul nsw i32 %24, 3
  %shl36 = shl i32 2, %mul35
  %m_flags37 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 21
  %25 = load i32, i32* %m_flags37, align 4, !tbaa !20
  %or38 = or i32 %25, %shl36
  store i32 %or38, i32* %m_flags37, align 4, !tbaa !20
  br label %sw.epilog49

sw.bb39:                                          ; preds = %if.then22
  %26 = load float, float* %value.addr, align 4, !tbaa !24
  %m_angularLimits40 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 6
  %27 = load i32, i32* %axis.addr, align 4, !tbaa !28
  %sub41 = sub nsw i32 %27, 3
  %arrayidx42 = getelementptr inbounds [3 x %class.btRotationalLimitMotor], [3 x %class.btRotationalLimitMotor]* %m_angularLimits40, i32 0, i32 %sub41
  %m_normalCFM43 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %arrayidx42, i32 0, i32 7
  store float %26, float* %m_normalCFM43, align 4, !tbaa !36
  %28 = load i32, i32* %axis.addr, align 4, !tbaa !28
  %mul44 = mul nsw i32 %28, 3
  %shl45 = shl i32 1, %mul44
  %m_flags46 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 21
  %29 = load i32, i32* %m_flags46, align 4, !tbaa !20
  %or47 = or i32 %29, %shl45
  store i32 %or47, i32* %m_flags46, align 4, !tbaa !20
  br label %sw.epilog49

sw.default48:                                     ; preds = %if.then22
  br label %sw.epilog49

sw.epilog49:                                      ; preds = %sw.default48, %sw.bb39, %sw.bb30, %sw.bb23
  br label %if.end

if.else50:                                        ; preds = %land.lhs.true20, %if.else
  br label %if.end

if.end:                                           ; preds = %if.else50, %sw.epilog49
  br label %if.end51

if.end51:                                         ; preds = %if.end, %sw.epilog
  ret void
}

define hidden float @_ZNK23btGeneric6DofConstraint8getParamEii(%class.btGeneric6DofConstraint* %this, i32 %num, i32 %axis) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  %num.addr = alloca i32, align 4
  %axis.addr = alloca i32, align 4
  %retVal = alloca float, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4, !tbaa !2
  store i32 %num, i32* %num.addr, align 4, !tbaa !28
  store i32 %axis, i32* %axis.addr, align 4, !tbaa !28
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %0 = bitcast float* %retVal to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 0.000000e+00, float* %retVal, align 4, !tbaa !24
  %1 = load i32, i32* %axis.addr, align 4, !tbaa !28
  %cmp = icmp sge i32 %1, 0
  br i1 %cmp, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %2 = load i32, i32* %axis.addr, align 4, !tbaa !28
  %cmp2 = icmp slt i32 %2, 3
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %3 = load i32, i32* %num.addr, align 4, !tbaa !28
  switch i32 %3, label %sw.default [
    i32 2, label %sw.bb
    i32 4, label %sw.bb3
    i32 3, label %sw.bb7
  ]

sw.bb:                                            ; preds = %if.then
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %m_stopERP = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %m_linearLimits, i32 0, i32 7
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_stopERP)
  %4 = load i32, i32* %axis.addr, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds float, float* %call, i32 %4
  %5 = load float, float* %arrayidx, align 4, !tbaa !24
  store float %5, float* %retVal, align 4, !tbaa !24
  br label %sw.epilog

sw.bb3:                                           ; preds = %if.then
  %m_linearLimits4 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %m_stopCFM = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %m_linearLimits4, i32 0, i32 8
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_stopCFM)
  %6 = load i32, i32* %axis.addr, align 4, !tbaa !28
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %6
  %7 = load float, float* %arrayidx6, align 4, !tbaa !24
  store float %7, float* %retVal, align 4, !tbaa !24
  br label %sw.epilog

sw.bb7:                                           ; preds = %if.then
  %m_linearLimits8 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %m_normalCFM = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %m_linearLimits8, i32 0, i32 6
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_normalCFM)
  %8 = load i32, i32* %axis.addr, align 4, !tbaa !28
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %8
  %9 = load float, float* %arrayidx10, align 4, !tbaa !24
  store float %9, float* %retVal, align 4, !tbaa !24
  br label %sw.epilog

sw.default:                                       ; preds = %if.then
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb7, %sw.bb3, %sw.bb
  br label %if.end31

if.else:                                          ; preds = %land.lhs.true, %entry
  %10 = load i32, i32* %axis.addr, align 4, !tbaa !28
  %cmp11 = icmp sge i32 %10, 3
  br i1 %cmp11, label %land.lhs.true12, label %if.else30

land.lhs.true12:                                  ; preds = %if.else
  %11 = load i32, i32* %axis.addr, align 4, !tbaa !28
  %cmp13 = icmp slt i32 %11, 6
  br i1 %cmp13, label %if.then14, label %if.else30

if.then14:                                        ; preds = %land.lhs.true12
  %12 = load i32, i32* %num.addr, align 4, !tbaa !28
  switch i32 %12, label %sw.default28 [
    i32 2, label %sw.bb15
    i32 4, label %sw.bb18
    i32 3, label %sw.bb23
  ]

sw.bb15:                                          ; preds = %if.then14
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 6
  %13 = load i32, i32* %axis.addr, align 4, !tbaa !28
  %sub = sub nsw i32 %13, 3
  %arrayidx16 = getelementptr inbounds [3 x %class.btRotationalLimitMotor], [3 x %class.btRotationalLimitMotor]* %m_angularLimits, i32 0, i32 %sub
  %m_stopERP17 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %arrayidx16, i32 0, i32 8
  %14 = load float, float* %m_stopERP17, align 4, !tbaa !37
  store float %14, float* %retVal, align 4, !tbaa !24
  br label %sw.epilog29

sw.bb18:                                          ; preds = %if.then14
  %m_angularLimits19 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 6
  %15 = load i32, i32* %axis.addr, align 4, !tbaa !28
  %sub20 = sub nsw i32 %15, 3
  %arrayidx21 = getelementptr inbounds [3 x %class.btRotationalLimitMotor], [3 x %class.btRotationalLimitMotor]* %m_angularLimits19, i32 0, i32 %sub20
  %m_stopCFM22 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %arrayidx21, i32 0, i32 9
  %16 = load float, float* %m_stopCFM22, align 4, !tbaa !38
  store float %16, float* %retVal, align 4, !tbaa !24
  br label %sw.epilog29

sw.bb23:                                          ; preds = %if.then14
  %m_angularLimits24 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 6
  %17 = load i32, i32* %axis.addr, align 4, !tbaa !28
  %sub25 = sub nsw i32 %17, 3
  %arrayidx26 = getelementptr inbounds [3 x %class.btRotationalLimitMotor], [3 x %class.btRotationalLimitMotor]* %m_angularLimits24, i32 0, i32 %sub25
  %m_normalCFM27 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %arrayidx26, i32 0, i32 7
  %18 = load float, float* %m_normalCFM27, align 4, !tbaa !36
  store float %18, float* %retVal, align 4, !tbaa !24
  br label %sw.epilog29

sw.default28:                                     ; preds = %if.then14
  br label %sw.epilog29

sw.epilog29:                                      ; preds = %sw.default28, %sw.bb23, %sw.bb18, %sw.bb15
  br label %if.end

if.else30:                                        ; preds = %land.lhs.true12, %if.else
  br label %if.end

if.end:                                           ; preds = %if.else30, %sw.epilog29
  br label %if.end31

if.end31:                                         ; preds = %if.end, %sw.epilog
  %19 = load float, float* %retVal, align 4, !tbaa !24
  %20 = bitcast float* %retVal to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #8
  ret float %19
}

define hidden void @_ZN23btGeneric6DofConstraint7setAxisERK9btVector3S2_(%class.btGeneric6DofConstraint* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %axis1, %class.btVector3* nonnull align 4 dereferenceable(16) %axis2) #0 {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  %axis1.addr = alloca %class.btVector3*, align 4
  %axis2.addr = alloca %class.btVector3*, align 4
  %zAxis = alloca %class.btVector3, align 4
  %yAxis = alloca %class.btVector3, align 4
  %xAxis = alloca %class.btVector3, align 4
  %frameInW = alloca %class.btTransform, align 4
  %ref.tmp = alloca %class.btTransform, align 4
  %ref.tmp20 = alloca %class.btTransform, align 4
  %ref.tmp23 = alloca %class.btTransform, align 4
  %ref.tmp24 = alloca %class.btTransform, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %axis1, %class.btVector3** %axis1.addr, align 4, !tbaa !2
  store %class.btVector3* %axis2, %class.btVector3** %axis2.addr, align 4, !tbaa !2
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %0 = bitcast %class.btVector3* %zAxis to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %axis1.addr, align 4, !tbaa !2
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %zAxis, %class.btVector3* %1)
  %2 = bitcast %class.btVector3* %yAxis to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #8
  %3 = load %class.btVector3*, %class.btVector3** %axis2.addr, align 4, !tbaa !2
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %yAxis, %class.btVector3* %3)
  %4 = bitcast %class.btVector3* %xAxis to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #8
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %xAxis, %class.btVector3* %yAxis, %class.btVector3* nonnull align 4 dereferenceable(16) %zAxis)
  %5 = bitcast %class.btTransform* %frameInW to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %5) #8
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %frameInW)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %frameInW)
  %call2 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %frameInW)
  %call3 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %xAxis)
  %arrayidx = getelementptr inbounds float, float* %call3, i32 0
  %call4 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %yAxis)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 0
  %call6 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %zAxis)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 0
  %call8 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %xAxis)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 1
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %yAxis)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 1
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %zAxis)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 1
  %call14 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %xAxis)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 2
  %call16 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %yAxis)
  %arrayidx17 = getelementptr inbounds float, float* %call16, i32 2
  %call18 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %zAxis)
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 2
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %call2, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7, float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11, float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %arrayidx15, float* nonnull align 4 dereferenceable(4) %arrayidx17, float* nonnull align 4 dereferenceable(4) %arrayidx19)
  %6 = bitcast %class.btTransform* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %6) #8
  %7 = bitcast %class.btTransform* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %7) #8
  %8 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %8, i32 0, i32 8
  %9 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4, !tbaa !45
  %call21 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %9)
  call void @_ZNK11btTransform7inverseEv(%class.btTransform* sret align 4 %ref.tmp20, %class.btTransform* %call21)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp, %class.btTransform* %ref.tmp20, %class.btTransform* nonnull align 4 dereferenceable(64) %frameInW)
  %m_frameInA = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_frameInA, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp)
  %10 = bitcast %class.btTransform* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %10) #8
  %11 = bitcast %class.btTransform* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %11) #8
  %12 = bitcast %class.btTransform* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %12) #8
  %13 = bitcast %class.btTransform* %ref.tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %13) #8
  %14 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %14, i32 0, i32 9
  %15 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4, !tbaa !47
  %call25 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %15)
  call void @_ZNK11btTransform7inverseEv(%class.btTransform* sret align 4 %ref.tmp24, %class.btTransform* %call25)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp23, %class.btTransform* %ref.tmp24, %class.btTransform* nonnull align 4 dereferenceable(64) %frameInW)
  %m_frameInB = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 2
  %call26 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_frameInB, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp23)
  %16 = bitcast %class.btTransform* %ref.tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %16) #8
  %17 = bitcast %class.btTransform* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %17) #8
  call void @_ZN23btGeneric6DofConstraint19calculateTransformsEv(%class.btGeneric6DofConstraint* %this1)
  %18 = bitcast %class.btTransform* %frameInW to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %18) #8
  %19 = bitcast %class.btVector3* %xAxis to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %19) #8
  %20 = bitcast %class.btVector3* %yAxis to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %20) #8
  %21 = bitcast %class.btVector3* %zAxis to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %21) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector310normalizedEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %norm = alloca %class.btVector3, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast %class.btVector3* %norm to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = bitcast %class.btVector3* %norm to i8*
  %2 = bitcast %class.btVector3* %this1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !22
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %norm)
  %3 = bitcast %class.btVector3* %agg.result to i8*
  %4 = bitcast %class.btVector3* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false), !tbaa.struct !22
  %5 = bitcast %class.btVector3* %norm to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #8
  ret void
}

define linkonce_odr hidden void @_ZN11btTransform11setIdentityEv(%class.btTransform* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !24
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !24
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !24
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_origin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

define linkonce_odr hidden void @_ZNK11btTransform7inverseEv(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %inv = alloca %class.btMatrix3x3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %1 = bitcast %class.btMatrix3x3* %inv to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %1) #8
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %inv, %class.btMatrix3x3* %m_basis)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #8
  %3 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #8
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %4 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #8
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #8
  %6 = bitcast %class.btMatrix3x3* %inv to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %6) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btTypedConstraint* @_ZN17btTypedConstraintD2Ev(%class.btTypedConstraint* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = bitcast %class.btTypedConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [13 x i8*] }, { [13 x i8*] }* @_ZTV17btTypedConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !8
  ret %class.btTypedConstraint* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN23btGeneric6DofConstraintD0Ev(%class.btGeneric6DofConstraint* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %call = call %class.btGeneric6DofConstraint* bitcast (%class.btTypedConstraint* (%class.btTypedConstraint*)* @_ZN17btTypedConstraintD2Ev to %class.btGeneric6DofConstraint* (%class.btGeneric6DofConstraint*)*)(%class.btGeneric6DofConstraint* %this1) #8
  %0 = bitcast %class.btGeneric6DofConstraint* %this1 to i8*
  call void @_ZN23btGeneric6DofConstraintdlEPv(i8* %0) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif(%class.btTypedConstraint* %this, %class.btAlignedObjectArray.1* nonnull align 1 %ca, i32 %solverBodyA, i32 %solverBodyB, float %timeStep) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  %ca.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %solverBodyA.addr = alloca i32, align 4
  %solverBodyB.addr = alloca i32, align 4
  %timeStep.addr = alloca float, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.1* %ca, %class.btAlignedObjectArray.1** %ca.addr, align 4, !tbaa !2
  store i32 %solverBodyA, i32* %solverBodyA.addr, align 4, !tbaa !28
  store i32 %solverBodyB, i32* %solverBodyB.addr, align 4, !tbaa !28
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !24
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %ca.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f(%class.btTypedConstraint* %this, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %0, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %1, float %2) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  %.addr = alloca %struct.btSolverBody*, align 4
  %.addr1 = alloca %struct.btSolverBody*, align 4
  %.addr2 = alloca float, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4, !tbaa !2
  store %struct.btSolverBody* %0, %struct.btSolverBody** %.addr, align 4, !tbaa !2
  store %struct.btSolverBody* %1, %struct.btSolverBody** %.addr1, align 4, !tbaa !2
  store float %2, float* %.addr2, align 4, !tbaa !24
  %this3 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK23btGeneric6DofConstraint28calculateSerializeBufferSizeEv(%class.btGeneric6DofConstraint* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  ret i32 252
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZNK23btGeneric6DofConstraint9serializeEPvP12btSerializer(%class.btGeneric6DofConstraint* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %dof = alloca %struct.btGeneric6DofConstraintData*, align 4
  %i = alloca i32, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4, !tbaa !2
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %0 = bitcast %struct.btGeneric6DofConstraintData** %dof to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i8*, i8** %dataBuffer.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.btGeneric6DofConstraintData*
  store %struct.btGeneric6DofConstraintData* %2, %struct.btGeneric6DofConstraintData** %dof, align 4, !tbaa !2
  %3 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %4 = load %struct.btGeneric6DofConstraintData*, %struct.btGeneric6DofConstraintData** %dof, align 4, !tbaa !2
  %m_typeConstraintData = getelementptr inbounds %struct.btGeneric6DofConstraintData, %struct.btGeneric6DofConstraintData* %4, i32 0, i32 0
  %5 = bitcast %struct.btTypedConstraintData* %m_typeConstraintData to i8*
  %6 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %call = call i8* @_ZNK17btTypedConstraint9serializeEPvP12btSerializer(%class.btTypedConstraint* %3, i8* %5, %class.btSerializer* %6)
  %m_frameInA = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 1
  %7 = load %struct.btGeneric6DofConstraintData*, %struct.btGeneric6DofConstraintData** %dof, align 4, !tbaa !2
  %m_rbAFrame = getelementptr inbounds %struct.btGeneric6DofConstraintData, %struct.btGeneric6DofConstraintData* %7, i32 0, i32 1
  call void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %m_frameInA, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %m_rbAFrame)
  %m_frameInB = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 2
  %8 = load %struct.btGeneric6DofConstraintData*, %struct.btGeneric6DofConstraintData** %dof, align 4, !tbaa !2
  %m_rbBFrame = getelementptr inbounds %struct.btGeneric6DofConstraintData, %struct.btGeneric6DofConstraintData* %8, i32 0, i32 2
  call void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %m_frameInB, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %m_rbBFrame)
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %10 = load i32, i32* %i, align 4, !tbaa !28
  %cmp = icmp slt i32 %10, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 6
  %11 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds [3 x %class.btRotationalLimitMotor], [3 x %class.btRotationalLimitMotor]* %m_angularLimits, i32 0, i32 %11
  %m_loLimit = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %arrayidx, i32 0, i32 0
  %12 = load float, float* %m_loLimit, align 4, !tbaa !34
  %13 = load %struct.btGeneric6DofConstraintData*, %struct.btGeneric6DofConstraintData** %dof, align 4, !tbaa !2
  %m_angularLowerLimit = getelementptr inbounds %struct.btGeneric6DofConstraintData, %struct.btGeneric6DofConstraintData* %13, i32 0, i32 6
  %m_floats = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularLowerLimit, i32 0, i32 0
  %14 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %14
  store float %12, float* %arrayidx2, align 4, !tbaa !24
  %m_angularLimits3 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 6
  %15 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx4 = getelementptr inbounds [3 x %class.btRotationalLimitMotor], [3 x %class.btRotationalLimitMotor]* %m_angularLimits3, i32 0, i32 %15
  %m_hiLimit = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %arrayidx4, i32 0, i32 1
  %16 = load float, float* %m_hiLimit, align 4, !tbaa !35
  %17 = load %struct.btGeneric6DofConstraintData*, %struct.btGeneric6DofConstraintData** %dof, align 4, !tbaa !2
  %m_angularUpperLimit = getelementptr inbounds %struct.btGeneric6DofConstraintData, %struct.btGeneric6DofConstraintData* %17, i32 0, i32 5
  %m_floats5 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularUpperLimit, i32 0, i32 0
  %18 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 %18
  store float %16, float* %arrayidx6, align 4, !tbaa !24
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %m_lowerLimit = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %m_linearLimits, i32 0, i32 0
  %call7 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_lowerLimit)
  %19 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx8 = getelementptr inbounds float, float* %call7, i32 %19
  %20 = load float, float* %arrayidx8, align 4, !tbaa !24
  %21 = load %struct.btGeneric6DofConstraintData*, %struct.btGeneric6DofConstraintData** %dof, align 4, !tbaa !2
  %m_linearLowerLimit = getelementptr inbounds %struct.btGeneric6DofConstraintData, %struct.btGeneric6DofConstraintData* %21, i32 0, i32 4
  %m_floats9 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_linearLowerLimit, i32 0, i32 0
  %22 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 %22
  store float %20, float* %arrayidx10, align 4, !tbaa !24
  %m_linearLimits11 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %m_upperLimit = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %m_linearLimits11, i32 0, i32 1
  %call12 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_upperLimit)
  %23 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 %23
  %24 = load float, float* %arrayidx13, align 4, !tbaa !24
  %25 = load %struct.btGeneric6DofConstraintData*, %struct.btGeneric6DofConstraintData** %dof, align 4, !tbaa !2
  %m_linearUpperLimit = getelementptr inbounds %struct.btGeneric6DofConstraintData, %struct.btGeneric6DofConstraintData* %25, i32 0, i32 3
  %m_floats14 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_linearUpperLimit, i32 0, i32 0
  %26 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx15 = getelementptr inbounds [4 x float], [4 x float]* %m_floats14, i32 0, i32 %26
  store float %24, float* %arrayidx15, align 4, !tbaa !24
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %27 = load i32, i32* %i, align 4, !tbaa !28
  %inc = add nsw i32 %27, 1
  store i32 %inc, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_useLinearReferenceFrameA = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 18
  %28 = load i8, i8* %m_useLinearReferenceFrameA, align 4, !tbaa !11, !range !10
  %tobool = trunc i8 %28 to i1
  %29 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 1, i32 0
  %30 = load %struct.btGeneric6DofConstraintData*, %struct.btGeneric6DofConstraintData** %dof, align 4, !tbaa !2
  %m_useLinearReferenceFrameA16 = getelementptr inbounds %struct.btGeneric6DofConstraintData, %struct.btGeneric6DofConstraintData* %30, i32 0, i32 7
  store i32 %cond, i32* %m_useLinearReferenceFrameA16, align 4, !tbaa !75
  %m_useOffsetForConstraintFrame = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 19
  %31 = load i8, i8* %m_useOffsetForConstraintFrame, align 1, !tbaa !19, !range !10
  %tobool17 = trunc i8 %31 to i1
  %32 = zext i1 %tobool17 to i64
  %cond18 = select i1 %tobool17, i32 1, i32 0
  %33 = load %struct.btGeneric6DofConstraintData*, %struct.btGeneric6DofConstraintData** %dof, align 4, !tbaa !2
  %m_useOffsetForConstraintFrame19 = getelementptr inbounds %struct.btGeneric6DofConstraintData, %struct.btGeneric6DofConstraintData* %33, i32 0, i32 8
  store i32 %cond18, i32* %m_useOffsetForConstraintFrame19, align 4, !tbaa !81
  %34 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #8
  %35 = bitcast %struct.btGeneric6DofConstraintData** %dof to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  ret i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !22
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !22
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !22
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %b.addr = alloca %class.btMatrix3x3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %b, %class.btMatrix3x3** %b.addr, align 4, !tbaa !2
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %b.addr, align 4, !tbaa !2
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !22
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4, !tbaa !24
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !24
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !24
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !24
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !24
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !24
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !24
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !24
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !24
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !24
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !22
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !22
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !22
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: nounwind readnone
declare float @atan2f(float, float) #6

; Function Attrs: nounwind readnone
declare float @asinf(float) #6

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !24
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !24
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !24
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !24
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !24
  %mul8 = fmul float %7, %9
  store float %mul8, float* %ref.tmp3, align 4, !tbaa !24
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !24
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !24
  %mul14 = fmul float %12, %14
  store float %mul14, float* %ref.tmp9, align 4, !tbaa !24
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !24
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !24
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !24
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !24
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !24
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !24
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !24
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !24
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !24
  ret %class.btVector3* %this1
}

define linkonce_odr hidden void @_ZN11btRigidBody19applyCentralImpulseERK9btVector3(%class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %impulse) #0 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %impulse.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %impulse, %class.btVector3** %impulse.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #8
  %2 = load %class.btVector3*, %class.btVector3** %impulse.addr, align 4, !tbaa !2
  %m_linearFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 5
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearFactor)
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %m_inverseMass)
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_linearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %3 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %3) #8
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !24
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !24
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !24
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !24
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !24
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !24
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !24
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !24
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !24
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !24
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !24
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !24
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !24
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !24
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !24
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !24
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !24
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !24
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this, i32 %r1, i32 %c1, i32 %r2, i32 %c2) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %r1.addr = alloca i32, align 4
  %c1.addr = alloca i32, align 4
  %r2.addr = alloca i32, align 4
  %c2.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %r1, i32* %r1.addr, align 4, !tbaa !28
  store i32 %c1, i32* %c1.addr, align 4, !tbaa !28
  store i32 %r2, i32* %r2.addr, align 4, !tbaa !28
  store i32 %c2, i32* %c2.addr, align 4, !tbaa !28
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %r1.addr, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx)
  %1 = load i32, i32* %c1.addr, align 4, !tbaa !28
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 %1
  %2 = load float, float* %arrayidx2, align 4, !tbaa !24
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %3 = load i32, i32* %r2.addr, align 4, !tbaa !28
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 %3
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx4)
  %4 = load i32, i32* %c2.addr, align 4, !tbaa !28
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %4
  %5 = load float, float* %arrayidx6, align 4, !tbaa !24
  %mul = fmul float %2, %5
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %6 = load i32, i32* %r1.addr, align 4, !tbaa !28
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 %6
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx8)
  %7 = load i32, i32* %c2.addr, align 4, !tbaa !28
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %7
  %8 = load float, float* %arrayidx10, align 4, !tbaa !24
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %9 = load i32, i32* %r2.addr, align 4, !tbaa !28
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 %9
  %call13 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx12)
  %10 = load i32, i32* %c1.addr, align 4, !tbaa !28
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 %10
  %11 = load float, float* %arrayidx14, align 4, !tbaa !24
  %mul15 = fmul float %8, %11
  %sub = fsub float %mul, %mul15
  ret float %sub
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !24
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !24
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  ret %class.btVector3* %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #4 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !24
  %0 = load float, float* %y.addr, align 4, !tbaa !24
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #7

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btFabsf(float %x) #4 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !24
  %0 = load float, float* %x.addr, align 4, !tbaa !24
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_Z16btNormalizeAnglef(float %angleInRadians) #2 comdat {
entry:
  %retval = alloca float, align 4
  %angleInRadians.addr = alloca float, align 4
  store float %angleInRadians, float* %angleInRadians.addr, align 4, !tbaa !24
  %0 = load float, float* %angleInRadians.addr, align 4, !tbaa !24
  %call = call float @_Z6btFmodff(float %0, float 0x401921FB60000000)
  store float %call, float* %angleInRadians.addr, align 4, !tbaa !24
  %1 = load float, float* %angleInRadians.addr, align 4, !tbaa !24
  %cmp = fcmp olt float %1, 0xC00921FB60000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load float, float* %angleInRadians.addr, align 4, !tbaa !24
  %add = fadd float %2, 0x401921FB60000000
  store float %add, float* %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %3 = load float, float* %angleInRadians.addr, align 4, !tbaa !24
  %cmp1 = fcmp ogt float %3, 0x400921FB60000000
  br i1 %cmp1, label %if.then2, label %if.else3

if.then2:                                         ; preds = %if.else
  %4 = load float, float* %angleInRadians.addr, align 4, !tbaa !24
  %sub = fsub float %4, 0x401921FB60000000
  store float %sub, float* %retval, align 4
  br label %return

if.else3:                                         ; preds = %if.else
  %5 = load float, float* %angleInRadians.addr, align 4, !tbaa !24
  store float %5, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.else3, %if.then2, %if.then
  %6 = load float, float* %retval, align 4
  ret float %6
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #7

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btFmodff(float %x, float %y) #4 comdat {
entry:
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !24
  store float %y, float* %y.addr, align 4, !tbaa !24
  %0 = load float, float* %x.addr, align 4, !tbaa !24
  %1 = load float, float* %y.addr, align 4, !tbaa !24
  %fmod = frem float %0, %1
  ret float %fmod
}

define linkonce_odr hidden void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !24
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !24
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !24
  %3 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !24
  %4 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  store float 1.000000e+00, float* %ref.tmp5, align 4, !tbaa !24
  %5 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !24
  %6 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !24
  %7 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  store float 0.000000e+00, float* %ref.tmp8, align 4, !tbaa !24
  %8 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  store float 1.000000e+00, float* %ref.tmp9, align 4, !tbaa !24
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %9 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  %12 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  %13 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  %14 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #8
  %15 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN23btGeneric6DofConstraintdlEPv(i8* %ptr) #4 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

declare i8* @_ZNK17btTypedConstraint9serializeEPvP12btSerializer(%class.btTypedConstraint*, i8*, %class.btSerializer*) unnamed_addr #1

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %this, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %dataOut) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %dataOut.addr = alloca %struct.btTransformFloatData*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %struct.btTransformFloatData* %dataOut, %struct.btTransformFloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %struct.btTransformFloatData*, %struct.btTransformFloatData** %dataOut.addr, align 4, !tbaa !2
  %m_basis2 = getelementptr inbounds %struct.btTransformFloatData, %struct.btTransformFloatData* %0, i32 0, i32 0
  call void @_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData(%class.btMatrix3x3* %m_basis, %struct.btMatrix3x3FloatData* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %struct.btTransformFloatData*, %struct.btTransformFloatData** %dataOut.addr, align 4, !tbaa !2
  %m_origin3 = getelementptr inbounds %struct.btTransformFloatData, %struct.btTransformFloatData* %1, i32 0, i32 1
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_origin, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_origin3)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData(%class.btMatrix3x3* %this, %struct.btMatrix3x3FloatData* nonnull align 4 dereferenceable(48) %dataOut) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %dataOut.addr = alloca %struct.btMatrix3x3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %struct.btMatrix3x3FloatData* %dataOut, %struct.btMatrix3x3FloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !28
  %cmp = icmp slt i32 %1, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %3 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %3
  %4 = load %struct.btMatrix3x3FloatData*, %struct.btMatrix3x3FloatData** %dataOut.addr, align 4, !tbaa !2
  %m_el2 = getelementptr inbounds %struct.btMatrix3x3FloatData, %struct.btMatrix3x3FloatData* %4, i32 0, i32 0
  %5 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx3 = getelementptr inbounds [3 x %struct.btVector3FloatData], [3 x %struct.btVector3FloatData]* %m_el2, i32 0, i32 %5
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %arrayidx, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %arrayidx3)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !28
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !28
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %3 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %3
  %4 = load float, float* %arrayidx, align 4, !tbaa !24
  %5 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %5, i32 0, i32 0
  %6 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %6
  store float %4, float* %arrayidx3, align 4, !tbaa !24
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !28
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { argmemonly nounwind willreturn }
attributes #6 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind readnone speculatable willreturn }
attributes #8 = { nounwind }
attributes #9 = { nounwind readnone }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"bool", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"vtable pointer", !5, i64 0}
!10 = !{i8 0, i8 2}
!11 = !{!12, !7, i64 1300}
!12 = !{!"_ZTS23btGeneric6DofConstraint", !13, i64 48, !13, i64 112, !4, i64 176, !4, i64 428, !16, i64 680, !4, i64 868, !17, i64 1060, !13, i64 1064, !13, i64 1128, !15, i64 1192, !4, i64 1208, !15, i64 1256, !17, i64 1272, !17, i64 1276, !7, i64 1280, !15, i64 1284, !7, i64 1300, !7, i64 1301, !18, i64 1304, !7, i64 1308}
!13 = !{!"_ZTS11btTransform", !14, i64 0, !15, i64 48}
!14 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!15 = !{!"_ZTS9btVector3", !4, i64 0}
!16 = !{!"_ZTS25btTranslationalLimitMotor", !15, i64 0, !15, i64 16, !15, i64 32, !17, i64 48, !17, i64 52, !17, i64 56, !15, i64 60, !15, i64 76, !15, i64 92, !4, i64 108, !15, i64 112, !15, i64 128, !15, i64 144, !15, i64 160, !4, i64 176}
!17 = !{!"float", !4, i64 0}
!18 = !{!"int", !4, i64 0}
!19 = !{!12, !7, i64 1301}
!20 = !{!12, !18, i64 1304}
!21 = !{!12, !7, i64 1308}
!22 = !{i64 0, i64 16, !23}
!23 = !{!4, !4, i64 0}
!24 = !{!17, !17, i64 0}
!25 = !{!16, !17, i64 48}
!26 = !{!16, !17, i64 52}
!27 = !{!16, !17, i64 56}
!28 = !{!18, !18, i64 0}
!29 = !{!30, !17, i64 60}
!30 = !{!"_ZTS22btRotationalLimitMotor", !17, i64 0, !17, i64 4, !17, i64 8, !17, i64 12, !17, i64 16, !17, i64 20, !17, i64 24, !17, i64 28, !17, i64 32, !17, i64 36, !17, i64 40, !7, i64 44, !17, i64 48, !17, i64 52, !18, i64 56, !17, i64 60}
!31 = !{!30, !17, i64 8}
!32 = !{!30, !17, i64 12}
!33 = !{!30, !17, i64 16}
!34 = !{!30, !17, i64 0}
!35 = !{!30, !17, i64 4}
!36 = !{!30, !17, i64 28}
!37 = !{!30, !17, i64 32}
!38 = !{!30, !17, i64 36}
!39 = !{!30, !17, i64 40}
!40 = !{!30, !17, i64 20}
!41 = !{!30, !17, i64 24}
!42 = !{!30, !18, i64 56}
!43 = !{!30, !17, i64 48}
!44 = !{!30, !7, i64 44}
!45 = !{!46, !3, i64 28}
!46 = !{!"_ZTS17btTypedConstraint", !18, i64 8, !4, i64 12, !17, i64 16, !7, i64 20, !7, i64 21, !18, i64 24, !3, i64 28, !3, i64 32, !17, i64 36, !17, i64 40, !3, i64 44}
!47 = !{!46, !3, i64 32}
!48 = !{!49, !17, i64 344}
!49 = !{!"_ZTS11btRigidBody", !14, i64 264, !15, i64 312, !15, i64 328, !17, i64 344, !15, i64 348, !15, i64 364, !15, i64 380, !15, i64 396, !15, i64 412, !15, i64 428, !17, i64 444, !17, i64 448, !7, i64 452, !17, i64 456, !17, i64 460, !17, i64 464, !17, i64 468, !17, i64 472, !17, i64 476, !3, i64 480, !50, i64 484, !18, i64 504, !18, i64 508, !15, i64 512, !15, i64 528, !15, i64 544, !15, i64 560, !15, i64 576, !15, i64 592, !18, i64 608, !18, i64 612}
!50 = !{!"_ZTS20btAlignedObjectArrayIP17btTypedConstraintE", !51, i64 0, !18, i64 4, !18, i64 8, !3, i64 12, !7, i64 16}
!51 = !{!"_ZTS18btAlignedAllocatorIP17btTypedConstraintLj16EE"}
!52 = !{!12, !7, i64 1280}
!53 = !{!12, !17, i64 1272}
!54 = !{!12, !17, i64 1276}
!55 = !{!56, !17, i64 80}
!56 = !{!"_ZTS15btJacobianEntry", !15, i64 0, !15, i64 16, !15, i64 32, !15, i64 48, !15, i64 64, !17, i64 80}
!57 = !{!30, !17, i64 52}
!58 = !{!59, !18, i64 0}
!59 = !{!"_ZTSN17btTypedConstraint17btConstraintInfo1E", !18, i64 0, !18, i64 4}
!60 = !{!59, !18, i64 4}
!61 = !{!62, !3, i64 32}
!62 = !{!"_ZTSN17btTypedConstraint17btConstraintInfo2E", !17, i64 0, !17, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !18, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !3, i64 44, !18, i64 48, !17, i64 52}
!63 = !{!62, !17, i64 4}
!64 = !{!12, !17, i64 732}
!65 = !{!12, !17, i64 728}
!66 = !{!62, !18, i64 24}
!67 = !{!62, !3, i64 12}
!68 = !{!62, !3, i64 8}
!69 = !{!62, !3, i64 20}
!70 = !{!62, !3, i64 16}
!71 = !{!62, !3, i64 28}
!72 = !{!62, !17, i64 0}
!73 = !{!62, !3, i64 36}
!74 = !{!62, !3, i64 40}
!75 = !{!76, !18, i64 244}
!76 = !{!"_ZTS27btGeneric6DofConstraintData", !77, i64 0, !78, i64 52, !78, i64 116, !80, i64 180, !80, i64 196, !80, i64 212, !80, i64 228, !18, i64 244, !18, i64 248}
!77 = !{!"_ZTS21btTypedConstraintData", !3, i64 0, !3, i64 4, !3, i64 8, !18, i64 12, !18, i64 16, !18, i64 20, !18, i64 24, !17, i64 28, !17, i64 32, !18, i64 36, !18, i64 40, !17, i64 44, !18, i64 48}
!78 = !{!"_ZTS20btTransformFloatData", !79, i64 0, !80, i64 48}
!79 = !{!"_ZTS20btMatrix3x3FloatData", !4, i64 0}
!80 = !{!"_ZTS18btVector3FloatData", !4, i64 0}
!81 = !{!76, !18, i64 248}
