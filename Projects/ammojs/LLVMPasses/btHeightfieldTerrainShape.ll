; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btHeightfieldTerrainShape.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btHeightfieldTerrainShape.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btHeightfieldTerrainShape = type { %class.btConcaveShape, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32, float, float, float, float, float, %union.anon, i32, i8, i8, i8, i32, %class.btVector3 }
%class.btConcaveShape = type { %class.btCollisionShape, float }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%union.anon = type { i8* }
%class.btVector3 = type { [4 x float] }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btTriangleCallback = type { i32 (...)** }
%class.btSerializer = type opaque

$_ZN9btVector3C2Ev = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZN25btHeightfieldTerrainShapedlEPv = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN9btVector3mLERKS_ = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x38absoluteEv = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZN11btMatrix3x3ixEi = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZN9btVector36setMaxERKS_ = comdat any

$_ZN9btVector36setMinERKS_ = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZNK25btHeightfieldTerrainShape7getNameEv = comdat any

$_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZN14btConcaveShape9setMarginEf = comdat any

$_ZNK14btConcaveShape9getMarginEv = comdat any

$_ZNK16btCollisionShape28calculateSerializeBufferSizeEv = comdat any

$_Z6btFabsf = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_Z8btSetMaxIfEvRT_RKS0_ = comdat any

$_ZNK9btVector31wEv = comdat any

$_Z8btSetMinIfEvRT_RKS0_ = comdat any

@_ZTV25btHeightfieldTerrainShape = hidden unnamed_addr constant { [20 x i8*] } { [20 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI25btHeightfieldTerrainShape to i8*), i8* bitcast (%class.btHeightfieldTerrainShape* (%class.btHeightfieldTerrainShape*)* @_ZN25btHeightfieldTerrainShapeD1Ev to i8*), i8* bitcast (void (%class.btHeightfieldTerrainShape*)* @_ZN25btHeightfieldTerrainShapeD0Ev to i8*), i8* bitcast (void (%class.btHeightfieldTerrainShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK25btHeightfieldTerrainShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btHeightfieldTerrainShape*, %class.btVector3*)* @_ZN25btHeightfieldTerrainShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btHeightfieldTerrainShape*)* @_ZNK25btHeightfieldTerrainShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btHeightfieldTerrainShape*, float, %class.btVector3*)* @_ZNK25btHeightfieldTerrainShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btHeightfieldTerrainShape*)* @_ZNK25btHeightfieldTerrainShape7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btConcaveShape*, float)* @_ZN14btConcaveShape9setMarginEf to i8*), i8* bitcast (float (%class.btConcaveShape*)* @_ZNK14btConcaveShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btCollisionShape*)* @_ZNK16btCollisionShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btCollisionShape*, i8*, %class.btSerializer*)* @_ZNK16btCollisionShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btHeightfieldTerrainShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)* @_ZNK25btHeightfieldTerrainShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_ to i8*), i8* bitcast (float (%class.btHeightfieldTerrainShape*, i32, i32)* @_ZNK25btHeightfieldTerrainShape22getRawHeightFieldValueEii to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS25btHeightfieldTerrainShape = hidden constant [28 x i8] c"25btHeightfieldTerrainShape\00", align 1
@_ZTI14btConcaveShape = external constant i8*
@_ZTI25btHeightfieldTerrainShape = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([28 x i8], [28 x i8]* @_ZTS25btHeightfieldTerrainShape, i32 0, i32 0), i8* bitcast (i8** @_ZTI14btConcaveShape to i8*) }, align 4
@.str = private unnamed_addr constant [12 x i8] c"HEIGHTFIELD\00", align 1

@_ZN25btHeightfieldTerrainShapeC1EiiPKvfffi14PHY_ScalarTypeb = hidden unnamed_addr alias %class.btHeightfieldTerrainShape* (%class.btHeightfieldTerrainShape*, i32, i32, i8*, float, float, float, i32, i32, i1), %class.btHeightfieldTerrainShape* (%class.btHeightfieldTerrainShape*, i32, i32, i8*, float, float, float, i32, i32, i1)* @_ZN25btHeightfieldTerrainShapeC2EiiPKvfffi14PHY_ScalarTypeb
@_ZN25btHeightfieldTerrainShapeC1EiiPKvfibb = hidden unnamed_addr alias %class.btHeightfieldTerrainShape* (%class.btHeightfieldTerrainShape*, i32, i32, i8*, float, i32, i1, i1), %class.btHeightfieldTerrainShape* (%class.btHeightfieldTerrainShape*, i32, i32, i8*, float, i32, i1, i1)* @_ZN25btHeightfieldTerrainShapeC2EiiPKvfibb
@_ZN25btHeightfieldTerrainShapeD1Ev = hidden unnamed_addr alias %class.btHeightfieldTerrainShape* (%class.btHeightfieldTerrainShape*), %class.btHeightfieldTerrainShape* (%class.btHeightfieldTerrainShape*)* @_ZN25btHeightfieldTerrainShapeD2Ev

define hidden %class.btHeightfieldTerrainShape* @_ZN25btHeightfieldTerrainShapeC2EiiPKvfffi14PHY_ScalarTypeb(%class.btHeightfieldTerrainShape* returned %this, i32 %heightStickWidth, i32 %heightStickLength, i8* %heightfieldData, float %heightScale, float %minHeight, float %maxHeight, i32 %upAxis, i32 %hdt, i1 zeroext %flipQuadEdges) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btHeightfieldTerrainShape*, align 4
  %heightStickWidth.addr = alloca i32, align 4
  %heightStickLength.addr = alloca i32, align 4
  %heightfieldData.addr = alloca i8*, align 4
  %heightScale.addr = alloca float, align 4
  %minHeight.addr = alloca float, align 4
  %maxHeight.addr = alloca float, align 4
  %upAxis.addr = alloca i32, align 4
  %hdt.addr = alloca i32, align 4
  %flipQuadEdges.addr = alloca i8, align 1
  store %class.btHeightfieldTerrainShape* %this, %class.btHeightfieldTerrainShape** %this.addr, align 4, !tbaa !2
  store i32 %heightStickWidth, i32* %heightStickWidth.addr, align 4, !tbaa !6
  store i32 %heightStickLength, i32* %heightStickLength.addr, align 4, !tbaa !6
  store i8* %heightfieldData, i8** %heightfieldData.addr, align 4, !tbaa !2
  store float %heightScale, float* %heightScale.addr, align 4, !tbaa !8
  store float %minHeight, float* %minHeight.addr, align 4, !tbaa !8
  store float %maxHeight, float* %maxHeight.addr, align 4, !tbaa !8
  store i32 %upAxis, i32* %upAxis.addr, align 4, !tbaa !6
  store i32 %hdt, i32* %hdt.addr, align 4, !tbaa !10
  %frombool = zext i1 %flipQuadEdges to i8
  store i8 %frombool, i8* %flipQuadEdges.addr, align 1, !tbaa !12
  %this1 = load %class.btHeightfieldTerrainShape*, %class.btHeightfieldTerrainShape** %this.addr, align 4
  %0 = bitcast %class.btHeightfieldTerrainShape* %this1 to %class.btConcaveShape*
  %call = call %class.btConcaveShape* @_ZN14btConcaveShapeC2Ev(%class.btConcaveShape* %0)
  %1 = bitcast %class.btHeightfieldTerrainShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [20 x i8*] }, { [20 x i8*] }* @_ZTV25btHeightfieldTerrainShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !14
  %m_localAabbMin = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_localAabbMin)
  %m_localAabbMax = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_localAabbMax)
  %m_localOrigin = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 3
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_localOrigin)
  %m_localScaling = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 17
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_localScaling)
  %2 = load i32, i32* %heightStickWidth.addr, align 4, !tbaa !6
  %3 = load i32, i32* %heightStickLength.addr, align 4, !tbaa !6
  %4 = load i8*, i8** %heightfieldData.addr, align 4, !tbaa !2
  %5 = load float, float* %heightScale.addr, align 4, !tbaa !8
  %6 = load float, float* %minHeight.addr, align 4, !tbaa !8
  %7 = load float, float* %maxHeight.addr, align 4, !tbaa !8
  %8 = load i32, i32* %upAxis.addr, align 4, !tbaa !6
  %9 = load i32, i32* %hdt.addr, align 4, !tbaa !10
  %10 = load i8, i8* %flipQuadEdges.addr, align 1, !tbaa !12, !range !16
  %tobool = trunc i8 %10 to i1
  call void @_ZN25btHeightfieldTerrainShape10initializeEiiPKvfffi14PHY_ScalarTypeb(%class.btHeightfieldTerrainShape* %this1, i32 %2, i32 %3, i8* %4, float %5, float %6, float %7, i32 %8, i32 %9, i1 zeroext %tobool)
  ret %class.btHeightfieldTerrainShape* %this1
}

declare %class.btConcaveShape* @_ZN14btConcaveShapeC2Ev(%class.btConcaveShape* returned) unnamed_addr #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

define hidden void @_ZN25btHeightfieldTerrainShape10initializeEiiPKvfffi14PHY_ScalarTypeb(%class.btHeightfieldTerrainShape* %this, i32 %heightStickWidth, i32 %heightStickLength, i8* %heightfieldData, float %heightScale, float %minHeight, float %maxHeight, i32 %upAxis, i32 %hdt, i1 zeroext %flipQuadEdges) #0 {
entry:
  %this.addr = alloca %class.btHeightfieldTerrainShape*, align 4
  %heightStickWidth.addr = alloca i32, align 4
  %heightStickLength.addr = alloca i32, align 4
  %heightfieldData.addr = alloca i8*, align 4
  %heightScale.addr = alloca float, align 4
  %minHeight.addr = alloca float, align 4
  %maxHeight.addr = alloca float, align 4
  %upAxis.addr = alloca i32, align 4
  %hdt.addr = alloca i32, align 4
  %flipQuadEdges.addr = alloca i8, align 1
  %ref.tmp = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp26 = alloca float, align 4
  %ref.tmp32 = alloca %class.btVector3, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp34 = alloca %class.btVector3, align 4
  store %class.btHeightfieldTerrainShape* %this, %class.btHeightfieldTerrainShape** %this.addr, align 4, !tbaa !2
  store i32 %heightStickWidth, i32* %heightStickWidth.addr, align 4, !tbaa !6
  store i32 %heightStickLength, i32* %heightStickLength.addr, align 4, !tbaa !6
  store i8* %heightfieldData, i8** %heightfieldData.addr, align 4, !tbaa !2
  store float %heightScale, float* %heightScale.addr, align 4, !tbaa !8
  store float %minHeight, float* %minHeight.addr, align 4, !tbaa !8
  store float %maxHeight, float* %maxHeight.addr, align 4, !tbaa !8
  store i32 %upAxis, i32* %upAxis.addr, align 4, !tbaa !6
  store i32 %hdt, i32* %hdt.addr, align 4, !tbaa !10
  %frombool = zext i1 %flipQuadEdges to i8
  store i8 %frombool, i8* %flipQuadEdges.addr, align 1, !tbaa !12
  %this1 = load %class.btHeightfieldTerrainShape*, %class.btHeightfieldTerrainShape** %this.addr, align 4
  %0 = bitcast %class.btHeightfieldTerrainShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %0, i32 0, i32 1
  store i32 24, i32* %m_shapeType, align 4, !tbaa !17
  %1 = load i32, i32* %heightStickWidth.addr, align 4, !tbaa !6
  %m_heightStickWidth = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 4
  store i32 %1, i32* %m_heightStickWidth, align 4, !tbaa !19
  %2 = load i32, i32* %heightStickLength.addr, align 4, !tbaa !6
  %m_heightStickLength = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 5
  store i32 %2, i32* %m_heightStickLength, align 4, !tbaa !22
  %3 = load float, float* %minHeight.addr, align 4, !tbaa !8
  %m_minHeight = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 6
  store float %3, float* %m_minHeight, align 4, !tbaa !23
  %4 = load float, float* %maxHeight.addr, align 4, !tbaa !8
  %m_maxHeight = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 7
  store float %4, float* %m_maxHeight, align 4, !tbaa !24
  %5 = load i32, i32* %heightStickWidth.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %5, 1
  %conv = sitofp i32 %sub to float
  %m_width = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 8
  store float %conv, float* %m_width, align 4, !tbaa !25
  %6 = load i32, i32* %heightStickLength.addr, align 4, !tbaa !6
  %sub2 = sub nsw i32 %6, 1
  %conv3 = sitofp i32 %sub2 to float
  %m_length = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 9
  store float %conv3, float* %m_length, align 4, !tbaa !26
  %7 = load float, float* %heightScale.addr, align 4, !tbaa !8
  %m_heightScale = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 10
  store float %7, float* %m_heightScale, align 4, !tbaa !27
  %8 = load i8*, i8** %heightfieldData.addr, align 4, !tbaa !2
  %9 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 11
  %m_heightfieldDataUnknown = bitcast %union.anon* %9 to i8**
  store i8* %8, i8** %m_heightfieldDataUnknown, align 4, !tbaa !28
  %10 = load i32, i32* %hdt.addr, align 4, !tbaa !10
  %m_heightDataType = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 12
  store i32 %10, i32* %m_heightDataType, align 4, !tbaa !29
  %11 = load i8, i8* %flipQuadEdges.addr, align 1, !tbaa !12, !range !16
  %tobool = trunc i8 %11 to i1
  %m_flipQuadEdges = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 13
  %frombool4 = zext i1 %tobool to i8
  store i8 %frombool4, i8* %m_flipQuadEdges, align 4, !tbaa !30
  %m_useDiamondSubdivision = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 14
  store i8 0, i8* %m_useDiamondSubdivision, align 1, !tbaa !31
  %m_useZigzagSubdivision = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 15
  store i8 0, i8* %m_useZigzagSubdivision, align 2, !tbaa !32
  %12 = load i32, i32* %upAxis.addr, align 4, !tbaa !6
  %m_upAxis = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 16
  store i32 %12, i32* %m_upAxis, align 4, !tbaa !33
  %m_localScaling = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 17
  %13 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !8
  %14 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #8
  store float 1.000000e+00, float* %ref.tmp5, align 4, !tbaa !8
  %15 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  store float 1.000000e+00, float* %ref.tmp6, align 4, !tbaa !8
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_localScaling, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %16 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  %18 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  %m_upAxis7 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 16
  %19 = load i32, i32* %m_upAxis7, align 4, !tbaa !33
  switch i32 %19, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb14
    i32 2, label %sw.bb23
  ]

sw.bb:                                            ; preds = %entry
  %m_localAabbMin = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 1
  %m_minHeight8 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 6
  %20 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #8
  store float 0.000000e+00, float* %ref.tmp9, align 4, !tbaa !8
  %21 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #8
  store float 0.000000e+00, float* %ref.tmp10, align 4, !tbaa !8
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_localAabbMin, float* nonnull align 4 dereferenceable(4) %m_minHeight8, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp10)
  %22 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #8
  %23 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #8
  %m_localAabbMax = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 2
  %m_maxHeight11 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 7
  %m_width12 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 8
  %m_length13 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 9
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_localAabbMax, float* nonnull align 4 dereferenceable(4) %m_maxHeight11, float* nonnull align 4 dereferenceable(4) %m_width12, float* nonnull align 4 dereferenceable(4) %m_length13)
  br label %sw.epilog

sw.bb14:                                          ; preds = %entry
  %m_localAabbMin15 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 1
  %24 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #8
  store float 0.000000e+00, float* %ref.tmp16, align 4, !tbaa !8
  %m_minHeight17 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 6
  %25 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  store float 0.000000e+00, float* %ref.tmp18, align 4, !tbaa !8
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_localAabbMin15, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %m_minHeight17, float* nonnull align 4 dereferenceable(4) %ref.tmp18)
  %26 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #8
  %27 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #8
  %m_localAabbMax19 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 2
  %m_width20 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 8
  %m_maxHeight21 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 7
  %m_length22 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 9
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_localAabbMax19, float* nonnull align 4 dereferenceable(4) %m_width20, float* nonnull align 4 dereferenceable(4) %m_maxHeight21, float* nonnull align 4 dereferenceable(4) %m_length22)
  br label %sw.epilog

sw.bb23:                                          ; preds = %entry
  %m_localAabbMin24 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 1
  %28 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #8
  store float 0.000000e+00, float* %ref.tmp25, align 4, !tbaa !8
  %29 = bitcast float* %ref.tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #8
  store float 0.000000e+00, float* %ref.tmp26, align 4, !tbaa !8
  %m_minHeight27 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 6
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_localAabbMin24, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp26, float* nonnull align 4 dereferenceable(4) %m_minHeight27)
  %30 = bitcast float* %ref.tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #8
  %31 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #8
  %m_localAabbMax28 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 2
  %m_width29 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 8
  %m_length30 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 9
  %m_maxHeight31 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 7
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_localAabbMax28, float* nonnull align 4 dereferenceable(4) %m_width29, float* nonnull align 4 dereferenceable(4) %m_length30, float* nonnull align 4 dereferenceable(4) %m_maxHeight31)
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb23, %sw.bb14, %sw.bb
  %32 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %32) #8
  %33 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #8
  store float 5.000000e-01, float* %ref.tmp33, align 4, !tbaa !8
  %34 = bitcast %class.btVector3* %ref.tmp34 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %34) #8
  %m_localAabbMin35 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 1
  %m_localAabbMax36 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp34, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localAabbMin35, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localAabbMax36)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp32, float* nonnull align 4 dereferenceable(4) %ref.tmp33, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp34)
  %m_localOrigin = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 3
  %35 = bitcast %class.btVector3* %m_localOrigin to i8*
  %36 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %35, i8* align 4 %36, i32 16, i1 false), !tbaa.struct !34
  %37 = bitcast %class.btVector3* %ref.tmp34 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %37) #8
  %38 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #8
  %39 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %39) #8
  ret void
}

define hidden %class.btHeightfieldTerrainShape* @_ZN25btHeightfieldTerrainShapeC2EiiPKvfibb(%class.btHeightfieldTerrainShape* returned %this, i32 %heightStickWidth, i32 %heightStickLength, i8* %heightfieldData, float %maxHeight, i32 %upAxis, i1 zeroext %useFloatData, i1 zeroext %flipQuadEdges) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btHeightfieldTerrainShape*, align 4
  %heightStickWidth.addr = alloca i32, align 4
  %heightStickLength.addr = alloca i32, align 4
  %heightfieldData.addr = alloca i8*, align 4
  %maxHeight.addr = alloca float, align 4
  %upAxis.addr = alloca i32, align 4
  %useFloatData.addr = alloca i8, align 1
  %flipQuadEdges.addr = alloca i8, align 1
  %hdt = alloca i32, align 4
  %minHeight = alloca float, align 4
  %heightScale = alloca float, align 4
  store %class.btHeightfieldTerrainShape* %this, %class.btHeightfieldTerrainShape** %this.addr, align 4, !tbaa !2
  store i32 %heightStickWidth, i32* %heightStickWidth.addr, align 4, !tbaa !6
  store i32 %heightStickLength, i32* %heightStickLength.addr, align 4, !tbaa !6
  store i8* %heightfieldData, i8** %heightfieldData.addr, align 4, !tbaa !2
  store float %maxHeight, float* %maxHeight.addr, align 4, !tbaa !8
  store i32 %upAxis, i32* %upAxis.addr, align 4, !tbaa !6
  %frombool = zext i1 %useFloatData to i8
  store i8 %frombool, i8* %useFloatData.addr, align 1, !tbaa !12
  %frombool1 = zext i1 %flipQuadEdges to i8
  store i8 %frombool1, i8* %flipQuadEdges.addr, align 1, !tbaa !12
  %this2 = load %class.btHeightfieldTerrainShape*, %class.btHeightfieldTerrainShape** %this.addr, align 4
  %0 = bitcast %class.btHeightfieldTerrainShape* %this2 to %class.btConcaveShape*
  %call = call %class.btConcaveShape* @_ZN14btConcaveShapeC2Ev(%class.btConcaveShape* %0)
  %1 = bitcast %class.btHeightfieldTerrainShape* %this2 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [20 x i8*] }, { [20 x i8*] }* @_ZTV25btHeightfieldTerrainShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !14
  %m_localAabbMin = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this2, i32 0, i32 1
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_localAabbMin)
  %m_localAabbMax = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this2, i32 0, i32 2
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_localAabbMax)
  %m_localOrigin = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this2, i32 0, i32 3
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_localOrigin)
  %m_localScaling = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this2, i32 0, i32 17
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_localScaling)
  %2 = bitcast i32* %hdt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load i8, i8* %useFloatData.addr, align 1, !tbaa !12, !range !16
  %tobool = trunc i8 %3 to i1
  %4 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 0, i32 5
  store i32 %cond, i32* %hdt, align 4, !tbaa !10
  %5 = bitcast float* %minHeight to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  store float 0.000000e+00, float* %minHeight, align 4, !tbaa !8
  %6 = bitcast float* %heightScale to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load float, float* %maxHeight.addr, align 4, !tbaa !8
  %div = fdiv float %7, 6.553500e+04
  store float %div, float* %heightScale, align 4, !tbaa !8
  %8 = load i32, i32* %heightStickWidth.addr, align 4, !tbaa !6
  %9 = load i32, i32* %heightStickLength.addr, align 4, !tbaa !6
  %10 = load i8*, i8** %heightfieldData.addr, align 4, !tbaa !2
  %11 = load float, float* %heightScale, align 4, !tbaa !8
  %12 = load float, float* %minHeight, align 4, !tbaa !8
  %13 = load float, float* %maxHeight.addr, align 4, !tbaa !8
  %14 = load i32, i32* %upAxis.addr, align 4, !tbaa !6
  %15 = load i32, i32* %hdt, align 4, !tbaa !10
  %16 = load i8, i8* %flipQuadEdges.addr, align 1, !tbaa !12, !range !16
  %tobool7 = trunc i8 %16 to i1
  call void @_ZN25btHeightfieldTerrainShape10initializeEiiPKvfffi14PHY_ScalarTypeb(%class.btHeightfieldTerrainShape* %this2, i32 %8, i32 %9, i8* %10, float %11, float %12, float %13, i32 %14, i32 %15, i1 zeroext %tobool7)
  %17 = bitcast float* %heightScale to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  %18 = bitcast float* %minHeight to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  %19 = bitcast i32* %hdt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  ret %class.btHeightfieldTerrainShape* %this2
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !8
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !8
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !8
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !8
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !8
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !8
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !8
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !8
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: nounwind
declare %class.btConcaveShape* @_ZN14btConcaveShapeD2Ev(%class.btConcaveShape* returned) unnamed_addr #5

; Function Attrs: nounwind
define hidden %class.btHeightfieldTerrainShape* @_ZN25btHeightfieldTerrainShapeD2Ev(%class.btHeightfieldTerrainShape* returned %this) unnamed_addr #6 {
entry:
  %this.addr = alloca %class.btHeightfieldTerrainShape*, align 4
  store %class.btHeightfieldTerrainShape* %this, %class.btHeightfieldTerrainShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHeightfieldTerrainShape*, %class.btHeightfieldTerrainShape** %this.addr, align 4
  %0 = bitcast %class.btHeightfieldTerrainShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [20 x i8*] }, { [20 x i8*] }* @_ZTV25btHeightfieldTerrainShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !14
  %1 = bitcast %class.btHeightfieldTerrainShape* %this1 to %class.btConcaveShape*
  %call = call %class.btConcaveShape* @_ZN14btConcaveShapeD2Ev(%class.btConcaveShape* %1) #8
  ret %class.btHeightfieldTerrainShape* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN25btHeightfieldTerrainShapeD0Ev(%class.btHeightfieldTerrainShape* %this) unnamed_addr #6 {
entry:
  %this.addr = alloca %class.btHeightfieldTerrainShape*, align 4
  store %class.btHeightfieldTerrainShape* %this, %class.btHeightfieldTerrainShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHeightfieldTerrainShape*, %class.btHeightfieldTerrainShape** %this.addr, align 4
  %call = call %class.btHeightfieldTerrainShape* @_ZN25btHeightfieldTerrainShapeD1Ev(%class.btHeightfieldTerrainShape* %this1) #8
  %0 = bitcast %class.btHeightfieldTerrainShape* %this1 to i8*
  call void @_ZN25btHeightfieldTerrainShapedlEPv(i8* %0) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN25btHeightfieldTerrainShapedlEPv(i8* %ptr) #2 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

define hidden void @_ZNK25btHeightfieldTerrainShape7getAabbERK11btTransformR9btVector3S4_(%class.btHeightfieldTerrainShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btHeightfieldTerrainShape*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %halfExtents = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca float, align 4
  %localOrigin = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %abs_b = alloca %class.btMatrix3x3, align 4
  %center = alloca %class.btVector3, align 4
  %extent = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp22 = alloca float, align 4
  %ref.tmp28 = alloca %class.btVector3, align 4
  %ref.tmp29 = alloca %class.btVector3, align 4
  store %class.btHeightfieldTerrainShape* %this, %class.btHeightfieldTerrainShape** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btHeightfieldTerrainShape*, %class.btHeightfieldTerrainShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %halfExtents to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #8
  %2 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #8
  %m_localAabbMax = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 2
  %m_localAabbMin = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localAabbMin)
  %m_localScaling = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 17
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store float 5.000000e-01, float* %ref.tmp3, align 4, !tbaa !8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %halfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %4 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #8
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %6) #8
  %7 = bitcast %class.btVector3* %localOrigin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #8
  %8 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !8
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  store float 0.000000e+00, float* %ref.tmp5, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %localOrigin, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %11 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  %12 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  %13 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  %m_minHeight = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 6
  %14 = load float, float* %m_minHeight, align 4, !tbaa !23
  %m_maxHeight = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 7
  %15 = load float, float* %m_maxHeight, align 4, !tbaa !24
  %add = fadd float %14, %15
  %mul = fmul float %add, 5.000000e-01
  %call7 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localOrigin)
  %m_upAxis = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 16
  %16 = load i32, i32* %m_upAxis, align 4, !tbaa !33
  %arrayidx = getelementptr inbounds float, float* %call7, i32 %16
  store float %mul, float* %arrayidx, align 4, !tbaa !8
  %m_localScaling8 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 17
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKS_(%class.btVector3* %localOrigin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling8)
  %17 = bitcast %class.btMatrix3x3* %abs_b to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %17) #8
  %18 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %call10 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %18)
  call void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* sret align 4 %abs_b, %class.btMatrix3x3* %call10)
  %19 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %19) #8
  %20 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %20)
  %21 = bitcast %class.btVector3* %center to i8*
  %22 = bitcast %class.btVector3* %call11 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false), !tbaa.struct !34
  %23 = bitcast %class.btVector3* %extent to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %23) #8
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 0)
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 1)
  %call14 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %extent, %class.btVector3* %halfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %call12, %class.btVector3* nonnull align 4 dereferenceable(16) %call13, %class.btVector3* nonnull align 4 dereferenceable(16) %call14)
  %24 = bitcast %class.btVector3* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %24) #8
  %25 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  %26 = bitcast %class.btHeightfieldTerrainShape* %this1 to %class.btConcaveShape*
  %27 = bitcast %class.btConcaveShape* %26 to float (%class.btConcaveShape*)***
  %vtable = load float (%class.btConcaveShape*)**, float (%class.btConcaveShape*)*** %27, align 4, !tbaa !14
  %vfn = getelementptr inbounds float (%class.btConcaveShape*)*, float (%class.btConcaveShape*)** %vtable, i64 12
  %28 = load float (%class.btConcaveShape*)*, float (%class.btConcaveShape*)** %vfn, align 4
  %call17 = call float %28(%class.btConcaveShape* %26)
  store float %call17, float* %ref.tmp16, align 4, !tbaa !8
  %29 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #8
  %30 = bitcast %class.btHeightfieldTerrainShape* %this1 to %class.btConcaveShape*
  %31 = bitcast %class.btConcaveShape* %30 to float (%class.btConcaveShape*)***
  %vtable19 = load float (%class.btConcaveShape*)**, float (%class.btConcaveShape*)*** %31, align 4, !tbaa !14
  %vfn20 = getelementptr inbounds float (%class.btConcaveShape*)*, float (%class.btConcaveShape*)** %vtable19, i64 12
  %32 = load float (%class.btConcaveShape*)*, float (%class.btConcaveShape*)** %vfn20, align 4
  %call21 = call float %32(%class.btConcaveShape* %30)
  store float %call21, float* %ref.tmp18, align 4, !tbaa !8
  %33 = bitcast float* %ref.tmp22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #8
  %34 = bitcast %class.btHeightfieldTerrainShape* %this1 to %class.btConcaveShape*
  %35 = bitcast %class.btConcaveShape* %34 to float (%class.btConcaveShape*)***
  %vtable23 = load float (%class.btConcaveShape*)**, float (%class.btConcaveShape*)*** %35, align 4, !tbaa !14
  %vfn24 = getelementptr inbounds float (%class.btConcaveShape*)*, float (%class.btConcaveShape*)** %vtable23, i64 12
  %36 = load float (%class.btConcaveShape*)*, float (%class.btConcaveShape*)** %vfn24, align 4
  %call25 = call float %36(%class.btConcaveShape* %34)
  store float %call25, float* %ref.tmp22, align 4, !tbaa !8
  %call26 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp22)
  %call27 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %extent, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp15)
  %37 = bitcast float* %ref.tmp22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #8
  %38 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #8
  %39 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #8
  %40 = bitcast %class.btVector3* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %40) #8
  %41 = bitcast %class.btVector3* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %41) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp28, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %42 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %43 = bitcast %class.btVector3* %42 to i8*
  %44 = bitcast %class.btVector3* %ref.tmp28 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %43, i8* align 4 %44, i32 16, i1 false), !tbaa.struct !34
  %45 = bitcast %class.btVector3* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %45) #8
  %46 = bitcast %class.btVector3* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %46) #8
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp29, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %47 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %48 = bitcast %class.btVector3* %47 to i8*
  %49 = bitcast %class.btVector3* %ref.tmp29 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %48, i8* align 4 %49, i32 16, i1 false), !tbaa.struct !34
  %50 = bitcast %class.btVector3* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %50) #8
  %51 = bitcast %class.btVector3* %extent to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %51) #8
  %52 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %52) #8
  %53 = bitcast %class.btMatrix3x3* %abs_b to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %53) #8
  %54 = bitcast %class.btVector3* %localOrigin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %54) #8
  %55 = bitcast %class.btVector3* %halfExtents to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %55) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #4 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !8
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !8
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !8
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !8
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !8
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !8
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !8
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !8
  %mul8 = fmul float %7, %9
  store float %mul8, float* %ref.tmp3, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !8
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !8
  %mul14 = fmul float %12, %14
  store float %mul14, float* %ref.tmp9, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !8
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !8
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !8
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !8
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !8
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !8
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !8
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !8
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !8
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !8
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx3, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !8
  %mul8 = fmul float %5, %4
  store float %mul8, float* %arrayidx7, align 4, !tbaa !8
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !8
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !8
  %mul13 = fmul float %8, %7
  store float %mul13, float* %arrayidx12, align 4, !tbaa !8
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #4 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %2 = load float, float* %call, align 4, !tbaa !8
  %call2 = call float @_Z6btFabsf(float %2)
  store float %call2, float* %ref.tmp, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 0
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx5)
  %4 = load float, float* %call6, align 4, !tbaa !8
  %call7 = call float @_Z6btFabsf(float %4)
  store float %call7, float* %ref.tmp3, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 0
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx10)
  %6 = load float, float* %call11, align 4, !tbaa !8
  %call12 = call float @_Z6btFabsf(float %6)
  store float %call12, float* %ref.tmp8, align 4, !tbaa !8
  %7 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 1
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx15)
  %8 = load float, float* %call16, align 4, !tbaa !8
  %call17 = call float @_Z6btFabsf(float %8)
  store float %call17, float* %ref.tmp13, align 4, !tbaa !8
  %9 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  %m_el19 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el19, i32 0, i32 1
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx20)
  %10 = load float, float* %call21, align 4, !tbaa !8
  %call22 = call float @_Z6btFabsf(float %10)
  store float %call22, float* %ref.tmp18, align 4, !tbaa !8
  %11 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  %m_el24 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el24, i32 0, i32 1
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx25)
  %12 = load float, float* %call26, align 4, !tbaa !8
  %call27 = call float @_Z6btFabsf(float %12)
  store float %call27, float* %ref.tmp23, align 4, !tbaa !8
  %13 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %m_el29 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el29, i32 0, i32 2
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx30)
  %14 = load float, float* %call31, align 4, !tbaa !8
  %call32 = call float @_Z6btFabsf(float %14)
  store float %call32, float* %ref.tmp28, align 4, !tbaa !8
  %15 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  %m_el34 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx35 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el34, i32 0, i32 2
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx35)
  %16 = load float, float* %call36, align 4, !tbaa !8
  %call37 = call float @_Z6btFabsf(float %16)
  store float %call37, float* %ref.tmp33, align 4, !tbaa !8
  %17 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #8
  %m_el39 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx40 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el39, i32 0, i32 2
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx40)
  %18 = load float, float* %call41, align 4, !tbaa !8
  %call42 = call float @_Z6btFabsf(float %18)
  store float %call42, float* %ref.tmp38, align 4, !tbaa !8
  %call43 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp33, float* nonnull align 4 dereferenceable(4) %ref.tmp38)
  %19 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  %20 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #8
  %21 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  %22 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #8
  %23 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #8
  %24 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #8
  %25 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #8
  %26 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #8
  %27 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4, !tbaa !8
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !8
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !8
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !8
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !8
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !8
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !8
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !8
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !8
  ret %class.btVector3* %this1
}

; Function Attrs: nounwind
define hidden float @_ZNK25btHeightfieldTerrainShape22getRawHeightFieldValueEii(%class.btHeightfieldTerrainShape* %this, i32 %x, i32 %y) unnamed_addr #6 {
entry:
  %this.addr = alloca %class.btHeightfieldTerrainShape*, align 4
  %x.addr = alloca i32, align 4
  %y.addr = alloca i32, align 4
  %val = alloca float, align 4
  %heightFieldValue = alloca i8, align 1
  %hfValue = alloca i16, align 2
  store %class.btHeightfieldTerrainShape* %this, %class.btHeightfieldTerrainShape** %this.addr, align 4, !tbaa !2
  store i32 %x, i32* %x.addr, align 4, !tbaa !6
  store i32 %y, i32* %y.addr, align 4, !tbaa !6
  %this1 = load %class.btHeightfieldTerrainShape*, %class.btHeightfieldTerrainShape** %this.addr, align 4
  %0 = bitcast float* %val to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 0.000000e+00, float* %val, align 4, !tbaa !8
  %m_heightDataType = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 12
  %1 = load i32, i32* %m_heightDataType, align 4, !tbaa !29
  switch i32 %1, label %sw.default [
    i32 0, label %sw.bb
    i32 5, label %sw.bb2
    i32 3, label %sw.bb9
  ]

sw.bb:                                            ; preds = %entry
  %2 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 11
  %m_heightfieldDataFloat = bitcast %union.anon* %2 to float**
  %3 = load float*, float** %m_heightfieldDataFloat, align 4, !tbaa !28
  %4 = load i32, i32* %y.addr, align 4, !tbaa !6
  %m_heightStickWidth = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 4
  %5 = load i32, i32* %m_heightStickWidth, align 4, !tbaa !19
  %mul = mul nsw i32 %4, %5
  %6 = load i32, i32* %x.addr, align 4, !tbaa !6
  %add = add nsw i32 %mul, %6
  %arrayidx = getelementptr inbounds float, float* %3, i32 %add
  %7 = load float, float* %arrayidx, align 4, !tbaa !8
  store float %7, float* %val, align 4, !tbaa !8
  br label %sw.epilog

sw.bb2:                                           ; preds = %entry
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %heightFieldValue) #8
  %8 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 11
  %m_heightfieldDataUnsignedChar = bitcast %union.anon* %8 to i8**
  %9 = load i8*, i8** %m_heightfieldDataUnsignedChar, align 4, !tbaa !28
  %10 = load i32, i32* %y.addr, align 4, !tbaa !6
  %m_heightStickWidth3 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 4
  %11 = load i32, i32* %m_heightStickWidth3, align 4, !tbaa !19
  %mul4 = mul nsw i32 %10, %11
  %12 = load i32, i32* %x.addr, align 4, !tbaa !6
  %add5 = add nsw i32 %mul4, %12
  %arrayidx6 = getelementptr inbounds i8, i8* %9, i32 %add5
  %13 = load i8, i8* %arrayidx6, align 1, !tbaa !28
  store i8 %13, i8* %heightFieldValue, align 1, !tbaa !28
  %14 = load i8, i8* %heightFieldValue, align 1, !tbaa !28
  %conv = zext i8 %14 to i32
  %conv7 = sitofp i32 %conv to float
  %m_heightScale = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 10
  %15 = load float, float* %m_heightScale, align 4, !tbaa !27
  %mul8 = fmul float %conv7, %15
  store float %mul8, float* %val, align 4, !tbaa !8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %heightFieldValue) #8
  br label %sw.epilog

sw.bb9:                                           ; preds = %entry
  %16 = bitcast i16* %hfValue to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %16) #8
  %17 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 11
  %m_heightfieldDataShort = bitcast %union.anon* %17 to i16**
  %18 = load i16*, i16** %m_heightfieldDataShort, align 4, !tbaa !28
  %19 = load i32, i32* %y.addr, align 4, !tbaa !6
  %m_heightStickWidth10 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 4
  %20 = load i32, i32* %m_heightStickWidth10, align 4, !tbaa !19
  %mul11 = mul nsw i32 %19, %20
  %21 = load i32, i32* %x.addr, align 4, !tbaa !6
  %add12 = add nsw i32 %mul11, %21
  %arrayidx13 = getelementptr inbounds i16, i16* %18, i32 %add12
  %22 = load i16, i16* %arrayidx13, align 2, !tbaa !35
  store i16 %22, i16* %hfValue, align 2, !tbaa !35
  %23 = load i16, i16* %hfValue, align 2, !tbaa !35
  %conv14 = sext i16 %23 to i32
  %conv15 = sitofp i32 %conv14 to float
  %m_heightScale16 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 10
  %24 = load float, float* %m_heightScale16, align 4, !tbaa !27
  %mul17 = fmul float %conv15, %24
  store float %mul17, float* %val, align 4, !tbaa !8
  %25 = bitcast i16* %hfValue to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %25) #8
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb9, %sw.bb2, %sw.bb
  %26 = load float, float* %val, align 4, !tbaa !8
  %27 = bitcast float* %val to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #8
  ret float %26
}

define hidden void @_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3(%class.btHeightfieldTerrainShape* %this, i32 %x, i32 %y, %class.btVector3* nonnull align 4 dereferenceable(16) %vertex) #0 {
entry:
  %this.addr = alloca %class.btHeightfieldTerrainShape*, align 4
  %x.addr = alloca i32, align 4
  %y.addr = alloca i32, align 4
  %vertex.addr = alloca %class.btVector3*, align 4
  %height = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp39 = alloca float, align 4
  store %class.btHeightfieldTerrainShape* %this, %class.btHeightfieldTerrainShape** %this.addr, align 4, !tbaa !2
  store i32 %x, i32* %x.addr, align 4, !tbaa !6
  store i32 %y, i32* %y.addr, align 4, !tbaa !6
  store %class.btVector3* %vertex, %class.btVector3** %vertex.addr, align 4, !tbaa !2
  %this1 = load %class.btHeightfieldTerrainShape*, %class.btHeightfieldTerrainShape** %this.addr, align 4
  %0 = bitcast float* %height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %x.addr, align 4, !tbaa !6
  %2 = load i32, i32* %y.addr, align 4, !tbaa !6
  %3 = bitcast %class.btHeightfieldTerrainShape* %this1 to float (%class.btHeightfieldTerrainShape*, i32, i32)***
  %vtable = load float (%class.btHeightfieldTerrainShape*, i32, i32)**, float (%class.btHeightfieldTerrainShape*, i32, i32)*** %3, align 4, !tbaa !14
  %vfn = getelementptr inbounds float (%class.btHeightfieldTerrainShape*, i32, i32)*, float (%class.btHeightfieldTerrainShape*, i32, i32)** %vtable, i64 17
  %4 = load float (%class.btHeightfieldTerrainShape*, i32, i32)*, float (%class.btHeightfieldTerrainShape*, i32, i32)** %vfn, align 4
  %call = call float %4(%class.btHeightfieldTerrainShape* %this1, i32 %1, i32 %2)
  store float %call, float* %height, align 4, !tbaa !8
  %m_upAxis = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 16
  %5 = load i32, i32* %m_upAxis, align 4, !tbaa !33
  switch i32 %5, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb9
    i32 2, label %sw.bb26
  ]

sw.bb:                                            ; preds = %entry
  %6 = load %class.btVector3*, %class.btVector3** %vertex.addr, align 4, !tbaa !2
  %7 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %8 = load float, float* %height, align 4, !tbaa !8
  %m_localOrigin = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 3
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %m_localOrigin)
  %9 = load float, float* %call2, align 4, !tbaa !8
  %sub = fsub float %8, %9
  store float %sub, float* %ref.tmp, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %m_width = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 8
  %11 = load float, float* %m_width, align 4, !tbaa !25
  %fneg = fneg float %11
  %div = fdiv float %fneg, 2.000000e+00
  %12 = load i32, i32* %x.addr, align 4, !tbaa !6
  %conv = sitofp i32 %12 to float
  %add = fadd float %div, %conv
  store float %add, float* %ref.tmp3, align 4, !tbaa !8
  %13 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %m_length = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 9
  %14 = load float, float* %m_length, align 4, !tbaa !26
  %fneg5 = fneg float %14
  %div6 = fdiv float %fneg5, 2.000000e+00
  %15 = load i32, i32* %y.addr, align 4, !tbaa !6
  %conv7 = sitofp i32 %15 to float
  %add8 = fadd float %div6, %conv7
  store float %add8, float* %ref.tmp4, align 4, !tbaa !8
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %6, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %16 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  %18 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  br label %sw.epilog

sw.bb9:                                           ; preds = %entry
  %19 = load %class.btVector3*, %class.btVector3** %vertex.addr, align 4, !tbaa !2
  %20 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #8
  %m_width11 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 8
  %21 = load float, float* %m_width11, align 4, !tbaa !25
  %fneg12 = fneg float %21
  %div13 = fdiv float %fneg12, 2.000000e+00
  %22 = load i32, i32* %x.addr, align 4, !tbaa !6
  %conv14 = sitofp i32 %22 to float
  %add15 = fadd float %div13, %conv14
  store float %add15, float* %ref.tmp10, align 4, !tbaa !8
  %23 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #8
  %24 = load float, float* %height, align 4, !tbaa !8
  %m_localOrigin17 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 3
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %m_localOrigin17)
  %25 = load float, float* %call18, align 4, !tbaa !8
  %sub19 = fsub float %24, %25
  store float %sub19, float* %ref.tmp16, align 4, !tbaa !8
  %26 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #8
  %m_length21 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 9
  %27 = load float, float* %m_length21, align 4, !tbaa !26
  %fneg22 = fneg float %27
  %div23 = fdiv float %fneg22, 2.000000e+00
  %28 = load i32, i32* %y.addr, align 4, !tbaa !6
  %conv24 = sitofp i32 %28 to float
  %add25 = fadd float %div23, %conv24
  store float %add25, float* %ref.tmp20, align 4, !tbaa !8
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %19, float* nonnull align 4 dereferenceable(4) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp20)
  %29 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #8
  %30 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #8
  %31 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #8
  br label %sw.epilog

sw.bb26:                                          ; preds = %entry
  %32 = load %class.btVector3*, %class.btVector3** %vertex.addr, align 4, !tbaa !2
  %33 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #8
  %m_width28 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 8
  %34 = load float, float* %m_width28, align 4, !tbaa !25
  %fneg29 = fneg float %34
  %div30 = fdiv float %fneg29, 2.000000e+00
  %35 = load i32, i32* %x.addr, align 4, !tbaa !6
  %conv31 = sitofp i32 %35 to float
  %add32 = fadd float %div30, %conv31
  store float %add32, float* %ref.tmp27, align 4, !tbaa !8
  %36 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #8
  %m_length34 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 9
  %37 = load float, float* %m_length34, align 4, !tbaa !26
  %fneg35 = fneg float %37
  %div36 = fdiv float %fneg35, 2.000000e+00
  %38 = load i32, i32* %y.addr, align 4, !tbaa !6
  %conv37 = sitofp i32 %38 to float
  %add38 = fadd float %div36, %conv37
  store float %add38, float* %ref.tmp33, align 4, !tbaa !8
  %39 = bitcast float* %ref.tmp39 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #8
  %40 = load float, float* %height, align 4, !tbaa !8
  %m_localOrigin40 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 3
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %m_localOrigin40)
  %41 = load float, float* %call41, align 4, !tbaa !8
  %sub42 = fsub float %40, %41
  store float %sub42, float* %ref.tmp39, align 4, !tbaa !8
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %32, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp33, float* nonnull align 4 dereferenceable(4) %ref.tmp39)
  %42 = bitcast float* %ref.tmp39 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #8
  %43 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #8
  %44 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #8
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb26, %sw.bb9, %sw.bb
  %m_localScaling = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 17
  %45 = load %class.btVector3*, %class.btVector3** %vertex.addr, align 4, !tbaa !2
  %call43 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKS_(%class.btVector3* %45, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling)
  %46 = bitcast float* %height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

define hidden void @_ZNK25btHeightfieldTerrainShape17quantizeWithClampEPiRK9btVector3i(%class.btHeightfieldTerrainShape* %this, i32* %out, %class.btVector3* nonnull align 4 dereferenceable(16) %point, i32 %0) #0 {
entry:
  %this.addr = alloca %class.btHeightfieldTerrainShape*, align 4
  %out.addr = alloca i32*, align 4
  %point.addr = alloca %class.btVector3*, align 4
  %.addr = alloca i32, align 4
  %clampedPoint = alloca %class.btVector3, align 4
  store %class.btHeightfieldTerrainShape* %this, %class.btHeightfieldTerrainShape** %this.addr, align 4, !tbaa !2
  store i32* %out, i32** %out.addr, align 4, !tbaa !2
  store %class.btVector3* %point, %class.btVector3** %point.addr, align 4, !tbaa !2
  store i32 %0, i32* %.addr, align 4, !tbaa !6
  %this1 = load %class.btHeightfieldTerrainShape*, %class.btHeightfieldTerrainShape** %this.addr, align 4
  %1 = bitcast %class.btVector3* %clampedPoint to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #8
  %2 = load %class.btVector3*, %class.btVector3** %point.addr, align 4, !tbaa !2
  %3 = bitcast %class.btVector3* %clampedPoint to i8*
  %4 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false), !tbaa.struct !34
  %m_localAabbMin = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 1
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %clampedPoint, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localAabbMin)
  %m_localAabbMax = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 2
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %clampedPoint, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localAabbMax)
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %clampedPoint)
  %5 = load float, float* %call, align 4, !tbaa !8
  %call2 = call i32 @_ZL12getQuantizedf(float %5)
  %6 = load i32*, i32** %out.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i32, i32* %6, i32 0
  store i32 %call2, i32* %arrayidx, align 4, !tbaa !6
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %clampedPoint)
  %7 = load float, float* %call3, align 4, !tbaa !8
  %call4 = call i32 @_ZL12getQuantizedf(float %7)
  %8 = load i32*, i32** %out.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i32, i32* %8, i32 1
  store i32 %call4, i32* %arrayidx5, align 4, !tbaa !6
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %clampedPoint)
  %9 = load float, float* %call6, align 4, !tbaa !8
  %call7 = call i32 @_ZL12getQuantizedf(float %9)
  %10 = load i32*, i32** %out.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i32, i32* %10, i32 2
  store i32 %call7, i32* %arrayidx8, align 4, !tbaa !6
  %11 = bitcast %class.btVector3* %clampedPoint to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %11) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN9btVector36setMaxERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7)
  %m_floats8 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 3
  %3 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %3)
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN9btVector36setMinERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7)
  %m_floats8 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 3
  %3 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %3)
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @_ZL12getQuantizedf(float %x) #2 {
entry:
  %retval = alloca i32, align 4
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !8
  %0 = load float, float* %x.addr, align 4, !tbaa !8
  %conv = fpext float %0 to double
  %cmp = fcmp olt double %conv, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load float, float* %x.addr, align 4, !tbaa !8
  %conv1 = fpext float %1 to double
  %sub = fsub double %conv1, 5.000000e-01
  %conv2 = fptosi double %sub to i32
  store i32 %conv2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load float, float* %x.addr, align 4, !tbaa !8
  %conv3 = fpext float %2 to double
  %add = fadd double %conv3, 5.000000e-01
  %conv4 = fptosi double %add to i32
  store i32 %conv4, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

define hidden void @_ZNK25btHeightfieldTerrainShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_(%class.btHeightfieldTerrainShape* %this, %class.btTriangleCallback* %callback, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btHeightfieldTerrainShape*, align 4
  %callback.addr = alloca %class.btTriangleCallback*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %localAabbMin = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %localAabbMax = alloca %class.btVector3, align 4
  %ref.tmp14 = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %quantizedAabbMin = alloca [3 x i32], align 4
  %quantizedAabbMax = alloca [3 x i32], align 4
  %i = alloca i32, align 4
  %startX = alloca i32, align 4
  %endX = alloca i32, align 4
  %startJ = alloca i32, align 4
  %endJ = alloca i32, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x = alloca i32, align 4
  %vertices = alloca [3 x %class.btVector3], align 16
  store %class.btHeightfieldTerrainShape* %this, %class.btHeightfieldTerrainShape** %this.addr, align 4, !tbaa !2
  store %class.btTriangleCallback* %callback, %class.btTriangleCallback** %callback.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btHeightfieldTerrainShape*, %class.btHeightfieldTerrainShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %localAabbMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #8
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %m_localScaling = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 17
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_localScaling)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %4 = load float, float* %arrayidx, align 4, !tbaa !8
  %div = fdiv float 1.000000e+00, %4
  store float %div, float* %ref.tmp2, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %m_localScaling4 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 17
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_localScaling4)
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 1
  %6 = load float, float* %arrayidx6, align 4, !tbaa !8
  %div7 = fdiv float 1.000000e+00, %6
  store float %div7, float* %ref.tmp3, align 4, !tbaa !8
  %7 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %m_localScaling9 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 17
  %call10 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_localScaling9)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 2
  %8 = load float, float* %arrayidx11, align 4, !tbaa !8
  %div12 = fdiv float 1.000000e+00, %8
  store float %div12, float* %ref.tmp8, align 4, !tbaa !8
  %call13 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %9 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  %12 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %12) #8
  %13 = bitcast %class.btVector3* %localAabbMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #8
  %14 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %15 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %15) #8
  %16 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  %m_localScaling16 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 17
  %call17 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_localScaling16)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 0
  %17 = load float, float* %arrayidx18, align 4, !tbaa !8
  %div19 = fdiv float 1.000000e+00, %17
  store float %div19, float* %ref.tmp15, align 4, !tbaa !8
  %18 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #8
  %m_localScaling21 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 17
  %call22 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_localScaling21)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 1
  %19 = load float, float* %arrayidx23, align 4, !tbaa !8
  %div24 = fdiv float 1.000000e+00, %19
  store float %div24, float* %ref.tmp20, align 4, !tbaa !8
  %20 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #8
  %m_localScaling26 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 17
  %call27 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_localScaling26)
  %arrayidx28 = getelementptr inbounds float, float* %call27, i32 2
  %21 = load float, float* %arrayidx28, align 4, !tbaa !8
  %div29 = fdiv float 1.000000e+00, %21
  store float %div29, float* %ref.tmp25, align 4, !tbaa !8
  %call30 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp25)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %localAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %14, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp14)
  %22 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #8
  %23 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #8
  %24 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #8
  %25 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %25) #8
  %m_localOrigin = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 3
  %call31 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localOrigin)
  %m_localOrigin32 = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 3
  %call33 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %localAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localOrigin32)
  %26 = bitcast [3 x i32]* %quantizedAabbMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %26) #8
  %27 = bitcast [3 x i32]* %quantizedAabbMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %27) #8
  %arraydecay = getelementptr inbounds [3 x i32], [3 x i32]* %quantizedAabbMin, i32 0, i32 0
  call void @_ZNK25btHeightfieldTerrainShape17quantizeWithClampEPiRK9btVector3i(%class.btHeightfieldTerrainShape* %this1, i32* %arraydecay, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin, i32 0)
  %arraydecay34 = getelementptr inbounds [3 x i32], [3 x i32]* %quantizedAabbMax, i32 0, i32 0
  call void @_ZNK25btHeightfieldTerrainShape17quantizeWithClampEPiRK9btVector3i(%class.btHeightfieldTerrainShape* %this1, i32* %arraydecay34, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax, i32 1)
  %28 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #8
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %29 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %29, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %30 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %31 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx35 = getelementptr inbounds [3 x i32], [3 x i32]* %quantizedAabbMin, i32 0, i32 %31
  %32 = load i32, i32* %arrayidx35, align 4, !tbaa !6
  %dec = add nsw i32 %32, -1
  store i32 %dec, i32* %arrayidx35, align 4, !tbaa !6
  %33 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx36 = getelementptr inbounds [3 x i32], [3 x i32]* %quantizedAabbMax, i32 0, i32 %33
  %34 = load i32, i32* %arrayidx36, align 4, !tbaa !6
  %inc = add nsw i32 %34, 1
  store i32 %inc, i32* %arrayidx36, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %35 = load i32, i32* %i, align 4, !tbaa !6
  %inc37 = add nsw i32 %35, 1
  store i32 %inc37, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %36 = bitcast i32* %startX to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #8
  store i32 0, i32* %startX, align 4, !tbaa !6
  %37 = bitcast i32* %endX to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #8
  %m_heightStickWidth = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 4
  %38 = load i32, i32* %m_heightStickWidth, align 4, !tbaa !19
  %sub = sub nsw i32 %38, 1
  store i32 %sub, i32* %endX, align 4, !tbaa !6
  %39 = bitcast i32* %startJ to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #8
  store i32 0, i32* %startJ, align 4, !tbaa !6
  %40 = bitcast i32* %endJ to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #8
  %m_heightStickLength = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 5
  %41 = load i32, i32* %m_heightStickLength, align 4, !tbaa !22
  %sub38 = sub nsw i32 %41, 1
  store i32 %sub38, i32* %endJ, align 4, !tbaa !6
  %m_upAxis = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 16
  %42 = load i32, i32* %m_upAxis, align 4, !tbaa !33
  switch i32 %42, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb57
    i32 2, label %sw.bb78
  ]

sw.bb:                                            ; preds = %for.end
  %arrayidx39 = getelementptr inbounds [3 x i32], [3 x i32]* %quantizedAabbMin, i32 0, i32 1
  %43 = load i32, i32* %arrayidx39, align 4, !tbaa !6
  %44 = load i32, i32* %startX, align 4, !tbaa !6
  %cmp40 = icmp sgt i32 %43, %44
  br i1 %cmp40, label %if.then, label %if.end

if.then:                                          ; preds = %sw.bb
  %arrayidx41 = getelementptr inbounds [3 x i32], [3 x i32]* %quantizedAabbMin, i32 0, i32 1
  %45 = load i32, i32* %arrayidx41, align 4, !tbaa !6
  store i32 %45, i32* %startX, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %sw.bb
  %arrayidx42 = getelementptr inbounds [3 x i32], [3 x i32]* %quantizedAabbMax, i32 0, i32 1
  %46 = load i32, i32* %arrayidx42, align 4, !tbaa !6
  %47 = load i32, i32* %endX, align 4, !tbaa !6
  %cmp43 = icmp slt i32 %46, %47
  br i1 %cmp43, label %if.then44, label %if.end46

if.then44:                                        ; preds = %if.end
  %arrayidx45 = getelementptr inbounds [3 x i32], [3 x i32]* %quantizedAabbMax, i32 0, i32 1
  %48 = load i32, i32* %arrayidx45, align 4, !tbaa !6
  store i32 %48, i32* %endX, align 4, !tbaa !6
  br label %if.end46

if.end46:                                         ; preds = %if.then44, %if.end
  %arrayidx47 = getelementptr inbounds [3 x i32], [3 x i32]* %quantizedAabbMin, i32 0, i32 2
  %49 = load i32, i32* %arrayidx47, align 4, !tbaa !6
  %50 = load i32, i32* %startJ, align 4, !tbaa !6
  %cmp48 = icmp sgt i32 %49, %50
  br i1 %cmp48, label %if.then49, label %if.end51

if.then49:                                        ; preds = %if.end46
  %arrayidx50 = getelementptr inbounds [3 x i32], [3 x i32]* %quantizedAabbMin, i32 0, i32 2
  %51 = load i32, i32* %arrayidx50, align 4, !tbaa !6
  store i32 %51, i32* %startJ, align 4, !tbaa !6
  br label %if.end51

if.end51:                                         ; preds = %if.then49, %if.end46
  %arrayidx52 = getelementptr inbounds [3 x i32], [3 x i32]* %quantizedAabbMax, i32 0, i32 2
  %52 = load i32, i32* %arrayidx52, align 4, !tbaa !6
  %53 = load i32, i32* %endJ, align 4, !tbaa !6
  %cmp53 = icmp slt i32 %52, %53
  br i1 %cmp53, label %if.then54, label %if.end56

if.then54:                                        ; preds = %if.end51
  %arrayidx55 = getelementptr inbounds [3 x i32], [3 x i32]* %quantizedAabbMax, i32 0, i32 2
  %54 = load i32, i32* %arrayidx55, align 4, !tbaa !6
  store i32 %54, i32* %endJ, align 4, !tbaa !6
  br label %if.end56

if.end56:                                         ; preds = %if.then54, %if.end51
  br label %sw.epilog

sw.bb57:                                          ; preds = %for.end
  %arrayidx58 = getelementptr inbounds [3 x i32], [3 x i32]* %quantizedAabbMin, i32 0, i32 0
  %55 = load i32, i32* %arrayidx58, align 4, !tbaa !6
  %56 = load i32, i32* %startX, align 4, !tbaa !6
  %cmp59 = icmp sgt i32 %55, %56
  br i1 %cmp59, label %if.then60, label %if.end62

if.then60:                                        ; preds = %sw.bb57
  %arrayidx61 = getelementptr inbounds [3 x i32], [3 x i32]* %quantizedAabbMin, i32 0, i32 0
  %57 = load i32, i32* %arrayidx61, align 4, !tbaa !6
  store i32 %57, i32* %startX, align 4, !tbaa !6
  br label %if.end62

if.end62:                                         ; preds = %if.then60, %sw.bb57
  %arrayidx63 = getelementptr inbounds [3 x i32], [3 x i32]* %quantizedAabbMax, i32 0, i32 0
  %58 = load i32, i32* %arrayidx63, align 4, !tbaa !6
  %59 = load i32, i32* %endX, align 4, !tbaa !6
  %cmp64 = icmp slt i32 %58, %59
  br i1 %cmp64, label %if.then65, label %if.end67

if.then65:                                        ; preds = %if.end62
  %arrayidx66 = getelementptr inbounds [3 x i32], [3 x i32]* %quantizedAabbMax, i32 0, i32 0
  %60 = load i32, i32* %arrayidx66, align 4, !tbaa !6
  store i32 %60, i32* %endX, align 4, !tbaa !6
  br label %if.end67

if.end67:                                         ; preds = %if.then65, %if.end62
  %arrayidx68 = getelementptr inbounds [3 x i32], [3 x i32]* %quantizedAabbMin, i32 0, i32 2
  %61 = load i32, i32* %arrayidx68, align 4, !tbaa !6
  %62 = load i32, i32* %startJ, align 4, !tbaa !6
  %cmp69 = icmp sgt i32 %61, %62
  br i1 %cmp69, label %if.then70, label %if.end72

if.then70:                                        ; preds = %if.end67
  %arrayidx71 = getelementptr inbounds [3 x i32], [3 x i32]* %quantizedAabbMin, i32 0, i32 2
  %63 = load i32, i32* %arrayidx71, align 4, !tbaa !6
  store i32 %63, i32* %startJ, align 4, !tbaa !6
  br label %if.end72

if.end72:                                         ; preds = %if.then70, %if.end67
  %arrayidx73 = getelementptr inbounds [3 x i32], [3 x i32]* %quantizedAabbMax, i32 0, i32 2
  %64 = load i32, i32* %arrayidx73, align 4, !tbaa !6
  %65 = load i32, i32* %endJ, align 4, !tbaa !6
  %cmp74 = icmp slt i32 %64, %65
  br i1 %cmp74, label %if.then75, label %if.end77

if.then75:                                        ; preds = %if.end72
  %arrayidx76 = getelementptr inbounds [3 x i32], [3 x i32]* %quantizedAabbMax, i32 0, i32 2
  %66 = load i32, i32* %arrayidx76, align 4, !tbaa !6
  store i32 %66, i32* %endJ, align 4, !tbaa !6
  br label %if.end77

if.end77:                                         ; preds = %if.then75, %if.end72
  br label %sw.epilog

sw.bb78:                                          ; preds = %for.end
  %arrayidx79 = getelementptr inbounds [3 x i32], [3 x i32]* %quantizedAabbMin, i32 0, i32 0
  %67 = load i32, i32* %arrayidx79, align 4, !tbaa !6
  %68 = load i32, i32* %startX, align 4, !tbaa !6
  %cmp80 = icmp sgt i32 %67, %68
  br i1 %cmp80, label %if.then81, label %if.end83

if.then81:                                        ; preds = %sw.bb78
  %arrayidx82 = getelementptr inbounds [3 x i32], [3 x i32]* %quantizedAabbMin, i32 0, i32 0
  %69 = load i32, i32* %arrayidx82, align 4, !tbaa !6
  store i32 %69, i32* %startX, align 4, !tbaa !6
  br label %if.end83

if.end83:                                         ; preds = %if.then81, %sw.bb78
  %arrayidx84 = getelementptr inbounds [3 x i32], [3 x i32]* %quantizedAabbMax, i32 0, i32 0
  %70 = load i32, i32* %arrayidx84, align 4, !tbaa !6
  %71 = load i32, i32* %endX, align 4, !tbaa !6
  %cmp85 = icmp slt i32 %70, %71
  br i1 %cmp85, label %if.then86, label %if.end88

if.then86:                                        ; preds = %if.end83
  %arrayidx87 = getelementptr inbounds [3 x i32], [3 x i32]* %quantizedAabbMax, i32 0, i32 0
  %72 = load i32, i32* %arrayidx87, align 4, !tbaa !6
  store i32 %72, i32* %endX, align 4, !tbaa !6
  br label %if.end88

if.end88:                                         ; preds = %if.then86, %if.end83
  %arrayidx89 = getelementptr inbounds [3 x i32], [3 x i32]* %quantizedAabbMin, i32 0, i32 1
  %73 = load i32, i32* %arrayidx89, align 4, !tbaa !6
  %74 = load i32, i32* %startJ, align 4, !tbaa !6
  %cmp90 = icmp sgt i32 %73, %74
  br i1 %cmp90, label %if.then91, label %if.end93

if.then91:                                        ; preds = %if.end88
  %arrayidx92 = getelementptr inbounds [3 x i32], [3 x i32]* %quantizedAabbMin, i32 0, i32 1
  %75 = load i32, i32* %arrayidx92, align 4, !tbaa !6
  store i32 %75, i32* %startJ, align 4, !tbaa !6
  br label %if.end93

if.end93:                                         ; preds = %if.then91, %if.end88
  %arrayidx94 = getelementptr inbounds [3 x i32], [3 x i32]* %quantizedAabbMax, i32 0, i32 1
  %76 = load i32, i32* %arrayidx94, align 4, !tbaa !6
  %77 = load i32, i32* %endJ, align 4, !tbaa !6
  %cmp95 = icmp slt i32 %76, %77
  br i1 %cmp95, label %if.then96, label %if.end98

if.then96:                                        ; preds = %if.end93
  %arrayidx97 = getelementptr inbounds [3 x i32], [3 x i32]* %quantizedAabbMax, i32 0, i32 1
  %78 = load i32, i32* %arrayidx97, align 4, !tbaa !6
  store i32 %78, i32* %endJ, align 4, !tbaa !6
  br label %if.end98

if.end98:                                         ; preds = %if.then96, %if.end93
  br label %sw.epilog

sw.default:                                       ; preds = %for.end
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %if.end98, %if.end77, %if.end56
  %79 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #8
  %80 = load i32, i32* %startJ, align 4, !tbaa !6
  store i32 %80, i32* %j, align 4, !tbaa !6
  br label %for.cond99

for.cond99:                                       ; preds = %for.inc151, %sw.epilog
  %81 = load i32, i32* %j, align 4, !tbaa !6
  %82 = load i32, i32* %endJ, align 4, !tbaa !6
  %cmp100 = icmp slt i32 %81, %82
  br i1 %cmp100, label %for.body102, label %for.cond.cleanup101

for.cond.cleanup101:                              ; preds = %for.cond99
  store i32 6, i32* %cleanup.dest.slot, align 4
  %83 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #8
  br label %for.end153

for.body102:                                      ; preds = %for.cond99
  %84 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %84) #8
  %85 = load i32, i32* %startX, align 4, !tbaa !6
  store i32 %85, i32* %x, align 4, !tbaa !6
  br label %for.cond103

for.cond103:                                      ; preds = %for.inc148, %for.body102
  %86 = load i32, i32* %x, align 4, !tbaa !6
  %87 = load i32, i32* %endX, align 4, !tbaa !6
  %cmp104 = icmp slt i32 %86, %87
  br i1 %cmp104, label %for.body106, label %for.cond.cleanup105

for.cond.cleanup105:                              ; preds = %for.cond103
  store i32 9, i32* %cleanup.dest.slot, align 4
  %88 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #8
  br label %for.end150

for.body106:                                      ; preds = %for.cond103
  %89 = bitcast [3 x %class.btVector3]* %vertices to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %89) #8
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %vertices, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %for.body106
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %for.body106 ], [ %arrayctor.next, %arrayctor.loop ]
  %call107 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_flipQuadEdges = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 13
  %90 = load i8, i8* %m_flipQuadEdges, align 4, !tbaa !30, !range !16
  %tobool = trunc i8 %90 to i1
  br i1 %tobool, label %if.then115, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %arrayctor.cont
  %m_useDiamondSubdivision = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 14
  %91 = load i8, i8* %m_useDiamondSubdivision, align 1, !tbaa !31, !range !16
  %tobool108 = trunc i8 %91 to i1
  br i1 %tobool108, label %land.lhs.true, label %lor.lhs.false110

land.lhs.true:                                    ; preds = %lor.lhs.false
  %92 = load i32, i32* %j, align 4, !tbaa !6
  %93 = load i32, i32* %x, align 4, !tbaa !6
  %add = add nsw i32 %92, %93
  %and = and i32 %add, 1
  %tobool109 = icmp ne i32 %and, 0
  br i1 %tobool109, label %lor.lhs.false110, label %if.then115

lor.lhs.false110:                                 ; preds = %land.lhs.true, %lor.lhs.false
  %m_useZigzagSubdivision = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 15
  %94 = load i8, i8* %m_useZigzagSubdivision, align 2, !tbaa !32, !range !16
  %tobool111 = trunc i8 %94 to i1
  br i1 %tobool111, label %land.lhs.true112, label %if.else

land.lhs.true112:                                 ; preds = %lor.lhs.false110
  %95 = load i32, i32* %j, align 4, !tbaa !6
  %and113 = and i32 %95, 1
  %tobool114 = icmp ne i32 %and113, 0
  br i1 %tobool114, label %if.else, label %if.then115

if.then115:                                       ; preds = %land.lhs.true112, %land.lhs.true, %arrayctor.cont
  %96 = load i32, i32* %x, align 4, !tbaa !6
  %97 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx116 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %vertices, i32 0, i32 0
  call void @_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3(%class.btHeightfieldTerrainShape* %this1, i32 %96, i32 %97, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx116)
  %98 = load i32, i32* %x, align 4, !tbaa !6
  %add117 = add nsw i32 %98, 1
  %99 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx118 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %vertices, i32 0, i32 1
  call void @_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3(%class.btHeightfieldTerrainShape* %this1, i32 %add117, i32 %99, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx118)
  %100 = load i32, i32* %x, align 4, !tbaa !6
  %add119 = add nsw i32 %100, 1
  %101 = load i32, i32* %j, align 4, !tbaa !6
  %add120 = add nsw i32 %101, 1
  %arrayidx121 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %vertices, i32 0, i32 2
  call void @_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3(%class.btHeightfieldTerrainShape* %this1, i32 %add119, i32 %add120, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx121)
  %102 = load %class.btTriangleCallback*, %class.btTriangleCallback** %callback.addr, align 4, !tbaa !2
  %arraydecay122 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %vertices, i32 0, i32 0
  %103 = load i32, i32* %x, align 4, !tbaa !6
  %104 = load i32, i32* %j, align 4, !tbaa !6
  %105 = bitcast %class.btTriangleCallback* %102 to void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)***
  %vtable = load void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)**, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*** %105, align 4, !tbaa !14
  %vfn = getelementptr inbounds void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)** %vtable, i64 2
  %106 = load void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)** %vfn, align 4
  call void %106(%class.btTriangleCallback* %102, %class.btVector3* %arraydecay122, i32 %103, i32 %104)
  %107 = load i32, i32* %x, align 4, !tbaa !6
  %add123 = add nsw i32 %107, 1
  %108 = load i32, i32* %j, align 4, !tbaa !6
  %add124 = add nsw i32 %108, 1
  %arrayidx125 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %vertices, i32 0, i32 1
  call void @_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3(%class.btHeightfieldTerrainShape* %this1, i32 %add123, i32 %add124, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx125)
  %109 = load i32, i32* %x, align 4, !tbaa !6
  %110 = load i32, i32* %j, align 4, !tbaa !6
  %add126 = add nsw i32 %110, 1
  %arrayidx127 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %vertices, i32 0, i32 2
  call void @_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3(%class.btHeightfieldTerrainShape* %this1, i32 %109, i32 %add126, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx127)
  %111 = load %class.btTriangleCallback*, %class.btTriangleCallback** %callback.addr, align 4, !tbaa !2
  %arraydecay128 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %vertices, i32 0, i32 0
  %112 = load i32, i32* %x, align 4, !tbaa !6
  %113 = load i32, i32* %j, align 4, !tbaa !6
  %114 = bitcast %class.btTriangleCallback* %111 to void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)***
  %vtable129 = load void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)**, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*** %114, align 4, !tbaa !14
  %vfn130 = getelementptr inbounds void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)** %vtable129, i64 2
  %115 = load void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)** %vfn130, align 4
  call void %115(%class.btTriangleCallback* %111, %class.btVector3* %arraydecay128, i32 %112, i32 %113)
  br label %if.end147

if.else:                                          ; preds = %land.lhs.true112, %lor.lhs.false110
  %116 = load i32, i32* %x, align 4, !tbaa !6
  %117 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx131 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %vertices, i32 0, i32 0
  call void @_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3(%class.btHeightfieldTerrainShape* %this1, i32 %116, i32 %117, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx131)
  %118 = load i32, i32* %x, align 4, !tbaa !6
  %119 = load i32, i32* %j, align 4, !tbaa !6
  %add132 = add nsw i32 %119, 1
  %arrayidx133 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %vertices, i32 0, i32 1
  call void @_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3(%class.btHeightfieldTerrainShape* %this1, i32 %118, i32 %add132, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx133)
  %120 = load i32, i32* %x, align 4, !tbaa !6
  %add134 = add nsw i32 %120, 1
  %121 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx135 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %vertices, i32 0, i32 2
  call void @_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3(%class.btHeightfieldTerrainShape* %this1, i32 %add134, i32 %121, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx135)
  %122 = load %class.btTriangleCallback*, %class.btTriangleCallback** %callback.addr, align 4, !tbaa !2
  %arraydecay136 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %vertices, i32 0, i32 0
  %123 = load i32, i32* %x, align 4, !tbaa !6
  %124 = load i32, i32* %j, align 4, !tbaa !6
  %125 = bitcast %class.btTriangleCallback* %122 to void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)***
  %vtable137 = load void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)**, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*** %125, align 4, !tbaa !14
  %vfn138 = getelementptr inbounds void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)** %vtable137, i64 2
  %126 = load void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)** %vfn138, align 4
  call void %126(%class.btTriangleCallback* %122, %class.btVector3* %arraydecay136, i32 %123, i32 %124)
  %127 = load i32, i32* %x, align 4, !tbaa !6
  %add139 = add nsw i32 %127, 1
  %128 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx140 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %vertices, i32 0, i32 0
  call void @_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3(%class.btHeightfieldTerrainShape* %this1, i32 %add139, i32 %128, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx140)
  %129 = load i32, i32* %x, align 4, !tbaa !6
  %add141 = add nsw i32 %129, 1
  %130 = load i32, i32* %j, align 4, !tbaa !6
  %add142 = add nsw i32 %130, 1
  %arrayidx143 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %vertices, i32 0, i32 2
  call void @_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3(%class.btHeightfieldTerrainShape* %this1, i32 %add141, i32 %add142, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx143)
  %131 = load %class.btTriangleCallback*, %class.btTriangleCallback** %callback.addr, align 4, !tbaa !2
  %arraydecay144 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %vertices, i32 0, i32 0
  %132 = load i32, i32* %x, align 4, !tbaa !6
  %133 = load i32, i32* %j, align 4, !tbaa !6
  %134 = bitcast %class.btTriangleCallback* %131 to void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)***
  %vtable145 = load void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)**, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*** %134, align 4, !tbaa !14
  %vfn146 = getelementptr inbounds void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)** %vtable145, i64 2
  %135 = load void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)** %vfn146, align 4
  call void %135(%class.btTriangleCallback* %131, %class.btVector3* %arraydecay144, i32 %132, i32 %133)
  br label %if.end147

if.end147:                                        ; preds = %if.else, %if.then115
  %136 = bitcast [3 x %class.btVector3]* %vertices to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %136) #8
  br label %for.inc148

for.inc148:                                       ; preds = %if.end147
  %137 = load i32, i32* %x, align 4, !tbaa !6
  %inc149 = add nsw i32 %137, 1
  store i32 %inc149, i32* %x, align 4, !tbaa !6
  br label %for.cond103

for.end150:                                       ; preds = %for.cond.cleanup105
  br label %for.inc151

for.inc151:                                       ; preds = %for.end150
  %138 = load i32, i32* %j, align 4, !tbaa !6
  %inc152 = add nsw i32 %138, 1
  store i32 %inc152, i32* %j, align 4, !tbaa !6
  br label %for.cond99

for.end153:                                       ; preds = %for.cond.cleanup101
  %139 = bitcast i32* %endJ to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #8
  %140 = bitcast i32* %startJ to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #8
  %141 = bitcast i32* %endX to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #8
  %142 = bitcast i32* %startX to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #8
  %143 = bitcast [3 x i32]* %quantizedAabbMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %143) #8
  %144 = bitcast [3 x i32]* %quantizedAabbMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %144) #8
  %145 = bitcast %class.btVector3* %localAabbMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %145) #8
  %146 = bitcast %class.btVector3* %localAabbMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %146) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

define hidden void @_ZNK25btHeightfieldTerrainShape21calculateLocalInertiaEfR9btVector3(%class.btHeightfieldTerrainShape* %this, float %0, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btHeightfieldTerrainShape*, align 4
  %.addr = alloca float, align 4
  %inertia.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btHeightfieldTerrainShape* %this, %class.btHeightfieldTerrainShape** %this.addr, align 4, !tbaa !2
  store float %0, float* %.addr, align 4, !tbaa !8
  store %class.btVector3* %inertia, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %this1 = load %class.btHeightfieldTerrainShape*, %class.btHeightfieldTerrainShape** %this.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %2 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !8
  %4 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !8
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  %6 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %7 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: nounwind
define hidden void @_ZN25btHeightfieldTerrainShape15setLocalScalingERK9btVector3(%class.btHeightfieldTerrainShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %scaling) unnamed_addr #6 {
entry:
  %this.addr = alloca %class.btHeightfieldTerrainShape*, align 4
  %scaling.addr = alloca %class.btVector3*, align 4
  store %class.btHeightfieldTerrainShape* %this, %class.btHeightfieldTerrainShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %scaling, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  %this1 = load %class.btHeightfieldTerrainShape*, %class.btHeightfieldTerrainShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  %m_localScaling = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 17
  %1 = bitcast %class.btVector3* %m_localScaling to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !34
  ret void
}

; Function Attrs: nounwind
define hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK25btHeightfieldTerrainShape15getLocalScalingEv(%class.btHeightfieldTerrainShape* %this) unnamed_addr #6 {
entry:
  %this.addr = alloca %class.btHeightfieldTerrainShape*, align 4
  store %class.btHeightfieldTerrainShape* %this, %class.btHeightfieldTerrainShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHeightfieldTerrainShape*, %class.btHeightfieldTerrainShape** %this.addr, align 4
  %m_localScaling = getelementptr inbounds %class.btHeightfieldTerrainShape, %class.btHeightfieldTerrainShape* %this1, i32 0, i32 17
  ret %class.btVector3* %m_localScaling
}

declare void @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf(%class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4)) unnamed_addr #1

declare float @_ZNK16btCollisionShape20getAngularMotionDiscEv(%class.btCollisionShape*) unnamed_addr #1

declare float @_ZNK16btCollisionShape27getContactBreakingThresholdEf(%class.btCollisionShape*, float) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden i8* @_ZNK25btHeightfieldTerrainShape7getNameEv(%class.btHeightfieldTerrainShape* %this) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %class.btHeightfieldTerrainShape*, align 4
  store %class.btHeightfieldTerrainShape* %this, %class.btHeightfieldTerrainShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHeightfieldTerrainShape*, %class.btHeightfieldTerrainShape** %this.addr, align 4
  ret i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0)
}

define linkonce_odr hidden void @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btCollisionShape* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !8
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 1.000000e+00, float* %ref.tmp2, align 4, !tbaa !8
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 1.000000e+00, float* %ref.tmp3, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN14btConcaveShape9setMarginEf(%class.btConcaveShape* %this, float %collisionMargin) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %class.btConcaveShape*, align 4
  %collisionMargin.addr = alloca float, align 4
  store %class.btConcaveShape* %this, %class.btConcaveShape** %this.addr, align 4, !tbaa !2
  store float %collisionMargin, float* %collisionMargin.addr, align 4, !tbaa !8
  %this1 = load %class.btConcaveShape*, %class.btConcaveShape** %this.addr, align 4
  %0 = load float, float* %collisionMargin.addr, align 4, !tbaa !8
  %m_collisionMargin = getelementptr inbounds %class.btConcaveShape, %class.btConcaveShape* %this1, i32 0, i32 1
  store float %0, float* %m_collisionMargin, align 4, !tbaa !37
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK14btConcaveShape9getMarginEv(%class.btConcaveShape* %this) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %class.btConcaveShape*, align 4
  store %class.btConcaveShape* %this, %class.btConcaveShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConcaveShape*, %class.btConcaveShape** %this.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btConcaveShape, %class.btConcaveShape* %this1, i32 0, i32 1
  %0 = load float, float* %m_collisionMargin, align 4, !tbaa !37
  ret float %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK16btCollisionShape28calculateSerializeBufferSizeEv(%class.btCollisionShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  ret i32 12
}

declare i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape*, i8*, %class.btSerializer*) unnamed_addr #1

declare void @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer(%class.btCollisionShape*, %class.btSerializer*) unnamed_addr #1

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btFabsf(float %x) #2 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !8
  %0 = load float, float* %x.addr, align 4, !tbaa !8
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #7

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #6 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !8
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !8
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !8
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #2 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store float* %b, float** %b.addr, align 4, !tbaa !2
  %0 = load float*, float** %a.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %2 = load float*, float** %b.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !8
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !8
  %6 = load float*, float** %a.addr, align 4, !tbaa !2
  store float %5, float* %6, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #2 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store float* %b, float** %b.addr, align 4, !tbaa !2
  %0 = load float*, float** %b.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %2 = load float*, float** %a.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !8
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !8
  %6 = load float*, float** %a.addr, align 4, !tbaa !2
  store float %5, float* %6, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind readnone speculatable willreturn }
attributes #8 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"float", !4, i64 0}
!10 = !{!11, !11, i64 0}
!11 = !{!"_ZTS14PHY_ScalarType", !4, i64 0}
!12 = !{!13, !13, i64 0}
!13 = !{!"bool", !4, i64 0}
!14 = !{!15, !15, i64 0}
!15 = !{!"vtable pointer", !5, i64 0}
!16 = !{i8 0, i8 2}
!17 = !{!18, !7, i64 4}
!18 = !{!"_ZTS16btCollisionShape", !7, i64 4, !3, i64 8}
!19 = !{!20, !7, i64 64}
!20 = !{!"_ZTS25btHeightfieldTerrainShape", !21, i64 16, !21, i64 32, !21, i64 48, !7, i64 64, !7, i64 68, !9, i64 72, !9, i64 76, !9, i64 80, !9, i64 84, !9, i64 88, !4, i64 92, !11, i64 96, !13, i64 100, !13, i64 101, !13, i64 102, !7, i64 104, !21, i64 108}
!21 = !{!"_ZTS9btVector3", !4, i64 0}
!22 = !{!20, !7, i64 68}
!23 = !{!20, !9, i64 72}
!24 = !{!20, !9, i64 76}
!25 = !{!20, !9, i64 80}
!26 = !{!20, !9, i64 84}
!27 = !{!20, !9, i64 88}
!28 = !{!4, !4, i64 0}
!29 = !{!20, !11, i64 96}
!30 = !{!20, !13, i64 100}
!31 = !{!20, !13, i64 101}
!32 = !{!20, !13, i64 102}
!33 = !{!20, !7, i64 104}
!34 = !{i64 0, i64 16, !28}
!35 = !{!36, !36, i64 0}
!36 = !{!"short", !4, i64 0}
!37 = !{!38, !9, i64 12}
!38 = !{!"_ZTS14btConcaveShape", !9, i64 12}
