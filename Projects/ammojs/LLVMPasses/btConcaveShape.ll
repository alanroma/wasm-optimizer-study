; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btConcaveShape.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btConcaveShape.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btConcaveShape = type { %class.btCollisionShape, float }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%class.btVector3 = type { [4 x float] }
%class.btSerializer = type opaque

$_ZN16btCollisionShapeC2Ev = comdat any

$_ZN16btCollisionShapeD2Ev = comdat any

$_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZN14btConcaveShape9setMarginEf = comdat any

$_ZNK14btConcaveShape9getMarginEv = comdat any

$_ZNK16btCollisionShape28calculateSerializeBufferSizeEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

@_ZTV14btConcaveShape = hidden unnamed_addr constant { [19 x i8*] } { [19 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI14btConcaveShape to i8*), i8* bitcast (%class.btConcaveShape* (%class.btConcaveShape*)* @_ZN14btConcaveShapeD1Ev to i8*), i8* bitcast (void (%class.btConcaveShape*)* @_ZN14btConcaveShapeD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btConcaveShape*, float)* @_ZN14btConcaveShape9setMarginEf to i8*), i8* bitcast (float (%class.btConcaveShape*)* @_ZNK14btConcaveShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btCollisionShape*)* @_ZNK16btCollisionShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btCollisionShape*, i8*, %class.btSerializer*)* @_ZNK16btCollisionShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS14btConcaveShape = hidden constant [17 x i8] c"14btConcaveShape\00", align 1
@_ZTI16btCollisionShape = external constant i8*
@_ZTI14btConcaveShape = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([17 x i8], [17 x i8]* @_ZTS14btConcaveShape, i32 0, i32 0), i8* bitcast (i8** @_ZTI16btCollisionShape to i8*) }, align 4
@_ZTV16btCollisionShape = external unnamed_addr constant { [18 x i8*] }, align 4

@_ZN14btConcaveShapeD1Ev = hidden unnamed_addr alias %class.btConcaveShape* (%class.btConcaveShape*), %class.btConcaveShape* (%class.btConcaveShape*)* @_ZN14btConcaveShapeD2Ev

define hidden %class.btConcaveShape* @_ZN14btConcaveShapeC2Ev(%class.btConcaveShape* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConcaveShape*, align 4
  store %class.btConcaveShape* %this, %class.btConcaveShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConcaveShape*, %class.btConcaveShape** %this.addr, align 4
  %0 = bitcast %class.btConcaveShape* %this1 to %class.btCollisionShape*
  %call = call %class.btCollisionShape* @_ZN16btCollisionShapeC2Ev(%class.btCollisionShape* %0)
  %1 = bitcast %class.btConcaveShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [19 x i8*] }, { [19 x i8*] }* @_ZTV14btConcaveShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_collisionMargin = getelementptr inbounds %class.btConcaveShape, %class.btConcaveShape* %this1, i32 0, i32 1
  store float 0.000000e+00, float* %m_collisionMargin, align 4, !tbaa !8
  ret %class.btConcaveShape* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btCollisionShape* @_ZN16btCollisionShapeC2Ev(%class.btCollisionShape* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %0 = bitcast %class.btCollisionShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [18 x i8*] }, { [18 x i8*] }* @_ZTV16btCollisionShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 1
  store i32 35, i32* %m_shapeType, align 4, !tbaa !11
  %m_userPointer = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 2
  store i8* null, i8** %m_userPointer, align 4, !tbaa !14
  ret %class.btCollisionShape* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btCollisionShape* @_ZN16btCollisionShapeD2Ev(%class.btCollisionShape* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  ret %class.btCollisionShape* %this1
}

; Function Attrs: nounwind
define hidden %class.btConcaveShape* @_ZN14btConcaveShapeD2Ev(%class.btConcaveShape* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btConcaveShape*, align 4
  store %class.btConcaveShape* %this, %class.btConcaveShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConcaveShape*, %class.btConcaveShape** %this.addr, align 4
  %0 = bitcast %class.btConcaveShape* %this1 to %class.btCollisionShape*
  %call = call %class.btCollisionShape* @_ZN16btCollisionShapeD2Ev(%class.btCollisionShape* %0) #6
  ret %class.btConcaveShape* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN14btConcaveShapeD0Ev(%class.btConcaveShape* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btConcaveShape*, align 4
  store %class.btConcaveShape* %this, %class.btConcaveShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConcaveShape*, %class.btConcaveShape** %this.addr, align 4
  call void @llvm.trap() #7
  unreachable
}

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #2

declare void @__cxa_pure_virtual() unnamed_addr

declare void @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf(%class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4)) unnamed_addr #3

declare float @_ZNK16btCollisionShape20getAngularMotionDiscEv(%class.btCollisionShape*) unnamed_addr #3

declare float @_ZNK16btCollisionShape27getContactBreakingThresholdEf(%class.btCollisionShape*, float) unnamed_addr #3

define linkonce_odr hidden void @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btCollisionShape* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !15
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store float 1.000000e+00, float* %ref.tmp2, align 4, !tbaa !15
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  store float 1.000000e+00, float* %ref.tmp3, align 4, !tbaa !15
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #6
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #6
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN14btConcaveShape9setMarginEf(%class.btConcaveShape* %this, float %collisionMargin) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConcaveShape*, align 4
  %collisionMargin.addr = alloca float, align 4
  store %class.btConcaveShape* %this, %class.btConcaveShape** %this.addr, align 4, !tbaa !2
  store float %collisionMargin, float* %collisionMargin.addr, align 4, !tbaa !15
  %this1 = load %class.btConcaveShape*, %class.btConcaveShape** %this.addr, align 4
  %0 = load float, float* %collisionMargin.addr, align 4, !tbaa !15
  %m_collisionMargin = getelementptr inbounds %class.btConcaveShape, %class.btConcaveShape* %this1, i32 0, i32 1
  store float %0, float* %m_collisionMargin, align 4, !tbaa !8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK14btConcaveShape9getMarginEv(%class.btConcaveShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConcaveShape*, align 4
  store %class.btConcaveShape* %this, %class.btConcaveShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConcaveShape*, %class.btConcaveShape** %this.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btConcaveShape, %class.btConcaveShape* %this1, i32 0, i32 1
  %0 = load float, float* %m_collisionMargin, align 4, !tbaa !8
  ret float %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK16btCollisionShape28calculateSerializeBufferSizeEv(%class.btCollisionShape* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  ret i32 12
}

declare i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape*, i8*, %class.btSerializer*) unnamed_addr #3

declare void @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer(%class.btCollisionShape*, %class.btSerializer*) unnamed_addr #3

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #5

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !15
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !15
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !15
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !15
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !15
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !15
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !15
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #5

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { cold noreturn nounwind }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { argmemonly nounwind willreturn }
attributes #6 = { nounwind }
attributes #7 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{!9, !10, i64 12}
!9 = !{!"_ZTS14btConcaveShape", !10, i64 12}
!10 = !{!"float", !4, i64 0}
!11 = !{!12, !13, i64 4}
!12 = !{!"_ZTS16btCollisionShape", !13, i64 4, !3, i64 8}
!13 = !{!"int", !4, i64 0}
!14 = !{!12, !3, i64 8}
!15 = !{!10, !10, i64 0}
