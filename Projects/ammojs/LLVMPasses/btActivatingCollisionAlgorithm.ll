; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionDispatch/btActivatingCollisionAlgorithm.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionDispatch/btActivatingCollisionAlgorithm.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btActivatingCollisionAlgorithm = type { %class.btCollisionAlgorithm }
%class.btCollisionAlgorithm = type { i32 (...)**, %class.btDispatcher* }
%class.btDispatcher = type { i32 (...)** }
%struct.btCollisionAlgorithmConstructionInfo = type { %class.btDispatcher*, %class.btPersistentManifold* }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, i32, i32, i32, i32, i8*, i8, float, float, float, float, float, float, float, i32, %class.btVector3, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%union.anon = type { i8* }
%struct.btCollisionObjectWrapper = type { %struct.btCollisionObjectWrapper*, %class.btCollisionShape*, %class.btCollisionObject*, %class.btTransform*, i32, i32 }

$_ZN20btCollisionAlgorithmD2Ev = comdat any

$_ZTS20btCollisionAlgorithm = comdat any

$_ZTI20btCollisionAlgorithm = comdat any

@_ZTV30btActivatingCollisionAlgorithm = hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI30btActivatingCollisionAlgorithm to i8*), i8* bitcast (%class.btActivatingCollisionAlgorithm* (%class.btActivatingCollisionAlgorithm*)* @_ZN30btActivatingCollisionAlgorithmD1Ev to i8*), i8* bitcast (void (%class.btActivatingCollisionAlgorithm*)* @_ZN30btActivatingCollisionAlgorithmD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS30btActivatingCollisionAlgorithm = hidden constant [33 x i8] c"30btActivatingCollisionAlgorithm\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS20btCollisionAlgorithm = linkonce_odr hidden constant [23 x i8] c"20btCollisionAlgorithm\00", comdat, align 1
@_ZTI20btCollisionAlgorithm = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([23 x i8], [23 x i8]* @_ZTS20btCollisionAlgorithm, i32 0, i32 0) }, comdat, align 4
@_ZTI30btActivatingCollisionAlgorithm = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([33 x i8], [33 x i8]* @_ZTS30btActivatingCollisionAlgorithm, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI20btCollisionAlgorithm to i8*) }, align 4

@_ZN30btActivatingCollisionAlgorithmD1Ev = hidden unnamed_addr alias %class.btActivatingCollisionAlgorithm* (%class.btActivatingCollisionAlgorithm*), %class.btActivatingCollisionAlgorithm* (%class.btActivatingCollisionAlgorithm*)* @_ZN30btActivatingCollisionAlgorithmD2Ev

define hidden %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo(%class.btActivatingCollisionAlgorithm* returned %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btActivatingCollisionAlgorithm*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  store %class.btActivatingCollisionAlgorithm* %this, %class.btActivatingCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %this1 = load %class.btActivatingCollisionAlgorithm*, %class.btActivatingCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast %class.btActivatingCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %1 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %call = call %class.btCollisionAlgorithm* @_ZN20btCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo(%class.btCollisionAlgorithm* %0, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %1)
  %2 = bitcast %class.btActivatingCollisionAlgorithm* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV30btActivatingCollisionAlgorithm, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %2, align 4, !tbaa !6
  ret %class.btActivatingCollisionAlgorithm* %this1
}

declare %class.btCollisionAlgorithm* @_ZN20btCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo(%class.btCollisionAlgorithm* returned, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8)) unnamed_addr #1

define hidden %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%class.btActivatingCollisionAlgorithm* returned %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %0, %struct.btCollisionObjectWrapper* %1) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btActivatingCollisionAlgorithm*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %.addr1 = alloca %struct.btCollisionObjectWrapper*, align 4
  store %class.btActivatingCollisionAlgorithm* %this, %class.btActivatingCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %0, %struct.btCollisionObjectWrapper** %.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %1, %struct.btCollisionObjectWrapper** %.addr1, align 4, !tbaa !2
  %this2 = load %class.btActivatingCollisionAlgorithm*, %class.btActivatingCollisionAlgorithm** %this.addr, align 4
  %2 = bitcast %class.btActivatingCollisionAlgorithm* %this2 to %class.btCollisionAlgorithm*
  %3 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %call = call %class.btCollisionAlgorithm* @_ZN20btCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo(%class.btCollisionAlgorithm* %2, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %3)
  %4 = bitcast %class.btActivatingCollisionAlgorithm* %this2 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV30btActivatingCollisionAlgorithm, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %4, align 4, !tbaa !6
  ret %class.btActivatingCollisionAlgorithm* %this2
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN20btCollisionAlgorithmD2Ev(%class.btCollisionAlgorithm* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionAlgorithm*, align 4
  store %class.btCollisionAlgorithm* %this, %class.btCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %this.addr, align 4
  ret %class.btCollisionAlgorithm* %this1
}

; Function Attrs: nounwind
define hidden %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmD2Ev(%class.btActivatingCollisionAlgorithm* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btActivatingCollisionAlgorithm*, align 4
  store %class.btActivatingCollisionAlgorithm* %this, %class.btActivatingCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btActivatingCollisionAlgorithm*, %class.btActivatingCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast %class.btActivatingCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %call = call %class.btCollisionAlgorithm* @_ZN20btCollisionAlgorithmD2Ev(%class.btCollisionAlgorithm* %0) #4
  ret %class.btActivatingCollisionAlgorithm* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN30btActivatingCollisionAlgorithmD0Ev(%class.btActivatingCollisionAlgorithm* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btActivatingCollisionAlgorithm*, align 4
  store %class.btActivatingCollisionAlgorithm* %this, %class.btActivatingCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btActivatingCollisionAlgorithm*, %class.btActivatingCollisionAlgorithm** %this.addr, align 4
  call void @llvm.trap() #5
  unreachable
}

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #3

declare void @__cxa_pure_virtual() unnamed_addr

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { cold noreturn nounwind }
attributes #4 = { nounwind }
attributes #5 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
