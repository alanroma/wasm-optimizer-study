; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionDispatch/btCollisionDispatcher.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionDispatch/btCollisionDispatcher.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btCollisionDispatcher = type { %class.btDispatcher, i32, %class.btAlignedObjectArray, %class.btManifoldResult, void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)*, %class.btPoolAllocator*, %class.btPoolAllocator*, [36 x [36 x %struct.btCollisionAlgorithmCreateFunc*]], %class.btCollisionConfiguration* }
%class.btDispatcher = type { i32 (...)** }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btPersistentManifold**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, i32, i32, i32, i32, i8*, i8, float, float, float, float, float, float, float, i32, %class.btVector3, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%union.anon = type { i8* }
%class.btManifoldResult = type { %"struct.btDiscreteCollisionDetectorInterface::Result", %class.btPersistentManifold*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i32, i32, i32, i32 }
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }
%struct.btCollisionObjectWrapper = type { %struct.btCollisionObjectWrapper*, %class.btCollisionShape*, %class.btCollisionObject*, %class.btTransform*, i32, i32 }
%struct.btBroadphasePair = type { %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btCollisionAlgorithm*, %union.anon.0 }
%class.btCollisionAlgorithm = type { i32 (...)**, %class.btDispatcher* }
%union.anon.0 = type { i8* }
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btIDebugDraw = type opaque
%class.btPoolAllocator = type { i32, i32, i32, i8*, i8* }
%struct.btCollisionAlgorithmCreateFunc = type <{ i32 (...)**, i8, [3 x i8] }>
%class.btCollisionConfiguration = type { i32 (...)** }
%struct.btCollisionAlgorithmConstructionInfo = type { %class.btDispatcher*, %class.btPersistentManifold* }
%class.btOverlappingPairCache = type { %class.btOverlappingPairCallback }
%class.btOverlappingPairCallback = type { i32 (...)** }
%class.btCollisionPairCallback = type { %struct.btOverlapCallback, %struct.btDispatcherInfo*, %class.btCollisionDispatcher* }
%struct.btOverlapCallback = type { i32 (...)** }

$_ZN12btDispatcherC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev = comdat any

$_ZN16btManifoldResultC2Ev = comdat any

$_ZN21btCollisionDispatcher15setNearCallbackEPFvR16btBroadphasePairRS_RK16btDispatcherInfoE = comdat any

$_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev = comdat any

$_Z5btMinIfERKT_S2_S2_ = comdat any

$_ZNK17btCollisionObject17getCollisionShapeEv = comdat any

$_ZNK17btCollisionObject29getContactProcessingThresholdEv = comdat any

$_ZNK15btPoolAllocator12getFreeCountEv = comdat any

$_ZN15btPoolAllocator8allocateEi = comdat any

$_ZN20btPersistentManifoldnwEmPv = comdat any

$_ZN20btPersistentManifoldC2EPK17btCollisionObjectS2_iff = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_ = comdat any

$_ZN20btPersistentManifold13clearManifoldEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4swapEii = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8pop_backEv = comdat any

$_ZN15btPoolAllocator8validPtrEPv = comdat any

$_ZN15btPoolAllocator10freeMemoryEPv = comdat any

$_ZN36btCollisionAlgorithmConstructionInfoC2Ev = comdat any

$_ZNK24btCollisionObjectWrapper17getCollisionShapeEv = comdat any

$_ZNK16btCollisionShape12getShapeTypeEv = comdat any

$_ZNK17btCollisionObject18hasContactResponseEv = comdat any

$_ZNK17btCollisionObject25isStaticOrKinematicObjectEv = comdat any

$_ZNK17btCollisionObject8isActiveEv = comdat any

$_ZNK17btCollisionObject16checkCollideWithEPKS_ = comdat any

$_ZN23btCollisionPairCallbackC2ERK16btDispatcherInfoP21btCollisionDispatcher = comdat any

$_ZN17btCollisionObject17getCollisionShapeEv = comdat any

$_ZN17btCollisionObject17getWorldTransformEv = comdat any

$_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii = comdat any

$_ZNK21btCollisionDispatcher15getNumManifoldsEv = comdat any

$_ZN21btCollisionDispatcher26getManifoldByIndexInternalEi = comdat any

$_ZN21btCollisionDispatcher26getInternalManifoldPointerEv = comdat any

$_ZN21btCollisionDispatcher23getInternalManifoldPoolEv = comdat any

$_ZNK21btCollisionDispatcher23getInternalManifoldPoolEv = comdat any

$_ZN36btDiscreteCollisionDetectorInterface6ResultC2Ev = comdat any

$_ZN36btDiscreteCollisionDetectorInterface6ResultD0Ev = comdat any

$_ZN13btTypedObjectC2Ei = comdat any

$_ZN15btManifoldPointC2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK17btCollisionObject18getActivationStateEv = comdat any

$_ZN17btOverlapCallbackC2Ev = comdat any

$_ZN23btCollisionPairCallbackD0Ev = comdat any

$_ZN23btCollisionPairCallback14processOverlapER16btBroadphasePair = comdat any

$_ZN17btOverlapCallbackD2Ev = comdat any

$_ZN17btOverlapCallbackD0Ev = comdat any

$_ZNK21btCollisionDispatcher15getNearCallbackEv = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_ = comdat any

$_ZTVN36btDiscreteCollisionDetectorInterface6ResultE = comdat any

$_ZTSN36btDiscreteCollisionDetectorInterface6ResultE = comdat any

$_ZTIN36btDiscreteCollisionDetectorInterface6ResultE = comdat any

$_ZTV23btCollisionPairCallback = comdat any

$_ZTS23btCollisionPairCallback = comdat any

$_ZTS17btOverlapCallback = comdat any

$_ZTI17btOverlapCallback = comdat any

$_ZTI23btCollisionPairCallback = comdat any

$_ZTV17btOverlapCallback = comdat any

@gNumManifold = hidden global i32 0, align 4
@_ZTV21btCollisionDispatcher = hidden unnamed_addr constant { [18 x i8*] } { [18 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI21btCollisionDispatcher to i8*), i8* bitcast (%class.btCollisionDispatcher* (%class.btCollisionDispatcher*)* @_ZN21btCollisionDispatcherD1Ev to i8*), i8* bitcast (void (%class.btCollisionDispatcher*)* @_ZN21btCollisionDispatcherD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%class.btCollisionDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)* @_ZN21btCollisionDispatcher13findAlgorithmEPK24btCollisionObjectWrapperS2_P20btPersistentManifold to i8*), i8* bitcast (%class.btPersistentManifold* (%class.btCollisionDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)* @_ZN21btCollisionDispatcher14getNewManifoldEPK17btCollisionObjectS2_ to i8*), i8* bitcast (void (%class.btCollisionDispatcher*, %class.btPersistentManifold*)* @_ZN21btCollisionDispatcher15releaseManifoldEP20btPersistentManifold to i8*), i8* bitcast (void (%class.btCollisionDispatcher*, %class.btPersistentManifold*)* @_ZN21btCollisionDispatcher13clearManifoldEP20btPersistentManifold to i8*), i8* bitcast (i1 (%class.btCollisionDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)* @_ZN21btCollisionDispatcher14needsCollisionEPK17btCollisionObjectS2_ to i8*), i8* bitcast (i1 (%class.btCollisionDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)* @_ZN21btCollisionDispatcher13needsResponseEPK17btCollisionObjectS2_ to i8*), i8* bitcast (void (%class.btCollisionDispatcher*, %class.btOverlappingPairCache*, %struct.btDispatcherInfo*, %class.btDispatcher*)* @_ZN21btCollisionDispatcher25dispatchAllCollisionPairsEP22btOverlappingPairCacheRK16btDispatcherInfoP12btDispatcher to i8*), i8* bitcast (i32 (%class.btCollisionDispatcher*)* @_ZNK21btCollisionDispatcher15getNumManifoldsEv to i8*), i8* bitcast (%class.btPersistentManifold* (%class.btCollisionDispatcher*, i32)* @_ZN21btCollisionDispatcher26getManifoldByIndexInternalEi to i8*), i8* bitcast (%class.btPersistentManifold** (%class.btCollisionDispatcher*)* @_ZN21btCollisionDispatcher26getInternalManifoldPointerEv to i8*), i8* bitcast (%class.btPoolAllocator* (%class.btCollisionDispatcher*)* @_ZN21btCollisionDispatcher23getInternalManifoldPoolEv to i8*), i8* bitcast (%class.btPoolAllocator* (%class.btCollisionDispatcher*)* @_ZNK21btCollisionDispatcher23getInternalManifoldPoolEv to i8*), i8* bitcast (i8* (%class.btCollisionDispatcher*, i32)* @_ZN21btCollisionDispatcher26allocateCollisionAlgorithmEi to i8*), i8* bitcast (void (%class.btCollisionDispatcher*, i8*)* @_ZN21btCollisionDispatcher22freeCollisionAlgorithmEPv to i8*)] }, align 4
@gContactBreakingThreshold = external global float, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS21btCollisionDispatcher = hidden constant [24 x i8] c"21btCollisionDispatcher\00", align 1
@_ZTI12btDispatcher = external constant i8*
@_ZTI21btCollisionDispatcher = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([24 x i8], [24 x i8]* @_ZTS21btCollisionDispatcher, i32 0, i32 0), i8* bitcast (i8** @_ZTI12btDispatcher to i8*) }, align 4
@_ZTV12btDispatcher = external unnamed_addr constant { [18 x i8*] }, align 4
@_ZTV16btManifoldResult = external unnamed_addr constant { [7 x i8*] }, align 4
@_ZTVN36btDiscreteCollisionDetectorInterface6ResultE = linkonce_odr hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTIN36btDiscreteCollisionDetectorInterface6ResultE to i8*), i8* bitcast (%"struct.btDiscreteCollisionDetectorInterface::Result"* (%"struct.btDiscreteCollisionDetectorInterface::Result"*)* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev to i8*), i8* bitcast (void (%"struct.btDiscreteCollisionDetectorInterface::Result"*)* @_ZN36btDiscreteCollisionDetectorInterface6ResultD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTSN36btDiscreteCollisionDetectorInterface6ResultE = linkonce_odr hidden constant [48 x i8] c"N36btDiscreteCollisionDetectorInterface6ResultE\00", comdat, align 1
@_ZTIN36btDiscreteCollisionDetectorInterface6ResultE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([48 x i8], [48 x i8]* @_ZTSN36btDiscreteCollisionDetectorInterface6ResultE, i32 0, i32 0) }, comdat, align 4
@_ZTV23btCollisionPairCallback = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI23btCollisionPairCallback to i8*), i8* bitcast (%struct.btOverlapCallback* (%struct.btOverlapCallback*)* @_ZN17btOverlapCallbackD2Ev to i8*), i8* bitcast (void (%class.btCollisionPairCallback*)* @_ZN23btCollisionPairCallbackD0Ev to i8*), i8* bitcast (i1 (%class.btCollisionPairCallback*, %struct.btBroadphasePair*)* @_ZN23btCollisionPairCallback14processOverlapER16btBroadphasePair to i8*)] }, comdat, align 4
@_ZTS23btCollisionPairCallback = linkonce_odr hidden constant [26 x i8] c"23btCollisionPairCallback\00", comdat, align 1
@_ZTS17btOverlapCallback = linkonce_odr hidden constant [20 x i8] c"17btOverlapCallback\00", comdat, align 1
@_ZTI17btOverlapCallback = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([20 x i8], [20 x i8]* @_ZTS17btOverlapCallback, i32 0, i32 0) }, comdat, align 4
@_ZTI23btCollisionPairCallback = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([26 x i8], [26 x i8]* @_ZTS23btCollisionPairCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI17btOverlapCallback to i8*) }, comdat, align 4
@_ZTV17btOverlapCallback = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI17btOverlapCallback to i8*), i8* bitcast (%struct.btOverlapCallback* (%struct.btOverlapCallback*)* @_ZN17btOverlapCallbackD2Ev to i8*), i8* bitcast (void (%struct.btOverlapCallback*)* @_ZN17btOverlapCallbackD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4

@_ZN21btCollisionDispatcherC1EP24btCollisionConfiguration = hidden unnamed_addr alias %class.btCollisionDispatcher* (%class.btCollisionDispatcher*, %class.btCollisionConfiguration*), %class.btCollisionDispatcher* (%class.btCollisionDispatcher*, %class.btCollisionConfiguration*)* @_ZN21btCollisionDispatcherC2EP24btCollisionConfiguration
@_ZN21btCollisionDispatcherD1Ev = hidden unnamed_addr alias %class.btCollisionDispatcher* (%class.btCollisionDispatcher*), %class.btCollisionDispatcher* (%class.btCollisionDispatcher*)* @_ZN21btCollisionDispatcherD2Ev

define hidden %class.btCollisionDispatcher* @_ZN21btCollisionDispatcherC2EP24btCollisionConfiguration(%class.btCollisionDispatcher* returned %this, %class.btCollisionConfiguration* %collisionConfiguration) unnamed_addr #0 {
entry:
  %retval = alloca %class.btCollisionDispatcher*, align 4
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  %collisionConfiguration.addr = alloca %class.btCollisionConfiguration*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4, !tbaa !2
  store %class.btCollisionConfiguration* %collisionConfiguration, %class.btCollisionConfiguration** %collisionConfiguration.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  store %class.btCollisionDispatcher* %this1, %class.btCollisionDispatcher** %retval, align 4
  %0 = bitcast %class.btCollisionDispatcher* %this1 to %class.btDispatcher*
  %call = call %class.btDispatcher* @_ZN12btDispatcherC2Ev(%class.btDispatcher* %0) #9
  %1 = bitcast %class.btCollisionDispatcher* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [18 x i8*] }, { [18 x i8*] }* @_ZTV21btCollisionDispatcher, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_dispatcherFlags = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 1
  store i32 2, i32* %m_dispatcherFlags, align 4, !tbaa !8
  %m_manifoldsPtr = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 2
  %call2 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray* %m_manifoldsPtr)
  %m_defaultManifoldResult = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 3
  %call3 = call %class.btManifoldResult* @_ZN16btManifoldResultC2Ev(%class.btManifoldResult* %m_defaultManifoldResult)
  %m_collisionConfiguration = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 8
  %2 = load %class.btCollisionConfiguration*, %class.btCollisionConfiguration** %collisionConfiguration.addr, align 4, !tbaa !2
  store %class.btCollisionConfiguration* %2, %class.btCollisionConfiguration** %m_collisionConfiguration, align 4, !tbaa !15
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  call void @_ZN21btCollisionDispatcher15setNearCallbackEPFvR16btBroadphasePairRS_RK16btDispatcherInfoE(%class.btCollisionDispatcher* %this1, void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)* @_ZN21btCollisionDispatcher19defaultNearCallbackER16btBroadphasePairRS_RK16btDispatcherInfo)
  %4 = load %class.btCollisionConfiguration*, %class.btCollisionConfiguration** %collisionConfiguration.addr, align 4, !tbaa !2
  %5 = bitcast %class.btCollisionConfiguration* %4 to %class.btPoolAllocator* (%class.btCollisionConfiguration*)***
  %vtable = load %class.btPoolAllocator* (%class.btCollisionConfiguration*)**, %class.btPoolAllocator* (%class.btCollisionConfiguration*)*** %5, align 4, !tbaa !6
  %vfn = getelementptr inbounds %class.btPoolAllocator* (%class.btCollisionConfiguration*)*, %class.btPoolAllocator* (%class.btCollisionConfiguration*)** %vtable, i64 3
  %6 = load %class.btPoolAllocator* (%class.btCollisionConfiguration*)*, %class.btPoolAllocator* (%class.btCollisionConfiguration*)** %vfn, align 4
  %call4 = call %class.btPoolAllocator* %6(%class.btCollisionConfiguration* %4)
  %m_collisionAlgorithmPoolAllocator = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 5
  store %class.btPoolAllocator* %call4, %class.btPoolAllocator** %m_collisionAlgorithmPoolAllocator, align 4, !tbaa !16
  %7 = load %class.btCollisionConfiguration*, %class.btCollisionConfiguration** %collisionConfiguration.addr, align 4, !tbaa !2
  %8 = bitcast %class.btCollisionConfiguration* %7 to %class.btPoolAllocator* (%class.btCollisionConfiguration*)***
  %vtable5 = load %class.btPoolAllocator* (%class.btCollisionConfiguration*)**, %class.btPoolAllocator* (%class.btCollisionConfiguration*)*** %8, align 4, !tbaa !6
  %vfn6 = getelementptr inbounds %class.btPoolAllocator* (%class.btCollisionConfiguration*)*, %class.btPoolAllocator* (%class.btCollisionConfiguration*)** %vtable5, i64 2
  %9 = load %class.btPoolAllocator* (%class.btCollisionConfiguration*)*, %class.btPoolAllocator* (%class.btCollisionConfiguration*)** %vfn6, align 4
  %call7 = call %class.btPoolAllocator* %9(%class.btCollisionConfiguration* %7)
  %m_persistentManifoldPoolAllocator = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 6
  store %class.btPoolAllocator* %call7, %class.btPoolAllocator** %m_persistentManifoldPoolAllocator, align 4, !tbaa !17
  store i32 0, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.cond:                                         ; preds = %for.inc16, %entry
  %10 = load i32, i32* %i, align 4, !tbaa !18
  %cmp = icmp slt i32 %10, 36
  br i1 %cmp, label %for.body, label %for.end18

for.body:                                         ; preds = %for.cond
  %11 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  store i32 0, i32* %j, align 4, !tbaa !18
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %for.body
  %12 = load i32, i32* %j, align 4, !tbaa !18
  %cmp9 = icmp slt i32 %12, 36
  br i1 %cmp9, label %for.body10, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond8
  %13 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  br label %for.end

for.body10:                                       ; preds = %for.cond8
  %m_collisionConfiguration11 = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 8
  %14 = load %class.btCollisionConfiguration*, %class.btCollisionConfiguration** %m_collisionConfiguration11, align 4, !tbaa !15
  %15 = load i32, i32* %i, align 4, !tbaa !18
  %16 = load i32, i32* %j, align 4, !tbaa !18
  %17 = bitcast %class.btCollisionConfiguration* %14 to %struct.btCollisionAlgorithmCreateFunc* (%class.btCollisionConfiguration*, i32, i32)***
  %vtable12 = load %struct.btCollisionAlgorithmCreateFunc* (%class.btCollisionConfiguration*, i32, i32)**, %struct.btCollisionAlgorithmCreateFunc* (%class.btCollisionConfiguration*, i32, i32)*** %17, align 4, !tbaa !6
  %vfn13 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%class.btCollisionConfiguration*, i32, i32)*, %struct.btCollisionAlgorithmCreateFunc* (%class.btCollisionConfiguration*, i32, i32)** %vtable12, i64 4
  %18 = load %struct.btCollisionAlgorithmCreateFunc* (%class.btCollisionConfiguration*, i32, i32)*, %struct.btCollisionAlgorithmCreateFunc* (%class.btCollisionConfiguration*, i32, i32)** %vfn13, align 4
  %call14 = call %struct.btCollisionAlgorithmCreateFunc* %18(%class.btCollisionConfiguration* %14, i32 %15, i32 %16)
  %m_doubleDispatch = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 7
  %19 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds [36 x [36 x %struct.btCollisionAlgorithmCreateFunc*]], [36 x [36 x %struct.btCollisionAlgorithmCreateFunc*]]* %m_doubleDispatch, i32 0, i32 %19
  %20 = load i32, i32* %j, align 4, !tbaa !18
  %arrayidx15 = getelementptr inbounds [36 x %struct.btCollisionAlgorithmCreateFunc*], [36 x %struct.btCollisionAlgorithmCreateFunc*]* %arrayidx, i32 0, i32 %20
  store %struct.btCollisionAlgorithmCreateFunc* %call14, %struct.btCollisionAlgorithmCreateFunc** %arrayidx15, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body10
  %21 = load i32, i32* %j, align 4, !tbaa !18
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %j, align 4, !tbaa !18
  br label %for.cond8

for.end:                                          ; preds = %for.cond.cleanup
  br label %for.inc16

for.inc16:                                        ; preds = %for.end
  %22 = load i32, i32* %i, align 4, !tbaa !18
  %inc17 = add nsw i32 %22, 1
  store i32 %inc17, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.end18:                                        ; preds = %for.cond
  %23 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #9
  %24 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %retval, align 4
  ret %class.btCollisionDispatcher* %24
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btDispatcher* @_ZN12btDispatcherC2Ev(%class.btDispatcher* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btDispatcher*, align 4
  store %class.btDispatcher* %this, %class.btDispatcher** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btDispatcher*, %class.btDispatcher** %this.addr, align 4
  %0 = bitcast %class.btDispatcher* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [18 x i8*] }, { [18 x i8*] }* @_ZTV12btDispatcher, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  ret %class.btDispatcher* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btManifoldResult* @_ZN16btManifoldResultC2Ev(%class.btManifoldResult* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %0 = bitcast %class.btManifoldResult* %this1 to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  %call = call %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultC2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* %0) #9
  %1 = bitcast %class.btManifoldResult* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV16btManifoldResult, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  ret %class.btManifoldResult* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN21btCollisionDispatcher15setNearCallbackEPFvR16btBroadphasePairRS_RK16btDispatcherInfoE(%class.btCollisionDispatcher* %this, void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)* %nearCallback) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  %nearCallback.addr = alloca void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)*, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4, !tbaa !2
  store void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)* %nearCallback, void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)** %nearCallback.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %0 = load void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)*, void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)** %nearCallback.addr, align 4, !tbaa !2
  %m_nearCallback = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 4
  store void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)* %0, void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)** %m_nearCallback, align 4, !tbaa !19
  ret void
}

define hidden void @_ZN21btCollisionDispatcher19defaultNearCallbackER16btBroadphasePairRS_RK16btDispatcherInfo(%struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %collisionPair, %class.btCollisionDispatcher* nonnull align 4 dereferenceable(5260) %dispatcher, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo) #0 {
entry:
  %collisionPair.addr = alloca %struct.btBroadphasePair*, align 4
  %dispatcher.addr = alloca %class.btCollisionDispatcher*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %colObj0 = alloca %class.btCollisionObject*, align 4
  %colObj1 = alloca %class.btCollisionObject*, align 4
  %obj0Wrap = alloca %struct.btCollisionObjectWrapper, align 4
  %obj1Wrap = alloca %struct.btCollisionObjectWrapper, align 4
  %contactPointResult = alloca %class.btManifoldResult, align 4
  %toi = alloca float, align 4
  store %struct.btBroadphasePair* %collisionPair, %struct.btBroadphasePair** %collisionPair.addr, align 4, !tbaa !2
  store %class.btCollisionDispatcher* %dispatcher, %class.btCollisionDispatcher** %dispatcher.addr, align 4, !tbaa !2
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  %0 = bitcast %class.btCollisionObject** %colObj0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %collisionPair.addr, align 4, !tbaa !2
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %1, i32 0, i32 0
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !20
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %2, i32 0, i32 0
  %3 = load i8*, i8** %m_clientObject, align 4, !tbaa !22
  %4 = bitcast i8* %3 to %class.btCollisionObject*
  store %class.btCollisionObject* %4, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %5 = bitcast %class.btCollisionObject** %colObj1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %collisionPair.addr, align 4, !tbaa !2
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %6, i32 0, i32 1
  %7 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !26
  %m_clientObject1 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %7, i32 0, i32 0
  %8 = load i8*, i8** %m_clientObject1, align 4, !tbaa !22
  %9 = bitcast i8* %8 to %class.btCollisionObject*
  store %class.btCollisionObject* %9, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %10 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %11 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %12 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %13 = bitcast %class.btCollisionDispatcher* %10 to i1 (%class.btCollisionDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)***
  %vtable = load i1 (%class.btCollisionDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)**, i1 (%class.btCollisionDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*** %13, align 4, !tbaa !6
  %vfn = getelementptr inbounds i1 (%class.btCollisionDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*, i1 (%class.btCollisionDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)** %vtable, i64 6
  %14 = load i1 (%class.btCollisionDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*, i1 (%class.btCollisionDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)** %vfn, align 4
  %call = call zeroext i1 %14(%class.btCollisionDispatcher* %10, %class.btCollisionObject* %11, %class.btCollisionObject* %12)
  br i1 %call, label %if.then, label %if.end32

if.then:                                          ; preds = %entry
  %15 = bitcast %struct.btCollisionObjectWrapper* %obj0Wrap to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %15) #9
  %16 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %call2 = call %class.btCollisionShape* @_ZN17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %16)
  %17 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %18 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %18)
  %call4 = call %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* %obj0Wrap, %struct.btCollisionObjectWrapper* null, %class.btCollisionShape* %call2, %class.btCollisionObject* %17, %class.btTransform* nonnull align 4 dereferenceable(64) %call3, i32 -1, i32 -1)
  %19 = bitcast %struct.btCollisionObjectWrapper* %obj1Wrap to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %19) #9
  %20 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %call5 = call %class.btCollisionShape* @_ZN17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %20)
  %21 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %22 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %22)
  %call7 = call %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* %obj1Wrap, %struct.btCollisionObjectWrapper* null, %class.btCollisionShape* %call5, %class.btCollisionObject* %21, %class.btTransform* nonnull align 4 dereferenceable(64) %call6, i32 -1, i32 -1)
  %23 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %collisionPair.addr, align 4, !tbaa !2
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %23, i32 0, i32 2
  %24 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm, align 4, !tbaa !27
  %tobool = icmp ne %class.btCollisionAlgorithm* %24, null
  br i1 %tobool, label %if.end, label %if.then8

if.then8:                                         ; preds = %if.then
  %25 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %26 = bitcast %class.btCollisionDispatcher* %25 to %class.btCollisionAlgorithm* (%class.btCollisionDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)***
  %vtable9 = load %class.btCollisionAlgorithm* (%class.btCollisionDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)**, %class.btCollisionAlgorithm* (%class.btCollisionDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)*** %26, align 4, !tbaa !6
  %vfn10 = getelementptr inbounds %class.btCollisionAlgorithm* (%class.btCollisionDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)*, %class.btCollisionAlgorithm* (%class.btCollisionDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)** %vtable9, i64 2
  %27 = load %class.btCollisionAlgorithm* (%class.btCollisionDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)*, %class.btCollisionAlgorithm* (%class.btCollisionDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)** %vfn10, align 4
  %call11 = call %class.btCollisionAlgorithm* %27(%class.btCollisionDispatcher* %25, %struct.btCollisionObjectWrapper* %obj0Wrap, %struct.btCollisionObjectWrapper* %obj1Wrap, %class.btPersistentManifold* null)
  %28 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %collisionPair.addr, align 4, !tbaa !2
  %m_algorithm12 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %28, i32 0, i32 2
  store %class.btCollisionAlgorithm* %call11, %class.btCollisionAlgorithm** %m_algorithm12, align 4, !tbaa !27
  br label %if.end

if.end:                                           ; preds = %if.then8, %if.then
  %29 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %collisionPair.addr, align 4, !tbaa !2
  %m_algorithm13 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %29, i32 0, i32 2
  %30 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm13, align 4, !tbaa !27
  %tobool14 = icmp ne %class.btCollisionAlgorithm* %30, null
  br i1 %tobool14, label %if.then15, label %if.end31

if.then15:                                        ; preds = %if.end
  %31 = bitcast %class.btManifoldResult* %contactPointResult to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %31) #9
  %call16 = call %class.btManifoldResult* @_ZN16btManifoldResultC1EPK24btCollisionObjectWrapperS2_(%class.btManifoldResult* %contactPointResult, %struct.btCollisionObjectWrapper* %obj0Wrap, %struct.btCollisionObjectWrapper* %obj1Wrap)
  %32 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  %m_dispatchFunc = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %32, i32 0, i32 2
  %33 = load i32, i32* %m_dispatchFunc, align 4, !tbaa !28
  %cmp = icmp eq i32 %33, 1
  br i1 %cmp, label %if.then17, label %if.else

if.then17:                                        ; preds = %if.then15
  %34 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %collisionPair.addr, align 4, !tbaa !2
  %m_algorithm18 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %34, i32 0, i32 2
  %35 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm18, align 4, !tbaa !27
  %36 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  %37 = bitcast %class.btCollisionAlgorithm* %35 to void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)***
  %vtable19 = load void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)**, void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*** %37, align 4, !tbaa !6
  %vfn20 = getelementptr inbounds void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*, void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)** %vtable19, i64 2
  %38 = load void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*, void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)** %vfn20, align 4
  call void %38(%class.btCollisionAlgorithm* %35, %struct.btCollisionObjectWrapper* %obj0Wrap, %struct.btCollisionObjectWrapper* %obj1Wrap, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %36, %class.btManifoldResult* %contactPointResult)
  br label %if.end29

if.else:                                          ; preds = %if.then15
  %39 = bitcast float* %toi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #9
  %40 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %collisionPair.addr, align 4, !tbaa !2
  %m_algorithm21 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %40, i32 0, i32 2
  %41 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm21, align 4, !tbaa !27
  %42 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %43 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %44 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  %45 = bitcast %class.btCollisionAlgorithm* %41 to float (%class.btCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)***
  %vtable22 = load float (%class.btCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)**, float (%class.btCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*** %45, align 4, !tbaa !6
  %vfn23 = getelementptr inbounds float (%class.btCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*, float (%class.btCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)** %vtable22, i64 3
  %46 = load float (%class.btCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*, float (%class.btCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)** %vfn23, align 4
  %call24 = call float %46(%class.btCollisionAlgorithm* %41, %class.btCollisionObject* %42, %class.btCollisionObject* %43, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %44, %class.btManifoldResult* %contactPointResult)
  store float %call24, float* %toi, align 4, !tbaa !31
  %47 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  %m_timeOfImpact = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %47, i32 0, i32 3
  %48 = load float, float* %m_timeOfImpact, align 4, !tbaa !32
  %49 = load float, float* %toi, align 4, !tbaa !31
  %cmp25 = fcmp ogt float %48, %49
  br i1 %cmp25, label %if.then26, label %if.end28

if.then26:                                        ; preds = %if.else
  %50 = load float, float* %toi, align 4, !tbaa !31
  %51 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  %m_timeOfImpact27 = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %51, i32 0, i32 3
  store float %50, float* %m_timeOfImpact27, align 4, !tbaa !32
  br label %if.end28

if.end28:                                         ; preds = %if.then26, %if.else
  %52 = bitcast float* %toi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #9
  br label %if.end29

if.end29:                                         ; preds = %if.end28, %if.then17
  %call30 = call %class.btManifoldResult* bitcast (%"struct.btDiscreteCollisionDetectorInterface::Result"* (%"struct.btDiscreteCollisionDetectorInterface::Result"*)* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev to %class.btManifoldResult* (%class.btManifoldResult*)*)(%class.btManifoldResult* %contactPointResult) #9
  %53 = bitcast %class.btManifoldResult* %contactPointResult to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %53) #9
  br label %if.end31

if.end31:                                         ; preds = %if.end29, %if.end
  %54 = bitcast %struct.btCollisionObjectWrapper* %obj1Wrap to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %54) #9
  %55 = bitcast %struct.btCollisionObjectWrapper* %obj0Wrap to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %55) #9
  br label %if.end32

if.end32:                                         ; preds = %if.end31, %entry
  %56 = bitcast %class.btCollisionObject** %colObj1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #9
  %57 = bitcast %class.btCollisionObject** %colObj0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #9
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: nounwind
define hidden void @_ZN21btCollisionDispatcher27registerCollisionCreateFuncEiiP30btCollisionAlgorithmCreateFunc(%class.btCollisionDispatcher* %this, i32 %proxyType0, i32 %proxyType1, %struct.btCollisionAlgorithmCreateFunc* %createFunc) #2 {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  %proxyType0.addr = alloca i32, align 4
  %proxyType1.addr = alloca i32, align 4
  %createFunc.addr = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4, !tbaa !2
  store i32 %proxyType0, i32* %proxyType0.addr, align 4, !tbaa !18
  store i32 %proxyType1, i32* %proxyType1.addr, align 4, !tbaa !18
  store %struct.btCollisionAlgorithmCreateFunc* %createFunc, %struct.btCollisionAlgorithmCreateFunc** %createFunc.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %0 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %createFunc.addr, align 4, !tbaa !2
  %m_doubleDispatch = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 7
  %1 = load i32, i32* %proxyType0.addr, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds [36 x [36 x %struct.btCollisionAlgorithmCreateFunc*]], [36 x [36 x %struct.btCollisionAlgorithmCreateFunc*]]* %m_doubleDispatch, i32 0, i32 %1
  %2 = load i32, i32* %proxyType1.addr, align 4, !tbaa !18
  %arrayidx2 = getelementptr inbounds [36 x %struct.btCollisionAlgorithmCreateFunc*], [36 x %struct.btCollisionAlgorithmCreateFunc*]* %arrayidx, i32 0, i32 %2
  store %struct.btCollisionAlgorithmCreateFunc* %0, %struct.btCollisionAlgorithmCreateFunc** %arrayidx2, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define hidden %class.btCollisionDispatcher* @_ZN21btCollisionDispatcherD2Ev(%class.btCollisionDispatcher* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %0 = bitcast %class.btCollisionDispatcher* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [18 x i8*] }, { [18 x i8*] }* @_ZTV21btCollisionDispatcher, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  %m_defaultManifoldResult = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 3
  %call = call %class.btManifoldResult* bitcast (%"struct.btDiscreteCollisionDetectorInterface::Result"* (%"struct.btDiscreteCollisionDetectorInterface::Result"*)* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev to %class.btManifoldResult* (%class.btManifoldResult*)*)(%class.btManifoldResult* %m_defaultManifoldResult) #9
  %m_manifoldsPtr = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 2
  %call2 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray* %m_manifoldsPtr) #9
  %1 = bitcast %class.btCollisionDispatcher* %this1 to %class.btDispatcher*
  %call3 = call %class.btDispatcher* @_ZN12btDispatcherD2Ev(%class.btDispatcher* %1) #9
  ret %class.btCollisionDispatcher* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %this, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  ret %"struct.btDiscreteCollisionDetectorInterface::Result"* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: nounwind
declare %class.btDispatcher* @_ZN12btDispatcherD2Ev(%class.btDispatcher* returned) unnamed_addr #4

; Function Attrs: nounwind
define hidden void @_ZN21btCollisionDispatcherD0Ev(%class.btCollisionDispatcher* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %call = call %class.btCollisionDispatcher* @_ZN21btCollisionDispatcherD1Ev(%class.btCollisionDispatcher* %this1) #9
  %0 = bitcast %class.btCollisionDispatcher* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #5

define hidden %class.btPersistentManifold* @_ZN21btCollisionDispatcher14getNewManifoldEPK17btCollisionObjectS2_(%class.btCollisionDispatcher* %this, %class.btCollisionObject* %body0, %class.btCollisionObject* %body1) unnamed_addr #0 {
entry:
  %retval = alloca %class.btPersistentManifold*, align 4
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  %body0.addr = alloca %class.btCollisionObject*, align 4
  %body1.addr = alloca %class.btCollisionObject*, align 4
  %contactBreakingThreshold = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %contactProcessingThreshold = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %mem = alloca i8*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %manifold = alloca %class.btPersistentManifold*, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %body0, %class.btCollisionObject** %body0.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %body1, %class.btCollisionObject** %body1.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %0 = load i32, i32* @gNumManifold, align 4, !tbaa !18
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* @gNumManifold, align 4, !tbaa !18
  %1 = bitcast float* %contactBreakingThreshold to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %m_dispatcherFlags = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 1
  %2 = load i32, i32* %m_dispatcherFlags, align 4, !tbaa !8
  %and = and i32 %2, 2
  %tobool = icmp ne i32 %and, 0
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %5 = load %class.btCollisionObject*, %class.btCollisionObject** %body0.addr, align 4, !tbaa !2
  %call = call %class.btCollisionShape* @_ZNK17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %5)
  %6 = load float, float* @gContactBreakingThreshold, align 4, !tbaa !31
  %7 = bitcast %class.btCollisionShape* %call to float (%class.btCollisionShape*, float)***
  %vtable = load float (%class.btCollisionShape*, float)**, float (%class.btCollisionShape*, float)*** %7, align 4, !tbaa !6
  %vfn = getelementptr inbounds float (%class.btCollisionShape*, float)*, float (%class.btCollisionShape*, float)** %vtable, i64 5
  %8 = load float (%class.btCollisionShape*, float)*, float (%class.btCollisionShape*, float)** %vfn, align 4
  %call2 = call float %8(%class.btCollisionShape* %call, float %6)
  store float %call2, float* %ref.tmp, align 4, !tbaa !31
  %9 = load %class.btCollisionObject*, %class.btCollisionObject** %body1.addr, align 4, !tbaa !2
  %call4 = call %class.btCollisionShape* @_ZNK17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %9)
  %10 = load float, float* @gContactBreakingThreshold, align 4, !tbaa !31
  %11 = bitcast %class.btCollisionShape* %call4 to float (%class.btCollisionShape*, float)***
  %vtable5 = load float (%class.btCollisionShape*, float)**, float (%class.btCollisionShape*, float)*** %11, align 4, !tbaa !6
  %vfn6 = getelementptr inbounds float (%class.btCollisionShape*, float)*, float (%class.btCollisionShape*, float)** %vtable5, i64 5
  %12 = load float (%class.btCollisionShape*, float)*, float (%class.btCollisionShape*, float)** %vfn6, align 4
  %call7 = call float %12(%class.btCollisionShape* %call4, float %10)
  store float %call7, float* %ref.tmp3, align 4, !tbaa !31
  %call8 = call nonnull align 4 dereferenceable(4) float* @_Z5btMinIfERKT_S2_S2_(float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %13 = load float, float* %call8, align 4, !tbaa !31
  br label %cond.end

cond.false:                                       ; preds = %entry
  %14 = load float, float* @gContactBreakingThreshold, align 4, !tbaa !31
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %13, %cond.true ], [ %14, %cond.false ]
  %15 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  store float %cond, float* %contactBreakingThreshold, align 4, !tbaa !31
  %17 = bitcast float* %contactProcessingThreshold to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #9
  %18 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #9
  %19 = load %class.btCollisionObject*, %class.btCollisionObject** %body0.addr, align 4, !tbaa !2
  %call10 = call float @_ZNK17btCollisionObject29getContactProcessingThresholdEv(%class.btCollisionObject* %19)
  store float %call10, float* %ref.tmp9, align 4, !tbaa !31
  %20 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #9
  %21 = load %class.btCollisionObject*, %class.btCollisionObject** %body1.addr, align 4, !tbaa !2
  %call12 = call float @_ZNK17btCollisionObject29getContactProcessingThresholdEv(%class.btCollisionObject* %21)
  store float %call12, float* %ref.tmp11, align 4, !tbaa !31
  %call13 = call nonnull align 4 dereferenceable(4) float* @_Z5btMinIfERKT_S2_S2_(float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp11)
  %22 = load float, float* %call13, align 4, !tbaa !31
  %23 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #9
  %24 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #9
  store float %22, float* %contactProcessingThreshold, align 4, !tbaa !31
  %25 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #9
  store i8* null, i8** %mem, align 4, !tbaa !2
  %m_persistentManifoldPoolAllocator = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 6
  %26 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_persistentManifoldPoolAllocator, align 4, !tbaa !17
  %call14 = call i32 @_ZNK15btPoolAllocator12getFreeCountEv(%class.btPoolAllocator* %26)
  %tobool15 = icmp ne i32 %call14, 0
  br i1 %tobool15, label %if.then, label %if.else

if.then:                                          ; preds = %cond.end
  %m_persistentManifoldPoolAllocator16 = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 6
  %27 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_persistentManifoldPoolAllocator16, align 4, !tbaa !17
  %call17 = call i8* @_ZN15btPoolAllocator8allocateEi(%class.btPoolAllocator* %27, i32 772)
  store i8* %call17, i8** %mem, align 4, !tbaa !2
  br label %if.end23

if.else:                                          ; preds = %cond.end
  %m_dispatcherFlags18 = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 1
  %28 = load i32, i32* %m_dispatcherFlags18, align 4, !tbaa !8
  %and19 = and i32 %28, 4
  %cmp = icmp eq i32 %and19, 0
  br i1 %cmp, label %if.then20, label %if.else22

if.then20:                                        ; preds = %if.else
  %call21 = call i8* @_Z22btAlignedAllocInternalmi(i32 772, i32 16)
  store i8* %call21, i8** %mem, align 4, !tbaa !2
  br label %if.end

if.else22:                                        ; preds = %if.else
  store %class.btPersistentManifold* null, %class.btPersistentManifold** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then20
  br label %if.end23

if.end23:                                         ; preds = %if.end, %if.then
  %29 = bitcast %class.btPersistentManifold** %manifold to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #9
  %30 = load i8*, i8** %mem, align 4, !tbaa !2
  %call24 = call i8* @_ZN20btPersistentManifoldnwEmPv(i32 772, i8* %30)
  %31 = bitcast i8* %call24 to %class.btPersistentManifold*
  %32 = load %class.btCollisionObject*, %class.btCollisionObject** %body0.addr, align 4, !tbaa !2
  %33 = load %class.btCollisionObject*, %class.btCollisionObject** %body1.addr, align 4, !tbaa !2
  %34 = load float, float* %contactBreakingThreshold, align 4, !tbaa !31
  %35 = load float, float* %contactProcessingThreshold, align 4, !tbaa !31
  %call25 = call %class.btPersistentManifold* @_ZN20btPersistentManifoldC2EPK17btCollisionObjectS2_iff(%class.btPersistentManifold* %31, %class.btCollisionObject* %32, %class.btCollisionObject* %33, i32 0, float %34, float %35)
  store %class.btPersistentManifold* %31, %class.btPersistentManifold** %manifold, align 4, !tbaa !2
  %m_manifoldsPtr = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 2
  %call26 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray* %m_manifoldsPtr)
  %36 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4, !tbaa !2
  %m_index1a = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %36, i32 0, i32 9
  store i32 %call26, i32* %m_index1a, align 4, !tbaa !33
  %m_manifoldsPtr27 = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_(%class.btAlignedObjectArray* %m_manifoldsPtr27, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %manifold)
  %37 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4, !tbaa !2
  store %class.btPersistentManifold* %37, %class.btPersistentManifold** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %38 = bitcast %class.btPersistentManifold** %manifold to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #9
  br label %cleanup

cleanup:                                          ; preds = %if.end23, %if.else22
  %39 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #9
  %40 = bitcast float* %contactProcessingThreshold to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #9
  %41 = bitcast float* %contactBreakingThreshold to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #9
  %42 = load %class.btPersistentManifold*, %class.btPersistentManifold** %retval, align 4
  ret %class.btPersistentManifold* %42
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_Z5btMinIfERKT_S2_S2_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store float* %b, float** %b.addr, align 4, !tbaa !2
  %0 = load float*, float** %a.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !31
  %2 = load float*, float** %b.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !31
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load float*, float** %a.addr, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %entry
  %5 = load float*, float** %b.addr, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi float* [ %4, %cond.true ], [ %5, %cond.false ]
  ret float* %cond-lvalue
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionShape* @_ZNK17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 9
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %m_collisionShape, align 4, !tbaa !35
  ret %class.btCollisionShape* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK17btCollisionObject29getContactProcessingThresholdEv(%class.btCollisionObject* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_contactProcessingThreshold = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 7
  %0 = load float, float* %m_contactProcessingThreshold, align 4, !tbaa !39
  ret float %0
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK15btPoolAllocator12getFreeCountEv(%class.btPoolAllocator* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btPoolAllocator*, align 4
  store %class.btPoolAllocator* %this, %class.btPoolAllocator** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPoolAllocator*, %class.btPoolAllocator** %this.addr, align 4
  %m_freeCount = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_freeCount, align 4, !tbaa !40
  ret i32 %0
}

; Function Attrs: nounwind
define linkonce_odr hidden i8* @_ZN15btPoolAllocator8allocateEi(%class.btPoolAllocator* %this, i32 %size) #2 comdat {
entry:
  %this.addr = alloca %class.btPoolAllocator*, align 4
  %size.addr = alloca i32, align 4
  %result = alloca i8*, align 4
  store %class.btPoolAllocator* %this, %class.btPoolAllocator** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !18
  %this1 = load %class.btPoolAllocator*, %class.btPoolAllocator** %this.addr, align 4
  %0 = bitcast i8** %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %m_firstFree = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 3
  %1 = load i8*, i8** %m_firstFree, align 4, !tbaa !42
  store i8* %1, i8** %result, align 4, !tbaa !2
  %m_firstFree2 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 3
  %2 = load i8*, i8** %m_firstFree2, align 4, !tbaa !42
  %3 = bitcast i8* %2 to i8**
  %4 = load i8*, i8** %3, align 4, !tbaa !2
  %m_firstFree3 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 3
  store i8* %4, i8** %m_firstFree3, align 4, !tbaa !42
  %m_freeCount = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 2
  %5 = load i32, i32* %m_freeCount, align 4, !tbaa !40
  %dec = add nsw i32 %5, -1
  store i32 %dec, i32* %m_freeCount, align 4, !tbaa !40
  %6 = load i8*, i8** %result, align 4, !tbaa !2
  %7 = bitcast i8** %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret i8* %6
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #6

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN20btPersistentManifoldnwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !43
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

define linkonce_odr hidden %class.btPersistentManifold* @_ZN20btPersistentManifoldC2EPK17btCollisionObjectS2_iff(%class.btPersistentManifold* returned %this, %class.btCollisionObject* %body0, %class.btCollisionObject* %body1, i32 %0, float %contactBreakingThreshold, float %contactProcessingThreshold) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btPersistentManifold*, align 4
  %this.addr = alloca %class.btPersistentManifold*, align 4
  %body0.addr = alloca %class.btCollisionObject*, align 4
  %body1.addr = alloca %class.btCollisionObject*, align 4
  %.addr = alloca i32, align 4
  %contactBreakingThreshold.addr = alloca float, align 4
  %contactProcessingThreshold.addr = alloca float, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %body0, %class.btCollisionObject** %body0.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %body1, %class.btCollisionObject** %body1.addr, align 4, !tbaa !2
  store i32 %0, i32* %.addr, align 4, !tbaa !18
  store float %contactBreakingThreshold, float* %contactBreakingThreshold.addr, align 4, !tbaa !31
  store float %contactProcessingThreshold, float* %contactProcessingThreshold.addr, align 4, !tbaa !31
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  store %class.btPersistentManifold* %this1, %class.btPersistentManifold** %retval, align 4
  %1 = bitcast %class.btPersistentManifold* %this1 to %struct.btTypedObject*
  %call = call %struct.btTypedObject* @_ZN13btTypedObjectC2Ei(%struct.btTypedObject* %1, i32 1025)
  %m_pointCache = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %array.begin, i32 4
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btManifoldPoint* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btManifoldPoint* @_ZN15btManifoldPointC2Ev(%class.btManifoldPoint* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btManifoldPoint* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_body0 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 2
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %body0.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %2, %class.btCollisionObject** %m_body0, align 4, !tbaa !45
  %m_body1 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 3
  %3 = load %class.btCollisionObject*, %class.btCollisionObject** %body1.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %3, %class.btCollisionObject** %m_body1, align 4, !tbaa !46
  %m_cachedPoints = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 4
  store i32 0, i32* %m_cachedPoints, align 4, !tbaa !47
  %m_contactBreakingThreshold = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 5
  %4 = load float, float* %contactBreakingThreshold.addr, align 4, !tbaa !31
  store float %4, float* %m_contactBreakingThreshold, align 4, !tbaa !48
  %m_contactProcessingThreshold = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 6
  %5 = load float, float* %contactProcessingThreshold.addr, align 4, !tbaa !31
  store float %5, float* %m_contactProcessingThreshold, align 4, !tbaa !49
  %6 = load %class.btPersistentManifold*, %class.btPersistentManifold** %retval, align 4
  ret %class.btPersistentManifold* %6
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !50
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_(%class.btAlignedObjectArray* %this, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %_Val) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %class.btPersistentManifold**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store %class.btPersistentManifold** %_Val, %class.btPersistentManifold*** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !18
  %1 = load i32, i32* %sz, align 4, !tbaa !18
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !51
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !50
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %2, i32 %3
  %4 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %5 = bitcast i8* %4 to %class.btPersistentManifold**
  %6 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %_Val.addr, align 4, !tbaa !2
  %7 = load %class.btPersistentManifold*, %class.btPersistentManifold** %6, align 4, !tbaa !2
  store %class.btPersistentManifold* %7, %class.btPersistentManifold** %5, align 4, !tbaa !2
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size5, align 4, !tbaa !50
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size5, align 4, !tbaa !50
  %9 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  ret void
}

define hidden void @_ZN21btCollisionDispatcher13clearManifoldEP20btPersistentManifold(%class.btCollisionDispatcher* %this, %class.btPersistentManifold* %manifold) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  %manifold.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4, !tbaa !2
  store %class.btPersistentManifold* %manifold, %class.btPersistentManifold** %manifold.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4, !tbaa !2
  call void @_ZN20btPersistentManifold13clearManifoldEv(%class.btPersistentManifold* %0)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btPersistentManifold13clearManifoldEv(%class.btPersistentManifold* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  %i = alloca i32, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store i32 0, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !18
  %m_cachedPoints = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 4
  %2 = load i32, i32* %m_cachedPoints, align 4, !tbaa !47
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_pointCache = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %3 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache, i32 0, i32 %3
  call void @_ZN20btPersistentManifold14clearUserCacheER15btManifoldPoint(%class.btPersistentManifold* %this1, %class.btManifoldPoint* nonnull align 4 dereferenceable(184) %arrayidx)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4, !tbaa !18
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_cachedPoints2 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 4
  store i32 0, i32* %m_cachedPoints2, align 4, !tbaa !47
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #9
  ret void
}

define hidden void @_ZN21btCollisionDispatcher15releaseManifoldEP20btPersistentManifold(%class.btCollisionDispatcher* %this, %class.btPersistentManifold* %manifold) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  %manifold.addr = alloca %class.btPersistentManifold*, align 4
  %findIndex = alloca i32, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4, !tbaa !2
  store %class.btPersistentManifold* %manifold, %class.btPersistentManifold** %manifold.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %0 = load i32, i32* @gNumManifold, align 4, !tbaa !18
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* @gNumManifold, align 4, !tbaa !18
  %1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4, !tbaa !2
  %2 = bitcast %class.btCollisionDispatcher* %this1 to void (%class.btCollisionDispatcher*, %class.btPersistentManifold*)***
  %vtable = load void (%class.btCollisionDispatcher*, %class.btPersistentManifold*)**, void (%class.btCollisionDispatcher*, %class.btPersistentManifold*)*** %2, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btCollisionDispatcher*, %class.btPersistentManifold*)*, void (%class.btCollisionDispatcher*, %class.btPersistentManifold*)** %vtable, i64 5
  %3 = load void (%class.btCollisionDispatcher*, %class.btPersistentManifold*)*, void (%class.btCollisionDispatcher*, %class.btPersistentManifold*)** %vfn, align 4
  call void %3(%class.btCollisionDispatcher* %this1, %class.btPersistentManifold* %1)
  %4 = bitcast i32* %findIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4, !tbaa !2
  %m_index1a = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %5, i32 0, i32 9
  %6 = load i32, i32* %m_index1a, align 4, !tbaa !33
  store i32 %6, i32* %findIndex, align 4, !tbaa !18
  %m_manifoldsPtr = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 2
  %7 = load i32, i32* %findIndex, align 4, !tbaa !18
  %m_manifoldsPtr2 = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray* %m_manifoldsPtr2)
  %sub = sub nsw i32 %call, 1
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4swapEii(%class.btAlignedObjectArray* %m_manifoldsPtr, i32 %7, i32 %sub)
  %8 = load i32, i32* %findIndex, align 4, !tbaa !18
  %m_manifoldsPtr3 = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 2
  %9 = load i32, i32* %findIndex, align 4, !tbaa !18
  %call4 = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray* %m_manifoldsPtr3, i32 %9)
  %10 = load %class.btPersistentManifold*, %class.btPersistentManifold** %call4, align 4, !tbaa !2
  %m_index1a5 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %10, i32 0, i32 9
  store i32 %8, i32* %m_index1a5, align 4, !tbaa !33
  %m_manifoldsPtr6 = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8pop_backEv(%class.btAlignedObjectArray* %m_manifoldsPtr6)
  %11 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4, !tbaa !2
  %m_persistentManifoldPoolAllocator = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 6
  %12 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_persistentManifoldPoolAllocator, align 4, !tbaa !17
  %13 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4, !tbaa !2
  %14 = bitcast %class.btPersistentManifold* %13 to i8*
  %call7 = call zeroext i1 @_ZN15btPoolAllocator8validPtrEPv(%class.btPoolAllocator* %12, i8* %14)
  br i1 %call7, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_persistentManifoldPoolAllocator8 = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 6
  %15 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_persistentManifoldPoolAllocator8, align 4, !tbaa !17
  %16 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4, !tbaa !2
  %17 = bitcast %class.btPersistentManifold* %16 to i8*
  call void @_ZN15btPoolAllocator10freeMemoryEPv(%class.btPoolAllocator* %15, i8* %17)
  br label %if.end

if.else:                                          ; preds = %entry
  %18 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4, !tbaa !2
  %19 = bitcast %class.btPersistentManifold* %18 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %19)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %20 = bitcast i32* %findIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4swapEii(%class.btAlignedObjectArray* %this, i32 %index0, i32 %index1) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %class.btPersistentManifold*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %index0, i32* %index0.addr, align 4, !tbaa !18
  store i32 %index1, i32* %index1.addr, align 4, !tbaa !18
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast %class.btPersistentManifold** %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !51
  %2 = load i32, i32* %index0.addr, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %1, i32 %2
  %3 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx, align 4, !tbaa !2
  store %class.btPersistentManifold* %3, %class.btPersistentManifold** %temp, align 4, !tbaa !2
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data2, align 4, !tbaa !51
  %5 = load i32, i32* %index1.addr, align 4, !tbaa !18
  %arrayidx3 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %4, i32 %5
  %6 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx3, align 4, !tbaa !2
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data4, align 4, !tbaa !51
  %8 = load i32, i32* %index0.addr, align 4, !tbaa !18
  %arrayidx5 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %7, i32 %8
  store %class.btPersistentManifold* %6, %class.btPersistentManifold** %arrayidx5, align 4, !tbaa !2
  %9 = load %class.btPersistentManifold*, %class.btPersistentManifold** %temp, align 4, !tbaa !2
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %10 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data6, align 4, !tbaa !51
  %11 = load i32, i32* %index1.addr, align 4, !tbaa !18
  %arrayidx7 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %10, i32 %11
  store %class.btPersistentManifold* %9, %class.btPersistentManifold** %arrayidx7, align 4, !tbaa !2
  %12 = bitcast %class.btPersistentManifold** %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !18
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !51
  %1 = load i32, i32* %n.addr, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %0, i32 %1
  ret %class.btPersistentManifold** %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8pop_backEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !50
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4, !tbaa !50
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !51
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4, !tbaa !50
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %1, i32 %2
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZN15btPoolAllocator8validPtrEPv(%class.btPoolAllocator* %this, i8* %ptr) #2 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btPoolAllocator*, align 4
  %ptr.addr = alloca i8*, align 4
  store %class.btPoolAllocator* %this, %class.btPoolAllocator** %this.addr, align 4, !tbaa !2
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btPoolAllocator*, %class.btPoolAllocator** %this.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  %tobool = icmp ne i8* %0, null
  br i1 %tobool, label %if.then, label %if.end5

if.then:                                          ; preds = %entry
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  %m_pool = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 4
  %2 = load i8*, i8** %m_pool, align 4, !tbaa !52
  %cmp = icmp uge i8* %1, %2
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then
  %3 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  %m_pool2 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 4
  %4 = load i8*, i8** %m_pool2, align 4, !tbaa !52
  %m_maxElements = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 1
  %5 = load i32, i32* %m_maxElements, align 4, !tbaa !53
  %m_elemSize = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 0
  %6 = load i32, i32* %m_elemSize, align 4, !tbaa !54
  %mul = mul nsw i32 %5, %6
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %mul
  %cmp3 = icmp ult i8* %3, %add.ptr
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %land.lhs.true
  store i1 true, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %land.lhs.true, %if.then
  br label %if.end5

if.end5:                                          ; preds = %if.end, %entry
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %7 = load i1, i1* %retval, align 1
  ret i1 %7
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN15btPoolAllocator10freeMemoryEPv(%class.btPoolAllocator* %this, i8* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btPoolAllocator*, align 4
  %ptr.addr = alloca i8*, align 4
  store %class.btPoolAllocator* %this, %class.btPoolAllocator** %this.addr, align 4, !tbaa !2
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btPoolAllocator*, %class.btPoolAllocator** %this.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  %tobool = icmp ne i8* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_firstFree = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 3
  %1 = load i8*, i8** %m_firstFree, align 4, !tbaa !42
  %2 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  %3 = bitcast i8* %2 to i8**
  store i8* %1, i8** %3, align 4, !tbaa !2
  %4 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  %m_firstFree2 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 3
  store i8* %4, i8** %m_firstFree2, align 4, !tbaa !42
  %m_freeCount = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 2
  %5 = load i32, i32* %m_freeCount, align 4, !tbaa !40
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %m_freeCount, align 4, !tbaa !40
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #6

define hidden %class.btCollisionAlgorithm* @_ZN21btCollisionDispatcher13findAlgorithmEPK24btCollisionObjectWrapperS2_P20btPersistentManifold(%class.btCollisionDispatcher* %this, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap, %class.btPersistentManifold* %sharedManifold) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %sharedManifold.addr = alloca %class.btPersistentManifold*, align 4
  %ci = alloca %struct.btCollisionAlgorithmConstructionInfo, align 4
  %algo = alloca %class.btCollisionAlgorithm*, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  store %class.btPersistentManifold* %sharedManifold, %class.btPersistentManifold** %sharedManifold.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %0 = bitcast %struct.btCollisionAlgorithmConstructionInfo* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #9
  %call = call %struct.btCollisionAlgorithmConstructionInfo* @_ZN36btCollisionAlgorithmConstructionInfoC2Ev(%struct.btCollisionAlgorithmConstructionInfo* %ci)
  %1 = bitcast %class.btCollisionDispatcher* %this1 to %class.btDispatcher*
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %ci, i32 0, i32 0
  store %class.btDispatcher* %1, %class.btDispatcher** %m_dispatcher1, align 4, !tbaa !55
  %2 = load %class.btPersistentManifold*, %class.btPersistentManifold** %sharedManifold.addr, align 4, !tbaa !2
  %m_manifold = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %ci, i32 0, i32 1
  store %class.btPersistentManifold* %2, %class.btPersistentManifold** %m_manifold, align 4, !tbaa !57
  %3 = bitcast %class.btCollisionAlgorithm** %algo to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %m_doubleDispatch = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 7
  %4 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %call2 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %4)
  %call3 = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %call2)
  %arrayidx = getelementptr inbounds [36 x [36 x %struct.btCollisionAlgorithmCreateFunc*]], [36 x [36 x %struct.btCollisionAlgorithmCreateFunc*]]* %m_doubleDispatch, i32 0, i32 %call3
  %5 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call4 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %5)
  %call5 = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %call4)
  %arrayidx6 = getelementptr inbounds [36 x %struct.btCollisionAlgorithmCreateFunc*], [36 x %struct.btCollisionAlgorithmCreateFunc*]* %arrayidx, i32 0, i32 %call5
  %6 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %arrayidx6, align 4, !tbaa !2
  %7 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %9 = bitcast %struct.btCollisionAlgorithmCreateFunc* %6 to %class.btCollisionAlgorithm* (%struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)***
  %vtable = load %class.btCollisionAlgorithm* (%struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)**, %class.btCollisionAlgorithm* (%struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)*** %9, align 4, !tbaa !6
  %vfn = getelementptr inbounds %class.btCollisionAlgorithm* (%struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)*, %class.btCollisionAlgorithm* (%struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)** %vtable, i64 2
  %10 = load %class.btCollisionAlgorithm* (%struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)*, %class.btCollisionAlgorithm* (%struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)** %vfn, align 4
  %call7 = call %class.btCollisionAlgorithm* %10(%struct.btCollisionAlgorithmCreateFunc* %6, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %7, %struct.btCollisionObjectWrapper* %8)
  store %class.btCollisionAlgorithm* %call7, %class.btCollisionAlgorithm** %algo, align 4, !tbaa !2
  %11 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %algo, align 4, !tbaa !2
  %12 = bitcast %class.btCollisionAlgorithm** %algo to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  %13 = bitcast %struct.btCollisionAlgorithmConstructionInfo* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %13) #9
  ret %class.btCollisionAlgorithm* %11
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btCollisionAlgorithmConstructionInfo* @_ZN36btCollisionAlgorithmConstructionInfoC2Ev(%struct.btCollisionAlgorithmConstructionInfo* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  store %struct.btCollisionAlgorithmConstructionInfo* %this, %struct.btCollisionAlgorithmConstructionInfo** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %this.addr, align 4
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %this1, i32 0, i32 0
  store %class.btDispatcher* null, %class.btDispatcher** %m_dispatcher1, align 4, !tbaa !55
  %m_manifold = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %this1, i32 0, i32 1
  store %class.btPersistentManifold* null, %class.btPersistentManifold** %m_manifold, align 4, !tbaa !57
  ret %struct.btCollisionAlgorithmConstructionInfo* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_shape = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 1
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %m_shape, align 4, !tbaa !58
  ret %class.btCollisionShape* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_shapeType, align 4, !tbaa !60
  ret i32 %0
}

define hidden zeroext i1 @_ZN21btCollisionDispatcher13needsResponseEPK17btCollisionObjectS2_(%class.btCollisionDispatcher* %this, %class.btCollisionObject* %body0, %class.btCollisionObject* %body1) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  %body0.addr = alloca %class.btCollisionObject*, align 4
  %body1.addr = alloca %class.btCollisionObject*, align 4
  %hasResponse = alloca i8, align 1
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %body0, %class.btCollisionObject** %body0.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %body1, %class.btCollisionObject** %body1.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %hasResponse) #9
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %body0.addr, align 4, !tbaa !2
  %call = call zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %0)
  br i1 %call, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %body1.addr, align 4, !tbaa !2
  %call2 = call zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %1)
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %2 = phi i1 [ false, %entry ], [ %call2, %land.rhs ]
  %frombool = zext i1 %2 to i8
  store i8 %frombool, i8* %hasResponse, align 1, !tbaa !62
  %3 = load i8, i8* %hasResponse, align 1, !tbaa !62, !range !63
  %tobool = trunc i8 %3 to i1
  br i1 %tobool, label %land.rhs3, label %land.end6

land.rhs3:                                        ; preds = %land.end
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %body0.addr, align 4, !tbaa !2
  %call4 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %4)
  br i1 %call4, label %lor.rhs, label %lor.end

lor.rhs:                                          ; preds = %land.rhs3
  %5 = load %class.btCollisionObject*, %class.btCollisionObject** %body1.addr, align 4, !tbaa !2
  %call5 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %5)
  %lnot = xor i1 %call5, true
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %land.rhs3
  %6 = phi i1 [ true, %land.rhs3 ], [ %lnot, %lor.rhs ]
  br label %land.end6

land.end6:                                        ; preds = %lor.end, %land.end
  %7 = phi i1 [ false, %land.end ], [ %6, %lor.end ]
  %frombool7 = zext i1 %7 to i8
  store i8 %frombool7, i8* %hasResponse, align 1, !tbaa !62
  %8 = load i8, i8* %hasResponse, align 1, !tbaa !62, !range !63
  %tobool8 = trunc i8 %8 to i1
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %hasResponse) #9
  ret i1 %tobool8
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4, !tbaa !64
  %and = and i32 %0, 4
  %cmp = icmp eq i32 %and, 0
  ret i1 %cmp
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4, !tbaa !64
  %and = and i32 %0, 3
  %cmp = icmp ne i32 %and, 0
  ret i1 %cmp
}

define hidden zeroext i1 @_ZN21btCollisionDispatcher14needsCollisionEPK17btCollisionObjectS2_(%class.btCollisionDispatcher* %this, %class.btCollisionObject* %body0, %class.btCollisionObject* %body1) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  %body0.addr = alloca %class.btCollisionObject*, align 4
  %body1.addr = alloca %class.btCollisionObject*, align 4
  %needsCollision = alloca i8, align 1
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %body0, %class.btCollisionObject** %body0.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %body1, %class.btCollisionObject** %body1.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %needsCollision) #9
  store i8 1, i8* %needsCollision, align 1, !tbaa !62
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %body0.addr, align 4, !tbaa !2
  %call = call zeroext i1 @_ZNK17btCollisionObject8isActiveEv(%class.btCollisionObject* %0)
  br i1 %call, label %if.else, label %land.lhs.true

land.lhs.true:                                    ; preds = %entry
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %body1.addr, align 4, !tbaa !2
  %call2 = call zeroext i1 @_ZNK17btCollisionObject8isActiveEv(%class.btCollisionObject* %1)
  br i1 %call2, label %if.else, label %if.then

if.then:                                          ; preds = %land.lhs.true
  store i8 0, i8* %needsCollision, align 1, !tbaa !62
  br label %if.end5

if.else:                                          ; preds = %land.lhs.true, %entry
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %body0.addr, align 4, !tbaa !2
  %3 = load %class.btCollisionObject*, %class.btCollisionObject** %body1.addr, align 4, !tbaa !2
  %call3 = call zeroext i1 @_ZNK17btCollisionObject16checkCollideWithEPKS_(%class.btCollisionObject* %2, %class.btCollisionObject* %3)
  br i1 %call3, label %if.end, label %if.then4

if.then4:                                         ; preds = %if.else
  store i8 0, i8* %needsCollision, align 1, !tbaa !62
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  br label %if.end5

if.end5:                                          ; preds = %if.end, %if.then
  %4 = load i8, i8* %needsCollision, align 1, !tbaa !62, !range !63
  %tobool = trunc i8 %4 to i1
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %needsCollision) #9
  ret i1 %tobool
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject8isActiveEv(%class.btCollisionObject* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %call = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %this1)
  %cmp = icmp ne i32 %call, 2
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %call2 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %this1)
  %cmp3 = icmp ne i32 %call2, 5
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %0 = phi i1 [ false, %entry ], [ %cmp3, %land.rhs ]
  ret i1 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject16checkCollideWithEPKS_(%class.btCollisionObject* %this, %class.btCollisionObject* %co) #7 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btCollisionObject*, align 4
  %co.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %co, %class.btCollisionObject** %co.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_checkCollideWith = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 25
  %0 = load i32, i32* %m_checkCollideWith, align 4, !tbaa !65
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %co.addr, align 4, !tbaa !2
  %2 = bitcast %class.btCollisionObject* %this1 to i1 (%class.btCollisionObject*, %class.btCollisionObject*)***
  %vtable = load i1 (%class.btCollisionObject*, %class.btCollisionObject*)**, i1 (%class.btCollisionObject*, %class.btCollisionObject*)*** %2, align 4, !tbaa !6
  %vfn = getelementptr inbounds i1 (%class.btCollisionObject*, %class.btCollisionObject*)*, i1 (%class.btCollisionObject*, %class.btCollisionObject*)** %vtable, i64 0
  %3 = load i1 (%class.btCollisionObject*, %class.btCollisionObject*)*, i1 (%class.btCollisionObject*, %class.btCollisionObject*)** %vfn, align 4
  %call = call zeroext i1 %3(%class.btCollisionObject* %this1, %class.btCollisionObject* %1)
  store i1 %call, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load i1, i1* %retval, align 1
  ret i1 %4
}

define hidden void @_ZN21btCollisionDispatcher25dispatchAllCollisionPairsEP22btOverlappingPairCacheRK16btDispatcherInfoP12btDispatcher(%class.btCollisionDispatcher* %this, %class.btOverlappingPairCache* %pairCache, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo, %class.btDispatcher* %dispatcher) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  %pairCache.addr = alloca %class.btOverlappingPairCache*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %collisionCallback = alloca %class.btCollisionPairCallback, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4, !tbaa !2
  store %class.btOverlappingPairCache* %pairCache, %class.btOverlappingPairCache** %pairCache.addr, align 4, !tbaa !2
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %0 = bitcast %class.btCollisionPairCallback* %collisionCallback to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %0) #9
  %1 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  %call = call %class.btCollisionPairCallback* @_ZN23btCollisionPairCallbackC2ERK16btDispatcherInfoP21btCollisionDispatcher(%class.btCollisionPairCallback* %collisionCallback, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %1, %class.btCollisionDispatcher* %this1)
  %2 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %pairCache.addr, align 4, !tbaa !2
  %3 = bitcast %class.btCollisionPairCallback* %collisionCallback to %struct.btOverlapCallback*
  %4 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %5 = bitcast %class.btOverlappingPairCache* %2 to void (%class.btOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)***
  %vtable = load void (%class.btOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)**, void (%class.btOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)*** %5, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)*, void (%class.btOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)** %vtable, i64 12
  %6 = load void (%class.btOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)*, void (%class.btOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)** %vfn, align 4
  call void %6(%class.btOverlappingPairCache* %2, %struct.btOverlapCallback* %3, %class.btDispatcher* %4)
  %call2 = call %class.btCollisionPairCallback* bitcast (%struct.btOverlapCallback* (%struct.btOverlapCallback*)* @_ZN17btOverlapCallbackD2Ev to %class.btCollisionPairCallback* (%class.btCollisionPairCallback*)*)(%class.btCollisionPairCallback* %collisionCallback) #9
  %7 = bitcast %class.btCollisionPairCallback* %collisionCallback to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %7) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btCollisionPairCallback* @_ZN23btCollisionPairCallbackC2ERK16btDispatcherInfoP21btCollisionDispatcher(%class.btCollisionPairCallback* returned %this, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo, %class.btCollisionDispatcher* %dispatcher) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionPairCallback*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %dispatcher.addr = alloca %class.btCollisionDispatcher*, align 4
  store %class.btCollisionPairCallback* %this, %class.btCollisionPairCallback** %this.addr, align 4, !tbaa !2
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  store %class.btCollisionDispatcher* %dispatcher, %class.btCollisionDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionPairCallback*, %class.btCollisionPairCallback** %this.addr, align 4
  %0 = bitcast %class.btCollisionPairCallback* %this1 to %struct.btOverlapCallback*
  %call = call %struct.btOverlapCallback* @_ZN17btOverlapCallbackC2Ev(%struct.btOverlapCallback* %0) #9
  %1 = bitcast %class.btCollisionPairCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV23btCollisionPairCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_dispatchInfo = getelementptr inbounds %class.btCollisionPairCallback, %class.btCollisionPairCallback* %this1, i32 0, i32 1
  %2 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  store %struct.btDispatcherInfo* %2, %struct.btDispatcherInfo** %m_dispatchInfo, align 4, !tbaa !2
  %m_dispatcher = getelementptr inbounds %class.btCollisionPairCallback, %class.btCollisionPairCallback* %this1, i32 0, i32 2
  %3 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %dispatcher.addr, align 4, !tbaa !2
  store %class.btCollisionDispatcher* %3, %class.btCollisionDispatcher** %m_dispatcher, align 4, !tbaa !66
  ret %class.btCollisionPairCallback* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionShape* @_ZN17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 9
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %m_collisionShape, align 4, !tbaa !35
  ret %class.btCollisionShape* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* returned %this, %struct.btCollisionObjectWrapper* %parent, %class.btCollisionShape* %shape, %class.btCollisionObject* %collisionObject, %class.btTransform* nonnull align 4 dereferenceable(64) %worldTransform, i32 %partId, i32 %index) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %parent.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %shape.addr = alloca %class.btCollisionShape*, align 4
  %collisionObject.addr = alloca %class.btCollisionObject*, align 4
  %worldTransform.addr = alloca %class.btTransform*, align 4
  %partId.addr = alloca i32, align 4
  %index.addr = alloca i32, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %parent, %struct.btCollisionObjectWrapper** %parent.addr, align 4, !tbaa !2
  store %class.btCollisionShape* %shape, %class.btCollisionShape** %shape.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %collisionObject, %class.btCollisionObject** %collisionObject.addr, align 4, !tbaa !2
  store %class.btTransform* %worldTransform, %class.btTransform** %worldTransform.addr, align 4, !tbaa !2
  store i32 %partId, i32* %partId.addr, align 4, !tbaa !18
  store i32 %index, i32* %index.addr, align 4, !tbaa !18
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_parent = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 0
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %parent.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %0, %struct.btCollisionObjectWrapper** %m_parent, align 4, !tbaa !68
  %m_shape = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 1
  %1 = load %class.btCollisionShape*, %class.btCollisionShape** %shape.addr, align 4, !tbaa !2
  store %class.btCollisionShape* %1, %class.btCollisionShape** %m_shape, align 4, !tbaa !58
  %m_collisionObject = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 2
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %2, %class.btCollisionObject** %m_collisionObject, align 4, !tbaa !69
  %m_worldTransform = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 3
  %3 = load %class.btTransform*, %class.btTransform** %worldTransform.addr, align 4, !tbaa !2
  store %class.btTransform* %3, %class.btTransform** %m_worldTransform, align 4, !tbaa !2
  %m_partId = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 4
  %4 = load i32, i32* %partId.addr, align 4, !tbaa !18
  store i32 %4, i32* %m_partId, align 4, !tbaa !70
  %m_index = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 5
  %5 = load i32, i32* %index.addr, align 4, !tbaa !18
  store i32 %5, i32* %m_index, align 4, !tbaa !71
  ret %struct.btCollisionObjectWrapper* %this1
}

declare %class.btManifoldResult* @_ZN16btManifoldResultC1EPK24btCollisionObjectWrapperS2_(%class.btManifoldResult* returned, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*) unnamed_addr #6

define hidden i8* @_ZN21btCollisionDispatcher26allocateCollisionAlgorithmEi(%class.btCollisionDispatcher* %this, i32 %size) unnamed_addr #0 {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  %size.addr = alloca i32, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !18
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %m_collisionAlgorithmPoolAllocator = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 5
  %0 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_collisionAlgorithmPoolAllocator, align 4, !tbaa !16
  %call = call i32 @_ZNK15btPoolAllocator12getFreeCountEv(%class.btPoolAllocator* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_collisionAlgorithmPoolAllocator2 = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 5
  %1 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_collisionAlgorithmPoolAllocator2, align 4, !tbaa !16
  %2 = load i32, i32* %size.addr, align 4, !tbaa !18
  %call3 = call i8* @_ZN15btPoolAllocator8allocateEi(%class.btPoolAllocator* %1, i32 %2)
  store i8* %call3, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = load i32, i32* %size.addr, align 4, !tbaa !18
  %call4 = call i8* @_Z22btAlignedAllocInternalmi(i32 %3, i32 16)
  store i8* %call4, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load i8*, i8** %retval, align 4
  ret i8* %4
}

define hidden void @_ZN21btCollisionDispatcher22freeCollisionAlgorithmEPv(%class.btCollisionDispatcher* %this, i8* %ptr) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  %ptr.addr = alloca i8*, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4, !tbaa !2
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %m_collisionAlgorithmPoolAllocator = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 5
  %0 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_collisionAlgorithmPoolAllocator, align 4, !tbaa !16
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  %call = call zeroext i1 @_ZN15btPoolAllocator8validPtrEPv(%class.btPoolAllocator* %0, i8* %1)
  br i1 %call, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_collisionAlgorithmPoolAllocator2 = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 5
  %2 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_collisionAlgorithmPoolAllocator2, align 4, !tbaa !16
  %3 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_ZN15btPoolAllocator10freeMemoryEPv(%class.btPoolAllocator* %2, i8* %3)
  br label %if.end

if.else:                                          ; preds = %entry
  %4 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %4)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

define linkonce_odr hidden i32 @_ZNK21btCollisionDispatcher15getNumManifoldsEv(%class.btCollisionDispatcher* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %m_manifoldsPtr = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray* %m_manifoldsPtr)
  ret i32 %call
}

define linkonce_odr hidden %class.btPersistentManifold* @_ZN21btCollisionDispatcher26getManifoldByIndexInternalEi(%class.btCollisionDispatcher* %this, i32 %index) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  %index.addr = alloca i32, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !18
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %m_manifoldsPtr = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 2
  %0 = load i32, i32* %index.addr, align 4, !tbaa !18
  %call = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray* %m_manifoldsPtr, i32 %0)
  %1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %call, align 4, !tbaa !2
  ret %class.btPersistentManifold* %1
}

define linkonce_odr hidden %class.btPersistentManifold** @_ZN21btCollisionDispatcher26getInternalManifoldPointerEv(%class.btCollisionDispatcher* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %m_manifoldsPtr = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray* %m_manifoldsPtr)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_manifoldsPtr2 = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 2
  %call3 = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray* %m_manifoldsPtr2, i32 0)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %class.btPersistentManifold** [ %call3, %cond.true ], [ null, %cond.false ]
  ret %class.btPersistentManifold** %cond
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btPoolAllocator* @_ZN21btCollisionDispatcher23getInternalManifoldPoolEv(%class.btCollisionDispatcher* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %m_persistentManifoldPoolAllocator = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 6
  %0 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_persistentManifoldPoolAllocator, align 4, !tbaa !17
  ret %class.btPoolAllocator* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btPoolAllocator* @_ZNK21btCollisionDispatcher23getInternalManifoldPoolEv(%class.btCollisionDispatcher* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %m_persistentManifoldPoolAllocator = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 6
  %0 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_persistentManifoldPoolAllocator, align 4, !tbaa !17
  ret %class.btPoolAllocator* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultC2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %this, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  %0 = bitcast %"struct.btDiscreteCollisionDetectorInterface::Result"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTVN36btDiscreteCollisionDetectorInterface6ResultE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  ret %"struct.btDiscreteCollisionDetectorInterface::Result"* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN36btDiscreteCollisionDetectorInterface6ResultD0Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %this, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  call void @llvm.trap() #11
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #8

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btTypedObject* @_ZN13btTypedObjectC2Ei(%struct.btTypedObject* returned %this, i32 %objectType) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btTypedObject*, align 4
  %objectType.addr = alloca i32, align 4
  store %struct.btTypedObject* %this, %struct.btTypedObject** %this.addr, align 4, !tbaa !2
  store i32 %objectType, i32* %objectType.addr, align 4, !tbaa !18
  %this1 = load %struct.btTypedObject*, %struct.btTypedObject** %this.addr, align 4
  %m_objectType = getelementptr inbounds %struct.btTypedObject, %struct.btTypedObject* %this1, i32 0, i32 0
  %0 = load i32, i32* %objectType.addr, align 4, !tbaa !18
  store i32 %0, i32* %m_objectType, align 4, !tbaa !72
  ret %struct.btTypedObject* %this1
}

define linkonce_odr hidden %class.btManifoldPoint* @_ZN15btManifoldPointC2Ev(%class.btManifoldPoint* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btManifoldPoint*, align 4
  store %class.btManifoldPoint* %this, %class.btManifoldPoint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btManifoldPoint*, %class.btManifoldPoint** %this.addr, align 4
  %m_localPointA = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_localPointA)
  %m_localPointB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_localPointB)
  %m_positionWorldOnB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_positionWorldOnB)
  %m_positionWorldOnA = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 3
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_positionWorldOnA)
  %m_normalWorldOnB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_normalWorldOnB)
  %m_userPersistentData = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 13
  store i8* null, i8** %m_userPersistentData, align 4, !tbaa !74
  %m_lateralFrictionInitialized = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 14
  store i8 0, i8* %m_lateralFrictionInitialized, align 4, !tbaa !76
  %m_appliedImpulse = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 15
  store float 0.000000e+00, float* %m_appliedImpulse, align 4, !tbaa !77
  %m_appliedImpulseLateral1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 16
  store float 0.000000e+00, float* %m_appliedImpulseLateral1, align 4, !tbaa !78
  %m_appliedImpulseLateral2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 17
  store float 0.000000e+00, float* %m_appliedImpulseLateral2, align 4, !tbaa !79
  %m_contactMotion1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 18
  store float 0.000000e+00, float* %m_contactMotion1, align 4, !tbaa !80
  %m_contactMotion2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 19
  store float 0.000000e+00, float* %m_contactMotion2, align 4, !tbaa !81
  %m_contactCFM1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 20
  store float 0.000000e+00, float* %m_contactCFM1, align 4, !tbaa !82
  %m_contactCFM2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 21
  store float 0.000000e+00, float* %m_contactCFM2, align 4, !tbaa !83
  %m_lifeTime = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 22
  store i32 0, i32* %m_lifeTime, align 4, !tbaa !84
  %m_lateralFrictionDir1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 23
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_lateralFrictionDir1)
  %m_lateralFrictionDir2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 24
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_lateralFrictionDir2)
  ret %class.btManifoldPoint* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

declare void @_ZN20btPersistentManifold14clearUserCacheER15btManifoldPoint(%class.btPersistentManifold*, %class.btManifoldPoint* nonnull align 4 dereferenceable(184)) #6

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_activationState1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 15
  %0 = load i32, i32* %m_activationState1, align 4, !tbaa !85
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %struct.btOverlapCallback* @_ZN17btOverlapCallbackC2Ev(%struct.btOverlapCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btOverlapCallback*, align 4
  store %struct.btOverlapCallback* %this, %struct.btOverlapCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btOverlapCallback*, %struct.btOverlapCallback** %this.addr, align 4
  %0 = bitcast %struct.btOverlapCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV17btOverlapCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  ret %struct.btOverlapCallback* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN23btCollisionPairCallbackD0Ev(%class.btCollisionPairCallback* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionPairCallback*, align 4
  store %class.btCollisionPairCallback* %this, %class.btCollisionPairCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionPairCallback*, %class.btCollisionPairCallback** %this.addr, align 4
  %call = call %class.btCollisionPairCallback* bitcast (%struct.btOverlapCallback* (%struct.btOverlapCallback*)* @_ZN17btOverlapCallbackD2Ev to %class.btCollisionPairCallback* (%class.btCollisionPairCallback*)*)(%class.btCollisionPairCallback* %this1) #9
  %0 = bitcast %class.btCollisionPairCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

define linkonce_odr hidden zeroext i1 @_ZN23btCollisionPairCallback14processOverlapER16btBroadphasePair(%class.btCollisionPairCallback* %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %pair) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionPairCallback*, align 4
  %pair.addr = alloca %struct.btBroadphasePair*, align 4
  store %class.btCollisionPairCallback* %this, %class.btCollisionPairCallback** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphasePair* %pair, %struct.btBroadphasePair** %pair.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionPairCallback*, %class.btCollisionPairCallback** %this.addr, align 4
  %m_dispatcher = getelementptr inbounds %class.btCollisionPairCallback, %class.btCollisionPairCallback* %this1, i32 0, i32 2
  %0 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %m_dispatcher, align 4, !tbaa !66
  %call = call void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)* @_ZNK21btCollisionDispatcher15getNearCallbackEv(%class.btCollisionDispatcher* %0)
  %1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4, !tbaa !2
  %m_dispatcher2 = getelementptr inbounds %class.btCollisionPairCallback, %class.btCollisionPairCallback* %this1, i32 0, i32 2
  %2 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %m_dispatcher2, align 4, !tbaa !66
  %m_dispatchInfo = getelementptr inbounds %class.btCollisionPairCallback, %class.btCollisionPairCallback* %this1, i32 0, i32 1
  %3 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %m_dispatchInfo, align 4, !tbaa !86
  call void %call(%struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %1, %class.btCollisionDispatcher* nonnull align 4 dereferenceable(5260) %2, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %3)
  ret i1 false
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btOverlapCallback* @_ZN17btOverlapCallbackD2Ev(%struct.btOverlapCallback* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btOverlapCallback*, align 4
  store %struct.btOverlapCallback* %this, %struct.btOverlapCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btOverlapCallback*, %struct.btOverlapCallback** %this.addr, align 4
  ret %struct.btOverlapCallback* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN17btOverlapCallbackD0Ev(%struct.btOverlapCallback* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btOverlapCallback*, align 4
  store %struct.btOverlapCallback* %this, %struct.btOverlapCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btOverlapCallback*, %struct.btOverlapCallback** %this.addr, align 4
  call void @llvm.trap() #11
  unreachable
}

; Function Attrs: nounwind
define linkonce_odr hidden void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)* @_ZNK21btCollisionDispatcher15getNearCallbackEv(%class.btCollisionDispatcher* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %m_nearCallback = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 4
  %0 = load void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)*, void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)** %m_nearCallback, align 4, !tbaa !19
  ret void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !87
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data, align 4, !tbaa !51
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !50
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !88
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv(%class.btAlignedObjectArray* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !18
  store i32 %last, i32* %last.addr, align 4, !tbaa !18
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %first.addr, align 4, !tbaa !18
  store i32 %1, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !18
  %3 = load i32, i32* %last.addr, align 4, !tbaa !18
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !51
  %5 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !18
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !51
  %tobool = icmp ne %class.btPersistentManifold** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !87, !range !63
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data4, align 4, !tbaa !51
  call void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator* %m_allocator, %class.btPersistentManifold** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data5, align 4, !tbaa !51
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator* %this, %class.btPersistentManifold** %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store %class.btPersistentManifold** %ptr, %class.btPersistentManifold*** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btPersistentManifold** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !88
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !18
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !18
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btPersistentManifold*** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !18
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btPersistentManifold**
  store %class.btPersistentManifold** %3, %class.btPersistentManifold*** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray* %this1)
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %class.btPersistentManifold** %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !87
  %5 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btPersistentManifold** %5, %class.btPersistentManifold*** %m_data, align 4, !tbaa !51
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !18
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !88
  %7 = bitcast %class.btPersistentManifold*** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !18
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !18
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !18
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #7 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !18
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !18
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !18
  %call = call %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator* %m_allocator, i32 %1, %class.btPersistentManifold*** null)
  %2 = bitcast %class.btPersistentManifold** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %class.btPersistentManifold** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btPersistentManifold**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !18
  store i32 %end, i32* %end.addr, align 4, !tbaa !18
  store %class.btPersistentManifold** %dest, %class.btPersistentManifold*** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %start.addr, align 4, !tbaa !18
  store i32 %1, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !18
  %3 = load i32, i32* %end.addr, align 4, !tbaa !18
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %4, i32 %5
  %6 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %7 = bitcast i8* %6 to %class.btPersistentManifold**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !51
  %9 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx2 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %8, i32 %9
  %10 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx2, align 4, !tbaa !2
  store %class.btPersistentManifold* %10, %class.btPersistentManifold** %7, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !18
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  ret void
}

define linkonce_odr hidden %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator* %this, i32 %n, %class.btPersistentManifold*** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btPersistentManifold***, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !18
  store %class.btPersistentManifold*** %hint, %class.btPersistentManifold**** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !18
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btPersistentManifold**
  ret %class.btPersistentManifold** %1
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { cold noreturn nounwind }
attributes #9 = { nounwind }
attributes #10 = { builtin nounwind }
attributes #11 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{!9, !10, i64 4}
!9 = !{!"_ZTS21btCollisionDispatcher", !10, i64 4, !11, i64 8, !14, i64 28, !3, i64 60, !3, i64 64, !3, i64 68, !4, i64 72, !3, i64 5256}
!10 = !{!"int", !4, i64 0}
!11 = !{!"_ZTS20btAlignedObjectArrayIP20btPersistentManifoldE", !12, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !13, i64 16}
!12 = !{!"_ZTS18btAlignedAllocatorIP20btPersistentManifoldLj16EE"}
!13 = !{!"bool", !4, i64 0}
!14 = !{!"_ZTS16btManifoldResult", !3, i64 4, !3, i64 8, !3, i64 12, !10, i64 16, !10, i64 20, !10, i64 24, !10, i64 28}
!15 = !{!9, !3, i64 5256}
!16 = !{!9, !3, i64 64}
!17 = !{!9, !3, i64 68}
!18 = !{!10, !10, i64 0}
!19 = !{!9, !3, i64 60}
!20 = !{!21, !3, i64 0}
!21 = !{!"_ZTS16btBroadphasePair", !3, i64 0, !3, i64 4, !3, i64 8, !4, i64 12}
!22 = !{!23, !3, i64 0}
!23 = !{!"_ZTS17btBroadphaseProxy", !3, i64 0, !24, i64 4, !24, i64 6, !3, i64 8, !10, i64 12, !25, i64 16, !25, i64 32}
!24 = !{!"short", !4, i64 0}
!25 = !{!"_ZTS9btVector3", !4, i64 0}
!26 = !{!21, !3, i64 4}
!27 = !{!21, !3, i64 8}
!28 = !{!29, !10, i64 8}
!29 = !{!"_ZTS16btDispatcherInfo", !30, i64 0, !10, i64 4, !10, i64 8, !30, i64 12, !13, i64 16, !3, i64 20, !13, i64 24, !13, i64 25, !13, i64 26, !30, i64 28, !13, i64 32, !30, i64 36}
!30 = !{!"float", !4, i64 0}
!31 = !{!30, !30, i64 0}
!32 = !{!29, !30, i64 12}
!33 = !{!34, !10, i64 768}
!34 = !{!"_ZTS20btPersistentManifold", !4, i64 4, !3, i64 740, !3, i64 744, !10, i64 748, !30, i64 752, !30, i64 756, !10, i64 760, !10, i64 764, !10, i64 768}
!35 = !{!36, !3, i64 192}
!36 = !{!"_ZTS17btCollisionObject", !37, i64 4, !37, i64 68, !25, i64 132, !25, i64 148, !25, i64 164, !10, i64 180, !30, i64 184, !3, i64 188, !3, i64 192, !3, i64 196, !3, i64 200, !10, i64 204, !10, i64 208, !10, i64 212, !10, i64 216, !30, i64 220, !30, i64 224, !30, i64 228, !30, i64 232, !10, i64 236, !4, i64 240, !30, i64 244, !30, i64 248, !30, i64 252, !10, i64 256, !10, i64 260}
!37 = !{!"_ZTS11btTransform", !38, i64 0, !25, i64 48}
!38 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!39 = !{!36, !30, i64 184}
!40 = !{!41, !10, i64 8}
!41 = !{!"_ZTS15btPoolAllocator", !10, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !3, i64 16}
!42 = !{!41, !3, i64 12}
!43 = !{!44, !44, i64 0}
!44 = !{!"long", !4, i64 0}
!45 = !{!34, !3, i64 740}
!46 = !{!34, !3, i64 744}
!47 = !{!34, !10, i64 748}
!48 = !{!34, !30, i64 752}
!49 = !{!34, !30, i64 756}
!50 = !{!11, !10, i64 4}
!51 = !{!11, !3, i64 12}
!52 = !{!41, !3, i64 16}
!53 = !{!41, !10, i64 4}
!54 = !{!41, !10, i64 0}
!55 = !{!56, !3, i64 0}
!56 = !{!"_ZTS36btCollisionAlgorithmConstructionInfo", !3, i64 0, !3, i64 4}
!57 = !{!56, !3, i64 4}
!58 = !{!59, !3, i64 4}
!59 = !{!"_ZTS24btCollisionObjectWrapper", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !10, i64 16, !10, i64 20}
!60 = !{!61, !10, i64 4}
!61 = !{!"_ZTS16btCollisionShape", !10, i64 4, !3, i64 8}
!62 = !{!13, !13, i64 0}
!63 = !{i8 0, i8 2}
!64 = !{!36, !10, i64 204}
!65 = !{!36, !10, i64 256}
!66 = !{!67, !3, i64 8}
!67 = !{!"_ZTS23btCollisionPairCallback", !3, i64 4, !3, i64 8}
!68 = !{!59, !3, i64 0}
!69 = !{!59, !3, i64 8}
!70 = !{!59, !10, i64 16}
!71 = !{!59, !10, i64 20}
!72 = !{!73, !10, i64 0}
!73 = !{!"_ZTS13btTypedObject", !10, i64 0}
!74 = !{!75, !3, i64 112}
!75 = !{!"_ZTS15btManifoldPoint", !25, i64 0, !25, i64 16, !25, i64 32, !25, i64 48, !25, i64 64, !30, i64 80, !30, i64 84, !30, i64 88, !30, i64 92, !10, i64 96, !10, i64 100, !10, i64 104, !10, i64 108, !3, i64 112, !13, i64 116, !30, i64 120, !30, i64 124, !30, i64 128, !30, i64 132, !30, i64 136, !30, i64 140, !30, i64 144, !10, i64 148, !25, i64 152, !25, i64 168}
!76 = !{!75, !13, i64 116}
!77 = !{!75, !30, i64 120}
!78 = !{!75, !30, i64 124}
!79 = !{!75, !30, i64 128}
!80 = !{!75, !30, i64 132}
!81 = !{!75, !30, i64 136}
!82 = !{!75, !30, i64 140}
!83 = !{!75, !30, i64 144}
!84 = !{!75, !10, i64 148}
!85 = !{!36, !10, i64 216}
!86 = !{!67, !3, i64 4}
!87 = !{!11, !13, i64 16}
!88 = !{!11, !10, i64 8}
