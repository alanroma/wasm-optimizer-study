; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/ConstraintSolver/btHingeConstraint.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/ConstraintSolver/btHingeConstraint.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btVector3 = type { [4 x float] }
%class.btHingeConstraint = type { %class.btTypedConstraint, [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry], %class.btTransform, %class.btTransform, float, float, %class.btAngularLimit, float, float, float, float, i8, i8, i8, i8, i8, float, i32, float, float, float }
%class.btTypedConstraint = type { i32 (...)**, %struct.btTypedObject, i32, %union.anon, float, i8, i8, i32, %class.btRigidBody*, %class.btRigidBody*, float, float, %struct.btJointFeedback* }
%struct.btTypedObject = type { i32 }
%union.anon = type { i32 }
%struct.btJointFeedback = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%class.btJacobianEntry = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btAngularLimit = type <{ float, float, float, float, float, float, float, i8, [3 x i8] }>
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon.0, float, float, float, i32, i32 }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%union.anon.0 = type { i8* }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }
%"struct.btTypedConstraint::btConstraintInfo1" = type { i32, i32 }
%"struct.btTypedConstraint::btConstraintInfo2" = type { float, float, float*, float*, float*, float*, i32, float*, float*, float*, float*, i32*, i32, float }
%class.btAlignedObjectArray.1 = type opaque
%struct.btSolverBody = type { %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btRigidBody* }
%class.btSerializer = type opaque
%struct.btHingeConstraintFloatData = type { %struct.btTypedConstraintData, %struct.btTransformFloatData, %struct.btTransformFloatData, i32, i32, i32, float, float, float, float, float, float, float }
%struct.btTypedConstraintData = type { %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData*, i8*, i32, i32, i32, i32, float, float, i32, i32, float, i32 }
%struct.btRigidBodyFloatData = type { %struct.btCollisionObjectFloatData, %struct.btMatrix3x3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, float, float, float, float, float, float, float, float, i32 }
%struct.btCollisionObjectFloatData = type { i8*, i8*, %struct.btCollisionShapeData*, i8*, %struct.btTransformFloatData, %struct.btTransformFloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, float, float, float, float, float, float, float, i32, i32, i32, i32, i32, i32, i32, [4 x i8] }
%struct.btCollisionShapeData = type opaque
%struct.btMatrix3x3FloatData = type { [3 x %struct.btVector3FloatData] }
%struct.btVector3FloatData = type { [4 x float] }
%struct.btTransformFloatData = type { %struct.btMatrix3x3FloatData, %struct.btVector3FloatData }

$_ZN15btJacobianEntryC2Ev = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN14btAngularLimitC2Ev = comdat any

$_ZN11btTransform9getOriginEv = comdat any

$_ZNK11btRigidBody24getCenterOfMassTransformEv = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x39getColumnEi = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZngRK9btVector3 = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZN11btTransform8getBasisEv = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_Z15shortestArcQuatRK9btVector3S1_ = comdat any

$_Z10quatRotateRK12btQuaternionRK9btVector3 = comdat any

$_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_ = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZNK11btTransformmlERK9btVector3 = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZNK9btVector310normalizedEv = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_ZNK11btRigidBody23getCenterOfMassPositionEv = comdat any

$_ZNK11btRigidBody22getInvInertiaDiagLocalEv = comdat any

$_ZNK11btRigidBody10getInvMassEv = comdat any

$_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f = comdat any

$_ZN17btHingeConstraint13getRigidBodyAEv = comdat any

$_ZN15btJacobianEntryC2ERK9btVector3RK11btMatrix3x3S5_S2_S2_ = comdat any

$_ZNK11btRigidBody32computeAngularImpulseDenominatorERK9btVector3 = comdat any

$_ZN17btHingeConstraint13getRigidBodyBEv = comdat any

$_ZN17btHingeConstraint13getSolveLimitEv = comdat any

$_ZN17btHingeConstraint21getEnableAngularMotorEv = comdat any

$_ZNK11btRigidBody18getAngularVelocityEv = comdat any

$_ZNK11btTransformmlERKS_ = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZNK9btVector322getSkewSymmetricMatrixEPS_S0_S0_ = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZNK14btAngularLimit13getCorrectionEv = comdat any

$_ZNK17btHingeConstraint13getLowerLimitEv = comdat any

$_ZNK17btHingeConstraint13getUpperLimitEv = comdat any

$_ZNK14btAngularLimit19getRelaxationFactorEv = comdat any

$_ZNK14btAngularLimit13getBiasFactorEv = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_Z7btAtan2ff = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZmlRK12btQuaternionS1_ = comdat any

$_ZNK11btTransform11getRotationEv = comdat any

$_ZNK12btQuaternion7inverseEv = comdat any

$_ZN12btQuaternion9normalizeEv = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZNK12btQuaternion8getAngleEv = comdat any

$_ZNK12btQuaternionngEv = comdat any

$_ZNK10btQuadWord4getZEv = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_Z6btSqrtf = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN17btTypedConstraintD2Ev = comdat any

$_ZN17btHingeConstraintD0Ev = comdat any

$_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif = comdat any

$_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f = comdat any

$_ZNK17btHingeConstraint28calculateSerializeBufferSizeEv = comdat any

$_ZNK17btHingeConstraint9serializeEPvP12btSerializer = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN12btQuaternionC2ERKfS1_S1_S1_ = comdat any

$_ZN10btQuadWordC2ERKfS1_S1_S1_ = comdat any

$_ZmlRK12btQuaternionRK9btVector3 = comdat any

$_ZN12btQuaternionmLERKS_ = comdat any

$_ZNK10btQuadWord4getXEv = comdat any

$_ZNK10btQuadWord4getYEv = comdat any

$_ZNK10btQuadWord1wEv = comdat any

$_ZNK10btQuadWord1yEv = comdat any

$_ZNK10btQuadWord1zEv = comdat any

$_ZNK10btQuadWord1xEv = comdat any

$_ZN10btQuadWord8setValueERKfS1_S1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZmlRK9btVector3RK11btMatrix3x3 = comdat any

$_ZNK11btRigidBody24getInvInertiaTensorWorldEv = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZNK14btAngularLimit7isLimitEv = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3 = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZN12btQuaternionC2Ev = comdat any

$_ZNK11btMatrix3x311getRotationER12btQuaternion = comdat any

$_ZN10btQuadWordC2Ev = comdat any

$_ZNK12btQuaternion6lengthEv = comdat any

$_ZN12btQuaterniondVERKf = comdat any

$_ZNK12btQuaternion7length2Ev = comdat any

$_ZNK12btQuaternion3dotERKS_ = comdat any

$_ZN12btQuaternionmLERKf = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_Z6btAcosf = comdat any

$_ZN17btHingeConstraintdlEPv = comdat any

$_ZNK11btTransform9serializeER20btTransformFloatData = comdat any

$_ZNK14btAngularLimit11getSoftnessEv = comdat any

$_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData = comdat any

$_ZNK9btVector39serializeER18btVector3FloatData = comdat any

$_Z6btFabsf = comdat any

@_ZTV17btHingeConstraint = hidden unnamed_addr constant { [13 x i8*] } { [13 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI17btHingeConstraint to i8*), i8* bitcast (%class.btTypedConstraint* (%class.btTypedConstraint*)* @_ZN17btTypedConstraintD2Ev to i8*), i8* bitcast (void (%class.btHingeConstraint*)* @_ZN17btHingeConstraintD0Ev to i8*), i8* bitcast (void (%class.btHingeConstraint*)* @_ZN17btHingeConstraint13buildJacobianEv to i8*), i8* bitcast (void (%class.btTypedConstraint*, %class.btAlignedObjectArray.1*, i32, i32, float)* @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif to i8*), i8* bitcast (void (%class.btHingeConstraint*, %"struct.btTypedConstraint::btConstraintInfo1"*)* @_ZN17btHingeConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E to i8*), i8* bitcast (void (%class.btHingeConstraint*, %"struct.btTypedConstraint::btConstraintInfo2"*)* @_ZN17btHingeConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E to i8*), i8* bitcast (void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)* @_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f to i8*), i8* bitcast (void (%class.btHingeConstraint*, i32, float, i32)* @_ZN17btHingeConstraint8setParamEifi to i8*), i8* bitcast (float (%class.btHingeConstraint*, i32, i32)* @_ZNK17btHingeConstraint8getParamEii to i8*), i8* bitcast (i32 (%class.btHingeConstraint*)* @_ZNK17btHingeConstraint28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btHingeConstraint*, i8*, %class.btSerializer*)* @_ZNK17btHingeConstraint9serializeEPvP12btSerializer to i8*)] }, align 4
@_ZL6vHinge = internal global %class.btVector3 zeroinitializer, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS17btHingeConstraint = hidden constant [20 x i8] c"17btHingeConstraint\00", align 1
@_ZTI17btTypedConstraint = external constant i8*
@_ZTI17btHingeConstraint = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([20 x i8], [20 x i8]* @_ZTS17btHingeConstraint, i32 0, i32 0), i8* bitcast (i8** @_ZTI17btTypedConstraint to i8*) }, align 4
@_ZTV17btTypedConstraint = external unnamed_addr constant { [13 x i8*] }, align 4
@.str = private unnamed_addr constant [27 x i8] c"btHingeConstraintFloatData\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btHingeConstraint.cpp, i8* null }]

@_ZN17btHingeConstraintC1ER11btRigidBodyS1_RK9btVector3S4_S4_S4_b = hidden unnamed_addr alias %class.btHingeConstraint* (%class.btHingeConstraint*, %class.btRigidBody*, %class.btRigidBody*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, i1), %class.btHingeConstraint* (%class.btHingeConstraint*, %class.btRigidBody*, %class.btRigidBody*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, i1)* @_ZN17btHingeConstraintC2ER11btRigidBodyS1_RK9btVector3S4_S4_S4_b
@_ZN17btHingeConstraintC1ER11btRigidBodyRK9btVector3S4_b = hidden unnamed_addr alias %class.btHingeConstraint* (%class.btHingeConstraint*, %class.btRigidBody*, %class.btVector3*, %class.btVector3*, i1), %class.btHingeConstraint* (%class.btHingeConstraint*, %class.btRigidBody*, %class.btVector3*, %class.btVector3*, i1)* @_ZN17btHingeConstraintC2ER11btRigidBodyRK9btVector3S4_b
@_ZN17btHingeConstraintC1ER11btRigidBodyS1_RK11btTransformS4_b = hidden unnamed_addr alias %class.btHingeConstraint* (%class.btHingeConstraint*, %class.btRigidBody*, %class.btRigidBody*, %class.btTransform*, %class.btTransform*, i1), %class.btHingeConstraint* (%class.btHingeConstraint*, %class.btRigidBody*, %class.btRigidBody*, %class.btTransform*, %class.btTransform*, i1)* @_ZN17btHingeConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b
@_ZN17btHingeConstraintC1ER11btRigidBodyRK11btTransformb = hidden unnamed_addr alias %class.btHingeConstraint* (%class.btHingeConstraint*, %class.btRigidBody*, %class.btTransform*, i1), %class.btHingeConstraint* (%class.btHingeConstraint*, %class.btRigidBody*, %class.btTransform*, i1)* @_ZN17btHingeConstraintC2ER11btRigidBodyRK11btTransformb

define hidden %class.btHingeConstraint* @_ZN17btHingeConstraintC2ER11btRigidBodyS1_RK9btVector3S4_S4_S4_b(%class.btHingeConstraint* returned %this, %class.btRigidBody* nonnull align 4 dereferenceable(616) %rbA, %class.btRigidBody* nonnull align 4 dereferenceable(616) %rbB, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotInA, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotInB, %class.btVector3* nonnull align 4 dereferenceable(16) %axisInA, %class.btVector3* nonnull align 4 dereferenceable(16) %axisInB, i1 zeroext %useReferenceFrameA) unnamed_addr #0 {
entry:
  %retval = alloca %class.btHingeConstraint*, align 4
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %rbA.addr = alloca %class.btRigidBody*, align 4
  %rbB.addr = alloca %class.btRigidBody*, align 4
  %pivotInA.addr = alloca %class.btVector3*, align 4
  %pivotInB.addr = alloca %class.btVector3*, align 4
  %axisInA.addr = alloca %class.btVector3*, align 4
  %axisInB.addr = alloca %class.btVector3*, align 4
  %useReferenceFrameA.addr = alloca i8, align 1
  %rbAxisA1 = alloca %class.btVector3, align 4
  %rbAxisA2 = alloca %class.btVector3, align 4
  %projection = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp21 = alloca %class.btVector3, align 4
  %ref.tmp24 = alloca %class.btVector3, align 4
  %ref.tmp29 = alloca %class.btVector3, align 4
  %ref.tmp32 = alloca %class.btVector3, align 4
  %ref.tmp36 = alloca %class.btVector3, align 4
  %ref.tmp37 = alloca %class.btVector3, align 4
  %rotationArc = alloca %class.btQuaternion, align 4
  %rbAxisB1 = alloca %class.btVector3, align 4
  %rbAxisB2 = alloca %class.btVector3, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4, !tbaa !2
  store %class.btRigidBody* %rbA, %class.btRigidBody** %rbA.addr, align 4, !tbaa !2
  store %class.btRigidBody* %rbB, %class.btRigidBody** %rbB.addr, align 4, !tbaa !2
  store %class.btVector3* %pivotInA, %class.btVector3** %pivotInA.addr, align 4, !tbaa !2
  store %class.btVector3* %pivotInB, %class.btVector3** %pivotInB.addr, align 4, !tbaa !2
  store %class.btVector3* %axisInA, %class.btVector3** %axisInA.addr, align 4, !tbaa !2
  store %class.btVector3* %axisInB, %class.btVector3** %axisInB.addr, align 4, !tbaa !2
  %frombool = zext i1 %useReferenceFrameA to i8
  store i8 %frombool, i8* %useReferenceFrameA.addr, align 1, !tbaa !6
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  store %class.btHingeConstraint* %this1, %class.btHingeConstraint** %retval, align 4
  %0 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %1 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4, !tbaa !2
  %2 = load %class.btRigidBody*, %class.btRigidBody** %rbB.addr, align 4, !tbaa !2
  %call = call %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_(%class.btTypedConstraint* %0, i32 4, %class.btRigidBody* nonnull align 4 dereferenceable(616) %1, %class.btRigidBody* nonnull align 4 dereferenceable(616) %2)
  %3 = bitcast %class.btHingeConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [13 x i8*] }, { [13 x i8*] }* @_ZTV17btHingeConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %3, align 4, !tbaa !8
  %m_jac = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jac, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btJacobianEntry* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btJacobianEntry* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_jacAng = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 2
  %array.begin3 = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jacAng, i32 0, i32 0
  %arrayctor.end4 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin3, i32 3
  br label %arrayctor.loop5

arrayctor.loop5:                                  ; preds = %arrayctor.loop5, %arrayctor.cont
  %arrayctor.cur6 = phi %class.btJacobianEntry* [ %array.begin3, %arrayctor.cont ], [ %arrayctor.next8, %arrayctor.loop5 ]
  %call7 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur6)
  %arrayctor.next8 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur6, i32 1
  %arrayctor.done9 = icmp eq %class.btJacobianEntry* %arrayctor.next8, %arrayctor.end4
  br i1 %arrayctor.done9, label %arrayctor.cont10, label %arrayctor.loop5

arrayctor.cont10:                                 ; preds = %arrayctor.loop5
  %m_rbAFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %call11 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_rbAFrame)
  %m_rbBFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  %call12 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_rbBFrame)
  %m_limit = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call13 = call %class.btAngularLimit* @_ZN14btAngularLimitC2Ev(%class.btAngularLimit* %m_limit)
  %m_angularOnly = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 12
  store i8 0, i8* %m_angularOnly, align 4, !tbaa !10
  %m_enableAngularMotor = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 13
  store i8 0, i8* %m_enableAngularMotor, align 1, !tbaa !18
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 14
  store i8 0, i8* %m_useSolveConstraintObsolete, align 2, !tbaa !19
  %m_useOffsetForConstraintFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 15
  store i8 1, i8* %m_useOffsetForConstraintFrame, align 1, !tbaa !20
  %m_useReferenceFrameA = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 16
  %4 = load i8, i8* %useReferenceFrameA.addr, align 1, !tbaa !6, !range !21
  %tobool = trunc i8 %4 to i1
  %frombool14 = zext i1 %tobool to i8
  store i8 %frombool14, i8* %m_useReferenceFrameA, align 4, !tbaa !22
  %m_flags = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 18
  store i32 0, i32* %m_flags, align 4, !tbaa !23
  %5 = load %class.btVector3*, %class.btVector3** %pivotInA.addr, align 4, !tbaa !2
  %m_rbAFrame15 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %call16 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_rbAFrame15)
  %6 = bitcast %class.btVector3* %call16 to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !24
  %8 = bitcast %class.btVector3* %rbAxisA1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #8
  %9 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4, !tbaa !2
  %call17 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %9)
  %call18 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call17)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %rbAxisA1, %class.btMatrix3x3* %call18, i32 0)
  %10 = bitcast %class.btVector3* %rbAxisA2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #8
  %call19 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %rbAxisA2)
  %11 = bitcast float* %projection to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  %12 = load %class.btVector3*, %class.btVector3** %axisInA.addr, align 4, !tbaa !2
  %call20 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %12, %class.btVector3* nonnull align 4 dereferenceable(16) %rbAxisA1)
  store float %call20, float* %projection, align 4, !tbaa !26
  %13 = load float, float* %projection, align 4, !tbaa !26
  %cmp = fcmp oge float %13, 0x3FEFFFFFC0000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %arrayctor.cont10
  %14 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %14) #8
  %15 = bitcast %class.btVector3* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %15) #8
  %16 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4, !tbaa !2
  %call22 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %16)
  %call23 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call22)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp21, %class.btMatrix3x3* %call23, i32 2)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp21)
  %17 = bitcast %class.btVector3* %rbAxisA1 to i8*
  %18 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %17, i8* align 4 %18, i32 16, i1 false), !tbaa.struct !24
  %19 = bitcast %class.btVector3* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %19) #8
  %20 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %20) #8
  %21 = bitcast %class.btVector3* %ref.tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %21) #8
  %22 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4, !tbaa !2
  %call25 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %22)
  %call26 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call25)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp24, %class.btMatrix3x3* %call26, i32 1)
  %23 = bitcast %class.btVector3* %rbAxisA2 to i8*
  %24 = bitcast %class.btVector3* %ref.tmp24 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false), !tbaa.struct !24
  %25 = bitcast %class.btVector3* %ref.tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %25) #8
  br label %if.end38

if.else:                                          ; preds = %arrayctor.cont10
  %26 = load float, float* %projection, align 4, !tbaa !26
  %cmp27 = fcmp ole float %26, 0xBFEFFFFFC0000000
  br i1 %cmp27, label %if.then28, label %if.else35

if.then28:                                        ; preds = %if.else
  %27 = bitcast %class.btVector3* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %27) #8
  %28 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4, !tbaa !2
  %call30 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %28)
  %call31 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call30)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp29, %class.btMatrix3x3* %call31, i32 2)
  %29 = bitcast %class.btVector3* %rbAxisA1 to i8*
  %30 = bitcast %class.btVector3* %ref.tmp29 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %29, i8* align 4 %30, i32 16, i1 false), !tbaa.struct !24
  %31 = bitcast %class.btVector3* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %31) #8
  %32 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %32) #8
  %33 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4, !tbaa !2
  %call33 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %33)
  %call34 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call33)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp32, %class.btMatrix3x3* %call34, i32 1)
  %34 = bitcast %class.btVector3* %rbAxisA2 to i8*
  %35 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %34, i8* align 4 %35, i32 16, i1 false), !tbaa.struct !24
  %36 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %36) #8
  br label %if.end

if.else35:                                        ; preds = %if.else
  %37 = bitcast %class.btVector3* %ref.tmp36 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %37) #8
  %38 = load %class.btVector3*, %class.btVector3** %axisInA.addr, align 4, !tbaa !2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp36, %class.btVector3* %38, %class.btVector3* nonnull align 4 dereferenceable(16) %rbAxisA1)
  %39 = bitcast %class.btVector3* %rbAxisA2 to i8*
  %40 = bitcast %class.btVector3* %ref.tmp36 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %39, i8* align 4 %40, i32 16, i1 false), !tbaa.struct !24
  %41 = bitcast %class.btVector3* %ref.tmp36 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %41) #8
  %42 = bitcast %class.btVector3* %ref.tmp37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %42) #8
  %43 = load %class.btVector3*, %class.btVector3** %axisInA.addr, align 4, !tbaa !2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp37, %class.btVector3* %rbAxisA2, %class.btVector3* nonnull align 4 dereferenceable(16) %43)
  %44 = bitcast %class.btVector3* %rbAxisA1 to i8*
  %45 = bitcast %class.btVector3* %ref.tmp37 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %44, i8* align 4 %45, i32 16, i1 false), !tbaa.struct !24
  %46 = bitcast %class.btVector3* %ref.tmp37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %46) #8
  br label %if.end

if.end:                                           ; preds = %if.else35, %if.then28
  br label %if.end38

if.end38:                                         ; preds = %if.end, %if.then
  %m_rbAFrame39 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %call40 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbAFrame39)
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %rbAxisA1)
  %call42 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %rbAxisA2)
  %47 = load %class.btVector3*, %class.btVector3** %axisInA.addr, align 4, !tbaa !2
  %call43 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %47)
  %call44 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %rbAxisA1)
  %call45 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %rbAxisA2)
  %48 = load %class.btVector3*, %class.btVector3** %axisInA.addr, align 4, !tbaa !2
  %call46 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %48)
  %call47 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %rbAxisA1)
  %call48 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %rbAxisA2)
  %49 = load %class.btVector3*, %class.btVector3** %axisInA.addr, align 4, !tbaa !2
  %call49 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %49)
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %call40, float* nonnull align 4 dereferenceable(4) %call41, float* nonnull align 4 dereferenceable(4) %call42, float* nonnull align 4 dereferenceable(4) %call43, float* nonnull align 4 dereferenceable(4) %call44, float* nonnull align 4 dereferenceable(4) %call45, float* nonnull align 4 dereferenceable(4) %call46, float* nonnull align 4 dereferenceable(4) %call47, float* nonnull align 4 dereferenceable(4) %call48, float* nonnull align 4 dereferenceable(4) %call49)
  %50 = bitcast %class.btQuaternion* %rotationArc to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %50) #8
  %51 = load %class.btVector3*, %class.btVector3** %axisInA.addr, align 4, !tbaa !2
  %52 = load %class.btVector3*, %class.btVector3** %axisInB.addr, align 4, !tbaa !2
  call void @_Z15shortestArcQuatRK9btVector3S1_(%class.btQuaternion* sret align 4 %rotationArc, %class.btVector3* nonnull align 4 dereferenceable(16) %51, %class.btVector3* nonnull align 4 dereferenceable(16) %52)
  %53 = bitcast %class.btVector3* %rbAxisB1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %53) #8
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %rbAxisB1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %rotationArc, %class.btVector3* nonnull align 4 dereferenceable(16) %rbAxisA1)
  %54 = bitcast %class.btVector3* %rbAxisB2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %54) #8
  %55 = load %class.btVector3*, %class.btVector3** %axisInB.addr, align 4, !tbaa !2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %rbAxisB2, %class.btVector3* %55, %class.btVector3* nonnull align 4 dereferenceable(16) %rbAxisB1)
  %56 = load %class.btVector3*, %class.btVector3** %pivotInB.addr, align 4, !tbaa !2
  %m_rbBFrame50 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  %call51 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_rbBFrame50)
  %57 = bitcast %class.btVector3* %call51 to i8*
  %58 = bitcast %class.btVector3* %56 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %57, i8* align 4 %58, i32 16, i1 false), !tbaa.struct !24
  %m_rbBFrame52 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  %call53 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbBFrame52)
  %call54 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %rbAxisB1)
  %call55 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %rbAxisB2)
  %59 = load %class.btVector3*, %class.btVector3** %axisInB.addr, align 4, !tbaa !2
  %call56 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %59)
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %rbAxisB1)
  %call58 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %rbAxisB2)
  %60 = load %class.btVector3*, %class.btVector3** %axisInB.addr, align 4, !tbaa !2
  %call59 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %60)
  %call60 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %rbAxisB1)
  %call61 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %rbAxisB2)
  %61 = load %class.btVector3*, %class.btVector3** %axisInB.addr, align 4, !tbaa !2
  %call62 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %61)
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %call53, float* nonnull align 4 dereferenceable(4) %call54, float* nonnull align 4 dereferenceable(4) %call55, float* nonnull align 4 dereferenceable(4) %call56, float* nonnull align 4 dereferenceable(4) %call57, float* nonnull align 4 dereferenceable(4) %call58, float* nonnull align 4 dereferenceable(4) %call59, float* nonnull align 4 dereferenceable(4) %call60, float* nonnull align 4 dereferenceable(4) %call61, float* nonnull align 4 dereferenceable(4) %call62)
  %m_useReferenceFrameA63 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 16
  %62 = load i8, i8* %m_useReferenceFrameA63, align 4, !tbaa !22, !range !21
  %tobool64 = trunc i8 %62 to i1
  %63 = zext i1 %tobool64 to i64
  %cond = select i1 %tobool64, float -1.000000e+00, float 1.000000e+00
  %m_referenceSign = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 11
  store float %cond, float* %m_referenceSign, align 4, !tbaa !27
  %64 = bitcast %class.btVector3* %rbAxisB2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %64) #8
  %65 = bitcast %class.btVector3* %rbAxisB1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %65) #8
  %66 = bitcast %class.btQuaternion* %rotationArc to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %66) #8
  %67 = bitcast float* %projection to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #8
  %68 = bitcast %class.btVector3* %rbAxisA2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %68) #8
  %69 = bitcast %class.btVector3* %rbAxisA1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %69) #8
  %70 = load %class.btHingeConstraint*, %class.btHingeConstraint** %retval, align 4
  ret %class.btHingeConstraint* %70
}

declare %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_(%class.btTypedConstraint* returned, i32, %class.btRigidBody* nonnull align 4 dereferenceable(616), %class.btRigidBody* nonnull align 4 dereferenceable(616)) unnamed_addr #1

define linkonce_odr hidden %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btJacobianEntry*, align 4
  store %class.btJacobianEntry* %this, %class.btJacobianEntry** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btJacobianEntry*, %class.btJacobianEntry** %this.addr, align 4
  %m_linearJointAxis = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_linearJointAxis)
  %m_aJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aJ)
  %m_bJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_bJ)
  %m_0MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_0MinvJt)
  %m_1MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_1MinvJt)
  ret %class.btJacobianEntry* %this1
}

define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAngularLimit* @_ZN14btAngularLimitC2Ev(%class.btAngularLimit* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAngularLimit*, align 4
  store %class.btAngularLimit* %this, %class.btAngularLimit** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAngularLimit*, %class.btAngularLimit** %this.addr, align 4
  %m_center = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 0
  store float 0.000000e+00, float* %m_center, align 4, !tbaa !28
  %m_halfRange = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 1
  store float -1.000000e+00, float* %m_halfRange, align 4, !tbaa !29
  %m_softness = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 2
  store float 0x3FECCCCCC0000000, float* %m_softness, align 4, !tbaa !30
  %m_biasFactor = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 3
  store float 0x3FD3333340000000, float* %m_biasFactor, align 4, !tbaa !31
  %m_relaxationFactor = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 4
  store float 1.000000e+00, float* %m_relaxationFactor, align 4, !tbaa !32
  %m_correction = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 5
  store float 0.000000e+00, float* %m_correction, align 4, !tbaa !33
  %m_sign = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 6
  store float 0.000000e+00, float* %m_sign, align 4, !tbaa !34
  %m_solveLimit = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 7
  store i8 0, i8* %m_solveLimit, align 4, !tbaa !35
  ret %class.btAngularLimit* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #4

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this, i32 %i) #5 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !36
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx)
  %0 = load i32, i32* %i.addr, align 4, !tbaa !36
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 %0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx4)
  %1 = load i32, i32* %i.addr, align 4, !tbaa !36
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 2
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx8)
  %2 = load i32, i32* %i.addr, align 4, !tbaa !36
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %2
  %call11 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %arrayidx2, float* nonnull align 4 dereferenceable(4) %arrayidx6, float* nonnull align 4 dereferenceable(4) %arrayidx10)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !26
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !26
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !26
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !26
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !26
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !26
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #5 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !26
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp, align 4, !tbaa !26
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !26
  %fneg4 = fneg float %5
  store float %fneg4, float* %ref.tmp1, align 4, !tbaa !26
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %8 = load float, float* %arrayidx7, align 4, !tbaa !26
  %fneg8 = fneg float %8
  store float %fneg8, float* %ref.tmp5, align 4, !tbaa !26
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #4

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %1 = load float, float* %arrayidx, align 4, !tbaa !26
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %3 = load float, float* %arrayidx3, align 4, !tbaa !26
  %mul = fmul float %1, %3
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %4 = load float, float* %arrayidx5, align 4, !tbaa !26
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %6 = load float, float* %arrayidx7, align 4, !tbaa !26
  %mul8 = fmul float %4, %6
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4, !tbaa !26
  %7 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %8 = load float, float* %arrayidx11, align 4, !tbaa !26
  %9 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %10 = load float, float* %arrayidx13, align 4, !tbaa !26
  %mul14 = fmul float %8, %10
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %11 = load float, float* %arrayidx16, align 4, !tbaa !26
  %12 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %13 = load float, float* %arrayidx18, align 4, !tbaa !26
  %mul19 = fmul float %11, %13
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4, !tbaa !26
  %14 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #8
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %15 = load float, float* %arrayidx23, align 4, !tbaa !26
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %17 = load float, float* %arrayidx25, align 4, !tbaa !26
  %mul26 = fmul float %15, %17
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %18 = load float, float* %arrayidx28, align 4, !tbaa !26
  %19 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %19, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %20 = load float, float* %arrayidx30, align 4, !tbaa !26
  %mul31 = fmul float %18, %20
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4, !tbaa !26
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %21 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  %22 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #8
  %23 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_Z15shortestArcQuatRK9btVector3S1_(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1) #5 comdat {
entry:
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %c = alloca %class.btVector3, align 4
  %d = alloca float, align 4
  %n = alloca %class.btVector3, align 4
  %unused = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %s = alloca float, align 4
  %rs = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %0 = bitcast %class.btVector3* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %c, %class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  %3 = bitcast float* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %5 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call, float* %d, align 4, !tbaa !26
  %6 = load float, float* %d, align 4, !tbaa !26
  %conv = fpext float %6 to double
  %cmp = fcmp olt double %conv, 0xBFEFFFFFC0000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = bitcast %class.btVector3* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #8
  %call1 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %n)
  %8 = bitcast %class.btVector3* %unused to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #8
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %unused)
  %9 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  call void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %9, %class.btVector3* nonnull align 4 dereferenceable(16) %n, %class.btVector3* nonnull align 4 dereferenceable(16) %unused)
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %n)
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %n)
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %n)
  %10 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !26
  %call6 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %call3, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call5, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  store i32 1, i32* %cleanup.dest.slot, align 4
  %12 = bitcast %class.btVector3* %unused to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %12) #8
  %13 = bitcast %class.btVector3* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %13) #8
  br label %cleanup

if.end:                                           ; preds = %entry
  %14 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #8
  %15 = load float, float* %d, align 4, !tbaa !26
  %add = fadd float 1.000000e+00, %15
  %mul = fmul float %add, 2.000000e+00
  %call7 = call float @_Z6btSqrtf(float %mul)
  store float %call7, float* %s, align 4, !tbaa !26
  %16 = bitcast float* %rs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  %17 = load float, float* %s, align 4, !tbaa !26
  %div = fdiv float 1.000000e+00, %17
  store float %div, float* %rs, align 4, !tbaa !26
  %18 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #8
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %c)
  %19 = load float, float* %call9, align 4, !tbaa !26
  %20 = load float, float* %rs, align 4, !tbaa !26
  %mul10 = fmul float %19, %20
  store float %mul10, float* %ref.tmp8, align 4, !tbaa !26
  %21 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #8
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %c)
  %22 = load float, float* %call12, align 4, !tbaa !26
  %23 = load float, float* %rs, align 4, !tbaa !26
  %mul13 = fmul float %22, %23
  store float %mul13, float* %ref.tmp11, align 4, !tbaa !26
  %24 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #8
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %c)
  %25 = load float, float* %call15, align 4, !tbaa !26
  %26 = load float, float* %rs, align 4, !tbaa !26
  %mul16 = fmul float %25, %26
  store float %mul16, float* %ref.tmp14, align 4, !tbaa !26
  %27 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #8
  %28 = load float, float* %s, align 4, !tbaa !26
  %mul18 = fmul float %28, 5.000000e-01
  store float %mul18, float* %ref.tmp17, align 4, !tbaa !26
  %call19 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  %29 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #8
  %30 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #8
  %31 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #8
  %32 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #8
  store i32 1, i32* %cleanup.dest.slot, align 4
  %33 = bitcast float* %rs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #8
  %34 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #8
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %35 = bitcast float* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  %36 = bitcast %class.btVector3* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %36) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %rotation, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #5 comdat {
entry:
  %rotation.addr = alloca %class.btQuaternion*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %q = alloca %class.btQuaternion, align 4
  %ref.tmp = alloca %class.btQuaternion, align 4
  store %class.btQuaternion* %rotation, %class.btQuaternion** %rotation.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast %class.btQuaternion* %q to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btQuaternion*, %class.btQuaternion** %rotation.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  call void @_ZmlRK12btQuaternionRK9btVector3(%class.btQuaternion* sret align 4 %q, %class.btQuaternion* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  %3 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #8
  %4 = load %class.btQuaternion*, %class.btQuaternion** %rotation.addr, align 4, !tbaa !2
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp, %class.btQuaternion* %4)
  %call = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKS_(%class.btQuaternion* %q, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp)
  %5 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #8
  %6 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getXEv(%class.btQuadWord* %6)
  %7 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getYEv(%class.btQuadWord* %7)
  %8 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getZEv(%class.btQuadWord* %8)
  %call4 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %call1, float* nonnull align 4 dereferenceable(4) %call2, float* nonnull align 4 dereferenceable(4) %call3)
  %9 = bitcast %class.btQuaternion* %q to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #8
  ret void
}

define hidden %class.btHingeConstraint* @_ZN17btHingeConstraintC2ER11btRigidBodyRK9btVector3S4_b(%class.btHingeConstraint* returned %this, %class.btRigidBody* nonnull align 4 dereferenceable(616) %rbA, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotInA, %class.btVector3* nonnull align 4 dereferenceable(16) %axisInA, i1 zeroext %useReferenceFrameA) unnamed_addr #0 {
entry:
  %retval = alloca %class.btHingeConstraint*, align 4
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %rbA.addr = alloca %class.btRigidBody*, align 4
  %pivotInA.addr = alloca %class.btVector3*, align 4
  %axisInA.addr = alloca %class.btVector3*, align 4
  %useReferenceFrameA.addr = alloca i8, align 1
  %rbAxisA1 = alloca %class.btVector3, align 4
  %rbAxisA2 = alloca %class.btVector3, align 4
  %axisInB = alloca %class.btVector3, align 4
  %rotationArc = alloca %class.btQuaternion, align 4
  %rbAxisB1 = alloca %class.btVector3, align 4
  %rbAxisB2 = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4, !tbaa !2
  store %class.btRigidBody* %rbA, %class.btRigidBody** %rbA.addr, align 4, !tbaa !2
  store %class.btVector3* %pivotInA, %class.btVector3** %pivotInA.addr, align 4, !tbaa !2
  store %class.btVector3* %axisInA, %class.btVector3** %axisInA.addr, align 4, !tbaa !2
  %frombool = zext i1 %useReferenceFrameA to i8
  store i8 %frombool, i8* %useReferenceFrameA.addr, align 1, !tbaa !6
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  store %class.btHingeConstraint* %this1, %class.btHingeConstraint** %retval, align 4
  %0 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %1 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4, !tbaa !2
  %call = call %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBody(%class.btTypedConstraint* %0, i32 4, %class.btRigidBody* nonnull align 4 dereferenceable(616) %1)
  %2 = bitcast %class.btHingeConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [13 x i8*] }, { [13 x i8*] }* @_ZTV17btHingeConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %2, align 4, !tbaa !8
  %m_jac = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jac, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btJacobianEntry* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btJacobianEntry* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_jacAng = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 2
  %array.begin3 = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jacAng, i32 0, i32 0
  %arrayctor.end4 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin3, i32 3
  br label %arrayctor.loop5

arrayctor.loop5:                                  ; preds = %arrayctor.loop5, %arrayctor.cont
  %arrayctor.cur6 = phi %class.btJacobianEntry* [ %array.begin3, %arrayctor.cont ], [ %arrayctor.next8, %arrayctor.loop5 ]
  %call7 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur6)
  %arrayctor.next8 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur6, i32 1
  %arrayctor.done9 = icmp eq %class.btJacobianEntry* %arrayctor.next8, %arrayctor.end4
  br i1 %arrayctor.done9, label %arrayctor.cont10, label %arrayctor.loop5

arrayctor.cont10:                                 ; preds = %arrayctor.loop5
  %m_rbAFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %call11 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_rbAFrame)
  %m_rbBFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  %call12 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_rbBFrame)
  %m_limit = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call13 = call %class.btAngularLimit* @_ZN14btAngularLimitC2Ev(%class.btAngularLimit* %m_limit)
  %m_angularOnly = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 12
  store i8 0, i8* %m_angularOnly, align 4, !tbaa !10
  %m_enableAngularMotor = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 13
  store i8 0, i8* %m_enableAngularMotor, align 1, !tbaa !18
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 14
  store i8 0, i8* %m_useSolveConstraintObsolete, align 2, !tbaa !19
  %m_useOffsetForConstraintFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 15
  store i8 1, i8* %m_useOffsetForConstraintFrame, align 1, !tbaa !20
  %m_useReferenceFrameA = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 16
  %3 = load i8, i8* %useReferenceFrameA.addr, align 1, !tbaa !6, !range !21
  %tobool = trunc i8 %3 to i1
  %frombool14 = zext i1 %tobool to i8
  store i8 %frombool14, i8* %m_useReferenceFrameA, align 4, !tbaa !22
  %m_flags = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 18
  store i32 0, i32* %m_flags, align 4, !tbaa !23
  %4 = bitcast %class.btVector3* %rbAxisA1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #8
  %call15 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %rbAxisA1)
  %5 = bitcast %class.btVector3* %rbAxisA2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #8
  %call16 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %rbAxisA2)
  %6 = load %class.btVector3*, %class.btVector3** %axisInA.addr, align 4, !tbaa !2
  call void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %6, %class.btVector3* nonnull align 4 dereferenceable(16) %rbAxisA1, %class.btVector3* nonnull align 4 dereferenceable(16) %rbAxisA2)
  %7 = load %class.btVector3*, %class.btVector3** %pivotInA.addr, align 4, !tbaa !2
  %m_rbAFrame17 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_rbAFrame17)
  %8 = bitcast %class.btVector3* %call18 to i8*
  %9 = bitcast %class.btVector3* %7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false), !tbaa.struct !24
  %m_rbAFrame19 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %call20 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbAFrame19)
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %rbAxisA1)
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %rbAxisA2)
  %10 = load %class.btVector3*, %class.btVector3** %axisInA.addr, align 4, !tbaa !2
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %10)
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %rbAxisA1)
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %rbAxisA2)
  %11 = load %class.btVector3*, %class.btVector3** %axisInA.addr, align 4, !tbaa !2
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %11)
  %call27 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %rbAxisA1)
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %rbAxisA2)
  %12 = load %class.btVector3*, %class.btVector3** %axisInA.addr, align 4, !tbaa !2
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %12)
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %call20, float* nonnull align 4 dereferenceable(4) %call21, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call23, float* nonnull align 4 dereferenceable(4) %call24, float* nonnull align 4 dereferenceable(4) %call25, float* nonnull align 4 dereferenceable(4) %call26, float* nonnull align 4 dereferenceable(4) %call27, float* nonnull align 4 dereferenceable(4) %call28, float* nonnull align 4 dereferenceable(4) %call29)
  %13 = bitcast %class.btVector3* %axisInB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #8
  %14 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4, !tbaa !2
  %call30 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %14)
  %call31 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call30)
  %15 = load %class.btVector3*, %class.btVector3** %axisInA.addr, align 4, !tbaa !2
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %axisInB, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call31, %class.btVector3* nonnull align 4 dereferenceable(16) %15)
  %16 = bitcast %class.btQuaternion* %rotationArc to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #8
  %17 = load %class.btVector3*, %class.btVector3** %axisInA.addr, align 4, !tbaa !2
  call void @_Z15shortestArcQuatRK9btVector3S1_(%class.btQuaternion* sret align 4 %rotationArc, %class.btVector3* nonnull align 4 dereferenceable(16) %17, %class.btVector3* nonnull align 4 dereferenceable(16) %axisInB)
  %18 = bitcast %class.btVector3* %rbAxisB1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %18) #8
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %rbAxisB1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %rotationArc, %class.btVector3* nonnull align 4 dereferenceable(16) %rbAxisA1)
  %19 = bitcast %class.btVector3* %rbAxisB2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %19) #8
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %rbAxisB2, %class.btVector3* %axisInB, %class.btVector3* nonnull align 4 dereferenceable(16) %rbAxisB1)
  %20 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %20) #8
  %21 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4, !tbaa !2
  %call32 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %21)
  %22 = load %class.btVector3*, %class.btVector3** %pivotInA.addr, align 4, !tbaa !2
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btTransform* %call32, %class.btVector3* nonnull align 4 dereferenceable(16) %22)
  %m_rbBFrame33 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  %call34 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_rbBFrame33)
  %23 = bitcast %class.btVector3* %call34 to i8*
  %24 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false), !tbaa.struct !24
  %25 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %25) #8
  %m_rbBFrame35 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  %call36 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbBFrame35)
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %rbAxisB1)
  %call38 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %rbAxisB2)
  %call39 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %axisInB)
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %rbAxisB1)
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %rbAxisB2)
  %call42 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %axisInB)
  %call43 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %rbAxisB1)
  %call44 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %rbAxisB2)
  %call45 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %axisInB)
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %call36, float* nonnull align 4 dereferenceable(4) %call37, float* nonnull align 4 dereferenceable(4) %call38, float* nonnull align 4 dereferenceable(4) %call39, float* nonnull align 4 dereferenceable(4) %call40, float* nonnull align 4 dereferenceable(4) %call41, float* nonnull align 4 dereferenceable(4) %call42, float* nonnull align 4 dereferenceable(4) %call43, float* nonnull align 4 dereferenceable(4) %call44, float* nonnull align 4 dereferenceable(4) %call45)
  %m_useReferenceFrameA46 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 16
  %26 = load i8, i8* %m_useReferenceFrameA46, align 4, !tbaa !22, !range !21
  %tobool47 = trunc i8 %26 to i1
  %27 = zext i1 %tobool47 to i64
  %cond = select i1 %tobool47, float -1.000000e+00, float 1.000000e+00
  %m_referenceSign = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 11
  store float %cond, float* %m_referenceSign, align 4, !tbaa !27
  %28 = bitcast %class.btVector3* %rbAxisB2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %28) #8
  %29 = bitcast %class.btVector3* %rbAxisB1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #8
  %30 = bitcast %class.btQuaternion* %rotationArc to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %30) #8
  %31 = bitcast %class.btVector3* %axisInB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %31) #8
  %32 = bitcast %class.btVector3* %rbAxisA2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %32) #8
  %33 = bitcast %class.btVector3* %rbAxisA1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %33) #8
  %34 = load %class.btHingeConstraint*, %class.btHingeConstraint** %retval, align 4
  ret %class.btHingeConstraint* %34
}

declare %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBody(%class.btTypedConstraint* returned, i32, %class.btRigidBody* nonnull align 4 dereferenceable(616)) unnamed_addr #1

; Function Attrs: inlinehint
define linkonce_odr hidden void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %n, %class.btVector3* nonnull align 4 dereferenceable(16) %p, %class.btVector3* nonnull align 4 dereferenceable(16) %q) #5 comdat {
entry:
  %n.addr = alloca %class.btVector3*, align 4
  %p.addr = alloca %class.btVector3*, align 4
  %q.addr = alloca %class.btVector3*, align 4
  %a = alloca float, align 4
  %k = alloca float, align 4
  %a42 = alloca float, align 4
  %k54 = alloca float, align 4
  store %class.btVector3* %n, %class.btVector3** %n.addr, align 4, !tbaa !2
  store %class.btVector3* %p, %class.btVector3** %p.addr, align 4, !tbaa !2
  store %class.btVector3* %q, %class.btVector3** %q.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %0)
  %arrayidx = getelementptr inbounds float, float* %call, i32 2
  %1 = load float, float* %arrayidx, align 4, !tbaa !26
  %call1 = call float @_Z6btFabsf(float %1)
  %cmp = fcmp ogt float %call1, 0x3FE6A09E60000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = bitcast float* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %3)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 1
  %4 = load float, float* %arrayidx3, align 4, !tbaa !26
  %5 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call4 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %5)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 1
  %6 = load float, float* %arrayidx5, align 4, !tbaa !26
  %mul = fmul float %4, %6
  %7 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %7)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 2
  %8 = load float, float* %arrayidx7, align 4, !tbaa !26
  %9 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %9)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 2
  %10 = load float, float* %arrayidx9, align 4, !tbaa !26
  %mul10 = fmul float %8, %10
  %add = fadd float %mul, %mul10
  store float %add, float* %a, align 4, !tbaa !26
  %11 = bitcast float* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  %12 = load float, float* %a, align 4, !tbaa !26
  %call11 = call float @_Z6btSqrtf(float %12)
  %div = fdiv float 1.000000e+00, %call11
  store float %div, float* %k, align 4, !tbaa !26
  %13 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %13)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 0
  store float 0.000000e+00, float* %arrayidx13, align 4, !tbaa !26
  %14 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call14 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %14)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 2
  %15 = load float, float* %arrayidx15, align 4, !tbaa !26
  %fneg = fneg float %15
  %16 = load float, float* %k, align 4, !tbaa !26
  %mul16 = fmul float %fneg, %16
  %17 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %17)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 1
  store float %mul16, float* %arrayidx18, align 4, !tbaa !26
  %18 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call19 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %18)
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 1
  %19 = load float, float* %arrayidx20, align 4, !tbaa !26
  %20 = load float, float* %k, align 4, !tbaa !26
  %mul21 = fmul float %19, %20
  %21 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call22 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %21)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 2
  store float %mul21, float* %arrayidx23, align 4, !tbaa !26
  %22 = load float, float* %a, align 4, !tbaa !26
  %23 = load float, float* %k, align 4, !tbaa !26
  %mul24 = fmul float %22, %23
  %24 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %24)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 0
  store float %mul24, float* %arrayidx26, align 4, !tbaa !26
  %25 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call27 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %25)
  %arrayidx28 = getelementptr inbounds float, float* %call27, i32 0
  %26 = load float, float* %arrayidx28, align 4, !tbaa !26
  %fneg29 = fneg float %26
  %27 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %27)
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 2
  %28 = load float, float* %arrayidx31, align 4, !tbaa !26
  %mul32 = fmul float %fneg29, %28
  %29 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %29)
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 1
  store float %mul32, float* %arrayidx34, align 4, !tbaa !26
  %30 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call35 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %30)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 0
  %31 = load float, float* %arrayidx36, align 4, !tbaa !26
  %32 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call37 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %32)
  %arrayidx38 = getelementptr inbounds float, float* %call37, i32 1
  %33 = load float, float* %arrayidx38, align 4, !tbaa !26
  %mul39 = fmul float %31, %33
  %34 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %call40 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %34)
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 2
  store float %mul39, float* %arrayidx41, align 4, !tbaa !26
  %35 = bitcast float* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  %36 = bitcast float* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #8
  br label %if.end

if.else:                                          ; preds = %entry
  %37 = bitcast float* %a42 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #8
  %38 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call43 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %38)
  %arrayidx44 = getelementptr inbounds float, float* %call43, i32 0
  %39 = load float, float* %arrayidx44, align 4, !tbaa !26
  %40 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call45 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %40)
  %arrayidx46 = getelementptr inbounds float, float* %call45, i32 0
  %41 = load float, float* %arrayidx46, align 4, !tbaa !26
  %mul47 = fmul float %39, %41
  %42 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call48 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %42)
  %arrayidx49 = getelementptr inbounds float, float* %call48, i32 1
  %43 = load float, float* %arrayidx49, align 4, !tbaa !26
  %44 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call50 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %44)
  %arrayidx51 = getelementptr inbounds float, float* %call50, i32 1
  %45 = load float, float* %arrayidx51, align 4, !tbaa !26
  %mul52 = fmul float %43, %45
  %add53 = fadd float %mul47, %mul52
  store float %add53, float* %a42, align 4, !tbaa !26
  %46 = bitcast float* %k54 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #8
  %47 = load float, float* %a42, align 4, !tbaa !26
  %call55 = call float @_Z6btSqrtf(float %47)
  %div56 = fdiv float 1.000000e+00, %call55
  store float %div56, float* %k54, align 4, !tbaa !26
  %48 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call57 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %48)
  %arrayidx58 = getelementptr inbounds float, float* %call57, i32 1
  %49 = load float, float* %arrayidx58, align 4, !tbaa !26
  %fneg59 = fneg float %49
  %50 = load float, float* %k54, align 4, !tbaa !26
  %mul60 = fmul float %fneg59, %50
  %51 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call61 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %51)
  %arrayidx62 = getelementptr inbounds float, float* %call61, i32 0
  store float %mul60, float* %arrayidx62, align 4, !tbaa !26
  %52 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call63 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %52)
  %arrayidx64 = getelementptr inbounds float, float* %call63, i32 0
  %53 = load float, float* %arrayidx64, align 4, !tbaa !26
  %54 = load float, float* %k54, align 4, !tbaa !26
  %mul65 = fmul float %53, %54
  %55 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call66 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %55)
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 1
  store float %mul65, float* %arrayidx67, align 4, !tbaa !26
  %56 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call68 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %56)
  %arrayidx69 = getelementptr inbounds float, float* %call68, i32 2
  store float 0.000000e+00, float* %arrayidx69, align 4, !tbaa !26
  %57 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call70 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %57)
  %arrayidx71 = getelementptr inbounds float, float* %call70, i32 2
  %58 = load float, float* %arrayidx71, align 4, !tbaa !26
  %fneg72 = fneg float %58
  %59 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call73 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %59)
  %arrayidx74 = getelementptr inbounds float, float* %call73, i32 1
  %60 = load float, float* %arrayidx74, align 4, !tbaa !26
  %mul75 = fmul float %fneg72, %60
  %61 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %call76 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %61)
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 0
  store float %mul75, float* %arrayidx77, align 4, !tbaa !26
  %62 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call78 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %62)
  %arrayidx79 = getelementptr inbounds float, float* %call78, i32 2
  %63 = load float, float* %arrayidx79, align 4, !tbaa !26
  %64 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call80 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %64)
  %arrayidx81 = getelementptr inbounds float, float* %call80, i32 0
  %65 = load float, float* %arrayidx81, align 4, !tbaa !26
  %mul82 = fmul float %63, %65
  %66 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %call83 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %66)
  %arrayidx84 = getelementptr inbounds float, float* %call83, i32 1
  store float %mul82, float* %arrayidx84, align 4, !tbaa !26
  %67 = load float, float* %a42, align 4, !tbaa !26
  %68 = load float, float* %k54, align 4, !tbaa !26
  %mul85 = fmul float %67, %68
  %69 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %call86 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %69)
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 2
  store float %mul85, float* %arrayidx87, align 4, !tbaa !26
  %70 = bitcast float* %k54 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #8
  %71 = bitcast float* %a42 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #5 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %1, i32 0)
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call1, float* %ref.tmp, align 4, !tbaa !26
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 1)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !26
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 2)
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !26
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #5 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #8
  ret void
}

define hidden %class.btHingeConstraint* @_ZN17btHingeConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b(%class.btHingeConstraint* returned %this, %class.btRigidBody* nonnull align 4 dereferenceable(616) %rbA, %class.btRigidBody* nonnull align 4 dereferenceable(616) %rbB, %class.btTransform* nonnull align 4 dereferenceable(64) %rbAFrame, %class.btTransform* nonnull align 4 dereferenceable(64) %rbBFrame, i1 zeroext %useReferenceFrameA) unnamed_addr #0 {
entry:
  %retval = alloca %class.btHingeConstraint*, align 4
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %rbA.addr = alloca %class.btRigidBody*, align 4
  %rbB.addr = alloca %class.btRigidBody*, align 4
  %rbAFrame.addr = alloca %class.btTransform*, align 4
  %rbBFrame.addr = alloca %class.btTransform*, align 4
  %useReferenceFrameA.addr = alloca i8, align 1
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4, !tbaa !2
  store %class.btRigidBody* %rbA, %class.btRigidBody** %rbA.addr, align 4, !tbaa !2
  store %class.btRigidBody* %rbB, %class.btRigidBody** %rbB.addr, align 4, !tbaa !2
  store %class.btTransform* %rbAFrame, %class.btTransform** %rbAFrame.addr, align 4, !tbaa !2
  store %class.btTransform* %rbBFrame, %class.btTransform** %rbBFrame.addr, align 4, !tbaa !2
  %frombool = zext i1 %useReferenceFrameA to i8
  store i8 %frombool, i8* %useReferenceFrameA.addr, align 1, !tbaa !6
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  store %class.btHingeConstraint* %this1, %class.btHingeConstraint** %retval, align 4
  %0 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %1 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4, !tbaa !2
  %2 = load %class.btRigidBody*, %class.btRigidBody** %rbB.addr, align 4, !tbaa !2
  %call = call %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_(%class.btTypedConstraint* %0, i32 4, %class.btRigidBody* nonnull align 4 dereferenceable(616) %1, %class.btRigidBody* nonnull align 4 dereferenceable(616) %2)
  %3 = bitcast %class.btHingeConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [13 x i8*] }, { [13 x i8*] }* @_ZTV17btHingeConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %3, align 4, !tbaa !8
  %m_jac = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jac, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btJacobianEntry* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btJacobianEntry* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_jacAng = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 2
  %array.begin3 = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jacAng, i32 0, i32 0
  %arrayctor.end4 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin3, i32 3
  br label %arrayctor.loop5

arrayctor.loop5:                                  ; preds = %arrayctor.loop5, %arrayctor.cont
  %arrayctor.cur6 = phi %class.btJacobianEntry* [ %array.begin3, %arrayctor.cont ], [ %arrayctor.next8, %arrayctor.loop5 ]
  %call7 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur6)
  %arrayctor.next8 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur6, i32 1
  %arrayctor.done9 = icmp eq %class.btJacobianEntry* %arrayctor.next8, %arrayctor.end4
  br i1 %arrayctor.done9, label %arrayctor.cont10, label %arrayctor.loop5

arrayctor.cont10:                                 ; preds = %arrayctor.loop5
  %m_rbAFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %4 = load %class.btTransform*, %class.btTransform** %rbAFrame.addr, align 4, !tbaa !2
  %call11 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_rbAFrame, %class.btTransform* nonnull align 4 dereferenceable(64) %4)
  %m_rbBFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  %5 = load %class.btTransform*, %class.btTransform** %rbBFrame.addr, align 4, !tbaa !2
  %call12 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_rbBFrame, %class.btTransform* nonnull align 4 dereferenceable(64) %5)
  %m_limit = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call13 = call %class.btAngularLimit* @_ZN14btAngularLimitC2Ev(%class.btAngularLimit* %m_limit)
  %m_angularOnly = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 12
  store i8 0, i8* %m_angularOnly, align 4, !tbaa !10
  %m_enableAngularMotor = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 13
  store i8 0, i8* %m_enableAngularMotor, align 1, !tbaa !18
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 14
  store i8 0, i8* %m_useSolveConstraintObsolete, align 2, !tbaa !19
  %m_useOffsetForConstraintFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 15
  store i8 1, i8* %m_useOffsetForConstraintFrame, align 1, !tbaa !20
  %m_useReferenceFrameA = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 16
  %6 = load i8, i8* %useReferenceFrameA.addr, align 1, !tbaa !6, !range !21
  %tobool = trunc i8 %6 to i1
  %frombool14 = zext i1 %tobool to i8
  store i8 %frombool14, i8* %m_useReferenceFrameA, align 4, !tbaa !22
  %m_flags = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 18
  store i32 0, i32* %m_flags, align 4, !tbaa !23
  %m_useReferenceFrameA15 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 16
  %7 = load i8, i8* %m_useReferenceFrameA15, align 4, !tbaa !22, !range !21
  %tobool16 = trunc i8 %7 to i1
  %8 = zext i1 %tobool16 to i64
  %cond = select i1 %tobool16, float -1.000000e+00, float 1.000000e+00
  %m_referenceSign = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 11
  store float %cond, float* %m_referenceSign, align 4, !tbaa !27
  %9 = load %class.btHingeConstraint*, %class.btHingeConstraint** %retval, align 4
  ret %class.btHingeConstraint* %9
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !24
  ret %class.btTransform* %this1
}

define hidden %class.btHingeConstraint* @_ZN17btHingeConstraintC2ER11btRigidBodyRK11btTransformb(%class.btHingeConstraint* returned %this, %class.btRigidBody* nonnull align 4 dereferenceable(616) %rbA, %class.btTransform* nonnull align 4 dereferenceable(64) %rbAFrame, i1 zeroext %useReferenceFrameA) unnamed_addr #0 {
entry:
  %retval = alloca %class.btHingeConstraint*, align 4
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %rbA.addr = alloca %class.btRigidBody*, align 4
  %rbAFrame.addr = alloca %class.btTransform*, align 4
  %useReferenceFrameA.addr = alloca i8, align 1
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4, !tbaa !2
  store %class.btRigidBody* %rbA, %class.btRigidBody** %rbA.addr, align 4, !tbaa !2
  store %class.btTransform* %rbAFrame, %class.btTransform** %rbAFrame.addr, align 4, !tbaa !2
  %frombool = zext i1 %useReferenceFrameA to i8
  store i8 %frombool, i8* %useReferenceFrameA.addr, align 1, !tbaa !6
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  store %class.btHingeConstraint* %this1, %class.btHingeConstraint** %retval, align 4
  %0 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %1 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4, !tbaa !2
  %call = call %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBody(%class.btTypedConstraint* %0, i32 4, %class.btRigidBody* nonnull align 4 dereferenceable(616) %1)
  %2 = bitcast %class.btHingeConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [13 x i8*] }, { [13 x i8*] }* @_ZTV17btHingeConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %2, align 4, !tbaa !8
  %m_jac = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jac, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btJacobianEntry* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btJacobianEntry* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_jacAng = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 2
  %array.begin3 = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jacAng, i32 0, i32 0
  %arrayctor.end4 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin3, i32 3
  br label %arrayctor.loop5

arrayctor.loop5:                                  ; preds = %arrayctor.loop5, %arrayctor.cont
  %arrayctor.cur6 = phi %class.btJacobianEntry* [ %array.begin3, %arrayctor.cont ], [ %arrayctor.next8, %arrayctor.loop5 ]
  %call7 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur6)
  %arrayctor.next8 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur6, i32 1
  %arrayctor.done9 = icmp eq %class.btJacobianEntry* %arrayctor.next8, %arrayctor.end4
  br i1 %arrayctor.done9, label %arrayctor.cont10, label %arrayctor.loop5

arrayctor.cont10:                                 ; preds = %arrayctor.loop5
  %m_rbAFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %3 = load %class.btTransform*, %class.btTransform** %rbAFrame.addr, align 4, !tbaa !2
  %call11 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_rbAFrame, %class.btTransform* nonnull align 4 dereferenceable(64) %3)
  %m_rbBFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  %4 = load %class.btTransform*, %class.btTransform** %rbAFrame.addr, align 4, !tbaa !2
  %call12 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_rbBFrame, %class.btTransform* nonnull align 4 dereferenceable(64) %4)
  %m_limit = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call13 = call %class.btAngularLimit* @_ZN14btAngularLimitC2Ev(%class.btAngularLimit* %m_limit)
  %m_angularOnly = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 12
  store i8 0, i8* %m_angularOnly, align 4, !tbaa !10
  %m_enableAngularMotor = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 13
  store i8 0, i8* %m_enableAngularMotor, align 1, !tbaa !18
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 14
  store i8 0, i8* %m_useSolveConstraintObsolete, align 2, !tbaa !19
  %m_useOffsetForConstraintFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 15
  store i8 1, i8* %m_useOffsetForConstraintFrame, align 1, !tbaa !20
  %m_useReferenceFrameA = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 16
  %5 = load i8, i8* %useReferenceFrameA.addr, align 1, !tbaa !6, !range !21
  %tobool = trunc i8 %5 to i1
  %frombool14 = zext i1 %tobool to i8
  store i8 %frombool14, i8* %m_useReferenceFrameA, align 4, !tbaa !22
  %m_flags = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 18
  store i32 0, i32* %m_flags, align 4, !tbaa !23
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #8
  %7 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %7, i32 0, i32 8
  %8 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4, !tbaa !37
  %call15 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %8)
  %m_rbAFrame16 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %call17 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_rbAFrame16)
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btTransform* %call15, %class.btVector3* nonnull align 4 dereferenceable(16) %call17)
  %m_rbBFrame18 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  %call19 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_rbBFrame18)
  %9 = bitcast %class.btVector3* %call19 to i8*
  %10 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false), !tbaa.struct !24
  %11 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %11) #8
  %m_useReferenceFrameA20 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 16
  %12 = load i8, i8* %m_useReferenceFrameA20, align 4, !tbaa !22, !range !21
  %tobool21 = trunc i8 %12 to i1
  %13 = zext i1 %tobool21 to i64
  %cond = select i1 %tobool21, float -1.000000e+00, float 1.000000e+00
  %m_referenceSign = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 11
  store float %cond, float* %m_referenceSign, align 4, !tbaa !27
  %14 = load %class.btHingeConstraint*, %class.btHingeConstraint** %retval, align 4
  ret %class.btHingeConstraint* %14
}

define hidden void @_ZN17btHingeConstraint13buildJacobianEv(%class.btHingeConstraint* %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %pivotAInW = alloca %class.btVector3, align 4
  %pivotBInW = alloca %class.btVector3, align 4
  %relPos = alloca %class.btVector3, align 4
  %normal = alloca [3 x %class.btVector3], align 16
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %i = alloca i32, align 4
  %ref.tmp19 = alloca %class.btMatrix3x3, align 4
  %ref.tmp23 = alloca %class.btMatrix3x3, align 4
  %ref.tmp27 = alloca %class.btVector3, align 4
  %ref.tmp30 = alloca %class.btVector3, align 4
  %jointAxis0local = alloca %class.btVector3, align 4
  %jointAxis1local = alloca %class.btVector3, align 4
  %ref.tmp46 = alloca %class.btVector3, align 4
  %jointAxis0 = alloca %class.btVector3, align 4
  %jointAxis1 = alloca %class.btVector3, align 4
  %hingeAxisWorld = alloca %class.btVector3, align 4
  %ref.tmp58 = alloca %class.btVector3, align 4
  %ref.tmp62 = alloca %class.btMatrix3x3, align 4
  %ref.tmp66 = alloca %class.btMatrix3x3, align 4
  %ref.tmp77 = alloca %class.btMatrix3x3, align 4
  %ref.tmp81 = alloca %class.btMatrix3x3, align 4
  %ref.tmp92 = alloca %class.btMatrix3x3, align 4
  %ref.tmp96 = alloca %class.btMatrix3x3, align 4
  %axisA = alloca %class.btVector3, align 4
  %ref.tmp112 = alloca %class.btVector3, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 14
  %0 = load i8, i8* %m_useSolveConstraintObsolete, align 2, !tbaa !19, !range !21
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end119

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_appliedImpulse = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %1, i32 0, i32 10
  store float 0.000000e+00, float* %m_appliedImpulse, align 4, !tbaa !39
  %m_accMotorImpulse = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 17
  store float 0.000000e+00, float* %m_accMotorImpulse, align 4, !tbaa !40
  %m_angularOnly = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 12
  %2 = load i8, i8* %m_angularOnly, align 4, !tbaa !10, !range !21
  %tobool2 = trunc i8 %2 to i1
  br i1 %tobool2, label %if.end43, label %if.then3

if.then3:                                         ; preds = %if.then
  %3 = bitcast %class.btVector3* %pivotAInW to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #8
  %4 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %4, i32 0, i32 8
  %5 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4, !tbaa !37
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %5)
  %m_rbAFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_rbAFrame)
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %pivotAInW, %class.btTransform* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call4)
  %6 = bitcast %class.btVector3* %pivotBInW to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #8
  %7 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %7, i32 0, i32 9
  %8 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4, !tbaa !41
  %call5 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %8)
  %m_rbBFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_rbBFrame)
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %pivotBInW, %class.btTransform* %call5, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  %9 = bitcast %class.btVector3* %relPos to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %relPos, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotBInW, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotAInW)
  %10 = bitcast [3 x %class.btVector3]* %normal to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %10) #8
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %normal, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %if.then3
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %if.then3 ], [ %arrayctor.next, %arrayctor.loop ]
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %call8 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %relPos)
  %cmp = fcmp ogt float %call8, 0x3E80000000000000
  br i1 %cmp, label %if.then9, label %if.else

if.then9:                                         ; preds = %arrayctor.cont
  %11 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #8
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %relPos)
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %normal, i32 0, i32 0
  %12 = bitcast %class.btVector3* %arrayidx to i8*
  %13 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %12, i8* align 4 %13, i32 16, i1 false), !tbaa.struct !24
  %14 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %14) #8
  br label %if.end

if.else:                                          ; preds = %arrayctor.cont
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %normal, i32 0, i32 0
  %15 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  store float 1.000000e+00, float* %ref.tmp11, align 4, !tbaa !26
  %16 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  store float 0.000000e+00, float* %ref.tmp12, align 4, !tbaa !26
  %17 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #8
  store float 0.000000e+00, float* %ref.tmp13, align 4, !tbaa !26
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx10, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp13)
  %18 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  %19 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  %20 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then9
  %arrayidx14 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %normal, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %normal, i32 0, i32 1
  %arrayidx16 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %normal, i32 0, i32 2
  call void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx14, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx15, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx16)
  %21 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #8
  store i32 0, i32* %i, align 4, !tbaa !36
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %22 = load i32, i32* %i, align 4, !tbaa !36
  %cmp17 = icmp slt i32 %22, 3
  br i1 %cmp17, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %23 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_jac = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 1
  %24 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx18 = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jac, i32 0, i32 %24
  %25 = bitcast %class.btJacobianEntry* %arrayidx18 to i8*
  %26 = bitcast i8* %25 to %class.btJacobianEntry*
  %27 = bitcast %class.btMatrix3x3* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %27) #8
  %28 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA20 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %28, i32 0, i32 8
  %29 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA20, align 4, !tbaa !37
  %call21 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %29)
  %call22 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call21)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp19, %class.btMatrix3x3* %call22)
  %30 = bitcast %class.btMatrix3x3* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %30) #8
  %31 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB24 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %31, i32 0, i32 9
  %32 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB24, align 4, !tbaa !41
  %call25 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %32)
  %call26 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call25)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp23, %class.btMatrix3x3* %call26)
  %33 = bitcast %class.btVector3* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %33) #8
  %34 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA28 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %34, i32 0, i32 8
  %35 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA28, align 4, !tbaa !37
  %call29 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %35)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp27, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotAInW, %class.btVector3* nonnull align 4 dereferenceable(16) %call29)
  %36 = bitcast %class.btVector3* %ref.tmp30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %36) #8
  %37 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB31 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %37, i32 0, i32 9
  %38 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB31, align 4, !tbaa !41
  %call32 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %38)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp30, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotBInW, %class.btVector3* nonnull align 4 dereferenceable(16) %call32)
  %39 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx33 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %normal, i32 0, i32 %39
  %40 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA34 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %40, i32 0, i32 8
  %41 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA34, align 4, !tbaa !37
  %call35 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %41)
  %42 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA36 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %42, i32 0, i32 8
  %43 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA36, align 4, !tbaa !37
  %call37 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %43)
  %44 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB38 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %44, i32 0, i32 9
  %45 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB38, align 4, !tbaa !41
  %call39 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %45)
  %46 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB40 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %46, i32 0, i32 9
  %47 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB40, align 4, !tbaa !41
  %call41 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %47)
  %call42 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f(%class.btJacobianEntry* %26, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp19, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp23, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp27, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp30, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx33, %class.btVector3* nonnull align 4 dereferenceable(16) %call35, float %call37, %class.btVector3* nonnull align 4 dereferenceable(16) %call39, float %call41)
  %48 = bitcast %class.btVector3* %ref.tmp30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %48) #8
  %49 = bitcast %class.btVector3* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %49) #8
  %50 = bitcast %class.btMatrix3x3* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %50) #8
  %51 = bitcast %class.btMatrix3x3* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %51) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %52 = load i32, i32* %i, align 4, !tbaa !36
  %inc = add nsw i32 %52, 1
  store i32 %inc, i32* %i, align 4, !tbaa !36
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %53 = bitcast [3 x %class.btVector3]* %normal to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %53) #8
  %54 = bitcast %class.btVector3* %relPos to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %54) #8
  %55 = bitcast %class.btVector3* %pivotBInW to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %55) #8
  %56 = bitcast %class.btVector3* %pivotAInW to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %56) #8
  br label %if.end43

if.end43:                                         ; preds = %for.end, %if.then
  %57 = bitcast %class.btVector3* %jointAxis0local to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %57) #8
  %call44 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %jointAxis0local)
  %58 = bitcast %class.btVector3* %jointAxis1local to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %58) #8
  %call45 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %jointAxis1local)
  %59 = bitcast %class.btVector3* %ref.tmp46 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %59) #8
  %m_rbAFrame47 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %call48 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbAFrame47)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp46, %class.btMatrix3x3* %call48, i32 2)
  call void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp46, %class.btVector3* nonnull align 4 dereferenceable(16) %jointAxis0local, %class.btVector3* nonnull align 4 dereferenceable(16) %jointAxis1local)
  %60 = bitcast %class.btVector3* %ref.tmp46 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %60) #8
  %61 = bitcast %class.btVector3* %jointAxis0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %61) #8
  %call49 = call nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZN17btHingeConstraint13getRigidBodyAEv(%class.btHingeConstraint* %this1)
  %call50 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %call49)
  %call51 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call50)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %jointAxis0, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call51, %class.btVector3* nonnull align 4 dereferenceable(16) %jointAxis0local)
  %62 = bitcast %class.btVector3* %jointAxis1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %62) #8
  %call52 = call nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZN17btHingeConstraint13getRigidBodyAEv(%class.btHingeConstraint* %this1)
  %call53 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %call52)
  %call54 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call53)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %jointAxis1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call54, %class.btVector3* nonnull align 4 dereferenceable(16) %jointAxis1local)
  %63 = bitcast %class.btVector3* %hingeAxisWorld to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %63) #8
  %call55 = call nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZN17btHingeConstraint13getRigidBodyAEv(%class.btHingeConstraint* %this1)
  %call56 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %call55)
  %call57 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call56)
  %64 = bitcast %class.btVector3* %ref.tmp58 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %64) #8
  %m_rbAFrame59 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %call60 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbAFrame59)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp58, %class.btMatrix3x3* %call60, i32 2)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %hingeAxisWorld, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call57, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp58)
  %65 = bitcast %class.btVector3* %ref.tmp58 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %65) #8
  %m_jacAng = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 2
  %arrayidx61 = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jacAng, i32 0, i32 0
  %66 = bitcast %class.btJacobianEntry* %arrayidx61 to i8*
  %67 = bitcast i8* %66 to %class.btJacobianEntry*
  %68 = bitcast %class.btMatrix3x3* %ref.tmp62 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %68) #8
  %69 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA63 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %69, i32 0, i32 8
  %70 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA63, align 4, !tbaa !37
  %call64 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %70)
  %call65 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call64)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp62, %class.btMatrix3x3* %call65)
  %71 = bitcast %class.btMatrix3x3* %ref.tmp66 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %71) #8
  %72 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB67 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %72, i32 0, i32 9
  %73 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB67, align 4, !tbaa !41
  %call68 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %73)
  %call69 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call68)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp66, %class.btMatrix3x3* %call69)
  %74 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA70 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %74, i32 0, i32 8
  %75 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA70, align 4, !tbaa !37
  %call71 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %75)
  %76 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB72 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %76, i32 0, i32 9
  %77 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB72, align 4, !tbaa !41
  %call73 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %77)
  %call74 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2ERK9btVector3RK11btMatrix3x3S5_S2_S2_(%class.btJacobianEntry* %67, %class.btVector3* nonnull align 4 dereferenceable(16) %jointAxis0, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp62, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp66, %class.btVector3* nonnull align 4 dereferenceable(16) %call71, %class.btVector3* nonnull align 4 dereferenceable(16) %call73)
  %78 = bitcast %class.btMatrix3x3* %ref.tmp66 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %78) #8
  %79 = bitcast %class.btMatrix3x3* %ref.tmp62 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %79) #8
  %m_jacAng75 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 2
  %arrayidx76 = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jacAng75, i32 0, i32 1
  %80 = bitcast %class.btJacobianEntry* %arrayidx76 to i8*
  %81 = bitcast i8* %80 to %class.btJacobianEntry*
  %82 = bitcast %class.btMatrix3x3* %ref.tmp77 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %82) #8
  %83 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA78 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %83, i32 0, i32 8
  %84 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA78, align 4, !tbaa !37
  %call79 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %84)
  %call80 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call79)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp77, %class.btMatrix3x3* %call80)
  %85 = bitcast %class.btMatrix3x3* %ref.tmp81 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %85) #8
  %86 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB82 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %86, i32 0, i32 9
  %87 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB82, align 4, !tbaa !41
  %call83 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %87)
  %call84 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call83)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp81, %class.btMatrix3x3* %call84)
  %88 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA85 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %88, i32 0, i32 8
  %89 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA85, align 4, !tbaa !37
  %call86 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %89)
  %90 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB87 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %90, i32 0, i32 9
  %91 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB87, align 4, !tbaa !41
  %call88 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %91)
  %call89 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2ERK9btVector3RK11btMatrix3x3S5_S2_S2_(%class.btJacobianEntry* %81, %class.btVector3* nonnull align 4 dereferenceable(16) %jointAxis1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp77, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp81, %class.btVector3* nonnull align 4 dereferenceable(16) %call86, %class.btVector3* nonnull align 4 dereferenceable(16) %call88)
  %92 = bitcast %class.btMatrix3x3* %ref.tmp81 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %92) #8
  %93 = bitcast %class.btMatrix3x3* %ref.tmp77 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %93) #8
  %m_jacAng90 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 2
  %arrayidx91 = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jacAng90, i32 0, i32 2
  %94 = bitcast %class.btJacobianEntry* %arrayidx91 to i8*
  %95 = bitcast i8* %94 to %class.btJacobianEntry*
  %96 = bitcast %class.btMatrix3x3* %ref.tmp92 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %96) #8
  %97 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA93 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %97, i32 0, i32 8
  %98 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA93, align 4, !tbaa !37
  %call94 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %98)
  %call95 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call94)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp92, %class.btMatrix3x3* %call95)
  %99 = bitcast %class.btMatrix3x3* %ref.tmp96 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %99) #8
  %100 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB97 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %100, i32 0, i32 9
  %101 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB97, align 4, !tbaa !41
  %call98 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %101)
  %call99 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call98)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp96, %class.btMatrix3x3* %call99)
  %102 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA100 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %102, i32 0, i32 8
  %103 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA100, align 4, !tbaa !37
  %call101 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %103)
  %104 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB102 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %104, i32 0, i32 9
  %105 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB102, align 4, !tbaa !41
  %call103 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %105)
  %call104 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2ERK9btVector3RK11btMatrix3x3S5_S2_S2_(%class.btJacobianEntry* %95, %class.btVector3* nonnull align 4 dereferenceable(16) %hingeAxisWorld, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp92, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp96, %class.btVector3* nonnull align 4 dereferenceable(16) %call101, %class.btVector3* nonnull align 4 dereferenceable(16) %call103)
  %106 = bitcast %class.btMatrix3x3* %ref.tmp96 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %106) #8
  %107 = bitcast %class.btMatrix3x3* %ref.tmp92 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %107) #8
  %m_accLimitImpulse = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 9
  store float 0.000000e+00, float* %m_accLimitImpulse, align 4, !tbaa !42
  %108 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA105 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %108, i32 0, i32 8
  %109 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA105, align 4, !tbaa !37
  %call106 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %109)
  %110 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB107 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %110, i32 0, i32 9
  %111 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB107, align 4, !tbaa !41
  %call108 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %111)
  call void @_ZN17btHingeConstraint9testLimitERK11btTransformS2_(%class.btHingeConstraint* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %call106, %class.btTransform* nonnull align 4 dereferenceable(64) %call108)
  %112 = bitcast %class.btVector3* %axisA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %112) #8
  %call109 = call nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZN17btHingeConstraint13getRigidBodyAEv(%class.btHingeConstraint* %this1)
  %call110 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %call109)
  %call111 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call110)
  %113 = bitcast %class.btVector3* %ref.tmp112 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %113) #8
  %m_rbAFrame113 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %call114 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbAFrame113)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp112, %class.btMatrix3x3* %call114, i32 2)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %axisA, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call111, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp112)
  %114 = bitcast %class.btVector3* %ref.tmp112 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %114) #8
  %call115 = call nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZN17btHingeConstraint13getRigidBodyAEv(%class.btHingeConstraint* %this1)
  %call116 = call float @_ZNK11btRigidBody32computeAngularImpulseDenominatorERK9btVector3(%class.btRigidBody* %call115, %class.btVector3* nonnull align 4 dereferenceable(16) %axisA)
  %call117 = call nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZN17btHingeConstraint13getRigidBodyBEv(%class.btHingeConstraint* %this1)
  %call118 = call float @_ZNK11btRigidBody32computeAngularImpulseDenominatorERK9btVector3(%class.btRigidBody* %call117, %class.btVector3* nonnull align 4 dereferenceable(16) %axisA)
  %add = fadd float %call116, %call118
  %div = fdiv float 1.000000e+00, %add
  %m_kHinge = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 8
  store float %div, float* %m_kHinge, align 4, !tbaa !43
  %115 = bitcast %class.btVector3* %axisA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %115) #8
  %116 = bitcast %class.btVector3* %hingeAxisWorld to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %116) #8
  %117 = bitcast %class.btVector3* %jointAxis1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %117) #8
  %118 = bitcast %class.btVector3* %jointAxis0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %118) #8
  %119 = bitcast %class.btVector3* %jointAxis1local to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %119) #8
  %120 = bitcast %class.btVector3* %jointAxis0local to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %120) #8
  br label %if.end119

if.end119:                                        ; preds = %if.end43, %entry
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #5 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #5 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !26
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !26
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !26
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !26
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !26
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !26
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !26
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !26
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !26
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector310normalizedEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %norm = alloca %class.btVector3, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast %class.btVector3* %norm to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = bitcast %class.btVector3* %norm to i8*
  %2 = bitcast %class.btVector3* %this1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !24
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %norm)
  %3 = bitcast %class.btVector3* %agg.result to i8*
  %4 = bitcast %class.btVector3* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false), !tbaa.struct !24
  %5 = bitcast %class.btVector3* %norm to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !26
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !26
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !26
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !26
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !26
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !26
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !26
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #5 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %m_worldTransform)
  ret %class.btVector3* %call
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_invInertiaLocal = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 8
  ret %class.btVector3* %m_invInertiaLocal
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %0 = load float, float* %m_inverseMass, align 4, !tbaa !44
  ret float %0
}

define linkonce_odr hidden %class.btJacobianEntry* @_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f(%class.btJacobianEntry* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %world2A, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %world2B, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2, %class.btVector3* nonnull align 4 dereferenceable(16) %jointAxis, %class.btVector3* nonnull align 4 dereferenceable(16) %inertiaInvA, float %massInvA, %class.btVector3* nonnull align 4 dereferenceable(16) %inertiaInvB, float %massInvB) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btJacobianEntry*, align 4
  %world2A.addr = alloca %class.btMatrix3x3*, align 4
  %world2B.addr = alloca %class.btMatrix3x3*, align 4
  %rel_pos1.addr = alloca %class.btVector3*, align 4
  %rel_pos2.addr = alloca %class.btVector3*, align 4
  %jointAxis.addr = alloca %class.btVector3*, align 4
  %inertiaInvA.addr = alloca %class.btVector3*, align 4
  %massInvA.addr = alloca float, align 4
  %inertiaInvB.addr = alloca %class.btVector3*, align 4
  %massInvB.addr = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca %class.btVector3, align 4
  store %class.btJacobianEntry* %this, %class.btJacobianEntry** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %world2A, %class.btMatrix3x3** %world2A.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %world2B, %class.btMatrix3x3** %world2B.addr, align 4, !tbaa !2
  store %class.btVector3* %rel_pos1, %class.btVector3** %rel_pos1.addr, align 4, !tbaa !2
  store %class.btVector3* %rel_pos2, %class.btVector3** %rel_pos2.addr, align 4, !tbaa !2
  store %class.btVector3* %jointAxis, %class.btVector3** %jointAxis.addr, align 4, !tbaa !2
  store %class.btVector3* %inertiaInvA, %class.btVector3** %inertiaInvA.addr, align 4, !tbaa !2
  store float %massInvA, float* %massInvA.addr, align 4, !tbaa !26
  store %class.btVector3* %inertiaInvB, %class.btVector3** %inertiaInvB.addr, align 4, !tbaa !2
  store float %massInvB, float* %massInvB.addr, align 4, !tbaa !26
  %this1 = load %class.btJacobianEntry*, %class.btJacobianEntry** %this.addr, align 4
  %m_linearJointAxis = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %jointAxis.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVector3* %m_linearJointAxis to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !24
  %m_aJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aJ)
  %m_bJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_bJ)
  %m_0MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_0MinvJt)
  %m_1MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_1MinvJt)
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #8
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %world2A.addr, align 4, !tbaa !2
  %5 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %rel_pos1.addr, align 4, !tbaa !2
  %m_linearJointAxis6 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* %6, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearJointAxis6)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %m_aJ7 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %7 = bitcast %class.btVector3* %m_aJ7 to i8*
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !24
  %9 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #8
  %10 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %10) #8
  %11 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #8
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %world2B.addr, align 4, !tbaa !2
  %13 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #8
  %14 = load %class.btVector3*, %class.btVector3** %rel_pos2.addr, align 4, !tbaa !2
  %15 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %15) #8
  %m_linearJointAxis11 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp10, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearJointAxis11)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* %14, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp10)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp8, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %12, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9)
  %m_bJ12 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %16 = bitcast %class.btVector3* %m_bJ12 to i8*
  %17 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false), !tbaa.struct !24
  %18 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #8
  %19 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %19) #8
  %20 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %20) #8
  %21 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %21) #8
  %22 = load %class.btVector3*, %class.btVector3** %inertiaInvA.addr, align 4, !tbaa !2
  %m_aJ14 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp13, %class.btVector3* nonnull align 4 dereferenceable(16) %22, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aJ14)
  %m_0MinvJt15 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %23 = bitcast %class.btVector3* %m_0MinvJt15 to i8*
  %24 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false), !tbaa.struct !24
  %25 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %25) #8
  %26 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %26) #8
  %27 = load %class.btVector3*, %class.btVector3** %inertiaInvB.addr, align 4, !tbaa !2
  %m_bJ17 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp16, %class.btVector3* nonnull align 4 dereferenceable(16) %27, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bJ17)
  %m_1MinvJt18 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %28 = bitcast %class.btVector3* %m_1MinvJt18 to i8*
  %29 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %29, i32 16, i1 false), !tbaa.struct !24
  %30 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %30) #8
  %31 = load float, float* %massInvA.addr, align 4, !tbaa !26
  %m_0MinvJt19 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %m_aJ20 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %call21 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_0MinvJt19, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aJ20)
  %add = fadd float %31, %call21
  %32 = load float, float* %massInvB.addr, align 4, !tbaa !26
  %add22 = fadd float %add, %32
  %m_1MinvJt23 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %m_bJ24 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %call25 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_1MinvJt23, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bJ24)
  %add26 = fadd float %add22, %call25
  %m_Adiag = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 5
  store float %add26, float* %m_Adiag, align 4, !tbaa !48
  ret %class.btJacobianEntry* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZN17btHingeConstraint13getRigidBodyAEv(%class.btHingeConstraint* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %0 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %0, i32 0, i32 8
  %1 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4, !tbaa !37
  ret %class.btRigidBody* %1
}

define linkonce_odr hidden %class.btJacobianEntry* @_ZN15btJacobianEntryC2ERK9btVector3RK11btMatrix3x3S5_S2_S2_(%class.btJacobianEntry* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %jointAxis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %world2A, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %world2B, %class.btVector3* nonnull align 4 dereferenceable(16) %inertiaInvA, %class.btVector3* nonnull align 4 dereferenceable(16) %inertiaInvB) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btJacobianEntry*, align 4
  %jointAxis.addr = alloca %class.btVector3*, align 4
  %world2A.addr = alloca %class.btMatrix3x3*, align 4
  %world2B.addr = alloca %class.btMatrix3x3*, align 4
  %inertiaInvA.addr = alloca %class.btVector3*, align 4
  %inertiaInvB.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca %class.btVector3, align 4
  store %class.btJacobianEntry* %this, %class.btJacobianEntry** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %jointAxis, %class.btVector3** %jointAxis.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %world2A, %class.btMatrix3x3** %world2A.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %world2B, %class.btMatrix3x3** %world2B.addr, align 4, !tbaa !2
  store %class.btVector3* %inertiaInvA, %class.btVector3** %inertiaInvA.addr, align 4, !tbaa !2
  store %class.btVector3* %inertiaInvB, %class.btVector3** %inertiaInvB.addr, align 4, !tbaa !2
  %this1 = load %class.btJacobianEntry*, %class.btJacobianEntry** %this.addr, align 4
  %m_linearJointAxis = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !26
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !26
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !26
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %m_linearJointAxis, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  %m_aJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aJ)
  %m_bJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_bJ)
  %m_0MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_0MinvJt)
  %m_1MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_1MinvJt)
  %6 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #8
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %world2A.addr, align 4, !tbaa !2
  %8 = load %class.btVector3*, %class.btVector3** %jointAxis.addr, align 4, !tbaa !2
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp8, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %7, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  %m_aJ9 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %9 = bitcast %class.btVector3* %m_aJ9 to i8*
  %10 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false), !tbaa.struct !24
  %11 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %11) #8
  %12 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %12) #8
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %world2B.addr, align 4, !tbaa !2
  %14 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %14) #8
  %15 = load %class.btVector3*, %class.btVector3** %jointAxis.addr, align 4, !tbaa !2
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp11, %class.btVector3* nonnull align 4 dereferenceable(16) %15)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp10, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %13, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp11)
  %m_bJ12 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %16 = bitcast %class.btVector3* %m_bJ12 to i8*
  %17 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false), !tbaa.struct !24
  %18 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #8
  %19 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %19) #8
  %20 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %20) #8
  %21 = load %class.btVector3*, %class.btVector3** %inertiaInvA.addr, align 4, !tbaa !2
  %m_aJ14 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp13, %class.btVector3* nonnull align 4 dereferenceable(16) %21, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aJ14)
  %m_0MinvJt15 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %22 = bitcast %class.btVector3* %m_0MinvJt15 to i8*
  %23 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %22, i8* align 4 %23, i32 16, i1 false), !tbaa.struct !24
  %24 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %24) #8
  %25 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %25) #8
  %26 = load %class.btVector3*, %class.btVector3** %inertiaInvB.addr, align 4, !tbaa !2
  %m_bJ17 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp16, %class.btVector3* nonnull align 4 dereferenceable(16) %26, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bJ17)
  %m_1MinvJt18 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %27 = bitcast %class.btVector3* %m_1MinvJt18 to i8*
  %28 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %27, i8* align 4 %28, i32 16, i1 false), !tbaa.struct !24
  %29 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #8
  %m_0MinvJt19 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %m_aJ20 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %call21 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_0MinvJt19, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aJ20)
  %m_1MinvJt22 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %m_bJ23 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %call24 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_1MinvJt22, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bJ23)
  %add = fadd float %call21, %call24
  %m_Adiag = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 5
  store float %add, float* %m_Adiag, align 4, !tbaa !48
  ret %class.btJacobianEntry* %this1
}

define hidden void @_ZN17btHingeConstraint9testLimitERK11btTransformS2_(%class.btHingeConstraint* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB) #0 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4, !tbaa !2
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %1 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %call = call float @_ZN17btHingeConstraint13getHingeAngleERK11btTransformS2_(%class.btHingeConstraint* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %0, %class.btTransform* nonnull align 4 dereferenceable(64) %1)
  %m_hingeAngle = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 10
  store float %call, float* %m_hingeAngle, align 4, !tbaa !50
  %m_limit = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %m_hingeAngle2 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 10
  %2 = load float, float* %m_hingeAngle2, align 4, !tbaa !50
  call void @_ZN14btAngularLimit4testEf(%class.btAngularLimit* %m_limit, float %2)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK11btRigidBody32computeAngularImpulseDenominatorERK9btVector3(%class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %axis) #5 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %axis.addr = alloca %class.btVector3*, align 4
  %vec = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %axis, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btVector3* %vec to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %this1)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %vec, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call)
  %2 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %call2 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %vec)
  %3 = bitcast %class.btVector3* %vec to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %3) #8
  ret float %call2
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZN17btHingeConstraint13getRigidBodyBEv(%class.btHingeConstraint* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %0 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %0, i32 0, i32 9
  %1 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4, !tbaa !41
  ret %class.btRigidBody* %1
}

define hidden void @_ZN17btHingeConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E(%class.btHingeConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo1"* %info) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo1"*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4, !tbaa !2
  store %"struct.btTypedConstraint::btConstraintInfo1"* %info, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 14
  %0 = load i8, i8* %m_useSolveConstraintObsolete, align 2, !tbaa !19, !range !21
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %m_numConstraintRows = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %1, i32 0, i32 0
  store i32 0, i32* %m_numConstraintRows, align 4, !tbaa !51
  %2 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %nub = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %2, i32 0, i32 1
  store i32 0, i32* %nub, align 4, !tbaa !53
  br label %if.end11

if.else:                                          ; preds = %entry
  %3 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %m_numConstraintRows2 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %3, i32 0, i32 0
  store i32 5, i32* %m_numConstraintRows2, align 4, !tbaa !51
  %4 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %nub3 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %4, i32 0, i32 1
  store i32 1, i32* %nub3, align 4, !tbaa !53
  %5 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %5, i32 0, i32 8
  %6 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4, !tbaa !37
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %6)
  %7 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %7, i32 0, i32 9
  %8 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4, !tbaa !41
  %call4 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %8)
  call void @_ZN17btHingeConstraint9testLimitERK11btTransformS2_(%class.btHingeConstraint* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %call, %class.btTransform* nonnull align 4 dereferenceable(64) %call4)
  %call5 = call i32 @_ZN17btHingeConstraint13getSolveLimitEv(%class.btHingeConstraint* %this1)
  %tobool6 = icmp ne i32 %call5, 0
  br i1 %tobool6, label %if.then8, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.else
  %call7 = call zeroext i1 @_ZN17btHingeConstraint21getEnableAngularMotorEv(%class.btHingeConstraint* %this1)
  br i1 %call7, label %if.then8, label %if.end

if.then8:                                         ; preds = %lor.lhs.false, %if.else
  %9 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %m_numConstraintRows9 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %9, i32 0, i32 0
  %10 = load i32, i32* %m_numConstraintRows9, align 4, !tbaa !51
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %m_numConstraintRows9, align 4, !tbaa !51
  %11 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %nub10 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %11, i32 0, i32 1
  %12 = load i32, i32* %nub10, align 4, !tbaa !53
  %dec = add nsw i32 %12, -1
  store i32 %dec, i32* %nub10, align 4, !tbaa !53
  br label %if.end

if.end:                                           ; preds = %if.then8, %lor.lhs.false
  br label %if.end11

if.end11:                                         ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden i32 @_ZN17btHingeConstraint13getSolveLimitEv(%class.btHingeConstraint* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %m_limit = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call = call zeroext i1 @_ZNK14btAngularLimit7isLimitEv(%class.btAngularLimit* %m_limit)
  %conv = zext i1 %call to i32
  ret i32 %conv
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZN17btHingeConstraint21getEnableAngularMotorEv(%class.btHingeConstraint* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %m_enableAngularMotor = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 13
  %0 = load i8, i8* %m_enableAngularMotor, align 1, !tbaa !18, !range !21
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

; Function Attrs: nounwind
define hidden void @_ZN17btHingeConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E(%class.btHingeConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo1"* %info) #2 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo1"*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4, !tbaa !2
  store %"struct.btTypedConstraint::btConstraintInfo1"* %info, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 14
  %0 = load i8, i8* %m_useSolveConstraintObsolete, align 2, !tbaa !19, !range !21
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %m_numConstraintRows = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %1, i32 0, i32 0
  store i32 0, i32* %m_numConstraintRows, align 4, !tbaa !51
  %2 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %nub = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %2, i32 0, i32 1
  store i32 0, i32* %nub, align 4, !tbaa !53
  br label %if.end

if.else:                                          ; preds = %entry
  %3 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %m_numConstraintRows2 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %3, i32 0, i32 0
  store i32 6, i32* %m_numConstraintRows2, align 4, !tbaa !51
  %4 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %nub3 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %4, i32 0, i32 1
  store i32 0, i32* %nub3, align 4, !tbaa !53
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

define hidden void @_ZN17btHingeConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E(%class.btHingeConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo2"* %info) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo2"*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4, !tbaa !2
  store %"struct.btTypedConstraint::btConstraintInfo2"* %info, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %m_useOffsetForConstraintFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 15
  %0 = load i8, i8* %m_useOffsetForConstraintFrame, align 1, !tbaa !20, !range !21
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %2 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %2, i32 0, i32 8
  %3 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4, !tbaa !37
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %3)
  %4 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %4, i32 0, i32 9
  %5 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4, !tbaa !41
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %5)
  %6 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA3 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %6, i32 0, i32 8
  %7 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA3, align 4, !tbaa !37
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %7)
  %8 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB5 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %8, i32 0, i32 9
  %9 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB5, align 4, !tbaa !41
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %9)
  call void @_ZN17btHingeConstraint32getInfo2InternalUsingFrameOffsetEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_(%class.btHingeConstraint* %this1, %"struct.btTypedConstraint::btConstraintInfo2"* %1, %class.btTransform* nonnull align 4 dereferenceable(64) %call, %class.btTransform* nonnull align 4 dereferenceable(64) %call2, %class.btVector3* nonnull align 4 dereferenceable(16) %call4, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  br label %if.end

if.else:                                          ; preds = %entry
  %10 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %11 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA7 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %11, i32 0, i32 8
  %12 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA7, align 4, !tbaa !37
  %call8 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %12)
  %13 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB9 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %13, i32 0, i32 9
  %14 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB9, align 4, !tbaa !41
  %call10 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %14)
  %15 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA11 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %15, i32 0, i32 8
  %16 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA11, align 4, !tbaa !37
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %16)
  %17 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB13 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %17, i32 0, i32 9
  %18 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB13, align 4, !tbaa !41
  %call14 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %18)
  call void @_ZN17btHingeConstraint16getInfo2InternalEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_(%class.btHingeConstraint* %this1, %"struct.btTypedConstraint::btConstraintInfo2"* %10, %class.btTransform* nonnull align 4 dereferenceable(64) %call8, %class.btTransform* nonnull align 4 dereferenceable(64) %call10, %class.btVector3* nonnull align 4 dereferenceable(16) %call12, %class.btVector3* nonnull align 4 dereferenceable(16) %call14)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

define hidden void @_ZN17btHingeConstraint32getInfo2InternalUsingFrameOffsetEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_(%class.btHingeConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo2"* %info, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelA, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelB) #0 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo2"*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %angVelA.addr = alloca %class.btVector3*, align 4
  %angVelB.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  %s = alloca i32, align 4
  %trA = alloca %class.btTransform, align 4
  %trB = alloca %class.btTransform, align 4
  %ofs = alloca %class.btVector3, align 4
  %miA = alloca float, align 4
  %miB = alloca float, align 4
  %hasStaticBody = alloca i8, align 1
  %miS = alloca float, align 4
  %factA = alloca float, align 4
  %factB = alloca float, align 4
  %ax1A = alloca %class.btVector3, align 4
  %ax1B = alloca %class.btVector3, align 4
  %ax1 = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  %bodyA_trans = alloca %class.btTransform, align 4
  %bodyB_trans = alloca %class.btTransform, align 4
  %s0 = alloca i32, align 4
  %s1 = alloca i32, align 4
  %s2 = alloca i32, align 4
  %nrow = alloca i32, align 4
  %tmpA = alloca %class.btVector3, align 4
  %tmpB = alloca %class.btVector3, align 4
  %relA = alloca %class.btVector3, align 4
  %relB = alloca %class.btVector3, align 4
  %p = alloca %class.btVector3, align 4
  %q = alloca %class.btVector3, align 4
  %ref.tmp21 = alloca %class.btVector3, align 4
  %projB = alloca %class.btVector3, align 4
  %ref.tmp24 = alloca float, align 4
  %orthoB = alloca %class.btVector3, align 4
  %ref.tmp26 = alloca %class.btVector3, align 4
  %projA = alloca %class.btVector3, align 4
  %ref.tmp29 = alloca float, align 4
  %orthoA = alloca %class.btVector3, align 4
  %totalDist = alloca %class.btVector3, align 4
  %ref.tmp31 = alloca %class.btVector3, align 4
  %ref.tmp32 = alloca %class.btVector3, align 4
  %ref.tmp33 = alloca %class.btVector3, align 4
  %ref.tmp34 = alloca %class.btVector3, align 4
  %ref.tmp35 = alloca %class.btVector3, align 4
  %ref.tmp36 = alloca %class.btVector3, align 4
  %ref.tmp37 = alloca %class.btVector3, align 4
  %len2 = alloca float, align 4
  %ref.tmp41 = alloca float, align 4
  %ref.tmp45 = alloca %class.btVector3, align 4
  %ref.tmp48 = alloca %class.btVector3, align 4
  %ref.tmp49 = alloca %class.btVector3, align 4
  %ref.tmp50 = alloca %class.btVector3, align 4
  %ref.tmp65 = alloca %class.btVector3, align 4
  %ref.tmp66 = alloca %class.btVector3, align 4
  %ref.tmp96 = alloca %class.btVector3, align 4
  %ref.tmp97 = alloca %class.btVector3, align 4
  %k = alloca float, align 4
  %rhs = alloca float, align 4
  %s3 = alloca i32, align 4
  %s4 = alloca i32, align 4
  %u = alloca %class.btVector3, align 4
  %srow = alloca i32, align 4
  %limit_err = alloca float, align 4
  %limit = alloca i32, align 4
  %powered = alloca i32, align 4
  %lostop = alloca float, align 4
  %histop = alloca float, align 4
  %currERP = alloca float, align 4
  %mot_fact = alloca float, align 4
  %bounce = alloca float, align 4
  %vel = alloca float, align 4
  %newc = alloca float, align 4
  %newc430 = alloca float, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4, !tbaa !2
  store %"struct.btTypedConstraint::btConstraintInfo2"* %info, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4, !tbaa !2
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4, !tbaa !2
  store %class.btVector3* %angVelA, %class.btVector3** %angVelA.addr, align 4, !tbaa !2
  store %class.btVector3* %angVelB, %class.btVector3** %angVelB.addr, align 4, !tbaa !2
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = bitcast i32* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %2, i32 0, i32 6
  %3 = load i32, i32* %rowskip, align 4, !tbaa !54
  store i32 %3, i32* %s, align 4, !tbaa !36
  %4 = bitcast %class.btTransform* %trA to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %4) #8
  %5 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %m_rbAFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %trA, %class.btTransform* %5, %class.btTransform* nonnull align 4 dereferenceable(64) %m_rbAFrame)
  %6 = bitcast %class.btTransform* %trB to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %6) #8
  %7 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %m_rbBFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %trB, %class.btTransform* %7, %class.btTransform* nonnull align 4 dereferenceable(64) %m_rbBFrame)
  %8 = bitcast %class.btVector3* %ofs to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #8
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %trB)
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %trA)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ofs, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call2)
  %9 = bitcast float* %miA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  %call3 = call nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZN17btHingeConstraint13getRigidBodyAEv(%class.btHingeConstraint* %this1)
  %call4 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %call3)
  store float %call4, float* %miA, align 4, !tbaa !26
  %10 = bitcast float* %miB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %call5 = call nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZN17btHingeConstraint13getRigidBodyBEv(%class.btHingeConstraint* %this1)
  %call6 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %call5)
  store float %call6, float* %miB, align 4, !tbaa !26
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %hasStaticBody) #8
  %11 = load float, float* %miA, align 4, !tbaa !26
  %cmp = fcmp olt float %11, 0x3E80000000000000
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %12 = load float, float* %miB, align 4, !tbaa !26
  %cmp7 = fcmp olt float %12, 0x3E80000000000000
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %13 = phi i1 [ true, %entry ], [ %cmp7, %lor.rhs ]
  %frombool = zext i1 %13 to i8
  store i8 %frombool, i8* %hasStaticBody, align 1, !tbaa !6
  %14 = bitcast float* %miS to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #8
  %15 = load float, float* %miA, align 4, !tbaa !26
  %16 = load float, float* %miB, align 4, !tbaa !26
  %add = fadd float %15, %16
  store float %add, float* %miS, align 4, !tbaa !26
  %17 = bitcast float* %factA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #8
  %18 = bitcast float* %factB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #8
  %19 = load float, float* %miS, align 4, !tbaa !26
  %cmp8 = fcmp ogt float %19, 0.000000e+00
  br i1 %cmp8, label %if.then, label %if.else

if.then:                                          ; preds = %lor.end
  %20 = load float, float* %miB, align 4, !tbaa !26
  %21 = load float, float* %miS, align 4, !tbaa !26
  %div = fdiv float %20, %21
  store float %div, float* %factA, align 4, !tbaa !26
  br label %if.end

if.else:                                          ; preds = %lor.end
  store float 5.000000e-01, float* %factA, align 4, !tbaa !26
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %22 = load float, float* %factA, align 4, !tbaa !26
  %sub = fsub float 1.000000e+00, %22
  store float %sub, float* %factB, align 4, !tbaa !26
  %23 = bitcast %class.btVector3* %ax1A to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %23) #8
  %call9 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %trA)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ax1A, %class.btMatrix3x3* %call9, i32 2)
  %24 = bitcast %class.btVector3* %ax1B to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %24) #8
  %call10 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %trB)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ax1B, %class.btMatrix3x3* %call10, i32 2)
  %25 = bitcast %class.btVector3* %ax1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %25) #8
  %26 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %26) #8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1A, float* nonnull align 4 dereferenceable(4) %factA)
  %27 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %27) #8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp11, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1B, float* nonnull align 4 dereferenceable(4) %factB)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ax1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp11)
  %28 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %28) #8
  %29 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #8
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %ax1)
  %30 = bitcast %class.btTransform* %bodyA_trans to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %30) #8
  %31 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %call13 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %bodyA_trans, %class.btTransform* nonnull align 4 dereferenceable(64) %31)
  %32 = bitcast %class.btTransform* %bodyB_trans to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %32) #8
  %33 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %call14 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %bodyB_trans, %class.btTransform* nonnull align 4 dereferenceable(64) %33)
  %34 = bitcast i32* %s0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #8
  store i32 0, i32* %s0, align 4, !tbaa !36
  %35 = bitcast i32* %s1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #8
  %36 = load i32, i32* %s, align 4, !tbaa !36
  store i32 %36, i32* %s1, align 4, !tbaa !36
  %37 = bitcast i32* %s2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #8
  %38 = load i32, i32* %s, align 4, !tbaa !36
  %mul = mul nsw i32 %38, 2
  store i32 %mul, i32* %s2, align 4, !tbaa !36
  %39 = bitcast i32* %nrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #8
  store i32 2, i32* %nrow, align 4, !tbaa !36
  %40 = bitcast %class.btVector3* %tmpA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %40) #8
  %call15 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %tmpA)
  %41 = bitcast %class.btVector3* %tmpB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %41) #8
  %call16 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %tmpB)
  %42 = bitcast %class.btVector3* %relA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %42) #8
  %call17 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %relA)
  %43 = bitcast %class.btVector3* %relB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %43) #8
  %call18 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %relB)
  %44 = bitcast %class.btVector3* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %44) #8
  %call19 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %p)
  %45 = bitcast %class.btVector3* %q to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %45) #8
  %call20 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %q)
  %46 = bitcast %class.btVector3* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %46) #8
  %call22 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %trB)
  %call23 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %bodyB_trans)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp21, %class.btVector3* nonnull align 4 dereferenceable(16) %call22, %class.btVector3* nonnull align 4 dereferenceable(16) %call23)
  %47 = bitcast %class.btVector3* %relB to i8*
  %48 = bitcast %class.btVector3* %ref.tmp21 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %47, i8* align 4 %48, i32 16, i1 false), !tbaa.struct !24
  %49 = bitcast %class.btVector3* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %49) #8
  %50 = bitcast %class.btVector3* %projB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %50) #8
  %51 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #8
  %call25 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %relB, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1)
  store float %call25, float* %ref.tmp24, align 4, !tbaa !26
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %projB, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1, float* nonnull align 4 dereferenceable(4) %ref.tmp24)
  %52 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #8
  %53 = bitcast %class.btVector3* %orthoB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %53) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %orthoB, %class.btVector3* nonnull align 4 dereferenceable(16) %relB, %class.btVector3* nonnull align 4 dereferenceable(16) %projB)
  %54 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %54) #8
  %call27 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %trA)
  %call28 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %bodyA_trans)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp26, %class.btVector3* nonnull align 4 dereferenceable(16) %call27, %class.btVector3* nonnull align 4 dereferenceable(16) %call28)
  %55 = bitcast %class.btVector3* %relA to i8*
  %56 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %55, i8* align 4 %56, i32 16, i1 false), !tbaa.struct !24
  %57 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %57) #8
  %58 = bitcast %class.btVector3* %projA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %58) #8
  %59 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #8
  %call30 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %relA, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1)
  store float %call30, float* %ref.tmp29, align 4, !tbaa !26
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %projA, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1, float* nonnull align 4 dereferenceable(4) %ref.tmp29)
  %60 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #8
  %61 = bitcast %class.btVector3* %orthoA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %61) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %orthoA, %class.btVector3* nonnull align 4 dereferenceable(16) %relA, %class.btVector3* nonnull align 4 dereferenceable(16) %projA)
  %62 = bitcast %class.btVector3* %totalDist to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %62) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %totalDist, %class.btVector3* nonnull align 4 dereferenceable(16) %projA, %class.btVector3* nonnull align 4 dereferenceable(16) %projB)
  %63 = bitcast %class.btVector3* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %63) #8
  %64 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %64) #8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp32, %class.btVector3* nonnull align 4 dereferenceable(16) %totalDist, float* nonnull align 4 dereferenceable(4) %factA)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp31, %class.btVector3* nonnull align 4 dereferenceable(16) %orthoA, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp32)
  %65 = bitcast %class.btVector3* %relA to i8*
  %66 = bitcast %class.btVector3* %ref.tmp31 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %65, i8* align 4 %66, i32 16, i1 false), !tbaa.struct !24
  %67 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %67) #8
  %68 = bitcast %class.btVector3* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %68) #8
  %69 = bitcast %class.btVector3* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %69) #8
  %70 = bitcast %class.btVector3* %ref.tmp34 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %70) #8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp34, %class.btVector3* nonnull align 4 dereferenceable(16) %totalDist, float* nonnull align 4 dereferenceable(4) %factB)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp33, %class.btVector3* nonnull align 4 dereferenceable(16) %orthoB, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp34)
  %71 = bitcast %class.btVector3* %relB to i8*
  %72 = bitcast %class.btVector3* %ref.tmp33 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %71, i8* align 4 %72, i32 16, i1 false), !tbaa.struct !24
  %73 = bitcast %class.btVector3* %ref.tmp34 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %73) #8
  %74 = bitcast %class.btVector3* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %74) #8
  %75 = bitcast %class.btVector3* %ref.tmp35 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %75) #8
  %76 = bitcast %class.btVector3* %ref.tmp36 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %76) #8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp36, %class.btVector3* nonnull align 4 dereferenceable(16) %orthoB, float* nonnull align 4 dereferenceable(4) %factA)
  %77 = bitcast %class.btVector3* %ref.tmp37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %77) #8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp37, %class.btVector3* nonnull align 4 dereferenceable(16) %orthoA, float* nonnull align 4 dereferenceable(4) %factB)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp35, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp36, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp37)
  %78 = bitcast %class.btVector3* %p to i8*
  %79 = bitcast %class.btVector3* %ref.tmp35 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %78, i8* align 4 %79, i32 16, i1 false), !tbaa.struct !24
  %80 = bitcast %class.btVector3* %ref.tmp37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %80) #8
  %81 = bitcast %class.btVector3* %ref.tmp36 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %81) #8
  %82 = bitcast %class.btVector3* %ref.tmp35 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %82) #8
  %83 = bitcast float* %len2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %83) #8
  %call38 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %p)
  store float %call38, float* %len2, align 4, !tbaa !26
  %84 = load float, float* %len2, align 4, !tbaa !26
  %cmp39 = fcmp ogt float %84, 0x3E80000000000000
  br i1 %cmp39, label %if.then40, label %if.else44

if.then40:                                        ; preds = %if.end
  %85 = bitcast float* %ref.tmp41 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %85) #8
  %86 = load float, float* %len2, align 4, !tbaa !26
  %call42 = call float @_Z6btSqrtf(float %86)
  store float %call42, float* %ref.tmp41, align 4, !tbaa !26
  %call43 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %p, float* nonnull align 4 dereferenceable(4) %ref.tmp41)
  %87 = bitcast float* %ref.tmp41 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #8
  br label %if.end47

if.else44:                                        ; preds = %if.end
  %88 = bitcast %class.btVector3* %ref.tmp45 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %88) #8
  %call46 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %trA)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp45, %class.btMatrix3x3* %call46, i32 1)
  %89 = bitcast %class.btVector3* %p to i8*
  %90 = bitcast %class.btVector3* %ref.tmp45 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %89, i8* align 4 %90, i32 16, i1 false), !tbaa.struct !24
  %91 = bitcast %class.btVector3* %ref.tmp45 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %91) #8
  br label %if.end47

if.end47:                                         ; preds = %if.else44, %if.then40
  %92 = bitcast %class.btVector3* %ref.tmp48 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %92) #8
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp48, %class.btVector3* %ax1, %class.btVector3* nonnull align 4 dereferenceable(16) %p)
  %93 = bitcast %class.btVector3* %q to i8*
  %94 = bitcast %class.btVector3* %ref.tmp48 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %93, i8* align 4 %94, i32 16, i1 false), !tbaa.struct !24
  %95 = bitcast %class.btVector3* %ref.tmp48 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %95) #8
  %96 = bitcast %class.btVector3* %ref.tmp49 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %96) #8
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp49, %class.btVector3* %relA, %class.btVector3* nonnull align 4 dereferenceable(16) %p)
  %97 = bitcast %class.btVector3* %tmpA to i8*
  %98 = bitcast %class.btVector3* %ref.tmp49 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %97, i8* align 4 %98, i32 16, i1 false), !tbaa.struct !24
  %99 = bitcast %class.btVector3* %ref.tmp49 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %99) #8
  %100 = bitcast %class.btVector3* %ref.tmp50 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %100) #8
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp50, %class.btVector3* %relB, %class.btVector3* nonnull align 4 dereferenceable(16) %p)
  %101 = bitcast %class.btVector3* %tmpB to i8*
  %102 = bitcast %class.btVector3* %ref.tmp50 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %101, i8* align 4 %102, i32 16, i1 false), !tbaa.struct !24
  %103 = bitcast %class.btVector3* %ref.tmp50 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %103) #8
  store i32 0, i32* %i, align 4, !tbaa !36
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end47
  %104 = load i32, i32* %i, align 4, !tbaa !36
  %cmp51 = icmp slt i32 %104, 3
  br i1 %cmp51, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call52 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpA)
  %105 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx = getelementptr inbounds float, float* %call52, i32 %105
  %106 = load float, float* %arrayidx, align 4, !tbaa !26
  %107 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %107, i32 0, i32 3
  %108 = load float*, float** %m_J1angularAxis, align 4, !tbaa !56
  %109 = load i32, i32* %s0, align 4, !tbaa !36
  %110 = load i32, i32* %i, align 4, !tbaa !36
  %add53 = add nsw i32 %109, %110
  %arrayidx54 = getelementptr inbounds float, float* %108, i32 %add53
  store float %106, float* %arrayidx54, align 4, !tbaa !26
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %111 = load i32, i32* %i, align 4, !tbaa !36
  %inc = add nsw i32 %111, 1
  store i32 %inc, i32* %i, align 4, !tbaa !36
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !36
  br label %for.cond55

for.cond55:                                       ; preds = %for.inc62, %for.end
  %112 = load i32, i32* %i, align 4, !tbaa !36
  %cmp56 = icmp slt i32 %112, 3
  br i1 %cmp56, label %for.body57, label %for.end64

for.body57:                                       ; preds = %for.cond55
  %call58 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpB)
  %113 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx59 = getelementptr inbounds float, float* %call58, i32 %113
  %114 = load float, float* %arrayidx59, align 4, !tbaa !26
  %fneg = fneg float %114
  %115 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %115, i32 0, i32 5
  %116 = load float*, float** %m_J2angularAxis, align 4, !tbaa !57
  %117 = load i32, i32* %s0, align 4, !tbaa !36
  %118 = load i32, i32* %i, align 4, !tbaa !36
  %add60 = add nsw i32 %117, %118
  %arrayidx61 = getelementptr inbounds float, float* %116, i32 %add60
  store float %fneg, float* %arrayidx61, align 4, !tbaa !26
  br label %for.inc62

for.inc62:                                        ; preds = %for.body57
  %119 = load i32, i32* %i, align 4, !tbaa !36
  %inc63 = add nsw i32 %119, 1
  store i32 %inc63, i32* %i, align 4, !tbaa !36
  br label %for.cond55

for.end64:                                        ; preds = %for.cond55
  %120 = bitcast %class.btVector3* %ref.tmp65 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %120) #8
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp65, %class.btVector3* %relA, %class.btVector3* nonnull align 4 dereferenceable(16) %q)
  %121 = bitcast %class.btVector3* %tmpA to i8*
  %122 = bitcast %class.btVector3* %ref.tmp65 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %121, i8* align 4 %122, i32 16, i1 false), !tbaa.struct !24
  %123 = bitcast %class.btVector3* %ref.tmp65 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %123) #8
  %124 = bitcast %class.btVector3* %ref.tmp66 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %124) #8
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp66, %class.btVector3* %relB, %class.btVector3* nonnull align 4 dereferenceable(16) %q)
  %125 = bitcast %class.btVector3* %tmpB to i8*
  %126 = bitcast %class.btVector3* %ref.tmp66 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %125, i8* align 4 %126, i32 16, i1 false), !tbaa.struct !24
  %127 = bitcast %class.btVector3* %ref.tmp66 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %127) #8
  %128 = load i8, i8* %hasStaticBody, align 1, !tbaa !6, !range !21
  %tobool = trunc i8 %128 to i1
  br i1 %tobool, label %land.lhs.true, label %if.end72

land.lhs.true:                                    ; preds = %for.end64
  %call67 = call i32 @_ZN17btHingeConstraint13getSolveLimitEv(%class.btHingeConstraint* %this1)
  %tobool68 = icmp ne i32 %call67, 0
  br i1 %tobool68, label %if.then69, label %if.end72

if.then69:                                        ; preds = %land.lhs.true
  %call70 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %tmpB, float* nonnull align 4 dereferenceable(4) %factB)
  %call71 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %tmpA, float* nonnull align 4 dereferenceable(4) %factA)
  br label %if.end72

if.end72:                                         ; preds = %if.then69, %land.lhs.true, %for.end64
  store i32 0, i32* %i, align 4, !tbaa !36
  br label %for.cond73

for.cond73:                                       ; preds = %for.inc81, %if.end72
  %129 = load i32, i32* %i, align 4, !tbaa !36
  %cmp74 = icmp slt i32 %129, 3
  br i1 %cmp74, label %for.body75, label %for.end83

for.body75:                                       ; preds = %for.cond73
  %call76 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpA)
  %130 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 %130
  %131 = load float, float* %arrayidx77, align 4, !tbaa !26
  %132 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis78 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %132, i32 0, i32 3
  %133 = load float*, float** %m_J1angularAxis78, align 4, !tbaa !56
  %134 = load i32, i32* %s1, align 4, !tbaa !36
  %135 = load i32, i32* %i, align 4, !tbaa !36
  %add79 = add nsw i32 %134, %135
  %arrayidx80 = getelementptr inbounds float, float* %133, i32 %add79
  store float %131, float* %arrayidx80, align 4, !tbaa !26
  br label %for.inc81

for.inc81:                                        ; preds = %for.body75
  %136 = load i32, i32* %i, align 4, !tbaa !36
  %inc82 = add nsw i32 %136, 1
  store i32 %inc82, i32* %i, align 4, !tbaa !36
  br label %for.cond73

for.end83:                                        ; preds = %for.cond73
  store i32 0, i32* %i, align 4, !tbaa !36
  br label %for.cond84

for.cond84:                                       ; preds = %for.inc93, %for.end83
  %137 = load i32, i32* %i, align 4, !tbaa !36
  %cmp85 = icmp slt i32 %137, 3
  br i1 %cmp85, label %for.body86, label %for.end95

for.body86:                                       ; preds = %for.cond84
  %call87 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpB)
  %138 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx88 = getelementptr inbounds float, float* %call87, i32 %138
  %139 = load float, float* %arrayidx88, align 4, !tbaa !26
  %fneg89 = fneg float %139
  %140 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis90 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %140, i32 0, i32 5
  %141 = load float*, float** %m_J2angularAxis90, align 4, !tbaa !57
  %142 = load i32, i32* %s1, align 4, !tbaa !36
  %143 = load i32, i32* %i, align 4, !tbaa !36
  %add91 = add nsw i32 %142, %143
  %arrayidx92 = getelementptr inbounds float, float* %141, i32 %add91
  store float %fneg89, float* %arrayidx92, align 4, !tbaa !26
  br label %for.inc93

for.inc93:                                        ; preds = %for.body86
  %144 = load i32, i32* %i, align 4, !tbaa !36
  %inc94 = add nsw i32 %144, 1
  store i32 %inc94, i32* %i, align 4, !tbaa !36
  br label %for.cond84

for.end95:                                        ; preds = %for.cond84
  %145 = bitcast %class.btVector3* %ref.tmp96 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %145) #8
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp96, %class.btVector3* %relA, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1)
  %146 = bitcast %class.btVector3* %tmpA to i8*
  %147 = bitcast %class.btVector3* %ref.tmp96 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %146, i8* align 4 %147, i32 16, i1 false), !tbaa.struct !24
  %148 = bitcast %class.btVector3* %ref.tmp96 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %148) #8
  %149 = bitcast %class.btVector3* %ref.tmp97 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %149) #8
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp97, %class.btVector3* %relB, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1)
  %150 = bitcast %class.btVector3* %tmpB to i8*
  %151 = bitcast %class.btVector3* %ref.tmp97 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %150, i8* align 4 %151, i32 16, i1 false), !tbaa.struct !24
  %152 = bitcast %class.btVector3* %ref.tmp97 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %152) #8
  %153 = load i8, i8* %hasStaticBody, align 1, !tbaa !6, !range !21
  %tobool98 = trunc i8 %153 to i1
  br i1 %tobool98, label %if.then99, label %if.end102

if.then99:                                        ; preds = %for.end95
  %call100 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %tmpB, float* nonnull align 4 dereferenceable(4) %factB)
  %call101 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %tmpA, float* nonnull align 4 dereferenceable(4) %factA)
  br label %if.end102

if.end102:                                        ; preds = %if.then99, %for.end95
  store i32 0, i32* %i, align 4, !tbaa !36
  br label %for.cond103

for.cond103:                                      ; preds = %for.inc111, %if.end102
  %154 = load i32, i32* %i, align 4, !tbaa !36
  %cmp104 = icmp slt i32 %154, 3
  br i1 %cmp104, label %for.body105, label %for.end113

for.body105:                                      ; preds = %for.cond103
  %call106 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpA)
  %155 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx107 = getelementptr inbounds float, float* %call106, i32 %155
  %156 = load float, float* %arrayidx107, align 4, !tbaa !26
  %157 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis108 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %157, i32 0, i32 3
  %158 = load float*, float** %m_J1angularAxis108, align 4, !tbaa !56
  %159 = load i32, i32* %s2, align 4, !tbaa !36
  %160 = load i32, i32* %i, align 4, !tbaa !36
  %add109 = add nsw i32 %159, %160
  %arrayidx110 = getelementptr inbounds float, float* %158, i32 %add109
  store float %156, float* %arrayidx110, align 4, !tbaa !26
  br label %for.inc111

for.inc111:                                       ; preds = %for.body105
  %161 = load i32, i32* %i, align 4, !tbaa !36
  %inc112 = add nsw i32 %161, 1
  store i32 %inc112, i32* %i, align 4, !tbaa !36
  br label %for.cond103

for.end113:                                       ; preds = %for.cond103
  store i32 0, i32* %i, align 4, !tbaa !36
  br label %for.cond114

for.cond114:                                      ; preds = %for.inc123, %for.end113
  %162 = load i32, i32* %i, align 4, !tbaa !36
  %cmp115 = icmp slt i32 %162, 3
  br i1 %cmp115, label %for.body116, label %for.end125

for.body116:                                      ; preds = %for.cond114
  %call117 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpB)
  %163 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx118 = getelementptr inbounds float, float* %call117, i32 %163
  %164 = load float, float* %arrayidx118, align 4, !tbaa !26
  %fneg119 = fneg float %164
  %165 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis120 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %165, i32 0, i32 5
  %166 = load float*, float** %m_J2angularAxis120, align 4, !tbaa !57
  %167 = load i32, i32* %s2, align 4, !tbaa !36
  %168 = load i32, i32* %i, align 4, !tbaa !36
  %add121 = add nsw i32 %167, %168
  %arrayidx122 = getelementptr inbounds float, float* %166, i32 %add121
  store float %fneg119, float* %arrayidx122, align 4, !tbaa !26
  br label %for.inc123

for.inc123:                                       ; preds = %for.body116
  %169 = load i32, i32* %i, align 4, !tbaa !36
  %inc124 = add nsw i32 %169, 1
  store i32 %inc124, i32* %i, align 4, !tbaa !36
  br label %for.cond114

for.end125:                                       ; preds = %for.cond114
  %170 = bitcast float* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %170) #8
  %171 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %fps = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %171, i32 0, i32 0
  %172 = load float, float* %fps, align 4, !tbaa !58
  %173 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %erp = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %173, i32 0, i32 1
  %174 = load float, float* %erp, align 4, !tbaa !59
  %mul126 = fmul float %172, %174
  store float %mul126, float* %k, align 4, !tbaa !26
  %m_angularOnly = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 12
  %175 = load i8, i8* %m_angularOnly, align 4, !tbaa !10, !range !21
  %tobool127 = trunc i8 %175 to i1
  br i1 %tobool127, label %if.end207, label %if.then128

if.then128:                                       ; preds = %for.end125
  store i32 0, i32* %i, align 4, !tbaa !36
  br label %for.cond129

for.cond129:                                      ; preds = %for.inc136, %if.then128
  %176 = load i32, i32* %i, align 4, !tbaa !36
  %cmp130 = icmp slt i32 %176, 3
  br i1 %cmp130, label %for.body131, label %for.end138

for.body131:                                      ; preds = %for.cond129
  %call132 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %177 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx133 = getelementptr inbounds float, float* %call132, i32 %177
  %178 = load float, float* %arrayidx133, align 4, !tbaa !26
  %179 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1linearAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %179, i32 0, i32 2
  %180 = load float*, float** %m_J1linearAxis, align 4, !tbaa !60
  %181 = load i32, i32* %s0, align 4, !tbaa !36
  %182 = load i32, i32* %i, align 4, !tbaa !36
  %add134 = add nsw i32 %181, %182
  %arrayidx135 = getelementptr inbounds float, float* %180, i32 %add134
  store float %178, float* %arrayidx135, align 4, !tbaa !26
  br label %for.inc136

for.inc136:                                       ; preds = %for.body131
  %183 = load i32, i32* %i, align 4, !tbaa !36
  %inc137 = add nsw i32 %183, 1
  store i32 %inc137, i32* %i, align 4, !tbaa !36
  br label %for.cond129

for.end138:                                       ; preds = %for.cond129
  store i32 0, i32* %i, align 4, !tbaa !36
  br label %for.cond139

for.cond139:                                      ; preds = %for.inc147, %for.end138
  %184 = load i32, i32* %i, align 4, !tbaa !36
  %cmp140 = icmp slt i32 %184, 3
  br i1 %cmp140, label %for.body141, label %for.end149

for.body141:                                      ; preds = %for.cond139
  %call142 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %185 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx143 = getelementptr inbounds float, float* %call142, i32 %185
  %186 = load float, float* %arrayidx143, align 4, !tbaa !26
  %187 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1linearAxis144 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %187, i32 0, i32 2
  %188 = load float*, float** %m_J1linearAxis144, align 4, !tbaa !60
  %189 = load i32, i32* %s1, align 4, !tbaa !36
  %190 = load i32, i32* %i, align 4, !tbaa !36
  %add145 = add nsw i32 %189, %190
  %arrayidx146 = getelementptr inbounds float, float* %188, i32 %add145
  store float %186, float* %arrayidx146, align 4, !tbaa !26
  br label %for.inc147

for.inc147:                                       ; preds = %for.body141
  %191 = load i32, i32* %i, align 4, !tbaa !36
  %inc148 = add nsw i32 %191, 1
  store i32 %inc148, i32* %i, align 4, !tbaa !36
  br label %for.cond139

for.end149:                                       ; preds = %for.cond139
  store i32 0, i32* %i, align 4, !tbaa !36
  br label %for.cond150

for.cond150:                                      ; preds = %for.inc158, %for.end149
  %192 = load i32, i32* %i, align 4, !tbaa !36
  %cmp151 = icmp slt i32 %192, 3
  br i1 %cmp151, label %for.body152, label %for.end160

for.body152:                                      ; preds = %for.cond150
  %call153 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %193 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx154 = getelementptr inbounds float, float* %call153, i32 %193
  %194 = load float, float* %arrayidx154, align 4, !tbaa !26
  %195 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1linearAxis155 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %195, i32 0, i32 2
  %196 = load float*, float** %m_J1linearAxis155, align 4, !tbaa !60
  %197 = load i32, i32* %s2, align 4, !tbaa !36
  %198 = load i32, i32* %i, align 4, !tbaa !36
  %add156 = add nsw i32 %197, %198
  %arrayidx157 = getelementptr inbounds float, float* %196, i32 %add156
  store float %194, float* %arrayidx157, align 4, !tbaa !26
  br label %for.inc158

for.inc158:                                       ; preds = %for.body152
  %199 = load i32, i32* %i, align 4, !tbaa !36
  %inc159 = add nsw i32 %199, 1
  store i32 %inc159, i32* %i, align 4, !tbaa !36
  br label %for.cond150

for.end160:                                       ; preds = %for.cond150
  store i32 0, i32* %i, align 4, !tbaa !36
  br label %for.cond161

for.cond161:                                      ; preds = %for.inc169, %for.end160
  %200 = load i32, i32* %i, align 4, !tbaa !36
  %cmp162 = icmp slt i32 %200, 3
  br i1 %cmp162, label %for.body163, label %for.end171

for.body163:                                      ; preds = %for.cond161
  %call164 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %201 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx165 = getelementptr inbounds float, float* %call164, i32 %201
  %202 = load float, float* %arrayidx165, align 4, !tbaa !26
  %fneg166 = fneg float %202
  %203 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2linearAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %203, i32 0, i32 4
  %204 = load float*, float** %m_J2linearAxis, align 4, !tbaa !61
  %205 = load i32, i32* %s0, align 4, !tbaa !36
  %206 = load i32, i32* %i, align 4, !tbaa !36
  %add167 = add nsw i32 %205, %206
  %arrayidx168 = getelementptr inbounds float, float* %204, i32 %add167
  store float %fneg166, float* %arrayidx168, align 4, !tbaa !26
  br label %for.inc169

for.inc169:                                       ; preds = %for.body163
  %207 = load i32, i32* %i, align 4, !tbaa !36
  %inc170 = add nsw i32 %207, 1
  store i32 %inc170, i32* %i, align 4, !tbaa !36
  br label %for.cond161

for.end171:                                       ; preds = %for.cond161
  store i32 0, i32* %i, align 4, !tbaa !36
  br label %for.cond172

for.cond172:                                      ; preds = %for.inc181, %for.end171
  %208 = load i32, i32* %i, align 4, !tbaa !36
  %cmp173 = icmp slt i32 %208, 3
  br i1 %cmp173, label %for.body174, label %for.end183

for.body174:                                      ; preds = %for.cond172
  %call175 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %209 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx176 = getelementptr inbounds float, float* %call175, i32 %209
  %210 = load float, float* %arrayidx176, align 4, !tbaa !26
  %fneg177 = fneg float %210
  %211 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2linearAxis178 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %211, i32 0, i32 4
  %212 = load float*, float** %m_J2linearAxis178, align 4, !tbaa !61
  %213 = load i32, i32* %s1, align 4, !tbaa !36
  %214 = load i32, i32* %i, align 4, !tbaa !36
  %add179 = add nsw i32 %213, %214
  %arrayidx180 = getelementptr inbounds float, float* %212, i32 %add179
  store float %fneg177, float* %arrayidx180, align 4, !tbaa !26
  br label %for.inc181

for.inc181:                                       ; preds = %for.body174
  %215 = load i32, i32* %i, align 4, !tbaa !36
  %inc182 = add nsw i32 %215, 1
  store i32 %inc182, i32* %i, align 4, !tbaa !36
  br label %for.cond172

for.end183:                                       ; preds = %for.cond172
  store i32 0, i32* %i, align 4, !tbaa !36
  br label %for.cond184

for.cond184:                                      ; preds = %for.inc193, %for.end183
  %216 = load i32, i32* %i, align 4, !tbaa !36
  %cmp185 = icmp slt i32 %216, 3
  br i1 %cmp185, label %for.body186, label %for.end195

for.body186:                                      ; preds = %for.cond184
  %call187 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %217 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx188 = getelementptr inbounds float, float* %call187, i32 %217
  %218 = load float, float* %arrayidx188, align 4, !tbaa !26
  %fneg189 = fneg float %218
  %219 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2linearAxis190 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %219, i32 0, i32 4
  %220 = load float*, float** %m_J2linearAxis190, align 4, !tbaa !61
  %221 = load i32, i32* %s2, align 4, !tbaa !36
  %222 = load i32, i32* %i, align 4, !tbaa !36
  %add191 = add nsw i32 %221, %222
  %arrayidx192 = getelementptr inbounds float, float* %220, i32 %add191
  store float %fneg189, float* %arrayidx192, align 4, !tbaa !26
  br label %for.inc193

for.inc193:                                       ; preds = %for.body186
  %223 = load i32, i32* %i, align 4, !tbaa !36
  %inc194 = add nsw i32 %223, 1
  store i32 %inc194, i32* %i, align 4, !tbaa !36
  br label %for.cond184

for.end195:                                       ; preds = %for.cond184
  %224 = bitcast float* %rhs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %224) #8
  %225 = load float, float* %k, align 4, !tbaa !26
  %call196 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %p, %class.btVector3* nonnull align 4 dereferenceable(16) %ofs)
  %mul197 = fmul float %225, %call196
  store float %mul197, float* %rhs, align 4, !tbaa !26
  %226 = load float, float* %rhs, align 4, !tbaa !26
  %227 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %227, i32 0, i32 7
  %228 = load float*, float** %m_constraintError, align 4, !tbaa !62
  %229 = load i32, i32* %s0, align 4, !tbaa !36
  %arrayidx198 = getelementptr inbounds float, float* %228, i32 %229
  store float %226, float* %arrayidx198, align 4, !tbaa !26
  %230 = load float, float* %k, align 4, !tbaa !26
  %call199 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %q, %class.btVector3* nonnull align 4 dereferenceable(16) %ofs)
  %mul200 = fmul float %230, %call199
  store float %mul200, float* %rhs, align 4, !tbaa !26
  %231 = load float, float* %rhs, align 4, !tbaa !26
  %232 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError201 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %232, i32 0, i32 7
  %233 = load float*, float** %m_constraintError201, align 4, !tbaa !62
  %234 = load i32, i32* %s1, align 4, !tbaa !36
  %arrayidx202 = getelementptr inbounds float, float* %233, i32 %234
  store float %231, float* %arrayidx202, align 4, !tbaa !26
  %235 = load float, float* %k, align 4, !tbaa !26
  %call203 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ax1, %class.btVector3* nonnull align 4 dereferenceable(16) %ofs)
  %mul204 = fmul float %235, %call203
  store float %mul204, float* %rhs, align 4, !tbaa !26
  %236 = load float, float* %rhs, align 4, !tbaa !26
  %237 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError205 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %237, i32 0, i32 7
  %238 = load float*, float** %m_constraintError205, align 4, !tbaa !62
  %239 = load i32, i32* %s2, align 4, !tbaa !36
  %arrayidx206 = getelementptr inbounds float, float* %238, i32 %239
  store float %236, float* %arrayidx206, align 4, !tbaa !26
  %240 = bitcast float* %rhs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %240) #8
  br label %if.end207

if.end207:                                        ; preds = %for.end195, %for.end125
  %241 = bitcast i32* %s3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %241) #8
  %242 = load i32, i32* %s, align 4, !tbaa !36
  %mul208 = mul nsw i32 3, %242
  store i32 %mul208, i32* %s3, align 4, !tbaa !36
  %243 = bitcast i32* %s4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %243) #8
  %244 = load i32, i32* %s, align 4, !tbaa !36
  %mul209 = mul nsw i32 4, %244
  store i32 %mul209, i32* %s4, align 4, !tbaa !36
  %call210 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx211 = getelementptr inbounds float, float* %call210, i32 0
  %245 = load float, float* %arrayidx211, align 4, !tbaa !26
  %246 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis212 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %246, i32 0, i32 3
  %247 = load float*, float** %m_J1angularAxis212, align 4, !tbaa !56
  %248 = load i32, i32* %s3, align 4, !tbaa !36
  %add213 = add nsw i32 %248, 0
  %arrayidx214 = getelementptr inbounds float, float* %247, i32 %add213
  store float %245, float* %arrayidx214, align 4, !tbaa !26
  %call215 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx216 = getelementptr inbounds float, float* %call215, i32 1
  %249 = load float, float* %arrayidx216, align 4, !tbaa !26
  %250 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis217 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %250, i32 0, i32 3
  %251 = load float*, float** %m_J1angularAxis217, align 4, !tbaa !56
  %252 = load i32, i32* %s3, align 4, !tbaa !36
  %add218 = add nsw i32 %252, 1
  %arrayidx219 = getelementptr inbounds float, float* %251, i32 %add218
  store float %249, float* %arrayidx219, align 4, !tbaa !26
  %call220 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx221 = getelementptr inbounds float, float* %call220, i32 2
  %253 = load float, float* %arrayidx221, align 4, !tbaa !26
  %254 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis222 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %254, i32 0, i32 3
  %255 = load float*, float** %m_J1angularAxis222, align 4, !tbaa !56
  %256 = load i32, i32* %s3, align 4, !tbaa !36
  %add223 = add nsw i32 %256, 2
  %arrayidx224 = getelementptr inbounds float, float* %255, i32 %add223
  store float %253, float* %arrayidx224, align 4, !tbaa !26
  %call225 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx226 = getelementptr inbounds float, float* %call225, i32 0
  %257 = load float, float* %arrayidx226, align 4, !tbaa !26
  %258 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis227 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %258, i32 0, i32 3
  %259 = load float*, float** %m_J1angularAxis227, align 4, !tbaa !56
  %260 = load i32, i32* %s4, align 4, !tbaa !36
  %add228 = add nsw i32 %260, 0
  %arrayidx229 = getelementptr inbounds float, float* %259, i32 %add228
  store float %257, float* %arrayidx229, align 4, !tbaa !26
  %call230 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx231 = getelementptr inbounds float, float* %call230, i32 1
  %261 = load float, float* %arrayidx231, align 4, !tbaa !26
  %262 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis232 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %262, i32 0, i32 3
  %263 = load float*, float** %m_J1angularAxis232, align 4, !tbaa !56
  %264 = load i32, i32* %s4, align 4, !tbaa !36
  %add233 = add nsw i32 %264, 1
  %arrayidx234 = getelementptr inbounds float, float* %263, i32 %add233
  store float %261, float* %arrayidx234, align 4, !tbaa !26
  %call235 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx236 = getelementptr inbounds float, float* %call235, i32 2
  %265 = load float, float* %arrayidx236, align 4, !tbaa !26
  %266 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis237 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %266, i32 0, i32 3
  %267 = load float*, float** %m_J1angularAxis237, align 4, !tbaa !56
  %268 = load i32, i32* %s4, align 4, !tbaa !36
  %add238 = add nsw i32 %268, 2
  %arrayidx239 = getelementptr inbounds float, float* %267, i32 %add238
  store float %265, float* %arrayidx239, align 4, !tbaa !26
  %call240 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx241 = getelementptr inbounds float, float* %call240, i32 0
  %269 = load float, float* %arrayidx241, align 4, !tbaa !26
  %fneg242 = fneg float %269
  %270 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis243 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %270, i32 0, i32 5
  %271 = load float*, float** %m_J2angularAxis243, align 4, !tbaa !57
  %272 = load i32, i32* %s3, align 4, !tbaa !36
  %add244 = add nsw i32 %272, 0
  %arrayidx245 = getelementptr inbounds float, float* %271, i32 %add244
  store float %fneg242, float* %arrayidx245, align 4, !tbaa !26
  %call246 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx247 = getelementptr inbounds float, float* %call246, i32 1
  %273 = load float, float* %arrayidx247, align 4, !tbaa !26
  %fneg248 = fneg float %273
  %274 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis249 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %274, i32 0, i32 5
  %275 = load float*, float** %m_J2angularAxis249, align 4, !tbaa !57
  %276 = load i32, i32* %s3, align 4, !tbaa !36
  %add250 = add nsw i32 %276, 1
  %arrayidx251 = getelementptr inbounds float, float* %275, i32 %add250
  store float %fneg248, float* %arrayidx251, align 4, !tbaa !26
  %call252 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx253 = getelementptr inbounds float, float* %call252, i32 2
  %277 = load float, float* %arrayidx253, align 4, !tbaa !26
  %fneg254 = fneg float %277
  %278 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis255 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %278, i32 0, i32 5
  %279 = load float*, float** %m_J2angularAxis255, align 4, !tbaa !57
  %280 = load i32, i32* %s3, align 4, !tbaa !36
  %add256 = add nsw i32 %280, 2
  %arrayidx257 = getelementptr inbounds float, float* %279, i32 %add256
  store float %fneg254, float* %arrayidx257, align 4, !tbaa !26
  %call258 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx259 = getelementptr inbounds float, float* %call258, i32 0
  %281 = load float, float* %arrayidx259, align 4, !tbaa !26
  %fneg260 = fneg float %281
  %282 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis261 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %282, i32 0, i32 5
  %283 = load float*, float** %m_J2angularAxis261, align 4, !tbaa !57
  %284 = load i32, i32* %s4, align 4, !tbaa !36
  %add262 = add nsw i32 %284, 0
  %arrayidx263 = getelementptr inbounds float, float* %283, i32 %add262
  store float %fneg260, float* %arrayidx263, align 4, !tbaa !26
  %call264 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx265 = getelementptr inbounds float, float* %call264, i32 1
  %285 = load float, float* %arrayidx265, align 4, !tbaa !26
  %fneg266 = fneg float %285
  %286 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis267 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %286, i32 0, i32 5
  %287 = load float*, float** %m_J2angularAxis267, align 4, !tbaa !57
  %288 = load i32, i32* %s4, align 4, !tbaa !36
  %add268 = add nsw i32 %288, 1
  %arrayidx269 = getelementptr inbounds float, float* %287, i32 %add268
  store float %fneg266, float* %arrayidx269, align 4, !tbaa !26
  %call270 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx271 = getelementptr inbounds float, float* %call270, i32 2
  %289 = load float, float* %arrayidx271, align 4, !tbaa !26
  %fneg272 = fneg float %289
  %290 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis273 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %290, i32 0, i32 5
  %291 = load float*, float** %m_J2angularAxis273, align 4, !tbaa !57
  %292 = load i32, i32* %s4, align 4, !tbaa !36
  %add274 = add nsw i32 %292, 2
  %arrayidx275 = getelementptr inbounds float, float* %291, i32 %add274
  store float %fneg272, float* %arrayidx275, align 4, !tbaa !26
  %293 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %fps276 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %293, i32 0, i32 0
  %294 = load float, float* %fps276, align 4, !tbaa !58
  %295 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %erp277 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %295, i32 0, i32 1
  %296 = load float, float* %erp277, align 4, !tbaa !59
  %mul278 = fmul float %294, %296
  store float %mul278, float* %k, align 4, !tbaa !26
  %297 = bitcast %class.btVector3* %u to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %297) #8
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %u, %class.btVector3* %ax1A, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1B)
  %298 = load float, float* %k, align 4, !tbaa !26
  %call279 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %u, %class.btVector3* nonnull align 4 dereferenceable(16) %p)
  %mul280 = fmul float %298, %call279
  %299 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError281 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %299, i32 0, i32 7
  %300 = load float*, float** %m_constraintError281, align 4, !tbaa !62
  %301 = load i32, i32* %s3, align 4, !tbaa !36
  %arrayidx282 = getelementptr inbounds float, float* %300, i32 %301
  store float %mul280, float* %arrayidx282, align 4, !tbaa !26
  %302 = load float, float* %k, align 4, !tbaa !26
  %call283 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %u, %class.btVector3* nonnull align 4 dereferenceable(16) %q)
  %mul284 = fmul float %302, %call283
  %303 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError285 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %303, i32 0, i32 7
  %304 = load float*, float** %m_constraintError285, align 4, !tbaa !62
  %305 = load i32, i32* %s4, align 4, !tbaa !36
  %arrayidx286 = getelementptr inbounds float, float* %304, i32 %305
  store float %mul284, float* %arrayidx286, align 4, !tbaa !26
  store i32 4, i32* %nrow, align 4, !tbaa !36
  %306 = bitcast i32* %srow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %306) #8
  %307 = bitcast float* %limit_err to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %307) #8
  store float 0.000000e+00, float* %limit_err, align 4, !tbaa !26
  %308 = bitcast i32* %limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %308) #8
  store i32 0, i32* %limit, align 4, !tbaa !36
  %call287 = call i32 @_ZN17btHingeConstraint13getSolveLimitEv(%class.btHingeConstraint* %this1)
  %tobool288 = icmp ne i32 %call287, 0
  br i1 %tobool288, label %if.then289, label %if.end293

if.then289:                                       ; preds = %if.end207
  %m_limit = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call290 = call float @_ZNK14btAngularLimit13getCorrectionEv(%class.btAngularLimit* %m_limit)
  %m_referenceSign = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 11
  %309 = load float, float* %m_referenceSign, align 4, !tbaa !27
  %mul291 = fmul float %call290, %309
  store float %mul291, float* %limit_err, align 4, !tbaa !26
  %310 = load float, float* %limit_err, align 4, !tbaa !26
  %cmp292 = fcmp ogt float %310, 0.000000e+00
  %311 = zext i1 %cmp292 to i64
  %cond = select i1 %cmp292, i32 1, i32 2
  store i32 %cond, i32* %limit, align 4, !tbaa !36
  br label %if.end293

if.end293:                                        ; preds = %if.then289, %if.end207
  %312 = bitcast i32* %powered to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %312) #8
  store i32 0, i32* %powered, align 4, !tbaa !36
  %call294 = call zeroext i1 @_ZN17btHingeConstraint21getEnableAngularMotorEv(%class.btHingeConstraint* %this1)
  br i1 %call294, label %if.then295, label %if.end296

if.then295:                                       ; preds = %if.end293
  store i32 1, i32* %powered, align 4, !tbaa !36
  br label %if.end296

if.end296:                                        ; preds = %if.then295, %if.end293
  %313 = load i32, i32* %limit, align 4, !tbaa !36
  %tobool297 = icmp ne i32 %313, 0
  br i1 %tobool297, label %if.then299, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end296
  %314 = load i32, i32* %powered, align 4, !tbaa !36
  %tobool298 = icmp ne i32 %314, 0
  br i1 %tobool298, label %if.then299, label %if.end449

if.then299:                                       ; preds = %lor.lhs.false, %if.end296
  %315 = load i32, i32* %nrow, align 4, !tbaa !36
  %inc300 = add nsw i32 %315, 1
  store i32 %inc300, i32* %nrow, align 4, !tbaa !36
  %316 = load i32, i32* %nrow, align 4, !tbaa !36
  %317 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip301 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %317, i32 0, i32 6
  %318 = load i32, i32* %rowskip301, align 4, !tbaa !54
  %mul302 = mul nsw i32 %316, %318
  store i32 %mul302, i32* %srow, align 4, !tbaa !36
  %call303 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx304 = getelementptr inbounds float, float* %call303, i32 0
  %319 = load float, float* %arrayidx304, align 4, !tbaa !26
  %320 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis305 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %320, i32 0, i32 3
  %321 = load float*, float** %m_J1angularAxis305, align 4, !tbaa !56
  %322 = load i32, i32* %srow, align 4, !tbaa !36
  %add306 = add nsw i32 %322, 0
  %arrayidx307 = getelementptr inbounds float, float* %321, i32 %add306
  store float %319, float* %arrayidx307, align 4, !tbaa !26
  %call308 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx309 = getelementptr inbounds float, float* %call308, i32 1
  %323 = load float, float* %arrayidx309, align 4, !tbaa !26
  %324 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis310 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %324, i32 0, i32 3
  %325 = load float*, float** %m_J1angularAxis310, align 4, !tbaa !56
  %326 = load i32, i32* %srow, align 4, !tbaa !36
  %add311 = add nsw i32 %326, 1
  %arrayidx312 = getelementptr inbounds float, float* %325, i32 %add311
  store float %323, float* %arrayidx312, align 4, !tbaa !26
  %call313 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx314 = getelementptr inbounds float, float* %call313, i32 2
  %327 = load float, float* %arrayidx314, align 4, !tbaa !26
  %328 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis315 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %328, i32 0, i32 3
  %329 = load float*, float** %m_J1angularAxis315, align 4, !tbaa !56
  %330 = load i32, i32* %srow, align 4, !tbaa !36
  %add316 = add nsw i32 %330, 2
  %arrayidx317 = getelementptr inbounds float, float* %329, i32 %add316
  store float %327, float* %arrayidx317, align 4, !tbaa !26
  %call318 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx319 = getelementptr inbounds float, float* %call318, i32 0
  %331 = load float, float* %arrayidx319, align 4, !tbaa !26
  %fneg320 = fneg float %331
  %332 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis321 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %332, i32 0, i32 5
  %333 = load float*, float** %m_J2angularAxis321, align 4, !tbaa !57
  %334 = load i32, i32* %srow, align 4, !tbaa !36
  %add322 = add nsw i32 %334, 0
  %arrayidx323 = getelementptr inbounds float, float* %333, i32 %add322
  store float %fneg320, float* %arrayidx323, align 4, !tbaa !26
  %call324 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx325 = getelementptr inbounds float, float* %call324, i32 1
  %335 = load float, float* %arrayidx325, align 4, !tbaa !26
  %fneg326 = fneg float %335
  %336 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis327 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %336, i32 0, i32 5
  %337 = load float*, float** %m_J2angularAxis327, align 4, !tbaa !57
  %338 = load i32, i32* %srow, align 4, !tbaa !36
  %add328 = add nsw i32 %338, 1
  %arrayidx329 = getelementptr inbounds float, float* %337, i32 %add328
  store float %fneg326, float* %arrayidx329, align 4, !tbaa !26
  %call330 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx331 = getelementptr inbounds float, float* %call330, i32 2
  %339 = load float, float* %arrayidx331, align 4, !tbaa !26
  %fneg332 = fneg float %339
  %340 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis333 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %340, i32 0, i32 5
  %341 = load float*, float** %m_J2angularAxis333, align 4, !tbaa !57
  %342 = load i32, i32* %srow, align 4, !tbaa !36
  %add334 = add nsw i32 %342, 2
  %arrayidx335 = getelementptr inbounds float, float* %341, i32 %add334
  store float %fneg332, float* %arrayidx335, align 4, !tbaa !26
  %343 = bitcast float* %lostop to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %343) #8
  %call336 = call float @_ZNK17btHingeConstraint13getLowerLimitEv(%class.btHingeConstraint* %this1)
  store float %call336, float* %lostop, align 4, !tbaa !26
  %344 = bitcast float* %histop to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %344) #8
  %call337 = call float @_ZNK17btHingeConstraint13getUpperLimitEv(%class.btHingeConstraint* %this1)
  store float %call337, float* %histop, align 4, !tbaa !26
  %345 = load i32, i32* %limit, align 4, !tbaa !36
  %tobool338 = icmp ne i32 %345, 0
  br i1 %tobool338, label %land.lhs.true339, label %if.end342

land.lhs.true339:                                 ; preds = %if.then299
  %346 = load float, float* %lostop, align 4, !tbaa !26
  %347 = load float, float* %histop, align 4, !tbaa !26
  %cmp340 = fcmp oeq float %346, %347
  br i1 %cmp340, label %if.then341, label %if.end342

if.then341:                                       ; preds = %land.lhs.true339
  store i32 0, i32* %powered, align 4, !tbaa !36
  br label %if.end342

if.end342:                                        ; preds = %if.then341, %land.lhs.true339, %if.then299
  %348 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError343 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %348, i32 0, i32 7
  %349 = load float*, float** %m_constraintError343, align 4, !tbaa !62
  %350 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx344 = getelementptr inbounds float, float* %349, i32 %350
  store float 0.000000e+00, float* %arrayidx344, align 4, !tbaa !26
  %351 = bitcast float* %currERP to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %351) #8
  %m_flags = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 18
  %352 = load i32, i32* %m_flags, align 4, !tbaa !23
  %and = and i32 %352, 2
  %tobool345 = icmp ne i32 %and, 0
  br i1 %tobool345, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end342
  %m_stopERP = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 21
  %353 = load float, float* %m_stopERP, align 4, !tbaa !63
  br label %cond.end

cond.false:                                       ; preds = %if.end342
  %354 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %erp346 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %354, i32 0, i32 1
  %355 = load float, float* %erp346, align 4, !tbaa !59
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond347 = phi float [ %353, %cond.true ], [ %355, %cond.false ]
  store float %cond347, float* %currERP, align 4, !tbaa !26
  %356 = load i32, i32* %powered, align 4, !tbaa !36
  %tobool348 = icmp ne i32 %356, 0
  br i1 %tobool348, label %if.then349, label %if.end370

if.then349:                                       ; preds = %cond.end
  %m_flags350 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 18
  %357 = load i32, i32* %m_flags350, align 4, !tbaa !23
  %and351 = and i32 %357, 4
  %tobool352 = icmp ne i32 %and351, 0
  br i1 %tobool352, label %if.then353, label %if.end355

if.then353:                                       ; preds = %if.then349
  %m_normalCFM = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 19
  %358 = load float, float* %m_normalCFM, align 4, !tbaa !64
  %359 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %cfm = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %359, i32 0, i32 8
  %360 = load float*, float** %cfm, align 4, !tbaa !65
  %361 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx354 = getelementptr inbounds float, float* %360, i32 %361
  store float %358, float* %arrayidx354, align 4, !tbaa !26
  br label %if.end355

if.end355:                                        ; preds = %if.then353, %if.then349
  %362 = bitcast float* %mot_fact to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %362) #8
  %363 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_hingeAngle = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 10
  %364 = load float, float* %m_hingeAngle, align 4, !tbaa !50
  %365 = load float, float* %lostop, align 4, !tbaa !26
  %366 = load float, float* %histop, align 4, !tbaa !26
  %m_motorTargetVelocity = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 5
  %367 = load float, float* %m_motorTargetVelocity, align 4, !tbaa !66
  %368 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %fps356 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %368, i32 0, i32 0
  %369 = load float, float* %fps356, align 4, !tbaa !58
  %370 = load float, float* %currERP, align 4, !tbaa !26
  %mul357 = fmul float %369, %370
  %call358 = call float @_ZN17btTypedConstraint14getMotorFactorEfffff(%class.btTypedConstraint* %363, float %364, float %365, float %366, float %367, float %mul357)
  store float %call358, float* %mot_fact, align 4, !tbaa !26
  %371 = load float, float* %mot_fact, align 4, !tbaa !26
  %m_motorTargetVelocity359 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 5
  %372 = load float, float* %m_motorTargetVelocity359, align 4, !tbaa !66
  %mul360 = fmul float %371, %372
  %m_referenceSign361 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 11
  %373 = load float, float* %m_referenceSign361, align 4, !tbaa !27
  %mul362 = fmul float %mul360, %373
  %374 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError363 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %374, i32 0, i32 7
  %375 = load float*, float** %m_constraintError363, align 4, !tbaa !62
  %376 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx364 = getelementptr inbounds float, float* %375, i32 %376
  %377 = load float, float* %arrayidx364, align 4, !tbaa !26
  %add365 = fadd float %377, %mul362
  store float %add365, float* %arrayidx364, align 4, !tbaa !26
  %m_maxMotorImpulse = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 6
  %378 = load float, float* %m_maxMotorImpulse, align 4, !tbaa !67
  %fneg366 = fneg float %378
  %379 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_lowerLimit = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %379, i32 0, i32 9
  %380 = load float*, float** %m_lowerLimit, align 4, !tbaa !68
  %381 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx367 = getelementptr inbounds float, float* %380, i32 %381
  store float %fneg366, float* %arrayidx367, align 4, !tbaa !26
  %m_maxMotorImpulse368 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 6
  %382 = load float, float* %m_maxMotorImpulse368, align 4, !tbaa !67
  %383 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_upperLimit = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %383, i32 0, i32 10
  %384 = load float*, float** %m_upperLimit, align 4, !tbaa !69
  %385 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx369 = getelementptr inbounds float, float* %384, i32 %385
  store float %382, float* %arrayidx369, align 4, !tbaa !26
  %386 = bitcast float* %mot_fact to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %386) #8
  br label %if.end370

if.end370:                                        ; preds = %if.end355, %cond.end
  %387 = load i32, i32* %limit, align 4, !tbaa !36
  %tobool371 = icmp ne i32 %387, 0
  br i1 %tobool371, label %if.then372, label %if.end448

if.then372:                                       ; preds = %if.end370
  %388 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %fps373 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %388, i32 0, i32 0
  %389 = load float, float* %fps373, align 4, !tbaa !58
  %390 = load float, float* %currERP, align 4, !tbaa !26
  %mul374 = fmul float %389, %390
  store float %mul374, float* %k, align 4, !tbaa !26
  %391 = load float, float* %k, align 4, !tbaa !26
  %392 = load float, float* %limit_err, align 4, !tbaa !26
  %mul375 = fmul float %391, %392
  %393 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError376 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %393, i32 0, i32 7
  %394 = load float*, float** %m_constraintError376, align 4, !tbaa !62
  %395 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx377 = getelementptr inbounds float, float* %394, i32 %395
  %396 = load float, float* %arrayidx377, align 4, !tbaa !26
  %add378 = fadd float %396, %mul375
  store float %add378, float* %arrayidx377, align 4, !tbaa !26
  %m_flags379 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 18
  %397 = load i32, i32* %m_flags379, align 4, !tbaa !23
  %and380 = and i32 %397, 1
  %tobool381 = icmp ne i32 %and380, 0
  br i1 %tobool381, label %if.then382, label %if.end385

if.then382:                                       ; preds = %if.then372
  %m_stopCFM = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 20
  %398 = load float, float* %m_stopCFM, align 4, !tbaa !70
  %399 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %cfm383 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %399, i32 0, i32 8
  %400 = load float*, float** %cfm383, align 4, !tbaa !65
  %401 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx384 = getelementptr inbounds float, float* %400, i32 %401
  store float %398, float* %arrayidx384, align 4, !tbaa !26
  br label %if.end385

if.end385:                                        ; preds = %if.then382, %if.then372
  %402 = load float, float* %lostop, align 4, !tbaa !26
  %403 = load float, float* %histop, align 4, !tbaa !26
  %cmp386 = fcmp oeq float %402, %403
  br i1 %cmp386, label %if.then387, label %if.else392

if.then387:                                       ; preds = %if.end385
  %404 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_lowerLimit388 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %404, i32 0, i32 9
  %405 = load float*, float** %m_lowerLimit388, align 4, !tbaa !68
  %406 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx389 = getelementptr inbounds float, float* %405, i32 %406
  store float 0xC7EFFFFFE0000000, float* %arrayidx389, align 4, !tbaa !26
  %407 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_upperLimit390 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %407, i32 0, i32 10
  %408 = load float*, float** %m_upperLimit390, align 4, !tbaa !69
  %409 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx391 = getelementptr inbounds float, float* %408, i32 %409
  store float 0x47EFFFFFE0000000, float* %arrayidx391, align 4, !tbaa !26
  br label %if.end405

if.else392:                                       ; preds = %if.end385
  %410 = load i32, i32* %limit, align 4, !tbaa !36
  %cmp393 = icmp eq i32 %410, 1
  br i1 %cmp393, label %if.then394, label %if.else399

if.then394:                                       ; preds = %if.else392
  %411 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_lowerLimit395 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %411, i32 0, i32 9
  %412 = load float*, float** %m_lowerLimit395, align 4, !tbaa !68
  %413 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx396 = getelementptr inbounds float, float* %412, i32 %413
  store float 0.000000e+00, float* %arrayidx396, align 4, !tbaa !26
  %414 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_upperLimit397 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %414, i32 0, i32 10
  %415 = load float*, float** %m_upperLimit397, align 4, !tbaa !69
  %416 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx398 = getelementptr inbounds float, float* %415, i32 %416
  store float 0x47EFFFFFE0000000, float* %arrayidx398, align 4, !tbaa !26
  br label %if.end404

if.else399:                                       ; preds = %if.else392
  %417 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_lowerLimit400 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %417, i32 0, i32 9
  %418 = load float*, float** %m_lowerLimit400, align 4, !tbaa !68
  %419 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx401 = getelementptr inbounds float, float* %418, i32 %419
  store float 0xC7EFFFFFE0000000, float* %arrayidx401, align 4, !tbaa !26
  %420 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_upperLimit402 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %420, i32 0, i32 10
  %421 = load float*, float** %m_upperLimit402, align 4, !tbaa !69
  %422 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx403 = getelementptr inbounds float, float* %421, i32 %422
  store float 0.000000e+00, float* %arrayidx403, align 4, !tbaa !26
  br label %if.end404

if.end404:                                        ; preds = %if.else399, %if.then394
  br label %if.end405

if.end405:                                        ; preds = %if.end404, %if.then387
  %423 = bitcast float* %bounce to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %423) #8
  %m_limit406 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call407 = call float @_ZNK14btAngularLimit19getRelaxationFactorEv(%class.btAngularLimit* %m_limit406)
  store float %call407, float* %bounce, align 4, !tbaa !26
  %424 = load float, float* %bounce, align 4, !tbaa !26
  %cmp408 = fcmp ogt float %424, 0.000000e+00
  br i1 %cmp408, label %if.then409, label %if.end442

if.then409:                                       ; preds = %if.end405
  %425 = bitcast float* %vel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %425) #8
  %426 = load %class.btVector3*, %class.btVector3** %angVelA.addr, align 4, !tbaa !2
  %call410 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %426, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1)
  store float %call410, float* %vel, align 4, !tbaa !26
  %427 = load %class.btVector3*, %class.btVector3** %angVelB.addr, align 4, !tbaa !2
  %call411 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %427, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1)
  %428 = load float, float* %vel, align 4, !tbaa !26
  %sub412 = fsub float %428, %call411
  store float %sub412, float* %vel, align 4, !tbaa !26
  %429 = load i32, i32* %limit, align 4, !tbaa !36
  %cmp413 = icmp eq i32 %429, 1
  br i1 %cmp413, label %if.then414, label %if.else427

if.then414:                                       ; preds = %if.then409
  %430 = load float, float* %vel, align 4, !tbaa !26
  %cmp415 = fcmp olt float %430, 0.000000e+00
  br i1 %cmp415, label %if.then416, label %if.end426

if.then416:                                       ; preds = %if.then414
  %431 = bitcast float* %newc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %431) #8
  %432 = load float, float* %bounce, align 4, !tbaa !26
  %fneg417 = fneg float %432
  %433 = load float, float* %vel, align 4, !tbaa !26
  %mul418 = fmul float %fneg417, %433
  store float %mul418, float* %newc, align 4, !tbaa !26
  %434 = load float, float* %newc, align 4, !tbaa !26
  %435 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError419 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %435, i32 0, i32 7
  %436 = load float*, float** %m_constraintError419, align 4, !tbaa !62
  %437 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx420 = getelementptr inbounds float, float* %436, i32 %437
  %438 = load float, float* %arrayidx420, align 4, !tbaa !26
  %cmp421 = fcmp ogt float %434, %438
  br i1 %cmp421, label %if.then422, label %if.end425

if.then422:                                       ; preds = %if.then416
  %439 = load float, float* %newc, align 4, !tbaa !26
  %440 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError423 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %440, i32 0, i32 7
  %441 = load float*, float** %m_constraintError423, align 4, !tbaa !62
  %442 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx424 = getelementptr inbounds float, float* %441, i32 %442
  store float %439, float* %arrayidx424, align 4, !tbaa !26
  br label %if.end425

if.end425:                                        ; preds = %if.then422, %if.then416
  %443 = bitcast float* %newc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %443) #8
  br label %if.end426

if.end426:                                        ; preds = %if.end425, %if.then414
  br label %if.end441

if.else427:                                       ; preds = %if.then409
  %444 = load float, float* %vel, align 4, !tbaa !26
  %cmp428 = fcmp ogt float %444, 0.000000e+00
  br i1 %cmp428, label %if.then429, label %if.end440

if.then429:                                       ; preds = %if.else427
  %445 = bitcast float* %newc430 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %445) #8
  %446 = load float, float* %bounce, align 4, !tbaa !26
  %fneg431 = fneg float %446
  %447 = load float, float* %vel, align 4, !tbaa !26
  %mul432 = fmul float %fneg431, %447
  store float %mul432, float* %newc430, align 4, !tbaa !26
  %448 = load float, float* %newc430, align 4, !tbaa !26
  %449 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError433 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %449, i32 0, i32 7
  %450 = load float*, float** %m_constraintError433, align 4, !tbaa !62
  %451 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx434 = getelementptr inbounds float, float* %450, i32 %451
  %452 = load float, float* %arrayidx434, align 4, !tbaa !26
  %cmp435 = fcmp olt float %448, %452
  br i1 %cmp435, label %if.then436, label %if.end439

if.then436:                                       ; preds = %if.then429
  %453 = load float, float* %newc430, align 4, !tbaa !26
  %454 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError437 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %454, i32 0, i32 7
  %455 = load float*, float** %m_constraintError437, align 4, !tbaa !62
  %456 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx438 = getelementptr inbounds float, float* %455, i32 %456
  store float %453, float* %arrayidx438, align 4, !tbaa !26
  br label %if.end439

if.end439:                                        ; preds = %if.then436, %if.then429
  %457 = bitcast float* %newc430 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %457) #8
  br label %if.end440

if.end440:                                        ; preds = %if.end439, %if.else427
  br label %if.end441

if.end441:                                        ; preds = %if.end440, %if.end426
  %458 = bitcast float* %vel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %458) #8
  br label %if.end442

if.end442:                                        ; preds = %if.end441, %if.end405
  %m_limit443 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call444 = call float @_ZNK14btAngularLimit13getBiasFactorEv(%class.btAngularLimit* %m_limit443)
  %459 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError445 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %459, i32 0, i32 7
  %460 = load float*, float** %m_constraintError445, align 4, !tbaa !62
  %461 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx446 = getelementptr inbounds float, float* %460, i32 %461
  %462 = load float, float* %arrayidx446, align 4, !tbaa !26
  %mul447 = fmul float %462, %call444
  store float %mul447, float* %arrayidx446, align 4, !tbaa !26
  %463 = bitcast float* %bounce to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %463) #8
  br label %if.end448

if.end448:                                        ; preds = %if.end442, %if.end370
  %464 = bitcast float* %currERP to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %464) #8
  %465 = bitcast float* %histop to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %465) #8
  %466 = bitcast float* %lostop to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %466) #8
  br label %if.end449

if.end449:                                        ; preds = %if.end448, %lor.lhs.false
  %467 = bitcast i32* %powered to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %467) #8
  %468 = bitcast i32* %limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %468) #8
  %469 = bitcast float* %limit_err to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %469) #8
  %470 = bitcast i32* %srow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %470) #8
  %471 = bitcast %class.btVector3* %u to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %471) #8
  %472 = bitcast i32* %s4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %472) #8
  %473 = bitcast i32* %s3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %473) #8
  %474 = bitcast float* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %474) #8
  %475 = bitcast float* %len2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %475) #8
  %476 = bitcast %class.btVector3* %totalDist to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %476) #8
  %477 = bitcast %class.btVector3* %orthoA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %477) #8
  %478 = bitcast %class.btVector3* %projA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %478) #8
  %479 = bitcast %class.btVector3* %orthoB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %479) #8
  %480 = bitcast %class.btVector3* %projB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %480) #8
  %481 = bitcast %class.btVector3* %q to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %481) #8
  %482 = bitcast %class.btVector3* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %482) #8
  %483 = bitcast %class.btVector3* %relB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %483) #8
  %484 = bitcast %class.btVector3* %relA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %484) #8
  %485 = bitcast %class.btVector3* %tmpB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %485) #8
  %486 = bitcast %class.btVector3* %tmpA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %486) #8
  %487 = bitcast i32* %nrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %487) #8
  %488 = bitcast i32* %s2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %488) #8
  %489 = bitcast i32* %s1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %489) #8
  %490 = bitcast i32* %s0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %490) #8
  %491 = bitcast %class.btTransform* %bodyB_trans to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %491) #8
  %492 = bitcast %class.btTransform* %bodyA_trans to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %492) #8
  %493 = bitcast %class.btVector3* %ax1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %493) #8
  %494 = bitcast %class.btVector3* %ax1B to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %494) #8
  %495 = bitcast %class.btVector3* %ax1A to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %495) #8
  %496 = bitcast float* %factB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %496) #8
  %497 = bitcast float* %factA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %497) #8
  %498 = bitcast float* %miS to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %498) #8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %hasStaticBody) #8
  %499 = bitcast float* %miB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %499) #8
  %500 = bitcast float* %miA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %500) #8
  %501 = bitcast %class.btVector3* %ofs to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %501) #8
  %502 = bitcast %class.btTransform* %trB to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %502) #8
  %503 = bitcast %class.btTransform* %trA to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %503) #8
  %504 = bitcast i32* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %504) #8
  %505 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %505) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  ret %class.btVector3* %m_angularVelocity
}

define hidden void @_ZN17btHingeConstraint16getInfo2InternalEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_(%class.btHingeConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo2"* %info, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelA, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelB) #0 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo2"*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %angVelA.addr = alloca %class.btVector3*, align 4
  %angVelB.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  %skip = alloca i32, align 4
  %trA = alloca %class.btTransform, align 4
  %trB = alloca %class.btTransform, align 4
  %pivotAInW = alloca %class.btVector3, align 4
  %pivotBInW = alloca %class.btVector3, align 4
  %a1 = alloca %class.btVector3, align 4
  %angular0 = alloca %class.btVector3*, align 4
  %angular1 = alloca %class.btVector3*, align 4
  %angular2 = alloca %class.btVector3*, align 4
  %a1neg = alloca %class.btVector3, align 4
  %a2 = alloca %class.btVector3, align 4
  %angular022 = alloca %class.btVector3*, align 4
  %angular123 = alloca %class.btVector3*, align 4
  %angular226 = alloca %class.btVector3*, align 4
  %k = alloca float, align 4
  %ax1 = alloca %class.btVector3, align 4
  %p = alloca %class.btVector3, align 4
  %q = alloca %class.btVector3, align 4
  %s3 = alloca i32, align 4
  %s4 = alloca i32, align 4
  %ax2 = alloca %class.btVector3, align 4
  %u = alloca %class.btVector3, align 4
  %nrow = alloca i32, align 4
  %srow = alloca i32, align 4
  %limit_err = alloca float, align 4
  %limit = alloca i32, align 4
  %powered = alloca i32, align 4
  %lostop = alloca float, align 4
  %histop = alloca float, align 4
  %currERP = alloca float, align 4
  %mot_fact = alloca float, align 4
  %bounce = alloca float, align 4
  %vel = alloca float, align 4
  %newc = alloca float, align 4
  %newc264 = alloca float, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4, !tbaa !2
  store %"struct.btTypedConstraint::btConstraintInfo2"* %info, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4, !tbaa !2
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4, !tbaa !2
  store %class.btVector3* %angVelA, %class.btVector3** %angVelA.addr, align 4, !tbaa !2
  store %class.btVector3* %angVelB, %class.btVector3** %angVelB.addr, align 4, !tbaa !2
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = bitcast i32* %skip to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %2, i32 0, i32 6
  %3 = load i32, i32* %rowskip, align 4, !tbaa !54
  store i32 %3, i32* %skip, align 4, !tbaa !36
  %4 = bitcast %class.btTransform* %trA to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %4) #8
  %5 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %m_rbAFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %trA, %class.btTransform* %5, %class.btTransform* nonnull align 4 dereferenceable(64) %m_rbAFrame)
  %6 = bitcast %class.btTransform* %trB to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %6) #8
  %7 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %m_rbBFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %trB, %class.btTransform* %7, %class.btTransform* nonnull align 4 dereferenceable(64) %m_rbBFrame)
  %8 = bitcast %class.btVector3* %pivotAInW to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #8
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %trA)
  %9 = bitcast %class.btVector3* %pivotAInW to i8*
  %10 = bitcast %class.btVector3* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false), !tbaa.struct !24
  %11 = bitcast %class.btVector3* %pivotBInW to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #8
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %trB)
  %12 = bitcast %class.btVector3* %pivotBInW to i8*
  %13 = bitcast %class.btVector3* %call2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false), !tbaa.struct !24
  %m_angularOnly = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 12
  %14 = load i8, i8* %m_angularOnly, align 4, !tbaa !10, !range !21
  %tobool = trunc i8 %14 to i1
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %15 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1linearAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %15, i32 0, i32 2
  %16 = load float*, float** %m_J1linearAxis, align 4, !tbaa !60
  %arrayidx = getelementptr inbounds float, float* %16, i32 0
  store float 1.000000e+00, float* %arrayidx, align 4, !tbaa !26
  %17 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1linearAxis3 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %17, i32 0, i32 2
  %18 = load float*, float** %m_J1linearAxis3, align 4, !tbaa !60
  %19 = load i32, i32* %skip, align 4, !tbaa !36
  %add = add nsw i32 %19, 1
  %arrayidx4 = getelementptr inbounds float, float* %18, i32 %add
  store float 1.000000e+00, float* %arrayidx4, align 4, !tbaa !26
  %20 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1linearAxis5 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %20, i32 0, i32 2
  %21 = load float*, float** %m_J1linearAxis5, align 4, !tbaa !60
  %22 = load i32, i32* %skip, align 4, !tbaa !36
  %mul = mul nsw i32 2, %22
  %add6 = add nsw i32 %mul, 2
  %arrayidx7 = getelementptr inbounds float, float* %21, i32 %add6
  store float 1.000000e+00, float* %arrayidx7, align 4, !tbaa !26
  %23 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2linearAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %23, i32 0, i32 4
  %24 = load float*, float** %m_J2linearAxis, align 4, !tbaa !61
  %arrayidx8 = getelementptr inbounds float, float* %24, i32 0
  store float -1.000000e+00, float* %arrayidx8, align 4, !tbaa !26
  %25 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2linearAxis9 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %25, i32 0, i32 4
  %26 = load float*, float** %m_J2linearAxis9, align 4, !tbaa !61
  %27 = load i32, i32* %skip, align 4, !tbaa !36
  %add10 = add nsw i32 %27, 1
  %arrayidx11 = getelementptr inbounds float, float* %26, i32 %add10
  store float -1.000000e+00, float* %arrayidx11, align 4, !tbaa !26
  %28 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2linearAxis12 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %28, i32 0, i32 4
  %29 = load float*, float** %m_J2linearAxis12, align 4, !tbaa !61
  %30 = load i32, i32* %skip, align 4, !tbaa !36
  %mul13 = mul nsw i32 2, %30
  %add14 = add nsw i32 %mul13, 2
  %arrayidx15 = getelementptr inbounds float, float* %29, i32 %add14
  store float -1.000000e+00, float* %arrayidx15, align 4, !tbaa !26
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %31 = bitcast %class.btVector3* %a1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %31) #8
  %32 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %call16 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %32)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %a1, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotAInW, %class.btVector3* nonnull align 4 dereferenceable(16) %call16)
  %33 = bitcast %class.btVector3** %angular0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #8
  %34 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %34, i32 0, i32 3
  %35 = load float*, float** %m_J1angularAxis, align 4, !tbaa !56
  %36 = bitcast float* %35 to %class.btVector3*
  store %class.btVector3* %36, %class.btVector3** %angular0, align 4, !tbaa !2
  %37 = bitcast %class.btVector3** %angular1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #8
  %38 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis17 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %38, i32 0, i32 3
  %39 = load float*, float** %m_J1angularAxis17, align 4, !tbaa !56
  %40 = load i32, i32* %skip, align 4, !tbaa !36
  %add.ptr = getelementptr inbounds float, float* %39, i32 %40
  %41 = bitcast float* %add.ptr to %class.btVector3*
  store %class.btVector3* %41, %class.btVector3** %angular1, align 4, !tbaa !2
  %42 = bitcast %class.btVector3** %angular2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #8
  %43 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis18 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %43, i32 0, i32 3
  %44 = load float*, float** %m_J1angularAxis18, align 4, !tbaa !56
  %45 = load i32, i32* %skip, align 4, !tbaa !36
  %mul19 = mul nsw i32 2, %45
  %add.ptr20 = getelementptr inbounds float, float* %44, i32 %mul19
  %46 = bitcast float* %add.ptr20 to %class.btVector3*
  store %class.btVector3* %46, %class.btVector3** %angular2, align 4, !tbaa !2
  %47 = bitcast %class.btVector3* %a1neg to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %47) #8
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %a1neg, %class.btVector3* nonnull align 4 dereferenceable(16) %a1)
  %48 = load %class.btVector3*, %class.btVector3** %angular0, align 4, !tbaa !2
  %49 = load %class.btVector3*, %class.btVector3** %angular1, align 4, !tbaa !2
  %50 = load %class.btVector3*, %class.btVector3** %angular2, align 4, !tbaa !2
  call void @_ZNK9btVector322getSkewSymmetricMatrixEPS_S0_S0_(%class.btVector3* %a1neg, %class.btVector3* %48, %class.btVector3* %49, %class.btVector3* %50)
  %51 = bitcast %class.btVector3* %a1neg to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %51) #8
  %52 = bitcast %class.btVector3** %angular2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #8
  %53 = bitcast %class.btVector3** %angular1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #8
  %54 = bitcast %class.btVector3** %angular0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #8
  %55 = bitcast %class.btVector3* %a2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %55) #8
  %56 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %56)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %a2, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotBInW, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  %57 = bitcast %class.btVector3** %angular022 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %57) #8
  %58 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %58, i32 0, i32 5
  %59 = load float*, float** %m_J2angularAxis, align 4, !tbaa !57
  %60 = bitcast float* %59 to %class.btVector3*
  store %class.btVector3* %60, %class.btVector3** %angular022, align 4, !tbaa !2
  %61 = bitcast %class.btVector3** %angular123 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %61) #8
  %62 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis24 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %62, i32 0, i32 5
  %63 = load float*, float** %m_J2angularAxis24, align 4, !tbaa !57
  %64 = load i32, i32* %skip, align 4, !tbaa !36
  %add.ptr25 = getelementptr inbounds float, float* %63, i32 %64
  %65 = bitcast float* %add.ptr25 to %class.btVector3*
  store %class.btVector3* %65, %class.btVector3** %angular123, align 4, !tbaa !2
  %66 = bitcast %class.btVector3** %angular226 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #8
  %67 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis27 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %67, i32 0, i32 5
  %68 = load float*, float** %m_J2angularAxis27, align 4, !tbaa !57
  %69 = load i32, i32* %skip, align 4, !tbaa !36
  %mul28 = mul nsw i32 2, %69
  %add.ptr29 = getelementptr inbounds float, float* %68, i32 %mul28
  %70 = bitcast float* %add.ptr29 to %class.btVector3*
  store %class.btVector3* %70, %class.btVector3** %angular226, align 4, !tbaa !2
  %71 = load %class.btVector3*, %class.btVector3** %angular022, align 4, !tbaa !2
  %72 = load %class.btVector3*, %class.btVector3** %angular123, align 4, !tbaa !2
  %73 = load %class.btVector3*, %class.btVector3** %angular226, align 4, !tbaa !2
  call void @_ZNK9btVector322getSkewSymmetricMatrixEPS_S0_S0_(%class.btVector3* %a2, %class.btVector3* %71, %class.btVector3* %72, %class.btVector3* %73)
  %74 = bitcast %class.btVector3** %angular226 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #8
  %75 = bitcast %class.btVector3** %angular123 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #8
  %76 = bitcast %class.btVector3** %angular022 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #8
  %77 = bitcast float* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %77) #8
  %78 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %fps = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %78, i32 0, i32 0
  %79 = load float, float* %fps, align 4, !tbaa !58
  %80 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %erp = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %80, i32 0, i32 1
  %81 = load float, float* %erp, align 4, !tbaa !59
  %mul30 = fmul float %79, %81
  store float %mul30, float* %k, align 4, !tbaa !26
  %m_angularOnly31 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 12
  %82 = load i8, i8* %m_angularOnly31, align 4, !tbaa !10, !range !21
  %tobool32 = trunc i8 %82 to i1
  br i1 %tobool32, label %if.end41, label %if.then33

if.then33:                                        ; preds = %if.end
  store i32 0, i32* %i, align 4, !tbaa !36
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then33
  %83 = load i32, i32* %i, align 4, !tbaa !36
  %cmp = icmp slt i32 %83, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %84 = load float, float* %k, align 4, !tbaa !26
  %call34 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pivotBInW)
  %85 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 %85
  %86 = load float, float* %arrayidx35, align 4, !tbaa !26
  %call36 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pivotAInW)
  %87 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx37 = getelementptr inbounds float, float* %call36, i32 %87
  %88 = load float, float* %arrayidx37, align 4, !tbaa !26
  %sub = fsub float %86, %88
  %mul38 = fmul float %84, %sub
  %89 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %89, i32 0, i32 7
  %90 = load float*, float** %m_constraintError, align 4, !tbaa !62
  %91 = load i32, i32* %i, align 4, !tbaa !36
  %92 = load i32, i32* %skip, align 4, !tbaa !36
  %mul39 = mul nsw i32 %91, %92
  %arrayidx40 = getelementptr inbounds float, float* %90, i32 %mul39
  store float %mul38, float* %arrayidx40, align 4, !tbaa !26
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %93 = load i32, i32* %i, align 4, !tbaa !36
  %inc = add nsw i32 %93, 1
  store i32 %inc, i32* %i, align 4, !tbaa !36
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end41

if.end41:                                         ; preds = %for.end, %if.end
  %94 = bitcast %class.btVector3* %ax1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %94) #8
  %call42 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %trA)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ax1, %class.btMatrix3x3* %call42, i32 2)
  %95 = bitcast %class.btVector3* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %95) #8
  %call43 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %trA)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %p, %class.btMatrix3x3* %call43, i32 0)
  %96 = bitcast %class.btVector3* %q to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %96) #8
  %call44 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %trA)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %q, %class.btMatrix3x3* %call44, i32 1)
  %97 = bitcast i32* %s3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %97) #8
  %98 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip45 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %98, i32 0, i32 6
  %99 = load i32, i32* %rowskip45, align 4, !tbaa !54
  %mul46 = mul nsw i32 3, %99
  store i32 %mul46, i32* %s3, align 4, !tbaa !36
  %100 = bitcast i32* %s4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %100) #8
  %101 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip47 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %101, i32 0, i32 6
  %102 = load i32, i32* %rowskip47, align 4, !tbaa !54
  %mul48 = mul nsw i32 4, %102
  store i32 %mul48, i32* %s4, align 4, !tbaa !36
  %call49 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx50 = getelementptr inbounds float, float* %call49, i32 0
  %103 = load float, float* %arrayidx50, align 4, !tbaa !26
  %104 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis51 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %104, i32 0, i32 3
  %105 = load float*, float** %m_J1angularAxis51, align 4, !tbaa !56
  %106 = load i32, i32* %s3, align 4, !tbaa !36
  %add52 = add nsw i32 %106, 0
  %arrayidx53 = getelementptr inbounds float, float* %105, i32 %add52
  store float %103, float* %arrayidx53, align 4, !tbaa !26
  %call54 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx55 = getelementptr inbounds float, float* %call54, i32 1
  %107 = load float, float* %arrayidx55, align 4, !tbaa !26
  %108 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis56 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %108, i32 0, i32 3
  %109 = load float*, float** %m_J1angularAxis56, align 4, !tbaa !56
  %110 = load i32, i32* %s3, align 4, !tbaa !36
  %add57 = add nsw i32 %110, 1
  %arrayidx58 = getelementptr inbounds float, float* %109, i32 %add57
  store float %107, float* %arrayidx58, align 4, !tbaa !26
  %call59 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx60 = getelementptr inbounds float, float* %call59, i32 2
  %111 = load float, float* %arrayidx60, align 4, !tbaa !26
  %112 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis61 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %112, i32 0, i32 3
  %113 = load float*, float** %m_J1angularAxis61, align 4, !tbaa !56
  %114 = load i32, i32* %s3, align 4, !tbaa !36
  %add62 = add nsw i32 %114, 2
  %arrayidx63 = getelementptr inbounds float, float* %113, i32 %add62
  store float %111, float* %arrayidx63, align 4, !tbaa !26
  %call64 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx65 = getelementptr inbounds float, float* %call64, i32 0
  %115 = load float, float* %arrayidx65, align 4, !tbaa !26
  %116 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis66 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %116, i32 0, i32 3
  %117 = load float*, float** %m_J1angularAxis66, align 4, !tbaa !56
  %118 = load i32, i32* %s4, align 4, !tbaa !36
  %add67 = add nsw i32 %118, 0
  %arrayidx68 = getelementptr inbounds float, float* %117, i32 %add67
  store float %115, float* %arrayidx68, align 4, !tbaa !26
  %call69 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx70 = getelementptr inbounds float, float* %call69, i32 1
  %119 = load float, float* %arrayidx70, align 4, !tbaa !26
  %120 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis71 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %120, i32 0, i32 3
  %121 = load float*, float** %m_J1angularAxis71, align 4, !tbaa !56
  %122 = load i32, i32* %s4, align 4, !tbaa !36
  %add72 = add nsw i32 %122, 1
  %arrayidx73 = getelementptr inbounds float, float* %121, i32 %add72
  store float %119, float* %arrayidx73, align 4, !tbaa !26
  %call74 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx75 = getelementptr inbounds float, float* %call74, i32 2
  %123 = load float, float* %arrayidx75, align 4, !tbaa !26
  %124 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis76 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %124, i32 0, i32 3
  %125 = load float*, float** %m_J1angularAxis76, align 4, !tbaa !56
  %126 = load i32, i32* %s4, align 4, !tbaa !36
  %add77 = add nsw i32 %126, 2
  %arrayidx78 = getelementptr inbounds float, float* %125, i32 %add77
  store float %123, float* %arrayidx78, align 4, !tbaa !26
  %call79 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx80 = getelementptr inbounds float, float* %call79, i32 0
  %127 = load float, float* %arrayidx80, align 4, !tbaa !26
  %fneg = fneg float %127
  %128 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis81 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %128, i32 0, i32 5
  %129 = load float*, float** %m_J2angularAxis81, align 4, !tbaa !57
  %130 = load i32, i32* %s3, align 4, !tbaa !36
  %add82 = add nsw i32 %130, 0
  %arrayidx83 = getelementptr inbounds float, float* %129, i32 %add82
  store float %fneg, float* %arrayidx83, align 4, !tbaa !26
  %call84 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx85 = getelementptr inbounds float, float* %call84, i32 1
  %131 = load float, float* %arrayidx85, align 4, !tbaa !26
  %fneg86 = fneg float %131
  %132 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis87 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %132, i32 0, i32 5
  %133 = load float*, float** %m_J2angularAxis87, align 4, !tbaa !57
  %134 = load i32, i32* %s3, align 4, !tbaa !36
  %add88 = add nsw i32 %134, 1
  %arrayidx89 = getelementptr inbounds float, float* %133, i32 %add88
  store float %fneg86, float* %arrayidx89, align 4, !tbaa !26
  %call90 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx91 = getelementptr inbounds float, float* %call90, i32 2
  %135 = load float, float* %arrayidx91, align 4, !tbaa !26
  %fneg92 = fneg float %135
  %136 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis93 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %136, i32 0, i32 5
  %137 = load float*, float** %m_J2angularAxis93, align 4, !tbaa !57
  %138 = load i32, i32* %s3, align 4, !tbaa !36
  %add94 = add nsw i32 %138, 2
  %arrayidx95 = getelementptr inbounds float, float* %137, i32 %add94
  store float %fneg92, float* %arrayidx95, align 4, !tbaa !26
  %call96 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx97 = getelementptr inbounds float, float* %call96, i32 0
  %139 = load float, float* %arrayidx97, align 4, !tbaa !26
  %fneg98 = fneg float %139
  %140 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis99 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %140, i32 0, i32 5
  %141 = load float*, float** %m_J2angularAxis99, align 4, !tbaa !57
  %142 = load i32, i32* %s4, align 4, !tbaa !36
  %add100 = add nsw i32 %142, 0
  %arrayidx101 = getelementptr inbounds float, float* %141, i32 %add100
  store float %fneg98, float* %arrayidx101, align 4, !tbaa !26
  %call102 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx103 = getelementptr inbounds float, float* %call102, i32 1
  %143 = load float, float* %arrayidx103, align 4, !tbaa !26
  %fneg104 = fneg float %143
  %144 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis105 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %144, i32 0, i32 5
  %145 = load float*, float** %m_J2angularAxis105, align 4, !tbaa !57
  %146 = load i32, i32* %s4, align 4, !tbaa !36
  %add106 = add nsw i32 %146, 1
  %arrayidx107 = getelementptr inbounds float, float* %145, i32 %add106
  store float %fneg104, float* %arrayidx107, align 4, !tbaa !26
  %call108 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx109 = getelementptr inbounds float, float* %call108, i32 2
  %147 = load float, float* %arrayidx109, align 4, !tbaa !26
  %fneg110 = fneg float %147
  %148 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis111 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %148, i32 0, i32 5
  %149 = load float*, float** %m_J2angularAxis111, align 4, !tbaa !57
  %150 = load i32, i32* %s4, align 4, !tbaa !36
  %add112 = add nsw i32 %150, 2
  %arrayidx113 = getelementptr inbounds float, float* %149, i32 %add112
  store float %fneg110, float* %arrayidx113, align 4, !tbaa !26
  %151 = bitcast %class.btVector3* %ax2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %151) #8
  %call114 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %trB)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ax2, %class.btMatrix3x3* %call114, i32 2)
  %152 = bitcast %class.btVector3* %u to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %152) #8
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %u, %class.btVector3* %ax1, %class.btVector3* nonnull align 4 dereferenceable(16) %ax2)
  %153 = load float, float* %k, align 4, !tbaa !26
  %call115 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %u, %class.btVector3* nonnull align 4 dereferenceable(16) %p)
  %mul116 = fmul float %153, %call115
  %154 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError117 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %154, i32 0, i32 7
  %155 = load float*, float** %m_constraintError117, align 4, !tbaa !62
  %156 = load i32, i32* %s3, align 4, !tbaa !36
  %arrayidx118 = getelementptr inbounds float, float* %155, i32 %156
  store float %mul116, float* %arrayidx118, align 4, !tbaa !26
  %157 = load float, float* %k, align 4, !tbaa !26
  %call119 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %u, %class.btVector3* nonnull align 4 dereferenceable(16) %q)
  %mul120 = fmul float %157, %call119
  %158 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError121 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %158, i32 0, i32 7
  %159 = load float*, float** %m_constraintError121, align 4, !tbaa !62
  %160 = load i32, i32* %s4, align 4, !tbaa !36
  %arrayidx122 = getelementptr inbounds float, float* %159, i32 %160
  store float %mul120, float* %arrayidx122, align 4, !tbaa !26
  %161 = bitcast i32* %nrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %161) #8
  store i32 4, i32* %nrow, align 4, !tbaa !36
  %162 = bitcast i32* %srow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %162) #8
  %163 = bitcast float* %limit_err to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %163) #8
  store float 0.000000e+00, float* %limit_err, align 4, !tbaa !26
  %164 = bitcast i32* %limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %164) #8
  store i32 0, i32* %limit, align 4, !tbaa !36
  %call123 = call i32 @_ZN17btHingeConstraint13getSolveLimitEv(%class.btHingeConstraint* %this1)
  %tobool124 = icmp ne i32 %call123, 0
  br i1 %tobool124, label %if.then125, label %if.end129

if.then125:                                       ; preds = %if.end41
  %m_limit = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call126 = call float @_ZNK14btAngularLimit13getCorrectionEv(%class.btAngularLimit* %m_limit)
  %m_referenceSign = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 11
  %165 = load float, float* %m_referenceSign, align 4, !tbaa !27
  %mul127 = fmul float %call126, %165
  store float %mul127, float* %limit_err, align 4, !tbaa !26
  %166 = load float, float* %limit_err, align 4, !tbaa !26
  %cmp128 = fcmp ogt float %166, 0.000000e+00
  %167 = zext i1 %cmp128 to i64
  %cond = select i1 %cmp128, i32 1, i32 2
  store i32 %cond, i32* %limit, align 4, !tbaa !36
  br label %if.end129

if.end129:                                        ; preds = %if.then125, %if.end41
  %168 = bitcast i32* %powered to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %168) #8
  store i32 0, i32* %powered, align 4, !tbaa !36
  %call130 = call zeroext i1 @_ZN17btHingeConstraint21getEnableAngularMotorEv(%class.btHingeConstraint* %this1)
  br i1 %call130, label %if.then131, label %if.end132

if.then131:                                       ; preds = %if.end129
  store i32 1, i32* %powered, align 4, !tbaa !36
  br label %if.end132

if.end132:                                        ; preds = %if.then131, %if.end129
  %169 = load i32, i32* %limit, align 4, !tbaa !36
  %tobool133 = icmp ne i32 %169, 0
  br i1 %tobool133, label %if.then135, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end132
  %170 = load i32, i32* %powered, align 4, !tbaa !36
  %tobool134 = icmp ne i32 %170, 0
  br i1 %tobool134, label %if.then135, label %if.end283

if.then135:                                       ; preds = %lor.lhs.false, %if.end132
  %171 = load i32, i32* %nrow, align 4, !tbaa !36
  %inc136 = add nsw i32 %171, 1
  store i32 %inc136, i32* %nrow, align 4, !tbaa !36
  %172 = load i32, i32* %nrow, align 4, !tbaa !36
  %173 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip137 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %173, i32 0, i32 6
  %174 = load i32, i32* %rowskip137, align 4, !tbaa !54
  %mul138 = mul nsw i32 %172, %174
  store i32 %mul138, i32* %srow, align 4, !tbaa !36
  %call139 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx140 = getelementptr inbounds float, float* %call139, i32 0
  %175 = load float, float* %arrayidx140, align 4, !tbaa !26
  %176 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis141 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %176, i32 0, i32 3
  %177 = load float*, float** %m_J1angularAxis141, align 4, !tbaa !56
  %178 = load i32, i32* %srow, align 4, !tbaa !36
  %add142 = add nsw i32 %178, 0
  %arrayidx143 = getelementptr inbounds float, float* %177, i32 %add142
  store float %175, float* %arrayidx143, align 4, !tbaa !26
  %call144 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx145 = getelementptr inbounds float, float* %call144, i32 1
  %179 = load float, float* %arrayidx145, align 4, !tbaa !26
  %180 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis146 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %180, i32 0, i32 3
  %181 = load float*, float** %m_J1angularAxis146, align 4, !tbaa !56
  %182 = load i32, i32* %srow, align 4, !tbaa !36
  %add147 = add nsw i32 %182, 1
  %arrayidx148 = getelementptr inbounds float, float* %181, i32 %add147
  store float %179, float* %arrayidx148, align 4, !tbaa !26
  %call149 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx150 = getelementptr inbounds float, float* %call149, i32 2
  %183 = load float, float* %arrayidx150, align 4, !tbaa !26
  %184 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis151 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %184, i32 0, i32 3
  %185 = load float*, float** %m_J1angularAxis151, align 4, !tbaa !56
  %186 = load i32, i32* %srow, align 4, !tbaa !36
  %add152 = add nsw i32 %186, 2
  %arrayidx153 = getelementptr inbounds float, float* %185, i32 %add152
  store float %183, float* %arrayidx153, align 4, !tbaa !26
  %call154 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx155 = getelementptr inbounds float, float* %call154, i32 0
  %187 = load float, float* %arrayidx155, align 4, !tbaa !26
  %fneg156 = fneg float %187
  %188 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis157 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %188, i32 0, i32 5
  %189 = load float*, float** %m_J2angularAxis157, align 4, !tbaa !57
  %190 = load i32, i32* %srow, align 4, !tbaa !36
  %add158 = add nsw i32 %190, 0
  %arrayidx159 = getelementptr inbounds float, float* %189, i32 %add158
  store float %fneg156, float* %arrayidx159, align 4, !tbaa !26
  %call160 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx161 = getelementptr inbounds float, float* %call160, i32 1
  %191 = load float, float* %arrayidx161, align 4, !tbaa !26
  %fneg162 = fneg float %191
  %192 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis163 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %192, i32 0, i32 5
  %193 = load float*, float** %m_J2angularAxis163, align 4, !tbaa !57
  %194 = load i32, i32* %srow, align 4, !tbaa !36
  %add164 = add nsw i32 %194, 1
  %arrayidx165 = getelementptr inbounds float, float* %193, i32 %add164
  store float %fneg162, float* %arrayidx165, align 4, !tbaa !26
  %call166 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx167 = getelementptr inbounds float, float* %call166, i32 2
  %195 = load float, float* %arrayidx167, align 4, !tbaa !26
  %fneg168 = fneg float %195
  %196 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis169 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %196, i32 0, i32 5
  %197 = load float*, float** %m_J2angularAxis169, align 4, !tbaa !57
  %198 = load i32, i32* %srow, align 4, !tbaa !36
  %add170 = add nsw i32 %198, 2
  %arrayidx171 = getelementptr inbounds float, float* %197, i32 %add170
  store float %fneg168, float* %arrayidx171, align 4, !tbaa !26
  %199 = bitcast float* %lostop to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %199) #8
  %call172 = call float @_ZNK17btHingeConstraint13getLowerLimitEv(%class.btHingeConstraint* %this1)
  store float %call172, float* %lostop, align 4, !tbaa !26
  %200 = bitcast float* %histop to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %200) #8
  %call173 = call float @_ZNK17btHingeConstraint13getUpperLimitEv(%class.btHingeConstraint* %this1)
  store float %call173, float* %histop, align 4, !tbaa !26
  %201 = load i32, i32* %limit, align 4, !tbaa !36
  %tobool174 = icmp ne i32 %201, 0
  br i1 %tobool174, label %land.lhs.true, label %if.end177

land.lhs.true:                                    ; preds = %if.then135
  %202 = load float, float* %lostop, align 4, !tbaa !26
  %203 = load float, float* %histop, align 4, !tbaa !26
  %cmp175 = fcmp oeq float %202, %203
  br i1 %cmp175, label %if.then176, label %if.end177

if.then176:                                       ; preds = %land.lhs.true
  store i32 0, i32* %powered, align 4, !tbaa !36
  br label %if.end177

if.end177:                                        ; preds = %if.then176, %land.lhs.true, %if.then135
  %204 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError178 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %204, i32 0, i32 7
  %205 = load float*, float** %m_constraintError178, align 4, !tbaa !62
  %206 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx179 = getelementptr inbounds float, float* %205, i32 %206
  store float 0.000000e+00, float* %arrayidx179, align 4, !tbaa !26
  %207 = bitcast float* %currERP to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %207) #8
  %m_flags = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 18
  %208 = load i32, i32* %m_flags, align 4, !tbaa !23
  %and = and i32 %208, 2
  %tobool180 = icmp ne i32 %and, 0
  br i1 %tobool180, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end177
  %m_stopERP = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 21
  %209 = load float, float* %m_stopERP, align 4, !tbaa !63
  br label %cond.end

cond.false:                                       ; preds = %if.end177
  %210 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %erp181 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %210, i32 0, i32 1
  %211 = load float, float* %erp181, align 4, !tbaa !59
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond182 = phi float [ %209, %cond.true ], [ %211, %cond.false ]
  store float %cond182, float* %currERP, align 4, !tbaa !26
  %212 = load i32, i32* %powered, align 4, !tbaa !36
  %tobool183 = icmp ne i32 %212, 0
  br i1 %tobool183, label %if.then184, label %if.end205

if.then184:                                       ; preds = %cond.end
  %m_flags185 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 18
  %213 = load i32, i32* %m_flags185, align 4, !tbaa !23
  %and186 = and i32 %213, 4
  %tobool187 = icmp ne i32 %and186, 0
  br i1 %tobool187, label %if.then188, label %if.end190

if.then188:                                       ; preds = %if.then184
  %m_normalCFM = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 19
  %214 = load float, float* %m_normalCFM, align 4, !tbaa !64
  %215 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %cfm = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %215, i32 0, i32 8
  %216 = load float*, float** %cfm, align 4, !tbaa !65
  %217 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx189 = getelementptr inbounds float, float* %216, i32 %217
  store float %214, float* %arrayidx189, align 4, !tbaa !26
  br label %if.end190

if.end190:                                        ; preds = %if.then188, %if.then184
  %218 = bitcast float* %mot_fact to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %218) #8
  %219 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_hingeAngle = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 10
  %220 = load float, float* %m_hingeAngle, align 4, !tbaa !50
  %221 = load float, float* %lostop, align 4, !tbaa !26
  %222 = load float, float* %histop, align 4, !tbaa !26
  %m_motorTargetVelocity = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 5
  %223 = load float, float* %m_motorTargetVelocity, align 4, !tbaa !66
  %224 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %fps191 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %224, i32 0, i32 0
  %225 = load float, float* %fps191, align 4, !tbaa !58
  %226 = load float, float* %currERP, align 4, !tbaa !26
  %mul192 = fmul float %225, %226
  %call193 = call float @_ZN17btTypedConstraint14getMotorFactorEfffff(%class.btTypedConstraint* %219, float %220, float %221, float %222, float %223, float %mul192)
  store float %call193, float* %mot_fact, align 4, !tbaa !26
  %227 = load float, float* %mot_fact, align 4, !tbaa !26
  %m_motorTargetVelocity194 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 5
  %228 = load float, float* %m_motorTargetVelocity194, align 4, !tbaa !66
  %mul195 = fmul float %227, %228
  %m_referenceSign196 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 11
  %229 = load float, float* %m_referenceSign196, align 4, !tbaa !27
  %mul197 = fmul float %mul195, %229
  %230 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError198 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %230, i32 0, i32 7
  %231 = load float*, float** %m_constraintError198, align 4, !tbaa !62
  %232 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx199 = getelementptr inbounds float, float* %231, i32 %232
  %233 = load float, float* %arrayidx199, align 4, !tbaa !26
  %add200 = fadd float %233, %mul197
  store float %add200, float* %arrayidx199, align 4, !tbaa !26
  %m_maxMotorImpulse = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 6
  %234 = load float, float* %m_maxMotorImpulse, align 4, !tbaa !67
  %fneg201 = fneg float %234
  %235 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_lowerLimit = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %235, i32 0, i32 9
  %236 = load float*, float** %m_lowerLimit, align 4, !tbaa !68
  %237 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx202 = getelementptr inbounds float, float* %236, i32 %237
  store float %fneg201, float* %arrayidx202, align 4, !tbaa !26
  %m_maxMotorImpulse203 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 6
  %238 = load float, float* %m_maxMotorImpulse203, align 4, !tbaa !67
  %239 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_upperLimit = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %239, i32 0, i32 10
  %240 = load float*, float** %m_upperLimit, align 4, !tbaa !69
  %241 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx204 = getelementptr inbounds float, float* %240, i32 %241
  store float %238, float* %arrayidx204, align 4, !tbaa !26
  %242 = bitcast float* %mot_fact to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %242) #8
  br label %if.end205

if.end205:                                        ; preds = %if.end190, %cond.end
  %243 = load i32, i32* %limit, align 4, !tbaa !36
  %tobool206 = icmp ne i32 %243, 0
  br i1 %tobool206, label %if.then207, label %if.end282

if.then207:                                       ; preds = %if.end205
  %244 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %fps208 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %244, i32 0, i32 0
  %245 = load float, float* %fps208, align 4, !tbaa !58
  %246 = load float, float* %currERP, align 4, !tbaa !26
  %mul209 = fmul float %245, %246
  store float %mul209, float* %k, align 4, !tbaa !26
  %247 = load float, float* %k, align 4, !tbaa !26
  %248 = load float, float* %limit_err, align 4, !tbaa !26
  %mul210 = fmul float %247, %248
  %249 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError211 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %249, i32 0, i32 7
  %250 = load float*, float** %m_constraintError211, align 4, !tbaa !62
  %251 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx212 = getelementptr inbounds float, float* %250, i32 %251
  %252 = load float, float* %arrayidx212, align 4, !tbaa !26
  %add213 = fadd float %252, %mul210
  store float %add213, float* %arrayidx212, align 4, !tbaa !26
  %m_flags214 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 18
  %253 = load i32, i32* %m_flags214, align 4, !tbaa !23
  %and215 = and i32 %253, 1
  %tobool216 = icmp ne i32 %and215, 0
  br i1 %tobool216, label %if.then217, label %if.end220

if.then217:                                       ; preds = %if.then207
  %m_stopCFM = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 20
  %254 = load float, float* %m_stopCFM, align 4, !tbaa !70
  %255 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %cfm218 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %255, i32 0, i32 8
  %256 = load float*, float** %cfm218, align 4, !tbaa !65
  %257 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx219 = getelementptr inbounds float, float* %256, i32 %257
  store float %254, float* %arrayidx219, align 4, !tbaa !26
  br label %if.end220

if.end220:                                        ; preds = %if.then217, %if.then207
  %258 = load float, float* %lostop, align 4, !tbaa !26
  %259 = load float, float* %histop, align 4, !tbaa !26
  %cmp221 = fcmp oeq float %258, %259
  br i1 %cmp221, label %if.then222, label %if.else

if.then222:                                       ; preds = %if.end220
  %260 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_lowerLimit223 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %260, i32 0, i32 9
  %261 = load float*, float** %m_lowerLimit223, align 4, !tbaa !68
  %262 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx224 = getelementptr inbounds float, float* %261, i32 %262
  store float 0xC7EFFFFFE0000000, float* %arrayidx224, align 4, !tbaa !26
  %263 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_upperLimit225 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %263, i32 0, i32 10
  %264 = load float*, float** %m_upperLimit225, align 4, !tbaa !69
  %265 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx226 = getelementptr inbounds float, float* %264, i32 %265
  store float 0x47EFFFFFE0000000, float* %arrayidx226, align 4, !tbaa !26
  br label %if.end239

if.else:                                          ; preds = %if.end220
  %266 = load i32, i32* %limit, align 4, !tbaa !36
  %cmp227 = icmp eq i32 %266, 1
  br i1 %cmp227, label %if.then228, label %if.else233

if.then228:                                       ; preds = %if.else
  %267 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_lowerLimit229 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %267, i32 0, i32 9
  %268 = load float*, float** %m_lowerLimit229, align 4, !tbaa !68
  %269 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx230 = getelementptr inbounds float, float* %268, i32 %269
  store float 0.000000e+00, float* %arrayidx230, align 4, !tbaa !26
  %270 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_upperLimit231 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %270, i32 0, i32 10
  %271 = load float*, float** %m_upperLimit231, align 4, !tbaa !69
  %272 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx232 = getelementptr inbounds float, float* %271, i32 %272
  store float 0x47EFFFFFE0000000, float* %arrayidx232, align 4, !tbaa !26
  br label %if.end238

if.else233:                                       ; preds = %if.else
  %273 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_lowerLimit234 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %273, i32 0, i32 9
  %274 = load float*, float** %m_lowerLimit234, align 4, !tbaa !68
  %275 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx235 = getelementptr inbounds float, float* %274, i32 %275
  store float 0xC7EFFFFFE0000000, float* %arrayidx235, align 4, !tbaa !26
  %276 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_upperLimit236 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %276, i32 0, i32 10
  %277 = load float*, float** %m_upperLimit236, align 4, !tbaa !69
  %278 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx237 = getelementptr inbounds float, float* %277, i32 %278
  store float 0.000000e+00, float* %arrayidx237, align 4, !tbaa !26
  br label %if.end238

if.end238:                                        ; preds = %if.else233, %if.then228
  br label %if.end239

if.end239:                                        ; preds = %if.end238, %if.then222
  %279 = bitcast float* %bounce to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %279) #8
  %m_limit240 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call241 = call float @_ZNK14btAngularLimit19getRelaxationFactorEv(%class.btAngularLimit* %m_limit240)
  store float %call241, float* %bounce, align 4, !tbaa !26
  %280 = load float, float* %bounce, align 4, !tbaa !26
  %cmp242 = fcmp ogt float %280, 0.000000e+00
  br i1 %cmp242, label %if.then243, label %if.end276

if.then243:                                       ; preds = %if.end239
  %281 = bitcast float* %vel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %281) #8
  %282 = load %class.btVector3*, %class.btVector3** %angVelA.addr, align 4, !tbaa !2
  %call244 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %282, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1)
  store float %call244, float* %vel, align 4, !tbaa !26
  %283 = load %class.btVector3*, %class.btVector3** %angVelB.addr, align 4, !tbaa !2
  %call245 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %283, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1)
  %284 = load float, float* %vel, align 4, !tbaa !26
  %sub246 = fsub float %284, %call245
  store float %sub246, float* %vel, align 4, !tbaa !26
  %285 = load i32, i32* %limit, align 4, !tbaa !36
  %cmp247 = icmp eq i32 %285, 1
  br i1 %cmp247, label %if.then248, label %if.else261

if.then248:                                       ; preds = %if.then243
  %286 = load float, float* %vel, align 4, !tbaa !26
  %cmp249 = fcmp olt float %286, 0.000000e+00
  br i1 %cmp249, label %if.then250, label %if.end260

if.then250:                                       ; preds = %if.then248
  %287 = bitcast float* %newc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %287) #8
  %288 = load float, float* %bounce, align 4, !tbaa !26
  %fneg251 = fneg float %288
  %289 = load float, float* %vel, align 4, !tbaa !26
  %mul252 = fmul float %fneg251, %289
  store float %mul252, float* %newc, align 4, !tbaa !26
  %290 = load float, float* %newc, align 4, !tbaa !26
  %291 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError253 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %291, i32 0, i32 7
  %292 = load float*, float** %m_constraintError253, align 4, !tbaa !62
  %293 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx254 = getelementptr inbounds float, float* %292, i32 %293
  %294 = load float, float* %arrayidx254, align 4, !tbaa !26
  %cmp255 = fcmp ogt float %290, %294
  br i1 %cmp255, label %if.then256, label %if.end259

if.then256:                                       ; preds = %if.then250
  %295 = load float, float* %newc, align 4, !tbaa !26
  %296 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError257 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %296, i32 0, i32 7
  %297 = load float*, float** %m_constraintError257, align 4, !tbaa !62
  %298 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx258 = getelementptr inbounds float, float* %297, i32 %298
  store float %295, float* %arrayidx258, align 4, !tbaa !26
  br label %if.end259

if.end259:                                        ; preds = %if.then256, %if.then250
  %299 = bitcast float* %newc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %299) #8
  br label %if.end260

if.end260:                                        ; preds = %if.end259, %if.then248
  br label %if.end275

if.else261:                                       ; preds = %if.then243
  %300 = load float, float* %vel, align 4, !tbaa !26
  %cmp262 = fcmp ogt float %300, 0.000000e+00
  br i1 %cmp262, label %if.then263, label %if.end274

if.then263:                                       ; preds = %if.else261
  %301 = bitcast float* %newc264 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %301) #8
  %302 = load float, float* %bounce, align 4, !tbaa !26
  %fneg265 = fneg float %302
  %303 = load float, float* %vel, align 4, !tbaa !26
  %mul266 = fmul float %fneg265, %303
  store float %mul266, float* %newc264, align 4, !tbaa !26
  %304 = load float, float* %newc264, align 4, !tbaa !26
  %305 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError267 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %305, i32 0, i32 7
  %306 = load float*, float** %m_constraintError267, align 4, !tbaa !62
  %307 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx268 = getelementptr inbounds float, float* %306, i32 %307
  %308 = load float, float* %arrayidx268, align 4, !tbaa !26
  %cmp269 = fcmp olt float %304, %308
  br i1 %cmp269, label %if.then270, label %if.end273

if.then270:                                       ; preds = %if.then263
  %309 = load float, float* %newc264, align 4, !tbaa !26
  %310 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError271 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %310, i32 0, i32 7
  %311 = load float*, float** %m_constraintError271, align 4, !tbaa !62
  %312 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx272 = getelementptr inbounds float, float* %311, i32 %312
  store float %309, float* %arrayidx272, align 4, !tbaa !26
  br label %if.end273

if.end273:                                        ; preds = %if.then270, %if.then263
  %313 = bitcast float* %newc264 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %313) #8
  br label %if.end274

if.end274:                                        ; preds = %if.end273, %if.else261
  br label %if.end275

if.end275:                                        ; preds = %if.end274, %if.end260
  %314 = bitcast float* %vel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %314) #8
  br label %if.end276

if.end276:                                        ; preds = %if.end275, %if.end239
  %m_limit277 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call278 = call float @_ZNK14btAngularLimit13getBiasFactorEv(%class.btAngularLimit* %m_limit277)
  %315 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError279 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %315, i32 0, i32 7
  %316 = load float*, float** %m_constraintError279, align 4, !tbaa !62
  %317 = load i32, i32* %srow, align 4, !tbaa !36
  %arrayidx280 = getelementptr inbounds float, float* %316, i32 %317
  %318 = load float, float* %arrayidx280, align 4, !tbaa !26
  %mul281 = fmul float %318, %call278
  store float %mul281, float* %arrayidx280, align 4, !tbaa !26
  %319 = bitcast float* %bounce to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %319) #8
  br label %if.end282

if.end282:                                        ; preds = %if.end276, %if.end205
  %320 = bitcast float* %currERP to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %320) #8
  %321 = bitcast float* %histop to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %321) #8
  %322 = bitcast float* %lostop to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %322) #8
  br label %if.end283

if.end283:                                        ; preds = %if.end282, %lor.lhs.false
  %323 = bitcast i32* %powered to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %323) #8
  %324 = bitcast i32* %limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %324) #8
  %325 = bitcast float* %limit_err to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %325) #8
  %326 = bitcast i32* %srow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %326) #8
  %327 = bitcast i32* %nrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %327) #8
  %328 = bitcast %class.btVector3* %u to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %328) #8
  %329 = bitcast %class.btVector3* %ax2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %329) #8
  %330 = bitcast i32* %s4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %330) #8
  %331 = bitcast i32* %s3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %331) #8
  %332 = bitcast %class.btVector3* %q to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %332) #8
  %333 = bitcast %class.btVector3* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %333) #8
  %334 = bitcast %class.btVector3* %ax1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %334) #8
  %335 = bitcast float* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %335) #8
  %336 = bitcast %class.btVector3* %a2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %336) #8
  %337 = bitcast %class.btVector3* %a1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %337) #8
  %338 = bitcast %class.btVector3* %pivotBInW to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %338) #8
  %339 = bitcast %class.btVector3* %pivotAInW to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %339) #8
  %340 = bitcast %class.btTransform* %trB to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %340) #8
  %341 = bitcast %class.btTransform* %trA to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %341) #8
  %342 = bitcast i32* %skip to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %342) #8
  %343 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %343) #8
  ret void
}

define hidden void @_ZN17btHingeConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_(%class.btHingeConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo2"* %info, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelA, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelB) #0 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo2"*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %angVelA.addr = alloca %class.btVector3*, align 4
  %angVelB.addr = alloca %class.btVector3*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4, !tbaa !2
  store %"struct.btTypedConstraint::btConstraintInfo2"* %info, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4, !tbaa !2
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4, !tbaa !2
  store %class.btVector3* %angVelA, %class.btVector3** %angVelA.addr, align 4, !tbaa !2
  store %class.btVector3* %angVelB, %class.btVector3** %angVelB.addr, align 4, !tbaa !2
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %1 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  call void @_ZN17btHingeConstraint9testLimitERK11btTransformS2_(%class.btHingeConstraint* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %0, %class.btTransform* nonnull align 4 dereferenceable(64) %1)
  %2 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %3 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %4 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %5 = load %class.btVector3*, %class.btVector3** %angVelA.addr, align 4, !tbaa !2
  %6 = load %class.btVector3*, %class.btVector3** %angVelB.addr, align 4, !tbaa !2
  call void @_ZN17btHingeConstraint16getInfo2InternalEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_(%class.btHingeConstraint* %this1, %"struct.btTypedConstraint::btConstraintInfo2"* %2, %class.btTransform* nonnull align 4 dereferenceable(64) %3, %class.btTransform* nonnull align 4 dereferenceable(64) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5, %class.btVector3* nonnull align 4 dereferenceable(16) %6)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformmlERKS_(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t) #5 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %1 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %1) #8
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %2 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %2, i32 0, i32 0
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %3 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #8
  %4 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %4, i32 0, i32 1
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp3, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  %5 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #8
  %6 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %6) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZNK9btVector322getSkewSymmetricMatrixEPS_S0_S0_(%class.btVector3* %this, %class.btVector3* %v0, %class.btVector3* %v1, %class.btVector3* %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !26
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this1)
  %3 = load float, float* %call, align 4, !tbaa !26
  %fneg = fneg float %3
  store float %fneg, float* %ref.tmp2, align 4, !tbaa !26
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this1)
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %0, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %call3)
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this1)
  %7 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  store float 0.000000e+00, float* %ref.tmp5, align 4, !tbaa !26
  %8 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this1)
  %9 = load float, float* %call7, align 4, !tbaa !26
  %fneg8 = fneg float %9
  store float %fneg8, float* %ref.tmp6, align 4, !tbaa !26
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %6, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %10 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  %12 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %13 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this1)
  %14 = load float, float* %call10, align 4, !tbaa !26
  %fneg11 = fneg float %14
  store float %fneg11, float* %ref.tmp9, align 4, !tbaa !26
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this1)
  %15 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  store float 0.000000e+00, float* %ref.tmp13, align 4, !tbaa !26
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %12, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %call12, float* nonnull align 4 dereferenceable(4) %ref.tmp13)
  %16 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK14btAngularLimit13getCorrectionEv(%class.btAngularLimit* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAngularLimit*, align 4
  store %class.btAngularLimit* %this, %class.btAngularLimit** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAngularLimit*, %class.btAngularLimit** %this.addr, align 4
  %m_correction = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 5
  %0 = load float, float* %m_correction, align 4, !tbaa !33
  ret float %0
}

define linkonce_odr hidden float @_ZNK17btHingeConstraint13getLowerLimitEv(%class.btHingeConstraint* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %m_limit = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call = call float @_ZNK14btAngularLimit6getLowEv(%class.btAngularLimit* %m_limit)
  ret float %call
}

define linkonce_odr hidden float @_ZNK17btHingeConstraint13getUpperLimitEv(%class.btHingeConstraint* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %m_limit = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call = call float @_ZNK14btAngularLimit7getHighEv(%class.btAngularLimit* %m_limit)
  ret float %call
}

declare float @_ZN17btTypedConstraint14getMotorFactorEfffff(%class.btTypedConstraint*, float, float, float, float, float) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK14btAngularLimit19getRelaxationFactorEv(%class.btAngularLimit* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAngularLimit*, align 4
  store %class.btAngularLimit* %this, %class.btAngularLimit** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAngularLimit*, %class.btAngularLimit** %this.addr, align 4
  %m_relaxationFactor = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 4
  %0 = load float, float* %m_relaxationFactor, align 4, !tbaa !32
  ret float %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK14btAngularLimit13getBiasFactorEv(%class.btAngularLimit* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAngularLimit*, align 4
  store %class.btAngularLimit* %this, %class.btAngularLimit** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAngularLimit*, %class.btAngularLimit** %this.addr, align 4
  %m_biasFactor = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 3
  %0 = load float, float* %m_biasFactor, align 4, !tbaa !31
  ret float %0
}

define hidden void @_ZN17btHingeConstraint9setFramesERK11btTransformS2_(%class.btHingeConstraint* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %frameA, %class.btTransform* nonnull align 4 dereferenceable(64) %frameB) #0 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %frameA.addr = alloca %class.btTransform*, align 4
  %frameB.addr = alloca %class.btTransform*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %frameA, %class.btTransform** %frameA.addr, align 4, !tbaa !2
  store %class.btTransform* %frameB, %class.btTransform** %frameB.addr, align 4, !tbaa !2
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %frameA.addr, align 4, !tbaa !2
  %m_rbAFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_rbAFrame, %class.btTransform* nonnull align 4 dereferenceable(64) %0)
  %1 = load %class.btTransform*, %class.btTransform** %frameB.addr, align 4, !tbaa !2
  %m_rbBFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_rbBFrame, %class.btTransform* nonnull align 4 dereferenceable(64) %1)
  %2 = bitcast %class.btHingeConstraint* %this1 to void (%class.btHingeConstraint*)***
  %vtable = load void (%class.btHingeConstraint*)**, void (%class.btHingeConstraint*)*** %2, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btHingeConstraint*)*, void (%class.btHingeConstraint*)** %vtable, i64 2
  %3 = load void (%class.btHingeConstraint*)*, void (%class.btHingeConstraint*)** %vfn, align 4
  call void %3(%class.btHingeConstraint* %this1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #5 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !24
  ret %class.btTransform* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN17btHingeConstraint9updateRHSEf(%class.btHingeConstraint* %this, float %timeStep) #2 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %timeStep.addr = alloca float, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4, !tbaa !2
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !26
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  ret void
}

define hidden float @_ZN17btHingeConstraint13getHingeAngleEv(%class.btHingeConstraint* %this) #0 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %0 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %0, i32 0, i32 8
  %1 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4, !tbaa !37
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %1)
  %2 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %2, i32 0, i32 9
  %3 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4, !tbaa !41
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %3)
  %call3 = call float @_ZN17btHingeConstraint13getHingeAngleERK11btTransformS2_(%class.btHingeConstraint* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %call, %class.btTransform* nonnull align 4 dereferenceable(64) %call2)
  ret float %call3
}

define hidden float @_ZN17btHingeConstraint13getHingeAngleERK11btTransformS2_(%class.btHingeConstraint* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB) #0 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %refAxis0 = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %refAxis1 = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %swingAxis = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %angle = alloca float, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4, !tbaa !2
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %0 = bitcast %class.btVector3* %refAxis0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %1)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #8
  %m_rbAFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %call2 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbAFrame)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* %call2, i32 0)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %refAxis0, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %3) #8
  %4 = bitcast %class.btVector3* %refAxis1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #8
  %5 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %5)
  %6 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #8
  %m_rbAFrame5 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %call6 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbAFrame5)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp4, %class.btMatrix3x3* %call6, i32 1)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %refAxis1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  %7 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %7) #8
  %8 = bitcast %class.btVector3* %swingAxis to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #8
  %9 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %call7 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %9)
  %10 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #8
  %m_rbBFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  %call9 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbBFrame)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp8, %class.btMatrix3x3* %call9, i32 1)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %swingAxis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call7, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp8)
  %11 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %11) #8
  %12 = bitcast float* %angle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  %call10 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %swingAxis, %class.btVector3* nonnull align 4 dereferenceable(16) %refAxis0)
  %call11 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %swingAxis, %class.btVector3* nonnull align 4 dereferenceable(16) %refAxis1)
  %call12 = call float @_Z7btAtan2ff(float %call10, float %call11)
  store float %call12, float* %angle, align 4, !tbaa !26
  %m_referenceSign = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 11
  %13 = load float, float* %m_referenceSign, align 4, !tbaa !27
  %14 = load float, float* %angle, align 4, !tbaa !26
  %mul = fmul float %13, %14
  %15 = bitcast float* %angle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast %class.btVector3* %swingAxis to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %16) #8
  %17 = bitcast %class.btVector3* %refAxis1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %17) #8
  %18 = bitcast %class.btVector3* %refAxis0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #8
  ret float %mul
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z7btAtan2ff(float %x, float %y) #3 comdat {
entry:
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !26
  store float %y, float* %y.addr, align 4, !tbaa !26
  %0 = load float, float* %x.addr, align 4, !tbaa !26
  %1 = load float, float* %y.addr, align 4, !tbaa !26
  %call = call float @atan2f(float %0, float %1) #9
  ret float %call
}

declare void @_ZN14btAngularLimit4testEf(%class.btAngularLimit*, float) #1

define internal void @__cxx_global_var_init() #0 {
entry:
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !26
  %1 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 0.000000e+00, float* %ref.tmp1, align 4, !tbaa !26
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 1.000000e+00, float* %ref.tmp2, align 4, !tbaa !26
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* @_ZL6vHinge, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  %4 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !26
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !26
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !26
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !26
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !26
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !26
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !26
  ret %class.btVector3* %this1
}

define hidden void @_ZN17btHingeConstraint14setMotorTargetERK12btQuaternionf(%class.btHingeConstraint* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qAinB, float %dt) #0 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %qAinB.addr = alloca %class.btQuaternion*, align 4
  %dt.addr = alloca float, align 4
  %qConstraint = alloca %class.btQuaternion, align 4
  %ref.tmp = alloca %class.btQuaternion, align 4
  %ref.tmp2 = alloca %class.btQuaternion, align 4
  %ref.tmp3 = alloca %class.btQuaternion, align 4
  %ref.tmp4 = alloca %class.btQuaternion, align 4
  %vNoHinge = alloca %class.btVector3, align 4
  %qNoHinge = alloca %class.btQuaternion, align 4
  %qHinge = alloca %class.btQuaternion, align 4
  %ref.tmp6 = alloca %class.btQuaternion, align 4
  %targetAngle = alloca float, align 4
  %ref.tmp9 = alloca %class.btQuaternion, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %qAinB, %class.btQuaternion** %qAinB.addr, align 4, !tbaa !2
  store float %dt, float* %dt.addr, align 4, !tbaa !26
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %qConstraint to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #8
  %2 = bitcast %class.btQuaternion* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #8
  %3 = bitcast %class.btQuaternion* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #8
  %m_rbBFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp3, %class.btTransform* %m_rbBFrame)
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp2, %class.btQuaternion* %ref.tmp3)
  %4 = load %class.btQuaternion*, %class.btQuaternion** %qAinB.addr, align 4, !tbaa !2
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %ref.tmp, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp2, %class.btQuaternion* nonnull align 4 dereferenceable(16) %4)
  %5 = bitcast %class.btQuaternion* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #8
  %m_rbAFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp4, %class.btTransform* %m_rbAFrame)
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %qConstraint, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp4)
  %6 = bitcast %class.btQuaternion* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %6) #8
  %7 = bitcast %class.btQuaternion* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %7) #8
  %8 = bitcast %class.btQuaternion* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #8
  %9 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #8
  %call = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %qConstraint)
  %10 = bitcast %class.btVector3* %vNoHinge to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #8
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %vNoHinge, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qConstraint, %class.btVector3* nonnull align 4 dereferenceable(16) @_ZL6vHinge)
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %vNoHinge)
  %11 = bitcast %class.btQuaternion* %qNoHinge to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #8
  call void @_Z15shortestArcQuatRK9btVector3S1_(%class.btQuaternion* sret align 4 %qNoHinge, %class.btVector3* nonnull align 4 dereferenceable(16) @_ZL6vHinge, %class.btVector3* nonnull align 4 dereferenceable(16) %vNoHinge)
  %12 = bitcast %class.btQuaternion* %qHinge to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %12) #8
  %13 = bitcast %class.btQuaternion* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #8
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp6, %class.btQuaternion* %qNoHinge)
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %qHinge, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp6, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qConstraint)
  %14 = bitcast %class.btQuaternion* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %14) #8
  %call7 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %qHinge)
  %15 = bitcast float* %targetAngle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  %call8 = call float @_ZNK12btQuaternion8getAngleEv(%class.btQuaternion* %qHinge)
  store float %call8, float* %targetAngle, align 4, !tbaa !26
  %16 = load float, float* %targetAngle, align 4, !tbaa !26
  %cmp = fcmp ogt float %16, 0x400921FB60000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %17 = bitcast %class.btQuaternion* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %17) #8
  call void @_ZNK12btQuaternionngEv(%class.btQuaternion* sret align 4 %ref.tmp9, %class.btQuaternion* %qHinge)
  %18 = bitcast %class.btQuaternion* %qHinge to i8*
  %19 = bitcast %class.btQuaternion* %ref.tmp9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %18, i8* align 4 %19, i32 16, i1 false)
  %20 = bitcast %class.btQuaternion* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %20) #8
  %call10 = call float @_ZNK12btQuaternion8getAngleEv(%class.btQuaternion* %qHinge)
  store float %call10, float* %targetAngle, align 4, !tbaa !26
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %21 = bitcast %class.btQuaternion* %qHinge to %class.btQuadWord*
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getZEv(%class.btQuadWord* %21)
  %22 = load float, float* %call11, align 4, !tbaa !26
  %cmp12 = fcmp olt float %22, 0.000000e+00
  br i1 %cmp12, label %if.then13, label %if.end14

if.then13:                                        ; preds = %if.end
  %23 = load float, float* %targetAngle, align 4, !tbaa !26
  %fneg = fneg float %23
  store float %fneg, float* %targetAngle, align 4, !tbaa !26
  br label %if.end14

if.end14:                                         ; preds = %if.then13, %if.end
  %24 = load float, float* %targetAngle, align 4, !tbaa !26
  %25 = load float, float* %dt.addr, align 4, !tbaa !26
  call void @_ZN17btHingeConstraint14setMotorTargetEff(%class.btHingeConstraint* %this1, float %24, float %25)
  %26 = bitcast float* %targetAngle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #8
  %27 = bitcast %class.btQuaternion* %qHinge to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %27) #8
  %28 = bitcast %class.btQuaternion* %qNoHinge to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %28) #8
  %29 = bitcast %class.btVector3* %vNoHinge to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #8
  %30 = bitcast %class.btQuaternion* %qConstraint to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %30) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q2) #5 comdat {
entry:
  %q1.addr = alloca %class.btQuaternion*, align 4
  %q2.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp44 = alloca float, align 4
  store %class.btQuaternion* %q1, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q2, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %2 = bitcast %class.btQuaternion* %1 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %2)
  %3 = load float, float* %call, align 4, !tbaa !26
  %4 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %5 = bitcast %class.btQuaternion* %4 to %class.btQuadWord*
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %5)
  %6 = load float, float* %call1, align 4, !tbaa !26
  %mul = fmul float %3, %6
  %7 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %8 = bitcast %class.btQuaternion* %7 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %8)
  %9 = load float, float* %call2, align 4, !tbaa !26
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %11)
  %12 = load float, float* %call3, align 4, !tbaa !26
  %mul4 = fmul float %9, %12
  %add = fadd float %mul, %mul4
  %13 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %14 = bitcast %class.btQuaternion* %13 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %14)
  %15 = load float, float* %call5, align 4, !tbaa !26
  %16 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %17 = bitcast %class.btQuaternion* %16 to %class.btQuadWord*
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %17)
  %18 = load float, float* %call6, align 4, !tbaa !26
  %mul7 = fmul float %15, %18
  %add8 = fadd float %add, %mul7
  %19 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %20 = bitcast %class.btQuaternion* %19 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %20)
  %21 = load float, float* %call9, align 4, !tbaa !26
  %22 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %23 = bitcast %class.btQuaternion* %22 to %class.btQuadWord*
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %23)
  %24 = load float, float* %call10, align 4, !tbaa !26
  %mul11 = fmul float %21, %24
  %sub = fsub float %add8, %mul11
  store float %sub, float* %ref.tmp, align 4, !tbaa !26
  %25 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  %26 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %27 = bitcast %class.btQuaternion* %26 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %27)
  %28 = load float, float* %call13, align 4, !tbaa !26
  %29 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %30 = bitcast %class.btQuaternion* %29 to %class.btQuadWord*
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %30)
  %31 = load float, float* %call14, align 4, !tbaa !26
  %mul15 = fmul float %28, %31
  %32 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %33 = bitcast %class.btQuaternion* %32 to %class.btQuadWord*
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %33)
  %34 = load float, float* %call16, align 4, !tbaa !26
  %35 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %36 = bitcast %class.btQuaternion* %35 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %36)
  %37 = load float, float* %call17, align 4, !tbaa !26
  %mul18 = fmul float %34, %37
  %add19 = fadd float %mul15, %mul18
  %38 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %39 = bitcast %class.btQuaternion* %38 to %class.btQuadWord*
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %39)
  %40 = load float, float* %call20, align 4, !tbaa !26
  %41 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %42 = bitcast %class.btQuaternion* %41 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %42)
  %43 = load float, float* %call21, align 4, !tbaa !26
  %mul22 = fmul float %40, %43
  %add23 = fadd float %add19, %mul22
  %44 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %45 = bitcast %class.btQuaternion* %44 to %class.btQuadWord*
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %45)
  %46 = load float, float* %call24, align 4, !tbaa !26
  %47 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %48 = bitcast %class.btQuaternion* %47 to %class.btQuadWord*
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %48)
  %49 = load float, float* %call25, align 4, !tbaa !26
  %mul26 = fmul float %46, %49
  %sub27 = fsub float %add23, %mul26
  store float %sub27, float* %ref.tmp12, align 4, !tbaa !26
  %50 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #8
  %51 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %52 = bitcast %class.btQuaternion* %51 to %class.btQuadWord*
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %52)
  %53 = load float, float* %call29, align 4, !tbaa !26
  %54 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %55 = bitcast %class.btQuaternion* %54 to %class.btQuadWord*
  %call30 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %55)
  %56 = load float, float* %call30, align 4, !tbaa !26
  %mul31 = fmul float %53, %56
  %57 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %58 = bitcast %class.btQuaternion* %57 to %class.btQuadWord*
  %call32 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %58)
  %59 = load float, float* %call32, align 4, !tbaa !26
  %60 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %61 = bitcast %class.btQuaternion* %60 to %class.btQuadWord*
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %61)
  %62 = load float, float* %call33, align 4, !tbaa !26
  %mul34 = fmul float %59, %62
  %add35 = fadd float %mul31, %mul34
  %63 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %64 = bitcast %class.btQuaternion* %63 to %class.btQuadWord*
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %64)
  %65 = load float, float* %call36, align 4, !tbaa !26
  %66 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %67 = bitcast %class.btQuaternion* %66 to %class.btQuadWord*
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %67)
  %68 = load float, float* %call37, align 4, !tbaa !26
  %mul38 = fmul float %65, %68
  %add39 = fadd float %add35, %mul38
  %69 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %70 = bitcast %class.btQuaternion* %69 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %70)
  %71 = load float, float* %call40, align 4, !tbaa !26
  %72 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %73 = bitcast %class.btQuaternion* %72 to %class.btQuadWord*
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %73)
  %74 = load float, float* %call41, align 4, !tbaa !26
  %mul42 = fmul float %71, %74
  %sub43 = fsub float %add39, %mul42
  store float %sub43, float* %ref.tmp28, align 4, !tbaa !26
  %75 = bitcast float* %ref.tmp44 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #8
  %76 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %77 = bitcast %class.btQuaternion* %76 to %class.btQuadWord*
  %call45 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %77)
  %78 = load float, float* %call45, align 4, !tbaa !26
  %79 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %80 = bitcast %class.btQuaternion* %79 to %class.btQuadWord*
  %call46 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %80)
  %81 = load float, float* %call46, align 4, !tbaa !26
  %mul47 = fmul float %78, %81
  %82 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %83 = bitcast %class.btQuaternion* %82 to %class.btQuadWord*
  %call48 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %83)
  %84 = load float, float* %call48, align 4, !tbaa !26
  %85 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %86 = bitcast %class.btQuaternion* %85 to %class.btQuadWord*
  %call49 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %86)
  %87 = load float, float* %call49, align 4, !tbaa !26
  %mul50 = fmul float %84, %87
  %sub51 = fsub float %mul47, %mul50
  %88 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %89 = bitcast %class.btQuaternion* %88 to %class.btQuadWord*
  %call52 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %89)
  %90 = load float, float* %call52, align 4, !tbaa !26
  %91 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %92 = bitcast %class.btQuaternion* %91 to %class.btQuadWord*
  %call53 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %92)
  %93 = load float, float* %call53, align 4, !tbaa !26
  %mul54 = fmul float %90, %93
  %sub55 = fsub float %sub51, %mul54
  %94 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %95 = bitcast %class.btQuaternion* %94 to %class.btQuadWord*
  %call56 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %95)
  %96 = load float, float* %call56, align 4, !tbaa !26
  %97 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %98 = bitcast %class.btQuaternion* %97 to %class.btQuadWord*
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %98)
  %99 = load float, float* %call57, align 4, !tbaa !26
  %mul58 = fmul float %96, %99
  %sub59 = fsub float %sub55, %mul58
  store float %sub59, float* %ref.tmp44, align 4, !tbaa !26
  %call60 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp44)
  %100 = bitcast float* %ref.tmp44 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #8
  %101 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #8
  %102 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #8
  %103 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #8
  ret void
}

define linkonce_odr hidden void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btTransform* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %agg.result)
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %m_basis, %class.btQuaternion* nonnull align 4 dereferenceable(16) %agg.result)
  ret void
}

define linkonce_odr hidden void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !26
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp, align 4, !tbaa !26
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats3 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %4, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [4 x float], [4 x float]* %m_floats3, i32 0, i32 1
  %5 = load float, float* %arrayidx4, align 4, !tbaa !26
  %fneg5 = fneg float %5
  store float %fneg5, float* %ref.tmp2, align 4, !tbaa !26
  %6 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats7 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %7, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 2
  %8 = load float, float* %arrayidx8, align 4, !tbaa !26
  %fneg9 = fneg float %8
  store float %fneg9, float* %ref.tmp6, align 4, !tbaa !26
  %9 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats10 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %9, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 3
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %10 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  %12 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  ret void
}

define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call float @_ZNK12btQuaternion6lengthEv(%class.btQuaternion* %this1)
  store float %call, float* %ref.tmp, align 4, !tbaa !26
  %call2 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaterniondVERKf(%class.btQuaternion* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1) #8
  ret %class.btQuaternion* %call2
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4, !tbaa !26
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1) #8
  ret %class.btVector3* %call2
}

define linkonce_odr hidden float @_ZNK12btQuaternion8getAngleEv(%class.btQuaternion* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %s = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  %2 = load float, float* %arrayidx, align 4, !tbaa !26
  %call = call float @_Z6btAcosf(float %2)
  %mul = fmul float 2.000000e+00, %call
  store float %mul, float* %s, align 4, !tbaa !26
  %3 = load float, float* %s, align 4, !tbaa !26
  %4 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  ret float %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK12btQuaternionngEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q2 = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion** %q2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store %class.btQuaternion* %this1, %class.btQuaternion** %q2, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q2, align 4, !tbaa !2
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call, align 4, !tbaa !26
  %fneg = fneg float %4
  store float %fneg, float* %ref.tmp, align 4, !tbaa !26
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btQuaternion*, %class.btQuaternion** %q2, align 4, !tbaa !2
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %7)
  %8 = load float, float* %call3, align 4, !tbaa !26
  %fneg4 = fneg float %8
  store float %fneg4, float* %ref.tmp2, align 4, !tbaa !26
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q2, align 4, !tbaa !2
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %11)
  %12 = load float, float* %call6, align 4, !tbaa !26
  %fneg7 = fneg float %12
  store float %fneg7, float* %ref.tmp5, align 4, !tbaa !26
  %13 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load %class.btQuaternion*, %class.btQuaternion** %q2, align 4, !tbaa !2
  %15 = bitcast %class.btQuaternion* %14 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %15, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  %16 = load float, float* %arrayidx, align 4, !tbaa !26
  %fneg9 = fneg float %16
  store float %fneg9, float* %ref.tmp8, align 4, !tbaa !26
  %call10 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  %17 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  %18 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  %19 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  %20 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #8
  %21 = bitcast %class.btQuaternion** %q2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getZEv(%class.btQuadWord* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

define hidden void @_ZN17btHingeConstraint14setMotorTargetEff(%class.btHingeConstraint* %this, float %targetAngle, float %dt) #0 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %targetAngle.addr = alloca float, align 4
  %dt.addr = alloca float, align 4
  %curAngle = alloca float, align 4
  %dAngle = alloca float, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4, !tbaa !2
  store float %targetAngle, float* %targetAngle.addr, align 4, !tbaa !26
  store float %dt, float* %dt.addr, align 4, !tbaa !26
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %m_limit = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  call void @_ZNK14btAngularLimit3fitERf(%class.btAngularLimit* %m_limit, float* nonnull align 4 dereferenceable(4) %targetAngle.addr)
  %0 = bitcast float* %curAngle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %1, i32 0, i32 8
  %2 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4, !tbaa !37
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %2)
  %3 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %3, i32 0, i32 9
  %4 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4, !tbaa !41
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %4)
  %call3 = call float @_ZN17btHingeConstraint13getHingeAngleERK11btTransformS2_(%class.btHingeConstraint* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %call, %class.btTransform* nonnull align 4 dereferenceable(64) %call2)
  store float %call3, float* %curAngle, align 4, !tbaa !26
  %5 = bitcast float* %dAngle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load float, float* %targetAngle.addr, align 4, !tbaa !26
  %7 = load float, float* %curAngle, align 4, !tbaa !26
  %sub = fsub float %6, %7
  store float %sub, float* %dAngle, align 4, !tbaa !26
  %8 = load float, float* %dAngle, align 4, !tbaa !26
  %9 = load float, float* %dt.addr, align 4, !tbaa !26
  %div = fdiv float %8, %9
  %m_motorTargetVelocity = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 5
  store float %div, float* %m_motorTargetVelocity, align 4, !tbaa !66
  %10 = bitcast float* %dAngle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %curAngle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret void
}

declare void @_ZNK14btAngularLimit3fitERf(%class.btAngularLimit*, float* nonnull align 4 dereferenceable(4)) #1

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #5 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !26
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !26
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !26
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !26
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !26
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !26
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !26
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !26
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !26
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #5 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !26
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !26
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !26
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !26
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !26
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !26
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !26
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !26
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !26
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #3 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !26
  %0 = load float, float* %y.addr, align 4, !tbaa !26
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !26
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !26
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  ret %class.btVector3* %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !26
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !26
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !26
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !26
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !26
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !26
  %6 = load float*, float** %s.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !26
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !26
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !26
  ret %class.btVector3* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN17btHingeConstraint8setParamEifi(%class.btHingeConstraint* %this, i32 %num, float %value, i32 %axis) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %num.addr = alloca i32, align 4
  %value.addr = alloca float, align 4
  %axis.addr = alloca i32, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4, !tbaa !2
  store i32 %num, i32* %num.addr, align 4, !tbaa !36
  store float %value, float* %value.addr, align 4, !tbaa !26
  store i32 %axis, i32* %axis.addr, align 4, !tbaa !36
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %0 = load i32, i32* %axis.addr, align 4, !tbaa !36
  %cmp = icmp eq i32 %0, -1
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %axis.addr, align 4, !tbaa !36
  %cmp2 = icmp eq i32 %1, 5
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %lor.lhs.false, %entry
  %2 = load i32, i32* %num.addr, align 4, !tbaa !36
  switch i32 %2, label %sw.default [
    i32 2, label %sw.bb
    i32 4, label %sw.bb3
    i32 3, label %sw.bb6
  ]

sw.bb:                                            ; preds = %if.then
  %3 = load float, float* %value.addr, align 4, !tbaa !26
  %m_stopERP = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 21
  store float %3, float* %m_stopERP, align 4, !tbaa !63
  %m_flags = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 18
  %4 = load i32, i32* %m_flags, align 4, !tbaa !23
  %or = or i32 %4, 2
  store i32 %or, i32* %m_flags, align 4, !tbaa !23
  br label %sw.epilog

sw.bb3:                                           ; preds = %if.then
  %5 = load float, float* %value.addr, align 4, !tbaa !26
  %m_stopCFM = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 20
  store float %5, float* %m_stopCFM, align 4, !tbaa !70
  %m_flags4 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 18
  %6 = load i32, i32* %m_flags4, align 4, !tbaa !23
  %or5 = or i32 %6, 1
  store i32 %or5, i32* %m_flags4, align 4, !tbaa !23
  br label %sw.epilog

sw.bb6:                                           ; preds = %if.then
  %7 = load float, float* %value.addr, align 4, !tbaa !26
  %m_normalCFM = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 19
  store float %7, float* %m_normalCFM, align 4, !tbaa !64
  %m_flags7 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 18
  %8 = load i32, i32* %m_flags7, align 4, !tbaa !23
  %or8 = or i32 %8, 4
  store i32 %or8, i32* %m_flags7, align 4, !tbaa !23
  br label %sw.epilog

sw.default:                                       ; preds = %if.then
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb6, %sw.bb3, %sw.bb
  br label %if.end

if.else:                                          ; preds = %lor.lhs.false
  br label %if.end

if.end:                                           ; preds = %if.else, %sw.epilog
  ret void
}

; Function Attrs: nounwind
define hidden float @_ZNK17btHingeConstraint8getParamEii(%class.btHingeConstraint* %this, i32 %num, i32 %axis) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %num.addr = alloca i32, align 4
  %axis.addr = alloca i32, align 4
  %retVal = alloca float, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4, !tbaa !2
  store i32 %num, i32* %num.addr, align 4, !tbaa !36
  store i32 %axis, i32* %axis.addr, align 4, !tbaa !36
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %0 = bitcast float* %retVal to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 0.000000e+00, float* %retVal, align 4, !tbaa !26
  %1 = load i32, i32* %axis.addr, align 4, !tbaa !36
  %cmp = icmp eq i32 %1, -1
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %2 = load i32, i32* %axis.addr, align 4, !tbaa !36
  %cmp2 = icmp eq i32 %2, 5
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %lor.lhs.false, %entry
  %3 = load i32, i32* %num.addr, align 4, !tbaa !36
  switch i32 %3, label %sw.default [
    i32 2, label %sw.bb
    i32 4, label %sw.bb3
    i32 3, label %sw.bb4
  ]

sw.bb:                                            ; preds = %if.then
  %m_stopERP = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 21
  %4 = load float, float* %m_stopERP, align 4, !tbaa !63
  store float %4, float* %retVal, align 4, !tbaa !26
  br label %sw.epilog

sw.bb3:                                           ; preds = %if.then
  %m_stopCFM = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 20
  %5 = load float, float* %m_stopCFM, align 4, !tbaa !70
  store float %5, float* %retVal, align 4, !tbaa !26
  br label %sw.epilog

sw.bb4:                                           ; preds = %if.then
  %m_normalCFM = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 19
  %6 = load float, float* %m_normalCFM, align 4, !tbaa !64
  store float %6, float* %retVal, align 4, !tbaa !26
  br label %sw.epilog

sw.default:                                       ; preds = %if.then
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb4, %sw.bb3, %sw.bb
  br label %if.end

if.else:                                          ; preds = %lor.lhs.false
  br label %if.end

if.end:                                           ; preds = %if.else, %sw.epilog
  %7 = load float, float* %retVal, align 4, !tbaa !26
  %8 = bitcast float* %retVal to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  ret float %7
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btTypedConstraint* @_ZN17btTypedConstraintD2Ev(%class.btTypedConstraint* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = bitcast %class.btTypedConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [13 x i8*] }, { [13 x i8*] }* @_ZTV17btTypedConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !8
  ret %class.btTypedConstraint* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN17btHingeConstraintD0Ev(%class.btHingeConstraint* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %call = call %class.btHingeConstraint* bitcast (%class.btTypedConstraint* (%class.btTypedConstraint*)* @_ZN17btTypedConstraintD2Ev to %class.btHingeConstraint* (%class.btHingeConstraint*)*)(%class.btHingeConstraint* %this1) #8
  %0 = bitcast %class.btHingeConstraint* %this1 to i8*
  call void @_ZN17btHingeConstraintdlEPv(i8* %0) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif(%class.btTypedConstraint* %this, %class.btAlignedObjectArray.1* nonnull align 1 %ca, i32 %solverBodyA, i32 %solverBodyB, float %timeStep) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  %ca.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %solverBodyA.addr = alloca i32, align 4
  %solverBodyB.addr = alloca i32, align 4
  %timeStep.addr = alloca float, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.1* %ca, %class.btAlignedObjectArray.1** %ca.addr, align 4, !tbaa !2
  store i32 %solverBodyA, i32* %solverBodyA.addr, align 4, !tbaa !36
  store i32 %solverBodyB, i32* %solverBodyB.addr, align 4, !tbaa !36
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !26
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %ca.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f(%class.btTypedConstraint* %this, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %0, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %1, float %2) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  %.addr = alloca %struct.btSolverBody*, align 4
  %.addr1 = alloca %struct.btSolverBody*, align 4
  %.addr2 = alloca float, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4, !tbaa !2
  store %struct.btSolverBody* %0, %struct.btSolverBody** %.addr, align 4, !tbaa !2
  store %struct.btSolverBody* %1, %struct.btSolverBody** %.addr1, align 4, !tbaa !2
  store float %2, float* %.addr2, align 4, !tbaa !26
  %this3 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK17btHingeConstraint28calculateSerializeBufferSizeEv(%class.btHingeConstraint* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  ret i32 220
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZNK17btHingeConstraint9serializeEPvP12btSerializer(%class.btHingeConstraint* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %hingeData = alloca %struct.btHingeConstraintFloatData*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4, !tbaa !2
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %0 = bitcast %struct.btHingeConstraintFloatData** %hingeData to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i8*, i8** %dataBuffer.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.btHingeConstraintFloatData*
  store %struct.btHingeConstraintFloatData* %2, %struct.btHingeConstraintFloatData** %hingeData, align 4, !tbaa !2
  %3 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %4 = load %struct.btHingeConstraintFloatData*, %struct.btHingeConstraintFloatData** %hingeData, align 4, !tbaa !2
  %m_typeConstraintData = getelementptr inbounds %struct.btHingeConstraintFloatData, %struct.btHingeConstraintFloatData* %4, i32 0, i32 0
  %5 = bitcast %struct.btTypedConstraintData* %m_typeConstraintData to i8*
  %6 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %call = call i8* @_ZNK17btTypedConstraint9serializeEPvP12btSerializer(%class.btTypedConstraint* %3, i8* %5, %class.btSerializer* %6)
  %m_rbAFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %7 = load %struct.btHingeConstraintFloatData*, %struct.btHingeConstraintFloatData** %hingeData, align 4, !tbaa !2
  %m_rbAFrame2 = getelementptr inbounds %struct.btHingeConstraintFloatData, %struct.btHingeConstraintFloatData* %7, i32 0, i32 1
  call void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %m_rbAFrame, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %m_rbAFrame2)
  %m_rbBFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  %8 = load %struct.btHingeConstraintFloatData*, %struct.btHingeConstraintFloatData** %hingeData, align 4, !tbaa !2
  %m_rbBFrame3 = getelementptr inbounds %struct.btHingeConstraintFloatData, %struct.btHingeConstraintFloatData* %8, i32 0, i32 2
  call void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %m_rbBFrame, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %m_rbBFrame3)
  %m_angularOnly = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 12
  %9 = load i8, i8* %m_angularOnly, align 4, !tbaa !10, !range !21
  %tobool = trunc i8 %9 to i1
  %conv = zext i1 %tobool to i32
  %10 = load %struct.btHingeConstraintFloatData*, %struct.btHingeConstraintFloatData** %hingeData, align 4, !tbaa !2
  %m_angularOnly4 = getelementptr inbounds %struct.btHingeConstraintFloatData, %struct.btHingeConstraintFloatData* %10, i32 0, i32 4
  store i32 %conv, i32* %m_angularOnly4, align 4, !tbaa !71
  %m_enableAngularMotor = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 13
  %11 = load i8, i8* %m_enableAngularMotor, align 1, !tbaa !18, !range !21
  %tobool5 = trunc i8 %11 to i1
  %conv6 = zext i1 %tobool5 to i32
  %12 = load %struct.btHingeConstraintFloatData*, %struct.btHingeConstraintFloatData** %hingeData, align 4, !tbaa !2
  %m_enableAngularMotor7 = getelementptr inbounds %struct.btHingeConstraintFloatData, %struct.btHingeConstraintFloatData* %12, i32 0, i32 5
  store i32 %conv6, i32* %m_enableAngularMotor7, align 4, !tbaa !77
  %m_maxMotorImpulse = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 6
  %13 = load float, float* %m_maxMotorImpulse, align 4, !tbaa !67
  %14 = load %struct.btHingeConstraintFloatData*, %struct.btHingeConstraintFloatData** %hingeData, align 4, !tbaa !2
  %m_maxMotorImpulse8 = getelementptr inbounds %struct.btHingeConstraintFloatData, %struct.btHingeConstraintFloatData* %14, i32 0, i32 7
  store float %13, float* %m_maxMotorImpulse8, align 4, !tbaa !78
  %m_motorTargetVelocity = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 5
  %15 = load float, float* %m_motorTargetVelocity, align 4, !tbaa !66
  %16 = load %struct.btHingeConstraintFloatData*, %struct.btHingeConstraintFloatData** %hingeData, align 4, !tbaa !2
  %m_motorTargetVelocity9 = getelementptr inbounds %struct.btHingeConstraintFloatData, %struct.btHingeConstraintFloatData* %16, i32 0, i32 6
  store float %15, float* %m_motorTargetVelocity9, align 4, !tbaa !79
  %m_useReferenceFrameA = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 16
  %17 = load i8, i8* %m_useReferenceFrameA, align 4, !tbaa !22, !range !21
  %tobool10 = trunc i8 %17 to i1
  %conv11 = zext i1 %tobool10 to i32
  %18 = load %struct.btHingeConstraintFloatData*, %struct.btHingeConstraintFloatData** %hingeData, align 4, !tbaa !2
  %m_useReferenceFrameA12 = getelementptr inbounds %struct.btHingeConstraintFloatData, %struct.btHingeConstraintFloatData* %18, i32 0, i32 3
  store i32 %conv11, i32* %m_useReferenceFrameA12, align 4, !tbaa !80
  %m_limit = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call13 = call float @_ZNK14btAngularLimit6getLowEv(%class.btAngularLimit* %m_limit)
  %19 = load %struct.btHingeConstraintFloatData*, %struct.btHingeConstraintFloatData** %hingeData, align 4, !tbaa !2
  %m_lowerLimit = getelementptr inbounds %struct.btHingeConstraintFloatData, %struct.btHingeConstraintFloatData* %19, i32 0, i32 8
  store float %call13, float* %m_lowerLimit, align 4, !tbaa !81
  %m_limit14 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call15 = call float @_ZNK14btAngularLimit7getHighEv(%class.btAngularLimit* %m_limit14)
  %20 = load %struct.btHingeConstraintFloatData*, %struct.btHingeConstraintFloatData** %hingeData, align 4, !tbaa !2
  %m_upperLimit = getelementptr inbounds %struct.btHingeConstraintFloatData, %struct.btHingeConstraintFloatData* %20, i32 0, i32 9
  store float %call15, float* %m_upperLimit, align 4, !tbaa !82
  %m_limit16 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call17 = call float @_ZNK14btAngularLimit11getSoftnessEv(%class.btAngularLimit* %m_limit16)
  %21 = load %struct.btHingeConstraintFloatData*, %struct.btHingeConstraintFloatData** %hingeData, align 4, !tbaa !2
  %m_limitSoftness = getelementptr inbounds %struct.btHingeConstraintFloatData, %struct.btHingeConstraintFloatData* %21, i32 0, i32 10
  store float %call17, float* %m_limitSoftness, align 4, !tbaa !83
  %m_limit18 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call19 = call float @_ZNK14btAngularLimit13getBiasFactorEv(%class.btAngularLimit* %m_limit18)
  %22 = load %struct.btHingeConstraintFloatData*, %struct.btHingeConstraintFloatData** %hingeData, align 4, !tbaa !2
  %m_biasFactor = getelementptr inbounds %struct.btHingeConstraintFloatData, %struct.btHingeConstraintFloatData* %22, i32 0, i32 11
  store float %call19, float* %m_biasFactor, align 4, !tbaa !84
  %m_limit20 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call21 = call float @_ZNK14btAngularLimit19getRelaxationFactorEv(%class.btAngularLimit* %m_limit20)
  %23 = load %struct.btHingeConstraintFloatData*, %struct.btHingeConstraintFloatData** %hingeData, align 4, !tbaa !2
  %m_relaxationFactor = getelementptr inbounds %struct.btHingeConstraintFloatData, %struct.btHingeConstraintFloatData* %23, i32 0, i32 12
  store float %call21, float* %m_relaxationFactor, align 4, !tbaa !85
  %24 = bitcast %struct.btHingeConstraintFloatData** %hingeData to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #8
  ret i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str, i32 0, i32 0)
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  store float* %_w, float** %_w.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = load float*, float** %_x.addr, align 4, !tbaa !2
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float*, float** %_z.addr, align 4, !tbaa !2
  %4 = load float*, float** %_w.addr, align 4, !tbaa !2
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4)
  ret %class.btQuaternion* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  store float* %_w, float** %_w.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !26
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !26
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !26
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !26
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !26
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !26
  %6 = load float*, float** %_w.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !26
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4, !tbaa !26
  ret %class.btQuadWord* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK12btQuaternionRK9btVector3(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q, %class.btVector3* nonnull align 4 dereferenceable(16) %w) #5 comdat {
entry:
  %q.addr = alloca %class.btQuaternion*, align 4
  %w.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  store %class.btVector3* %w, %class.btVector3** %w.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %2 = bitcast %class.btQuaternion* %1 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %2)
  %3 = load float, float* %call, align 4, !tbaa !26
  %4 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %4)
  %5 = load float, float* %call1, align 4, !tbaa !26
  %mul = fmul float %3, %5
  %6 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %7)
  %8 = load float, float* %call2, align 4, !tbaa !26
  %9 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %9)
  %10 = load float, float* %call3, align 4, !tbaa !26
  %mul4 = fmul float %8, %10
  %add = fadd float %mul, %mul4
  %11 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %12 = bitcast %class.btQuaternion* %11 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %12)
  %13 = load float, float* %call5, align 4, !tbaa !26
  %14 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %14)
  %15 = load float, float* %call6, align 4, !tbaa !26
  %mul7 = fmul float %13, %15
  %sub = fsub float %add, %mul7
  store float %sub, float* %ref.tmp, align 4, !tbaa !26
  %16 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  %17 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %18 = bitcast %class.btQuaternion* %17 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %18)
  %19 = load float, float* %call9, align 4, !tbaa !26
  %20 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %20)
  %21 = load float, float* %call10, align 4, !tbaa !26
  %mul11 = fmul float %19, %21
  %22 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %23 = bitcast %class.btQuaternion* %22 to %class.btQuadWord*
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %23)
  %24 = load float, float* %call12, align 4, !tbaa !26
  %25 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %25)
  %26 = load float, float* %call13, align 4, !tbaa !26
  %mul14 = fmul float %24, %26
  %add15 = fadd float %mul11, %mul14
  %27 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %28 = bitcast %class.btQuaternion* %27 to %class.btQuadWord*
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %28)
  %29 = load float, float* %call16, align 4, !tbaa !26
  %30 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %30)
  %31 = load float, float* %call17, align 4, !tbaa !26
  %mul18 = fmul float %29, %31
  %sub19 = fsub float %add15, %mul18
  store float %sub19, float* %ref.tmp8, align 4, !tbaa !26
  %32 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #8
  %33 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %34 = bitcast %class.btQuaternion* %33 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %34)
  %35 = load float, float* %call21, align 4, !tbaa !26
  %36 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %36)
  %37 = load float, float* %call22, align 4, !tbaa !26
  %mul23 = fmul float %35, %37
  %38 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %39 = bitcast %class.btQuaternion* %38 to %class.btQuadWord*
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %39)
  %40 = load float, float* %call24, align 4, !tbaa !26
  %41 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %41)
  %42 = load float, float* %call25, align 4, !tbaa !26
  %mul26 = fmul float %40, %42
  %add27 = fadd float %mul23, %mul26
  %43 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %44 = bitcast %class.btQuaternion* %43 to %class.btQuadWord*
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %44)
  %45 = load float, float* %call28, align 4, !tbaa !26
  %46 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %46)
  %47 = load float, float* %call29, align 4, !tbaa !26
  %mul30 = fmul float %45, %47
  %sub31 = fsub float %add27, %mul30
  store float %sub31, float* %ref.tmp20, align 4, !tbaa !26
  %48 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #8
  %49 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %50 = bitcast %class.btQuaternion* %49 to %class.btQuadWord*
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %50)
  %51 = load float, float* %call33, align 4, !tbaa !26
  %fneg = fneg float %51
  %52 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %52)
  %53 = load float, float* %call34, align 4, !tbaa !26
  %mul35 = fmul float %fneg, %53
  %54 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %55 = bitcast %class.btQuaternion* %54 to %class.btQuadWord*
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %55)
  %56 = load float, float* %call36, align 4, !tbaa !26
  %57 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %57)
  %58 = load float, float* %call37, align 4, !tbaa !26
  %mul38 = fmul float %56, %58
  %sub39 = fsub float %mul35, %mul38
  %59 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %60 = bitcast %class.btQuaternion* %59 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %60)
  %61 = load float, float* %call40, align 4, !tbaa !26
  %62 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %62)
  %63 = load float, float* %call41, align 4, !tbaa !26
  %mul42 = fmul float %61, %63
  %sub43 = fsub float %sub39, %mul42
  store float %sub43, float* %ref.tmp32, align 4, !tbaa !26
  %call44 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  %64 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #8
  %65 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #8
  %66 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #8
  %67 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #8
  ret void
}

define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp58 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %2, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  %3 = load float, float* %arrayidx, align 4, !tbaa !26
  %4 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %5 = bitcast %class.btQuaternion* %4 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %5)
  %6 = load float, float* %call, align 4, !tbaa !26
  %mul = fmul float %3, %6
  %7 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %7, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %8 = load float, float* %arrayidx3, align 4, !tbaa !26
  %9 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %10 = bitcast %class.btQuaternion* %9 to %class.btQuadWord*
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 3
  %11 = load float, float* %arrayidx5, align 4, !tbaa !26
  %mul6 = fmul float %8, %11
  %add = fadd float %mul, %mul6
  %12 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats7 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %12, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 1
  %13 = load float, float* %arrayidx8, align 4, !tbaa !26
  %14 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %15 = bitcast %class.btQuaternion* %14 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %15)
  %16 = load float, float* %call9, align 4, !tbaa !26
  %mul10 = fmul float %13, %16
  %add11 = fadd float %add, %mul10
  %17 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats12 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %17, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %18 = load float, float* %arrayidx13, align 4, !tbaa !26
  %19 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %20 = bitcast %class.btQuaternion* %19 to %class.btQuadWord*
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %20)
  %21 = load float, float* %call14, align 4, !tbaa !26
  %mul15 = fmul float %18, %21
  %sub = fsub float %add11, %mul15
  store float %sub, float* %ref.tmp, align 4, !tbaa !26
  %22 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #8
  %23 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats17 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %23, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 3
  %24 = load float, float* %arrayidx18, align 4, !tbaa !26
  %25 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %26 = bitcast %class.btQuaternion* %25 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %26)
  %27 = load float, float* %call19, align 4, !tbaa !26
  %mul20 = fmul float %24, %27
  %28 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats21 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %28, i32 0, i32 0
  %arrayidx22 = getelementptr inbounds [4 x float], [4 x float]* %m_floats21, i32 0, i32 1
  %29 = load float, float* %arrayidx22, align 4, !tbaa !26
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %m_floats23 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %31, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [4 x float], [4 x float]* %m_floats23, i32 0, i32 3
  %32 = load float, float* %arrayidx24, align 4, !tbaa !26
  %mul25 = fmul float %29, %32
  %add26 = fadd float %mul20, %mul25
  %33 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats27 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %33, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 2
  %34 = load float, float* %arrayidx28, align 4, !tbaa !26
  %35 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %36 = bitcast %class.btQuaternion* %35 to %class.btQuadWord*
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %36)
  %37 = load float, float* %call29, align 4, !tbaa !26
  %mul30 = fmul float %34, %37
  %add31 = fadd float %add26, %mul30
  %38 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats32 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %38, i32 0, i32 0
  %arrayidx33 = getelementptr inbounds [4 x float], [4 x float]* %m_floats32, i32 0, i32 0
  %39 = load float, float* %arrayidx33, align 4, !tbaa !26
  %40 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %41 = bitcast %class.btQuaternion* %40 to %class.btQuadWord*
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %41)
  %42 = load float, float* %call34, align 4, !tbaa !26
  %mul35 = fmul float %39, %42
  %sub36 = fsub float %add31, %mul35
  store float %sub36, float* %ref.tmp16, align 4, !tbaa !26
  %43 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #8
  %44 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats38 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %44, i32 0, i32 0
  %arrayidx39 = getelementptr inbounds [4 x float], [4 x float]* %m_floats38, i32 0, i32 3
  %45 = load float, float* %arrayidx39, align 4, !tbaa !26
  %46 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %47 = bitcast %class.btQuaternion* %46 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %47)
  %48 = load float, float* %call40, align 4, !tbaa !26
  %mul41 = fmul float %45, %48
  %49 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats42 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %49, i32 0, i32 0
  %arrayidx43 = getelementptr inbounds [4 x float], [4 x float]* %m_floats42, i32 0, i32 2
  %50 = load float, float* %arrayidx43, align 4, !tbaa !26
  %51 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %52 = bitcast %class.btQuaternion* %51 to %class.btQuadWord*
  %m_floats44 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %52, i32 0, i32 0
  %arrayidx45 = getelementptr inbounds [4 x float], [4 x float]* %m_floats44, i32 0, i32 3
  %53 = load float, float* %arrayidx45, align 4, !tbaa !26
  %mul46 = fmul float %50, %53
  %add47 = fadd float %mul41, %mul46
  %54 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats48 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %54, i32 0, i32 0
  %arrayidx49 = getelementptr inbounds [4 x float], [4 x float]* %m_floats48, i32 0, i32 0
  %55 = load float, float* %arrayidx49, align 4, !tbaa !26
  %56 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %57 = bitcast %class.btQuaternion* %56 to %class.btQuadWord*
  %call50 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %57)
  %58 = load float, float* %call50, align 4, !tbaa !26
  %mul51 = fmul float %55, %58
  %add52 = fadd float %add47, %mul51
  %59 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats53 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %59, i32 0, i32 0
  %arrayidx54 = getelementptr inbounds [4 x float], [4 x float]* %m_floats53, i32 0, i32 1
  %60 = load float, float* %arrayidx54, align 4, !tbaa !26
  %61 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %62 = bitcast %class.btQuaternion* %61 to %class.btQuadWord*
  %call55 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %62)
  %63 = load float, float* %call55, align 4, !tbaa !26
  %mul56 = fmul float %60, %63
  %sub57 = fsub float %add52, %mul56
  store float %sub57, float* %ref.tmp37, align 4, !tbaa !26
  %64 = bitcast float* %ref.tmp58 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #8
  %65 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats59 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %65, i32 0, i32 0
  %arrayidx60 = getelementptr inbounds [4 x float], [4 x float]* %m_floats59, i32 0, i32 3
  %66 = load float, float* %arrayidx60, align 4, !tbaa !26
  %67 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %68 = bitcast %class.btQuaternion* %67 to %class.btQuadWord*
  %m_floats61 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %68, i32 0, i32 0
  %arrayidx62 = getelementptr inbounds [4 x float], [4 x float]* %m_floats61, i32 0, i32 3
  %69 = load float, float* %arrayidx62, align 4, !tbaa !26
  %mul63 = fmul float %66, %69
  %70 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats64 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %70, i32 0, i32 0
  %arrayidx65 = getelementptr inbounds [4 x float], [4 x float]* %m_floats64, i32 0, i32 0
  %71 = load float, float* %arrayidx65, align 4, !tbaa !26
  %72 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %73 = bitcast %class.btQuaternion* %72 to %class.btQuadWord*
  %call66 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %73)
  %74 = load float, float* %call66, align 4, !tbaa !26
  %mul67 = fmul float %71, %74
  %sub68 = fsub float %mul63, %mul67
  %75 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats69 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %75, i32 0, i32 0
  %arrayidx70 = getelementptr inbounds [4 x float], [4 x float]* %m_floats69, i32 0, i32 1
  %76 = load float, float* %arrayidx70, align 4, !tbaa !26
  %77 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %78 = bitcast %class.btQuaternion* %77 to %class.btQuadWord*
  %call71 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %78)
  %79 = load float, float* %call71, align 4, !tbaa !26
  %mul72 = fmul float %76, %79
  %sub73 = fsub float %sub68, %mul72
  %80 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats74 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %80, i32 0, i32 0
  %arrayidx75 = getelementptr inbounds [4 x float], [4 x float]* %m_floats74, i32 0, i32 2
  %81 = load float, float* %arrayidx75, align 4, !tbaa !26
  %82 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %83 = bitcast %class.btQuaternion* %82 to %class.btQuadWord*
  %call76 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %83)
  %84 = load float, float* %call76, align 4, !tbaa !26
  %mul77 = fmul float %81, %84
  %sub78 = fsub float %sub73, %mul77
  store float %sub78, float* %ref.tmp58, align 4, !tbaa !26
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp37, float* nonnull align 4 dereferenceable(4) %ref.tmp58)
  %85 = bitcast float* %ref.tmp58 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #8
  %86 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #8
  %87 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #8
  %88 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #8
  ret %class.btQuaternion* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getXEv(%class.btQuadWord* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getYEv(%class.btQuadWord* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  store float* %_w, float** %_w.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !26
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !26
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !26
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !26
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !26
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !26
  %6 = load float*, float** %_w.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !26
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4, !tbaa !26
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !36
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !36
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4, !tbaa !26
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !26
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !26
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #5 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !24
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !24
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !24
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #5 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !26
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !26
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !26
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !26
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !26
  %mul8 = fmul float %7, %9
  store float %mul8, float* %ref.tmp3, align 4, !tbaa !26
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !26
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !26
  %mul14 = fmul float %12, %14
  store float %mul14, float* %ref.tmp9, align 4, !tbaa !26
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m) #5 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call, float* %ref.tmp, align 4, !tbaa !26
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call2, float* %ref.tmp1, align 4, !tbaa !26
  %6 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call4, float* %ref.tmp3, align 4, !tbaa !26
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %9 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_invInertiaTensorWorld = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 1
  ret %class.btMatrix3x3* %m_invInertiaTensorWorld
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !26
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !26
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !26
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !26
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !26
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !26
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !26
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !26
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !26
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !26
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !26
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !26
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !26
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !26
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !26
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !26
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !26
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !26
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZNK14btAngularLimit7isLimitEv(%class.btAngularLimit* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAngularLimit*, align 4
  store %class.btAngularLimit* %this, %class.btAngularLimit** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAngularLimit*, %class.btAngularLimit** %this.addr, align 4
  %m_solveLimit = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 7
  %0 = load i8, i8* %m_solveLimit, align 4, !tbaa !35, !range !21
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #5 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %3, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4, !tbaa !26
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !26
  %7 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %9, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %8, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !26
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4, !tbaa !26
  %13 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %15, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %14, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4, !tbaa !26
  %16 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4, !tbaa !26
  %19 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  %20 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %21 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %21, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %20, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4, !tbaa !26
  %22 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #8
  %23 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %24 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %24, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %23, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4, !tbaa !26
  %25 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  %26 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %27 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %27, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %26, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4, !tbaa !26
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  %28 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #8
  %29 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #8
  %30 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #8
  %31 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #8
  %32 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #8
  %33 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #8
  %34 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #8
  %35 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  %36 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %b.addr = alloca %class.btMatrix3x3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %b, %class.btMatrix3x3** %b.addr, align 4, !tbaa !2
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %b.addr, align 4, !tbaa !2
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !24
  ret %class.btTransform* %this1
}

declare float @_ZNK14btAngularLimit6getLowEv(%class.btAngularLimit*) #1

declare float @_ZNK14btAngularLimit7getHighEv(%class.btAngularLimit*) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !24
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !24
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !24
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: nounwind readnone
declare float @atan2f(float, float) #6

define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* %0)
  ret %class.btQuaternion* %this1
}

define linkonce_odr hidden void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %trace = alloca float, align 4
  %temp = alloca [4 x float], align 16
  %s = alloca float, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %s64 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast float* %trace to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %1 = load float, float* %call, align 4, !tbaa !26
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx3)
  %2 = load float, float* %call4, align 4, !tbaa !26
  %add = fadd float %1, %2
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx6)
  %3 = load float, float* %call7, align 4, !tbaa !26
  %add8 = fadd float %add, %3
  store float %add8, float* %trace, align 4, !tbaa !26
  %4 = bitcast [4 x float]* %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #8
  %5 = load float, float* %trace, align 4, !tbaa !26
  %cmp = fcmp ogt float %5, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %6 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load float, float* %trace, align 4, !tbaa !26
  %add9 = fadd float %7, 1.000000e+00
  %call10 = call float @_Z6btSqrtf(float %add9)
  store float %call10, float* %s, align 4, !tbaa !26
  %8 = load float, float* %s, align 4, !tbaa !26
  %mul = fmul float %8, 5.000000e-01
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  store float %mul, float* %arrayidx11, align 4, !tbaa !26
  %9 = load float, float* %s, align 4, !tbaa !26
  %div = fdiv float 5.000000e-01, %9
  store float %div, float* %s, align 4, !tbaa !26
  %m_el12 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el12, i32 0, i32 2
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx13)
  %10 = load float, float* %call14, align 4, !tbaa !26
  %m_el15 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el15, i32 0, i32 1
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx16)
  %11 = load float, float* %call17, align 4, !tbaa !26
  %sub = fsub float %10, %11
  %12 = load float, float* %s, align 4, !tbaa !26
  %mul18 = fmul float %sub, %12
  %arrayidx19 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 0
  store float %mul18, float* %arrayidx19, align 16, !tbaa !26
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 0
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %13 = load float, float* %call22, align 4, !tbaa !26
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx24)
  %14 = load float, float* %call25, align 4, !tbaa !26
  %sub26 = fsub float %13, %14
  %15 = load float, float* %s, align 4, !tbaa !26
  %mul27 = fmul float %sub26, %15
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 1
  store float %mul27, float* %arrayidx28, align 4, !tbaa !26
  %m_el29 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el29, i32 0, i32 1
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx30)
  %16 = load float, float* %call31, align 4, !tbaa !26
  %m_el32 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx33 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el32, i32 0, i32 0
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx33)
  %17 = load float, float* %call34, align 4, !tbaa !26
  %sub35 = fsub float %16, %17
  %18 = load float, float* %s, align 4, !tbaa !26
  %mul36 = fmul float %sub35, %18
  %arrayidx37 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 2
  store float %mul36, float* %arrayidx37, align 8, !tbaa !26
  %19 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  br label %if.end

if.else:                                          ; preds = %entry
  %20 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #8
  %m_el38 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx39 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el38, i32 0, i32 0
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx39)
  %21 = load float, float* %call40, align 4, !tbaa !26
  %m_el41 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx42 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el41, i32 0, i32 1
  %call43 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx42)
  %22 = load float, float* %call43, align 4, !tbaa !26
  %cmp44 = fcmp olt float %21, %22
  br i1 %cmp44, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else
  %m_el45 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx46 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el45, i32 0, i32 1
  %call47 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx46)
  %23 = load float, float* %call47, align 4, !tbaa !26
  %m_el48 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx49 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el48, i32 0, i32 2
  %call50 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx49)
  %24 = load float, float* %call50, align 4, !tbaa !26
  %cmp51 = fcmp olt float %23, %24
  %25 = zext i1 %cmp51 to i64
  %cond = select i1 %cmp51, i32 2, i32 1
  br label %cond.end

cond.false:                                       ; preds = %if.else
  %m_el52 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx53 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el52, i32 0, i32 0
  %call54 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx53)
  %26 = load float, float* %call54, align 4, !tbaa !26
  %m_el55 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx56 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el55, i32 0, i32 2
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx56)
  %27 = load float, float* %call57, align 4, !tbaa !26
  %cmp58 = fcmp olt float %26, %27
  %28 = zext i1 %cmp58 to i64
  %cond59 = select i1 %cmp58, i32 2, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond60 = phi i32 [ %cond, %cond.true ], [ %cond59, %cond.false ]
  store i32 %cond60, i32* %i, align 4, !tbaa !36
  %29 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #8
  %30 = load i32, i32* %i, align 4, !tbaa !36
  %add61 = add nsw i32 %30, 1
  %rem = srem i32 %add61, 3
  store i32 %rem, i32* %j, align 4, !tbaa !36
  %31 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #8
  %32 = load i32, i32* %i, align 4, !tbaa !36
  %add62 = add nsw i32 %32, 2
  %rem63 = srem i32 %add62, 3
  store i32 %rem63, i32* %k, align 4, !tbaa !36
  %33 = bitcast float* %s64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #8
  %m_el65 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %34 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx66 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el65, i32 0, i32 %34
  %call67 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx66)
  %35 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx68 = getelementptr inbounds float, float* %call67, i32 %35
  %36 = load float, float* %arrayidx68, align 4, !tbaa !26
  %m_el69 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %37 = load i32, i32* %j, align 4, !tbaa !36
  %arrayidx70 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el69, i32 0, i32 %37
  %call71 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx70)
  %38 = load i32, i32* %j, align 4, !tbaa !36
  %arrayidx72 = getelementptr inbounds float, float* %call71, i32 %38
  %39 = load float, float* %arrayidx72, align 4, !tbaa !26
  %sub73 = fsub float %36, %39
  %m_el74 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %40 = load i32, i32* %k, align 4, !tbaa !36
  %arrayidx75 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el74, i32 0, i32 %40
  %call76 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx75)
  %41 = load i32, i32* %k, align 4, !tbaa !36
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 %41
  %42 = load float, float* %arrayidx77, align 4, !tbaa !26
  %sub78 = fsub float %sub73, %42
  %add79 = fadd float %sub78, 1.000000e+00
  %call80 = call float @_Z6btSqrtf(float %add79)
  store float %call80, float* %s64, align 4, !tbaa !26
  %43 = load float, float* %s64, align 4, !tbaa !26
  %mul81 = fmul float %43, 5.000000e-01
  %44 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx82 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %44
  store float %mul81, float* %arrayidx82, align 4, !tbaa !26
  %45 = load float, float* %s64, align 4, !tbaa !26
  %div83 = fdiv float 5.000000e-01, %45
  store float %div83, float* %s64, align 4, !tbaa !26
  %m_el84 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %46 = load i32, i32* %k, align 4, !tbaa !36
  %arrayidx85 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el84, i32 0, i32 %46
  %call86 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx85)
  %47 = load i32, i32* %j, align 4, !tbaa !36
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 %47
  %48 = load float, float* %arrayidx87, align 4, !tbaa !26
  %m_el88 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %49 = load i32, i32* %j, align 4, !tbaa !36
  %arrayidx89 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el88, i32 0, i32 %49
  %call90 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx89)
  %50 = load i32, i32* %k, align 4, !tbaa !36
  %arrayidx91 = getelementptr inbounds float, float* %call90, i32 %50
  %51 = load float, float* %arrayidx91, align 4, !tbaa !26
  %sub92 = fsub float %48, %51
  %52 = load float, float* %s64, align 4, !tbaa !26
  %mul93 = fmul float %sub92, %52
  %arrayidx94 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  store float %mul93, float* %arrayidx94, align 4, !tbaa !26
  %m_el95 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %53 = load i32, i32* %j, align 4, !tbaa !36
  %arrayidx96 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el95, i32 0, i32 %53
  %call97 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx96)
  %54 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx98 = getelementptr inbounds float, float* %call97, i32 %54
  %55 = load float, float* %arrayidx98, align 4, !tbaa !26
  %m_el99 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %56 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx100 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el99, i32 0, i32 %56
  %call101 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx100)
  %57 = load i32, i32* %j, align 4, !tbaa !36
  %arrayidx102 = getelementptr inbounds float, float* %call101, i32 %57
  %58 = load float, float* %arrayidx102, align 4, !tbaa !26
  %add103 = fadd float %55, %58
  %59 = load float, float* %s64, align 4, !tbaa !26
  %mul104 = fmul float %add103, %59
  %60 = load i32, i32* %j, align 4, !tbaa !36
  %arrayidx105 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %60
  store float %mul104, float* %arrayidx105, align 4, !tbaa !26
  %m_el106 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %61 = load i32, i32* %k, align 4, !tbaa !36
  %arrayidx107 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el106, i32 0, i32 %61
  %call108 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx107)
  %62 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx109 = getelementptr inbounds float, float* %call108, i32 %62
  %63 = load float, float* %arrayidx109, align 4, !tbaa !26
  %m_el110 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %64 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx111 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el110, i32 0, i32 %64
  %call112 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx111)
  %65 = load i32, i32* %k, align 4, !tbaa !36
  %arrayidx113 = getelementptr inbounds float, float* %call112, i32 %65
  %66 = load float, float* %arrayidx113, align 4, !tbaa !26
  %add114 = fadd float %63, %66
  %67 = load float, float* %s64, align 4, !tbaa !26
  %mul115 = fmul float %add114, %67
  %68 = load i32, i32* %k, align 4, !tbaa !36
  %arrayidx116 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %68
  store float %mul115, float* %arrayidx116, align 4, !tbaa !26
  %69 = bitcast float* %s64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #8
  %70 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #8
  %71 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #8
  %72 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #8
  br label %if.end

if.end:                                           ; preds = %cond.end, %if.then
  %73 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %74 = bitcast %class.btQuaternion* %73 to %class.btQuadWord*
  %arrayidx117 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 0
  %arrayidx118 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 1
  %arrayidx119 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 2
  %arrayidx120 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %74, float* nonnull align 4 dereferenceable(4) %arrayidx117, float* nonnull align 4 dereferenceable(4) %arrayidx118, float* nonnull align 4 dereferenceable(4) %arrayidx119, float* nonnull align 4 dereferenceable(4) %arrayidx120)
  %75 = bitcast [4 x float]* %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %75) #8
  %76 = bitcast float* %trace to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  ret %class.btQuadWord* %this1
}

define linkonce_odr hidden float @_ZNK12btQuaternion6lengthEv(%class.btQuaternion* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaterniondVERKf(%class.btQuaternion* %this, float* nonnull align 4 dereferenceable(4) %s) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !26
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !26
  %call = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKf(%class.btQuaternion* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  ret %class.btQuaternion* %call
}

define linkonce_odr hidden float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !26
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call, align 4, !tbaa !26
  %mul = fmul float %1, %4
  %5 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %5, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %6 = load float, float* %arrayidx3, align 4, !tbaa !26
  %7 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %8 = bitcast %class.btQuaternion* %7 to %class.btQuadWord*
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %8)
  %9 = load float, float* %call4, align 4, !tbaa !26
  %mul5 = fmul float %6, %9
  %add = fadd float %mul, %mul5
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %11 = load float, float* %arrayidx7, align 4, !tbaa !26
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %13)
  %14 = load float, float* %call8, align 4, !tbaa !26
  %mul9 = fmul float %11, %14
  %add10 = fadd float %add, %mul9
  %15 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats11 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %15, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 3
  %16 = load float, float* %arrayidx12, align 4, !tbaa !26
  %17 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %18 = bitcast %class.btQuaternion* %17 to %class.btQuadWord*
  %m_floats13 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %18, i32 0, i32 0
  %arrayidx14 = getelementptr inbounds [4 x float], [4 x float]* %m_floats13, i32 0, i32 3
  %19 = load float, float* %arrayidx14, align 4, !tbaa !26
  %mul15 = fmul float %16, %19
  %add16 = fadd float %add10, %mul15
  ret float %add16
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKf(%class.btQuaternion* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %s.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !26
  %2 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %2, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %3 = load float, float* %arrayidx, align 4, !tbaa !26
  %mul = fmul float %3, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !26
  %4 = load float*, float** %s.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !26
  %6 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !26
  %mul4 = fmul float %7, %5
  store float %mul4, float* %arrayidx3, align 4, !tbaa !26
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !26
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats5 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %11 = load float, float* %arrayidx6, align 4, !tbaa !26
  %mul7 = fmul float %11, %9
  store float %mul7, float* %arrayidx6, align 4, !tbaa !26
  %12 = load float*, float** %s.addr, align 4, !tbaa !2
  %13 = load float, float* %12, align 4, !tbaa !26
  %14 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats8 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %14, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 3
  %15 = load float, float* %arrayidx9, align 4, !tbaa !26
  %mul10 = fmul float %15, %13
  store float %mul10, float* %arrayidx9, align 4, !tbaa !26
  ret %class.btQuaternion* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btAcosf(float %x) #3 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !26
  %0 = load float, float* %x.addr, align 4, !tbaa !26
  %cmp = fcmp olt float %0, -1.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store float -1.000000e+00, float* %x.addr, align 4, !tbaa !26
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %1 = load float, float* %x.addr, align 4, !tbaa !26
  %cmp1 = fcmp ogt float %1, 1.000000e+00
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store float 1.000000e+00, float* %x.addr, align 4, !tbaa !26
  br label %if.end3

if.end3:                                          ; preds = %if.then2, %if.end
  %2 = load float, float* %x.addr, align 4, !tbaa !26
  %call = call float @acosf(float %2) #9
  ret float %call
}

; Function Attrs: nounwind readnone
declare float @acosf(float) #6

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #7

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN17btHingeConstraintdlEPv(i8* %ptr) #3 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

declare i8* @_ZNK17btTypedConstraint9serializeEPvP12btSerializer(%class.btTypedConstraint*, i8*, %class.btSerializer*) unnamed_addr #1

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %this, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %dataOut) #5 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %dataOut.addr = alloca %struct.btTransformFloatData*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %struct.btTransformFloatData* %dataOut, %struct.btTransformFloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %struct.btTransformFloatData*, %struct.btTransformFloatData** %dataOut.addr, align 4, !tbaa !2
  %m_basis2 = getelementptr inbounds %struct.btTransformFloatData, %struct.btTransformFloatData* %0, i32 0, i32 0
  call void @_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData(%class.btMatrix3x3* %m_basis, %struct.btMatrix3x3FloatData* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %struct.btTransformFloatData*, %struct.btTransformFloatData** %dataOut.addr, align 4, !tbaa !2
  %m_origin3 = getelementptr inbounds %struct.btTransformFloatData, %struct.btTransformFloatData* %1, i32 0, i32 1
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_origin, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_origin3)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK14btAngularLimit11getSoftnessEv(%class.btAngularLimit* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAngularLimit*, align 4
  store %class.btAngularLimit* %this, %class.btAngularLimit** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAngularLimit*, %class.btAngularLimit** %this.addr, align 4
  %m_softness = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 2
  %0 = load float, float* %m_softness, align 4, !tbaa !30
  ret float %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData(%class.btMatrix3x3* %this, %struct.btMatrix3x3FloatData* nonnull align 4 dereferenceable(48) %dataOut) #5 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %dataOut.addr = alloca %struct.btMatrix3x3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %struct.btMatrix3x3FloatData* %dataOut, %struct.btMatrix3x3FloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %i, align 4, !tbaa !36
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !36
  %cmp = icmp slt i32 %1, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %3 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %3
  %4 = load %struct.btMatrix3x3FloatData*, %struct.btMatrix3x3FloatData** %dataOut.addr, align 4, !tbaa !2
  %m_el2 = getelementptr inbounds %struct.btMatrix3x3FloatData, %struct.btMatrix3x3FloatData* %4, i32 0, i32 0
  %5 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx3 = getelementptr inbounds [3 x %struct.btVector3FloatData], [3 x %struct.btVector3FloatData]* %m_el2, i32 0, i32 %5
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %arrayidx, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %arrayidx3)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !36
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !36
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %i, align 4, !tbaa !36
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !36
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %3 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %3
  %4 = load float, float* %arrayidx, align 4, !tbaa !26
  %5 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %5, i32 0, i32 0
  %6 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %6
  store float %4, float* %arrayidx3, align 4, !tbaa !26
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !36
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !36
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btFabsf(float %x) #3 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !26
  %0 = load float, float* %x.addr, align 4, !tbaa !26
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #7

define internal void @_GLOBAL__sub_I_btHingeConstraint.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind readnone speculatable willreturn }
attributes #8 = { nounwind }
attributes #9 = { nounwind readnone }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"bool", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"vtable pointer", !5, i64 0}
!10 = !{!11, !7, i64 736}
!11 = !{!"_ZTS17btHingeConstraint", !4, i64 48, !4, i64 300, !12, i64 552, !12, i64 616, !15, i64 680, !15, i64 684, !16, i64 688, !15, i64 720, !15, i64 724, !15, i64 728, !15, i64 732, !7, i64 736, !7, i64 737, !7, i64 738, !7, i64 739, !7, i64 740, !15, i64 744, !17, i64 748, !15, i64 752, !15, i64 756, !15, i64 760}
!12 = !{!"_ZTS11btTransform", !13, i64 0, !14, i64 48}
!13 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!14 = !{!"_ZTS9btVector3", !4, i64 0}
!15 = !{!"float", !4, i64 0}
!16 = !{!"_ZTS14btAngularLimit", !15, i64 0, !15, i64 4, !15, i64 8, !15, i64 12, !15, i64 16, !15, i64 20, !15, i64 24, !7, i64 28}
!17 = !{!"int", !4, i64 0}
!18 = !{!11, !7, i64 737}
!19 = !{!11, !7, i64 738}
!20 = !{!11, !7, i64 739}
!21 = !{i8 0, i8 2}
!22 = !{!11, !7, i64 740}
!23 = !{!11, !17, i64 748}
!24 = !{i64 0, i64 16, !25}
!25 = !{!4, !4, i64 0}
!26 = !{!15, !15, i64 0}
!27 = !{!11, !15, i64 732}
!28 = !{!16, !15, i64 0}
!29 = !{!16, !15, i64 4}
!30 = !{!16, !15, i64 8}
!31 = !{!16, !15, i64 12}
!32 = !{!16, !15, i64 16}
!33 = !{!16, !15, i64 20}
!34 = !{!16, !15, i64 24}
!35 = !{!16, !7, i64 28}
!36 = !{!17, !17, i64 0}
!37 = !{!38, !3, i64 28}
!38 = !{!"_ZTS17btTypedConstraint", !17, i64 8, !4, i64 12, !15, i64 16, !7, i64 20, !7, i64 21, !17, i64 24, !3, i64 28, !3, i64 32, !15, i64 36, !15, i64 40, !3, i64 44}
!39 = !{!38, !15, i64 36}
!40 = !{!11, !15, i64 744}
!41 = !{!38, !3, i64 32}
!42 = !{!11, !15, i64 724}
!43 = !{!11, !15, i64 720}
!44 = !{!45, !15, i64 344}
!45 = !{!"_ZTS11btRigidBody", !13, i64 264, !14, i64 312, !14, i64 328, !15, i64 344, !14, i64 348, !14, i64 364, !14, i64 380, !14, i64 396, !14, i64 412, !14, i64 428, !15, i64 444, !15, i64 448, !7, i64 452, !15, i64 456, !15, i64 460, !15, i64 464, !15, i64 468, !15, i64 472, !15, i64 476, !3, i64 480, !46, i64 484, !17, i64 504, !17, i64 508, !14, i64 512, !14, i64 528, !14, i64 544, !14, i64 560, !14, i64 576, !14, i64 592, !17, i64 608, !17, i64 612}
!46 = !{!"_ZTS20btAlignedObjectArrayIP17btTypedConstraintE", !47, i64 0, !17, i64 4, !17, i64 8, !3, i64 12, !7, i64 16}
!47 = !{!"_ZTS18btAlignedAllocatorIP17btTypedConstraintLj16EE"}
!48 = !{!49, !15, i64 80}
!49 = !{!"_ZTS15btJacobianEntry", !14, i64 0, !14, i64 16, !14, i64 32, !14, i64 48, !14, i64 64, !15, i64 80}
!50 = !{!11, !15, i64 728}
!51 = !{!52, !17, i64 0}
!52 = !{!"_ZTSN17btTypedConstraint17btConstraintInfo1E", !17, i64 0, !17, i64 4}
!53 = !{!52, !17, i64 4}
!54 = !{!55, !17, i64 24}
!55 = !{!"_ZTSN17btTypedConstraint17btConstraintInfo2E", !15, i64 0, !15, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !17, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !3, i64 44, !17, i64 48, !15, i64 52}
!56 = !{!55, !3, i64 12}
!57 = !{!55, !3, i64 20}
!58 = !{!55, !15, i64 0}
!59 = !{!55, !15, i64 4}
!60 = !{!55, !3, i64 8}
!61 = !{!55, !3, i64 16}
!62 = !{!55, !3, i64 28}
!63 = !{!11, !15, i64 760}
!64 = !{!11, !15, i64 752}
!65 = !{!55, !3, i64 32}
!66 = !{!11, !15, i64 680}
!67 = !{!11, !15, i64 684}
!68 = !{!55, !3, i64 36}
!69 = !{!55, !3, i64 40}
!70 = !{!11, !15, i64 756}
!71 = !{!72, !17, i64 184}
!72 = !{!"_ZTS26btHingeConstraintFloatData", !73, i64 0, !74, i64 52, !74, i64 116, !17, i64 180, !17, i64 184, !17, i64 188, !15, i64 192, !15, i64 196, !15, i64 200, !15, i64 204, !15, i64 208, !15, i64 212, !15, i64 216}
!73 = !{!"_ZTS21btTypedConstraintData", !3, i64 0, !3, i64 4, !3, i64 8, !17, i64 12, !17, i64 16, !17, i64 20, !17, i64 24, !15, i64 28, !15, i64 32, !17, i64 36, !17, i64 40, !15, i64 44, !17, i64 48}
!74 = !{!"_ZTS20btTransformFloatData", !75, i64 0, !76, i64 48}
!75 = !{!"_ZTS20btMatrix3x3FloatData", !4, i64 0}
!76 = !{!"_ZTS18btVector3FloatData", !4, i64 0}
!77 = !{!72, !17, i64 188}
!78 = !{!72, !15, i64 196}
!79 = !{!72, !15, i64 192}
!80 = !{!72, !17, i64 180}
!81 = !{!72, !15, i64 200}
!82 = !{!72, !15, i64 204}
!83 = !{!72, !15, i64 208}
!84 = !{!72, !15, i64 212}
!85 = !{!72, !15, i64 216}
