; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btMultimaterialTriangleMeshShape.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btMultimaterialTriangleMeshShape.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btMaterial = type { float, float, [2 x i32] }
%class.btMultimaterialTriangleMeshShape = type { %class.btBvhTriangleMeshShape.base, [3 x i8], %class.btAlignedObjectArray.20, i32** }
%class.btBvhTriangleMeshShape.base = type <{ %class.btTriangleMeshShape, %class.btOptimizedBvh*, %struct.btTriangleInfoMap*, i8, i8, [11 x i8] }>
%class.btTriangleMeshShape = type { %class.btConcaveShape, %class.btVector3, %class.btVector3, %class.btStridingMeshInterface* }
%class.btConcaveShape = type { %class.btCollisionShape, float }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%class.btVector3 = type { [4 x float] }
%class.btStridingMeshInterface = type { i32 (...)**, %class.btVector3 }
%class.btOptimizedBvh = type { %class.btQuantizedBvh }
%class.btQuantizedBvh = type { i32 (...)**, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32, i8, [3 x i8], %class.btAlignedObjectArray, %class.btAlignedObjectArray, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0, i32, %class.btAlignedObjectArray.4, i32 }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btOptimizedBvhNode*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btOptimizedBvhNode = type { %class.btVector3, %class.btVector3, i32, i32, i32, [20 x i8] }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.btQuantizedBvhNode*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btQuantizedBvhNode = type { [3 x i16], [3 x i16], i32 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %class.btBvhSubtreeInfo*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btBvhSubtreeInfo = type { [3 x i16], [3 x i16], i32, i32, [3 x i32] }
%struct.btTriangleInfoMap = type { i32 (...)**, %class.btHashMap, float, float, float, float, float, float }
%class.btHashMap = type { %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.16 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, %struct.btTriangleInfo*, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%struct.btTriangleInfo = type { i32, float, float, float }
%class.btAlignedObjectArray.16 = type <{ %class.btAlignedAllocator.17, [3 x i8], i32, i32, %class.btHashInt*, i8, [3 x i8] }>
%class.btAlignedAllocator.17 = type { i8 }
%class.btHashInt = type { i32 }
%class.btAlignedObjectArray.20 = type <{ %class.btAlignedAllocator.21, [3 x i8], i32, i32, %class.btMaterial**, i8, [3 x i8] }>
%class.btAlignedAllocator.21 = type { i8 }
%class.btTriangleIndexVertexMaterialArray = type { %class.btTriangleIndexVertexArray, %class.btAlignedObjectArray.28 }
%class.btTriangleIndexVertexArray = type { %class.btStridingMeshInterface, %class.btAlignedObjectArray.24, [2 x i32], i32, %class.btVector3, %class.btVector3 }
%class.btAlignedObjectArray.24 = type <{ %class.btAlignedAllocator.25, [3 x i8], i32, i32, %struct.btIndexedMesh*, i8, [3 x i8] }>
%class.btAlignedAllocator.25 = type { i8 }
%struct.btIndexedMesh = type { i32, i8*, i32, i32, i8*, i32, i32, i32 }
%class.btAlignedObjectArray.28 = type <{ %class.btAlignedAllocator.29, [3 x i8], i32, i32, %struct.btMaterialProperties*, i8, [3 x i8] }>
%class.btAlignedAllocator.29 = type { i8 }
%struct.btMaterialProperties = type { i32, i8*, i32, i32, i32, i8*, i32, i32 }

define hidden %class.btMaterial* @_ZN32btMultimaterialTriangleMeshShape21getMaterialPropertiesEii(%class.btMultimaterialTriangleMeshShape* %this, i32 %partID, i32 %triIndex) #0 {
entry:
  %this.addr = alloca %class.btMultimaterialTriangleMeshShape*, align 4
  %partID.addr = alloca i32, align 4
  %triIndex.addr = alloca i32, align 4
  %materialBase = alloca i8*, align 4
  %numMaterials = alloca i32, align 4
  %materialType = alloca i32, align 4
  %materialStride = alloca i32, align 4
  %triangleMaterialBase = alloca i8*, align 4
  %numTriangles = alloca i32, align 4
  %triangleMaterialStride = alloca i32, align 4
  %triangleType = alloca i32, align 4
  %matInd = alloca i32*, align 4
  %matVal = alloca %class.btMaterial*, align 4
  store %class.btMultimaterialTriangleMeshShape* %this, %class.btMultimaterialTriangleMeshShape** %this.addr, align 4, !tbaa !2
  store i32 %partID, i32* %partID.addr, align 4, !tbaa !6
  store i32 %triIndex, i32* %triIndex.addr, align 4, !tbaa !6
  %this1 = load %class.btMultimaterialTriangleMeshShape*, %class.btMultimaterialTriangleMeshShape** %this.addr, align 4
  %0 = bitcast i8** %materialBase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  store i8* null, i8** %materialBase, align 4, !tbaa !2
  %1 = bitcast i32* %numMaterials to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  %2 = bitcast i32* %materialType to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #2
  %3 = bitcast i32* %materialStride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #2
  %4 = bitcast i8** %triangleMaterialBase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  store i8* null, i8** %triangleMaterialBase, align 4, !tbaa !2
  %5 = bitcast i32* %numTriangles to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  %6 = bitcast i32* %triangleMaterialStride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #2
  %7 = bitcast i32* %triangleType to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #2
  %8 = bitcast %class.btMultimaterialTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %m_meshInterface = getelementptr inbounds %class.btTriangleMeshShape, %class.btTriangleMeshShape* %8, i32 0, i32 3
  %9 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface, align 4, !tbaa !8
  %10 = bitcast %class.btStridingMeshInterface* %9 to %class.btTriangleIndexVertexMaterialArray*
  %11 = load i32, i32* %partID.addr, align 4, !tbaa !6
  %12 = bitcast %class.btTriangleIndexVertexMaterialArray* %10 to void (%class.btTriangleIndexVertexMaterialArray*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)***
  %vtable = load void (%class.btTriangleIndexVertexMaterialArray*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)**, void (%class.btTriangleIndexVertexMaterialArray*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*** %12, align 4, !tbaa !11
  %vfn = getelementptr inbounds void (%class.btTriangleIndexVertexMaterialArray*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btTriangleIndexVertexMaterialArray*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vtable, i64 16
  %13 = load void (%class.btTriangleIndexVertexMaterialArray*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btTriangleIndexVertexMaterialArray*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vfn, align 4
  call void %13(%class.btTriangleIndexVertexMaterialArray* %10, i8** %materialBase, i32* nonnull align 4 dereferenceable(4) %numMaterials, i32* nonnull align 4 dereferenceable(4) %materialType, i32* nonnull align 4 dereferenceable(4) %materialStride, i8** %triangleMaterialBase, i32* nonnull align 4 dereferenceable(4) %numTriangles, i32* nonnull align 4 dereferenceable(4) %triangleMaterialStride, i32* nonnull align 4 dereferenceable(4) %triangleType, i32 %11)
  %14 = bitcast i32** %matInd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #2
  %15 = load i8*, i8** %triangleMaterialBase, align 4, !tbaa !2
  %16 = load i32, i32* %triIndex.addr, align 4, !tbaa !6
  %17 = load i32, i32* %triangleMaterialStride, align 4, !tbaa !6
  %mul = mul nsw i32 %16, %17
  %arrayidx = getelementptr inbounds i8, i8* %15, i32 %mul
  %18 = bitcast i8* %arrayidx to i32*
  store i32* %18, i32** %matInd, align 4, !tbaa !2
  %19 = bitcast %class.btMaterial** %matVal to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #2
  %20 = load i8*, i8** %materialBase, align 4, !tbaa !2
  %21 = load i32*, i32** %matInd, align 4, !tbaa !2
  %22 = load i32, i32* %21, align 4, !tbaa !6
  %23 = load i32, i32* %materialStride, align 4, !tbaa !6
  %mul2 = mul nsw i32 %22, %23
  %arrayidx3 = getelementptr inbounds i8, i8* %20, i32 %mul2
  %24 = bitcast i8* %arrayidx3 to %class.btMaterial*
  store %class.btMaterial* %24, %class.btMaterial** %matVal, align 4, !tbaa !2
  %25 = load %class.btMaterial*, %class.btMaterial** %matVal, align 4, !tbaa !2
  %26 = bitcast %class.btMaterial** %matVal to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #2
  %27 = bitcast i32** %matInd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #2
  %28 = bitcast i32* %triangleType to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #2
  %29 = bitcast i32* %triangleMaterialStride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #2
  %30 = bitcast i32* %numTriangles to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #2
  %31 = bitcast i8** %triangleMaterialBase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #2
  %32 = bitcast i32* %materialStride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #2
  %33 = bitcast i32* %materialType to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #2
  %34 = bitcast i32* %numMaterials to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #2
  %35 = bitcast i8** %materialBase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #2
  ret %class.btMaterial* %25
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !3, i64 48}
!9 = !{!"_ZTS19btTriangleMeshShape", !10, i64 16, !10, i64 32, !3, i64 48}
!10 = !{!"_ZTS9btVector3", !4, i64 0}
!11 = !{!12, !12, i64 0}
!12 = !{!"vtable pointer", !5, i64 0}
