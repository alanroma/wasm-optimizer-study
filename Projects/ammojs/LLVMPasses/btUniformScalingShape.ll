; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btUniformScalingShape.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btUniformScalingShape.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btUniformScalingShape = type { %class.btConvexShape, %class.btConvexShape*, float }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%class.btVector3 = type { [4 x float] }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btSerializer = type opaque

$_ZN21btUniformScalingShapedlEPv = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZmlRK9btVector3RK11btMatrix3x3 = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK21btUniformScalingShape7getNameEv = comdat any

$_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZNK16btCollisionShape28calculateSerializeBufferSizeEv = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

@_ZTV21btUniformScalingShape = hidden unnamed_addr constant { [25 x i8*] } { [25 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI21btUniformScalingShape to i8*), i8* bitcast (%class.btUniformScalingShape* (%class.btUniformScalingShape*)* @_ZN21btUniformScalingShapeD1Ev to i8*), i8* bitcast (void (%class.btUniformScalingShape*)* @_ZN21btUniformScalingShapeD0Ev to i8*), i8* bitcast (void (%class.btUniformScalingShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btUniformScalingShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btUniformScalingShape*, %class.btVector3*)* @_ZN21btUniformScalingShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btUniformScalingShape*)* @_ZNK21btUniformScalingShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btUniformScalingShape*, float, %class.btVector3*)* @_ZNK21btUniformScalingShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btUniformScalingShape*)* @_ZNK21btUniformScalingShape7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btUniformScalingShape*, float)* @_ZN21btUniformScalingShape9setMarginEf to i8*), i8* bitcast (float (%class.btUniformScalingShape*)* @_ZNK21btUniformScalingShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btCollisionShape*)* @_ZNK16btCollisionShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btCollisionShape*, i8*, %class.btSerializer*)* @_ZNK16btCollisionShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btVector3*, %class.btUniformScalingShape*, %class.btVector3*)* @_ZNK21btUniformScalingShape24localGetSupportingVertexERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btUniformScalingShape*, %class.btVector3*)* @_ZNK21btUniformScalingShape37localGetSupportingVertexWithoutMarginERK9btVector3 to i8*), i8* bitcast (void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, float*, float*)* @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_ to i8*), i8* bitcast (void (%class.btUniformScalingShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK21btUniformScalingShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i to i8*), i8* bitcast (void (%class.btUniformScalingShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btUniformScalingShape11getAabbSlowERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (i32 (%class.btUniformScalingShape*)* @_ZNK21btUniformScalingShape36getNumPreferredPenetrationDirectionsEv to i8*), i8* bitcast (void (%class.btUniformScalingShape*, i32, %class.btVector3*)* @_ZNK21btUniformScalingShape32getPreferredPenetrationDirectionEiR9btVector3 to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS21btUniformScalingShape = hidden constant [24 x i8] c"21btUniformScalingShape\00", align 1
@_ZTI13btConvexShape = external constant i8*
@_ZTI21btUniformScalingShape = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([24 x i8], [24 x i8]* @_ZTS21btUniformScalingShape, i32 0, i32 0), i8* bitcast (i8** @_ZTI13btConvexShape to i8*) }, align 4
@.str = private unnamed_addr constant [20 x i8] c"UniformScalingShape\00", align 1

@_ZN21btUniformScalingShapeC1EP13btConvexShapef = hidden unnamed_addr alias %class.btUniformScalingShape* (%class.btUniformScalingShape*, %class.btConvexShape*, float), %class.btUniformScalingShape* (%class.btUniformScalingShape*, %class.btConvexShape*, float)* @_ZN21btUniformScalingShapeC2EP13btConvexShapef
@_ZN21btUniformScalingShapeD1Ev = hidden unnamed_addr alias %class.btUniformScalingShape* (%class.btUniformScalingShape*), %class.btUniformScalingShape* (%class.btUniformScalingShape*)* @_ZN21btUniformScalingShapeD2Ev

define hidden %class.btUniformScalingShape* @_ZN21btUniformScalingShapeC2EP13btConvexShapef(%class.btUniformScalingShape* returned %this, %class.btConvexShape* %convexChildShape, float %uniformScalingFactor) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  %convexChildShape.addr = alloca %class.btConvexShape*, align 4
  %uniformScalingFactor.addr = alloca float, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4, !tbaa !2
  store %class.btConvexShape* %convexChildShape, %class.btConvexShape** %convexChildShape.addr, align 4, !tbaa !2
  store float %uniformScalingFactor, float* %uniformScalingFactor.addr, align 4, !tbaa !6
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  %0 = bitcast %class.btUniformScalingShape* %this1 to %class.btConvexShape*
  %call = call %class.btConvexShape* @_ZN13btConvexShapeC2Ev(%class.btConvexShape* %0)
  %1 = bitcast %class.btUniformScalingShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [25 x i8*] }, { [25 x i8*] }* @_ZTV21btUniformScalingShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !8
  %m_childConvexShape = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 1
  %2 = load %class.btConvexShape*, %class.btConvexShape** %convexChildShape.addr, align 4, !tbaa !2
  store %class.btConvexShape* %2, %class.btConvexShape** %m_childConvexShape, align 4, !tbaa !10
  %m_uniformScalingFactor = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 2
  %3 = load float, float* %uniformScalingFactor.addr, align 4, !tbaa !6
  store float %3, float* %m_uniformScalingFactor, align 4, !tbaa !12
  %4 = bitcast %class.btUniformScalingShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %4, i32 0, i32 1
  store i32 14, i32* %m_shapeType, align 4, !tbaa !13
  ret %class.btUniformScalingShape* %this1
}

declare %class.btConvexShape* @_ZN13btConvexShapeC2Ev(%class.btConvexShape* returned) unnamed_addr #1

; Function Attrs: nounwind
declare %class.btConvexShape* @_ZN13btConvexShapeD2Ev(%class.btConvexShape* returned) unnamed_addr #2

; Function Attrs: nounwind
define hidden %class.btUniformScalingShape* @_ZN21btUniformScalingShapeD2Ev(%class.btUniformScalingShape* returned %this) unnamed_addr #3 {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  %0 = bitcast %class.btUniformScalingShape* %this1 to %class.btConvexShape*
  %call = call %class.btConvexShape* @_ZN13btConvexShapeD2Ev(%class.btConvexShape* %0) #7
  ret %class.btUniformScalingShape* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN21btUniformScalingShapeD0Ev(%class.btUniformScalingShape* %this) unnamed_addr #3 {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  %call = call %class.btUniformScalingShape* @_ZN21btUniformScalingShapeD1Ev(%class.btUniformScalingShape* %this1) #7
  %0 = bitcast %class.btUniformScalingShape* %this1 to i8*
  call void @_ZN21btUniformScalingShapedlEPv(i8* %0) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN21btUniformScalingShapedlEPv(i8* %ptr) #4 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

define hidden void @_ZNK21btUniformScalingShape37localGetSupportingVertexWithoutMarginERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btUniformScalingShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %vec) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  %vec.addr = alloca %class.btVector3*, align 4
  %tmpVertex = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %vec, %class.btVector3** %vec.addr, align 4, !tbaa !2
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %tmpVertex to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #7
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %tmpVertex)
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #7
  %m_childConvexShape = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 1
  %2 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4, !tbaa !10
  %3 = load %class.btVector3*, %class.btVector3** %vec.addr, align 4, !tbaa !2
  %4 = bitcast %class.btConvexShape* %2 to void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)***
  %vtable = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)**, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*** %4, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vtable, i64 17
  %5 = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vfn, align 4
  call void %5(%class.btVector3* sret align 4 %ref.tmp, %class.btConvexShape* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  %6 = bitcast %class.btVector3* %tmpVertex to i8*
  %7 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !16
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #7
  %m_uniformScalingFactor = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %tmpVertex, float* nonnull align 4 dereferenceable(4) %m_uniformScalingFactor)
  %9 = bitcast %class.btVector3* %tmpVertex to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #7
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #5

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #5

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #5

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #6 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !6
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !6
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !6
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !6
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !6
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !6
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #7
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #7
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  ret void
}

define hidden void @_ZNK21btUniformScalingShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i(%class.btUniformScalingShape* %this, %class.btVector3* %vectors, %class.btVector3* %supportVerticesOut, i32 %numVectors) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  %vectors.addr = alloca %class.btVector3*, align 4
  %supportVerticesOut.addr = alloca %class.btVector3*, align 4
  %numVectors.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %vectors, %class.btVector3** %vectors.addr, align 4, !tbaa !2
  store %class.btVector3* %supportVerticesOut, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  store i32 %numVectors, i32* %numVectors.addr, align 4, !tbaa !18
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  %m_childConvexShape = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4, !tbaa !10
  %1 = load %class.btVector3*, %class.btVector3** %vectors.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  %3 = load i32, i32* %numVectors.addr, align 4, !tbaa !18
  %4 = bitcast %class.btConvexShape* %0 to void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)***
  %vtable = load void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)**, void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)*** %4, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)*, void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)** %vtable, i64 19
  %5 = load void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)*, void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)** %vfn, align 4
  call void %5(%class.btConvexShape* %0, %class.btVector3* %1, %class.btVector3* %2, i32 %3)
  %6 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  store i32 0, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %7 = load i32, i32* %i, align 4, !tbaa !18
  %8 = load i32, i32* %numVectors.addr, align 4, !tbaa !18
  %cmp = icmp slt i32 %7, %8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %9 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #7
  %10 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  %11 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 %11
  %m_uniformScalingFactor = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, float* nonnull align 4 dereferenceable(4) %m_uniformScalingFactor)
  %12 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  %13 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 %13
  %14 = bitcast %class.btVector3* %arrayidx2 to i8*
  %15 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %15, i32 16, i1 false), !tbaa.struct !16
  %16 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %16) #7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %17 = load i32, i32* %i, align 4, !tbaa !18
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %18 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #7
  ret void
}

define hidden void @_ZNK21btUniformScalingShape24localGetSupportingVertexERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btUniformScalingShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %vec) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  %vec.addr = alloca %class.btVector3*, align 4
  %tmpVertex = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %vec, %class.btVector3** %vec.addr, align 4, !tbaa !2
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %tmpVertex to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #7
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %tmpVertex)
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #7
  %m_childConvexShape = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 1
  %2 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4, !tbaa !10
  %3 = load %class.btVector3*, %class.btVector3** %vec.addr, align 4, !tbaa !2
  %4 = bitcast %class.btConvexShape* %2 to void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)***
  %vtable = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)**, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*** %4, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vtable, i64 16
  %5 = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vfn, align 4
  call void %5(%class.btVector3* sret align 4 %ref.tmp, %class.btConvexShape* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  %6 = bitcast %class.btVector3* %tmpVertex to i8*
  %7 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !16
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #7
  %m_uniformScalingFactor = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %tmpVertex, float* nonnull align 4 dereferenceable(4) %m_uniformScalingFactor)
  %9 = bitcast %class.btVector3* %tmpVertex to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #7
  ret void
}

define hidden void @_ZNK21btUniformScalingShape21calculateLocalInertiaEfR9btVector3(%class.btUniformScalingShape* %this, float %mass, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  %mass.addr = alloca float, align 4
  %inertia.addr = alloca %class.btVector3*, align 4
  %tmpInertia = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4, !tbaa !2
  store float %mass, float* %mass.addr, align 4, !tbaa !6
  store %class.btVector3* %inertia, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %tmpInertia to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #7
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %tmpInertia)
  %m_childConvexShape = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 1
  %1 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4, !tbaa !10
  %2 = bitcast %class.btConvexShape* %1 to %class.btCollisionShape*
  %3 = load float, float* %mass.addr, align 4, !tbaa !6
  %4 = bitcast %class.btCollisionShape* %2 to void (%class.btCollisionShape*, float, %class.btVector3*)***
  %vtable = load void (%class.btCollisionShape*, float, %class.btVector3*)**, void (%class.btCollisionShape*, float, %class.btVector3*)*** %4, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btCollisionShape*, float, %class.btVector3*)*, void (%class.btCollisionShape*, float, %class.btVector3*)** %vtable, i64 8
  %5 = load void (%class.btCollisionShape*, float, %class.btVector3*)*, void (%class.btCollisionShape*, float, %class.btVector3*)** %vfn, align 4
  call void %5(%class.btCollisionShape* %2, float %3, %class.btVector3* nonnull align 4 dereferenceable(16) %tmpInertia)
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #7
  %m_uniformScalingFactor = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %tmpInertia, float* nonnull align 4 dereferenceable(4) %m_uniformScalingFactor)
  %7 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %8 = bitcast %class.btVector3* %7 to i8*
  %9 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false), !tbaa.struct !16
  %10 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %10) #7
  %11 = bitcast %class.btVector3* %tmpInertia to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %11) #7
  ret void
}

define hidden void @_ZNK21btUniformScalingShape7getAabbERK11btTransformR9btVector3S4_(%class.btUniformScalingShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %trans, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  %trans.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %trans, %class.btTransform** %trans.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4, !tbaa !2
  %1 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %3 = bitcast %class.btUniformScalingShape* %this1 to void (%class.btUniformScalingShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btUniformScalingShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btUniformScalingShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %3, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btUniformScalingShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btUniformScalingShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 20
  %4 = load void (%class.btUniformScalingShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btUniformScalingShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %4(%class.btUniformScalingShape* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  ret void
}

define hidden void @_ZNK21btUniformScalingShape11getAabbSlowERK11btTransformR9btVector3S4_(%class.btUniformScalingShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %_directions = alloca [6 x %class.btVector3], align 16
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  %ref.tmp24 = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp26 = alloca float, align 4
  %_supporting = alloca [6 x %class.btVector3], align 16
  %ref.tmp29 = alloca float, align 4
  %ref.tmp30 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp34 = alloca float, align 4
  %ref.tmp35 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp39 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  %ref.tmp41 = alloca float, align 4
  %ref.tmp44 = alloca float, align 4
  %ref.tmp45 = alloca float, align 4
  %ref.tmp46 = alloca float, align 4
  %ref.tmp49 = alloca float, align 4
  %ref.tmp50 = alloca float, align 4
  %ref.tmp51 = alloca float, align 4
  %ref.tmp54 = alloca float, align 4
  %ref.tmp55 = alloca float, align 4
  %ref.tmp56 = alloca float, align 4
  %i = alloca i32, align 4
  %ref.tmp58 = alloca %class.btVector3, align 4
  %aabbMin1 = alloca %class.btVector3, align 4
  %ref.tmp62 = alloca float, align 4
  %ref.tmp63 = alloca float, align 4
  %ref.tmp64 = alloca float, align 4
  %aabbMax1 = alloca %class.btVector3, align 4
  %ref.tmp66 = alloca float, align 4
  %ref.tmp67 = alloca float, align 4
  %ref.tmp68 = alloca float, align 4
  %i70 = alloca i32, align 4
  %ref.tmp75 = alloca %class.btVector3, align 4
  %ref.tmp81 = alloca %class.btVector3, align 4
  %marginVec = alloca %class.btVector3, align 4
  %ref.tmp90 = alloca float, align 4
  %ref.tmp94 = alloca float, align 4
  %ref.tmp98 = alloca float, align 4
  %ref.tmp103 = alloca %class.btVector3, align 4
  %ref.tmp104 = alloca %class.btVector3, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  %0 = bitcast [6 x %class.btVector3]* %_directions to i8*
  call void @llvm.lifetime.start.p0i8(i64 96, i8* %0) #7
  %arrayinit.begin = getelementptr inbounds [6 x %class.btVector3], [6 x %class.btVector3]* %_directions, i32 0, i32 0
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !6
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !6
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.begin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %arrayinit.element = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.begin, i32 1
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  store float 1.000000e+00, float* %ref.tmp5, align 4, !tbaa !6
  %6 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !6
  %call7 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %arrayinit.element8 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element, i32 1
  %7 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  store float 0.000000e+00, float* %ref.tmp9, align 4, !tbaa !6
  %8 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  store float 0.000000e+00, float* %ref.tmp10, align 4, !tbaa !6
  %9 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  store float 1.000000e+00, float* %ref.tmp11, align 4, !tbaa !6
  %call12 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element8, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11)
  %arrayinit.element13 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element8, i32 1
  %10 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  store float -1.000000e+00, float* %ref.tmp14, align 4, !tbaa !6
  %11 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #7
  store float 0.000000e+00, float* %ref.tmp15, align 4, !tbaa !6
  %12 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #7
  store float 0.000000e+00, float* %ref.tmp16, align 4, !tbaa !6
  %call17 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element13, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp16)
  %arrayinit.element18 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element13, i32 1
  %13 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  store float 0.000000e+00, float* %ref.tmp19, align 4, !tbaa !6
  %14 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #7
  store float -1.000000e+00, float* %ref.tmp20, align 4, !tbaa !6
  %15 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #7
  store float 0.000000e+00, float* %ref.tmp21, align 4, !tbaa !6
  %call22 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element18, float* nonnull align 4 dereferenceable(4) %ref.tmp19, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %arrayinit.element23 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element18, i32 1
  %16 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #7
  store float 0.000000e+00, float* %ref.tmp24, align 4, !tbaa !6
  %17 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #7
  store float 0.000000e+00, float* %ref.tmp25, align 4, !tbaa !6
  %18 = bitcast float* %ref.tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #7
  store float -1.000000e+00, float* %ref.tmp26, align 4, !tbaa !6
  %call27 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element23, float* nonnull align 4 dereferenceable(4) %ref.tmp24, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp26)
  %19 = bitcast float* %ref.tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #7
  %20 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #7
  %21 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #7
  %22 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #7
  %23 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #7
  %24 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #7
  %25 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #7
  %26 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #7
  %27 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #7
  %28 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #7
  %29 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #7
  %30 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #7
  %31 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #7
  %32 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #7
  %33 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #7
  %34 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #7
  %35 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #7
  %36 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #7
  %37 = bitcast [6 x %class.btVector3]* %_supporting to i8*
  call void @llvm.lifetime.start.p0i8(i64 96, i8* %37) #7
  %arrayinit.begin28 = getelementptr inbounds [6 x %class.btVector3], [6 x %class.btVector3]* %_supporting, i32 0, i32 0
  %38 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #7
  store float 0.000000e+00, float* %ref.tmp29, align 4, !tbaa !6
  %39 = bitcast float* %ref.tmp30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #7
  store float 0.000000e+00, float* %ref.tmp30, align 4, !tbaa !6
  %40 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #7
  store float 0.000000e+00, float* %ref.tmp31, align 4, !tbaa !6
  %call32 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.begin28, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp30, float* nonnull align 4 dereferenceable(4) %ref.tmp31)
  %arrayinit.element33 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.begin28, i32 1
  %41 = bitcast float* %ref.tmp34 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #7
  store float 0.000000e+00, float* %ref.tmp34, align 4, !tbaa !6
  %42 = bitcast float* %ref.tmp35 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #7
  store float 0.000000e+00, float* %ref.tmp35, align 4, !tbaa !6
  %43 = bitcast float* %ref.tmp36 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #7
  store float 0.000000e+00, float* %ref.tmp36, align 4, !tbaa !6
  %call37 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element33, float* nonnull align 4 dereferenceable(4) %ref.tmp34, float* nonnull align 4 dereferenceable(4) %ref.tmp35, float* nonnull align 4 dereferenceable(4) %ref.tmp36)
  %arrayinit.element38 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element33, i32 1
  %44 = bitcast float* %ref.tmp39 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #7
  store float 0.000000e+00, float* %ref.tmp39, align 4, !tbaa !6
  %45 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #7
  store float 0.000000e+00, float* %ref.tmp40, align 4, !tbaa !6
  %46 = bitcast float* %ref.tmp41 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #7
  store float 0.000000e+00, float* %ref.tmp41, align 4, !tbaa !6
  %call42 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element38, float* nonnull align 4 dereferenceable(4) %ref.tmp39, float* nonnull align 4 dereferenceable(4) %ref.tmp40, float* nonnull align 4 dereferenceable(4) %ref.tmp41)
  %arrayinit.element43 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element38, i32 1
  %47 = bitcast float* %ref.tmp44 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #7
  store float 0.000000e+00, float* %ref.tmp44, align 4, !tbaa !6
  %48 = bitcast float* %ref.tmp45 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #7
  store float 0.000000e+00, float* %ref.tmp45, align 4, !tbaa !6
  %49 = bitcast float* %ref.tmp46 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #7
  store float 0.000000e+00, float* %ref.tmp46, align 4, !tbaa !6
  %call47 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element43, float* nonnull align 4 dereferenceable(4) %ref.tmp44, float* nonnull align 4 dereferenceable(4) %ref.tmp45, float* nonnull align 4 dereferenceable(4) %ref.tmp46)
  %arrayinit.element48 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element43, i32 1
  %50 = bitcast float* %ref.tmp49 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #7
  store float 0.000000e+00, float* %ref.tmp49, align 4, !tbaa !6
  %51 = bitcast float* %ref.tmp50 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #7
  store float 0.000000e+00, float* %ref.tmp50, align 4, !tbaa !6
  %52 = bitcast float* %ref.tmp51 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #7
  store float 0.000000e+00, float* %ref.tmp51, align 4, !tbaa !6
  %call52 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element48, float* nonnull align 4 dereferenceable(4) %ref.tmp49, float* nonnull align 4 dereferenceable(4) %ref.tmp50, float* nonnull align 4 dereferenceable(4) %ref.tmp51)
  %arrayinit.element53 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element48, i32 1
  %53 = bitcast float* %ref.tmp54 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #7
  store float 0.000000e+00, float* %ref.tmp54, align 4, !tbaa !6
  %54 = bitcast float* %ref.tmp55 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #7
  store float 0.000000e+00, float* %ref.tmp55, align 4, !tbaa !6
  %55 = bitcast float* %ref.tmp56 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #7
  store float 0.000000e+00, float* %ref.tmp56, align 4, !tbaa !6
  %call57 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element53, float* nonnull align 4 dereferenceable(4) %ref.tmp54, float* nonnull align 4 dereferenceable(4) %ref.tmp55, float* nonnull align 4 dereferenceable(4) %ref.tmp56)
  %56 = bitcast float* %ref.tmp56 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #7
  %57 = bitcast float* %ref.tmp55 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #7
  %58 = bitcast float* %ref.tmp54 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #7
  %59 = bitcast float* %ref.tmp51 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #7
  %60 = bitcast float* %ref.tmp50 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #7
  %61 = bitcast float* %ref.tmp49 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #7
  %62 = bitcast float* %ref.tmp46 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #7
  %63 = bitcast float* %ref.tmp45 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #7
  %64 = bitcast float* %ref.tmp44 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #7
  %65 = bitcast float* %ref.tmp41 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #7
  %66 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #7
  %67 = bitcast float* %ref.tmp39 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #7
  %68 = bitcast float* %ref.tmp36 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #7
  %69 = bitcast float* %ref.tmp35 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #7
  %70 = bitcast float* %ref.tmp34 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #7
  %71 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #7
  %72 = bitcast float* %ref.tmp30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #7
  %73 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #7
  %74 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %74) #7
  store i32 0, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %75 = load i32, i32* %i, align 4, !tbaa !18
  %cmp = icmp slt i32 %75, 6
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %76 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %77 = bitcast %class.btVector3* %ref.tmp58 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %77) #7
  %78 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds [6 x %class.btVector3], [6 x %class.btVector3]* %_directions, i32 0, i32 %78
  %79 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %call59 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %79)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp58, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call59)
  %80 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx60 = getelementptr inbounds [6 x %class.btVector3], [6 x %class.btVector3]* %_directions, i32 0, i32 %80
  %81 = bitcast %class.btVector3* %arrayidx60 to i8*
  %82 = bitcast %class.btVector3* %ref.tmp58 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %81, i8* align 4 %82, i32 16, i1 false), !tbaa.struct !16
  %83 = bitcast %class.btVector3* %ref.tmp58 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %83) #7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %84 = load i32, i32* %i, align 4, !tbaa !18
  %inc = add nsw i32 %84, 1
  store i32 %inc, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %arraydecay = getelementptr inbounds [6 x %class.btVector3], [6 x %class.btVector3]* %_directions, i32 0, i32 0
  %arraydecay61 = getelementptr inbounds [6 x %class.btVector3], [6 x %class.btVector3]* %_supporting, i32 0, i32 0
  %85 = bitcast %class.btUniformScalingShape* %this1 to void (%class.btUniformScalingShape*, %class.btVector3*, %class.btVector3*, i32)***
  %vtable = load void (%class.btUniformScalingShape*, %class.btVector3*, %class.btVector3*, i32)**, void (%class.btUniformScalingShape*, %class.btVector3*, %class.btVector3*, i32)*** %85, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btUniformScalingShape*, %class.btVector3*, %class.btVector3*, i32)*, void (%class.btUniformScalingShape*, %class.btVector3*, %class.btVector3*, i32)** %vtable, i64 19
  %86 = load void (%class.btUniformScalingShape*, %class.btVector3*, %class.btVector3*, i32)*, void (%class.btUniformScalingShape*, %class.btVector3*, %class.btVector3*, i32)** %vfn, align 4
  call void %86(%class.btUniformScalingShape* %this1, %class.btVector3* %arraydecay, %class.btVector3* %arraydecay61, i32 6)
  %87 = bitcast %class.btVector3* %aabbMin1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %87) #7
  %88 = bitcast float* %ref.tmp62 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #7
  store float 0.000000e+00, float* %ref.tmp62, align 4, !tbaa !6
  %89 = bitcast float* %ref.tmp63 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %89) #7
  store float 0.000000e+00, float* %ref.tmp63, align 4, !tbaa !6
  %90 = bitcast float* %ref.tmp64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %90) #7
  store float 0.000000e+00, float* %ref.tmp64, align 4, !tbaa !6
  %call65 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %aabbMin1, float* nonnull align 4 dereferenceable(4) %ref.tmp62, float* nonnull align 4 dereferenceable(4) %ref.tmp63, float* nonnull align 4 dereferenceable(4) %ref.tmp64)
  %91 = bitcast float* %ref.tmp64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #7
  %92 = bitcast float* %ref.tmp63 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #7
  %93 = bitcast float* %ref.tmp62 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #7
  %94 = bitcast %class.btVector3* %aabbMax1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %94) #7
  %95 = bitcast float* %ref.tmp66 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %95) #7
  store float 0.000000e+00, float* %ref.tmp66, align 4, !tbaa !6
  %96 = bitcast float* %ref.tmp67 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %96) #7
  store float 0.000000e+00, float* %ref.tmp67, align 4, !tbaa !6
  %97 = bitcast float* %ref.tmp68 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %97) #7
  store float 0.000000e+00, float* %ref.tmp68, align 4, !tbaa !6
  %call69 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %aabbMax1, float* nonnull align 4 dereferenceable(4) %ref.tmp66, float* nonnull align 4 dereferenceable(4) %ref.tmp67, float* nonnull align 4 dereferenceable(4) %ref.tmp68)
  %98 = bitcast float* %ref.tmp68 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #7
  %99 = bitcast float* %ref.tmp67 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #7
  %100 = bitcast float* %ref.tmp66 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #7
  %101 = bitcast i32* %i70 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %101) #7
  store i32 0, i32* %i70, align 4, !tbaa !18
  br label %for.cond71

for.cond71:                                       ; preds = %for.inc87, %for.end
  %102 = load i32, i32* %i70, align 4, !tbaa !18
  %cmp72 = icmp slt i32 %102, 3
  br i1 %cmp72, label %for.body74, label %for.cond.cleanup73

for.cond.cleanup73:                               ; preds = %for.cond71
  %103 = bitcast i32* %i70 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #7
  br label %for.end89

for.body74:                                       ; preds = %for.cond71
  %104 = bitcast %class.btVector3* %ref.tmp75 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %104) #7
  %105 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %106 = load i32, i32* %i70, align 4, !tbaa !18
  %arrayidx76 = getelementptr inbounds [6 x %class.btVector3], [6 x %class.btVector3]* %_supporting, i32 0, i32 %106
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp75, %class.btTransform* %105, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx76)
  %call77 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ref.tmp75)
  %107 = load i32, i32* %i70, align 4, !tbaa !18
  %arrayidx78 = getelementptr inbounds float, float* %call77, i32 %107
  %108 = load float, float* %arrayidx78, align 4, !tbaa !6
  %call79 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %aabbMax1)
  %109 = load i32, i32* %i70, align 4, !tbaa !18
  %arrayidx80 = getelementptr inbounds float, float* %call79, i32 %109
  store float %108, float* %arrayidx80, align 4, !tbaa !6
  %110 = bitcast %class.btVector3* %ref.tmp75 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %110) #7
  %111 = bitcast %class.btVector3* %ref.tmp81 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %111) #7
  %112 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %113 = load i32, i32* %i70, align 4, !tbaa !18
  %add = add nsw i32 %113, 3
  %arrayidx82 = getelementptr inbounds [6 x %class.btVector3], [6 x %class.btVector3]* %_supporting, i32 0, i32 %add
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp81, %class.btTransform* %112, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx82)
  %call83 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ref.tmp81)
  %114 = load i32, i32* %i70, align 4, !tbaa !18
  %arrayidx84 = getelementptr inbounds float, float* %call83, i32 %114
  %115 = load float, float* %arrayidx84, align 4, !tbaa !6
  %call85 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %aabbMin1)
  %116 = load i32, i32* %i70, align 4, !tbaa !18
  %arrayidx86 = getelementptr inbounds float, float* %call85, i32 %116
  store float %115, float* %arrayidx86, align 4, !tbaa !6
  %117 = bitcast %class.btVector3* %ref.tmp81 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %117) #7
  br label %for.inc87

for.inc87:                                        ; preds = %for.body74
  %118 = load i32, i32* %i70, align 4, !tbaa !18
  %inc88 = add nsw i32 %118, 1
  store i32 %inc88, i32* %i70, align 4, !tbaa !18
  br label %for.cond71

for.end89:                                        ; preds = %for.cond.cleanup73
  %119 = bitcast %class.btVector3* %marginVec to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %119) #7
  %120 = bitcast float* %ref.tmp90 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %120) #7
  %121 = bitcast %class.btUniformScalingShape* %this1 to float (%class.btUniformScalingShape*)***
  %vtable91 = load float (%class.btUniformScalingShape*)**, float (%class.btUniformScalingShape*)*** %121, align 4, !tbaa !8
  %vfn92 = getelementptr inbounds float (%class.btUniformScalingShape*)*, float (%class.btUniformScalingShape*)** %vtable91, i64 12
  %122 = load float (%class.btUniformScalingShape*)*, float (%class.btUniformScalingShape*)** %vfn92, align 4
  %call93 = call float %122(%class.btUniformScalingShape* %this1)
  store float %call93, float* %ref.tmp90, align 4, !tbaa !6
  %123 = bitcast float* %ref.tmp94 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %123) #7
  %124 = bitcast %class.btUniformScalingShape* %this1 to float (%class.btUniformScalingShape*)***
  %vtable95 = load float (%class.btUniformScalingShape*)**, float (%class.btUniformScalingShape*)*** %124, align 4, !tbaa !8
  %vfn96 = getelementptr inbounds float (%class.btUniformScalingShape*)*, float (%class.btUniformScalingShape*)** %vtable95, i64 12
  %125 = load float (%class.btUniformScalingShape*)*, float (%class.btUniformScalingShape*)** %vfn96, align 4
  %call97 = call float %125(%class.btUniformScalingShape* %this1)
  store float %call97, float* %ref.tmp94, align 4, !tbaa !6
  %126 = bitcast float* %ref.tmp98 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %126) #7
  %127 = bitcast %class.btUniformScalingShape* %this1 to float (%class.btUniformScalingShape*)***
  %vtable99 = load float (%class.btUniformScalingShape*)**, float (%class.btUniformScalingShape*)*** %127, align 4, !tbaa !8
  %vfn100 = getelementptr inbounds float (%class.btUniformScalingShape*)*, float (%class.btUniformScalingShape*)** %vtable99, i64 12
  %128 = load float (%class.btUniformScalingShape*)*, float (%class.btUniformScalingShape*)** %vfn100, align 4
  %call101 = call float %128(%class.btUniformScalingShape* %this1)
  store float %call101, float* %ref.tmp98, align 4, !tbaa !6
  %call102 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %marginVec, float* nonnull align 4 dereferenceable(4) %ref.tmp90, float* nonnull align 4 dereferenceable(4) %ref.tmp94, float* nonnull align 4 dereferenceable(4) %ref.tmp98)
  %129 = bitcast float* %ref.tmp98 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #7
  %130 = bitcast float* %ref.tmp94 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #7
  %131 = bitcast float* %ref.tmp90 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %131) #7
  %132 = bitcast %class.btVector3* %ref.tmp103 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %132) #7
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp103, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin1, %class.btVector3* nonnull align 4 dereferenceable(16) %marginVec)
  %133 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %134 = bitcast %class.btVector3* %133 to i8*
  %135 = bitcast %class.btVector3* %ref.tmp103 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %134, i8* align 4 %135, i32 16, i1 false), !tbaa.struct !16
  %136 = bitcast %class.btVector3* %ref.tmp103 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %136) #7
  %137 = bitcast %class.btVector3* %ref.tmp104 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %137) #7
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp104, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax1, %class.btVector3* nonnull align 4 dereferenceable(16) %marginVec)
  %138 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %139 = bitcast %class.btVector3* %138 to i8*
  %140 = bitcast %class.btVector3* %ref.tmp104 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %139, i8* align 4 %140, i32 16, i1 false), !tbaa.struct !16
  %141 = bitcast %class.btVector3* %ref.tmp104 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %141) #7
  %142 = bitcast %class.btVector3* %marginVec to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %142) #7
  %143 = bitcast %class.btVector3* %aabbMax1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %143) #7
  %144 = bitcast %class.btVector3* %aabbMin1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %144) #7
  %145 = bitcast [6 x %class.btVector3]* %_supporting to i8*
  call void @llvm.lifetime.end.p0i8(i64 96, i8* %145) #7
  %146 = bitcast [6 x %class.btVector3]* %_directions to i8*
  call void @llvm.lifetime.end.p0i8(i64 96, i8* %146) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !6
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !6
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !6
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !6
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !6
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !6
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !6
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m) #6 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call, float* %ref.tmp, align 4, !tbaa !6
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call2, float* %ref.tmp1, align 4, !tbaa !6
  %6 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call4, float* %ref.tmp3, align 4, !tbaa !6
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %9 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #7
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #7
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #6 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #7
  %1 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #6 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !6
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !6
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !6
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !6
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !6
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !6
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #7
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #7
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #6 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !6
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !6
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !6
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !6
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !6
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !6
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #7
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #7
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  ret void
}

define hidden void @_ZN21btUniformScalingShape15setLocalScalingERK9btVector3(%class.btUniformScalingShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %scaling) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  %scaling.addr = alloca %class.btVector3*, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %scaling, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  %m_childConvexShape = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4, !tbaa !10
  %1 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  %2 = bitcast %class.btConvexShape* %0 to void (%class.btConvexShape*, %class.btVector3*)***
  %vtable = load void (%class.btConvexShape*, %class.btVector3*)**, void (%class.btConvexShape*, %class.btVector3*)*** %2, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btConvexShape*, %class.btVector3*)*, void (%class.btConvexShape*, %class.btVector3*)** %vtable, i64 6
  %3 = load void (%class.btConvexShape*, %class.btVector3*)*, void (%class.btConvexShape*, %class.btVector3*)** %vfn, align 4
  call void %3(%class.btConvexShape* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  ret void
}

define hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btUniformScalingShape15getLocalScalingEv(%class.btUniformScalingShape* %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  %m_childConvexShape = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4, !tbaa !10
  %1 = bitcast %class.btConvexShape* %0 to %class.btVector3* (%class.btConvexShape*)***
  %vtable = load %class.btVector3* (%class.btConvexShape*)**, %class.btVector3* (%class.btConvexShape*)*** %1, align 4, !tbaa !8
  %vfn = getelementptr inbounds %class.btVector3* (%class.btConvexShape*)*, %class.btVector3* (%class.btConvexShape*)** %vtable, i64 7
  %2 = load %class.btVector3* (%class.btConvexShape*)*, %class.btVector3* (%class.btConvexShape*)** %vfn, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* %2(%class.btConvexShape* %0)
  ret %class.btVector3* %call
}

define hidden void @_ZN21btUniformScalingShape9setMarginEf(%class.btUniformScalingShape* %this, float %margin) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  %margin.addr = alloca float, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4, !tbaa !2
  store float %margin, float* %margin.addr, align 4, !tbaa !6
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  %m_childConvexShape = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4, !tbaa !10
  %1 = load float, float* %margin.addr, align 4, !tbaa !6
  %2 = bitcast %class.btConvexShape* %0 to void (%class.btConvexShape*, float)***
  %vtable = load void (%class.btConvexShape*, float)**, void (%class.btConvexShape*, float)*** %2, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btConvexShape*, float)*, void (%class.btConvexShape*, float)** %vtable, i64 11
  %3 = load void (%class.btConvexShape*, float)*, void (%class.btConvexShape*, float)** %vfn, align 4
  call void %3(%class.btConvexShape* %0, float %1)
  ret void
}

define hidden float @_ZNK21btUniformScalingShape9getMarginEv(%class.btUniformScalingShape* %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  %m_childConvexShape = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4, !tbaa !10
  %1 = bitcast %class.btConvexShape* %0 to float (%class.btConvexShape*)***
  %vtable = load float (%class.btConvexShape*)**, float (%class.btConvexShape*)*** %1, align 4, !tbaa !8
  %vfn = getelementptr inbounds float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vtable, i64 12
  %2 = load float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vfn, align 4
  %call = call float %2(%class.btConvexShape* %0)
  %m_uniformScalingFactor = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 2
  %3 = load float, float* %m_uniformScalingFactor, align 4, !tbaa !12
  %mul = fmul float %call, %3
  ret float %mul
}

define hidden i32 @_ZNK21btUniformScalingShape36getNumPreferredPenetrationDirectionsEv(%class.btUniformScalingShape* %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  %m_childConvexShape = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4, !tbaa !10
  %1 = bitcast %class.btConvexShape* %0 to i32 (%class.btConvexShape*)***
  %vtable = load i32 (%class.btConvexShape*)**, i32 (%class.btConvexShape*)*** %1, align 4, !tbaa !8
  %vfn = getelementptr inbounds i32 (%class.btConvexShape*)*, i32 (%class.btConvexShape*)** %vtable, i64 21
  %2 = load i32 (%class.btConvexShape*)*, i32 (%class.btConvexShape*)** %vfn, align 4
  %call = call i32 %2(%class.btConvexShape* %0)
  ret i32 %call
}

define hidden void @_ZNK21btUniformScalingShape32getPreferredPenetrationDirectionEiR9btVector3(%class.btUniformScalingShape* %this, i32 %index, %class.btVector3* nonnull align 4 dereferenceable(16) %penetrationVector) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  %index.addr = alloca i32, align 4
  %penetrationVector.addr = alloca %class.btVector3*, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !18
  store %class.btVector3* %penetrationVector, %class.btVector3** %penetrationVector.addr, align 4, !tbaa !2
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  %m_childConvexShape = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4, !tbaa !10
  %1 = load i32, i32* %index.addr, align 4, !tbaa !18
  %2 = load %class.btVector3*, %class.btVector3** %penetrationVector.addr, align 4, !tbaa !2
  %3 = bitcast %class.btConvexShape* %0 to void (%class.btConvexShape*, i32, %class.btVector3*)***
  %vtable = load void (%class.btConvexShape*, i32, %class.btVector3*)**, void (%class.btConvexShape*, i32, %class.btVector3*)*** %3, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btConvexShape*, i32, %class.btVector3*)*, void (%class.btConvexShape*, i32, %class.btVector3*)** %vtable, i64 22
  %4 = load void (%class.btConvexShape*, i32, %class.btVector3*)*, void (%class.btConvexShape*, i32, %class.btVector3*)** %vfn, align 4
  call void %4(%class.btConvexShape* %0, i32 %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  ret void
}

declare void @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf(%class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4)) unnamed_addr #1

declare float @_ZNK16btCollisionShape20getAngularMotionDiscEv(%class.btCollisionShape*) unnamed_addr #1

declare float @_ZNK16btCollisionShape27getContactBreakingThresholdEf(%class.btCollisionShape*, float) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden i8* @_ZNK21btUniformScalingShape7getNameEv(%class.btUniformScalingShape* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  ret i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str, i32 0, i32 0)
}

define linkonce_odr hidden void @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btCollisionShape* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !6
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  store float 1.000000e+00, float* %ref.tmp2, align 4, !tbaa !6
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  store float 1.000000e+00, float* %ref.tmp3, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #7
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #7
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK16btCollisionShape28calculateSerializeBufferSizeEv(%class.btCollisionShape* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  ret i32 12
}

declare i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape*, i8*, %class.btSerializer*) unnamed_addr #1

declare void @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer(%class.btCollisionShape*, %class.btSerializer*) unnamed_addr #1

declare void @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_(%class.btConvexShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4), float* nonnull align 4 dereferenceable(4)) unnamed_addr #1

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #6 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !6
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !6
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !6
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !6
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !6
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !6
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !6
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !6
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !6
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !6
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !6
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !6
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !6
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !6
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !6
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !6
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !6
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !6
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4, !tbaa !6
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !6
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !6
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #7
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !18
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !6
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !6
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !6
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !6
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !6
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !6
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { argmemonly nounwind willreturn }
attributes #6 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"float", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"vtable pointer", !5, i64 0}
!10 = !{!11, !3, i64 12}
!11 = !{!"_ZTS21btUniformScalingShape", !3, i64 12, !7, i64 16}
!12 = !{!11, !7, i64 16}
!13 = !{!14, !15, i64 4}
!14 = !{!"_ZTS16btCollisionShape", !15, i64 4, !3, i64 8}
!15 = !{!"int", !4, i64 0}
!16 = !{i64 0, i64 16, !17}
!17 = !{!4, !4, i64 0}
!18 = !{!15, !15, i64 0}
