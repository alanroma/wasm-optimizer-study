; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btTriangleBuffer.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btTriangleBuffer.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btTriangleBuffer = type { %class.btTriangleCallback, %class.btAlignedObjectArray }
%class.btTriangleCallback = type { i32 (...)** }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btTriangle*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btTriangle = type { %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btVector3 = type { [4 x float] }

$_ZN10btTriangleC2Ev = comdat any

$_ZN20btAlignedObjectArrayI10btTriangleE9push_backERKS0_ = comdat any

$_ZN16btTriangleBufferD2Ev = comdat any

$_ZN16btTriangleBufferD0Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN20btAlignedObjectArrayI10btTriangleED2Ev = comdat any

$_ZN20btAlignedObjectArrayI10btTriangleE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI10btTriangleE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayI10btTriangleE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI10btTriangleE10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayI10btTriangleE4initEv = comdat any

$_ZN18btAlignedAllocatorI10btTriangleLj16EE10deallocateEPS0_ = comdat any

$_ZNK20btAlignedObjectArrayI10btTriangleE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI10btTriangleE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI10btTriangleE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayI10btTriangleE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI10btTriangleE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI10btTriangleLj16EE8allocateEiPPKS0_ = comdat any

@_ZTV16btTriangleBuffer = hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI16btTriangleBuffer to i8*), i8* bitcast (%class.btTriangleBuffer* (%class.btTriangleBuffer*)* @_ZN16btTriangleBufferD2Ev to i8*), i8* bitcast (void (%class.btTriangleBuffer*)* @_ZN16btTriangleBufferD0Ev to i8*), i8* bitcast (void (%class.btTriangleBuffer*, %class.btVector3*, i32, i32)* @_ZN16btTriangleBuffer15processTriangleEP9btVector3ii to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS16btTriangleBuffer = hidden constant [19 x i8] c"16btTriangleBuffer\00", align 1
@_ZTI18btTriangleCallback = external constant i8*
@_ZTI16btTriangleBuffer = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([19 x i8], [19 x i8]* @_ZTS16btTriangleBuffer, i32 0, i32 0), i8* bitcast (i8** @_ZTI18btTriangleCallback to i8*) }, align 4

define hidden void @_ZN16btTriangleBuffer15processTriangleEP9btVector3ii(%class.btTriangleBuffer* %this, %class.btVector3* %triangle, i32 %partId, i32 %triangleIndex) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btTriangleBuffer*, align 4
  %triangle.addr = alloca %class.btVector3*, align 4
  %partId.addr = alloca i32, align 4
  %triangleIndex.addr = alloca i32, align 4
  %tri = alloca %struct.btTriangle, align 4
  store %class.btTriangleBuffer* %this, %class.btTriangleBuffer** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %triangle, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  store i32 %partId, i32* %partId.addr, align 4, !tbaa !6
  store i32 %triangleIndex, i32* %triangleIndex.addr, align 4, !tbaa !6
  %this1 = load %class.btTriangleBuffer*, %class.btTriangleBuffer** %this.addr, align 4
  %0 = bitcast %struct.btTriangle* %tri to i8*
  call void @llvm.lifetime.start.p0i8(i64 56, i8* %0) #8
  %call = call %struct.btTriangle* @_ZN10btTriangleC2Ev(%struct.btTriangle* %tri)
  %1 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0
  %m_vertex0 = getelementptr inbounds %struct.btTriangle, %struct.btTriangle* %tri, i32 0, i32 0
  %2 = bitcast %class.btVector3* %m_vertex0 to i8*
  %3 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !8
  %4 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 1
  %m_vertex1 = getelementptr inbounds %struct.btTriangle, %struct.btTriangle* %tri, i32 0, i32 1
  %5 = bitcast %class.btVector3* %m_vertex1 to i8*
  %6 = bitcast %class.btVector3* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false), !tbaa.struct !8
  %7 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 2
  %m_vertex2 = getelementptr inbounds %struct.btTriangle, %struct.btTriangle* %tri, i32 0, i32 2
  %8 = bitcast %class.btVector3* %m_vertex2 to i8*
  %9 = bitcast %class.btVector3* %arrayidx3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false), !tbaa.struct !8
  %10 = load i32, i32* %partId.addr, align 4, !tbaa !6
  %m_partId = getelementptr inbounds %struct.btTriangle, %struct.btTriangle* %tri, i32 0, i32 3
  store i32 %10, i32* %m_partId, align 4, !tbaa !10
  %11 = load i32, i32* %triangleIndex.addr, align 4, !tbaa !6
  %m_triangleIndex = getelementptr inbounds %struct.btTriangle, %struct.btTriangle* %tri, i32 0, i32 4
  store i32 %11, i32* %m_triangleIndex, align 4, !tbaa !13
  %m_triangleBuffer = getelementptr inbounds %class.btTriangleBuffer, %class.btTriangleBuffer* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayI10btTriangleE9push_backERKS0_(%class.btAlignedObjectArray* %m_triangleBuffer, %struct.btTriangle* nonnull align 4 dereferenceable(56) %tri)
  %12 = bitcast %struct.btTriangle* %tri to i8*
  call void @llvm.lifetime.end.p0i8(i64 56, i8* %12) #8
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint
define linkonce_odr hidden %struct.btTriangle* @_ZN10btTriangleC2Ev(%struct.btTriangle* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btTriangle*, align 4
  store %struct.btTriangle* %this, %struct.btTriangle** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btTriangle*, %struct.btTriangle** %this.addr, align 4
  %m_vertex0 = getelementptr inbounds %struct.btTriangle, %struct.btTriangle* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_vertex0)
  %m_vertex1 = getelementptr inbounds %struct.btTriangle, %struct.btTriangle* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_vertex1)
  %m_vertex2 = getelementptr inbounds %struct.btTriangle, %struct.btTriangle* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_vertex2)
  ret %struct.btTriangle* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI10btTriangleE9push_backERKS0_(%class.btAlignedObjectArray* %this, %struct.btTriangle* nonnull align 4 dereferenceable(56) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %struct.btTriangle*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store %struct.btTriangle* %_Val, %struct.btTriangle** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayI10btTriangleE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !6
  %1 = load i32, i32* %sz, align 4, !tbaa !6
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI10btTriangleE8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI10btTriangleE4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI10btTriangleE9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI10btTriangleE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btTriangle*, %struct.btTriangle** %m_data, align 4, !tbaa !14
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds %struct.btTriangle, %struct.btTriangle* %2, i32 %3
  %4 = bitcast %struct.btTriangle* %arrayidx to i8*
  %5 = bitcast i8* %4 to %struct.btTriangle*
  %6 = load %struct.btTriangle*, %struct.btTriangle** %_Val.addr, align 4, !tbaa !2
  %7 = bitcast %struct.btTriangle* %5 to i8*
  %8 = bitcast %struct.btTriangle* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 56, i1 false), !tbaa.struct !19
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %9 = load i32, i32* %m_size5, align 4, !tbaa !18
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %m_size5, align 4, !tbaa !18
  %10 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btTriangleBuffer* @_ZN16btTriangleBufferD2Ev(%class.btTriangleBuffer* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btTriangleBuffer*, align 4
  store %class.btTriangleBuffer* %this, %class.btTriangleBuffer** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleBuffer*, %class.btTriangleBuffer** %this.addr, align 4
  %0 = bitcast %class.btTriangleBuffer* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV16btTriangleBuffer, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !20
  %m_triangleBuffer = getelementptr inbounds %class.btTriangleBuffer, %class.btTriangleBuffer* %this1, i32 0, i32 1
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI10btTriangleED2Ev(%class.btAlignedObjectArray* %m_triangleBuffer) #8
  %1 = bitcast %class.btTriangleBuffer* %this1 to %class.btTriangleCallback*
  %call2 = call %class.btTriangleCallback* @_ZN18btTriangleCallbackD2Ev(%class.btTriangleCallback* %1) #8
  ret %class.btTriangleBuffer* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN16btTriangleBufferD0Ev(%class.btTriangleBuffer* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btTriangleBuffer*, align 4
  store %class.btTriangleBuffer* %this, %class.btTriangleBuffer** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleBuffer*, %class.btTriangleBuffer** %this.addr, align 4
  %call = call %class.btTriangleBuffer* @_ZN16btTriangleBufferD2Ev(%class.btTriangleBuffer* %this1) #8
  %0 = bitcast %class.btTriangleBuffer* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI10btTriangleED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI10btTriangleE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: nounwind
declare %class.btTriangleCallback* @_ZN18btTriangleCallbackD2Ev(%class.btTriangleCallback* returned) unnamed_addr #5

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI10btTriangleE5clearEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI10btTriangleE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI10btTriangleE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI10btTriangleE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI10btTriangleE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI10btTriangleE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %struct.btTriangle*, %struct.btTriangle** %m_data, align 4, !tbaa !14
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btTriangle, %struct.btTriangle* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI10btTriangleE4sizeEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !18
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI10btTriangleE10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btTriangle*, %struct.btTriangle** %m_data, align 4, !tbaa !14
  %tobool = icmp ne %struct.btTriangle* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !22, !range !23
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btTriangle*, %struct.btTriangle** %m_data4, align 4, !tbaa !14
  call void @_ZN18btAlignedAllocatorI10btTriangleLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %struct.btTriangle* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btTriangle* null, %struct.btTriangle** %m_data5, align 4, !tbaa !14
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI10btTriangleE4initEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !22
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btTriangle* null, %struct.btTriangle** %m_data, align 4, !tbaa !14
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !18
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !24
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI10btTriangleLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %struct.btTriangle* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %struct.btTriangle*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store %struct.btTriangle* %ptr, %struct.btTriangle** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %struct.btTriangle*, %struct.btTriangle** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btTriangle* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #6

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #7

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI10btTriangleE8capacityEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !24
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI10btTriangleE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btTriangle*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI10btTriangleE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btTriangle** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayI10btTriangleE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.btTriangle*
  store %struct.btTriangle* %3, %struct.btTriangle** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI10btTriangleE4sizeEv(%class.btAlignedObjectArray* %this1)
  %4 = load %struct.btTriangle*, %struct.btTriangle** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI10btTriangleE4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %struct.btTriangle* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI10btTriangleE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI10btTriangleE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI10btTriangleE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !22
  %5 = load %struct.btTriangle*, %struct.btTriangle** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btTriangle* %5, %struct.btTriangle** %m_data, align 4, !tbaa !14
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !24
  %7 = bitcast %struct.btTriangle** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI10btTriangleE9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI10btTriangleE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call %struct.btTriangle* @_ZN18btAlignedAllocatorI10btTriangleLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %struct.btTriangle** null)
  %2 = bitcast %struct.btTriangle* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI10btTriangleE4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %struct.btTriangle* %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btTriangle*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store %struct.btTriangle* %dest, %struct.btTriangle** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.btTriangle*, %struct.btTriangle** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btTriangle, %struct.btTriangle* %4, i32 %5
  %6 = bitcast %struct.btTriangle* %arrayidx to i8*
  %7 = bitcast i8* %6 to %struct.btTriangle*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %struct.btTriangle*, %struct.btTriangle** %m_data, align 4, !tbaa !14
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds %struct.btTriangle, %struct.btTriangle* %8, i32 %9
  %10 = bitcast %struct.btTriangle* %7 to i8*
  %11 = bitcast %struct.btTriangle* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 56, i1 false), !tbaa.struct !19
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  ret void
}

define linkonce_odr hidden %struct.btTriangle* @_ZN18btAlignedAllocatorI10btTriangleLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %struct.btTriangle** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btTriangle**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store %struct.btTriangle** %hint, %struct.btTriangle*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 56, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btTriangle*
  ret %struct.btTriangle* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #6

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind }
attributes #9 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{i64 0, i64 16, !9}
!9 = !{!4, !4, i64 0}
!10 = !{!11, !7, i64 48}
!11 = !{!"_ZTS10btTriangle", !12, i64 0, !12, i64 16, !12, i64 32, !7, i64 48, !7, i64 52}
!12 = !{!"_ZTS9btVector3", !4, i64 0}
!13 = !{!11, !7, i64 52}
!14 = !{!15, !3, i64 12}
!15 = !{!"_ZTS20btAlignedObjectArrayI10btTriangleE", !16, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !17, i64 16}
!16 = !{!"_ZTS18btAlignedAllocatorI10btTriangleLj16EE"}
!17 = !{!"bool", !4, i64 0}
!18 = !{!15, !7, i64 4}
!19 = !{i64 0, i64 16, !9, i64 16, i64 16, !9, i64 32, i64 16, !9, i64 48, i64 4, !6, i64 52, i64 4, !6}
!20 = !{!21, !21, i64 0}
!21 = !{!"vtable pointer", !5, i64 0}
!22 = !{!15, !17, i64 16}
!23 = !{i8 0, i8 2}
!24 = !{!15, !7, i64 8}
