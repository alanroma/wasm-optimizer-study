; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/Gimpact/gim_contact.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/Gimpact/gim_contact.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.gim_contact_array = type { %class.gim_array }
%class.gim_array = type { %class.GIM_CONTACT*, i32, i32 }
%class.GIM_CONTACT = type { %class.btVector3, %class.btVector3, float, float, i32, i32 }
%class.btVector3 = type { [4 x float] }
%class.gim_array.0 = type { %struct.GIM_RSORT_TOKEN*, i32, i32 }
%struct.GIM_RSORT_TOKEN = type { i32, i32 }
%class.GIM_RSORT_TOKEN_COMPARATOR = type { i8 }

$_ZN9gim_arrayI11GIM_CONTACTE5clearEv = comdat any

$_ZNK9gim_arrayI11GIM_CONTACTE4sizeEv = comdat any

$_ZN9gim_arrayI11GIM_CONTACTE9push_backERKS0_ = comdat any

$_ZNK9gim_arrayI11GIM_CONTACTE4backEv = comdat any

$_ZN9gim_arrayI15GIM_RSORT_TOKENEC2Ej = comdat any

$_ZN9gim_arrayI15GIM_RSORT_TOKENE6resizeEjbRKS0_ = comdat any

$_ZN15GIM_RSORT_TOKENC2Ev = comdat any

$_ZNK9gim_arrayI11GIM_CONTACTEixEm = comdat any

$_ZNK11GIM_CONTACT16calc_key_contactEv = comdat any

$_ZN9gim_arrayI15GIM_RSORT_TOKENEixEm = comdat any

$_Z13gim_heap_sortI15GIM_RSORT_TOKEN26GIM_RSORT_TOKEN_COMPARATOREvPT_jT0_ = comdat any

$_ZN9gim_arrayI15GIM_RSORT_TOKENE7pointerEv = comdat any

$_ZNK9gim_arrayI15GIM_RSORT_TOKENE4sizeEv = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN9gim_arrayI11GIM_CONTACTE4backEv = comdat any

$_Z6btFabsf = comdat any

$_ZN11GIM_CONTACT19interpolate_normalsEP9btVector3j = comdat any

$_ZN9gim_arrayI15GIM_RSORT_TOKENED2Ev = comdat any

$_ZN11GIM_CONTACTC2ERKS_ = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_Z6btSqrtf = comdat any

$_ZN9gim_arrayI11GIM_CONTACTE11clear_rangeEj = comdat any

$_ZN9gim_arrayI11GIM_CONTACTE12growingCheckEv = comdat any

$_ZN9gim_arrayI11GIM_CONTACTE10resizeDataEj = comdat any

$_ZN9gim_arrayI11GIM_CONTACTE11destroyDataEv = comdat any

$_ZN9gim_arrayI15GIM_RSORT_TOKENE7reserveEj = comdat any

$_ZN9gim_arrayI15GIM_RSORT_TOKENE10resizeDataEj = comdat any

$_ZN9gim_arrayI15GIM_RSORT_TOKENE11destroyDataEv = comdat any

$_ZN9gim_arrayI15GIM_RSORT_TOKENE12clear_memoryEv = comdat any

$_ZN9gim_arrayI15GIM_RSORT_TOKENE5clearEv = comdat any

$_ZN9gim_arrayI15GIM_RSORT_TOKENE11clear_rangeEj = comdat any

$_Z13gim_down_heapI15GIM_RSORT_TOKEN26GIM_RSORT_TOKEN_COMPARATOREvPT_jjT0_ = comdat any

$_Z17gim_swap_elementsI15GIM_RSORT_TOKENEvPT_mm = comdat any

$_ZN15GIM_RSORT_TOKENC2ERKS_ = comdat any

$_ZN26GIM_RSORT_TOKEN_COMPARATORclERK15GIM_RSORT_TOKENS2_ = comdat any

define hidden void @_ZN17gim_contact_array14merge_contactsERKS_b(%class.gim_contact_array* %this, %class.gim_contact_array* nonnull align 4 dereferenceable(12) %contacts, i1 zeroext %normal_contact_average) #0 {
entry:
  %this.addr = alloca %class.gim_contact_array*, align 4
  %contacts.addr = alloca %class.gim_contact_array*, align 4
  %normal_contact_average.addr = alloca i8, align 1
  %keycontacts = alloca %class.gim_array.0, align 4
  %ref.tmp = alloca %struct.GIM_RSORT_TOKEN, align 4
  %i = alloca i32, align 4
  %agg.tmp = alloca %class.GIM_RSORT_TOKEN_COMPARATOR, align 1
  %coincident_count = alloca i32, align 4
  %coincident_normals = alloca [8 x %class.btVector3], align 16
  %last_key = alloca i32, align 4
  %key = alloca i32, align 4
  %pcontact = alloca %class.GIM_CONTACT*, align 4
  %scontact = alloca %class.GIM_CONTACT*, align 4
  store %class.gim_contact_array* %this, %class.gim_contact_array** %this.addr, align 4, !tbaa !2
  store %class.gim_contact_array* %contacts, %class.gim_contact_array** %contacts.addr, align 4, !tbaa !2
  %frombool = zext i1 %normal_contact_average to i8
  store i8 %frombool, i8* %normal_contact_average.addr, align 1, !tbaa !6
  %this1 = load %class.gim_contact_array*, %class.gim_contact_array** %this.addr, align 4
  %0 = bitcast %class.gim_contact_array* %this1 to %class.gim_array*
  call void @_ZN9gim_arrayI11GIM_CONTACTE5clearEv(%class.gim_array* %0)
  %1 = load %class.gim_contact_array*, %class.gim_contact_array** %contacts.addr, align 4, !tbaa !2
  %2 = bitcast %class.gim_contact_array* %1 to %class.gim_array*
  %call = call i32 @_ZNK9gim_arrayI11GIM_CONTACTE4sizeEv(%class.gim_array* %2)
  %cmp = icmp eq i32 %call, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = bitcast %class.gim_contact_array* %this1 to %class.gim_array*
  %4 = load %class.gim_contact_array*, %class.gim_contact_array** %contacts.addr, align 4, !tbaa !2
  %5 = bitcast %class.gim_contact_array* %4 to %class.gim_array*
  %call2 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK9gim_arrayI11GIM_CONTACTE4backEv(%class.gim_array* %5)
  call void @_ZN9gim_arrayI11GIM_CONTACTE9push_backERKS0_(%class.gim_array* %3, %class.GIM_CONTACT* nonnull align 4 dereferenceable(48) %call2)
  br label %return

if.end:                                           ; preds = %entry
  %6 = bitcast %class.gim_array.0* %keycontacts to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %6) #7
  %7 = load %class.gim_contact_array*, %class.gim_contact_array** %contacts.addr, align 4, !tbaa !2
  %8 = bitcast %class.gim_contact_array* %7 to %class.gim_array*
  %call3 = call i32 @_ZNK9gim_arrayI11GIM_CONTACTE4sizeEv(%class.gim_array* %8)
  %call4 = call %class.gim_array.0* @_ZN9gim_arrayI15GIM_RSORT_TOKENEC2Ej(%class.gim_array.0* %keycontacts, i32 %call3)
  %9 = load %class.gim_contact_array*, %class.gim_contact_array** %contacts.addr, align 4, !tbaa !2
  %10 = bitcast %class.gim_contact_array* %9 to %class.gim_array*
  %call5 = call i32 @_ZNK9gim_arrayI11GIM_CONTACTE4sizeEv(%class.gim_array* %10)
  %11 = bitcast %struct.GIM_RSORT_TOKEN* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %11) #7
  %call6 = call %struct.GIM_RSORT_TOKEN* @_ZN15GIM_RSORT_TOKENC2Ev(%struct.GIM_RSORT_TOKEN* %ref.tmp)
  call void @_ZN9gim_arrayI15GIM_RSORT_TOKENE6resizeEjbRKS0_(%class.gim_array.0* %keycontacts, i32 %call5, i1 zeroext false, %struct.GIM_RSORT_TOKEN* nonnull align 4 dereferenceable(8) %ref.tmp)
  %12 = bitcast %struct.GIM_RSORT_TOKEN* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %12) #7
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %14 = load i32, i32* %i, align 4, !tbaa !8
  %15 = load %class.gim_contact_array*, %class.gim_contact_array** %contacts.addr, align 4, !tbaa !2
  %16 = bitcast %class.gim_contact_array* %15 to %class.gim_array*
  %call7 = call i32 @_ZNK9gim_arrayI11GIM_CONTACTE4sizeEv(%class.gim_array* %16)
  %cmp8 = icmp ult i32 %14, %call7
  br i1 %cmp8, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %17 = load %class.gim_contact_array*, %class.gim_contact_array** %contacts.addr, align 4, !tbaa !2
  %18 = bitcast %class.gim_contact_array* %17 to %class.gim_array*
  %19 = load i32, i32* %i, align 4, !tbaa !8
  %call9 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK9gim_arrayI11GIM_CONTACTEixEm(%class.gim_array* %18, i32 %19)
  %call10 = call i32 @_ZNK11GIM_CONTACT16calc_key_contactEv(%class.GIM_CONTACT* %call9)
  %20 = load i32, i32* %i, align 4, !tbaa !8
  %call11 = call nonnull align 4 dereferenceable(8) %struct.GIM_RSORT_TOKEN* @_ZN9gim_arrayI15GIM_RSORT_TOKENEixEm(%class.gim_array.0* %keycontacts, i32 %20)
  %m_key = getelementptr inbounds %struct.GIM_RSORT_TOKEN, %struct.GIM_RSORT_TOKEN* %call11, i32 0, i32 0
  store i32 %call10, i32* %m_key, align 4, !tbaa !10
  %21 = load i32, i32* %i, align 4, !tbaa !8
  %22 = load i32, i32* %i, align 4, !tbaa !8
  %call12 = call nonnull align 4 dereferenceable(8) %struct.GIM_RSORT_TOKEN* @_ZN9gim_arrayI15GIM_RSORT_TOKENEixEm(%class.gim_array.0* %keycontacts, i32 %22)
  %m_value = getelementptr inbounds %struct.GIM_RSORT_TOKEN, %struct.GIM_RSORT_TOKEN* %call12, i32 0, i32 1
  store i32 %21, i32* %m_value, align 4, !tbaa !12
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %23 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add i32 %23, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call13 = call %struct.GIM_RSORT_TOKEN* @_ZN9gim_arrayI15GIM_RSORT_TOKENE7pointerEv(%class.gim_array.0* %keycontacts)
  %call14 = call i32 @_ZNK9gim_arrayI15GIM_RSORT_TOKENE4sizeEv(%class.gim_array.0* %keycontacts)
  call void @_Z13gim_heap_sortI15GIM_RSORT_TOKEN26GIM_RSORT_TOKEN_COMPARATOREvPT_jT0_(%struct.GIM_RSORT_TOKEN* %call13, i32 %call14)
  %24 = bitcast i32* %coincident_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #7
  store i32 0, i32* %coincident_count, align 4, !tbaa !8
  %25 = bitcast [8 x %class.btVector3]* %coincident_normals to i8*
  call void @llvm.lifetime.start.p0i8(i64 128, i8* %25) #7
  %array.begin = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %coincident_normals, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 8
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %for.end
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %for.end ], [ %arrayctor.next, %arrayctor.loop ]
  %call15 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %26 = bitcast i32* %last_key to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #7
  %call16 = call nonnull align 4 dereferenceable(8) %struct.GIM_RSORT_TOKEN* @_ZN9gim_arrayI15GIM_RSORT_TOKENEixEm(%class.gim_array.0* %keycontacts, i32 0)
  %m_key17 = getelementptr inbounds %struct.GIM_RSORT_TOKEN, %struct.GIM_RSORT_TOKEN* %call16, i32 0, i32 0
  %27 = load i32, i32* %m_key17, align 4, !tbaa !10
  store i32 %27, i32* %last_key, align 4, !tbaa !8
  %28 = bitcast i32* %key to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #7
  store i32 0, i32* %key, align 4, !tbaa !8
  %29 = bitcast %class.gim_contact_array* %this1 to %class.gim_array*
  %30 = load %class.gim_contact_array*, %class.gim_contact_array** %contacts.addr, align 4, !tbaa !2
  %31 = bitcast %class.gim_contact_array* %30 to %class.gim_array*
  %call18 = call nonnull align 4 dereferenceable(8) %struct.GIM_RSORT_TOKEN* @_ZN9gim_arrayI15GIM_RSORT_TOKENEixEm(%class.gim_array.0* %keycontacts, i32 0)
  %m_value19 = getelementptr inbounds %struct.GIM_RSORT_TOKEN, %struct.GIM_RSORT_TOKEN* %call18, i32 0, i32 1
  %32 = load i32, i32* %m_value19, align 4, !tbaa !12
  %call20 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK9gim_arrayI11GIM_CONTACTEixEm(%class.gim_array* %31, i32 %32)
  call void @_ZN9gim_arrayI11GIM_CONTACTE9push_backERKS0_(%class.gim_array* %29, %class.GIM_CONTACT* nonnull align 4 dereferenceable(48) %call20)
  %33 = bitcast %class.GIM_CONTACT** %pcontact to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #7
  %34 = bitcast %class.gim_contact_array* %this1 to %class.gim_array*
  %call21 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZN9gim_arrayI11GIM_CONTACTE4backEv(%class.gim_array* %34)
  store %class.GIM_CONTACT* %call21, %class.GIM_CONTACT** %pcontact, align 4, !tbaa !2
  store i32 1, i32* %i, align 4, !tbaa !8
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc57, %arrayctor.cont
  %35 = load i32, i32* %i, align 4, !tbaa !8
  %call23 = call i32 @_ZNK9gim_arrayI15GIM_RSORT_TOKENE4sizeEv(%class.gim_array.0* %keycontacts)
  %cmp24 = icmp ult i32 %35, %call23
  br i1 %cmp24, label %for.body25, label %for.end59

for.body25:                                       ; preds = %for.cond22
  %36 = load i32, i32* %i, align 4, !tbaa !8
  %call26 = call nonnull align 4 dereferenceable(8) %struct.GIM_RSORT_TOKEN* @_ZN9gim_arrayI15GIM_RSORT_TOKENEixEm(%class.gim_array.0* %keycontacts, i32 %36)
  %m_key27 = getelementptr inbounds %struct.GIM_RSORT_TOKEN, %struct.GIM_RSORT_TOKEN* %call26, i32 0, i32 0
  %37 = load i32, i32* %m_key27, align 4, !tbaa !10
  store i32 %37, i32* %key, align 4, !tbaa !8
  %38 = bitcast %class.GIM_CONTACT** %scontact to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #7
  %39 = load %class.gim_contact_array*, %class.gim_contact_array** %contacts.addr, align 4, !tbaa !2
  %40 = bitcast %class.gim_contact_array* %39 to %class.gim_array*
  %41 = load i32, i32* %i, align 4, !tbaa !8
  %call28 = call nonnull align 4 dereferenceable(8) %struct.GIM_RSORT_TOKEN* @_ZN9gim_arrayI15GIM_RSORT_TOKENEixEm(%class.gim_array.0* %keycontacts, i32 %41)
  %m_value29 = getelementptr inbounds %struct.GIM_RSORT_TOKEN, %struct.GIM_RSORT_TOKEN* %call28, i32 0, i32 1
  %42 = load i32, i32* %m_value29, align 4, !tbaa !12
  %call30 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK9gim_arrayI11GIM_CONTACTEixEm(%class.gim_array* %40, i32 %42)
  store %class.GIM_CONTACT* %call30, %class.GIM_CONTACT** %scontact, align 4, !tbaa !2
  %43 = load i32, i32* %last_key, align 4, !tbaa !8
  %44 = load i32, i32* %key, align 4, !tbaa !8
  %cmp31 = icmp eq i32 %43, %44
  br i1 %cmp31, label %if.then32, label %if.else50

if.then32:                                        ; preds = %for.body25
  %45 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %pcontact, align 4, !tbaa !2
  %m_depth = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %45, i32 0, i32 2
  %46 = load float, float* %m_depth, align 4, !tbaa !13
  %sub = fsub float %46, 0x3EE4F8B580000000
  %47 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %scontact, align 4, !tbaa !2
  %m_depth33 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %47, i32 0, i32 2
  %48 = load float, float* %m_depth33, align 4, !tbaa !13
  %cmp34 = fcmp ogt float %sub, %48
  br i1 %cmp34, label %if.then35, label %if.else

if.then35:                                        ; preds = %if.then32
  %49 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %scontact, align 4, !tbaa !2
  %50 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %pcontact, align 4, !tbaa !2
  %51 = bitcast %class.GIM_CONTACT* %50 to i8*
  %52 = bitcast %class.GIM_CONTACT* %49 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %51, i8* align 4 %52, i32 48, i1 false), !tbaa.struct !17
  store i32 0, i32* %coincident_count, align 4, !tbaa !8
  br label %if.end49

if.else:                                          ; preds = %if.then32
  %53 = load i8, i8* %normal_contact_average.addr, align 1, !tbaa !6, !range !20
  %tobool = trunc i8 %53 to i1
  br i1 %tobool, label %if.then36, label %if.end48

if.then36:                                        ; preds = %if.else
  %54 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %pcontact, align 4, !tbaa !2
  %m_depth37 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %54, i32 0, i32 2
  %55 = load float, float* %m_depth37, align 4, !tbaa !13
  %56 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %scontact, align 4, !tbaa !2
  %m_depth38 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %56, i32 0, i32 2
  %57 = load float, float* %m_depth38, align 4, !tbaa !13
  %sub39 = fsub float %55, %57
  %call40 = call float @_Z6btFabsf(float %sub39)
  %cmp41 = fcmp olt float %call40, 0x3EE4F8B580000000
  br i1 %cmp41, label %if.then42, label %if.end47

if.then42:                                        ; preds = %if.then36
  %58 = load i32, i32* %coincident_count, align 4, !tbaa !8
  %cmp43 = icmp ult i32 %58, 8
  br i1 %cmp43, label %if.then44, label %if.end46

if.then44:                                        ; preds = %if.then42
  %59 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %scontact, align 4, !tbaa !2
  %m_normal = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %59, i32 0, i32 1
  %60 = load i32, i32* %coincident_count, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %coincident_normals, i32 0, i32 %60
  %61 = bitcast %class.btVector3* %arrayidx to i8*
  %62 = bitcast %class.btVector3* %m_normal to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %61, i8* align 4 %62, i32 16, i1 false), !tbaa.struct !21
  %63 = load i32, i32* %coincident_count, align 4, !tbaa !8
  %inc45 = add i32 %63, 1
  store i32 %inc45, i32* %coincident_count, align 4, !tbaa !8
  br label %if.end46

if.end46:                                         ; preds = %if.then44, %if.then42
  br label %if.end47

if.end47:                                         ; preds = %if.end46, %if.then36
  br label %if.end48

if.end48:                                         ; preds = %if.end47, %if.else
  br label %if.end49

if.end49:                                         ; preds = %if.end48, %if.then35
  br label %if.end56

if.else50:                                        ; preds = %for.body25
  %64 = load i8, i8* %normal_contact_average.addr, align 1, !tbaa !6, !range !20
  %tobool51 = trunc i8 %64 to i1
  br i1 %tobool51, label %land.lhs.true, label %if.end54

land.lhs.true:                                    ; preds = %if.else50
  %65 = load i32, i32* %coincident_count, align 4, !tbaa !8
  %cmp52 = icmp ugt i32 %65, 0
  br i1 %cmp52, label %if.then53, label %if.end54

if.then53:                                        ; preds = %land.lhs.true
  %66 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %pcontact, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %coincident_normals, i32 0, i32 0
  %67 = load i32, i32* %coincident_count, align 4, !tbaa !8
  call void @_ZN11GIM_CONTACT19interpolate_normalsEP9btVector3j(%class.GIM_CONTACT* %66, %class.btVector3* %arraydecay, i32 %67)
  store i32 0, i32* %coincident_count, align 4, !tbaa !8
  br label %if.end54

if.end54:                                         ; preds = %if.then53, %land.lhs.true, %if.else50
  %68 = bitcast %class.gim_contact_array* %this1 to %class.gim_array*
  %69 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %scontact, align 4, !tbaa !2
  call void @_ZN9gim_arrayI11GIM_CONTACTE9push_backERKS0_(%class.gim_array* %68, %class.GIM_CONTACT* nonnull align 4 dereferenceable(48) %69)
  %70 = bitcast %class.gim_contact_array* %this1 to %class.gim_array*
  %call55 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZN9gim_arrayI11GIM_CONTACTE4backEv(%class.gim_array* %70)
  store %class.GIM_CONTACT* %call55, %class.GIM_CONTACT** %pcontact, align 4, !tbaa !2
  br label %if.end56

if.end56:                                         ; preds = %if.end54, %if.end49
  %71 = load i32, i32* %key, align 4, !tbaa !8
  store i32 %71, i32* %last_key, align 4, !tbaa !8
  %72 = bitcast %class.GIM_CONTACT** %scontact to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #7
  br label %for.inc57

for.inc57:                                        ; preds = %if.end56
  %73 = load i32, i32* %i, align 4, !tbaa !8
  %inc58 = add i32 %73, 1
  store i32 %inc58, i32* %i, align 4, !tbaa !8
  br label %for.cond22

for.end59:                                        ; preds = %for.cond22
  %74 = bitcast %class.GIM_CONTACT** %pcontact to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #7
  %75 = bitcast i32* %key to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #7
  %76 = bitcast i32* %last_key to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #7
  %77 = bitcast [8 x %class.btVector3]* %coincident_normals to i8*
  call void @llvm.lifetime.end.p0i8(i64 128, i8* %77) #7
  %78 = bitcast i32* %coincident_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #7
  %79 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #7
  %call60 = call %class.gim_array.0* @_ZN9gim_arrayI15GIM_RSORT_TOKENED2Ev(%class.gim_array.0* %keycontacts) #7
  %80 = bitcast %class.gim_array.0* %keycontacts to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %80) #7
  br label %return

return:                                           ; preds = %for.end59, %if.then
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN9gim_arrayI11GIM_CONTACTE5clearEv(%class.gim_array* %this) #1 comdat {
entry:
  %this.addr = alloca %class.gim_array*, align 4
  store %class.gim_array* %this, %class.gim_array** %this.addr, align 4, !tbaa !2
  %this1 = load %class.gim_array*, %class.gim_array** %this.addr, align 4
  %m_size = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_size, align 4, !tbaa !22
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  call void @_ZN9gim_arrayI11GIM_CONTACTE11clear_rangeEj(%class.gim_array* %this1, i32 0)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK9gim_arrayI11GIM_CONTACTE4sizeEv(%class.gim_array* %this) #2 comdat {
entry:
  %this.addr = alloca %class.gim_array*, align 4
  store %class.gim_array* %this, %class.gim_array** %this.addr, align 4, !tbaa !2
  %this1 = load %class.gim_array*, %class.gim_array** %this.addr, align 4
  %m_size = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_size, align 4, !tbaa !22
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN9gim_arrayI11GIM_CONTACTE9push_backERKS0_(%class.gim_array* %this, %class.GIM_CONTACT* nonnull align 4 dereferenceable(48) %obj) #1 comdat {
entry:
  %this.addr = alloca %class.gim_array*, align 4
  %obj.addr = alloca %class.GIM_CONTACT*, align 4
  store %class.gim_array* %this, %class.gim_array** %this.addr, align 4, !tbaa !2
  store %class.GIM_CONTACT* %obj, %class.GIM_CONTACT** %obj.addr, align 4, !tbaa !2
  %this1 = load %class.gim_array*, %class.gim_array** %this.addr, align 4
  %call = call zeroext i1 @_ZN9gim_arrayI11GIM_CONTACTE12growingCheckEv(%class.gim_array* %this1)
  %0 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %obj.addr, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 0
  %1 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %m_data, align 4, !tbaa !24
  %m_size = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 1
  %2 = load i32, i32* %m_size, align 4, !tbaa !22
  %arrayidx = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %1, i32 %2
  %3 = bitcast %class.GIM_CONTACT* %arrayidx to i8*
  %4 = bitcast %class.GIM_CONTACT* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 48, i1 false), !tbaa.struct !17
  %m_size2 = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 1
  %5 = load i32, i32* %m_size2, align 4, !tbaa !22
  %inc = add i32 %5, 1
  store i32 %inc, i32* %m_size2, align 4, !tbaa !22
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK9gim_arrayI11GIM_CONTACTE4backEv(%class.gim_array* %this) #2 comdat {
entry:
  %this.addr = alloca %class.gim_array*, align 4
  store %class.gim_array* %this, %class.gim_array** %this.addr, align 4, !tbaa !2
  %this1 = load %class.gim_array*, %class.gim_array** %this.addr, align 4
  %m_data = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 0
  %0 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %m_data, align 4, !tbaa !24
  %m_size = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 1
  %1 = load i32, i32* %m_size, align 4, !tbaa !22
  %sub = sub i32 %1, 1
  %arrayidx = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %0, i32 %sub
  ret %class.GIM_CONTACT* %arrayidx
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #3

define linkonce_odr hidden %class.gim_array.0* @_ZN9gim_arrayI15GIM_RSORT_TOKENEC2Ej(%class.gim_array.0* returned %this, i32 %reservesize) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.gim_array.0*, align 4
  %reservesize.addr = alloca i32, align 4
  store %class.gim_array.0* %this, %class.gim_array.0** %this.addr, align 4, !tbaa !2
  store i32 %reservesize, i32* %reservesize.addr, align 4, !tbaa !8
  %this1 = load %class.gim_array.0*, %class.gim_array.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 0
  store %struct.GIM_RSORT_TOKEN* null, %struct.GIM_RSORT_TOKEN** %m_data, align 4, !tbaa !25
  %m_size = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 1
  store i32 0, i32* %m_size, align 4, !tbaa !27
  %m_allocated_size = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_allocated_size, align 4, !tbaa !28
  %0 = load i32, i32* %reservesize.addr, align 4, !tbaa !8
  %call = call zeroext i1 @_ZN9gim_arrayI15GIM_RSORT_TOKENE7reserveEj(%class.gim_array.0* %this1, i32 %0)
  ret %class.gim_array.0* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN9gim_arrayI15GIM_RSORT_TOKENE6resizeEjbRKS0_(%class.gim_array.0* %this, i32 %size, i1 zeroext %call_constructor, %struct.GIM_RSORT_TOKEN* nonnull align 4 dereferenceable(8) %fillData) #1 comdat {
entry:
  %this.addr = alloca %class.gim_array.0*, align 4
  %size.addr = alloca i32, align 4
  %call_constructor.addr = alloca i8, align 1
  %fillData.addr = alloca %struct.GIM_RSORT_TOKEN*, align 4
  store %class.gim_array.0* %this, %class.gim_array.0** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !8
  %frombool = zext i1 %call_constructor to i8
  store i8 %frombool, i8* %call_constructor.addr, align 1, !tbaa !6
  store %struct.GIM_RSORT_TOKEN* %fillData, %struct.GIM_RSORT_TOKEN** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.gim_array.0*, %class.gim_array.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !8
  %m_size = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 1
  %1 = load i32, i32* %m_size, align 4, !tbaa !27
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else8

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %size.addr, align 4, !tbaa !8
  %call = call zeroext i1 @_ZN9gim_arrayI15GIM_RSORT_TOKENE7reserveEj(%class.gim_array.0* %this1, i32 %2)
  %3 = load i8, i8* %call_constructor.addr, align 1, !tbaa !6, !range !20
  %tobool = trunc i8 %3 to i1
  br i1 %tobool, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.then2
  %m_size3 = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 1
  %4 = load i32, i32* %m_size3, align 4, !tbaa !27
  %5 = load i32, i32* %size.addr, align 4, !tbaa !8
  %cmp4 = icmp ult i32 %4, %5
  br i1 %cmp4, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %6 = load %struct.GIM_RSORT_TOKEN*, %struct.GIM_RSORT_TOKEN** %fillData.addr, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 0
  %7 = load %struct.GIM_RSORT_TOKEN*, %struct.GIM_RSORT_TOKEN** %m_data, align 4, !tbaa !25
  %m_size5 = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 1
  %8 = load i32, i32* %m_size5, align 4, !tbaa !27
  %arrayidx = getelementptr inbounds %struct.GIM_RSORT_TOKEN, %struct.GIM_RSORT_TOKEN* %7, i32 %8
  %9 = bitcast %struct.GIM_RSORT_TOKEN* %arrayidx to i8*
  %10 = bitcast %struct.GIM_RSORT_TOKEN* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 8, i1 false), !tbaa.struct !29
  %m_size6 = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 1
  %11 = load i32, i32* %m_size6, align 4, !tbaa !27
  %inc = add i32 %11, 1
  store i32 %inc, i32* %m_size6, align 4, !tbaa !27
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %if.end

if.else:                                          ; preds = %if.then
  %12 = load i32, i32* %size.addr, align 4, !tbaa !8
  %m_size7 = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 1
  store i32 %12, i32* %m_size7, align 4, !tbaa !27
  br label %if.end

if.end:                                           ; preds = %if.else, %while.end
  br label %if.end17

if.else8:                                         ; preds = %entry
  %13 = load i32, i32* %size.addr, align 4, !tbaa !8
  %m_size9 = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 1
  %14 = load i32, i32* %m_size9, align 4, !tbaa !27
  %cmp10 = icmp ult i32 %13, %14
  br i1 %cmp10, label %if.then11, label %if.end16

if.then11:                                        ; preds = %if.else8
  %15 = load i8, i8* %call_constructor.addr, align 1, !tbaa !6, !range !20
  %tobool12 = trunc i8 %15 to i1
  br i1 %tobool12, label %if.then13, label %if.end14

if.then13:                                        ; preds = %if.then11
  %16 = load i32, i32* %size.addr, align 4, !tbaa !8
  call void @_ZN9gim_arrayI15GIM_RSORT_TOKENE11clear_rangeEj(%class.gim_array.0* %this1, i32 %16)
  br label %if.end14

if.end14:                                         ; preds = %if.then13, %if.then11
  %17 = load i32, i32* %size.addr, align 4, !tbaa !8
  %m_size15 = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 1
  store i32 %17, i32* %m_size15, align 4, !tbaa !27
  br label %if.end16

if.end16:                                         ; preds = %if.end14, %if.else8
  br label %if.end17

if.end17:                                         ; preds = %if.end16, %if.end
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.GIM_RSORT_TOKEN* @_ZN15GIM_RSORT_TOKENC2Ev(%struct.GIM_RSORT_TOKEN* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %struct.GIM_RSORT_TOKEN*, align 4
  store %struct.GIM_RSORT_TOKEN* %this, %struct.GIM_RSORT_TOKEN** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.GIM_RSORT_TOKEN*, %struct.GIM_RSORT_TOKEN** %this.addr, align 4
  ret %struct.GIM_RSORT_TOKEN* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK9gim_arrayI11GIM_CONTACTEixEm(%class.gim_array* %this, i32 %i) #2 comdat {
entry:
  %this.addr = alloca %class.gim_array*, align 4
  %i.addr = alloca i32, align 4
  store %class.gim_array* %this, %class.gim_array** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !30
  %this1 = load %class.gim_array*, %class.gim_array** %this.addr, align 4
  %m_data = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 0
  %0 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %m_data, align 4, !tbaa !24
  %1 = load i32, i32* %i.addr, align 4, !tbaa !30
  %arrayidx = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %0, i32 %1
  ret %class.GIM_CONTACT* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden i32 @_ZNK11GIM_CONTACT16calc_key_contactEv(%class.GIM_CONTACT* %this) #1 comdat {
entry:
  %this.addr = alloca %class.GIM_CONTACT*, align 4
  %_coords = alloca [3 x i32], align 4
  %_hash = alloca i32, align 4
  %_uitmp = alloca i32*, align 4
  store %class.GIM_CONTACT* %this, %class.GIM_CONTACT** %this.addr, align 4, !tbaa !2
  %this1 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %this.addr, align 4
  %0 = bitcast [3 x i32]* %_coords to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %0) #7
  %arrayinit.begin = getelementptr inbounds [3 x i32], [3 x i32]* %_coords, i32 0, i32 0
  %m_point = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_point)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !19
  %mul = fmul float %1, 1.000000e+03
  %add = fadd float %mul, 1.000000e+00
  %conv = fptosi float %add to i32
  store i32 %conv, i32* %arrayinit.begin, align 4, !tbaa !8
  %arrayinit.element = getelementptr inbounds i32, i32* %arrayinit.begin, i32 1
  %m_point2 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 0
  %call3 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_point2)
  %arrayidx4 = getelementptr inbounds float, float* %call3, i32 1
  %2 = load float, float* %arrayidx4, align 4, !tbaa !19
  %mul5 = fmul float %2, 1.333000e+03
  %conv6 = fptosi float %mul5 to i32
  store i32 %conv6, i32* %arrayinit.element, align 4, !tbaa !8
  %arrayinit.element7 = getelementptr inbounds i32, i32* %arrayinit.element, i32 1
  %m_point8 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 0
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_point8)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 2
  %3 = load float, float* %arrayidx10, align 4, !tbaa !19
  %mul11 = fmul float %3, 2.133000e+03
  %add12 = fadd float %mul11, 3.000000e+00
  %conv13 = fptosi float %add12 to i32
  store i32 %conv13, i32* %arrayinit.element7, align 4, !tbaa !8
  %4 = bitcast i32* %_hash to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  store i32 0, i32* %_hash, align 4, !tbaa !8
  %5 = bitcast i32** %_uitmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %arrayidx14 = getelementptr inbounds [3 x i32], [3 x i32]* %_coords, i32 0, i32 0
  store i32* %arrayidx14, i32** %_uitmp, align 4, !tbaa !2
  %6 = load i32*, i32** %_uitmp, align 4, !tbaa !2
  %7 = load i32, i32* %6, align 4, !tbaa !8
  store i32 %7, i32* %_hash, align 4, !tbaa !8
  %8 = load i32*, i32** %_uitmp, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i32, i32* %8, i32 1
  store i32* %incdec.ptr, i32** %_uitmp, align 4, !tbaa !2
  %9 = load i32*, i32** %_uitmp, align 4, !tbaa !2
  %10 = load i32, i32* %9, align 4, !tbaa !8
  %shl = shl i32 %10, 4
  %11 = load i32, i32* %_hash, align 4, !tbaa !8
  %add15 = add i32 %11, %shl
  store i32 %add15, i32* %_hash, align 4, !tbaa !8
  %12 = load i32*, i32** %_uitmp, align 4, !tbaa !2
  %incdec.ptr16 = getelementptr inbounds i32, i32* %12, i32 1
  store i32* %incdec.ptr16, i32** %_uitmp, align 4, !tbaa !2
  %13 = load i32*, i32** %_uitmp, align 4, !tbaa !2
  %14 = load i32, i32* %13, align 4, !tbaa !8
  %shl17 = shl i32 %14, 8
  %15 = load i32, i32* %_hash, align 4, !tbaa !8
  %add18 = add i32 %15, %shl17
  store i32 %add18, i32* %_hash, align 4, !tbaa !8
  %16 = load i32, i32* %_hash, align 4, !tbaa !8
  %17 = bitcast i32** %_uitmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  %18 = bitcast i32* %_hash to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #7
  %19 = bitcast [3 x i32]* %_coords to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %19) #7
  ret i32 %16
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %struct.GIM_RSORT_TOKEN* @_ZN9gim_arrayI15GIM_RSORT_TOKENEixEm(%class.gim_array.0* %this, i32 %i) #2 comdat {
entry:
  %this.addr = alloca %class.gim_array.0*, align 4
  %i.addr = alloca i32, align 4
  store %class.gim_array.0* %this, %class.gim_array.0** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !30
  %this1 = load %class.gim_array.0*, %class.gim_array.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 0
  %0 = load %struct.GIM_RSORT_TOKEN*, %struct.GIM_RSORT_TOKEN** %m_data, align 4, !tbaa !25
  %1 = load i32, i32* %i.addr, align 4, !tbaa !30
  %arrayidx = getelementptr inbounds %struct.GIM_RSORT_TOKEN, %struct.GIM_RSORT_TOKEN* %0, i32 %1
  ret %struct.GIM_RSORT_TOKEN* %arrayidx
}

define linkonce_odr hidden void @_Z13gim_heap_sortI15GIM_RSORT_TOKEN26GIM_RSORT_TOKEN_COMPARATOREvPT_jT0_(%struct.GIM_RSORT_TOKEN* %pArr, i32 %element_count) #0 comdat {
entry:
  %CompareFunc = alloca %class.GIM_RSORT_TOKEN_COMPARATOR, align 1
  %pArr.addr = alloca %struct.GIM_RSORT_TOKEN*, align 4
  %element_count.addr = alloca i32, align 4
  %k = alloca i32, align 4
  %n = alloca i32, align 4
  %agg.tmp = alloca %class.GIM_RSORT_TOKEN_COMPARATOR, align 1
  %agg.tmp3 = alloca %class.GIM_RSORT_TOKEN_COMPARATOR, align 1
  store %struct.GIM_RSORT_TOKEN* %pArr, %struct.GIM_RSORT_TOKEN** %pArr.addr, align 4, !tbaa !2
  store i32 %element_count, i32* %element_count.addr, align 4, !tbaa !8
  %0 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i32, i32* %element_count.addr, align 4, !tbaa !8
  store i32 %2, i32* %n, align 4, !tbaa !8
  %3 = load i32, i32* %n, align 4, !tbaa !8
  %div = udiv i32 %3, 2
  store i32 %div, i32* %k, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %4 = load i32, i32* %k, align 4, !tbaa !8
  %cmp = icmp ugt i32 %4, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load %struct.GIM_RSORT_TOKEN*, %struct.GIM_RSORT_TOKEN** %pArr.addr, align 4, !tbaa !2
  %6 = load i32, i32* %k, align 4, !tbaa !8
  %7 = load i32, i32* %n, align 4, !tbaa !8
  call void @_Z13gim_down_heapI15GIM_RSORT_TOKEN26GIM_RSORT_TOKEN_COMPARATOREvPT_jjT0_(%struct.GIM_RSORT_TOKEN* %5, i32 %6, i32 %7)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %k, align 4, !tbaa !8
  %dec = add i32 %8, -1
  store i32 %dec, i32* %k, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.cond:                                       ; preds = %while.body, %for.end
  %9 = load i32, i32* %n, align 4, !tbaa !8
  %cmp1 = icmp uge i32 %9, 2
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %10 = load %struct.GIM_RSORT_TOKEN*, %struct.GIM_RSORT_TOKEN** %pArr.addr, align 4, !tbaa !2
  %11 = load i32, i32* %n, align 4, !tbaa !8
  %sub = sub i32 %11, 1
  call void @_Z17gim_swap_elementsI15GIM_RSORT_TOKENEvPT_mm(%struct.GIM_RSORT_TOKEN* %10, i32 0, i32 %sub)
  %12 = load i32, i32* %n, align 4, !tbaa !8
  %dec2 = add i32 %12, -1
  store i32 %dec2, i32* %n, align 4, !tbaa !8
  %13 = load %struct.GIM_RSORT_TOKEN*, %struct.GIM_RSORT_TOKEN** %pArr.addr, align 4, !tbaa !2
  %14 = load i32, i32* %n, align 4, !tbaa !8
  call void @_Z13gim_down_heapI15GIM_RSORT_TOKEN26GIM_RSORT_TOKEN_COMPARATOREvPT_jjT0_(%struct.GIM_RSORT_TOKEN* %13, i32 1, i32 %14)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %15 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #7
  %16 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %struct.GIM_RSORT_TOKEN* @_ZN9gim_arrayI15GIM_RSORT_TOKENE7pointerEv(%class.gim_array.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.gim_array.0*, align 4
  store %class.gim_array.0* %this, %class.gim_array.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.gim_array.0*, %class.gim_array.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 0
  %0 = load %struct.GIM_RSORT_TOKEN*, %struct.GIM_RSORT_TOKEN** %m_data, align 4, !tbaa !25
  ret %struct.GIM_RSORT_TOKEN* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK9gim_arrayI15GIM_RSORT_TOKENE4sizeEv(%class.gim_array.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.gim_array.0*, align 4
  store %class.gim_array.0* %this, %class.gim_array.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.gim_array.0*, %class.gim_array.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_size, align 4, !tbaa !27
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZN9gim_arrayI11GIM_CONTACTE4backEv(%class.gim_array* %this) #2 comdat {
entry:
  %this.addr = alloca %class.gim_array*, align 4
  store %class.gim_array* %this, %class.gim_array** %this.addr, align 4, !tbaa !2
  %this1 = load %class.gim_array*, %class.gim_array** %this.addr, align 4
  %m_data = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 0
  %0 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %m_data, align 4, !tbaa !24
  %m_size = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 1
  %1 = load i32, i32* %m_size, align 4, !tbaa !22
  %sub = sub i32 %1, 1
  %arrayidx = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %0, i32 %sub
  ret %class.GIM_CONTACT* %arrayidx
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btFabsf(float %x) #2 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !19
  %0 = load float, float* %x.addr, align 4, !tbaa !19
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN11GIM_CONTACT19interpolate_normalsEP9btVector3j(%class.GIM_CONTACT* %this, %class.btVector3* %normals, i32 %normal_count) #1 comdat {
entry:
  %this.addr = alloca %class.GIM_CONTACT*, align 4
  %normals.addr = alloca %class.btVector3*, align 4
  %normal_count.addr = alloca i32, align 4
  %vec_sum = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  %vec_sum_len = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %_x = alloca float, align 4
  %_y = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.GIM_CONTACT* %this, %class.GIM_CONTACT** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %normals, %class.btVector3** %normals.addr, align 4, !tbaa !2
  store i32 %normal_count, i32* %normal_count.addr, align 4, !tbaa !8
  %this1 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %this.addr, align 4
  %0 = bitcast %class.btVector3* %vec_sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #7
  %m_normal = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %vec_sum to i8*
  %2 = bitcast %class.btVector3* %m_normal to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !21
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %4 = load i32, i32* %i, align 4, !tbaa !8
  %5 = load i32, i32* %normal_count.addr, align 4, !tbaa !8
  %cmp = icmp ult i32 %4, %5
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %6 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load %class.btVector3*, %class.btVector3** %normals.addr, align 4, !tbaa !2
  %8 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 %8
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %vec_sum, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add i32 %9, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %10 = bitcast float* %vec_sum_len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %call2 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %vec_sum)
  store float %call2, float* %vec_sum_len, align 4, !tbaa !19
  %11 = load float, float* %vec_sum_len, align 4, !tbaa !19
  %cmp3 = fcmp olt float %11, 0x3EE4F8B580000000
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.end
  %12 = load float, float* %vec_sum_len, align 4, !tbaa !19
  %cmp4 = fcmp ole float %12, 0x3E7AD7F2A0000000
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  store float 0x47EFFFFFE0000000, float* %vec_sum_len, align 4, !tbaa !19
  br label %if.end10

if.else:                                          ; preds = %if.end
  %13 = bitcast float* %_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = load float, float* %vec_sum_len, align 4, !tbaa !19
  %mul = fmul float %14, 5.000000e-01
  store float %mul, float* %_x, align 4, !tbaa !19
  %15 = bitcast i32* %_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #7
  %16 = bitcast float* %vec_sum_len to i32*
  %17 = load i32, i32* %16, align 4, !tbaa !8
  %shr = lshr i32 %17, 1
  %sub = sub i32 1597463007, %shr
  store i32 %sub, i32* %_y, align 4, !tbaa !8
  %18 = bitcast i32* %_y to float*
  %19 = load float, float* %18, align 4, !tbaa !19
  store float %19, float* %vec_sum_len, align 4, !tbaa !19
  %20 = load float, float* %vec_sum_len, align 4, !tbaa !19
  %21 = load float, float* %_x, align 4, !tbaa !19
  %22 = load float, float* %vec_sum_len, align 4, !tbaa !19
  %mul6 = fmul float %21, %22
  %23 = load float, float* %vec_sum_len, align 4, !tbaa !19
  %mul7 = fmul float %mul6, %23
  %sub8 = fsub float 1.500000e+00, %mul7
  %mul9 = fmul float %20, %sub8
  store float %mul9, float* %vec_sum_len, align 4, !tbaa !19
  %24 = bitcast i32* %_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #7
  %25 = bitcast float* %_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #7
  br label %if.end10

if.end10:                                         ; preds = %if.else, %if.then5
  %26 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %26) #7
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %vec_sum, float* nonnull align 4 dereferenceable(4) %vec_sum_len)
  %m_normal11 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 1
  %27 = bitcast %class.btVector3* %m_normal11 to i8*
  %28 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %27, i8* align 4 %28, i32 16, i1 false), !tbaa.struct !21
  %29 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end10, %if.then
  %30 = bitcast float* %vec_sum_len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #7
  %31 = bitcast %class.btVector3* %vec_sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %31) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.gim_array.0* @_ZN9gim_arrayI15GIM_RSORT_TOKENED2Ev(%class.gim_array.0* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.gim_array.0*, align 4
  store %class.gim_array.0* %this, %class.gim_array.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.gim_array.0*, %class.gim_array.0** %this.addr, align 4
  call void @_ZN9gim_arrayI15GIM_RSORT_TOKENE12clear_memoryEv(%class.gim_array.0* %this1)
  ret %class.gim_array.0* %this1
}

define hidden void @_ZN17gim_contact_array21merge_contacts_uniqueERKS_(%class.gim_contact_array* %this, %class.gim_contact_array* nonnull align 4 dereferenceable(12) %contacts) #0 {
entry:
  %this.addr = alloca %class.gim_contact_array*, align 4
  %contacts.addr = alloca %class.gim_contact_array*, align 4
  %average_contact = alloca %class.GIM_CONTACT, align 4
  %i = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %divide_average = alloca float, align 4
  store %class.gim_contact_array* %this, %class.gim_contact_array** %this.addr, align 4, !tbaa !2
  store %class.gim_contact_array* %contacts, %class.gim_contact_array** %contacts.addr, align 4, !tbaa !2
  %this1 = load %class.gim_contact_array*, %class.gim_contact_array** %this.addr, align 4
  %0 = bitcast %class.gim_contact_array* %this1 to %class.gim_array*
  call void @_ZN9gim_arrayI11GIM_CONTACTE5clearEv(%class.gim_array* %0)
  %1 = load %class.gim_contact_array*, %class.gim_contact_array** %contacts.addr, align 4, !tbaa !2
  %2 = bitcast %class.gim_contact_array* %1 to %class.gim_array*
  %call = call i32 @_ZNK9gim_arrayI11GIM_CONTACTE4sizeEv(%class.gim_array* %2)
  %cmp = icmp eq i32 %call, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = bitcast %class.gim_contact_array* %this1 to %class.gim_array*
  %4 = load %class.gim_contact_array*, %class.gim_contact_array** %contacts.addr, align 4, !tbaa !2
  %5 = bitcast %class.gim_contact_array* %4 to %class.gim_array*
  %call2 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK9gim_arrayI11GIM_CONTACTE4backEv(%class.gim_array* %5)
  call void @_ZN9gim_arrayI11GIM_CONTACTE9push_backERKS0_(%class.gim_array* %3, %class.GIM_CONTACT* nonnull align 4 dereferenceable(48) %call2)
  br label %return

if.end:                                           ; preds = %entry
  %6 = bitcast %class.GIM_CONTACT* %average_contact to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %6) #7
  %7 = load %class.gim_contact_array*, %class.gim_contact_array** %contacts.addr, align 4, !tbaa !2
  %8 = bitcast %class.gim_contact_array* %7 to %class.gim_array*
  %call3 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK9gim_arrayI11GIM_CONTACTE4backEv(%class.gim_array* %8)
  %call4 = call %class.GIM_CONTACT* @_ZN11GIM_CONTACTC2ERKS_(%class.GIM_CONTACT* %average_contact, %class.GIM_CONTACT* nonnull align 4 dereferenceable(48) %call3)
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  store i32 1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %10 = load i32, i32* %i, align 4, !tbaa !8
  %11 = load %class.gim_contact_array*, %class.gim_contact_array** %contacts.addr, align 4, !tbaa !2
  %12 = bitcast %class.gim_contact_array* %11 to %class.gim_array*
  %call5 = call i32 @_ZNK9gim_arrayI11GIM_CONTACTE4sizeEv(%class.gim_array* %12)
  %cmp6 = icmp ult i32 %10, %call5
  br i1 %cmp6, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %14 = load %class.gim_contact_array*, %class.gim_contact_array** %contacts.addr, align 4, !tbaa !2
  %15 = bitcast %class.gim_contact_array* %14 to %class.gim_array*
  %16 = load i32, i32* %i, align 4, !tbaa !8
  %call7 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK9gim_arrayI11GIM_CONTACTEixEm(%class.gim_array* %15, i32 %16)
  %m_point = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %call7, i32 0, i32 0
  %m_point8 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %average_contact, i32 0, i32 0
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_point8, %class.btVector3* nonnull align 4 dereferenceable(16) %m_point)
  %17 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %17) #7
  %18 = load %class.gim_contact_array*, %class.gim_contact_array** %contacts.addr, align 4, !tbaa !2
  %19 = bitcast %class.gim_contact_array* %18 to %class.gim_array*
  %20 = load i32, i32* %i, align 4, !tbaa !8
  %call10 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK9gim_arrayI11GIM_CONTACTEixEm(%class.gim_array* %19, i32 %20)
  %m_normal = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %call10, i32 0, i32 1
  %21 = load %class.gim_contact_array*, %class.gim_contact_array** %contacts.addr, align 4, !tbaa !2
  %22 = bitcast %class.gim_contact_array* %21 to %class.gim_array*
  %23 = load i32, i32* %i, align 4, !tbaa !8
  %call11 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK9gim_arrayI11GIM_CONTACTEixEm(%class.gim_array* %22, i32 %23)
  %m_depth = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %call11, i32 0, i32 2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normal, float* nonnull align 4 dereferenceable(4) %m_depth)
  %m_normal12 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %average_contact, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_normal12, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %24 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %24) #7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %25 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add i32 %25, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %26 = bitcast float* %divide_average to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #7
  %27 = load %class.gim_contact_array*, %class.gim_contact_array** %contacts.addr, align 4, !tbaa !2
  %28 = bitcast %class.gim_contact_array* %27 to %class.gim_array*
  %call14 = call i32 @_ZNK9gim_arrayI11GIM_CONTACTE4sizeEv(%class.gim_array* %28)
  %conv = uitofp i32 %call14 to float
  %div = fdiv float 1.000000e+00, %conv
  store float %div, float* %divide_average, align 4, !tbaa !19
  %m_point15 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %average_contact, i32 0, i32 0
  %call16 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %m_point15, float* nonnull align 4 dereferenceable(4) %divide_average)
  %m_normal17 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %average_contact, i32 0, i32 1
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %m_normal17, float* nonnull align 4 dereferenceable(4) %divide_average)
  %m_normal19 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %average_contact, i32 0, i32 1
  %call20 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %m_normal19)
  %m_depth21 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %average_contact, i32 0, i32 2
  store float %call20, float* %m_depth21, align 4, !tbaa !13
  %m_depth22 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %average_contact, i32 0, i32 2
  %m_normal23 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %average_contact, i32 0, i32 1
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %m_normal23, float* nonnull align 4 dereferenceable(4) %m_depth22)
  %29 = bitcast float* %divide_average to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #7
  %30 = bitcast %class.GIM_CONTACT* %average_contact to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %30) #7
  br label %return

return:                                           ; preds = %for.end, %if.then
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.GIM_CONTACT* @_ZN11GIM_CONTACTC2ERKS_(%class.GIM_CONTACT* returned %this, %class.GIM_CONTACT* nonnull align 4 dereferenceable(48) %contact) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.GIM_CONTACT*, align 4
  %contact.addr = alloca %class.GIM_CONTACT*, align 4
  store %class.GIM_CONTACT* %this, %class.GIM_CONTACT** %this.addr, align 4, !tbaa !2
  store %class.GIM_CONTACT* %contact, %class.GIM_CONTACT** %contact.addr, align 4, !tbaa !2
  %this1 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %this.addr, align 4
  %m_point = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 0
  %0 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %contact.addr, align 4, !tbaa !2
  %m_point2 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %0, i32 0, i32 0
  %1 = bitcast %class.btVector3* %m_point to i8*
  %2 = bitcast %class.btVector3* %m_point2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !21
  %m_normal = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 1
  %3 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %contact.addr, align 4, !tbaa !2
  %m_normal3 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %3, i32 0, i32 1
  %4 = bitcast %class.btVector3* %m_normal to i8*
  %5 = bitcast %class.btVector3* %m_normal3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !21
  %m_depth = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 2
  %6 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %contact.addr, align 4, !tbaa !2
  %m_depth4 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %6, i32 0, i32 2
  %7 = load float, float* %m_depth4, align 4, !tbaa !13
  store float %7, float* %m_depth, align 4, !tbaa !13
  %m_feature1 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 4
  %8 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %contact.addr, align 4, !tbaa !2
  %m_feature15 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %8, i32 0, i32 4
  %9 = load i32, i32* %m_feature15, align 4, !tbaa !32
  store i32 %9, i32* %m_feature1, align 4, !tbaa !32
  %m_feature2 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 5
  %10 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %contact.addr, align 4, !tbaa !2
  %m_feature26 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %10, i32 0, i32 5
  %11 = load i32, i32* %m_feature26, align 4, !tbaa !33
  store i32 %11, i32* %m_feature2, align 4, !tbaa !33
  %12 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %contact.addr, align 4, !tbaa !2
  %m_point7 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %12, i32 0, i32 0
  %m_point8 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 0
  %13 = bitcast %class.btVector3* %m_point8 to i8*
  %14 = bitcast %class.btVector3* %m_point7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 16, i1 false), !tbaa.struct !21
  %15 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %contact.addr, align 4, !tbaa !2
  %m_normal9 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %15, i32 0, i32 1
  %m_normal10 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 1
  %16 = bitcast %class.btVector3* %m_normal10 to i8*
  %17 = bitcast %class.btVector3* %m_normal9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false), !tbaa.struct !21
  %18 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %contact.addr, align 4, !tbaa !2
  %m_depth11 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %18, i32 0, i32 2
  %19 = load float, float* %m_depth11, align 4, !tbaa !13
  %m_depth12 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 2
  store float %19, float* %m_depth12, align 4, !tbaa !13
  %20 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %contact.addr, align 4, !tbaa !2
  %m_feature113 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %20, i32 0, i32 4
  %21 = load i32, i32* %m_feature113, align 4, !tbaa !32
  %m_feature114 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 4
  store i32 %21, i32* %m_feature114, align 4, !tbaa !32
  %22 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %contact.addr, align 4, !tbaa !2
  %m_feature215 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %22, i32 0, i32 5
  %23 = load i32, i32* %m_feature215, align 4, !tbaa !33
  %m_feature216 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 5
  store i32 %23, i32* %m_feature216, align 4, !tbaa !33
  ret %class.GIM_CONTACT* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !19
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !19
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !19
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !19
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !19
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !19
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !19
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !19
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !19
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !19
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !19
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !19
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !19
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !19
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !19
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !19
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !19
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !19
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #7
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #7
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !19
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !19
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !19
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !19
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !19
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !19
  %6 = load float*, float** %s.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !19
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !19
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !19
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !19
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !19
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #7
  ret %class.btVector3* %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #5

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !19
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !19
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !19
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !19
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !19
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !19
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !19
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !19
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !19
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !19
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !19
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !19
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !19
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #2 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !19
  %0 = load float, float* %y.addr, align 4, !tbaa !19
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #5

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9gim_arrayI11GIM_CONTACTE11clear_rangeEj(%class.gim_array* %this, i32 %start_range) #2 comdat {
entry:
  %this.addr = alloca %class.gim_array*, align 4
  %start_range.addr = alloca i32, align 4
  store %class.gim_array* %this, %class.gim_array** %this.addr, align 4, !tbaa !2
  store i32 %start_range, i32* %start_range.addr, align 4, !tbaa !8
  %this1 = load %class.gim_array*, %class.gim_array** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %m_size = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_size, align 4, !tbaa !22
  %1 = load i32, i32* %start_range.addr, align 4, !tbaa !8
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %m_data = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 0
  %2 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %m_data, align 4, !tbaa !24
  %m_size2 = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 1
  %3 = load i32, i32* %m_size2, align 4, !tbaa !22
  %dec = add i32 %3, -1
  store i32 %dec, i32* %m_size2, align 4, !tbaa !22
  %arrayidx = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %2, i32 %dec
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_ZN9gim_arrayI11GIM_CONTACTE12growingCheckEv(%class.gim_array* %this) #1 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.gim_array*, align 4
  %requestsize = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.gim_array* %this, %class.gim_array** %this.addr, align 4, !tbaa !2
  %this1 = load %class.gim_array*, %class.gim_array** %this.addr, align 4
  %m_allocated_size = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_allocated_size, align 4, !tbaa !34
  %m_size = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 1
  %1 = load i32, i32* %m_size, align 4, !tbaa !22
  %cmp = icmp ule i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end7

if.then:                                          ; preds = %entry
  %2 = bitcast i32* %requestsize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %m_size2 = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 1
  %3 = load i32, i32* %m_size2, align 4, !tbaa !22
  store i32 %3, i32* %requestsize, align 4, !tbaa !8
  %m_allocated_size3 = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 2
  %4 = load i32, i32* %m_allocated_size3, align 4, !tbaa !34
  %m_size4 = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 1
  store i32 %4, i32* %m_size4, align 4, !tbaa !22
  %5 = load i32, i32* %requestsize, align 4, !tbaa !8
  %add = add i32 %5, 2
  %mul = mul i32 %add, 2
  %call = call zeroext i1 @_ZN9gim_arrayI11GIM_CONTACTE10resizeDataEj(%class.gim_array* %this1, i32 %mul)
  %conv = zext i1 %call to i32
  %cmp5 = icmp eq i32 %conv, 0
  br i1 %cmp5, label %if.then6, label %if.end

if.then6:                                         ; preds = %if.then
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then6
  %6 = bitcast i32* %requestsize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %return
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end7

if.end7:                                          ; preds = %cleanup.cont, %entry
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end7, %cleanup
  %7 = load i1, i1* %retval, align 1
  ret i1 %7

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_ZN9gim_arrayI11GIM_CONTACTE10resizeDataEj(%class.gim_array* %this, i32 %newsize) #1 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.gim_array*, align 4
  %newsize.addr = alloca i32, align 4
  store %class.gim_array* %this, %class.gim_array** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !8
  %this1 = load %class.gim_array*, %class.gim_array** %this.addr, align 4
  %0 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZN9gim_arrayI11GIM_CONTACTE11destroyDataEv(%class.gim_array* %this1)
  store i1 true, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %m_size = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 1
  %1 = load i32, i32* %m_size, align 4, !tbaa !22
  %cmp2 = icmp ugt i32 %1, 0
  br i1 %cmp2, label %if.then3, label %if.else

if.then3:                                         ; preds = %if.end
  %m_data = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 0
  %2 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %m_data, align 4, !tbaa !24
  %3 = bitcast %class.GIM_CONTACT* %2 to i8*
  %m_size4 = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 1
  %4 = load i32, i32* %m_size4, align 4, !tbaa !22
  %mul = mul i32 %4, 48
  %5 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %mul5 = mul i32 %5, 48
  %call = call i8* @_Z11gim_reallocPvmm(i8* %3, i32 %mul, i32 %mul5)
  %6 = bitcast i8* %call to %class.GIM_CONTACT*
  %m_data6 = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 0
  store %class.GIM_CONTACT* %6, %class.GIM_CONTACT** %m_data6, align 4, !tbaa !24
  br label %if.end10

if.else:                                          ; preds = %if.end
  %7 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %mul7 = mul i32 %7, 48
  %call8 = call i8* @_Z9gim_allocm(i32 %mul7)
  %8 = bitcast i8* %call8 to %class.GIM_CONTACT*
  %m_data9 = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 0
  store %class.GIM_CONTACT* %8, %class.GIM_CONTACT** %m_data9, align 4, !tbaa !24
  br label %if.end10

if.end10:                                         ; preds = %if.else, %if.then3
  %9 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %m_allocated_size = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 2
  store i32 %9, i32* %m_allocated_size, align 4, !tbaa !34
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end10, %if.then
  %10 = load i1, i1* %retval, align 1
  ret i1 %10
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN9gim_arrayI11GIM_CONTACTE11destroyDataEv(%class.gim_array* %this) #1 comdat {
entry:
  %this.addr = alloca %class.gim_array*, align 4
  store %class.gim_array* %this, %class.gim_array** %this.addr, align 4, !tbaa !2
  %this1 = load %class.gim_array*, %class.gim_array** %this.addr, align 4
  %m_allocated_size = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 2
  store i32 0, i32* %m_allocated_size, align 4, !tbaa !34
  %m_data = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 0
  %0 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %m_data, align 4, !tbaa !24
  %cmp = icmp eq %class.GIM_CONTACT* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %m_data2 = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 0
  %1 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %m_data2, align 4, !tbaa !24
  %2 = bitcast %class.GIM_CONTACT* %1 to i8*
  call void @_Z8gim_freePv(i8* %2)
  %m_data3 = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 0
  store %class.GIM_CONTACT* null, %class.GIM_CONTACT** %m_data3, align 4, !tbaa !24
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

declare i8* @_Z11gim_reallocPvmm(i8*, i32, i32) #6

declare i8* @_Z9gim_allocm(i32) #6

declare void @_Z8gim_freePv(i8*) #6

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_ZN9gim_arrayI15GIM_RSORT_TOKENE7reserveEj(%class.gim_array.0* %this, i32 %size) #1 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.gim_array.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.gim_array.0* %this, %class.gim_array.0** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !8
  %this1 = load %class.gim_array.0*, %class.gim_array.0** %this.addr, align 4
  %m_allocated_size = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_allocated_size, align 4, !tbaa !28
  %1 = load i32, i32* %size.addr, align 4, !tbaa !8
  %cmp = icmp uge i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %size.addr, align 4, !tbaa !8
  %call = call zeroext i1 @_ZN9gim_arrayI15GIM_RSORT_TOKENE10resizeDataEj(%class.gim_array.0* %this1, i32 %2)
  store i1 %call, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i1, i1* %retval, align 1
  ret i1 %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_ZN9gim_arrayI15GIM_RSORT_TOKENE10resizeDataEj(%class.gim_array.0* %this, i32 %newsize) #1 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.gim_array.0*, align 4
  %newsize.addr = alloca i32, align 4
  store %class.gim_array.0* %this, %class.gim_array.0** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !8
  %this1 = load %class.gim_array.0*, %class.gim_array.0** %this.addr, align 4
  %0 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZN9gim_arrayI15GIM_RSORT_TOKENE11destroyDataEv(%class.gim_array.0* %this1)
  store i1 true, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %m_size = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 1
  %1 = load i32, i32* %m_size, align 4, !tbaa !27
  %cmp2 = icmp ugt i32 %1, 0
  br i1 %cmp2, label %if.then3, label %if.else

if.then3:                                         ; preds = %if.end
  %m_data = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 0
  %2 = load %struct.GIM_RSORT_TOKEN*, %struct.GIM_RSORT_TOKEN** %m_data, align 4, !tbaa !25
  %3 = bitcast %struct.GIM_RSORT_TOKEN* %2 to i8*
  %m_size4 = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 1
  %4 = load i32, i32* %m_size4, align 4, !tbaa !27
  %mul = mul i32 %4, 8
  %5 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %mul5 = mul i32 %5, 8
  %call = call i8* @_Z11gim_reallocPvmm(i8* %3, i32 %mul, i32 %mul5)
  %6 = bitcast i8* %call to %struct.GIM_RSORT_TOKEN*
  %m_data6 = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 0
  store %struct.GIM_RSORT_TOKEN* %6, %struct.GIM_RSORT_TOKEN** %m_data6, align 4, !tbaa !25
  br label %if.end10

if.else:                                          ; preds = %if.end
  %7 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %mul7 = mul i32 %7, 8
  %call8 = call i8* @_Z9gim_allocm(i32 %mul7)
  %8 = bitcast i8* %call8 to %struct.GIM_RSORT_TOKEN*
  %m_data9 = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 0
  store %struct.GIM_RSORT_TOKEN* %8, %struct.GIM_RSORT_TOKEN** %m_data9, align 4, !tbaa !25
  br label %if.end10

if.end10:                                         ; preds = %if.else, %if.then3
  %9 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %m_allocated_size = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 2
  store i32 %9, i32* %m_allocated_size, align 4, !tbaa !28
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end10, %if.then
  %10 = load i1, i1* %retval, align 1
  ret i1 %10
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN9gim_arrayI15GIM_RSORT_TOKENE11destroyDataEv(%class.gim_array.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.gim_array.0*, align 4
  store %class.gim_array.0* %this, %class.gim_array.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.gim_array.0*, %class.gim_array.0** %this.addr, align 4
  %m_allocated_size = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_allocated_size, align 4, !tbaa !28
  %m_data = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 0
  %0 = load %struct.GIM_RSORT_TOKEN*, %struct.GIM_RSORT_TOKEN** %m_data, align 4, !tbaa !25
  %cmp = icmp eq %struct.GIM_RSORT_TOKEN* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %m_data2 = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 0
  %1 = load %struct.GIM_RSORT_TOKEN*, %struct.GIM_RSORT_TOKEN** %m_data2, align 4, !tbaa !25
  %2 = bitcast %struct.GIM_RSORT_TOKEN* %1 to i8*
  call void @_Z8gim_freePv(i8* %2)
  %m_data3 = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 0
  store %struct.GIM_RSORT_TOKEN* null, %struct.GIM_RSORT_TOKEN** %m_data3, align 4, !tbaa !25
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN9gim_arrayI15GIM_RSORT_TOKENE12clear_memoryEv(%class.gim_array.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.gim_array.0*, align 4
  store %class.gim_array.0* %this, %class.gim_array.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.gim_array.0*, %class.gim_array.0** %this.addr, align 4
  call void @_ZN9gim_arrayI15GIM_RSORT_TOKENE5clearEv(%class.gim_array.0* %this1)
  call void @_ZN9gim_arrayI15GIM_RSORT_TOKENE11destroyDataEv(%class.gim_array.0* %this1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN9gim_arrayI15GIM_RSORT_TOKENE5clearEv(%class.gim_array.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.gim_array.0*, align 4
  store %class.gim_array.0* %this, %class.gim_array.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.gim_array.0*, %class.gim_array.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_size, align 4, !tbaa !27
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  call void @_ZN9gim_arrayI15GIM_RSORT_TOKENE11clear_rangeEj(%class.gim_array.0* %this1, i32 0)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9gim_arrayI15GIM_RSORT_TOKENE11clear_rangeEj(%class.gim_array.0* %this, i32 %start_range) #2 comdat {
entry:
  %this.addr = alloca %class.gim_array.0*, align 4
  %start_range.addr = alloca i32, align 4
  store %class.gim_array.0* %this, %class.gim_array.0** %this.addr, align 4, !tbaa !2
  store i32 %start_range, i32* %start_range.addr, align 4, !tbaa !8
  %this1 = load %class.gim_array.0*, %class.gim_array.0** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %m_size = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_size, align 4, !tbaa !27
  %1 = load i32, i32* %start_range.addr, align 4, !tbaa !8
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %m_data = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 0
  %2 = load %struct.GIM_RSORT_TOKEN*, %struct.GIM_RSORT_TOKEN** %m_data, align 4, !tbaa !25
  %m_size2 = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 1
  %3 = load i32, i32* %m_size2, align 4, !tbaa !27
  %dec = add i32 %3, -1
  store i32 %dec, i32* %m_size2, align 4, !tbaa !27
  %arrayidx = getelementptr inbounds %struct.GIM_RSORT_TOKEN, %struct.GIM_RSORT_TOKEN* %2, i32 %dec
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

define linkonce_odr hidden void @_Z13gim_down_heapI15GIM_RSORT_TOKEN26GIM_RSORT_TOKEN_COMPARATOREvPT_jjT0_(%struct.GIM_RSORT_TOKEN* %pArr, i32 %k, i32 %n) #0 comdat {
entry:
  %CompareFunc = alloca %class.GIM_RSORT_TOKEN_COMPARATOR, align 1
  %pArr.addr = alloca %struct.GIM_RSORT_TOKEN*, align 4
  %k.addr = alloca i32, align 4
  %n.addr = alloca i32, align 4
  %temp = alloca %struct.GIM_RSORT_TOKEN, align 4
  %child = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.GIM_RSORT_TOKEN* %pArr, %struct.GIM_RSORT_TOKEN** %pArr.addr, align 4, !tbaa !2
  store i32 %k, i32* %k.addr, align 4, !tbaa !8
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %0 = bitcast %struct.GIM_RSORT_TOKEN* %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #7
  %1 = load %struct.GIM_RSORT_TOKEN*, %struct.GIM_RSORT_TOKEN** %pArr.addr, align 4, !tbaa !2
  %2 = load i32, i32* %k.addr, align 4, !tbaa !8
  %sub = sub i32 %2, 1
  %arrayidx = getelementptr inbounds %struct.GIM_RSORT_TOKEN, %struct.GIM_RSORT_TOKEN* %1, i32 %sub
  %call = call %struct.GIM_RSORT_TOKEN* @_ZN15GIM_RSORT_TOKENC2ERKS_(%struct.GIM_RSORT_TOKEN* %temp, %struct.GIM_RSORT_TOKEN* nonnull align 4 dereferenceable(8) %arrayidx)
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont, %entry
  %3 = load i32, i32* %k.addr, align 4, !tbaa !8
  %4 = load i32, i32* %n.addr, align 4, !tbaa !8
  %div = udiv i32 %4, 2
  %cmp = icmp ule i32 %3, %div
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %5 = bitcast i32* %child to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load i32, i32* %k.addr, align 4, !tbaa !8
  %mul = mul i32 2, %6
  store i32 %mul, i32* %child, align 4, !tbaa !8
  %7 = load i32, i32* %child, align 4, !tbaa !8
  %8 = load i32, i32* %n.addr, align 4, !tbaa !8
  %cmp1 = icmp slt i32 %7, %8
  br i1 %cmp1, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %while.body
  %9 = load %struct.GIM_RSORT_TOKEN*, %struct.GIM_RSORT_TOKEN** %pArr.addr, align 4, !tbaa !2
  %10 = load i32, i32* %child, align 4, !tbaa !8
  %sub2 = sub nsw i32 %10, 1
  %arrayidx3 = getelementptr inbounds %struct.GIM_RSORT_TOKEN, %struct.GIM_RSORT_TOKEN* %9, i32 %sub2
  %11 = load %struct.GIM_RSORT_TOKEN*, %struct.GIM_RSORT_TOKEN** %pArr.addr, align 4, !tbaa !2
  %12 = load i32, i32* %child, align 4, !tbaa !8
  %arrayidx4 = getelementptr inbounds %struct.GIM_RSORT_TOKEN, %struct.GIM_RSORT_TOKEN* %11, i32 %12
  %call5 = call i32 @_ZN26GIM_RSORT_TOKEN_COMPARATORclERK15GIM_RSORT_TOKENS2_(%class.GIM_RSORT_TOKEN_COMPARATOR* %CompareFunc, %struct.GIM_RSORT_TOKEN* nonnull align 4 dereferenceable(8) %arrayidx3, %struct.GIM_RSORT_TOKEN* nonnull align 4 dereferenceable(8) %arrayidx4)
  %cmp6 = icmp slt i32 %call5, 0
  br i1 %cmp6, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %13 = load i32, i32* %child, align 4, !tbaa !8
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %child, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %while.body
  %14 = load %struct.GIM_RSORT_TOKEN*, %struct.GIM_RSORT_TOKEN** %pArr.addr, align 4, !tbaa !2
  %15 = load i32, i32* %child, align 4, !tbaa !8
  %sub7 = sub nsw i32 %15, 1
  %arrayidx8 = getelementptr inbounds %struct.GIM_RSORT_TOKEN, %struct.GIM_RSORT_TOKEN* %14, i32 %sub7
  %call9 = call i32 @_ZN26GIM_RSORT_TOKEN_COMPARATORclERK15GIM_RSORT_TOKENS2_(%class.GIM_RSORT_TOKEN_COMPARATOR* %CompareFunc, %struct.GIM_RSORT_TOKEN* nonnull align 4 dereferenceable(8) %temp, %struct.GIM_RSORT_TOKEN* nonnull align 4 dereferenceable(8) %arrayidx8)
  %cmp10 = icmp slt i32 %call9, 0
  br i1 %cmp10, label %if.then11, label %if.else

if.then11:                                        ; preds = %if.end
  %16 = load %struct.GIM_RSORT_TOKEN*, %struct.GIM_RSORT_TOKEN** %pArr.addr, align 4, !tbaa !2
  %17 = load i32, i32* %child, align 4, !tbaa !8
  %sub12 = sub nsw i32 %17, 1
  %arrayidx13 = getelementptr inbounds %struct.GIM_RSORT_TOKEN, %struct.GIM_RSORT_TOKEN* %16, i32 %sub12
  %18 = load %struct.GIM_RSORT_TOKEN*, %struct.GIM_RSORT_TOKEN** %pArr.addr, align 4, !tbaa !2
  %19 = load i32, i32* %k.addr, align 4, !tbaa !8
  %sub14 = sub i32 %19, 1
  %arrayidx15 = getelementptr inbounds %struct.GIM_RSORT_TOKEN, %struct.GIM_RSORT_TOKEN* %18, i32 %sub14
  %20 = bitcast %struct.GIM_RSORT_TOKEN* %arrayidx15 to i8*
  %21 = bitcast %struct.GIM_RSORT_TOKEN* %arrayidx13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %20, i8* align 4 %21, i32 8, i1 false), !tbaa.struct !29
  %22 = load i32, i32* %child, align 4, !tbaa !8
  store i32 %22, i32* %k.addr, align 4, !tbaa !8
  br label %if.end16

if.else:                                          ; preds = %if.end
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end16:                                         ; preds = %if.then11
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end16, %if.else
  %23 = bitcast i32* %child to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 3, label %while.end
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %while.cond

while.end:                                        ; preds = %cleanup, %while.cond
  %24 = load %struct.GIM_RSORT_TOKEN*, %struct.GIM_RSORT_TOKEN** %pArr.addr, align 4, !tbaa !2
  %25 = load i32, i32* %k.addr, align 4, !tbaa !8
  %sub17 = sub i32 %25, 1
  %arrayidx18 = getelementptr inbounds %struct.GIM_RSORT_TOKEN, %struct.GIM_RSORT_TOKEN* %24, i32 %sub17
  %26 = bitcast %struct.GIM_RSORT_TOKEN* %arrayidx18 to i8*
  %27 = bitcast %struct.GIM_RSORT_TOKEN* %temp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %26, i8* align 4 %27, i32 8, i1 false), !tbaa.struct !29
  %28 = bitcast %struct.GIM_RSORT_TOKEN* %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %28) #7
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_Z17gim_swap_elementsI15GIM_RSORT_TOKENEvPT_mm(%struct.GIM_RSORT_TOKEN* %_array, i32 %_i, i32 %_j) #1 comdat {
entry:
  %_array.addr = alloca %struct.GIM_RSORT_TOKEN*, align 4
  %_i.addr = alloca i32, align 4
  %_j.addr = alloca i32, align 4
  %_e_tmp_ = alloca %struct.GIM_RSORT_TOKEN, align 4
  store %struct.GIM_RSORT_TOKEN* %_array, %struct.GIM_RSORT_TOKEN** %_array.addr, align 4, !tbaa !2
  store i32 %_i, i32* %_i.addr, align 4, !tbaa !30
  store i32 %_j, i32* %_j.addr, align 4, !tbaa !30
  %0 = bitcast %struct.GIM_RSORT_TOKEN* %_e_tmp_ to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #7
  %1 = load %struct.GIM_RSORT_TOKEN*, %struct.GIM_RSORT_TOKEN** %_array.addr, align 4, !tbaa !2
  %2 = load i32, i32* %_i.addr, align 4, !tbaa !30
  %arrayidx = getelementptr inbounds %struct.GIM_RSORT_TOKEN, %struct.GIM_RSORT_TOKEN* %1, i32 %2
  %call = call %struct.GIM_RSORT_TOKEN* @_ZN15GIM_RSORT_TOKENC2ERKS_(%struct.GIM_RSORT_TOKEN* %_e_tmp_, %struct.GIM_RSORT_TOKEN* nonnull align 4 dereferenceable(8) %arrayidx)
  %3 = load %struct.GIM_RSORT_TOKEN*, %struct.GIM_RSORT_TOKEN** %_array.addr, align 4, !tbaa !2
  %4 = load i32, i32* %_j.addr, align 4, !tbaa !30
  %arrayidx1 = getelementptr inbounds %struct.GIM_RSORT_TOKEN, %struct.GIM_RSORT_TOKEN* %3, i32 %4
  %5 = load %struct.GIM_RSORT_TOKEN*, %struct.GIM_RSORT_TOKEN** %_array.addr, align 4, !tbaa !2
  %6 = load i32, i32* %_i.addr, align 4, !tbaa !30
  %arrayidx2 = getelementptr inbounds %struct.GIM_RSORT_TOKEN, %struct.GIM_RSORT_TOKEN* %5, i32 %6
  %7 = bitcast %struct.GIM_RSORT_TOKEN* %arrayidx2 to i8*
  %8 = bitcast %struct.GIM_RSORT_TOKEN* %arrayidx1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 8, i1 false), !tbaa.struct !29
  %9 = load %struct.GIM_RSORT_TOKEN*, %struct.GIM_RSORT_TOKEN** %_array.addr, align 4, !tbaa !2
  %10 = load i32, i32* %_j.addr, align 4, !tbaa !30
  %arrayidx3 = getelementptr inbounds %struct.GIM_RSORT_TOKEN, %struct.GIM_RSORT_TOKEN* %9, i32 %10
  %11 = bitcast %struct.GIM_RSORT_TOKEN* %arrayidx3 to i8*
  %12 = bitcast %struct.GIM_RSORT_TOKEN* %_e_tmp_ to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 8, i1 false), !tbaa.struct !29
  %13 = bitcast %struct.GIM_RSORT_TOKEN* %_e_tmp_ to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %13) #7
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.GIM_RSORT_TOKEN* @_ZN15GIM_RSORT_TOKENC2ERKS_(%struct.GIM_RSORT_TOKEN* returned %this, %struct.GIM_RSORT_TOKEN* nonnull align 4 dereferenceable(8) %rtoken) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %struct.GIM_RSORT_TOKEN*, align 4
  %rtoken.addr = alloca %struct.GIM_RSORT_TOKEN*, align 4
  store %struct.GIM_RSORT_TOKEN* %this, %struct.GIM_RSORT_TOKEN** %this.addr, align 4, !tbaa !2
  store %struct.GIM_RSORT_TOKEN* %rtoken, %struct.GIM_RSORT_TOKEN** %rtoken.addr, align 4, !tbaa !2
  %this1 = load %struct.GIM_RSORT_TOKEN*, %struct.GIM_RSORT_TOKEN** %this.addr, align 4
  %0 = load %struct.GIM_RSORT_TOKEN*, %struct.GIM_RSORT_TOKEN** %rtoken.addr, align 4, !tbaa !2
  %m_key = getelementptr inbounds %struct.GIM_RSORT_TOKEN, %struct.GIM_RSORT_TOKEN* %0, i32 0, i32 0
  %1 = load i32, i32* %m_key, align 4, !tbaa !10
  %m_key2 = getelementptr inbounds %struct.GIM_RSORT_TOKEN, %struct.GIM_RSORT_TOKEN* %this1, i32 0, i32 0
  store i32 %1, i32* %m_key2, align 4, !tbaa !10
  %2 = load %struct.GIM_RSORT_TOKEN*, %struct.GIM_RSORT_TOKEN** %rtoken.addr, align 4, !tbaa !2
  %m_value = getelementptr inbounds %struct.GIM_RSORT_TOKEN, %struct.GIM_RSORT_TOKEN* %2, i32 0, i32 1
  %3 = load i32, i32* %m_value, align 4, !tbaa !12
  %m_value3 = getelementptr inbounds %struct.GIM_RSORT_TOKEN, %struct.GIM_RSORT_TOKEN* %this1, i32 0, i32 1
  store i32 %3, i32* %m_value3, align 4, !tbaa !12
  ret %struct.GIM_RSORT_TOKEN* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN26GIM_RSORT_TOKEN_COMPARATORclERK15GIM_RSORT_TOKENS2_(%class.GIM_RSORT_TOKEN_COMPARATOR* %this, %struct.GIM_RSORT_TOKEN* nonnull align 4 dereferenceable(8) %a, %struct.GIM_RSORT_TOKEN* nonnull align 4 dereferenceable(8) %b) #2 comdat {
entry:
  %this.addr = alloca %class.GIM_RSORT_TOKEN_COMPARATOR*, align 4
  %a.addr = alloca %struct.GIM_RSORT_TOKEN*, align 4
  %b.addr = alloca %struct.GIM_RSORT_TOKEN*, align 4
  store %class.GIM_RSORT_TOKEN_COMPARATOR* %this, %class.GIM_RSORT_TOKEN_COMPARATOR** %this.addr, align 4, !tbaa !2
  store %struct.GIM_RSORT_TOKEN* %a, %struct.GIM_RSORT_TOKEN** %a.addr, align 4, !tbaa !2
  store %struct.GIM_RSORT_TOKEN* %b, %struct.GIM_RSORT_TOKEN** %b.addr, align 4, !tbaa !2
  %this1 = load %class.GIM_RSORT_TOKEN_COMPARATOR*, %class.GIM_RSORT_TOKEN_COMPARATOR** %this.addr, align 4
  %0 = load %struct.GIM_RSORT_TOKEN*, %struct.GIM_RSORT_TOKEN** %a.addr, align 4, !tbaa !2
  %m_key = getelementptr inbounds %struct.GIM_RSORT_TOKEN, %struct.GIM_RSORT_TOKEN* %0, i32 0, i32 0
  %1 = load i32, i32* %m_key, align 4, !tbaa !10
  %2 = load %struct.GIM_RSORT_TOKEN*, %struct.GIM_RSORT_TOKEN** %b.addr, align 4, !tbaa !2
  %m_key2 = getelementptr inbounds %struct.GIM_RSORT_TOKEN, %struct.GIM_RSORT_TOKEN* %2, i32 0, i32 0
  %3 = load i32, i32* %m_key2, align 4, !tbaa !10
  %sub = sub i32 %1, %3
  ret i32 %sub
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind readnone speculatable willreturn }
attributes #6 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"bool", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"int", !4, i64 0}
!10 = !{!11, !9, i64 0}
!11 = !{!"_ZTS15GIM_RSORT_TOKEN", !9, i64 0, !9, i64 4}
!12 = !{!11, !9, i64 4}
!13 = !{!14, !16, i64 32}
!14 = !{!"_ZTS11GIM_CONTACT", !15, i64 0, !15, i64 16, !16, i64 32, !16, i64 36, !9, i64 40, !9, i64 44}
!15 = !{!"_ZTS9btVector3", !4, i64 0}
!16 = !{!"float", !4, i64 0}
!17 = !{i64 0, i64 16, !18, i64 16, i64 16, !18, i64 32, i64 4, !19, i64 36, i64 4, !19, i64 40, i64 4, !8, i64 44, i64 4, !8}
!18 = !{!4, !4, i64 0}
!19 = !{!16, !16, i64 0}
!20 = !{i8 0, i8 2}
!21 = !{i64 0, i64 16, !18}
!22 = !{!23, !9, i64 4}
!23 = !{!"_ZTS9gim_arrayI11GIM_CONTACTE", !3, i64 0, !9, i64 4, !9, i64 8}
!24 = !{!23, !3, i64 0}
!25 = !{!26, !3, i64 0}
!26 = !{!"_ZTS9gim_arrayI15GIM_RSORT_TOKENE", !3, i64 0, !9, i64 4, !9, i64 8}
!27 = !{!26, !9, i64 4}
!28 = !{!26, !9, i64 8}
!29 = !{i64 0, i64 4, !8, i64 4, i64 4, !8}
!30 = !{!31, !31, i64 0}
!31 = !{!"long", !4, i64 0}
!32 = !{!14, !9, i64 40}
!33 = !{!14, !9, i64 44}
!34 = !{!23, !9, i64 8}
