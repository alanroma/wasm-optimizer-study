; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btPolyhedralConvexShape.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btPolyhedralConvexShape.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btVector3 = type { [4 x float] }
%class.btPolyhedralConvexShape = type { %class.btConvexInternalShape, %class.btConvexPolyhedron* }
%class.btConvexInternalShape = type { %class.btConvexShape, %class.btVector3, %class.btVector3, float, float }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%class.btConvexPolyhedron = type { i32 (...)**, %class.btAlignedObjectArray, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.btFace*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btFace = type { %class.btAlignedObjectArray.3, [4 x float] }
%class.btAlignedObjectArray.3 = type <{ %class.btAlignedAllocator.4, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.4 = type { i8 }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btConvexHullComputer = type { %class.btAlignedObjectArray, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.3 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, %"class.btConvexHullComputer::Edge"*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%"class.btConvexHullComputer::Edge" = type { i32, i32, i32 }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, %struct.GrahamVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%struct.GrahamVector3 = type { %class.btVector3, float, i32 }
%struct.btAngleCompareFunc = type { %class.btVector3 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btPolyhedralConvexAabbCachingShape = type <{ %class.btPolyhedralConvexShape, %class.btVector3, %class.btVector3, i8, [3 x i8] }>
%class.btSerializer = type opaque
%struct.btConvexInternalShapeData = type { %struct.btCollisionShapeData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, i32 }
%struct.btCollisionShapeData = type { i8*, i32, [4 x i8] }
%struct.btVector3FloatData = type { [4 x float] }

$_ZN18btConvexPolyhedronnwEmPv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E6expandERKS0_ = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN20btConvexHullComputerC2Ev = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EixEi = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_ = comdat any

$_ZN20btConvexHullComputer7computeEPKfiiff = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3ED2Ev = comdat any

$_ZNK20btAlignedObjectArrayIiE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_ = comdat any

$_ZN20btAlignedObjectArrayI6btFaceEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI6btFaceE6resizeEiRKS0_ = comdat any

$_ZN6btFaceC2Ev = comdat any

$_ZN6btFaceD2Ev = comdat any

$_ZN20btAlignedObjectArrayIiEixEi = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEEixEi = comdat any

$_ZNK20btConvexHullComputer4Edge15getSourceVertexEv = comdat any

$_ZN20btAlignedObjectArrayI6btFaceEixEi = comdat any

$_ZN20btAlignedObjectArrayIiE9push_backERKi = comdat any

$_ZNK20btConvexHullComputer4Edge15getTargetVertexEv = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZNK20btConvexHullComputer4Edge17getNextEdgeOfFaceEv = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZN9btVector37setZeroEv = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN20btAlignedObjectArrayIiEC2Ev = comdat any

$_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIiE8pop_backEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN20btAlignedObjectArrayIiE6removeERKi = comdat any

$_ZN20btAlignedObjectArrayI13GrahamVector3EC2Ev = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI13GrahamVector3EixEi = comdat any

$_ZN20btAlignedObjectArrayI13GrahamVector3E9push_backERKS0_ = comdat any

$_ZN13GrahamVector3C2ERK9btVector3i = comdat any

$_Z22GrahamScanConvexHull2DR20btAlignedObjectArrayI13GrahamVector3ES2_RK9btVector3 = comdat any

$_ZN20btAlignedObjectArrayI6btFaceE9push_backERKS0_ = comdat any

$_ZN20btAlignedObjectArrayI13GrahamVector3ED2Ev = comdat any

$_ZN6btFaceC2ERKS_ = comdat any

$_ZN20btAlignedObjectArrayIiED2Ev = comdat any

$_ZN20btAlignedObjectArrayI6btFaceED2Ev = comdat any

$_ZN20btConvexHullComputerD2Ev = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_Z6btSqrtf = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZNK9btVector36maxDotEPKS_lRf = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN11btTransform11setIdentityEv = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZNK34btPolyhedralConvexAabbCachingShape17getNonvirtualAabbERK11btTransformR9btVector3S4_f = comdat any

$_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_ = comdat any

$_ZNK21btConvexInternalShape15getLocalScalingEv = comdat any

$_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZN21btConvexInternalShape9setMarginEf = comdat any

$_ZNK21btConvexInternalShape9getMarginEv = comdat any

$_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv = comdat any

$_ZNK21btConvexInternalShape9serializeEPvP12btSerializer = comdat any

$_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv = comdat any

$_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3 = comdat any

$_ZN34btPolyhedralConvexAabbCachingShapeD0Ev = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEEC2Ev = comdat any

$_ZN18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4initEv = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZNK20btConvexHullComputer4Edge19getNextEdgeOfVertexEv = comdat any

$_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_ = comdat any

$_ZN20btAlignedObjectArrayI13GrahamVector3E4swapEii = comdat any

$_Z7btCrossRK9btVector3S1_ = comdat any

$_ZN18btAngleCompareFuncC2ERK9btVector3 = comdat any

$_ZN20btAlignedObjectArrayI13GrahamVector3E17quickSortInternalI18btAngleCompareFuncEEvRKT_ii = comdat any

$_ZN20btAlignedObjectArrayI13GrahamVector3E8pop_backEv = comdat any

$_Z6btFabsf = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZNK18btAngleCompareFuncclERK13GrahamVector3S2_ = comdat any

$_ZN20btAlignedObjectArrayIiEC2ERKS0_ = comdat any

$_ZN18btAlignedAllocatorIiLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiE4initEv = comdat any

$_ZN20btAlignedObjectArrayIiE6resizeEiRKi = comdat any

$_ZNK20btAlignedObjectArrayIiE4copyEiiPi = comdat any

$_ZN20btAlignedObjectArrayIiE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIiE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIiE8allocateEi = comdat any

$_ZN20btAlignedObjectArrayIiE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIiE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi = comdat any

$_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEED2Ev = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EE10deallocateEPS1_ = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN11btMatrix3x311setIdentityEv = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_Z15btTransformAabbRK9btVector3S1_fRK11btTransformRS_S5_ = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x38absoluteEv = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZN11btMatrix3x3ixEi = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector314serializeFloatER18btVector3FloatData = comdat any

$_ZNK20btAlignedObjectArrayI13GrahamVector3E8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI13GrahamVector3E7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI13GrahamVector3E9allocSizeEi = comdat any

$_ZN9btVector3nwEmPv = comdat any

$_ZN20btAlignedObjectArrayI13GrahamVector3E8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI13GrahamVector3E4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI13GrahamVector3E7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI13GrahamVector3E10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI13GrahamVector3Lj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI13GrahamVector3Lj16EE10deallocateEPS0_ = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E4initEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E5clearEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_ = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAlignedObjectArrayIiE5clearEv = comdat any

$_ZN18btAlignedAllocatorI6btFaceLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI6btFaceE4initEv = comdat any

$_ZN20btAlignedObjectArrayI6btFaceE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI6btFaceE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI6btFaceE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI6btFaceLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI6btFaceE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayI6btFaceE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI6btFaceE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI6btFaceE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI6btFaceLj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAlignedObjectArrayIiE9allocSizeEi = comdat any

$_ZNK20btAlignedObjectArrayIiE16findLinearSearchERKi = comdat any

$_ZN20btAlignedObjectArrayIiE4swapEii = comdat any

$_ZN18btAlignedAllocatorI13GrahamVector3Lj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI13GrahamVector3E4initEv = comdat any

$_ZN20btAlignedObjectArrayI13GrahamVector3E5clearEv = comdat any

$_ZN20btAlignedObjectArrayI6btFaceE9allocSizeEi = comdat any

@_ZTV23btPolyhedralConvexShape = hidden unnamed_addr constant { [33 x i8*] } { [33 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI23btPolyhedralConvexShape to i8*), i8* bitcast (%class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD1Ev to i8*), i8* bitcast (void (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD0Ev to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btVector3*)* @_ZN21btConvexInternalShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btPolyhedralConvexShape*, float, %class.btVector3*)* @_ZNK23btPolyhedralConvexShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, float)* @_ZN21btConvexInternalShape9setMarginEf to i8*), i8* bitcast (float (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btConvexInternalShape*, i8*, %class.btSerializer*)* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btVector3*, %class.btConvexInternalShape*, %class.btVector3*)* @_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btPolyhedralConvexShape*, %class.btVector3*)* @_ZNK23btPolyhedralConvexShape37localGetSupportingVertexWithoutMarginERK9btVector3 to i8*), i8* bitcast (void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, float*, float*)* @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_ to i8*), i8* bitcast (void (%class.btPolyhedralConvexShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK23btPolyhedralConvexShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, i32, %class.btVector3*)* @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3 to i8*), i8* bitcast (i1 (%class.btPolyhedralConvexShape*, i32)* @_ZN23btPolyhedralConvexShape28initializePolyhedralFeaturesEi to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, align 4
@_ZTV34btPolyhedralConvexAabbCachingShape = hidden unnamed_addr constant { [33 x i8*] } { [33 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI34btPolyhedralConvexAabbCachingShape to i8*), i8* bitcast (%class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD2Ev to i8*), i8* bitcast (void (%class.btPolyhedralConvexAabbCachingShape*)* @_ZN34btPolyhedralConvexAabbCachingShapeD0Ev to i8*), i8* bitcast (void (%class.btPolyhedralConvexAabbCachingShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK34btPolyhedralConvexAabbCachingShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btPolyhedralConvexAabbCachingShape*, %class.btVector3*)* @_ZN34btPolyhedralConvexAabbCachingShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btPolyhedralConvexShape*, float, %class.btVector3*)* @_ZNK23btPolyhedralConvexShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, float)* @_ZN21btConvexInternalShape9setMarginEf to i8*), i8* bitcast (float (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btConvexInternalShape*, i8*, %class.btSerializer*)* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btVector3*, %class.btConvexInternalShape*, %class.btVector3*)* @_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btPolyhedralConvexShape*, %class.btVector3*)* @_ZNK23btPolyhedralConvexShape37localGetSupportingVertexWithoutMarginERK9btVector3 to i8*), i8* bitcast (void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, float*, float*)* @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_ to i8*), i8* bitcast (void (%class.btPolyhedralConvexShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK23btPolyhedralConvexShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, i32, %class.btVector3*)* @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3 to i8*), i8* bitcast (i1 (%class.btPolyhedralConvexShape*, i32)* @_ZN23btPolyhedralConvexShape28initializePolyhedralFeaturesEi to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, align 4
@_ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions = internal global [6 x %class.btVector3] zeroinitializer, align 16
@_ZGVZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions = internal global i32 0, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS23btPolyhedralConvexShape = hidden constant [26 x i8] c"23btPolyhedralConvexShape\00", align 1
@_ZTI21btConvexInternalShape = external constant i8*
@_ZTI23btPolyhedralConvexShape = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([26 x i8], [26 x i8]* @_ZTS23btPolyhedralConvexShape, i32 0, i32 0), i8* bitcast (i8** @_ZTI21btConvexInternalShape to i8*) }, align 4
@_ZTS34btPolyhedralConvexAabbCachingShape = hidden constant [37 x i8] c"34btPolyhedralConvexAabbCachingShape\00", align 1
@_ZTI34btPolyhedralConvexAabbCachingShape = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([37 x i8], [37 x i8]* @_ZTS34btPolyhedralConvexAabbCachingShape, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI23btPolyhedralConvexShape to i8*) }, align 4
@.str = private unnamed_addr constant [26 x i8] c"btConvexInternalShapeData\00", align 1

@_ZN23btPolyhedralConvexShapeD1Ev = hidden unnamed_addr alias %class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*), %class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD2Ev

define hidden %class.btPolyhedralConvexShape* @_ZN23btPolyhedralConvexShapeC2Ev(%class.btPolyhedralConvexShape* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btPolyhedralConvexShape*, align 4
  store %class.btPolyhedralConvexShape* %this, %class.btPolyhedralConvexShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPolyhedralConvexShape*, %class.btPolyhedralConvexShape** %this.addr, align 4
  %0 = bitcast %class.btPolyhedralConvexShape* %this1 to %class.btConvexInternalShape*
  %call = call %class.btConvexInternalShape* @_ZN21btConvexInternalShapeC2Ev(%class.btConvexInternalShape* %0)
  %1 = bitcast %class.btPolyhedralConvexShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [33 x i8*] }, { [33 x i8*] }* @_ZTV23btPolyhedralConvexShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_polyhedron = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  store %class.btConvexPolyhedron* null, %class.btConvexPolyhedron** %m_polyhedron, align 4, !tbaa !8
  ret %class.btPolyhedralConvexShape* %this1
}

declare %class.btConvexInternalShape* @_ZN21btConvexInternalShapeC2Ev(%class.btConvexInternalShape* returned) unnamed_addr #1

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

; Function Attrs: nounwind
declare %class.btConvexShape* @_ZN13btConvexShapeD2Ev(%class.btConvexShape* returned) unnamed_addr #2

; Function Attrs: nounwind
define hidden void @_ZN23btPolyhedralConvexShapeD0Ev(%class.btPolyhedralConvexShape* %this) unnamed_addr #3 {
entry:
  %this.addr = alloca %class.btPolyhedralConvexShape*, align 4
  store %class.btPolyhedralConvexShape* %this, %class.btPolyhedralConvexShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPolyhedralConvexShape*, %class.btPolyhedralConvexShape** %this.addr, align 4
  call void @llvm.trap() #11
  unreachable
}

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #4

define hidden zeroext i1 @_ZN23btPolyhedralConvexShape28initializePolyhedralFeaturesEi(%class.btPolyhedralConvexShape* %this, i32 %shiftVerticesByMargin) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btPolyhedralConvexShape*, align 4
  %shiftVerticesByMargin.addr = alloca i32, align 4
  %mem = alloca i8*, align 4
  %orgVertices = alloca %class.btAlignedObjectArray, align 4
  %i = alloca i32, align 4
  %newVertex = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %conv = alloca %class.btConvexHullComputer, align 4
  %planeEquations = alloca %class.btAlignedObjectArray, align 4
  %shiftedPlaneEquations = alloca %class.btAlignedObjectArray, align 4
  %p = alloca i32, align 4
  %plane = alloca %class.btVector3, align 4
  %tmpVertices = alloca %class.btAlignedObjectArray, align 4
  %faceNormals = alloca %class.btAlignedObjectArray, align 4
  %numFaces = alloca i32, align 4
  %ref.tmp49 = alloca %class.btVector3, align 4
  %convexUtil = alloca %class.btConvexHullComputer*, align 4
  %tmpFaces = alloca %class.btAlignedObjectArray.0, align 4
  %ref.tmp52 = alloca %struct.btFace, align 4
  %numVertices = alloca i32, align 4
  %ref.tmp57 = alloca %class.btVector3, align 4
  %p59 = alloca i32, align 4
  %i72 = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %face = alloca i32, align 4
  %firstEdge = alloca %"class.btConvexHullComputer::Edge"*, align 4
  %edge = alloca %"class.btConvexHullComputer::Edge"*, align 4
  %edges80 = alloca [3 x %class.btVector3], align 16
  %numEdges = alloca i32, align 4
  %src = alloca i32, align 4
  %targ = alloca i32, align 4
  %wa = alloca %class.btVector3, align 4
  %wb = alloca %class.btVector3, align 4
  %newEdge = alloca %class.btVector3, align 4
  %planeEq = alloca float, align 4
  %ref.tmp99 = alloca %class.btVector3, align 4
  %v = alloca i32, align 4
  %eq = alloca float, align 4
  %faceWeldThreshold = alloca float, align 4
  %todoFaces = alloca %class.btAlignedObjectArray.3, align 4
  %i153 = alloca i32, align 4
  %coplanarFaceGroup = alloca %class.btAlignedObjectArray.3, align 4
  %refFace = alloca i32, align 4
  %faceA = alloca %struct.btFace*, align 4
  %faceNormalA = alloca %class.btVector3, align 4
  %j = alloca i32, align 4
  %i182 = alloca i32, align 4
  %faceB = alloca %struct.btFace*, align 4
  %faceNormalB = alloca %class.btVector3, align 4
  %did_merge = alloca i8, align 1
  %orgpoints = alloca %class.btAlignedObjectArray.12, align 4
  %averageFaceNormal = alloca %class.btVector3, align 4
  %ref.tmp202 = alloca float, align 4
  %ref.tmp203 = alloca float, align 4
  %ref.tmp204 = alloca float, align 4
  %i206 = alloca i32, align 4
  %face212 = alloca %struct.btFace*, align 4
  %faceNormal = alloca %class.btVector3, align 4
  %f = alloca i32, align 4
  %orgIndex = alloca i32, align 4
  %pt = alloca %class.btVector3, align 4
  %found = alloca i8, align 1
  %i234 = alloca i32, align 4
  %ref.tmp249 = alloca %struct.GrahamVector3, align 4
  %combinedFace = alloca %struct.btFace, align 4
  %i261 = alloca i32, align 4
  %hull = alloca %class.btAlignedObjectArray.12, align 4
  %i278 = alloca i32, align 4
  %k = alloca i32, align 4
  %reject_merge = alloca i8, align 1
  %i309 = alloca i32, align 4
  %j320 = alloca i32, align 4
  %face326 = alloca %struct.btFace*, align 4
  %is_in_current_group = alloca i8, align 1
  %k328 = alloca i32, align 4
  %v345 = alloca i32, align 4
  %i389 = alloca i32, align 4
  %face395 = alloca %struct.btFace, align 4
  store %class.btPolyhedralConvexShape* %this, %class.btPolyhedralConvexShape** %this.addr, align 4, !tbaa !2
  store i32 %shiftVerticesByMargin, i32* %shiftVerticesByMargin.addr, align 4, !tbaa !10
  %this1 = load %class.btPolyhedralConvexShape*, %class.btPolyhedralConvexShape** %this.addr, align 4
  %m_polyhedron = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %m_polyhedron, align 4, !tbaa !8
  %tobool = icmp ne %class.btConvexPolyhedron* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_polyhedron2 = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  %1 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %m_polyhedron2, align 4, !tbaa !8
  %2 = bitcast %class.btConvexPolyhedron* %1 to %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)***
  %vtable = load %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)**, %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)*** %2, align 4, !tbaa !6
  %vfn = getelementptr inbounds %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)*, %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)** %vtable, i64 0
  %3 = load %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)*, %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)** %vfn, align 4
  %call = call %class.btConvexPolyhedron* %3(%class.btConvexPolyhedron* %1) #9
  %m_polyhedron3 = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  %4 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %m_polyhedron3, align 4, !tbaa !8
  %5 = bitcast %class.btConvexPolyhedron* %4 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %5)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %6 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %call4 = call i8* @_Z22btAlignedAllocInternalmi(i32 132, i32 16)
  store i8* %call4, i8** %mem, align 4, !tbaa !2
  %7 = load i8*, i8** %mem, align 4, !tbaa !2
  %call5 = call i8* @_ZN18btConvexPolyhedronnwEmPv(i32 132, i8* %7)
  %8 = bitcast i8* %call5 to %class.btConvexPolyhedron*
  %call6 = call %class.btConvexPolyhedron* @_ZN18btConvexPolyhedronC1Ev(%class.btConvexPolyhedron* %8)
  %m_polyhedron7 = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  store %class.btConvexPolyhedron* %8, %class.btConvexPolyhedron** %m_polyhedron7, align 4, !tbaa !8
  %9 = bitcast %class.btAlignedObjectArray* %orgVertices to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %9) #9
  %call8 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* %orgVertices)
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  store i32 0, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %11 = load i32, i32* %i, align 4, !tbaa !10
  %12 = bitcast %class.btPolyhedralConvexShape* %this1 to i32 (%class.btPolyhedralConvexShape*)***
  %vtable9 = load i32 (%class.btPolyhedralConvexShape*)**, i32 (%class.btPolyhedralConvexShape*)*** %12, align 4, !tbaa !6
  %vfn10 = getelementptr inbounds i32 (%class.btPolyhedralConvexShape*)*, i32 (%class.btPolyhedralConvexShape*)** %vtable9, i64 24
  %13 = load i32 (%class.btPolyhedralConvexShape*)*, i32 (%class.btPolyhedralConvexShape*)** %vfn10, align 4
  %call11 = call i32 %13(%class.btPolyhedralConvexShape* %this1)
  %cmp = icmp slt i32 %11, %call11
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %14 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %15 = bitcast %class.btVector3** %newVertex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #9
  %16 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #9
  %call12 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp)
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3E6expandERKS0_(%class.btAlignedObjectArray* %orgVertices, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %17 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %17) #9
  store %class.btVector3* %call13, %class.btVector3** %newVertex, align 4, !tbaa !2
  %18 = load i32, i32* %i, align 4, !tbaa !10
  %19 = load %class.btVector3*, %class.btVector3** %newVertex, align 4, !tbaa !2
  %20 = bitcast %class.btPolyhedralConvexShape* %this1 to void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)***
  %vtable14 = load void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)**, void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)*** %20, align 4, !tbaa !6
  %vfn15 = getelementptr inbounds void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)*, void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)** %vtable14, i64 27
  %21 = load void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)*, void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)** %vfn15, align 4
  call void %21(%class.btPolyhedralConvexShape* %this1, i32 %18, %class.btVector3* nonnull align 4 dereferenceable(16) %19)
  %22 = bitcast %class.btVector3** %newVertex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %23 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add nsw i32 %23, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %24 = bitcast %class.btConvexHullComputer* %conv to i8*
  call void @llvm.lifetime.start.p0i8(i64 60, i8* %24) #9
  %call16 = call %class.btConvexHullComputer* @_ZN20btConvexHullComputerC2Ev(%class.btConvexHullComputer* %conv)
  %25 = load i32, i32* %shiftVerticesByMargin.addr, align 4, !tbaa !10
  %tobool17 = icmp ne i32 %25, 0
  br i1 %tobool17, label %if.then18, label %if.else

if.then18:                                        ; preds = %for.end
  %26 = bitcast %class.btAlignedObjectArray* %planeEquations to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %26) #9
  %call19 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* %planeEquations)
  call void @_ZN14btGeometryUtil29getPlaneEquationsFromVerticesER20btAlignedObjectArrayI9btVector3ES3_(%class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %orgVertices, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %planeEquations)
  %27 = bitcast %class.btAlignedObjectArray* %shiftedPlaneEquations to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %27) #9
  %call20 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* %shiftedPlaneEquations)
  %28 = bitcast i32* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #9
  store i32 0, i32* %p, align 4, !tbaa !10
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc31, %if.then18
  %29 = load i32, i32* %p, align 4, !tbaa !10
  %call22 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %planeEquations)
  %cmp23 = icmp slt i32 %29, %call22
  br i1 %cmp23, label %for.body25, label %for.cond.cleanup24

for.cond.cleanup24:                               ; preds = %for.cond21
  %30 = bitcast i32* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #9
  br label %for.end33

for.body25:                                       ; preds = %for.cond21
  %31 = bitcast %class.btVector3* %plane to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %31) #9
  %32 = load i32, i32* %p, align 4, !tbaa !10
  %call26 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %planeEquations, i32 %32)
  %33 = bitcast %class.btVector3* %plane to i8*
  %34 = bitcast %class.btVector3* %call26 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %33, i8* align 4 %34, i32 16, i1 false), !tbaa.struct !12
  %35 = bitcast %class.btPolyhedralConvexShape* %this1 to %class.btConvexInternalShape*
  %36 = bitcast %class.btConvexInternalShape* %35 to float (%class.btConvexInternalShape*)***
  %vtable27 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %36, align 4, !tbaa !6
  %vfn28 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable27, i64 12
  %37 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn28, align 4
  %call29 = call float %37(%class.btConvexInternalShape* %35)
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %plane)
  %arrayidx = getelementptr inbounds float, float* %call30, i32 3
  %38 = load float, float* %arrayidx, align 4, !tbaa !14
  %sub = fsub float %38, %call29
  store float %sub, float* %arrayidx, align 4, !tbaa !14
  call void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray* %shiftedPlaneEquations, %class.btVector3* nonnull align 4 dereferenceable(16) %plane)
  %39 = bitcast %class.btVector3* %plane to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %39) #9
  br label %for.inc31

for.inc31:                                        ; preds = %for.body25
  %40 = load i32, i32* %p, align 4, !tbaa !10
  %inc32 = add nsw i32 %40, 1
  store i32 %inc32, i32* %p, align 4, !tbaa !10
  br label %for.cond21

for.end33:                                        ; preds = %for.cond.cleanup24
  %41 = bitcast %class.btAlignedObjectArray* %tmpVertices to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %41) #9
  %call34 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* %tmpVertices)
  call void @_ZN14btGeometryUtil29getVerticesFromPlaneEquationsERK20btAlignedObjectArrayI9btVector3ERS2_(%class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %shiftedPlaneEquations, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %tmpVertices)
  %call35 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %tmpVertices, i32 0)
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %call35)
  %call37 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %tmpVertices)
  %call38 = call float @_ZN20btConvexHullComputer7computeEPKfiiff(%class.btConvexHullComputer* %conv, float* %call36, i32 16, i32 %call37, float 0.000000e+00, float 0.000000e+00)
  %call39 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* %tmpVertices) #9
  %42 = bitcast %class.btAlignedObjectArray* %tmpVertices to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %42) #9
  %call40 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* %shiftedPlaneEquations) #9
  %43 = bitcast %class.btAlignedObjectArray* %shiftedPlaneEquations to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %43) #9
  %call41 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* %planeEquations) #9
  %44 = bitcast %class.btAlignedObjectArray* %planeEquations to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %44) #9
  br label %if.end46

if.else:                                          ; preds = %for.end
  %call42 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %orgVertices, i32 0)
  %call43 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %call42)
  %call44 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %orgVertices)
  %call45 = call float @_ZN20btConvexHullComputer7computeEPKfiiff(%class.btConvexHullComputer* %conv, float* %call43, i32 16, i32 %call44, float 0.000000e+00, float 0.000000e+00)
  br label %if.end46

if.end46:                                         ; preds = %if.else, %for.end33
  %45 = bitcast %class.btAlignedObjectArray* %faceNormals to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %45) #9
  %call47 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* %faceNormals)
  %46 = bitcast i32* %numFaces to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #9
  %faces = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %conv, i32 0, i32 2
  %call48 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %faces)
  store i32 %call48, i32* %numFaces, align 4, !tbaa !10
  %47 = load i32, i32* %numFaces, align 4, !tbaa !10
  %48 = bitcast %class.btVector3* %ref.tmp49 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %48) #9
  %call50 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp49)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray* %faceNormals, i32 %47, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp49)
  %49 = bitcast %class.btVector3* %ref.tmp49 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %49) #9
  %50 = bitcast %class.btConvexHullComputer** %convexUtil to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #9
  store %class.btConvexHullComputer* %conv, %class.btConvexHullComputer** %convexUtil, align 4, !tbaa !2
  %51 = bitcast %class.btAlignedObjectArray.0* %tmpFaces to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %51) #9
  %call51 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI6btFaceEC2Ev(%class.btAlignedObjectArray.0* %tmpFaces)
  %52 = load i32, i32* %numFaces, align 4, !tbaa !10
  %53 = bitcast %struct.btFace* %ref.tmp52 to i8*
  call void @llvm.lifetime.start.p0i8(i64 36, i8* %53) #9
  %54 = bitcast %struct.btFace* %ref.tmp52 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %54, i8 0, i32 36, i1 false)
  %call53 = call %struct.btFace* @_ZN6btFaceC2Ev(%struct.btFace* %ref.tmp52)
  call void @_ZN20btAlignedObjectArrayI6btFaceE6resizeEiRKS0_(%class.btAlignedObjectArray.0* %tmpFaces, i32 %52, %struct.btFace* nonnull align 4 dereferenceable(36) %ref.tmp52)
  %call54 = call %struct.btFace* @_ZN6btFaceD2Ev(%struct.btFace* %ref.tmp52) #9
  %55 = bitcast %struct.btFace* %ref.tmp52 to i8*
  call void @llvm.lifetime.end.p0i8(i64 36, i8* %55) #9
  %56 = bitcast i32* %numVertices to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #9
  %57 = load %class.btConvexHullComputer*, %class.btConvexHullComputer** %convexUtil, align 4, !tbaa !2
  %vertices = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %57, i32 0, i32 0
  %call55 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %vertices)
  store i32 %call55, i32* %numVertices, align 4, !tbaa !10
  %m_polyhedron56 = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  %58 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %m_polyhedron56, align 4, !tbaa !8
  %m_vertices = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %58, i32 0, i32 1
  %59 = load i32, i32* %numVertices, align 4, !tbaa !10
  %60 = bitcast %class.btVector3* %ref.tmp57 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %60) #9
  %call58 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp57)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray* %m_vertices, i32 %59, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp57)
  %61 = bitcast %class.btVector3* %ref.tmp57 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %61) #9
  %62 = bitcast i32* %p59 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #9
  store i32 0, i32* %p59, align 4, !tbaa !10
  br label %for.cond60

for.cond60:                                       ; preds = %for.inc69, %if.end46
  %63 = load i32, i32* %p59, align 4, !tbaa !10
  %64 = load i32, i32* %numVertices, align 4, !tbaa !10
  %cmp61 = icmp slt i32 %63, %64
  br i1 %cmp61, label %for.body63, label %for.cond.cleanup62

for.cond.cleanup62:                               ; preds = %for.cond60
  %65 = bitcast i32* %p59 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #9
  br label %for.end71

for.body63:                                       ; preds = %for.cond60
  %66 = load %class.btConvexHullComputer*, %class.btConvexHullComputer** %convexUtil, align 4, !tbaa !2
  %vertices64 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %66, i32 0, i32 0
  %67 = load i32, i32* %p59, align 4, !tbaa !10
  %call65 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %vertices64, i32 %67)
  %m_polyhedron66 = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  %68 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %m_polyhedron66, align 4, !tbaa !8
  %m_vertices67 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %68, i32 0, i32 1
  %69 = load i32, i32* %p59, align 4, !tbaa !10
  %call68 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_vertices67, i32 %69)
  %70 = bitcast %class.btVector3* %call68 to i8*
  %71 = bitcast %class.btVector3* %call65 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %70, i8* align 4 %71, i32 16, i1 false), !tbaa.struct !12
  br label %for.inc69

for.inc69:                                        ; preds = %for.body63
  %72 = load i32, i32* %p59, align 4, !tbaa !10
  %inc70 = add nsw i32 %72, 1
  store i32 %inc70, i32* %p59, align 4, !tbaa !10
  br label %for.cond60

for.end71:                                        ; preds = %for.cond.cleanup62
  %73 = bitcast i32* %i72 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #9
  store i32 0, i32* %i72, align 4, !tbaa !10
  br label %for.cond73

for.cond73:                                       ; preds = %for.inc149, %for.end71
  %74 = load i32, i32* %i72, align 4, !tbaa !10
  %75 = load i32, i32* %numFaces, align 4, !tbaa !10
  %cmp74 = icmp slt i32 %74, %75
  br i1 %cmp74, label %for.body76, label %for.cond.cleanup75

for.cond.cleanup75:                               ; preds = %for.cond73
  store i32 11, i32* %cleanup.dest.slot, align 4
  %76 = bitcast i32* %i72 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #9
  br label %for.end151

for.body76:                                       ; preds = %for.cond73
  %77 = bitcast i32* %face to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %77) #9
  %78 = load %class.btConvexHullComputer*, %class.btConvexHullComputer** %convexUtil, align 4, !tbaa !2
  %faces77 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %78, i32 0, i32 2
  %79 = load i32, i32* %i72, align 4, !tbaa !10
  %call78 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %faces77, i32 %79)
  %80 = load i32, i32* %call78, align 4, !tbaa !10
  store i32 %80, i32* %face, align 4, !tbaa !10
  %81 = bitcast %"class.btConvexHullComputer::Edge"** %firstEdge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %81) #9
  %82 = load %class.btConvexHullComputer*, %class.btConvexHullComputer** %convexUtil, align 4, !tbaa !2
  %edges = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %82, i32 0, i32 1
  %83 = load i32, i32* %face, align 4, !tbaa !10
  %call79 = call nonnull align 4 dereferenceable(12) %"class.btConvexHullComputer::Edge"* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEEixEi(%class.btAlignedObjectArray.8* %edges, i32 %83)
  store %"class.btConvexHullComputer::Edge"* %call79, %"class.btConvexHullComputer::Edge"** %firstEdge, align 4, !tbaa !2
  %84 = bitcast %"class.btConvexHullComputer::Edge"** %edge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %84) #9
  %85 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %firstEdge, align 4, !tbaa !2
  store %"class.btConvexHullComputer::Edge"* %85, %"class.btConvexHullComputer::Edge"** %edge, align 4, !tbaa !2
  %86 = bitcast [3 x %class.btVector3]* %edges80 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %86) #9
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %edges80, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %for.body76
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %for.body76 ], [ %arrayctor.next, %arrayctor.loop ]
  %call81 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %87 = bitcast i32* %numEdges to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %87) #9
  store i32 0, i32* %numEdges, align 4, !tbaa !10
  br label %do.body

do.body:                                          ; preds = %do.cond, %arrayctor.cont
  %88 = bitcast i32* %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #9
  %89 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %edge, align 4, !tbaa !2
  %call82 = call i32 @_ZNK20btConvexHullComputer4Edge15getSourceVertexEv(%"class.btConvexHullComputer::Edge"* %89)
  store i32 %call82, i32* %src, align 4, !tbaa !10
  %90 = load i32, i32* %i72, align 4, !tbaa !10
  %call83 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %tmpFaces, i32 %90)
  %m_indices = getelementptr inbounds %struct.btFace, %struct.btFace* %call83, i32 0, i32 0
  call void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.3* %m_indices, i32* nonnull align 4 dereferenceable(4) %src)
  %91 = bitcast i32* %targ to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %91) #9
  %92 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %edge, align 4, !tbaa !2
  %call84 = call i32 @_ZNK20btConvexHullComputer4Edge15getTargetVertexEv(%"class.btConvexHullComputer::Edge"* %92)
  store i32 %call84, i32* %targ, align 4, !tbaa !10
  %93 = bitcast %class.btVector3* %wa to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %93) #9
  %94 = load %class.btConvexHullComputer*, %class.btConvexHullComputer** %convexUtil, align 4, !tbaa !2
  %vertices85 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %94, i32 0, i32 0
  %95 = load i32, i32* %src, align 4, !tbaa !10
  %call86 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %vertices85, i32 %95)
  %96 = bitcast %class.btVector3* %wa to i8*
  %97 = bitcast %class.btVector3* %call86 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %96, i8* align 4 %97, i32 16, i1 false), !tbaa.struct !12
  %98 = bitcast %class.btVector3* %wb to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %98) #9
  %99 = load %class.btConvexHullComputer*, %class.btConvexHullComputer** %convexUtil, align 4, !tbaa !2
  %vertices87 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %99, i32 0, i32 0
  %100 = load i32, i32* %targ, align 4, !tbaa !10
  %call88 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %vertices87, i32 %100)
  %101 = bitcast %class.btVector3* %wb to i8*
  %102 = bitcast %class.btVector3* %call88 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %101, i8* align 4 %102, i32 16, i1 false), !tbaa.struct !12
  %103 = bitcast %class.btVector3* %newEdge to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %103) #9
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %newEdge, %class.btVector3* nonnull align 4 dereferenceable(16) %wb, %class.btVector3* nonnull align 4 dereferenceable(16) %wa)
  %call89 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %newEdge)
  %104 = load i32, i32* %numEdges, align 4, !tbaa !10
  %cmp90 = icmp slt i32 %104, 2
  br i1 %cmp90, label %if.then91, label %if.end94

if.then91:                                        ; preds = %do.body
  %105 = load i32, i32* %numEdges, align 4, !tbaa !10
  %inc92 = add nsw i32 %105, 1
  store i32 %inc92, i32* %numEdges, align 4, !tbaa !10
  %arrayidx93 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %edges80, i32 0, i32 %105
  %106 = bitcast %class.btVector3* %arrayidx93 to i8*
  %107 = bitcast %class.btVector3* %newEdge to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %106, i8* align 4 %107, i32 16, i1 false), !tbaa.struct !12
  br label %if.end94

if.end94:                                         ; preds = %if.then91, %do.body
  %108 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %edge, align 4, !tbaa !2
  %call95 = call %"class.btConvexHullComputer::Edge"* @_ZNK20btConvexHullComputer4Edge17getNextEdgeOfFaceEv(%"class.btConvexHullComputer::Edge"* %108)
  store %"class.btConvexHullComputer::Edge"* %call95, %"class.btConvexHullComputer::Edge"** %edge, align 4, !tbaa !2
  %109 = bitcast %class.btVector3* %newEdge to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %109) #9
  %110 = bitcast %class.btVector3* %wb to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %110) #9
  %111 = bitcast %class.btVector3* %wa to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %111) #9
  %112 = bitcast i32* %targ to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #9
  %113 = bitcast i32* %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #9
  br label %do.cond

do.cond:                                          ; preds = %if.end94
  %114 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %edge, align 4, !tbaa !2
  %115 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %firstEdge, align 4, !tbaa !2
  %cmp96 = icmp ne %"class.btConvexHullComputer::Edge"* %114, %115
  br i1 %cmp96, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %116 = bitcast float* %planeEq to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %116) #9
  store float 0x46293E5940000000, float* %planeEq, align 4, !tbaa !14
  %117 = load i32, i32* %numEdges, align 4, !tbaa !10
  %cmp97 = icmp eq i32 %117, 2
  br i1 %cmp97, label %if.then98, label %if.else122

if.then98:                                        ; preds = %do.end
  %118 = bitcast %class.btVector3* %ref.tmp99 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %118) #9
  %arrayidx100 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %edges80, i32 0, i32 0
  %arrayidx101 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %edges80, i32 0, i32 1
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp99, %class.btVector3* %arrayidx100, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx101)
  %119 = load i32, i32* %i72, align 4, !tbaa !10
  %call102 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %faceNormals, i32 %119)
  %120 = bitcast %class.btVector3* %call102 to i8*
  %121 = bitcast %class.btVector3* %ref.tmp99 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %120, i8* align 4 %121, i32 16, i1 false), !tbaa.struct !12
  %122 = bitcast %class.btVector3* %ref.tmp99 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %122) #9
  %123 = load i32, i32* %i72, align 4, !tbaa !10
  %call103 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %faceNormals, i32 %123)
  %call104 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %call103)
  %124 = load i32, i32* %i72, align 4, !tbaa !10
  %call105 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %faceNormals, i32 %124)
  %call106 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %call105)
  %125 = load float, float* %call106, align 4, !tbaa !14
  %126 = load i32, i32* %i72, align 4, !tbaa !10
  %call107 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %tmpFaces, i32 %126)
  %m_plane = getelementptr inbounds %struct.btFace, %struct.btFace* %call107, i32 0, i32 1
  %arrayidx108 = getelementptr inbounds [4 x float], [4 x float]* %m_plane, i32 0, i32 0
  store float %125, float* %arrayidx108, align 4, !tbaa !14
  %127 = load i32, i32* %i72, align 4, !tbaa !10
  %call109 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %faceNormals, i32 %127)
  %call110 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %call109)
  %128 = load float, float* %call110, align 4, !tbaa !14
  %129 = load i32, i32* %i72, align 4, !tbaa !10
  %call111 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %tmpFaces, i32 %129)
  %m_plane112 = getelementptr inbounds %struct.btFace, %struct.btFace* %call111, i32 0, i32 1
  %arrayidx113 = getelementptr inbounds [4 x float], [4 x float]* %m_plane112, i32 0, i32 1
  store float %128, float* %arrayidx113, align 4, !tbaa !14
  %130 = load i32, i32* %i72, align 4, !tbaa !10
  %call114 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %faceNormals, i32 %130)
  %call115 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %call114)
  %131 = load float, float* %call115, align 4, !tbaa !14
  %132 = load i32, i32* %i72, align 4, !tbaa !10
  %call116 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %tmpFaces, i32 %132)
  %m_plane117 = getelementptr inbounds %struct.btFace, %struct.btFace* %call116, i32 0, i32 1
  %arrayidx118 = getelementptr inbounds [4 x float], [4 x float]* %m_plane117, i32 0, i32 2
  store float %131, float* %arrayidx118, align 4, !tbaa !14
  %133 = load float, float* %planeEq, align 4, !tbaa !14
  %134 = load i32, i32* %i72, align 4, !tbaa !10
  %call119 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %tmpFaces, i32 %134)
  %m_plane120 = getelementptr inbounds %struct.btFace, %struct.btFace* %call119, i32 0, i32 1
  %arrayidx121 = getelementptr inbounds [4 x float], [4 x float]* %m_plane120, i32 0, i32 3
  store float %133, float* %arrayidx121, align 4, !tbaa !14
  br label %if.end124

if.else122:                                       ; preds = %do.end
  %135 = load i32, i32* %i72, align 4, !tbaa !10
  %call123 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %faceNormals, i32 %135)
  call void @_ZN9btVector37setZeroEv(%class.btVector3* %call123)
  br label %if.end124

if.end124:                                        ; preds = %if.else122, %if.then98
  %136 = bitcast i32* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %136) #9
  store i32 0, i32* %v, align 4, !tbaa !10
  br label %for.cond125

for.cond125:                                      ; preds = %for.inc143, %if.end124
  %137 = load i32, i32* %v, align 4, !tbaa !10
  %138 = load i32, i32* %i72, align 4, !tbaa !10
  %call126 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %tmpFaces, i32 %138)
  %m_indices127 = getelementptr inbounds %struct.btFace, %struct.btFace* %call126, i32 0, i32 0
  %call128 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %m_indices127)
  %cmp129 = icmp slt i32 %137, %call128
  br i1 %cmp129, label %for.body131, label %for.cond.cleanup130

for.cond.cleanup130:                              ; preds = %for.cond125
  store i32 16, i32* %cleanup.dest.slot, align 4
  %139 = bitcast i32* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #9
  br label %for.end145

for.body131:                                      ; preds = %for.cond125
  %140 = bitcast float* %eq to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %140) #9
  %m_polyhedron132 = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  %141 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %m_polyhedron132, align 4, !tbaa !8
  %m_vertices133 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %141, i32 0, i32 1
  %142 = load i32, i32* %i72, align 4, !tbaa !10
  %call134 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %tmpFaces, i32 %142)
  %m_indices135 = getelementptr inbounds %struct.btFace, %struct.btFace* %call134, i32 0, i32 0
  %143 = load i32, i32* %v, align 4, !tbaa !10
  %call136 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_indices135, i32 %143)
  %144 = load i32, i32* %call136, align 4, !tbaa !10
  %call137 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_vertices133, i32 %144)
  %145 = load i32, i32* %i72, align 4, !tbaa !10
  %call138 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %faceNormals, i32 %145)
  %call139 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call137, %class.btVector3* nonnull align 4 dereferenceable(16) %call138)
  store float %call139, float* %eq, align 4, !tbaa !14
  %146 = load float, float* %planeEq, align 4, !tbaa !14
  %147 = load float, float* %eq, align 4, !tbaa !14
  %cmp140 = fcmp ogt float %146, %147
  br i1 %cmp140, label %if.then141, label %if.end142

if.then141:                                       ; preds = %for.body131
  %148 = load float, float* %eq, align 4, !tbaa !14
  store float %148, float* %planeEq, align 4, !tbaa !14
  br label %if.end142

if.end142:                                        ; preds = %if.then141, %for.body131
  %149 = bitcast float* %eq to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #9
  br label %for.inc143

for.inc143:                                       ; preds = %if.end142
  %150 = load i32, i32* %v, align 4, !tbaa !10
  %inc144 = add nsw i32 %150, 1
  store i32 %inc144, i32* %v, align 4, !tbaa !10
  br label %for.cond125

for.end145:                                       ; preds = %for.cond.cleanup130
  %151 = load float, float* %planeEq, align 4, !tbaa !14
  %fneg = fneg float %151
  %152 = load i32, i32* %i72, align 4, !tbaa !10
  %call146 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %tmpFaces, i32 %152)
  %m_plane147 = getelementptr inbounds %struct.btFace, %struct.btFace* %call146, i32 0, i32 1
  %arrayidx148 = getelementptr inbounds [4 x float], [4 x float]* %m_plane147, i32 0, i32 3
  store float %fneg, float* %arrayidx148, align 4, !tbaa !14
  %153 = bitcast float* %planeEq to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #9
  %154 = bitcast i32* %numEdges to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %154) #9
  %155 = bitcast [3 x %class.btVector3]* %edges80 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %155) #9
  %156 = bitcast %"class.btConvexHullComputer::Edge"** %edge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %156) #9
  %157 = bitcast %"class.btConvexHullComputer::Edge"** %firstEdge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #9
  %158 = bitcast i32* %face to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %158) #9
  br label %for.inc149

for.inc149:                                       ; preds = %for.end145
  %159 = load i32, i32* %i72, align 4, !tbaa !10
  %inc150 = add nsw i32 %159, 1
  store i32 %inc150, i32* %i72, align 4, !tbaa !10
  br label %for.cond73

for.end151:                                       ; preds = %for.cond.cleanup75
  %160 = bitcast float* %faceWeldThreshold to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %160) #9
  store float 0x3FEFF7CEE0000000, float* %faceWeldThreshold, align 4, !tbaa !14
  %161 = bitcast %class.btAlignedObjectArray.3* %todoFaces to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %161) #9
  %call152 = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.3* %todoFaces)
  %162 = bitcast i32* %i153 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %162) #9
  store i32 0, i32* %i153, align 4, !tbaa !10
  br label %for.cond154

for.cond154:                                      ; preds = %for.inc159, %for.end151
  %163 = load i32, i32* %i153, align 4, !tbaa !10
  %call155 = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %tmpFaces)
  %cmp156 = icmp slt i32 %163, %call155
  br i1 %cmp156, label %for.body158, label %for.cond.cleanup157

for.cond.cleanup157:                              ; preds = %for.cond154
  store i32 19, i32* %cleanup.dest.slot, align 4
  %164 = bitcast i32* %i153 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #9
  br label %for.end161

for.body158:                                      ; preds = %for.cond154
  call void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.3* %todoFaces, i32* nonnull align 4 dereferenceable(4) %i153)
  br label %for.inc159

for.inc159:                                       ; preds = %for.body158
  %165 = load i32, i32* %i153, align 4, !tbaa !10
  %inc160 = add nsw i32 %165, 1
  store i32 %inc160, i32* %i153, align 4, !tbaa !10
  br label %for.cond154

for.end161:                                       ; preds = %for.cond.cleanup157
  br label %while.cond

while.cond:                                       ; preds = %if.end406, %for.end161
  %call162 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %todoFaces)
  %tobool163 = icmp ne i32 %call162, 0
  br i1 %tobool163, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %166 = bitcast %class.btAlignedObjectArray.3* %coplanarFaceGroup to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %166) #9
  %call164 = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.3* %coplanarFaceGroup)
  %167 = bitcast i32* %refFace to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %167) #9
  %call165 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %todoFaces)
  %sub166 = sub nsw i32 %call165, 1
  %call167 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %todoFaces, i32 %sub166)
  %168 = load i32, i32* %call167, align 4, !tbaa !10
  store i32 %168, i32* %refFace, align 4, !tbaa !10
  call void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.3* %coplanarFaceGroup, i32* nonnull align 4 dereferenceable(4) %refFace)
  %169 = bitcast %struct.btFace** %faceA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %169) #9
  %170 = load i32, i32* %refFace, align 4, !tbaa !10
  %call168 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %tmpFaces, i32 %170)
  store %struct.btFace* %call168, %struct.btFace** %faceA, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayIiE8pop_backEv(%class.btAlignedObjectArray.3* %todoFaces)
  %171 = bitcast %class.btVector3* %faceNormalA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %171) #9
  %172 = load %struct.btFace*, %struct.btFace** %faceA, align 4, !tbaa !2
  %m_plane169 = getelementptr inbounds %struct.btFace, %struct.btFace* %172, i32 0, i32 1
  %arrayidx170 = getelementptr inbounds [4 x float], [4 x float]* %m_plane169, i32 0, i32 0
  %173 = load %struct.btFace*, %struct.btFace** %faceA, align 4, !tbaa !2
  %m_plane171 = getelementptr inbounds %struct.btFace, %struct.btFace* %173, i32 0, i32 1
  %arrayidx172 = getelementptr inbounds [4 x float], [4 x float]* %m_plane171, i32 0, i32 1
  %174 = load %struct.btFace*, %struct.btFace** %faceA, align 4, !tbaa !2
  %m_plane173 = getelementptr inbounds %struct.btFace, %struct.btFace* %174, i32 0, i32 1
  %arrayidx174 = getelementptr inbounds [4 x float], [4 x float]* %m_plane173, i32 0, i32 2
  %call175 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %faceNormalA, float* nonnull align 4 dereferenceable(4) %arrayidx170, float* nonnull align 4 dereferenceable(4) %arrayidx172, float* nonnull align 4 dereferenceable(4) %arrayidx174)
  %175 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %175) #9
  %call176 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %todoFaces)
  %sub177 = sub nsw i32 %call176, 1
  store i32 %sub177, i32* %j, align 4, !tbaa !10
  br label %for.cond178

for.cond178:                                      ; preds = %for.inc196, %while.body
  %176 = load i32, i32* %j, align 4, !tbaa !10
  %cmp179 = icmp sge i32 %176, 0
  br i1 %cmp179, label %for.body181, label %for.cond.cleanup180

for.cond.cleanup180:                              ; preds = %for.cond178
  store i32 24, i32* %cleanup.dest.slot, align 4
  %177 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %177) #9
  br label %for.end197

for.body181:                                      ; preds = %for.cond178
  %178 = bitcast i32* %i182 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %178) #9
  %179 = load i32, i32* %j, align 4, !tbaa !10
  %call183 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %todoFaces, i32 %179)
  %180 = load i32, i32* %call183, align 4, !tbaa !10
  store i32 %180, i32* %i182, align 4, !tbaa !10
  %181 = bitcast %struct.btFace** %faceB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %181) #9
  %182 = load i32, i32* %i182, align 4, !tbaa !10
  %call184 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %tmpFaces, i32 %182)
  store %struct.btFace* %call184, %struct.btFace** %faceB, align 4, !tbaa !2
  %183 = bitcast %class.btVector3* %faceNormalB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %183) #9
  %184 = load %struct.btFace*, %struct.btFace** %faceB, align 4, !tbaa !2
  %m_plane185 = getelementptr inbounds %struct.btFace, %struct.btFace* %184, i32 0, i32 1
  %arrayidx186 = getelementptr inbounds [4 x float], [4 x float]* %m_plane185, i32 0, i32 0
  %185 = load %struct.btFace*, %struct.btFace** %faceB, align 4, !tbaa !2
  %m_plane187 = getelementptr inbounds %struct.btFace, %struct.btFace* %185, i32 0, i32 1
  %arrayidx188 = getelementptr inbounds [4 x float], [4 x float]* %m_plane187, i32 0, i32 1
  %186 = load %struct.btFace*, %struct.btFace** %faceB, align 4, !tbaa !2
  %m_plane189 = getelementptr inbounds %struct.btFace, %struct.btFace* %186, i32 0, i32 1
  %arrayidx190 = getelementptr inbounds [4 x float], [4 x float]* %m_plane189, i32 0, i32 2
  %call191 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %faceNormalB, float* nonnull align 4 dereferenceable(4) %arrayidx186, float* nonnull align 4 dereferenceable(4) %arrayidx188, float* nonnull align 4 dereferenceable(4) %arrayidx190)
  %call192 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %faceNormalA, %class.btVector3* nonnull align 4 dereferenceable(16) %faceNormalB)
  %187 = load float, float* %faceWeldThreshold, align 4, !tbaa !14
  %cmp193 = fcmp ogt float %call192, %187
  br i1 %cmp193, label %if.then194, label %if.end195

if.then194:                                       ; preds = %for.body181
  call void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.3* %coplanarFaceGroup, i32* nonnull align 4 dereferenceable(4) %i182)
  call void @_ZN20btAlignedObjectArrayIiE6removeERKi(%class.btAlignedObjectArray.3* %todoFaces, i32* nonnull align 4 dereferenceable(4) %i182)
  br label %if.end195

if.end195:                                        ; preds = %if.then194, %for.body181
  %188 = bitcast %class.btVector3* %faceNormalB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %188) #9
  %189 = bitcast %struct.btFace** %faceB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %189) #9
  %190 = bitcast i32* %i182 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %190) #9
  br label %for.inc196

for.inc196:                                       ; preds = %if.end195
  %191 = load i32, i32* %j, align 4, !tbaa !10
  %dec = add nsw i32 %191, -1
  store i32 %dec, i32* %j, align 4, !tbaa !10
  br label %for.cond178

for.end197:                                       ; preds = %for.cond.cleanup180
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %did_merge) #9
  store i8 0, i8* %did_merge, align 1, !tbaa !16
  %call198 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %coplanarFaceGroup)
  %cmp199 = icmp sgt i32 %call198, 1
  br i1 %cmp199, label %if.then200, label %if.end386

if.then200:                                       ; preds = %for.end197
  %192 = bitcast %class.btAlignedObjectArray.12* %orgpoints to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %192) #9
  %call201 = call %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayI13GrahamVector3EC2Ev(%class.btAlignedObjectArray.12* %orgpoints)
  %193 = bitcast %class.btVector3* %averageFaceNormal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %193) #9
  %194 = bitcast float* %ref.tmp202 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %194) #9
  store float 0.000000e+00, float* %ref.tmp202, align 4, !tbaa !14
  %195 = bitcast float* %ref.tmp203 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %195) #9
  store float 0.000000e+00, float* %ref.tmp203, align 4, !tbaa !14
  %196 = bitcast float* %ref.tmp204 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %196) #9
  store float 0.000000e+00, float* %ref.tmp204, align 4, !tbaa !14
  %call205 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %averageFaceNormal, float* nonnull align 4 dereferenceable(4) %ref.tmp202, float* nonnull align 4 dereferenceable(4) %ref.tmp203, float* nonnull align 4 dereferenceable(4) %ref.tmp204)
  %197 = bitcast float* %ref.tmp204 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %197) #9
  %198 = bitcast float* %ref.tmp203 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %198) #9
  %199 = bitcast float* %ref.tmp202 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %199) #9
  %200 = bitcast i32* %i206 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %200) #9
  store i32 0, i32* %i206, align 4, !tbaa !10
  br label %for.cond207

for.cond207:                                      ; preds = %for.inc256, %if.then200
  %201 = load i32, i32* %i206, align 4, !tbaa !10
  %call208 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %coplanarFaceGroup)
  %cmp209 = icmp slt i32 %201, %call208
  br i1 %cmp209, label %for.body211, label %for.cond.cleanup210

for.cond.cleanup210:                              ; preds = %for.cond207
  store i32 27, i32* %cleanup.dest.slot, align 4
  %202 = bitcast i32* %i206 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %202) #9
  br label %for.end259

for.body211:                                      ; preds = %for.cond207
  %203 = bitcast %struct.btFace** %face212 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %203) #9
  %204 = load i32, i32* %i206, align 4, !tbaa !10
  %call213 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %coplanarFaceGroup, i32 %204)
  %205 = load i32, i32* %call213, align 4, !tbaa !10
  %call214 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %tmpFaces, i32 %205)
  store %struct.btFace* %call214, %struct.btFace** %face212, align 4, !tbaa !2
  %206 = bitcast %class.btVector3* %faceNormal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %206) #9
  %207 = load %struct.btFace*, %struct.btFace** %face212, align 4, !tbaa !2
  %m_plane215 = getelementptr inbounds %struct.btFace, %struct.btFace* %207, i32 0, i32 1
  %arrayidx216 = getelementptr inbounds [4 x float], [4 x float]* %m_plane215, i32 0, i32 0
  %208 = load %struct.btFace*, %struct.btFace** %face212, align 4, !tbaa !2
  %m_plane217 = getelementptr inbounds %struct.btFace, %struct.btFace* %208, i32 0, i32 1
  %arrayidx218 = getelementptr inbounds [4 x float], [4 x float]* %m_plane217, i32 0, i32 1
  %209 = load %struct.btFace*, %struct.btFace** %face212, align 4, !tbaa !2
  %m_plane219 = getelementptr inbounds %struct.btFace, %struct.btFace* %209, i32 0, i32 1
  %arrayidx220 = getelementptr inbounds [4 x float], [4 x float]* %m_plane219, i32 0, i32 2
  %call221 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %faceNormal, float* nonnull align 4 dereferenceable(4) %arrayidx216, float* nonnull align 4 dereferenceable(4) %arrayidx218, float* nonnull align 4 dereferenceable(4) %arrayidx220)
  %call222 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %averageFaceNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %faceNormal)
  %210 = bitcast i32* %f to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %210) #9
  store i32 0, i32* %f, align 4, !tbaa !10
  br label %for.cond223

for.cond223:                                      ; preds = %for.inc252, %for.body211
  %211 = load i32, i32* %f, align 4, !tbaa !10
  %212 = load %struct.btFace*, %struct.btFace** %face212, align 4, !tbaa !2
  %m_indices224 = getelementptr inbounds %struct.btFace, %struct.btFace* %212, i32 0, i32 0
  %call225 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %m_indices224)
  %cmp226 = icmp slt i32 %211, %call225
  br i1 %cmp226, label %for.body228, label %for.cond.cleanup227

for.cond.cleanup227:                              ; preds = %for.cond223
  store i32 30, i32* %cleanup.dest.slot, align 4
  %213 = bitcast i32* %f to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %213) #9
  br label %for.end255

for.body228:                                      ; preds = %for.cond223
  %214 = bitcast i32* %orgIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %214) #9
  %215 = load %struct.btFace*, %struct.btFace** %face212, align 4, !tbaa !2
  %m_indices229 = getelementptr inbounds %struct.btFace, %struct.btFace* %215, i32 0, i32 0
  %216 = load i32, i32* %f, align 4, !tbaa !10
  %call230 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_indices229, i32 %216)
  %217 = load i32, i32* %call230, align 4, !tbaa !10
  store i32 %217, i32* %orgIndex, align 4, !tbaa !10
  %218 = bitcast %class.btVector3* %pt to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %218) #9
  %m_polyhedron231 = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  %219 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %m_polyhedron231, align 4, !tbaa !8
  %m_vertices232 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %219, i32 0, i32 1
  %220 = load i32, i32* %orgIndex, align 4, !tbaa !10
  %call233 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_vertices232, i32 %220)
  %221 = bitcast %class.btVector3* %pt to i8*
  %222 = bitcast %class.btVector3* %call233 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %221, i8* align 4 %222, i32 16, i1 false), !tbaa.struct !12
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %found) #9
  store i8 0, i8* %found, align 1, !tbaa !16
  %223 = bitcast i32* %i234 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %223) #9
  store i32 0, i32* %i234, align 4, !tbaa !10
  br label %for.cond235

for.cond235:                                      ; preds = %for.inc244, %for.body228
  %224 = load i32, i32* %i234, align 4, !tbaa !10
  %call236 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %orgpoints)
  %cmp237 = icmp slt i32 %224, %call236
  br i1 %cmp237, label %for.body239, label %for.cond.cleanup238

for.cond.cleanup238:                              ; preds = %for.cond235
  store i32 33, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body239:                                      ; preds = %for.cond235
  %225 = load i32, i32* %i234, align 4, !tbaa !10
  %call240 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %orgpoints, i32 %225)
  %m_orgIndex = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %call240, i32 0, i32 2
  %226 = load i32, i32* %m_orgIndex, align 4, !tbaa !18
  %227 = load i32, i32* %orgIndex, align 4, !tbaa !10
  %cmp241 = icmp eq i32 %226, %227
  br i1 %cmp241, label %if.then242, label %if.end243

if.then242:                                       ; preds = %for.body239
  store i8 1, i8* %found, align 1, !tbaa !16
  store i32 33, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end243:                                        ; preds = %for.body239
  br label %for.inc244

for.inc244:                                       ; preds = %if.end243
  %228 = load i32, i32* %i234, align 4, !tbaa !10
  %inc245 = add nsw i32 %228, 1
  store i32 %inc245, i32* %i234, align 4, !tbaa !10
  br label %for.cond235

cleanup:                                          ; preds = %if.then242, %for.cond.cleanup238
  %229 = bitcast i32* %i234 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %229) #9
  br label %for.end246

for.end246:                                       ; preds = %cleanup
  %230 = load i8, i8* %found, align 1, !tbaa !16, !range !20
  %tobool247 = trunc i8 %230 to i1
  br i1 %tobool247, label %if.end251, label %if.then248

if.then248:                                       ; preds = %for.end246
  %231 = bitcast %struct.GrahamVector3* %ref.tmp249 to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %231) #9
  %232 = load i32, i32* %orgIndex, align 4, !tbaa !10
  %call250 = call %struct.GrahamVector3* @_ZN13GrahamVector3C2ERK9btVector3i(%struct.GrahamVector3* %ref.tmp249, %class.btVector3* nonnull align 4 dereferenceable(16) %pt, i32 %232)
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E9push_backERKS0_(%class.btAlignedObjectArray.12* %orgpoints, %struct.GrahamVector3* nonnull align 4 dereferenceable(24) %ref.tmp249)
  %233 = bitcast %struct.GrahamVector3* %ref.tmp249 to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %233) #9
  br label %if.end251

if.end251:                                        ; preds = %if.then248, %for.end246
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %found) #9
  %234 = bitcast %class.btVector3* %pt to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %234) #9
  %235 = bitcast i32* %orgIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %235) #9
  br label %for.inc252

for.inc252:                                       ; preds = %if.end251
  %236 = load i32, i32* %f, align 4, !tbaa !10
  %inc253 = add nsw i32 %236, 1
  store i32 %inc253, i32* %f, align 4, !tbaa !10
  br label %for.cond223

for.end255:                                       ; preds = %for.cond.cleanup227
  %237 = bitcast %class.btVector3* %faceNormal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %237) #9
  %238 = bitcast %struct.btFace** %face212 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %238) #9
  br label %for.inc256

for.inc256:                                       ; preds = %for.end255
  %239 = load i32, i32* %i206, align 4, !tbaa !10
  %inc257 = add nsw i32 %239, 1
  store i32 %inc257, i32* %i206, align 4, !tbaa !10
  br label %for.cond207

for.end259:                                       ; preds = %for.cond.cleanup210
  %240 = bitcast %struct.btFace* %combinedFace to i8*
  call void @llvm.lifetime.start.p0i8(i64 36, i8* %240) #9
  %call260 = call %struct.btFace* @_ZN6btFaceC2Ev(%struct.btFace* %combinedFace)
  %241 = bitcast i32* %i261 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %241) #9
  store i32 0, i32* %i261, align 4, !tbaa !10
  br label %for.cond262

for.cond262:                                      ; preds = %for.inc272, %for.end259
  %242 = load i32, i32* %i261, align 4, !tbaa !10
  %cmp263 = icmp slt i32 %242, 4
  br i1 %cmp263, label %for.body265, label %for.cond.cleanup264

for.cond.cleanup264:                              ; preds = %for.cond262
  store i32 36, i32* %cleanup.dest.slot, align 4
  %243 = bitcast i32* %i261 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %243) #9
  br label %for.end275

for.body265:                                      ; preds = %for.cond262
  %call266 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %coplanarFaceGroup, i32 0)
  %244 = load i32, i32* %call266, align 4, !tbaa !10
  %call267 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %tmpFaces, i32 %244)
  %m_plane268 = getelementptr inbounds %struct.btFace, %struct.btFace* %call267, i32 0, i32 1
  %245 = load i32, i32* %i261, align 4, !tbaa !10
  %arrayidx269 = getelementptr inbounds [4 x float], [4 x float]* %m_plane268, i32 0, i32 %245
  %246 = load float, float* %arrayidx269, align 4, !tbaa !14
  %m_plane270 = getelementptr inbounds %struct.btFace, %struct.btFace* %combinedFace, i32 0, i32 1
  %247 = load i32, i32* %i261, align 4, !tbaa !10
  %arrayidx271 = getelementptr inbounds [4 x float], [4 x float]* %m_plane270, i32 0, i32 %247
  store float %246, float* %arrayidx271, align 4, !tbaa !14
  br label %for.inc272

for.inc272:                                       ; preds = %for.body265
  %248 = load i32, i32* %i261, align 4, !tbaa !10
  %inc273 = add nsw i32 %248, 1
  store i32 %inc273, i32* %i261, align 4, !tbaa !10
  br label %for.cond262

for.end275:                                       ; preds = %for.cond.cleanup264
  %249 = bitcast %class.btAlignedObjectArray.12* %hull to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %249) #9
  %call276 = call %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayI13GrahamVector3EC2Ev(%class.btAlignedObjectArray.12* %hull)
  %call277 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %averageFaceNormal)
  call void @_Z22GrahamScanConvexHull2DR20btAlignedObjectArrayI13GrahamVector3ES2_RK9btVector3(%class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17) %orgpoints, %class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17) %hull, %class.btVector3* nonnull align 4 dereferenceable(16) %averageFaceNormal)
  %250 = bitcast i32* %i278 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %250) #9
  store i32 0, i32* %i278, align 4, !tbaa !10
  br label %for.cond279

for.cond279:                                      ; preds = %for.inc305, %for.end275
  %251 = load i32, i32* %i278, align 4, !tbaa !10
  %call280 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %hull)
  %cmp281 = icmp slt i32 %251, %call280
  br i1 %cmp281, label %for.body283, label %for.cond.cleanup282

for.cond.cleanup282:                              ; preds = %for.cond279
  store i32 39, i32* %cleanup.dest.slot, align 4
  %252 = bitcast i32* %i278 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %252) #9
  br label %for.end308

for.body283:                                      ; preds = %for.cond279
  %m_indices284 = getelementptr inbounds %struct.btFace, %struct.btFace* %combinedFace, i32 0, i32 0
  %253 = load i32, i32* %i278, align 4, !tbaa !10
  %call285 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %hull, i32 %253)
  %m_orgIndex286 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %call285, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.3* %m_indices284, i32* nonnull align 4 dereferenceable(4) %m_orgIndex286)
  %254 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %254) #9
  store i32 0, i32* %k, align 4, !tbaa !10
  br label %for.cond287

for.cond287:                                      ; preds = %for.inc301, %for.body283
  %255 = load i32, i32* %k, align 4, !tbaa !10
  %call288 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %orgpoints)
  %cmp289 = icmp slt i32 %255, %call288
  br i1 %cmp289, label %for.body291, label %for.cond.cleanup290

for.cond.cleanup290:                              ; preds = %for.cond287
  store i32 42, i32* %cleanup.dest.slot, align 4
  br label %cleanup303

for.body291:                                      ; preds = %for.cond287
  %256 = load i32, i32* %k, align 4, !tbaa !10
  %call292 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %orgpoints, i32 %256)
  %m_orgIndex293 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %call292, i32 0, i32 2
  %257 = load i32, i32* %m_orgIndex293, align 4, !tbaa !18
  %258 = load i32, i32* %i278, align 4, !tbaa !10
  %call294 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %hull, i32 %258)
  %m_orgIndex295 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %call294, i32 0, i32 2
  %259 = load i32, i32* %m_orgIndex295, align 4, !tbaa !18
  %cmp296 = icmp eq i32 %257, %259
  br i1 %cmp296, label %if.then297, label %if.end300

if.then297:                                       ; preds = %for.body291
  %260 = load i32, i32* %k, align 4, !tbaa !10
  %call298 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %orgpoints, i32 %260)
  %m_orgIndex299 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %call298, i32 0, i32 2
  store i32 -1, i32* %m_orgIndex299, align 4, !tbaa !18
  store i32 42, i32* %cleanup.dest.slot, align 4
  br label %cleanup303

if.end300:                                        ; preds = %for.body291
  br label %for.inc301

for.inc301:                                       ; preds = %if.end300
  %261 = load i32, i32* %k, align 4, !tbaa !10
  %inc302 = add nsw i32 %261, 1
  store i32 %inc302, i32* %k, align 4, !tbaa !10
  br label %for.cond287

cleanup303:                                       ; preds = %if.then297, %for.cond.cleanup290
  %262 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %262) #9
  br label %for.end304

for.end304:                                       ; preds = %cleanup303
  br label %for.inc305

for.inc305:                                       ; preds = %for.end304
  %263 = load i32, i32* %i278, align 4, !tbaa !10
  %inc306 = add nsw i32 %263, 1
  store i32 %inc306, i32* %i278, align 4, !tbaa !10
  br label %for.cond279

for.end308:                                       ; preds = %for.cond.cleanup282
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %reject_merge) #9
  store i8 0, i8* %reject_merge, align 1, !tbaa !16
  %264 = bitcast i32* %i309 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %264) #9
  store i32 0, i32* %i309, align 4, !tbaa !10
  br label %for.cond310

for.cond310:                                      ; preds = %for.inc375, %for.end308
  %265 = load i32, i32* %i309, align 4, !tbaa !10
  %call311 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %orgpoints)
  %cmp312 = icmp slt i32 %265, %call311
  br i1 %cmp312, label %for.body314, label %for.cond.cleanup313

for.cond.cleanup313:                              ; preds = %for.cond310
  store i32 45, i32* %cleanup.dest.slot, align 4
  br label %cleanup377

for.body314:                                      ; preds = %for.cond310
  %266 = load i32, i32* %i309, align 4, !tbaa !10
  %call315 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %orgpoints, i32 %266)
  %m_orgIndex316 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %call315, i32 0, i32 2
  %267 = load i32, i32* %m_orgIndex316, align 4, !tbaa !18
  %cmp317 = icmp eq i32 %267, -1
  br i1 %cmp317, label %if.then318, label %if.end319

if.then318:                                       ; preds = %for.body314
  br label %for.inc375

if.end319:                                        ; preds = %for.body314
  %268 = bitcast i32* %j320 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %268) #9
  store i32 0, i32* %j320, align 4, !tbaa !10
  br label %for.cond321

for.cond321:                                      ; preds = %for.inc368, %if.end319
  %269 = load i32, i32* %j320, align 4, !tbaa !10
  %call322 = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %tmpFaces)
  %cmp323 = icmp slt i32 %269, %call322
  br i1 %cmp323, label %for.body325, label %for.cond.cleanup324

for.cond.cleanup324:                              ; preds = %for.cond321
  store i32 48, i32* %cleanup.dest.slot, align 4
  br label %cleanup370

for.body325:                                      ; preds = %for.cond321
  %270 = bitcast %struct.btFace** %face326 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %270) #9
  %271 = load i32, i32* %j320, align 4, !tbaa !10
  %call327 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %tmpFaces, i32 %271)
  store %struct.btFace* %call327, %struct.btFace** %face326, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %is_in_current_group) #9
  store i8 0, i8* %is_in_current_group, align 1, !tbaa !16
  %272 = bitcast i32* %k328 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %272) #9
  store i32 0, i32* %k328, align 4, !tbaa !10
  br label %for.cond329

for.cond329:                                      ; preds = %for.inc338, %for.body325
  %273 = load i32, i32* %k328, align 4, !tbaa !10
  %call330 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %coplanarFaceGroup)
  %cmp331 = icmp slt i32 %273, %call330
  br i1 %cmp331, label %for.body333, label %for.cond.cleanup332

for.cond.cleanup332:                              ; preds = %for.cond329
  store i32 51, i32* %cleanup.dest.slot, align 4
  br label %cleanup340

for.body333:                                      ; preds = %for.cond329
  %274 = load i32, i32* %k328, align 4, !tbaa !10
  %call334 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %coplanarFaceGroup, i32 %274)
  %275 = load i32, i32* %call334, align 4, !tbaa !10
  %276 = load i32, i32* %j320, align 4, !tbaa !10
  %cmp335 = icmp eq i32 %275, %276
  br i1 %cmp335, label %if.then336, label %if.end337

if.then336:                                       ; preds = %for.body333
  store i8 1, i8* %is_in_current_group, align 1, !tbaa !16
  store i32 51, i32* %cleanup.dest.slot, align 4
  br label %cleanup340

if.end337:                                        ; preds = %for.body333
  br label %for.inc338

for.inc338:                                       ; preds = %if.end337
  %277 = load i32, i32* %k328, align 4, !tbaa !10
  %inc339 = add nsw i32 %277, 1
  store i32 %inc339, i32* %k328, align 4, !tbaa !10
  br label %for.cond329

cleanup340:                                       ; preds = %if.then336, %for.cond.cleanup332
  %278 = bitcast i32* %k328 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %278) #9
  br label %for.end341

for.end341:                                       ; preds = %cleanup340
  %279 = load i8, i8* %is_in_current_group, align 1, !tbaa !16, !range !20
  %tobool342 = trunc i8 %279 to i1
  br i1 %tobool342, label %if.then343, label %if.end344

if.then343:                                       ; preds = %for.end341
  store i32 50, i32* %cleanup.dest.slot, align 4
  br label %cleanup366

if.end344:                                        ; preds = %for.end341
  %280 = bitcast i32* %v345 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %280) #9
  store i32 0, i32* %v345, align 4, !tbaa !10
  br label %for.cond346

for.cond346:                                      ; preds = %for.inc359, %if.end344
  %281 = load i32, i32* %v345, align 4, !tbaa !10
  %282 = load %struct.btFace*, %struct.btFace** %face326, align 4, !tbaa !2
  %m_indices347 = getelementptr inbounds %struct.btFace, %struct.btFace* %282, i32 0, i32 0
  %call348 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %m_indices347)
  %cmp349 = icmp slt i32 %281, %call348
  br i1 %cmp349, label %for.body351, label %for.cond.cleanup350

for.cond.cleanup350:                              ; preds = %for.cond346
  store i32 54, i32* %cleanup.dest.slot, align 4
  br label %cleanup361

for.body351:                                      ; preds = %for.cond346
  %283 = load %struct.btFace*, %struct.btFace** %face326, align 4, !tbaa !2
  %m_indices352 = getelementptr inbounds %struct.btFace, %struct.btFace* %283, i32 0, i32 0
  %284 = load i32, i32* %v345, align 4, !tbaa !10
  %call353 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_indices352, i32 %284)
  %285 = load i32, i32* %call353, align 4, !tbaa !10
  %286 = load i32, i32* %i309, align 4, !tbaa !10
  %call354 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %orgpoints, i32 %286)
  %m_orgIndex355 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %call354, i32 0, i32 2
  %287 = load i32, i32* %m_orgIndex355, align 4, !tbaa !18
  %cmp356 = icmp eq i32 %285, %287
  br i1 %cmp356, label %if.then357, label %if.end358

if.then357:                                       ; preds = %for.body351
  store i8 1, i8* %reject_merge, align 1, !tbaa !16
  store i32 54, i32* %cleanup.dest.slot, align 4
  br label %cleanup361

if.end358:                                        ; preds = %for.body351
  br label %for.inc359

for.inc359:                                       ; preds = %if.end358
  %288 = load i32, i32* %v345, align 4, !tbaa !10
  %inc360 = add nsw i32 %288, 1
  store i32 %inc360, i32* %v345, align 4, !tbaa !10
  br label %for.cond346

cleanup361:                                       ; preds = %if.then357, %for.cond.cleanup350
  %289 = bitcast i32* %v345 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %289) #9
  br label %for.end362

for.end362:                                       ; preds = %cleanup361
  %290 = load i8, i8* %reject_merge, align 1, !tbaa !16, !range !20
  %tobool363 = trunc i8 %290 to i1
  br i1 %tobool363, label %if.then364, label %if.end365

if.then364:                                       ; preds = %for.end362
  store i32 48, i32* %cleanup.dest.slot, align 4
  br label %cleanup366

if.end365:                                        ; preds = %for.end362
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup366

cleanup366:                                       ; preds = %if.end365, %if.then364, %if.then343
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %is_in_current_group) #9
  %291 = bitcast %struct.btFace** %face326 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %291) #9
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup370 [
    i32 0, label %cleanup.cont
    i32 50, label %for.inc368
  ]

cleanup.cont:                                     ; preds = %cleanup366
  br label %for.inc368

for.inc368:                                       ; preds = %cleanup.cont, %cleanup366
  %292 = load i32, i32* %j320, align 4, !tbaa !10
  %inc369 = add nsw i32 %292, 1
  store i32 %inc369, i32* %j320, align 4, !tbaa !10
  br label %for.cond321

cleanup370:                                       ; preds = %cleanup366, %for.cond.cleanup324
  %293 = bitcast i32* %j320 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %293) #9
  br label %for.end371

for.end371:                                       ; preds = %cleanup370
  %294 = load i8, i8* %reject_merge, align 1, !tbaa !16, !range !20
  %tobool372 = trunc i8 %294 to i1
  br i1 %tobool372, label %if.then373, label %if.end374

if.then373:                                       ; preds = %for.end371
  store i32 45, i32* %cleanup.dest.slot, align 4
  br label %cleanup377

if.end374:                                        ; preds = %for.end371
  br label %for.inc375

for.inc375:                                       ; preds = %if.end374, %if.then318
  %295 = load i32, i32* %i309, align 4, !tbaa !10
  %inc376 = add nsw i32 %295, 1
  store i32 %inc376, i32* %i309, align 4, !tbaa !10
  br label %for.cond310

cleanup377:                                       ; preds = %if.then373, %for.cond.cleanup313
  %296 = bitcast i32* %i309 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %296) #9
  br label %for.end378

for.end378:                                       ; preds = %cleanup377
  %297 = load i8, i8* %reject_merge, align 1, !tbaa !16, !range !20
  %tobool379 = trunc i8 %297 to i1
  br i1 %tobool379, label %if.end382, label %if.then380

if.then380:                                       ; preds = %for.end378
  store i8 1, i8* %did_merge, align 1, !tbaa !16
  %m_polyhedron381 = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  %298 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %m_polyhedron381, align 4, !tbaa !8
  %m_faces = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %298, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayI6btFaceE9push_backERKS0_(%class.btAlignedObjectArray.0* %m_faces, %struct.btFace* nonnull align 4 dereferenceable(36) %combinedFace)
  br label %if.end382

if.end382:                                        ; preds = %if.then380, %for.end378
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %reject_merge) #9
  %call383 = call %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayI13GrahamVector3ED2Ev(%class.btAlignedObjectArray.12* %hull) #9
  %299 = bitcast %class.btAlignedObjectArray.12* %hull to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %299) #9
  %call384 = call %struct.btFace* @_ZN6btFaceD2Ev(%struct.btFace* %combinedFace) #9
  %300 = bitcast %struct.btFace* %combinedFace to i8*
  call void @llvm.lifetime.end.p0i8(i64 36, i8* %300) #9
  %301 = bitcast %class.btVector3* %averageFaceNormal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %301) #9
  %call385 = call %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayI13GrahamVector3ED2Ev(%class.btAlignedObjectArray.12* %orgpoints) #9
  %302 = bitcast %class.btAlignedObjectArray.12* %orgpoints to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %302) #9
  br label %if.end386

if.end386:                                        ; preds = %if.end382, %for.end197
  %303 = load i8, i8* %did_merge, align 1, !tbaa !16, !range !20
  %tobool387 = trunc i8 %303 to i1
  br i1 %tobool387, label %if.end406, label %if.then388

if.then388:                                       ; preds = %if.end386
  %304 = bitcast i32* %i389 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %304) #9
  store i32 0, i32* %i389, align 4, !tbaa !10
  br label %for.cond390

for.cond390:                                      ; preds = %for.inc402, %if.then388
  %305 = load i32, i32* %i389, align 4, !tbaa !10
  %call391 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %coplanarFaceGroup)
  %cmp392 = icmp slt i32 %305, %call391
  br i1 %cmp392, label %for.body394, label %for.cond.cleanup393

for.cond.cleanup393:                              ; preds = %for.cond390
  store i32 57, i32* %cleanup.dest.slot, align 4
  %306 = bitcast i32* %i389 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %306) #9
  br label %for.end405

for.body394:                                      ; preds = %for.cond390
  %307 = bitcast %struct.btFace* %face395 to i8*
  call void @llvm.lifetime.start.p0i8(i64 36, i8* %307) #9
  %308 = load i32, i32* %i389, align 4, !tbaa !10
  %call396 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %coplanarFaceGroup, i32 %308)
  %309 = load i32, i32* %call396, align 4, !tbaa !10
  %call397 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %tmpFaces, i32 %309)
  %call398 = call %struct.btFace* @_ZN6btFaceC2ERKS_(%struct.btFace* %face395, %struct.btFace* nonnull align 4 dereferenceable(36) %call397)
  %m_polyhedron399 = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  %310 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %m_polyhedron399, align 4, !tbaa !8
  %m_faces400 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %310, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayI6btFaceE9push_backERKS0_(%class.btAlignedObjectArray.0* %m_faces400, %struct.btFace* nonnull align 4 dereferenceable(36) %face395)
  %call401 = call %struct.btFace* @_ZN6btFaceD2Ev(%struct.btFace* %face395) #9
  %311 = bitcast %struct.btFace* %face395 to i8*
  call void @llvm.lifetime.end.p0i8(i64 36, i8* %311) #9
  br label %for.inc402

for.inc402:                                       ; preds = %for.body394
  %312 = load i32, i32* %i389, align 4, !tbaa !10
  %inc403 = add nsw i32 %312, 1
  store i32 %inc403, i32* %i389, align 4, !tbaa !10
  br label %for.cond390

for.end405:                                       ; preds = %for.cond.cleanup393
  br label %if.end406

if.end406:                                        ; preds = %for.end405, %if.end386
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %did_merge) #9
  %313 = bitcast %class.btVector3* %faceNormalA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %313) #9
  %314 = bitcast %struct.btFace** %faceA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %314) #9
  %315 = bitcast i32* %refFace to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %315) #9
  %call407 = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.3* %coplanarFaceGroup) #9
  %316 = bitcast %class.btAlignedObjectArray.3* %coplanarFaceGroup to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %316) #9
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %m_polyhedron408 = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  %317 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %m_polyhedron408, align 4, !tbaa !8
  call void @_ZN18btConvexPolyhedron10initializeEv(%class.btConvexPolyhedron* %317)
  store i32 1, i32* %cleanup.dest.slot, align 4
  %call410 = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.3* %todoFaces) #9
  %318 = bitcast %class.btAlignedObjectArray.3* %todoFaces to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %318) #9
  %319 = bitcast float* %faceWeldThreshold to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %319) #9
  %320 = bitcast i32* %numVertices to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %320) #9
  %call415 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI6btFaceED2Ev(%class.btAlignedObjectArray.0* %tmpFaces) #9
  %321 = bitcast %class.btAlignedObjectArray.0* %tmpFaces to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %321) #9
  %322 = bitcast %class.btConvexHullComputer** %convexUtil to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %322) #9
  %323 = bitcast i32* %numFaces to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %323) #9
  %call420 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* %faceNormals) #9
  %324 = bitcast %class.btAlignedObjectArray* %faceNormals to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %324) #9
  %call423 = call %class.btConvexHullComputer* @_ZN20btConvexHullComputerD2Ev(%class.btConvexHullComputer* %conv) #9
  %325 = bitcast %class.btConvexHullComputer* %conv to i8*
  call void @llvm.lifetime.end.p0i8(i64 60, i8* %325) #9
  %call426 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* %orgVertices) #9
  %326 = bitcast %class.btAlignedObjectArray* %orgVertices to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %326) #9
  %327 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %327) #9
  ret i1 true
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #5

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN18btConvexPolyhedronnwEmPv(i32 %0, i8* %ptr) #6 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !21
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

declare %class.btConvexPolyhedron* @_ZN18btConvexPolyhedronC1Ev(%class.btConvexPolyhedron* returned) unnamed_addr #1

define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3E6expandERKS0_(%class.btAlignedObjectArray* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %fillValue) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %fillValue.addr = alloca %class.btVector3*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %fillValue, %class.btVector3** %fillValue.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !10
  %1 = load i32, i32* %sz, align 4, !tbaa !10
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4, !tbaa !23
  %inc = add nsw i32 %2, 1
  store i32 %inc, i32* %m_size, align 4, !tbaa !23
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !26
  %4 = load i32, i32* %sz, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  %5 = bitcast %class.btVector3* %arrayidx to i8*
  %call5 = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %5)
  %6 = bitcast i8* %call5 to %class.btVector3*
  %7 = load %class.btVector3*, %class.btVector3** %fillValue.addr, align 4, !tbaa !2
  %8 = bitcast %class.btVector3* %6 to i8*
  %9 = bitcast %class.btVector3* %7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false), !tbaa.struct !12
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %10 = load %class.btVector3*, %class.btVector3** %m_data6, align 4, !tbaa !26
  %11 = load i32, i32* %sz, align 4, !tbaa !10
  %arrayidx7 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 %11
  %12 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  ret %class.btVector3* %arrayidx7
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #5

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btConvexHullComputer* @_ZN20btConvexHullComputerC2Ev(%class.btConvexHullComputer* returned %this) unnamed_addr #7 comdat {
entry:
  %this.addr = alloca %class.btConvexHullComputer*, align 4
  store %class.btConvexHullComputer* %this, %class.btConvexHullComputer** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexHullComputer*, %class.btConvexHullComputer** %this.addr, align 4
  %vertices = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* %vertices)
  %edges = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEEC2Ev(%class.btAlignedObjectArray.8* %edges)
  %faces = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 2
  %call3 = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.3* %faces)
  ret %class.btConvexHullComputer* %this1
}

declare void @_ZN14btGeometryUtil29getPlaneEquationsFromVerticesER20btAlignedObjectArrayI9btVector3ES3_(%class.btAlignedObjectArray* nonnull align 4 dereferenceable(17), %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17)) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !23
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %this, i32 %n) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !10
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !26
  %1 = load i32, i32* %n.addr, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %1
  ret %class.btVector3* %arrayidx
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #5

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %_Val) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %class.btVector3*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %_Val, %class.btVector3** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !10
  %1 = load i32, i32* %sz, align 4, !tbaa !10
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !26
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !23
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 %3
  %4 = bitcast %class.btVector3* %arrayidx to i8*
  %call5 = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %4)
  %5 = bitcast i8* %call5 to %class.btVector3*
  %6 = load %class.btVector3*, %class.btVector3** %_Val.addr, align 4, !tbaa !2
  %7 = bitcast %class.btVector3* %5 to i8*
  %8 = bitcast %class.btVector3* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !12
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %9 = load i32, i32* %m_size6, align 4, !tbaa !23
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %m_size6, align 4, !tbaa !23
  %10 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  ret void
}

declare void @_ZN14btGeometryUtil29getVerticesFromPlaneEquationsERK20btAlignedObjectArrayI9btVector3ERS2_(%class.btAlignedObjectArray* nonnull align 4 dereferenceable(17), %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17)) #1

define linkonce_odr hidden float @_ZN20btConvexHullComputer7computeEPKfiiff(%class.btConvexHullComputer* %this, float* %coords, i32 %stride, i32 %count, float %shrink, float %shrinkClamp) #0 comdat {
entry:
  %this.addr = alloca %class.btConvexHullComputer*, align 4
  %coords.addr = alloca float*, align 4
  %stride.addr = alloca i32, align 4
  %count.addr = alloca i32, align 4
  %shrink.addr = alloca float, align 4
  %shrinkClamp.addr = alloca float, align 4
  store %class.btConvexHullComputer* %this, %class.btConvexHullComputer** %this.addr, align 4, !tbaa !2
  store float* %coords, float** %coords.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !10
  store i32 %count, i32* %count.addr, align 4, !tbaa !10
  store float %shrink, float* %shrink.addr, align 4, !tbaa !14
  store float %shrinkClamp, float* %shrinkClamp.addr, align 4, !tbaa !14
  %this1 = load %class.btConvexHullComputer*, %class.btConvexHullComputer** %this.addr, align 4
  %0 = load float*, float** %coords.addr, align 4, !tbaa !2
  %1 = bitcast float* %0 to i8*
  %2 = load i32, i32* %stride.addr, align 4, !tbaa !10
  %3 = load i32, i32* %count.addr, align 4, !tbaa !10
  %4 = load float, float* %shrink.addr, align 4, !tbaa !14
  %5 = load float, float* %shrinkClamp.addr, align 4, !tbaa !14
  %call = call float @_ZN20btConvexHullComputer7computeEPKvbiiff(%class.btConvexHullComputer* %this1, i8* %1, i1 zeroext false, i32 %2, i32 %3, float %4, float %5)
  ret float %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !27
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray* %this, i32 %newsize, %class.btVector3* nonnull align 4 dereferenceable(16) %fillData) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btVector3*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !10
  store %class.btVector3* %fillData, %class.btVector3** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !10
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !10
  %2 = load i32, i32* %curSize, align 4, !tbaa !10
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !10
  store i32 %4, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !10
  %6 = load i32, i32* %curSize, align 4, !tbaa !10
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !26
  %9 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end17

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !10
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !10
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %14 = load i32, i32* %curSize, align 4, !tbaa !10
  store i32 %14, i32* %i6, align 4, !tbaa !10
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc14, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !10
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !10
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  br label %for.end16

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %18 = load %class.btVector3*, %class.btVector3** %m_data11, align 4, !tbaa !26
  %19 = load i32, i32* %i6, align 4, !tbaa !10
  %arrayidx12 = getelementptr inbounds %class.btVector3, %class.btVector3* %18, i32 %19
  %20 = bitcast %class.btVector3* %arrayidx12 to i8*
  %call13 = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %20)
  %21 = bitcast i8* %call13 to %class.btVector3*
  %22 = load %class.btVector3*, %class.btVector3** %fillData.addr, align 4, !tbaa !2
  %23 = bitcast %class.btVector3* %21 to i8*
  %24 = bitcast %class.btVector3* %22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false), !tbaa.struct !12
  br label %for.inc14

for.inc14:                                        ; preds = %for.body10
  %25 = load i32, i32* %i6, align 4, !tbaa !10
  %inc15 = add nsw i32 %25, 1
  store i32 %inc15, i32* %i6, align 4, !tbaa !10
  br label %for.cond7

for.end16:                                        ; preds = %for.cond.cleanup9
  br label %if.end17

if.end17:                                         ; preds = %for.end16, %for.end
  %26 = load i32, i32* %newsize.addr, align 4, !tbaa !10
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %26, i32* %m_size, align 4, !tbaa !23
  %27 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #9
  ret void
}

define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI6btFaceEC2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorI6btFaceLj16EEC2Ev(%class.btAlignedAllocator.1* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI6btFaceE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI6btFaceE6resizeEiRKS0_(%class.btAlignedObjectArray.0* %this, i32 %newsize, %struct.btFace* nonnull align 4 dereferenceable(36) %fillData) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %struct.btFace*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i7 = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !10
  store %struct.btFace* %fillData, %struct.btFace** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !10
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !10
  %2 = load i32, i32* %curSize, align 4, !tbaa !10
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !10
  store i32 %4, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !10
  %6 = load i32, i32* %curSize, align 4, !tbaa !10
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %8 = load %struct.btFace*, %struct.btFace** %m_data, align 4, !tbaa !30
  %9 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds %struct.btFace, %struct.btFace* %8, i32 %9
  %call3 = call %struct.btFace* @_ZN6btFaceD2Ev(%struct.btFace* %arrayidx) #9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end18

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !10
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %cmp5 = icmp sgt i32 %11, %call4
  br i1 %cmp5, label %if.then6, label %if.end

if.then6:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !10
  call void @_ZN20btAlignedObjectArrayI6btFaceE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then6, %if.else
  %13 = bitcast i32* %i7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %14 = load i32, i32* %curSize, align 4, !tbaa !10
  store i32 %14, i32* %i7, align 4, !tbaa !10
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc15, %if.end
  %15 = load i32, i32* %i7, align 4, !tbaa !10
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !10
  %cmp9 = icmp slt i32 %15, %16
  br i1 %cmp9, label %for.body11, label %for.cond.cleanup10

for.cond.cleanup10:                               ; preds = %for.cond8
  %17 = bitcast i32* %i7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  br label %for.end17

for.body11:                                       ; preds = %for.cond8
  %m_data12 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %18 = load %struct.btFace*, %struct.btFace** %m_data12, align 4, !tbaa !30
  %19 = load i32, i32* %i7, align 4, !tbaa !10
  %arrayidx13 = getelementptr inbounds %struct.btFace, %struct.btFace* %18, i32 %19
  %20 = bitcast %struct.btFace* %arrayidx13 to i8*
  %21 = bitcast i8* %20 to %struct.btFace*
  %22 = load %struct.btFace*, %struct.btFace** %fillData.addr, align 4, !tbaa !2
  %call14 = call %struct.btFace* @_ZN6btFaceC2ERKS_(%struct.btFace* %21, %struct.btFace* nonnull align 4 dereferenceable(36) %22)
  br label %for.inc15

for.inc15:                                        ; preds = %for.body11
  %23 = load i32, i32* %i7, align 4, !tbaa !10
  %inc16 = add nsw i32 %23, 1
  store i32 %inc16, i32* %i7, align 4, !tbaa !10
  br label %for.cond8

for.end17:                                        ; preds = %for.cond.cleanup10
  br label %if.end18

if.end18:                                         ; preds = %for.end17, %for.end
  %24 = load i32, i32* %newsize.addr, align 4, !tbaa !10
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 %24, i32* %m_size, align 4, !tbaa !33
  %25 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #9
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #8

; Function Attrs: inlinehint
define linkonce_odr hidden %struct.btFace* @_ZN6btFaceC2Ev(%struct.btFace* returned %this) unnamed_addr #7 comdat {
entry:
  %this.addr = alloca %struct.btFace*, align 4
  store %struct.btFace* %this, %struct.btFace** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btFace*, %struct.btFace** %this.addr, align 4
  %m_indices = getelementptr inbounds %struct.btFace, %struct.btFace* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.3* %m_indices)
  ret %struct.btFace* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %struct.btFace* @_ZN6btFaceD2Ev(%struct.btFace* returned %this) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %struct.btFace*, align 4
  store %struct.btFace* %this, %struct.btFace** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btFace*, %struct.btFace** %this.addr, align 4
  %m_indices = getelementptr inbounds %struct.btFace, %struct.btFace* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.3* %m_indices) #9
  ret %struct.btFace* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %this, i32 %n) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !10
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !34
  %1 = load i32, i32* %n.addr, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"class.btConvexHullComputer::Edge"* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEEixEi(%class.btAlignedObjectArray.8* %this, i32 %n) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !10
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %m_data, align 4, !tbaa !35
  %1 = load i32, i32* %n.addr, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %0, i32 %1
  ret %"class.btConvexHullComputer::Edge"* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK20btConvexHullComputer4Edge15getSourceVertexEv(%"class.btConvexHullComputer::Edge"* %this) #3 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullComputer::Edge"*, align 4
  store %"class.btConvexHullComputer::Edge"* %this, %"class.btConvexHullComputer::Edge"** %this.addr, align 4, !tbaa !2
  %this1 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %this.addr, align 4
  %reverse = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %this1, i32 0, i32 1
  %0 = load i32, i32* %reverse, align 4, !tbaa !38
  %add.ptr = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %this1, i32 %0
  %targetVertex = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %add.ptr, i32 0, i32 2
  %1 = load i32, i32* %targetVertex, align 4, !tbaa !40
  ret i32 %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !10
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btFace*, %struct.btFace** %m_data, align 4, !tbaa !30
  %1 = load i32, i32* %n.addr, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds %struct.btFace, %struct.btFace* %0, i32 %1
  ret %struct.btFace* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.3* %this, i32* nonnull align 4 dereferenceable(4) %_Val) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %_Val.addr = alloca i32*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  store i32* %_Val, i32** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !10
  %1 = load i32, i32* %sz, align 4, !tbaa !10
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.3* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIiE9allocSizeEi(%class.btAlignedObjectArray.3* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.3* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data, align 4, !tbaa !34
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !27
  %arrayidx = getelementptr inbounds i32, i32* %2, i32 %3
  %4 = bitcast i32* %arrayidx to i8*
  %5 = bitcast i8* %4 to i32*
  %6 = load i32*, i32** %_Val.addr, align 4, !tbaa !2
  %7 = load i32, i32* %6, align 4, !tbaa !10
  store i32 %7, i32* %5, align 4, !tbaa !10
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size5, align 4, !tbaa !27
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size5, align 4, !tbaa !27
  %9 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK20btConvexHullComputer4Edge15getTargetVertexEv(%"class.btConvexHullComputer::Edge"* %this) #3 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullComputer::Edge"*, align 4
  store %"class.btConvexHullComputer::Edge"* %this, %"class.btConvexHullComputer::Edge"** %this.addr, align 4, !tbaa !2
  %this1 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %this.addr, align 4
  %targetVertex = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %this1, i32 0, i32 2
  %0 = load i32, i32* %targetVertex, align 4, !tbaa !40
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #7 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !14
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !14
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !14
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !14
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !14
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !14
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !14
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !14
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !14
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4, !tbaa !14
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1) #9
  ret %class.btVector3* %call2
}

define linkonce_odr hidden %"class.btConvexHullComputer::Edge"* @_ZNK20btConvexHullComputer4Edge17getNextEdgeOfFaceEv(%"class.btConvexHullComputer::Edge"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullComputer::Edge"*, align 4
  store %"class.btConvexHullComputer::Edge"* %this, %"class.btConvexHullComputer::Edge"** %this.addr, align 4, !tbaa !2
  %this1 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %this.addr, align 4
  %reverse = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %this1, i32 0, i32 1
  %0 = load i32, i32* %reverse, align 4, !tbaa !38
  %add.ptr = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %this1, i32 %0
  %call = call %"class.btConvexHullComputer::Edge"* @_ZNK20btConvexHullComputer4Edge19getNextEdgeOfVertexEv(%"class.btConvexHullComputer::Edge"* %add.ptr)
  ret %"class.btConvexHullComputer::Edge"* %call
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #7 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %1 = load float, float* %arrayidx, align 4, !tbaa !14
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %3 = load float, float* %arrayidx3, align 4, !tbaa !14
  %mul = fmul float %1, %3
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %4 = load float, float* %arrayidx5, align 4, !tbaa !14
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %6 = load float, float* %arrayidx7, align 4, !tbaa !14
  %mul8 = fmul float %4, %6
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4, !tbaa !14
  %7 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %8 = load float, float* %arrayidx11, align 4, !tbaa !14
  %9 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %10 = load float, float* %arrayidx13, align 4, !tbaa !14
  %mul14 = fmul float %8, %10
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %11 = load float, float* %arrayidx16, align 4, !tbaa !14
  %12 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %13 = load float, float* %arrayidx18, align 4, !tbaa !14
  %mul19 = fmul float %11, %13
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4, !tbaa !14
  %14 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #9
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %15 = load float, float* %arrayidx23, align 4, !tbaa !14
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %17 = load float, float* %arrayidx25, align 4, !tbaa !14
  %mul26 = fmul float %15, %17
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %18 = load float, float* %arrayidx28, align 4, !tbaa !14
  %19 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %19, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %20 = load float, float* %arrayidx30, align 4, !tbaa !14
  %mul31 = fmul float %18, %20
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4, !tbaa !14
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %21 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #9
  %22 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #9
  %23 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

define linkonce_odr hidden void @_ZN9btVector37setZeroEv(%class.btVector3* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !14
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !14
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !14
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !14
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !14
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !14
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !14
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !14
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !14
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

define linkonce_odr hidden %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.3* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.4* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.4* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.3* %this1)
  ret %class.btAlignedObjectArray.3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !33
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE8pop_backEv(%class.btAlignedObjectArray.3* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !27
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4, !tbaa !27
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %1 = load i32*, i32** %m_data, align 4, !tbaa !34
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4, !tbaa !27
  %arrayidx = getelementptr inbounds i32, i32* %1, i32 %2
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !14
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !14
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !14
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !14
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !14
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !14
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !14
  ret %class.btVector3* %this1
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE6removeERKi(%class.btAlignedObjectArray.3* %this, i32* nonnull align 4 dereferenceable(4) %key) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %key.addr = alloca i32*, align 4
  %findIndex = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  store i32* %key, i32** %key.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %0 = bitcast i32* %findIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32*, i32** %key.addr, align 4, !tbaa !2
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE16findLinearSearchERKi(%class.btAlignedObjectArray.3* %this1, i32* nonnull align 4 dereferenceable(4) %1)
  store i32 %call, i32* %findIndex, align 4, !tbaa !10
  %2 = load i32, i32* %findIndex, align 4, !tbaa !10
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  %cmp = icmp slt i32 %2, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load i32, i32* %findIndex, align 4, !tbaa !10
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  %sub = sub nsw i32 %call3, 1
  call void @_ZN20btAlignedObjectArrayIiE4swapEii(%class.btAlignedObjectArray.3* %this1, i32 %3, i32 %sub)
  call void @_ZN20btAlignedObjectArrayIiE8pop_backEv(%class.btAlignedObjectArray.3* %this1)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %4 = bitcast i32* %findIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  ret void
}

define linkonce_odr hidden %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayI13GrahamVector3EC2Ev(%class.btAlignedObjectArray.12* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.13* @_ZN18btAlignedAllocatorI13GrahamVector3Lj16EEC2Ev(%class.btAlignedAllocator.13* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E4initEv(%class.btAlignedObjectArray.12* %this1)
  ret %class.btAlignedObjectArray.12* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !14
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !14
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !14
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !14
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !14
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !14
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !14
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !14
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !14
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !41
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %this, i32 %n) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !10
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %0 = load %struct.GrahamVector3*, %struct.GrahamVector3** %m_data, align 4, !tbaa !44
  %1 = load i32, i32* %n.addr, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %0, i32 %1
  ret %struct.GrahamVector3* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI13GrahamVector3E9push_backERKS0_(%class.btAlignedObjectArray.12* %this, %struct.GrahamVector3* nonnull align 4 dereferenceable(24) %_Val) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %_Val.addr = alloca %struct.GrahamVector3*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store %struct.GrahamVector3* %_Val, %struct.GrahamVector3** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !10
  %1 = load i32, i32* %sz, align 4, !tbaa !10
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E8capacityEv(%class.btAlignedObjectArray.12* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI13GrahamVector3E9allocSizeEi(%class.btAlignedObjectArray.12* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E7reserveEi(%class.btAlignedObjectArray.12* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %2 = load %struct.GrahamVector3*, %struct.GrahamVector3** %m_data, align 4, !tbaa !44
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !41
  %arrayidx = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %2, i32 %3
  %4 = bitcast %struct.GrahamVector3* %arrayidx to i8*
  %call5 = call i8* @_ZN9btVector3nwEmPv(i32 24, i8* %4)
  %5 = bitcast i8* %call5 to %struct.GrahamVector3*
  %6 = load %struct.GrahamVector3*, %struct.GrahamVector3** %_Val.addr, align 4, !tbaa !2
  %7 = bitcast %struct.GrahamVector3* %5 to i8*
  %8 = bitcast %struct.GrahamVector3* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 24, i1 false)
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %9 = load i32, i32* %m_size6, align 4, !tbaa !41
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %m_size6, align 4, !tbaa !41
  %10 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.GrahamVector3* @_ZN13GrahamVector3C2ERK9btVector3i(%struct.GrahamVector3* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %org, i32 %orgIndex) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %struct.GrahamVector3*, align 4
  %org.addr = alloca %class.btVector3*, align 4
  %orgIndex.addr = alloca i32, align 4
  store %struct.GrahamVector3* %this, %struct.GrahamVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %org, %class.btVector3** %org.addr, align 4, !tbaa !2
  store i32 %orgIndex, i32* %orgIndex.addr, align 4, !tbaa !10
  %this1 = load %struct.GrahamVector3*, %struct.GrahamVector3** %this.addr, align 4
  %0 = bitcast %struct.GrahamVector3* %this1 to %class.btVector3*
  %1 = load %class.btVector3*, %class.btVector3** %org.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %0 to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !12
  %m_orgIndex = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %this1, i32 0, i32 2
  %4 = load i32, i32* %orgIndex.addr, align 4, !tbaa !10
  store i32 %4, i32* %m_orgIndex, align 4, !tbaa !18
  ret %struct.GrahamVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_Z22GrahamScanConvexHull2DR20btAlignedObjectArrayI13GrahamVector3ES2_RK9btVector3(%class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17) %originalPoints, %class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17) %hull, %class.btVector3* nonnull align 4 dereferenceable(16) %normalAxis) #7 comdat {
entry:
  %originalPoints.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %hull.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %normalAxis.addr = alloca %class.btVector3*, align 4
  %axis0 = alloca %class.btVector3, align 4
  %axis1 = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i6 = alloca i32, align 4
  %projL = alloca float, align 4
  %projR = alloca float, align 4
  %i23 = alloca i32, align 4
  %xvec = alloca %class.btVector3, align 4
  %ar = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %comp = alloca %struct.btAngleCompareFunc, align 4
  %i41 = alloca i32, align 4
  %isConvex = alloca i8, align 1
  %a = alloca %class.btVector3*, align 4
  %b = alloca %class.btVector3*, align 4
  %ref.tmp61 = alloca %class.btVector3, align 4
  %ref.tmp62 = alloca %class.btVector3, align 4
  %ref.tmp63 = alloca %class.btVector3, align 4
  store %class.btAlignedObjectArray.12* %originalPoints, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.12* %hull, %class.btAlignedObjectArray.12** %hull.addr, align 4, !tbaa !2
  store %class.btVector3* %normalAxis, %class.btVector3** %normalAxis.addr, align 4, !tbaa !2
  %0 = bitcast %class.btVector3* %axis0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %axis0)
  %1 = bitcast %class.btVector3* %axis1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #9
  %call1 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %axis1)
  %2 = load %class.btVector3*, %class.btVector3** %normalAxis.addr, align 4, !tbaa !2
  call void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %axis0, %class.btVector3* nonnull align 4 dereferenceable(16) %axis1)
  %3 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4, !tbaa !2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %3)
  %cmp = icmp sle i32 %call2, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  store i32 0, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !10
  %6 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %6)
  %cmp4 = icmp slt i32 %5, %call3
  br i1 %cmp4, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %8 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %hull.addr, align 4, !tbaa !2
  %9 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4, !tbaa !2
  %call5 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %9, i32 0)
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E9push_backERKS0_(%class.btAlignedObjectArray.12* %8, %struct.GrahamVector3* nonnull align 4 dereferenceable(24) %call5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %11 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  store i32 0, i32* %i6, align 4, !tbaa !10
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc19, %if.end
  %12 = load i32, i32* %i6, align 4, !tbaa !10
  %13 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4, !tbaa !2
  %call8 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %13)
  %cmp9 = icmp slt i32 %12, %call8
  br i1 %cmp9, label %for.body11, label %for.cond.cleanup10

for.cond.cleanup10:                               ; preds = %for.cond7
  store i32 5, i32* %cleanup.dest.slot, align 4
  %14 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #9
  br label %for.end21

for.body11:                                       ; preds = %for.cond7
  %15 = bitcast float* %projL to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #9
  %16 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4, !tbaa !2
  %17 = load i32, i32* %i6, align 4, !tbaa !10
  %call12 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %16, i32 %17)
  %18 = bitcast %struct.GrahamVector3* %call12 to %class.btVector3*
  %call13 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %18, %class.btVector3* nonnull align 4 dereferenceable(16) %axis0)
  store float %call13, float* %projL, align 4, !tbaa !14
  %19 = bitcast float* %projR to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #9
  %20 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4, !tbaa !2
  %call14 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %20, i32 0)
  %21 = bitcast %struct.GrahamVector3* %call14 to %class.btVector3*
  %call15 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %21, %class.btVector3* nonnull align 4 dereferenceable(16) %axis0)
  store float %call15, float* %projR, align 4, !tbaa !14
  %22 = load float, float* %projL, align 4, !tbaa !14
  %23 = load float, float* %projR, align 4, !tbaa !14
  %cmp16 = fcmp olt float %22, %23
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %for.body11
  %24 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4, !tbaa !2
  %25 = load i32, i32* %i6, align 4, !tbaa !10
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E4swapEii(%class.btAlignedObjectArray.12* %24, i32 0, i32 %25)
  br label %if.end18

if.end18:                                         ; preds = %if.then17, %for.body11
  %26 = bitcast float* %projR to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #9
  %27 = bitcast float* %projL to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #9
  br label %for.inc19

for.inc19:                                        ; preds = %if.end18
  %28 = load i32, i32* %i6, align 4, !tbaa !10
  %inc20 = add nsw i32 %28, 1
  store i32 %inc20, i32* %i6, align 4, !tbaa !10
  br label %for.cond7

for.end21:                                        ; preds = %for.cond.cleanup10
  %29 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4, !tbaa !2
  %call22 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %29, i32 0)
  %m_angle = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %call22, i32 0, i32 1
  store float 0xC6293E5940000000, float* %m_angle, align 4, !tbaa !45
  %30 = bitcast i32* %i23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #9
  store i32 1, i32* %i23, align 4, !tbaa !10
  br label %for.cond24

for.cond24:                                       ; preds = %for.inc35, %for.end21
  %31 = load i32, i32* %i23, align 4, !tbaa !10
  %32 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4, !tbaa !2
  %call25 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %32)
  %cmp26 = icmp slt i32 %31, %call25
  br i1 %cmp26, label %for.body28, label %for.cond.cleanup27

for.cond.cleanup27:                               ; preds = %for.cond24
  store i32 8, i32* %cleanup.dest.slot, align 4
  %33 = bitcast i32* %i23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #9
  br label %for.end37

for.body28:                                       ; preds = %for.cond24
  %34 = bitcast %class.btVector3* %xvec to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %34) #9
  %35 = bitcast %class.btVector3* %xvec to i8*
  %36 = bitcast %class.btVector3* %axis0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %35, i8* align 4 %36, i32 16, i1 false), !tbaa.struct !12
  %37 = bitcast %class.btVector3* %ar to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %37) #9
  %38 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4, !tbaa !2
  %39 = load i32, i32* %i23, align 4, !tbaa !10
  %call29 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %38, i32 %39)
  %40 = bitcast %struct.GrahamVector3* %call29 to %class.btVector3*
  %41 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4, !tbaa !2
  %call30 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %41, i32 0)
  %42 = bitcast %struct.GrahamVector3* %call30 to %class.btVector3*
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ar, %class.btVector3* nonnull align 4 dereferenceable(16) %40, %class.btVector3* nonnull align 4 dereferenceable(16) %42)
  %43 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %43) #9
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %xvec, %class.btVector3* nonnull align 4 dereferenceable(16) %ar)
  %44 = load %class.btVector3*, %class.btVector3** %normalAxis.addr, align 4, !tbaa !2
  %call31 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %44)
  %call32 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ar)
  %div = fdiv float %call31, %call32
  %45 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4, !tbaa !2
  %46 = load i32, i32* %i23, align 4, !tbaa !10
  %call33 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %45, i32 %46)
  %m_angle34 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %call33, i32 0, i32 1
  store float %div, float* %m_angle34, align 4, !tbaa !45
  %47 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %47) #9
  %48 = bitcast %class.btVector3* %ar to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %48) #9
  %49 = bitcast %class.btVector3* %xvec to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %49) #9
  br label %for.inc35

for.inc35:                                        ; preds = %for.body28
  %50 = load i32, i32* %i23, align 4, !tbaa !10
  %inc36 = add nsw i32 %50, 1
  store i32 %inc36, i32* %i23, align 4, !tbaa !10
  br label %for.cond24

for.end37:                                        ; preds = %for.cond.cleanup27
  %51 = bitcast %struct.btAngleCompareFunc* %comp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %51) #9
  %52 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4, !tbaa !2
  %call38 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %52, i32 0)
  %53 = bitcast %struct.GrahamVector3* %call38 to %class.btVector3*
  %call39 = call %struct.btAngleCompareFunc* @_ZN18btAngleCompareFuncC2ERK9btVector3(%struct.btAngleCompareFunc* %comp, %class.btVector3* nonnull align 4 dereferenceable(16) %53)
  %54 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4, !tbaa !2
  %55 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4, !tbaa !2
  %call40 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %55)
  %sub = sub nsw i32 %call40, 1
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E17quickSortInternalI18btAngleCompareFuncEEvRKT_ii(%class.btAlignedObjectArray.12* %54, %struct.btAngleCompareFunc* nonnull align 4 dereferenceable(16) %comp, i32 1, i32 %sub)
  %56 = bitcast i32* %i41 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #9
  store i32 0, i32* %i41, align 4, !tbaa !10
  br label %for.cond42

for.cond42:                                       ; preds = %for.inc46, %for.end37
  %57 = load i32, i32* %i41, align 4, !tbaa !10
  %cmp43 = icmp slt i32 %57, 2
  br i1 %cmp43, label %for.body44, label %for.end48

for.body44:                                       ; preds = %for.cond42
  %58 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %hull.addr, align 4, !tbaa !2
  %59 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4, !tbaa !2
  %60 = load i32, i32* %i41, align 4, !tbaa !10
  %call45 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %59, i32 %60)
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E9push_backERKS0_(%class.btAlignedObjectArray.12* %58, %struct.GrahamVector3* nonnull align 4 dereferenceable(24) %call45)
  br label %for.inc46

for.inc46:                                        ; preds = %for.body44
  %61 = load i32, i32* %i41, align 4, !tbaa !10
  %inc47 = add nsw i32 %61, 1
  store i32 %inc47, i32* %i41, align 4, !tbaa !10
  br label %for.cond42

for.end48:                                        ; preds = %for.cond42
  br label %for.cond49

for.cond49:                                       ; preds = %for.inc71, %for.end48
  %62 = load i32, i32* %i41, align 4, !tbaa !10
  %63 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4, !tbaa !2
  %call50 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %63)
  %cmp51 = icmp ne i32 %62, %call50
  br i1 %cmp51, label %for.body52, label %for.end73

for.body52:                                       ; preds = %for.cond49
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %isConvex) #9
  store i8 0, i8* %isConvex, align 1, !tbaa !16
  br label %while.cond

while.cond:                                       ; preds = %if.end70, %for.body52
  %64 = load i8, i8* %isConvex, align 1, !tbaa !16, !range !20
  %tobool = trunc i8 %64 to i1
  br i1 %tobool, label %land.end, label %land.rhs

land.rhs:                                         ; preds = %while.cond
  %65 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %hull.addr, align 4, !tbaa !2
  %call53 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %65)
  %cmp54 = icmp sgt i32 %call53, 1
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %66 = phi i1 [ false, %while.cond ], [ %cmp54, %land.rhs ]
  br i1 %66, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %67 = bitcast %class.btVector3** %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #9
  %68 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %hull.addr, align 4, !tbaa !2
  %69 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %hull.addr, align 4, !tbaa !2
  %call55 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %69)
  %sub56 = sub nsw i32 %call55, 2
  %call57 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %68, i32 %sub56)
  %70 = bitcast %struct.GrahamVector3* %call57 to %class.btVector3*
  store %class.btVector3* %70, %class.btVector3** %a, align 4, !tbaa !2
  %71 = bitcast %class.btVector3** %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #9
  %72 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %hull.addr, align 4, !tbaa !2
  %73 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %hull.addr, align 4, !tbaa !2
  %call58 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %73)
  %sub59 = sub nsw i32 %call58, 1
  %call60 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %72, i32 %sub59)
  %74 = bitcast %struct.GrahamVector3* %call60 to %class.btVector3*
  store %class.btVector3* %74, %class.btVector3** %b, align 4, !tbaa !2
  %75 = bitcast %class.btVector3* %ref.tmp61 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %75) #9
  %76 = bitcast %class.btVector3* %ref.tmp62 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %76) #9
  %77 = load %class.btVector3*, %class.btVector3** %a, align 4, !tbaa !2
  %78 = load %class.btVector3*, %class.btVector3** %b, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp62, %class.btVector3* nonnull align 4 dereferenceable(16) %77, %class.btVector3* nonnull align 4 dereferenceable(16) %78)
  %79 = bitcast %class.btVector3* %ref.tmp63 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %79) #9
  %80 = load %class.btVector3*, %class.btVector3** %a, align 4, !tbaa !2
  %81 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4, !tbaa !2
  %82 = load i32, i32* %i41, align 4, !tbaa !10
  %call64 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %81, i32 %82)
  %83 = bitcast %struct.GrahamVector3* %call64 to %class.btVector3*
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp63, %class.btVector3* nonnull align 4 dereferenceable(16) %80, %class.btVector3* nonnull align 4 dereferenceable(16) %83)
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp61, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp62, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp63)
  %84 = load %class.btVector3*, %class.btVector3** %normalAxis.addr, align 4, !tbaa !2
  %call65 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp61, %class.btVector3* nonnull align 4 dereferenceable(16) %84)
  %cmp66 = fcmp ogt float %call65, 0.000000e+00
  %frombool = zext i1 %cmp66 to i8
  store i8 %frombool, i8* %isConvex, align 1, !tbaa !16
  %85 = bitcast %class.btVector3* %ref.tmp63 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %85) #9
  %86 = bitcast %class.btVector3* %ref.tmp62 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %86) #9
  %87 = bitcast %class.btVector3* %ref.tmp61 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %87) #9
  %88 = load i8, i8* %isConvex, align 1, !tbaa !16, !range !20
  %tobool67 = trunc i8 %88 to i1
  br i1 %tobool67, label %if.else, label %if.then68

if.then68:                                        ; preds = %while.body
  %89 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %hull.addr, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E8pop_backEv(%class.btAlignedObjectArray.12* %89)
  br label %if.end70

if.else:                                          ; preds = %while.body
  %90 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %hull.addr, align 4, !tbaa !2
  %91 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4, !tbaa !2
  %92 = load i32, i32* %i41, align 4, !tbaa !10
  %call69 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %91, i32 %92)
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E9push_backERKS0_(%class.btAlignedObjectArray.12* %90, %struct.GrahamVector3* nonnull align 4 dereferenceable(24) %call69)
  br label %if.end70

if.end70:                                         ; preds = %if.else, %if.then68
  %93 = bitcast %class.btVector3** %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #9
  %94 = bitcast %class.btVector3** %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #9
  br label %while.cond

while.end:                                        ; preds = %land.end
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %isConvex) #9
  br label %for.inc71

for.inc71:                                        ; preds = %while.end
  %95 = load i32, i32* %i41, align 4, !tbaa !10
  %inc72 = add nsw i32 %95, 1
  store i32 %inc72, i32* %i41, align 4, !tbaa !10
  br label %for.cond49

for.end73:                                        ; preds = %for.cond49
  %96 = bitcast i32* %i41 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #9
  %97 = bitcast %struct.btAngleCompareFunc* %comp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %97) #9
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end73, %for.end
  %98 = bitcast %class.btVector3* %axis1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %98) #9
  %99 = bitcast %class.btVector3* %axis0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %99) #9
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI6btFaceE9push_backERKS0_(%class.btAlignedObjectArray.0* %this, %struct.btFace* nonnull align 4 dereferenceable(36) %_Val) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Val.addr = alloca %struct.btFace*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store %struct.btFace* %_Val, %struct.btFace** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !10
  %1 = load i32, i32* %sz, align 4, !tbaa !10
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI6btFaceE9allocSizeEi(%class.btAlignedObjectArray.0* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI6btFaceE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %struct.btFace*, %struct.btFace** %m_data, align 4, !tbaa !30
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !33
  %arrayidx = getelementptr inbounds %struct.btFace, %struct.btFace* %2, i32 %3
  %4 = bitcast %struct.btFace* %arrayidx to i8*
  %5 = bitcast i8* %4 to %struct.btFace*
  %6 = load %struct.btFace*, %struct.btFace** %_Val.addr, align 4, !tbaa !2
  %call5 = call %struct.btFace* @_ZN6btFaceC2ERKS_(%struct.btFace* %5, %struct.btFace* nonnull align 4 dereferenceable(36) %6)
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size6, align 4, !tbaa !33
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size6, align 4, !tbaa !33
  %8 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayI13GrahamVector3ED2Ev(%class.btAlignedObjectArray.12* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E5clearEv(%class.btAlignedObjectArray.12* %this1)
  ret %class.btAlignedObjectArray.12* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden %struct.btFace* @_ZN6btFaceC2ERKS_(%struct.btFace* returned %this, %struct.btFace* nonnull align 4 dereferenceable(36) %0) unnamed_addr #7 comdat {
entry:
  %this.addr = alloca %struct.btFace*, align 4
  %.addr = alloca %struct.btFace*, align 4
  store %struct.btFace* %this, %struct.btFace** %this.addr, align 4, !tbaa !2
  store %struct.btFace* %0, %struct.btFace** %.addr, align 4, !tbaa !2
  %this1 = load %struct.btFace*, %struct.btFace** %this.addr, align 4
  %m_indices = getelementptr inbounds %struct.btFace, %struct.btFace* %this1, i32 0, i32 0
  %1 = load %struct.btFace*, %struct.btFace** %.addr, align 4, !tbaa !2
  %m_indices2 = getelementptr inbounds %struct.btFace, %struct.btFace* %1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiEC2ERKS0_(%class.btAlignedObjectArray.3* %m_indices, %class.btAlignedObjectArray.3* nonnull align 4 dereferenceable(17) %m_indices2)
  %m_plane = getelementptr inbounds %struct.btFace, %struct.btFace* %this1, i32 0, i32 1
  %2 = load %struct.btFace*, %struct.btFace** %.addr, align 4
  %m_plane3 = getelementptr inbounds %struct.btFace, %struct.btFace* %2, i32 0, i32 1
  %3 = bitcast [4 x float]* %m_plane to i8*
  %4 = bitcast [4 x float]* %m_plane3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false), !tbaa.struct !12
  ret %struct.btFace* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.3* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.3* %this1)
  ret %class.btAlignedObjectArray.3* %this1
}

declare void @_ZN18btConvexPolyhedron10initializeEv(%class.btConvexPolyhedron*) #1

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI6btFaceED2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI6btFaceE5clearEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btConvexHullComputer* @_ZN20btConvexHullComputerD2Ev(%class.btConvexHullComputer* returned %this) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %class.btConvexHullComputer*, align 4
  store %class.btConvexHullComputer* %this, %class.btConvexHullComputer** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexHullComputer*, %class.btConvexHullComputer** %this.addr, align 4
  %faces = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 2
  %call = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.3* %faces) #9
  %edges = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEED2Ev(%class.btAlignedObjectArray.8* %edges) #9
  %vertices = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 0
  %call3 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* %vertices) #9
  ret %class.btConvexHullComputer* %this1
}

define hidden void @_ZNK23btPolyhedralConvexShape37localGetSupportingVertexWithoutMarginERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btPolyhedralConvexShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %vec0) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btPolyhedralConvexShape*, align 4
  %vec0.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %i = alloca i32, align 4
  %maxDot = alloca float, align 4
  %vec = alloca %class.btVector3, align 4
  %lenSqr = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %rlen = alloca float, align 4
  %vtx = alloca %class.btVector3, align 4
  %newDot = alloca float, align 4
  %k = alloca i32, align 4
  %temp = alloca [128 x %class.btVector3], align 16
  %inner_count = alloca i32, align 4
  store %class.btPolyhedralConvexShape* %this, %class.btPolyhedralConvexShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %vec0, %class.btVector3** %vec0.addr, align 4, !tbaa !2
  %this1 = load %class.btPolyhedralConvexShape*, %class.btPolyhedralConvexShape** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !14
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !14
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !14
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #9
  %6 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %7 = bitcast float* %maxDot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  store float 0xC3ABC16D60000000, float* %maxDot, align 4, !tbaa !14
  %8 = bitcast %class.btVector3* %vec to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #9
  %9 = load %class.btVector3*, %class.btVector3** %vec0.addr, align 4, !tbaa !2
  %10 = bitcast %class.btVector3* %vec to i8*
  %11 = bitcast %class.btVector3* %9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !12
  %12 = bitcast float* %lenSqr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #9
  %call4 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %vec)
  store float %call4, float* %lenSqr, align 4, !tbaa !14
  %13 = load float, float* %lenSqr, align 4, !tbaa !14
  %cmp = fcmp olt float %13, 0x3F1A36E2E0000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %14 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #9
  store float 1.000000e+00, float* %ref.tmp5, align 4, !tbaa !14
  %15 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #9
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !14
  %16 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #9
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !14
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %vec, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %17 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  %18 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #9
  %19 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #9
  br label %if.end

if.else:                                          ; preds = %entry
  %20 = bitcast float* %rlen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #9
  %21 = load float, float* %lenSqr, align 4, !tbaa !14
  %call8 = call float @_Z6btSqrtf(float %21)
  %div = fdiv float 1.000000e+00, %call8
  store float %div, float* %rlen, align 4, !tbaa !14
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %vec, float* nonnull align 4 dereferenceable(4) %rlen)
  %22 = bitcast float* %rlen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #9
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %23 = bitcast %class.btVector3* %vtx to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %23) #9
  %call10 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vtx)
  %24 = bitcast float* %newDot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #9
  %25 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #9
  store i32 0, i32* %k, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc32, %if.end
  %26 = load i32, i32* %k, align 4, !tbaa !10
  %27 = bitcast %class.btPolyhedralConvexShape* %this1 to i32 (%class.btPolyhedralConvexShape*)***
  %vtable = load i32 (%class.btPolyhedralConvexShape*)**, i32 (%class.btPolyhedralConvexShape*)*** %27, align 4, !tbaa !6
  %vfn = getelementptr inbounds i32 (%class.btPolyhedralConvexShape*)*, i32 (%class.btPolyhedralConvexShape*)** %vtable, i64 24
  %28 = load i32 (%class.btPolyhedralConvexShape*)*, i32 (%class.btPolyhedralConvexShape*)** %vfn, align 4
  %call11 = call i32 %28(%class.btPolyhedralConvexShape* %this1)
  %cmp12 = icmp slt i32 %26, %call11
  br i1 %cmp12, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %29 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #9
  br label %for.end33

for.body:                                         ; preds = %for.cond
  %30 = bitcast [128 x %class.btVector3]* %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 2048, i8* %30) #9
  %array.begin = getelementptr inbounds [128 x %class.btVector3], [128 x %class.btVector3]* %temp, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 128
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %for.body
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %for.body ], [ %arrayctor.next, %arrayctor.loop ]
  %call13 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %31 = bitcast i32* %inner_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #9
  %32 = bitcast %class.btPolyhedralConvexShape* %this1 to i32 (%class.btPolyhedralConvexShape*)***
  %vtable14 = load i32 (%class.btPolyhedralConvexShape*)**, i32 (%class.btPolyhedralConvexShape*)*** %32, align 4, !tbaa !6
  %vfn15 = getelementptr inbounds i32 (%class.btPolyhedralConvexShape*)*, i32 (%class.btPolyhedralConvexShape*)** %vtable14, i64 24
  %33 = load i32 (%class.btPolyhedralConvexShape*)*, i32 (%class.btPolyhedralConvexShape*)** %vfn15, align 4
  %call16 = call i32 %33(%class.btPolyhedralConvexShape* %this1)
  %34 = load i32, i32* %k, align 4, !tbaa !10
  %sub = sub nsw i32 %call16, %34
  %cmp17 = icmp slt i32 %sub, 128
  br i1 %cmp17, label %cond.true, label %cond.false

cond.true:                                        ; preds = %arrayctor.cont
  %35 = bitcast %class.btPolyhedralConvexShape* %this1 to i32 (%class.btPolyhedralConvexShape*)***
  %vtable18 = load i32 (%class.btPolyhedralConvexShape*)**, i32 (%class.btPolyhedralConvexShape*)*** %35, align 4, !tbaa !6
  %vfn19 = getelementptr inbounds i32 (%class.btPolyhedralConvexShape*)*, i32 (%class.btPolyhedralConvexShape*)** %vtable18, i64 24
  %36 = load i32 (%class.btPolyhedralConvexShape*)*, i32 (%class.btPolyhedralConvexShape*)** %vfn19, align 4
  %call20 = call i32 %36(%class.btPolyhedralConvexShape* %this1)
  %37 = load i32, i32* %k, align 4, !tbaa !10
  %sub21 = sub nsw i32 %call20, %37
  br label %cond.end

cond.false:                                       ; preds = %arrayctor.cont
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub21, %cond.true ], [ 128, %cond.false ]
  store i32 %cond, i32* %inner_count, align 4, !tbaa !10
  store i32 0, i32* %i, align 4, !tbaa !10
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc, %cond.end
  %38 = load i32, i32* %i, align 4, !tbaa !10
  %39 = load i32, i32* %inner_count, align 4, !tbaa !10
  %cmp23 = icmp slt i32 %38, %39
  br i1 %cmp23, label %for.body24, label %for.end

for.body24:                                       ; preds = %for.cond22
  %40 = load i32, i32* %i, align 4, !tbaa !10
  %41 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds [128 x %class.btVector3], [128 x %class.btVector3]* %temp, i32 0, i32 %41
  %42 = bitcast %class.btPolyhedralConvexShape* %this1 to void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)***
  %vtable25 = load void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)**, void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)*** %42, align 4, !tbaa !6
  %vfn26 = getelementptr inbounds void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)*, void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)** %vtable25, i64 27
  %43 = load void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)*, void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)** %vfn26, align 4
  call void %43(%class.btPolyhedralConvexShape* %this1, i32 %40, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  br label %for.inc

for.inc:                                          ; preds = %for.body24
  %44 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add nsw i32 %44, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond22

for.end:                                          ; preds = %for.cond22
  %arraydecay = getelementptr inbounds [128 x %class.btVector3], [128 x %class.btVector3]* %temp, i32 0, i32 0
  %45 = load i32, i32* %inner_count, align 4, !tbaa !10
  %call27 = call i32 @_ZNK9btVector36maxDotEPKS_lRf(%class.btVector3* %vec, %class.btVector3* %arraydecay, i32 %45, float* nonnull align 4 dereferenceable(4) %newDot)
  store i32 %call27, i32* %i, align 4, !tbaa !10
  %46 = load float, float* %newDot, align 4, !tbaa !14
  %47 = load float, float* %maxDot, align 4, !tbaa !14
  %cmp28 = fcmp ogt float %46, %47
  br i1 %cmp28, label %if.then29, label %if.end31

if.then29:                                        ; preds = %for.end
  %48 = load float, float* %newDot, align 4, !tbaa !14
  store float %48, float* %maxDot, align 4, !tbaa !14
  %49 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx30 = getelementptr inbounds [128 x %class.btVector3], [128 x %class.btVector3]* %temp, i32 0, i32 %49
  %50 = bitcast %class.btVector3* %agg.result to i8*
  %51 = bitcast %class.btVector3* %arrayidx30 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %50, i8* align 16 %51, i32 16, i1 false), !tbaa.struct !12
  br label %if.end31

if.end31:                                         ; preds = %if.then29, %for.end
  %52 = bitcast i32* %inner_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #9
  %53 = bitcast [128 x %class.btVector3]* %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 2048, i8* %53) #9
  br label %for.inc32

for.inc32:                                        ; preds = %if.end31
  %54 = load i32, i32* %k, align 4, !tbaa !10
  %add = add nsw i32 %54, 128
  store i32 %add, i32* %k, align 4, !tbaa !10
  br label %for.cond

for.end33:                                        ; preds = %for.cond.cleanup
  %55 = bitcast float* %newDot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #9
  %56 = bitcast %class.btVector3* %vtx to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %56) #9
  %57 = bitcast float* %lenSqr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #9
  %58 = bitcast %class.btVector3* %vec to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %58) #9
  %59 = bitcast float* %maxDot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #9
  %60 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !14
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !14
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !14
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !14
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !14
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !14
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !14
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #6 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !14
  %0 = load float, float* %y.addr, align 4, !tbaa !14
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !14
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !14
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !14
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !14
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !14
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !14
  %6 = load float*, float** %s.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !14
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !14
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !14
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK9btVector36maxDotEPKS_lRf(%class.btVector3* %this, %class.btVector3* %array, i32 %array_count, float* nonnull align 4 dereferenceable(4) %dotOut) #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %array.addr = alloca %class.btVector3*, align 4
  %array_count.addr = alloca i32, align 4
  %dotOut.addr = alloca float*, align 4
  %maxDot = alloca float, align 4
  %i = alloca i32, align 4
  %ptIndex = alloca i32, align 4
  %dot = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %array, %class.btVector3** %array.addr, align 4, !tbaa !2
  store i32 %array_count, i32* %array_count.addr, align 4, !tbaa !21
  store float* %dotOut, float** %dotOut.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %maxDot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store float 0xC7EFFFFFE0000000, float* %maxDot, align 4, !tbaa !14
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store i32 0, i32* %i, align 4, !tbaa !10
  %2 = bitcast i32* %ptIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store i32 -1, i32* %ptIndex, align 4, !tbaa !10
  store i32 0, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4, !tbaa !10
  %4 = load i32, i32* %array_count.addr, align 4, !tbaa !21
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = bitcast float* %dot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %array.addr, align 4, !tbaa !2
  %7 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 %7
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  store float %call, float* %dot, align 4, !tbaa !14
  %8 = load float, float* %dot, align 4, !tbaa !14
  %9 = load float, float* %maxDot, align 4, !tbaa !14
  %cmp2 = fcmp ogt float %8, %9
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %10 = load float, float* %dot, align 4, !tbaa !14
  store float %10, float* %maxDot, align 4, !tbaa !14
  %11 = load i32, i32* %i, align 4, !tbaa !10
  store i32 %11, i32* %ptIndex, align 4, !tbaa !10
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %12 = bitcast float* %dot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %13 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %14 = load float, float* %maxDot, align 4, !tbaa !14
  %15 = load float*, float** %dotOut.addr, align 4, !tbaa !2
  store float %14, float* %15, align 4, !tbaa !14
  %16 = load i32, i32* %ptIndex, align 4, !tbaa !10
  %17 = bitcast i32* %ptIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  %18 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #9
  %19 = bitcast float* %maxDot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #9
  ret i32 %16
}

define hidden void @_ZNK23btPolyhedralConvexShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i(%class.btPolyhedralConvexShape* %this, %class.btVector3* %vectors, %class.btVector3* %supportVerticesOut, i32 %numVectors) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btPolyhedralConvexShape*, align 4
  %vectors.addr = alloca %class.btVector3*, align 4
  %supportVerticesOut.addr = alloca %class.btVector3*, align 4
  %numVectors.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %vtx = alloca %class.btVector3, align 4
  %newDot = alloca float, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %vec = alloca %class.btVector3*, align 4
  %k = alloca i32, align 4
  %temp = alloca [128 x %class.btVector3], align 16
  %inner_count = alloca i32, align 4
  store %class.btPolyhedralConvexShape* %this, %class.btPolyhedralConvexShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %vectors, %class.btVector3** %vectors.addr, align 4, !tbaa !2
  store %class.btVector3* %supportVerticesOut, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  store i32 %numVectors, i32* %numVectors.addr, align 4, !tbaa !10
  %this1 = load %class.btPolyhedralConvexShape*, %class.btPolyhedralConvexShape** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = bitcast %class.btVector3* %vtx to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #9
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vtx)
  %2 = bitcast float* %newDot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store i32 0, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4, !tbaa !10
  %4 = load i32, i32* %numVectors.addr, align 4, !tbaa !10
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  %6 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 %6
  %call2 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 3
  store float 0xC3ABC16D60000000, float* %arrayidx3, align 4, !tbaa !14
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %8 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  store i32 0, i32* %j, align 4, !tbaa !10
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc43, %for.end
  %9 = load i32, i32* %j, align 4, !tbaa !10
  %10 = load i32, i32* %numVectors.addr, align 4, !tbaa !10
  %cmp5 = icmp slt i32 %9, %10
  br i1 %cmp5, label %for.body6, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond4
  store i32 5, i32* %cleanup.dest.slot, align 4
  %11 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  br label %for.end45

for.body6:                                        ; preds = %for.cond4
  %12 = bitcast %class.btVector3** %vec to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #9
  %13 = load %class.btVector3*, %class.btVector3** %vectors.addr, align 4, !tbaa !2
  %14 = load i32, i32* %j, align 4, !tbaa !10
  %arrayidx7 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 %14
  store %class.btVector3* %arrayidx7, %class.btVector3** %vec, align 4, !tbaa !2
  %15 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #9
  store i32 0, i32* %k, align 4, !tbaa !10
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc41, %for.body6
  %16 = load i32, i32* %k, align 4, !tbaa !10
  %17 = bitcast %class.btPolyhedralConvexShape* %this1 to i32 (%class.btPolyhedralConvexShape*)***
  %vtable = load i32 (%class.btPolyhedralConvexShape*)**, i32 (%class.btPolyhedralConvexShape*)*** %17, align 4, !tbaa !6
  %vfn = getelementptr inbounds i32 (%class.btPolyhedralConvexShape*)*, i32 (%class.btPolyhedralConvexShape*)** %vtable, i64 24
  %18 = load i32 (%class.btPolyhedralConvexShape*)*, i32 (%class.btPolyhedralConvexShape*)** %vfn, align 4
  %call9 = call i32 %18(%class.btPolyhedralConvexShape* %this1)
  %cmp10 = icmp slt i32 %16, %call9
  br i1 %cmp10, label %for.body12, label %for.cond.cleanup11

for.cond.cleanup11:                               ; preds = %for.cond8
  store i32 8, i32* %cleanup.dest.slot, align 4
  %19 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #9
  br label %for.end42

for.body12:                                       ; preds = %for.cond8
  %20 = bitcast [128 x %class.btVector3]* %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 2048, i8* %20) #9
  %array.begin = getelementptr inbounds [128 x %class.btVector3], [128 x %class.btVector3]* %temp, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 128
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %for.body12
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %for.body12 ], [ %arrayctor.next, %arrayctor.loop ]
  %call13 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %21 = bitcast i32* %inner_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #9
  %22 = bitcast %class.btPolyhedralConvexShape* %this1 to i32 (%class.btPolyhedralConvexShape*)***
  %vtable14 = load i32 (%class.btPolyhedralConvexShape*)**, i32 (%class.btPolyhedralConvexShape*)*** %22, align 4, !tbaa !6
  %vfn15 = getelementptr inbounds i32 (%class.btPolyhedralConvexShape*)*, i32 (%class.btPolyhedralConvexShape*)** %vtable14, i64 24
  %23 = load i32 (%class.btPolyhedralConvexShape*)*, i32 (%class.btPolyhedralConvexShape*)** %vfn15, align 4
  %call16 = call i32 %23(%class.btPolyhedralConvexShape* %this1)
  %24 = load i32, i32* %k, align 4, !tbaa !10
  %sub = sub nsw i32 %call16, %24
  %cmp17 = icmp slt i32 %sub, 128
  br i1 %cmp17, label %cond.true, label %cond.false

cond.true:                                        ; preds = %arrayctor.cont
  %25 = bitcast %class.btPolyhedralConvexShape* %this1 to i32 (%class.btPolyhedralConvexShape*)***
  %vtable18 = load i32 (%class.btPolyhedralConvexShape*)**, i32 (%class.btPolyhedralConvexShape*)*** %25, align 4, !tbaa !6
  %vfn19 = getelementptr inbounds i32 (%class.btPolyhedralConvexShape*)*, i32 (%class.btPolyhedralConvexShape*)** %vtable18, i64 24
  %26 = load i32 (%class.btPolyhedralConvexShape*)*, i32 (%class.btPolyhedralConvexShape*)** %vfn19, align 4
  %call20 = call i32 %26(%class.btPolyhedralConvexShape* %this1)
  %27 = load i32, i32* %k, align 4, !tbaa !10
  %sub21 = sub nsw i32 %call20, %27
  br label %cond.end

cond.false:                                       ; preds = %arrayctor.cont
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub21, %cond.true ], [ 128, %cond.false ]
  store i32 %cond, i32* %inner_count, align 4, !tbaa !10
  store i32 0, i32* %i, align 4, !tbaa !10
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc28, %cond.end
  %28 = load i32, i32* %i, align 4, !tbaa !10
  %29 = load i32, i32* %inner_count, align 4, !tbaa !10
  %cmp23 = icmp slt i32 %28, %29
  br i1 %cmp23, label %for.body24, label %for.end30

for.body24:                                       ; preds = %for.cond22
  %30 = load i32, i32* %i, align 4, !tbaa !10
  %31 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx25 = getelementptr inbounds [128 x %class.btVector3], [128 x %class.btVector3]* %temp, i32 0, i32 %31
  %32 = bitcast %class.btPolyhedralConvexShape* %this1 to void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)***
  %vtable26 = load void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)**, void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)*** %32, align 4, !tbaa !6
  %vfn27 = getelementptr inbounds void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)*, void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)** %vtable26, i64 27
  %33 = load void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)*, void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)** %vfn27, align 4
  call void %33(%class.btPolyhedralConvexShape* %this1, i32 %30, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx25)
  br label %for.inc28

for.inc28:                                        ; preds = %for.body24
  %34 = load i32, i32* %i, align 4, !tbaa !10
  %inc29 = add nsw i32 %34, 1
  store i32 %inc29, i32* %i, align 4, !tbaa !10
  br label %for.cond22

for.end30:                                        ; preds = %for.cond22
  %35 = load %class.btVector3*, %class.btVector3** %vec, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [128 x %class.btVector3], [128 x %class.btVector3]* %temp, i32 0, i32 0
  %36 = load i32, i32* %inner_count, align 4, !tbaa !10
  %call31 = call i32 @_ZNK9btVector36maxDotEPKS_lRf(%class.btVector3* %35, %class.btVector3* %arraydecay, i32 %36, float* nonnull align 4 dereferenceable(4) %newDot)
  store i32 %call31, i32* %i, align 4, !tbaa !10
  %37 = load float, float* %newDot, align 4, !tbaa !14
  %38 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  %39 = load i32, i32* %j, align 4, !tbaa !10
  %arrayidx32 = getelementptr inbounds %class.btVector3, %class.btVector3* %38, i32 %39
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx32)
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 3
  %40 = load float, float* %arrayidx34, align 4, !tbaa !14
  %cmp35 = fcmp ogt float %37, %40
  br i1 %cmp35, label %if.then, label %if.end

if.then:                                          ; preds = %for.end30
  %41 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx36 = getelementptr inbounds [128 x %class.btVector3], [128 x %class.btVector3]* %temp, i32 0, i32 %41
  %42 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  %43 = load i32, i32* %j, align 4, !tbaa !10
  %arrayidx37 = getelementptr inbounds %class.btVector3, %class.btVector3* %42, i32 %43
  %44 = bitcast %class.btVector3* %arrayidx37 to i8*
  %45 = bitcast %class.btVector3* %arrayidx36 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %44, i8* align 16 %45, i32 16, i1 false), !tbaa.struct !12
  %46 = load float, float* %newDot, align 4, !tbaa !14
  %47 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  %48 = load i32, i32* %j, align 4, !tbaa !10
  %arrayidx38 = getelementptr inbounds %class.btVector3, %class.btVector3* %47, i32 %48
  %call39 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx38)
  %arrayidx40 = getelementptr inbounds float, float* %call39, i32 3
  store float %46, float* %arrayidx40, align 4, !tbaa !14
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end30
  %49 = bitcast i32* %inner_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #9
  %50 = bitcast [128 x %class.btVector3]* %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 2048, i8* %50) #9
  br label %for.inc41

for.inc41:                                        ; preds = %if.end
  %51 = load i32, i32* %k, align 4, !tbaa !10
  %add = add nsw i32 %51, 128
  store i32 %add, i32* %k, align 4, !tbaa !10
  br label %for.cond8

for.end42:                                        ; preds = %for.cond.cleanup11
  %52 = bitcast %class.btVector3** %vec to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #9
  br label %for.inc43

for.inc43:                                        ; preds = %for.end42
  %53 = load i32, i32* %j, align 4, !tbaa !10
  %inc44 = add nsw i32 %53, 1
  store i32 %inc44, i32* %j, align 4, !tbaa !10
  br label %for.cond4

for.end45:                                        ; preds = %for.cond.cleanup
  %54 = bitcast float* %newDot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #9
  %55 = bitcast %class.btVector3* %vtx to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %55) #9
  %56 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #9
  ret void
}

define hidden void @_ZNK23btPolyhedralConvexShape21calculateLocalInertiaEfR9btVector3(%class.btPolyhedralConvexShape* %this, float %mass, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btPolyhedralConvexShape*, align 4
  %mass.addr = alloca float, align 4
  %inertia.addr = alloca %class.btVector3*, align 4
  %margin = alloca float, align 4
  %ident = alloca %class.btTransform, align 4
  %aabbMin = alloca %class.btVector3, align 4
  %aabbMax = alloca %class.btVector3, align 4
  %halfExtents = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca float, align 4
  %lx = alloca float, align 4
  %ly = alloca float, align 4
  %lz = alloca float, align 4
  %x2 = alloca float, align 4
  %y2 = alloca float, align 4
  %z2 = alloca float, align 4
  %scaledmass = alloca float, align 4
  %ref.tmp19 = alloca %class.btVector3, align 4
  %ref.tmp20 = alloca %class.btVector3, align 4
  %ref.tmp21 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  store %class.btPolyhedralConvexShape* %this, %class.btPolyhedralConvexShape** %this.addr, align 4, !tbaa !2
  store float %mass, float* %mass.addr, align 4, !tbaa !14
  store %class.btVector3* %inertia, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %this1 = load %class.btPolyhedralConvexShape*, %class.btPolyhedralConvexShape** %this.addr, align 4
  %0 = bitcast float* %margin to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = bitcast %class.btPolyhedralConvexShape* %this1 to %class.btConvexInternalShape*
  %2 = bitcast %class.btConvexInternalShape* %1 to float (%class.btConvexInternalShape*)***
  %vtable = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %2, align 4, !tbaa !6
  %vfn = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable, i64 12
  %3 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn, align 4
  %call = call float %3(%class.btConvexInternalShape* %1)
  store float %call, float* %margin, align 4, !tbaa !14
  %4 = bitcast %class.btTransform* %ident to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %4) #9
  %call2 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %ident)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %ident)
  %5 = bitcast %class.btVector3* %aabbMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #9
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin)
  %6 = bitcast %class.btVector3* %aabbMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #9
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax)
  %7 = bitcast %class.btPolyhedralConvexShape* %this1 to %class.btConvexInternalShape*
  %8 = bitcast %class.btConvexInternalShape* %7 to void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable5 = load void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %8, align 4, !tbaa !6
  %vfn6 = getelementptr inbounds void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable5, i64 2
  %9 = load void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn6, align 4
  call void %9(%class.btConvexInternalShape* %7, %class.btTransform* nonnull align 4 dereferenceable(64) %ident, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax)
  %10 = bitcast %class.btVector3* %halfExtents to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #9
  %11 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #9
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin)
  %12 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #9
  store float 5.000000e-01, float* %ref.tmp7, align 4, !tbaa !14
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %halfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %13 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  %14 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %14) #9
  %15 = bitcast float* %lx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #9
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %halfExtents)
  %16 = load float, float* %call8, align 4, !tbaa !14
  %17 = load float, float* %margin, align 4, !tbaa !14
  %add = fadd float %16, %17
  %mul = fmul float 2.000000e+00, %add
  store float %mul, float* %lx, align 4, !tbaa !14
  %18 = bitcast float* %ly to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #9
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %halfExtents)
  %19 = load float, float* %call9, align 4, !tbaa !14
  %20 = load float, float* %margin, align 4, !tbaa !14
  %add10 = fadd float %19, %20
  %mul11 = fmul float 2.000000e+00, %add10
  store float %mul11, float* %ly, align 4, !tbaa !14
  %21 = bitcast float* %lz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #9
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %halfExtents)
  %22 = load float, float* %call12, align 4, !tbaa !14
  %23 = load float, float* %margin, align 4, !tbaa !14
  %add13 = fadd float %22, %23
  %mul14 = fmul float 2.000000e+00, %add13
  store float %mul14, float* %lz, align 4, !tbaa !14
  %24 = bitcast float* %x2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #9
  %25 = load float, float* %lx, align 4, !tbaa !14
  %26 = load float, float* %lx, align 4, !tbaa !14
  %mul15 = fmul float %25, %26
  store float %mul15, float* %x2, align 4, !tbaa !14
  %27 = bitcast float* %y2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #9
  %28 = load float, float* %ly, align 4, !tbaa !14
  %29 = load float, float* %ly, align 4, !tbaa !14
  %mul16 = fmul float %28, %29
  store float %mul16, float* %y2, align 4, !tbaa !14
  %30 = bitcast float* %z2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #9
  %31 = load float, float* %lz, align 4, !tbaa !14
  %32 = load float, float* %lz, align 4, !tbaa !14
  %mul17 = fmul float %31, %32
  store float %mul17, float* %z2, align 4, !tbaa !14
  %33 = bitcast float* %scaledmass to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #9
  %34 = load float, float* %mass.addr, align 4, !tbaa !14
  %mul18 = fmul float %34, 0x3FB5555540000000
  store float %mul18, float* %scaledmass, align 4, !tbaa !14
  %35 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %35) #9
  %36 = bitcast %class.btVector3* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %36) #9
  %37 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #9
  %38 = load float, float* %y2, align 4, !tbaa !14
  %39 = load float, float* %z2, align 4, !tbaa !14
  %add22 = fadd float %38, %39
  store float %add22, float* %ref.tmp21, align 4, !tbaa !14
  %40 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #9
  %41 = load float, float* %x2, align 4, !tbaa !14
  %42 = load float, float* %z2, align 4, !tbaa !14
  %add24 = fadd float %41, %42
  store float %add24, float* %ref.tmp23, align 4, !tbaa !14
  %43 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #9
  %44 = load float, float* %x2, align 4, !tbaa !14
  %45 = load float, float* %y2, align 4, !tbaa !14
  %add26 = fadd float %44, %45
  store float %add26, float* %ref.tmp25, align 4, !tbaa !14
  %call27 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp21, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp25)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp19, float* nonnull align 4 dereferenceable(4) %scaledmass, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp20)
  %46 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %47 = bitcast %class.btVector3* %46 to i8*
  %48 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %47, i8* align 4 %48, i32 16, i1 false), !tbaa.struct !12
  %49 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #9
  %50 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #9
  %51 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #9
  %52 = bitcast %class.btVector3* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %52) #9
  %53 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %53) #9
  %54 = bitcast float* %scaledmass to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #9
  %55 = bitcast float* %z2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #9
  %56 = bitcast float* %y2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #9
  %57 = bitcast float* %x2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #9
  %58 = bitcast float* %lz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #9
  %59 = bitcast float* %ly to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #9
  %60 = bitcast float* %lx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #9
  %61 = bitcast %class.btVector3* %halfExtents to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %61) #9
  %62 = bitcast %class.btVector3* %aabbMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %62) #9
  %63 = bitcast %class.btVector3* %aabbMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %63) #9
  %64 = bitcast %class.btTransform* %ident to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %64) #9
  %65 = bitcast float* %margin to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #9
  ret void
}

define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

define linkonce_odr hidden void @_ZN11btTransform11setIdentityEv(%class.btTransform* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !14
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !14
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !14
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_origin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #7 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !14
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !14
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !14
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !14
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !14
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !14
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !14
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !14
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !14
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #7 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

define hidden void @_ZN34btPolyhedralConvexAabbCachingShape15setLocalScalingERK9btVector3(%class.btPolyhedralConvexAabbCachingShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %scaling) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btPolyhedralConvexAabbCachingShape*, align 4
  %scaling.addr = alloca %class.btVector3*, align 4
  store %class.btPolyhedralConvexAabbCachingShape* %this, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %scaling, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  %this1 = load %class.btPolyhedralConvexAabbCachingShape*, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4
  %0 = bitcast %class.btPolyhedralConvexAabbCachingShape* %this1 to %class.btConvexInternalShape*
  %1 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  call void @_ZN21btConvexInternalShape15setLocalScalingERK9btVector3(%class.btConvexInternalShape* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  call void @_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv(%class.btPolyhedralConvexAabbCachingShape* %this1)
  ret void
}

declare void @_ZN21btConvexInternalShape15setLocalScalingERK9btVector3(%class.btConvexInternalShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

define hidden void @_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv(%class.btPolyhedralConvexAabbCachingShape* %this) #0 {
entry:
  %this.addr = alloca %class.btPolyhedralConvexAabbCachingShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  %ref.tmp22 = alloca float, align 4
  %_supporting = alloca [6 x %class.btVector3], align 16
  %ref.tmp24 = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp26 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp30 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp34 = alloca float, align 4
  %ref.tmp35 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp39 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  %ref.tmp43 = alloca float, align 4
  %ref.tmp44 = alloca float, align 4
  %ref.tmp45 = alloca float, align 4
  %ref.tmp48 = alloca float, align 4
  %ref.tmp49 = alloca float, align 4
  %ref.tmp50 = alloca float, align 4
  %i = alloca i32, align 4
  store %class.btPolyhedralConvexAabbCachingShape* %this, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPolyhedralConvexAabbCachingShape*, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4
  %m_isLocalAabbValid = getelementptr inbounds %class.btPolyhedralConvexAabbCachingShape, %class.btPolyhedralConvexAabbCachingShape* %this1, i32 0, i32 3
  store i8 1, i8* %m_isLocalAabbValid, align 4, !tbaa !46
  %0 = load atomic i8, i8* bitcast (i32* @_ZGVZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions to i8*) acquire, align 4
  %1 = and i8 %0, 1
  %guard.uninitialized = icmp eq i8 %1, 0
  br i1 %guard.uninitialized, label %init.check, label %init.end, !prof !49

init.check:                                       ; preds = %entry
  %2 = call i32 @__cxa_guard_acquire(i32* @_ZGVZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions) #9
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %init, label %init.end

init:                                             ; preds = %init.check
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !14
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !14
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !14
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([6 x %class.btVector3], [6 x %class.btVector3]* @_ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions, i32 0, i32 0), float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !14
  %7 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  store float 1.000000e+00, float* %ref.tmp5, align 4, !tbaa !14
  %8 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !14
  %call7 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([6 x %class.btVector3], [6 x %class.btVector3]* @_ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions, i32 0, i32 1), float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %9 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  store float 0.000000e+00, float* %ref.tmp8, align 4, !tbaa !14
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  store float 0.000000e+00, float* %ref.tmp9, align 4, !tbaa !14
  %11 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  store float 1.000000e+00, float* %ref.tmp10, align 4, !tbaa !14
  %call11 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([6 x %class.btVector3], [6 x %class.btVector3]* @_ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions, i32 0, i32 2), float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp10)
  %12 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #9
  store float -1.000000e+00, float* %ref.tmp12, align 4, !tbaa !14
  %13 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  store float 0.000000e+00, float* %ref.tmp13, align 4, !tbaa !14
  %14 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #9
  store float 0.000000e+00, float* %ref.tmp14, align 4, !tbaa !14
  %call15 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([6 x %class.btVector3], [6 x %class.btVector3]* @_ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions, i32 0, i32 3), float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp14)
  %15 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #9
  store float 0.000000e+00, float* %ref.tmp16, align 4, !tbaa !14
  %16 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #9
  store float -1.000000e+00, float* %ref.tmp17, align 4, !tbaa !14
  %17 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #9
  store float 0.000000e+00, float* %ref.tmp18, align 4, !tbaa !14
  %call19 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([6 x %class.btVector3], [6 x %class.btVector3]* @_ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions, i32 0, i32 4), float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp18)
  %18 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #9
  store float 0.000000e+00, float* %ref.tmp20, align 4, !tbaa !14
  %19 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #9
  store float 0.000000e+00, float* %ref.tmp21, align 4, !tbaa !14
  %20 = bitcast float* %ref.tmp22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #9
  store float -1.000000e+00, float* %ref.tmp22, align 4, !tbaa !14
  %call23 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([6 x %class.btVector3], [6 x %class.btVector3]* @_ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions, i32 0, i32 5), float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp21, float* nonnull align 4 dereferenceable(4) %ref.tmp22)
  %21 = bitcast float* %ref.tmp22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #9
  %22 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #9
  %23 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #9
  %24 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #9
  %25 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #9
  %26 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #9
  %27 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #9
  %28 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #9
  %29 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #9
  %30 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #9
  %31 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #9
  %32 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #9
  %33 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #9
  %34 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #9
  %35 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #9
  %36 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #9
  %37 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #9
  %38 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #9
  %39 = call {}* @llvm.invariant.start.p0i8(i64 96, i8* bitcast ([6 x %class.btVector3]* @_ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions to i8*))
  call void @__cxa_guard_release(i32* @_ZGVZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions) #9
  br label %init.end

init.end:                                         ; preds = %init, %init.check, %entry
  %40 = bitcast [6 x %class.btVector3]* %_supporting to i8*
  call void @llvm.lifetime.start.p0i8(i64 96, i8* %40) #9
  %arrayinit.begin = getelementptr inbounds [6 x %class.btVector3], [6 x %class.btVector3]* %_supporting, i32 0, i32 0
  %41 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #9
  store float 0.000000e+00, float* %ref.tmp24, align 4, !tbaa !14
  %42 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #9
  store float 0.000000e+00, float* %ref.tmp25, align 4, !tbaa !14
  %43 = bitcast float* %ref.tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #9
  store float 0.000000e+00, float* %ref.tmp26, align 4, !tbaa !14
  %call27 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.begin, float* nonnull align 4 dereferenceable(4) %ref.tmp24, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp26)
  %arrayinit.element = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.begin, i32 1
  %44 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #9
  store float 0.000000e+00, float* %ref.tmp28, align 4, !tbaa !14
  %45 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #9
  store float 0.000000e+00, float* %ref.tmp29, align 4, !tbaa !14
  %46 = bitcast float* %ref.tmp30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #9
  store float 0.000000e+00, float* %ref.tmp30, align 4, !tbaa !14
  %call31 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp30)
  %arrayinit.element32 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element, i32 1
  %47 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #9
  store float 0.000000e+00, float* %ref.tmp33, align 4, !tbaa !14
  %48 = bitcast float* %ref.tmp34 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #9
  store float 0.000000e+00, float* %ref.tmp34, align 4, !tbaa !14
  %49 = bitcast float* %ref.tmp35 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #9
  store float 0.000000e+00, float* %ref.tmp35, align 4, !tbaa !14
  %call36 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element32, float* nonnull align 4 dereferenceable(4) %ref.tmp33, float* nonnull align 4 dereferenceable(4) %ref.tmp34, float* nonnull align 4 dereferenceable(4) %ref.tmp35)
  %arrayinit.element37 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element32, i32 1
  %50 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #9
  store float 0.000000e+00, float* %ref.tmp38, align 4, !tbaa !14
  %51 = bitcast float* %ref.tmp39 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #9
  store float 0.000000e+00, float* %ref.tmp39, align 4, !tbaa !14
  %52 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #9
  store float 0.000000e+00, float* %ref.tmp40, align 4, !tbaa !14
  %call41 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element37, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp39, float* nonnull align 4 dereferenceable(4) %ref.tmp40)
  %arrayinit.element42 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element37, i32 1
  %53 = bitcast float* %ref.tmp43 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #9
  store float 0.000000e+00, float* %ref.tmp43, align 4, !tbaa !14
  %54 = bitcast float* %ref.tmp44 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #9
  store float 0.000000e+00, float* %ref.tmp44, align 4, !tbaa !14
  %55 = bitcast float* %ref.tmp45 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #9
  store float 0.000000e+00, float* %ref.tmp45, align 4, !tbaa !14
  %call46 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element42, float* nonnull align 4 dereferenceable(4) %ref.tmp43, float* nonnull align 4 dereferenceable(4) %ref.tmp44, float* nonnull align 4 dereferenceable(4) %ref.tmp45)
  %arrayinit.element47 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element42, i32 1
  %56 = bitcast float* %ref.tmp48 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #9
  store float 0.000000e+00, float* %ref.tmp48, align 4, !tbaa !14
  %57 = bitcast float* %ref.tmp49 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %57) #9
  store float 0.000000e+00, float* %ref.tmp49, align 4, !tbaa !14
  %58 = bitcast float* %ref.tmp50 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #9
  store float 0.000000e+00, float* %ref.tmp50, align 4, !tbaa !14
  %call51 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element47, float* nonnull align 4 dereferenceable(4) %ref.tmp48, float* nonnull align 4 dereferenceable(4) %ref.tmp49, float* nonnull align 4 dereferenceable(4) %ref.tmp50)
  %59 = bitcast float* %ref.tmp50 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #9
  %60 = bitcast float* %ref.tmp49 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #9
  %61 = bitcast float* %ref.tmp48 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #9
  %62 = bitcast float* %ref.tmp45 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #9
  %63 = bitcast float* %ref.tmp44 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #9
  %64 = bitcast float* %ref.tmp43 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #9
  %65 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #9
  %66 = bitcast float* %ref.tmp39 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #9
  %67 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #9
  %68 = bitcast float* %ref.tmp35 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #9
  %69 = bitcast float* %ref.tmp34 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #9
  %70 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #9
  %71 = bitcast float* %ref.tmp30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #9
  %72 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #9
  %73 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #9
  %74 = bitcast float* %ref.tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #9
  %75 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #9
  %76 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #9
  %77 = bitcast %class.btPolyhedralConvexAabbCachingShape* %this1 to %class.btPolyhedralConvexShape*
  %arraydecay = getelementptr inbounds [6 x %class.btVector3], [6 x %class.btVector3]* %_supporting, i32 0, i32 0
  %78 = bitcast %class.btPolyhedralConvexShape* %77 to void (%class.btPolyhedralConvexShape*, %class.btVector3*, %class.btVector3*, i32)***
  %vtable = load void (%class.btPolyhedralConvexShape*, %class.btVector3*, %class.btVector3*, i32)**, void (%class.btPolyhedralConvexShape*, %class.btVector3*, %class.btVector3*, i32)*** %78, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btPolyhedralConvexShape*, %class.btVector3*, %class.btVector3*, i32)*, void (%class.btPolyhedralConvexShape*, %class.btVector3*, %class.btVector3*, i32)** %vtable, i64 19
  %79 = load void (%class.btPolyhedralConvexShape*, %class.btVector3*, %class.btVector3*, i32)*, void (%class.btPolyhedralConvexShape*, %class.btVector3*, %class.btVector3*, i32)** %vfn, align 4
  call void %79(%class.btPolyhedralConvexShape* %77, %class.btVector3* getelementptr inbounds ([6 x %class.btVector3], [6 x %class.btVector3]* @_ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions, i32 0, i32 0), %class.btVector3* %arraydecay, i32 6)
  %80 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #9
  store i32 0, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %init.end
  %81 = load i32, i32* %i, align 4, !tbaa !10
  %cmp = icmp slt i32 %81, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %82 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %83 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds [6 x %class.btVector3], [6 x %class.btVector3]* %_supporting, i32 0, i32 %83
  %call52 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx)
  %84 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx53 = getelementptr inbounds float, float* %call52, i32 %84
  %85 = load float, float* %arrayidx53, align 4, !tbaa !14
  %86 = bitcast %class.btPolyhedralConvexAabbCachingShape* %this1 to %class.btConvexInternalShape*
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %86, i32 0, i32 3
  %87 = load float, float* %m_collisionMargin, align 4, !tbaa !50
  %add = fadd float %85, %87
  %m_localAabbMax = getelementptr inbounds %class.btPolyhedralConvexAabbCachingShape, %class.btPolyhedralConvexAabbCachingShape* %this1, i32 0, i32 2
  %call54 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_localAabbMax)
  %88 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx55 = getelementptr inbounds float, float* %call54, i32 %88
  store float %add, float* %arrayidx55, align 4, !tbaa !14
  %89 = load i32, i32* %i, align 4, !tbaa !10
  %add56 = add nsw i32 %89, 3
  %arrayidx57 = getelementptr inbounds [6 x %class.btVector3], [6 x %class.btVector3]* %_supporting, i32 0, i32 %add56
  %call58 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx57)
  %90 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx59 = getelementptr inbounds float, float* %call58, i32 %90
  %91 = load float, float* %arrayidx59, align 4, !tbaa !14
  %92 = bitcast %class.btPolyhedralConvexAabbCachingShape* %this1 to %class.btConvexInternalShape*
  %m_collisionMargin60 = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %92, i32 0, i32 3
  %93 = load float, float* %m_collisionMargin60, align 4, !tbaa !50
  %sub = fsub float %91, %93
  %m_localAabbMin = getelementptr inbounds %class.btPolyhedralConvexAabbCachingShape, %class.btPolyhedralConvexAabbCachingShape* %this1, i32 0, i32 1
  %call61 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_localAabbMin)
  %94 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx62 = getelementptr inbounds float, float* %call61, i32 %94
  store float %sub, float* %arrayidx62, align 4, !tbaa !14
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %95 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add nsw i32 %95, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %96 = bitcast [6 x %class.btVector3]* %_supporting to i8*
  call void @llvm.lifetime.end.p0i8(i64 96, i8* %96) #9
  ret void
}

define hidden %class.btPolyhedralConvexAabbCachingShape* @_ZN34btPolyhedralConvexAabbCachingShapeC2Ev(%class.btPolyhedralConvexAabbCachingShape* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btPolyhedralConvexAabbCachingShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  store %class.btPolyhedralConvexAabbCachingShape* %this, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPolyhedralConvexAabbCachingShape*, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4
  %0 = bitcast %class.btPolyhedralConvexAabbCachingShape* %this1 to %class.btPolyhedralConvexShape*
  %call = call %class.btPolyhedralConvexShape* @_ZN23btPolyhedralConvexShapeC2Ev(%class.btPolyhedralConvexShape* %0)
  %1 = bitcast %class.btPolyhedralConvexAabbCachingShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [33 x i8*] }, { [33 x i8*] }* @_ZTV34btPolyhedralConvexAabbCachingShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_localAabbMin = getelementptr inbounds %class.btPolyhedralConvexAabbCachingShape, %class.btPolyhedralConvexAabbCachingShape* %this1, i32 0, i32 1
  %2 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !14
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  store float 1.000000e+00, float* %ref.tmp2, align 4, !tbaa !14
  %4 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  store float 1.000000e+00, float* %ref.tmp3, align 4, !tbaa !14
  %call4 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %m_localAabbMin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #9
  %6 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  %7 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  %m_localAabbMax = getelementptr inbounds %class.btPolyhedralConvexAabbCachingShape, %class.btPolyhedralConvexAabbCachingShape* %this1, i32 0, i32 2
  %8 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  store float -1.000000e+00, float* %ref.tmp5, align 4, !tbaa !14
  %9 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  store float -1.000000e+00, float* %ref.tmp6, align 4, !tbaa !14
  %10 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  store float -1.000000e+00, float* %ref.tmp7, align 4, !tbaa !14
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %m_localAabbMax, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %11 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  %12 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  %13 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  %m_isLocalAabbValid = getelementptr inbounds %class.btPolyhedralConvexAabbCachingShape, %class.btPolyhedralConvexAabbCachingShape* %this1, i32 0, i32 3
  store i8 0, i8* %m_isLocalAabbValid, align 4, !tbaa !46
  ret %class.btPolyhedralConvexAabbCachingShape* %this1
}

define hidden void @_ZNK34btPolyhedralConvexAabbCachingShape7getAabbERK11btTransformR9btVector3S4_(%class.btPolyhedralConvexAabbCachingShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %trans, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btPolyhedralConvexAabbCachingShape*, align 4
  %trans.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btPolyhedralConvexAabbCachingShape* %this, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %trans, %class.btTransform** %trans.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btPolyhedralConvexAabbCachingShape*, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4, !tbaa !2
  %1 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %3 = bitcast %class.btPolyhedralConvexAabbCachingShape* %this1 to %class.btConvexInternalShape*
  %4 = bitcast %class.btConvexInternalShape* %3 to float (%class.btConvexInternalShape*)***
  %vtable = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %4, align 4, !tbaa !6
  %vfn = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable, i64 12
  %5 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn, align 4
  %call = call float %5(%class.btConvexInternalShape* %3)
  call void @_ZNK34btPolyhedralConvexAabbCachingShape17getNonvirtualAabbERK11btTransformR9btVector3S4_f(%class.btPolyhedralConvexAabbCachingShape* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, float %call)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK34btPolyhedralConvexAabbCachingShape17getNonvirtualAabbERK11btTransformR9btVector3S4_f(%class.btPolyhedralConvexAabbCachingShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %trans, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, float %margin) #7 comdat {
entry:
  %this.addr = alloca %class.btPolyhedralConvexAabbCachingShape*, align 4
  %trans.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %margin.addr = alloca float, align 4
  store %class.btPolyhedralConvexAabbCachingShape* %this, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %trans, %class.btTransform** %trans.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  store float %margin, float* %margin.addr, align 4, !tbaa !14
  %this1 = load %class.btPolyhedralConvexAabbCachingShape*, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4
  %m_localAabbMin = getelementptr inbounds %class.btPolyhedralConvexAabbCachingShape, %class.btPolyhedralConvexAabbCachingShape* %this1, i32 0, i32 1
  %m_localAabbMax = getelementptr inbounds %class.btPolyhedralConvexAabbCachingShape, %class.btPolyhedralConvexAabbCachingShape* %this1, i32 0, i32 2
  %0 = load float, float* %margin.addr, align 4, !tbaa !14
  %1 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %3 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  call void @_Z15btTransformAabbRK9btVector3S1_fRK11btTransformRS_S5_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localAabbMax, float %0, %class.btTransform* nonnull align 4 dereferenceable(64) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  ret void
}

; Function Attrs: nounwind
declare i32 @__cxa_guard_acquire(i32*) #9

; Function Attrs: argmemonly nounwind willreturn
declare {}* @llvm.invariant.start.p0i8(i64 immarg, i8* nocapture) #5

; Function Attrs: nounwind
declare void @__cxa_guard_release(i32*) #9

define linkonce_odr hidden void @_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_(%class.btConvexInternalShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %1 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %3 = bitcast %class.btConvexInternalShape* %this1 to void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %3, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 20
  %4 = load void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %4(%class.btConvexInternalShape* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  ret void
}

declare void @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf(%class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4)) unnamed_addr #1

declare float @_ZNK16btCollisionShape20getAngularMotionDiscEv(%class.btCollisionShape*) unnamed_addr #1

declare float @_ZNK16btCollisionShape27getContactBreakingThresholdEf(%class.btCollisionShape*, float) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btConvexInternalShape15getLocalScalingEv(%class.btConvexInternalShape* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 1
  ret %class.btVector3* %m_localScaling
}

declare void @__cxa_pure_virtual() unnamed_addr

define linkonce_odr hidden void @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btCollisionShape* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !14
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store float 1.000000e+00, float* %ref.tmp2, align 4, !tbaa !14
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 1.000000e+00, float* %ref.tmp3, align 4, !tbaa !14
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN21btConvexInternalShape9setMarginEf(%class.btConvexInternalShape* %this, float %margin) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %margin.addr = alloca float, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  store float %margin, float* %margin.addr, align 4, !tbaa !14
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load float, float* %margin.addr, align 4, !tbaa !14
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  store float %0, float* %m_collisionMargin, align 4, !tbaa !50
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK21btConvexInternalShape9getMarginEv(%class.btConvexInternalShape* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  %0 = load float, float* %m_collisionMargin, align 4, !tbaa !50
  ret float %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv(%class.btConvexInternalShape* %this) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  ret i32 52
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer(%class.btConvexInternalShape* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #7 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %shapeData = alloca %struct.btConvexInternalShapeData*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = bitcast %struct.btConvexInternalShapeData** %shapeData to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i8*, i8** %dataBuffer.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.btConvexInternalShapeData*
  store %struct.btConvexInternalShapeData* %2, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %3 = bitcast %class.btConvexInternalShape* %this1 to %class.btCollisionShape*
  %4 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_collisionShapeData = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %4, i32 0, i32 0
  %5 = bitcast %struct.btCollisionShapeData* %m_collisionShapeData to i8*
  %6 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %call = call i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape* %3, i8* %5, %class.btSerializer* %6)
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 2
  %7 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_implicitShapeDimensions2 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %7, i32 0, i32 2
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_implicitShapeDimensions, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_implicitShapeDimensions2)
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 1
  %8 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_localScaling3 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %8, i32 0, i32 1
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_localScaling, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_localScaling3)
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  %9 = load float, float* %m_collisionMargin, align 4, !tbaa !50
  %10 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_collisionMargin4 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %10, i32 0, i32 3
  store float %9, float* %m_collisionMargin4, align 4, !tbaa !52
  %11 = bitcast %struct.btConvexInternalShapeData** %shapeData to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  ret i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str, i32 0, i32 0)
}

declare void @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer(%class.btCollisionShape*, %class.btSerializer*) unnamed_addr #1

declare void @_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3(%class.btVector3* sret align 4, %class.btConvexInternalShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

declare void @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_(%class.btConvexShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4), float* nonnull align 4 dereferenceable(4)) unnamed_addr #1

declare void @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_(%class.btConvexInternalShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv(%class.btConvexInternalShape* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  ret i32 0
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3(%class.btConvexInternalShape* %this, i32 %index, %class.btVector3* nonnull align 4 dereferenceable(16) %penetrationVector) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %index.addr = alloca i32, align 4
  %penetrationVector.addr = alloca %class.btVector3*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !10
  store %class.btVector3* %penetrationVector, %class.btVector3** %penetrationVector.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %penetrationVector.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define hidden %class.btPolyhedralConvexShape* @_ZN23btPolyhedralConvexShapeD2Ev(%class.btPolyhedralConvexShape* returned %this) unnamed_addr #3 {
entry:
  %retval = alloca %class.btPolyhedralConvexShape*, align 4
  %this.addr = alloca %class.btPolyhedralConvexShape*, align 4
  store %class.btPolyhedralConvexShape* %this, %class.btPolyhedralConvexShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPolyhedralConvexShape*, %class.btPolyhedralConvexShape** %this.addr, align 4
  store %class.btPolyhedralConvexShape* %this1, %class.btPolyhedralConvexShape** %retval, align 4
  %0 = bitcast %class.btPolyhedralConvexShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [33 x i8*] }, { [33 x i8*] }* @_ZTV23btPolyhedralConvexShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  %m_polyhedron = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  %1 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %m_polyhedron, align 4, !tbaa !8
  %tobool = icmp ne %class.btConvexPolyhedron* %1, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_polyhedron2 = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  %2 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %m_polyhedron2, align 4, !tbaa !8
  %3 = bitcast %class.btConvexPolyhedron* %2 to %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)***
  %vtable = load %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)**, %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)*** %3, align 4, !tbaa !6
  %vfn = getelementptr inbounds %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)*, %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)** %vtable, i64 0
  %4 = load %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)*, %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)** %vfn, align 4
  %call = call %class.btConvexPolyhedron* %4(%class.btConvexPolyhedron* %2) #9
  %m_polyhedron3 = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  %5 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %m_polyhedron3, align 4, !tbaa !8
  %6 = bitcast %class.btConvexPolyhedron* %5 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %6)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %7 = bitcast %class.btPolyhedralConvexShape* %this1 to %class.btConvexInternalShape*
  %call4 = call %class.btConvexInternalShape* bitcast (%class.btConvexShape* (%class.btConvexShape*)* @_ZN13btConvexShapeD2Ev to %class.btConvexInternalShape* (%class.btConvexInternalShape*)*)(%class.btConvexInternalShape* %7) #9
  %8 = load %class.btPolyhedralConvexShape*, %class.btPolyhedralConvexShape** %retval, align 4
  ret %class.btPolyhedralConvexShape* %8
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN34btPolyhedralConvexAabbCachingShapeD0Ev(%class.btPolyhedralConvexAabbCachingShape* %this) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %class.btPolyhedralConvexAabbCachingShape*, align 4
  store %class.btPolyhedralConvexAabbCachingShape* %this, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPolyhedralConvexAabbCachingShape*, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4
  call void @llvm.trap() #11
  unreachable
}

define linkonce_odr hidden %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEEC2Ev(%class.btAlignedObjectArray.8* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.9* @_ZN18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EEC2Ev(%class.btAlignedAllocator.9* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4initEv(%class.btAlignedObjectArray.8* %this1)
  ret %class.btAlignedObjectArray.8* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.9* @_ZN18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EEC2Ev(%class.btAlignedAllocator.9* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  ret %class.btAlignedAllocator.9* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4initEv(%class.btAlignedObjectArray.8* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !56
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %"class.btConvexHullComputer::Edge"* null, %"class.btConvexHullComputer::Edge"** %m_data, align 4, !tbaa !35
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !57
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !58
  ret void
}

declare float @_ZN20btConvexHullComputer7computeEPKvbiiff(%class.btConvexHullComputer*, i8*, i1 zeroext, i32, i32, float, float) #1

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #7 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !14
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !14
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  ret %class.btVector3* %call
}

; Function Attrs: nounwind
define linkonce_odr hidden %"class.btConvexHullComputer::Edge"* @_ZNK20btConvexHullComputer4Edge19getNextEdgeOfVertexEv(%"class.btConvexHullComputer::Edge"* %this) #3 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullComputer::Edge"*, align 4
  store %"class.btConvexHullComputer::Edge"* %this, %"class.btConvexHullComputer::Edge"** %this.addr, align 4, !tbaa !2
  %this1 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %this.addr, align 4
  %next = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %this1, i32 0, i32 0
  %0 = load i32, i32* %next, align 4, !tbaa !59
  %add.ptr = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %this1, i32 %0
  ret %"class.btConvexHullComputer::Edge"* %add.ptr
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %n, %class.btVector3* nonnull align 4 dereferenceable(16) %p, %class.btVector3* nonnull align 4 dereferenceable(16) %q) #7 comdat {
entry:
  %n.addr = alloca %class.btVector3*, align 4
  %p.addr = alloca %class.btVector3*, align 4
  %q.addr = alloca %class.btVector3*, align 4
  %a = alloca float, align 4
  %k = alloca float, align 4
  %a42 = alloca float, align 4
  %k54 = alloca float, align 4
  store %class.btVector3* %n, %class.btVector3** %n.addr, align 4, !tbaa !2
  store %class.btVector3* %p, %class.btVector3** %p.addr, align 4, !tbaa !2
  store %class.btVector3* %q, %class.btVector3** %q.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %0)
  %arrayidx = getelementptr inbounds float, float* %call, i32 2
  %1 = load float, float* %arrayidx, align 4, !tbaa !14
  %call1 = call float @_Z6btFabsf(float %1)
  %cmp = fcmp ogt float %call1, 0x3FE6A09E60000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = bitcast float* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %3 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %3)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 1
  %4 = load float, float* %arrayidx3, align 4, !tbaa !14
  %5 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call4 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %5)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 1
  %6 = load float, float* %arrayidx5, align 4, !tbaa !14
  %mul = fmul float %4, %6
  %7 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %7)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 2
  %8 = load float, float* %arrayidx7, align 4, !tbaa !14
  %9 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %9)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 2
  %10 = load float, float* %arrayidx9, align 4, !tbaa !14
  %mul10 = fmul float %8, %10
  %add = fadd float %mul, %mul10
  store float %add, float* %a, align 4, !tbaa !14
  %11 = bitcast float* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  %12 = load float, float* %a, align 4, !tbaa !14
  %call11 = call float @_Z6btSqrtf(float %12)
  %div = fdiv float 1.000000e+00, %call11
  store float %div, float* %k, align 4, !tbaa !14
  %13 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %13)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 0
  store float 0.000000e+00, float* %arrayidx13, align 4, !tbaa !14
  %14 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call14 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %14)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 2
  %15 = load float, float* %arrayidx15, align 4, !tbaa !14
  %fneg = fneg float %15
  %16 = load float, float* %k, align 4, !tbaa !14
  %mul16 = fmul float %fneg, %16
  %17 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %17)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 1
  store float %mul16, float* %arrayidx18, align 4, !tbaa !14
  %18 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call19 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %18)
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 1
  %19 = load float, float* %arrayidx20, align 4, !tbaa !14
  %20 = load float, float* %k, align 4, !tbaa !14
  %mul21 = fmul float %19, %20
  %21 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call22 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %21)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 2
  store float %mul21, float* %arrayidx23, align 4, !tbaa !14
  %22 = load float, float* %a, align 4, !tbaa !14
  %23 = load float, float* %k, align 4, !tbaa !14
  %mul24 = fmul float %22, %23
  %24 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %24)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 0
  store float %mul24, float* %arrayidx26, align 4, !tbaa !14
  %25 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call27 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %25)
  %arrayidx28 = getelementptr inbounds float, float* %call27, i32 0
  %26 = load float, float* %arrayidx28, align 4, !tbaa !14
  %fneg29 = fneg float %26
  %27 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %27)
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 2
  %28 = load float, float* %arrayidx31, align 4, !tbaa !14
  %mul32 = fmul float %fneg29, %28
  %29 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %29)
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 1
  store float %mul32, float* %arrayidx34, align 4, !tbaa !14
  %30 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call35 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %30)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 0
  %31 = load float, float* %arrayidx36, align 4, !tbaa !14
  %32 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call37 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %32)
  %arrayidx38 = getelementptr inbounds float, float* %call37, i32 1
  %33 = load float, float* %arrayidx38, align 4, !tbaa !14
  %mul39 = fmul float %31, %33
  %34 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %call40 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %34)
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 2
  store float %mul39, float* %arrayidx41, align 4, !tbaa !14
  %35 = bitcast float* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #9
  %36 = bitcast float* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #9
  br label %if.end

if.else:                                          ; preds = %entry
  %37 = bitcast float* %a42 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #9
  %38 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call43 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %38)
  %arrayidx44 = getelementptr inbounds float, float* %call43, i32 0
  %39 = load float, float* %arrayidx44, align 4, !tbaa !14
  %40 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call45 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %40)
  %arrayidx46 = getelementptr inbounds float, float* %call45, i32 0
  %41 = load float, float* %arrayidx46, align 4, !tbaa !14
  %mul47 = fmul float %39, %41
  %42 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call48 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %42)
  %arrayidx49 = getelementptr inbounds float, float* %call48, i32 1
  %43 = load float, float* %arrayidx49, align 4, !tbaa !14
  %44 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call50 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %44)
  %arrayidx51 = getelementptr inbounds float, float* %call50, i32 1
  %45 = load float, float* %arrayidx51, align 4, !tbaa !14
  %mul52 = fmul float %43, %45
  %add53 = fadd float %mul47, %mul52
  store float %add53, float* %a42, align 4, !tbaa !14
  %46 = bitcast float* %k54 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #9
  %47 = load float, float* %a42, align 4, !tbaa !14
  %call55 = call float @_Z6btSqrtf(float %47)
  %div56 = fdiv float 1.000000e+00, %call55
  store float %div56, float* %k54, align 4, !tbaa !14
  %48 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call57 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %48)
  %arrayidx58 = getelementptr inbounds float, float* %call57, i32 1
  %49 = load float, float* %arrayidx58, align 4, !tbaa !14
  %fneg59 = fneg float %49
  %50 = load float, float* %k54, align 4, !tbaa !14
  %mul60 = fmul float %fneg59, %50
  %51 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call61 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %51)
  %arrayidx62 = getelementptr inbounds float, float* %call61, i32 0
  store float %mul60, float* %arrayidx62, align 4, !tbaa !14
  %52 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call63 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %52)
  %arrayidx64 = getelementptr inbounds float, float* %call63, i32 0
  %53 = load float, float* %arrayidx64, align 4, !tbaa !14
  %54 = load float, float* %k54, align 4, !tbaa !14
  %mul65 = fmul float %53, %54
  %55 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call66 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %55)
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 1
  store float %mul65, float* %arrayidx67, align 4, !tbaa !14
  %56 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call68 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %56)
  %arrayidx69 = getelementptr inbounds float, float* %call68, i32 2
  store float 0.000000e+00, float* %arrayidx69, align 4, !tbaa !14
  %57 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call70 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %57)
  %arrayidx71 = getelementptr inbounds float, float* %call70, i32 2
  %58 = load float, float* %arrayidx71, align 4, !tbaa !14
  %fneg72 = fneg float %58
  %59 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call73 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %59)
  %arrayidx74 = getelementptr inbounds float, float* %call73, i32 1
  %60 = load float, float* %arrayidx74, align 4, !tbaa !14
  %mul75 = fmul float %fneg72, %60
  %61 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %call76 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %61)
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 0
  store float %mul75, float* %arrayidx77, align 4, !tbaa !14
  %62 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call78 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %62)
  %arrayidx79 = getelementptr inbounds float, float* %call78, i32 2
  %63 = load float, float* %arrayidx79, align 4, !tbaa !14
  %64 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call80 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %64)
  %arrayidx81 = getelementptr inbounds float, float* %call80, i32 0
  %65 = load float, float* %arrayidx81, align 4, !tbaa !14
  %mul82 = fmul float %63, %65
  %66 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %call83 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %66)
  %arrayidx84 = getelementptr inbounds float, float* %call83, i32 1
  store float %mul82, float* %arrayidx84, align 4, !tbaa !14
  %67 = load float, float* %a42, align 4, !tbaa !14
  %68 = load float, float* %k54, align 4, !tbaa !14
  %mul85 = fmul float %67, %68
  %69 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %call86 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %69)
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 2
  store float %mul85, float* %arrayidx87, align 4, !tbaa !14
  %70 = bitcast float* %k54 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #9
  %71 = bitcast float* %a42 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #9
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI13GrahamVector3E4swapEii(%class.btAlignedObjectArray.12* %this, i32 %index0, i32 %index1) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %struct.GrahamVector3, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %index0, i32* %index0.addr, align 4, !tbaa !10
  store i32 %index1, i32* %index1.addr, align 4, !tbaa !10
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = bitcast %struct.GrahamVector3* %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %0) #9
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %1 = load %struct.GrahamVector3*, %struct.GrahamVector3** %m_data, align 4, !tbaa !44
  %2 = load i32, i32* %index0.addr, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %1, i32 %2
  %3 = bitcast %struct.GrahamVector3* %temp to i8*
  %4 = bitcast %struct.GrahamVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 24, i1 false)
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %5 = load %struct.GrahamVector3*, %struct.GrahamVector3** %m_data2, align 4, !tbaa !44
  %6 = load i32, i32* %index1.addr, align 4, !tbaa !10
  %arrayidx3 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %5, i32 %6
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %7 = load %struct.GrahamVector3*, %struct.GrahamVector3** %m_data4, align 4, !tbaa !44
  %8 = load i32, i32* %index0.addr, align 4, !tbaa !10
  %arrayidx5 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %7, i32 %8
  %9 = bitcast %struct.GrahamVector3* %arrayidx5 to i8*
  %10 = bitcast %struct.GrahamVector3* %arrayidx3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 24, i1 false)
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %11 = load %struct.GrahamVector3*, %struct.GrahamVector3** %m_data6, align 4, !tbaa !44
  %12 = load i32, i32* %index1.addr, align 4, !tbaa !10
  %arrayidx7 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %11, i32 %12
  %13 = bitcast %struct.GrahamVector3* %arrayidx7 to i8*
  %14 = bitcast %struct.GrahamVector3* %temp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 24, i1 false)
  %15 = bitcast %struct.GrahamVector3* %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %15) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_Z7btCrossRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #7 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %1 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btAngleCompareFunc* @_ZN18btAngleCompareFuncC2ERK9btVector3(%struct.btAngleCompareFunc* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %anchor) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %struct.btAngleCompareFunc*, align 4
  %anchor.addr = alloca %class.btVector3*, align 4
  store %struct.btAngleCompareFunc* %this, %struct.btAngleCompareFunc** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %anchor, %class.btVector3** %anchor.addr, align 4, !tbaa !2
  %this1 = load %struct.btAngleCompareFunc*, %struct.btAngleCompareFunc** %this.addr, align 4
  %m_anchor = getelementptr inbounds %struct.btAngleCompareFunc, %struct.btAngleCompareFunc* %this1, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %anchor.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVector3* %m_anchor to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !12
  ret %struct.btAngleCompareFunc* %this1
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI13GrahamVector3E17quickSortInternalI18btAngleCompareFuncEEvRKT_ii(%class.btAlignedObjectArray.12* %this, %struct.btAngleCompareFunc* nonnull align 4 dereferenceable(16) %CompareFunc, i32 %lo, i32 %hi) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %CompareFunc.addr = alloca %struct.btAngleCompareFunc*, align 4
  %lo.addr = alloca i32, align 4
  %hi.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %x = alloca %struct.GrahamVector3, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store %struct.btAngleCompareFunc* %CompareFunc, %struct.btAngleCompareFunc** %CompareFunc.addr, align 4, !tbaa !2
  store i32 %lo, i32* %lo.addr, align 4, !tbaa !10
  store i32 %hi, i32* %hi.addr, align 4, !tbaa !10
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %lo.addr, align 4, !tbaa !10
  store i32 %1, i32* %i, align 4, !tbaa !10
  %2 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %3 = load i32, i32* %hi.addr, align 4, !tbaa !10
  store i32 %3, i32* %j, align 4, !tbaa !10
  %4 = bitcast %struct.GrahamVector3* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %4) #9
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %5 = load %struct.GrahamVector3*, %struct.GrahamVector3** %m_data, align 4, !tbaa !44
  %6 = load i32, i32* %lo.addr, align 4, !tbaa !10
  %7 = load i32, i32* %hi.addr, align 4, !tbaa !10
  %add = add nsw i32 %6, %7
  %div = sdiv i32 %add, 2
  %arrayidx = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %5, i32 %div
  %8 = bitcast %struct.GrahamVector3* %x to i8*
  %9 = bitcast %struct.GrahamVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 24, i1 false)
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %do.body
  %10 = load %struct.btAngleCompareFunc*, %struct.btAngleCompareFunc** %CompareFunc.addr, align 4, !tbaa !2
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %11 = load %struct.GrahamVector3*, %struct.GrahamVector3** %m_data2, align 4, !tbaa !44
  %12 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx3 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %11, i32 %12
  %call = call zeroext i1 @_ZNK18btAngleCompareFuncclERK13GrahamVector3S2_(%struct.btAngleCompareFunc* %10, %struct.GrahamVector3* nonnull align 4 dereferenceable(24) %arrayidx3, %struct.GrahamVector3* nonnull align 4 dereferenceable(24) %x)
  br i1 %call, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %13 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %while.cond4

while.cond4:                                      ; preds = %while.body8, %while.end
  %14 = load %struct.btAngleCompareFunc*, %struct.btAngleCompareFunc** %CompareFunc.addr, align 4, !tbaa !2
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %15 = load %struct.GrahamVector3*, %struct.GrahamVector3** %m_data5, align 4, !tbaa !44
  %16 = load i32, i32* %j, align 4, !tbaa !10
  %arrayidx6 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %15, i32 %16
  %call7 = call zeroext i1 @_ZNK18btAngleCompareFuncclERK13GrahamVector3S2_(%struct.btAngleCompareFunc* %14, %struct.GrahamVector3* nonnull align 4 dereferenceable(24) %x, %struct.GrahamVector3* nonnull align 4 dereferenceable(24) %arrayidx6)
  br i1 %call7, label %while.body8, label %while.end9

while.body8:                                      ; preds = %while.cond4
  %17 = load i32, i32* %j, align 4, !tbaa !10
  %dec = add nsw i32 %17, -1
  store i32 %dec, i32* %j, align 4, !tbaa !10
  br label %while.cond4

while.end9:                                       ; preds = %while.cond4
  %18 = load i32, i32* %i, align 4, !tbaa !10
  %19 = load i32, i32* %j, align 4, !tbaa !10
  %cmp = icmp sle i32 %18, %19
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %while.end9
  %20 = load i32, i32* %i, align 4, !tbaa !10
  %21 = load i32, i32* %j, align 4, !tbaa !10
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E4swapEii(%class.btAlignedObjectArray.12* %this1, i32 %20, i32 %21)
  %22 = load i32, i32* %i, align 4, !tbaa !10
  %inc10 = add nsw i32 %22, 1
  store i32 %inc10, i32* %i, align 4, !tbaa !10
  %23 = load i32, i32* %j, align 4, !tbaa !10
  %dec11 = add nsw i32 %23, -1
  store i32 %dec11, i32* %j, align 4, !tbaa !10
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end9
  br label %do.cond

do.cond:                                          ; preds = %if.end
  %24 = load i32, i32* %i, align 4, !tbaa !10
  %25 = load i32, i32* %j, align 4, !tbaa !10
  %cmp12 = icmp sle i32 %24, %25
  br i1 %cmp12, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %26 = load i32, i32* %lo.addr, align 4, !tbaa !10
  %27 = load i32, i32* %j, align 4, !tbaa !10
  %cmp13 = icmp slt i32 %26, %27
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %do.end
  %28 = load %struct.btAngleCompareFunc*, %struct.btAngleCompareFunc** %CompareFunc.addr, align 4, !tbaa !2
  %29 = load i32, i32* %lo.addr, align 4, !tbaa !10
  %30 = load i32, i32* %j, align 4, !tbaa !10
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E17quickSortInternalI18btAngleCompareFuncEEvRKT_ii(%class.btAlignedObjectArray.12* %this1, %struct.btAngleCompareFunc* nonnull align 4 dereferenceable(16) %28, i32 %29, i32 %30)
  br label %if.end15

if.end15:                                         ; preds = %if.then14, %do.end
  %31 = load i32, i32* %i, align 4, !tbaa !10
  %32 = load i32, i32* %hi.addr, align 4, !tbaa !10
  %cmp16 = icmp slt i32 %31, %32
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end15
  %33 = load %struct.btAngleCompareFunc*, %struct.btAngleCompareFunc** %CompareFunc.addr, align 4, !tbaa !2
  %34 = load i32, i32* %i, align 4, !tbaa !10
  %35 = load i32, i32* %hi.addr, align 4, !tbaa !10
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E17quickSortInternalI18btAngleCompareFuncEEvRKT_ii(%class.btAlignedObjectArray.12* %this1, %struct.btAngleCompareFunc* nonnull align 4 dereferenceable(16) %33, i32 %34, i32 %35)
  br label %if.end18

if.end18:                                         ; preds = %if.then17, %if.end15
  %36 = bitcast %struct.GrahamVector3* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %36) #9
  %37 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #9
  %38 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI13GrahamVector3E8pop_backEv(%class.btAlignedObjectArray.12* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !41
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4, !tbaa !41
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %1 = load %struct.GrahamVector3*, %struct.GrahamVector3** %m_data, align 4, !tbaa !44
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4, !tbaa !41
  %arrayidx = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %1, i32 %2
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btFabsf(float %x) #6 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !14
  %0 = load float, float* %x.addr, align 4, !tbaa !14
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #10

define linkonce_odr hidden zeroext i1 @_ZNK18btAngleCompareFuncclERK13GrahamVector3S2_(%struct.btAngleCompareFunc* %this, %struct.GrahamVector3* nonnull align 4 dereferenceable(24) %a, %struct.GrahamVector3* nonnull align 4 dereferenceable(24) %b) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %struct.btAngleCompareFunc*, align 4
  %a.addr = alloca %struct.GrahamVector3*, align 4
  %b.addr = alloca %struct.GrahamVector3*, align 4
  %al = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %bl = alloca float, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.btAngleCompareFunc* %this, %struct.btAngleCompareFunc** %this.addr, align 4, !tbaa !2
  store %struct.GrahamVector3* %a, %struct.GrahamVector3** %a.addr, align 4, !tbaa !2
  store %struct.GrahamVector3* %b, %struct.GrahamVector3** %b.addr, align 4, !tbaa !2
  %this1 = load %struct.btAngleCompareFunc*, %struct.btAngleCompareFunc** %this.addr, align 4
  %0 = load %struct.GrahamVector3*, %struct.GrahamVector3** %a.addr, align 4, !tbaa !2
  %m_angle = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %0, i32 0, i32 1
  %1 = load float, float* %m_angle, align 4, !tbaa !45
  %2 = load %struct.GrahamVector3*, %struct.GrahamVector3** %b.addr, align 4, !tbaa !2
  %m_angle2 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %2, i32 0, i32 1
  %3 = load float, float* %m_angle2, align 4, !tbaa !45
  %cmp = fcmp une float %1, %3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load %struct.GrahamVector3*, %struct.GrahamVector3** %a.addr, align 4, !tbaa !2
  %m_angle3 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %4, i32 0, i32 1
  %5 = load float, float* %m_angle3, align 4, !tbaa !45
  %6 = load %struct.GrahamVector3*, %struct.GrahamVector3** %b.addr, align 4, !tbaa !2
  %m_angle4 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %6, i32 0, i32 1
  %7 = load float, float* %m_angle4, align 4, !tbaa !45
  %cmp5 = fcmp olt float %5, %7
  store i1 %cmp5, i1* %retval, align 1
  br label %return

if.else:                                          ; preds = %entry
  %8 = bitcast float* %al to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  %9 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #9
  %10 = load %struct.GrahamVector3*, %struct.GrahamVector3** %a.addr, align 4, !tbaa !2
  %11 = bitcast %struct.GrahamVector3* %10 to %class.btVector3*
  %m_anchor = getelementptr inbounds %struct.btAngleCompareFunc, %struct.btAngleCompareFunc* %this1, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %11, %class.btVector3* nonnull align 4 dereferenceable(16) %m_anchor)
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ref.tmp)
  %12 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %12) #9
  store float %call, float* %al, align 4, !tbaa !14
  %13 = bitcast float* %bl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %14 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %14) #9
  %15 = load %struct.GrahamVector3*, %struct.GrahamVector3** %b.addr, align 4, !tbaa !2
  %16 = bitcast %struct.GrahamVector3* %15 to %class.btVector3*
  %m_anchor7 = getelementptr inbounds %struct.btAngleCompareFunc, %struct.btAngleCompareFunc* %this1, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %16, %class.btVector3* nonnull align 4 dereferenceable(16) %m_anchor7)
  %call8 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ref.tmp6)
  %17 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %17) #9
  store float %call8, float* %bl, align 4, !tbaa !14
  %18 = load float, float* %al, align 4, !tbaa !14
  %19 = load float, float* %bl, align 4, !tbaa !14
  %cmp9 = fcmp une float %18, %19
  br i1 %cmp9, label %if.then10, label %if.else12

if.then10:                                        ; preds = %if.else
  %20 = load float, float* %al, align 4, !tbaa !14
  %21 = load float, float* %bl, align 4, !tbaa !14
  %cmp11 = fcmp olt float %20, %21
  store i1 %cmp11, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else12:                                        ; preds = %if.else
  %22 = load %struct.GrahamVector3*, %struct.GrahamVector3** %a.addr, align 4, !tbaa !2
  %m_orgIndex = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %22, i32 0, i32 2
  %23 = load i32, i32* %m_orgIndex, align 4, !tbaa !18
  %24 = load %struct.GrahamVector3*, %struct.GrahamVector3** %b.addr, align 4, !tbaa !2
  %m_orgIndex13 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %24, i32 0, i32 2
  %25 = load i32, i32* %m_orgIndex13, align 4, !tbaa !18
  %cmp14 = icmp slt i32 %23, %25
  store i1 %cmp14, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else12, %if.then10
  %26 = bitcast float* %bl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #9
  %27 = bitcast float* %al to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #9
  br label %return

return:                                           ; preds = %cleanup, %if.then
  %28 = load i1, i1* %retval, align 1
  ret i1 %28
}

define linkonce_odr hidden %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiEC2ERKS0_(%class.btAlignedObjectArray.3* returned %this, %class.btAlignedObjectArray.3* nonnull align 4 dereferenceable(17) %otherArray) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %otherArray.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %otherSize = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.3* %otherArray, %class.btAlignedObjectArray.3** %otherArray.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.4* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.4* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.3* %this1)
  %0 = bitcast i32* %otherSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %otherArray.addr, align 4, !tbaa !2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %1)
  store i32 %call2, i32* %otherSize, align 4, !tbaa !10
  %2 = load i32, i32* %otherSize, align 4, !tbaa !10
  %3 = bitcast i32* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  store i32 0, i32* %ref.tmp, align 4, !tbaa !10
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.3* %this1, i32 %2, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  %4 = bitcast i32* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  %5 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %otherArray.addr, align 4, !tbaa !2
  %6 = load i32, i32* %otherSize, align 4, !tbaa !10
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %7 = load i32*, i32** %m_data, align 4, !tbaa !34
  call void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.3* %5, i32 0, i32 %6, i32* %7)
  %8 = bitcast i32* %otherSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #9
  ret %class.btAlignedObjectArray.3* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.4* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.4* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.4*, align 4
  store %class.btAlignedAllocator.4* %this, %class.btAlignedAllocator.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.4*, %class.btAlignedAllocator.4** %this.addr, align 4
  ret %class.btAlignedAllocator.4* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.3* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !60
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  store i32* null, i32** %m_data, align 4, !tbaa !34
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !27
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !61
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.3* %this, i32 %newsize, i32* nonnull align 4 dereferenceable(4) %fillData) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca i32*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !10
  store i32* %fillData, i32** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !10
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !10
  %2 = load i32, i32* %curSize, align 4, !tbaa !10
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !10
  store i32 %4, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !10
  %6 = load i32, i32* %curSize, align 4, !tbaa !10
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %8 = load i32*, i32** %m_data, align 4, !tbaa !34
  %9 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds i32, i32* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !10
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !10
  call void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.3* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %14 = load i32, i32* %curSize, align 4, !tbaa !10
  store i32 %14, i32* %i6, align 4, !tbaa !10
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !10
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !10
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %18 = load i32*, i32** %m_data11, align 4, !tbaa !34
  %19 = load i32, i32* %i6, align 4, !tbaa !10
  %arrayidx12 = getelementptr inbounds i32, i32* %18, i32 %19
  %20 = bitcast i32* %arrayidx12 to i8*
  %21 = bitcast i8* %20 to i32*
  %22 = load i32*, i32** %fillData.addr, align 4, !tbaa !2
  %23 = load i32, i32* %22, align 4, !tbaa !10
  store i32 %23, i32* %21, align 4, !tbaa !10
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %24 = load i32, i32* %i6, align 4, !tbaa !10
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !10
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %25 = load i32, i32* %newsize.addr, align 4, !tbaa !10
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 2
  store i32 %25, i32* %m_size, align 4, !tbaa !27
  %26 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.3* %this, i32 %start, i32 %end, i32* %dest) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !10
  store i32 %end, i32* %end.addr, align 4, !tbaa !10
  store i32* %dest, i32** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %start.addr, align 4, !tbaa !10
  store i32 %1, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !10
  %3 = load i32, i32* %end.addr, align 4, !tbaa !10
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i32*, i32** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  %6 = bitcast i32* %arrayidx to i8*
  %7 = bitcast i8* %6 to i32*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %8 = load i32*, i32** %m_data, align 4, !tbaa !34
  %9 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx2 = getelementptr inbounds i32, i32* %8, i32 %9
  %10 = load i32, i32* %arrayidx2, align 4, !tbaa !10
  store i32 %10, i32* %7, align 4, !tbaa !10
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.3* %this, i32 %_Count) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca i32*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !10
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.3* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !10
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast i32** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !10
  %call2 = call i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.3* %this1, i32 %2)
  %3 = bitcast i8* %call2 to i32*
  store i32* %3, i32** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  %4 = load i32*, i32** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.3* %this1, i32 0, i32 %call3, i32* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.3* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.3* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !60
  %5 = load i32*, i32** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  store i32* %5, i32** %m_data, align 4, !tbaa !34
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !10
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !61
  %7 = bitcast i32** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.3* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !61
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.3* %this, i32 %size) #7 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !10
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !10
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !10
  %call = call i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.4* %m_allocator, i32 %1, i32** null)
  %2 = bitcast i32* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.3* %this, i32 %first, i32 %last) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !10
  store i32 %last, i32* %last.addr, align 4, !tbaa !10
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %first.addr, align 4, !tbaa !10
  store i32 %1, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !10
  %3 = load i32, i32* %last.addr, align 4, !tbaa !10
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %4 = load i32*, i32** %m_data, align 4, !tbaa !34
  %5 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.3* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !34
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !60, !range !20
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4, !tbaa !34
  call void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.4* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4, !tbaa !34
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.4* %this, i32 %n, i32** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.4*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca i32**, align 4
  store %class.btAlignedAllocator.4* %this, %class.btAlignedAllocator.4** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !10
  store i32** %hint, i32*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.4*, %class.btAlignedAllocator.4** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !10
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to i32*
  ret i32* %1
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.4* %this, i32* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.4*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.4* %this, %class.btAlignedAllocator.4** %this.addr, align 4, !tbaa !2
  store i32* %ptr, i32** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.4*, %class.btAlignedAllocator.4** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEED2Ev(%class.btAlignedObjectArray.8* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE5clearEv(%class.btAlignedObjectArray.8* %this1)
  ret %class.btAlignedObjectArray.8* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE5clearEv(%class.btAlignedObjectArray.8* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE7destroyEii(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE10deallocateEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4initEv(%class.btAlignedObjectArray.8* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE7destroyEii(%class.btAlignedObjectArray.8* %this, i32 %first, i32 %last) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !10
  store i32 %last, i32* %last.addr, align 4, !tbaa !10
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %first.addr, align 4, !tbaa !10
  store i32 %1, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !10
  %3 = load i32, i32* %last.addr, align 4, !tbaa !10
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %4 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %m_data, align 4, !tbaa !35
  %5 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4sizeEv(%class.btAlignedObjectArray.8* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !57
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE10deallocateEv(%class.btAlignedObjectArray.8* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %m_data, align 4, !tbaa !35
  %tobool = icmp ne %"class.btConvexHullComputer::Edge"* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !56, !range !20
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %2 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %m_data4, align 4, !tbaa !35
  call void @_ZN18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EE10deallocateEPS1_(%class.btAlignedAllocator.9* %m_allocator, %"class.btConvexHullComputer::Edge"* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %"class.btConvexHullComputer::Edge"* null, %"class.btConvexHullComputer::Edge"** %m_data5, align 4, !tbaa !35
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EE10deallocateEPS1_(%class.btAlignedAllocator.9* %this, %"class.btConvexHullComputer::Edge"* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %ptr.addr = alloca %"class.btConvexHullComputer::Edge"*, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4, !tbaa !2
  store %"class.btConvexHullComputer::Edge"* %ptr, %"class.btConvexHullComputer::Edge"** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %"class.btConvexHullComputer::Edge"* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #10

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

define linkonce_odr hidden void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !14
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !14
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !14
  %3 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !14
  %4 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  store float 1.000000e+00, float* %ref.tmp5, align 4, !tbaa !14
  %5 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !14
  %6 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !14
  %7 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  store float 0.000000e+00, float* %ref.tmp8, align 4, !tbaa !14
  %8 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  store float 1.000000e+00, float* %ref.tmp9, align 4, !tbaa !14
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %9 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  %11 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  %12 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  %13 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  %14 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #9
  %15 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_Z15btTransformAabbRK9btVector3S1_fRK11btTransformRS_S5_(%class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax, float %margin, %class.btTransform* nonnull align 4 dereferenceable(64) %trans, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMinOut, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMaxOut) #7 comdat {
entry:
  %localAabbMin.addr = alloca %class.btVector3*, align 4
  %localAabbMax.addr = alloca %class.btVector3*, align 4
  %margin.addr = alloca float, align 4
  %trans.addr = alloca %class.btTransform*, align 4
  %aabbMinOut.addr = alloca %class.btVector3*, align 4
  %aabbMaxOut.addr = alloca %class.btVector3*, align 4
  %localHalfExtents = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %localCenter = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %abs_b = alloca %class.btMatrix3x3, align 4
  %center = alloca %class.btVector3, align 4
  %extent = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  store %class.btVector3* %localAabbMin, %class.btVector3** %localAabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %localAabbMax, %class.btVector3** %localAabbMax.addr, align 4, !tbaa !2
  store float %margin, float* %margin.addr, align 4, !tbaa !14
  store %class.btTransform* %trans, %class.btTransform** %trans.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMinOut, %class.btVector3** %aabbMinOut.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMaxOut, %class.btVector3** %aabbMaxOut.addr, align 4, !tbaa !2
  %0 = bitcast %class.btVector3* %localHalfExtents to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store float 5.000000e-01, float* %ref.tmp, align 4, !tbaa !14
  %2 = bitcast %class.btVector3* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #9
  %3 = load %class.btVector3*, %class.btVector3** %localAabbMax.addr, align 4, !tbaa !2
  %4 = load %class.btVector3*, %class.btVector3** %localAabbMin.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp1, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %4)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %localHalfExtents, float* nonnull align 4 dereferenceable(4) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1)
  %5 = bitcast %class.btVector3* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #9
  %6 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  %7 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #9
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp2, float* nonnull align 4 dereferenceable(4) %margin.addr, float* nonnull align 4 dereferenceable(4) %margin.addr, float* nonnull align 4 dereferenceable(4) %margin.addr)
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %localHalfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %8 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #9
  %9 = bitcast %class.btVector3* %localCenter to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #9
  %10 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  store float 5.000000e-01, float* %ref.tmp4, align 4, !tbaa !14
  %11 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #9
  %12 = load %class.btVector3*, %class.btVector3** %localAabbMax.addr, align 4, !tbaa !2
  %13 = load %class.btVector3*, %class.btVector3** %localAabbMin.addr, align 4, !tbaa !2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %12, %class.btVector3* nonnull align 4 dereferenceable(16) %13)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %localCenter, float* nonnull align 4 dereferenceable(4) %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %14 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %14) #9
  %15 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast %class.btMatrix3x3* %abs_b to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %16) #9
  %17 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %17)
  call void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* sret align 4 %abs_b, %class.btMatrix3x3* %call6)
  %18 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %18) #9
  %19 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4, !tbaa !2
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %center, %class.btTransform* %19, %class.btVector3* nonnull align 4 dereferenceable(16) %localCenter)
  %20 = bitcast %class.btVector3* %extent to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %20) #9
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 0)
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 1)
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %extent, %class.btVector3* %localHalfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %call7, %class.btVector3* nonnull align 4 dereferenceable(16) %call8, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  %21 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %21) #9
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp10, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %22 = load %class.btVector3*, %class.btVector3** %aabbMinOut.addr, align 4, !tbaa !2
  %23 = bitcast %class.btVector3* %22 to i8*
  %24 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false), !tbaa.struct !12
  %25 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %25) #9
  %26 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %26) #9
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp11, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %27 = load %class.btVector3*, %class.btVector3** %aabbMaxOut.addr, align 4, !tbaa !2
  %28 = bitcast %class.btVector3* %27 to i8*
  %29 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %29, i32 16, i1 false), !tbaa.struct !12
  %30 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %30) #9
  %31 = bitcast %class.btVector3* %extent to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %31) #9
  %32 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %32) #9
  %33 = bitcast %class.btMatrix3x3* %abs_b to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %33) #9
  %34 = bitcast %class.btVector3* %localCenter to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %34) #9
  %35 = bitcast %class.btVector3* %localHalfExtents to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %35) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #7 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !14
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !14
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !14
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !14
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !14
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !14
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !14
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !14
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !14
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #7 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %2 = load float, float* %call, align 4, !tbaa !14
  %call2 = call float @_Z6btFabsf(float %2)
  store float %call2, float* %ref.tmp, align 4, !tbaa !14
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 0
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx5)
  %4 = load float, float* %call6, align 4, !tbaa !14
  %call7 = call float @_Z6btFabsf(float %4)
  store float %call7, float* %ref.tmp3, align 4, !tbaa !14
  %5 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 0
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx10)
  %6 = load float, float* %call11, align 4, !tbaa !14
  %call12 = call float @_Z6btFabsf(float %6)
  store float %call12, float* %ref.tmp8, align 4, !tbaa !14
  %7 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 1
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx15)
  %8 = load float, float* %call16, align 4, !tbaa !14
  %call17 = call float @_Z6btFabsf(float %8)
  store float %call17, float* %ref.tmp13, align 4, !tbaa !14
  %9 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  %m_el19 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el19, i32 0, i32 1
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx20)
  %10 = load float, float* %call21, align 4, !tbaa !14
  %call22 = call float @_Z6btFabsf(float %10)
  store float %call22, float* %ref.tmp18, align 4, !tbaa !14
  %11 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  %m_el24 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el24, i32 0, i32 1
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx25)
  %12 = load float, float* %call26, align 4, !tbaa !14
  %call27 = call float @_Z6btFabsf(float %12)
  store float %call27, float* %ref.tmp23, align 4, !tbaa !14
  %13 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %m_el29 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el29, i32 0, i32 2
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx30)
  %14 = load float, float* %call31, align 4, !tbaa !14
  %call32 = call float @_Z6btFabsf(float %14)
  store float %call32, float* %ref.tmp28, align 4, !tbaa !14
  %15 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #9
  %m_el34 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx35 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el34, i32 0, i32 2
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx35)
  %16 = load float, float* %call36, align 4, !tbaa !14
  %call37 = call float @_Z6btFabsf(float %16)
  store float %call37, float* %ref.tmp33, align 4, !tbaa !14
  %17 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #9
  %m_el39 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx40 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el39, i32 0, i32 2
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx40)
  %18 = load float, float* %call41, align 4, !tbaa !14
  %call42 = call float @_Z6btFabsf(float %18)
  store float %call42, float* %ref.tmp38, align 4, !tbaa !14
  %call43 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp33, float* nonnull align 4 dereferenceable(4) %ref.tmp38)
  %19 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #9
  %20 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #9
  %21 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #9
  %22 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #9
  %23 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #9
  %24 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #9
  %25 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #9
  %26 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #9
  %27 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #7 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #7 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4, !tbaa !14
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !14
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !14
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #6 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !10
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #6 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !10
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

declare i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape*, i8*, %class.btSerializer*) unnamed_addr #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store i32 0, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !10
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %3 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %3
  %4 = load float, float* %arrayidx, align 4, !tbaa !14
  %5 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %5, i32 0, i32 0
  %6 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %6
  store float %4, float* %arrayidx3, align 4, !tbaa !14
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E8capacityEv(%class.btAlignedObjectArray.12* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !62
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI13GrahamVector3E7reserveEi(%class.btAlignedObjectArray.12* %this, i32 %_Count) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.GrahamVector3*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !10
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E8capacityEv(%class.btAlignedObjectArray.12* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !10
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.GrahamVector3** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !10
  %call2 = call i8* @_ZN20btAlignedObjectArrayI13GrahamVector3E8allocateEi(%class.btAlignedObjectArray.12* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.GrahamVector3*
  store %struct.GrahamVector3* %3, %struct.GrahamVector3** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %this1)
  %4 = load %struct.GrahamVector3*, %struct.GrahamVector3** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI13GrahamVector3E4copyEiiPS0_(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call3, %struct.GrahamVector3* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %this1)
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E7destroyEii(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E10deallocateEv(%class.btAlignedObjectArray.12* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !63
  %5 = load %struct.GrahamVector3*, %struct.GrahamVector3** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store %struct.GrahamVector3* %5, %struct.GrahamVector3** %m_data, align 4, !tbaa !44
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !10
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !62
  %7 = bitcast %struct.GrahamVector3** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI13GrahamVector3E9allocSizeEi(%class.btAlignedObjectArray.12* %this, i32 %size) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !10
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !10
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !10
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN9btVector3nwEmPv(i32 %0, i8* %ptr) #6 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !21
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI13GrahamVector3E8allocateEi(%class.btAlignedObjectArray.12* %this, i32 %size) #7 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !10
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !10
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !10
  %call = call %struct.GrahamVector3* @_ZN18btAlignedAllocatorI13GrahamVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.13* %m_allocator, i32 %1, %struct.GrahamVector3** null)
  %2 = bitcast %struct.GrahamVector3* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI13GrahamVector3E4copyEiiPS0_(%class.btAlignedObjectArray.12* %this, i32 %start, i32 %end, %struct.GrahamVector3* %dest) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.GrahamVector3*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !10
  store i32 %end, i32* %end.addr, align 4, !tbaa !10
  store %struct.GrahamVector3* %dest, %struct.GrahamVector3** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %start.addr, align 4, !tbaa !10
  store i32 %1, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !10
  %3 = load i32, i32* %end.addr, align 4, !tbaa !10
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.GrahamVector3*, %struct.GrahamVector3** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %4, i32 %5
  %6 = bitcast %struct.GrahamVector3* %arrayidx to i8*
  %call = call i8* @_ZN9btVector3nwEmPv(i32 24, i8* %6)
  %7 = bitcast i8* %call to %struct.GrahamVector3*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %8 = load %struct.GrahamVector3*, %struct.GrahamVector3** %m_data, align 4, !tbaa !44
  %9 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx2 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %8, i32 %9
  %10 = bitcast %struct.GrahamVector3* %7 to i8*
  %11 = bitcast %struct.GrahamVector3* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 24, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI13GrahamVector3E7destroyEii(%class.btAlignedObjectArray.12* %this, i32 %first, i32 %last) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !10
  store i32 %last, i32* %last.addr, align 4, !tbaa !10
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %first.addr, align 4, !tbaa !10
  store i32 %1, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !10
  %3 = load i32, i32* %last.addr, align 4, !tbaa !10
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %4 = load %struct.GrahamVector3*, %struct.GrahamVector3** %m_data, align 4, !tbaa !44
  %5 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI13GrahamVector3E10deallocateEv(%class.btAlignedObjectArray.12* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %0 = load %struct.GrahamVector3*, %struct.GrahamVector3** %m_data, align 4, !tbaa !44
  %tobool = icmp ne %struct.GrahamVector3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !63, !range !20
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %2 = load %struct.GrahamVector3*, %struct.GrahamVector3** %m_data4, align 4, !tbaa !44
  call void @_ZN18btAlignedAllocatorI13GrahamVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.13* %m_allocator, %struct.GrahamVector3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store %struct.GrahamVector3* null, %struct.GrahamVector3** %m_data5, align 4, !tbaa !44
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden %struct.GrahamVector3* @_ZN18btAlignedAllocatorI13GrahamVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.13* %this, i32 %n, %struct.GrahamVector3** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.13*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.GrahamVector3**, align 4
  store %class.btAlignedAllocator.13* %this, %class.btAlignedAllocator.13** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !10
  store %struct.GrahamVector3** %hint, %struct.GrahamVector3*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.13*, %class.btAlignedAllocator.13** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !10
  %mul = mul i32 24, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.GrahamVector3*
  ret %struct.GrahamVector3* %1
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI13GrahamVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.13* %this, %struct.GrahamVector3* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.13*, align 4
  %ptr.addr = alloca %struct.GrahamVector3*, align 4
  store %class.btAlignedAllocator.13* %this, %class.btAlignedAllocator.13** %this.addr, align 4, !tbaa !2
  store %struct.GrahamVector3* %ptr, %struct.GrahamVector3** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.13*, %class.btAlignedAllocator.13** %this.addr, align 4
  %0 = load %struct.GrahamVector3*, %struct.GrahamVector3** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.GrahamVector3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !64
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data, align 4, !tbaa !26
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !23
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !65
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !10
  store i32 %last, i32* %last.addr, align 4, !tbaa !10
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %first.addr, align 4, !tbaa !10
  store i32 %1, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !10
  %3 = load i32, i32* %last.addr, align 4, !tbaa !10
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !26
  %5 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !26
  %tobool = icmp ne %class.btVector3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !64, !range !20
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btVector3*, %class.btVector3** %m_data4, align 4, !tbaa !26
  call void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %class.btVector3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data5, align 4, !tbaa !26
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %class.btVector3* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.btVector3*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %ptr, %class.btVector3** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVector3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !65
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btVector3*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !10
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !10
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btVector3** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !10
  %call2 = call i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btVector3*
  store %class.btVector3* %3, %class.btVector3** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  %4 = load %class.btVector3*, %class.btVector3** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %class.btVector3* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !64
  %5 = load %class.btVector3*, %class.btVector3** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* %5, %class.btVector3** %m_data, align 4, !tbaa !26
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !10
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !65
  %7 = bitcast %class.btVector3** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !10
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !10
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !10
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #7 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !10
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !10
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !10
  %call = call %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %class.btVector3** null)
  %2 = bitcast %class.btVector3* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %class.btVector3* %dest) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !10
  store i32 %end, i32* %end.addr, align 4, !tbaa !10
  store %class.btVector3* %dest, %class.btVector3** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %start.addr, align 4, !tbaa !10
  store i32 %1, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !10
  %3 = load i32, i32* %end.addr, align 4, !tbaa !10
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btVector3*, %class.btVector3** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 %5
  %6 = bitcast %class.btVector3* %arrayidx to i8*
  %call = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %6)
  %7 = bitcast i8* %call to %class.btVector3*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !26
  %9 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 %9
  %10 = bitcast %class.btVector3* %7 to i8*
  %11 = bitcast %class.btVector3* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !12
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  ret void
}

define linkonce_odr hidden %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %class.btVector3** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btVector3**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !10
  store %class.btVector3** %hint, %class.btVector3*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !10
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btVector3*
  ret %class.btVector3* %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.3* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.3* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.3* %this1)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.3* %this1)
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorI6btFaceLj16EEC2Ev(%class.btAlignedAllocator.1* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  ret %class.btAlignedAllocator.1* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI6btFaceE4initEv(%class.btAlignedObjectArray.0* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !66
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.btFace* null, %struct.btFace** %m_data, align 4, !tbaa !30
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !33
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !67
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI6btFaceE5clearEv(%class.btAlignedObjectArray.0* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI6btFaceE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI6btFaceE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI6btFaceE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI6btFaceE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !10
  store i32 %last, i32* %last.addr, align 4, !tbaa !10
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %first.addr, align 4, !tbaa !10
  store i32 %1, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !10
  %3 = load i32, i32* %last.addr, align 4, !tbaa !10
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %4 = load %struct.btFace*, %struct.btFace** %m_data, align 4, !tbaa !30
  %5 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds %struct.btFace, %struct.btFace* %4, i32 %5
  %call = call %struct.btFace* @_ZN6btFaceD2Ev(%struct.btFace* %arrayidx) #9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI6btFaceE10deallocateEv(%class.btAlignedObjectArray.0* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btFace*, %struct.btFace** %m_data, align 4, !tbaa !30
  %tobool = icmp ne %struct.btFace* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !66, !range !20
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %struct.btFace*, %struct.btFace** %m_data4, align 4, !tbaa !30
  call void @_ZN18btAlignedAllocatorI6btFaceLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %m_allocator, %struct.btFace* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.btFace* null, %struct.btFace** %m_data5, align 4, !tbaa !30
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI6btFaceLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %this, %struct.btFace* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca %struct.btFace*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  store %struct.btFace* %ptr, %struct.btFace** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load %struct.btFace*, %struct.btFace** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btFace* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI6btFaceE7reserveEi(%class.btAlignedObjectArray.0* %this, i32 %_Count) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btFace*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !10
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !10
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btFace** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !10
  %call2 = call i8* @_ZN20btAlignedObjectArrayI6btFaceE8allocateEi(%class.btAlignedObjectArray.0* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.btFace*
  store %struct.btFace* %3, %struct.btFace** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %4 = load %struct.btFace*, %struct.btFace** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI6btFaceE4copyEiiPS0_(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call3, %struct.btFace* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI6btFaceE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI6btFaceE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !66
  %5 = load %struct.btFace*, %struct.btFace** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.btFace* %5, %struct.btFace** %m_data, align 4, !tbaa !30
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !10
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !67
  %7 = bitcast %struct.btFace** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI6btFaceE8capacityEv(%class.btAlignedObjectArray.0* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !67
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI6btFaceE8allocateEi(%class.btAlignedObjectArray.0* %this, i32 %size) #7 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !10
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !10
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !10
  %call = call %struct.btFace* @_ZN18btAlignedAllocatorI6btFaceLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %m_allocator, i32 %1, %struct.btFace** null)
  %2 = bitcast %struct.btFace* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI6btFaceE4copyEiiPS0_(%class.btAlignedObjectArray.0* %this, i32 %start, i32 %end, %struct.btFace* %dest) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btFace*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !10
  store i32 %end, i32* %end.addr, align 4, !tbaa !10
  store %struct.btFace* %dest, %struct.btFace** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %start.addr, align 4, !tbaa !10
  store i32 %1, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !10
  %3 = load i32, i32* %end.addr, align 4, !tbaa !10
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.btFace*, %struct.btFace** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds %struct.btFace, %struct.btFace* %4, i32 %5
  %6 = bitcast %struct.btFace* %arrayidx to i8*
  %7 = bitcast i8* %6 to %struct.btFace*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %8 = load %struct.btFace*, %struct.btFace** %m_data, align 4, !tbaa !30
  %9 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx2 = getelementptr inbounds %struct.btFace, %struct.btFace* %8, i32 %9
  %call = call %struct.btFace* @_ZN6btFaceC2ERKS_(%struct.btFace* %7, %struct.btFace* nonnull align 4 dereferenceable(36) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  ret void
}

define linkonce_odr hidden %struct.btFace* @_ZN18btAlignedAllocatorI6btFaceLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %this, i32 %n, %struct.btFace** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btFace**, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !10
  store %struct.btFace** %hint, %struct.btFace*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !10
  %mul = mul i32 36, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btFace*
  ret %struct.btFace* %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIiE9allocSizeEi(%class.btAlignedObjectArray.3* %this, i32 %size) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !10
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !10
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !10
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE16findLinearSearchERKi(%class.btAlignedObjectArray.3* %this, i32* nonnull align 4 dereferenceable(4) %key) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %key.addr = alloca i32*, align 4
  %index = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  store i32* %key, i32** %key.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %0 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  store i32 %call, i32* %index, align 4, !tbaa !10
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store i32 0, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !10
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  %cmp = icmp slt i32 %2, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %3 = load i32*, i32** %m_data, align 4, !tbaa !34
  %4 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  %5 = load i32, i32* %arrayidx, align 4, !tbaa !10
  %6 = load i32*, i32** %key.addr, align 4, !tbaa !2
  %7 = load i32, i32* %6, align 4, !tbaa !10
  %cmp3 = icmp eq i32 %5, %7
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4, !tbaa !10
  store i32 %8, i32* %index, align 4, !tbaa !10
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %9 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %10 = load i32, i32* %index, align 4, !tbaa !10
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  %12 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  ret i32 %10
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE4swapEii(%class.btAlignedObjectArray.3* %this, i32 %index0, i32 %index1) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  store i32 %index0, i32* %index0.addr, align 4, !tbaa !10
  store i32 %index1, i32* %index1.addr, align 4, !tbaa !10
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %0 = bitcast i32* %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %1 = load i32*, i32** %m_data, align 4, !tbaa !34
  %2 = load i32, i32* %index0.addr, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds i32, i32* %1, i32 %2
  %3 = load i32, i32* %arrayidx, align 4, !tbaa !10
  store i32 %3, i32* %temp, align 4, !tbaa !10
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %4 = load i32*, i32** %m_data2, align 4, !tbaa !34
  %5 = load i32, i32* %index1.addr, align 4, !tbaa !10
  %arrayidx3 = getelementptr inbounds i32, i32* %4, i32 %5
  %6 = load i32, i32* %arrayidx3, align 4, !tbaa !10
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %7 = load i32*, i32** %m_data4, align 4, !tbaa !34
  %8 = load i32, i32* %index0.addr, align 4, !tbaa !10
  %arrayidx5 = getelementptr inbounds i32, i32* %7, i32 %8
  store i32 %6, i32* %arrayidx5, align 4, !tbaa !10
  %9 = load i32, i32* %temp, align 4, !tbaa !10
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %10 = load i32*, i32** %m_data6, align 4, !tbaa !34
  %11 = load i32, i32* %index1.addr, align 4, !tbaa !10
  %arrayidx7 = getelementptr inbounds i32, i32* %10, i32 %11
  store i32 %9, i32* %arrayidx7, align 4, !tbaa !10
  %12 = bitcast i32* %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.13* @_ZN18btAlignedAllocatorI13GrahamVector3Lj16EEC2Ev(%class.btAlignedAllocator.13* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.13*, align 4
  store %class.btAlignedAllocator.13* %this, %class.btAlignedAllocator.13** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.13*, %class.btAlignedAllocator.13** %this.addr, align 4
  ret %class.btAlignedAllocator.13* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI13GrahamVector3E4initEv(%class.btAlignedObjectArray.12* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !63
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store %struct.GrahamVector3* null, %struct.GrahamVector3** %m_data, align 4, !tbaa !44
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !41
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !62
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI13GrahamVector3E5clearEv(%class.btAlignedObjectArray.12* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %this1)
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E7destroyEii(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E10deallocateEv(%class.btAlignedObjectArray.12* %this1)
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E4initEv(%class.btAlignedObjectArray.12* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI6btFaceE9allocSizeEi(%class.btAlignedObjectArray.0* %this, i32 %size) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !10
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !10
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !10
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { cold noreturn nounwind }
attributes #5 = { argmemonly nounwind willreturn }
attributes #6 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { argmemonly nounwind willreturn writeonly }
attributes #9 = { nounwind }
attributes #10 = { nounwind readnone speculatable willreturn }
attributes #11 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{!9, !3, i64 52}
!9 = !{!"_ZTS23btPolyhedralConvexShape", !3, i64 52}
!10 = !{!11, !11, i64 0}
!11 = !{!"int", !4, i64 0}
!12 = !{i64 0, i64 16, !13}
!13 = !{!4, !4, i64 0}
!14 = !{!15, !15, i64 0}
!15 = !{!"float", !4, i64 0}
!16 = !{!17, !17, i64 0}
!17 = !{!"bool", !4, i64 0}
!18 = !{!19, !11, i64 20}
!19 = !{!"_ZTS13GrahamVector3", !15, i64 16, !11, i64 20}
!20 = !{i8 0, i8 2}
!21 = !{!22, !22, i64 0}
!22 = !{!"long", !4, i64 0}
!23 = !{!24, !11, i64 4}
!24 = !{!"_ZTS20btAlignedObjectArrayI9btVector3E", !25, i64 0, !11, i64 4, !11, i64 8, !3, i64 12, !17, i64 16}
!25 = !{!"_ZTS18btAlignedAllocatorI9btVector3Lj16EE"}
!26 = !{!24, !3, i64 12}
!27 = !{!28, !11, i64 4}
!28 = !{!"_ZTS20btAlignedObjectArrayIiE", !29, i64 0, !11, i64 4, !11, i64 8, !3, i64 12, !17, i64 16}
!29 = !{!"_ZTS18btAlignedAllocatorIiLj16EE"}
!30 = !{!31, !3, i64 12}
!31 = !{!"_ZTS20btAlignedObjectArrayI6btFaceE", !32, i64 0, !11, i64 4, !11, i64 8, !3, i64 12, !17, i64 16}
!32 = !{!"_ZTS18btAlignedAllocatorI6btFaceLj16EE"}
!33 = !{!31, !11, i64 4}
!34 = !{!28, !3, i64 12}
!35 = !{!36, !3, i64 12}
!36 = !{!"_ZTS20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE", !37, i64 0, !11, i64 4, !11, i64 8, !3, i64 12, !17, i64 16}
!37 = !{!"_ZTS18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EE"}
!38 = !{!39, !11, i64 4}
!39 = !{!"_ZTSN20btConvexHullComputer4EdgeE", !11, i64 0, !11, i64 4, !11, i64 8}
!40 = !{!39, !11, i64 8}
!41 = !{!42, !11, i64 4}
!42 = !{!"_ZTS20btAlignedObjectArrayI13GrahamVector3E", !43, i64 0, !11, i64 4, !11, i64 8, !3, i64 12, !17, i64 16}
!43 = !{!"_ZTS18btAlignedAllocatorI13GrahamVector3Lj16EE"}
!44 = !{!42, !3, i64 12}
!45 = !{!19, !15, i64 16}
!46 = !{!47, !17, i64 88}
!47 = !{!"_ZTS34btPolyhedralConvexAabbCachingShape", !48, i64 56, !48, i64 72, !17, i64 88}
!48 = !{!"_ZTS9btVector3", !4, i64 0}
!49 = !{!"branch_weights", i32 1, i32 1048575}
!50 = !{!51, !15, i64 44}
!51 = !{!"_ZTS21btConvexInternalShape", !48, i64 12, !48, i64 28, !15, i64 44, !15, i64 48}
!52 = !{!53, !15, i64 44}
!53 = !{!"_ZTS25btConvexInternalShapeData", !54, i64 0, !55, i64 12, !55, i64 28, !15, i64 44, !11, i64 48}
!54 = !{!"_ZTS20btCollisionShapeData", !3, i64 0, !11, i64 4, !4, i64 8}
!55 = !{!"_ZTS18btVector3FloatData", !4, i64 0}
!56 = !{!36, !17, i64 16}
!57 = !{!36, !11, i64 4}
!58 = !{!36, !11, i64 8}
!59 = !{!39, !11, i64 0}
!60 = !{!28, !17, i64 16}
!61 = !{!28, !11, i64 8}
!62 = !{!42, !11, i64 8}
!63 = !{!42, !17, i64 16}
!64 = !{!24, !17, i64 16}
!65 = !{!24, !11, i64 8}
!66 = !{!31, !17, i64 16}
!67 = !{!31, !11, i64 8}
