; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btConvexPolyhedron.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btConvexPolyhedron.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btConvexPolyhedron = type { i32 (...)**, %class.btAlignedObjectArray, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.btFace*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btFace = type { %class.btAlignedObjectArray.3, [4 x float] }
%class.btAlignedObjectArray.3 = type <{ %class.btAlignedAllocator.4, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.4 = type { i8 }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btVector3 = type { [4 x float] }
%class.btHashMap = type { %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, %struct.btInternalEdge*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%struct.btInternalEdge = type { i16, i16 }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, %struct.btInternalVertexPair*, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%struct.btInternalVertexPair = type { i16, i16 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }

$_ZN20btAlignedObjectArrayI9btVector3EC2Ev = comdat any

$_ZN20btAlignedObjectArrayI6btFaceEC2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3ED2Ev = comdat any

$_ZN20btAlignedObjectArrayI6btFaceED2Ev = comdat any

$_ZN18btConvexPolyhedrondlEPv = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayI6btFaceEixEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN9btHashMapI20btInternalVertexPair14btInternalEdgeEC2Ev = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN20btAlignedObjectArrayI6btFaceEixEi = comdat any

$_ZNK20btAlignedObjectArrayIiE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIiEixEi = comdat any

$_ZN20btInternalVertexPairC2Ess = comdat any

$_ZN9btHashMapI20btInternalVertexPair14btInternalEdgeE4findERKS0_ = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EixEi = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv = comdat any

$_Z12IsAlmostZeroRK9btVector3 = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_ = comdat any

$_ZN14btInternalEdgeC2Ev = comdat any

$_ZN9btHashMapI20btInternalVertexPair14btInternalEdgeE6insertERKS0_RKS1_ = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZdvRK9btVector3RKf = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZN9btVector3dVERKf = comdat any

$_Z6btFabsf = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZNK9btVector37maxAxisEv = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN9btHashMapI20btInternalVertexPair14btInternalEdgeED2Ev = comdat any

$_ZNK11btTransformmlERK9btVector3 = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3EixEi = comdat any

$_Z6btSwapIfEvRT_S1_ = comdat any

$_Z6btSwapI9btVector3EvRT_S2_ = comdat any

$_ZN20btAlignedObjectArrayIiEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI14btInternalEdgeEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI20btInternalVertexPairEC2Ev = comdat any

$_ZN18btAlignedAllocatorIiLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiE4initEv = comdat any

$_ZN18btAlignedAllocatorI14btInternalEdgeLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI14btInternalEdgeE4initEv = comdat any

$_ZN18btAlignedAllocatorI20btInternalVertexPairLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI20btInternalVertexPairE4initEv = comdat any

$_Z6btSwapIsEvRT_S1_ = comdat any

$_Z6btSqrtf = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN20btAlignedObjectArrayI20btInternalVertexPairED2Ev = comdat any

$_ZN20btAlignedObjectArrayI14btInternalEdgeED2Ev = comdat any

$_ZN20btAlignedObjectArrayIiED2Ev = comdat any

$_ZN20btAlignedObjectArrayI20btInternalVertexPairE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI20btInternalVertexPairE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayI20btInternalVertexPairE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI20btInternalVertexPairE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI20btInternalVertexPairLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI14btInternalEdgeE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI14btInternalEdgeE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayI14btInternalEdgeE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI14btInternalEdgeE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI14btInternalEdgeLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayIiE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIiE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIiE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E4initEv = comdat any

$_ZN18btAlignedAllocatorI6btFaceLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI6btFaceE4initEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E5clearEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI6btFaceE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI6btFaceE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI6btFaceE10deallocateEv = comdat any

$_ZN6btFaceD2Ev = comdat any

$_ZN18btAlignedAllocatorI6btFaceLj16EE10deallocateEPS0_ = comdat any

$_ZNK9btHashMapI20btInternalVertexPair14btInternalEdgeE9findIndexERKS0_ = comdat any

$_ZN20btAlignedObjectArrayI14btInternalEdgeEixEi = comdat any

$_ZNK20btInternalVertexPair7getHashEv = comdat any

$_ZNK20btAlignedObjectArrayI14btInternalEdgeE8capacityEv = comdat any

$_ZNK20btAlignedObjectArrayIiEixEi = comdat any

$_ZNK20btInternalVertexPair6equalsERKS_ = comdat any

$_ZNK20btAlignedObjectArrayI20btInternalVertexPairEixEi = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi = comdat any

$_ZN9btVector3nwEmPv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAlignedObjectArrayI14btInternalEdgeE9push_backERKS0_ = comdat any

$_ZN20btAlignedObjectArrayI20btInternalVertexPairE9push_backERKS0_ = comdat any

$_ZN9btHashMapI20btInternalVertexPair14btInternalEdgeE10growTablesERKS0_ = comdat any

$_ZN20btAlignedObjectArrayI14btInternalEdgeE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI14btInternalEdgeE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayI14btInternalEdgeE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI14btInternalEdgeE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI14btInternalEdgeLj16EE8allocateEiPPKS0_ = comdat any

$_ZNK20btAlignedObjectArrayI20btInternalVertexPairE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI20btInternalVertexPairE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI20btInternalVertexPairE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayI20btInternalVertexPairE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI20btInternalVertexPairE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI20btInternalVertexPairLj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAlignedObjectArrayIiE6resizeEiRKi = comdat any

$_ZN20btAlignedObjectArrayI20btInternalVertexPairEixEi = comdat any

$_ZN20btAlignedObjectArrayIiE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIiE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIiE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIiE4copyEiiPi = comdat any

$_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi = comdat any

@_ZTV18btConvexPolyhedron = hidden unnamed_addr constant { [4 x i8*] } { [4 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI18btConvexPolyhedron to i8*), i8* bitcast (%class.btConvexPolyhedron* (%class.btConvexPolyhedron*)* @_ZN18btConvexPolyhedronD1Ev to i8*), i8* bitcast (void (%class.btConvexPolyhedron*)* @_ZN18btConvexPolyhedronD0Ev to i8*)] }, align 4
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS18btConvexPolyhedron = hidden constant [21 x i8] c"18btConvexPolyhedron\00", align 1
@_ZTI18btConvexPolyhedron = hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([21 x i8], [21 x i8]* @_ZTS18btConvexPolyhedron, i32 0, i32 0) }, align 4

@_ZN18btConvexPolyhedronC1Ev = hidden unnamed_addr alias %class.btConvexPolyhedron* (%class.btConvexPolyhedron*), %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)* @_ZN18btConvexPolyhedronC2Ev
@_ZN18btConvexPolyhedronD1Ev = hidden unnamed_addr alias %class.btConvexPolyhedron* (%class.btConvexPolyhedron*), %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)* @_ZN18btConvexPolyhedronD2Ev

define hidden %class.btConvexPolyhedron* @_ZN18btConvexPolyhedronC2Ev(%class.btConvexPolyhedron* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConvexPolyhedron*, align 4
  store %class.btConvexPolyhedron* %this, %class.btConvexPolyhedron** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %this.addr, align 4
  %0 = bitcast %class.btConvexPolyhedron* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [4 x i8*] }, { [4 x i8*] }* @_ZTV18btConvexPolyhedron, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  %m_vertices = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 1
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* %m_vertices)
  %m_faces = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %call2 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI6btFaceEC2Ev(%class.btAlignedObjectArray.0* %m_faces)
  %m_uniqueEdges = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 3
  %call3 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* %m_uniqueEdges)
  %m_localCenter = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 4
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_localCenter)
  %m_extents = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_extents)
  %mC = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 7
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %mC)
  %mE = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 8
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %mE)
  ret %class.btConvexPolyhedron* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI6btFaceEC2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorI6btFaceLj16EEC2Ev(%class.btAlignedAllocator.1* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI6btFaceE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: nounwind
define hidden %class.btConvexPolyhedron* @_ZN18btConvexPolyhedronD2Ev(%class.btConvexPolyhedron* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btConvexPolyhedron*, align 4
  store %class.btConvexPolyhedron* %this, %class.btConvexPolyhedron** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %this.addr, align 4
  %0 = bitcast %class.btConvexPolyhedron* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [4 x i8*] }, { [4 x i8*] }* @_ZTV18btConvexPolyhedron, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  %m_uniqueEdges = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 3
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* %m_uniqueEdges) #7
  %m_faces = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %call2 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI6btFaceED2Ev(%class.btAlignedObjectArray.0* %m_faces) #7
  %m_vertices = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 1
  %call3 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* %m_vertices) #7
  ret %class.btConvexPolyhedron* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI6btFaceED2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI6btFaceE5clearEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN18btConvexPolyhedronD0Ev(%class.btConvexPolyhedron* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btConvexPolyhedron*, align 4
  store %class.btConvexPolyhedron* %this, %class.btConvexPolyhedron** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %this.addr, align 4
  %call = call %class.btConvexPolyhedron* @_ZN18btConvexPolyhedronD1Ev(%class.btConvexPolyhedron* %this1) #7
  %0 = bitcast %class.btConvexPolyhedron* %this1 to i8*
  call void @_ZN18btConvexPolyhedrondlEPv(i8* %0) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN18btConvexPolyhedrondlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

define hidden zeroext i1 @_ZNK18btConvexPolyhedron15testContainmentEv(%class.btConvexPolyhedron* %this) #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btConvexPolyhedron*, align 4
  %p = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %LocalPt = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %ref.tmp14 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca %class.btVector3, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp31 = alloca %class.btVector3, align 4
  %ref.tmp33 = alloca %class.btVector3, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp49 = alloca %class.btVector3, align 4
  %ref.tmp51 = alloca %class.btVector3, align 4
  %ref.tmp55 = alloca float, align 4
  %ref.tmp60 = alloca float, align 4
  %ref.tmp69 = alloca %class.btVector3, align 4
  %ref.tmp71 = alloca %class.btVector3, align 4
  %ref.tmp72 = alloca float, align 4
  %ref.tmp87 = alloca %class.btVector3, align 4
  %ref.tmp89 = alloca %class.btVector3, align 4
  %ref.tmp90 = alloca float, align 4
  %ref.tmp98 = alloca float, align 4
  %ref.tmp107 = alloca %class.btVector3, align 4
  %ref.tmp109 = alloca %class.btVector3, align 4
  %ref.tmp110 = alloca float, align 4
  %ref.tmp115 = alloca float, align 4
  %ref.tmp127 = alloca %class.btVector3, align 4
  %ref.tmp129 = alloca %class.btVector3, align 4
  %ref.tmp130 = alloca float, align 4
  %ref.tmp135 = alloca float, align 4
  %ref.tmp140 = alloca float, align 4
  %i = alloca i32, align 4
  %Normal = alloca %class.btVector3, align 4
  %d = alloca float, align 4
  store %class.btConvexPolyhedron* %this, %class.btConvexPolyhedron** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %this.addr, align 4
  %0 = bitcast i32* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  store i32 0, i32* %p, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc184, %entry
  %1 = load i32, i32* %p, align 4, !tbaa !8
  %cmp = icmp slt i32 %1, 8
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup186

for.body:                                         ; preds = %for.cond
  %2 = bitcast %class.btVector3* %LocalPt to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #7
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %LocalPt)
  %3 = load i32, i32* %p, align 4, !tbaa !8
  %cmp2 = icmp eq i32 %3, 0
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #7
  %m_localCenter = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 4
  %5 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #7
  %m_extents = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call4 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents)
  %arrayidx = getelementptr inbounds float, float* %call4, i32 0
  %m_extents5 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents5)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 1
  %m_extents8 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents8)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 2
  %call11 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp3, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx7, float* nonnull align 4 dereferenceable(4) %arrayidx10)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localCenter, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  %6 = bitcast %class.btVector3* %LocalPt to i8*
  %7 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !10
  %8 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #7
  %9 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #7
  br label %if.end152

if.else:                                          ; preds = %for.body
  %10 = load i32, i32* %p, align 4, !tbaa !8
  %cmp12 = icmp eq i32 %10, 1
  br i1 %cmp12, label %if.then13, label %if.else28

if.then13:                                        ; preds = %if.else
  %11 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #7
  %m_localCenter15 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 4
  %12 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %12) #7
  %m_extents17 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call18 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents17)
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 0
  %m_extents20 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call21 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents20)
  %arrayidx22 = getelementptr inbounds float, float* %call21, i32 1
  %13 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %m_extents24 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call25 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents24)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 2
  %14 = load float, float* %arrayidx26, align 4, !tbaa !12
  %fneg = fneg float %14
  store float %fneg, float* %ref.tmp23, align 4, !tbaa !12
  %call27 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp16, float* nonnull align 4 dereferenceable(4) %arrayidx19, float* nonnull align 4 dereferenceable(4) %arrayidx22, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp14, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localCenter15, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp16)
  %15 = bitcast %class.btVector3* %LocalPt to i8*
  %16 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 16, i1 false), !tbaa.struct !10
  %17 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  %18 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #7
  %19 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %19) #7
  br label %if.end151

if.else28:                                        ; preds = %if.else
  %20 = load i32, i32* %p, align 4, !tbaa !8
  %cmp29 = icmp eq i32 %20, 2
  br i1 %cmp29, label %if.then30, label %if.else46

if.then30:                                        ; preds = %if.else28
  %21 = bitcast %class.btVector3* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %21) #7
  %m_localCenter32 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 4
  %22 = bitcast %class.btVector3* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %22) #7
  %m_extents34 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call35 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents34)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 0
  %23 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #7
  %m_extents38 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call39 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents38)
  %arrayidx40 = getelementptr inbounds float, float* %call39, i32 1
  %24 = load float, float* %arrayidx40, align 4, !tbaa !12
  %fneg41 = fneg float %24
  store float %fneg41, float* %ref.tmp37, align 4, !tbaa !12
  %m_extents42 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call43 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents42)
  %arrayidx44 = getelementptr inbounds float, float* %call43, i32 2
  %call45 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp33, float* nonnull align 4 dereferenceable(4) %arrayidx36, float* nonnull align 4 dereferenceable(4) %ref.tmp37, float* nonnull align 4 dereferenceable(4) %arrayidx44)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp31, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localCenter32, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp33)
  %25 = bitcast %class.btVector3* %LocalPt to i8*
  %26 = bitcast %class.btVector3* %ref.tmp31 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %25, i8* align 4 %26, i32 16, i1 false), !tbaa.struct !10
  %27 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #7
  %28 = bitcast %class.btVector3* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %28) #7
  %29 = bitcast %class.btVector3* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #7
  br label %if.end150

if.else46:                                        ; preds = %if.else28
  %30 = load i32, i32* %p, align 4, !tbaa !8
  %cmp47 = icmp eq i32 %30, 3
  br i1 %cmp47, label %if.then48, label %if.else66

if.then48:                                        ; preds = %if.else46
  %31 = bitcast %class.btVector3* %ref.tmp49 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %31) #7
  %m_localCenter50 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 4
  %32 = bitcast %class.btVector3* %ref.tmp51 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %32) #7
  %m_extents52 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call53 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents52)
  %arrayidx54 = getelementptr inbounds float, float* %call53, i32 0
  %33 = bitcast float* %ref.tmp55 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #7
  %m_extents56 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call57 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents56)
  %arrayidx58 = getelementptr inbounds float, float* %call57, i32 1
  %34 = load float, float* %arrayidx58, align 4, !tbaa !12
  %fneg59 = fneg float %34
  store float %fneg59, float* %ref.tmp55, align 4, !tbaa !12
  %35 = bitcast float* %ref.tmp60 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #7
  %m_extents61 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call62 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents61)
  %arrayidx63 = getelementptr inbounds float, float* %call62, i32 2
  %36 = load float, float* %arrayidx63, align 4, !tbaa !12
  %fneg64 = fneg float %36
  store float %fneg64, float* %ref.tmp60, align 4, !tbaa !12
  %call65 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp51, float* nonnull align 4 dereferenceable(4) %arrayidx54, float* nonnull align 4 dereferenceable(4) %ref.tmp55, float* nonnull align 4 dereferenceable(4) %ref.tmp60)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp49, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localCenter50, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp51)
  %37 = bitcast %class.btVector3* %LocalPt to i8*
  %38 = bitcast %class.btVector3* %ref.tmp49 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %37, i8* align 4 %38, i32 16, i1 false), !tbaa.struct !10
  %39 = bitcast float* %ref.tmp60 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #7
  %40 = bitcast float* %ref.tmp55 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #7
  %41 = bitcast %class.btVector3* %ref.tmp51 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %41) #7
  %42 = bitcast %class.btVector3* %ref.tmp49 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %42) #7
  br label %if.end149

if.else66:                                        ; preds = %if.else46
  %43 = load i32, i32* %p, align 4, !tbaa !8
  %cmp67 = icmp eq i32 %43, 4
  br i1 %cmp67, label %if.then68, label %if.else84

if.then68:                                        ; preds = %if.else66
  %44 = bitcast %class.btVector3* %ref.tmp69 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %44) #7
  %m_localCenter70 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 4
  %45 = bitcast %class.btVector3* %ref.tmp71 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %45) #7
  %46 = bitcast float* %ref.tmp72 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #7
  %m_extents73 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call74 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents73)
  %arrayidx75 = getelementptr inbounds float, float* %call74, i32 0
  %47 = load float, float* %arrayidx75, align 4, !tbaa !12
  %fneg76 = fneg float %47
  store float %fneg76, float* %ref.tmp72, align 4, !tbaa !12
  %m_extents77 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call78 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents77)
  %arrayidx79 = getelementptr inbounds float, float* %call78, i32 1
  %m_extents80 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call81 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents80)
  %arrayidx82 = getelementptr inbounds float, float* %call81, i32 2
  %call83 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp71, float* nonnull align 4 dereferenceable(4) %ref.tmp72, float* nonnull align 4 dereferenceable(4) %arrayidx79, float* nonnull align 4 dereferenceable(4) %arrayidx82)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp69, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localCenter70, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp71)
  %48 = bitcast %class.btVector3* %LocalPt to i8*
  %49 = bitcast %class.btVector3* %ref.tmp69 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %48, i8* align 4 %49, i32 16, i1 false), !tbaa.struct !10
  %50 = bitcast float* %ref.tmp72 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #7
  %51 = bitcast %class.btVector3* %ref.tmp71 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %51) #7
  %52 = bitcast %class.btVector3* %ref.tmp69 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %52) #7
  br label %if.end148

if.else84:                                        ; preds = %if.else66
  %53 = load i32, i32* %p, align 4, !tbaa !8
  %cmp85 = icmp eq i32 %53, 5
  br i1 %cmp85, label %if.then86, label %if.else104

if.then86:                                        ; preds = %if.else84
  %54 = bitcast %class.btVector3* %ref.tmp87 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %54) #7
  %m_localCenter88 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 4
  %55 = bitcast %class.btVector3* %ref.tmp89 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %55) #7
  %56 = bitcast float* %ref.tmp90 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #7
  %m_extents91 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call92 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents91)
  %arrayidx93 = getelementptr inbounds float, float* %call92, i32 0
  %57 = load float, float* %arrayidx93, align 4, !tbaa !12
  %fneg94 = fneg float %57
  store float %fneg94, float* %ref.tmp90, align 4, !tbaa !12
  %m_extents95 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call96 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents95)
  %arrayidx97 = getelementptr inbounds float, float* %call96, i32 1
  %58 = bitcast float* %ref.tmp98 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #7
  %m_extents99 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call100 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents99)
  %arrayidx101 = getelementptr inbounds float, float* %call100, i32 2
  %59 = load float, float* %arrayidx101, align 4, !tbaa !12
  %fneg102 = fneg float %59
  store float %fneg102, float* %ref.tmp98, align 4, !tbaa !12
  %call103 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp89, float* nonnull align 4 dereferenceable(4) %ref.tmp90, float* nonnull align 4 dereferenceable(4) %arrayidx97, float* nonnull align 4 dereferenceable(4) %ref.tmp98)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp87, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localCenter88, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp89)
  %60 = bitcast %class.btVector3* %LocalPt to i8*
  %61 = bitcast %class.btVector3* %ref.tmp87 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %60, i8* align 4 %61, i32 16, i1 false), !tbaa.struct !10
  %62 = bitcast float* %ref.tmp98 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #7
  %63 = bitcast float* %ref.tmp90 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #7
  %64 = bitcast %class.btVector3* %ref.tmp89 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %64) #7
  %65 = bitcast %class.btVector3* %ref.tmp87 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %65) #7
  br label %if.end147

if.else104:                                       ; preds = %if.else84
  %66 = load i32, i32* %p, align 4, !tbaa !8
  %cmp105 = icmp eq i32 %66, 6
  br i1 %cmp105, label %if.then106, label %if.else124

if.then106:                                       ; preds = %if.else104
  %67 = bitcast %class.btVector3* %ref.tmp107 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %67) #7
  %m_localCenter108 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 4
  %68 = bitcast %class.btVector3* %ref.tmp109 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %68) #7
  %69 = bitcast float* %ref.tmp110 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #7
  %m_extents111 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call112 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents111)
  %arrayidx113 = getelementptr inbounds float, float* %call112, i32 0
  %70 = load float, float* %arrayidx113, align 4, !tbaa !12
  %fneg114 = fneg float %70
  store float %fneg114, float* %ref.tmp110, align 4, !tbaa !12
  %71 = bitcast float* %ref.tmp115 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #7
  %m_extents116 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call117 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents116)
  %arrayidx118 = getelementptr inbounds float, float* %call117, i32 1
  %72 = load float, float* %arrayidx118, align 4, !tbaa !12
  %fneg119 = fneg float %72
  store float %fneg119, float* %ref.tmp115, align 4, !tbaa !12
  %m_extents120 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call121 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents120)
  %arrayidx122 = getelementptr inbounds float, float* %call121, i32 2
  %call123 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp109, float* nonnull align 4 dereferenceable(4) %ref.tmp110, float* nonnull align 4 dereferenceable(4) %ref.tmp115, float* nonnull align 4 dereferenceable(4) %arrayidx122)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp107, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localCenter108, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp109)
  %73 = bitcast %class.btVector3* %LocalPt to i8*
  %74 = bitcast %class.btVector3* %ref.tmp107 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %73, i8* align 4 %74, i32 16, i1 false), !tbaa.struct !10
  %75 = bitcast float* %ref.tmp115 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #7
  %76 = bitcast float* %ref.tmp110 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #7
  %77 = bitcast %class.btVector3* %ref.tmp109 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %77) #7
  %78 = bitcast %class.btVector3* %ref.tmp107 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %78) #7
  br label %if.end146

if.else124:                                       ; preds = %if.else104
  %79 = load i32, i32* %p, align 4, !tbaa !8
  %cmp125 = icmp eq i32 %79, 7
  br i1 %cmp125, label %if.then126, label %if.end

if.then126:                                       ; preds = %if.else124
  %80 = bitcast %class.btVector3* %ref.tmp127 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %80) #7
  %m_localCenter128 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 4
  %81 = bitcast %class.btVector3* %ref.tmp129 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %81) #7
  %82 = bitcast float* %ref.tmp130 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #7
  %m_extents131 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call132 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents131)
  %arrayidx133 = getelementptr inbounds float, float* %call132, i32 0
  %83 = load float, float* %arrayidx133, align 4, !tbaa !12
  %fneg134 = fneg float %83
  store float %fneg134, float* %ref.tmp130, align 4, !tbaa !12
  %84 = bitcast float* %ref.tmp135 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %84) #7
  %m_extents136 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call137 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents136)
  %arrayidx138 = getelementptr inbounds float, float* %call137, i32 1
  %85 = load float, float* %arrayidx138, align 4, !tbaa !12
  %fneg139 = fneg float %85
  store float %fneg139, float* %ref.tmp135, align 4, !tbaa !12
  %86 = bitcast float* %ref.tmp140 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %86) #7
  %m_extents141 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call142 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents141)
  %arrayidx143 = getelementptr inbounds float, float* %call142, i32 2
  %87 = load float, float* %arrayidx143, align 4, !tbaa !12
  %fneg144 = fneg float %87
  store float %fneg144, float* %ref.tmp140, align 4, !tbaa !12
  %call145 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp129, float* nonnull align 4 dereferenceable(4) %ref.tmp130, float* nonnull align 4 dereferenceable(4) %ref.tmp135, float* nonnull align 4 dereferenceable(4) %ref.tmp140)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp127, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localCenter128, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp129)
  %88 = bitcast %class.btVector3* %LocalPt to i8*
  %89 = bitcast %class.btVector3* %ref.tmp127 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %88, i8* align 4 %89, i32 16, i1 false), !tbaa.struct !10
  %90 = bitcast float* %ref.tmp140 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #7
  %91 = bitcast float* %ref.tmp135 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #7
  %92 = bitcast float* %ref.tmp130 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #7
  %93 = bitcast %class.btVector3* %ref.tmp129 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %93) #7
  %94 = bitcast %class.btVector3* %ref.tmp127 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %94) #7
  br label %if.end

if.end:                                           ; preds = %if.then126, %if.else124
  br label %if.end146

if.end146:                                        ; preds = %if.end, %if.then106
  br label %if.end147

if.end147:                                        ; preds = %if.end146, %if.then86
  br label %if.end148

if.end148:                                        ; preds = %if.end147, %if.then68
  br label %if.end149

if.end149:                                        ; preds = %if.end148, %if.then48
  br label %if.end150

if.end150:                                        ; preds = %if.end149, %if.then30
  br label %if.end151

if.end151:                                        ; preds = %if.end150, %if.then13
  br label %if.end152

if.end152:                                        ; preds = %if.end151, %if.then
  %95 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %95) #7
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond153

for.cond153:                                      ; preds = %for.inc, %if.end152
  %96 = load i32, i32* %i, align 4, !tbaa !8
  %m_faces = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %call154 = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %m_faces)
  %cmp155 = icmp slt i32 %96, %call154
  br i1 %cmp155, label %for.body157, label %for.cond.cleanup156

for.cond.cleanup156:                              ; preds = %for.cond153
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup179

for.body157:                                      ; preds = %for.cond153
  %97 = bitcast %class.btVector3* %Normal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %97) #7
  %m_faces158 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %98 = load i32, i32* %i, align 4, !tbaa !8
  %call159 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces158, i32 %98)
  %m_plane = getelementptr inbounds %struct.btFace, %struct.btFace* %call159, i32 0, i32 1
  %arrayidx160 = getelementptr inbounds [4 x float], [4 x float]* %m_plane, i32 0, i32 0
  %m_faces161 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %99 = load i32, i32* %i, align 4, !tbaa !8
  %call162 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces161, i32 %99)
  %m_plane163 = getelementptr inbounds %struct.btFace, %struct.btFace* %call162, i32 0, i32 1
  %arrayidx164 = getelementptr inbounds [4 x float], [4 x float]* %m_plane163, i32 0, i32 1
  %m_faces165 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %100 = load i32, i32* %i, align 4, !tbaa !8
  %call166 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces165, i32 %100)
  %m_plane167 = getelementptr inbounds %struct.btFace, %struct.btFace* %call166, i32 0, i32 1
  %arrayidx168 = getelementptr inbounds [4 x float], [4 x float]* %m_plane167, i32 0, i32 2
  %call169 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %Normal, float* nonnull align 4 dereferenceable(4) %arrayidx160, float* nonnull align 4 dereferenceable(4) %arrayidx164, float* nonnull align 4 dereferenceable(4) %arrayidx168)
  %101 = bitcast float* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %101) #7
  %call170 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %LocalPt, %class.btVector3* nonnull align 4 dereferenceable(16) %Normal)
  %m_faces171 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %102 = load i32, i32* %i, align 4, !tbaa !8
  %call172 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces171, i32 %102)
  %m_plane173 = getelementptr inbounds %struct.btFace, %struct.btFace* %call172, i32 0, i32 1
  %arrayidx174 = getelementptr inbounds [4 x float], [4 x float]* %m_plane173, i32 0, i32 3
  %103 = load float, float* %arrayidx174, align 4, !tbaa !12
  %add = fadd float %call170, %103
  store float %add, float* %d, align 4, !tbaa !12
  %104 = load float, float* %d, align 4, !tbaa !12
  %cmp175 = fcmp ogt float %104, 0.000000e+00
  br i1 %cmp175, label %if.then176, label %if.end177

if.then176:                                       ; preds = %for.body157
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end177:                                        ; preds = %for.body157
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end177, %if.then176
  %105 = bitcast float* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #7
  %106 = bitcast %class.btVector3* %Normal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %106) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup179 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %107 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %107, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond153

cleanup179:                                       ; preds = %cleanup, %for.cond.cleanup156
  %108 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #7
  %cleanup.dest180 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest180, label %cleanup181 [
    i32 5, label %for.end
  ]

for.end:                                          ; preds = %cleanup179
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup181

cleanup181:                                       ; preds = %for.end, %cleanup179
  %109 = bitcast %class.btVector3* %LocalPt to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %109) #7
  %cleanup.dest182 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest182, label %cleanup186 [
    i32 0, label %cleanup.cont183
  ]

cleanup.cont183:                                  ; preds = %cleanup181
  br label %for.inc184

for.inc184:                                       ; preds = %cleanup.cont183
  %110 = load i32, i32* %p, align 4, !tbaa !8
  %inc185 = add nsw i32 %110, 1
  store i32 %inc185, i32* %p, align 4, !tbaa !8
  br label %for.cond

cleanup186:                                       ; preds = %cleanup181, %for.cond.cleanup
  %111 = bitcast i32* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #7
  %cleanup.dest187 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest187, label %unreachable [
    i32 2, label %for.end188
    i32 1, label %return
  ]

for.end188:                                       ; preds = %cleanup186
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %for.end188, %cleanup186
  %112 = load i1, i1* %retval, align 1
  ret i1 %112

unreachable:                                      ; preds = %cleanup186
  unreachable
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !12
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !12
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !12
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !12
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !12
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !12
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !12
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !12
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !12
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #7
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #7
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !12
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !12
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !12
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !12
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !12
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !12
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !12
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !14
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btFace*, %struct.btFace** %m_data, align 4, !tbaa !18
  %1 = load i32, i32* %n.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %struct.btFace, %struct.btFace* %0, i32 %1
  ret %struct.btFace* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !12
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !12
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !12
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !12
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !12
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !12
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

define hidden void @_ZN18btConvexPolyhedron10initializeEv(%class.btConvexPolyhedron* %this) #0 {
entry:
  %this.addr = alloca %class.btConvexPolyhedron*, align 4
  %edges = alloca %class.btHashMap, align 4
  %TotalArea = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %numVertices = alloca i32, align 4
  %NbTris = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %vp = alloca %struct.btInternalVertexPair, align 2
  %edptr = alloca %struct.btInternalEdge*, align 4
  %edge = alloca %class.btVector3, align 4
  %found = alloca i8, align 1
  %p = alloca i32, align 4
  %ref.tmp34 = alloca %class.btVector3, align 4
  %ref.tmp38 = alloca %class.btVector3, align 4
  %ed = alloca %struct.btInternalEdge, align 2
  %i59 = alloca i32, align 4
  %numVertices66 = alloca i32, align 4
  %NbTris71 = alloca i32, align 4
  %p0 = alloca %class.btVector3*, align 4
  %j78 = alloca i32, align 4
  %k83 = alloca i32, align 4
  %p1 = alloca %class.btVector3*, align 4
  %p2 = alloca %class.btVector3*, align 4
  %Area = alloca float, align 4
  %ref.tmp98 = alloca %class.btVector3, align 4
  %ref.tmp99 = alloca %class.btVector3, align 4
  %ref.tmp100 = alloca %class.btVector3, align 4
  %Center = alloca %class.btVector3, align 4
  %ref.tmp102 = alloca %class.btVector3, align 4
  %ref.tmp103 = alloca %class.btVector3, align 4
  %ref.tmp104 = alloca float, align 4
  %ref.tmp105 = alloca %class.btVector3, align 4
  %i119 = alloca i32, align 4
  %Normal = alloca %class.btVector3, align 4
  %dist = alloca float, align 4
  %MinX = alloca float, align 4
  %MinY = alloca float, align 4
  %MinZ = alloca float, align 4
  %MaxX = alloca float, align 4
  %MaxY = alloca float, align 4
  %MaxZ = alloca float, align 4
  %i154 = alloca i32, align 4
  %pt = alloca %class.btVector3*, align 4
  %ref.tmp197 = alloca float, align 4
  %ref.tmp199 = alloca float, align 4
  %ref.tmp201 = alloca float, align 4
  %ref.tmp203 = alloca float, align 4
  %ref.tmp205 = alloca float, align 4
  %ref.tmp207 = alloca float, align 4
  %r = alloca float, align 4
  %LargestExtent = alloca i32, align 4
  %Step = alloca float, align 4
  %FoundBox = alloca i8, align 1
  %j233 = alloca i32, align 4
  %Step261 = alloca float, align 4
  %e0 = alloca i32, align 4
  %e1 = alloca i32, align 4
  %j267 = alloca i32, align 4
  %Saved0 = alloca float, align 4
  %Saved1 = alloca float, align 4
  store %class.btConvexPolyhedron* %this, %class.btConvexPolyhedron** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %this.addr, align 4
  %0 = bitcast %class.btHashMap* %edges to i8*
  call void @llvm.lifetime.start.p0i8(i64 80, i8* %0) #7
  %call = call %class.btHashMap* @_ZN9btHashMapI20btInternalVertexPair14btInternalEdgeEC2Ev(%class.btHashMap* %edges)
  %1 = bitcast float* %TotalArea to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  store float 0.000000e+00, float* %TotalArea, align 4, !tbaa !12
  %m_localCenter = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 4
  %2 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !12
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !12
  %4 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !12
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_localCenter, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #7
  %6 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #7
  %7 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc55, %entry
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %m_faces = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %m_faces)
  %cmp = icmp slt i32 %9, %call4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #7
  br label %for.end58

for.body:                                         ; preds = %for.cond
  %11 = bitcast i32* %numVertices to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #7
  %m_faces5 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %12 = load i32, i32* %i, align 4, !tbaa !8
  %call6 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces5, i32 %12)
  %m_indices = getelementptr inbounds %struct.btFace, %struct.btFace* %call6, i32 0, i32 0
  %call7 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %m_indices)
  store i32 %call7, i32* %numVertices, align 4, !tbaa !8
  %13 = bitcast i32* %NbTris to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = load i32, i32* %numVertices, align 4, !tbaa !8
  store i32 %14, i32* %NbTris, align 4, !tbaa !8
  %15 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #7
  store i32 0, i32* %j, align 4, !tbaa !8
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc51, %for.body
  %16 = load i32, i32* %j, align 4, !tbaa !8
  %17 = load i32, i32* %NbTris, align 4, !tbaa !8
  %cmp9 = icmp slt i32 %16, %17
  br i1 %cmp9, label %for.body11, label %for.cond.cleanup10

for.cond.cleanup10:                               ; preds = %for.cond8
  store i32 5, i32* %cleanup.dest.slot, align 4
  %18 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #7
  br label %for.end54

for.body11:                                       ; preds = %for.cond8
  %19 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #7
  %20 = load i32, i32* %j, align 4, !tbaa !8
  %add = add nsw i32 %20, 1
  %21 = load i32, i32* %numVertices, align 4, !tbaa !8
  %rem = srem i32 %add, %21
  store i32 %rem, i32* %k, align 4, !tbaa !8
  %22 = bitcast %struct.btInternalVertexPair* %vp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #7
  %m_faces12 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %23 = load i32, i32* %i, align 4, !tbaa !8
  %call13 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces12, i32 %23)
  %m_indices14 = getelementptr inbounds %struct.btFace, %struct.btFace* %call13, i32 0, i32 0
  %24 = load i32, i32* %j, align 4, !tbaa !8
  %call15 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_indices14, i32 %24)
  %25 = load i32, i32* %call15, align 4, !tbaa !8
  %conv = trunc i32 %25 to i16
  %m_faces16 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %26 = load i32, i32* %i, align 4, !tbaa !8
  %call17 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces16, i32 %26)
  %m_indices18 = getelementptr inbounds %struct.btFace, %struct.btFace* %call17, i32 0, i32 0
  %27 = load i32, i32* %k, align 4, !tbaa !8
  %call19 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_indices18, i32 %27)
  %28 = load i32, i32* %call19, align 4, !tbaa !8
  %conv20 = trunc i32 %28 to i16
  %call21 = call %struct.btInternalVertexPair* @_ZN20btInternalVertexPairC2Ess(%struct.btInternalVertexPair* %vp, i16 signext %conv, i16 signext %conv20)
  %29 = bitcast %struct.btInternalEdge** %edptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #7
  %call22 = call %struct.btInternalEdge* @_ZN9btHashMapI20btInternalVertexPair14btInternalEdgeE4findERKS0_(%class.btHashMap* %edges, %struct.btInternalVertexPair* nonnull align 2 dereferenceable(4) %vp)
  store %struct.btInternalEdge* %call22, %struct.btInternalEdge** %edptr, align 4, !tbaa !2
  %30 = bitcast %class.btVector3* %edge to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %30) #7
  %m_vertices = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 1
  %m_v1 = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %vp, i32 0, i32 1
  %31 = load i16, i16* %m_v1, align 2, !tbaa !19
  %conv23 = sext i16 %31 to i32
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_vertices, i32 %conv23)
  %m_vertices25 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 1
  %m_v0 = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %vp, i32 0, i32 0
  %32 = load i16, i16* %m_v0, align 2, !tbaa !22
  %conv26 = sext i16 %32 to i32
  %call27 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_vertices25, i32 %conv26)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %edge, %class.btVector3* nonnull align 4 dereferenceable(16) %call24, %class.btVector3* nonnull align 4 dereferenceable(16) %call27)
  %call28 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %edge)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %found) #7
  store i8 0, i8* %found, align 1, !tbaa !23
  %33 = bitcast i32* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #7
  store i32 0, i32* %p, align 4, !tbaa !8
  br label %for.cond29

for.cond29:                                       ; preds = %for.inc, %for.body11
  %34 = load i32, i32* %p, align 4, !tbaa !8
  %m_uniqueEdges = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 3
  %call30 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %m_uniqueEdges)
  %cmp31 = icmp slt i32 %34, %call30
  br i1 %cmp31, label %for.body33, label %for.cond.cleanup32

for.cond.cleanup32:                               ; preds = %for.cond29
  store i32 8, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body33:                                       ; preds = %for.cond29
  %35 = bitcast %class.btVector3* %ref.tmp34 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %35) #7
  %m_uniqueEdges35 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 3
  %36 = load i32, i32* %p, align 4, !tbaa !8
  %call36 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_uniqueEdges35, i32 %36)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp34, %class.btVector3* nonnull align 4 dereferenceable(16) %call36, %class.btVector3* nonnull align 4 dereferenceable(16) %edge)
  %call37 = call zeroext i1 @_Z12IsAlmostZeroRK9btVector3(%class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp34)
  %37 = bitcast %class.btVector3* %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %37) #7
  br i1 %call37, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %for.body33
  %m_uniqueEdges39 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 3
  %38 = load i32, i32* %p, align 4, !tbaa !8
  %call40 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_uniqueEdges39, i32 %38)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp38, %class.btVector3* nonnull align 4 dereferenceable(16) %call40, %class.btVector3* nonnull align 4 dereferenceable(16) %edge)
  %call41 = call zeroext i1 @_Z12IsAlmostZeroRK9btVector3(%class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp38)
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %for.body33
  %39 = phi i1 [ true, %for.body33 ], [ %call41, %lor.rhs ]
  %40 = bitcast %class.btVector3* %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %40) #7
  %41 = bitcast %class.btVector3* %ref.tmp34 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %41) #7
  br i1 %39, label %if.then, label %if.end

if.then:                                          ; preds = %lor.end
  store i8 1, i8* %found, align 1, !tbaa !23
  store i32 8, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %lor.end
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %42 = load i32, i32* %p, align 4, !tbaa !8
  %inc = add nsw i32 %42, 1
  store i32 %inc, i32* %p, align 4, !tbaa !8
  br label %for.cond29

cleanup:                                          ; preds = %if.then, %for.cond.cleanup32
  %43 = bitcast i32* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #7
  br label %for.end

for.end:                                          ; preds = %cleanup
  %44 = load i8, i8* %found, align 1, !tbaa !23, !range !24
  %tobool = trunc i8 %44 to i1
  br i1 %tobool, label %if.end44, label %if.then42

if.then42:                                        ; preds = %for.end
  %m_uniqueEdges43 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 3
  call void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray* %m_uniqueEdges43, %class.btVector3* nonnull align 4 dereferenceable(16) %edge)
  br label %if.end44

if.end44:                                         ; preds = %if.then42, %for.end
  %45 = load %struct.btInternalEdge*, %struct.btInternalEdge** %edptr, align 4, !tbaa !2
  %tobool45 = icmp ne %struct.btInternalEdge* %45, null
  br i1 %tobool45, label %if.then46, label %if.else

if.then46:                                        ; preds = %if.end44
  %46 = load i32, i32* %i, align 4, !tbaa !8
  %conv47 = trunc i32 %46 to i16
  %47 = load %struct.btInternalEdge*, %struct.btInternalEdge** %edptr, align 4, !tbaa !2
  %m_face1 = getelementptr inbounds %struct.btInternalEdge, %struct.btInternalEdge* %47, i32 0, i32 1
  store i16 %conv47, i16* %m_face1, align 2, !tbaa !25
  br label %if.end50

if.else:                                          ; preds = %if.end44
  %48 = bitcast %struct.btInternalEdge* %ed to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #7
  %call48 = call %struct.btInternalEdge* @_ZN14btInternalEdgeC2Ev(%struct.btInternalEdge* %ed)
  %49 = load i32, i32* %i, align 4, !tbaa !8
  %conv49 = trunc i32 %49 to i16
  %m_face0 = getelementptr inbounds %struct.btInternalEdge, %struct.btInternalEdge* %ed, i32 0, i32 0
  store i16 %conv49, i16* %m_face0, align 2, !tbaa !27
  call void @_ZN9btHashMapI20btInternalVertexPair14btInternalEdgeE6insertERKS0_RKS1_(%class.btHashMap* %edges, %struct.btInternalVertexPair* nonnull align 2 dereferenceable(4) %vp, %struct.btInternalEdge* nonnull align 2 dereferenceable(4) %ed)
  %50 = bitcast %struct.btInternalEdge* %ed to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #7
  br label %if.end50

if.end50:                                         ; preds = %if.else, %if.then46
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %found) #7
  %51 = bitcast %class.btVector3* %edge to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %51) #7
  %52 = bitcast %struct.btInternalEdge** %edptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #7
  %53 = bitcast %struct.btInternalVertexPair* %vp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #7
  %54 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #7
  br label %for.inc51

for.inc51:                                        ; preds = %if.end50
  %55 = load i32, i32* %j, align 4, !tbaa !8
  %inc52 = add nsw i32 %55, 1
  store i32 %inc52, i32* %j, align 4, !tbaa !8
  br label %for.cond8

for.end54:                                        ; preds = %for.cond.cleanup10
  %56 = bitcast i32* %NbTris to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #7
  %57 = bitcast i32* %numVertices to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #7
  br label %for.inc55

for.inc55:                                        ; preds = %for.end54
  %58 = load i32, i32* %i, align 4, !tbaa !8
  %inc56 = add nsw i32 %58, 1
  store i32 %inc56, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end58:                                        ; preds = %for.cond.cleanup
  %59 = bitcast i32* %i59 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #7
  store i32 0, i32* %i59, align 4, !tbaa !8
  br label %for.cond60

for.cond60:                                       ; preds = %for.inc113, %for.end58
  %60 = load i32, i32* %i59, align 4, !tbaa !8
  %m_faces61 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %call62 = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %m_faces61)
  %cmp63 = icmp slt i32 %60, %call62
  br i1 %cmp63, label %for.body65, label %for.cond.cleanup64

for.cond.cleanup64:                               ; preds = %for.cond60
  store i32 11, i32* %cleanup.dest.slot, align 4
  %61 = bitcast i32* %i59 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #7
  br label %for.end116

for.body65:                                       ; preds = %for.cond60
  %62 = bitcast i32* %numVertices66 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #7
  %m_faces67 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %63 = load i32, i32* %i59, align 4, !tbaa !8
  %call68 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces67, i32 %63)
  %m_indices69 = getelementptr inbounds %struct.btFace, %struct.btFace* %call68, i32 0, i32 0
  %call70 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %m_indices69)
  store i32 %call70, i32* %numVertices66, align 4, !tbaa !8
  %64 = bitcast i32* %NbTris71 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #7
  %65 = load i32, i32* %numVertices66, align 4, !tbaa !8
  %sub = sub nsw i32 %65, 2
  store i32 %sub, i32* %NbTris71, align 4, !tbaa !8
  %66 = bitcast %class.btVector3** %p0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #7
  %m_vertices72 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 1
  %m_faces73 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %67 = load i32, i32* %i59, align 4, !tbaa !8
  %call74 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces73, i32 %67)
  %m_indices75 = getelementptr inbounds %struct.btFace, %struct.btFace* %call74, i32 0, i32 0
  %call76 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_indices75, i32 0)
  %68 = load i32, i32* %call76, align 4, !tbaa !8
  %call77 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_vertices72, i32 %68)
  store %class.btVector3* %call77, %class.btVector3** %p0, align 4, !tbaa !2
  %69 = bitcast i32* %j78 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #7
  store i32 1, i32* %j78, align 4, !tbaa !8
  br label %for.cond79

for.cond79:                                       ; preds = %for.inc109, %for.body65
  %70 = load i32, i32* %j78, align 4, !tbaa !8
  %71 = load i32, i32* %NbTris71, align 4, !tbaa !8
  %cmp80 = icmp sle i32 %70, %71
  br i1 %cmp80, label %for.body82, label %for.cond.cleanup81

for.cond.cleanup81:                               ; preds = %for.cond79
  store i32 14, i32* %cleanup.dest.slot, align 4
  %72 = bitcast i32* %j78 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #7
  br label %for.end112

for.body82:                                       ; preds = %for.cond79
  %73 = bitcast i32* %k83 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #7
  %74 = load i32, i32* %j78, align 4, !tbaa !8
  %add84 = add nsw i32 %74, 1
  %75 = load i32, i32* %numVertices66, align 4, !tbaa !8
  %rem85 = srem i32 %add84, %75
  store i32 %rem85, i32* %k83, align 4, !tbaa !8
  %76 = bitcast %class.btVector3** %p1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #7
  %m_vertices86 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 1
  %m_faces87 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %77 = load i32, i32* %i59, align 4, !tbaa !8
  %call88 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces87, i32 %77)
  %m_indices89 = getelementptr inbounds %struct.btFace, %struct.btFace* %call88, i32 0, i32 0
  %78 = load i32, i32* %j78, align 4, !tbaa !8
  %call90 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_indices89, i32 %78)
  %79 = load i32, i32* %call90, align 4, !tbaa !8
  %call91 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_vertices86, i32 %79)
  store %class.btVector3* %call91, %class.btVector3** %p1, align 4, !tbaa !2
  %80 = bitcast %class.btVector3** %p2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #7
  %m_vertices92 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 1
  %m_faces93 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %81 = load i32, i32* %i59, align 4, !tbaa !8
  %call94 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces93, i32 %81)
  %m_indices95 = getelementptr inbounds %struct.btFace, %struct.btFace* %call94, i32 0, i32 0
  %82 = load i32, i32* %k83, align 4, !tbaa !8
  %call96 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_indices95, i32 %82)
  %83 = load i32, i32* %call96, align 4, !tbaa !8
  %call97 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_vertices92, i32 %83)
  store %class.btVector3* %call97, %class.btVector3** %p2, align 4, !tbaa !2
  %84 = bitcast float* %Area to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %84) #7
  %85 = bitcast %class.btVector3* %ref.tmp98 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %85) #7
  %86 = bitcast %class.btVector3* %ref.tmp99 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %86) #7
  %87 = load %class.btVector3*, %class.btVector3** %p0, align 4, !tbaa !2
  %88 = load %class.btVector3*, %class.btVector3** %p1, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp99, %class.btVector3* nonnull align 4 dereferenceable(16) %87, %class.btVector3* nonnull align 4 dereferenceable(16) %88)
  %89 = bitcast %class.btVector3* %ref.tmp100 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %89) #7
  %90 = load %class.btVector3*, %class.btVector3** %p0, align 4, !tbaa !2
  %91 = load %class.btVector3*, %class.btVector3** %p2, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp100, %class.btVector3* nonnull align 4 dereferenceable(16) %90, %class.btVector3* nonnull align 4 dereferenceable(16) %91)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp98, %class.btVector3* %ref.tmp99, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp100)
  %call101 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp98)
  %mul = fmul float %call101, 5.000000e-01
  %92 = bitcast %class.btVector3* %ref.tmp100 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %92) #7
  %93 = bitcast %class.btVector3* %ref.tmp99 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %93) #7
  %94 = bitcast %class.btVector3* %ref.tmp98 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %94) #7
  store float %mul, float* %Area, align 4, !tbaa !12
  %95 = bitcast %class.btVector3* %Center to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %95) #7
  %96 = bitcast %class.btVector3* %ref.tmp102 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %96) #7
  %97 = bitcast %class.btVector3* %ref.tmp103 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %97) #7
  %98 = load %class.btVector3*, %class.btVector3** %p0, align 4, !tbaa !2
  %99 = load %class.btVector3*, %class.btVector3** %p1, align 4, !tbaa !2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp103, %class.btVector3* nonnull align 4 dereferenceable(16) %98, %class.btVector3* nonnull align 4 dereferenceable(16) %99)
  %100 = load %class.btVector3*, %class.btVector3** %p2, align 4, !tbaa !2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp102, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp103, %class.btVector3* nonnull align 4 dereferenceable(16) %100)
  %101 = bitcast float* %ref.tmp104 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %101) #7
  store float 3.000000e+00, float* %ref.tmp104, align 4, !tbaa !12
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %Center, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp102, float* nonnull align 4 dereferenceable(4) %ref.tmp104)
  %102 = bitcast float* %ref.tmp104 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #7
  %103 = bitcast %class.btVector3* %ref.tmp103 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %103) #7
  %104 = bitcast %class.btVector3* %ref.tmp102 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %104) #7
  %105 = bitcast %class.btVector3* %ref.tmp105 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %105) #7
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp105, float* nonnull align 4 dereferenceable(4) %Area, %class.btVector3* nonnull align 4 dereferenceable(16) %Center)
  %m_localCenter106 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 4
  %call107 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_localCenter106, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp105)
  %106 = bitcast %class.btVector3* %ref.tmp105 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %106) #7
  %107 = load float, float* %Area, align 4, !tbaa !12
  %108 = load float, float* %TotalArea, align 4, !tbaa !12
  %add108 = fadd float %108, %107
  store float %add108, float* %TotalArea, align 4, !tbaa !12
  %109 = bitcast %class.btVector3* %Center to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %109) #7
  %110 = bitcast float* %Area to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #7
  %111 = bitcast %class.btVector3** %p2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #7
  %112 = bitcast %class.btVector3** %p1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #7
  %113 = bitcast i32* %k83 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #7
  br label %for.inc109

for.inc109:                                       ; preds = %for.body82
  %114 = load i32, i32* %j78, align 4, !tbaa !8
  %inc110 = add nsw i32 %114, 1
  store i32 %inc110, i32* %j78, align 4, !tbaa !8
  br label %for.cond79

for.end112:                                       ; preds = %for.cond.cleanup81
  %115 = bitcast %class.btVector3** %p0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #7
  %116 = bitcast i32* %NbTris71 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #7
  %117 = bitcast i32* %numVertices66 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #7
  br label %for.inc113

for.inc113:                                       ; preds = %for.end112
  %118 = load i32, i32* %i59, align 4, !tbaa !8
  %inc114 = add nsw i32 %118, 1
  store i32 %inc114, i32* %i59, align 4, !tbaa !8
  br label %for.cond60

for.end116:                                       ; preds = %for.cond.cleanup64
  %m_localCenter117 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 4
  %call118 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %m_localCenter117, float* nonnull align 4 dereferenceable(4) %TotalArea)
  %m_radius = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 6
  store float 0x47EFFFFFE0000000, float* %m_radius, align 4, !tbaa !28
  %119 = bitcast i32* %i119 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %119) #7
  store i32 0, i32* %i119, align 4, !tbaa !8
  br label %for.cond120

for.cond120:                                      ; preds = %for.inc150, %for.end116
  %120 = load i32, i32* %i119, align 4, !tbaa !8
  %m_faces121 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %call122 = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %m_faces121)
  %cmp123 = icmp slt i32 %120, %call122
  br i1 %cmp123, label %for.body125, label %for.cond.cleanup124

for.cond.cleanup124:                              ; preds = %for.cond120
  store i32 17, i32* %cleanup.dest.slot, align 4
  %121 = bitcast i32* %i119 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #7
  br label %for.end153

for.body125:                                      ; preds = %for.cond120
  %122 = bitcast %class.btVector3* %Normal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %122) #7
  %m_faces126 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %123 = load i32, i32* %i119, align 4, !tbaa !8
  %call127 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces126, i32 %123)
  %m_plane = getelementptr inbounds %struct.btFace, %struct.btFace* %call127, i32 0, i32 1
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_plane, i32 0, i32 0
  %m_faces128 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %124 = load i32, i32* %i119, align 4, !tbaa !8
  %call129 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces128, i32 %124)
  %m_plane130 = getelementptr inbounds %struct.btFace, %struct.btFace* %call129, i32 0, i32 1
  %arrayidx131 = getelementptr inbounds [4 x float], [4 x float]* %m_plane130, i32 0, i32 1
  %m_faces132 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %125 = load i32, i32* %i119, align 4, !tbaa !8
  %call133 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces132, i32 %125)
  %m_plane134 = getelementptr inbounds %struct.btFace, %struct.btFace* %call133, i32 0, i32 1
  %arrayidx135 = getelementptr inbounds [4 x float], [4 x float]* %m_plane134, i32 0, i32 2
  %call136 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %Normal, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx131, float* nonnull align 4 dereferenceable(4) %arrayidx135)
  %126 = bitcast float* %dist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %126) #7
  %m_localCenter137 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 4
  %call138 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_localCenter137, %class.btVector3* nonnull align 4 dereferenceable(16) %Normal)
  %m_faces139 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %127 = load i32, i32* %i119, align 4, !tbaa !8
  %call140 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces139, i32 %127)
  %m_plane141 = getelementptr inbounds %struct.btFace, %struct.btFace* %call140, i32 0, i32 1
  %arrayidx142 = getelementptr inbounds [4 x float], [4 x float]* %m_plane141, i32 0, i32 3
  %128 = load float, float* %arrayidx142, align 4, !tbaa !12
  %add143 = fadd float %call138, %128
  %call144 = call float @_Z6btFabsf(float %add143)
  store float %call144, float* %dist, align 4, !tbaa !12
  %129 = load float, float* %dist, align 4, !tbaa !12
  %m_radius145 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 6
  %130 = load float, float* %m_radius145, align 4, !tbaa !28
  %cmp146 = fcmp olt float %129, %130
  br i1 %cmp146, label %if.then147, label %if.end149

if.then147:                                       ; preds = %for.body125
  %131 = load float, float* %dist, align 4, !tbaa !12
  %m_radius148 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 6
  store float %131, float* %m_radius148, align 4, !tbaa !28
  br label %if.end149

if.end149:                                        ; preds = %if.then147, %for.body125
  %132 = bitcast float* %dist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #7
  %133 = bitcast %class.btVector3* %Normal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %133) #7
  br label %for.inc150

for.inc150:                                       ; preds = %if.end149
  %134 = load i32, i32* %i119, align 4, !tbaa !8
  %inc151 = add nsw i32 %134, 1
  store i32 %inc151, i32* %i119, align 4, !tbaa !8
  br label %for.cond120

for.end153:                                       ; preds = %for.cond.cleanup124
  %135 = bitcast float* %MinX to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %135) #7
  store float 0x47EFFFFFE0000000, float* %MinX, align 4, !tbaa !12
  %136 = bitcast float* %MinY to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %136) #7
  store float 0x47EFFFFFE0000000, float* %MinY, align 4, !tbaa !12
  %137 = bitcast float* %MinZ to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %137) #7
  store float 0x47EFFFFFE0000000, float* %MinZ, align 4, !tbaa !12
  %138 = bitcast float* %MaxX to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %138) #7
  store float 0xC7EFFFFFE0000000, float* %MaxX, align 4, !tbaa !12
  %139 = bitcast float* %MaxY to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %139) #7
  store float 0xC7EFFFFFE0000000, float* %MaxY, align 4, !tbaa !12
  %140 = bitcast float* %MaxZ to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %140) #7
  store float 0xC7EFFFFFE0000000, float* %MaxZ, align 4, !tbaa !12
  %141 = bitcast i32* %i154 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %141) #7
  store i32 0, i32* %i154, align 4, !tbaa !8
  br label %for.cond155

for.cond155:                                      ; preds = %for.inc193, %for.end153
  %142 = load i32, i32* %i154, align 4, !tbaa !8
  %m_vertices156 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 1
  %call157 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %m_vertices156)
  %cmp158 = icmp slt i32 %142, %call157
  br i1 %cmp158, label %for.body160, label %for.cond.cleanup159

for.cond.cleanup159:                              ; preds = %for.cond155
  store i32 20, i32* %cleanup.dest.slot, align 4
  %143 = bitcast i32* %i154 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #7
  br label %for.end196

for.body160:                                      ; preds = %for.cond155
  %144 = bitcast %class.btVector3** %pt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %144) #7
  %m_vertices161 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 1
  %145 = load i32, i32* %i154, align 4, !tbaa !8
  %call162 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_vertices161, i32 %145)
  store %class.btVector3* %call162, %class.btVector3** %pt, align 4, !tbaa !2
  %146 = load %class.btVector3*, %class.btVector3** %pt, align 4, !tbaa !2
  %call163 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %146)
  %147 = load float, float* %call163, align 4, !tbaa !12
  %148 = load float, float* %MinX, align 4, !tbaa !12
  %cmp164 = fcmp olt float %147, %148
  br i1 %cmp164, label %if.then165, label %if.end167

if.then165:                                       ; preds = %for.body160
  %149 = load %class.btVector3*, %class.btVector3** %pt, align 4, !tbaa !2
  %call166 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %149)
  %150 = load float, float* %call166, align 4, !tbaa !12
  store float %150, float* %MinX, align 4, !tbaa !12
  br label %if.end167

if.end167:                                        ; preds = %if.then165, %for.body160
  %151 = load %class.btVector3*, %class.btVector3** %pt, align 4, !tbaa !2
  %call168 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %151)
  %152 = load float, float* %call168, align 4, !tbaa !12
  %153 = load float, float* %MaxX, align 4, !tbaa !12
  %cmp169 = fcmp ogt float %152, %153
  br i1 %cmp169, label %if.then170, label %if.end172

if.then170:                                       ; preds = %if.end167
  %154 = load %class.btVector3*, %class.btVector3** %pt, align 4, !tbaa !2
  %call171 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %154)
  %155 = load float, float* %call171, align 4, !tbaa !12
  store float %155, float* %MaxX, align 4, !tbaa !12
  br label %if.end172

if.end172:                                        ; preds = %if.then170, %if.end167
  %156 = load %class.btVector3*, %class.btVector3** %pt, align 4, !tbaa !2
  %call173 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %156)
  %157 = load float, float* %call173, align 4, !tbaa !12
  %158 = load float, float* %MinY, align 4, !tbaa !12
  %cmp174 = fcmp olt float %157, %158
  br i1 %cmp174, label %if.then175, label %if.end177

if.then175:                                       ; preds = %if.end172
  %159 = load %class.btVector3*, %class.btVector3** %pt, align 4, !tbaa !2
  %call176 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %159)
  %160 = load float, float* %call176, align 4, !tbaa !12
  store float %160, float* %MinY, align 4, !tbaa !12
  br label %if.end177

if.end177:                                        ; preds = %if.then175, %if.end172
  %161 = load %class.btVector3*, %class.btVector3** %pt, align 4, !tbaa !2
  %call178 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %161)
  %162 = load float, float* %call178, align 4, !tbaa !12
  %163 = load float, float* %MaxY, align 4, !tbaa !12
  %cmp179 = fcmp ogt float %162, %163
  br i1 %cmp179, label %if.then180, label %if.end182

if.then180:                                       ; preds = %if.end177
  %164 = load %class.btVector3*, %class.btVector3** %pt, align 4, !tbaa !2
  %call181 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %164)
  %165 = load float, float* %call181, align 4, !tbaa !12
  store float %165, float* %MaxY, align 4, !tbaa !12
  br label %if.end182

if.end182:                                        ; preds = %if.then180, %if.end177
  %166 = load %class.btVector3*, %class.btVector3** %pt, align 4, !tbaa !2
  %call183 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %166)
  %167 = load float, float* %call183, align 4, !tbaa !12
  %168 = load float, float* %MinZ, align 4, !tbaa !12
  %cmp184 = fcmp olt float %167, %168
  br i1 %cmp184, label %if.then185, label %if.end187

if.then185:                                       ; preds = %if.end182
  %169 = load %class.btVector3*, %class.btVector3** %pt, align 4, !tbaa !2
  %call186 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %169)
  %170 = load float, float* %call186, align 4, !tbaa !12
  store float %170, float* %MinZ, align 4, !tbaa !12
  br label %if.end187

if.end187:                                        ; preds = %if.then185, %if.end182
  %171 = load %class.btVector3*, %class.btVector3** %pt, align 4, !tbaa !2
  %call188 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %171)
  %172 = load float, float* %call188, align 4, !tbaa !12
  %173 = load float, float* %MaxZ, align 4, !tbaa !12
  %cmp189 = fcmp ogt float %172, %173
  br i1 %cmp189, label %if.then190, label %if.end192

if.then190:                                       ; preds = %if.end187
  %174 = load %class.btVector3*, %class.btVector3** %pt, align 4, !tbaa !2
  %call191 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %174)
  %175 = load float, float* %call191, align 4, !tbaa !12
  store float %175, float* %MaxZ, align 4, !tbaa !12
  br label %if.end192

if.end192:                                        ; preds = %if.then190, %if.end187
  %176 = bitcast %class.btVector3** %pt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %176) #7
  br label %for.inc193

for.inc193:                                       ; preds = %if.end192
  %177 = load i32, i32* %i154, align 4, !tbaa !8
  %inc194 = add nsw i32 %177, 1
  store i32 %inc194, i32* %i154, align 4, !tbaa !8
  br label %for.cond155

for.end196:                                       ; preds = %for.cond.cleanup159
  %mC = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 7
  %178 = bitcast float* %ref.tmp197 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %178) #7
  %179 = load float, float* %MaxX, align 4, !tbaa !12
  %180 = load float, float* %MinX, align 4, !tbaa !12
  %add198 = fadd float %179, %180
  store float %add198, float* %ref.tmp197, align 4, !tbaa !12
  %181 = bitcast float* %ref.tmp199 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %181) #7
  %182 = load float, float* %MaxY, align 4, !tbaa !12
  %183 = load float, float* %MinY, align 4, !tbaa !12
  %add200 = fadd float %182, %183
  store float %add200, float* %ref.tmp199, align 4, !tbaa !12
  %184 = bitcast float* %ref.tmp201 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %184) #7
  %185 = load float, float* %MaxZ, align 4, !tbaa !12
  %186 = load float, float* %MinZ, align 4, !tbaa !12
  %add202 = fadd float %185, %186
  store float %add202, float* %ref.tmp201, align 4, !tbaa !12
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %mC, float* nonnull align 4 dereferenceable(4) %ref.tmp197, float* nonnull align 4 dereferenceable(4) %ref.tmp199, float* nonnull align 4 dereferenceable(4) %ref.tmp201)
  %187 = bitcast float* %ref.tmp201 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %187) #7
  %188 = bitcast float* %ref.tmp199 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %188) #7
  %189 = bitcast float* %ref.tmp197 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %189) #7
  %mE = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 8
  %190 = bitcast float* %ref.tmp203 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %190) #7
  %191 = load float, float* %MaxX, align 4, !tbaa !12
  %192 = load float, float* %MinX, align 4, !tbaa !12
  %sub204 = fsub float %191, %192
  store float %sub204, float* %ref.tmp203, align 4, !tbaa !12
  %193 = bitcast float* %ref.tmp205 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %193) #7
  %194 = load float, float* %MaxY, align 4, !tbaa !12
  %195 = load float, float* %MinY, align 4, !tbaa !12
  %sub206 = fsub float %194, %195
  store float %sub206, float* %ref.tmp205, align 4, !tbaa !12
  %196 = bitcast float* %ref.tmp207 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %196) #7
  %197 = load float, float* %MaxZ, align 4, !tbaa !12
  %198 = load float, float* %MinZ, align 4, !tbaa !12
  %sub208 = fsub float %197, %198
  store float %sub208, float* %ref.tmp207, align 4, !tbaa !12
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %mE, float* nonnull align 4 dereferenceable(4) %ref.tmp203, float* nonnull align 4 dereferenceable(4) %ref.tmp205, float* nonnull align 4 dereferenceable(4) %ref.tmp207)
  %199 = bitcast float* %ref.tmp207 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %199) #7
  %200 = bitcast float* %ref.tmp205 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %200) #7
  %201 = bitcast float* %ref.tmp203 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %201) #7
  %202 = bitcast float* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %202) #7
  %m_radius209 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 6
  %203 = load float, float* %m_radius209, align 4, !tbaa !28
  %204 = call float @llvm.sqrt.f32(float 3.000000e+00)
  %div = fdiv float %203, %204
  store float %div, float* %r, align 4, !tbaa !12
  %205 = bitcast i32* %LargestExtent to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %205) #7
  %mE210 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 8
  %call211 = call i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %mE210)
  store i32 %call211, i32* %LargestExtent, align 4, !tbaa !8
  %206 = bitcast float* %Step to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %206) #7
  %mE212 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 8
  %call213 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %mE212)
  %207 = load i32, i32* %LargestExtent, align 4, !tbaa !8
  %arrayidx214 = getelementptr inbounds float, float* %call213, i32 %207
  %208 = load float, float* %arrayidx214, align 4, !tbaa !12
  %mul215 = fmul float %208, 5.000000e-01
  %209 = load float, float* %r, align 4, !tbaa !12
  %sub216 = fsub float %mul215, %209
  %div217 = fdiv float %sub216, 1.024000e+03
  store float %div217, float* %Step, align 4, !tbaa !12
  %210 = load float, float* %r, align 4, !tbaa !12
  %m_extents = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call218 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_extents)
  %arrayidx219 = getelementptr inbounds float, float* %call218, i32 2
  store float %210, float* %arrayidx219, align 4, !tbaa !12
  %m_extents220 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call221 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_extents220)
  %arrayidx222 = getelementptr inbounds float, float* %call221, i32 1
  store float %210, float* %arrayidx222, align 4, !tbaa !12
  %m_extents223 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call224 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_extents223)
  %arrayidx225 = getelementptr inbounds float, float* %call224, i32 0
  store float %210, float* %arrayidx225, align 4, !tbaa !12
  %mE226 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 8
  %call227 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %mE226)
  %211 = load i32, i32* %LargestExtent, align 4, !tbaa !8
  %arrayidx228 = getelementptr inbounds float, float* %call227, i32 %211
  %212 = load float, float* %arrayidx228, align 4, !tbaa !12
  %mul229 = fmul float %212, 5.000000e-01
  %m_extents230 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call231 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_extents230)
  %213 = load i32, i32* %LargestExtent, align 4, !tbaa !8
  %arrayidx232 = getelementptr inbounds float, float* %call231, i32 %213
  store float %mul229, float* %arrayidx232, align 4, !tbaa !12
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %FoundBox) #7
  store i8 0, i8* %FoundBox, align 1, !tbaa !23
  %214 = bitcast i32* %j233 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %214) #7
  store i32 0, i32* %j233, align 4, !tbaa !8
  br label %for.cond234

for.cond234:                                      ; preds = %for.inc245, %for.end196
  %215 = load i32, i32* %j233, align 4, !tbaa !8
  %cmp235 = icmp slt i32 %215, 1024
  br i1 %cmp235, label %for.body237, label %for.cond.cleanup236

for.cond.cleanup236:                              ; preds = %for.cond234
  store i32 23, i32* %cleanup.dest.slot, align 4
  br label %cleanup247

for.body237:                                      ; preds = %for.cond234
  %call238 = call zeroext i1 @_ZNK18btConvexPolyhedron15testContainmentEv(%class.btConvexPolyhedron* %this1)
  br i1 %call238, label %if.then239, label %if.end240

if.then239:                                       ; preds = %for.body237
  store i8 1, i8* %FoundBox, align 1, !tbaa !23
  store i32 23, i32* %cleanup.dest.slot, align 4
  br label %cleanup247

if.end240:                                        ; preds = %for.body237
  %216 = load float, float* %Step, align 4, !tbaa !12
  %m_extents241 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call242 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_extents241)
  %217 = load i32, i32* %LargestExtent, align 4, !tbaa !8
  %arrayidx243 = getelementptr inbounds float, float* %call242, i32 %217
  %218 = load float, float* %arrayidx243, align 4, !tbaa !12
  %sub244 = fsub float %218, %216
  store float %sub244, float* %arrayidx243, align 4, !tbaa !12
  br label %for.inc245

for.inc245:                                       ; preds = %if.end240
  %219 = load i32, i32* %j233, align 4, !tbaa !8
  %inc246 = add nsw i32 %219, 1
  store i32 %inc246, i32* %j233, align 4, !tbaa !8
  br label %for.cond234

cleanup247:                                       ; preds = %if.then239, %for.cond.cleanup236
  %220 = bitcast i32* %j233 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %220) #7
  br label %for.end248

for.end248:                                       ; preds = %cleanup247
  %221 = load i8, i8* %FoundBox, align 1, !tbaa !23, !range !24
  %tobool249 = trunc i8 %221 to i1
  br i1 %tobool249, label %if.else260, label %if.then250

if.then250:                                       ; preds = %for.end248
  %222 = load float, float* %r, align 4, !tbaa !12
  %m_extents251 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call252 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_extents251)
  %arrayidx253 = getelementptr inbounds float, float* %call252, i32 2
  store float %222, float* %arrayidx253, align 4, !tbaa !12
  %m_extents254 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call255 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_extents254)
  %arrayidx256 = getelementptr inbounds float, float* %call255, i32 1
  store float %222, float* %arrayidx256, align 4, !tbaa !12
  %m_extents257 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call258 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_extents257)
  %arrayidx259 = getelementptr inbounds float, float* %call258, i32 0
  store float %222, float* %arrayidx259, align 4, !tbaa !12
  br label %if.end301

if.else260:                                       ; preds = %for.end248
  %223 = bitcast float* %Step261 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %223) #7
  %m_radius262 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 6
  %224 = load float, float* %m_radius262, align 4, !tbaa !28
  %225 = load float, float* %r, align 4, !tbaa !12
  %sub263 = fsub float %224, %225
  %div264 = fdiv float %sub263, 1.024000e+03
  store float %div264, float* %Step261, align 4, !tbaa !12
  %226 = bitcast i32* %e0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %226) #7
  %227 = load i32, i32* %LargestExtent, align 4, !tbaa !8
  %shl = shl i32 1, %227
  %and = and i32 %shl, 3
  store i32 %and, i32* %e0, align 4, !tbaa !8
  %228 = bitcast i32* %e1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %228) #7
  %229 = load i32, i32* %e0, align 4, !tbaa !8
  %shl265 = shl i32 1, %229
  %and266 = and i32 %shl265, 3
  store i32 %and266, i32* %e1, align 4, !tbaa !8
  %230 = bitcast i32* %j267 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %230) #7
  store i32 0, i32* %j267, align 4, !tbaa !8
  br label %for.cond268

for.cond268:                                      ; preds = %for.inc297, %if.else260
  %231 = load i32, i32* %j267, align 4, !tbaa !8
  %cmp269 = icmp slt i32 %231, 1024
  br i1 %cmp269, label %for.body271, label %for.cond.cleanup270

for.cond.cleanup270:                              ; preds = %for.cond268
  store i32 26, i32* %cleanup.dest.slot, align 4
  br label %cleanup299

for.body271:                                      ; preds = %for.cond268
  %232 = bitcast float* %Saved0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %232) #7
  %m_extents272 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call273 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_extents272)
  %233 = load i32, i32* %e0, align 4, !tbaa !8
  %arrayidx274 = getelementptr inbounds float, float* %call273, i32 %233
  %234 = load float, float* %arrayidx274, align 4, !tbaa !12
  store float %234, float* %Saved0, align 4, !tbaa !12
  %235 = bitcast float* %Saved1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %235) #7
  %m_extents275 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call276 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_extents275)
  %236 = load i32, i32* %e1, align 4, !tbaa !8
  %arrayidx277 = getelementptr inbounds float, float* %call276, i32 %236
  %237 = load float, float* %arrayidx277, align 4, !tbaa !12
  store float %237, float* %Saved1, align 4, !tbaa !12
  %238 = load float, float* %Step261, align 4, !tbaa !12
  %m_extents278 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call279 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_extents278)
  %239 = load i32, i32* %e0, align 4, !tbaa !8
  %arrayidx280 = getelementptr inbounds float, float* %call279, i32 %239
  %240 = load float, float* %arrayidx280, align 4, !tbaa !12
  %add281 = fadd float %240, %238
  store float %add281, float* %arrayidx280, align 4, !tbaa !12
  %241 = load float, float* %Step261, align 4, !tbaa !12
  %m_extents282 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call283 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_extents282)
  %242 = load i32, i32* %e1, align 4, !tbaa !8
  %arrayidx284 = getelementptr inbounds float, float* %call283, i32 %242
  %243 = load float, float* %arrayidx284, align 4, !tbaa !12
  %add285 = fadd float %243, %241
  store float %add285, float* %arrayidx284, align 4, !tbaa !12
  %call286 = call zeroext i1 @_ZNK18btConvexPolyhedron15testContainmentEv(%class.btConvexPolyhedron* %this1)
  br i1 %call286, label %if.end294, label %if.then287

if.then287:                                       ; preds = %for.body271
  %244 = load float, float* %Saved0, align 4, !tbaa !12
  %m_extents288 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call289 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_extents288)
  %245 = load i32, i32* %e0, align 4, !tbaa !8
  %arrayidx290 = getelementptr inbounds float, float* %call289, i32 %245
  store float %244, float* %arrayidx290, align 4, !tbaa !12
  %246 = load float, float* %Saved1, align 4, !tbaa !12
  %m_extents291 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call292 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_extents291)
  %247 = load i32, i32* %e1, align 4, !tbaa !8
  %arrayidx293 = getelementptr inbounds float, float* %call292, i32 %247
  store float %246, float* %arrayidx293, align 4, !tbaa !12
  store i32 26, i32* %cleanup.dest.slot, align 4
  br label %cleanup295

if.end294:                                        ; preds = %for.body271
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup295

cleanup295:                                       ; preds = %if.end294, %if.then287
  %248 = bitcast float* %Saved1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %248) #7
  %249 = bitcast float* %Saved0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %249) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup299 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup295
  br label %for.inc297

for.inc297:                                       ; preds = %cleanup.cont
  %250 = load i32, i32* %j267, align 4, !tbaa !8
  %inc298 = add nsw i32 %250, 1
  store i32 %inc298, i32* %j267, align 4, !tbaa !8
  br label %for.cond268

cleanup299:                                       ; preds = %cleanup295, %for.cond.cleanup270
  %251 = bitcast i32* %j267 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %251) #7
  br label %for.end300

for.end300:                                       ; preds = %cleanup299
  %252 = bitcast i32* %e1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %252) #7
  %253 = bitcast i32* %e0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %253) #7
  %254 = bitcast float* %Step261 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %254) #7
  br label %if.end301

if.end301:                                        ; preds = %for.end300, %if.then250
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %FoundBox) #7
  %255 = bitcast float* %Step to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %255) #7
  %256 = bitcast i32* %LargestExtent to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %256) #7
  %257 = bitcast float* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %257) #7
  %258 = bitcast float* %MaxZ to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %258) #7
  %259 = bitcast float* %MaxY to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %259) #7
  %260 = bitcast float* %MaxX to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %260) #7
  %261 = bitcast float* %MinZ to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %261) #7
  %262 = bitcast float* %MinY to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %262) #7
  %263 = bitcast float* %MinX to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %263) #7
  %264 = bitcast float* %TotalArea to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %264) #7
  %call302 = call %class.btHashMap* @_ZN9btHashMapI20btInternalVertexPair14btInternalEdgeED2Ev(%class.btHashMap* %edges) #7
  %265 = bitcast %class.btHashMap* %edges to i8*
  call void @llvm.lifetime.end.p0i8(i64 80, i8* %265) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btHashMap* @_ZN9btHashMapI20btInternalVertexPair14btInternalEdgeEC2Ev(%class.btHashMap* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btHashMap*, align 4
  store %class.btHashMap* %this, %class.btHashMap** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHashMap*, %class.btHashMap** %this.addr, align 4
  %m_hashTable = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.3* %m_hashTable)
  %m_next = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.3* %m_next)
  %m_valueArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call3 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI14btInternalEdgeEC2Ev(%class.btAlignedObjectArray.8* %m_valueArray)
  %m_keyArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 3
  %call4 = call %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayI20btInternalVertexPairEC2Ev(%class.btAlignedObjectArray.12* %m_keyArray)
  ret %class.btHashMap* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !12
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !12
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !12
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !12
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !12
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !12
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !12
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btFace*, %struct.btFace** %m_data, align 4, !tbaa !18
  %1 = load i32, i32* %n.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %struct.btFace, %struct.btFace* %0, i32 %1
  ret %struct.btFace* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !33
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !36
  %1 = load i32, i32* %n.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

define linkonce_odr hidden %struct.btInternalVertexPair* @_ZN20btInternalVertexPairC2Ess(%struct.btInternalVertexPair* returned %this, i16 signext %v0, i16 signext %v1) unnamed_addr #0 comdat {
entry:
  %retval = alloca %struct.btInternalVertexPair*, align 4
  %this.addr = alloca %struct.btInternalVertexPair*, align 4
  %v0.addr = alloca i16, align 2
  %v1.addr = alloca i16, align 2
  store %struct.btInternalVertexPair* %this, %struct.btInternalVertexPair** %this.addr, align 4, !tbaa !2
  store i16 %v0, i16* %v0.addr, align 2, !tbaa !37
  store i16 %v1, i16* %v1.addr, align 2, !tbaa !37
  %this1 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %this.addr, align 4
  store %struct.btInternalVertexPair* %this1, %struct.btInternalVertexPair** %retval, align 4
  %m_v0 = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %this1, i32 0, i32 0
  %0 = load i16, i16* %v0.addr, align 2, !tbaa !37
  store i16 %0, i16* %m_v0, align 2, !tbaa !22
  %m_v1 = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %this1, i32 0, i32 1
  %1 = load i16, i16* %v1.addr, align 2, !tbaa !37
  store i16 %1, i16* %m_v1, align 2, !tbaa !19
  %m_v12 = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %this1, i32 0, i32 1
  %2 = load i16, i16* %m_v12, align 2, !tbaa !19
  %conv = sext i16 %2 to i32
  %m_v03 = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %this1, i32 0, i32 0
  %3 = load i16, i16* %m_v03, align 2, !tbaa !22
  %conv4 = sext i16 %3 to i32
  %cmp = icmp sgt i32 %conv, %conv4
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_v05 = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %this1, i32 0, i32 0
  %m_v16 = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %this1, i32 0, i32 1
  call void @_Z6btSwapIsEvRT_S1_(i16* nonnull align 2 dereferenceable(2) %m_v05, i16* nonnull align 2 dereferenceable(2) %m_v16)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %4 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %retval, align 4
  ret %struct.btInternalVertexPair* %4
}

define linkonce_odr hidden %struct.btInternalEdge* @_ZN9btHashMapI20btInternalVertexPair14btInternalEdgeE4findERKS0_(%class.btHashMap* %this, %struct.btInternalVertexPair* nonnull align 2 dereferenceable(4) %key) #0 comdat {
entry:
  %retval = alloca %struct.btInternalEdge*, align 4
  %this.addr = alloca %class.btHashMap*, align 4
  %key.addr = alloca %struct.btInternalVertexPair*, align 4
  %index = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btHashMap* %this, %class.btHashMap** %this.addr, align 4, !tbaa !2
  store %struct.btInternalVertexPair* %key, %struct.btInternalVertexPair** %key.addr, align 4, !tbaa !2
  %this1 = load %class.btHashMap*, %class.btHashMap** %this.addr, align 4
  %0 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %key.addr, align 4, !tbaa !2
  %call = call i32 @_ZNK9btHashMapI20btInternalVertexPair14btInternalEdgeE9findIndexERKS0_(%class.btHashMap* %this1, %struct.btInternalVertexPair* nonnull align 2 dereferenceable(4) %1)
  store i32 %call, i32* %index, align 4, !tbaa !8
  %2 = load i32, i32* %index, align 4, !tbaa !8
  %cmp = icmp eq i32 %2, -1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store %struct.btInternalEdge* null, %struct.btInternalEdge** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %m_valueArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %3 = load i32, i32* %index, align 4, !tbaa !8
  %call2 = call nonnull align 2 dereferenceable(4) %struct.btInternalEdge* @_ZN20btAlignedObjectArrayI14btInternalEdgeEixEi(%class.btAlignedObjectArray.8* %m_valueArray, i32 %3)
  store %struct.btInternalEdge* %call2, %struct.btInternalEdge** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %4 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #7
  %5 = load %struct.btInternalEdge*, %struct.btInternalEdge** %retval, align 4
  ret %struct.btInternalEdge* %5
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !12
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !12
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !12
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !12
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !12
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !12
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !12
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !12
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !12
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #7
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #7
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !38
  %1 = load i32, i32* %n.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %1
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4, !tbaa !12
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1) #7
  ret %class.btVector3* %call2
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !39
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_Z12IsAlmostZeroRK9btVector3(%class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %retval = alloca i1, align 1
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %0)
  %1 = load float, float* %call, align 4, !tbaa !12
  %2 = call float @llvm.fabs.f32(float %1)
  %conv = fpext float %2 to double
  %cmp = fcmp ogt double %conv, 0x3EB0C6F7A0B5ED8D
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %3)
  %4 = load float, float* %call1, align 4, !tbaa !12
  %5 = call float @llvm.fabs.f32(float %4)
  %conv2 = fpext float %5 to double
  %cmp3 = fcmp ogt double %conv2, 0x3EB0C6F7A0B5ED8D
  br i1 %cmp3, label %if.then, label %lor.lhs.false4

lor.lhs.false4:                                   ; preds = %lor.lhs.false
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %6)
  %7 = load float, float* %call5, align 4, !tbaa !12
  %8 = call float @llvm.fabs.f32(float %7)
  %conv6 = fpext float %8 to double
  %cmp7 = fcmp ogt double %conv6, 0x3EB0C6F7A0B5ED8D
  br i1 %cmp7, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false4, %lor.lhs.false, %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %lor.lhs.false4
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %9 = load i1, i1* %retval, align 1
  ret i1 %9
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %_Val) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %class.btVector3*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %_Val, %class.btVector3** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !8
  %1 = load i32, i32* %sz, align 4, !tbaa !8
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !38
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !39
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 %3
  %4 = bitcast %class.btVector3* %arrayidx to i8*
  %call5 = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %4)
  %5 = bitcast i8* %call5 to %class.btVector3*
  %6 = load %class.btVector3*, %class.btVector3** %_Val.addr, align 4, !tbaa !2
  %7 = bitcast %class.btVector3* %5 to i8*
  %8 = bitcast %class.btVector3* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !10
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %9 = load i32, i32* %m_size6, align 4, !tbaa !39
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %m_size6, align 4, !tbaa !39
  %10 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #7
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btInternalEdge* @_ZN14btInternalEdgeC2Ev(%struct.btInternalEdge* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btInternalEdge*, align 4
  store %struct.btInternalEdge* %this, %struct.btInternalEdge** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btInternalEdge*, %struct.btInternalEdge** %this.addr, align 4
  %m_face0 = getelementptr inbounds %struct.btInternalEdge, %struct.btInternalEdge* %this1, i32 0, i32 0
  store i16 -1, i16* %m_face0, align 2, !tbaa !27
  %m_face1 = getelementptr inbounds %struct.btInternalEdge, %struct.btInternalEdge* %this1, i32 0, i32 1
  store i16 -1, i16* %m_face1, align 2, !tbaa !25
  ret %struct.btInternalEdge* %this1
}

define linkonce_odr hidden void @_ZN9btHashMapI20btInternalVertexPair14btInternalEdgeE6insertERKS0_RKS1_(%class.btHashMap* %this, %struct.btInternalVertexPair* nonnull align 2 dereferenceable(4) %key, %struct.btInternalEdge* nonnull align 2 dereferenceable(4) %value) #0 comdat {
entry:
  %this.addr = alloca %class.btHashMap*, align 4
  %key.addr = alloca %struct.btInternalVertexPair*, align 4
  %value.addr = alloca %struct.btInternalEdge*, align 4
  %hash = alloca i32, align 4
  %index = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %count = alloca i32, align 4
  %oldCapacity = alloca i32, align 4
  %newCapacity = alloca i32, align 4
  store %class.btHashMap* %this, %class.btHashMap** %this.addr, align 4, !tbaa !2
  store %struct.btInternalVertexPair* %key, %struct.btInternalVertexPair** %key.addr, align 4, !tbaa !2
  store %struct.btInternalEdge* %value, %struct.btInternalEdge** %value.addr, align 4, !tbaa !2
  %this1 = load %class.btHashMap*, %class.btHashMap** %this.addr, align 4
  %0 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %key.addr, align 4, !tbaa !2
  %call = call i32 @_ZNK20btInternalVertexPair7getHashEv(%struct.btInternalVertexPair* %1)
  %m_valueArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE8capacityEv(%class.btAlignedObjectArray.8* %m_valueArray)
  %sub = sub nsw i32 %call2, 1
  %and = and i32 %call, %sub
  store i32 %and, i32* %hash, align 4, !tbaa !8
  %2 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %key.addr, align 4, !tbaa !2
  %call3 = call i32 @_ZNK9btHashMapI20btInternalVertexPair14btInternalEdgeE9findIndexERKS0_(%class.btHashMap* %this1, %struct.btInternalVertexPair* nonnull align 2 dereferenceable(4) %3)
  store i32 %call3, i32* %index, align 4, !tbaa !8
  %4 = load i32, i32* %index, align 4, !tbaa !8
  %cmp = icmp ne i32 %4, -1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load %struct.btInternalEdge*, %struct.btInternalEdge** %value.addr, align 4, !tbaa !2
  %m_valueArray4 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %6 = load i32, i32* %index, align 4, !tbaa !8
  %call5 = call nonnull align 2 dereferenceable(4) %struct.btInternalEdge* @_ZN20btAlignedObjectArrayI14btInternalEdgeEixEi(%class.btAlignedObjectArray.8* %m_valueArray4, i32 %6)
  %7 = bitcast %struct.btInternalEdge* %call5 to i8*
  %8 = bitcast %struct.btInternalEdge* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %7, i8* align 2 %8, i32 4, i1 false), !tbaa.struct !40
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %9 = bitcast i32* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %m_valueArray6 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call7 = call i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE4sizeEv(%class.btAlignedObjectArray.8* %m_valueArray6)
  store i32 %call7, i32* %count, align 4, !tbaa !8
  %10 = bitcast i32* %oldCapacity to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %m_valueArray8 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call9 = call i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE8capacityEv(%class.btAlignedObjectArray.8* %m_valueArray8)
  store i32 %call9, i32* %oldCapacity, align 4, !tbaa !8
  %m_valueArray10 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %11 = load %struct.btInternalEdge*, %struct.btInternalEdge** %value.addr, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayI14btInternalEdgeE9push_backERKS0_(%class.btAlignedObjectArray.8* %m_valueArray10, %struct.btInternalEdge* nonnull align 2 dereferenceable(4) %11)
  %m_keyArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 3
  %12 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %key.addr, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayI20btInternalVertexPairE9push_backERKS0_(%class.btAlignedObjectArray.12* %m_keyArray, %struct.btInternalVertexPair* nonnull align 2 dereferenceable(4) %12)
  %13 = bitcast i32* %newCapacity to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %m_valueArray11 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call12 = call i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE8capacityEv(%class.btAlignedObjectArray.8* %m_valueArray11)
  store i32 %call12, i32* %newCapacity, align 4, !tbaa !8
  %14 = load i32, i32* %oldCapacity, align 4, !tbaa !8
  %15 = load i32, i32* %newCapacity, align 4, !tbaa !8
  %cmp13 = icmp slt i32 %14, %15
  br i1 %cmp13, label %if.then14, label %if.end20

if.then14:                                        ; preds = %if.end
  %16 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %key.addr, align 4, !tbaa !2
  call void @_ZN9btHashMapI20btInternalVertexPair14btInternalEdgeE10growTablesERKS0_(%class.btHashMap* %this1, %struct.btInternalVertexPair* nonnull align 2 dereferenceable(4) %16)
  %17 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %key.addr, align 4, !tbaa !2
  %call15 = call i32 @_ZNK20btInternalVertexPair7getHashEv(%struct.btInternalVertexPair* %17)
  %m_valueArray16 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call17 = call i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE8capacityEv(%class.btAlignedObjectArray.8* %m_valueArray16)
  %sub18 = sub nsw i32 %call17, 1
  %and19 = and i32 %call15, %sub18
  store i32 %and19, i32* %hash, align 4, !tbaa !8
  br label %if.end20

if.end20:                                         ; preds = %if.then14, %if.end
  %m_hashTable = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %18 = load i32, i32* %hash, align 4, !tbaa !8
  %call21 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_hashTable, i32 %18)
  %19 = load i32, i32* %call21, align 4, !tbaa !8
  %m_next = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %20 = load i32, i32* %count, align 4, !tbaa !8
  %call22 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_next, i32 %20)
  store i32 %19, i32* %call22, align 4, !tbaa !8
  %21 = load i32, i32* %count, align 4, !tbaa !8
  %m_hashTable23 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %22 = load i32, i32* %hash, align 4, !tbaa !8
  %call24 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_hashTable23, i32 %22)
  store i32 %21, i32* %call24, align 4, !tbaa !8
  %23 = bitcast i32* %newCapacity to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #7
  %24 = bitcast i32* %oldCapacity to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #7
  %25 = bitcast i32* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end20, %if.then
  %26 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #7
  %27 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %1 = load float, float* %arrayidx, align 4, !tbaa !12
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %3 = load float, float* %arrayidx3, align 4, !tbaa !12
  %mul = fmul float %1, %3
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %4 = load float, float* %arrayidx5, align 4, !tbaa !12
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %6 = load float, float* %arrayidx7, align 4, !tbaa !12
  %mul8 = fmul float %4, %6
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4, !tbaa !12
  %7 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %8 = load float, float* %arrayidx11, align 4, !tbaa !12
  %9 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %10 = load float, float* %arrayidx13, align 4, !tbaa !12
  %mul14 = fmul float %8, %10
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %11 = load float, float* %arrayidx16, align 4, !tbaa !12
  %12 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %13 = load float, float* %arrayidx18, align 4, !tbaa !12
  %mul19 = fmul float %11, %13
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4, !tbaa !12
  %14 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #7
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %15 = load float, float* %arrayidx23, align 4, !tbaa !12
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %17 = load float, float* %arrayidx25, align 4, !tbaa !12
  %mul26 = fmul float %15, %17
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %18 = load float, float* %arrayidx28, align 4, !tbaa !12
  %19 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %19, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %20 = load float, float* %arrayidx30, align 4, !tbaa !12
  %mul31 = fmul float %18, %20
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4, !tbaa !12
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %21 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #7
  %22 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #7
  %23 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZdvRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #4 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load float*, float** %s.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !12
  %div = fdiv float 1.000000e+00, %3
  store float %div, float* %ref.tmp, align 4, !tbaa !12
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %4 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !12
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !12
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !12
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !12
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !12
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !12
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !12
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !12
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !12
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !12
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !12
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #7
  ret %class.btVector3* %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !12
  %0 = load float, float* %x.addr, align 4, !tbaa !12
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #5

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !12
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %1 = load float, float* %arrayidx3, align 4, !tbaa !12
  %cmp = fcmp olt float %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %2 = load float, float* %arrayidx5, align 4, !tbaa !12
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %3 = load float, float* %arrayidx7, align 4, !tbaa !12
  %cmp8 = fcmp olt float %2, %3
  %4 = zext i1 %cmp8 to i64
  %cond = select i1 %cmp8, i32 2, i32 1
  br label %cond.end

cond.false:                                       ; preds = %entry
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 0
  %5 = load float, float* %arrayidx10, align 4, !tbaa !12
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %6 = load float, float* %arrayidx12, align 4, !tbaa !12
  %cmp13 = fcmp olt float %5, %6
  %7 = zext i1 %cmp13 to i64
  %cond14 = select i1 %cmp13, i32 2, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond15 = phi i32 [ %cond, %cond.true ], [ %cond14, %cond.false ]
  ret i32 %cond15
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btHashMap* @_ZN9btHashMapI20btInternalVertexPair14btInternalEdgeED2Ev(%class.btHashMap* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btHashMap*, align 4
  store %class.btHashMap* %this, %class.btHashMap** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHashMap*, %class.btHashMap** %this.addr, align 4
  %m_keyArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 3
  %call = call %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayI20btInternalVertexPairED2Ev(%class.btAlignedObjectArray.12* %m_keyArray) #7
  %m_valueArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call2 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI14btInternalEdgeED2Ev(%class.btAlignedObjectArray.8* %m_valueArray) #7
  %m_next = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %call3 = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.3* %m_next) #7
  %m_hashTable = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %call4 = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.3* %m_hashTable) #7
  ret %class.btHashMap* %this1
}

define hidden void @_ZNK18btConvexPolyhedron7projectERK11btTransformRK9btVector3RfS6_RS3_S7_(%class.btConvexPolyhedron* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %trans, %class.btVector3* nonnull align 4 dereferenceable(16) %dir, float* nonnull align 4 dereferenceable(4) %minProj, float* nonnull align 4 dereferenceable(4) %maxProj, %class.btVector3* nonnull align 4 dereferenceable(16) %witnesPtMin, %class.btVector3* nonnull align 4 dereferenceable(16) %witnesPtMax) #0 {
entry:
  %this.addr = alloca %class.btConvexPolyhedron*, align 4
  %trans.addr = alloca %class.btTransform*, align 4
  %dir.addr = alloca %class.btVector3*, align 4
  %minProj.addr = alloca float*, align 4
  %maxProj.addr = alloca float*, align 4
  %witnesPtMin.addr = alloca %class.btVector3*, align 4
  %witnesPtMax.addr = alloca %class.btVector3*, align 4
  %numVerts = alloca i32, align 4
  %i = alloca i32, align 4
  %pt = alloca %class.btVector3, align 4
  %dp = alloca float, align 4
  store %class.btConvexPolyhedron* %this, %class.btConvexPolyhedron** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %trans, %class.btTransform** %trans.addr, align 4, !tbaa !2
  store %class.btVector3* %dir, %class.btVector3** %dir.addr, align 4, !tbaa !2
  store float* %minProj, float** %minProj.addr, align 4, !tbaa !2
  store float* %maxProj, float** %maxProj.addr, align 4, !tbaa !2
  store %class.btVector3* %witnesPtMin, %class.btVector3** %witnesPtMin.addr, align 4, !tbaa !2
  store %class.btVector3* %witnesPtMax, %class.btVector3** %witnesPtMax.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %this.addr, align 4
  %0 = load float*, float** %minProj.addr, align 4, !tbaa !2
  store float 0x47EFFFFFE0000000, float* %0, align 4, !tbaa !12
  %1 = load float*, float** %maxProj.addr, align 4, !tbaa !2
  store float 0xC7EFFFFFE0000000, float* %1, align 4, !tbaa !12
  %2 = bitcast i32* %numVerts to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %m_vertices = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %m_vertices)
  store i32 %call, i32* %numVerts, align 4, !tbaa !8
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %4 = load i32, i32* %i, align 4, !tbaa !8
  %5 = load i32, i32* %numVerts, align 4, !tbaa !8
  %cmp = icmp slt i32 %4, %5
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %6 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %7 = bitcast %class.btVector3* %pt to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #7
  %8 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4, !tbaa !2
  %m_vertices2 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 1
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_vertices2, i32 %9)
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %pt, %class.btTransform* %8, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  %10 = bitcast float* %dp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = load %class.btVector3*, %class.btVector3** %dir.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %pt, %class.btVector3* nonnull align 4 dereferenceable(16) %11)
  store float %call4, float* %dp, align 4, !tbaa !12
  %12 = load float, float* %dp, align 4, !tbaa !12
  %13 = load float*, float** %minProj.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !12
  %cmp5 = fcmp olt float %12, %14
  br i1 %cmp5, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %15 = load float, float* %dp, align 4, !tbaa !12
  %16 = load float*, float** %minProj.addr, align 4, !tbaa !2
  store float %15, float* %16, align 4, !tbaa !12
  %17 = load %class.btVector3*, %class.btVector3** %witnesPtMin.addr, align 4, !tbaa !2
  %18 = bitcast %class.btVector3* %17 to i8*
  %19 = bitcast %class.btVector3* %pt to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %18, i8* align 4 %19, i32 16, i1 false), !tbaa.struct !10
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %20 = load float, float* %dp, align 4, !tbaa !12
  %21 = load float*, float** %maxProj.addr, align 4, !tbaa !2
  %22 = load float, float* %21, align 4, !tbaa !12
  %cmp6 = fcmp ogt float %20, %22
  br i1 %cmp6, label %if.then7, label %if.end8

if.then7:                                         ; preds = %if.end
  %23 = load float, float* %dp, align 4, !tbaa !12
  %24 = load float*, float** %maxProj.addr, align 4, !tbaa !2
  store float %23, float* %24, align 4, !tbaa !12
  %25 = load %class.btVector3*, %class.btVector3** %witnesPtMax.addr, align 4, !tbaa !2
  %26 = bitcast %class.btVector3* %25 to i8*
  %27 = bitcast %class.btVector3* %pt to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %26, i8* align 4 %27, i32 16, i1 false), !tbaa.struct !10
  br label %if.end8

if.end8:                                          ; preds = %if.then7, %if.end
  %28 = bitcast float* %dp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #7
  %29 = bitcast %class.btVector3* %pt to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #7
  br label %for.inc

for.inc:                                          ; preds = %if.end8
  %30 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %30, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %31 = load float*, float** %minProj.addr, align 4, !tbaa !2
  %32 = load float, float* %31, align 4, !tbaa !12
  %33 = load float*, float** %maxProj.addr, align 4, !tbaa !2
  %34 = load float, float* %33, align 4, !tbaa !12
  %cmp9 = fcmp ogt float %32, %34
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %for.end
  %35 = load float*, float** %minProj.addr, align 4, !tbaa !2
  %36 = load float*, float** %maxProj.addr, align 4, !tbaa !2
  call void @_Z6btSwapIfEvRT_S1_(float* nonnull align 4 dereferenceable(4) %35, float* nonnull align 4 dereferenceable(4) %36)
  %37 = load %class.btVector3*, %class.btVector3** %witnesPtMin.addr, align 4, !tbaa !2
  %38 = load %class.btVector3*, %class.btVector3** %witnesPtMax.addr, align 4, !tbaa !2
  call void @_Z6btSwapI9btVector3EvRT_S2_(%class.btVector3* nonnull align 4 dereferenceable(16) %37, %class.btVector3* nonnull align 4 dereferenceable(16) %38)
  br label %if.end11

if.end11:                                         ; preds = %if.then10, %for.end
  %39 = bitcast i32* %numVerts to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !38
  %1 = load i32, i32* %n.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %1
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z6btSwapIfEvRT_S1_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  %tmp = alloca float, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store float* %b, float** %b.addr, align 4, !tbaa !2
  %0 = bitcast float* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load float*, float** %a.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !12
  store float %2, float* %tmp, align 4, !tbaa !12
  %3 = load float*, float** %b.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !12
  %5 = load float*, float** %a.addr, align 4, !tbaa !2
  store float %4, float* %5, align 4, !tbaa !12
  %6 = load float, float* %tmp, align 4, !tbaa !12
  %7 = load float*, float** %b.addr, align 4, !tbaa !2
  store float %6, float* %7, align 4, !tbaa !12
  %8 = bitcast float* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z6btSwapI9btVector3EvRT_S2_(%class.btVector3* nonnull align 4 dereferenceable(16) %a, %class.btVector3* nonnull align 4 dereferenceable(16) %b) #1 comdat {
entry:
  %a.addr = alloca %class.btVector3*, align 4
  %b.addr = alloca %class.btVector3*, align 4
  %tmp = alloca %class.btVector3, align 4
  store %class.btVector3* %a, %class.btVector3** %a.addr, align 4, !tbaa !2
  store %class.btVector3* %b, %class.btVector3** %b.addr, align 4, !tbaa !2
  %0 = bitcast %class.btVector3* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #7
  %1 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %tmp to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !10
  %4 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  %5 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %6 = bitcast %class.btVector3* %5 to i8*
  %7 = bitcast %class.btVector3* %4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !10
  %8 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  %9 = bitcast %class.btVector3* %8 to i8*
  %10 = bitcast %class.btVector3* %tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false), !tbaa.struct !10
  %11 = bitcast %class.btVector3* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %11) #7
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #6

define linkonce_odr hidden %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.3* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.4* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.4* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.3* %this1)
  ret %class.btAlignedObjectArray.3* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI14btInternalEdgeEC2Ev(%class.btAlignedObjectArray.8* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.9* @_ZN18btAlignedAllocatorI14btInternalEdgeLj16EEC2Ev(%class.btAlignedAllocator.9* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI14btInternalEdgeE4initEv(%class.btAlignedObjectArray.8* %this1)
  ret %class.btAlignedObjectArray.8* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayI20btInternalVertexPairEC2Ev(%class.btAlignedObjectArray.12* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.13* @_ZN18btAlignedAllocatorI20btInternalVertexPairLj16EEC2Ev(%class.btAlignedAllocator.13* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI20btInternalVertexPairE4initEv(%class.btAlignedObjectArray.12* %this1)
  ret %class.btAlignedObjectArray.12* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.4* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.4* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.4*, align 4
  store %class.btAlignedAllocator.4* %this, %class.btAlignedAllocator.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.4*, %class.btAlignedAllocator.4** %this.addr, align 4
  ret %class.btAlignedAllocator.4* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !41
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  store i32* null, i32** %m_data, align 4, !tbaa !36
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !33
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !42
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.9* @_ZN18btAlignedAllocatorI14btInternalEdgeLj16EEC2Ev(%class.btAlignedAllocator.9* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  ret %class.btAlignedAllocator.9* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI14btInternalEdgeE4initEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !43
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %struct.btInternalEdge* null, %struct.btInternalEdge** %m_data, align 4, !tbaa !46
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !47
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !48
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.13* @_ZN18btAlignedAllocatorI20btInternalVertexPairLj16EEC2Ev(%class.btAlignedAllocator.13* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.13*, align 4
  store %class.btAlignedAllocator.13* %this, %class.btAlignedAllocator.13** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.13*, %class.btAlignedAllocator.13** %this.addr, align 4
  ret %class.btAlignedAllocator.13* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btInternalVertexPairE4initEv(%class.btAlignedObjectArray.12* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !49
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store %struct.btInternalVertexPair* null, %struct.btInternalVertexPair** %m_data, align 4, !tbaa !52
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !53
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !54
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z6btSwapIsEvRT_S1_(i16* nonnull align 2 dereferenceable(2) %a, i16* nonnull align 2 dereferenceable(2) %b) #1 comdat {
entry:
  %a.addr = alloca i16*, align 4
  %b.addr = alloca i16*, align 4
  %tmp = alloca i16, align 2
  store i16* %a, i16** %a.addr, align 4, !tbaa !2
  store i16* %b, i16** %b.addr, align 4, !tbaa !2
  %0 = bitcast i16* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %0) #7
  %1 = load i16*, i16** %a.addr, align 4, !tbaa !2
  %2 = load i16, i16* %1, align 2, !tbaa !37
  store i16 %2, i16* %tmp, align 2, !tbaa !37
  %3 = load i16*, i16** %b.addr, align 4, !tbaa !2
  %4 = load i16, i16* %3, align 2, !tbaa !37
  %5 = load i16*, i16** %a.addr, align 4, !tbaa !2
  store i16 %4, i16* %5, align 2, !tbaa !37
  %6 = load i16, i16* %tmp, align 2, !tbaa !37
  %7 = load i16*, i16** %b.addr, align 4, !tbaa !2
  store i16 %6, i16* %7, align 2, !tbaa !37
  %8 = bitcast i16* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %8) #7
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #5

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !12
  %0 = load float, float* %y.addr, align 4, !tbaa !12
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #4 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !12
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !12
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !12
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !12
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !12
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !12
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !12
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !12
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !12
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #7
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #7
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !12
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !12
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !12
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !12
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !12
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !12
  %6 = load float*, float** %s.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !12
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !12
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !12
  ret %class.btVector3* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayI20btInternalVertexPairED2Ev(%class.btAlignedObjectArray.12* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI20btInternalVertexPairE5clearEv(%class.btAlignedObjectArray.12* %this1)
  ret %class.btAlignedObjectArray.12* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI14btInternalEdgeED2Ev(%class.btAlignedObjectArray.8* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI14btInternalEdgeE5clearEv(%class.btAlignedObjectArray.8* %this1)
  ret %class.btAlignedObjectArray.8* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.3* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.3* %this1)
  ret %class.btAlignedObjectArray.3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btInternalVertexPairE5clearEv(%class.btAlignedObjectArray.12* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btInternalVertexPairE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  call void @_ZN20btAlignedObjectArrayI20btInternalVertexPairE7destroyEii(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI20btInternalVertexPairE10deallocateEv(%class.btAlignedObjectArray.12* %this1)
  call void @_ZN20btAlignedObjectArrayI20btInternalVertexPairE4initEv(%class.btAlignedObjectArray.12* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btInternalVertexPairE7destroyEii(%class.btAlignedObjectArray.12* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !8
  store i32 %last, i32* %last.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %first.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %last.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %4 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %m_data, align 4, !tbaa !52
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI20btInternalVertexPairE4sizeEv(%class.btAlignedObjectArray.12* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !53
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btInternalVertexPairE10deallocateEv(%class.btAlignedObjectArray.12* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %0 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %m_data, align 4, !tbaa !52
  %tobool = icmp ne %struct.btInternalVertexPair* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !49, !range !24
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %2 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %m_data4, align 4, !tbaa !52
  call void @_ZN18btAlignedAllocatorI20btInternalVertexPairLj16EE10deallocateEPS0_(%class.btAlignedAllocator.13* %m_allocator, %struct.btInternalVertexPair* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store %struct.btInternalVertexPair* null, %struct.btInternalVertexPair** %m_data5, align 4, !tbaa !52
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI20btInternalVertexPairLj16EE10deallocateEPS0_(%class.btAlignedAllocator.13* %this, %struct.btInternalVertexPair* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.13*, align 4
  %ptr.addr = alloca %struct.btInternalVertexPair*, align 4
  store %class.btAlignedAllocator.13* %this, %class.btAlignedAllocator.13** %this.addr, align 4, !tbaa !2
  store %struct.btInternalVertexPair* %ptr, %struct.btInternalVertexPair** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.13*, %class.btAlignedAllocator.13** %this.addr, align 4
  %0 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btInternalVertexPair* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI14btInternalEdgeE5clearEv(%class.btAlignedObjectArray.8* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayI14btInternalEdgeE7destroyEii(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI14btInternalEdgeE10deallocateEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayI14btInternalEdgeE4initEv(%class.btAlignedObjectArray.8* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI14btInternalEdgeE7destroyEii(%class.btAlignedObjectArray.8* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !8
  store i32 %last, i32* %last.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %first.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %last.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %4 = load %struct.btInternalEdge*, %struct.btInternalEdge** %m_data, align 4, !tbaa !46
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %struct.btInternalEdge, %struct.btInternalEdge* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE4sizeEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !47
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI14btInternalEdgeE10deallocateEv(%class.btAlignedObjectArray.8* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load %struct.btInternalEdge*, %struct.btInternalEdge** %m_data, align 4, !tbaa !46
  %tobool = icmp ne %struct.btInternalEdge* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !43, !range !24
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %2 = load %struct.btInternalEdge*, %struct.btInternalEdge** %m_data4, align 4, !tbaa !46
  call void @_ZN18btAlignedAllocatorI14btInternalEdgeLj16EE10deallocateEPS0_(%class.btAlignedAllocator.9* %m_allocator, %struct.btInternalEdge* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %struct.btInternalEdge* null, %struct.btInternalEdge** %m_data5, align 4, !tbaa !46
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI14btInternalEdgeLj16EE10deallocateEPS0_(%class.btAlignedAllocator.9* %this, %struct.btInternalEdge* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %ptr.addr = alloca %struct.btInternalEdge*, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4, !tbaa !2
  store %struct.btInternalEdge* %ptr, %struct.btInternalEdge** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load %struct.btInternalEdge*, %struct.btInternalEdge** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btInternalEdge* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.3* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.3* %this1)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.3* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.3* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !8
  store i32 %last, i32* %last.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %first.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %last.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %4 = load i32*, i32** %m_data, align 4, !tbaa !36
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !36
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !41, !range !24
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4, !tbaa !36
  call void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.4* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4, !tbaa !36
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.4* %this, i32* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.4*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.4* %this, %class.btAlignedAllocator.4** %this.addr, align 4, !tbaa !2
  store i32* %ptr, i32** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.4*, %class.btAlignedAllocator.4** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #7
  %1 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4, !tbaa !12
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !12
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !12
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #7
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !8
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !55
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data, align 4, !tbaa !38
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !39
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !56
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorI6btFaceLj16EEC2Ev(%class.btAlignedAllocator.1* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  ret %class.btAlignedAllocator.1* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI6btFaceE4initEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !57
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.btFace* null, %struct.btFace** %m_data, align 4, !tbaa !18
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !14
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !58
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !8
  store i32 %last, i32* %last.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %first.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %last.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !38
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !38
  %tobool = icmp ne %class.btVector3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !55, !range !24
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btVector3*, %class.btVector3** %m_data4, align 4, !tbaa !38
  call void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %class.btVector3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data5, align 4, !tbaa !38
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %class.btVector3* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.btVector3*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %ptr, %class.btVector3** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVector3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI6btFaceE5clearEv(%class.btAlignedObjectArray.0* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI6btFaceE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI6btFaceE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI6btFaceE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI6btFaceE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !8
  store i32 %last, i32* %last.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %first.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %last.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %4 = load %struct.btFace*, %struct.btFace** %m_data, align 4, !tbaa !18
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %struct.btFace, %struct.btFace* %4, i32 %5
  %call = call %struct.btFace* @_ZN6btFaceD2Ev(%struct.btFace* %arrayidx) #7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI6btFaceE10deallocateEv(%class.btAlignedObjectArray.0* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btFace*, %struct.btFace** %m_data, align 4, !tbaa !18
  %tobool = icmp ne %struct.btFace* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !57, !range !24
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %struct.btFace*, %struct.btFace** %m_data4, align 4, !tbaa !18
  call void @_ZN18btAlignedAllocatorI6btFaceLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %m_allocator, %struct.btFace* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.btFace* null, %struct.btFace** %m_data5, align 4, !tbaa !18
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %struct.btFace* @_ZN6btFaceD2Ev(%struct.btFace* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btFace*, align 4
  store %struct.btFace* %this, %struct.btFace** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btFace*, %struct.btFace** %this.addr, align 4
  %m_indices = getelementptr inbounds %struct.btFace, %struct.btFace* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.3* %m_indices) #7
  ret %struct.btFace* %this1
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI6btFaceLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %this, %struct.btFace* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca %struct.btFace*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  store %struct.btFace* %ptr, %struct.btFace** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load %struct.btFace*, %struct.btFace** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btFace* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

define linkonce_odr hidden i32 @_ZNK9btHashMapI20btInternalVertexPair14btInternalEdgeE9findIndexERKS0_(%class.btHashMap* %this, %struct.btInternalVertexPair* nonnull align 2 dereferenceable(4) %key) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btHashMap*, align 4
  %key.addr = alloca %struct.btInternalVertexPair*, align 4
  %hash = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %index = alloca i32, align 4
  store %class.btHashMap* %this, %class.btHashMap** %this.addr, align 4, !tbaa !2
  store %struct.btInternalVertexPair* %key, %struct.btInternalVertexPair** %key.addr, align 4, !tbaa !2
  %this1 = load %class.btHashMap*, %class.btHashMap** %this.addr, align 4
  %0 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %key.addr, align 4, !tbaa !2
  %call = call i32 @_ZNK20btInternalVertexPair7getHashEv(%struct.btInternalVertexPair* %1)
  %m_valueArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE8capacityEv(%class.btAlignedObjectArray.8* %m_valueArray)
  %sub = sub nsw i32 %call2, 1
  %and = and i32 %call, %sub
  store i32 %and, i32* %hash, align 4, !tbaa !8
  %2 = load i32, i32* %hash, align 4, !tbaa !8
  %m_hashTable = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %m_hashTable)
  %cmp = icmp uge i32 %2, %call3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %3 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %m_hashTable4 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %4 = load i32, i32* %hash, align 4, !tbaa !8
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNK20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_hashTable4, i32 %4)
  %5 = load i32, i32* %call5, align 4, !tbaa !8
  store i32 %5, i32* %index, align 4, !tbaa !8
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end
  %6 = load i32, i32* %index, align 4, !tbaa !8
  %cmp6 = icmp ne i32 %6, -1
  br i1 %cmp6, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %7 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %key.addr, align 4, !tbaa !2
  %m_keyArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 3
  %8 = load i32, i32* %index, align 4, !tbaa !8
  %call7 = call nonnull align 2 dereferenceable(4) %struct.btInternalVertexPair* @_ZNK20btAlignedObjectArrayI20btInternalVertexPairEixEi(%class.btAlignedObjectArray.12* %m_keyArray, i32 %8)
  %call8 = call zeroext i1 @_ZNK20btInternalVertexPair6equalsERKS_(%struct.btInternalVertexPair* %7, %struct.btInternalVertexPair* nonnull align 2 dereferenceable(4) %call7)
  %conv = zext i1 %call8 to i32
  %cmp9 = icmp eq i32 %conv, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %9 = phi i1 [ false, %while.cond ], [ %cmp9, %land.rhs ]
  br i1 %9, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %m_next = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %10 = load i32, i32* %index, align 4, !tbaa !8
  %call10 = call nonnull align 4 dereferenceable(4) i32* @_ZNK20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_next, i32 %10)
  %11 = load i32, i32* %call10, align 4, !tbaa !8
  store i32 %11, i32* %index, align 4, !tbaa !8
  br label %while.cond

while.end:                                        ; preds = %land.end
  %12 = load i32, i32* %index, align 4, !tbaa !8
  store i32 %12, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %13 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #7
  br label %cleanup

cleanup:                                          ; preds = %while.end, %if.then
  %14 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #7
  %15 = load i32, i32* %retval, align 4
  ret i32 %15
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 2 dereferenceable(4) %struct.btInternalEdge* @_ZN20btAlignedObjectArrayI14btInternalEdgeEixEi(%class.btAlignedObjectArray.8* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load %struct.btInternalEdge*, %struct.btInternalEdge** %m_data, align 4, !tbaa !46
  %1 = load i32, i32* %n.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %struct.btInternalEdge, %struct.btInternalEdge* %0, i32 %1
  ret %struct.btInternalEdge* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK20btInternalVertexPair7getHashEv(%struct.btInternalVertexPair* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btInternalVertexPair*, align 4
  store %struct.btInternalVertexPair* %this, %struct.btInternalVertexPair** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %this.addr, align 4
  %m_v0 = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %this1, i32 0, i32 0
  %0 = load i16, i16* %m_v0, align 2, !tbaa !22
  %conv = sext i16 %0 to i32
  %m_v1 = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %this1, i32 0, i32 1
  %1 = load i16, i16* %m_v1, align 2, !tbaa !19
  %conv2 = sext i16 %1 to i32
  %shl = shl i32 %conv2, 16
  %add = add nsw i32 %conv, %shl
  ret i32 %add
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE8capacityEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !48
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNK20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !36
  %1 = load i32, i32* %n.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZNK20btInternalVertexPair6equalsERKS_(%struct.btInternalVertexPair* %this, %struct.btInternalVertexPair* nonnull align 2 dereferenceable(4) %other) #2 comdat {
entry:
  %this.addr = alloca %struct.btInternalVertexPair*, align 4
  %other.addr = alloca %struct.btInternalVertexPair*, align 4
  store %struct.btInternalVertexPair* %this, %struct.btInternalVertexPair** %this.addr, align 4, !tbaa !2
  store %struct.btInternalVertexPair* %other, %struct.btInternalVertexPair** %other.addr, align 4, !tbaa !2
  %this1 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %this.addr, align 4
  %m_v0 = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %this1, i32 0, i32 0
  %0 = load i16, i16* %m_v0, align 2, !tbaa !22
  %conv = sext i16 %0 to i32
  %1 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %other.addr, align 4, !tbaa !2
  %m_v02 = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %1, i32 0, i32 0
  %2 = load i16, i16* %m_v02, align 2, !tbaa !22
  %conv3 = sext i16 %2 to i32
  %cmp = icmp eq i32 %conv, %conv3
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %m_v1 = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %this1, i32 0, i32 1
  %3 = load i16, i16* %m_v1, align 2, !tbaa !19
  %conv4 = sext i16 %3 to i32
  %4 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %other.addr, align 4, !tbaa !2
  %m_v15 = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %4, i32 0, i32 1
  %5 = load i16, i16* %m_v15, align 2, !tbaa !19
  %conv6 = sext i16 %5 to i32
  %cmp7 = icmp eq i32 %conv4, %conv6
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %6 = phi i1 [ false, %entry ], [ %cmp7, %land.rhs ]
  ret i1 %6
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 2 dereferenceable(4) %struct.btInternalVertexPair* @_ZNK20btAlignedObjectArrayI20btInternalVertexPairEixEi(%class.btAlignedObjectArray.12* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %0 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %m_data, align 4, !tbaa !52
  %1 = load i32, i32* %n.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %0, i32 %1
  ret %struct.btInternalVertexPair* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !56
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btVector3*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btVector3** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %call2 = call i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btVector3*
  store %class.btVector3* %3, %class.btVector3** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  %4 = load %class.btVector3*, %class.btVector3** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %class.btVector3* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !55
  %5 = load %class.btVector3*, %class.btVector3** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* %5, %class.btVector3** %m_data, align 4, !tbaa !38
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !56
  %7 = bitcast %class.btVector3** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !8
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN9btVector3nwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !59
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #4 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %class.btVector3** null)
  %2 = bitcast %class.btVector3* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %class.btVector3* %dest) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !8
  store i32 %end, i32* %end.addr, align 4, !tbaa !8
  store %class.btVector3* %dest, %class.btVector3** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %start.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %end.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btVector3*, %class.btVector3** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 %5
  %6 = bitcast %class.btVector3* %arrayidx to i8*
  %call = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %6)
  %7 = bitcast i8* %call to %class.btVector3*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !38
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 %9
  %10 = bitcast %class.btVector3* %7 to i8*
  %11 = bitcast %class.btVector3* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !10
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #7
  ret void
}

define linkonce_odr hidden %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %class.btVector3** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btVector3**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  store %class.btVector3** %hint, %class.btVector3*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !8
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btVector3*
  ret %class.btVector3* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #6

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI14btInternalEdgeE9push_backERKS0_(%class.btAlignedObjectArray.8* %this, %struct.btInternalEdge* nonnull align 2 dereferenceable(4) %_Val) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %_Val.addr = alloca %struct.btInternalEdge*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store %struct.btInternalEdge* %_Val, %struct.btInternalEdge** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !8
  %1 = load i32, i32* %sz, align 4, !tbaa !8
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE8capacityEv(%class.btAlignedObjectArray.8* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI14btInternalEdgeE9allocSizeEi(%class.btAlignedObjectArray.8* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI14btInternalEdgeE7reserveEi(%class.btAlignedObjectArray.8* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %2 = load %struct.btInternalEdge*, %struct.btInternalEdge** %m_data, align 4, !tbaa !46
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !47
  %arrayidx = getelementptr inbounds %struct.btInternalEdge, %struct.btInternalEdge* %2, i32 %3
  %4 = bitcast %struct.btInternalEdge* %arrayidx to i8*
  %5 = bitcast i8* %4 to %struct.btInternalEdge*
  %6 = load %struct.btInternalEdge*, %struct.btInternalEdge** %_Val.addr, align 4, !tbaa !2
  %7 = bitcast %struct.btInternalEdge* %5 to i8*
  %8 = bitcast %struct.btInternalEdge* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %7, i8* align 2 %8, i32 4, i1 false), !tbaa.struct !40
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %9 = load i32, i32* %m_size5, align 4, !tbaa !47
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %m_size5, align 4, !tbaa !47
  %10 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btInternalVertexPairE9push_backERKS0_(%class.btAlignedObjectArray.12* %this, %struct.btInternalVertexPair* nonnull align 2 dereferenceable(4) %_Val) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %_Val.addr = alloca %struct.btInternalVertexPair*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store %struct.btInternalVertexPair* %_Val, %struct.btInternalVertexPair** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btInternalVertexPairE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !8
  %1 = load i32, i32* %sz, align 4, !tbaa !8
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI20btInternalVertexPairE8capacityEv(%class.btAlignedObjectArray.12* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI20btInternalVertexPairE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI20btInternalVertexPairE9allocSizeEi(%class.btAlignedObjectArray.12* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI20btInternalVertexPairE7reserveEi(%class.btAlignedObjectArray.12* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %2 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %m_data, align 4, !tbaa !52
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !53
  %arrayidx = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %2, i32 %3
  %4 = bitcast %struct.btInternalVertexPair* %arrayidx to i8*
  %5 = bitcast i8* %4 to %struct.btInternalVertexPair*
  %6 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %_Val.addr, align 4, !tbaa !2
  %7 = bitcast %struct.btInternalVertexPair* %5 to i8*
  %8 = bitcast %struct.btInternalVertexPair* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %7, i8* align 2 %8, i32 4, i1 false), !tbaa.struct !40
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %9 = load i32, i32* %m_size5, align 4, !tbaa !53
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %m_size5, align 4, !tbaa !53
  %10 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #7
  ret void
}

define linkonce_odr hidden void @_ZN9btHashMapI20btInternalVertexPair14btInternalEdgeE10growTablesERKS0_(%class.btHashMap* %this, %struct.btInternalVertexPair* nonnull align 2 dereferenceable(4) %0) #0 comdat {
entry:
  %this.addr = alloca %class.btHashMap*, align 4
  %.addr = alloca %struct.btInternalVertexPair*, align 4
  %newCapacity = alloca i32, align 4
  %curHashtableSize = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp6 = alloca i32, align 4
  %i = alloca i32, align 4
  %hashValue = alloca i32, align 4
  store %class.btHashMap* %this, %class.btHashMap** %this.addr, align 4, !tbaa !2
  store %struct.btInternalVertexPair* %0, %struct.btInternalVertexPair** %.addr, align 4, !tbaa !2
  %this1 = load %class.btHashMap*, %class.btHashMap** %this.addr, align 4
  %1 = bitcast i32* %newCapacity to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %m_valueArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE8capacityEv(%class.btAlignedObjectArray.8* %m_valueArray)
  store i32 %call, i32* %newCapacity, align 4, !tbaa !8
  %m_hashTable = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %m_hashTable)
  %2 = load i32, i32* %newCapacity, align 4, !tbaa !8
  %cmp = icmp slt i32 %call2, %2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %curHashtableSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %m_hashTable3 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %m_hashTable3)
  store i32 %call4, i32* %curHashtableSize, align 4, !tbaa !8
  %m_hashTable5 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %4 = load i32, i32* %newCapacity, align 4, !tbaa !8
  %5 = bitcast i32* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  store i32 0, i32* %ref.tmp, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.3* %m_hashTable5, i32 %4, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  %6 = bitcast i32* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #7
  %m_next = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %7 = load i32, i32* %newCapacity, align 4, !tbaa !8
  %8 = bitcast i32* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  store i32 0, i32* %ref.tmp6, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.3* %m_next, i32 %7, i32* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %9 = bitcast i32* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #7
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %11 = load i32, i32* %i, align 4, !tbaa !8
  %12 = load i32, i32* %newCapacity, align 4, !tbaa !8
  %cmp7 = icmp slt i32 %11, %12
  br i1 %cmp7, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_hashTable8 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %13 = load i32, i32* %i, align 4, !tbaa !8
  %call9 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_hashTable8, i32 %13)
  store i32 -1, i32* %call9, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %14 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc15, %for.end
  %15 = load i32, i32* %i, align 4, !tbaa !8
  %16 = load i32, i32* %newCapacity, align 4, !tbaa !8
  %cmp11 = icmp slt i32 %15, %16
  br i1 %cmp11, label %for.body12, label %for.end17

for.body12:                                       ; preds = %for.cond10
  %m_next13 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %17 = load i32, i32* %i, align 4, !tbaa !8
  %call14 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_next13, i32 %17)
  store i32 -1, i32* %call14, align 4, !tbaa !8
  br label %for.inc15

for.inc15:                                        ; preds = %for.body12
  %18 = load i32, i32* %i, align 4, !tbaa !8
  %inc16 = add nsw i32 %18, 1
  store i32 %inc16, i32* %i, align 4, !tbaa !8
  br label %for.cond10

for.end17:                                        ; preds = %for.cond10
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc31, %for.end17
  %19 = load i32, i32* %i, align 4, !tbaa !8
  %20 = load i32, i32* %curHashtableSize, align 4, !tbaa !8
  %cmp19 = icmp slt i32 %19, %20
  br i1 %cmp19, label %for.body20, label %for.end33

for.body20:                                       ; preds = %for.cond18
  %21 = bitcast i32* %hashValue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #7
  %m_keyArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 3
  %22 = load i32, i32* %i, align 4, !tbaa !8
  %call21 = call nonnull align 2 dereferenceable(4) %struct.btInternalVertexPair* @_ZN20btAlignedObjectArrayI20btInternalVertexPairEixEi(%class.btAlignedObjectArray.12* %m_keyArray, i32 %22)
  %call22 = call i32 @_ZNK20btInternalVertexPair7getHashEv(%struct.btInternalVertexPair* %call21)
  %m_valueArray23 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call24 = call i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE8capacityEv(%class.btAlignedObjectArray.8* %m_valueArray23)
  %sub = sub nsw i32 %call24, 1
  %and = and i32 %call22, %sub
  store i32 %and, i32* %hashValue, align 4, !tbaa !8
  %m_hashTable25 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %23 = load i32, i32* %hashValue, align 4, !tbaa !8
  %call26 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_hashTable25, i32 %23)
  %24 = load i32, i32* %call26, align 4, !tbaa !8
  %m_next27 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %25 = load i32, i32* %i, align 4, !tbaa !8
  %call28 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_next27, i32 %25)
  store i32 %24, i32* %call28, align 4, !tbaa !8
  %26 = load i32, i32* %i, align 4, !tbaa !8
  %m_hashTable29 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %27 = load i32, i32* %hashValue, align 4, !tbaa !8
  %call30 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_hashTable29, i32 %27)
  store i32 %26, i32* %call30, align 4, !tbaa !8
  %28 = bitcast i32* %hashValue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #7
  br label %for.inc31

for.inc31:                                        ; preds = %for.body20
  %29 = load i32, i32* %i, align 4, !tbaa !8
  %inc32 = add nsw i32 %29, 1
  store i32 %inc32, i32* %i, align 4, !tbaa !8
  br label %for.cond18

for.end33:                                        ; preds = %for.cond18
  %30 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #7
  %31 = bitcast i32* %curHashtableSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #7
  br label %if.end

if.end:                                           ; preds = %for.end33, %entry
  %32 = bitcast i32* %newCapacity to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI14btInternalEdgeE7reserveEi(%class.btAlignedObjectArray.8* %this, i32 %_Count) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btInternalEdge*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE8capacityEv(%class.btAlignedObjectArray.8* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btInternalEdge** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %call2 = call i8* @_ZN20btAlignedObjectArrayI14btInternalEdgeE8allocateEi(%class.btAlignedObjectArray.8* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.btInternalEdge*
  store %struct.btInternalEdge* %3, %struct.btInternalEdge** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  %4 = load %struct.btInternalEdge*, %struct.btInternalEdge** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI14btInternalEdgeE4copyEiiPS0_(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call3, %struct.btInternalEdge* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayI14btInternalEdgeE7destroyEii(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI14btInternalEdgeE10deallocateEv(%class.btAlignedObjectArray.8* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !43
  %5 = load %struct.btInternalEdge*, %struct.btInternalEdge** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %struct.btInternalEdge* %5, %struct.btInternalEdge** %m_data, align 4, !tbaa !46
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !48
  %7 = bitcast %struct.btInternalEdge** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI14btInternalEdgeE9allocSizeEi(%class.btAlignedObjectArray.8* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !8
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI14btInternalEdgeE8allocateEi(%class.btAlignedObjectArray.8* %this, i32 %size) #4 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !8
  %call = call %struct.btInternalEdge* @_ZN18btAlignedAllocatorI14btInternalEdgeLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.9* %m_allocator, i32 %1, %struct.btInternalEdge** null)
  %2 = bitcast %struct.btInternalEdge* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI14btInternalEdgeE4copyEiiPS0_(%class.btAlignedObjectArray.8* %this, i32 %start, i32 %end, %struct.btInternalEdge* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btInternalEdge*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !8
  store i32 %end, i32* %end.addr, align 4, !tbaa !8
  store %struct.btInternalEdge* %dest, %struct.btInternalEdge** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %start.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %end.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.btInternalEdge*, %struct.btInternalEdge** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %struct.btInternalEdge, %struct.btInternalEdge* %4, i32 %5
  %6 = bitcast %struct.btInternalEdge* %arrayidx to i8*
  %7 = bitcast i8* %6 to %struct.btInternalEdge*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %8 = load %struct.btInternalEdge*, %struct.btInternalEdge** %m_data, align 4, !tbaa !46
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx2 = getelementptr inbounds %struct.btInternalEdge, %struct.btInternalEdge* %8, i32 %9
  %10 = bitcast %struct.btInternalEdge* %7 to i8*
  %11 = bitcast %struct.btInternalEdge* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %10, i8* align 2 %11, i32 4, i1 false), !tbaa.struct !40
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #7
  ret void
}

define linkonce_odr hidden %struct.btInternalEdge* @_ZN18btAlignedAllocatorI14btInternalEdgeLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.9* %this, i32 %n, %struct.btInternalEdge** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btInternalEdge**, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  store %struct.btInternalEdge** %hint, %struct.btInternalEdge*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !8
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btInternalEdge*
  ret %struct.btInternalEdge* %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI20btInternalVertexPairE8capacityEv(%class.btAlignedObjectArray.12* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !54
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btInternalVertexPairE7reserveEi(%class.btAlignedObjectArray.12* %this, i32 %_Count) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btInternalVertexPair*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btInternalVertexPairE8capacityEv(%class.btAlignedObjectArray.12* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btInternalVertexPair** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %call2 = call i8* @_ZN20btAlignedObjectArrayI20btInternalVertexPairE8allocateEi(%class.btAlignedObjectArray.12* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.btInternalVertexPair*
  store %struct.btInternalVertexPair* %3, %struct.btInternalVertexPair** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI20btInternalVertexPairE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  %4 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI20btInternalVertexPairE4copyEiiPS0_(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call3, %struct.btInternalVertexPair* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI20btInternalVertexPairE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  call void @_ZN20btAlignedObjectArrayI20btInternalVertexPairE7destroyEii(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI20btInternalVertexPairE10deallocateEv(%class.btAlignedObjectArray.12* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !49
  %5 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store %struct.btInternalVertexPair* %5, %struct.btInternalVertexPair** %m_data, align 4, !tbaa !52
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !54
  %7 = bitcast %struct.btInternalVertexPair** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI20btInternalVertexPairE9allocSizeEi(%class.btAlignedObjectArray.12* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !8
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI20btInternalVertexPairE8allocateEi(%class.btAlignedObjectArray.12* %this, i32 %size) #4 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !8
  %call = call %struct.btInternalVertexPair* @_ZN18btAlignedAllocatorI20btInternalVertexPairLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.13* %m_allocator, i32 %1, %struct.btInternalVertexPair** null)
  %2 = bitcast %struct.btInternalVertexPair* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI20btInternalVertexPairE4copyEiiPS0_(%class.btAlignedObjectArray.12* %this, i32 %start, i32 %end, %struct.btInternalVertexPair* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btInternalVertexPair*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !8
  store i32 %end, i32* %end.addr, align 4, !tbaa !8
  store %struct.btInternalVertexPair* %dest, %struct.btInternalVertexPair** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %start.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %end.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %4, i32 %5
  %6 = bitcast %struct.btInternalVertexPair* %arrayidx to i8*
  %7 = bitcast i8* %6 to %struct.btInternalVertexPair*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %8 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %m_data, align 4, !tbaa !52
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx2 = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %8, i32 %9
  %10 = bitcast %struct.btInternalVertexPair* %7 to i8*
  %11 = bitcast %struct.btInternalVertexPair* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %10, i8* align 2 %11, i32 4, i1 false), !tbaa.struct !40
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #7
  ret void
}

define linkonce_odr hidden %struct.btInternalVertexPair* @_ZN18btAlignedAllocatorI20btInternalVertexPairLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.13* %this, i32 %n, %struct.btInternalVertexPair** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.13*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btInternalVertexPair**, align 4
  store %class.btAlignedAllocator.13* %this, %class.btAlignedAllocator.13** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  store %struct.btInternalVertexPair** %hint, %struct.btInternalVertexPair*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.13*, %class.btAlignedAllocator.13** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !8
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btInternalVertexPair*
  ret %struct.btInternalVertexPair* %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.3* %this, i32 %newsize, i32* nonnull align 4 dereferenceable(4) %fillData) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca i32*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !8
  store i32* %fillData, i32** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !8
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %2 = load i32, i32* %curSize, align 4, !tbaa !8
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  store i32 %4, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %6 = load i32, i32* %curSize, align 4, !tbaa !8
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %8 = load i32*, i32** %m_data, align 4, !tbaa !36
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds i32, i32* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.3* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = load i32, i32* %curSize, align 4, !tbaa !8
  store i32 %14, i32* %i6, align 4, !tbaa !8
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !8
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %18 = load i32*, i32** %m_data11, align 4, !tbaa !36
  %19 = load i32, i32* %i6, align 4, !tbaa !8
  %arrayidx12 = getelementptr inbounds i32, i32* %18, i32 %19
  %20 = bitcast i32* %arrayidx12 to i8*
  %21 = bitcast i8* %20 to i32*
  %22 = load i32*, i32** %fillData.addr, align 4, !tbaa !2
  %23 = load i32, i32* %22, align 4, !tbaa !8
  store i32 %23, i32* %21, align 4, !tbaa !8
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %24 = load i32, i32* %i6, align 4, !tbaa !8
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !8
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %25 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 2
  store i32 %25, i32* %m_size, align 4, !tbaa !33
  %26 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 2 dereferenceable(4) %struct.btInternalVertexPair* @_ZN20btAlignedObjectArrayI20btInternalVertexPairEixEi(%class.btAlignedObjectArray.12* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %0 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %m_data, align 4, !tbaa !52
  %1 = load i32, i32* %n.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %0, i32 %1
  ret %struct.btInternalVertexPair* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.3* %this, i32 %_Count) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca i32*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.3* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast i32** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %call2 = call i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.3* %this1, i32 %2)
  %3 = bitcast i8* %call2 to i32*
  store i32* %3, i32** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  %4 = load i32*, i32** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.3* %this1, i32 0, i32 %call3, i32* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.3* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.3* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !41
  %5 = load i32*, i32** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  store i32* %5, i32** %m_data, align 4, !tbaa !36
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !42
  %7 = bitcast i32** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !42
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.3* %this, i32 %size) #4 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !8
  %call = call i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.4* %m_allocator, i32 %1, i32** null)
  %2 = bitcast i32* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.3* %this, i32 %start, i32 %end, i32* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !8
  store i32 %end, i32* %end.addr, align 4, !tbaa !8
  store i32* %dest, i32** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %start.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %end.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i32*, i32** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  %6 = bitcast i32* %arrayidx to i8*
  %7 = bitcast i8* %6 to i32*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %8 = load i32*, i32** %m_data, align 4, !tbaa !36
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx2 = getelementptr inbounds i32, i32* %8, i32 %9
  %10 = load i32, i32* %arrayidx2, align 4, !tbaa !8
  store i32 %10, i32* %7, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #7
  ret void
}

define linkonce_odr hidden i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.4* %this, i32 %n, i32** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.4*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca i32**, align 4
  store %class.btAlignedAllocator.4* %this, %class.btAlignedAllocator.4** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  store i32** %hint, i32*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.4*, %class.btAlignedAllocator.4** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !8
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to i32*
  ret i32* %1
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind readnone speculatable willreturn }
attributes #6 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"int", !4, i64 0}
!10 = !{i64 0, i64 16, !11}
!11 = !{!4, !4, i64 0}
!12 = !{!13, !13, i64 0}
!13 = !{!"float", !4, i64 0}
!14 = !{!15, !9, i64 4}
!15 = !{!"_ZTS20btAlignedObjectArrayI6btFaceE", !16, i64 0, !9, i64 4, !9, i64 8, !3, i64 12, !17, i64 16}
!16 = !{!"_ZTS18btAlignedAllocatorI6btFaceLj16EE"}
!17 = !{!"bool", !4, i64 0}
!18 = !{!15, !3, i64 12}
!19 = !{!20, !21, i64 2}
!20 = !{!"_ZTS20btInternalVertexPair", !21, i64 0, !21, i64 2}
!21 = !{!"short", !4, i64 0}
!22 = !{!20, !21, i64 0}
!23 = !{!17, !17, i64 0}
!24 = !{i8 0, i8 2}
!25 = !{!26, !21, i64 2}
!26 = !{!"_ZTS14btInternalEdge", !21, i64 0, !21, i64 2}
!27 = !{!26, !21, i64 0}
!28 = !{!29, !13, i64 96}
!29 = !{!"_ZTS18btConvexPolyhedron", !30, i64 4, !15, i64 24, !30, i64 44, !32, i64 64, !32, i64 80, !13, i64 96, !32, i64 100, !32, i64 116}
!30 = !{!"_ZTS20btAlignedObjectArrayI9btVector3E", !31, i64 0, !9, i64 4, !9, i64 8, !3, i64 12, !17, i64 16}
!31 = !{!"_ZTS18btAlignedAllocatorI9btVector3Lj16EE"}
!32 = !{!"_ZTS9btVector3", !4, i64 0}
!33 = !{!34, !9, i64 4}
!34 = !{!"_ZTS20btAlignedObjectArrayIiE", !35, i64 0, !9, i64 4, !9, i64 8, !3, i64 12, !17, i64 16}
!35 = !{!"_ZTS18btAlignedAllocatorIiLj16EE"}
!36 = !{!34, !3, i64 12}
!37 = !{!21, !21, i64 0}
!38 = !{!30, !3, i64 12}
!39 = !{!30, !9, i64 4}
!40 = !{i64 0, i64 2, !37, i64 2, i64 2, !37}
!41 = !{!34, !17, i64 16}
!42 = !{!34, !9, i64 8}
!43 = !{!44, !17, i64 16}
!44 = !{!"_ZTS20btAlignedObjectArrayI14btInternalEdgeE", !45, i64 0, !9, i64 4, !9, i64 8, !3, i64 12, !17, i64 16}
!45 = !{!"_ZTS18btAlignedAllocatorI14btInternalEdgeLj16EE"}
!46 = !{!44, !3, i64 12}
!47 = !{!44, !9, i64 4}
!48 = !{!44, !9, i64 8}
!49 = !{!50, !17, i64 16}
!50 = !{!"_ZTS20btAlignedObjectArrayI20btInternalVertexPairE", !51, i64 0, !9, i64 4, !9, i64 8, !3, i64 12, !17, i64 16}
!51 = !{!"_ZTS18btAlignedAllocatorI20btInternalVertexPairLj16EE"}
!52 = !{!50, !3, i64 12}
!53 = !{!50, !9, i64 4}
!54 = !{!50, !9, i64 8}
!55 = !{!30, !17, i64 16}
!56 = !{!30, !9, i64 8}
!57 = !{!15, !17, i64 16}
!58 = !{!15, !9, i64 8}
!59 = !{!60, !60, i64 0}
!60 = !{!"long", !4, i64 0}
