; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btScaledBvhTriangleMeshShape.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btScaledBvhTriangleMeshShape.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btScaledBvhTriangleMeshShape = type { %class.btConcaveShape, %class.btVector3, %class.btBvhTriangleMeshShape* }
%class.btConcaveShape = type { %class.btCollisionShape, float }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%class.btVector3 = type { [4 x float] }
%class.btBvhTriangleMeshShape = type <{ %class.btTriangleMeshShape, %class.btOptimizedBvh*, %struct.btTriangleInfoMap*, i8, i8, [11 x i8], [3 x i8] }>
%class.btTriangleMeshShape = type { %class.btConcaveShape, %class.btVector3, %class.btVector3, %class.btStridingMeshInterface* }
%class.btStridingMeshInterface = type { i32 (...)**, %class.btVector3 }
%class.btOptimizedBvh = type { %class.btQuantizedBvh }
%class.btQuantizedBvh = type { i32 (...)**, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32, i8, [3 x i8], %class.btAlignedObjectArray, %class.btAlignedObjectArray, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0, i32, %class.btAlignedObjectArray.4, i32 }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btOptimizedBvhNode*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btOptimizedBvhNode = type { %class.btVector3, %class.btVector3, i32, i32, i32, [20 x i8] }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.btQuantizedBvhNode*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btQuantizedBvhNode = type { [3 x i16], [3 x i16], i32 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %class.btBvhSubtreeInfo*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btBvhSubtreeInfo = type { [3 x i16], [3 x i16], i32, i32, [3 x i32] }
%struct.btTriangleInfoMap = type { i32 (...)**, %class.btHashMap, float, float, float, float, float, float }
%class.btHashMap = type { %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.16 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, %struct.btTriangleInfo*, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%struct.btTriangleInfo = type { i32, float, float, float }
%class.btAlignedObjectArray.16 = type <{ %class.btAlignedAllocator.17, [3 x i8], i32, i32, %class.btHashInt*, i8, [3 x i8] }>
%class.btAlignedAllocator.17 = type { i8 }
%class.btHashInt = type { i32 }
%class.btTriangleCallback = type { i32 (...)** }
%class.btScaledTriangleCallback = type { %class.btTriangleCallback, %class.btTriangleCallback*, %class.btVector3 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btSerializer = type { i32 (...)** }
%struct.btScaledTriangleMeshShapeData = type { %struct.btTriangleMeshShapeData, %struct.btVector3FloatData }
%struct.btTriangleMeshShapeData = type { %struct.btCollisionShapeData, %struct.btStridingMeshInterfaceData, %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhDoubleData*, %struct.btTriangleInfoMapData*, float, [4 x i8] }
%struct.btCollisionShapeData = type { i8*, i32, [4 x i8] }
%struct.btStridingMeshInterfaceData = type { %struct.btMeshPartData*, %struct.btVector3FloatData, i32, [4 x i8] }
%struct.btMeshPartData = type { %struct.btVector3FloatData*, %struct.btVector3DoubleData*, %struct.btIntIndexData*, %struct.btShortIntIndexTripletData*, %struct.btCharIndexTripletData*, %struct.btShortIntIndexData*, i32, i32 }
%struct.btVector3DoubleData = type { [4 x double] }
%struct.btIntIndexData = type { i32 }
%struct.btShortIntIndexTripletData = type { [3 x i16], [2 x i8] }
%struct.btCharIndexTripletData = type { [3 x i8], i8 }
%struct.btShortIntIndexData = type { i16, [2 x i8] }
%struct.btQuantizedBvhFloatData = type { %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, i32, i32, i32, i32, %struct.btOptimizedBvhNodeFloatData*, %struct.btQuantizedBvhNodeData*, %struct.btBvhSubtreeInfoData*, i32, i32 }
%struct.btOptimizedBvhNodeFloatData = type { %struct.btVector3FloatData, %struct.btVector3FloatData, i32, i32, i32, [4 x i8] }
%struct.btQuantizedBvhNodeData = type { [3 x i16], [3 x i16], i32 }
%struct.btBvhSubtreeInfoData = type { i32, i32, [3 x i16], [3 x i16] }
%struct.btQuantizedBvhDoubleData = type { %struct.btVector3DoubleData, %struct.btVector3DoubleData, %struct.btVector3DoubleData, i32, i32, i32, i32, %struct.btOptimizedBvhNodeDoubleData*, %struct.btQuantizedBvhNodeData*, i32, i32, %struct.btBvhSubtreeInfoData* }
%struct.btOptimizedBvhNodeDoubleData = type { %struct.btVector3DoubleData, %struct.btVector3DoubleData, i32, i32, i32, [4 x i8] }
%struct.btTriangleInfoMapData = type { i32*, i32*, %struct.btTriangleInfoData*, i32*, float, float, float, float, float, i32, i32, i32, i32, [4 x i8] }
%struct.btTriangleInfoData = type { i32, float, float, float }
%struct.btVector3FloatData = type { [4 x float] }

$_ZN28btScaledBvhTriangleMeshShapedlEPv = comdat any

$_ZN24btScaledTriangleCallbackC2EP18btTriangleCallbackRK9btVector3 = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZNK19btTriangleMeshShape15getLocalAabbMinEv = comdat any

$_ZNK19btTriangleMeshShape15getLocalAabbMaxEv = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x38absoluteEv = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZN11btMatrix3x3ixEi = comdat any

$_ZNK28btScaledBvhTriangleMeshShape7getNameEv = comdat any

$_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZN14btConcaveShape9setMarginEf = comdat any

$_ZNK14btConcaveShape9getMarginEv = comdat any

$_ZNK28btScaledBvhTriangleMeshShape28calculateSerializeBufferSizeEv = comdat any

$_ZNK28btScaledBvhTriangleMeshShape9serializeEPvP12btSerializer = comdat any

$_ZN18btTriangleCallbackC2Ev = comdat any

$_ZN24btScaledTriangleCallbackD0Ev = comdat any

$_ZN24btScaledTriangleCallback15processTriangleEP9btVector3ii = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_Z6btFabsf = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZNK9btVector314serializeFloatER18btVector3FloatData = comdat any

$_ZTV24btScaledTriangleCallback = comdat any

$_ZTS24btScaledTriangleCallback = comdat any

$_ZTI24btScaledTriangleCallback = comdat any

@_ZTV28btScaledBvhTriangleMeshShape = hidden unnamed_addr constant { [19 x i8*] } { [19 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI28btScaledBvhTriangleMeshShape to i8*), i8* bitcast (%class.btScaledBvhTriangleMeshShape* (%class.btScaledBvhTriangleMeshShape*)* @_ZN28btScaledBvhTriangleMeshShapeD1Ev to i8*), i8* bitcast (void (%class.btScaledBvhTriangleMeshShape*)* @_ZN28btScaledBvhTriangleMeshShapeD0Ev to i8*), i8* bitcast (void (%class.btScaledBvhTriangleMeshShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK28btScaledBvhTriangleMeshShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btScaledBvhTriangleMeshShape*, %class.btVector3*)* @_ZN28btScaledBvhTriangleMeshShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btScaledBvhTriangleMeshShape*)* @_ZNK28btScaledBvhTriangleMeshShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btScaledBvhTriangleMeshShape*, float, %class.btVector3*)* @_ZNK28btScaledBvhTriangleMeshShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btScaledBvhTriangleMeshShape*)* @_ZNK28btScaledBvhTriangleMeshShape7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btConcaveShape*, float)* @_ZN14btConcaveShape9setMarginEf to i8*), i8* bitcast (float (%class.btConcaveShape*)* @_ZNK14btConcaveShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btScaledBvhTriangleMeshShape*)* @_ZNK28btScaledBvhTriangleMeshShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btScaledBvhTriangleMeshShape*, i8*, %class.btSerializer*)* @_ZNK28btScaledBvhTriangleMeshShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btScaledBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)* @_ZNK28btScaledBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_ to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS28btScaledBvhTriangleMeshShape = hidden constant [31 x i8] c"28btScaledBvhTriangleMeshShape\00", align 1
@_ZTI14btConcaveShape = external constant i8*
@_ZTI28btScaledBvhTriangleMeshShape = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([31 x i8], [31 x i8]* @_ZTS28btScaledBvhTriangleMeshShape, i32 0, i32 0), i8* bitcast (i8** @_ZTI14btConcaveShape to i8*) }, align 4
@_ZTV24btScaledTriangleCallback = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI24btScaledTriangleCallback to i8*), i8* bitcast (%class.btTriangleCallback* (%class.btTriangleCallback*)* @_ZN18btTriangleCallbackD2Ev to i8*), i8* bitcast (void (%class.btScaledTriangleCallback*)* @_ZN24btScaledTriangleCallbackD0Ev to i8*), i8* bitcast (void (%class.btScaledTriangleCallback*, %class.btVector3*, i32, i32)* @_ZN24btScaledTriangleCallback15processTriangleEP9btVector3ii to i8*)] }, comdat, align 4
@_ZTS24btScaledTriangleCallback = linkonce_odr hidden constant [27 x i8] c"24btScaledTriangleCallback\00", comdat, align 1
@_ZTI18btTriangleCallback = external constant i8*
@_ZTI24btScaledTriangleCallback = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([27 x i8], [27 x i8]* @_ZTS24btScaledTriangleCallback, i32 0, i32 0), i8* bitcast (i8** @_ZTI18btTriangleCallback to i8*) }, comdat, align 4
@_ZTV18btTriangleCallback = external unnamed_addr constant { [5 x i8*] }, align 4
@.str = private unnamed_addr constant [22 x i8] c"SCALEDBVHTRIANGLEMESH\00", align 1
@.str.1 = private unnamed_addr constant [30 x i8] c"btScaledTriangleMeshShapeData\00", align 1

@_ZN28btScaledBvhTriangleMeshShapeC1EP22btBvhTriangleMeshShapeRK9btVector3 = hidden unnamed_addr alias %class.btScaledBvhTriangleMeshShape* (%class.btScaledBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape*, %class.btVector3*), %class.btScaledBvhTriangleMeshShape* (%class.btScaledBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape*, %class.btVector3*)* @_ZN28btScaledBvhTriangleMeshShapeC2EP22btBvhTriangleMeshShapeRK9btVector3
@_ZN28btScaledBvhTriangleMeshShapeD1Ev = hidden unnamed_addr alias %class.btScaledBvhTriangleMeshShape* (%class.btScaledBvhTriangleMeshShape*), %class.btScaledBvhTriangleMeshShape* (%class.btScaledBvhTriangleMeshShape*)* @_ZN28btScaledBvhTriangleMeshShapeD2Ev

define hidden %class.btScaledBvhTriangleMeshShape* @_ZN28btScaledBvhTriangleMeshShapeC2EP22btBvhTriangleMeshShapeRK9btVector3(%class.btScaledBvhTriangleMeshShape* returned %this, %class.btBvhTriangleMeshShape* %childShape, %class.btVector3* nonnull align 4 dereferenceable(16) %localScaling) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btScaledBvhTriangleMeshShape*, align 4
  %childShape.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %localScaling.addr = alloca %class.btVector3*, align 4
  store %class.btScaledBvhTriangleMeshShape* %this, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  store %class.btBvhTriangleMeshShape* %childShape, %class.btBvhTriangleMeshShape** %childShape.addr, align 4, !tbaa !2
  store %class.btVector3* %localScaling, %class.btVector3** %localScaling.addr, align 4, !tbaa !2
  %this1 = load %class.btScaledBvhTriangleMeshShape*, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  %0 = bitcast %class.btScaledBvhTriangleMeshShape* %this1 to %class.btConcaveShape*
  %call = call %class.btConcaveShape* @_ZN14btConcaveShapeC2Ev(%class.btConcaveShape* %0)
  %1 = bitcast %class.btScaledBvhTriangleMeshShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [19 x i8*] }, { [19 x i8*] }* @_ZTV28btScaledBvhTriangleMeshShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_localScaling = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %2 = load %class.btVector3*, %class.btVector3** %localScaling.addr, align 4, !tbaa !2
  %3 = bitcast %class.btVector3* %m_localScaling to i8*
  %4 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false), !tbaa.struct !8
  %m_bvhTriMeshShape = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 2
  %5 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %childShape.addr, align 4, !tbaa !2
  store %class.btBvhTriangleMeshShape* %5, %class.btBvhTriangleMeshShape** %m_bvhTriMeshShape, align 4, !tbaa !10
  %6 = bitcast %class.btScaledBvhTriangleMeshShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %6, i32 0, i32 1
  store i32 22, i32* %m_shapeType, align 4, !tbaa !13
  ret %class.btScaledBvhTriangleMeshShape* %this1
}

declare %class.btConcaveShape* @_ZN14btConcaveShapeC2Ev(%class.btConcaveShape* returned) unnamed_addr #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: nounwind
declare %class.btConcaveShape* @_ZN14btConcaveShapeD2Ev(%class.btConcaveShape* returned) unnamed_addr #3

; Function Attrs: nounwind
define hidden %class.btScaledBvhTriangleMeshShape* @_ZN28btScaledBvhTriangleMeshShapeD2Ev(%class.btScaledBvhTriangleMeshShape* returned %this) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btScaledBvhTriangleMeshShape*, align 4
  store %class.btScaledBvhTriangleMeshShape* %this, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btScaledBvhTriangleMeshShape*, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  %0 = bitcast %class.btScaledBvhTriangleMeshShape* %this1 to %class.btConcaveShape*
  %call = call %class.btConcaveShape* @_ZN14btConcaveShapeD2Ev(%class.btConcaveShape* %0) #9
  ret %class.btScaledBvhTriangleMeshShape* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN28btScaledBvhTriangleMeshShapeD0Ev(%class.btScaledBvhTriangleMeshShape* %this) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btScaledBvhTriangleMeshShape*, align 4
  store %class.btScaledBvhTriangleMeshShape* %this, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btScaledBvhTriangleMeshShape*, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  %call = call %class.btScaledBvhTriangleMeshShape* @_ZN28btScaledBvhTriangleMeshShapeD1Ev(%class.btScaledBvhTriangleMeshShape* %this1) #9
  %0 = bitcast %class.btScaledBvhTriangleMeshShape* %this1 to i8*
  call void @_ZN28btScaledBvhTriangleMeshShapedlEPv(i8* %0) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN28btScaledBvhTriangleMeshShapedlEPv(i8* %ptr) #5 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

define hidden void @_ZNK28btScaledBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_(%class.btScaledBvhTriangleMeshShape* %this, %class.btTriangleCallback* %callback, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btScaledBvhTriangleMeshShape*, align 4
  %callback.addr = alloca %class.btTriangleCallback*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %scaledCallback = alloca %class.btScaledTriangleCallback, align 4
  %invLocalScaling = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %scaledAabbMin = alloca %class.btVector3, align 4
  %scaledAabbMax = alloca %class.btVector3, align 4
  store %class.btScaledBvhTriangleMeshShape* %this, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  store %class.btTriangleCallback* %callback, %class.btTriangleCallback** %callback.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btScaledBvhTriangleMeshShape*, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  %0 = bitcast %class.btScaledTriangleCallback* %scaledCallback to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %0) #9
  %1 = load %class.btTriangleCallback*, %class.btTriangleCallback** %callback.addr, align 4, !tbaa !2
  %m_localScaling = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call = call %class.btScaledTriangleCallback* @_ZN24btScaledTriangleCallbackC2EP18btTriangleCallbackRK9btVector3(%class.btScaledTriangleCallback* %scaledCallback, %class.btTriangleCallback* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling)
  %2 = bitcast %class.btVector3* %invLocalScaling to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #9
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %m_localScaling2 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %m_localScaling2)
  %4 = load float, float* %call3, align 4, !tbaa !16
  %div = fdiv float 1.000000e+00, %4
  store float %div, float* %ref.tmp, align 4, !tbaa !16
  %5 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %m_localScaling5 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %m_localScaling5)
  %6 = load float, float* %call6, align 4, !tbaa !16
  %div7 = fdiv float 1.000000e+00, %6
  store float %div7, float* %ref.tmp4, align 4, !tbaa !16
  %7 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  %m_localScaling9 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %m_localScaling9)
  %8 = load float, float* %call10, align 4, !tbaa !16
  %div11 = fdiv float 1.000000e+00, %8
  store float %div11, float* %ref.tmp8, align 4, !tbaa !16
  %call12 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %invLocalScaling, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  %9 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  %10 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  %12 = bitcast %class.btVector3* %scaledAabbMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %12) #9
  %call13 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %scaledAabbMin)
  %13 = bitcast %class.btVector3* %scaledAabbMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #9
  %call14 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %scaledAabbMax)
  %m_localScaling15 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %m_localScaling15)
  %14 = load float, float* %call16, align 4, !tbaa !16
  %conv = fpext float %14 to double
  %cmp = fcmp oge double %conv, 0.000000e+00
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %15 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %call17 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %15)
  %arrayidx = getelementptr inbounds float, float* %call17, i32 0
  %16 = load float, float* %arrayidx, align 4, !tbaa !16
  %call18 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %invLocalScaling)
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 0
  %17 = load float, float* %arrayidx19, align 4, !tbaa !16
  %mul = fmul float %16, %17
  br label %cond.end

cond.false:                                       ; preds = %entry
  %18 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %call20 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %18)
  %arrayidx21 = getelementptr inbounds float, float* %call20, i32 0
  %19 = load float, float* %arrayidx21, align 4, !tbaa !16
  %call22 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %invLocalScaling)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 0
  %20 = load float, float* %arrayidx23, align 4, !tbaa !16
  %mul24 = fmul float %19, %20
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %mul, %cond.true ], [ %mul24, %cond.false ]
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %scaledAabbMin)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 0
  store float %cond, float* %arrayidx26, align 4, !tbaa !16
  %m_localScaling27 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %m_localScaling27)
  %21 = load float, float* %call28, align 4, !tbaa !16
  %conv29 = fpext float %21 to double
  %cmp30 = fcmp oge double %conv29, 0.000000e+00
  br i1 %cmp30, label %cond.true31, label %cond.false37

cond.true31:                                      ; preds = %cond.end
  %22 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %call32 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %22)
  %arrayidx33 = getelementptr inbounds float, float* %call32, i32 1
  %23 = load float, float* %arrayidx33, align 4, !tbaa !16
  %call34 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %invLocalScaling)
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 1
  %24 = load float, float* %arrayidx35, align 4, !tbaa !16
  %mul36 = fmul float %23, %24
  br label %cond.end43

cond.false37:                                     ; preds = %cond.end
  %25 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %call38 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %25)
  %arrayidx39 = getelementptr inbounds float, float* %call38, i32 1
  %26 = load float, float* %arrayidx39, align 4, !tbaa !16
  %call40 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %invLocalScaling)
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 1
  %27 = load float, float* %arrayidx41, align 4, !tbaa !16
  %mul42 = fmul float %26, %27
  br label %cond.end43

cond.end43:                                       ; preds = %cond.false37, %cond.true31
  %cond44 = phi float [ %mul36, %cond.true31 ], [ %mul42, %cond.false37 ]
  %call45 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %scaledAabbMin)
  %arrayidx46 = getelementptr inbounds float, float* %call45, i32 1
  store float %cond44, float* %arrayidx46, align 4, !tbaa !16
  %m_localScaling47 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call48 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %m_localScaling47)
  %28 = load float, float* %call48, align 4, !tbaa !16
  %conv49 = fpext float %28 to double
  %cmp50 = fcmp oge double %conv49, 0.000000e+00
  br i1 %cmp50, label %cond.true51, label %cond.false57

cond.true51:                                      ; preds = %cond.end43
  %29 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %call52 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %29)
  %arrayidx53 = getelementptr inbounds float, float* %call52, i32 2
  %30 = load float, float* %arrayidx53, align 4, !tbaa !16
  %call54 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %invLocalScaling)
  %arrayidx55 = getelementptr inbounds float, float* %call54, i32 2
  %31 = load float, float* %arrayidx55, align 4, !tbaa !16
  %mul56 = fmul float %30, %31
  br label %cond.end63

cond.false57:                                     ; preds = %cond.end43
  %32 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %call58 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %32)
  %arrayidx59 = getelementptr inbounds float, float* %call58, i32 2
  %33 = load float, float* %arrayidx59, align 4, !tbaa !16
  %call60 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %invLocalScaling)
  %arrayidx61 = getelementptr inbounds float, float* %call60, i32 2
  %34 = load float, float* %arrayidx61, align 4, !tbaa !16
  %mul62 = fmul float %33, %34
  br label %cond.end63

cond.end63:                                       ; preds = %cond.false57, %cond.true51
  %cond64 = phi float [ %mul56, %cond.true51 ], [ %mul62, %cond.false57 ]
  %call65 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %scaledAabbMin)
  %arrayidx66 = getelementptr inbounds float, float* %call65, i32 2
  store float %cond64, float* %arrayidx66, align 4, !tbaa !16
  %call67 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %scaledAabbMin)
  %arrayidx68 = getelementptr inbounds float, float* %call67, i32 3
  store float 0.000000e+00, float* %arrayidx68, align 4, !tbaa !16
  %m_localScaling69 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call70 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %m_localScaling69)
  %35 = load float, float* %call70, align 4, !tbaa !16
  %conv71 = fpext float %35 to double
  %cmp72 = fcmp ole double %conv71, 0.000000e+00
  br i1 %cmp72, label %cond.true73, label %cond.false79

cond.true73:                                      ; preds = %cond.end63
  %36 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %call74 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %36)
  %arrayidx75 = getelementptr inbounds float, float* %call74, i32 0
  %37 = load float, float* %arrayidx75, align 4, !tbaa !16
  %call76 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %invLocalScaling)
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 0
  %38 = load float, float* %arrayidx77, align 4, !tbaa !16
  %mul78 = fmul float %37, %38
  br label %cond.end85

cond.false79:                                     ; preds = %cond.end63
  %39 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %call80 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %39)
  %arrayidx81 = getelementptr inbounds float, float* %call80, i32 0
  %40 = load float, float* %arrayidx81, align 4, !tbaa !16
  %call82 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %invLocalScaling)
  %arrayidx83 = getelementptr inbounds float, float* %call82, i32 0
  %41 = load float, float* %arrayidx83, align 4, !tbaa !16
  %mul84 = fmul float %40, %41
  br label %cond.end85

cond.end85:                                       ; preds = %cond.false79, %cond.true73
  %cond86 = phi float [ %mul78, %cond.true73 ], [ %mul84, %cond.false79 ]
  %call87 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %scaledAabbMax)
  %arrayidx88 = getelementptr inbounds float, float* %call87, i32 0
  store float %cond86, float* %arrayidx88, align 4, !tbaa !16
  %m_localScaling89 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call90 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %m_localScaling89)
  %42 = load float, float* %call90, align 4, !tbaa !16
  %conv91 = fpext float %42 to double
  %cmp92 = fcmp ole double %conv91, 0.000000e+00
  br i1 %cmp92, label %cond.true93, label %cond.false99

cond.true93:                                      ; preds = %cond.end85
  %43 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %call94 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %43)
  %arrayidx95 = getelementptr inbounds float, float* %call94, i32 1
  %44 = load float, float* %arrayidx95, align 4, !tbaa !16
  %call96 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %invLocalScaling)
  %arrayidx97 = getelementptr inbounds float, float* %call96, i32 1
  %45 = load float, float* %arrayidx97, align 4, !tbaa !16
  %mul98 = fmul float %44, %45
  br label %cond.end105

cond.false99:                                     ; preds = %cond.end85
  %46 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %call100 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %46)
  %arrayidx101 = getelementptr inbounds float, float* %call100, i32 1
  %47 = load float, float* %arrayidx101, align 4, !tbaa !16
  %call102 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %invLocalScaling)
  %arrayidx103 = getelementptr inbounds float, float* %call102, i32 1
  %48 = load float, float* %arrayidx103, align 4, !tbaa !16
  %mul104 = fmul float %47, %48
  br label %cond.end105

cond.end105:                                      ; preds = %cond.false99, %cond.true93
  %cond106 = phi float [ %mul98, %cond.true93 ], [ %mul104, %cond.false99 ]
  %call107 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %scaledAabbMax)
  %arrayidx108 = getelementptr inbounds float, float* %call107, i32 1
  store float %cond106, float* %arrayidx108, align 4, !tbaa !16
  %m_localScaling109 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call110 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %m_localScaling109)
  %49 = load float, float* %call110, align 4, !tbaa !16
  %conv111 = fpext float %49 to double
  %cmp112 = fcmp ole double %conv111, 0.000000e+00
  br i1 %cmp112, label %cond.true113, label %cond.false119

cond.true113:                                     ; preds = %cond.end105
  %50 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %call114 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %50)
  %arrayidx115 = getelementptr inbounds float, float* %call114, i32 2
  %51 = load float, float* %arrayidx115, align 4, !tbaa !16
  %call116 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %invLocalScaling)
  %arrayidx117 = getelementptr inbounds float, float* %call116, i32 2
  %52 = load float, float* %arrayidx117, align 4, !tbaa !16
  %mul118 = fmul float %51, %52
  br label %cond.end125

cond.false119:                                    ; preds = %cond.end105
  %53 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %call120 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %53)
  %arrayidx121 = getelementptr inbounds float, float* %call120, i32 2
  %54 = load float, float* %arrayidx121, align 4, !tbaa !16
  %call122 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %invLocalScaling)
  %arrayidx123 = getelementptr inbounds float, float* %call122, i32 2
  %55 = load float, float* %arrayidx123, align 4, !tbaa !16
  %mul124 = fmul float %54, %55
  br label %cond.end125

cond.end125:                                      ; preds = %cond.false119, %cond.true113
  %cond126 = phi float [ %mul118, %cond.true113 ], [ %mul124, %cond.false119 ]
  %call127 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %scaledAabbMax)
  %arrayidx128 = getelementptr inbounds float, float* %call127, i32 2
  store float %cond126, float* %arrayidx128, align 4, !tbaa !16
  %call129 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %scaledAabbMax)
  %arrayidx130 = getelementptr inbounds float, float* %call129, i32 3
  store float 0.000000e+00, float* %arrayidx130, align 4, !tbaa !16
  %m_bvhTriMeshShape = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 2
  %56 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %m_bvhTriMeshShape, align 4, !tbaa !10
  %57 = bitcast %class.btScaledTriangleCallback* %scaledCallback to %class.btTriangleCallback*
  %58 = bitcast %class.btBvhTriangleMeshShape* %56 to void (%class.btBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)**, void (%class.btBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)*** %58, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)** %vtable, i64 16
  %59 = load void (%class.btBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %59(%class.btBvhTriangleMeshShape* %56, %class.btTriangleCallback* %57, %class.btVector3* nonnull align 4 dereferenceable(16) %scaledAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %scaledAabbMax)
  %60 = bitcast %class.btVector3* %scaledAabbMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %60) #9
  %61 = bitcast %class.btVector3* %scaledAabbMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %61) #9
  %62 = bitcast %class.btVector3* %invLocalScaling to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %62) #9
  %call131 = call %class.btScaledTriangleCallback* bitcast (%class.btTriangleCallback* (%class.btTriangleCallback*)* @_ZN18btTriangleCallbackD2Ev to %class.btScaledTriangleCallback* (%class.btScaledTriangleCallback*)*)(%class.btScaledTriangleCallback* %scaledCallback) #9
  %63 = bitcast %class.btScaledTriangleCallback* %scaledCallback to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %63) #9
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: nounwind
define linkonce_odr hidden %class.btScaledTriangleCallback* @_ZN24btScaledTriangleCallbackC2EP18btTriangleCallbackRK9btVector3(%class.btScaledTriangleCallback* returned %this, %class.btTriangleCallback* %originalCallback, %class.btVector3* nonnull align 4 dereferenceable(16) %localScaling) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btScaledTriangleCallback*, align 4
  %originalCallback.addr = alloca %class.btTriangleCallback*, align 4
  %localScaling.addr = alloca %class.btVector3*, align 4
  store %class.btScaledTriangleCallback* %this, %class.btScaledTriangleCallback** %this.addr, align 4, !tbaa !2
  store %class.btTriangleCallback* %originalCallback, %class.btTriangleCallback** %originalCallback.addr, align 4, !tbaa !2
  store %class.btVector3* %localScaling, %class.btVector3** %localScaling.addr, align 4, !tbaa !2
  %this1 = load %class.btScaledTriangleCallback*, %class.btScaledTriangleCallback** %this.addr, align 4
  %0 = bitcast %class.btScaledTriangleCallback* %this1 to %class.btTriangleCallback*
  %call = call %class.btTriangleCallback* @_ZN18btTriangleCallbackC2Ev(%class.btTriangleCallback* %0) #9
  %1 = bitcast %class.btScaledTriangleCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV24btScaledTriangleCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_originalCallback = getelementptr inbounds %class.btScaledTriangleCallback, %class.btScaledTriangleCallback* %this1, i32 0, i32 1
  %2 = load %class.btTriangleCallback*, %class.btTriangleCallback** %originalCallback.addr, align 4, !tbaa !2
  store %class.btTriangleCallback* %2, %class.btTriangleCallback** %m_originalCallback, align 4, !tbaa !18
  %m_localScaling = getelementptr inbounds %class.btScaledTriangleCallback, %class.btScaledTriangleCallback* %this1, i32 0, i32 2
  %3 = load %class.btVector3*, %class.btVector3** %localScaling.addr, align 4, !tbaa !2
  %4 = bitcast %class.btVector3* %m_localScaling to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !8
  ret %class.btScaledTriangleCallback* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !16
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !16
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !16
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !16
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !16
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !16
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !16
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: nounwind
declare %class.btTriangleCallback* @_ZN18btTriangleCallbackD2Ev(%class.btTriangleCallback* returned) unnamed_addr #3

define hidden void @_ZNK28btScaledBvhTriangleMeshShape7getAabbERK11btTransformR9btVector3S4_(%class.btScaledBvhTriangleMeshShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %trans, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btScaledBvhTriangleMeshShape*, align 4
  %trans.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %localAabbMin = alloca %class.btVector3, align 4
  %localAabbMax = alloca %class.btVector3, align 4
  %tmpLocalAabbMin = alloca %class.btVector3, align 4
  %tmpLocalAabbMax = alloca %class.btVector3, align 4
  %localHalfExtents = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp82 = alloca %class.btVector3, align 4
  %margin = alloca float, align 4
  %ref.tmp85 = alloca %class.btVector3, align 4
  %localCenter = alloca %class.btVector3, align 4
  %ref.tmp88 = alloca float, align 4
  %ref.tmp89 = alloca %class.btVector3, align 4
  %abs_b = alloca %class.btMatrix3x3, align 4
  %center = alloca %class.btVector3, align 4
  %extent = alloca %class.btVector3, align 4
  %ref.tmp94 = alloca %class.btVector3, align 4
  %ref.tmp95 = alloca %class.btVector3, align 4
  store %class.btScaledBvhTriangleMeshShape* %this, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %trans, %class.btTransform** %trans.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btScaledBvhTriangleMeshShape*, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %localAabbMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %m_bvhTriMeshShape = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 2
  %1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %m_bvhTriMeshShape, align 4, !tbaa !10
  %2 = bitcast %class.btBvhTriangleMeshShape* %1 to %class.btTriangleMeshShape*
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK19btTriangleMeshShape15getLocalAabbMinEv(%class.btTriangleMeshShape* %2)
  %3 = bitcast %class.btVector3* %localAabbMin to i8*
  %4 = bitcast %class.btVector3* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false), !tbaa.struct !8
  %5 = bitcast %class.btVector3* %localAabbMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #9
  %m_bvhTriMeshShape2 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 2
  %6 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %m_bvhTriMeshShape2, align 4, !tbaa !10
  %7 = bitcast %class.btBvhTriangleMeshShape* %6 to %class.btTriangleMeshShape*
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK19btTriangleMeshShape15getLocalAabbMaxEv(%class.btTriangleMeshShape* %7)
  %8 = bitcast %class.btVector3* %localAabbMax to i8*
  %9 = bitcast %class.btVector3* %call3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false), !tbaa.struct !8
  %10 = bitcast %class.btVector3* %tmpLocalAabbMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #9
  %m_localScaling = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %tmpLocalAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling)
  %11 = bitcast %class.btVector3* %tmpLocalAabbMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #9
  %m_localScaling4 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %tmpLocalAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling4)
  %m_localScaling5 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %m_localScaling5)
  %12 = load float, float* %call6, align 4, !tbaa !16
  %conv = fpext float %12 to double
  %cmp = fcmp oge double %conv, 0.000000e+00
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call7 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpLocalAabbMin)
  %arrayidx = getelementptr inbounds float, float* %call7, i32 0
  %13 = load float, float* %arrayidx, align 4, !tbaa !16
  br label %cond.end

cond.false:                                       ; preds = %entry
  %call8 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpLocalAabbMax)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 0
  %14 = load float, float* %arrayidx9, align 4, !tbaa !16
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %13, %cond.true ], [ %14, %cond.false ]
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAabbMin)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 0
  store float %cond, float* %arrayidx11, align 4, !tbaa !16
  %m_localScaling12 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %m_localScaling12)
  %15 = load float, float* %call13, align 4, !tbaa !16
  %conv14 = fpext float %15 to double
  %cmp15 = fcmp oge double %conv14, 0.000000e+00
  br i1 %cmp15, label %cond.true16, label %cond.false19

cond.true16:                                      ; preds = %cond.end
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpLocalAabbMin)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 1
  %16 = load float, float* %arrayidx18, align 4, !tbaa !16
  br label %cond.end22

cond.false19:                                     ; preds = %cond.end
  %call20 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpLocalAabbMax)
  %arrayidx21 = getelementptr inbounds float, float* %call20, i32 1
  %17 = load float, float* %arrayidx21, align 4, !tbaa !16
  br label %cond.end22

cond.end22:                                       ; preds = %cond.false19, %cond.true16
  %cond23 = phi float [ %16, %cond.true16 ], [ %17, %cond.false19 ]
  %call24 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAabbMin)
  %arrayidx25 = getelementptr inbounds float, float* %call24, i32 1
  store float %cond23, float* %arrayidx25, align 4, !tbaa !16
  %m_localScaling26 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call27 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %m_localScaling26)
  %18 = load float, float* %call27, align 4, !tbaa !16
  %conv28 = fpext float %18 to double
  %cmp29 = fcmp oge double %conv28, 0.000000e+00
  br i1 %cmp29, label %cond.true30, label %cond.false33

cond.true30:                                      ; preds = %cond.end22
  %call31 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpLocalAabbMin)
  %arrayidx32 = getelementptr inbounds float, float* %call31, i32 2
  %19 = load float, float* %arrayidx32, align 4, !tbaa !16
  br label %cond.end36

cond.false33:                                     ; preds = %cond.end22
  %call34 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpLocalAabbMax)
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 2
  %20 = load float, float* %arrayidx35, align 4, !tbaa !16
  br label %cond.end36

cond.end36:                                       ; preds = %cond.false33, %cond.true30
  %cond37 = phi float [ %19, %cond.true30 ], [ %20, %cond.false33 ]
  %call38 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAabbMin)
  %arrayidx39 = getelementptr inbounds float, float* %call38, i32 2
  store float %cond37, float* %arrayidx39, align 4, !tbaa !16
  %m_localScaling40 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %m_localScaling40)
  %21 = load float, float* %call41, align 4, !tbaa !16
  %conv42 = fpext float %21 to double
  %cmp43 = fcmp ole double %conv42, 0.000000e+00
  br i1 %cmp43, label %cond.true44, label %cond.false47

cond.true44:                                      ; preds = %cond.end36
  %call45 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpLocalAabbMin)
  %arrayidx46 = getelementptr inbounds float, float* %call45, i32 0
  %22 = load float, float* %arrayidx46, align 4, !tbaa !16
  br label %cond.end50

cond.false47:                                     ; preds = %cond.end36
  %call48 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpLocalAabbMax)
  %arrayidx49 = getelementptr inbounds float, float* %call48, i32 0
  %23 = load float, float* %arrayidx49, align 4, !tbaa !16
  br label %cond.end50

cond.end50:                                       ; preds = %cond.false47, %cond.true44
  %cond51 = phi float [ %22, %cond.true44 ], [ %23, %cond.false47 ]
  %call52 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAabbMax)
  %arrayidx53 = getelementptr inbounds float, float* %call52, i32 0
  store float %cond51, float* %arrayidx53, align 4, !tbaa !16
  %m_localScaling54 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call55 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %m_localScaling54)
  %24 = load float, float* %call55, align 4, !tbaa !16
  %conv56 = fpext float %24 to double
  %cmp57 = fcmp ole double %conv56, 0.000000e+00
  br i1 %cmp57, label %cond.true58, label %cond.false61

cond.true58:                                      ; preds = %cond.end50
  %call59 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpLocalAabbMin)
  %arrayidx60 = getelementptr inbounds float, float* %call59, i32 1
  %25 = load float, float* %arrayidx60, align 4, !tbaa !16
  br label %cond.end64

cond.false61:                                     ; preds = %cond.end50
  %call62 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpLocalAabbMax)
  %arrayidx63 = getelementptr inbounds float, float* %call62, i32 1
  %26 = load float, float* %arrayidx63, align 4, !tbaa !16
  br label %cond.end64

cond.end64:                                       ; preds = %cond.false61, %cond.true58
  %cond65 = phi float [ %25, %cond.true58 ], [ %26, %cond.false61 ]
  %call66 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAabbMax)
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 1
  store float %cond65, float* %arrayidx67, align 4, !tbaa !16
  %m_localScaling68 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call69 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %m_localScaling68)
  %27 = load float, float* %call69, align 4, !tbaa !16
  %conv70 = fpext float %27 to double
  %cmp71 = fcmp ole double %conv70, 0.000000e+00
  br i1 %cmp71, label %cond.true72, label %cond.false75

cond.true72:                                      ; preds = %cond.end64
  %call73 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpLocalAabbMin)
  %arrayidx74 = getelementptr inbounds float, float* %call73, i32 2
  %28 = load float, float* %arrayidx74, align 4, !tbaa !16
  br label %cond.end78

cond.false75:                                     ; preds = %cond.end64
  %call76 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpLocalAabbMax)
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 2
  %29 = load float, float* %arrayidx77, align 4, !tbaa !16
  br label %cond.end78

cond.end78:                                       ; preds = %cond.false75, %cond.true72
  %cond79 = phi float [ %28, %cond.true72 ], [ %29, %cond.false75 ]
  %call80 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAabbMax)
  %arrayidx81 = getelementptr inbounds float, float* %call80, i32 2
  store float %cond79, float* %arrayidx81, align 4, !tbaa !16
  %30 = bitcast %class.btVector3* %localHalfExtents to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %30) #9
  %31 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #9
  store float 5.000000e-01, float* %ref.tmp, align 4, !tbaa !16
  %32 = bitcast %class.btVector3* %ref.tmp82 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %32) #9
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp82, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %localHalfExtents, float* nonnull align 4 dereferenceable(4) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp82)
  %33 = bitcast %class.btVector3* %ref.tmp82 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %33) #9
  %34 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #9
  %35 = bitcast float* %margin to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #9
  %m_bvhTriMeshShape83 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 2
  %36 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %m_bvhTriMeshShape83, align 4, !tbaa !10
  %37 = bitcast %class.btBvhTriangleMeshShape* %36 to %class.btConcaveShape*
  %38 = bitcast %class.btConcaveShape* %37 to float (%class.btConcaveShape*)***
  %vtable = load float (%class.btConcaveShape*)**, float (%class.btConcaveShape*)*** %38, align 4, !tbaa !6
  %vfn = getelementptr inbounds float (%class.btConcaveShape*)*, float (%class.btConcaveShape*)** %vtable, i64 12
  %39 = load float (%class.btConcaveShape*)*, float (%class.btConcaveShape*)** %vfn, align 4
  %call84 = call float %39(%class.btConcaveShape* %37)
  store float %call84, float* %margin, align 4, !tbaa !16
  %40 = bitcast %class.btVector3* %ref.tmp85 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %40) #9
  %call86 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp85, float* nonnull align 4 dereferenceable(4) %margin, float* nonnull align 4 dereferenceable(4) %margin, float* nonnull align 4 dereferenceable(4) %margin)
  %call87 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %localHalfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp85)
  %41 = bitcast %class.btVector3* %ref.tmp85 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %41) #9
  %42 = bitcast %class.btVector3* %localCenter to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %42) #9
  %43 = bitcast float* %ref.tmp88 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #9
  store float 5.000000e-01, float* %ref.tmp88, align 4, !tbaa !16
  %44 = bitcast %class.btVector3* %ref.tmp89 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %44) #9
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp89, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %localCenter, float* nonnull align 4 dereferenceable(4) %ref.tmp88, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp89)
  %45 = bitcast %class.btVector3* %ref.tmp89 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %45) #9
  %46 = bitcast float* %ref.tmp88 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #9
  %47 = bitcast %class.btMatrix3x3* %abs_b to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %47) #9
  %48 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4, !tbaa !2
  %call90 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %48)
  call void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* sret align 4 %abs_b, %class.btMatrix3x3* %call90)
  %49 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %49) #9
  %50 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4, !tbaa !2
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %center, %class.btTransform* %50, %class.btVector3* nonnull align 4 dereferenceable(16) %localCenter)
  %51 = bitcast %class.btVector3* %extent to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %51) #9
  %call91 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 0)
  %call92 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 1)
  %call93 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %extent, %class.btVector3* %localHalfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %call91, %class.btVector3* nonnull align 4 dereferenceable(16) %call92, %class.btVector3* nonnull align 4 dereferenceable(16) %call93)
  %52 = bitcast %class.btVector3* %ref.tmp94 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %52) #9
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp94, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %53 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %54 = bitcast %class.btVector3* %53 to i8*
  %55 = bitcast %class.btVector3* %ref.tmp94 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %54, i8* align 4 %55, i32 16, i1 false), !tbaa.struct !8
  %56 = bitcast %class.btVector3* %ref.tmp94 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %56) #9
  %57 = bitcast %class.btVector3* %ref.tmp95 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %57) #9
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp95, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %58 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %59 = bitcast %class.btVector3* %58 to i8*
  %60 = bitcast %class.btVector3* %ref.tmp95 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %59, i8* align 4 %60, i32 16, i1 false), !tbaa.struct !8
  %61 = bitcast %class.btVector3* %ref.tmp95 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %61) #9
  %62 = bitcast %class.btVector3* %extent to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %62) #9
  %63 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %63) #9
  %64 = bitcast %class.btMatrix3x3* %abs_b to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %64) #9
  %65 = bitcast %class.btVector3* %localCenter to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %65) #9
  %66 = bitcast float* %margin to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #9
  %67 = bitcast %class.btVector3* %localHalfExtents to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %67) #9
  %68 = bitcast %class.btVector3* %tmpLocalAabbMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %68) #9
  %69 = bitcast %class.btVector3* %tmpLocalAabbMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %69) #9
  %70 = bitcast %class.btVector3* %localAabbMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %70) #9
  %71 = bitcast %class.btVector3* %localAabbMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %71) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK19btTriangleMeshShape15getLocalAabbMinEv(%class.btTriangleMeshShape* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btTriangleMeshShape*, align 4
  store %class.btTriangleMeshShape* %this, %class.btTriangleMeshShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleMeshShape*, %class.btTriangleMeshShape** %this.addr, align 4
  %m_localAabbMin = getelementptr inbounds %class.btTriangleMeshShape, %class.btTriangleMeshShape* %this1, i32 0, i32 1
  ret %class.btVector3* %m_localAabbMin
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK19btTriangleMeshShape15getLocalAabbMaxEv(%class.btTriangleMeshShape* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btTriangleMeshShape*, align 4
  store %class.btTriangleMeshShape* %this, %class.btTriangleMeshShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleMeshShape*, %class.btTriangleMeshShape** %this.addr, align 4
  %m_localAabbMax = getelementptr inbounds %class.btTriangleMeshShape, %class.btTriangleMeshShape* %this1, i32 0, i32 2
  ret %class.btVector3* %m_localAabbMax
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #6 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !16
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !16
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !16
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !16
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !16
  %mul8 = fmul float %7, %9
  store float %mul8, float* %ref.tmp3, align 4, !tbaa !16
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !16
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !16
  %mul14 = fmul float %12, %14
  store float %mul14, float* %ref.tmp9, align 4, !tbaa !16
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #6 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #6 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !16
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !16
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !16
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !16
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !16
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !16
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !16
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !16
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !16
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !16
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !16
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !16
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !16
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !16
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !16
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !16
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !16
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !16
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #6 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !16
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !16
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !16
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !16
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !16
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !16
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !16
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !16
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !16
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #6 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %2 = load float, float* %call, align 4, !tbaa !16
  %call2 = call float @_Z6btFabsf(float %2)
  store float %call2, float* %ref.tmp, align 4, !tbaa !16
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 0
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx5)
  %4 = load float, float* %call6, align 4, !tbaa !16
  %call7 = call float @_Z6btFabsf(float %4)
  store float %call7, float* %ref.tmp3, align 4, !tbaa !16
  %5 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 0
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx10)
  %6 = load float, float* %call11, align 4, !tbaa !16
  %call12 = call float @_Z6btFabsf(float %6)
  store float %call12, float* %ref.tmp8, align 4, !tbaa !16
  %7 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 1
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx15)
  %8 = load float, float* %call16, align 4, !tbaa !16
  %call17 = call float @_Z6btFabsf(float %8)
  store float %call17, float* %ref.tmp13, align 4, !tbaa !16
  %9 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  %m_el19 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el19, i32 0, i32 1
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx20)
  %10 = load float, float* %call21, align 4, !tbaa !16
  %call22 = call float @_Z6btFabsf(float %10)
  store float %call22, float* %ref.tmp18, align 4, !tbaa !16
  %11 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  %m_el24 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el24, i32 0, i32 1
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx25)
  %12 = load float, float* %call26, align 4, !tbaa !16
  %call27 = call float @_Z6btFabsf(float %12)
  store float %call27, float* %ref.tmp23, align 4, !tbaa !16
  %13 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %m_el29 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el29, i32 0, i32 2
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx30)
  %14 = load float, float* %call31, align 4, !tbaa !16
  %call32 = call float @_Z6btFabsf(float %14)
  store float %call32, float* %ref.tmp28, align 4, !tbaa !16
  %15 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #9
  %m_el34 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx35 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el34, i32 0, i32 2
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx35)
  %16 = load float, float* %call36, align 4, !tbaa !16
  %call37 = call float @_Z6btFabsf(float %16)
  store float %call37, float* %ref.tmp33, align 4, !tbaa !16
  %17 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #9
  %m_el39 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx40 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el39, i32 0, i32 2
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx40)
  %18 = load float, float* %call41, align 4, !tbaa !16
  %call42 = call float @_Z6btFabsf(float %18)
  store float %call42, float* %ref.tmp38, align 4, !tbaa !16
  %call43 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp33, float* nonnull align 4 dereferenceable(4) %ref.tmp38)
  %19 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #9
  %20 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #9
  %21 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #9
  %22 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #9
  %23 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #9
  %24 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #9
  %25 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #9
  %26 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #9
  %27 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #6 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4, !tbaa !16
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !16
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !16
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #5 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !20
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !20
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: nounwind
define hidden void @_ZN28btScaledBvhTriangleMeshShape15setLocalScalingERK9btVector3(%class.btScaledBvhTriangleMeshShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %scaling) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btScaledBvhTriangleMeshShape*, align 4
  %scaling.addr = alloca %class.btVector3*, align 4
  store %class.btScaledBvhTriangleMeshShape* %this, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %scaling, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  %this1 = load %class.btScaledBvhTriangleMeshShape*, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  %m_localScaling = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_localScaling to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !8
  ret void
}

; Function Attrs: nounwind
define hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK28btScaledBvhTriangleMeshShape15getLocalScalingEv(%class.btScaledBvhTriangleMeshShape* %this) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btScaledBvhTriangleMeshShape*, align 4
  store %class.btScaledBvhTriangleMeshShape* %this, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btScaledBvhTriangleMeshShape*, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  %m_localScaling = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  ret %class.btVector3* %m_localScaling
}

; Function Attrs: nounwind
define hidden void @_ZNK28btScaledBvhTriangleMeshShape21calculateLocalInertiaEfR9btVector3(%class.btScaledBvhTriangleMeshShape* %this, float %mass, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btScaledBvhTriangleMeshShape*, align 4
  %mass.addr = alloca float, align 4
  %inertia.addr = alloca %class.btVector3*, align 4
  store %class.btScaledBvhTriangleMeshShape* %this, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  store float %mass, float* %mass.addr, align 4, !tbaa !16
  store %class.btVector3* %inertia, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %this1 = load %class.btScaledBvhTriangleMeshShape*, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  ret void
}

declare void @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf(%class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4)) unnamed_addr #1

declare float @_ZNK16btCollisionShape20getAngularMotionDiscEv(%class.btCollisionShape*) unnamed_addr #1

declare float @_ZNK16btCollisionShape27getContactBreakingThresholdEf(%class.btCollisionShape*, float) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden i8* @_ZNK28btScaledBvhTriangleMeshShape7getNameEv(%class.btScaledBvhTriangleMeshShape* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btScaledBvhTriangleMeshShape*, align 4
  store %class.btScaledBvhTriangleMeshShape* %this, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btScaledBvhTriangleMeshShape*, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  ret i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str, i32 0, i32 0)
}

define linkonce_odr hidden void @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btCollisionShape* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !16
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store float 1.000000e+00, float* %ref.tmp2, align 4, !tbaa !16
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 1.000000e+00, float* %ref.tmp3, align 4, !tbaa !16
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN14btConcaveShape9setMarginEf(%class.btConcaveShape* %this, float %collisionMargin) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btConcaveShape*, align 4
  %collisionMargin.addr = alloca float, align 4
  store %class.btConcaveShape* %this, %class.btConcaveShape** %this.addr, align 4, !tbaa !2
  store float %collisionMargin, float* %collisionMargin.addr, align 4, !tbaa !16
  %this1 = load %class.btConcaveShape*, %class.btConcaveShape** %this.addr, align 4
  %0 = load float, float* %collisionMargin.addr, align 4, !tbaa !16
  %m_collisionMargin = getelementptr inbounds %class.btConcaveShape, %class.btConcaveShape* %this1, i32 0, i32 1
  store float %0, float* %m_collisionMargin, align 4, !tbaa !21
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK14btConcaveShape9getMarginEv(%class.btConcaveShape* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btConcaveShape*, align 4
  store %class.btConcaveShape* %this, %class.btConcaveShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConcaveShape*, %class.btConcaveShape** %this.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btConcaveShape, %class.btConcaveShape* %this1, i32 0, i32 1
  %0 = load float, float* %m_collisionMargin, align 4, !tbaa !21
  ret float %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK28btScaledBvhTriangleMeshShape28calculateSerializeBufferSizeEv(%class.btScaledBvhTriangleMeshShape* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btScaledBvhTriangleMeshShape*, align 4
  store %class.btScaledBvhTriangleMeshShape* %this, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btScaledBvhTriangleMeshShape*, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  ret i32 76
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZNK28btScaledBvhTriangleMeshShape9serializeEPvP12btSerializer(%class.btScaledBvhTriangleMeshShape* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %class.btScaledBvhTriangleMeshShape*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %scaledMeshData = alloca %struct.btScaledTriangleMeshShapeData*, align 4
  store %class.btScaledBvhTriangleMeshShape* %this, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btScaledBvhTriangleMeshShape*, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  %0 = bitcast %struct.btScaledTriangleMeshShapeData** %scaledMeshData to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i8*, i8** %dataBuffer.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.btScaledTriangleMeshShapeData*
  store %struct.btScaledTriangleMeshShapeData* %2, %struct.btScaledTriangleMeshShapeData** %scaledMeshData, align 4, !tbaa !2
  %m_bvhTriMeshShape = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 2
  %3 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %m_bvhTriMeshShape, align 4, !tbaa !10
  %4 = load %struct.btScaledTriangleMeshShapeData*, %struct.btScaledTriangleMeshShapeData** %scaledMeshData, align 4, !tbaa !2
  %m_trimeshShapeData = getelementptr inbounds %struct.btScaledTriangleMeshShapeData, %struct.btScaledTriangleMeshShapeData* %4, i32 0, i32 0
  %5 = bitcast %struct.btTriangleMeshShapeData* %m_trimeshShapeData to i8*
  %6 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %7 = bitcast %class.btBvhTriangleMeshShape* %3 to i8* (%class.btBvhTriangleMeshShape*, i8*, %class.btSerializer*)***
  %vtable = load i8* (%class.btBvhTriangleMeshShape*, i8*, %class.btSerializer*)**, i8* (%class.btBvhTriangleMeshShape*, i8*, %class.btSerializer*)*** %7, align 4, !tbaa !6
  %vfn = getelementptr inbounds i8* (%class.btBvhTriangleMeshShape*, i8*, %class.btSerializer*)*, i8* (%class.btBvhTriangleMeshShape*, i8*, %class.btSerializer*)** %vtable, i64 14
  %8 = load i8* (%class.btBvhTriangleMeshShape*, i8*, %class.btSerializer*)*, i8* (%class.btBvhTriangleMeshShape*, i8*, %class.btSerializer*)** %vfn, align 4
  %call = call i8* %8(%class.btBvhTriangleMeshShape* %3, i8* %5, %class.btSerializer* %6)
  %9 = load %struct.btScaledTriangleMeshShapeData*, %struct.btScaledTriangleMeshShapeData** %scaledMeshData, align 4, !tbaa !2
  %m_trimeshShapeData2 = getelementptr inbounds %struct.btScaledTriangleMeshShapeData, %struct.btScaledTriangleMeshShapeData* %9, i32 0, i32 0
  %m_collisionShapeData = getelementptr inbounds %struct.btTriangleMeshShapeData, %struct.btTriangleMeshShapeData* %m_trimeshShapeData2, i32 0, i32 0
  %m_shapeType = getelementptr inbounds %struct.btCollisionShapeData, %struct.btCollisionShapeData* %m_collisionShapeData, i32 0, i32 1
  store i32 22, i32* %m_shapeType, align 4, !tbaa !23
  %m_localScaling = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %10 = load %struct.btScaledTriangleMeshShapeData*, %struct.btScaledTriangleMeshShapeData** %scaledMeshData, align 4, !tbaa !2
  %m_localScaling3 = getelementptr inbounds %struct.btScaledTriangleMeshShapeData, %struct.btScaledTriangleMeshShapeData* %10, i32 0, i32 1
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_localScaling, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_localScaling3)
  %11 = bitcast %struct.btScaledTriangleMeshShapeData** %scaledMeshData to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  ret i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.1, i32 0, i32 0)
}

declare void @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer(%class.btCollisionShape*, %class.btSerializer*) unnamed_addr #1

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btTriangleCallback* @_ZN18btTriangleCallbackC2Ev(%class.btTriangleCallback* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btTriangleCallback*, align 4
  store %class.btTriangleCallback* %this, %class.btTriangleCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleCallback*, %class.btTriangleCallback** %this.addr, align 4
  %0 = bitcast %class.btTriangleCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV18btTriangleCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  ret %class.btTriangleCallback* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN24btScaledTriangleCallbackD0Ev(%class.btScaledTriangleCallback* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btScaledTriangleCallback*, align 4
  store %class.btScaledTriangleCallback* %this, %class.btScaledTriangleCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btScaledTriangleCallback*, %class.btScaledTriangleCallback** %this.addr, align 4
  %call = call %class.btScaledTriangleCallback* bitcast (%class.btTriangleCallback* (%class.btTriangleCallback*)* @_ZN18btTriangleCallbackD2Ev to %class.btScaledTriangleCallback* (%class.btScaledTriangleCallback*)*)(%class.btScaledTriangleCallback* %this1) #9
  %0 = bitcast %class.btScaledTriangleCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

define linkonce_odr hidden void @_ZN24btScaledTriangleCallback15processTriangleEP9btVector3ii(%class.btScaledTriangleCallback* %this, %class.btVector3* %triangle, i32 %partId, i32 %triangleIndex) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btScaledTriangleCallback*, align 4
  %triangle.addr = alloca %class.btVector3*, align 4
  %partId.addr = alloca i32, align 4
  %triangleIndex.addr = alloca i32, align 4
  %newTriangle = alloca [3 x %class.btVector3], align 16
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca %class.btVector3, align 4
  store %class.btScaledTriangleCallback* %this, %class.btScaledTriangleCallback** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %triangle, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  store i32 %partId, i32* %partId.addr, align 4, !tbaa !20
  store i32 %triangleIndex, i32* %triangleIndex.addr, align 4, !tbaa !20
  %this1 = load %class.btScaledTriangleCallback*, %class.btScaledTriangleCallback** %this.addr, align 4
  %0 = bitcast [3 x %class.btVector3]* %newTriangle to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %0) #9
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %newTriangle, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #9
  %2 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0
  %m_localScaling = getelementptr inbounds %class.btScaledTriangleCallback, %class.btScaledTriangleCallback* %this1, i32 0, i32 2
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling)
  %arrayidx2 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %newTriangle, i32 0, i32 0
  %3 = bitcast %class.btVector3* %arrayidx2 to i8*
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %3, i8* align 4 %4, i32 16, i1 false), !tbaa.struct !8
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #9
  %6 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #9
  %7 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 1
  %m_localScaling5 = getelementptr inbounds %class.btScaledTriangleCallback, %class.btScaledTriangleCallback* %this1, i32 0, i32 2
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx4, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling5)
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %newTriangle, i32 0, i32 1
  %8 = bitcast %class.btVector3* %arrayidx6 to i8*
  %9 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %8, i8* align 4 %9, i32 16, i1 false), !tbaa.struct !8
  %10 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %10) #9
  %11 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #9
  %12 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 2
  %m_localScaling9 = getelementptr inbounds %class.btScaledTriangleCallback, %class.btScaledTriangleCallback* %this1, i32 0, i32 2
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp7, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx8, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling9)
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %newTriangle, i32 0, i32 2
  %13 = bitcast %class.btVector3* %arrayidx10 to i8*
  %14 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %13, i8* align 4 %14, i32 16, i1 false), !tbaa.struct !8
  %15 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %15) #9
  %m_originalCallback = getelementptr inbounds %class.btScaledTriangleCallback, %class.btScaledTriangleCallback* %this1, i32 0, i32 1
  %16 = load %class.btTriangleCallback*, %class.btTriangleCallback** %m_originalCallback, align 4, !tbaa !18
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %newTriangle, i32 0, i32 0
  %17 = load i32, i32* %partId.addr, align 4, !tbaa !20
  %18 = load i32, i32* %triangleIndex.addr, align 4, !tbaa !20
  %19 = bitcast %class.btTriangleCallback* %16 to void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)***
  %vtable = load void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)**, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*** %19, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)** %vtable, i64 2
  %20 = load void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)** %vfn, align 4
  call void %20(%class.btTriangleCallback* %16, %class.btVector3* %arrayidx11, i32 %17, i32 %18)
  %21 = bitcast [3 x %class.btVector3]* %newTriangle to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %21) #9
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #7

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #6 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !16
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !16
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !16
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !16
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !16
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !16
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !16
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !16
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !16
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btFabsf(float %x) #5 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !16
  %0 = load float, float* %x.addr, align 4, !tbaa !16
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #8

define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !16
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !16
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !16
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !16
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !16
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !16
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !16
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #5 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !20
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !20
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !16
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !16
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !16
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !16
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !16
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !16
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store i32 0, i32* %i, align 4, !tbaa !20
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !20
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %3 = load i32, i32* %i, align 4, !tbaa !20
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %3
  %4 = load float, float* %arrayidx, align 4, !tbaa !16
  %5 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %5, i32 0, i32 0
  %6 = load i32, i32* %i, align 4, !tbaa !20
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %6
  store float %4, float* %arrayidx3, align 4, !tbaa !16
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !20
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !20
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind readnone speculatable willreturn }
attributes #9 = { nounwind }
attributes #10 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{i64 0, i64 16, !9}
!9 = !{!4, !4, i64 0}
!10 = !{!11, !3, i64 32}
!11 = !{!"_ZTS28btScaledBvhTriangleMeshShape", !12, i64 16, !3, i64 32}
!12 = !{!"_ZTS9btVector3", !4, i64 0}
!13 = !{!14, !15, i64 4}
!14 = !{!"_ZTS16btCollisionShape", !15, i64 4, !3, i64 8}
!15 = !{!"int", !4, i64 0}
!16 = !{!17, !17, i64 0}
!17 = !{!"float", !4, i64 0}
!18 = !{!19, !3, i64 4}
!19 = !{!"_ZTS24btScaledTriangleCallback", !3, i64 4, !12, i64 8}
!20 = !{!15, !15, i64 0}
!21 = !{!22, !17, i64 12}
!22 = !{!"_ZTS14btConcaveShape", !17, i64 12}
!23 = !{!24, !15, i64 4}
!24 = !{!"_ZTS29btScaledTriangleMeshShapeData", !25, i64 0, !28, i64 60}
!25 = !{!"_ZTS23btTriangleMeshShapeData", !26, i64 0, !27, i64 12, !3, i64 40, !3, i64 44, !3, i64 48, !17, i64 52, !4, i64 56}
!26 = !{!"_ZTS20btCollisionShapeData", !3, i64 0, !15, i64 4, !4, i64 8}
!27 = !{!"_ZTS27btStridingMeshInterfaceData", !3, i64 0, !28, i64 4, !15, i64 20, !4, i64 24}
!28 = !{!"_ZTS18btVector3FloatData", !4, i64 0}
