; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/Featherstone/btMultiBodyDynamicsWorld.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/Featherstone/btMultiBodyDynamicsWorld.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btMultiBodyDynamicsWorld = type { %class.btDiscreteDynamicsWorld, %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45, %class.btMultiBodyConstraintSolver*, %struct.MultiBodyInplaceSolverIslandCallback* }
%class.btDiscreteDynamicsWorld = type { %class.btDynamicsWorld, %class.btAlignedObjectArray.0, %struct.InplaceSolverIslandCallback*, %class.btConstraintSolver*, %class.btSimulationIslandManager*, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.13, %class.btVector3, float, float, i8, i8, i8, i8, %class.btAlignedObjectArray.17, i32, i8, [3 x i8], %class.btAlignedObjectArray.9 }
%class.btDynamicsWorld = type { %class.btCollisionWorld.base, void (%class.btDynamicsWorld*, float)*, void (%class.btDynamicsWorld*, float)*, i8*, %struct.btContactSolverInfo }
%class.btCollisionWorld.base = type <{ i32 (...)**, %class.btAlignedObjectArray, %class.btDispatcher*, %struct.btDispatcherInfo, %class.btBroadphaseInterface*, %class.btIDebugDraw*, i8 }>
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%union.anon = type { i8* }
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btIDebugDraw = type opaque
%struct.btContactSolverInfo = type { %struct.btContactSolverInfoData }
%struct.btContactSolverInfoData = type { float, float, float, float, float, i32, float, float, float, float, float, i32, float, float, float, float, i32, i32, i32, float, float }
%struct.InplaceSolverIslandCallback = type opaque
%class.btConstraintSolver = type { i32 (...)** }
%class.btSimulationIslandManager = type <{ i32 (...)**, %class.btUnionFind, %class.btAlignedObjectArray.9, %class.btAlignedObjectArray, i8, [3 x i8] }>
%class.btUnionFind = type { %class.btAlignedObjectArray.5 }
%class.btAlignedObjectArray.5 = type <{ %class.btAlignedAllocator.6, [3 x i8], i32, i32, %struct.btElement*, i8, [3 x i8] }>
%class.btAlignedAllocator.6 = type { i8 }
%struct.btElement = type { i32, i32 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btTypedConstraint = type { i32 (...)**, %struct.btTypedObject, i32, %union.anon.3, float, i8, i8, i32, %class.btRigidBody*, %class.btRigidBody*, float, float, %struct.btJointFeedback* }
%struct.btTypedObject = type { i32 }
%union.anon.3 = type { i32 }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.0, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btMotionState = type { i32 (...)** }
%struct.btJointFeedback = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%class.btAlignedObjectArray.13 = type <{ %class.btAlignedAllocator.14, [3 x i8], i32, i32, %class.btRigidBody**, i8, [3 x i8] }>
%class.btAlignedAllocator.14 = type { i8 }
%class.btVector3 = type { [4 x float] }
%class.btAlignedObjectArray.17 = type <{ %class.btAlignedAllocator.18, [3 x i8], i32, i32, %class.btActionInterface**, i8, [3 x i8] }>
%class.btAlignedAllocator.18 = type { i8 }
%class.btActionInterface = type opaque
%class.btAlignedObjectArray.9 = type <{ %class.btAlignedAllocator.10, [3 x i8], i32, i32, %class.btPersistentManifold**, i8, [3 x i8] }>
%class.btAlignedAllocator.10 = type { i8 }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, i32, i32, i32, i32, i8*, i8, float, float, float, float, float, float, float, i32, %class.btVector3, %class.btVector3 }
%class.btAlignedObjectArray.21 = type <{ %class.btAlignedAllocator.22, [3 x i8], i32, i32, %class.btMultiBody**, i8, [3 x i8] }>
%class.btAlignedAllocator.22 = type { i8 }
%class.btMultiBody = type <{ %class.btMultiBodyLinkCollider*, %class.btVector3, %class.btQuaternion, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btAlignedObjectArray.24, %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.32, %class.btAlignedObjectArray.36, %class.btAlignedObjectArray.40, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3, i8, i8, i8, i8, float, i32, float, float, i8, [3 x i8], float, i8, [3 x i8] }>
%class.btMultiBodyLinkCollider = type { %class.btCollisionObject, %class.btMultiBody*, i32 }
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }
%class.btAlignedObjectArray.24 = type <{ %class.btAlignedAllocator.25, [3 x i8], i32, i32, %struct.btMultibodyLink*, i8, [3 x i8] }>
%class.btAlignedAllocator.25 = type { i8 }
%struct.btMultibodyLink = type { float, float, %class.btVector3, i32, %class.btQuaternion, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i8, %class.btQuaternion, %class.btVector3, %class.btVector3, %class.btVector3, float, %class.btMultiBodyLinkCollider*, i32 }
%class.btAlignedObjectArray.28 = type <{ %class.btAlignedAllocator.29, [3 x i8], i32, i32, %class.btMultiBodyLinkCollider**, i8, [3 x i8] }>
%class.btAlignedAllocator.29 = type { i8 }
%class.btAlignedObjectArray.32 = type <{ %class.btAlignedAllocator.33, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator.33 = type { i8 }
%class.btAlignedObjectArray.36 = type <{ %class.btAlignedAllocator.37, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator.37 = type { i8 }
%class.btAlignedObjectArray.40 = type <{ %class.btAlignedAllocator.41, [3 x i8], i32, i32, %class.btMatrix3x3*, i8, [3 x i8] }>
%class.btAlignedAllocator.41 = type { i8 }
%class.btAlignedObjectArray.45 = type <{ %class.btAlignedAllocator.46, [3 x i8], i32, i32, %class.btMultiBodyConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.46 = type { i8 }
%class.btMultiBodyConstraint = type { i32 (...)**, %class.btMultiBody*, %class.btMultiBody*, i32, i32, i32, i32, i32, i32, i8, float, %class.btAlignedObjectArray.32 }
%struct.MultiBodyInplaceSolverIslandCallback = type { %"struct.btSimulationIslandManager::IslandCallback", %struct.btContactSolverInfo*, %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraint**, i32, %class.btTypedConstraint**, i32, %class.btIDebugDraw*, %class.btDispatcher*, %class.btAlignedObjectArray, %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.45 }
%"struct.btSimulationIslandManager::IslandCallback" = type { i32 (...)** }
%class.btDispatcher = type { i32 (...)** }
%class.btBroadphaseInterface = type { i32 (...)** }
%class.btMultiBodyConstraintSolver = type { %class.btSequentialImpulseConstraintSolver, %class.btAlignedObjectArray.66, %class.btAlignedObjectArray.66, %class.btAlignedObjectArray.66, %struct.btMultiBodyJacobianData, %class.btMultiBodyConstraint**, i32 }
%class.btSequentialImpulseConstraintSolver = type { %class.btConstraintSolver, %class.btAlignedObjectArray.49, %class.btAlignedObjectArray.53, %class.btAlignedObjectArray.53, %class.btAlignedObjectArray.53, %class.btAlignedObjectArray.53, %class.btAlignedObjectArray.58, %class.btAlignedObjectArray.58, %class.btAlignedObjectArray.58, %class.btAlignedObjectArray.62, i32, i32, i32 }
%class.btAlignedObjectArray.49 = type <{ %class.btAlignedAllocator.50, [3 x i8], i32, i32, %struct.btSolverBody*, i8, [3 x i8] }>
%class.btAlignedAllocator.50 = type { i8 }
%struct.btSolverBody = type { %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btRigidBody* }
%class.btAlignedObjectArray.53 = type <{ %class.btAlignedAllocator.54, [3 x i8], i32, i32, %struct.btSolverConstraint*, i8, [3 x i8] }>
%class.btAlignedAllocator.54 = type { i8 }
%struct.btSolverConstraint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, float, float, float, float, %union.anon.56, i32, i32, i32, i32 }
%union.anon.56 = type { i8* }
%class.btAlignedObjectArray.58 = type <{ %class.btAlignedAllocator.59, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.59 = type { i8 }
%class.btAlignedObjectArray.62 = type <{ %class.btAlignedAllocator.63, [3 x i8], i32, i32, %"struct.btTypedConstraint::btConstraintInfo1"*, i8, [3 x i8] }>
%class.btAlignedAllocator.63 = type { i8 }
%"struct.btTypedConstraint::btConstraintInfo1" = type { i32, i32 }
%class.btAlignedObjectArray.66 = type <{ %class.btAlignedAllocator.67, [3 x i8], i32, i32, %struct.btMultiBodySolverConstraint*, i8, [3 x i8] }>
%class.btAlignedAllocator.67 = type { i8 }
%struct.btMultiBodySolverConstraint = type { i32, %class.btVector3, %class.btVector3, i32, i32, %class.btVector3, %class.btVector3, i32, %class.btVector3, %class.btVector3, float, float, float, float, float, float, float, float, float, %union.anon.69, i32, i32, i32, %class.btMultiBody*, i32, i32, %class.btMultiBody*, i32 }
%union.anon.69 = type { i8* }
%struct.btMultiBodyJacobianData = type { %class.btAlignedObjectArray.32, %class.btAlignedObjectArray.32, %class.btAlignedObjectArray.32, %class.btAlignedObjectArray.32, %class.btAlignedObjectArray.36, %class.btAlignedObjectArray.40, %class.btAlignedObjectArray.49*, i32 }
%class.btCollisionConfiguration = type opaque
%class.CProfileSample = type { i8 }
%class.btCollisionWorld = type <{ i32 (...)**, %class.btAlignedObjectArray, %class.btDispatcher*, %struct.btDispatcherInfo, %class.btBroadphaseInterface*, %class.btIDebugDraw*, i8, [3 x i8] }>
%class.btSortConstraintOnIslandPredicate2 = type { i8 }
%class.btSortMultiBodyConstraintOnIslandPredicate = type { i8 }
%class.btAlignedObjectArray.71 = type <{ %class.btAlignedAllocator.72, [3 x i8], i32, i32, %class.btQuaternion*, i8, [3 x i8] }>
%class.btAlignedAllocator.72 = type { i8 }
%"struct.btCollisionWorld::RayResultCallback" = type { i32 (...)**, float, %class.btCollisionObject*, i16, i16, i32 }
%class.btSerializer = type opaque

$_ZN20btAlignedObjectArrayIP11btMultiBodyE9push_backERKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP11btMultiBodyE6removeERKS1_ = comdat any

$_ZN14CProfileSampleC2EPKc = comdat any

$_ZN23btDiscreteDynamicsWorld26getSimulationIslandManagerEv = comdat any

$_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv = comdat any

$_ZN16btCollisionWorld13getDispatcherEv = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi = comdat any

$_ZNK20btPersistentManifold8getBody0Ev = comdat any

$_ZNK20btPersistentManifold8getBody1Ev = comdat any

$_ZNK17btCollisionObject25isStaticOrKinematicObjectEv = comdat any

$_ZN25btSimulationIslandManager12getUnionFindEv = comdat any

$_ZN11btUnionFind5uniteEii = comdat any

$_ZNK17btCollisionObject12getIslandTagEv = comdat any

$_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi = comdat any

$_ZNK17btTypedConstraint9isEnabledEv = comdat any

$_ZN17btTypedConstraint13getRigidBodyAEv = comdat any

$_ZN17btTypedConstraint13getRigidBodyBEv = comdat any

$_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP11btMultiBodyEixEi = comdat any

$_ZN11btMultiBody15getBaseColliderEv = comdat any

$_ZNK11btMultiBody11getNumLinksEv = comdat any

$_ZN11btMultiBody7getLinkEi = comdat any

$_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintEixEi = comdat any

$_ZN14CProfileSampleD2Ev = comdat any

$_ZNK11btMultiBody7isAwakeEv = comdat any

$_ZNK17btCollisionObject18getActivationStateEv = comdat any

$_ZN17btCollisionObject19setDeactivationTimeEf = comdat any

$_ZN20btAlignedObjectArrayIP11btMultiBodyEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintEC2Ev = comdat any

$_ZN15btDynamicsWorld13getSolverInfoEv = comdat any

$_ZN36MultiBodyInplaceSolverIslandCallbackC2EP27btMultiBodyConstraintSolverP12btDispatcher = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP11btMultiBodyED2Ev = comdat any

$_ZN23btDiscreteDynamicsWorlddlEPv = comdat any

$_ZN20btAlignedObjectArrayIfEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EC2Ev = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3EC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE6resizeEiRKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE9quickSortI34btSortConstraintOnIslandPredicate2EEvRKT_ = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE6resizeEiRKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE9quickSortI42btSortMultiBodyConstraintOnIslandPredicateEEvRKT_ = comdat any

$_ZN36MultiBodyInplaceSolverIslandCallback5setupEP19btContactSolverInfoPP17btTypedConstraintiPP21btMultiBodyConstraintiP12btIDebugDraw = comdat any

$_ZNK16btCollisionWorld22getNumCollisionObjectsEv = comdat any

$_ZN20btAlignedObjectArrayIfE6resizeEiRKf = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_ = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3E6resizeEiRKS0_ = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN11btMultiBody12addBaseForceERK9btVector3 = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK11btMultiBody11getBaseMassEv = comdat any

$_ZN36MultiBodyInplaceSolverIslandCallback18processConstraintsEv = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3ED2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3ED2Ev = comdat any

$_ZN20btAlignedObjectArrayIfED2Ev = comdat any

$_ZN20btAlignedObjectArrayI12btQuaternionEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI12btQuaternionE6resizeEiRKS0_ = comdat any

$_ZN12btQuaternionC2Ev = comdat any

$_ZNK11btMultiBody17getWorldToBaseRotEv = comdat any

$_ZN20btAlignedObjectArrayI12btQuaternionEixEi = comdat any

$_ZNK11btMultiBody10getBasePosEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EixEi = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZNK10btQuadWord1xEv = comdat any

$_ZNK10btQuadWord1yEv = comdat any

$_ZNK10btQuadWord1zEv = comdat any

$_ZNK10btQuadWord1wEv = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN11btTransform11setIdentityEv = comdat any

$_ZN11btTransform9setOriginERK9btVector3 = comdat any

$_ZN11btTransform11setRotationERK12btQuaternion = comdat any

$_ZN12btQuaternionC2ERKfS1_S1_S1_ = comdat any

$_ZN17btCollisionObject17setWorldTransformERK11btTransform = comdat any

$_ZmlRK12btQuaternionS1_ = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_Z10quatRotateRK12btQuaternionRK9btVector3 = comdat any

$_ZNK12btQuaternion7inverseEv = comdat any

$_ZN20btAlignedObjectArrayI12btQuaternionED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE9push_backERKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE6removeERKS1_ = comdat any

$_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw = comdat any

$_ZN16btCollisionWorld14getDebugDrawerEv = comdat any

$_ZNK23btDiscreteDynamicsWorld12getWorldTypeEv = comdat any

$_ZN23btDiscreteDynamicsWorld11setNumTasksEi = comdat any

$_ZN23btDiscreteDynamicsWorld14updateVehiclesEf = comdat any

$_ZN11btUnionFind4findEi = comdat any

$_ZN20btAlignedObjectArrayI9btElementEixEi = comdat any

$_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi = comdat any

$_ZN25btSimulationIslandManager14IslandCallbackC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintEC2Ev = comdat any

$_ZN36MultiBodyInplaceSolverIslandCallbackD2Ev = comdat any

$_ZN36MultiBodyInplaceSolverIslandCallbackD0Ev = comdat any

$_ZN36MultiBodyInplaceSolverIslandCallback13processIslandEPP17btCollisionObjectiPP20btPersistentManifoldii = comdat any

$_ZN25btSimulationIslandManager14IslandCallbackD2Ev = comdat any

$_ZN25btSimulationIslandManager14IslandCallbackD0Ev = comdat any

$_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE4initEv = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv = comdat any

$_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE10deallocateEPS1_ = comdat any

$_Z24btGetConstraintIslandId2PK17btTypedConstraint = comdat any

$_Z32btGetMultiBodyConstraintIslandIdPK21btMultiBodyConstraint = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE9push_backERKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE9push_backERKS1_ = comdat any

$_ZNK17btTypedConstraint13getRigidBodyAEv = comdat any

$_ZNK17btTypedConstraint13getRigidBodyBEv = comdat any

$_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE8allocateEiPPKS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP17btTypedConstraintE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE8allocateEiPPKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE6resizeEiRKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_ = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi = comdat any

$_ZN10btQuadWordC2Ev = comdat any

$_ZN11btMatrix3x311setIdentityEv = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btMatrix3x311setRotationERK12btQuaternion = comdat any

$_ZNK12btQuaternion7length2Ev = comdat any

$_ZNK12btQuaternion3dotERKS_ = comdat any

$_ZN10btQuadWordC2ERKfS1_S1_S1_ = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZmlRK12btQuaternionRK9btVector3 = comdat any

$_ZN12btQuaternionmLERKS_ = comdat any

$_ZNK10btQuadWord4getXEv = comdat any

$_ZNK10btQuadWord4getYEv = comdat any

$_ZNK10btQuadWord4getZEv = comdat any

$_ZN10btQuadWord8setValueERKfS1_S1_S1_ = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E4initEv = comdat any

$_ZN18btAlignedAllocatorIfLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIfE4initEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E5clearEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayIfE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIfE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIfE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIfE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf = comdat any

$_ZNK20btAlignedObjectArrayIP11btMultiBodyE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP11btMultiBodyE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIP11btMultiBodyE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP11btMultiBodyE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP11btMultiBodyE4copyEiiPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP11btMultiBodyE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP11btMultiBodyE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP11btMultiBodyLj16EE8allocateEiPPKS1_ = comdat any

$_ZN18btAlignedAllocatorIP11btMultiBodyLj16EE10deallocateEPS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP11btMultiBodyE16findLinearSearchERKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP11btMultiBodyE4swapEii = comdat any

$_ZN20btAlignedObjectArrayIP11btMultiBodyE8pop_backEv = comdat any

$_ZN18btAlignedAllocatorIP21btMultiBodyConstraintLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE4initEv = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP21btMultiBodyConstraintLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP21btMultiBodyConstraintLj16EE8allocateEiPPKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE9allocSizeEi = comdat any

$_ZN18btAlignedAllocatorIP11btMultiBodyLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP11btMultiBodyE4initEv = comdat any

$_ZN20btAlignedObjectArrayIP11btMultiBodyE5clearEv = comdat any

$_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3E4initEv = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3E5clearEv = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3E7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayI11btMatrix3x3E4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3E10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE17quickSortInternalI34btSortConstraintOnIslandPredicate2EEvRKT_ii = comdat any

$_ZNK34btSortConstraintOnIslandPredicate2clEPK17btTypedConstraintS2_ = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE4swapEii = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE17quickSortInternalI42btSortMultiBodyConstraintOnIslandPredicateEEvRKT_ii = comdat any

$_ZNK42btSortMultiBodyConstraintOnIslandPredicateclEPK21btMultiBodyConstraintS2_ = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE4swapEii = comdat any

$_ZN20btAlignedObjectArrayIfE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIfE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIfE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIfE4copyEiiPf = comdat any

$_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7reserveEi = comdat any

$_ZN9btVector3nwEmPv = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3E7reserveEi = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZNK20btAlignedObjectArrayI11btMatrix3x3E8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3E8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI11btMatrix3x3E4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI12btQuaternionLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI12btQuaternionE4initEv = comdat any

$_ZN20btAlignedObjectArrayI12btQuaternionE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI12btQuaternionE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayI12btQuaternionE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI12btQuaternionE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI12btQuaternionLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI12btQuaternionE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayI12btQuaternionE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI12btQuaternionE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI12btQuaternionE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI12btQuaternionLj16EE8allocateEiPPKS0_ = comdat any

$_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE16findLinearSearchERKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE8pop_backEv = comdat any

$_ZTV36MultiBodyInplaceSolverIslandCallback = comdat any

$_ZTS36MultiBodyInplaceSolverIslandCallback = comdat any

$_ZTSN25btSimulationIslandManager14IslandCallbackE = comdat any

$_ZTIN25btSimulationIslandManager14IslandCallbackE = comdat any

$_ZTI36MultiBodyInplaceSolverIslandCallback = comdat any

$_ZTVN25btSimulationIslandManager14IslandCallbackE = comdat any

@.str = private unnamed_addr constant [27 x i8] c"calculateSimulationIslands\00", align 1
@.str.1 = private unnamed_addr constant [48 x i8] c"btMultiBodyDynamicsWorld::updateActivationState\00", align 1
@_ZTV24btMultiBodyDynamicsWorld = hidden unnamed_addr constant { [51 x i8*] } { [51 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI24btMultiBodyDynamicsWorld to i8*), i8* bitcast (%class.btMultiBodyDynamicsWorld* (%class.btMultiBodyDynamicsWorld*)* @_ZN24btMultiBodyDynamicsWorldD1Ev to i8*), i8* bitcast (void (%class.btMultiBodyDynamicsWorld*)* @_ZN24btMultiBodyDynamicsWorldD0Ev to i8*), i8* bitcast (void (%class.btCollisionWorld*)* @_ZN16btCollisionWorld11updateAabbsEv to i8*), i8* bitcast (void (%class.btCollisionWorld*)* @_ZN16btCollisionWorld23computeOverlappingPairsEv to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btIDebugDraw*)* @_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw to i8*), i8* bitcast (%class.btIDebugDraw* (%class.btCollisionWorld*)* @_ZN16btCollisionWorld14getDebugDrawerEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorld14debugDrawWorldEv to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btTransform*, %class.btCollisionShape*, %class.btVector3*)* @_ZN16btCollisionWorld15debugDrawObjectERK11btTransformPK16btCollisionShapeRK9btVector3 to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btVector3*, %class.btVector3*, %"struct.btCollisionWorld::RayResultCallback"*)* @_ZNK16btCollisionWorld7rayTestERK9btVector3S2_RNS_17RayResultCallbackE to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*, i16, i16)* @_ZN23btDiscreteDynamicsWorld18addCollisionObjectEP17btCollisionObjectss to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*)* @_ZN23btDiscreteDynamicsWorld21removeCollisionObjectEP17btCollisionObject to i8*), i8* bitcast (void (%class.btCollisionWorld*)* @_ZN16btCollisionWorld33performDiscreteCollisionDetectionEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btSerializer*)* @_ZN23btDiscreteDynamicsWorld9serializeEP12btSerializer to i8*), i8* bitcast (i32 (%class.btDiscreteDynamicsWorld*, float, i32, float)* @_ZN23btDiscreteDynamicsWorld14stepSimulationEfif to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*, i1)* @_ZN23btDiscreteDynamicsWorld13addConstraintEP17btTypedConstraintb to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*)* @_ZN23btDiscreteDynamicsWorld16removeConstraintEP17btTypedConstraint to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld9addActionEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld12removeActionEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btVector3*)* @_ZN23btDiscreteDynamicsWorld10setGravityERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btDiscreteDynamicsWorld*)* @_ZNK23btDiscreteDynamicsWorld10getGravityEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorld23synchronizeMotionStatesEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btRigidBody*)* @_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBody to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btRigidBody*, i16, i16)* @_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBodyss to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btRigidBody*)* @_ZN23btDiscreteDynamicsWorld15removeRigidBodyEP11btRigidBody to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btConstraintSolver*)* @_ZN23btDiscreteDynamicsWorld19setConstraintSolverEP18btConstraintSolver to i8*), i8* bitcast (%class.btConstraintSolver* (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorld19getConstraintSolverEv to i8*), i8* bitcast (i32 (%class.btDiscreteDynamicsWorld*)* @_ZNK23btDiscreteDynamicsWorld17getNumConstraintsEv to i8*), i8* bitcast (%class.btTypedConstraint* (%class.btDiscreteDynamicsWorld*, i32)* @_ZN23btDiscreteDynamicsWorld13getConstraintEi to i8*), i8* bitcast (%class.btTypedConstraint* (%class.btDiscreteDynamicsWorld*, i32)* @_ZNK23btDiscreteDynamicsWorld13getConstraintEi to i8*), i8* bitcast (i32 (%class.btDiscreteDynamicsWorld*)* @_ZNK23btDiscreteDynamicsWorld12getWorldTypeEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorld11clearForcesEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld10addVehicleEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld13removeVehicleEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld12addCharacterEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld15removeCharacterEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld25predictUnconstraintMotionEf to i8*), i8* bitcast (void (%class.btMultiBodyDynamicsWorld*, float)* @_ZN24btMultiBodyDynamicsWorld19integrateTransformsEf to i8*), i8* bitcast (void (%class.btMultiBodyDynamicsWorld*)* @_ZN24btMultiBodyDynamicsWorld26calculateSimulationIslandsEv to i8*), i8* bitcast (void (%class.btMultiBodyDynamicsWorld*, %struct.btContactSolverInfo*)* @_ZN24btMultiBodyDynamicsWorld16solveConstraintsER19btContactSolverInfo to i8*), i8* bitcast (void (%class.btMultiBodyDynamicsWorld*, float)* @_ZN24btMultiBodyDynamicsWorld21updateActivationStateEf to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld28internalSingleStepSimulationEf to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld18saveKinematicStateEf to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorld12applyGravityEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, i32)* @_ZN23btDiscreteDynamicsWorld11setNumTasksEi to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld14updateVehiclesEf to i8*), i8* bitcast (void (%class.btMultiBodyDynamicsWorld*, %class.btMultiBody*, i16, i16)* @_ZN24btMultiBodyDynamicsWorld12addMultiBodyEP11btMultiBodyss to i8*), i8* bitcast (void (%class.btMultiBodyDynamicsWorld*, %class.btMultiBody*)* @_ZN24btMultiBodyDynamicsWorld15removeMultiBodyEP11btMultiBody to i8*), i8* bitcast (void (%class.btMultiBodyDynamicsWorld*, %class.btMultiBodyConstraint*)* @_ZN24btMultiBodyDynamicsWorld22addMultiBodyConstraintEP21btMultiBodyConstraint to i8*), i8* bitcast (void (%class.btMultiBodyDynamicsWorld*, %class.btMultiBodyConstraint*)* @_ZN24btMultiBodyDynamicsWorld25removeMultiBodyConstraintEP21btMultiBodyConstraint to i8*)] }, align 4
@.str.2 = private unnamed_addr constant [17 x i8] c"solveConstraints\00", align 1
@.str.3 = private unnamed_addr constant [40 x i8] c"btMultiBody addForce and stepVelocities\00", align 1
@.str.4 = private unnamed_addr constant [26 x i8] c"btMultiBody stepPositions\00", align 1
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS24btMultiBodyDynamicsWorld = hidden constant [27 x i8] c"24btMultiBodyDynamicsWorld\00", align 1
@_ZTI23btDiscreteDynamicsWorld = external constant i8*
@_ZTI24btMultiBodyDynamicsWorld = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([27 x i8], [27 x i8]* @_ZTS24btMultiBodyDynamicsWorld, i32 0, i32 0), i8* bitcast (i8** @_ZTI23btDiscreteDynamicsWorld to i8*) }, align 4
@_ZTV36MultiBodyInplaceSolverIslandCallback = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI36MultiBodyInplaceSolverIslandCallback to i8*), i8* bitcast (%struct.MultiBodyInplaceSolverIslandCallback* (%struct.MultiBodyInplaceSolverIslandCallback*)* @_ZN36MultiBodyInplaceSolverIslandCallbackD2Ev to i8*), i8* bitcast (void (%struct.MultiBodyInplaceSolverIslandCallback*)* @_ZN36MultiBodyInplaceSolverIslandCallbackD0Ev to i8*), i8* bitcast (void (%struct.MultiBodyInplaceSolverIslandCallback*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)* @_ZN36MultiBodyInplaceSolverIslandCallback13processIslandEPP17btCollisionObjectiPP20btPersistentManifoldii to i8*)] }, comdat, align 4
@_ZTS36MultiBodyInplaceSolverIslandCallback = linkonce_odr hidden constant [39 x i8] c"36MultiBodyInplaceSolverIslandCallback\00", comdat, align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTSN25btSimulationIslandManager14IslandCallbackE = linkonce_odr hidden constant [46 x i8] c"N25btSimulationIslandManager14IslandCallbackE\00", comdat, align 1
@_ZTIN25btSimulationIslandManager14IslandCallbackE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([46 x i8], [46 x i8]* @_ZTSN25btSimulationIslandManager14IslandCallbackE, i32 0, i32 0) }, comdat, align 4
@_ZTI36MultiBodyInplaceSolverIslandCallback = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([39 x i8], [39 x i8]* @_ZTS36MultiBodyInplaceSolverIslandCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTIN25btSimulationIslandManager14IslandCallbackE to i8*) }, comdat, align 4
@_ZTVN25btSimulationIslandManager14IslandCallbackE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTIN25btSimulationIslandManager14IslandCallbackE to i8*), i8* bitcast (%"struct.btSimulationIslandManager::IslandCallback"* (%"struct.btSimulationIslandManager::IslandCallback"*)* @_ZN25btSimulationIslandManager14IslandCallbackD2Ev to i8*), i8* bitcast (void (%"struct.btSimulationIslandManager::IslandCallback"*)* @_ZN25btSimulationIslandManager14IslandCallbackD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4

@_ZN24btMultiBodyDynamicsWorldC1EP12btDispatcherP21btBroadphaseInterfaceP27btMultiBodyConstraintSolverP24btCollisionConfiguration = hidden unnamed_addr alias %class.btMultiBodyDynamicsWorld* (%class.btMultiBodyDynamicsWorld*, %class.btDispatcher*, %class.btBroadphaseInterface*, %class.btMultiBodyConstraintSolver*, %class.btCollisionConfiguration*), %class.btMultiBodyDynamicsWorld* (%class.btMultiBodyDynamicsWorld*, %class.btDispatcher*, %class.btBroadphaseInterface*, %class.btMultiBodyConstraintSolver*, %class.btCollisionConfiguration*)* @_ZN24btMultiBodyDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP27btMultiBodyConstraintSolverP24btCollisionConfiguration
@_ZN24btMultiBodyDynamicsWorldD1Ev = hidden unnamed_addr alias %class.btMultiBodyDynamicsWorld* (%class.btMultiBodyDynamicsWorld*), %class.btMultiBodyDynamicsWorld* (%class.btMultiBodyDynamicsWorld*)* @_ZN24btMultiBodyDynamicsWorldD2Ev

define hidden void @_ZN24btMultiBodyDynamicsWorld12addMultiBodyEP11btMultiBodyss(%class.btMultiBodyDynamicsWorld* %this, %class.btMultiBody* %body, i16 signext %group, i16 signext %mask) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  %body.addr = alloca %class.btMultiBody*, align 4
  %group.addr = alloca i16, align 2
  %mask.addr = alloca i16, align 2
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4, !tbaa !2
  store %class.btMultiBody* %body, %class.btMultiBody** %body.addr, align 4, !tbaa !2
  store i16 %group, i16* %group.addr, align 2, !tbaa !6
  store i16 %mask, i16* %mask.addr, align 2, !tbaa !6
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %m_multiBodies = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayIP11btMultiBodyE9push_backERKS1_(%class.btAlignedObjectArray.21* %m_multiBodies, %class.btMultiBody** nonnull align 4 dereferenceable(4) %body.addr)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP11btMultiBodyE9push_backERKS1_(%class.btAlignedObjectArray.21* %this, %class.btMultiBody** nonnull align 4 dereferenceable(4) %_Val) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.21*, align 4
  %_Val.addr = alloca %class.btMultiBody**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.21* %this, %class.btAlignedObjectArray.21** %this.addr, align 4, !tbaa !2
  store %class.btMultiBody** %_Val, %class.btMultiBody*** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.21*, %class.btAlignedObjectArray.21** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %call = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.21* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !8
  %1 = load i32, i32* %sz, align 4, !tbaa !8
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE8capacityEv(%class.btAlignedObjectArray.21* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.21* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP11btMultiBodyE9allocSizeEi(%class.btAlignedObjectArray.21* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP11btMultiBodyE7reserveEi(%class.btAlignedObjectArray.21* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.21* %this1, i32 0, i32 4
  %2 = load %class.btMultiBody**, %class.btMultiBody*** %m_data, align 4, !tbaa !10
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.21* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !14
  %arrayidx = getelementptr inbounds %class.btMultiBody*, %class.btMultiBody** %2, i32 %3
  %4 = bitcast %class.btMultiBody** %arrayidx to i8*
  %5 = bitcast i8* %4 to %class.btMultiBody**
  %6 = load %class.btMultiBody**, %class.btMultiBody*** %_Val.addr, align 4, !tbaa !2
  %7 = load %class.btMultiBody*, %class.btMultiBody** %6, align 4, !tbaa !2
  store %class.btMultiBody* %7, %class.btMultiBody** %5, align 4, !tbaa !2
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.21* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size5, align 4, !tbaa !14
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size5, align 4, !tbaa !14
  %9 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #10
  ret void
}

define hidden void @_ZN24btMultiBodyDynamicsWorld15removeMultiBodyEP11btMultiBody(%class.btMultiBodyDynamicsWorld* %this, %class.btMultiBody* %body) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  %body.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4, !tbaa !2
  store %class.btMultiBody* %body, %class.btMultiBody** %body.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %m_multiBodies = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayIP11btMultiBodyE6removeERKS1_(%class.btAlignedObjectArray.21* %m_multiBodies, %class.btMultiBody** nonnull align 4 dereferenceable(4) %body.addr)
  ret void
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP11btMultiBodyE6removeERKS1_(%class.btAlignedObjectArray.21* %this, %class.btMultiBody** nonnull align 4 dereferenceable(4) %key) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.21*, align 4
  %key.addr = alloca %class.btMultiBody**, align 4
  %findIndex = alloca i32, align 4
  store %class.btAlignedObjectArray.21* %this, %class.btAlignedObjectArray.21** %this.addr, align 4, !tbaa !2
  store %class.btMultiBody** %key, %class.btMultiBody*** %key.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.21*, %class.btAlignedObjectArray.21** %this.addr, align 4
  %0 = bitcast i32* %findIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %class.btMultiBody**, %class.btMultiBody*** %key.addr, align 4, !tbaa !2
  %call = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE16findLinearSearchERKS1_(%class.btAlignedObjectArray.21* %this1, %class.btMultiBody** nonnull align 4 dereferenceable(4) %1)
  store i32 %call, i32* %findIndex, align 4, !tbaa !8
  %2 = load i32, i32* %findIndex, align 4, !tbaa !8
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.21* %this1)
  %cmp = icmp slt i32 %2, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load i32, i32* %findIndex, align 4, !tbaa !8
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.21* %this1)
  %sub = sub nsw i32 %call3, 1
  call void @_ZN20btAlignedObjectArrayIP11btMultiBodyE4swapEii(%class.btAlignedObjectArray.21* %this1, i32 %3, i32 %sub)
  call void @_ZN20btAlignedObjectArrayIP11btMultiBodyE8pop_backEv(%class.btAlignedObjectArray.21* %this1)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %4 = bitcast i32* %findIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #10
  ret void
}

define hidden void @_ZN24btMultiBodyDynamicsWorld26calculateSimulationIslandsEv(%class.btMultiBodyDynamicsWorld* %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %i = alloca i32, align 4
  %manifold = alloca %class.btPersistentManifold*, align 4
  %colObj0 = alloca %class.btCollisionObject*, align 4
  %colObj1 = alloca %class.btCollisionObject*, align 4
  %i20 = alloca i32, align 4
  %numConstraints = alloca i32, align 4
  %constraint = alloca %class.btTypedConstraint*, align 4
  %colObj029 = alloca %class.btRigidBody*, align 4
  %colObj131 = alloca %class.btRigidBody*, align 4
  %i50 = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %body = alloca %class.btMultiBody*, align 4
  %prev = alloca %class.btMultiBodyLinkCollider*, align 4
  %b = alloca i32, align 4
  %cur = alloca %class.btMultiBodyLinkCollider*, align 4
  %tagPrev = alloca i32, align 4
  %tagCur = alloca i32, align 4
  %i89 = alloca i32, align 4
  %c = alloca %class.btMultiBodyConstraint*, align 4
  %tagA = alloca i32, align 4
  %tagB = alloca i32, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %0 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %0) #10
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str, i32 0, i32 0))
  %1 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %call2 = call %class.btSimulationIslandManager* @_ZN23btDiscreteDynamicsWorld26getSimulationIslandManagerEv(%class.btDiscreteDynamicsWorld* %1)
  %2 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %call3 = call %class.btCollisionWorld* @_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv(%class.btDiscreteDynamicsWorld* %2)
  %3 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %call4 = call %class.btCollisionWorld* @_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv(%class.btDiscreteDynamicsWorld* %3)
  %call5 = call %class.btDispatcher* @_ZN16btCollisionWorld13getDispatcherEv(%class.btCollisionWorld* %call4)
  %4 = bitcast %class.btSimulationIslandManager* %call2 to void (%class.btSimulationIslandManager*, %class.btCollisionWorld*, %class.btDispatcher*)***
  %vtable = load void (%class.btSimulationIslandManager*, %class.btCollisionWorld*, %class.btDispatcher*)**, void (%class.btSimulationIslandManager*, %class.btCollisionWorld*, %class.btDispatcher*)*** %4, align 4, !tbaa !15
  %vfn = getelementptr inbounds void (%class.btSimulationIslandManager*, %class.btCollisionWorld*, %class.btDispatcher*)*, void (%class.btSimulationIslandManager*, %class.btCollisionWorld*, %class.btDispatcher*)** %vtable, i64 2
  %5 = load void (%class.btSimulationIslandManager*, %class.btCollisionWorld*, %class.btDispatcher*)*, void (%class.btSimulationIslandManager*, %class.btCollisionWorld*, %class.btDispatcher*)** %vfn, align 4
  call void %5(%class.btSimulationIslandManager* %call2, %class.btCollisionWorld* %call3, %class.btDispatcher* %call5)
  %6 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #10
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %7 = load i32, i32* %i, align 4, !tbaa !8
  %8 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_predictiveManifolds = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %8, i32 0, i32 18
  %call6 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %m_predictiveManifolds)
  %cmp = icmp slt i32 %7, %call6
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #10
  br label %for.end

for.body:                                         ; preds = %for.cond
  %10 = bitcast %class.btPersistentManifold** %manifold to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #10
  %11 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_predictiveManifolds7 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %11, i32 0, i32 18
  %12 = load i32, i32* %i, align 4, !tbaa !8
  %call8 = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.9* %m_predictiveManifolds7, i32 %12)
  %13 = load %class.btPersistentManifold*, %class.btPersistentManifold** %call8, align 4, !tbaa !2
  store %class.btPersistentManifold* %13, %class.btPersistentManifold** %manifold, align 4, !tbaa !2
  %14 = bitcast %class.btCollisionObject** %colObj0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #10
  %15 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4, !tbaa !2
  %call9 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %15)
  store %class.btCollisionObject* %call9, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %16 = bitcast %class.btCollisionObject** %colObj1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #10
  %17 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4, !tbaa !2
  %call10 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody1Ev(%class.btPersistentManifold* %17)
  store %class.btCollisionObject* %call10, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %18 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %tobool = icmp ne %class.btCollisionObject* %18, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body
  %19 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %call11 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %19)
  br i1 %call11, label %if.end, label %land.lhs.true12

land.lhs.true12:                                  ; preds = %land.lhs.true
  %20 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %tobool13 = icmp ne %class.btCollisionObject* %20, null
  br i1 %tobool13, label %land.lhs.true14, label %if.end

land.lhs.true14:                                  ; preds = %land.lhs.true12
  %21 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %call15 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %21)
  br i1 %call15, label %if.end, label %if.then

if.then:                                          ; preds = %land.lhs.true14
  %22 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %call16 = call %class.btSimulationIslandManager* @_ZN23btDiscreteDynamicsWorld26getSimulationIslandManagerEv(%class.btDiscreteDynamicsWorld* %22)
  %call17 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %call16)
  %23 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %call18 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %23)
  %24 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %call19 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %24)
  call void @_ZN11btUnionFind5uniteEii(%class.btUnionFind* %call17, i32 %call18, i32 %call19)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true14, %land.lhs.true12, %land.lhs.true, %for.body
  %25 = bitcast %class.btCollisionObject** %colObj1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #10
  %26 = bitcast %class.btCollisionObject** %colObj0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #10
  %27 = bitcast %class.btPersistentManifold** %manifold to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #10
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %28 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %28, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %29 = bitcast i32* %i20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #10
  %30 = bitcast i32* %numConstraints to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #10
  %31 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_constraints = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %31, i32 0, i32 5
  %call21 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.0* %m_constraints)
  store i32 %call21, i32* %numConstraints, align 4, !tbaa !8
  store i32 0, i32* %i20, align 4, !tbaa !8
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc47, %for.end
  %32 = load i32, i32* %i20, align 4, !tbaa !8
  %33 = load i32, i32* %numConstraints, align 4, !tbaa !8
  %cmp23 = icmp slt i32 %32, %33
  br i1 %cmp23, label %for.body24, label %for.end49

for.body24:                                       ; preds = %for.cond22
  %34 = bitcast %class.btTypedConstraint** %constraint to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #10
  %35 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_constraints25 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %35, i32 0, i32 5
  %36 = load i32, i32* %i20, align 4, !tbaa !8
  %call26 = call nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray.0* %m_constraints25, i32 %36)
  %37 = load %class.btTypedConstraint*, %class.btTypedConstraint** %call26, align 4, !tbaa !2
  store %class.btTypedConstraint* %37, %class.btTypedConstraint** %constraint, align 4, !tbaa !2
  %38 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint, align 4, !tbaa !2
  %call27 = call zeroext i1 @_ZNK17btTypedConstraint9isEnabledEv(%class.btTypedConstraint* %38)
  br i1 %call27, label %if.then28, label %if.end46

if.then28:                                        ; preds = %for.body24
  %39 = bitcast %class.btRigidBody** %colObj029 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #10
  %40 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint, align 4, !tbaa !2
  %call30 = call nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %40)
  store %class.btRigidBody* %call30, %class.btRigidBody** %colObj029, align 4, !tbaa !2
  %41 = bitcast %class.btRigidBody** %colObj131 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #10
  %42 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint, align 4, !tbaa !2
  %call32 = call nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %42)
  store %class.btRigidBody* %call32, %class.btRigidBody** %colObj131, align 4, !tbaa !2
  %43 = load %class.btRigidBody*, %class.btRigidBody** %colObj029, align 4, !tbaa !2
  %tobool33 = icmp ne %class.btRigidBody* %43, null
  br i1 %tobool33, label %land.lhs.true34, label %if.end45

land.lhs.true34:                                  ; preds = %if.then28
  %44 = load %class.btRigidBody*, %class.btRigidBody** %colObj029, align 4, !tbaa !2
  %45 = bitcast %class.btRigidBody* %44 to %class.btCollisionObject*
  %call35 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %45)
  br i1 %call35, label %if.end45, label %land.lhs.true36

land.lhs.true36:                                  ; preds = %land.lhs.true34
  %46 = load %class.btRigidBody*, %class.btRigidBody** %colObj131, align 4, !tbaa !2
  %tobool37 = icmp ne %class.btRigidBody* %46, null
  br i1 %tobool37, label %land.lhs.true38, label %if.end45

land.lhs.true38:                                  ; preds = %land.lhs.true36
  %47 = load %class.btRigidBody*, %class.btRigidBody** %colObj131, align 4, !tbaa !2
  %48 = bitcast %class.btRigidBody* %47 to %class.btCollisionObject*
  %call39 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %48)
  br i1 %call39, label %if.end45, label %if.then40

if.then40:                                        ; preds = %land.lhs.true38
  %49 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %call41 = call %class.btSimulationIslandManager* @_ZN23btDiscreteDynamicsWorld26getSimulationIslandManagerEv(%class.btDiscreteDynamicsWorld* %49)
  %call42 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %call41)
  %50 = load %class.btRigidBody*, %class.btRigidBody** %colObj029, align 4, !tbaa !2
  %51 = bitcast %class.btRigidBody* %50 to %class.btCollisionObject*
  %call43 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %51)
  %52 = load %class.btRigidBody*, %class.btRigidBody** %colObj131, align 4, !tbaa !2
  %53 = bitcast %class.btRigidBody* %52 to %class.btCollisionObject*
  %call44 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %53)
  call void @_ZN11btUnionFind5uniteEii(%class.btUnionFind* %call42, i32 %call43, i32 %call44)
  br label %if.end45

if.end45:                                         ; preds = %if.then40, %land.lhs.true38, %land.lhs.true36, %land.lhs.true34, %if.then28
  %54 = bitcast %class.btRigidBody** %colObj131 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #10
  %55 = bitcast %class.btRigidBody** %colObj029 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #10
  br label %if.end46

if.end46:                                         ; preds = %if.end45, %for.body24
  %56 = bitcast %class.btTypedConstraint** %constraint to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #10
  br label %for.inc47

for.inc47:                                        ; preds = %if.end46
  %57 = load i32, i32* %i20, align 4, !tbaa !8
  %inc48 = add nsw i32 %57, 1
  store i32 %inc48, i32* %i20, align 4, !tbaa !8
  br label %for.cond22

for.end49:                                        ; preds = %for.cond22
  %58 = bitcast i32* %numConstraints to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #10
  %59 = bitcast i32* %i20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #10
  %60 = bitcast i32* %i50 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #10
  store i32 0, i32* %i50, align 4, !tbaa !8
  br label %for.cond51

for.cond51:                                       ; preds = %for.inc86, %for.end49
  %61 = load i32, i32* %i50, align 4, !tbaa !8
  %m_multiBodies = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %call52 = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.21* %m_multiBodies)
  %cmp53 = icmp slt i32 %61, %call52
  br i1 %cmp53, label %for.body55, label %for.cond.cleanup54

for.cond.cleanup54:                               ; preds = %for.cond51
  store i32 8, i32* %cleanup.dest.slot, align 4
  %62 = bitcast i32* %i50 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #10
  br label %for.end88

for.body55:                                       ; preds = %for.cond51
  %63 = bitcast %class.btMultiBody** %body to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #10
  %m_multiBodies56 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %64 = load i32, i32* %i50, align 4, !tbaa !8
  %call57 = call nonnull align 4 dereferenceable(4) %class.btMultiBody** @_ZN20btAlignedObjectArrayIP11btMultiBodyEixEi(%class.btAlignedObjectArray.21* %m_multiBodies56, i32 %64)
  %65 = load %class.btMultiBody*, %class.btMultiBody** %call57, align 4, !tbaa !2
  store %class.btMultiBody* %65, %class.btMultiBody** %body, align 4, !tbaa !2
  %66 = bitcast %class.btMultiBodyLinkCollider** %prev to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #10
  %67 = load %class.btMultiBody*, %class.btMultiBody** %body, align 4, !tbaa !2
  %call58 = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %67)
  store %class.btMultiBodyLinkCollider* %call58, %class.btMultiBodyLinkCollider** %prev, align 4, !tbaa !2
  %68 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #10
  store i32 0, i32* %b, align 4, !tbaa !8
  br label %for.cond59

for.cond59:                                       ; preds = %for.inc83, %for.body55
  %69 = load i32, i32* %b, align 4, !tbaa !8
  %70 = load %class.btMultiBody*, %class.btMultiBody** %body, align 4, !tbaa !2
  %call60 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %70)
  %cmp61 = icmp slt i32 %69, %call60
  br i1 %cmp61, label %for.body63, label %for.cond.cleanup62

for.cond.cleanup62:                               ; preds = %for.cond59
  store i32 11, i32* %cleanup.dest.slot, align 4
  %71 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #10
  br label %for.end85

for.body63:                                       ; preds = %for.cond59
  %72 = bitcast %class.btMultiBodyLinkCollider** %cur to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %72) #10
  %73 = load %class.btMultiBody*, %class.btMultiBody** %body, align 4, !tbaa !2
  %74 = load i32, i32* %b, align 4, !tbaa !8
  %call64 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %73, i32 %74)
  %m_collider = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call64, i32 0, i32 15
  %75 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider, align 4, !tbaa !17
  store %class.btMultiBodyLinkCollider* %75, %class.btMultiBodyLinkCollider** %cur, align 4, !tbaa !2
  %76 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %cur, align 4, !tbaa !2
  %tobool65 = icmp ne %class.btMultiBodyLinkCollider* %76, null
  br i1 %tobool65, label %land.lhs.true66, label %if.end77

land.lhs.true66:                                  ; preds = %for.body63
  %77 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %cur, align 4, !tbaa !2
  %78 = bitcast %class.btMultiBodyLinkCollider* %77 to %class.btCollisionObject*
  %call67 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %78)
  br i1 %call67, label %if.end77, label %land.lhs.true68

land.lhs.true68:                                  ; preds = %land.lhs.true66
  %79 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %prev, align 4, !tbaa !2
  %tobool69 = icmp ne %class.btMultiBodyLinkCollider* %79, null
  br i1 %tobool69, label %land.lhs.true70, label %if.end77

land.lhs.true70:                                  ; preds = %land.lhs.true68
  %80 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %prev, align 4, !tbaa !2
  %81 = bitcast %class.btMultiBodyLinkCollider* %80 to %class.btCollisionObject*
  %call71 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %81)
  br i1 %call71, label %if.end77, label %if.then72

if.then72:                                        ; preds = %land.lhs.true70
  %82 = bitcast i32* %tagPrev to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #10
  %83 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %prev, align 4, !tbaa !2
  %84 = bitcast %class.btMultiBodyLinkCollider* %83 to %class.btCollisionObject*
  %call73 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %84)
  store i32 %call73, i32* %tagPrev, align 4, !tbaa !8
  %85 = bitcast i32* %tagCur to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %85) #10
  %86 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %cur, align 4, !tbaa !2
  %87 = bitcast %class.btMultiBodyLinkCollider* %86 to %class.btCollisionObject*
  %call74 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %87)
  store i32 %call74, i32* %tagCur, align 4, !tbaa !8
  %88 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %call75 = call %class.btSimulationIslandManager* @_ZN23btDiscreteDynamicsWorld26getSimulationIslandManagerEv(%class.btDiscreteDynamicsWorld* %88)
  %call76 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %call75)
  %89 = load i32, i32* %tagPrev, align 4, !tbaa !8
  %90 = load i32, i32* %tagCur, align 4, !tbaa !8
  call void @_ZN11btUnionFind5uniteEii(%class.btUnionFind* %call76, i32 %89, i32 %90)
  %91 = bitcast i32* %tagCur to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #10
  %92 = bitcast i32* %tagPrev to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #10
  br label %if.end77

if.end77:                                         ; preds = %if.then72, %land.lhs.true70, %land.lhs.true68, %land.lhs.true66, %for.body63
  %93 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %cur, align 4, !tbaa !2
  %tobool78 = icmp ne %class.btMultiBodyLinkCollider* %93, null
  br i1 %tobool78, label %land.lhs.true79, label %if.end82

land.lhs.true79:                                  ; preds = %if.end77
  %94 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %cur, align 4, !tbaa !2
  %95 = bitcast %class.btMultiBodyLinkCollider* %94 to %class.btCollisionObject*
  %call80 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %95)
  br i1 %call80, label %if.end82, label %if.then81

if.then81:                                        ; preds = %land.lhs.true79
  %96 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %cur, align 4, !tbaa !2
  store %class.btMultiBodyLinkCollider* %96, %class.btMultiBodyLinkCollider** %prev, align 4, !tbaa !2
  br label %if.end82

if.end82:                                         ; preds = %if.then81, %land.lhs.true79, %if.end77
  %97 = bitcast %class.btMultiBodyLinkCollider** %cur to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #10
  br label %for.inc83

for.inc83:                                        ; preds = %if.end82
  %98 = load i32, i32* %b, align 4, !tbaa !8
  %inc84 = add nsw i32 %98, 1
  store i32 %inc84, i32* %b, align 4, !tbaa !8
  br label %for.cond59

for.end85:                                        ; preds = %for.cond.cleanup62
  %99 = bitcast %class.btMultiBodyLinkCollider** %prev to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #10
  %100 = bitcast %class.btMultiBody** %body to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #10
  br label %for.inc86

for.inc86:                                        ; preds = %for.end85
  %101 = load i32, i32* %i50, align 4, !tbaa !8
  %inc87 = add nsw i32 %101, 1
  store i32 %inc87, i32* %i50, align 4, !tbaa !8
  br label %for.cond51

for.end88:                                        ; preds = %for.cond.cleanup54
  %102 = bitcast i32* %i89 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %102) #10
  store i32 0, i32* %i89, align 4, !tbaa !8
  br label %for.cond90

for.cond90:                                       ; preds = %for.inc110, %for.end88
  %103 = load i32, i32* %i89, align 4, !tbaa !8
  %m_multiBodyConstraints = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 2
  %call91 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.45* %m_multiBodyConstraints)
  %cmp92 = icmp slt i32 %103, %call91
  br i1 %cmp92, label %for.body94, label %for.cond.cleanup93

for.cond.cleanup93:                               ; preds = %for.cond90
  store i32 14, i32* %cleanup.dest.slot, align 4
  %104 = bitcast i32* %i89 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #10
  br label %for.end112

for.body94:                                       ; preds = %for.cond90
  %105 = bitcast %class.btMultiBodyConstraint** %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %105) #10
  %m_multiBodyConstraints95 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 2
  %106 = load i32, i32* %i89, align 4, !tbaa !8
  %call96 = call nonnull align 4 dereferenceable(4) %class.btMultiBodyConstraint** @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintEixEi(%class.btAlignedObjectArray.45* %m_multiBodyConstraints95, i32 %106)
  %107 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %call96, align 4, !tbaa !2
  store %class.btMultiBodyConstraint* %107, %class.btMultiBodyConstraint** %c, align 4, !tbaa !2
  %108 = bitcast i32* %tagA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %108) #10
  %109 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %c, align 4, !tbaa !2
  %110 = bitcast %class.btMultiBodyConstraint* %109 to i32 (%class.btMultiBodyConstraint*)***
  %vtable97 = load i32 (%class.btMultiBodyConstraint*)**, i32 (%class.btMultiBodyConstraint*)*** %110, align 4, !tbaa !15
  %vfn98 = getelementptr inbounds i32 (%class.btMultiBodyConstraint*)*, i32 (%class.btMultiBodyConstraint*)** %vtable97, i64 2
  %111 = load i32 (%class.btMultiBodyConstraint*)*, i32 (%class.btMultiBodyConstraint*)** %vfn98, align 4
  %call99 = call i32 %111(%class.btMultiBodyConstraint* %109)
  store i32 %call99, i32* %tagA, align 4, !tbaa !8
  %112 = bitcast i32* %tagB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %112) #10
  %113 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %c, align 4, !tbaa !2
  %114 = bitcast %class.btMultiBodyConstraint* %113 to i32 (%class.btMultiBodyConstraint*)***
  %vtable100 = load i32 (%class.btMultiBodyConstraint*)**, i32 (%class.btMultiBodyConstraint*)*** %114, align 4, !tbaa !15
  %vfn101 = getelementptr inbounds i32 (%class.btMultiBodyConstraint*)*, i32 (%class.btMultiBodyConstraint*)** %vtable100, i64 3
  %115 = load i32 (%class.btMultiBodyConstraint*)*, i32 (%class.btMultiBodyConstraint*)** %vfn101, align 4
  %call102 = call i32 %115(%class.btMultiBodyConstraint* %113)
  store i32 %call102, i32* %tagB, align 4, !tbaa !8
  %116 = load i32, i32* %tagA, align 4, !tbaa !8
  %cmp103 = icmp sge i32 %116, 0
  br i1 %cmp103, label %land.lhs.true104, label %if.end109

land.lhs.true104:                                 ; preds = %for.body94
  %117 = load i32, i32* %tagB, align 4, !tbaa !8
  %cmp105 = icmp sge i32 %117, 0
  br i1 %cmp105, label %if.then106, label %if.end109

if.then106:                                       ; preds = %land.lhs.true104
  %118 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %call107 = call %class.btSimulationIslandManager* @_ZN23btDiscreteDynamicsWorld26getSimulationIslandManagerEv(%class.btDiscreteDynamicsWorld* %118)
  %call108 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %call107)
  %119 = load i32, i32* %tagA, align 4, !tbaa !8
  %120 = load i32, i32* %tagB, align 4, !tbaa !8
  call void @_ZN11btUnionFind5uniteEii(%class.btUnionFind* %call108, i32 %119, i32 %120)
  br label %if.end109

if.end109:                                        ; preds = %if.then106, %land.lhs.true104, %for.body94
  %121 = bitcast i32* %tagB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #10
  %122 = bitcast i32* %tagA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #10
  %123 = bitcast %class.btMultiBodyConstraint** %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #10
  br label %for.inc110

for.inc110:                                       ; preds = %if.end109
  %124 = load i32, i32* %i89, align 4, !tbaa !8
  %inc111 = add nsw i32 %124, 1
  store i32 %inc111, i32* %i89, align 4, !tbaa !8
  br label %for.cond90

for.end112:                                       ; preds = %for.cond.cleanup93
  %125 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %call113 = call %class.btSimulationIslandManager* @_ZN23btDiscreteDynamicsWorld26getSimulationIslandManagerEv(%class.btDiscreteDynamicsWorld* %125)
  %126 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %call114 = call %class.btCollisionWorld* @_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv(%class.btDiscreteDynamicsWorld* %126)
  %127 = bitcast %class.btSimulationIslandManager* %call113 to void (%class.btSimulationIslandManager*, %class.btCollisionWorld*)***
  %vtable115 = load void (%class.btSimulationIslandManager*, %class.btCollisionWorld*)**, void (%class.btSimulationIslandManager*, %class.btCollisionWorld*)*** %127, align 4, !tbaa !15
  %vfn116 = getelementptr inbounds void (%class.btSimulationIslandManager*, %class.btCollisionWorld*)*, void (%class.btSimulationIslandManager*, %class.btCollisionWorld*)** %vtable115, i64 3
  %128 = load void (%class.btSimulationIslandManager*, %class.btCollisionWorld*)*, void (%class.btSimulationIslandManager*, %class.btCollisionWorld*)** %vfn116, align 4
  call void %128(%class.btSimulationIslandManager* %call113, %class.btCollisionWorld* %call114)
  %call117 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile) #10
  %129 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %129) #10
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

define linkonce_odr hidden %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* returned %this, i8* %name) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.CProfileSample*, align 4
  %name.addr = alloca i8*, align 4
  store %class.CProfileSample* %this, %class.CProfileSample** %this.addr, align 4, !tbaa !2
  store i8* %name, i8** %name.addr, align 4, !tbaa !2
  %this1 = load %class.CProfileSample*, %class.CProfileSample** %this.addr, align 4
  %0 = load i8*, i8** %name.addr, align 4, !tbaa !2
  call void @_ZN15CProfileManager13Start_ProfileEPKc(i8* %0)
  ret %class.CProfileSample* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btSimulationIslandManager* @_ZN23btDiscreteDynamicsWorld26getSimulationIslandManagerEv(%class.btDiscreteDynamicsWorld* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %m_islandManager = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 4
  %0 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %m_islandManager, align 4, !tbaa !22
  ret %class.btSimulationIslandManager* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btCollisionWorld* @_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv(%class.btDiscreteDynamicsWorld* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %0 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  ret %class.btCollisionWorld* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btDispatcher* @_ZN16btCollisionWorld13getDispatcherEv(%class.btCollisionWorld* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_dispatcher1 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 2
  %0 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4, !tbaa !32
  ret %class.btDispatcher* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !37
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.9* %this, i32 %n) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !38
  %1 = load i32, i32* %n.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %0, i32 %1
  ret %class.btPersistentManifold** %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_body0 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body0, align 4, !tbaa !39
  ret %class.btCollisionObject* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody1Ev(%class.btPersistentManifold* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_body1 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 3
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body1, align 4, !tbaa !41
  ret %class.btCollisionObject* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4, !tbaa !42
  %and = and i32 %0, 3
  %cmp = icmp ne i32 %and, 0
  ret i1 %cmp
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btSimulationIslandManager*, align 4
  store %class.btSimulationIslandManager* %this, %class.btSimulationIslandManager** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %this.addr, align 4
  %m_unionFind = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 1
  ret %class.btUnionFind* %m_unionFind
}

define linkonce_odr hidden void @_ZN11btUnionFind5uniteEii(%class.btUnionFind* %this, i32 %p, i32 %q) #0 comdat {
entry:
  %this.addr = alloca %class.btUnionFind*, align 4
  %p.addr = alloca i32, align 4
  %q.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btUnionFind* %this, %class.btUnionFind** %this.addr, align 4, !tbaa !2
  store i32 %p, i32* %p.addr, align 4, !tbaa !8
  store i32 %q, i32* %q.addr, align 4, !tbaa !8
  %this1 = load %class.btUnionFind*, %class.btUnionFind** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %p.addr, align 4, !tbaa !8
  %call = call i32 @_ZN11btUnionFind4findEi(%class.btUnionFind* %this1, i32 %1)
  store i32 %call, i32* %i, align 4, !tbaa !8
  %2 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #10
  %3 = load i32, i32* %q.addr, align 4, !tbaa !8
  %call2 = call i32 @_ZN11btUnionFind4findEi(%class.btUnionFind* %this1, i32 %3)
  store i32 %call2, i32* %j, align 4, !tbaa !8
  %4 = load i32, i32* %i, align 4, !tbaa !8
  %5 = load i32, i32* %j, align 4, !tbaa !8
  %cmp = icmp eq i32 %4, %5
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %6 = load i32, i32* %j, align 4, !tbaa !8
  %m_elements = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %7 = load i32, i32* %i, align 4, !tbaa !8
  %call3 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray.5* %m_elements, i32 %7)
  %m_id = getelementptr inbounds %struct.btElement, %struct.btElement* %call3, i32 0, i32 0
  store i32 %6, i32* %m_id, align 4, !tbaa !46
  %m_elements4 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %8 = load i32, i32* %i, align 4, !tbaa !8
  %call5 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray.5* %m_elements4, i32 %8)
  %m_sz = getelementptr inbounds %struct.btElement, %struct.btElement* %call5, i32 0, i32 1
  %9 = load i32, i32* %m_sz, align 4, !tbaa !48
  %m_elements6 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %10 = load i32, i32* %j, align 4, !tbaa !8
  %call7 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray.5* %m_elements6, i32 %10)
  %m_sz8 = getelementptr inbounds %struct.btElement, %struct.btElement* %call7, i32 0, i32 1
  %11 = load i32, i32* %m_sz8, align 4, !tbaa !48
  %add = add nsw i32 %11, %9
  store i32 %add, i32* %m_sz8, align 4, !tbaa !48
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %12 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #10
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #10
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_islandTag1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 13
  %0 = load i32, i32* %m_islandTag1, align 4, !tbaa !49
  ret i32 %0
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.0* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !50
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4, !tbaa !51
  %1 = load i32, i32* %n.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %0, i32 %1
  ret %class.btTypedConstraint** %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZNK17btTypedConstraint9isEnabledEv(%class.btTypedConstraint* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_isEnabled = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 5
  %0 = load i8, i8* %m_isEnabled, align 4, !tbaa !52, !range !54
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 8
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4, !tbaa !55
  ret %class.btRigidBody* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 9
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4, !tbaa !56
  ret %class.btRigidBody* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.21* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.21*, align 4
  store %class.btAlignedObjectArray.21* %this, %class.btAlignedObjectArray.21** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.21*, %class.btAlignedObjectArray.21** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.21* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !14
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btMultiBody** @_ZN20btAlignedObjectArrayIP11btMultiBodyEixEi(%class.btAlignedObjectArray.21* %this, i32 %n) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.21*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.21* %this, %class.btAlignedObjectArray.21** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.21*, %class.btAlignedObjectArray.21** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.21* %this1, i32 0, i32 4
  %0 = load %class.btMultiBody**, %class.btMultiBody*** %m_data, align 4, !tbaa !10
  %1 = load i32, i32* %n.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btMultiBody*, %class.btMultiBody** %0, i32 %1
  ret %class.btMultiBody** %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_baseCollider = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 0
  %0 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_baseCollider, align 4, !tbaa !57
  ret %class.btMultiBodyLinkCollider* %0
}

define linkonce_odr hidden i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %call = call i32 @_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv(%class.btAlignedObjectArray.24* %links)
  ret i32 %call
}

define linkonce_odr hidden nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %this, i32 %index) #0 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %index.addr = alloca i32, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !8
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %0 = load i32, i32* %index.addr, align 4, !tbaa !8
  %call = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray.24* %links, i32 %0)
  ret %struct.btMultibodyLink* %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.45* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.45*, align 4
  store %class.btAlignedObjectArray.45* %this, %class.btAlignedObjectArray.45** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.45*, %class.btAlignedObjectArray.45** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !69
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btMultiBodyConstraint** @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintEixEi(%class.btAlignedObjectArray.45* %this, i32 %n) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.45*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.45* %this, %class.btAlignedObjectArray.45** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.45*, %class.btAlignedObjectArray.45** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 4
  %0 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data, align 4, !tbaa !72
  %1 = load i32, i32* %n.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %0, i32 %1
  ret %class.btMultiBodyConstraint** %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.CProfileSample*, align 4
  store %class.CProfileSample* %this, %class.CProfileSample** %this.addr, align 4, !tbaa !2
  %this1 = load %class.CProfileSample*, %class.CProfileSample** %this.addr, align 4
  call void @_ZN15CProfileManager12Stop_ProfileEv()
  ret %class.CProfileSample* %this1
}

define hidden void @_ZN24btMultiBodyDynamicsWorld21updateActivationStateEf(%class.btMultiBodyDynamicsWorld* %this, float %timeStep) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  %timeStep.addr = alloca float, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %body = alloca %class.btMultiBody*, align 4
  %col = alloca %class.btMultiBodyLinkCollider*, align 4
  %b = alloca i32, align 4
  %col17 = alloca %class.btMultiBodyLinkCollider*, align 4
  %col25 = alloca %class.btMultiBodyLinkCollider*, align 4
  %b33 = alloca i32, align 4
  %col39 = alloca %class.btMultiBodyLinkCollider*, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4, !tbaa !2
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !73
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %0 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %0) #10
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([48 x i8], [48 x i8]* @.str.1, i32 0, i32 0))
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc53, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %m_multiBodies = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.21* %m_multiBodies)
  %cmp = icmp slt i32 %2, %call2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #10
  br label %for.end55

for.body:                                         ; preds = %for.cond
  %4 = bitcast %class.btMultiBody** %body to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #10
  %m_multiBodies3 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %call4 = call nonnull align 4 dereferenceable(4) %class.btMultiBody** @_ZN20btAlignedObjectArrayIP11btMultiBodyEixEi(%class.btAlignedObjectArray.21* %m_multiBodies3, i32 %5)
  %6 = load %class.btMultiBody*, %class.btMultiBody** %call4, align 4, !tbaa !2
  store %class.btMultiBody* %6, %class.btMultiBody** %body, align 4, !tbaa !2
  %7 = load %class.btMultiBody*, %class.btMultiBody** %body, align 4, !tbaa !2
  %tobool = icmp ne %class.btMultiBody* %7, null
  br i1 %tobool, label %if.then, label %if.end52

if.then:                                          ; preds = %for.body
  %8 = load %class.btMultiBody*, %class.btMultiBody** %body, align 4, !tbaa !2
  %9 = load float, float* %timeStep.addr, align 4, !tbaa !73
  call void @_ZN11btMultiBody29checkMotionAndSleepIfRequiredEf(%class.btMultiBody* %8, float %9)
  %10 = load %class.btMultiBody*, %class.btMultiBody** %body, align 4, !tbaa !2
  %call5 = call zeroext i1 @_ZNK11btMultiBody7isAwakeEv(%class.btMultiBody* %10)
  br i1 %call5, label %if.else, label %if.then6

if.then6:                                         ; preds = %if.then
  %11 = bitcast %class.btMultiBodyLinkCollider** %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #10
  %12 = load %class.btMultiBody*, %class.btMultiBody** %body, align 4, !tbaa !2
  %call7 = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %12)
  store %class.btMultiBodyLinkCollider* %call7, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %13 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %tobool8 = icmp ne %class.btMultiBodyLinkCollider* %13, null
  br i1 %tobool8, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then6
  %14 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %15 = bitcast %class.btMultiBodyLinkCollider* %14 to %class.btCollisionObject*
  %call9 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %15)
  %cmp10 = icmp eq i32 %call9, 1
  br i1 %cmp10, label %if.then11, label %if.end

if.then11:                                        ; preds = %land.lhs.true
  %16 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %17 = bitcast %class.btMultiBodyLinkCollider* %16 to %class.btCollisionObject*
  call void @_ZNK17btCollisionObject18setActivationStateEi(%class.btCollisionObject* %17, i32 3)
  %18 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %19 = bitcast %class.btMultiBodyLinkCollider* %18 to %class.btCollisionObject*
  call void @_ZN17btCollisionObject19setDeactivationTimeEf(%class.btCollisionObject* %19, float 0.000000e+00)
  br label %if.end

if.end:                                           ; preds = %if.then11, %land.lhs.true, %if.then6
  %20 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #10
  store i32 0, i32* %b, align 4, !tbaa !8
  br label %for.cond12

for.cond12:                                       ; preds = %for.inc, %if.end
  %21 = load i32, i32* %b, align 4, !tbaa !8
  %22 = load %class.btMultiBody*, %class.btMultiBody** %body, align 4, !tbaa !2
  %call13 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %22)
  %cmp14 = icmp slt i32 %21, %call13
  br i1 %cmp14, label %for.body16, label %for.cond.cleanup15

for.cond.cleanup15:                               ; preds = %for.cond12
  store i32 5, i32* %cleanup.dest.slot, align 4
  %23 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #10
  br label %for.end

for.body16:                                       ; preds = %for.cond12
  %24 = bitcast %class.btMultiBodyLinkCollider** %col17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #10
  %25 = load %class.btMultiBody*, %class.btMultiBody** %body, align 4, !tbaa !2
  %26 = load i32, i32* %b, align 4, !tbaa !8
  %call18 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %25, i32 %26)
  %m_collider = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call18, i32 0, i32 15
  %27 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider, align 4, !tbaa !17
  store %class.btMultiBodyLinkCollider* %27, %class.btMultiBodyLinkCollider** %col17, align 4, !tbaa !2
  %28 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col17, align 4, !tbaa !2
  %tobool19 = icmp ne %class.btMultiBodyLinkCollider* %28, null
  br i1 %tobool19, label %land.lhs.true20, label %if.end24

land.lhs.true20:                                  ; preds = %for.body16
  %29 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col17, align 4, !tbaa !2
  %30 = bitcast %class.btMultiBodyLinkCollider* %29 to %class.btCollisionObject*
  %call21 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %30)
  %cmp22 = icmp eq i32 %call21, 1
  br i1 %cmp22, label %if.then23, label %if.end24

if.then23:                                        ; preds = %land.lhs.true20
  %31 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col17, align 4, !tbaa !2
  %32 = bitcast %class.btMultiBodyLinkCollider* %31 to %class.btCollisionObject*
  call void @_ZNK17btCollisionObject18setActivationStateEi(%class.btCollisionObject* %32, i32 3)
  %33 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col17, align 4, !tbaa !2
  %34 = bitcast %class.btMultiBodyLinkCollider* %33 to %class.btCollisionObject*
  call void @_ZN17btCollisionObject19setDeactivationTimeEf(%class.btCollisionObject* %34, float 0.000000e+00)
  br label %if.end24

if.end24:                                         ; preds = %if.then23, %land.lhs.true20, %for.body16
  %35 = bitcast %class.btMultiBodyLinkCollider** %col17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #10
  br label %for.inc

for.inc:                                          ; preds = %if.end24
  %36 = load i32, i32* %b, align 4, !tbaa !8
  %inc = add nsw i32 %36, 1
  store i32 %inc, i32* %b, align 4, !tbaa !8
  br label %for.cond12

for.end:                                          ; preds = %for.cond.cleanup15
  %37 = bitcast %class.btMultiBodyLinkCollider** %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #10
  br label %if.end51

if.else:                                          ; preds = %if.then
  %38 = bitcast %class.btMultiBodyLinkCollider** %col25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #10
  %39 = load %class.btMultiBody*, %class.btMultiBody** %body, align 4, !tbaa !2
  %call26 = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %39)
  store %class.btMultiBodyLinkCollider* %call26, %class.btMultiBodyLinkCollider** %col25, align 4, !tbaa !2
  %40 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col25, align 4, !tbaa !2
  %tobool27 = icmp ne %class.btMultiBodyLinkCollider* %40, null
  br i1 %tobool27, label %land.lhs.true28, label %if.end32

land.lhs.true28:                                  ; preds = %if.else
  %41 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col25, align 4, !tbaa !2
  %42 = bitcast %class.btMultiBodyLinkCollider* %41 to %class.btCollisionObject*
  %call29 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %42)
  %cmp30 = icmp ne i32 %call29, 4
  br i1 %cmp30, label %if.then31, label %if.end32

if.then31:                                        ; preds = %land.lhs.true28
  %43 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col25, align 4, !tbaa !2
  %44 = bitcast %class.btMultiBodyLinkCollider* %43 to %class.btCollisionObject*
  call void @_ZNK17btCollisionObject18setActivationStateEi(%class.btCollisionObject* %44, i32 1)
  br label %if.end32

if.end32:                                         ; preds = %if.then31, %land.lhs.true28, %if.else
  %45 = bitcast i32* %b33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #10
  store i32 0, i32* %b33, align 4, !tbaa !8
  br label %for.cond34

for.cond34:                                       ; preds = %for.inc48, %if.end32
  %46 = load i32, i32* %b33, align 4, !tbaa !8
  %47 = load %class.btMultiBody*, %class.btMultiBody** %body, align 4, !tbaa !2
  %call35 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %47)
  %cmp36 = icmp slt i32 %46, %call35
  br i1 %cmp36, label %for.body38, label %for.cond.cleanup37

for.cond.cleanup37:                               ; preds = %for.cond34
  store i32 8, i32* %cleanup.dest.slot, align 4
  %48 = bitcast i32* %b33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #10
  br label %for.end50

for.body38:                                       ; preds = %for.cond34
  %49 = bitcast %class.btMultiBodyLinkCollider** %col39 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #10
  %50 = load %class.btMultiBody*, %class.btMultiBody** %body, align 4, !tbaa !2
  %51 = load i32, i32* %b33, align 4, !tbaa !8
  %call40 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %50, i32 %51)
  %m_collider41 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call40, i32 0, i32 15
  %52 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider41, align 4, !tbaa !17
  store %class.btMultiBodyLinkCollider* %52, %class.btMultiBodyLinkCollider** %col39, align 4, !tbaa !2
  %53 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col39, align 4, !tbaa !2
  %tobool42 = icmp ne %class.btMultiBodyLinkCollider* %53, null
  br i1 %tobool42, label %land.lhs.true43, label %if.end47

land.lhs.true43:                                  ; preds = %for.body38
  %54 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col39, align 4, !tbaa !2
  %55 = bitcast %class.btMultiBodyLinkCollider* %54 to %class.btCollisionObject*
  %call44 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %55)
  %cmp45 = icmp ne i32 %call44, 4
  br i1 %cmp45, label %if.then46, label %if.end47

if.then46:                                        ; preds = %land.lhs.true43
  %56 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col39, align 4, !tbaa !2
  %57 = bitcast %class.btMultiBodyLinkCollider* %56 to %class.btCollisionObject*
  call void @_ZNK17btCollisionObject18setActivationStateEi(%class.btCollisionObject* %57, i32 1)
  br label %if.end47

if.end47:                                         ; preds = %if.then46, %land.lhs.true43, %for.body38
  %58 = bitcast %class.btMultiBodyLinkCollider** %col39 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #10
  br label %for.inc48

for.inc48:                                        ; preds = %if.end47
  %59 = load i32, i32* %b33, align 4, !tbaa !8
  %inc49 = add nsw i32 %59, 1
  store i32 %inc49, i32* %b33, align 4, !tbaa !8
  br label %for.cond34

for.end50:                                        ; preds = %for.cond.cleanup37
  %60 = bitcast %class.btMultiBodyLinkCollider** %col25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #10
  br label %if.end51

if.end51:                                         ; preds = %for.end50, %for.end
  br label %if.end52

if.end52:                                         ; preds = %if.end51, %for.body
  %61 = bitcast %class.btMultiBody** %body to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #10
  br label %for.inc53

for.inc53:                                        ; preds = %if.end52
  %62 = load i32, i32* %i, align 4, !tbaa !8
  %inc54 = add nsw i32 %62, 1
  store i32 %inc54, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end55:                                        ; preds = %for.cond.cleanup
  %63 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %64 = load float, float* %timeStep.addr, align 4, !tbaa !73
  call void @_ZN23btDiscreteDynamicsWorld21updateActivationStateEf(%class.btDiscreteDynamicsWorld* %63, float %64)
  %call56 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile) #10
  %65 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %65) #10
  ret void
}

declare void @_ZN11btMultiBody29checkMotionAndSleepIfRequiredEf(%class.btMultiBody*, float) #5

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZNK11btMultiBody7isAwakeEv(%class.btMultiBody* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %awake = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 17
  %0 = load i8, i8* %awake, align 1, !tbaa !74, !range !54
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_activationState1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 15
  %0 = load i32, i32* %m_activationState1, align 4, !tbaa !75
  ret i32 %0
}

declare void @_ZNK17btCollisionObject18setActivationStateEi(%class.btCollisionObject*, i32) #5

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN17btCollisionObject19setDeactivationTimeEf(%class.btCollisionObject* %this, float %time) #3 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %time.addr = alloca float, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  store float %time, float* %time.addr, align 4, !tbaa !73
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %0 = load float, float* %time.addr, align 4, !tbaa !73
  %m_deactivationTime = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 16
  store float %0, float* %m_deactivationTime, align 4, !tbaa !76
  ret void
}

declare void @_ZN23btDiscreteDynamicsWorld21updateActivationStateEf(%class.btDiscreteDynamicsWorld*, float) unnamed_addr #5

define hidden %class.btMultiBodyDynamicsWorld* @_ZN24btMultiBodyDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP27btMultiBodyConstraintSolverP24btCollisionConfiguration(%class.btMultiBodyDynamicsWorld* returned %this, %class.btDispatcher* %dispatcher, %class.btBroadphaseInterface* %pairCache, %class.btMultiBodyConstraintSolver* %constraintSolver, %class.btCollisionConfiguration* %collisionConfiguration) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %pairCache.addr = alloca %class.btBroadphaseInterface*, align 4
  %constraintSolver.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  %collisionConfiguration.addr = alloca %class.btCollisionConfiguration*, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  store %class.btBroadphaseInterface* %pairCache, %class.btBroadphaseInterface** %pairCache.addr, align 4, !tbaa !2
  store %class.btMultiBodyConstraintSolver* %constraintSolver, %class.btMultiBodyConstraintSolver** %constraintSolver.addr, align 4, !tbaa !2
  store %class.btCollisionConfiguration* %collisionConfiguration, %class.btCollisionConfiguration** %collisionConfiguration.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %1 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %2 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %pairCache.addr, align 4, !tbaa !2
  %3 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %constraintSolver.addr, align 4, !tbaa !2
  %4 = bitcast %class.btMultiBodyConstraintSolver* %3 to %class.btConstraintSolver*
  %5 = load %class.btCollisionConfiguration*, %class.btCollisionConfiguration** %collisionConfiguration.addr, align 4, !tbaa !2
  %call = call %class.btDiscreteDynamicsWorld* @_ZN23btDiscreteDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration(%class.btDiscreteDynamicsWorld* %0, %class.btDispatcher* %1, %class.btBroadphaseInterface* %2, %class.btConstraintSolver* %4, %class.btCollisionConfiguration* %5)
  %6 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [51 x i8*] }, { [51 x i8*] }* @_ZTV24btMultiBodyDynamicsWorld, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %6, align 4, !tbaa !15
  %m_multiBodies = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.21* @_ZN20btAlignedObjectArrayIP11btMultiBodyEC2Ev(%class.btAlignedObjectArray.21* %m_multiBodies)
  %m_multiBodyConstraints = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 2
  %call3 = call %class.btAlignedObjectArray.45* @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintEC2Ev(%class.btAlignedObjectArray.45* %m_multiBodyConstraints)
  %m_sortedMultiBodyConstraints = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 3
  %call4 = call %class.btAlignedObjectArray.45* @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintEC2Ev(%class.btAlignedObjectArray.45* %m_sortedMultiBodyConstraints)
  %m_multiBodyConstraintSolver = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 4
  %7 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %constraintSolver.addr, align 4, !tbaa !2
  store %class.btMultiBodyConstraintSolver* %7, %class.btMultiBodyConstraintSolver** %m_multiBodyConstraintSolver, align 4, !tbaa !77
  %8 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %call5 = call nonnull align 4 dereferenceable(84) %struct.btContactSolverInfo* @_ZN15btDynamicsWorld13getSolverInfoEv(%class.btDynamicsWorld* %8)
  %9 = bitcast %struct.btContactSolverInfo* %call5 to %struct.btContactSolverInfoData*
  %m_splitImpulse = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %9, i32 0, i32 11
  store i32 0, i32* %m_splitImpulse, align 4, !tbaa !79
  %10 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %call6 = call nonnull align 4 dereferenceable(84) %struct.btContactSolverInfo* @_ZN15btDynamicsWorld13getSolverInfoEv(%class.btDynamicsWorld* %10)
  %11 = bitcast %struct.btContactSolverInfo* %call6 to %struct.btContactSolverInfoData*
  %m_solverMode = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %11, i32 0, i32 16
  %12 = load i32, i32* %m_solverMode, align 4, !tbaa !81
  %or = or i32 %12, 16
  store i32 %or, i32* %m_solverMode, align 4, !tbaa !81
  %call7 = call noalias nonnull i8* @_Znwm(i32 116) #11
  %13 = bitcast i8* %call7 to %struct.MultiBodyInplaceSolverIslandCallback*
  %14 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %constraintSolver.addr, align 4, !tbaa !2
  %15 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %call8 = call %struct.MultiBodyInplaceSolverIslandCallback* @_ZN36MultiBodyInplaceSolverIslandCallbackC2EP27btMultiBodyConstraintSolverP12btDispatcher(%struct.MultiBodyInplaceSolverIslandCallback* %13, %class.btMultiBodyConstraintSolver* %14, %class.btDispatcher* %15)
  %m_solverMultiBodyIslandCallback = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 5
  store %struct.MultiBodyInplaceSolverIslandCallback* %13, %struct.MultiBodyInplaceSolverIslandCallback** %m_solverMultiBodyIslandCallback, align 4, !tbaa !82
  ret %class.btMultiBodyDynamicsWorld* %this1
}

declare %class.btDiscreteDynamicsWorld* @_ZN23btDiscreteDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration(%class.btDiscreteDynamicsWorld* returned, %class.btDispatcher*, %class.btBroadphaseInterface*, %class.btConstraintSolver*, %class.btCollisionConfiguration*) unnamed_addr #5

define linkonce_odr hidden %class.btAlignedObjectArray.21* @_ZN20btAlignedObjectArrayIP11btMultiBodyEC2Ev(%class.btAlignedObjectArray.21* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.21*, align 4
  store %class.btAlignedObjectArray.21* %this, %class.btAlignedObjectArray.21** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.21*, %class.btAlignedObjectArray.21** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.21* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.22* @_ZN18btAlignedAllocatorIP11btMultiBodyLj16EEC2Ev(%class.btAlignedAllocator.22* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP11btMultiBodyE4initEv(%class.btAlignedObjectArray.21* %this1)
  ret %class.btAlignedObjectArray.21* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.45* @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintEC2Ev(%class.btAlignedObjectArray.45* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.45*, align 4
  store %class.btAlignedObjectArray.45* %this, %class.btAlignedObjectArray.45** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.45*, %class.btAlignedObjectArray.45** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.46* @_ZN18btAlignedAllocatorIP21btMultiBodyConstraintLj16EEC2Ev(%class.btAlignedAllocator.46* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE4initEv(%class.btAlignedObjectArray.45* %this1)
  ret %class.btAlignedObjectArray.45* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(84) %struct.btContactSolverInfo* @_ZN15btDynamicsWorld13getSolverInfoEv(%class.btDynamicsWorld* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btDynamicsWorld*, align 4
  store %class.btDynamicsWorld* %this, %class.btDynamicsWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %this.addr, align 4
  %m_solverInfo = getelementptr inbounds %class.btDynamicsWorld, %class.btDynamicsWorld* %this1, i32 0, i32 4
  ret %struct.btContactSolverInfo* %m_solverInfo
}

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #6

define linkonce_odr hidden %struct.MultiBodyInplaceSolverIslandCallback* @_ZN36MultiBodyInplaceSolverIslandCallbackC2EP27btMultiBodyConstraintSolverP12btDispatcher(%struct.MultiBodyInplaceSolverIslandCallback* returned %this, %class.btMultiBodyConstraintSolver* %solver, %class.btDispatcher* %dispatcher) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %struct.MultiBodyInplaceSolverIslandCallback*, align 4
  %solver.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %struct.MultiBodyInplaceSolverIslandCallback* %this, %struct.MultiBodyInplaceSolverIslandCallback** %this.addr, align 4, !tbaa !2
  store %class.btMultiBodyConstraintSolver* %solver, %class.btMultiBodyConstraintSolver** %solver.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %struct.MultiBodyInplaceSolverIslandCallback*, %struct.MultiBodyInplaceSolverIslandCallback** %this.addr, align 4
  %0 = bitcast %struct.MultiBodyInplaceSolverIslandCallback* %this1 to %"struct.btSimulationIslandManager::IslandCallback"*
  %call = call %"struct.btSimulationIslandManager::IslandCallback"* @_ZN25btSimulationIslandManager14IslandCallbackC2Ev(%"struct.btSimulationIslandManager::IslandCallback"* %0) #10
  %1 = bitcast %struct.MultiBodyInplaceSolverIslandCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV36MultiBodyInplaceSolverIslandCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !15
  %m_solverInfo = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 1
  store %struct.btContactSolverInfo* null, %struct.btContactSolverInfo** %m_solverInfo, align 4, !tbaa !83
  %m_solver = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 2
  %2 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %solver.addr, align 4, !tbaa !2
  store %class.btMultiBodyConstraintSolver* %2, %class.btMultiBodyConstraintSolver** %m_solver, align 4, !tbaa !85
  %m_multiBodySortedConstraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 3
  store %class.btMultiBodyConstraint** null, %class.btMultiBodyConstraint*** %m_multiBodySortedConstraints, align 4, !tbaa !86
  %m_numConstraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 6
  store i32 0, i32* %m_numConstraints, align 4, !tbaa !87
  %m_debugDrawer = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 7
  store %class.btIDebugDraw* null, %class.btIDebugDraw** %m_debugDrawer, align 4, !tbaa !88
  %m_dispatcher = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 8
  %3 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  store %class.btDispatcher* %3, %class.btDispatcher** %m_dispatcher, align 4, !tbaa !89
  %m_bodies = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 9
  %call2 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP17btCollisionObjectEC2Ev(%class.btAlignedObjectArray* %m_bodies)
  %m_manifolds = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 10
  %call3 = call %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray.9* %m_manifolds)
  %m_constraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 11
  %call4 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIP17btTypedConstraintEC2Ev(%class.btAlignedObjectArray.0* %m_constraints)
  %m_multiBodyConstraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 12
  %call5 = call %class.btAlignedObjectArray.45* @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintEC2Ev(%class.btAlignedObjectArray.45* %m_multiBodyConstraints)
  ret %struct.MultiBodyInplaceSolverIslandCallback* %this1
}

; Function Attrs: nounwind
define hidden %class.btMultiBodyDynamicsWorld* @_ZN24btMultiBodyDynamicsWorldD2Ev(%class.btMultiBodyDynamicsWorld* returned %this) unnamed_addr #3 {
entry:
  %retval = alloca %class.btMultiBodyDynamicsWorld*, align 4
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  store %class.btMultiBodyDynamicsWorld* %this1, %class.btMultiBodyDynamicsWorld** %retval, align 4
  %0 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [51 x i8*] }, { [51 x i8*] }* @_ZTV24btMultiBodyDynamicsWorld, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !15
  %m_solverMultiBodyIslandCallback = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 5
  %1 = load %struct.MultiBodyInplaceSolverIslandCallback*, %struct.MultiBodyInplaceSolverIslandCallback** %m_solverMultiBodyIslandCallback, align 4, !tbaa !82
  %isnull = icmp eq %struct.MultiBodyInplaceSolverIslandCallback* %1, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %2 = bitcast %struct.MultiBodyInplaceSolverIslandCallback* %1 to void (%struct.MultiBodyInplaceSolverIslandCallback*)***
  %vtable = load void (%struct.MultiBodyInplaceSolverIslandCallback*)**, void (%struct.MultiBodyInplaceSolverIslandCallback*)*** %2, align 4, !tbaa !15
  %vfn = getelementptr inbounds void (%struct.MultiBodyInplaceSolverIslandCallback*)*, void (%struct.MultiBodyInplaceSolverIslandCallback*)** %vtable, i64 1
  %3 = load void (%struct.MultiBodyInplaceSolverIslandCallback*)*, void (%struct.MultiBodyInplaceSolverIslandCallback*)** %vfn, align 4
  call void %3(%struct.MultiBodyInplaceSolverIslandCallback* %1) #10
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  %m_sortedMultiBodyConstraints = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 3
  %call = call %class.btAlignedObjectArray.45* @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintED2Ev(%class.btAlignedObjectArray.45* %m_sortedMultiBodyConstraints) #10
  %m_multiBodyConstraints = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 2
  %call2 = call %class.btAlignedObjectArray.45* @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintED2Ev(%class.btAlignedObjectArray.45* %m_multiBodyConstraints) #10
  %m_multiBodies = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %call3 = call %class.btAlignedObjectArray.21* @_ZN20btAlignedObjectArrayIP11btMultiBodyED2Ev(%class.btAlignedObjectArray.21* %m_multiBodies) #10
  %4 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %call4 = call %class.btDiscreteDynamicsWorld* @_ZN23btDiscreteDynamicsWorldD2Ev(%class.btDiscreteDynamicsWorld* %4) #10
  %5 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %retval, align 4
  ret %class.btMultiBodyDynamicsWorld* %5
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.45* @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintED2Ev(%class.btAlignedObjectArray.45* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.45*, align 4
  store %class.btAlignedObjectArray.45* %this, %class.btAlignedObjectArray.45** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.45*, %class.btAlignedObjectArray.45** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE5clearEv(%class.btAlignedObjectArray.45* %this1)
  ret %class.btAlignedObjectArray.45* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.21* @_ZN20btAlignedObjectArrayIP11btMultiBodyED2Ev(%class.btAlignedObjectArray.21* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.21*, align 4
  store %class.btAlignedObjectArray.21* %this, %class.btAlignedObjectArray.21** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.21*, %class.btAlignedObjectArray.21** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP11btMultiBodyE5clearEv(%class.btAlignedObjectArray.21* %this1)
  ret %class.btAlignedObjectArray.21* %this1
}

; Function Attrs: nounwind
declare %class.btDiscreteDynamicsWorld* @_ZN23btDiscreteDynamicsWorldD2Ev(%class.btDiscreteDynamicsWorld* returned) unnamed_addr #7

; Function Attrs: nounwind
define hidden void @_ZN24btMultiBodyDynamicsWorldD0Ev(%class.btMultiBodyDynamicsWorld* %this) unnamed_addr #3 {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %call = call %class.btMultiBodyDynamicsWorld* @_ZN24btMultiBodyDynamicsWorldD1Ev(%class.btMultiBodyDynamicsWorld* %this1) #10
  %0 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to i8*
  call void @_ZN23btDiscreteDynamicsWorlddlEPv(i8* %0) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN23btDiscreteDynamicsWorlddlEPv(i8* %ptr) #4 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

define hidden void @_ZN24btMultiBodyDynamicsWorld16solveConstraintsER19btContactSolverInfo(%class.btMultiBodyDynamicsWorld* %this, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %solverInfo) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  %solverInfo.addr = alloca %struct.btContactSolverInfo*, align 4
  %scratch_r = alloca %class.btAlignedObjectArray.32, align 4
  %scratch_v = alloca %class.btAlignedObjectArray.36, align 4
  %scratch_m = alloca %class.btAlignedObjectArray.40, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %ref.tmp = alloca %class.btTypedConstraint*, align 4
  %i = alloca i32, align 4
  %ref.tmp12 = alloca %class.btSortConstraintOnIslandPredicate2, align 1
  %constraintsPtr = alloca %class.btTypedConstraint**, align 4
  %ref.tmp19 = alloca %class.btMultiBodyConstraint*, align 4
  %ref.tmp33 = alloca %class.btSortMultiBodyConstraintOnIslandPredicate, align 1
  %sortedMultiBodyConstraints = alloca %class.btMultiBodyConstraint**, align 4
  %__profile63 = alloca %class.CProfileSample, align 1
  %i65 = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %bod = alloca %class.btMultiBody*, align 4
  %isSleeping = alloca i8, align 1
  %b = alloca i32, align 4
  %ref.tmp97 = alloca float, align 4
  %ref.tmp100 = alloca %class.btVector3, align 4
  %ref.tmp104 = alloca %class.btMatrix3x3, align 4
  %ref.tmp106 = alloca %class.btVector3, align 4
  %ref.tmp107 = alloca float, align 4
  %j = alloca i32, align 4
  %ref.tmp114 = alloca %class.btVector3, align 4
  %ref.tmp116 = alloca float, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4, !tbaa !2
  store %struct.btContactSolverInfo* %solverInfo, %struct.btContactSolverInfo** %solverInfo.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %0 = bitcast %class.btAlignedObjectArray.32* %scratch_r to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %0) #10
  %call = call %class.btAlignedObjectArray.32* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray.32* %scratch_r)
  %1 = bitcast %class.btAlignedObjectArray.36* %scratch_v to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %1) #10
  %call2 = call %class.btAlignedObjectArray.36* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.36* %scratch_v)
  %2 = bitcast %class.btAlignedObjectArray.40* %scratch_m to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %2) #10
  %call3 = call %class.btAlignedObjectArray.40* @_ZN20btAlignedObjectArrayI11btMatrix3x3EC2Ev(%class.btAlignedObjectArray.40* %scratch_m)
  %3 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %3) #10
  %call4 = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.2, i32 0, i32 0))
  %4 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_sortedConstraints = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %4, i32 0, i32 1
  %5 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_constraints = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %5, i32 0, i32 5
  %call5 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.0* %m_constraints)
  %6 = bitcast %class.btTypedConstraint** %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #10
  store %class.btTypedConstraint* null, %class.btTypedConstraint** %ref.tmp, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE6resizeEiRKS1_(%class.btAlignedObjectArray.0* %m_sortedConstraints, i32 %call5, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %ref.tmp)
  %7 = bitcast %class.btTypedConstraint** %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #10
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %10 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %11 = bitcast %class.btDiscreteDynamicsWorld* %10 to i32 (%class.btDiscreteDynamicsWorld*)***
  %vtable = load i32 (%class.btDiscreteDynamicsWorld*)**, i32 (%class.btDiscreteDynamicsWorld*)*** %11, align 4, !tbaa !15
  %vfn = getelementptr inbounds i32 (%class.btDiscreteDynamicsWorld*)*, i32 (%class.btDiscreteDynamicsWorld*)** %vtable, i64 26
  %12 = load i32 (%class.btDiscreteDynamicsWorld*)*, i32 (%class.btDiscreteDynamicsWorld*)** %vfn, align 4
  %call6 = call i32 %12(%class.btDiscreteDynamicsWorld* %10)
  %cmp = icmp slt i32 %9, %call6
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %13 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_constraints7 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %13, i32 0, i32 5
  %14 = load i32, i32* %i, align 4, !tbaa !8
  %call8 = call nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray.0* %m_constraints7, i32 %14)
  %15 = load %class.btTypedConstraint*, %class.btTypedConstraint** %call8, align 4, !tbaa !2
  %16 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_sortedConstraints9 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %16, i32 0, i32 1
  %17 = load i32, i32* %i, align 4, !tbaa !8
  %call10 = call nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray.0* %m_sortedConstraints9, i32 %17)
  store %class.btTypedConstraint* %15, %class.btTypedConstraint** %call10, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %18 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %18, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %19 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_sortedConstraints11 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %19, i32 0, i32 1
  %20 = bitcast %class.btSortConstraintOnIslandPredicate2* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %20) #10
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9quickSortI34btSortConstraintOnIslandPredicate2EEvRKT_(%class.btAlignedObjectArray.0* %m_sortedConstraints11, %class.btSortConstraintOnIslandPredicate2* nonnull align 1 dereferenceable(1) %ref.tmp12)
  %21 = bitcast %class.btSortConstraintOnIslandPredicate2* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %21) #10
  %22 = bitcast %class.btTypedConstraint*** %constraintsPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #10
  %23 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %24 = bitcast %class.btDiscreteDynamicsWorld* %23 to i32 (%class.btDiscreteDynamicsWorld*)***
  %vtable13 = load i32 (%class.btDiscreteDynamicsWorld*)**, i32 (%class.btDiscreteDynamicsWorld*)*** %24, align 4, !tbaa !15
  %vfn14 = getelementptr inbounds i32 (%class.btDiscreteDynamicsWorld*)*, i32 (%class.btDiscreteDynamicsWorld*)** %vtable13, i64 26
  %25 = load i32 (%class.btDiscreteDynamicsWorld*)*, i32 (%class.btDiscreteDynamicsWorld*)** %vfn14, align 4
  %call15 = call i32 %25(%class.btDiscreteDynamicsWorld* %23)
  %tobool = icmp ne i32 %call15, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.end
  %26 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_sortedConstraints16 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %26, i32 0, i32 1
  %call17 = call nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray.0* %m_sortedConstraints16, i32 0)
  br label %cond.end

cond.false:                                       ; preds = %for.end
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %class.btTypedConstraint** [ %call17, %cond.true ], [ null, %cond.false ]
  store %class.btTypedConstraint** %cond, %class.btTypedConstraint*** %constraintsPtr, align 4, !tbaa !2
  %m_sortedMultiBodyConstraints = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 3
  %m_multiBodyConstraints = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 2
  %call18 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.45* %m_multiBodyConstraints)
  %27 = bitcast %class.btMultiBodyConstraint** %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #10
  store %class.btMultiBodyConstraint* null, %class.btMultiBodyConstraint** %ref.tmp19, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE6resizeEiRKS1_(%class.btAlignedObjectArray.45* %m_sortedMultiBodyConstraints, i32 %call18, %class.btMultiBodyConstraint** nonnull align 4 dereferenceable(4) %ref.tmp19)
  %28 = bitcast %class.btMultiBodyConstraint** %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #10
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond20

for.cond20:                                       ; preds = %for.inc29, %cond.end
  %29 = load i32, i32* %i, align 4, !tbaa !8
  %m_multiBodyConstraints21 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 2
  %call22 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.45* %m_multiBodyConstraints21)
  %cmp23 = icmp slt i32 %29, %call22
  br i1 %cmp23, label %for.body24, label %for.end31

for.body24:                                       ; preds = %for.cond20
  %m_multiBodyConstraints25 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 2
  %30 = load i32, i32* %i, align 4, !tbaa !8
  %call26 = call nonnull align 4 dereferenceable(4) %class.btMultiBodyConstraint** @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintEixEi(%class.btAlignedObjectArray.45* %m_multiBodyConstraints25, i32 %30)
  %31 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %call26, align 4, !tbaa !2
  %m_sortedMultiBodyConstraints27 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 3
  %32 = load i32, i32* %i, align 4, !tbaa !8
  %call28 = call nonnull align 4 dereferenceable(4) %class.btMultiBodyConstraint** @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintEixEi(%class.btAlignedObjectArray.45* %m_sortedMultiBodyConstraints27, i32 %32)
  store %class.btMultiBodyConstraint* %31, %class.btMultiBodyConstraint** %call28, align 4, !tbaa !2
  br label %for.inc29

for.inc29:                                        ; preds = %for.body24
  %33 = load i32, i32* %i, align 4, !tbaa !8
  %inc30 = add nsw i32 %33, 1
  store i32 %inc30, i32* %i, align 4, !tbaa !8
  br label %for.cond20

for.end31:                                        ; preds = %for.cond20
  %m_sortedMultiBodyConstraints32 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 3
  %34 = bitcast %class.btSortMultiBodyConstraintOnIslandPredicate* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %34) #10
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE9quickSortI42btSortMultiBodyConstraintOnIslandPredicateEEvRKT_(%class.btAlignedObjectArray.45* %m_sortedMultiBodyConstraints32, %class.btSortMultiBodyConstraintOnIslandPredicate* nonnull align 1 dereferenceable(1) %ref.tmp33)
  %35 = bitcast %class.btSortMultiBodyConstraintOnIslandPredicate* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %35) #10
  %36 = bitcast %class.btMultiBodyConstraint*** %sortedMultiBodyConstraints to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #10
  %m_sortedMultiBodyConstraints34 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 3
  %call35 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.45* %m_sortedMultiBodyConstraints34)
  %tobool36 = icmp ne i32 %call35, 0
  br i1 %tobool36, label %cond.true37, label %cond.false40

cond.true37:                                      ; preds = %for.end31
  %m_sortedMultiBodyConstraints38 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 3
  %call39 = call nonnull align 4 dereferenceable(4) %class.btMultiBodyConstraint** @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintEixEi(%class.btAlignedObjectArray.45* %m_sortedMultiBodyConstraints38, i32 0)
  br label %cond.end41

cond.false40:                                     ; preds = %for.end31
  br label %cond.end41

cond.end41:                                       ; preds = %cond.false40, %cond.true37
  %cond42 = phi %class.btMultiBodyConstraint** [ %call39, %cond.true37 ], [ null, %cond.false40 ]
  store %class.btMultiBodyConstraint** %cond42, %class.btMultiBodyConstraint*** %sortedMultiBodyConstraints, align 4, !tbaa !2
  %m_solverMultiBodyIslandCallback = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 5
  %37 = load %struct.MultiBodyInplaceSolverIslandCallback*, %struct.MultiBodyInplaceSolverIslandCallback** %m_solverMultiBodyIslandCallback, align 4, !tbaa !82
  %38 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %solverInfo.addr, align 4, !tbaa !2
  %39 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraintsPtr, align 4, !tbaa !2
  %40 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_sortedConstraints43 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %40, i32 0, i32 1
  %call44 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.0* %m_sortedConstraints43)
  %41 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %sortedMultiBodyConstraints, align 4, !tbaa !2
  %m_sortedMultiBodyConstraints45 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 3
  %call46 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.45* %m_sortedMultiBodyConstraints45)
  %42 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btCollisionWorld*
  %43 = bitcast %class.btCollisionWorld* %42 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable47 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %43, align 4, !tbaa !15
  %vfn48 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable47, i64 5
  %44 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn48, align 4
  %call49 = call %class.btIDebugDraw* %44(%class.btCollisionWorld* %42)
  call void @_ZN36MultiBodyInplaceSolverIslandCallback5setupEP19btContactSolverInfoPP17btTypedConstraintiPP21btMultiBodyConstraintiP12btIDebugDraw(%struct.MultiBodyInplaceSolverIslandCallback* %37, %struct.btContactSolverInfo* %38, %class.btTypedConstraint** %39, i32 %call44, %class.btMultiBodyConstraint** %41, i32 %call46, %class.btIDebugDraw* %call49)
  %45 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_constraintSolver = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %45, i32 0, i32 3
  %46 = load %class.btConstraintSolver*, %class.btConstraintSolver** %m_constraintSolver, align 4, !tbaa !90
  %47 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %call50 = call %class.btCollisionWorld* @_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv(%class.btDiscreteDynamicsWorld* %47)
  %call51 = call i32 @_ZNK16btCollisionWorld22getNumCollisionObjectsEv(%class.btCollisionWorld* %call50)
  %48 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %call52 = call %class.btCollisionWorld* @_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv(%class.btDiscreteDynamicsWorld* %48)
  %call53 = call %class.btDispatcher* @_ZN16btCollisionWorld13getDispatcherEv(%class.btCollisionWorld* %call52)
  %49 = bitcast %class.btDispatcher* %call53 to i32 (%class.btDispatcher*)***
  %vtable54 = load i32 (%class.btDispatcher*)**, i32 (%class.btDispatcher*)*** %49, align 4, !tbaa !15
  %vfn55 = getelementptr inbounds i32 (%class.btDispatcher*)*, i32 (%class.btDispatcher*)** %vtable54, i64 9
  %50 = load i32 (%class.btDispatcher*)*, i32 (%class.btDispatcher*)** %vfn55, align 4
  %call56 = call i32 %50(%class.btDispatcher* %call53)
  %51 = bitcast %class.btConstraintSolver* %46 to void (%class.btConstraintSolver*, i32, i32)***
  %vtable57 = load void (%class.btConstraintSolver*, i32, i32)**, void (%class.btConstraintSolver*, i32, i32)*** %51, align 4, !tbaa !15
  %vfn58 = getelementptr inbounds void (%class.btConstraintSolver*, i32, i32)*, void (%class.btConstraintSolver*, i32, i32)** %vtable57, i64 2
  %52 = load void (%class.btConstraintSolver*, i32, i32)*, void (%class.btConstraintSolver*, i32, i32)** %vfn58, align 4
  call void %52(%class.btConstraintSolver* %46, i32 %call51, i32 %call56)
  %53 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_islandManager = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %53, i32 0, i32 4
  %54 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %m_islandManager, align 4, !tbaa !22
  %55 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %call59 = call %class.btCollisionWorld* @_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv(%class.btDiscreteDynamicsWorld* %55)
  %call60 = call %class.btDispatcher* @_ZN16btCollisionWorld13getDispatcherEv(%class.btCollisionWorld* %call59)
  %56 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %call61 = call %class.btCollisionWorld* @_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv(%class.btDiscreteDynamicsWorld* %56)
  %m_solverMultiBodyIslandCallback62 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 5
  %57 = load %struct.MultiBodyInplaceSolverIslandCallback*, %struct.MultiBodyInplaceSolverIslandCallback** %m_solverMultiBodyIslandCallback62, align 4, !tbaa !82
  %58 = bitcast %struct.MultiBodyInplaceSolverIslandCallback* %57 to %"struct.btSimulationIslandManager::IslandCallback"*
  call void @_ZN25btSimulationIslandManager22buildAndProcessIslandsEP12btDispatcherP16btCollisionWorldPNS_14IslandCallbackE(%class.btSimulationIslandManager* %54, %class.btDispatcher* %call60, %class.btCollisionWorld* %call61, %"struct.btSimulationIslandManager::IslandCallback"* %58)
  %59 = bitcast %class.CProfileSample* %__profile63 to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %59) #10
  %call64 = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile63, i8* getelementptr inbounds ([40 x i8], [40 x i8]* @.str.3, i32 0, i32 0))
  %60 = bitcast i32* %i65 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #10
  store i32 0, i32* %i65, align 4, !tbaa !8
  br label %for.cond66

for.cond66:                                       ; preds = %for.inc122, %cond.end41
  %61 = load i32, i32* %i65, align 4, !tbaa !8
  %m_multiBodies = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %call67 = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.21* %m_multiBodies)
  %cmp68 = icmp slt i32 %61, %call67
  br i1 %cmp68, label %for.body69, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond66
  store i32 8, i32* %cleanup.dest.slot, align 4
  %62 = bitcast i32* %i65 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #10
  br label %for.end124

for.body69:                                       ; preds = %for.cond66
  %63 = bitcast %class.btMultiBody** %bod to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #10
  %m_multiBodies70 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %64 = load i32, i32* %i65, align 4, !tbaa !8
  %call71 = call nonnull align 4 dereferenceable(4) %class.btMultiBody** @_ZN20btAlignedObjectArrayIP11btMultiBodyEixEi(%class.btAlignedObjectArray.21* %m_multiBodies70, i32 %64)
  %65 = load %class.btMultiBody*, %class.btMultiBody** %call71, align 4, !tbaa !2
  store %class.btMultiBody* %65, %class.btMultiBody** %bod, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %isSleeping) #10
  store i8 0, i8* %isSleeping, align 1, !tbaa !91
  %66 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %call72 = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %66)
  %tobool73 = icmp ne %class.btMultiBodyLinkCollider* %call72, null
  br i1 %tobool73, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body69
  %67 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %call74 = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %67)
  %68 = bitcast %class.btMultiBodyLinkCollider* %call74 to %class.btCollisionObject*
  %call75 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %68)
  %cmp76 = icmp eq i32 %call75, 2
  br i1 %cmp76, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  store i8 1, i8* %isSleeping, align 1, !tbaa !91
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %for.body69
  %69 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #10
  store i32 0, i32* %b, align 4, !tbaa !8
  br label %for.cond77

for.cond77:                                       ; preds = %for.inc91, %if.end
  %70 = load i32, i32* %b, align 4, !tbaa !8
  %71 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %call78 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %71)
  %cmp79 = icmp slt i32 %70, %call78
  br i1 %cmp79, label %for.body81, label %for.cond.cleanup80

for.cond.cleanup80:                               ; preds = %for.cond77
  store i32 11, i32* %cleanup.dest.slot, align 4
  %72 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #10
  br label %for.end93

for.body81:                                       ; preds = %for.cond77
  %73 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %74 = load i32, i32* %b, align 4, !tbaa !8
  %call82 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %73, i32 %74)
  %m_collider = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call82, i32 0, i32 15
  %75 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider, align 4, !tbaa !17
  %tobool83 = icmp ne %class.btMultiBodyLinkCollider* %75, null
  br i1 %tobool83, label %land.lhs.true84, label %if.end90

land.lhs.true84:                                  ; preds = %for.body81
  %76 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %77 = load i32, i32* %b, align 4, !tbaa !8
  %call85 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %76, i32 %77)
  %m_collider86 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call85, i32 0, i32 15
  %78 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider86, align 4, !tbaa !17
  %79 = bitcast %class.btMultiBodyLinkCollider* %78 to %class.btCollisionObject*
  %call87 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %79)
  %cmp88 = icmp eq i32 %call87, 2
  br i1 %cmp88, label %if.then89, label %if.end90

if.then89:                                        ; preds = %land.lhs.true84
  store i8 1, i8* %isSleeping, align 1, !tbaa !91
  br label %if.end90

if.end90:                                         ; preds = %if.then89, %land.lhs.true84, %for.body81
  br label %for.inc91

for.inc91:                                        ; preds = %if.end90
  %80 = load i32, i32* %b, align 4, !tbaa !8
  %inc92 = add nsw i32 %80, 1
  store i32 %inc92, i32* %b, align 4, !tbaa !8
  br label %for.cond77

for.end93:                                        ; preds = %for.cond.cleanup80
  %81 = load i8, i8* %isSleeping, align 1, !tbaa !91, !range !54
  %tobool94 = trunc i8 %81 to i1
  br i1 %tobool94, label %if.end121, label %if.then95

if.then95:                                        ; preds = %for.end93
  %82 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %call96 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %82)
  %add = add nsw i32 %call96, 1
  %83 = bitcast float* %ref.tmp97 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %83) #10
  store float 0.000000e+00, float* %ref.tmp97, align 4, !tbaa !73
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.32* %scratch_r, i32 %add, float* nonnull align 4 dereferenceable(4) %ref.tmp97)
  %84 = bitcast float* %ref.tmp97 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #10
  %85 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %call98 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %85)
  %add99 = add nsw i32 %call98, 1
  %86 = bitcast %class.btVector3* %ref.tmp100 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %86) #10
  %call101 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp100)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.36* %scratch_v, i32 %add99, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp100)
  %87 = bitcast %class.btVector3* %ref.tmp100 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %87) #10
  %88 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %call102 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %88)
  %add103 = add nsw i32 %call102, 1
  %89 = bitcast %class.btMatrix3x3* %ref.tmp104 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %89) #10
  %call105 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %ref.tmp104)
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E6resizeEiRKS0_(%class.btAlignedObjectArray.40* %scratch_m, i32 %add103, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp104)
  %90 = bitcast %class.btMatrix3x3* %ref.tmp104 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %90) #10
  %91 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  call void @_ZN11btMultiBody21clearForcesAndTorquesEv(%class.btMultiBody* %91)
  %92 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %93 = bitcast %class.btVector3* %ref.tmp106 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %93) #10
  %94 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_gravity = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %94, i32 0, i32 7
  %95 = bitcast float* %ref.tmp107 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %95) #10
  %96 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %call108 = call float @_ZNK11btMultiBody11getBaseMassEv(%class.btMultiBody* %96)
  store float %call108, float* %ref.tmp107, align 4, !tbaa !73
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp106, %class.btVector3* nonnull align 4 dereferenceable(16) %m_gravity, float* nonnull align 4 dereferenceable(4) %ref.tmp107)
  call void @_ZN11btMultiBody12addBaseForceERK9btVector3(%class.btMultiBody* %92, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp106)
  %97 = bitcast float* %ref.tmp107 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #10
  %98 = bitcast %class.btVector3* %ref.tmp106 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %98) #10
  %99 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %99) #10
  store i32 0, i32* %j, align 4, !tbaa !8
  br label %for.cond109

for.cond109:                                      ; preds = %for.inc118, %if.then95
  %100 = load i32, i32* %j, align 4, !tbaa !8
  %101 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %call110 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %101)
  %cmp111 = icmp slt i32 %100, %call110
  br i1 %cmp111, label %for.body113, label %for.cond.cleanup112

for.cond.cleanup112:                              ; preds = %for.cond109
  store i32 14, i32* %cleanup.dest.slot, align 4
  %102 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #10
  br label %for.end120

for.body113:                                      ; preds = %for.cond109
  %103 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %104 = load i32, i32* %j, align 4, !tbaa !8
  %105 = bitcast %class.btVector3* %ref.tmp114 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %105) #10
  %106 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_gravity115 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %106, i32 0, i32 7
  %107 = bitcast float* %ref.tmp116 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %107) #10
  %108 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %109 = load i32, i32* %j, align 4, !tbaa !8
  %call117 = call float @_ZNK11btMultiBody11getLinkMassEi(%class.btMultiBody* %108, i32 %109)
  store float %call117, float* %ref.tmp116, align 4, !tbaa !73
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp114, %class.btVector3* nonnull align 4 dereferenceable(16) %m_gravity115, float* nonnull align 4 dereferenceable(4) %ref.tmp116)
  call void @_ZN11btMultiBody12addLinkForceEiRK9btVector3(%class.btMultiBody* %103, i32 %104, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp114)
  %110 = bitcast float* %ref.tmp116 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #10
  %111 = bitcast %class.btVector3* %ref.tmp114 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %111) #10
  br label %for.inc118

for.inc118:                                       ; preds = %for.body113
  %112 = load i32, i32* %j, align 4, !tbaa !8
  %inc119 = add nsw i32 %112, 1
  store i32 %inc119, i32* %j, align 4, !tbaa !8
  br label %for.cond109

for.end120:                                       ; preds = %for.cond.cleanup112
  %113 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %114 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %solverInfo.addr, align 4, !tbaa !2
  %115 = bitcast %struct.btContactSolverInfo* %114 to %struct.btContactSolverInfoData*
  %m_timeStep = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %115, i32 0, i32 3
  %116 = load float, float* %m_timeStep, align 4, !tbaa !92
  call void @_ZN11btMultiBody14stepVelocitiesEfR20btAlignedObjectArrayIfERS0_I9btVector3ERS0_I11btMatrix3x3E(%class.btMultiBody* %113, float %116, %class.btAlignedObjectArray.32* nonnull align 4 dereferenceable(17) %scratch_r, %class.btAlignedObjectArray.36* nonnull align 4 dereferenceable(17) %scratch_v, %class.btAlignedObjectArray.40* nonnull align 4 dereferenceable(17) %scratch_m)
  br label %if.end121

if.end121:                                        ; preds = %for.end120, %for.end93
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %isSleeping) #10
  %117 = bitcast %class.btMultiBody** %bod to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #10
  br label %for.inc122

for.inc122:                                       ; preds = %if.end121
  %118 = load i32, i32* %i65, align 4, !tbaa !8
  %inc123 = add nsw i32 %118, 1
  store i32 %inc123, i32* %i65, align 4, !tbaa !8
  br label %for.cond66

for.end124:                                       ; preds = %for.cond.cleanup
  %call125 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile63) #10
  %119 = bitcast %class.CProfileSample* %__profile63 to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %119) #10
  %m_solverMultiBodyIslandCallback126 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 5
  %120 = load %struct.MultiBodyInplaceSolverIslandCallback*, %struct.MultiBodyInplaceSolverIslandCallback** %m_solverMultiBodyIslandCallback126, align 4, !tbaa !82
  call void @_ZN36MultiBodyInplaceSolverIslandCallback18processConstraintsEv(%struct.MultiBodyInplaceSolverIslandCallback* %120)
  %121 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_constraintSolver127 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %121, i32 0, i32 3
  %122 = load %class.btConstraintSolver*, %class.btConstraintSolver** %m_constraintSolver127, align 4, !tbaa !90
  %123 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %solverInfo.addr, align 4, !tbaa !2
  %124 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_debugDrawer = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %124, i32 0, i32 5
  %125 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer, align 4, !tbaa !93
  %126 = bitcast %class.btConstraintSolver* %122 to void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)***
  %vtable128 = load void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)**, void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)*** %126, align 4, !tbaa !15
  %vfn129 = getelementptr inbounds void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)*, void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)** %vtable128, i64 4
  %127 = load void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)*, void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)** %vfn129, align 4
  call void %127(%class.btConstraintSolver* %122, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %123, %class.btIDebugDraw* %125)
  %128 = bitcast %class.btMultiBodyConstraint*** %sortedMultiBodyConstraints to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #10
  %129 = bitcast %class.btTypedConstraint*** %constraintsPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #10
  %130 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #10
  %call130 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile) #10
  %131 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %131) #10
  %call131 = call %class.btAlignedObjectArray.40* @_ZN20btAlignedObjectArrayI11btMatrix3x3ED2Ev(%class.btAlignedObjectArray.40* %scratch_m) #10
  %132 = bitcast %class.btAlignedObjectArray.40* %scratch_m to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %132) #10
  %call132 = call %class.btAlignedObjectArray.36* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.36* %scratch_v) #10
  %133 = bitcast %class.btAlignedObjectArray.36* %scratch_v to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %133) #10
  %call133 = call %class.btAlignedObjectArray.32* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.32* %scratch_r) #10
  %134 = bitcast %class.btAlignedObjectArray.32* %scratch_r to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %134) #10
  ret void
}

define linkonce_odr hidden %class.btAlignedObjectArray.32* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray.32* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.32*, align 4
  store %class.btAlignedObjectArray.32* %this, %class.btAlignedObjectArray.32** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.32*, %class.btAlignedObjectArray.32** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.32, %class.btAlignedObjectArray.32* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.33* @_ZN18btAlignedAllocatorIfLj16EEC2Ev(%class.btAlignedAllocator.33* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.32* %this1)
  ret %class.btAlignedObjectArray.32* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.36* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.36* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.36*, align 4
  store %class.btAlignedObjectArray.36* %this, %class.btAlignedObjectArray.36** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.36*, %class.btAlignedObjectArray.36** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.36, %class.btAlignedObjectArray.36* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.37* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator.37* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray.36* %this1)
  ret %class.btAlignedObjectArray.36* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.40* @_ZN20btAlignedObjectArrayI11btMatrix3x3EC2Ev(%class.btAlignedObjectArray.40* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.40*, align 4
  store %class.btAlignedObjectArray.40* %this, %class.btAlignedObjectArray.40** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.40*, %class.btAlignedObjectArray.40** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.40, %class.btAlignedObjectArray.40* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.41* @_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EEC2Ev(%class.btAlignedAllocator.41* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E4initEv(%class.btAlignedObjectArray.40* %this1)
  ret %class.btAlignedObjectArray.40* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE6resizeEiRKS1_(%class.btAlignedObjectArray.0* %this, i32 %newsize, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %fillData) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btTypedConstraint**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !8
  store %class.btTypedConstraint** %fillData, %class.btTypedConstraint*** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !8
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %2 = load i32, i32* %curSize, align 4, !tbaa !8
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #10
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  store i32 %4, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %6 = load i32, i32* %curSize, align 4, !tbaa !8
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %8 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4, !tbaa !51
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #10
  %14 = load i32, i32* %curSize, align 4, !tbaa !8
  store i32 %14, i32* %i6, align 4, !tbaa !8
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !8
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #10
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %18 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data11, align 4, !tbaa !51
  %19 = load i32, i32* %i6, align 4, !tbaa !8
  %arrayidx12 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %18, i32 %19
  %20 = bitcast %class.btTypedConstraint** %arrayidx12 to i8*
  %21 = bitcast i8* %20 to %class.btTypedConstraint**
  %22 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %fillData.addr, align 4, !tbaa !2
  %23 = load %class.btTypedConstraint*, %class.btTypedConstraint** %22, align 4, !tbaa !2
  store %class.btTypedConstraint* %23, %class.btTypedConstraint** %21, align 4, !tbaa !2
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %24 = load i32, i32* %i6, align 4, !tbaa !8
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !8
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %25 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 %25, i32* %m_size, align 4, !tbaa !50
  %26 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #10
  ret void
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9quickSortI34btSortConstraintOnIslandPredicate2EEvRKT_(%class.btAlignedObjectArray.0* %this, %class.btSortConstraintOnIslandPredicate2* nonnull align 1 dereferenceable(1) %CompareFunc) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %CompareFunc.addr = alloca %class.btSortConstraintOnIslandPredicate2*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store %class.btSortConstraintOnIslandPredicate2* %CompareFunc, %class.btSortConstraintOnIslandPredicate2** %CompareFunc.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %cmp = icmp sgt i32 %call, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = load %class.btSortConstraintOnIslandPredicate2*, %class.btSortConstraintOnIslandPredicate2** %CompareFunc.addr, align 4, !tbaa !2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE17quickSortInternalI34btSortConstraintOnIslandPredicate2EEvRKT_ii(%class.btAlignedObjectArray.0* %this1, %class.btSortConstraintOnIslandPredicate2* nonnull align 1 dereferenceable(1) %0, i32 0, i32 %sub)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE6resizeEiRKS1_(%class.btAlignedObjectArray.45* %this, i32 %newsize, %class.btMultiBodyConstraint** nonnull align 4 dereferenceable(4) %fillData) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.45*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btMultiBodyConstraint**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.45* %this, %class.btAlignedObjectArray.45** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !8
  store %class.btMultiBodyConstraint** %fillData, %class.btMultiBodyConstraint*** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.45*, %class.btAlignedObjectArray.45** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %call = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.45* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !8
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %2 = load i32, i32* %curSize, align 4, !tbaa !8
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #10
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  store i32 %4, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %6 = load i32, i32* %curSize, align 4, !tbaa !8
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 4
  %8 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data, align 4, !tbaa !72
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.45* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE7reserveEi(%class.btAlignedObjectArray.45* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #10
  %14 = load i32, i32* %curSize, align 4, !tbaa !8
  store i32 %14, i32* %i6, align 4, !tbaa !8
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !8
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #10
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 4
  %18 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data11, align 4, !tbaa !72
  %19 = load i32, i32* %i6, align 4, !tbaa !8
  %arrayidx12 = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %18, i32 %19
  %20 = bitcast %class.btMultiBodyConstraint** %arrayidx12 to i8*
  %21 = bitcast i8* %20 to %class.btMultiBodyConstraint**
  %22 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %fillData.addr, align 4, !tbaa !2
  %23 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %22, align 4, !tbaa !2
  store %class.btMultiBodyConstraint* %23, %class.btMultiBodyConstraint** %21, align 4, !tbaa !2
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %24 = load i32, i32* %i6, align 4, !tbaa !8
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !8
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %25 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 2
  store i32 %25, i32* %m_size, align 4, !tbaa !69
  %26 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #10
  ret void
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE9quickSortI42btSortMultiBodyConstraintOnIslandPredicateEEvRKT_(%class.btAlignedObjectArray.45* %this, %class.btSortMultiBodyConstraintOnIslandPredicate* nonnull align 1 dereferenceable(1) %CompareFunc) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.45*, align 4
  %CompareFunc.addr = alloca %class.btSortMultiBodyConstraintOnIslandPredicate*, align 4
  store %class.btAlignedObjectArray.45* %this, %class.btAlignedObjectArray.45** %this.addr, align 4, !tbaa !2
  store %class.btSortMultiBodyConstraintOnIslandPredicate* %CompareFunc, %class.btSortMultiBodyConstraintOnIslandPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.45*, %class.btAlignedObjectArray.45** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.45* %this1)
  %cmp = icmp sgt i32 %call, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = load %class.btSortMultiBodyConstraintOnIslandPredicate*, %class.btSortMultiBodyConstraintOnIslandPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.45* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE17quickSortInternalI42btSortMultiBodyConstraintOnIslandPredicateEEvRKT_ii(%class.btAlignedObjectArray.45* %this1, %class.btSortMultiBodyConstraintOnIslandPredicate* nonnull align 1 dereferenceable(1) %0, i32 0, i32 %sub)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN36MultiBodyInplaceSolverIslandCallback5setupEP19btContactSolverInfoPP17btTypedConstraintiPP21btMultiBodyConstraintiP12btIDebugDraw(%struct.MultiBodyInplaceSolverIslandCallback* %this, %struct.btContactSolverInfo* %solverInfo, %class.btTypedConstraint** %sortedConstraints, i32 %numConstraints, %class.btMultiBodyConstraint** %sortedMultiBodyConstraints, i32 %numMultiBodyConstraints, %class.btIDebugDraw* %debugDrawer) #1 comdat {
entry:
  %this.addr = alloca %struct.MultiBodyInplaceSolverIslandCallback*, align 4
  %solverInfo.addr = alloca %struct.btContactSolverInfo*, align 4
  %sortedConstraints.addr = alloca %class.btTypedConstraint**, align 4
  %numConstraints.addr = alloca i32, align 4
  %sortedMultiBodyConstraints.addr = alloca %class.btMultiBodyConstraint**, align 4
  %numMultiBodyConstraints.addr = alloca i32, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  %ref.tmp = alloca %class.btCollisionObject*, align 4
  %ref.tmp2 = alloca %class.btPersistentManifold*, align 4
  %ref.tmp3 = alloca %class.btTypedConstraint*, align 4
  %ref.tmp4 = alloca %class.btMultiBodyConstraint*, align 4
  store %struct.MultiBodyInplaceSolverIslandCallback* %this, %struct.MultiBodyInplaceSolverIslandCallback** %this.addr, align 4, !tbaa !2
  store %struct.btContactSolverInfo* %solverInfo, %struct.btContactSolverInfo** %solverInfo.addr, align 4, !tbaa !2
  store %class.btTypedConstraint** %sortedConstraints, %class.btTypedConstraint*** %sortedConstraints.addr, align 4, !tbaa !2
  store i32 %numConstraints, i32* %numConstraints.addr, align 4, !tbaa !8
  store %class.btMultiBodyConstraint** %sortedMultiBodyConstraints, %class.btMultiBodyConstraint*** %sortedMultiBodyConstraints.addr, align 4, !tbaa !2
  store i32 %numMultiBodyConstraints, i32* %numMultiBodyConstraints.addr, align 4, !tbaa !8
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4, !tbaa !2
  %this1 = load %struct.MultiBodyInplaceSolverIslandCallback*, %struct.MultiBodyInplaceSolverIslandCallback** %this.addr, align 4
  %0 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %solverInfo.addr, align 4, !tbaa !2
  %m_solverInfo = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 1
  store %struct.btContactSolverInfo* %0, %struct.btContactSolverInfo** %m_solverInfo, align 4, !tbaa !83
  %1 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %sortedMultiBodyConstraints.addr, align 4, !tbaa !2
  %m_multiBodySortedConstraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 3
  store %class.btMultiBodyConstraint** %1, %class.btMultiBodyConstraint*** %m_multiBodySortedConstraints, align 4, !tbaa !86
  %2 = load i32, i32* %numMultiBodyConstraints.addr, align 4, !tbaa !8
  %m_numMultiBodyConstraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 4
  store i32 %2, i32* %m_numMultiBodyConstraints, align 4, !tbaa !94
  %3 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %sortedConstraints.addr, align 4, !tbaa !2
  %m_sortedConstraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 5
  store %class.btTypedConstraint** %3, %class.btTypedConstraint*** %m_sortedConstraints, align 4, !tbaa !95
  %4 = load i32, i32* %numConstraints.addr, align 4, !tbaa !8
  %m_numConstraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 6
  store i32 %4, i32* %m_numConstraints, align 4, !tbaa !87
  %5 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4, !tbaa !2
  %m_debugDrawer = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 7
  store %class.btIDebugDraw* %5, %class.btIDebugDraw** %m_debugDrawer, align 4, !tbaa !88
  %m_bodies = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 9
  %6 = bitcast %class.btCollisionObject** %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #10
  store %class.btCollisionObject* null, %class.btCollisionObject** %ref.tmp, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE6resizeEiRKS1_(%class.btAlignedObjectArray* %m_bodies, i32 0, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %ref.tmp)
  %7 = bitcast %class.btCollisionObject** %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  %m_manifolds = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 10
  %8 = bitcast %class.btPersistentManifold** %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #10
  store %class.btPersistentManifold* null, %class.btPersistentManifold** %ref.tmp2, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_(%class.btAlignedObjectArray.9* %m_manifolds, i32 0, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %ref.tmp2)
  %9 = bitcast %class.btPersistentManifold** %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #10
  %m_constraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 11
  %10 = bitcast %class.btTypedConstraint** %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #10
  store %class.btTypedConstraint* null, %class.btTypedConstraint** %ref.tmp3, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE6resizeEiRKS1_(%class.btAlignedObjectArray.0* %m_constraints, i32 0, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %ref.tmp3)
  %11 = bitcast %class.btTypedConstraint** %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #10
  %m_multiBodyConstraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 12
  %12 = bitcast %class.btMultiBodyConstraint** %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #10
  store %class.btMultiBodyConstraint* null, %class.btMultiBodyConstraint** %ref.tmp4, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE6resizeEiRKS1_(%class.btAlignedObjectArray.45* %m_multiBodyConstraints, i32 0, %class.btMultiBodyConstraint** nonnull align 4 dereferenceable(4) %ref.tmp4)
  %13 = bitcast %class.btMultiBodyConstraint** %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #10
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK16btCollisionWorld22getNumCollisionObjectsEv(%class.btCollisionWorld* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_collisionObjects = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_collisionObjects)
  ret i32 %call
}

declare void @_ZN25btSimulationIslandManager22buildAndProcessIslandsEP12btDispatcherP16btCollisionWorldPNS_14IslandCallbackE(%class.btSimulationIslandManager*, %class.btDispatcher*, %class.btCollisionWorld*, %"struct.btSimulationIslandManager::IslandCallback"*) #5

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.32* %this, i32 %newsize, float* nonnull align 4 dereferenceable(4) %fillData) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.32*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca float*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.32* %this, %class.btAlignedObjectArray.32** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !8
  store float* %fillData, float** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.32*, %class.btAlignedObjectArray.32** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.32* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !8
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %2 = load i32, i32* %curSize, align 4, !tbaa !8
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #10
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  store i32 %4, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %6 = load i32, i32* %curSize, align 4, !tbaa !8
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.32, %class.btAlignedObjectArray.32* %this1, i32 0, i32 4
  %8 = load float*, float** %m_data, align 4, !tbaa !96
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds float, float* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.32* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray.32* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #10
  %14 = load i32, i32* %curSize, align 4, !tbaa !8
  store i32 %14, i32* %i6, align 4, !tbaa !8
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !8
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #10
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.32, %class.btAlignedObjectArray.32* %this1, i32 0, i32 4
  %18 = load float*, float** %m_data11, align 4, !tbaa !96
  %19 = load i32, i32* %i6, align 4, !tbaa !8
  %arrayidx12 = getelementptr inbounds float, float* %18, i32 %19
  %20 = bitcast float* %arrayidx12 to i8*
  %21 = bitcast i8* %20 to float*
  %22 = load float*, float** %fillData.addr, align 4, !tbaa !2
  %23 = load float, float* %22, align 4, !tbaa !73
  store float %23, float* %21, align 4, !tbaa !73
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %24 = load i32, i32* %i6, align 4, !tbaa !8
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !8
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %25 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.32, %class.btAlignedObjectArray.32* %this1, i32 0, i32 2
  store i32 %25, i32* %m_size, align 4, !tbaa !97
  %26 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #10
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.36* %this, i32 %newsize, %class.btVector3* nonnull align 4 dereferenceable(16) %fillData) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.36*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btVector3*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.36* %this, %class.btAlignedObjectArray.36** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !8
  store %class.btVector3* %fillData, %class.btVector3** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.36*, %class.btAlignedObjectArray.36** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.36* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !8
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %2 = load i32, i32* %curSize, align 4, !tbaa !8
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #10
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  store i32 %4, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %6 = load i32, i32* %curSize, align 4, !tbaa !8
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.36, %class.btAlignedObjectArray.36* %this1, i32 0, i32 4
  %8 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !98
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end17

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.36* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray.36* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #10
  %14 = load i32, i32* %curSize, align 4, !tbaa !8
  store i32 %14, i32* %i6, align 4, !tbaa !8
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc14, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !8
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #10
  br label %for.end16

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.36, %class.btAlignedObjectArray.36* %this1, i32 0, i32 4
  %18 = load %class.btVector3*, %class.btVector3** %m_data11, align 4, !tbaa !98
  %19 = load i32, i32* %i6, align 4, !tbaa !8
  %arrayidx12 = getelementptr inbounds %class.btVector3, %class.btVector3* %18, i32 %19
  %20 = bitcast %class.btVector3* %arrayidx12 to i8*
  %call13 = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %20)
  %21 = bitcast i8* %call13 to %class.btVector3*
  %22 = load %class.btVector3*, %class.btVector3** %fillData.addr, align 4, !tbaa !2
  %23 = bitcast %class.btVector3* %21 to i8*
  %24 = bitcast %class.btVector3* %22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false), !tbaa.struct !99
  br label %for.inc14

for.inc14:                                        ; preds = %for.body10
  %25 = load i32, i32* %i6, align 4, !tbaa !8
  %inc15 = add nsw i32 %25, 1
  store i32 %inc15, i32* %i6, align 4, !tbaa !8
  br label %for.cond7

for.end16:                                        ; preds = %for.cond.cleanup9
  br label %if.end17

if.end17:                                         ; preds = %for.end16, %for.end
  %26 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.36, %class.btAlignedObjectArray.36* %this1, i32 0, i32 2
  store i32 %26, i32* %m_size, align 4, !tbaa !101
  %27 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btMatrix3x3E6resizeEiRKS0_(%class.btAlignedObjectArray.40* %this, i32 %newsize, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %fillData) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.40*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btMatrix3x3*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.40* %this, %class.btAlignedObjectArray.40** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !8
  store %class.btMatrix3x3* %fillData, %class.btMatrix3x3** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.40*, %class.btAlignedObjectArray.40** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %call = call i32 @_ZNK20btAlignedObjectArrayI11btMatrix3x3E4sizeEv(%class.btAlignedObjectArray.40* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !8
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %2 = load i32, i32* %curSize, align 4, !tbaa !8
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #10
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  store i32 %4, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %6 = load i32, i32* %curSize, align 4, !tbaa !8
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.40, %class.btAlignedObjectArray.40* %this1, i32 0, i32 4
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m_data, align 4, !tbaa !102
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end17

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI11btMatrix3x3E4sizeEv(%class.btAlignedObjectArray.40* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E7reserveEi(%class.btAlignedObjectArray.40* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #10
  %14 = load i32, i32* %curSize, align 4, !tbaa !8
  store i32 %14, i32* %i6, align 4, !tbaa !8
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc14, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !8
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #10
  br label %for.end16

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.40, %class.btAlignedObjectArray.40* %this1, i32 0, i32 4
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m_data11, align 4, !tbaa !102
  %19 = load i32, i32* %i6, align 4, !tbaa !8
  %arrayidx12 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %18, i32 %19
  %20 = bitcast %class.btMatrix3x3* %arrayidx12 to i8*
  %21 = bitcast i8* %20 to %class.btMatrix3x3*
  %22 = load %class.btMatrix3x3*, %class.btMatrix3x3** %fillData.addr, align 4, !tbaa !2
  %call13 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %21, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %22)
  br label %for.inc14

for.inc14:                                        ; preds = %for.body10
  %23 = load i32, i32* %i6, align 4, !tbaa !8
  %inc15 = add nsw i32 %23, 1
  store i32 %inc15, i32* %i6, align 4, !tbaa !8
  br label %for.cond7

for.end16:                                        ; preds = %for.cond.cleanup9
  br label %if.end17

if.end17:                                         ; preds = %for.end16, %for.end
  %24 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.40, %class.btAlignedObjectArray.40* %this1, i32 0, i32 2
  store i32 %24, i32* %m_size, align 4, !tbaa !103
  %25 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #10
  ret void
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

declare void @_ZN11btMultiBody21clearForcesAndTorquesEv(%class.btMultiBody*) #5

define linkonce_odr hidden void @_ZN11btMultiBody12addBaseForceERK9btVector3(%class.btMultiBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %f) #0 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %f.addr = alloca %class.btVector3*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %f, %class.btVector3** %f.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %f.addr, align 4, !tbaa !2
  %base_force = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 5
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %base_force, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !73
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !73
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !73
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #10
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !73
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !73
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !73
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #10
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !73
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !73
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !73
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #10
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #10
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #10
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK11btMultiBody11getBaseMassEv(%class.btMultiBody* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %base_mass = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 3
  %0 = load float, float* %base_mass, align 4, !tbaa !104
  ret float %0
}

declare void @_ZN11btMultiBody12addLinkForceEiRK9btVector3(%class.btMultiBody*, i32, %class.btVector3* nonnull align 4 dereferenceable(16)) #5

declare float @_ZNK11btMultiBody11getLinkMassEi(%class.btMultiBody*, i32) #5

declare void @_ZN11btMultiBody14stepVelocitiesEfR20btAlignedObjectArrayIfERS0_I9btVector3ERS0_I11btMatrix3x3E(%class.btMultiBody*, float, %class.btAlignedObjectArray.32* nonnull align 4 dereferenceable(17), %class.btAlignedObjectArray.36* nonnull align 4 dereferenceable(17), %class.btAlignedObjectArray.40* nonnull align 4 dereferenceable(17)) #5

define linkonce_odr hidden void @_ZN36MultiBodyInplaceSolverIslandCallback18processConstraintsEv(%struct.MultiBodyInplaceSolverIslandCallback* %this) #0 comdat {
entry:
  %this.addr = alloca %struct.MultiBodyInplaceSolverIslandCallback*, align 4
  %bodies = alloca %class.btCollisionObject**, align 4
  %manifold = alloca %class.btPersistentManifold**, align 4
  %constraints = alloca %class.btTypedConstraint**, align 4
  %multiBodyConstraints = alloca %class.btMultiBodyConstraint**, align 4
  %ref.tmp = alloca %class.btCollisionObject*, align 4
  %ref.tmp38 = alloca %class.btPersistentManifold*, align 4
  %ref.tmp40 = alloca %class.btTypedConstraint*, align 4
  %ref.tmp42 = alloca %class.btMultiBodyConstraint*, align 4
  store %struct.MultiBodyInplaceSolverIslandCallback* %this, %struct.MultiBodyInplaceSolverIslandCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.MultiBodyInplaceSolverIslandCallback*, %struct.MultiBodyInplaceSolverIslandCallback** %this.addr, align 4
  %0 = bitcast %class.btCollisionObject*** %bodies to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %m_bodies = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 9
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_bodies)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_bodies2 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 9
  %call3 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray* %m_bodies2, i32 0)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %class.btCollisionObject** [ %call3, %cond.true ], [ null, %cond.false ]
  store %class.btCollisionObject** %cond, %class.btCollisionObject*** %bodies, align 4, !tbaa !2
  %1 = bitcast %class.btPersistentManifold*** %manifold to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  %m_manifolds = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 10
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %m_manifolds)
  %tobool5 = icmp ne i32 %call4, 0
  br i1 %tobool5, label %cond.true6, label %cond.false9

cond.true6:                                       ; preds = %cond.end
  %m_manifolds7 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 10
  %call8 = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.9* %m_manifolds7, i32 0)
  br label %cond.end10

cond.false9:                                      ; preds = %cond.end
  br label %cond.end10

cond.end10:                                       ; preds = %cond.false9, %cond.true6
  %cond11 = phi %class.btPersistentManifold** [ %call8, %cond.true6 ], [ null, %cond.false9 ]
  store %class.btPersistentManifold** %cond11, %class.btPersistentManifold*** %manifold, align 4, !tbaa !2
  %2 = bitcast %class.btTypedConstraint*** %constraints to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #10
  %m_constraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 11
  %call12 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.0* %m_constraints)
  %tobool13 = icmp ne i32 %call12, 0
  br i1 %tobool13, label %cond.true14, label %cond.false17

cond.true14:                                      ; preds = %cond.end10
  %m_constraints15 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 11
  %call16 = call nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray.0* %m_constraints15, i32 0)
  br label %cond.end18

cond.false17:                                     ; preds = %cond.end10
  br label %cond.end18

cond.end18:                                       ; preds = %cond.false17, %cond.true14
  %cond19 = phi %class.btTypedConstraint** [ %call16, %cond.true14 ], [ null, %cond.false17 ]
  store %class.btTypedConstraint** %cond19, %class.btTypedConstraint*** %constraints, align 4, !tbaa !2
  %3 = bitcast %class.btMultiBodyConstraint*** %multiBodyConstraints to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #10
  %m_multiBodyConstraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 12
  %call20 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.45* %m_multiBodyConstraints)
  %tobool21 = icmp ne i32 %call20, 0
  br i1 %tobool21, label %cond.true22, label %cond.false25

cond.true22:                                      ; preds = %cond.end18
  %m_multiBodyConstraints23 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 12
  %call24 = call nonnull align 4 dereferenceable(4) %class.btMultiBodyConstraint** @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintEixEi(%class.btAlignedObjectArray.45* %m_multiBodyConstraints23, i32 0)
  br label %cond.end26

cond.false25:                                     ; preds = %cond.end18
  br label %cond.end26

cond.end26:                                       ; preds = %cond.false25, %cond.true22
  %cond27 = phi %class.btMultiBodyConstraint** [ %call24, %cond.true22 ], [ null, %cond.false25 ]
  store %class.btMultiBodyConstraint** %cond27, %class.btMultiBodyConstraint*** %multiBodyConstraints, align 4, !tbaa !2
  %m_solver = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 2
  %4 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %m_solver, align 4, !tbaa !85
  %5 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies, align 4, !tbaa !2
  %m_bodies28 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 9
  %call29 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_bodies28)
  %6 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifold, align 4, !tbaa !2
  %m_manifolds30 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 10
  %call31 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %m_manifolds30)
  %7 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints, align 4, !tbaa !2
  %m_constraints32 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 11
  %call33 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.0* %m_constraints32)
  %8 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %multiBodyConstraints, align 4, !tbaa !2
  %m_multiBodyConstraints34 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 12
  %call35 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.45* %m_multiBodyConstraints34)
  %m_solverInfo = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 1
  %9 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %m_solverInfo, align 4, !tbaa !83
  %m_debugDrawer = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 7
  %10 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer, align 4, !tbaa !88
  %m_dispatcher = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 8
  %11 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4, !tbaa !89
  %12 = bitcast %class.btMultiBodyConstraintSolver* %4 to void (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %class.btMultiBodyConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)***
  %vtable = load void (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %class.btMultiBodyConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)**, void (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %class.btMultiBodyConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)*** %12, align 4, !tbaa !15
  %vfn = getelementptr inbounds void (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %class.btMultiBodyConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)*, void (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %class.btMultiBodyConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)** %vtable, i64 13
  %13 = load void (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %class.btMultiBodyConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)*, void (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %class.btMultiBodyConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)** %vfn, align 4
  call void %13(%class.btMultiBodyConstraintSolver* %4, %class.btCollisionObject** %5, i32 %call29, %class.btPersistentManifold** %6, i32 %call31, %class.btTypedConstraint** %7, i32 %call33, %class.btMultiBodyConstraint** %8, i32 %call35, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %9, %class.btIDebugDraw* %10, %class.btDispatcher* %11)
  %m_bodies36 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 9
  %14 = bitcast %class.btCollisionObject** %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #10
  store %class.btCollisionObject* null, %class.btCollisionObject** %ref.tmp, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE6resizeEiRKS1_(%class.btAlignedObjectArray* %m_bodies36, i32 0, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %ref.tmp)
  %15 = bitcast %class.btCollisionObject** %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #10
  %m_manifolds37 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 10
  %16 = bitcast %class.btPersistentManifold** %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #10
  store %class.btPersistentManifold* null, %class.btPersistentManifold** %ref.tmp38, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_(%class.btAlignedObjectArray.9* %m_manifolds37, i32 0, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %ref.tmp38)
  %17 = bitcast %class.btPersistentManifold** %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #10
  %m_constraints39 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 11
  %18 = bitcast %class.btTypedConstraint** %ref.tmp40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #10
  store %class.btTypedConstraint* null, %class.btTypedConstraint** %ref.tmp40, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE6resizeEiRKS1_(%class.btAlignedObjectArray.0* %m_constraints39, i32 0, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %ref.tmp40)
  %19 = bitcast %class.btTypedConstraint** %ref.tmp40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #10
  %m_multiBodyConstraints41 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 12
  %20 = bitcast %class.btMultiBodyConstraint** %ref.tmp42 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #10
  store %class.btMultiBodyConstraint* null, %class.btMultiBodyConstraint** %ref.tmp42, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE6resizeEiRKS1_(%class.btAlignedObjectArray.45* %m_multiBodyConstraints41, i32 0, %class.btMultiBodyConstraint** nonnull align 4 dereferenceable(4) %ref.tmp42)
  %21 = bitcast %class.btMultiBodyConstraint** %ref.tmp42 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #10
  %22 = bitcast %class.btMultiBodyConstraint*** %multiBodyConstraints to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #10
  %23 = bitcast %class.btTypedConstraint*** %constraints to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #10
  %24 = bitcast %class.btPersistentManifold*** %manifold to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #10
  %25 = bitcast %class.btCollisionObject*** %bodies to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #10
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.40* @_ZN20btAlignedObjectArrayI11btMatrix3x3ED2Ev(%class.btAlignedObjectArray.40* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.40*, align 4
  store %class.btAlignedObjectArray.40* %this, %class.btAlignedObjectArray.40** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.40*, %class.btAlignedObjectArray.40** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E5clearEv(%class.btAlignedObjectArray.40* %this1)
  ret %class.btAlignedObjectArray.40* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.36* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.36* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.36*, align 4
  store %class.btAlignedObjectArray.36* %this, %class.btAlignedObjectArray.36** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.36*, %class.btAlignedObjectArray.36** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray.36* %this1)
  ret %class.btAlignedObjectArray.36* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.32* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.32* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.32*, align 4
  store %class.btAlignedObjectArray.32* %this, %class.btAlignedObjectArray.32** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.32*, %class.btAlignedObjectArray.32** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIfE5clearEv(%class.btAlignedObjectArray.32* %this1)
  ret %class.btAlignedObjectArray.32* %this1
}

define hidden void @_ZN24btMultiBodyDynamicsWorld19integrateTransformsEf(%class.btMultiBodyDynamicsWorld* %this, float %timeStep) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  %timeStep.addr = alloca float, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %world_to_local = alloca %class.btAlignedObjectArray.71, align 4
  %local_origin = alloca %class.btAlignedObjectArray.36, align 4
  %b = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %bod = alloca %class.btMultiBody*, align 4
  %isSleeping = alloca i8, align 1
  %b11 = alloca i32, align 4
  %nLinks = alloca i32, align 4
  %ref.tmp = alloca %class.btQuaternion, align 4
  %ref.tmp31 = alloca %class.btVector3, align 4
  %posr = alloca %class.btVector3, align 4
  %pos = alloca [4 x float], align 16
  %quat = alloca [4 x float], align 16
  %tr = alloca %class.btTransform, align 4
  %ref.tmp61 = alloca %class.btQuaternion, align 4
  %k = alloca i32, align 4
  %parent = alloca i32, align 4
  %ref.tmp74 = alloca %class.btQuaternion, align 4
  %ref.tmp80 = alloca %class.btVector3, align 4
  %ref.tmp83 = alloca %class.btVector3, align 4
  %ref.tmp84 = alloca %class.btQuaternion, align 4
  %m = alloca i32, align 4
  %col = alloca %class.btMultiBodyLinkCollider*, align 4
  %link = alloca i32, align 4
  %index = alloca i32, align 4
  %posr103 = alloca %class.btVector3, align 4
  %pos105 = alloca [4 x float], align 16
  %quat113 = alloca [4 x float], align 16
  %tr129 = alloca %class.btTransform, align 4
  %ref.tmp131 = alloca %class.btQuaternion, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4, !tbaa !2
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !73
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %1 = load float, float* %timeStep.addr, align 4, !tbaa !73
  call void @_ZN23btDiscreteDynamicsWorld19integrateTransformsEf(%class.btDiscreteDynamicsWorld* %0, float %1)
  %2 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %2) #10
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.4, i32 0, i32 0))
  %3 = bitcast %class.btAlignedObjectArray.71* %world_to_local to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %3) #10
  %call2 = call %class.btAlignedObjectArray.71* @_ZN20btAlignedObjectArrayI12btQuaternionEC2Ev(%class.btAlignedObjectArray.71* %world_to_local)
  %4 = bitcast %class.btAlignedObjectArray.36* %local_origin to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %4) #10
  %call3 = call %class.btAlignedObjectArray.36* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.36* %local_origin)
  %5 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #10
  store i32 0, i32* %b, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc142, %entry
  %6 = load i32, i32* %b, align 4, !tbaa !8
  %m_multiBodies = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.21* %m_multiBodies)
  %cmp = icmp slt i32 %6, %call4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %7 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  br label %for.end144

for.body:                                         ; preds = %for.cond
  %8 = bitcast %class.btMultiBody** %bod to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #10
  %m_multiBodies5 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %9 = load i32, i32* %b, align 4, !tbaa !8
  %call6 = call nonnull align 4 dereferenceable(4) %class.btMultiBody** @_ZN20btAlignedObjectArrayIP11btMultiBodyEixEi(%class.btAlignedObjectArray.21* %m_multiBodies5, i32 %9)
  %10 = load %class.btMultiBody*, %class.btMultiBody** %call6, align 4, !tbaa !2
  store %class.btMultiBody* %10, %class.btMultiBody** %bod, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %isSleeping) #10
  store i8 0, i8* %isSleeping, align 1, !tbaa !91
  %11 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %call7 = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %11)
  %tobool = icmp ne %class.btMultiBodyLinkCollider* %call7, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body
  %12 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %call8 = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %12)
  %13 = bitcast %class.btMultiBodyLinkCollider* %call8 to %class.btCollisionObject*
  %call9 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %13)
  %cmp10 = icmp eq i32 %call9, 2
  br i1 %cmp10, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  store i8 1, i8* %isSleeping, align 1, !tbaa !91
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %for.body
  %14 = bitcast i32* %b11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #10
  store i32 0, i32* %b11, align 4, !tbaa !8
  br label %for.cond12

for.cond12:                                       ; preds = %for.inc, %if.end
  %15 = load i32, i32* %b11, align 4, !tbaa !8
  %16 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %call13 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %16)
  %cmp14 = icmp slt i32 %15, %call13
  br i1 %cmp14, label %for.body16, label %for.cond.cleanup15

for.cond.cleanup15:                               ; preds = %for.cond12
  store i32 5, i32* %cleanup.dest.slot, align 4
  %17 = bitcast i32* %b11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #10
  br label %for.end

for.body16:                                       ; preds = %for.cond12
  %18 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %19 = load i32, i32* %b11, align 4, !tbaa !8
  %call17 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %18, i32 %19)
  %m_collider = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call17, i32 0, i32 15
  %20 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider, align 4, !tbaa !17
  %tobool18 = icmp ne %class.btMultiBodyLinkCollider* %20, null
  br i1 %tobool18, label %land.lhs.true19, label %if.end25

land.lhs.true19:                                  ; preds = %for.body16
  %21 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %22 = load i32, i32* %b11, align 4, !tbaa !8
  %call20 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %21, i32 %22)
  %m_collider21 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call20, i32 0, i32 15
  %23 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider21, align 4, !tbaa !17
  %24 = bitcast %class.btMultiBodyLinkCollider* %23 to %class.btCollisionObject*
  %call22 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %24)
  %cmp23 = icmp eq i32 %call22, 2
  br i1 %cmp23, label %if.then24, label %if.end25

if.then24:                                        ; preds = %land.lhs.true19
  store i8 1, i8* %isSleeping, align 1, !tbaa !91
  br label %if.end25

if.end25:                                         ; preds = %if.then24, %land.lhs.true19, %for.body16
  br label %for.inc

for.inc:                                          ; preds = %if.end25
  %25 = load i32, i32* %b11, align 4, !tbaa !8
  %inc = add nsw i32 %25, 1
  store i32 %inc, i32* %b11, align 4, !tbaa !8
  br label %for.cond12

for.end:                                          ; preds = %for.cond.cleanup15
  %26 = load i8, i8* %isSleeping, align 1, !tbaa !91, !range !54
  %tobool26 = trunc i8 %26 to i1
  br i1 %tobool26, label %if.else, label %if.then27

if.then27:                                        ; preds = %for.end
  %27 = bitcast i32* %nLinks to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #10
  %28 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %call28 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %28)
  store i32 %call28, i32* %nLinks, align 4, !tbaa !8
  %29 = load i32, i32* %nLinks, align 4, !tbaa !8
  %add = add nsw i32 %29, 1
  %30 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %30) #10
  %call29 = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayI12btQuaternionE6resizeEiRKS0_(%class.btAlignedObjectArray.71* %world_to_local, i32 %add, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp)
  %31 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %31) #10
  %32 = load i32, i32* %nLinks, align 4, !tbaa !8
  %add30 = add nsw i32 %32, 1
  %33 = bitcast %class.btVector3* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %33) #10
  %call32 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp31)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.36* %local_origin, i32 %add30, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp31)
  %34 = bitcast %class.btVector3* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %34) #10
  %35 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %36 = load float, float* %timeStep.addr, align 4, !tbaa !73
  call void @_ZN11btMultiBody13stepPositionsEf(%class.btMultiBody* %35, float %36)
  %37 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %call33 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZNK11btMultiBody17getWorldToBaseRotEv(%class.btMultiBody* %37)
  %call34 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN20btAlignedObjectArrayI12btQuaternionEixEi(%class.btAlignedObjectArray.71* %world_to_local, i32 0)
  %38 = bitcast %class.btQuaternion* %call34 to i8*
  %39 = bitcast %class.btQuaternion* %call33 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %38, i8* align 4 %39, i32 16, i1 false)
  %40 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %call35 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMultiBody10getBasePosEv(%class.btMultiBody* %40)
  %call36 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.36* %local_origin, i32 0)
  %41 = bitcast %class.btVector3* %call36 to i8*
  %42 = bitcast %class.btVector3* %call35 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %41, i8* align 4 %42, i32 16, i1 false), !tbaa.struct !99
  %43 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %call37 = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %43)
  %tobool38 = icmp ne %class.btMultiBodyLinkCollider* %call37, null
  br i1 %tobool38, label %if.then39, label %if.end67

if.then39:                                        ; preds = %if.then27
  %44 = bitcast %class.btVector3* %posr to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %44) #10
  %call40 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.36* %local_origin, i32 0)
  %45 = bitcast %class.btVector3* %posr to i8*
  %46 = bitcast %class.btVector3* %call40 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %45, i8* align 4 %46, i32 16, i1 false), !tbaa.struct !99
  %47 = bitcast [4 x float]* %pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %47) #10
  %arrayinit.begin = getelementptr inbounds [4 x float], [4 x float]* %pos, i32 0, i32 0
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %posr)
  %48 = load float, float* %call41, align 4, !tbaa !73
  store float %48, float* %arrayinit.begin, align 4, !tbaa !73
  %arrayinit.element = getelementptr inbounds float, float* %arrayinit.begin, i32 1
  %call42 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %posr)
  %49 = load float, float* %call42, align 4, !tbaa !73
  store float %49, float* %arrayinit.element, align 4, !tbaa !73
  %arrayinit.element43 = getelementptr inbounds float, float* %arrayinit.element, i32 1
  %call44 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %posr)
  %50 = load float, float* %call44, align 4, !tbaa !73
  store float %50, float* %arrayinit.element43, align 4, !tbaa !73
  %arrayinit.element45 = getelementptr inbounds float, float* %arrayinit.element43, i32 1
  store float 1.000000e+00, float* %arrayinit.element45, align 4, !tbaa !73
  %51 = bitcast [4 x float]* %quat to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %51) #10
  %arrayinit.begin46 = getelementptr inbounds [4 x float], [4 x float]* %quat, i32 0, i32 0
  %call47 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN20btAlignedObjectArrayI12btQuaternionEixEi(%class.btAlignedObjectArray.71* %world_to_local, i32 0)
  %52 = bitcast %class.btQuaternion* %call47 to %class.btQuadWord*
  %call48 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %52)
  %53 = load float, float* %call48, align 4, !tbaa !73
  %fneg = fneg float %53
  store float %fneg, float* %arrayinit.begin46, align 4, !tbaa !73
  %arrayinit.element49 = getelementptr inbounds float, float* %arrayinit.begin46, i32 1
  %call50 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN20btAlignedObjectArrayI12btQuaternionEixEi(%class.btAlignedObjectArray.71* %world_to_local, i32 0)
  %54 = bitcast %class.btQuaternion* %call50 to %class.btQuadWord*
  %call51 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %54)
  %55 = load float, float* %call51, align 4, !tbaa !73
  %fneg52 = fneg float %55
  store float %fneg52, float* %arrayinit.element49, align 4, !tbaa !73
  %arrayinit.element53 = getelementptr inbounds float, float* %arrayinit.element49, i32 1
  %call54 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN20btAlignedObjectArrayI12btQuaternionEixEi(%class.btAlignedObjectArray.71* %world_to_local, i32 0)
  %56 = bitcast %class.btQuaternion* %call54 to %class.btQuadWord*
  %call55 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %56)
  %57 = load float, float* %call55, align 4, !tbaa !73
  %fneg56 = fneg float %57
  store float %fneg56, float* %arrayinit.element53, align 4, !tbaa !73
  %arrayinit.element57 = getelementptr inbounds float, float* %arrayinit.element53, i32 1
  %call58 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN20btAlignedObjectArrayI12btQuaternionEixEi(%class.btAlignedObjectArray.71* %world_to_local, i32 0)
  %58 = bitcast %class.btQuaternion* %call58 to %class.btQuadWord*
  %call59 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %58)
  %59 = load float, float* %call59, align 4, !tbaa !73
  store float %59, float* %arrayinit.element57, align 4, !tbaa !73
  %60 = bitcast %class.btTransform* %tr to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %60) #10
  %call60 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %tr)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %tr)
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %tr, %class.btVector3* nonnull align 4 dereferenceable(16) %posr)
  %61 = bitcast %class.btQuaternion* %ref.tmp61 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %61) #10
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %quat, i32 0, i32 0
  %arrayidx62 = getelementptr inbounds [4 x float], [4 x float]* %quat, i32 0, i32 1
  %arrayidx63 = getelementptr inbounds [4 x float], [4 x float]* %quat, i32 0, i32 2
  %arrayidx64 = getelementptr inbounds [4 x float], [4 x float]* %quat, i32 0, i32 3
  %call65 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %ref.tmp61, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx62, float* nonnull align 4 dereferenceable(4) %arrayidx63, float* nonnull align 4 dereferenceable(4) %arrayidx64)
  call void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %tr, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp61)
  %62 = bitcast %class.btQuaternion* %ref.tmp61 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %62) #10
  %63 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %call66 = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %63)
  %64 = bitcast %class.btMultiBodyLinkCollider* %call66 to %class.btCollisionObject*
  call void @_ZN17btCollisionObject17setWorldTransformERK11btTransform(%class.btCollisionObject* %64, %class.btTransform* nonnull align 4 dereferenceable(64) %tr)
  %65 = bitcast %class.btTransform* %tr to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %65) #10
  %66 = bitcast [4 x float]* %quat to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %66) #10
  %67 = bitcast [4 x float]* %pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %67) #10
  %68 = bitcast %class.btVector3* %posr to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %68) #10
  br label %if.end67

if.end67:                                         ; preds = %if.then39, %if.then27
  %69 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #10
  store i32 0, i32* %k, align 4, !tbaa !8
  br label %for.cond68

for.cond68:                                       ; preds = %for.inc90, %if.end67
  %70 = load i32, i32* %k, align 4, !tbaa !8
  %71 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %call69 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %71)
  %cmp70 = icmp slt i32 %70, %call69
  br i1 %cmp70, label %for.body72, label %for.cond.cleanup71

for.cond.cleanup71:                               ; preds = %for.cond68
  store i32 8, i32* %cleanup.dest.slot, align 4
  %72 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #10
  br label %for.end92

for.body72:                                       ; preds = %for.cond68
  %73 = bitcast i32* %parent to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #10
  %74 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %75 = load i32, i32* %k, align 4, !tbaa !8
  %call73 = call i32 @_ZNK11btMultiBody9getParentEi(%class.btMultiBody* %74, i32 %75)
  store i32 %call73, i32* %parent, align 4, !tbaa !8
  %76 = bitcast %class.btQuaternion* %ref.tmp74 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %76) #10
  %77 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %78 = load i32, i32* %k, align 4, !tbaa !8
  %call75 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZNK11btMultiBody19getParentToLocalRotEi(%class.btMultiBody* %77, i32 %78)
  %79 = load i32, i32* %parent, align 4, !tbaa !8
  %add76 = add nsw i32 %79, 1
  %call77 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN20btAlignedObjectArrayI12btQuaternionEixEi(%class.btAlignedObjectArray.71* %world_to_local, i32 %add76)
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %ref.tmp74, %class.btQuaternion* nonnull align 4 dereferenceable(16) %call75, %class.btQuaternion* nonnull align 4 dereferenceable(16) %call77)
  %80 = load i32, i32* %k, align 4, !tbaa !8
  %add78 = add nsw i32 %80, 1
  %call79 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN20btAlignedObjectArrayI12btQuaternionEixEi(%class.btAlignedObjectArray.71* %world_to_local, i32 %add78)
  %81 = bitcast %class.btQuaternion* %call79 to i8*
  %82 = bitcast %class.btQuaternion* %ref.tmp74 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %81, i8* align 4 %82, i32 16, i1 false)
  %83 = bitcast %class.btQuaternion* %ref.tmp74 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %83) #10
  %84 = bitcast %class.btVector3* %ref.tmp80 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %84) #10
  %85 = load i32, i32* %parent, align 4, !tbaa !8
  %add81 = add nsw i32 %85, 1
  %call82 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.36* %local_origin, i32 %add81)
  %86 = bitcast %class.btVector3* %ref.tmp83 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %86) #10
  %87 = bitcast %class.btQuaternion* %ref.tmp84 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %87) #10
  %88 = load i32, i32* %k, align 4, !tbaa !8
  %add85 = add nsw i32 %88, 1
  %call86 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN20btAlignedObjectArrayI12btQuaternionEixEi(%class.btAlignedObjectArray.71* %world_to_local, i32 %add85)
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp84, %class.btQuaternion* %call86)
  %89 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %90 = load i32, i32* %k, align 4, !tbaa !8
  %call87 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMultiBody10getRVectorEi(%class.btMultiBody* %89, i32 %90)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %ref.tmp83, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp84, %class.btVector3* nonnull align 4 dereferenceable(16) %call87)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp80, %class.btVector3* nonnull align 4 dereferenceable(16) %call82, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp83)
  %91 = load i32, i32* %k, align 4, !tbaa !8
  %add88 = add nsw i32 %91, 1
  %call89 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.36* %local_origin, i32 %add88)
  %92 = bitcast %class.btVector3* %call89 to i8*
  %93 = bitcast %class.btVector3* %ref.tmp80 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %92, i8* align 4 %93, i32 16, i1 false), !tbaa.struct !99
  %94 = bitcast %class.btQuaternion* %ref.tmp84 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %94) #10
  %95 = bitcast %class.btVector3* %ref.tmp83 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %95) #10
  %96 = bitcast %class.btVector3* %ref.tmp80 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %96) #10
  %97 = bitcast i32* %parent to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #10
  br label %for.inc90

for.inc90:                                        ; preds = %for.body72
  %98 = load i32, i32* %k, align 4, !tbaa !8
  %inc91 = add nsw i32 %98, 1
  store i32 %inc91, i32* %k, align 4, !tbaa !8
  br label %for.cond68

for.end92:                                        ; preds = %for.cond.cleanup71
  %99 = bitcast i32* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %99) #10
  store i32 0, i32* %m, align 4, !tbaa !8
  br label %for.cond93

for.cond93:                                       ; preds = %for.inc138, %for.end92
  %100 = load i32, i32* %m, align 4, !tbaa !8
  %101 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %call94 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %101)
  %cmp95 = icmp slt i32 %100, %call94
  br i1 %cmp95, label %for.body97, label %for.cond.cleanup96

for.cond.cleanup96:                               ; preds = %for.cond93
  store i32 11, i32* %cleanup.dest.slot, align 4
  %102 = bitcast i32* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #10
  br label %for.end140

for.body97:                                       ; preds = %for.cond93
  %103 = bitcast %class.btMultiBodyLinkCollider** %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %103) #10
  %104 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  %105 = load i32, i32* %m, align 4, !tbaa !8
  %call98 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %104, i32 %105)
  %m_collider99 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call98, i32 0, i32 15
  %106 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider99, align 4, !tbaa !17
  store %class.btMultiBodyLinkCollider* %106, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %107 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %tobool100 = icmp ne %class.btMultiBodyLinkCollider* %107, null
  br i1 %tobool100, label %if.then101, label %if.end137

if.then101:                                       ; preds = %for.body97
  %108 = bitcast i32* %link to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %108) #10
  %109 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %m_link = getelementptr inbounds %class.btMultiBodyLinkCollider, %class.btMultiBodyLinkCollider* %109, i32 0, i32 2
  %110 = load i32, i32* %m_link, align 4, !tbaa !105
  store i32 %110, i32* %link, align 4, !tbaa !8
  %111 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %111) #10
  %112 = load i32, i32* %link, align 4, !tbaa !8
  %add102 = add nsw i32 %112, 1
  store i32 %add102, i32* %index, align 4, !tbaa !8
  %113 = bitcast %class.btVector3* %posr103 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %113) #10
  %114 = load i32, i32* %index, align 4, !tbaa !8
  %call104 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.36* %local_origin, i32 %114)
  %115 = bitcast %class.btVector3* %posr103 to i8*
  %116 = bitcast %class.btVector3* %call104 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %115, i8* align 4 %116, i32 16, i1 false), !tbaa.struct !99
  %117 = bitcast [4 x float]* %pos105 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %117) #10
  %arrayinit.begin106 = getelementptr inbounds [4 x float], [4 x float]* %pos105, i32 0, i32 0
  %call107 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %posr103)
  %118 = load float, float* %call107, align 4, !tbaa !73
  store float %118, float* %arrayinit.begin106, align 4, !tbaa !73
  %arrayinit.element108 = getelementptr inbounds float, float* %arrayinit.begin106, i32 1
  %call109 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %posr103)
  %119 = load float, float* %call109, align 4, !tbaa !73
  store float %119, float* %arrayinit.element108, align 4, !tbaa !73
  %arrayinit.element110 = getelementptr inbounds float, float* %arrayinit.element108, i32 1
  %call111 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %posr103)
  %120 = load float, float* %call111, align 4, !tbaa !73
  store float %120, float* %arrayinit.element110, align 4, !tbaa !73
  %arrayinit.element112 = getelementptr inbounds float, float* %arrayinit.element110, i32 1
  store float 1.000000e+00, float* %arrayinit.element112, align 4, !tbaa !73
  %121 = bitcast [4 x float]* %quat113 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %121) #10
  %arrayinit.begin114 = getelementptr inbounds [4 x float], [4 x float]* %quat113, i32 0, i32 0
  %122 = load i32, i32* %index, align 4, !tbaa !8
  %call115 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN20btAlignedObjectArrayI12btQuaternionEixEi(%class.btAlignedObjectArray.71* %world_to_local, i32 %122)
  %123 = bitcast %class.btQuaternion* %call115 to %class.btQuadWord*
  %call116 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %123)
  %124 = load float, float* %call116, align 4, !tbaa !73
  %fneg117 = fneg float %124
  store float %fneg117, float* %arrayinit.begin114, align 4, !tbaa !73
  %arrayinit.element118 = getelementptr inbounds float, float* %arrayinit.begin114, i32 1
  %125 = load i32, i32* %index, align 4, !tbaa !8
  %call119 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN20btAlignedObjectArrayI12btQuaternionEixEi(%class.btAlignedObjectArray.71* %world_to_local, i32 %125)
  %126 = bitcast %class.btQuaternion* %call119 to %class.btQuadWord*
  %call120 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %126)
  %127 = load float, float* %call120, align 4, !tbaa !73
  %fneg121 = fneg float %127
  store float %fneg121, float* %arrayinit.element118, align 4, !tbaa !73
  %arrayinit.element122 = getelementptr inbounds float, float* %arrayinit.element118, i32 1
  %128 = load i32, i32* %index, align 4, !tbaa !8
  %call123 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN20btAlignedObjectArrayI12btQuaternionEixEi(%class.btAlignedObjectArray.71* %world_to_local, i32 %128)
  %129 = bitcast %class.btQuaternion* %call123 to %class.btQuadWord*
  %call124 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %129)
  %130 = load float, float* %call124, align 4, !tbaa !73
  %fneg125 = fneg float %130
  store float %fneg125, float* %arrayinit.element122, align 4, !tbaa !73
  %arrayinit.element126 = getelementptr inbounds float, float* %arrayinit.element122, i32 1
  %131 = load i32, i32* %index, align 4, !tbaa !8
  %call127 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN20btAlignedObjectArrayI12btQuaternionEixEi(%class.btAlignedObjectArray.71* %world_to_local, i32 %131)
  %132 = bitcast %class.btQuaternion* %call127 to %class.btQuadWord*
  %call128 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %132)
  %133 = load float, float* %call128, align 4, !tbaa !73
  store float %133, float* %arrayinit.element126, align 4, !tbaa !73
  %134 = bitcast %class.btTransform* %tr129 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %134) #10
  %call130 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %tr129)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %tr129)
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %tr129, %class.btVector3* nonnull align 4 dereferenceable(16) %posr103)
  %135 = bitcast %class.btQuaternion* %ref.tmp131 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %135) #10
  %arrayidx132 = getelementptr inbounds [4 x float], [4 x float]* %quat113, i32 0, i32 0
  %arrayidx133 = getelementptr inbounds [4 x float], [4 x float]* %quat113, i32 0, i32 1
  %arrayidx134 = getelementptr inbounds [4 x float], [4 x float]* %quat113, i32 0, i32 2
  %arrayidx135 = getelementptr inbounds [4 x float], [4 x float]* %quat113, i32 0, i32 3
  %call136 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %ref.tmp131, float* nonnull align 4 dereferenceable(4) %arrayidx132, float* nonnull align 4 dereferenceable(4) %arrayidx133, float* nonnull align 4 dereferenceable(4) %arrayidx134, float* nonnull align 4 dereferenceable(4) %arrayidx135)
  call void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %tr129, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp131)
  %136 = bitcast %class.btQuaternion* %ref.tmp131 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %136) #10
  %137 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %138 = bitcast %class.btMultiBodyLinkCollider* %137 to %class.btCollisionObject*
  call void @_ZN17btCollisionObject17setWorldTransformERK11btTransform(%class.btCollisionObject* %138, %class.btTransform* nonnull align 4 dereferenceable(64) %tr129)
  %139 = bitcast %class.btTransform* %tr129 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %139) #10
  %140 = bitcast [4 x float]* %quat113 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %140) #10
  %141 = bitcast [4 x float]* %pos105 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %141) #10
  %142 = bitcast %class.btVector3* %posr103 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %142) #10
  %143 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #10
  %144 = bitcast i32* %link to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #10
  br label %if.end137

if.end137:                                        ; preds = %if.then101, %for.body97
  %145 = bitcast %class.btMultiBodyLinkCollider** %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #10
  br label %for.inc138

for.inc138:                                       ; preds = %if.end137
  %146 = load i32, i32* %m, align 4, !tbaa !8
  %inc139 = add nsw i32 %146, 1
  store i32 %inc139, i32* %m, align 4, !tbaa !8
  br label %for.cond93

for.end140:                                       ; preds = %for.cond.cleanup96
  %147 = bitcast i32* %nLinks to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #10
  br label %if.end141

if.else:                                          ; preds = %for.end
  %148 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4, !tbaa !2
  call void @_ZN11btMultiBody15clearVelocitiesEv(%class.btMultiBody* %148)
  br label %if.end141

if.end141:                                        ; preds = %if.else, %for.end140
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %isSleeping) #10
  %149 = bitcast %class.btMultiBody** %bod to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #10
  br label %for.inc142

for.inc142:                                       ; preds = %if.end141
  %150 = load i32, i32* %b, align 4, !tbaa !8
  %inc143 = add nsw i32 %150, 1
  store i32 %inc143, i32* %b, align 4, !tbaa !8
  br label %for.cond

for.end144:                                       ; preds = %for.cond.cleanup
  %call145 = call %class.btAlignedObjectArray.36* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.36* %local_origin) #10
  %151 = bitcast %class.btAlignedObjectArray.36* %local_origin to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %151) #10
  %call146 = call %class.btAlignedObjectArray.71* @_ZN20btAlignedObjectArrayI12btQuaternionED2Ev(%class.btAlignedObjectArray.71* %world_to_local) #10
  %152 = bitcast %class.btAlignedObjectArray.71* %world_to_local to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %152) #10
  %call147 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile) #10
  %153 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %153) #10
  ret void
}

declare void @_ZN23btDiscreteDynamicsWorld19integrateTransformsEf(%class.btDiscreteDynamicsWorld*, float) unnamed_addr #5

define linkonce_odr hidden %class.btAlignedObjectArray.71* @_ZN20btAlignedObjectArrayI12btQuaternionEC2Ev(%class.btAlignedObjectArray.71* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.71*, align 4
  store %class.btAlignedObjectArray.71* %this, %class.btAlignedObjectArray.71** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.71*, %class.btAlignedObjectArray.71** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.71, %class.btAlignedObjectArray.71* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.72* @_ZN18btAlignedAllocatorI12btQuaternionLj16EEC2Ev(%class.btAlignedAllocator.72* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI12btQuaternionE4initEv(%class.btAlignedObjectArray.71* %this1)
  ret %class.btAlignedObjectArray.71* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btQuaternionE6resizeEiRKS0_(%class.btAlignedObjectArray.71* %this, i32 %newsize, %class.btQuaternion* nonnull align 4 dereferenceable(16) %fillData) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.71*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btQuaternion*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.71* %this, %class.btAlignedObjectArray.71** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !8
  store %class.btQuaternion* %fillData, %class.btQuaternion** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.71*, %class.btAlignedObjectArray.71** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %call = call i32 @_ZNK20btAlignedObjectArrayI12btQuaternionE4sizeEv(%class.btAlignedObjectArray.71* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !8
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %2 = load i32, i32* %curSize, align 4, !tbaa !8
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #10
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  store i32 %4, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %6 = load i32, i32* %curSize, align 4, !tbaa !8
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.71, %class.btAlignedObjectArray.71* %this1, i32 0, i32 4
  %8 = load %class.btQuaternion*, %class.btQuaternion** %m_data, align 4, !tbaa !107
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btQuaternion, %class.btQuaternion* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI12btQuaternionE4sizeEv(%class.btAlignedObjectArray.71* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayI12btQuaternionE7reserveEi(%class.btAlignedObjectArray.71* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #10
  %14 = load i32, i32* %curSize, align 4, !tbaa !8
  store i32 %14, i32* %i6, align 4, !tbaa !8
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !8
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #10
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.71, %class.btAlignedObjectArray.71* %this1, i32 0, i32 4
  %18 = load %class.btQuaternion*, %class.btQuaternion** %m_data11, align 4, !tbaa !107
  %19 = load i32, i32* %i6, align 4, !tbaa !8
  %arrayidx12 = getelementptr inbounds %class.btQuaternion, %class.btQuaternion* %18, i32 %19
  %20 = bitcast %class.btQuaternion* %arrayidx12 to i8*
  %21 = bitcast i8* %20 to %class.btQuaternion*
  %22 = load %class.btQuaternion*, %class.btQuaternion** %fillData.addr, align 4, !tbaa !2
  %23 = bitcast %class.btQuaternion* %21 to i8*
  %24 = bitcast %class.btQuaternion* %22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false)
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %25 = load i32, i32* %i6, align 4, !tbaa !8
  %inc14 = add nsw i32 %25, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !8
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %26 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.71, %class.btAlignedObjectArray.71* %this1, i32 0, i32 2
  store i32 %26, i32* %m_size, align 4, !tbaa !110
  %27 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #10
  ret void
}

define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* %0)
  ret %class.btQuaternion* %this1
}

declare void @_ZN11btMultiBody13stepPositionsEf(%class.btMultiBody*, float) #5

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZNK11btMultiBody17getWorldToBaseRotEv(%class.btMultiBody* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %base_quat = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 2
  ret %class.btQuaternion* %base_quat
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN20btAlignedObjectArrayI12btQuaternionEixEi(%class.btAlignedObjectArray.71* %this, i32 %n) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.71*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.71* %this, %class.btAlignedObjectArray.71** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.71*, %class.btAlignedObjectArray.71** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.71, %class.btAlignedObjectArray.71* %this1, i32 0, i32 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %m_data, align 4, !tbaa !107
  %1 = load i32, i32* %n.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btQuaternion, %class.btQuaternion* %0, i32 %1
  ret %class.btQuaternion* %arrayidx
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMultiBody10getBasePosEv(%class.btMultiBody* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %base_pos = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 1
  ret %class.btVector3* %base_pos
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.36* %this, i32 %n) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.36*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.36* %this, %class.btAlignedObjectArray.36** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.36*, %class.btAlignedObjectArray.36** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.36, %class.btAlignedObjectArray.36* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !98
  %1 = load i32, i32* %n.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %1
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

define linkonce_odr hidden void @_ZN11btTransform11setIdentityEv(%class.btTransform* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !73
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !73
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #10
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !73
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_origin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #10
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #10
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %origin) #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %origin.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %origin, %class.btVector3** %origin.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %origin.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_origin to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !99
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %m_basis, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0)
  ret void
}

define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  store float* %_w, float** %_w.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = load float*, float** %_x.addr, align 4, !tbaa !2
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float*, float** %_z.addr, align 4, !tbaa !2
  %4 = load float*, float** %_w.addr, align 4, !tbaa !2
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4)
  ret %class.btQuaternion* %this1
}

define linkonce_odr hidden void @_ZN17btCollisionObject17setWorldTransformERK11btTransform(%class.btCollisionObject* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %worldTrans) #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %worldTrans.addr = alloca %class.btTransform*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %worldTrans, %class.btTransform** %worldTrans.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_updateRevision = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 26
  %0 = load i32, i32* %m_updateRevision, align 4, !tbaa !111
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %m_updateRevision, align 4, !tbaa !111
  %1 = load %class.btTransform*, %class.btTransform** %worldTrans.addr, align 4, !tbaa !2
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_worldTransform, %class.btTransform* nonnull align 4 dereferenceable(64) %1)
  ret void
}

declare i32 @_ZNK11btMultiBody9getParentEi(%class.btMultiBody*, i32) #5

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q2) #1 comdat {
entry:
  %q1.addr = alloca %class.btQuaternion*, align 4
  %q2.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp44 = alloca float, align 4
  store %class.btQuaternion* %q1, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q2, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %2 = bitcast %class.btQuaternion* %1 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %2)
  %3 = load float, float* %call, align 4, !tbaa !73
  %4 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %5 = bitcast %class.btQuaternion* %4 to %class.btQuadWord*
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %5)
  %6 = load float, float* %call1, align 4, !tbaa !73
  %mul = fmul float %3, %6
  %7 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %8 = bitcast %class.btQuaternion* %7 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %8)
  %9 = load float, float* %call2, align 4, !tbaa !73
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %11)
  %12 = load float, float* %call3, align 4, !tbaa !73
  %mul4 = fmul float %9, %12
  %add = fadd float %mul, %mul4
  %13 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %14 = bitcast %class.btQuaternion* %13 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %14)
  %15 = load float, float* %call5, align 4, !tbaa !73
  %16 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %17 = bitcast %class.btQuaternion* %16 to %class.btQuadWord*
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %17)
  %18 = load float, float* %call6, align 4, !tbaa !73
  %mul7 = fmul float %15, %18
  %add8 = fadd float %add, %mul7
  %19 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %20 = bitcast %class.btQuaternion* %19 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %20)
  %21 = load float, float* %call9, align 4, !tbaa !73
  %22 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %23 = bitcast %class.btQuaternion* %22 to %class.btQuadWord*
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %23)
  %24 = load float, float* %call10, align 4, !tbaa !73
  %mul11 = fmul float %21, %24
  %sub = fsub float %add8, %mul11
  store float %sub, float* %ref.tmp, align 4, !tbaa !73
  %25 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #10
  %26 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %27 = bitcast %class.btQuaternion* %26 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %27)
  %28 = load float, float* %call13, align 4, !tbaa !73
  %29 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %30 = bitcast %class.btQuaternion* %29 to %class.btQuadWord*
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %30)
  %31 = load float, float* %call14, align 4, !tbaa !73
  %mul15 = fmul float %28, %31
  %32 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %33 = bitcast %class.btQuaternion* %32 to %class.btQuadWord*
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %33)
  %34 = load float, float* %call16, align 4, !tbaa !73
  %35 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %36 = bitcast %class.btQuaternion* %35 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %36)
  %37 = load float, float* %call17, align 4, !tbaa !73
  %mul18 = fmul float %34, %37
  %add19 = fadd float %mul15, %mul18
  %38 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %39 = bitcast %class.btQuaternion* %38 to %class.btQuadWord*
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %39)
  %40 = load float, float* %call20, align 4, !tbaa !73
  %41 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %42 = bitcast %class.btQuaternion* %41 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %42)
  %43 = load float, float* %call21, align 4, !tbaa !73
  %mul22 = fmul float %40, %43
  %add23 = fadd float %add19, %mul22
  %44 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %45 = bitcast %class.btQuaternion* %44 to %class.btQuadWord*
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %45)
  %46 = load float, float* %call24, align 4, !tbaa !73
  %47 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %48 = bitcast %class.btQuaternion* %47 to %class.btQuadWord*
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %48)
  %49 = load float, float* %call25, align 4, !tbaa !73
  %mul26 = fmul float %46, %49
  %sub27 = fsub float %add23, %mul26
  store float %sub27, float* %ref.tmp12, align 4, !tbaa !73
  %50 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #10
  %51 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %52 = bitcast %class.btQuaternion* %51 to %class.btQuadWord*
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %52)
  %53 = load float, float* %call29, align 4, !tbaa !73
  %54 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %55 = bitcast %class.btQuaternion* %54 to %class.btQuadWord*
  %call30 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %55)
  %56 = load float, float* %call30, align 4, !tbaa !73
  %mul31 = fmul float %53, %56
  %57 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %58 = bitcast %class.btQuaternion* %57 to %class.btQuadWord*
  %call32 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %58)
  %59 = load float, float* %call32, align 4, !tbaa !73
  %60 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %61 = bitcast %class.btQuaternion* %60 to %class.btQuadWord*
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %61)
  %62 = load float, float* %call33, align 4, !tbaa !73
  %mul34 = fmul float %59, %62
  %add35 = fadd float %mul31, %mul34
  %63 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %64 = bitcast %class.btQuaternion* %63 to %class.btQuadWord*
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %64)
  %65 = load float, float* %call36, align 4, !tbaa !73
  %66 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %67 = bitcast %class.btQuaternion* %66 to %class.btQuadWord*
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %67)
  %68 = load float, float* %call37, align 4, !tbaa !73
  %mul38 = fmul float %65, %68
  %add39 = fadd float %add35, %mul38
  %69 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %70 = bitcast %class.btQuaternion* %69 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %70)
  %71 = load float, float* %call40, align 4, !tbaa !73
  %72 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %73 = bitcast %class.btQuaternion* %72 to %class.btQuadWord*
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %73)
  %74 = load float, float* %call41, align 4, !tbaa !73
  %mul42 = fmul float %71, %74
  %sub43 = fsub float %add39, %mul42
  store float %sub43, float* %ref.tmp28, align 4, !tbaa !73
  %75 = bitcast float* %ref.tmp44 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #10
  %76 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %77 = bitcast %class.btQuaternion* %76 to %class.btQuadWord*
  %call45 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %77)
  %78 = load float, float* %call45, align 4, !tbaa !73
  %79 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %80 = bitcast %class.btQuaternion* %79 to %class.btQuadWord*
  %call46 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %80)
  %81 = load float, float* %call46, align 4, !tbaa !73
  %mul47 = fmul float %78, %81
  %82 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %83 = bitcast %class.btQuaternion* %82 to %class.btQuadWord*
  %call48 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %83)
  %84 = load float, float* %call48, align 4, !tbaa !73
  %85 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %86 = bitcast %class.btQuaternion* %85 to %class.btQuadWord*
  %call49 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %86)
  %87 = load float, float* %call49, align 4, !tbaa !73
  %mul50 = fmul float %84, %87
  %sub51 = fsub float %mul47, %mul50
  %88 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %89 = bitcast %class.btQuaternion* %88 to %class.btQuadWord*
  %call52 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %89)
  %90 = load float, float* %call52, align 4, !tbaa !73
  %91 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %92 = bitcast %class.btQuaternion* %91 to %class.btQuadWord*
  %call53 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %92)
  %93 = load float, float* %call53, align 4, !tbaa !73
  %mul54 = fmul float %90, %93
  %sub55 = fsub float %sub51, %mul54
  %94 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %95 = bitcast %class.btQuaternion* %94 to %class.btQuadWord*
  %call56 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %95)
  %96 = load float, float* %call56, align 4, !tbaa !73
  %97 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %98 = bitcast %class.btQuaternion* %97 to %class.btQuadWord*
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %98)
  %99 = load float, float* %call57, align 4, !tbaa !73
  %mul58 = fmul float %96, %99
  %sub59 = fsub float %sub55, %mul58
  store float %sub59, float* %ref.tmp44, align 4, !tbaa !73
  %call60 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp44)
  %100 = bitcast float* %ref.tmp44 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #10
  %101 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #10
  %102 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #10
  %103 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #10
  ret void
}

declare nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZNK11btMultiBody19getParentToLocalRotEi(%class.btMultiBody*, i32) #5

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #1 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !73
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !73
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !73
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #10
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !73
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !73
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !73
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #10
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !73
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !73
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !73
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #10
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #10
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #10
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %rotation, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %rotation.addr = alloca %class.btQuaternion*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %q = alloca %class.btQuaternion, align 4
  %ref.tmp = alloca %class.btQuaternion, align 4
  store %class.btQuaternion* %rotation, %class.btQuaternion** %rotation.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast %class.btQuaternion* %q to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #10
  %1 = load %class.btQuaternion*, %class.btQuaternion** %rotation.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  call void @_ZmlRK12btQuaternionRK9btVector3(%class.btQuaternion* sret align 4 %q, %class.btQuaternion* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  %3 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #10
  %4 = load %class.btQuaternion*, %class.btQuaternion** %rotation.addr, align 4, !tbaa !2
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp, %class.btQuaternion* %4)
  %call = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKS_(%class.btQuaternion* %q, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp)
  %5 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #10
  %6 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getXEv(%class.btQuadWord* %6)
  %7 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getYEv(%class.btQuadWord* %7)
  %8 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getZEv(%class.btQuadWord* %8)
  %call4 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %call1, float* nonnull align 4 dereferenceable(4) %call2, float* nonnull align 4 dereferenceable(4) %call3)
  %9 = bitcast %class.btQuaternion* %q to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #10
  ret void
}

define linkonce_odr hidden void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !73
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp, align 4, !tbaa !73
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #10
  %4 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats3 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %4, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [4 x float], [4 x float]* %m_floats3, i32 0, i32 1
  %5 = load float, float* %arrayidx4, align 4, !tbaa !73
  %fneg5 = fneg float %5
  store float %fneg5, float* %ref.tmp2, align 4, !tbaa !73
  %6 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #10
  %7 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats7 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %7, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 2
  %8 = load float, float* %arrayidx8, align 4, !tbaa !73
  %fneg9 = fneg float %8
  store float %fneg9, float* %ref.tmp6, align 4, !tbaa !73
  %9 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats10 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %9, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 3
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %10 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #10
  %11 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #10
  %12 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #10
  ret void
}

declare nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMultiBody10getRVectorEi(%class.btMultiBody*, i32) #5

declare void @_ZN11btMultiBody15clearVelocitiesEv(%class.btMultiBody*) #5

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.71* @_ZN20btAlignedObjectArrayI12btQuaternionED2Ev(%class.btAlignedObjectArray.71* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.71*, align 4
  store %class.btAlignedObjectArray.71* %this, %class.btAlignedObjectArray.71** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.71*, %class.btAlignedObjectArray.71** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI12btQuaternionE5clearEv(%class.btAlignedObjectArray.71* %this1)
  ret %class.btAlignedObjectArray.71* %this1
}

define hidden void @_ZN24btMultiBodyDynamicsWorld22addMultiBodyConstraintEP21btMultiBodyConstraint(%class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyConstraint* %constraint) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  %constraint.addr = alloca %class.btMultiBodyConstraint*, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4, !tbaa !2
  store %class.btMultiBodyConstraint* %constraint, %class.btMultiBodyConstraint** %constraint.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %m_multiBodyConstraints = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE9push_backERKS1_(%class.btAlignedObjectArray.45* %m_multiBodyConstraints, %class.btMultiBodyConstraint** nonnull align 4 dereferenceable(4) %constraint.addr)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE9push_backERKS1_(%class.btAlignedObjectArray.45* %this, %class.btMultiBodyConstraint** nonnull align 4 dereferenceable(4) %_Val) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.45*, align 4
  %_Val.addr = alloca %class.btMultiBodyConstraint**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.45* %this, %class.btAlignedObjectArray.45** %this.addr, align 4, !tbaa !2
  store %class.btMultiBodyConstraint** %_Val, %class.btMultiBodyConstraint*** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.45*, %class.btAlignedObjectArray.45** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %call = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.45* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !8
  %1 = load i32, i32* %sz, align 4, !tbaa !8
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE8capacityEv(%class.btAlignedObjectArray.45* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.45* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE9allocSizeEi(%class.btAlignedObjectArray.45* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE7reserveEi(%class.btAlignedObjectArray.45* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 4
  %2 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data, align 4, !tbaa !72
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !69
  %arrayidx = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %2, i32 %3
  %4 = bitcast %class.btMultiBodyConstraint** %arrayidx to i8*
  %5 = bitcast i8* %4 to %class.btMultiBodyConstraint**
  %6 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %_Val.addr, align 4, !tbaa !2
  %7 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %6, align 4, !tbaa !2
  store %class.btMultiBodyConstraint* %7, %class.btMultiBodyConstraint** %5, align 4, !tbaa !2
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size5, align 4, !tbaa !69
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size5, align 4, !tbaa !69
  %9 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #10
  ret void
}

define hidden void @_ZN24btMultiBodyDynamicsWorld25removeMultiBodyConstraintEP21btMultiBodyConstraint(%class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyConstraint* %constraint) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  %constraint.addr = alloca %class.btMultiBodyConstraint*, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4, !tbaa !2
  store %class.btMultiBodyConstraint* %constraint, %class.btMultiBodyConstraint** %constraint.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %m_multiBodyConstraints = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE6removeERKS1_(%class.btAlignedObjectArray.45* %m_multiBodyConstraints, %class.btMultiBodyConstraint** nonnull align 4 dereferenceable(4) %constraint.addr)
  ret void
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE6removeERKS1_(%class.btAlignedObjectArray.45* %this, %class.btMultiBodyConstraint** nonnull align 4 dereferenceable(4) %key) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.45*, align 4
  %key.addr = alloca %class.btMultiBodyConstraint**, align 4
  %findIndex = alloca i32, align 4
  store %class.btAlignedObjectArray.45* %this, %class.btAlignedObjectArray.45** %this.addr, align 4, !tbaa !2
  store %class.btMultiBodyConstraint** %key, %class.btMultiBodyConstraint*** %key.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.45*, %class.btAlignedObjectArray.45** %this.addr, align 4
  %0 = bitcast i32* %findIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %key.addr, align 4, !tbaa !2
  %call = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE16findLinearSearchERKS1_(%class.btAlignedObjectArray.45* %this1, %class.btMultiBodyConstraint** nonnull align 4 dereferenceable(4) %1)
  store i32 %call, i32* %findIndex, align 4, !tbaa !8
  %2 = load i32, i32* %findIndex, align 4, !tbaa !8
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.45* %this1)
  %cmp = icmp slt i32 %2, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load i32, i32* %findIndex, align 4, !tbaa !8
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.45* %this1)
  %sub = sub nsw i32 %call3, 1
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE4swapEii(%class.btAlignedObjectArray.45* %this1, i32 %3, i32 %sub)
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE8pop_backEv(%class.btAlignedObjectArray.45* %this1)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %4 = bitcast i32* %findIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #10
  ret void
}

declare void @_ZN16btCollisionWorld11updateAabbsEv(%class.btCollisionWorld*) unnamed_addr #5

declare void @_ZN16btCollisionWorld23computeOverlappingPairsEv(%class.btCollisionWorld*) unnamed_addr #5

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw(%class.btCollisionWorld* %this, %class.btIDebugDraw* %debugDrawer) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4, !tbaa !2
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %0 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4, !tbaa !2
  %m_debugDrawer = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 5
  store %class.btIDebugDraw* %0, %class.btIDebugDraw** %m_debugDrawer, align 4, !tbaa !93
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btIDebugDraw* @_ZN16btCollisionWorld14getDebugDrawerEv(%class.btCollisionWorld* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_debugDrawer = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 5
  %0 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer, align 4, !tbaa !93
  ret %class.btIDebugDraw* %0
}

declare void @_ZN23btDiscreteDynamicsWorld14debugDrawWorldEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #5

declare void @_ZN16btCollisionWorld15debugDrawObjectERK11btTransformPK16btCollisionShapeRK9btVector3(%class.btCollisionWorld*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #5

declare void @_ZNK16btCollisionWorld7rayTestERK9btVector3S2_RNS_17RayResultCallbackE(%class.btCollisionWorld*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), %"struct.btCollisionWorld::RayResultCallback"* nonnull align 4 dereferenceable(20)) unnamed_addr #5

declare void @_ZN23btDiscreteDynamicsWorld18addCollisionObjectEP17btCollisionObjectss(%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*, i16 signext, i16 signext) unnamed_addr #5

declare void @_ZN23btDiscreteDynamicsWorld21removeCollisionObjectEP17btCollisionObject(%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*) unnamed_addr #5

declare void @_ZN16btCollisionWorld33performDiscreteCollisionDetectionEv(%class.btCollisionWorld*) unnamed_addr #5

declare void @_ZN23btDiscreteDynamicsWorld9serializeEP12btSerializer(%class.btDiscreteDynamicsWorld*, %class.btSerializer*) unnamed_addr #5

declare i32 @_ZN23btDiscreteDynamicsWorld14stepSimulationEfif(%class.btDiscreteDynamicsWorld*, float, i32, float) unnamed_addr #5

declare void @_ZN23btDiscreteDynamicsWorld13addConstraintEP17btTypedConstraintb(%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*, i1 zeroext) unnamed_addr #5

declare void @_ZN23btDiscreteDynamicsWorld16removeConstraintEP17btTypedConstraint(%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*) unnamed_addr #5

declare void @_ZN23btDiscreteDynamicsWorld9addActionEP17btActionInterface(%class.btDiscreteDynamicsWorld*, %class.btActionInterface*) unnamed_addr #5

declare void @_ZN23btDiscreteDynamicsWorld12removeActionEP17btActionInterface(%class.btDiscreteDynamicsWorld*, %class.btActionInterface*) unnamed_addr #5

declare void @_ZN23btDiscreteDynamicsWorld10setGravityERK9btVector3(%class.btDiscreteDynamicsWorld*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #5

declare void @_ZNK23btDiscreteDynamicsWorld10getGravityEv(%class.btVector3* sret align 4, %class.btDiscreteDynamicsWorld*) unnamed_addr #5

declare void @_ZN23btDiscreteDynamicsWorld23synchronizeMotionStatesEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #5

declare void @_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBody(%class.btDiscreteDynamicsWorld*, %class.btRigidBody*) unnamed_addr #5

declare void @_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBodyss(%class.btDiscreteDynamicsWorld*, %class.btRigidBody*, i16 signext, i16 signext) unnamed_addr #5

declare void @_ZN23btDiscreteDynamicsWorld15removeRigidBodyEP11btRigidBody(%class.btDiscreteDynamicsWorld*, %class.btRigidBody*) unnamed_addr #5

declare void @_ZN23btDiscreteDynamicsWorld19setConstraintSolverEP18btConstraintSolver(%class.btDiscreteDynamicsWorld*, %class.btConstraintSolver*) unnamed_addr #5

declare %class.btConstraintSolver* @_ZN23btDiscreteDynamicsWorld19getConstraintSolverEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #5

declare i32 @_ZNK23btDiscreteDynamicsWorld17getNumConstraintsEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #5

declare %class.btTypedConstraint* @_ZN23btDiscreteDynamicsWorld13getConstraintEi(%class.btDiscreteDynamicsWorld*, i32) unnamed_addr #5

declare %class.btTypedConstraint* @_ZNK23btDiscreteDynamicsWorld13getConstraintEi(%class.btDiscreteDynamicsWorld*, i32) unnamed_addr #5

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK23btDiscreteDynamicsWorld12getWorldTypeEv(%class.btDiscreteDynamicsWorld* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  ret i32 2
}

declare void @_ZN23btDiscreteDynamicsWorld11clearForcesEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #5

declare void @_ZN23btDiscreteDynamicsWorld10addVehicleEP17btActionInterface(%class.btDiscreteDynamicsWorld*, %class.btActionInterface*) unnamed_addr #5

declare void @_ZN23btDiscreteDynamicsWorld13removeVehicleEP17btActionInterface(%class.btDiscreteDynamicsWorld*, %class.btActionInterface*) unnamed_addr #5

declare void @_ZN23btDiscreteDynamicsWorld12addCharacterEP17btActionInterface(%class.btDiscreteDynamicsWorld*, %class.btActionInterface*) unnamed_addr #5

declare void @_ZN23btDiscreteDynamicsWorld15removeCharacterEP17btActionInterface(%class.btDiscreteDynamicsWorld*, %class.btActionInterface*) unnamed_addr #5

declare void @_ZN23btDiscreteDynamicsWorld25predictUnconstraintMotionEf(%class.btDiscreteDynamicsWorld*, float) unnamed_addr #5

declare void @_ZN23btDiscreteDynamicsWorld28internalSingleStepSimulationEf(%class.btDiscreteDynamicsWorld*, float) unnamed_addr #5

declare void @_ZN23btDiscreteDynamicsWorld18saveKinematicStateEf(%class.btDiscreteDynamicsWorld*, float) unnamed_addr #5

declare void @_ZN23btDiscreteDynamicsWorld12applyGravityEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #5

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN23btDiscreteDynamicsWorld11setNumTasksEi(%class.btDiscreteDynamicsWorld* %this, i32 %numTasks) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %numTasks.addr = alloca i32, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4, !tbaa !2
  store i32 %numTasks, i32* %numTasks.addr, align 4, !tbaa !8
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  ret void
}

define linkonce_odr hidden void @_ZN23btDiscreteDynamicsWorld14updateVehiclesEf(%class.btDiscreteDynamicsWorld* %this, float %timeStep) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %timeStep.addr = alloca float, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4, !tbaa !2
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !73
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %0 = load float, float* %timeStep.addr, align 4, !tbaa !73
  call void @_ZN23btDiscreteDynamicsWorld13updateActionsEf(%class.btDiscreteDynamicsWorld* %this1, float %0)
  ret void
}

declare void @_ZN15CProfileManager13Start_ProfileEPKc(i8*) #5

define linkonce_odr hidden i32 @_ZN11btUnionFind4findEi(%class.btUnionFind* %this, i32 %x) #0 comdat {
entry:
  %this.addr = alloca %class.btUnionFind*, align 4
  %x.addr = alloca i32, align 4
  %elementPtr = alloca %struct.btElement*, align 4
  store %class.btUnionFind* %this, %class.btUnionFind** %this.addr, align 4, !tbaa !2
  store i32 %x, i32* %x.addr, align 4, !tbaa !8
  %this1 = load %class.btUnionFind*, %class.btUnionFind** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i32, i32* %x.addr, align 4, !tbaa !8
  %m_elements = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %1 = load i32, i32* %x.addr, align 4, !tbaa !8
  %call = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray.5* %m_elements, i32 %1)
  %m_id = getelementptr inbounds %struct.btElement, %struct.btElement* %call, i32 0, i32 0
  %2 = load i32, i32* %m_id, align 4, !tbaa !46
  %cmp = icmp ne i32 %0, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %3 = bitcast %struct.btElement** %elementPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #10
  %m_elements2 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %m_elements3 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %4 = load i32, i32* %x.addr, align 4, !tbaa !8
  %call4 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray.5* %m_elements3, i32 %4)
  %m_id5 = getelementptr inbounds %struct.btElement, %struct.btElement* %call4, i32 0, i32 0
  %5 = load i32, i32* %m_id5, align 4, !tbaa !46
  %call6 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray.5* %m_elements2, i32 %5)
  store %struct.btElement* %call6, %struct.btElement** %elementPtr, align 4, !tbaa !2
  %6 = load %struct.btElement*, %struct.btElement** %elementPtr, align 4, !tbaa !2
  %m_id7 = getelementptr inbounds %struct.btElement, %struct.btElement* %6, i32 0, i32 0
  %7 = load i32, i32* %m_id7, align 4, !tbaa !46
  %m_elements8 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %8 = load i32, i32* %x.addr, align 4, !tbaa !8
  %call9 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray.5* %m_elements8, i32 %8)
  %m_id10 = getelementptr inbounds %struct.btElement, %struct.btElement* %call9, i32 0, i32 0
  store i32 %7, i32* %m_id10, align 4, !tbaa !46
  %9 = load %struct.btElement*, %struct.btElement** %elementPtr, align 4, !tbaa !2
  %m_id11 = getelementptr inbounds %struct.btElement, %struct.btElement* %9, i32 0, i32 0
  %10 = load i32, i32* %m_id11, align 4, !tbaa !46
  store i32 %10, i32* %x.addr, align 4, !tbaa !8
  %11 = bitcast %struct.btElement** %elementPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #10
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %12 = load i32, i32* %x.addr, align 4, !tbaa !8
  ret i32 %12
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray.5* %this, i32 %n) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %0 = load %struct.btElement*, %struct.btElement** %m_data, align 4, !tbaa !112
  %1 = load i32, i32* %n.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %struct.btElement, %struct.btElement* %0, i32 %1
  ret %struct.btElement* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv(%class.btAlignedObjectArray.24* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.24*, align 4
  store %class.btAlignedObjectArray.24* %this, %class.btAlignedObjectArray.24** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.24*, %class.btAlignedObjectArray.24** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.24, %class.btAlignedObjectArray.24* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !115
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray.24* %this, i32 %n) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.24*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.24* %this, %class.btAlignedObjectArray.24** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.24*, %class.btAlignedObjectArray.24** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.24, %class.btAlignedObjectArray.24* %this1, i32 0, i32 4
  %0 = load %struct.btMultibodyLink*, %struct.btMultibodyLink** %m_data, align 4, !tbaa !116
  %1 = load i32, i32* %n.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %0, i32 %1
  ret %struct.btMultibodyLink* %arrayidx
}

declare void @_ZN15CProfileManager12Stop_ProfileEv() #5

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %"struct.btSimulationIslandManager::IslandCallback"* @_ZN25btSimulationIslandManager14IslandCallbackC2Ev(%"struct.btSimulationIslandManager::IslandCallback"* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.btSimulationIslandManager::IslandCallback"*, align 4
  store %"struct.btSimulationIslandManager::IslandCallback"* %this, %"struct.btSimulationIslandManager::IslandCallback"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btSimulationIslandManager::IslandCallback"*, %"struct.btSimulationIslandManager::IslandCallback"** %this.addr, align 4
  %0 = bitcast %"struct.btSimulationIslandManager::IslandCallback"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN25btSimulationIslandManager14IslandCallbackE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !15
  ret %"struct.btSimulationIslandManager::IslandCallback"* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP17btCollisionObjectEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray.9* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.10* @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev(%class.btAlignedAllocator.10* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.9* %this1)
  ret %class.btAlignedObjectArray.9* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIP17btTypedConstraintEC2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EEC2Ev(%class.btAlignedAllocator.1* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %struct.MultiBodyInplaceSolverIslandCallback* @_ZN36MultiBodyInplaceSolverIslandCallbackD2Ev(%struct.MultiBodyInplaceSolverIslandCallback* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %struct.MultiBodyInplaceSolverIslandCallback*, align 4
  store %struct.MultiBodyInplaceSolverIslandCallback* %this, %struct.MultiBodyInplaceSolverIslandCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.MultiBodyInplaceSolverIslandCallback*, %struct.MultiBodyInplaceSolverIslandCallback** %this.addr, align 4
  %0 = bitcast %struct.MultiBodyInplaceSolverIslandCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV36MultiBodyInplaceSolverIslandCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !15
  %m_multiBodyConstraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 12
  %call = call %class.btAlignedObjectArray.45* @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintED2Ev(%class.btAlignedObjectArray.45* %m_multiBodyConstraints) #10
  %m_constraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 11
  %call2 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev(%class.btAlignedObjectArray.0* %m_constraints) #10
  %m_manifolds = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 10
  %call3 = call %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray.9* %m_manifolds) #10
  %m_bodies = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 9
  %call4 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP17btCollisionObjectED2Ev(%class.btAlignedObjectArray* %m_bodies) #10
  %1 = bitcast %struct.MultiBodyInplaceSolverIslandCallback* %this1 to %"struct.btSimulationIslandManager::IslandCallback"*
  %call5 = call %"struct.btSimulationIslandManager::IslandCallback"* @_ZN25btSimulationIslandManager14IslandCallbackD2Ev(%"struct.btSimulationIslandManager::IslandCallback"* %1) #10
  ret %struct.MultiBodyInplaceSolverIslandCallback* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN36MultiBodyInplaceSolverIslandCallbackD0Ev(%struct.MultiBodyInplaceSolverIslandCallback* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %struct.MultiBodyInplaceSolverIslandCallback*, align 4
  store %struct.MultiBodyInplaceSolverIslandCallback* %this, %struct.MultiBodyInplaceSolverIslandCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.MultiBodyInplaceSolverIslandCallback*, %struct.MultiBodyInplaceSolverIslandCallback** %this.addr, align 4
  %call = call %struct.MultiBodyInplaceSolverIslandCallback* @_ZN36MultiBodyInplaceSolverIslandCallbackD2Ev(%struct.MultiBodyInplaceSolverIslandCallback* %this1) #10
  %0 = bitcast %struct.MultiBodyInplaceSolverIslandCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #12
  ret void
}

define linkonce_odr hidden void @_ZN36MultiBodyInplaceSolverIslandCallback13processIslandEPP17btCollisionObjectiPP20btPersistentManifoldii(%struct.MultiBodyInplaceSolverIslandCallback* %this, %class.btCollisionObject** %bodies, i32 %numBodies, %class.btPersistentManifold** %manifolds, i32 %numManifolds, i32 %islandId) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %struct.MultiBodyInplaceSolverIslandCallback*, align 4
  %bodies.addr = alloca %class.btCollisionObject**, align 4
  %numBodies.addr = alloca i32, align 4
  %manifolds.addr = alloca %class.btPersistentManifold**, align 4
  %numManifolds.addr = alloca i32, align 4
  %islandId.addr = alloca i32, align 4
  %startConstraint = alloca %class.btTypedConstraint**, align 4
  %startMultiBodyConstraint = alloca %class.btMultiBodyConstraint**, align 4
  %numCurConstraints = alloca i32, align 4
  %numCurMultiBodyConstraints = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.MultiBodyInplaceSolverIslandCallback* %this, %struct.MultiBodyInplaceSolverIslandCallback** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject** %bodies, %class.btCollisionObject*** %bodies.addr, align 4, !tbaa !2
  store i32 %numBodies, i32* %numBodies.addr, align 4, !tbaa !8
  store %class.btPersistentManifold** %manifolds, %class.btPersistentManifold*** %manifolds.addr, align 4, !tbaa !2
  store i32 %numManifolds, i32* %numManifolds.addr, align 4, !tbaa !8
  store i32 %islandId, i32* %islandId.addr, align 4, !tbaa !8
  %this1 = load %struct.MultiBodyInplaceSolverIslandCallback*, %struct.MultiBodyInplaceSolverIslandCallback** %this.addr, align 4
  %0 = load i32, i32* %islandId.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_solver = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 2
  %1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %m_solver, align 4, !tbaa !85
  %2 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4, !tbaa !2
  %3 = load i32, i32* %numBodies.addr, align 4, !tbaa !8
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifolds.addr, align 4, !tbaa !2
  %5 = load i32, i32* %numManifolds.addr, align 4, !tbaa !8
  %m_sortedConstraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 5
  %6 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_sortedConstraints, align 4, !tbaa !95
  %m_numConstraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 6
  %7 = load i32, i32* %m_numConstraints, align 4, !tbaa !87
  %m_multiBodySortedConstraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 3
  %8 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_multiBodySortedConstraints, align 4, !tbaa !86
  %arrayidx = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %8, i32 0
  %m_numConstraints2 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 6
  %9 = load i32, i32* %m_numConstraints2, align 4, !tbaa !87
  %m_solverInfo = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 1
  %10 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %m_solverInfo, align 4, !tbaa !83
  %m_debugDrawer = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 7
  %11 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer, align 4, !tbaa !88
  %m_dispatcher = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 8
  %12 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4, !tbaa !89
  %13 = bitcast %class.btMultiBodyConstraintSolver* %1 to void (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %class.btMultiBodyConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)***
  %vtable = load void (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %class.btMultiBodyConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)**, void (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %class.btMultiBodyConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)*** %13, align 4, !tbaa !15
  %vfn = getelementptr inbounds void (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %class.btMultiBodyConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)*, void (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %class.btMultiBodyConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)** %vtable, i64 13
  %14 = load void (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %class.btMultiBodyConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)*, void (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %class.btMultiBodyConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)** %vfn, align 4
  call void %14(%class.btMultiBodyConstraintSolver* %1, %class.btCollisionObject** %2, i32 %3, %class.btPersistentManifold** %4, i32 %5, %class.btTypedConstraint** %6, i32 %7, %class.btMultiBodyConstraint** %arrayidx, i32 %9, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %10, %class.btIDebugDraw* %11, %class.btDispatcher* %12)
  br label %if.end103

if.else:                                          ; preds = %entry
  %15 = bitcast %class.btTypedConstraint*** %startConstraint to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #10
  store %class.btTypedConstraint** null, %class.btTypedConstraint*** %startConstraint, align 4, !tbaa !2
  %16 = bitcast %class.btMultiBodyConstraint*** %startMultiBodyConstraint to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #10
  store %class.btMultiBodyConstraint** null, %class.btMultiBodyConstraint*** %startMultiBodyConstraint, align 4, !tbaa !2
  %17 = bitcast i32* %numCurConstraints to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #10
  store i32 0, i32* %numCurConstraints, align 4, !tbaa !8
  %18 = bitcast i32* %numCurMultiBodyConstraints to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #10
  store i32 0, i32* %numCurMultiBodyConstraints, align 4, !tbaa !8
  %19 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #10
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else
  %20 = load i32, i32* %i, align 4, !tbaa !8
  %m_numConstraints3 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 6
  %21 = load i32, i32* %m_numConstraints3, align 4, !tbaa !87
  %cmp4 = icmp slt i32 %20, %21
  br i1 %cmp4, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_sortedConstraints5 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 5
  %22 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_sortedConstraints5, align 4, !tbaa !95
  %23 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx6 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %22, i32 %23
  %24 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx6, align 4, !tbaa !2
  %call = call i32 @_Z24btGetConstraintIslandId2PK17btTypedConstraint(%class.btTypedConstraint* %24)
  %25 = load i32, i32* %islandId.addr, align 4, !tbaa !8
  %cmp7 = icmp eq i32 %call, %25
  br i1 %cmp7, label %if.then8, label %if.end

if.then8:                                         ; preds = %for.body
  %m_sortedConstraints9 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 5
  %26 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_sortedConstraints9, align 4, !tbaa !95
  %27 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx10 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %26, i32 %27
  store %class.btTypedConstraint** %arrayidx10, %class.btTypedConstraint*** %startConstraint, align 4, !tbaa !2
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %28 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %28, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %if.then8, %for.cond
  br label %for.cond11

for.cond11:                                       ; preds = %for.inc22, %for.end
  %29 = load i32, i32* %i, align 4, !tbaa !8
  %m_numConstraints12 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 6
  %30 = load i32, i32* %m_numConstraints12, align 4, !tbaa !87
  %cmp13 = icmp slt i32 %29, %30
  br i1 %cmp13, label %for.body14, label %for.end24

for.body14:                                       ; preds = %for.cond11
  %m_sortedConstraints15 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 5
  %31 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_sortedConstraints15, align 4, !tbaa !95
  %32 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx16 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %31, i32 %32
  %33 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx16, align 4, !tbaa !2
  %call17 = call i32 @_Z24btGetConstraintIslandId2PK17btTypedConstraint(%class.btTypedConstraint* %33)
  %34 = load i32, i32* %islandId.addr, align 4, !tbaa !8
  %cmp18 = icmp eq i32 %call17, %34
  br i1 %cmp18, label %if.then19, label %if.end21

if.then19:                                        ; preds = %for.body14
  %35 = load i32, i32* %numCurConstraints, align 4, !tbaa !8
  %inc20 = add nsw i32 %35, 1
  store i32 %inc20, i32* %numCurConstraints, align 4, !tbaa !8
  br label %if.end21

if.end21:                                         ; preds = %if.then19, %for.body14
  br label %for.inc22

for.inc22:                                        ; preds = %if.end21
  %36 = load i32, i32* %i, align 4, !tbaa !8
  %inc23 = add nsw i32 %36, 1
  store i32 %inc23, i32* %i, align 4, !tbaa !8
  br label %for.cond11

for.end24:                                        ; preds = %for.cond11
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond25

for.cond25:                                       ; preds = %for.inc36, %for.end24
  %37 = load i32, i32* %i, align 4, !tbaa !8
  %m_numMultiBodyConstraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 4
  %38 = load i32, i32* %m_numMultiBodyConstraints, align 4, !tbaa !94
  %cmp26 = icmp slt i32 %37, %38
  br i1 %cmp26, label %for.body27, label %for.end38

for.body27:                                       ; preds = %for.cond25
  %m_multiBodySortedConstraints28 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 3
  %39 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_multiBodySortedConstraints28, align 4, !tbaa !86
  %40 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx29 = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %39, i32 %40
  %41 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %arrayidx29, align 4, !tbaa !2
  %call30 = call i32 @_Z32btGetMultiBodyConstraintIslandIdPK21btMultiBodyConstraint(%class.btMultiBodyConstraint* %41)
  %42 = load i32, i32* %islandId.addr, align 4, !tbaa !8
  %cmp31 = icmp eq i32 %call30, %42
  br i1 %cmp31, label %if.then32, label %if.end35

if.then32:                                        ; preds = %for.body27
  %m_multiBodySortedConstraints33 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 3
  %43 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_multiBodySortedConstraints33, align 4, !tbaa !86
  %44 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx34 = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %43, i32 %44
  store %class.btMultiBodyConstraint** %arrayidx34, %class.btMultiBodyConstraint*** %startMultiBodyConstraint, align 4, !tbaa !2
  br label %for.end38

if.end35:                                         ; preds = %for.body27
  br label %for.inc36

for.inc36:                                        ; preds = %if.end35
  %45 = load i32, i32* %i, align 4, !tbaa !8
  %inc37 = add nsw i32 %45, 1
  store i32 %inc37, i32* %i, align 4, !tbaa !8
  br label %for.cond25

for.end38:                                        ; preds = %if.then32, %for.cond25
  br label %for.cond39

for.cond39:                                       ; preds = %for.inc50, %for.end38
  %46 = load i32, i32* %i, align 4, !tbaa !8
  %m_numMultiBodyConstraints40 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 4
  %47 = load i32, i32* %m_numMultiBodyConstraints40, align 4, !tbaa !94
  %cmp41 = icmp slt i32 %46, %47
  br i1 %cmp41, label %for.body42, label %for.end52

for.body42:                                       ; preds = %for.cond39
  %m_multiBodySortedConstraints43 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 3
  %48 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_multiBodySortedConstraints43, align 4, !tbaa !86
  %49 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx44 = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %48, i32 %49
  %50 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %arrayidx44, align 4, !tbaa !2
  %call45 = call i32 @_Z32btGetMultiBodyConstraintIslandIdPK21btMultiBodyConstraint(%class.btMultiBodyConstraint* %50)
  %51 = load i32, i32* %islandId.addr, align 4, !tbaa !8
  %cmp46 = icmp eq i32 %call45, %51
  br i1 %cmp46, label %if.then47, label %if.end49

if.then47:                                        ; preds = %for.body42
  %52 = load i32, i32* %numCurMultiBodyConstraints, align 4, !tbaa !8
  %inc48 = add nsw i32 %52, 1
  store i32 %inc48, i32* %numCurMultiBodyConstraints, align 4, !tbaa !8
  br label %if.end49

if.end49:                                         ; preds = %if.then47, %for.body42
  br label %for.inc50

for.inc50:                                        ; preds = %if.end49
  %53 = load i32, i32* %i, align 4, !tbaa !8
  %inc51 = add nsw i32 %53, 1
  store i32 %inc51, i32* %i, align 4, !tbaa !8
  br label %for.cond39

for.end52:                                        ; preds = %for.cond39
  %m_solverInfo53 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 1
  %54 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %m_solverInfo53, align 4, !tbaa !83
  %55 = bitcast %struct.btContactSolverInfo* %54 to %struct.btContactSolverInfoData*
  %m_minimumSolverBatchSize = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %55, i32 0, i32 18
  %56 = load i32, i32* %m_minimumSolverBatchSize, align 4, !tbaa !117
  %cmp54 = icmp sle i32 %56, 1
  br i1 %cmp54, label %if.then55, label %if.else63

if.then55:                                        ; preds = %for.end52
  %m_solver56 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 2
  %57 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %m_solver56, align 4, !tbaa !85
  %58 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4, !tbaa !2
  %59 = load i32, i32* %numBodies.addr, align 4, !tbaa !8
  %60 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifolds.addr, align 4, !tbaa !2
  %61 = load i32, i32* %numManifolds.addr, align 4, !tbaa !8
  %62 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %startConstraint, align 4, !tbaa !2
  %63 = load i32, i32* %numCurConstraints, align 4, !tbaa !8
  %m_solverInfo57 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 1
  %64 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %m_solverInfo57, align 4, !tbaa !83
  %m_debugDrawer58 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 7
  %65 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer58, align 4, !tbaa !88
  %m_dispatcher59 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 8
  %66 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher59, align 4, !tbaa !89
  %67 = bitcast %class.btMultiBodyConstraintSolver* %57 to float (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)***
  %vtable60 = load float (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)**, float (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)*** %67, align 4, !tbaa !15
  %vfn61 = getelementptr inbounds float (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)*, float (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)** %vtable60, i64 3
  %68 = load float (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)*, float (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)** %vfn61, align 4
  %call62 = call float %68(%class.btMultiBodyConstraintSolver* %57, %class.btCollisionObject** %58, i32 %59, %class.btPersistentManifold** %60, i32 %61, %class.btTypedConstraint** %62, i32 %63, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %64, %class.btIDebugDraw* %65, %class.btDispatcher* %66)
  br label %if.end102

if.else63:                                        ; preds = %for.end52
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond64

for.cond64:                                       ; preds = %for.inc68, %if.else63
  %69 = load i32, i32* %i, align 4, !tbaa !8
  %70 = load i32, i32* %numBodies.addr, align 4, !tbaa !8
  %cmp65 = icmp slt i32 %69, %70
  br i1 %cmp65, label %for.body66, label %for.end70

for.body66:                                       ; preds = %for.cond64
  %m_bodies = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 9
  %71 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4, !tbaa !2
  %72 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx67 = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %71, i32 %72
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE9push_backERKS1_(%class.btAlignedObjectArray* %m_bodies, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %arrayidx67)
  br label %for.inc68

for.inc68:                                        ; preds = %for.body66
  %73 = load i32, i32* %i, align 4, !tbaa !8
  %inc69 = add nsw i32 %73, 1
  store i32 %inc69, i32* %i, align 4, !tbaa !8
  br label %for.cond64

for.end70:                                        ; preds = %for.cond64
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond71

for.cond71:                                       ; preds = %for.inc75, %for.end70
  %74 = load i32, i32* %i, align 4, !tbaa !8
  %75 = load i32, i32* %numManifolds.addr, align 4, !tbaa !8
  %cmp72 = icmp slt i32 %74, %75
  br i1 %cmp72, label %for.body73, label %for.end77

for.body73:                                       ; preds = %for.cond71
  %m_manifolds = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 10
  %76 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifolds.addr, align 4, !tbaa !2
  %77 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx74 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %76, i32 %77
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_(%class.btAlignedObjectArray.9* %m_manifolds, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %arrayidx74)
  br label %for.inc75

for.inc75:                                        ; preds = %for.body73
  %78 = load i32, i32* %i, align 4, !tbaa !8
  %inc76 = add nsw i32 %78, 1
  store i32 %inc76, i32* %i, align 4, !tbaa !8
  br label %for.cond71

for.end77:                                        ; preds = %for.cond71
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond78

for.cond78:                                       ; preds = %for.inc82, %for.end77
  %79 = load i32, i32* %i, align 4, !tbaa !8
  %80 = load i32, i32* %numCurConstraints, align 4, !tbaa !8
  %cmp79 = icmp slt i32 %79, %80
  br i1 %cmp79, label %for.body80, label %for.end84

for.body80:                                       ; preds = %for.cond78
  %m_constraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 11
  %81 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %startConstraint, align 4, !tbaa !2
  %82 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx81 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %81, i32 %82
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9push_backERKS1_(%class.btAlignedObjectArray.0* %m_constraints, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %arrayidx81)
  br label %for.inc82

for.inc82:                                        ; preds = %for.body80
  %83 = load i32, i32* %i, align 4, !tbaa !8
  %inc83 = add nsw i32 %83, 1
  store i32 %inc83, i32* %i, align 4, !tbaa !8
  br label %for.cond78

for.end84:                                        ; preds = %for.cond78
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond85

for.cond85:                                       ; preds = %for.inc89, %for.end84
  %84 = load i32, i32* %i, align 4, !tbaa !8
  %85 = load i32, i32* %numCurMultiBodyConstraints, align 4, !tbaa !8
  %cmp86 = icmp slt i32 %84, %85
  br i1 %cmp86, label %for.body87, label %for.end91

for.body87:                                       ; preds = %for.cond85
  %m_multiBodyConstraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 12
  %86 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %startMultiBodyConstraint, align 4, !tbaa !2
  %87 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx88 = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %86, i32 %87
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE9push_backERKS1_(%class.btAlignedObjectArray.45* %m_multiBodyConstraints, %class.btMultiBodyConstraint** nonnull align 4 dereferenceable(4) %arrayidx88)
  br label %for.inc89

for.inc89:                                        ; preds = %for.body87
  %88 = load i32, i32* %i, align 4, !tbaa !8
  %inc90 = add nsw i32 %88, 1
  store i32 %inc90, i32* %i, align 4, !tbaa !8
  br label %for.cond85

for.end91:                                        ; preds = %for.cond85
  %m_constraints92 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 11
  %call93 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.0* %m_constraints92)
  %m_manifolds94 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 10
  %call95 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %m_manifolds94)
  %add = add nsw i32 %call93, %call95
  %m_solverInfo96 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 1
  %89 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %m_solverInfo96, align 4, !tbaa !83
  %90 = bitcast %struct.btContactSolverInfo* %89 to %struct.btContactSolverInfoData*
  %m_minimumSolverBatchSize97 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %90, i32 0, i32 18
  %91 = load i32, i32* %m_minimumSolverBatchSize97, align 4, !tbaa !117
  %cmp98 = icmp sgt i32 %add, %91
  br i1 %cmp98, label %if.then99, label %if.else100

if.then99:                                        ; preds = %for.end91
  call void @_ZN36MultiBodyInplaceSolverIslandCallback18processConstraintsEv(%struct.MultiBodyInplaceSolverIslandCallback* %this1)
  br label %if.end101

if.else100:                                       ; preds = %for.end91
  br label %if.end101

if.end101:                                        ; preds = %if.else100, %if.then99
  br label %if.end102

if.end102:                                        ; preds = %if.end101, %if.then55
  %92 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #10
  %93 = bitcast i32* %numCurMultiBodyConstraints to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #10
  %94 = bitcast i32* %numCurConstraints to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #10
  %95 = bitcast %class.btMultiBodyConstraint*** %startMultiBodyConstraint to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #10
  %96 = bitcast %class.btTypedConstraint*** %startConstraint to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #10
  br label %if.end103

if.end103:                                        ; preds = %if.end102, %if.then
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %"struct.btSimulationIslandManager::IslandCallback"* @_ZN25btSimulationIslandManager14IslandCallbackD2Ev(%"struct.btSimulationIslandManager::IslandCallback"* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %"struct.btSimulationIslandManager::IslandCallback"*, align 4
  store %"struct.btSimulationIslandManager::IslandCallback"* %this, %"struct.btSimulationIslandManager::IslandCallback"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btSimulationIslandManager::IslandCallback"*, %"struct.btSimulationIslandManager::IslandCallback"** %this.addr, align 4
  ret %"struct.btSimulationIslandManager::IslandCallback"* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN25btSimulationIslandManager14IslandCallbackD0Ev(%"struct.btSimulationIslandManager::IslandCallback"* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %"struct.btSimulationIslandManager::IslandCallback"*, align 4
  store %"struct.btSimulationIslandManager::IslandCallback"* %this, %"struct.btSimulationIslandManager::IslandCallback"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btSimulationIslandManager::IslandCallback"*, %"struct.btSimulationIslandManager::IslandCallback"** %this.addr, align 4
  call void @llvm.trap() #13
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #8

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE4initEv(%class.btAlignedObjectArray* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !118
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btCollisionObject** null, %class.btCollisionObject*** %m_data, align 4, !tbaa !119
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !120
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !121
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.10* @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev(%class.btAlignedAllocator.10* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.10*, align 4
  store %class.btAlignedAllocator.10* %this, %class.btAlignedAllocator.10** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.10*, %class.btAlignedAllocator.10** %this.addr, align 4
  ret %class.btAlignedAllocator.10* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.9* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !122
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data, align 4, !tbaa !38
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !37
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !123
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EEC2Ev(%class.btAlignedAllocator.1* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  ret %class.btAlignedAllocator.1* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv(%class.btAlignedObjectArray.0* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !124
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %class.btTypedConstraint** null, %class.btTypedConstraint*** %m_data, align 4, !tbaa !51
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !50
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !125
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE5clearEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray.9* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv(%class.btAlignedObjectArray.9* %this1)
  ret %class.btAlignedObjectArray.9* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP17btCollisionObjectED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE5clearEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !8
  store i32 %last, i32* %last.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %first.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %last.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %4 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4, !tbaa !51
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4, !tbaa !51
  %tobool = icmp ne %class.btTypedConstraint** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !124, !range !54
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data4, align 4, !tbaa !51
  call void @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE10deallocateEPS1_(%class.btAlignedAllocator.1* %m_allocator, %class.btTypedConstraint** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %class.btTypedConstraint** null, %class.btTypedConstraint*** %m_data5, align 4, !tbaa !51
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE10deallocateEPS1_(%class.btAlignedAllocator.1* %this, %class.btTypedConstraint** %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca %class.btTypedConstraint**, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  store %class.btTypedConstraint** %ptr, %class.btTypedConstraint*** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btTypedConstraint** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #5

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv(%class.btAlignedObjectArray.9* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.9* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.9* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.9* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.9* %this, i32 %first, i32 %last) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !8
  store i32 %last, i32* %last.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %first.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %last.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !38
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.9* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !38
  %tobool = icmp ne %class.btPersistentManifold** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !122, !range !54
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data4, align 4, !tbaa !38
  call void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.10* %m_allocator, %class.btPersistentManifold** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data5, align 4, !tbaa !38
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.10* %this, %class.btPersistentManifold** %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.10*, align 4
  %ptr.addr = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedAllocator.10* %this, %class.btAlignedAllocator.10** %this.addr, align 4, !tbaa !2
  store %class.btPersistentManifold** %ptr, %class.btPersistentManifold*** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.10*, %class.btAlignedAllocator.10** %this.addr, align 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btPersistentManifold** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE5clearEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !8
  store i32 %last, i32* %last.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %first.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %last.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4, !tbaa !119
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !120
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4, !tbaa !119
  %tobool = icmp ne %class.btCollisionObject** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !118, !range !54
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data4, align 4, !tbaa !119
  call void @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE10deallocateEPS1_(%class.btAlignedAllocator* %m_allocator, %class.btCollisionObject** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btCollisionObject** null, %class.btCollisionObject*** %m_data5, align 4, !tbaa !119
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE10deallocateEPS1_(%class.btAlignedAllocator* %this, %class.btCollisionObject** %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.btCollisionObject**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject** %ptr, %class.btCollisionObject*** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btCollisionObject** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #9

; Function Attrs: inlinehint
define linkonce_odr hidden i32 @_Z24btGetConstraintIslandId2PK17btTypedConstraint(%class.btTypedConstraint* %lhs) #1 comdat {
entry:
  %lhs.addr = alloca %class.btTypedConstraint*, align 4
  %islandId = alloca i32, align 4
  %rcolObj0 = alloca %class.btCollisionObject*, align 4
  %rcolObj1 = alloca %class.btCollisionObject*, align 4
  store %class.btTypedConstraint* %lhs, %class.btTypedConstraint** %lhs.addr, align 4, !tbaa !2
  %0 = bitcast i32* %islandId to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = bitcast %class.btCollisionObject** %rcolObj0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  %2 = load %class.btTypedConstraint*, %class.btTypedConstraint** %lhs.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZNK17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %2)
  %3 = bitcast %class.btRigidBody* %call to %class.btCollisionObject*
  store %class.btCollisionObject* %3, %class.btCollisionObject** %rcolObj0, align 4, !tbaa !2
  %4 = bitcast %class.btCollisionObject** %rcolObj1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #10
  %5 = load %class.btTypedConstraint*, %class.btTypedConstraint** %lhs.addr, align 4, !tbaa !2
  %call1 = call nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZNK17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %5)
  %6 = bitcast %class.btRigidBody* %call1 to %class.btCollisionObject*
  store %class.btCollisionObject* %6, %class.btCollisionObject** %rcolObj1, align 4, !tbaa !2
  %7 = load %class.btCollisionObject*, %class.btCollisionObject** %rcolObj0, align 4, !tbaa !2
  %call2 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %7)
  %cmp = icmp sge i32 %call2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %8 = load %class.btCollisionObject*, %class.btCollisionObject** %rcolObj0, align 4, !tbaa !2
  %call3 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %8)
  br label %cond.end

cond.false:                                       ; preds = %entry
  %9 = load %class.btCollisionObject*, %class.btCollisionObject** %rcolObj1, align 4, !tbaa !2
  %call4 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %9)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %call3, %cond.true ], [ %call4, %cond.false ]
  store i32 %cond, i32* %islandId, align 4, !tbaa !8
  %10 = load i32, i32* %islandId, align 4, !tbaa !8
  %11 = bitcast %class.btCollisionObject** %rcolObj1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #10
  %12 = bitcast %class.btCollisionObject** %rcolObj0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #10
  %13 = bitcast i32* %islandId to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #10
  ret i32 %10
}

; Function Attrs: inlinehint
define linkonce_odr hidden i32 @_Z32btGetMultiBodyConstraintIslandIdPK21btMultiBodyConstraint(%class.btMultiBodyConstraint* %lhs) #1 comdat {
entry:
  %lhs.addr = alloca %class.btMultiBodyConstraint*, align 4
  %islandId = alloca i32, align 4
  %islandTagA = alloca i32, align 4
  %islandTagB = alloca i32, align 4
  store %class.btMultiBodyConstraint* %lhs, %class.btMultiBodyConstraint** %lhs.addr, align 4, !tbaa !2
  %0 = bitcast i32* %islandId to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = bitcast i32* %islandTagA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  %2 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %lhs.addr, align 4, !tbaa !2
  %3 = bitcast %class.btMultiBodyConstraint* %2 to i32 (%class.btMultiBodyConstraint*)***
  %vtable = load i32 (%class.btMultiBodyConstraint*)**, i32 (%class.btMultiBodyConstraint*)*** %3, align 4, !tbaa !15
  %vfn = getelementptr inbounds i32 (%class.btMultiBodyConstraint*)*, i32 (%class.btMultiBodyConstraint*)** %vtable, i64 2
  %4 = load i32 (%class.btMultiBodyConstraint*)*, i32 (%class.btMultiBodyConstraint*)** %vfn, align 4
  %call = call i32 %4(%class.btMultiBodyConstraint* %2)
  store i32 %call, i32* %islandTagA, align 4, !tbaa !8
  %5 = bitcast i32* %islandTagB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #10
  %6 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %lhs.addr, align 4, !tbaa !2
  %7 = bitcast %class.btMultiBodyConstraint* %6 to i32 (%class.btMultiBodyConstraint*)***
  %vtable1 = load i32 (%class.btMultiBodyConstraint*)**, i32 (%class.btMultiBodyConstraint*)*** %7, align 4, !tbaa !15
  %vfn2 = getelementptr inbounds i32 (%class.btMultiBodyConstraint*)*, i32 (%class.btMultiBodyConstraint*)** %vtable1, i64 3
  %8 = load i32 (%class.btMultiBodyConstraint*)*, i32 (%class.btMultiBodyConstraint*)** %vfn2, align 4
  %call3 = call i32 %8(%class.btMultiBodyConstraint* %6)
  store i32 %call3, i32* %islandTagB, align 4, !tbaa !8
  %9 = load i32, i32* %islandTagA, align 4, !tbaa !8
  %cmp = icmp sge i32 %9, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %10 = load i32, i32* %islandTagA, align 4, !tbaa !8
  br label %cond.end

cond.false:                                       ; preds = %entry
  %11 = load i32, i32* %islandTagB, align 4, !tbaa !8
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %10, %cond.true ], [ %11, %cond.false ]
  store i32 %cond, i32* %islandId, align 4, !tbaa !8
  %12 = load i32, i32* %islandId, align 4, !tbaa !8
  %13 = bitcast i32* %islandTagB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #10
  %14 = bitcast i32* %islandTagA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #10
  %15 = bitcast i32* %islandId to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #10
  ret i32 %12
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE9push_backERKS1_(%class.btAlignedObjectArray* %this, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %_Val) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %class.btCollisionObject**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject** %_Val, %class.btCollisionObject*** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !8
  %1 = load i32, i32* %sz, align 4, !tbaa !8
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP17btCollisionObjectE9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4, !tbaa !119
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !120
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %2, i32 %3
  %4 = bitcast %class.btCollisionObject** %arrayidx to i8*
  %5 = bitcast i8* %4 to %class.btCollisionObject**
  %6 = load %class.btCollisionObject**, %class.btCollisionObject*** %_Val.addr, align 4, !tbaa !2
  %7 = load %class.btCollisionObject*, %class.btCollisionObject** %6, align 4, !tbaa !2
  store %class.btCollisionObject* %7, %class.btCollisionObject** %5, align 4, !tbaa !2
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size5, align 4, !tbaa !120
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size5, align 4, !tbaa !120
  %9 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #10
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_(%class.btAlignedObjectArray.9* %this, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %_Val) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %_Val.addr = alloca %class.btPersistentManifold**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  store %class.btPersistentManifold** %_Val, %class.btPersistentManifold*** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !8
  %1 = load i32, i32* %sz, align 4, !tbaa !8
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.9* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi(%class.btAlignedObjectArray.9* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.9* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !38
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !37
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %2, i32 %3
  %4 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %5 = bitcast i8* %4 to %class.btPersistentManifold**
  %6 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %_Val.addr, align 4, !tbaa !2
  %7 = load %class.btPersistentManifold*, %class.btPersistentManifold** %6, align 4, !tbaa !2
  store %class.btPersistentManifold* %7, %class.btPersistentManifold** %5, align 4, !tbaa !2
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size5, align 4, !tbaa !37
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size5, align 4, !tbaa !37
  %9 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #10
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9push_backERKS1_(%class.btAlignedObjectArray.0* %this, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %_Val) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Val.addr = alloca %class.btTypedConstraint**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store %class.btTypedConstraint** %_Val, %class.btTypedConstraint*** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !8
  %1 = load i32, i32* %sz, align 4, !tbaa !8
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9allocSizeEi(%class.btAlignedObjectArray.0* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4, !tbaa !51
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !50
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %2, i32 %3
  %4 = bitcast %class.btTypedConstraint** %arrayidx to i8*
  %5 = bitcast i8* %4 to %class.btTypedConstraint**
  %6 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %_Val.addr, align 4, !tbaa !2
  %7 = load %class.btTypedConstraint*, %class.btTypedConstraint** %6, align 4, !tbaa !2
  store %class.btTypedConstraint* %7, %class.btTypedConstraint** %5, align 4, !tbaa !2
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size5, align 4, !tbaa !50
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size5, align 4, !tbaa !50
  %9 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #10
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZNK17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 8
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4, !tbaa !55
  ret %class.btRigidBody* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZNK17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 9
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4, !tbaa !56
  ret %class.btRigidBody* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv(%class.btAlignedObjectArray* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !121
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btCollisionObject**, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btCollisionObject*** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP17btCollisionObjectE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btCollisionObject**
  store %class.btCollisionObject** %3, %class.btCollisionObject*** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  %4 = load %class.btCollisionObject**, %class.btCollisionObject*** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4copyEiiPS1_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %class.btCollisionObject** %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !118
  %5 = load %class.btCollisionObject**, %class.btCollisionObject*** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btCollisionObject** %5, %class.btCollisionObject*** %m_data, align 4, !tbaa !119
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !121
  %7 = bitcast %class.btCollisionObject*** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP17btCollisionObjectE9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !8
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP17btCollisionObjectE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #1 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !8
  %call = call %class.btCollisionObject** @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator* %m_allocator, i32 %1, %class.btCollisionObject*** null)
  %2 = bitcast %class.btCollisionObject** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4copyEiiPS1_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %class.btCollisionObject** %dest) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btCollisionObject**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !8
  store i32 %end, i32* %end.addr, align 4, !tbaa !8
  store %class.btCollisionObject** %dest, %class.btCollisionObject*** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %start.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %end.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btCollisionObject**, %class.btCollisionObject*** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %4, i32 %5
  %6 = bitcast %class.btCollisionObject** %arrayidx to i8*
  %7 = bitcast i8* %6 to %class.btCollisionObject**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4, !tbaa !119
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx2 = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %8, i32 %9
  %10 = load %class.btCollisionObject*, %class.btCollisionObject** %arrayidx2, align 4, !tbaa !2
  store %class.btCollisionObject* %10, %class.btCollisionObject** %7, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #10
  ret void
}

define linkonce_odr hidden %class.btCollisionObject** @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator* %this, i32 %n, %class.btCollisionObject*** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btCollisionObject***, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  store %class.btCollisionObject*** %hint, %class.btCollisionObject**** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !8
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btCollisionObject**
  ret %class.btCollisionObject** %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #5

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.9* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !123
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.9* %this, i32 %_Count) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.9* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btPersistentManifold*** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.9* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btPersistentManifold**
  store %class.btPersistentManifold** %3, %class.btPersistentManifold*** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.9* %this1, i32 0, i32 %call3, %class.btPersistentManifold** %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.9* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.9* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !122
  %5 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  store %class.btPersistentManifold** %5, %class.btPersistentManifold*** %m_data, align 4, !tbaa !38
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !123
  %7 = bitcast %class.btPersistentManifold*** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi(%class.btAlignedObjectArray.9* %this, i32 %size) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !8
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.9* %this, i32 %size) #1 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !8
  %call = call %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.10* %m_allocator, i32 %1, %class.btPersistentManifold*** null)
  %2 = bitcast %class.btPersistentManifold** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.9* %this, i32 %start, i32 %end, %class.btPersistentManifold** %dest) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btPersistentManifold**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !8
  store i32 %end, i32* %end.addr, align 4, !tbaa !8
  store %class.btPersistentManifold** %dest, %class.btPersistentManifold*** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %start.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %end.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %4, i32 %5
  %6 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %7 = bitcast i8* %6 to %class.btPersistentManifold**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %8 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !38
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx2 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %8, i32 %9
  %10 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx2, align 4, !tbaa !2
  store %class.btPersistentManifold* %10, %class.btPersistentManifold** %7, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #10
  ret void
}

define linkonce_odr hidden %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.10* %this, i32 %n, %class.btPersistentManifold*** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.10*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btPersistentManifold***, align 4
  store %class.btAlignedAllocator.10* %this, %class.btAlignedAllocator.10** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  store %class.btPersistentManifold*** %hint, %class.btPersistentManifold**** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.10*, %class.btAlignedAllocator.10** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !8
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btPersistentManifold**
  ret %class.btPersistentManifold** %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE8capacityEv(%class.btAlignedObjectArray.0* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !125
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi(%class.btAlignedObjectArray.0* %this, i32 %_Count) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btTypedConstraint**, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btTypedConstraint*** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP17btTypedConstraintE8allocateEi(%class.btAlignedObjectArray.0* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btTypedConstraint**
  store %class.btTypedConstraint** %3, %class.btTypedConstraint*** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %4 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4copyEiiPS1_(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call3, %class.btTypedConstraint** %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !124
  %5 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %class.btTypedConstraint** %5, %class.btTypedConstraint*** %m_data, align 4, !tbaa !51
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !125
  %7 = bitcast %class.btTypedConstraint*** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9allocSizeEi(%class.btAlignedObjectArray.0* %this, i32 %size) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !8
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP17btTypedConstraintE8allocateEi(%class.btAlignedObjectArray.0* %this, i32 %size) #1 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !8
  %call = call %class.btTypedConstraint** @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.1* %m_allocator, i32 %1, %class.btTypedConstraint*** null)
  %2 = bitcast %class.btTypedConstraint** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4copyEiiPS1_(%class.btAlignedObjectArray.0* %this, i32 %start, i32 %end, %class.btTypedConstraint** %dest) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btTypedConstraint**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !8
  store i32 %end, i32* %end.addr, align 4, !tbaa !8
  store %class.btTypedConstraint** %dest, %class.btTypedConstraint*** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %start.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %end.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %4, i32 %5
  %6 = bitcast %class.btTypedConstraint** %arrayidx to i8*
  %7 = bitcast i8* %6 to %class.btTypedConstraint**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %8 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4, !tbaa !51
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx2 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %8, i32 %9
  %10 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx2, align 4, !tbaa !2
  store %class.btTypedConstraint* %10, %class.btTypedConstraint** %7, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #10
  ret void
}

define linkonce_odr hidden %class.btTypedConstraint** @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.1* %this, i32 %n, %class.btTypedConstraint*** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btTypedConstraint***, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  store %class.btTypedConstraint*** %hint, %class.btTypedConstraint**** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !8
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btTypedConstraint**
  ret %class.btTypedConstraint** %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE6resizeEiRKS1_(%class.btAlignedObjectArray* %this, i32 %newsize, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %fillData) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btCollisionObject**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !8
  store %class.btCollisionObject** %fillData, %class.btCollisionObject*** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !8
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %2 = load i32, i32* %curSize, align 4, !tbaa !8
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #10
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  store i32 %4, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %6 = load i32, i32* %curSize, align 4, !tbaa !8
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4, !tbaa !119
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #10
  %14 = load i32, i32* %curSize, align 4, !tbaa !8
  store i32 %14, i32* %i6, align 4, !tbaa !8
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !8
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #10
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %18 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data11, align 4, !tbaa !119
  %19 = load i32, i32* %i6, align 4, !tbaa !8
  %arrayidx12 = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %18, i32 %19
  %20 = bitcast %class.btCollisionObject** %arrayidx12 to i8*
  %21 = bitcast i8* %20 to %class.btCollisionObject**
  %22 = load %class.btCollisionObject**, %class.btCollisionObject*** %fillData.addr, align 4, !tbaa !2
  %23 = load %class.btCollisionObject*, %class.btCollisionObject** %22, align 4, !tbaa !2
  store %class.btCollisionObject* %23, %class.btCollisionObject** %21, align 4, !tbaa !2
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %24 = load i32, i32* %i6, align 4, !tbaa !8
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !8
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %25 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %25, i32* %m_size, align 4, !tbaa !120
  %26 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #10
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_(%class.btAlignedObjectArray.9* %this, i32 %newsize, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %fillData) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btPersistentManifold**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !8
  store %class.btPersistentManifold** %fillData, %class.btPersistentManifold*** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !8
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %2 = load i32, i32* %curSize, align 4, !tbaa !8
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #10
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  store i32 %4, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %6 = load i32, i32* %curSize, align 4, !tbaa !8
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %8 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !38
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.9* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #10
  %14 = load i32, i32* %curSize, align 4, !tbaa !8
  store i32 %14, i32* %i6, align 4, !tbaa !8
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !8
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #10
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %18 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data11, align 4, !tbaa !38
  %19 = load i32, i32* %i6, align 4, !tbaa !8
  %arrayidx12 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %18, i32 %19
  %20 = bitcast %class.btPersistentManifold** %arrayidx12 to i8*
  %21 = bitcast i8* %20 to %class.btPersistentManifold**
  %22 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %fillData.addr, align 4, !tbaa !2
  %23 = load %class.btPersistentManifold*, %class.btPersistentManifold** %22, align 4, !tbaa !2
  store %class.btPersistentManifold* %23, %class.btPersistentManifold** %21, align 4, !tbaa !2
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %24 = load i32, i32* %i6, align 4, !tbaa !8
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !8
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %25 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  store i32 %25, i32* %m_size, align 4, !tbaa !37
  %26 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !73
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !73
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !73
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !73
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !73
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !73
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !73
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !73
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !73
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !73
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !73
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !73
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !73
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !73
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !73
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !73
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray* %this, i32 %n) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4, !tbaa !119
  %1 = load i32, i32* %n.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %0, i32 %1
  ret %class.btCollisionObject** %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  ret %class.btQuadWord* %this1
}

define linkonce_odr hidden void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !73
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !73
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #10
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !73
  %3 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #10
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !73
  %4 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #10
  store float 1.000000e+00, float* %ref.tmp5, align 4, !tbaa !73
  %5 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #10
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !73
  %6 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #10
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !73
  %7 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #10
  store float 0.000000e+00, float* %ref.tmp8, align 4, !tbaa !73
  %8 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #10
  store float 1.000000e+00, float* %ref.tmp9, align 4, !tbaa !73
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %9 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #10
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #10
  %11 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #10
  %12 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #10
  %13 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #10
  %14 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #10
  %15 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #10
  %16 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #10
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !73
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !73
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !73
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !73
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !73
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !73
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !73
  ret void
}

define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

define linkonce_odr hidden void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %xs = alloca float, align 4
  %ys = alloca float, align 4
  %zs = alloca float, align 4
  %wx = alloca float, align 4
  %wy = alloca float, align 4
  %wz = alloca float, align 4
  %xx = alloca float, align 4
  %xy = alloca float, align 4
  %xz = alloca float, align 4
  %yy = alloca float, align 4
  %yz = alloca float, align 4
  %zz = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp34 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast float* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %1)
  store float %call, float* %d, align 4, !tbaa !73
  %2 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #10
  %3 = load float, float* %d, align 4, !tbaa !73
  %div = fdiv float 2.000000e+00, %3
  store float %div, float* %s, align 4, !tbaa !73
  %4 = bitcast float* %xs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #10
  %5 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %6 = bitcast %class.btQuaternion* %5 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %6)
  %7 = load float, float* %call2, align 4, !tbaa !73
  %8 = load float, float* %s, align 4, !tbaa !73
  %mul = fmul float %7, %8
  store float %mul, float* %xs, align 4, !tbaa !73
  %9 = bitcast float* %ys to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #10
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %11)
  %12 = load float, float* %call3, align 4, !tbaa !73
  %13 = load float, float* %s, align 4, !tbaa !73
  %mul4 = fmul float %12, %13
  store float %mul4, float* %ys, align 4, !tbaa !73
  %14 = bitcast float* %zs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #10
  %15 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %16 = bitcast %class.btQuaternion* %15 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %16)
  %17 = load float, float* %call5, align 4, !tbaa !73
  %18 = load float, float* %s, align 4, !tbaa !73
  %mul6 = fmul float %17, %18
  store float %mul6, float* %zs, align 4, !tbaa !73
  %19 = bitcast float* %wx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #10
  %20 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %21 = bitcast %class.btQuaternion* %20 to %class.btQuadWord*
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %21)
  %22 = load float, float* %call7, align 4, !tbaa !73
  %23 = load float, float* %xs, align 4, !tbaa !73
  %mul8 = fmul float %22, %23
  store float %mul8, float* %wx, align 4, !tbaa !73
  %24 = bitcast float* %wy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #10
  %25 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %26 = bitcast %class.btQuaternion* %25 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %26)
  %27 = load float, float* %call9, align 4, !tbaa !73
  %28 = load float, float* %ys, align 4, !tbaa !73
  %mul10 = fmul float %27, %28
  store float %mul10, float* %wy, align 4, !tbaa !73
  %29 = bitcast float* %wz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #10
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %31)
  %32 = load float, float* %call11, align 4, !tbaa !73
  %33 = load float, float* %zs, align 4, !tbaa !73
  %mul12 = fmul float %32, %33
  store float %mul12, float* %wz, align 4, !tbaa !73
  %34 = bitcast float* %xx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #10
  %35 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %36 = bitcast %class.btQuaternion* %35 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %36)
  %37 = load float, float* %call13, align 4, !tbaa !73
  %38 = load float, float* %xs, align 4, !tbaa !73
  %mul14 = fmul float %37, %38
  store float %mul14, float* %xx, align 4, !tbaa !73
  %39 = bitcast float* %xy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #10
  %40 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %41 = bitcast %class.btQuaternion* %40 to %class.btQuadWord*
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %41)
  %42 = load float, float* %call15, align 4, !tbaa !73
  %43 = load float, float* %ys, align 4, !tbaa !73
  %mul16 = fmul float %42, %43
  store float %mul16, float* %xy, align 4, !tbaa !73
  %44 = bitcast float* %xz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #10
  %45 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %46 = bitcast %class.btQuaternion* %45 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %46)
  %47 = load float, float* %call17, align 4, !tbaa !73
  %48 = load float, float* %zs, align 4, !tbaa !73
  %mul18 = fmul float %47, %48
  store float %mul18, float* %xz, align 4, !tbaa !73
  %49 = bitcast float* %yy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #10
  %50 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %51 = bitcast %class.btQuaternion* %50 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %51)
  %52 = load float, float* %call19, align 4, !tbaa !73
  %53 = load float, float* %ys, align 4, !tbaa !73
  %mul20 = fmul float %52, %53
  store float %mul20, float* %yy, align 4, !tbaa !73
  %54 = bitcast float* %yz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #10
  %55 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %56 = bitcast %class.btQuaternion* %55 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %56)
  %57 = load float, float* %call21, align 4, !tbaa !73
  %58 = load float, float* %zs, align 4, !tbaa !73
  %mul22 = fmul float %57, %58
  store float %mul22, float* %yz, align 4, !tbaa !73
  %59 = bitcast float* %zz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #10
  %60 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %61 = bitcast %class.btQuaternion* %60 to %class.btQuadWord*
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %61)
  %62 = load float, float* %call23, align 4, !tbaa !73
  %63 = load float, float* %zs, align 4, !tbaa !73
  %mul24 = fmul float %62, %63
  store float %mul24, float* %zz, align 4, !tbaa !73
  %64 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #10
  %65 = load float, float* %yy, align 4, !tbaa !73
  %66 = load float, float* %zz, align 4, !tbaa !73
  %add = fadd float %65, %66
  %sub = fsub float 1.000000e+00, %add
  store float %sub, float* %ref.tmp, align 4, !tbaa !73
  %67 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #10
  %68 = load float, float* %xy, align 4, !tbaa !73
  %69 = load float, float* %wz, align 4, !tbaa !73
  %sub26 = fsub float %68, %69
  store float %sub26, float* %ref.tmp25, align 4, !tbaa !73
  %70 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #10
  %71 = load float, float* %xz, align 4, !tbaa !73
  %72 = load float, float* %wy, align 4, !tbaa !73
  %add28 = fadd float %71, %72
  store float %add28, float* %ref.tmp27, align 4, !tbaa !73
  %73 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #10
  %74 = load float, float* %xy, align 4, !tbaa !73
  %75 = load float, float* %wz, align 4, !tbaa !73
  %add30 = fadd float %74, %75
  store float %add30, float* %ref.tmp29, align 4, !tbaa !73
  %76 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #10
  %77 = load float, float* %xx, align 4, !tbaa !73
  %78 = load float, float* %zz, align 4, !tbaa !73
  %add32 = fadd float %77, %78
  %sub33 = fsub float 1.000000e+00, %add32
  store float %sub33, float* %ref.tmp31, align 4, !tbaa !73
  %79 = bitcast float* %ref.tmp34 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #10
  %80 = load float, float* %yz, align 4, !tbaa !73
  %81 = load float, float* %wx, align 4, !tbaa !73
  %sub35 = fsub float %80, %81
  store float %sub35, float* %ref.tmp34, align 4, !tbaa !73
  %82 = bitcast float* %ref.tmp36 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #10
  %83 = load float, float* %xz, align 4, !tbaa !73
  %84 = load float, float* %wy, align 4, !tbaa !73
  %sub37 = fsub float %83, %84
  store float %sub37, float* %ref.tmp36, align 4, !tbaa !73
  %85 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %85) #10
  %86 = load float, float* %yz, align 4, !tbaa !73
  %87 = load float, float* %wx, align 4, !tbaa !73
  %add39 = fadd float %86, %87
  store float %add39, float* %ref.tmp38, align 4, !tbaa !73
  %88 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #10
  %89 = load float, float* %xx, align 4, !tbaa !73
  %90 = load float, float* %yy, align 4, !tbaa !73
  %add41 = fadd float %89, %90
  %sub42 = fsub float 1.000000e+00, %add41
  store float %sub42, float* %ref.tmp40, align 4, !tbaa !73
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp34, float* nonnull align 4 dereferenceable(4) %ref.tmp36, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp40)
  %91 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #10
  %92 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #10
  %93 = bitcast float* %ref.tmp36 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #10
  %94 = bitcast float* %ref.tmp34 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #10
  %95 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #10
  %96 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #10
  %97 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #10
  %98 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #10
  %99 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #10
  %100 = bitcast float* %zz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #10
  %101 = bitcast float* %yz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #10
  %102 = bitcast float* %yy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #10
  %103 = bitcast float* %xz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #10
  %104 = bitcast float* %xy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #10
  %105 = bitcast float* %xx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #10
  %106 = bitcast float* %wz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #10
  %107 = bitcast float* %wy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #10
  %108 = bitcast float* %wx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #10
  %109 = bitcast float* %zs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #10
  %110 = bitcast float* %ys to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #10
  %111 = bitcast float* %xs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #10
  %112 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #10
  %113 = bitcast float* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #10
  ret void
}

define linkonce_odr hidden float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #3 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !73
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call, align 4, !tbaa !73
  %mul = fmul float %1, %4
  %5 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %5, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %6 = load float, float* %arrayidx3, align 4, !tbaa !73
  %7 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %8 = bitcast %class.btQuaternion* %7 to %class.btQuadWord*
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %8)
  %9 = load float, float* %call4, align 4, !tbaa !73
  %mul5 = fmul float %6, %9
  %add = fadd float %mul, %mul5
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %11 = load float, float* %arrayidx7, align 4, !tbaa !73
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %13)
  %14 = load float, float* %call8, align 4, !tbaa !73
  %mul9 = fmul float %11, %14
  %add10 = fadd float %add, %mul9
  %15 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats11 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %15, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 3
  %16 = load float, float* %arrayidx12, align 4, !tbaa !73
  %17 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %18 = bitcast %class.btQuaternion* %17 to %class.btQuadWord*
  %m_floats13 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %18, i32 0, i32 0
  %arrayidx14 = getelementptr inbounds [4 x float], [4 x float]* %m_floats13, i32 0, i32 3
  %19 = load float, float* %arrayidx14, align 4, !tbaa !73
  %mul15 = fmul float %16, %19
  %add16 = fadd float %add10, %mul15
  ret float %add16
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  store float* %_w, float** %_w.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !73
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !73
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !73
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !73
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !73
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !73
  %6 = load float*, float** %_w.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !73
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4, !tbaa !73
  ret %class.btQuadWord* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !99
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !99
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !99
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !99
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK12btQuaternionRK9btVector3(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q, %class.btVector3* nonnull align 4 dereferenceable(16) %w) #1 comdat {
entry:
  %q.addr = alloca %class.btQuaternion*, align 4
  %w.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  store %class.btVector3* %w, %class.btVector3** %w.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %2 = bitcast %class.btQuaternion* %1 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %2)
  %3 = load float, float* %call, align 4, !tbaa !73
  %4 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %4)
  %5 = load float, float* %call1, align 4, !tbaa !73
  %mul = fmul float %3, %5
  %6 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %7)
  %8 = load float, float* %call2, align 4, !tbaa !73
  %9 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %9)
  %10 = load float, float* %call3, align 4, !tbaa !73
  %mul4 = fmul float %8, %10
  %add = fadd float %mul, %mul4
  %11 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %12 = bitcast %class.btQuaternion* %11 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %12)
  %13 = load float, float* %call5, align 4, !tbaa !73
  %14 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %14)
  %15 = load float, float* %call6, align 4, !tbaa !73
  %mul7 = fmul float %13, %15
  %sub = fsub float %add, %mul7
  store float %sub, float* %ref.tmp, align 4, !tbaa !73
  %16 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #10
  %17 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %18 = bitcast %class.btQuaternion* %17 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %18)
  %19 = load float, float* %call9, align 4, !tbaa !73
  %20 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %20)
  %21 = load float, float* %call10, align 4, !tbaa !73
  %mul11 = fmul float %19, %21
  %22 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %23 = bitcast %class.btQuaternion* %22 to %class.btQuadWord*
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %23)
  %24 = load float, float* %call12, align 4, !tbaa !73
  %25 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %25)
  %26 = load float, float* %call13, align 4, !tbaa !73
  %mul14 = fmul float %24, %26
  %add15 = fadd float %mul11, %mul14
  %27 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %28 = bitcast %class.btQuaternion* %27 to %class.btQuadWord*
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %28)
  %29 = load float, float* %call16, align 4, !tbaa !73
  %30 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %30)
  %31 = load float, float* %call17, align 4, !tbaa !73
  %mul18 = fmul float %29, %31
  %sub19 = fsub float %add15, %mul18
  store float %sub19, float* %ref.tmp8, align 4, !tbaa !73
  %32 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #10
  %33 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %34 = bitcast %class.btQuaternion* %33 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %34)
  %35 = load float, float* %call21, align 4, !tbaa !73
  %36 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %36)
  %37 = load float, float* %call22, align 4, !tbaa !73
  %mul23 = fmul float %35, %37
  %38 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %39 = bitcast %class.btQuaternion* %38 to %class.btQuadWord*
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %39)
  %40 = load float, float* %call24, align 4, !tbaa !73
  %41 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %41)
  %42 = load float, float* %call25, align 4, !tbaa !73
  %mul26 = fmul float %40, %42
  %add27 = fadd float %mul23, %mul26
  %43 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %44 = bitcast %class.btQuaternion* %43 to %class.btQuadWord*
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %44)
  %45 = load float, float* %call28, align 4, !tbaa !73
  %46 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %46)
  %47 = load float, float* %call29, align 4, !tbaa !73
  %mul30 = fmul float %45, %47
  %sub31 = fsub float %add27, %mul30
  store float %sub31, float* %ref.tmp20, align 4, !tbaa !73
  %48 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #10
  %49 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %50 = bitcast %class.btQuaternion* %49 to %class.btQuadWord*
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %50)
  %51 = load float, float* %call33, align 4, !tbaa !73
  %fneg = fneg float %51
  %52 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %52)
  %53 = load float, float* %call34, align 4, !tbaa !73
  %mul35 = fmul float %fneg, %53
  %54 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %55 = bitcast %class.btQuaternion* %54 to %class.btQuadWord*
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %55)
  %56 = load float, float* %call36, align 4, !tbaa !73
  %57 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %57)
  %58 = load float, float* %call37, align 4, !tbaa !73
  %mul38 = fmul float %56, %58
  %sub39 = fsub float %mul35, %mul38
  %59 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %60 = bitcast %class.btQuaternion* %59 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %60)
  %61 = load float, float* %call40, align 4, !tbaa !73
  %62 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %62)
  %63 = load float, float* %call41, align 4, !tbaa !73
  %mul42 = fmul float %61, %63
  %sub43 = fsub float %sub39, %mul42
  store float %sub43, float* %ref.tmp32, align 4, !tbaa !73
  %call44 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  %64 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #10
  %65 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #10
  %66 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #10
  %67 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #10
  ret void
}

define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp58 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  %2 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %2, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  %3 = load float, float* %arrayidx, align 4, !tbaa !73
  %4 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %5 = bitcast %class.btQuaternion* %4 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %5)
  %6 = load float, float* %call, align 4, !tbaa !73
  %mul = fmul float %3, %6
  %7 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %7, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %8 = load float, float* %arrayidx3, align 4, !tbaa !73
  %9 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %10 = bitcast %class.btQuaternion* %9 to %class.btQuadWord*
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 3
  %11 = load float, float* %arrayidx5, align 4, !tbaa !73
  %mul6 = fmul float %8, %11
  %add = fadd float %mul, %mul6
  %12 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats7 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %12, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 1
  %13 = load float, float* %arrayidx8, align 4, !tbaa !73
  %14 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %15 = bitcast %class.btQuaternion* %14 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %15)
  %16 = load float, float* %call9, align 4, !tbaa !73
  %mul10 = fmul float %13, %16
  %add11 = fadd float %add, %mul10
  %17 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats12 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %17, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %18 = load float, float* %arrayidx13, align 4, !tbaa !73
  %19 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %20 = bitcast %class.btQuaternion* %19 to %class.btQuadWord*
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %20)
  %21 = load float, float* %call14, align 4, !tbaa !73
  %mul15 = fmul float %18, %21
  %sub = fsub float %add11, %mul15
  store float %sub, float* %ref.tmp, align 4, !tbaa !73
  %22 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #10
  %23 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats17 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %23, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 3
  %24 = load float, float* %arrayidx18, align 4, !tbaa !73
  %25 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %26 = bitcast %class.btQuaternion* %25 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %26)
  %27 = load float, float* %call19, align 4, !tbaa !73
  %mul20 = fmul float %24, %27
  %28 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats21 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %28, i32 0, i32 0
  %arrayidx22 = getelementptr inbounds [4 x float], [4 x float]* %m_floats21, i32 0, i32 1
  %29 = load float, float* %arrayidx22, align 4, !tbaa !73
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %m_floats23 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %31, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [4 x float], [4 x float]* %m_floats23, i32 0, i32 3
  %32 = load float, float* %arrayidx24, align 4, !tbaa !73
  %mul25 = fmul float %29, %32
  %add26 = fadd float %mul20, %mul25
  %33 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats27 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %33, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 2
  %34 = load float, float* %arrayidx28, align 4, !tbaa !73
  %35 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %36 = bitcast %class.btQuaternion* %35 to %class.btQuadWord*
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %36)
  %37 = load float, float* %call29, align 4, !tbaa !73
  %mul30 = fmul float %34, %37
  %add31 = fadd float %add26, %mul30
  %38 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats32 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %38, i32 0, i32 0
  %arrayidx33 = getelementptr inbounds [4 x float], [4 x float]* %m_floats32, i32 0, i32 0
  %39 = load float, float* %arrayidx33, align 4, !tbaa !73
  %40 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %41 = bitcast %class.btQuaternion* %40 to %class.btQuadWord*
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %41)
  %42 = load float, float* %call34, align 4, !tbaa !73
  %mul35 = fmul float %39, %42
  %sub36 = fsub float %add31, %mul35
  store float %sub36, float* %ref.tmp16, align 4, !tbaa !73
  %43 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #10
  %44 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats38 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %44, i32 0, i32 0
  %arrayidx39 = getelementptr inbounds [4 x float], [4 x float]* %m_floats38, i32 0, i32 3
  %45 = load float, float* %arrayidx39, align 4, !tbaa !73
  %46 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %47 = bitcast %class.btQuaternion* %46 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %47)
  %48 = load float, float* %call40, align 4, !tbaa !73
  %mul41 = fmul float %45, %48
  %49 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats42 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %49, i32 0, i32 0
  %arrayidx43 = getelementptr inbounds [4 x float], [4 x float]* %m_floats42, i32 0, i32 2
  %50 = load float, float* %arrayidx43, align 4, !tbaa !73
  %51 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %52 = bitcast %class.btQuaternion* %51 to %class.btQuadWord*
  %m_floats44 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %52, i32 0, i32 0
  %arrayidx45 = getelementptr inbounds [4 x float], [4 x float]* %m_floats44, i32 0, i32 3
  %53 = load float, float* %arrayidx45, align 4, !tbaa !73
  %mul46 = fmul float %50, %53
  %add47 = fadd float %mul41, %mul46
  %54 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats48 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %54, i32 0, i32 0
  %arrayidx49 = getelementptr inbounds [4 x float], [4 x float]* %m_floats48, i32 0, i32 0
  %55 = load float, float* %arrayidx49, align 4, !tbaa !73
  %56 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %57 = bitcast %class.btQuaternion* %56 to %class.btQuadWord*
  %call50 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %57)
  %58 = load float, float* %call50, align 4, !tbaa !73
  %mul51 = fmul float %55, %58
  %add52 = fadd float %add47, %mul51
  %59 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats53 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %59, i32 0, i32 0
  %arrayidx54 = getelementptr inbounds [4 x float], [4 x float]* %m_floats53, i32 0, i32 1
  %60 = load float, float* %arrayidx54, align 4, !tbaa !73
  %61 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %62 = bitcast %class.btQuaternion* %61 to %class.btQuadWord*
  %call55 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %62)
  %63 = load float, float* %call55, align 4, !tbaa !73
  %mul56 = fmul float %60, %63
  %sub57 = fsub float %add52, %mul56
  store float %sub57, float* %ref.tmp37, align 4, !tbaa !73
  %64 = bitcast float* %ref.tmp58 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #10
  %65 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats59 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %65, i32 0, i32 0
  %arrayidx60 = getelementptr inbounds [4 x float], [4 x float]* %m_floats59, i32 0, i32 3
  %66 = load float, float* %arrayidx60, align 4, !tbaa !73
  %67 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %68 = bitcast %class.btQuaternion* %67 to %class.btQuadWord*
  %m_floats61 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %68, i32 0, i32 0
  %arrayidx62 = getelementptr inbounds [4 x float], [4 x float]* %m_floats61, i32 0, i32 3
  %69 = load float, float* %arrayidx62, align 4, !tbaa !73
  %mul63 = fmul float %66, %69
  %70 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats64 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %70, i32 0, i32 0
  %arrayidx65 = getelementptr inbounds [4 x float], [4 x float]* %m_floats64, i32 0, i32 0
  %71 = load float, float* %arrayidx65, align 4, !tbaa !73
  %72 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %73 = bitcast %class.btQuaternion* %72 to %class.btQuadWord*
  %call66 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %73)
  %74 = load float, float* %call66, align 4, !tbaa !73
  %mul67 = fmul float %71, %74
  %sub68 = fsub float %mul63, %mul67
  %75 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats69 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %75, i32 0, i32 0
  %arrayidx70 = getelementptr inbounds [4 x float], [4 x float]* %m_floats69, i32 0, i32 1
  %76 = load float, float* %arrayidx70, align 4, !tbaa !73
  %77 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %78 = bitcast %class.btQuaternion* %77 to %class.btQuadWord*
  %call71 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %78)
  %79 = load float, float* %call71, align 4, !tbaa !73
  %mul72 = fmul float %76, %79
  %sub73 = fsub float %sub68, %mul72
  %80 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats74 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %80, i32 0, i32 0
  %arrayidx75 = getelementptr inbounds [4 x float], [4 x float]* %m_floats74, i32 0, i32 2
  %81 = load float, float* %arrayidx75, align 4, !tbaa !73
  %82 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %83 = bitcast %class.btQuaternion* %82 to %class.btQuadWord*
  %call76 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %83)
  %84 = load float, float* %call76, align 4, !tbaa !73
  %mul77 = fmul float %81, %84
  %sub78 = fsub float %sub73, %mul77
  store float %sub78, float* %ref.tmp58, align 4, !tbaa !73
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp37, float* nonnull align 4 dereferenceable(4) %ref.tmp58)
  %85 = bitcast float* %ref.tmp58 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #10
  %86 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #10
  %87 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #10
  %88 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #10
  ret %class.btQuaternion* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getXEv(%class.btQuadWord* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getYEv(%class.btQuadWord* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getZEv(%class.btQuadWord* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) #4 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  store float* %_w, float** %_w.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !73
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !73
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !73
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !73
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !73
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !73
  %6 = load float*, float** %_w.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !73
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4, !tbaa !73
  ret void
}

declare void @_ZN23btDiscreteDynamicsWorld13updateActionsEf(%class.btDiscreteDynamicsWorld*, float) #5

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.37* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator.37* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.37*, align 4
  store %class.btAlignedAllocator.37* %this, %class.btAlignedAllocator.37** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.37*, %class.btAlignedAllocator.37** %this.addr, align 4
  ret %class.btAlignedAllocator.37* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray.36* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.36*, align 4
  store %class.btAlignedObjectArray.36* %this, %class.btAlignedObjectArray.36** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.36*, %class.btAlignedObjectArray.36** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.36, %class.btAlignedObjectArray.36* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !126
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.36, %class.btAlignedObjectArray.36* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data, align 4, !tbaa !98
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.36, %class.btAlignedObjectArray.36* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !101
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.36, %class.btAlignedObjectArray.36* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !127
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.33* @_ZN18btAlignedAllocatorIfLj16EEC2Ev(%class.btAlignedAllocator.33* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.33*, align 4
  store %class.btAlignedAllocator.33* %this, %class.btAlignedAllocator.33** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.33*, %class.btAlignedAllocator.33** %this.addr, align 4
  ret %class.btAlignedAllocator.33* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.32* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.32*, align 4
  store %class.btAlignedObjectArray.32* %this, %class.btAlignedObjectArray.32** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.32*, %class.btAlignedObjectArray.32** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.32, %class.btAlignedObjectArray.32* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !128
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.32, %class.btAlignedObjectArray.32* %this1, i32 0, i32 4
  store float* null, float** %m_data, align 4, !tbaa !96
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.32, %class.btAlignedObjectArray.32* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !97
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.32, %class.btAlignedObjectArray.32* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !129
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray.36* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.36*, align 4
  store %class.btAlignedObjectArray.36* %this, %class.btAlignedObjectArray.36** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.36*, %class.btAlignedObjectArray.36** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.36* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.36* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.36* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray.36* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.36* %this, i32 %first, i32 %last) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.36*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.36* %this, %class.btAlignedObjectArray.36** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !8
  store i32 %last, i32* %last.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.36*, %class.btAlignedObjectArray.36** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %first.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %last.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.36, %class.btAlignedObjectArray.36* %this1, i32 0, i32 4
  %4 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !98
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.36* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.36*, align 4
  store %class.btAlignedObjectArray.36* %this, %class.btAlignedObjectArray.36** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.36*, %class.btAlignedObjectArray.36** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.36, %class.btAlignedObjectArray.36* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !101
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.36* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.36*, align 4
  store %class.btAlignedObjectArray.36* %this, %class.btAlignedObjectArray.36** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.36*, %class.btAlignedObjectArray.36** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.36, %class.btAlignedObjectArray.36* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !98
  %tobool = icmp ne %class.btVector3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.36, %class.btAlignedObjectArray.36* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !126, !range !54
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.36, %class.btAlignedObjectArray.36* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.36, %class.btAlignedObjectArray.36* %this1, i32 0, i32 4
  %2 = load %class.btVector3*, %class.btVector3** %m_data4, align 4, !tbaa !98
  call void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.37* %m_allocator, %class.btVector3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.36, %class.btAlignedObjectArray.36* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data5, align 4, !tbaa !98
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.37* %this, %class.btVector3* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.37*, align 4
  %ptr.addr = alloca %class.btVector3*, align 4
  store %class.btAlignedAllocator.37* %this, %class.btAlignedAllocator.37** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %ptr, %class.btVector3** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.37*, %class.btAlignedAllocator.37** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVector3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE5clearEv(%class.btAlignedObjectArray.32* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.32*, align 4
  store %class.btAlignedObjectArray.32* %this, %class.btAlignedObjectArray.32** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.32*, %class.btAlignedObjectArray.32** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.32* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.32* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.32* %this1)
  call void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.32* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.32* %this, i32 %first, i32 %last) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.32*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.32* %this, %class.btAlignedObjectArray.32** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !8
  store i32 %last, i32* %last.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.32*, %class.btAlignedObjectArray.32** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %first.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %last.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.32, %class.btAlignedObjectArray.32* %this1, i32 0, i32 4
  %4 = load float*, float** %m_data, align 4, !tbaa !96
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds float, float* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.32* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.32*, align 4
  store %class.btAlignedObjectArray.32* %this, %class.btAlignedObjectArray.32** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.32*, %class.btAlignedObjectArray.32** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.32, %class.btAlignedObjectArray.32* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !97
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.32* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.32*, align 4
  store %class.btAlignedObjectArray.32* %this, %class.btAlignedObjectArray.32** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.32*, %class.btAlignedObjectArray.32** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.32, %class.btAlignedObjectArray.32* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4, !tbaa !96
  %tobool = icmp ne float* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.32, %class.btAlignedObjectArray.32* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !128, !range !54
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.32, %class.btAlignedObjectArray.32* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.32, %class.btAlignedObjectArray.32* %this1, i32 0, i32 4
  %2 = load float*, float** %m_data4, align 4, !tbaa !96
  call void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator.33* %m_allocator, float* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.32, %class.btAlignedObjectArray.32* %this1, i32 0, i32 4
  store float* null, float** %m_data5, align 4, !tbaa !96
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator.33* %this, float* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.33*, align 4
  %ptr.addr = alloca float*, align 4
  store %class.btAlignedAllocator.33* %this, %class.btAlignedAllocator.33** %this.addr, align 4, !tbaa !2
  store float* %ptr, float** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.33*, %class.btAlignedAllocator.33** %this.addr, align 4
  %0 = load float*, float** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast float* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE8capacityEv(%class.btAlignedObjectArray.21* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.21*, align 4
  store %class.btAlignedObjectArray.21* %this, %class.btAlignedObjectArray.21** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.21*, %class.btAlignedObjectArray.21** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.21* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !130
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP11btMultiBodyE7reserveEi(%class.btAlignedObjectArray.21* %this, i32 %_Count) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.21*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btMultiBody**, align 4
  store %class.btAlignedObjectArray.21* %this, %class.btAlignedObjectArray.21** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.21*, %class.btAlignedObjectArray.21** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE8capacityEv(%class.btAlignedObjectArray.21* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btMultiBody*** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP11btMultiBodyE8allocateEi(%class.btAlignedObjectArray.21* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btMultiBody**
  store %class.btMultiBody** %3, %class.btMultiBody*** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.21* %this1)
  %4 = load %class.btMultiBody**, %class.btMultiBody*** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4copyEiiPS1_(%class.btAlignedObjectArray.21* %this1, i32 0, i32 %call3, %class.btMultiBody** %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.21* %this1)
  call void @_ZN20btAlignedObjectArrayIP11btMultiBodyE7destroyEii(%class.btAlignedObjectArray.21* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP11btMultiBodyE10deallocateEv(%class.btAlignedObjectArray.21* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.21* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !131
  %5 = load %class.btMultiBody**, %class.btMultiBody*** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.21* %this1, i32 0, i32 4
  store %class.btMultiBody** %5, %class.btMultiBody*** %m_data, align 4, !tbaa !10
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.21* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !130
  %7 = bitcast %class.btMultiBody*** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP11btMultiBodyE9allocSizeEi(%class.btAlignedObjectArray.21* %this, i32 %size) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.21*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.21* %this, %class.btAlignedObjectArray.21** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.21*, %class.btAlignedObjectArray.21** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !8
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP11btMultiBodyE8allocateEi(%class.btAlignedObjectArray.21* %this, i32 %size) #1 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.21*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.21* %this, %class.btAlignedObjectArray.21** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.21*, %class.btAlignedObjectArray.21** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.21* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !8
  %call = call %class.btMultiBody** @_ZN18btAlignedAllocatorIP11btMultiBodyLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.22* %m_allocator, i32 %1, %class.btMultiBody*** null)
  %2 = bitcast %class.btMultiBody** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4copyEiiPS1_(%class.btAlignedObjectArray.21* %this, i32 %start, i32 %end, %class.btMultiBody** %dest) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.21*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btMultiBody**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.21* %this, %class.btAlignedObjectArray.21** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !8
  store i32 %end, i32* %end.addr, align 4, !tbaa !8
  store %class.btMultiBody** %dest, %class.btMultiBody*** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.21*, %class.btAlignedObjectArray.21** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %start.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %end.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btMultiBody**, %class.btMultiBody*** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btMultiBody*, %class.btMultiBody** %4, i32 %5
  %6 = bitcast %class.btMultiBody** %arrayidx to i8*
  %7 = bitcast i8* %6 to %class.btMultiBody**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.21* %this1, i32 0, i32 4
  %8 = load %class.btMultiBody**, %class.btMultiBody*** %m_data, align 4, !tbaa !10
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx2 = getelementptr inbounds %class.btMultiBody*, %class.btMultiBody** %8, i32 %9
  %10 = load %class.btMultiBody*, %class.btMultiBody** %arrayidx2, align 4, !tbaa !2
  store %class.btMultiBody* %10, %class.btMultiBody** %7, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP11btMultiBodyE7destroyEii(%class.btAlignedObjectArray.21* %this, i32 %first, i32 %last) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.21*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.21* %this, %class.btAlignedObjectArray.21** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !8
  store i32 %last, i32* %last.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.21*, %class.btAlignedObjectArray.21** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %first.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %last.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.21* %this1, i32 0, i32 4
  %4 = load %class.btMultiBody**, %class.btMultiBody*** %m_data, align 4, !tbaa !10
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btMultiBody*, %class.btMultiBody** %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP11btMultiBodyE10deallocateEv(%class.btAlignedObjectArray.21* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.21*, align 4
  store %class.btAlignedObjectArray.21* %this, %class.btAlignedObjectArray.21** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.21*, %class.btAlignedObjectArray.21** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.21* %this1, i32 0, i32 4
  %0 = load %class.btMultiBody**, %class.btMultiBody*** %m_data, align 4, !tbaa !10
  %tobool = icmp ne %class.btMultiBody** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.21* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !131, !range !54
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.21* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.21* %this1, i32 0, i32 4
  %2 = load %class.btMultiBody**, %class.btMultiBody*** %m_data4, align 4, !tbaa !10
  call void @_ZN18btAlignedAllocatorIP11btMultiBodyLj16EE10deallocateEPS1_(%class.btAlignedAllocator.22* %m_allocator, %class.btMultiBody** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.21* %this1, i32 0, i32 4
  store %class.btMultiBody** null, %class.btMultiBody*** %m_data5, align 4, !tbaa !10
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden %class.btMultiBody** @_ZN18btAlignedAllocatorIP11btMultiBodyLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.22* %this, i32 %n, %class.btMultiBody*** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.22*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btMultiBody***, align 4
  store %class.btAlignedAllocator.22* %this, %class.btAlignedAllocator.22** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  store %class.btMultiBody*** %hint, %class.btMultiBody**** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.22*, %class.btAlignedAllocator.22** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !8
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btMultiBody**
  ret %class.btMultiBody** %1
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP11btMultiBodyLj16EE10deallocateEPS1_(%class.btAlignedAllocator.22* %this, %class.btMultiBody** %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.22*, align 4
  %ptr.addr = alloca %class.btMultiBody**, align 4
  store %class.btAlignedAllocator.22* %this, %class.btAlignedAllocator.22** %this.addr, align 4, !tbaa !2
  store %class.btMultiBody** %ptr, %class.btMultiBody*** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.22*, %class.btAlignedAllocator.22** %this.addr, align 4
  %0 = load %class.btMultiBody**, %class.btMultiBody*** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btMultiBody** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE16findLinearSearchERKS1_(%class.btAlignedObjectArray.21* %this, %class.btMultiBody** nonnull align 4 dereferenceable(4) %key) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.21*, align 4
  %key.addr = alloca %class.btMultiBody**, align 4
  %index = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.21* %this, %class.btAlignedObjectArray.21** %this.addr, align 4, !tbaa !2
  store %class.btMultiBody** %key, %class.btMultiBody*** %key.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.21*, %class.btAlignedObjectArray.21** %this.addr, align 4
  %0 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %call = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.21* %this1)
  store i32 %call, i32* %index, align 4, !tbaa !8
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.21* %this1)
  %cmp = icmp slt i32 %2, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.21* %this1, i32 0, i32 4
  %3 = load %class.btMultiBody**, %class.btMultiBody*** %m_data, align 4, !tbaa !10
  %4 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btMultiBody*, %class.btMultiBody** %3, i32 %4
  %5 = load %class.btMultiBody*, %class.btMultiBody** %arrayidx, align 4, !tbaa !2
  %6 = load %class.btMultiBody**, %class.btMultiBody*** %key.addr, align 4, !tbaa !2
  %7 = load %class.btMultiBody*, %class.btMultiBody** %6, align 4, !tbaa !2
  %cmp3 = icmp eq %class.btMultiBody* %5, %7
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4, !tbaa !8
  store i32 %8, i32* %index, align 4, !tbaa !8
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %10 = load i32, i32* %index, align 4, !tbaa !8
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #10
  %12 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #10
  ret i32 %10
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP11btMultiBodyE4swapEii(%class.btAlignedObjectArray.21* %this, i32 %index0, i32 %index1) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.21*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %class.btMultiBody*, align 4
  store %class.btAlignedObjectArray.21* %this, %class.btAlignedObjectArray.21** %this.addr, align 4, !tbaa !2
  store i32 %index0, i32* %index0.addr, align 4, !tbaa !8
  store i32 %index1, i32* %index1.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.21*, %class.btAlignedObjectArray.21** %this.addr, align 4
  %0 = bitcast %class.btMultiBody** %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.21* %this1, i32 0, i32 4
  %1 = load %class.btMultiBody**, %class.btMultiBody*** %m_data, align 4, !tbaa !10
  %2 = load i32, i32* %index0.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btMultiBody*, %class.btMultiBody** %1, i32 %2
  %3 = load %class.btMultiBody*, %class.btMultiBody** %arrayidx, align 4, !tbaa !2
  store %class.btMultiBody* %3, %class.btMultiBody** %temp, align 4, !tbaa !2
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.21* %this1, i32 0, i32 4
  %4 = load %class.btMultiBody**, %class.btMultiBody*** %m_data2, align 4, !tbaa !10
  %5 = load i32, i32* %index1.addr, align 4, !tbaa !8
  %arrayidx3 = getelementptr inbounds %class.btMultiBody*, %class.btMultiBody** %4, i32 %5
  %6 = load %class.btMultiBody*, %class.btMultiBody** %arrayidx3, align 4, !tbaa !2
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.21* %this1, i32 0, i32 4
  %7 = load %class.btMultiBody**, %class.btMultiBody*** %m_data4, align 4, !tbaa !10
  %8 = load i32, i32* %index0.addr, align 4, !tbaa !8
  %arrayidx5 = getelementptr inbounds %class.btMultiBody*, %class.btMultiBody** %7, i32 %8
  store %class.btMultiBody* %6, %class.btMultiBody** %arrayidx5, align 4, !tbaa !2
  %9 = load %class.btMultiBody*, %class.btMultiBody** %temp, align 4, !tbaa !2
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.21* %this1, i32 0, i32 4
  %10 = load %class.btMultiBody**, %class.btMultiBody*** %m_data6, align 4, !tbaa !10
  %11 = load i32, i32* %index1.addr, align 4, !tbaa !8
  %arrayidx7 = getelementptr inbounds %class.btMultiBody*, %class.btMultiBody** %10, i32 %11
  store %class.btMultiBody* %9, %class.btMultiBody** %arrayidx7, align 4, !tbaa !2
  %12 = bitcast %class.btMultiBody** %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP11btMultiBodyE8pop_backEv(%class.btAlignedObjectArray.21* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.21*, align 4
  store %class.btAlignedObjectArray.21* %this, %class.btAlignedObjectArray.21** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.21*, %class.btAlignedObjectArray.21** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.21* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !14
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4, !tbaa !14
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.21* %this1, i32 0, i32 4
  %1 = load %class.btMultiBody**, %class.btMultiBody*** %m_data, align 4, !tbaa !10
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.21* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4, !tbaa !14
  %arrayidx = getelementptr inbounds %class.btMultiBody*, %class.btMultiBody** %1, i32 %2
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.46* @_ZN18btAlignedAllocatorIP21btMultiBodyConstraintLj16EEC2Ev(%class.btAlignedAllocator.46* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.46*, align 4
  store %class.btAlignedAllocator.46* %this, %class.btAlignedAllocator.46** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.46*, %class.btAlignedAllocator.46** %this.addr, align 4
  ret %class.btAlignedAllocator.46* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE4initEv(%class.btAlignedObjectArray.45* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.45*, align 4
  store %class.btAlignedObjectArray.45* %this, %class.btAlignedObjectArray.45** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.45*, %class.btAlignedObjectArray.45** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !132
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 4
  store %class.btMultiBodyConstraint** null, %class.btMultiBodyConstraint*** %m_data, align 4, !tbaa !72
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !69
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !133
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE5clearEv(%class.btAlignedObjectArray.45* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.45*, align 4
  store %class.btAlignedObjectArray.45* %this, %class.btAlignedObjectArray.45** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.45*, %class.btAlignedObjectArray.45** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.45* %this1)
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE7destroyEii(%class.btAlignedObjectArray.45* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE10deallocateEv(%class.btAlignedObjectArray.45* %this1)
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE4initEv(%class.btAlignedObjectArray.45* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE7destroyEii(%class.btAlignedObjectArray.45* %this, i32 %first, i32 %last) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.45*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.45* %this, %class.btAlignedObjectArray.45** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !8
  store i32 %last, i32* %last.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.45*, %class.btAlignedObjectArray.45** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %first.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %last.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 4
  %4 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data, align 4, !tbaa !72
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE10deallocateEv(%class.btAlignedObjectArray.45* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.45*, align 4
  store %class.btAlignedObjectArray.45* %this, %class.btAlignedObjectArray.45** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.45*, %class.btAlignedObjectArray.45** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 4
  %0 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data, align 4, !tbaa !72
  %tobool = icmp ne %class.btMultiBodyConstraint** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !132, !range !54
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 4
  %2 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data4, align 4, !tbaa !72
  call void @_ZN18btAlignedAllocatorIP21btMultiBodyConstraintLj16EE10deallocateEPS1_(%class.btAlignedAllocator.46* %m_allocator, %class.btMultiBodyConstraint** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 4
  store %class.btMultiBodyConstraint** null, %class.btMultiBodyConstraint*** %m_data5, align 4, !tbaa !72
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP21btMultiBodyConstraintLj16EE10deallocateEPS1_(%class.btAlignedAllocator.46* %this, %class.btMultiBodyConstraint** %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.46*, align 4
  %ptr.addr = alloca %class.btMultiBodyConstraint**, align 4
  store %class.btAlignedAllocator.46* %this, %class.btAlignedAllocator.46** %this.addr, align 4, !tbaa !2
  store %class.btMultiBodyConstraint** %ptr, %class.btMultiBodyConstraint*** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.46*, %class.btAlignedAllocator.46** %this.addr, align 4
  %0 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btMultiBodyConstraint** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE7reserveEi(%class.btAlignedObjectArray.45* %this, i32 %_Count) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.45*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btMultiBodyConstraint**, align 4
  store %class.btAlignedObjectArray.45* %this, %class.btAlignedObjectArray.45** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.45*, %class.btAlignedObjectArray.45** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE8capacityEv(%class.btAlignedObjectArray.45* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btMultiBodyConstraint*** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE8allocateEi(%class.btAlignedObjectArray.45* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btMultiBodyConstraint**
  store %class.btMultiBodyConstraint** %3, %class.btMultiBodyConstraint*** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.45* %this1)
  %4 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4copyEiiPS1_(%class.btAlignedObjectArray.45* %this1, i32 0, i32 %call3, %class.btMultiBodyConstraint** %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.45* %this1)
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE7destroyEii(%class.btAlignedObjectArray.45* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE10deallocateEv(%class.btAlignedObjectArray.45* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !132
  %5 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 4
  store %class.btMultiBodyConstraint** %5, %class.btMultiBodyConstraint*** %m_data, align 4, !tbaa !72
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !133
  %7 = bitcast %class.btMultiBodyConstraint*** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE8capacityEv(%class.btAlignedObjectArray.45* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.45*, align 4
  store %class.btAlignedObjectArray.45* %this, %class.btAlignedObjectArray.45** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.45*, %class.btAlignedObjectArray.45** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !133
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE8allocateEi(%class.btAlignedObjectArray.45* %this, i32 %size) #1 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.45*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.45* %this, %class.btAlignedObjectArray.45** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.45*, %class.btAlignedObjectArray.45** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !8
  %call = call %class.btMultiBodyConstraint** @_ZN18btAlignedAllocatorIP21btMultiBodyConstraintLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.46* %m_allocator, i32 %1, %class.btMultiBodyConstraint*** null)
  %2 = bitcast %class.btMultiBodyConstraint** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4copyEiiPS1_(%class.btAlignedObjectArray.45* %this, i32 %start, i32 %end, %class.btMultiBodyConstraint** %dest) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.45*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btMultiBodyConstraint**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.45* %this, %class.btAlignedObjectArray.45** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !8
  store i32 %end, i32* %end.addr, align 4, !tbaa !8
  store %class.btMultiBodyConstraint** %dest, %class.btMultiBodyConstraint*** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.45*, %class.btAlignedObjectArray.45** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %start.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %end.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %4, i32 %5
  %6 = bitcast %class.btMultiBodyConstraint** %arrayidx to i8*
  %7 = bitcast i8* %6 to %class.btMultiBodyConstraint**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 4
  %8 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data, align 4, !tbaa !72
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx2 = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %8, i32 %9
  %10 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %arrayidx2, align 4, !tbaa !2
  store %class.btMultiBodyConstraint* %10, %class.btMultiBodyConstraint** %7, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #10
  ret void
}

define linkonce_odr hidden %class.btMultiBodyConstraint** @_ZN18btAlignedAllocatorIP21btMultiBodyConstraintLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.46* %this, i32 %n, %class.btMultiBodyConstraint*** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.46*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btMultiBodyConstraint***, align 4
  store %class.btAlignedAllocator.46* %this, %class.btAlignedAllocator.46** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  store %class.btMultiBodyConstraint*** %hint, %class.btMultiBodyConstraint**** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.46*, %class.btAlignedAllocator.46** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !8
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btMultiBodyConstraint**
  ret %class.btMultiBodyConstraint** %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE9allocSizeEi(%class.btAlignedObjectArray.45* %this, i32 %size) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.45*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.45* %this, %class.btAlignedObjectArray.45** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.45*, %class.btAlignedObjectArray.45** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !8
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.22* @_ZN18btAlignedAllocatorIP11btMultiBodyLj16EEC2Ev(%class.btAlignedAllocator.22* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.22*, align 4
  store %class.btAlignedAllocator.22* %this, %class.btAlignedAllocator.22** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.22*, %class.btAlignedAllocator.22** %this.addr, align 4
  ret %class.btAlignedAllocator.22* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP11btMultiBodyE4initEv(%class.btAlignedObjectArray.21* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.21*, align 4
  store %class.btAlignedObjectArray.21* %this, %class.btAlignedObjectArray.21** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.21*, %class.btAlignedObjectArray.21** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.21* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !131
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.21* %this1, i32 0, i32 4
  store %class.btMultiBody** null, %class.btMultiBody*** %m_data, align 4, !tbaa !10
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.21* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !14
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.21, %class.btAlignedObjectArray.21* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !130
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP11btMultiBodyE5clearEv(%class.btAlignedObjectArray.21* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.21*, align 4
  store %class.btAlignedObjectArray.21* %this, %class.btAlignedObjectArray.21** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.21*, %class.btAlignedObjectArray.21** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.21* %this1)
  call void @_ZN20btAlignedObjectArrayIP11btMultiBodyE7destroyEii(%class.btAlignedObjectArray.21* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP11btMultiBodyE10deallocateEv(%class.btAlignedObjectArray.21* %this1)
  call void @_ZN20btAlignedObjectArrayIP11btMultiBodyE4initEv(%class.btAlignedObjectArray.21* %this1)
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.41* @_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EEC2Ev(%class.btAlignedAllocator.41* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.41*, align 4
  store %class.btAlignedAllocator.41* %this, %class.btAlignedAllocator.41** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.41*, %class.btAlignedAllocator.41** %this.addr, align 4
  ret %class.btAlignedAllocator.41* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btMatrix3x3E4initEv(%class.btAlignedObjectArray.40* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.40*, align 4
  store %class.btAlignedObjectArray.40* %this, %class.btAlignedObjectArray.40** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.40*, %class.btAlignedObjectArray.40** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.40, %class.btAlignedObjectArray.40* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !134
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.40, %class.btAlignedObjectArray.40* %this1, i32 0, i32 4
  store %class.btMatrix3x3* null, %class.btMatrix3x3** %m_data, align 4, !tbaa !102
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.40, %class.btAlignedObjectArray.40* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !103
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.40, %class.btAlignedObjectArray.40* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !135
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btMatrix3x3E5clearEv(%class.btAlignedObjectArray.40* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.40*, align 4
  store %class.btAlignedObjectArray.40* %this, %class.btAlignedObjectArray.40** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.40*, %class.btAlignedObjectArray.40** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI11btMatrix3x3E4sizeEv(%class.btAlignedObjectArray.40* %this1)
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E7destroyEii(%class.btAlignedObjectArray.40* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E10deallocateEv(%class.btAlignedObjectArray.40* %this1)
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E4initEv(%class.btAlignedObjectArray.40* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btMatrix3x3E7destroyEii(%class.btAlignedObjectArray.40* %this, i32 %first, i32 %last) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.40*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.40* %this, %class.btAlignedObjectArray.40** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !8
  store i32 %last, i32* %last.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.40*, %class.btAlignedObjectArray.40** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %first.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %last.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.40, %class.btAlignedObjectArray.40* %this1, i32 0, i32 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m_data, align 4, !tbaa !102
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI11btMatrix3x3E4sizeEv(%class.btAlignedObjectArray.40* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.40*, align 4
  store %class.btAlignedObjectArray.40* %this, %class.btAlignedObjectArray.40** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.40*, %class.btAlignedObjectArray.40** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.40, %class.btAlignedObjectArray.40* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !103
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btMatrix3x3E10deallocateEv(%class.btAlignedObjectArray.40* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.40*, align 4
  store %class.btAlignedObjectArray.40* %this, %class.btAlignedObjectArray.40** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.40*, %class.btAlignedObjectArray.40** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.40, %class.btAlignedObjectArray.40* %this1, i32 0, i32 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m_data, align 4, !tbaa !102
  %tobool = icmp ne %class.btMatrix3x3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.40, %class.btAlignedObjectArray.40* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !134, !range !54
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.40, %class.btAlignedObjectArray.40* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.40, %class.btAlignedObjectArray.40* %this1, i32 0, i32 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m_data4, align 4, !tbaa !102
  call void @_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.41* %m_allocator, %class.btMatrix3x3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.40, %class.btAlignedObjectArray.40* %this1, i32 0, i32 4
  store %class.btMatrix3x3* null, %class.btMatrix3x3** %m_data5, align 4, !tbaa !102
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.41* %this, %class.btMatrix3x3* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.41*, align 4
  %ptr.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btAlignedAllocator.41* %this, %class.btAlignedAllocator.41** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %ptr, %class.btMatrix3x3** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.41*, %class.btAlignedAllocator.41** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btMatrix3x3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE17quickSortInternalI34btSortConstraintOnIslandPredicate2EEvRKT_ii(%class.btAlignedObjectArray.0* %this, %class.btSortConstraintOnIslandPredicate2* nonnull align 1 dereferenceable(1) %CompareFunc, i32 %lo, i32 %hi) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %CompareFunc.addr = alloca %class.btSortConstraintOnIslandPredicate2*, align 4
  %lo.addr = alloca i32, align 4
  %hi.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %x = alloca %class.btTypedConstraint*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store %class.btSortConstraintOnIslandPredicate2* %CompareFunc, %class.btSortConstraintOnIslandPredicate2** %CompareFunc.addr, align 4, !tbaa !2
  store i32 %lo, i32* %lo.addr, align 4, !tbaa !8
  store i32 %hi, i32* %hi.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %lo.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  %2 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #10
  %3 = load i32, i32* %hi.addr, align 4, !tbaa !8
  store i32 %3, i32* %j, align 4, !tbaa !8
  %4 = bitcast %class.btTypedConstraint** %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #10
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %5 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4, !tbaa !51
  %6 = load i32, i32* %lo.addr, align 4, !tbaa !8
  %7 = load i32, i32* %hi.addr, align 4, !tbaa !8
  %add = add nsw i32 %6, %7
  %div = sdiv i32 %add, 2
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %5, i32 %div
  %8 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx, align 4, !tbaa !2
  store %class.btTypedConstraint* %8, %class.btTypedConstraint** %x, align 4, !tbaa !2
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %do.body
  %9 = load %class.btSortConstraintOnIslandPredicate2*, %class.btSortConstraintOnIslandPredicate2** %CompareFunc.addr, align 4, !tbaa !2
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %10 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data2, align 4, !tbaa !51
  %11 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx3 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %10, i32 %11
  %12 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx3, align 4, !tbaa !2
  %13 = load %class.btTypedConstraint*, %class.btTypedConstraint** %x, align 4, !tbaa !2
  %call = call zeroext i1 @_ZNK34btSortConstraintOnIslandPredicate2clEPK17btTypedConstraintS2_(%class.btSortConstraintOnIslandPredicate2* %9, %class.btTypedConstraint* %12, %class.btTypedConstraint* %13)
  br i1 %call, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %14 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %while.cond4

while.cond4:                                      ; preds = %while.body8, %while.end
  %15 = load %class.btSortConstraintOnIslandPredicate2*, %class.btSortConstraintOnIslandPredicate2** %CompareFunc.addr, align 4, !tbaa !2
  %16 = load %class.btTypedConstraint*, %class.btTypedConstraint** %x, align 4, !tbaa !2
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %17 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data5, align 4, !tbaa !51
  %18 = load i32, i32* %j, align 4, !tbaa !8
  %arrayidx6 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %17, i32 %18
  %19 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx6, align 4, !tbaa !2
  %call7 = call zeroext i1 @_ZNK34btSortConstraintOnIslandPredicate2clEPK17btTypedConstraintS2_(%class.btSortConstraintOnIslandPredicate2* %15, %class.btTypedConstraint* %16, %class.btTypedConstraint* %19)
  br i1 %call7, label %while.body8, label %while.end9

while.body8:                                      ; preds = %while.cond4
  %20 = load i32, i32* %j, align 4, !tbaa !8
  %dec = add nsw i32 %20, -1
  store i32 %dec, i32* %j, align 4, !tbaa !8
  br label %while.cond4

while.end9:                                       ; preds = %while.cond4
  %21 = load i32, i32* %i, align 4, !tbaa !8
  %22 = load i32, i32* %j, align 4, !tbaa !8
  %cmp = icmp sle i32 %21, %22
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %while.end9
  %23 = load i32, i32* %i, align 4, !tbaa !8
  %24 = load i32, i32* %j, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4swapEii(%class.btAlignedObjectArray.0* %this1, i32 %23, i32 %24)
  %25 = load i32, i32* %i, align 4, !tbaa !8
  %inc10 = add nsw i32 %25, 1
  store i32 %inc10, i32* %i, align 4, !tbaa !8
  %26 = load i32, i32* %j, align 4, !tbaa !8
  %dec11 = add nsw i32 %26, -1
  store i32 %dec11, i32* %j, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end9
  br label %do.cond

do.cond:                                          ; preds = %if.end
  %27 = load i32, i32* %i, align 4, !tbaa !8
  %28 = load i32, i32* %j, align 4, !tbaa !8
  %cmp12 = icmp sle i32 %27, %28
  br i1 %cmp12, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %29 = load i32, i32* %lo.addr, align 4, !tbaa !8
  %30 = load i32, i32* %j, align 4, !tbaa !8
  %cmp13 = icmp slt i32 %29, %30
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %do.end
  %31 = load %class.btSortConstraintOnIslandPredicate2*, %class.btSortConstraintOnIslandPredicate2** %CompareFunc.addr, align 4, !tbaa !2
  %32 = load i32, i32* %lo.addr, align 4, !tbaa !8
  %33 = load i32, i32* %j, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE17quickSortInternalI34btSortConstraintOnIslandPredicate2EEvRKT_ii(%class.btAlignedObjectArray.0* %this1, %class.btSortConstraintOnIslandPredicate2* nonnull align 1 dereferenceable(1) %31, i32 %32, i32 %33)
  br label %if.end15

if.end15:                                         ; preds = %if.then14, %do.end
  %34 = load i32, i32* %i, align 4, !tbaa !8
  %35 = load i32, i32* %hi.addr, align 4, !tbaa !8
  %cmp16 = icmp slt i32 %34, %35
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end15
  %36 = load %class.btSortConstraintOnIslandPredicate2*, %class.btSortConstraintOnIslandPredicate2** %CompareFunc.addr, align 4, !tbaa !2
  %37 = load i32, i32* %i, align 4, !tbaa !8
  %38 = load i32, i32* %hi.addr, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE17quickSortInternalI34btSortConstraintOnIslandPredicate2EEvRKT_ii(%class.btAlignedObjectArray.0* %this1, %class.btSortConstraintOnIslandPredicate2* nonnull align 1 dereferenceable(1) %36, i32 %37, i32 %38)
  br label %if.end18

if.end18:                                         ; preds = %if.then17, %if.end15
  %39 = bitcast %class.btTypedConstraint** %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #10
  %40 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #10
  %41 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #10
  ret void
}

define linkonce_odr hidden zeroext i1 @_ZNK34btSortConstraintOnIslandPredicate2clEPK17btTypedConstraintS2_(%class.btSortConstraintOnIslandPredicate2* %this, %class.btTypedConstraint* %lhs, %class.btTypedConstraint* %rhs) #0 comdat {
entry:
  %this.addr = alloca %class.btSortConstraintOnIslandPredicate2*, align 4
  %lhs.addr = alloca %class.btTypedConstraint*, align 4
  %rhs.addr = alloca %class.btTypedConstraint*, align 4
  %rIslandId0 = alloca i32, align 4
  %lIslandId0 = alloca i32, align 4
  store %class.btSortConstraintOnIslandPredicate2* %this, %class.btSortConstraintOnIslandPredicate2** %this.addr, align 4, !tbaa !2
  store %class.btTypedConstraint* %lhs, %class.btTypedConstraint** %lhs.addr, align 4, !tbaa !2
  store %class.btTypedConstraint* %rhs, %class.btTypedConstraint** %rhs.addr, align 4, !tbaa !2
  %this1 = load %class.btSortConstraintOnIslandPredicate2*, %class.btSortConstraintOnIslandPredicate2** %this.addr, align 4
  %0 = bitcast i32* %rIslandId0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = bitcast i32* %lIslandId0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  %2 = load %class.btTypedConstraint*, %class.btTypedConstraint** %rhs.addr, align 4, !tbaa !2
  %call = call i32 @_Z24btGetConstraintIslandId2PK17btTypedConstraint(%class.btTypedConstraint* %2)
  store i32 %call, i32* %rIslandId0, align 4, !tbaa !8
  %3 = load %class.btTypedConstraint*, %class.btTypedConstraint** %lhs.addr, align 4, !tbaa !2
  %call2 = call i32 @_Z24btGetConstraintIslandId2PK17btTypedConstraint(%class.btTypedConstraint* %3)
  store i32 %call2, i32* %lIslandId0, align 4, !tbaa !8
  %4 = load i32, i32* %lIslandId0, align 4, !tbaa !8
  %5 = load i32, i32* %rIslandId0, align 4, !tbaa !8
  %cmp = icmp slt i32 %4, %5
  %6 = bitcast i32* %lIslandId0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #10
  %7 = bitcast i32* %rIslandId0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  ret i1 %cmp
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4swapEii(%class.btAlignedObjectArray.0* %this, i32 %index0, i32 %index1) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %class.btTypedConstraint*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %index0, i32* %index0.addr, align 4, !tbaa !8
  store i32 %index1, i32* %index1.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast %class.btTypedConstraint** %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %1 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4, !tbaa !51
  %2 = load i32, i32* %index0.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %1, i32 %2
  %3 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx, align 4, !tbaa !2
  store %class.btTypedConstraint* %3, %class.btTypedConstraint** %temp, align 4, !tbaa !2
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %4 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data2, align 4, !tbaa !51
  %5 = load i32, i32* %index1.addr, align 4, !tbaa !8
  %arrayidx3 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %4, i32 %5
  %6 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx3, align 4, !tbaa !2
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %7 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data4, align 4, !tbaa !51
  %8 = load i32, i32* %index0.addr, align 4, !tbaa !8
  %arrayidx5 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %7, i32 %8
  store %class.btTypedConstraint* %6, %class.btTypedConstraint** %arrayidx5, align 4, !tbaa !2
  %9 = load %class.btTypedConstraint*, %class.btTypedConstraint** %temp, align 4, !tbaa !2
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %10 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data6, align 4, !tbaa !51
  %11 = load i32, i32* %index1.addr, align 4, !tbaa !8
  %arrayidx7 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %10, i32 %11
  store %class.btTypedConstraint* %9, %class.btTypedConstraint** %arrayidx7, align 4, !tbaa !2
  %12 = bitcast %class.btTypedConstraint** %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #10
  ret void
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE17quickSortInternalI42btSortMultiBodyConstraintOnIslandPredicateEEvRKT_ii(%class.btAlignedObjectArray.45* %this, %class.btSortMultiBodyConstraintOnIslandPredicate* nonnull align 1 dereferenceable(1) %CompareFunc, i32 %lo, i32 %hi) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.45*, align 4
  %CompareFunc.addr = alloca %class.btSortMultiBodyConstraintOnIslandPredicate*, align 4
  %lo.addr = alloca i32, align 4
  %hi.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %x = alloca %class.btMultiBodyConstraint*, align 4
  store %class.btAlignedObjectArray.45* %this, %class.btAlignedObjectArray.45** %this.addr, align 4, !tbaa !2
  store %class.btSortMultiBodyConstraintOnIslandPredicate* %CompareFunc, %class.btSortMultiBodyConstraintOnIslandPredicate** %CompareFunc.addr, align 4, !tbaa !2
  store i32 %lo, i32* %lo.addr, align 4, !tbaa !8
  store i32 %hi, i32* %hi.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.45*, %class.btAlignedObjectArray.45** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %lo.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  %2 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #10
  %3 = load i32, i32* %hi.addr, align 4, !tbaa !8
  store i32 %3, i32* %j, align 4, !tbaa !8
  %4 = bitcast %class.btMultiBodyConstraint** %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #10
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 4
  %5 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data, align 4, !tbaa !72
  %6 = load i32, i32* %lo.addr, align 4, !tbaa !8
  %7 = load i32, i32* %hi.addr, align 4, !tbaa !8
  %add = add nsw i32 %6, %7
  %div = sdiv i32 %add, 2
  %arrayidx = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %5, i32 %div
  %8 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %arrayidx, align 4, !tbaa !2
  store %class.btMultiBodyConstraint* %8, %class.btMultiBodyConstraint** %x, align 4, !tbaa !2
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %do.body
  %9 = load %class.btSortMultiBodyConstraintOnIslandPredicate*, %class.btSortMultiBodyConstraintOnIslandPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 4
  %10 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data2, align 4, !tbaa !72
  %11 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx3 = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %10, i32 %11
  %12 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %arrayidx3, align 4, !tbaa !2
  %13 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %x, align 4, !tbaa !2
  %call = call zeroext i1 @_ZNK42btSortMultiBodyConstraintOnIslandPredicateclEPK21btMultiBodyConstraintS2_(%class.btSortMultiBodyConstraintOnIslandPredicate* %9, %class.btMultiBodyConstraint* %12, %class.btMultiBodyConstraint* %13)
  br i1 %call, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %14 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %while.cond4

while.cond4:                                      ; preds = %while.body8, %while.end
  %15 = load %class.btSortMultiBodyConstraintOnIslandPredicate*, %class.btSortMultiBodyConstraintOnIslandPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %16 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %x, align 4, !tbaa !2
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 4
  %17 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data5, align 4, !tbaa !72
  %18 = load i32, i32* %j, align 4, !tbaa !8
  %arrayidx6 = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %17, i32 %18
  %19 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %arrayidx6, align 4, !tbaa !2
  %call7 = call zeroext i1 @_ZNK42btSortMultiBodyConstraintOnIslandPredicateclEPK21btMultiBodyConstraintS2_(%class.btSortMultiBodyConstraintOnIslandPredicate* %15, %class.btMultiBodyConstraint* %16, %class.btMultiBodyConstraint* %19)
  br i1 %call7, label %while.body8, label %while.end9

while.body8:                                      ; preds = %while.cond4
  %20 = load i32, i32* %j, align 4, !tbaa !8
  %dec = add nsw i32 %20, -1
  store i32 %dec, i32* %j, align 4, !tbaa !8
  br label %while.cond4

while.end9:                                       ; preds = %while.cond4
  %21 = load i32, i32* %i, align 4, !tbaa !8
  %22 = load i32, i32* %j, align 4, !tbaa !8
  %cmp = icmp sle i32 %21, %22
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %while.end9
  %23 = load i32, i32* %i, align 4, !tbaa !8
  %24 = load i32, i32* %j, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE4swapEii(%class.btAlignedObjectArray.45* %this1, i32 %23, i32 %24)
  %25 = load i32, i32* %i, align 4, !tbaa !8
  %inc10 = add nsw i32 %25, 1
  store i32 %inc10, i32* %i, align 4, !tbaa !8
  %26 = load i32, i32* %j, align 4, !tbaa !8
  %dec11 = add nsw i32 %26, -1
  store i32 %dec11, i32* %j, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end9
  br label %do.cond

do.cond:                                          ; preds = %if.end
  %27 = load i32, i32* %i, align 4, !tbaa !8
  %28 = load i32, i32* %j, align 4, !tbaa !8
  %cmp12 = icmp sle i32 %27, %28
  br i1 %cmp12, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %29 = load i32, i32* %lo.addr, align 4, !tbaa !8
  %30 = load i32, i32* %j, align 4, !tbaa !8
  %cmp13 = icmp slt i32 %29, %30
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %do.end
  %31 = load %class.btSortMultiBodyConstraintOnIslandPredicate*, %class.btSortMultiBodyConstraintOnIslandPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %32 = load i32, i32* %lo.addr, align 4, !tbaa !8
  %33 = load i32, i32* %j, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE17quickSortInternalI42btSortMultiBodyConstraintOnIslandPredicateEEvRKT_ii(%class.btAlignedObjectArray.45* %this1, %class.btSortMultiBodyConstraintOnIslandPredicate* nonnull align 1 dereferenceable(1) %31, i32 %32, i32 %33)
  br label %if.end15

if.end15:                                         ; preds = %if.then14, %do.end
  %34 = load i32, i32* %i, align 4, !tbaa !8
  %35 = load i32, i32* %hi.addr, align 4, !tbaa !8
  %cmp16 = icmp slt i32 %34, %35
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end15
  %36 = load %class.btSortMultiBodyConstraintOnIslandPredicate*, %class.btSortMultiBodyConstraintOnIslandPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %37 = load i32, i32* %i, align 4, !tbaa !8
  %38 = load i32, i32* %hi.addr, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE17quickSortInternalI42btSortMultiBodyConstraintOnIslandPredicateEEvRKT_ii(%class.btAlignedObjectArray.45* %this1, %class.btSortMultiBodyConstraintOnIslandPredicate* nonnull align 1 dereferenceable(1) %36, i32 %37, i32 %38)
  br label %if.end18

if.end18:                                         ; preds = %if.then17, %if.end15
  %39 = bitcast %class.btMultiBodyConstraint** %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #10
  %40 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #10
  %41 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #10
  ret void
}

define linkonce_odr hidden zeroext i1 @_ZNK42btSortMultiBodyConstraintOnIslandPredicateclEPK21btMultiBodyConstraintS2_(%class.btSortMultiBodyConstraintOnIslandPredicate* %this, %class.btMultiBodyConstraint* %lhs, %class.btMultiBodyConstraint* %rhs) #0 comdat {
entry:
  %this.addr = alloca %class.btSortMultiBodyConstraintOnIslandPredicate*, align 4
  %lhs.addr = alloca %class.btMultiBodyConstraint*, align 4
  %rhs.addr = alloca %class.btMultiBodyConstraint*, align 4
  %rIslandId0 = alloca i32, align 4
  %lIslandId0 = alloca i32, align 4
  store %class.btSortMultiBodyConstraintOnIslandPredicate* %this, %class.btSortMultiBodyConstraintOnIslandPredicate** %this.addr, align 4, !tbaa !2
  store %class.btMultiBodyConstraint* %lhs, %class.btMultiBodyConstraint** %lhs.addr, align 4, !tbaa !2
  store %class.btMultiBodyConstraint* %rhs, %class.btMultiBodyConstraint** %rhs.addr, align 4, !tbaa !2
  %this1 = load %class.btSortMultiBodyConstraintOnIslandPredicate*, %class.btSortMultiBodyConstraintOnIslandPredicate** %this.addr, align 4
  %0 = bitcast i32* %rIslandId0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = bitcast i32* %lIslandId0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  %2 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %rhs.addr, align 4, !tbaa !2
  %call = call i32 @_Z32btGetMultiBodyConstraintIslandIdPK21btMultiBodyConstraint(%class.btMultiBodyConstraint* %2)
  store i32 %call, i32* %rIslandId0, align 4, !tbaa !8
  %3 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %lhs.addr, align 4, !tbaa !2
  %call2 = call i32 @_Z32btGetMultiBodyConstraintIslandIdPK21btMultiBodyConstraint(%class.btMultiBodyConstraint* %3)
  store i32 %call2, i32* %lIslandId0, align 4, !tbaa !8
  %4 = load i32, i32* %lIslandId0, align 4, !tbaa !8
  %5 = load i32, i32* %rIslandId0, align 4, !tbaa !8
  %cmp = icmp slt i32 %4, %5
  %6 = bitcast i32* %lIslandId0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #10
  %7 = bitcast i32* %rIslandId0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  ret i1 %cmp
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE4swapEii(%class.btAlignedObjectArray.45* %this, i32 %index0, i32 %index1) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.45*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %class.btMultiBodyConstraint*, align 4
  store %class.btAlignedObjectArray.45* %this, %class.btAlignedObjectArray.45** %this.addr, align 4, !tbaa !2
  store i32 %index0, i32* %index0.addr, align 4, !tbaa !8
  store i32 %index1, i32* %index1.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.45*, %class.btAlignedObjectArray.45** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyConstraint** %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 4
  %1 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data, align 4, !tbaa !72
  %2 = load i32, i32* %index0.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %1, i32 %2
  %3 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %arrayidx, align 4, !tbaa !2
  store %class.btMultiBodyConstraint* %3, %class.btMultiBodyConstraint** %temp, align 4, !tbaa !2
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 4
  %4 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data2, align 4, !tbaa !72
  %5 = load i32, i32* %index1.addr, align 4, !tbaa !8
  %arrayidx3 = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %4, i32 %5
  %6 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %arrayidx3, align 4, !tbaa !2
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 4
  %7 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data4, align 4, !tbaa !72
  %8 = load i32, i32* %index0.addr, align 4, !tbaa !8
  %arrayidx5 = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %7, i32 %8
  store %class.btMultiBodyConstraint* %6, %class.btMultiBodyConstraint** %arrayidx5, align 4, !tbaa !2
  %9 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %temp, align 4, !tbaa !2
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 4
  %10 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data6, align 4, !tbaa !72
  %11 = load i32, i32* %index1.addr, align 4, !tbaa !8
  %arrayidx7 = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %10, i32 %11
  store %class.btMultiBodyConstraint* %9, %class.btMultiBodyConstraint** %arrayidx7, align 4, !tbaa !2
  %12 = bitcast %class.btMultiBodyConstraint** %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #10
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray.32* %this, i32 %_Count) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.32*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca float*, align 4
  store %class.btAlignedObjectArray.32* %this, %class.btAlignedObjectArray.32** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.32*, %class.btAlignedObjectArray.32** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray.32* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast float** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %call2 = call i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray.32* %this1, i32 %2)
  %3 = bitcast i8* %call2 to float*
  store float* %3, float** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.32* %this1)
  %4 = load float*, float** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.32* %this1, i32 0, i32 %call3, float* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.32* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.32* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.32* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.32, %class.btAlignedObjectArray.32* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !128
  %5 = load float*, float** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.32, %class.btAlignedObjectArray.32* %this1, i32 0, i32 4
  store float* %5, float** %m_data, align 4, !tbaa !96
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.32, %class.btAlignedObjectArray.32* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !129
  %7 = bitcast float** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray.32* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.32*, align 4
  store %class.btAlignedObjectArray.32* %this, %class.btAlignedObjectArray.32** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.32*, %class.btAlignedObjectArray.32** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.32, %class.btAlignedObjectArray.32* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !129
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray.32* %this, i32 %size) #1 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.32*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.32* %this, %class.btAlignedObjectArray.32** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.32*, %class.btAlignedObjectArray.32** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.32, %class.btAlignedObjectArray.32* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !8
  %call = call float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator.33* %m_allocator, i32 %1, float** null)
  %2 = bitcast float* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.32* %this, i32 %start, i32 %end, float* %dest) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.32*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca float*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.32* %this, %class.btAlignedObjectArray.32** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !8
  store i32 %end, i32* %end.addr, align 4, !tbaa !8
  store float* %dest, float** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.32*, %class.btAlignedObjectArray.32** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %start.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %end.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load float*, float** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds float, float* %4, i32 %5
  %6 = bitcast float* %arrayidx to i8*
  %7 = bitcast i8* %6 to float*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.32, %class.btAlignedObjectArray.32* %this1, i32 0, i32 4
  %8 = load float*, float** %m_data, align 4, !tbaa !96
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx2 = getelementptr inbounds float, float* %8, i32 %9
  %10 = load float, float* %arrayidx2, align 4, !tbaa !73
  store float %10, float* %7, align 4, !tbaa !73
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #10
  ret void
}

define linkonce_odr hidden float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator.33* %this, i32 %n, float** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.33*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca float**, align 4
  store %class.btAlignedAllocator.33* %this, %class.btAlignedAllocator.33** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  store float** %hint, float*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.33*, %class.btAlignedAllocator.33** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !8
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to float*
  ret float* %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray.36* %this, i32 %_Count) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.36*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btVector3*, align 4
  store %class.btAlignedObjectArray.36* %this, %class.btAlignedObjectArray.36** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.36*, %class.btAlignedObjectArray.36** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray.36* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btVector3** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %call2 = call i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray.36* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btVector3*
  store %class.btVector3* %3, %class.btVector3** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.36* %this1)
  %4 = load %class.btVector3*, %class.btVector3** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray.36* %this1, i32 0, i32 %call3, %class.btVector3* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.36* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.36* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.36* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.36, %class.btAlignedObjectArray.36* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !126
  %5 = load %class.btVector3*, %class.btVector3** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.36, %class.btAlignedObjectArray.36* %this1, i32 0, i32 4
  store %class.btVector3* %5, %class.btVector3** %m_data, align 4, !tbaa !98
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.36, %class.btAlignedObjectArray.36* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !127
  %7 = bitcast %class.btVector3** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN9btVector3nwEmPv(i32 %0, i8* %ptr) #4 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !136
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray.36* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.36*, align 4
  store %class.btAlignedObjectArray.36* %this, %class.btAlignedObjectArray.36** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.36*, %class.btAlignedObjectArray.36** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.36, %class.btAlignedObjectArray.36* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !127
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray.36* %this, i32 %size) #1 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.36*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.36* %this, %class.btAlignedObjectArray.36** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.36*, %class.btAlignedObjectArray.36** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.36, %class.btAlignedObjectArray.36* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.37* %m_allocator, i32 %1, %class.btVector3** null)
  %2 = bitcast %class.btVector3* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray.36* %this, i32 %start, i32 %end, %class.btVector3* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.36*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.36* %this, %class.btAlignedObjectArray.36** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !8
  store i32 %end, i32* %end.addr, align 4, !tbaa !8
  store %class.btVector3* %dest, %class.btVector3** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.36*, %class.btAlignedObjectArray.36** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %start.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %end.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btVector3*, %class.btVector3** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 %5
  %6 = bitcast %class.btVector3* %arrayidx to i8*
  %call = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %6)
  %7 = bitcast i8* %call to %class.btVector3*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.36, %class.btAlignedObjectArray.36* %this1, i32 0, i32 4
  %8 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !98
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 %9
  %10 = bitcast %class.btVector3* %7 to i8*
  %11 = bitcast %class.btVector3* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !99
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #10
  ret void
}

define linkonce_odr hidden %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.37* %this, i32 %n, %class.btVector3** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.37*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btVector3**, align 4
  store %class.btAlignedAllocator.37* %this, %class.btAlignedAllocator.37** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  store %class.btVector3** %hint, %class.btVector3*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.37*, %class.btAlignedAllocator.37** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !8
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btVector3*
  ret %class.btVector3* %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btMatrix3x3E7reserveEi(%class.btAlignedObjectArray.40* %this, i32 %_Count) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.40*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btMatrix3x3*, align 4
  store %class.btAlignedObjectArray.40* %this, %class.btAlignedObjectArray.40** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.40*, %class.btAlignedObjectArray.40** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI11btMatrix3x3E8capacityEv(%class.btAlignedObjectArray.40* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btMatrix3x3** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %call2 = call i8* @_ZN20btAlignedObjectArrayI11btMatrix3x3E8allocateEi(%class.btAlignedObjectArray.40* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btMatrix3x3*
  store %class.btMatrix3x3* %3, %class.btMatrix3x3** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI11btMatrix3x3E4sizeEv(%class.btAlignedObjectArray.40* %this1)
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI11btMatrix3x3E4copyEiiPS0_(%class.btAlignedObjectArray.40* %this1, i32 0, i32 %call3, %class.btMatrix3x3* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI11btMatrix3x3E4sizeEv(%class.btAlignedObjectArray.40* %this1)
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E7destroyEii(%class.btAlignedObjectArray.40* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E10deallocateEv(%class.btAlignedObjectArray.40* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.40, %class.btAlignedObjectArray.40* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !134
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.40, %class.btAlignedObjectArray.40* %this1, i32 0, i32 4
  store %class.btMatrix3x3* %5, %class.btMatrix3x3** %m_data, align 4, !tbaa !102
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.40, %class.btAlignedObjectArray.40* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !135
  %7 = bitcast %class.btMatrix3x3** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #1 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !99
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !99
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !99
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI11btMatrix3x3E8capacityEv(%class.btAlignedObjectArray.40* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.40*, align 4
  store %class.btAlignedObjectArray.40* %this, %class.btAlignedObjectArray.40** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.40*, %class.btAlignedObjectArray.40** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.40, %class.btAlignedObjectArray.40* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !135
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI11btMatrix3x3E8allocateEi(%class.btAlignedObjectArray.40* %this, i32 %size) #1 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.40*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.40* %this, %class.btAlignedObjectArray.40** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.40*, %class.btAlignedObjectArray.40** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.40, %class.btAlignedObjectArray.40* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !8
  %call = call %class.btMatrix3x3* @_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.41* %m_allocator, i32 %1, %class.btMatrix3x3** null)
  %2 = bitcast %class.btMatrix3x3* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI11btMatrix3x3E4copyEiiPS0_(%class.btAlignedObjectArray.40* %this, i32 %start, i32 %end, %class.btMatrix3x3* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.40*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btMatrix3x3*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.40* %this, %class.btAlignedObjectArray.40** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !8
  store i32 %end, i32* %end.addr, align 4, !tbaa !8
  store %class.btMatrix3x3* %dest, %class.btMatrix3x3** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.40*, %class.btAlignedObjectArray.40** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %start.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %end.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %4, i32 %5
  %6 = bitcast %class.btMatrix3x3* %arrayidx to i8*
  %7 = bitcast i8* %6 to %class.btMatrix3x3*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.40, %class.btAlignedObjectArray.40* %this1, i32 0, i32 4
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m_data, align 4, !tbaa !102
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %8, i32 %9
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %7, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #10
  ret void
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.41* %this, i32 %n, %class.btMatrix3x3** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.41*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btMatrix3x3**, align 4
  store %class.btAlignedAllocator.41* %this, %class.btAlignedAllocator.41** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  store %class.btMatrix3x3** %hint, %class.btMatrix3x3*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.41*, %class.btAlignedAllocator.41** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !8
  %mul = mul i32 48, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btMatrix3x3*
  ret %class.btMatrix3x3* %1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.72* @_ZN18btAlignedAllocatorI12btQuaternionLj16EEC2Ev(%class.btAlignedAllocator.72* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.72*, align 4
  store %class.btAlignedAllocator.72* %this, %class.btAlignedAllocator.72** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.72*, %class.btAlignedAllocator.72** %this.addr, align 4
  ret %class.btAlignedAllocator.72* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btQuaternionE4initEv(%class.btAlignedObjectArray.71* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.71*, align 4
  store %class.btAlignedObjectArray.71* %this, %class.btAlignedObjectArray.71** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.71*, %class.btAlignedObjectArray.71** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.71, %class.btAlignedObjectArray.71* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !138
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.71, %class.btAlignedObjectArray.71* %this1, i32 0, i32 4
  store %class.btQuaternion* null, %class.btQuaternion** %m_data, align 4, !tbaa !107
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.71, %class.btAlignedObjectArray.71* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !110
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.71, %class.btAlignedObjectArray.71* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !139
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btQuaternionE5clearEv(%class.btAlignedObjectArray.71* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.71*, align 4
  store %class.btAlignedObjectArray.71* %this, %class.btAlignedObjectArray.71** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.71*, %class.btAlignedObjectArray.71** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI12btQuaternionE4sizeEv(%class.btAlignedObjectArray.71* %this1)
  call void @_ZN20btAlignedObjectArrayI12btQuaternionE7destroyEii(%class.btAlignedObjectArray.71* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI12btQuaternionE10deallocateEv(%class.btAlignedObjectArray.71* %this1)
  call void @_ZN20btAlignedObjectArrayI12btQuaternionE4initEv(%class.btAlignedObjectArray.71* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btQuaternionE7destroyEii(%class.btAlignedObjectArray.71* %this, i32 %first, i32 %last) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.71*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.71* %this, %class.btAlignedObjectArray.71** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !8
  store i32 %last, i32* %last.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.71*, %class.btAlignedObjectArray.71** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %first.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %last.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.71, %class.btAlignedObjectArray.71* %this1, i32 0, i32 4
  %4 = load %class.btQuaternion*, %class.btQuaternion** %m_data, align 4, !tbaa !107
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btQuaternion, %class.btQuaternion* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI12btQuaternionE4sizeEv(%class.btAlignedObjectArray.71* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.71*, align 4
  store %class.btAlignedObjectArray.71* %this, %class.btAlignedObjectArray.71** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.71*, %class.btAlignedObjectArray.71** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.71, %class.btAlignedObjectArray.71* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !110
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btQuaternionE10deallocateEv(%class.btAlignedObjectArray.71* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.71*, align 4
  store %class.btAlignedObjectArray.71* %this, %class.btAlignedObjectArray.71** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.71*, %class.btAlignedObjectArray.71** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.71, %class.btAlignedObjectArray.71* %this1, i32 0, i32 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %m_data, align 4, !tbaa !107
  %tobool = icmp ne %class.btQuaternion* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.71, %class.btAlignedObjectArray.71* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !138, !range !54
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.71, %class.btAlignedObjectArray.71* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.71, %class.btAlignedObjectArray.71* %this1, i32 0, i32 4
  %2 = load %class.btQuaternion*, %class.btQuaternion** %m_data4, align 4, !tbaa !107
  call void @_ZN18btAlignedAllocatorI12btQuaternionLj16EE10deallocateEPS0_(%class.btAlignedAllocator.72* %m_allocator, %class.btQuaternion* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.71, %class.btAlignedObjectArray.71* %this1, i32 0, i32 4
  store %class.btQuaternion* null, %class.btQuaternion** %m_data5, align 4, !tbaa !107
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI12btQuaternionLj16EE10deallocateEPS0_(%class.btAlignedAllocator.72* %this, %class.btQuaternion* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.72*, align 4
  %ptr.addr = alloca %class.btQuaternion*, align 4
  store %class.btAlignedAllocator.72* %this, %class.btAlignedAllocator.72** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %ptr, %class.btQuaternion** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.72*, %class.btAlignedAllocator.72** %this.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btQuaternion* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btQuaternionE7reserveEi(%class.btAlignedObjectArray.71* %this, i32 %_Count) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.71*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btQuaternion*, align 4
  store %class.btAlignedObjectArray.71* %this, %class.btAlignedObjectArray.71** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.71*, %class.btAlignedObjectArray.71** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI12btQuaternionE8capacityEv(%class.btAlignedObjectArray.71* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btQuaternion** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %call2 = call i8* @_ZN20btAlignedObjectArrayI12btQuaternionE8allocateEi(%class.btAlignedObjectArray.71* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btQuaternion*
  store %class.btQuaternion* %3, %class.btQuaternion** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI12btQuaternionE4sizeEv(%class.btAlignedObjectArray.71* %this1)
  %4 = load %class.btQuaternion*, %class.btQuaternion** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI12btQuaternionE4copyEiiPS0_(%class.btAlignedObjectArray.71* %this1, i32 0, i32 %call3, %class.btQuaternion* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI12btQuaternionE4sizeEv(%class.btAlignedObjectArray.71* %this1)
  call void @_ZN20btAlignedObjectArrayI12btQuaternionE7destroyEii(%class.btAlignedObjectArray.71* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI12btQuaternionE10deallocateEv(%class.btAlignedObjectArray.71* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.71, %class.btAlignedObjectArray.71* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !138
  %5 = load %class.btQuaternion*, %class.btQuaternion** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.71, %class.btAlignedObjectArray.71* %this1, i32 0, i32 4
  store %class.btQuaternion* %5, %class.btQuaternion** %m_data, align 4, !tbaa !107
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.71, %class.btAlignedObjectArray.71* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !139
  %7 = bitcast %class.btQuaternion** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI12btQuaternionE8capacityEv(%class.btAlignedObjectArray.71* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.71*, align 4
  store %class.btAlignedObjectArray.71* %this, %class.btAlignedObjectArray.71** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.71*, %class.btAlignedObjectArray.71** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.71, %class.btAlignedObjectArray.71* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !139
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI12btQuaternionE8allocateEi(%class.btAlignedObjectArray.71* %this, i32 %size) #1 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.71*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.71* %this, %class.btAlignedObjectArray.71** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.71*, %class.btAlignedObjectArray.71** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.71, %class.btAlignedObjectArray.71* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !8
  %call = call %class.btQuaternion* @_ZN18btAlignedAllocatorI12btQuaternionLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.72* %m_allocator, i32 %1, %class.btQuaternion** null)
  %2 = bitcast %class.btQuaternion* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI12btQuaternionE4copyEiiPS0_(%class.btAlignedObjectArray.71* %this, i32 %start, i32 %end, %class.btQuaternion* %dest) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.71*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btQuaternion*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.71* %this, %class.btAlignedObjectArray.71** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !8
  store i32 %end, i32* %end.addr, align 4, !tbaa !8
  store %class.btQuaternion* %dest, %class.btQuaternion** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.71*, %class.btAlignedObjectArray.71** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %start.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %end.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btQuaternion*, %class.btQuaternion** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btQuaternion, %class.btQuaternion* %4, i32 %5
  %6 = bitcast %class.btQuaternion* %arrayidx to i8*
  %7 = bitcast i8* %6 to %class.btQuaternion*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.71, %class.btAlignedObjectArray.71* %this1, i32 0, i32 4
  %8 = load %class.btQuaternion*, %class.btQuaternion** %m_data, align 4, !tbaa !107
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx2 = getelementptr inbounds %class.btQuaternion, %class.btQuaternion* %8, i32 %9
  %10 = bitcast %class.btQuaternion* %7 to i8*
  %11 = bitcast %class.btQuaternion* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #10
  ret void
}

define linkonce_odr hidden %class.btQuaternion* @_ZN18btAlignedAllocatorI12btQuaternionLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.72* %this, i32 %n, %class.btQuaternion** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.72*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btQuaternion**, align 4
  store %class.btAlignedAllocator.72* %this, %class.btAlignedAllocator.72** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  store %class.btQuaternion** %hint, %class.btQuaternion*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.72*, %class.btAlignedAllocator.72** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !8
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btQuaternion*
  ret %class.btQuaternion* %1
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE16findLinearSearchERKS1_(%class.btAlignedObjectArray.45* %this, %class.btMultiBodyConstraint** nonnull align 4 dereferenceable(4) %key) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.45*, align 4
  %key.addr = alloca %class.btMultiBodyConstraint**, align 4
  %index = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.45* %this, %class.btAlignedObjectArray.45** %this.addr, align 4, !tbaa !2
  store %class.btMultiBodyConstraint** %key, %class.btMultiBodyConstraint*** %key.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.45*, %class.btAlignedObjectArray.45** %this.addr, align 4
  %0 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %call = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.45* %this1)
  store i32 %call, i32* %index, align 4, !tbaa !8
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.45* %this1)
  %cmp = icmp slt i32 %2, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 4
  %3 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data, align 4, !tbaa !72
  %4 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %3, i32 %4
  %5 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %arrayidx, align 4, !tbaa !2
  %6 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %key.addr, align 4, !tbaa !2
  %7 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %6, align 4, !tbaa !2
  %cmp3 = icmp eq %class.btMultiBodyConstraint* %5, %7
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4, !tbaa !8
  store i32 %8, i32* %index, align 4, !tbaa !8
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %10 = load i32, i32* %index, align 4, !tbaa !8
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #10
  %12 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #10
  ret i32 %10
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE8pop_backEv(%class.btAlignedObjectArray.45* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.45*, align 4
  store %class.btAlignedObjectArray.45* %this, %class.btAlignedObjectArray.45** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.45*, %class.btAlignedObjectArray.45** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !69
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4, !tbaa !69
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 4
  %1 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data, align 4, !tbaa !72
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.45* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4, !tbaa !69
  %arrayidx = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %1, i32 %2
  ret void
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { cold noreturn nounwind }
attributes #9 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #10 = { nounwind }
attributes #11 = { builtin allocsize(0) }
attributes #12 = { builtin nounwind }
attributes #13 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"short", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"int", !4, i64 0}
!10 = !{!11, !3, i64 12}
!11 = !{!"_ZTS20btAlignedObjectArrayIP11btMultiBodyE", !12, i64 0, !9, i64 4, !9, i64 8, !3, i64 12, !13, i64 16}
!12 = !{!"_ZTS18btAlignedAllocatorIP11btMultiBodyLj16EE"}
!13 = !{!"bool", !4, i64 0}
!14 = !{!11, !9, i64 4}
!15 = !{!16, !16, i64 0}
!16 = !{!"vtable pointer", !5, i64 0}
!17 = !{!18, !3, i64 180}
!18 = !{!"_ZTS15btMultibodyLink", !19, i64 0, !19, i64 4, !20, i64 8, !9, i64 24, !21, i64 28, !20, i64 44, !20, i64 60, !20, i64 76, !20, i64 92, !13, i64 108, !21, i64 112, !20, i64 128, !20, i64 144, !20, i64 160, !19, i64 176, !3, i64 180, !9, i64 184}
!19 = !{!"float", !4, i64 0}
!20 = !{!"_ZTS9btVector3", !4, i64 0}
!21 = !{!"_ZTS12btQuaternion"}
!22 = !{!23, !3, i64 204}
!23 = !{!"_ZTS23btDiscreteDynamicsWorld", !24, i64 176, !3, i64 196, !3, i64 200, !3, i64 204, !24, i64 208, !26, i64 228, !20, i64 248, !19, i64 264, !19, i64 268, !13, i64 272, !13, i64 273, !13, i64 274, !13, i64 275, !28, i64 276, !9, i64 296, !13, i64 300, !30, i64 304}
!24 = !{!"_ZTS20btAlignedObjectArrayIP17btTypedConstraintE", !25, i64 0, !9, i64 4, !9, i64 8, !3, i64 12, !13, i64 16}
!25 = !{!"_ZTS18btAlignedAllocatorIP17btTypedConstraintLj16EE"}
!26 = !{!"_ZTS20btAlignedObjectArrayIP11btRigidBodyE", !27, i64 0, !9, i64 4, !9, i64 8, !3, i64 12, !13, i64 16}
!27 = !{!"_ZTS18btAlignedAllocatorIP11btRigidBodyLj16EE"}
!28 = !{!"_ZTS20btAlignedObjectArrayIP17btActionInterfaceE", !29, i64 0, !9, i64 4, !9, i64 8, !3, i64 12, !13, i64 16}
!29 = !{!"_ZTS18btAlignedAllocatorIP17btActionInterfaceLj16EE"}
!30 = !{!"_ZTS20btAlignedObjectArrayIP20btPersistentManifoldE", !31, i64 0, !9, i64 4, !9, i64 8, !3, i64 12, !13, i64 16}
!31 = !{!"_ZTS18btAlignedAllocatorIP20btPersistentManifoldLj16EE"}
!32 = !{!33, !3, i64 24}
!33 = !{!"_ZTS16btCollisionWorld", !34, i64 4, !3, i64 24, !36, i64 28, !3, i64 68, !3, i64 72, !13, i64 76}
!34 = !{!"_ZTS20btAlignedObjectArrayIP17btCollisionObjectE", !35, i64 0, !9, i64 4, !9, i64 8, !3, i64 12, !13, i64 16}
!35 = !{!"_ZTS18btAlignedAllocatorIP17btCollisionObjectLj16EE"}
!36 = !{!"_ZTS16btDispatcherInfo", !19, i64 0, !9, i64 4, !9, i64 8, !19, i64 12, !13, i64 16, !3, i64 20, !13, i64 24, !13, i64 25, !13, i64 26, !19, i64 28, !13, i64 32, !19, i64 36}
!37 = !{!30, !9, i64 4}
!38 = !{!30, !3, i64 12}
!39 = !{!40, !3, i64 740}
!40 = !{!"_ZTS20btPersistentManifold", !4, i64 4, !3, i64 740, !3, i64 744, !9, i64 748, !19, i64 752, !19, i64 756, !9, i64 760, !9, i64 764, !9, i64 768}
!41 = !{!40, !3, i64 744}
!42 = !{!43, !9, i64 204}
!43 = !{!"_ZTS17btCollisionObject", !44, i64 4, !44, i64 68, !20, i64 132, !20, i64 148, !20, i64 164, !9, i64 180, !19, i64 184, !3, i64 188, !3, i64 192, !3, i64 196, !3, i64 200, !9, i64 204, !9, i64 208, !9, i64 212, !9, i64 216, !19, i64 220, !19, i64 224, !19, i64 228, !19, i64 232, !9, i64 236, !4, i64 240, !19, i64 244, !19, i64 248, !19, i64 252, !9, i64 256, !9, i64 260}
!44 = !{!"_ZTS11btTransform", !45, i64 0, !20, i64 48}
!45 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!46 = !{!47, !9, i64 0}
!47 = !{!"_ZTS9btElement", !9, i64 0, !9, i64 4}
!48 = !{!47, !9, i64 4}
!49 = !{!43, !9, i64 208}
!50 = !{!24, !9, i64 4}
!51 = !{!24, !3, i64 12}
!52 = !{!53, !13, i64 20}
!53 = !{!"_ZTS17btTypedConstraint", !9, i64 8, !4, i64 12, !19, i64 16, !13, i64 20, !13, i64 21, !9, i64 24, !3, i64 28, !3, i64 32, !19, i64 36, !19, i64 40, !3, i64 44}
!54 = !{i8 0, i8 2}
!55 = !{!53, !3, i64 28}
!56 = !{!53, !3, i64 32}
!57 = !{!58, !3, i64 0}
!58 = !{!"_ZTS11btMultiBody", !3, i64 0, !20, i64 4, !21, i64 20, !19, i64 36, !20, i64 40, !20, i64 56, !20, i64 72, !59, i64 88, !61, i64 108, !63, i64 128, !65, i64 148, !67, i64 168, !45, i64 188, !45, i64 236, !45, i64 284, !45, i64 332, !13, i64 380, !13, i64 381, !13, i64 382, !19, i64 384, !9, i64 388, !19, i64 392, !19, i64 396, !13, i64 400, !19, i64 404, !13, i64 408}
!59 = !{!"_ZTS20btAlignedObjectArrayI15btMultibodyLinkE", !60, i64 0, !9, i64 4, !9, i64 8, !3, i64 12, !13, i64 16}
!60 = !{!"_ZTS18btAlignedAllocatorI15btMultibodyLinkLj16EE"}
!61 = !{!"_ZTS20btAlignedObjectArrayIP23btMultiBodyLinkColliderE", !62, i64 0, !9, i64 4, !9, i64 8, !3, i64 12, !13, i64 16}
!62 = !{!"_ZTS18btAlignedAllocatorIP23btMultiBodyLinkColliderLj16EE"}
!63 = !{!"_ZTS20btAlignedObjectArrayIfE", !64, i64 0, !9, i64 4, !9, i64 8, !3, i64 12, !13, i64 16}
!64 = !{!"_ZTS18btAlignedAllocatorIfLj16EE"}
!65 = !{!"_ZTS20btAlignedObjectArrayI9btVector3E", !66, i64 0, !9, i64 4, !9, i64 8, !3, i64 12, !13, i64 16}
!66 = !{!"_ZTS18btAlignedAllocatorI9btVector3Lj16EE"}
!67 = !{!"_ZTS20btAlignedObjectArrayI11btMatrix3x3E", !68, i64 0, !9, i64 4, !9, i64 8, !3, i64 12, !13, i64 16}
!68 = !{!"_ZTS18btAlignedAllocatorI11btMatrix3x3Lj16EE"}
!69 = !{!70, !9, i64 4}
!70 = !{!"_ZTS20btAlignedObjectArrayIP21btMultiBodyConstraintE", !71, i64 0, !9, i64 4, !9, i64 8, !3, i64 12, !13, i64 16}
!71 = !{!"_ZTS18btAlignedAllocatorIP21btMultiBodyConstraintLj16EE"}
!72 = !{!70, !3, i64 12}
!73 = !{!19, !19, i64 0}
!74 = !{!58, !13, i64 381}
!75 = !{!43, !9, i64 216}
!76 = !{!43, !19, i64 220}
!77 = !{!78, !3, i64 384}
!78 = !{!"_ZTS24btMultiBodyDynamicsWorld", !11, i64 324, !70, i64 344, !70, i64 364, !3, i64 384, !3, i64 388}
!79 = !{!80, !9, i64 44}
!80 = !{!"_ZTS23btContactSolverInfoData", !19, i64 0, !19, i64 4, !19, i64 8, !19, i64 12, !19, i64 16, !9, i64 20, !19, i64 24, !19, i64 28, !19, i64 32, !19, i64 36, !19, i64 40, !9, i64 44, !19, i64 48, !19, i64 52, !19, i64 56, !19, i64 60, !9, i64 64, !9, i64 68, !9, i64 72, !19, i64 76, !19, i64 80}
!81 = !{!80, !9, i64 64}
!82 = !{!78, !3, i64 388}
!83 = !{!84, !3, i64 4}
!84 = !{!"_ZTS36MultiBodyInplaceSolverIslandCallback", !3, i64 4, !3, i64 8, !3, i64 12, !9, i64 16, !3, i64 20, !9, i64 24, !3, i64 28, !3, i64 32, !34, i64 36, !30, i64 56, !24, i64 76, !70, i64 96}
!85 = !{!84, !3, i64 8}
!86 = !{!84, !3, i64 12}
!87 = !{!84, !9, i64 24}
!88 = !{!84, !3, i64 28}
!89 = !{!84, !3, i64 32}
!90 = !{!23, !3, i64 200}
!91 = !{!13, !13, i64 0}
!92 = !{!80, !19, i64 12}
!93 = !{!33, !3, i64 72}
!94 = !{!84, !9, i64 16}
!95 = !{!84, !3, i64 20}
!96 = !{!63, !3, i64 12}
!97 = !{!63, !9, i64 4}
!98 = !{!65, !3, i64 12}
!99 = !{i64 0, i64 16, !100}
!100 = !{!4, !4, i64 0}
!101 = !{!65, !9, i64 4}
!102 = !{!67, !3, i64 12}
!103 = !{!67, !9, i64 4}
!104 = !{!58, !19, i64 36}
!105 = !{!106, !9, i64 268}
!106 = !{!"_ZTS23btMultiBodyLinkCollider", !3, i64 264, !9, i64 268}
!107 = !{!108, !3, i64 12}
!108 = !{!"_ZTS20btAlignedObjectArrayI12btQuaternionE", !109, i64 0, !9, i64 4, !9, i64 8, !3, i64 12, !13, i64 16}
!109 = !{!"_ZTS18btAlignedAllocatorI12btQuaternionLj16EE"}
!110 = !{!108, !9, i64 4}
!111 = !{!43, !9, i64 260}
!112 = !{!113, !3, i64 12}
!113 = !{!"_ZTS20btAlignedObjectArrayI9btElementE", !114, i64 0, !9, i64 4, !9, i64 8, !3, i64 12, !13, i64 16}
!114 = !{!"_ZTS18btAlignedAllocatorI9btElementLj16EE"}
!115 = !{!59, !9, i64 4}
!116 = !{!59, !3, i64 12}
!117 = !{!80, !9, i64 72}
!118 = !{!34, !13, i64 16}
!119 = !{!34, !3, i64 12}
!120 = !{!34, !9, i64 4}
!121 = !{!34, !9, i64 8}
!122 = !{!30, !13, i64 16}
!123 = !{!30, !9, i64 8}
!124 = !{!24, !13, i64 16}
!125 = !{!24, !9, i64 8}
!126 = !{!65, !13, i64 16}
!127 = !{!65, !9, i64 8}
!128 = !{!63, !13, i64 16}
!129 = !{!63, !9, i64 8}
!130 = !{!11, !9, i64 8}
!131 = !{!11, !13, i64 16}
!132 = !{!70, !13, i64 16}
!133 = !{!70, !9, i64 8}
!134 = !{!67, !13, i64 16}
!135 = !{!67, !9, i64 8}
!136 = !{!137, !137, i64 0}
!137 = !{!"long", !4, i64 0}
!138 = !{!108, !13, i64 16}
!139 = !{!108, !9, i64 8}
