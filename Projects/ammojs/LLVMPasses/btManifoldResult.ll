; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionDispatch/btManifoldResult.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionDispatch/btManifoldResult.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, i32, i32, i32, i32, i8*, i8, float, float, float, float, float, float, float, i32, %class.btVector3, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%struct.btCollisionObjectWrapper = type { %struct.btCollisionObjectWrapper*, %class.btCollisionShape*, %class.btCollisionObject*, %class.btTransform*, i32, i32 }
%class.btCollisionShape = type opaque
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type opaque
%union.anon = type { i8* }
%class.btManifoldResult = type { %"struct.btDiscreteCollisionDetectorInterface::Result", %class.btPersistentManifold*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i32, i32, i32, i32 }
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }

$_ZNK17btCollisionObject11getFrictionEv = comdat any

$_ZNK17btCollisionObject14getRestitutionEv = comdat any

$_ZN36btDiscreteCollisionDetectorInterface6ResultC2Ev = comdat any

$_ZNK20btPersistentManifold8getBody0Ev = comdat any

$_ZNK24btCollisionObjectWrapper18getCollisionObjectEv = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK17btCollisionObject17getWorldTransformEv = comdat any

$_ZNK11btTransform8invXformERK9btVector3 = comdat any

$_ZN15btManifoldPointC2ERK9btVector3S2_S2_f = comdat any

$_Z32calculateCombinedRollingFrictionPK17btCollisionObjectS1_ = comdat any

$_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_ = comdat any

$_ZN20btPersistentManifold19replaceContactPointERK15btManifoldPointi = comdat any

$_ZNK17btCollisionObject17getCollisionFlagsEv = comdat any

$_ZN20btPersistentManifold15getContactPointEi = comdat any

$_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev = comdat any

$_ZN16btManifoldResultD0Ev = comdat any

$_ZN16btManifoldResult20setShapeIdentifiersAEii = comdat any

$_ZN16btManifoldResult20setShapeIdentifiersBEii = comdat any

$_ZN36btDiscreteCollisionDetectorInterface6ResultD0Ev = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZNK17btCollisionObject18getRollingFrictionEv = comdat any

$_ZNK15btManifoldPoint11getLifeTimeEv = comdat any

$_Z6btFabsf = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_Z6btSqrtf = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZTSN36btDiscreteCollisionDetectorInterface6ResultE = comdat any

$_ZTIN36btDiscreteCollisionDetectorInterface6ResultE = comdat any

$_ZTVN36btDiscreteCollisionDetectorInterface6ResultE = comdat any

@gContactAddedCallback = hidden global i1 (%class.btManifoldPoint*, %struct.btCollisionObjectWrapper*, i32, i32, %struct.btCollisionObjectWrapper*, i32, i32)* null, align 4
@_ZTV16btManifoldResult = hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI16btManifoldResult to i8*), i8* bitcast (%"struct.btDiscreteCollisionDetectorInterface::Result"* (%"struct.btDiscreteCollisionDetectorInterface::Result"*)* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev to i8*), i8* bitcast (void (%class.btManifoldResult*)* @_ZN16btManifoldResultD0Ev to i8*), i8* bitcast (void (%class.btManifoldResult*, i32, i32)* @_ZN16btManifoldResult20setShapeIdentifiersAEii to i8*), i8* bitcast (void (%class.btManifoldResult*, i32, i32)* @_ZN16btManifoldResult20setShapeIdentifiersBEii to i8*), i8* bitcast (void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)* @_ZN16btManifoldResult15addContactPointERK9btVector3S2_f to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS16btManifoldResult = hidden constant [19 x i8] c"16btManifoldResult\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTSN36btDiscreteCollisionDetectorInterface6ResultE = linkonce_odr hidden constant [48 x i8] c"N36btDiscreteCollisionDetectorInterface6ResultE\00", comdat, align 1
@_ZTIN36btDiscreteCollisionDetectorInterface6ResultE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([48 x i8], [48 x i8]* @_ZTSN36btDiscreteCollisionDetectorInterface6ResultE, i32 0, i32 0) }, comdat, align 4
@_ZTI16btManifoldResult = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([19 x i8], [19 x i8]* @_ZTS16btManifoldResult, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTIN36btDiscreteCollisionDetectorInterface6ResultE to i8*) }, align 4
@_ZTVN36btDiscreteCollisionDetectorInterface6ResultE = linkonce_odr hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTIN36btDiscreteCollisionDetectorInterface6ResultE to i8*), i8* bitcast (%"struct.btDiscreteCollisionDetectorInterface::Result"* (%"struct.btDiscreteCollisionDetectorInterface::Result"*)* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev to i8*), i8* bitcast (void (%"struct.btDiscreteCollisionDetectorInterface::Result"*)* @_ZN36btDiscreteCollisionDetectorInterface6ResultD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4

@_ZN16btManifoldResultC1EPK24btCollisionObjectWrapperS2_ = hidden unnamed_addr alias %class.btManifoldResult* (%class.btManifoldResult*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*), %class.btManifoldResult* (%class.btManifoldResult*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN16btManifoldResultC2EPK24btCollisionObjectWrapperS2_

define hidden float @_ZN16btManifoldResult25calculateCombinedFrictionEPK17btCollisionObjectS2_(%class.btCollisionObject* %body0, %class.btCollisionObject* %body1) #0 {
entry:
  %body0.addr = alloca %class.btCollisionObject*, align 4
  %body1.addr = alloca %class.btCollisionObject*, align 4
  %friction = alloca float, align 4
  %MAX_FRICTION = alloca float, align 4
  store %class.btCollisionObject* %body0, %class.btCollisionObject** %body0.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %body1, %class.btCollisionObject** %body1.addr, align 4, !tbaa !2
  %0 = bitcast float* %friction to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %body0.addr, align 4, !tbaa !2
  %call = call float @_ZNK17btCollisionObject11getFrictionEv(%class.btCollisionObject* %1)
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %body1.addr, align 4, !tbaa !2
  %call1 = call float @_ZNK17btCollisionObject11getFrictionEv(%class.btCollisionObject* %2)
  %mul = fmul float %call, %call1
  store float %mul, float* %friction, align 4, !tbaa !6
  %3 = bitcast float* %MAX_FRICTION to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  store float 1.000000e+01, float* %MAX_FRICTION, align 4, !tbaa !6
  %4 = load float, float* %friction, align 4, !tbaa !6
  %cmp = fcmp olt float %4, -1.000000e+01
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store float -1.000000e+01, float* %friction, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %5 = load float, float* %friction, align 4, !tbaa !6
  %cmp2 = fcmp ogt float %5, 1.000000e+01
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  store float 1.000000e+01, float* %friction, align 4, !tbaa !6
  br label %if.end4

if.end4:                                          ; preds = %if.then3, %if.end
  %6 = load float, float* %friction, align 4, !tbaa !6
  %7 = bitcast float* %MAX_FRICTION to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  %8 = bitcast float* %friction to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #9
  ret float %6
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK17btCollisionObject11getFrictionEv(%class.btCollisionObject* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_friction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 17
  %0 = load float, float* %m_friction, align 4, !tbaa !8
  ret float %0
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

define hidden float @_ZN16btManifoldResult28calculateCombinedRestitutionEPK17btCollisionObjectS2_(%class.btCollisionObject* %body0, %class.btCollisionObject* %body1) #0 {
entry:
  %body0.addr = alloca %class.btCollisionObject*, align 4
  %body1.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %body0, %class.btCollisionObject** %body0.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %body1, %class.btCollisionObject** %body1.addr, align 4, !tbaa !2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %body0.addr, align 4, !tbaa !2
  %call = call float @_ZNK17btCollisionObject14getRestitutionEv(%class.btCollisionObject* %0)
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %body1.addr, align 4, !tbaa !2
  %call1 = call float @_ZNK17btCollisionObject14getRestitutionEv(%class.btCollisionObject* %1)
  %mul = fmul float %call, %call1
  ret float %mul
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK17btCollisionObject14getRestitutionEv(%class.btCollisionObject* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_restitution = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 18
  %0 = load float, float* %m_restitution, align 4, !tbaa !14
  ret float %0
}

; Function Attrs: nounwind
define hidden %class.btManifoldResult* @_ZN16btManifoldResultC2EPK24btCollisionObjectWrapperS2_(%class.btManifoldResult* returned %this, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %0 = bitcast %class.btManifoldResult* %this1 to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  %call = call %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultC2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* %0) #9
  %1 = bitcast %class.btManifoldResult* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV16btManifoldResult, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !15
  %m_manifoldPtr = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  store %class.btPersistentManifold* null, %class.btPersistentManifold** %m_manifoldPtr, align 4, !tbaa !17
  %m_body0Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %2, %struct.btCollisionObjectWrapper** %m_body0Wrap, align 4, !tbaa !19
  %m_body1Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  %3 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %3, %struct.btCollisionObjectWrapper** %m_body1Wrap, align 4, !tbaa !20
  ret %class.btManifoldResult* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultC2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %this, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  %0 = bitcast %"struct.btDiscreteCollisionDetectorInterface::Result"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTVN36btDiscreteCollisionDetectorInterface6ResultE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !15
  ret %"struct.btDiscreteCollisionDetectorInterface::Result"* %this1
}

define hidden void @_ZN16btManifoldResult15addContactPointERK9btVector3S2_f(%class.btManifoldResult* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %normalOnBInWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %pointInWorld, float %depth) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %normalOnBInWorld.addr = alloca %class.btVector3*, align 4
  %pointInWorld.addr = alloca %class.btVector3*, align 4
  %depth.addr = alloca float, align 4
  %isSwapped = alloca i8, align 1
  %pointA = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %localA = alloca %class.btVector3, align 4
  %localB = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp12 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca %class.btVector3, align 4
  %ref.tmp20 = alloca %class.btVector3, align 4
  %newPt = alloca %class.btManifoldPoint, align 4
  %insertIndex = alloca i32, align 4
  %obj0Wrap = alloca %struct.btCollisionObjectWrapper*, align 4
  %obj1Wrap = alloca %struct.btCollisionObjectWrapper*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %normalOnBInWorld, %class.btVector3** %normalOnBInWorld.addr, align 4, !tbaa !2
  store %class.btVector3* %pointInWorld, %class.btVector3** %pointInWorld.addr, align 4, !tbaa !2
  store float %depth, float* %depth.addr, align 4, !tbaa !6
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %0 = load float, float* %depth.addr, align 4, !tbaa !6
  %m_manifoldPtr = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr, align 4, !tbaa !17
  %call = call float @_ZNK20btPersistentManifold27getContactBreakingThresholdEv(%class.btPersistentManifold* %1)
  %cmp = fcmp ogt float %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %isSwapped) #9
  %m_manifoldPtr2 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %2 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr2, align 4, !tbaa !17
  %call3 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %2)
  %m_body0Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %3 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap, align 4, !tbaa !19
  %call4 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %3)
  %cmp5 = icmp ne %class.btCollisionObject* %call3, %call4
  %frombool = zext i1 %cmp5 to i8
  store i8 %frombool, i8* %isSwapped, align 1, !tbaa !21
  %4 = bitcast %class.btVector3* %pointA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #9
  %5 = load %class.btVector3*, %class.btVector3** %pointInWorld.addr, align 4, !tbaa !2
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #9
  %7 = load %class.btVector3*, %class.btVector3** %normalOnBInWorld.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %7, float* nonnull align 4 dereferenceable(4) %depth.addr)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %pointA, %class.btVector3* nonnull align 4 dereferenceable(16) %5, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #9
  %9 = bitcast %class.btVector3* %localA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #9
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %localA)
  %10 = bitcast %class.btVector3* %localB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #9
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %localB)
  %11 = load i8, i8* %isSwapped, align 1, !tbaa !21, !range !23
  %tobool = trunc i8 %11 to i1
  br i1 %tobool, label %if.then8, label %if.else

if.then8:                                         ; preds = %if.end
  %12 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %12) #9
  %m_body1Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  %13 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body1Wrap, align 4, !tbaa !20
  %call10 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %13)
  %call11 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call10)
  call void @_ZNK11btTransform8invXformERK9btVector3(%class.btVector3* sret align 4 %ref.tmp9, %class.btTransform* %call11, %class.btVector3* nonnull align 4 dereferenceable(16) %pointA)
  %14 = bitcast %class.btVector3* %localA to i8*
  %15 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %15, i32 16, i1 false), !tbaa.struct !24
  %16 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %16) #9
  %17 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %17) #9
  %m_body0Wrap13 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %18 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap13, align 4, !tbaa !19
  %call14 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %18)
  %call15 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call14)
  %19 = load %class.btVector3*, %class.btVector3** %pointInWorld.addr, align 4, !tbaa !2
  call void @_ZNK11btTransform8invXformERK9btVector3(%class.btVector3* sret align 4 %ref.tmp12, %class.btTransform* %call15, %class.btVector3* nonnull align 4 dereferenceable(16) %19)
  %20 = bitcast %class.btVector3* %localB to i8*
  %21 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %20, i8* align 4 %21, i32 16, i1 false), !tbaa.struct !24
  %22 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %22) #9
  br label %if.end24

if.else:                                          ; preds = %if.end
  %23 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %23) #9
  %m_body0Wrap17 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %24 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap17, align 4, !tbaa !19
  %call18 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %24)
  %call19 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call18)
  call void @_ZNK11btTransform8invXformERK9btVector3(%class.btVector3* sret align 4 %ref.tmp16, %class.btTransform* %call19, %class.btVector3* nonnull align 4 dereferenceable(16) %pointA)
  %25 = bitcast %class.btVector3* %localA to i8*
  %26 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %25, i8* align 4 %26, i32 16, i1 false), !tbaa.struct !24
  %27 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %27) #9
  %28 = bitcast %class.btVector3* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %28) #9
  %m_body1Wrap21 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  %29 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body1Wrap21, align 4, !tbaa !20
  %call22 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %29)
  %call23 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call22)
  %30 = load %class.btVector3*, %class.btVector3** %pointInWorld.addr, align 4, !tbaa !2
  call void @_ZNK11btTransform8invXformERK9btVector3(%class.btVector3* sret align 4 %ref.tmp20, %class.btTransform* %call23, %class.btVector3* nonnull align 4 dereferenceable(16) %30)
  %31 = bitcast %class.btVector3* %localB to i8*
  %32 = bitcast %class.btVector3* %ref.tmp20 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %31, i8* align 4 %32, i32 16, i1 false), !tbaa.struct !24
  %33 = bitcast %class.btVector3* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %33) #9
  br label %if.end24

if.end24:                                         ; preds = %if.else, %if.then8
  %34 = bitcast %class.btManifoldPoint* %newPt to i8*
  call void @llvm.lifetime.start.p0i8(i64 184, i8* %34) #9
  %35 = load %class.btVector3*, %class.btVector3** %normalOnBInWorld.addr, align 4, !tbaa !2
  %36 = load float, float* %depth.addr, align 4, !tbaa !6
  %call25 = call %class.btManifoldPoint* @_ZN15btManifoldPointC2ERK9btVector3S2_S2_f(%class.btManifoldPoint* %newPt, %class.btVector3* nonnull align 4 dereferenceable(16) %localA, %class.btVector3* nonnull align 4 dereferenceable(16) %localB, %class.btVector3* nonnull align 4 dereferenceable(16) %35, float %36)
  %m_positionWorldOnA = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %newPt, i32 0, i32 3
  %37 = bitcast %class.btVector3* %m_positionWorldOnA to i8*
  %38 = bitcast %class.btVector3* %pointA to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %37, i8* align 4 %38, i32 16, i1 false), !tbaa.struct !24
  %39 = load %class.btVector3*, %class.btVector3** %pointInWorld.addr, align 4, !tbaa !2
  %m_positionWorldOnB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %newPt, i32 0, i32 2
  %40 = bitcast %class.btVector3* %m_positionWorldOnB to i8*
  %41 = bitcast %class.btVector3* %39 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %40, i8* align 4 %41, i32 16, i1 false), !tbaa.struct !24
  %42 = bitcast i32* %insertIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #9
  %m_manifoldPtr26 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %43 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr26, align 4, !tbaa !17
  %call27 = call i32 @_ZNK20btPersistentManifold13getCacheEntryERK15btManifoldPoint(%class.btPersistentManifold* %43, %class.btManifoldPoint* nonnull align 4 dereferenceable(184) %newPt)
  store i32 %call27, i32* %insertIndex, align 4, !tbaa !26
  %m_body0Wrap28 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %44 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap28, align 4, !tbaa !19
  %call29 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %44)
  %m_body1Wrap30 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  %45 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body1Wrap30, align 4, !tbaa !20
  %call31 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %45)
  %call32 = call float @_ZN16btManifoldResult25calculateCombinedFrictionEPK17btCollisionObjectS2_(%class.btCollisionObject* %call29, %class.btCollisionObject* %call31)
  %m_combinedFriction = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %newPt, i32 0, i32 6
  store float %call32, float* %m_combinedFriction, align 4, !tbaa !27
  %m_body0Wrap33 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %46 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap33, align 4, !tbaa !19
  %call34 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %46)
  %m_body1Wrap35 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  %47 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body1Wrap35, align 4, !tbaa !20
  %call36 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %47)
  %call37 = call float @_ZN16btManifoldResult28calculateCombinedRestitutionEPK17btCollisionObjectS2_(%class.btCollisionObject* %call34, %class.btCollisionObject* %call36)
  %m_combinedRestitution = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %newPt, i32 0, i32 8
  store float %call37, float* %m_combinedRestitution, align 4, !tbaa !29
  %m_body0Wrap38 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %48 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap38, align 4, !tbaa !19
  %call39 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %48)
  %m_body1Wrap40 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  %49 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body1Wrap40, align 4, !tbaa !20
  %call41 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %49)
  %call42 = call float @_Z32calculateCombinedRollingFrictionPK17btCollisionObjectS1_(%class.btCollisionObject* %call39, %class.btCollisionObject* %call41)
  %m_combinedRollingFriction = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %newPt, i32 0, i32 7
  store float %call42, float* %m_combinedRollingFriction, align 4, !tbaa !30
  %m_normalWorldOnB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %newPt, i32 0, i32 4
  %m_lateralFrictionDir1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %newPt, i32 0, i32 23
  %m_lateralFrictionDir2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %newPt, i32 0, i32 24
  call void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir2)
  %50 = load i8, i8* %isSwapped, align 1, !tbaa !21, !range !23
  %tobool43 = trunc i8 %50 to i1
  br i1 %tobool43, label %if.then44, label %if.else49

if.then44:                                        ; preds = %if.end24
  %m_partId1 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 5
  %51 = load i32, i32* %m_partId1, align 4, !tbaa !31
  %m_partId0 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %newPt, i32 0, i32 9
  store i32 %51, i32* %m_partId0, align 4, !tbaa !32
  %m_partId045 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 4
  %52 = load i32, i32* %m_partId045, align 4, !tbaa !33
  %m_partId146 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %newPt, i32 0, i32 10
  store i32 %52, i32* %m_partId146, align 4, !tbaa !34
  %m_index1 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 7
  %53 = load i32, i32* %m_index1, align 4, !tbaa !35
  %m_index0 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %newPt, i32 0, i32 11
  store i32 %53, i32* %m_index0, align 4, !tbaa !36
  %m_index047 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 6
  %54 = load i32, i32* %m_index047, align 4, !tbaa !37
  %m_index148 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %newPt, i32 0, i32 12
  store i32 %54, i32* %m_index148, align 4, !tbaa !38
  br label %if.end58

if.else49:                                        ; preds = %if.end24
  %m_partId050 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 4
  %55 = load i32, i32* %m_partId050, align 4, !tbaa !33
  %m_partId051 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %newPt, i32 0, i32 9
  store i32 %55, i32* %m_partId051, align 4, !tbaa !32
  %m_partId152 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 5
  %56 = load i32, i32* %m_partId152, align 4, !tbaa !31
  %m_partId153 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %newPt, i32 0, i32 10
  store i32 %56, i32* %m_partId153, align 4, !tbaa !34
  %m_index054 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 6
  %57 = load i32, i32* %m_index054, align 4, !tbaa !37
  %m_index055 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %newPt, i32 0, i32 11
  store i32 %57, i32* %m_index055, align 4, !tbaa !36
  %m_index156 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 7
  %58 = load i32, i32* %m_index156, align 4, !tbaa !35
  %m_index157 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %newPt, i32 0, i32 12
  store i32 %58, i32* %m_index157, align 4, !tbaa !38
  br label %if.end58

if.end58:                                         ; preds = %if.else49, %if.then44
  %59 = load i32, i32* %insertIndex, align 4, !tbaa !26
  %cmp59 = icmp sge i32 %59, 0
  br i1 %cmp59, label %if.then60, label %if.else62

if.then60:                                        ; preds = %if.end58
  %m_manifoldPtr61 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %60 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr61, align 4, !tbaa !17
  %61 = load i32, i32* %insertIndex, align 4, !tbaa !26
  call void @_ZN20btPersistentManifold19replaceContactPointERK15btManifoldPointi(%class.btPersistentManifold* %60, %class.btManifoldPoint* nonnull align 4 dereferenceable(184) %newPt, i32 %61)
  br label %if.end65

if.else62:                                        ; preds = %if.end58
  %m_manifoldPtr63 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %62 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr63, align 4, !tbaa !17
  %call64 = call i32 @_ZN20btPersistentManifold16addManifoldPointERK15btManifoldPointb(%class.btPersistentManifold* %62, %class.btManifoldPoint* nonnull align 4 dereferenceable(184) %newPt, i1 zeroext false)
  store i32 %call64, i32* %insertIndex, align 4, !tbaa !26
  br label %if.end65

if.end65:                                         ; preds = %if.else62, %if.then60
  %63 = load i1 (%class.btManifoldPoint*, %struct.btCollisionObjectWrapper*, i32, i32, %struct.btCollisionObjectWrapper*, i32, i32)*, i1 (%class.btManifoldPoint*, %struct.btCollisionObjectWrapper*, i32, i32, %struct.btCollisionObjectWrapper*, i32, i32)** @gContactAddedCallback, align 4, !tbaa !2
  %tobool66 = icmp ne i1 (%class.btManifoldPoint*, %struct.btCollisionObjectWrapper*, i32, i32, %struct.btCollisionObjectWrapper*, i32, i32)* %63, null
  br i1 %tobool66, label %land.lhs.true, label %if.end94

land.lhs.true:                                    ; preds = %if.end65
  %m_body0Wrap67 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %64 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap67, align 4, !tbaa !19
  %call68 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %64)
  %call69 = call i32 @_ZNK17btCollisionObject17getCollisionFlagsEv(%class.btCollisionObject* %call68)
  %and = and i32 %call69, 8
  %tobool70 = icmp ne i32 %and, 0
  br i1 %tobool70, label %if.then76, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.lhs.true
  %m_body1Wrap71 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  %65 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body1Wrap71, align 4, !tbaa !20
  %call72 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %65)
  %call73 = call i32 @_ZNK17btCollisionObject17getCollisionFlagsEv(%class.btCollisionObject* %call72)
  %and74 = and i32 %call73, 8
  %tobool75 = icmp ne i32 %and74, 0
  br i1 %tobool75, label %if.then76, label %if.end94

if.then76:                                        ; preds = %lor.lhs.false, %land.lhs.true
  %66 = bitcast %struct.btCollisionObjectWrapper** %obj0Wrap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #9
  %67 = load i8, i8* %isSwapped, align 1, !tbaa !21, !range !23
  %tobool77 = trunc i8 %67 to i1
  br i1 %tobool77, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then76
  %m_body1Wrap78 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  %68 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body1Wrap78, align 4, !tbaa !20
  br label %cond.end

cond.false:                                       ; preds = %if.then76
  %m_body0Wrap79 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %69 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap79, align 4, !tbaa !19
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.btCollisionObjectWrapper* [ %68, %cond.true ], [ %69, %cond.false ]
  store %struct.btCollisionObjectWrapper* %cond, %struct.btCollisionObjectWrapper** %obj0Wrap, align 4, !tbaa !2
  %70 = bitcast %struct.btCollisionObjectWrapper** %obj1Wrap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #9
  %71 = load i8, i8* %isSwapped, align 1, !tbaa !21, !range !23
  %tobool80 = trunc i8 %71 to i1
  br i1 %tobool80, label %cond.true81, label %cond.false83

cond.true81:                                      ; preds = %cond.end
  %m_body0Wrap82 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %72 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap82, align 4, !tbaa !19
  br label %cond.end85

cond.false83:                                     ; preds = %cond.end
  %m_body1Wrap84 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  %73 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body1Wrap84, align 4, !tbaa !20
  br label %cond.end85

cond.end85:                                       ; preds = %cond.false83, %cond.true81
  %cond86 = phi %struct.btCollisionObjectWrapper* [ %72, %cond.true81 ], [ %73, %cond.false83 ]
  store %struct.btCollisionObjectWrapper* %cond86, %struct.btCollisionObjectWrapper** %obj1Wrap, align 4, !tbaa !2
  %74 = load i1 (%class.btManifoldPoint*, %struct.btCollisionObjectWrapper*, i32, i32, %struct.btCollisionObjectWrapper*, i32, i32)*, i1 (%class.btManifoldPoint*, %struct.btCollisionObjectWrapper*, i32, i32, %struct.btCollisionObjectWrapper*, i32, i32)** @gContactAddedCallback, align 4, !tbaa !2
  %m_manifoldPtr87 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %75 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr87, align 4, !tbaa !17
  %76 = load i32, i32* %insertIndex, align 4, !tbaa !26
  %call88 = call nonnull align 4 dereferenceable(184) %class.btManifoldPoint* @_ZN20btPersistentManifold15getContactPointEi(%class.btPersistentManifold* %75, i32 %76)
  %77 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %obj0Wrap, align 4, !tbaa !2
  %m_partId089 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %newPt, i32 0, i32 9
  %78 = load i32, i32* %m_partId089, align 4, !tbaa !32
  %m_index090 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %newPt, i32 0, i32 11
  %79 = load i32, i32* %m_index090, align 4, !tbaa !36
  %80 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %obj1Wrap, align 4, !tbaa !2
  %m_partId191 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %newPt, i32 0, i32 10
  %81 = load i32, i32* %m_partId191, align 4, !tbaa !34
  %m_index192 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %newPt, i32 0, i32 12
  %82 = load i32, i32* %m_index192, align 4, !tbaa !38
  %call93 = call zeroext i1 %74(%class.btManifoldPoint* nonnull align 4 dereferenceable(184) %call88, %struct.btCollisionObjectWrapper* %77, i32 %78, i32 %79, %struct.btCollisionObjectWrapper* %80, i32 %81, i32 %82)
  %83 = bitcast %struct.btCollisionObjectWrapper** %obj1Wrap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #9
  %84 = bitcast %struct.btCollisionObjectWrapper** %obj0Wrap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #9
  br label %if.end94

if.end94:                                         ; preds = %cond.end85, %lor.lhs.false, %if.end65
  %85 = bitcast i32* %insertIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #9
  %86 = bitcast %class.btManifoldPoint* %newPt to i8*
  call void @llvm.lifetime.end.p0i8(i64 184, i8* %86) #9
  %87 = bitcast %class.btVector3* %localB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %87) #9
  %88 = bitcast %class.btVector3* %localA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %88) #9
  %89 = bitcast %class.btVector3* %pointA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %89) #9
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %isSwapped) #9
  br label %return

return:                                           ; preds = %if.end94, %if.then
  ret void
}

declare float @_ZNK20btPersistentManifold27getContactBreakingThresholdEv(%class.btPersistentManifold*) #4

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_body0 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body0, align 4, !tbaa !39
  ret %class.btCollisionObject* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %this) #3 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_collisionObject = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_collisionObject, align 4, !tbaa !41
  ret %class.btCollisionObject* %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #5 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !6
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !6
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !6
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !6
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !6
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !6
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #5 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !6
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !6
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !6
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !6
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !6
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !6
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransform8invXformERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %inVec) #5 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %inVec.addr = alloca %class.btVector3*, align 4
  %v = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %inVec, %class.btVector3** %inVec.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = bitcast %class.btVector3* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %inVec.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %v, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %2 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %2) #9
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* %m_basis)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %v)
  %3 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %3) #9
  %4 = bitcast %class.btVector3* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #9
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

define linkonce_odr hidden %class.btManifoldPoint* @_ZN15btManifoldPointC2ERK9btVector3S2_S2_f(%class.btManifoldPoint* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %pointA, %class.btVector3* nonnull align 4 dereferenceable(16) %pointB, %class.btVector3* nonnull align 4 dereferenceable(16) %normal, float %distance) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btManifoldPoint*, align 4
  %pointA.addr = alloca %class.btVector3*, align 4
  %pointB.addr = alloca %class.btVector3*, align 4
  %normal.addr = alloca %class.btVector3*, align 4
  %distance.addr = alloca float, align 4
  store %class.btManifoldPoint* %this, %class.btManifoldPoint** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %pointA, %class.btVector3** %pointA.addr, align 4, !tbaa !2
  store %class.btVector3* %pointB, %class.btVector3** %pointB.addr, align 4, !tbaa !2
  store %class.btVector3* %normal, %class.btVector3** %normal.addr, align 4, !tbaa !2
  store float %distance, float* %distance.addr, align 4, !tbaa !6
  %this1 = load %class.btManifoldPoint*, %class.btManifoldPoint** %this.addr, align 4
  %m_localPointA = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %pointA.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVector3* %m_localPointA to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !24
  %m_localPointB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 1
  %3 = load %class.btVector3*, %class.btVector3** %pointB.addr, align 4, !tbaa !2
  %4 = bitcast %class.btVector3* %m_localPointB to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !24
  %m_positionWorldOnB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 2
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_positionWorldOnB)
  %m_positionWorldOnA = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 3
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_positionWorldOnA)
  %m_normalWorldOnB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 4
  %6 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %7 = bitcast %class.btVector3* %m_normalWorldOnB to i8*
  %8 = bitcast %class.btVector3* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !24
  %m_distance1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 5
  %9 = load float, float* %distance.addr, align 4, !tbaa !6
  store float %9, float* %m_distance1, align 4, !tbaa !43
  %m_combinedFriction = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 6
  store float 0.000000e+00, float* %m_combinedFriction, align 4, !tbaa !27
  %m_combinedRollingFriction = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 7
  store float 0.000000e+00, float* %m_combinedRollingFriction, align 4, !tbaa !30
  %m_combinedRestitution = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 8
  store float 0.000000e+00, float* %m_combinedRestitution, align 4, !tbaa !29
  %m_userPersistentData = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 13
  store i8* null, i8** %m_userPersistentData, align 4, !tbaa !44
  %m_lateralFrictionInitialized = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 14
  store i8 0, i8* %m_lateralFrictionInitialized, align 4, !tbaa !45
  %m_appliedImpulse = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 15
  store float 0.000000e+00, float* %m_appliedImpulse, align 4, !tbaa !46
  %m_appliedImpulseLateral1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 16
  store float 0.000000e+00, float* %m_appliedImpulseLateral1, align 4, !tbaa !47
  %m_appliedImpulseLateral2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 17
  store float 0.000000e+00, float* %m_appliedImpulseLateral2, align 4, !tbaa !48
  %m_contactMotion1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 18
  store float 0.000000e+00, float* %m_contactMotion1, align 4, !tbaa !49
  %m_contactMotion2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 19
  store float 0.000000e+00, float* %m_contactMotion2, align 4, !tbaa !50
  %m_contactCFM1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 20
  store float 0.000000e+00, float* %m_contactCFM1, align 4, !tbaa !51
  %m_contactCFM2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 21
  store float 0.000000e+00, float* %m_contactCFM2, align 4, !tbaa !52
  %m_lifeTime = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 22
  store i32 0, i32* %m_lifeTime, align 4, !tbaa !53
  %m_lateralFrictionDir1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 23
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_lateralFrictionDir1)
  %m_lateralFrictionDir2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 24
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_lateralFrictionDir2)
  ret %class.btManifoldPoint* %this1
}

declare i32 @_ZNK20btPersistentManifold13getCacheEntryERK15btManifoldPoint(%class.btPersistentManifold*, %class.btManifoldPoint* nonnull align 4 dereferenceable(184)) #4

; Function Attrs: inlinehint
define linkonce_odr hidden float @_Z32calculateCombinedRollingFrictionPK17btCollisionObjectS1_(%class.btCollisionObject* %body0, %class.btCollisionObject* %body1) #5 comdat {
entry:
  %body0.addr = alloca %class.btCollisionObject*, align 4
  %body1.addr = alloca %class.btCollisionObject*, align 4
  %friction = alloca float, align 4
  %MAX_FRICTION = alloca float, align 4
  store %class.btCollisionObject* %body0, %class.btCollisionObject** %body0.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %body1, %class.btCollisionObject** %body1.addr, align 4, !tbaa !2
  %0 = bitcast float* %friction to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %body0.addr, align 4, !tbaa !2
  %call = call float @_ZNK17btCollisionObject18getRollingFrictionEv(%class.btCollisionObject* %1)
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %body1.addr, align 4, !tbaa !2
  %call1 = call float @_ZNK17btCollisionObject18getRollingFrictionEv(%class.btCollisionObject* %2)
  %mul = fmul float %call, %call1
  store float %mul, float* %friction, align 4, !tbaa !6
  %3 = bitcast float* %MAX_FRICTION to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  store float 1.000000e+01, float* %MAX_FRICTION, align 4, !tbaa !6
  %4 = load float, float* %friction, align 4, !tbaa !6
  %cmp = fcmp olt float %4, -1.000000e+01
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store float -1.000000e+01, float* %friction, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %5 = load float, float* %friction, align 4, !tbaa !6
  %cmp2 = fcmp ogt float %5, 1.000000e+01
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  store float 1.000000e+01, float* %friction, align 4, !tbaa !6
  br label %if.end4

if.end4:                                          ; preds = %if.then3, %if.end
  %6 = load float, float* %friction, align 4, !tbaa !6
  %7 = bitcast float* %MAX_FRICTION to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  %8 = bitcast float* %friction to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #9
  ret float %6
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %n, %class.btVector3* nonnull align 4 dereferenceable(16) %p, %class.btVector3* nonnull align 4 dereferenceable(16) %q) #5 comdat {
entry:
  %n.addr = alloca %class.btVector3*, align 4
  %p.addr = alloca %class.btVector3*, align 4
  %q.addr = alloca %class.btVector3*, align 4
  %a = alloca float, align 4
  %k = alloca float, align 4
  %a42 = alloca float, align 4
  %k54 = alloca float, align 4
  store %class.btVector3* %n, %class.btVector3** %n.addr, align 4, !tbaa !2
  store %class.btVector3* %p, %class.btVector3** %p.addr, align 4, !tbaa !2
  store %class.btVector3* %q, %class.btVector3** %q.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %0)
  %arrayidx = getelementptr inbounds float, float* %call, i32 2
  %1 = load float, float* %arrayidx, align 4, !tbaa !6
  %call1 = call float @_Z6btFabsf(float %1)
  %cmp = fcmp ogt float %call1, 0x3FE6A09E60000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = bitcast float* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %3 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %3)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 1
  %4 = load float, float* %arrayidx3, align 4, !tbaa !6
  %5 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call4 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %5)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 1
  %6 = load float, float* %arrayidx5, align 4, !tbaa !6
  %mul = fmul float %4, %6
  %7 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %7)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 2
  %8 = load float, float* %arrayidx7, align 4, !tbaa !6
  %9 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %9)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 2
  %10 = load float, float* %arrayidx9, align 4, !tbaa !6
  %mul10 = fmul float %8, %10
  %add = fadd float %mul, %mul10
  store float %add, float* %a, align 4, !tbaa !6
  %11 = bitcast float* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  %12 = load float, float* %a, align 4, !tbaa !6
  %call11 = call float @_Z6btSqrtf(float %12)
  %div = fdiv float 1.000000e+00, %call11
  store float %div, float* %k, align 4, !tbaa !6
  %13 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %13)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 0
  store float 0.000000e+00, float* %arrayidx13, align 4, !tbaa !6
  %14 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call14 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %14)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 2
  %15 = load float, float* %arrayidx15, align 4, !tbaa !6
  %fneg = fneg float %15
  %16 = load float, float* %k, align 4, !tbaa !6
  %mul16 = fmul float %fneg, %16
  %17 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %17)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 1
  store float %mul16, float* %arrayidx18, align 4, !tbaa !6
  %18 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call19 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %18)
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 1
  %19 = load float, float* %arrayidx20, align 4, !tbaa !6
  %20 = load float, float* %k, align 4, !tbaa !6
  %mul21 = fmul float %19, %20
  %21 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call22 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %21)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 2
  store float %mul21, float* %arrayidx23, align 4, !tbaa !6
  %22 = load float, float* %a, align 4, !tbaa !6
  %23 = load float, float* %k, align 4, !tbaa !6
  %mul24 = fmul float %22, %23
  %24 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %24)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 0
  store float %mul24, float* %arrayidx26, align 4, !tbaa !6
  %25 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call27 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %25)
  %arrayidx28 = getelementptr inbounds float, float* %call27, i32 0
  %26 = load float, float* %arrayidx28, align 4, !tbaa !6
  %fneg29 = fneg float %26
  %27 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %27)
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 2
  %28 = load float, float* %arrayidx31, align 4, !tbaa !6
  %mul32 = fmul float %fneg29, %28
  %29 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %29)
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 1
  store float %mul32, float* %arrayidx34, align 4, !tbaa !6
  %30 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call35 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %30)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 0
  %31 = load float, float* %arrayidx36, align 4, !tbaa !6
  %32 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call37 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %32)
  %arrayidx38 = getelementptr inbounds float, float* %call37, i32 1
  %33 = load float, float* %arrayidx38, align 4, !tbaa !6
  %mul39 = fmul float %31, %33
  %34 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %call40 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %34)
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 2
  store float %mul39, float* %arrayidx41, align 4, !tbaa !6
  %35 = bitcast float* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #9
  %36 = bitcast float* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #9
  br label %if.end

if.else:                                          ; preds = %entry
  %37 = bitcast float* %a42 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #9
  %38 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call43 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %38)
  %arrayidx44 = getelementptr inbounds float, float* %call43, i32 0
  %39 = load float, float* %arrayidx44, align 4, !tbaa !6
  %40 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call45 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %40)
  %arrayidx46 = getelementptr inbounds float, float* %call45, i32 0
  %41 = load float, float* %arrayidx46, align 4, !tbaa !6
  %mul47 = fmul float %39, %41
  %42 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call48 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %42)
  %arrayidx49 = getelementptr inbounds float, float* %call48, i32 1
  %43 = load float, float* %arrayidx49, align 4, !tbaa !6
  %44 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call50 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %44)
  %arrayidx51 = getelementptr inbounds float, float* %call50, i32 1
  %45 = load float, float* %arrayidx51, align 4, !tbaa !6
  %mul52 = fmul float %43, %45
  %add53 = fadd float %mul47, %mul52
  store float %add53, float* %a42, align 4, !tbaa !6
  %46 = bitcast float* %k54 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #9
  %47 = load float, float* %a42, align 4, !tbaa !6
  %call55 = call float @_Z6btSqrtf(float %47)
  %div56 = fdiv float 1.000000e+00, %call55
  store float %div56, float* %k54, align 4, !tbaa !6
  %48 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call57 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %48)
  %arrayidx58 = getelementptr inbounds float, float* %call57, i32 1
  %49 = load float, float* %arrayidx58, align 4, !tbaa !6
  %fneg59 = fneg float %49
  %50 = load float, float* %k54, align 4, !tbaa !6
  %mul60 = fmul float %fneg59, %50
  %51 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call61 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %51)
  %arrayidx62 = getelementptr inbounds float, float* %call61, i32 0
  store float %mul60, float* %arrayidx62, align 4, !tbaa !6
  %52 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call63 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %52)
  %arrayidx64 = getelementptr inbounds float, float* %call63, i32 0
  %53 = load float, float* %arrayidx64, align 4, !tbaa !6
  %54 = load float, float* %k54, align 4, !tbaa !6
  %mul65 = fmul float %53, %54
  %55 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call66 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %55)
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 1
  store float %mul65, float* %arrayidx67, align 4, !tbaa !6
  %56 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call68 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %56)
  %arrayidx69 = getelementptr inbounds float, float* %call68, i32 2
  store float 0.000000e+00, float* %arrayidx69, align 4, !tbaa !6
  %57 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call70 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %57)
  %arrayidx71 = getelementptr inbounds float, float* %call70, i32 2
  %58 = load float, float* %arrayidx71, align 4, !tbaa !6
  %fneg72 = fneg float %58
  %59 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call73 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %59)
  %arrayidx74 = getelementptr inbounds float, float* %call73, i32 1
  %60 = load float, float* %arrayidx74, align 4, !tbaa !6
  %mul75 = fmul float %fneg72, %60
  %61 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %call76 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %61)
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 0
  store float %mul75, float* %arrayidx77, align 4, !tbaa !6
  %62 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call78 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %62)
  %arrayidx79 = getelementptr inbounds float, float* %call78, i32 2
  %63 = load float, float* %arrayidx79, align 4, !tbaa !6
  %64 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call80 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %64)
  %arrayidx81 = getelementptr inbounds float, float* %call80, i32 0
  %65 = load float, float* %arrayidx81, align 4, !tbaa !6
  %mul82 = fmul float %63, %65
  %66 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %call83 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %66)
  %arrayidx84 = getelementptr inbounds float, float* %call83, i32 1
  store float %mul82, float* %arrayidx84, align 4, !tbaa !6
  %67 = load float, float* %a42, align 4, !tbaa !6
  %68 = load float, float* %k54, align 4, !tbaa !6
  %mul85 = fmul float %67, %68
  %69 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %call86 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %69)
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 2
  store float %mul85, float* %arrayidx87, align 4, !tbaa !6
  %70 = bitcast float* %k54 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #9
  %71 = bitcast float* %a42 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #9
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

define linkonce_odr hidden void @_ZN20btPersistentManifold19replaceContactPointERK15btManifoldPointi(%class.btPersistentManifold* %this, %class.btManifoldPoint* nonnull align 4 dereferenceable(184) %newPoint, i32 %insertIndex) #0 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  %newPoint.addr = alloca %class.btManifoldPoint*, align 4
  %insertIndex.addr = alloca i32, align 4
  %lifeTime = alloca i32, align 4
  %appliedImpulse = alloca float, align 4
  %appliedLateralImpulse1 = alloca float, align 4
  %appliedLateralImpulse2 = alloca float, align 4
  %cache = alloca i8*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4, !tbaa !2
  store %class.btManifoldPoint* %newPoint, %class.btManifoldPoint** %newPoint.addr, align 4, !tbaa !2
  store i32 %insertIndex, i32* %insertIndex.addr, align 4, !tbaa !26
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %0 = bitcast i32* %lifeTime to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %m_pointCache = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %1 = load i32, i32* %insertIndex.addr, align 4, !tbaa !26
  %arrayidx = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache, i32 0, i32 %1
  %call = call i32 @_ZNK15btManifoldPoint11getLifeTimeEv(%class.btManifoldPoint* %arrayidx)
  store i32 %call, i32* %lifeTime, align 4, !tbaa !26
  %2 = bitcast float* %appliedImpulse to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %m_pointCache2 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %3 = load i32, i32* %insertIndex.addr, align 4, !tbaa !26
  %arrayidx3 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache2, i32 0, i32 %3
  %m_appliedImpulse = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx3, i32 0, i32 15
  %4 = load float, float* %m_appliedImpulse, align 4, !tbaa !46
  store float %4, float* %appliedImpulse, align 4, !tbaa !6
  %5 = bitcast float* %appliedLateralImpulse1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %m_pointCache4 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %6 = load i32, i32* %insertIndex.addr, align 4, !tbaa !26
  %arrayidx5 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache4, i32 0, i32 %6
  %m_appliedImpulseLateral1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx5, i32 0, i32 16
  %7 = load float, float* %m_appliedImpulseLateral1, align 4, !tbaa !47
  store float %7, float* %appliedLateralImpulse1, align 4, !tbaa !6
  %8 = bitcast float* %appliedLateralImpulse2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  %m_pointCache6 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %9 = load i32, i32* %insertIndex.addr, align 4, !tbaa !26
  %arrayidx7 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache6, i32 0, i32 %9
  %m_appliedImpulseLateral2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx7, i32 0, i32 17
  %10 = load float, float* %m_appliedImpulseLateral2, align 4, !tbaa !48
  store float %10, float* %appliedLateralImpulse2, align 4, !tbaa !6
  %11 = bitcast i8** %cache to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  %m_pointCache8 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %12 = load i32, i32* %insertIndex.addr, align 4, !tbaa !26
  %arrayidx9 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache8, i32 0, i32 %12
  %m_userPersistentData = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx9, i32 0, i32 13
  %13 = load i8*, i8** %m_userPersistentData, align 4, !tbaa !44
  store i8* %13, i8** %cache, align 4, !tbaa !2
  %14 = load %class.btManifoldPoint*, %class.btManifoldPoint** %newPoint.addr, align 4, !tbaa !2
  %m_pointCache10 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %15 = load i32, i32* %insertIndex.addr, align 4, !tbaa !26
  %arrayidx11 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache10, i32 0, i32 %15
  %16 = bitcast %class.btManifoldPoint* %arrayidx11 to i8*
  %17 = bitcast %class.btManifoldPoint* %14 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 184, i1 false), !tbaa.struct !54
  %18 = load i8*, i8** %cache, align 4, !tbaa !2
  %m_pointCache12 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %19 = load i32, i32* %insertIndex.addr, align 4, !tbaa !26
  %arrayidx13 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache12, i32 0, i32 %19
  %m_userPersistentData14 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx13, i32 0, i32 13
  store i8* %18, i8** %m_userPersistentData14, align 4, !tbaa !44
  %20 = load float, float* %appliedImpulse, align 4, !tbaa !6
  %m_pointCache15 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %21 = load i32, i32* %insertIndex.addr, align 4, !tbaa !26
  %arrayidx16 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache15, i32 0, i32 %21
  %m_appliedImpulse17 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx16, i32 0, i32 15
  store float %20, float* %m_appliedImpulse17, align 4, !tbaa !46
  %22 = load float, float* %appliedLateralImpulse1, align 4, !tbaa !6
  %m_pointCache18 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %23 = load i32, i32* %insertIndex.addr, align 4, !tbaa !26
  %arrayidx19 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache18, i32 0, i32 %23
  %m_appliedImpulseLateral120 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx19, i32 0, i32 16
  store float %22, float* %m_appliedImpulseLateral120, align 4, !tbaa !47
  %24 = load float, float* %appliedLateralImpulse2, align 4, !tbaa !6
  %m_pointCache21 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %25 = load i32, i32* %insertIndex.addr, align 4, !tbaa !26
  %arrayidx22 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache21, i32 0, i32 %25
  %m_appliedImpulseLateral223 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx22, i32 0, i32 17
  store float %24, float* %m_appliedImpulseLateral223, align 4, !tbaa !48
  %26 = load float, float* %appliedImpulse, align 4, !tbaa !6
  %m_pointCache24 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %27 = load i32, i32* %insertIndex.addr, align 4, !tbaa !26
  %arrayidx25 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache24, i32 0, i32 %27
  %m_appliedImpulse26 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx25, i32 0, i32 15
  store float %26, float* %m_appliedImpulse26, align 4, !tbaa !46
  %28 = load float, float* %appliedLateralImpulse1, align 4, !tbaa !6
  %m_pointCache27 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %29 = load i32, i32* %insertIndex.addr, align 4, !tbaa !26
  %arrayidx28 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache27, i32 0, i32 %29
  %m_appliedImpulseLateral129 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx28, i32 0, i32 16
  store float %28, float* %m_appliedImpulseLateral129, align 4, !tbaa !47
  %30 = load float, float* %appliedLateralImpulse2, align 4, !tbaa !6
  %m_pointCache30 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %31 = load i32, i32* %insertIndex.addr, align 4, !tbaa !26
  %arrayidx31 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache30, i32 0, i32 %31
  %m_appliedImpulseLateral232 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx31, i32 0, i32 17
  store float %30, float* %m_appliedImpulseLateral232, align 4, !tbaa !48
  %32 = load i32, i32* %lifeTime, align 4, !tbaa !26
  %m_pointCache33 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %33 = load i32, i32* %insertIndex.addr, align 4, !tbaa !26
  %arrayidx34 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache33, i32 0, i32 %33
  %m_lifeTime = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx34, i32 0, i32 22
  store i32 %32, i32* %m_lifeTime, align 4, !tbaa !53
  %34 = bitcast i8** %cache to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #9
  %35 = bitcast float* %appliedLateralImpulse2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #9
  %36 = bitcast float* %appliedLateralImpulse1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #9
  %37 = bitcast float* %appliedImpulse to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #9
  %38 = bitcast i32* %lifeTime to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #9
  ret void
}

declare i32 @_ZN20btPersistentManifold16addManifoldPointERK15btManifoldPointb(%class.btPersistentManifold*, %class.btManifoldPoint* nonnull align 4 dereferenceable(184), i1 zeroext) #4

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK17btCollisionObject17getCollisionFlagsEv(%class.btCollisionObject* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4, !tbaa !55
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(184) %class.btManifoldPoint* @_ZN20btPersistentManifold15getContactPointEi(%class.btPersistentManifold* %this, i32 %index) #3 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  %index.addr = alloca i32, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !26
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_pointCache = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %0 = load i32, i32* %index.addr, align 4, !tbaa !26
  %arrayidx = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache, i32 0, i32 %0
  ret %class.btManifoldPoint* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %this, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  ret %"struct.btDiscreteCollisionDetectorInterface::Result"* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN16btManifoldResultD0Ev(%class.btManifoldResult* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %call = call %class.btManifoldResult* bitcast (%"struct.btDiscreteCollisionDetectorInterface::Result"* (%"struct.btDiscreteCollisionDetectorInterface::Result"*)* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev to %class.btManifoldResult* (%class.btManifoldResult*)*)(%class.btManifoldResult* %this1) #9
  %0 = bitcast %class.btManifoldResult* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN16btManifoldResult20setShapeIdentifiersAEii(%class.btManifoldResult* %this, i32 %partId0, i32 %index0) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %partId0.addr = alloca i32, align 4
  %index0.addr = alloca i32, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4, !tbaa !2
  store i32 %partId0, i32* %partId0.addr, align 4, !tbaa !26
  store i32 %index0, i32* %index0.addr, align 4, !tbaa !26
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %0 = load i32, i32* %partId0.addr, align 4, !tbaa !26
  %m_partId0 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 4
  store i32 %0, i32* %m_partId0, align 4, !tbaa !33
  %1 = load i32, i32* %index0.addr, align 4, !tbaa !26
  %m_index0 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 6
  store i32 %1, i32* %m_index0, align 4, !tbaa !37
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN16btManifoldResult20setShapeIdentifiersBEii(%class.btManifoldResult* %this, i32 %partId1, i32 %index1) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %partId1.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4, !tbaa !2
  store i32 %partId1, i32* %partId1.addr, align 4, !tbaa !26
  store i32 %index1, i32* %index1.addr, align 4, !tbaa !26
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %0 = load i32, i32* %partId1.addr, align 4, !tbaa !26
  %m_partId1 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 5
  store i32 %0, i32* %m_partId1, align 4, !tbaa !31
  %1 = load i32, i32* %index1.addr, align 4, !tbaa !26
  %m_index1 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 7
  store i32 %1, i32* %m_index1, align 4, !tbaa !35
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN36btDiscreteCollisionDetectorInterface6ResultD0Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %this, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  call void @llvm.trap() #11
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #6

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !6
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !6
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !6
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !6
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !6
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !6
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !6
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #5 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !6
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !6
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !6
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !6
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !6
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !6
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #5 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %1, i32 0)
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call1, float* %ref.tmp, align 4, !tbaa !6
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 1)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !6
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 2)
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !6
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  %10 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #5 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !26
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !26
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !6
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !6
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !6
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !6
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !6
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !6
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !6
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !6
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !6
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !6
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !6
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !6
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !6
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK17btCollisionObject18getRollingFrictionEv(%class.btCollisionObject* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_rollingFriction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 19
  %0 = load float, float* %m_rollingFriction, align 4, !tbaa !56
  ret float %0
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK15btManifoldPoint11getLifeTimeEv(%class.btManifoldPoint* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btManifoldPoint*, align 4
  store %class.btManifoldPoint* %this, %class.btManifoldPoint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btManifoldPoint*, %class.btManifoldPoint** %this.addr, align 4
  %m_lifeTime = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 22
  %0 = load i32, i32* %m_lifeTime, align 4, !tbaa !53
  ret i32 %0
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #7

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btFabsf(float %x) #3 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !6
  %0 = load float, float* %x.addr, align 4, !tbaa !6
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #3 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !6
  %0 = load float, float* %y.addr, align 4, !tbaa !6
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #8

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #8

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { cold noreturn nounwind }
attributes #7 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind readnone speculatable willreturn }
attributes #9 = { nounwind }
attributes #10 = { builtin nounwind }
attributes #11 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"float", !4, i64 0}
!8 = !{!9, !7, i64 224}
!9 = !{!"_ZTS17btCollisionObject", !10, i64 4, !10, i64 68, !12, i64 132, !12, i64 148, !12, i64 164, !13, i64 180, !7, i64 184, !3, i64 188, !3, i64 192, !3, i64 196, !3, i64 200, !13, i64 204, !13, i64 208, !13, i64 212, !13, i64 216, !7, i64 220, !7, i64 224, !7, i64 228, !7, i64 232, !13, i64 236, !4, i64 240, !7, i64 244, !7, i64 248, !7, i64 252, !13, i64 256, !13, i64 260}
!10 = !{!"_ZTS11btTransform", !11, i64 0, !12, i64 48}
!11 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!12 = !{!"_ZTS9btVector3", !4, i64 0}
!13 = !{!"int", !4, i64 0}
!14 = !{!9, !7, i64 228}
!15 = !{!16, !16, i64 0}
!16 = !{!"vtable pointer", !5, i64 0}
!17 = !{!18, !3, i64 4}
!18 = !{!"_ZTS16btManifoldResult", !3, i64 4, !3, i64 8, !3, i64 12, !13, i64 16, !13, i64 20, !13, i64 24, !13, i64 28}
!19 = !{!18, !3, i64 8}
!20 = !{!18, !3, i64 12}
!21 = !{!22, !22, i64 0}
!22 = !{!"bool", !4, i64 0}
!23 = !{i8 0, i8 2}
!24 = !{i64 0, i64 16, !25}
!25 = !{!4, !4, i64 0}
!26 = !{!13, !13, i64 0}
!27 = !{!28, !7, i64 84}
!28 = !{!"_ZTS15btManifoldPoint", !12, i64 0, !12, i64 16, !12, i64 32, !12, i64 48, !12, i64 64, !7, i64 80, !7, i64 84, !7, i64 88, !7, i64 92, !13, i64 96, !13, i64 100, !13, i64 104, !13, i64 108, !3, i64 112, !22, i64 116, !7, i64 120, !7, i64 124, !7, i64 128, !7, i64 132, !7, i64 136, !7, i64 140, !7, i64 144, !13, i64 148, !12, i64 152, !12, i64 168}
!29 = !{!28, !7, i64 92}
!30 = !{!28, !7, i64 88}
!31 = !{!18, !13, i64 20}
!32 = !{!28, !13, i64 96}
!33 = !{!18, !13, i64 16}
!34 = !{!28, !13, i64 100}
!35 = !{!18, !13, i64 28}
!36 = !{!28, !13, i64 104}
!37 = !{!18, !13, i64 24}
!38 = !{!28, !13, i64 108}
!39 = !{!40, !3, i64 740}
!40 = !{!"_ZTS20btPersistentManifold", !4, i64 4, !3, i64 740, !3, i64 744, !13, i64 748, !7, i64 752, !7, i64 756, !13, i64 760, !13, i64 764, !13, i64 768}
!41 = !{!42, !3, i64 8}
!42 = !{!"_ZTS24btCollisionObjectWrapper", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !13, i64 16, !13, i64 20}
!43 = !{!28, !7, i64 80}
!44 = !{!28, !3, i64 112}
!45 = !{!28, !22, i64 116}
!46 = !{!28, !7, i64 120}
!47 = !{!28, !7, i64 124}
!48 = !{!28, !7, i64 128}
!49 = !{!28, !7, i64 132}
!50 = !{!28, !7, i64 136}
!51 = !{!28, !7, i64 140}
!52 = !{!28, !7, i64 144}
!53 = !{!28, !13, i64 148}
!54 = !{i64 0, i64 16, !25, i64 16, i64 16, !25, i64 32, i64 16, !25, i64 48, i64 16, !25, i64 64, i64 16, !25, i64 80, i64 4, !6, i64 84, i64 4, !6, i64 88, i64 4, !6, i64 92, i64 4, !6, i64 96, i64 4, !26, i64 100, i64 4, !26, i64 104, i64 4, !26, i64 108, i64 4, !26, i64 112, i64 4, !2, i64 116, i64 1, !21, i64 120, i64 4, !6, i64 124, i64 4, !6, i64 128, i64 4, !6, i64 132, i64 4, !6, i64 136, i64 4, !6, i64 140, i64 4, !6, i64 144, i64 4, !6, i64 148, i64 4, !26, i64 152, i64 16, !25, i64 168, i64 16, !25}
!55 = !{!9, !13, i64 204}
!56 = !{!9, !7, i64 232}
