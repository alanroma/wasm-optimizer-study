; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/Featherstone/btMultiBodyPoint2Point.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/Featherstone/btMultiBodyPoint2Point.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btMultiBodyPoint2Point = type { %class.btMultiBodyConstraint, %class.btRigidBody*, %class.btRigidBody*, %class.btVector3, %class.btVector3 }
%class.btMultiBodyConstraint = type { i32 (...)**, %class.btMultiBody*, %class.btMultiBody*, i32, i32, i32, i32, i32, i32, i8, float, %class.btAlignedObjectArray.4 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btVector3 = type { [4 x float] }
%class.btMultiBody = type <{ %class.btMultiBodyLinkCollider*, %class.btVector3, %class.btQuaternion, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btAlignedObjectArray, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3, i8, i8, i8, i8, float, i32, float, float, i8, [3 x i8], float, i8, [3 x i8] }>
%class.btMultiBodyLinkCollider = type { %class.btCollisionObject, %class.btMultiBody*, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%union.anon = type { i8* }
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btMultibodyLink*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btMultibodyLink = type { float, float, %class.btVector3, i32, %class.btQuaternion, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i8, %class.btQuaternion, %class.btVector3, %class.btVector3, %class.btVector3, float, %class.btMultiBodyLinkCollider*, i32 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %class.btMultiBodyLinkCollider**, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, %class.btMatrix3x3*, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.16, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray.16 = type <{ %class.btAlignedAllocator.17, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.17 = type { i8 }
%class.btTypedConstraint = type opaque
%class.btAlignedObjectArray.20 = type <{ %class.btAlignedAllocator.21, [3 x i8], i32, i32, %struct.btMultiBodySolverConstraint*, i8, [3 x i8] }>
%class.btAlignedAllocator.21 = type { i8 }
%struct.btMultiBodySolverConstraint = type { i32, %class.btVector3, %class.btVector3, i32, i32, %class.btVector3, %class.btVector3, i32, %class.btVector3, %class.btVector3, float, float, float, float, float, float, float, float, float, %union.anon.23, i32, i32, i32, %class.btMultiBody*, i32, i32, %class.btMultiBody*, i32 }
%union.anon.23 = type { i8* }
%struct.btMultiBodyJacobianData = type { %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.25*, i32 }
%class.btAlignedObjectArray.25 = type opaque
%struct.btContactSolverInfo = type { %struct.btContactSolverInfoData }
%struct.btContactSolverInfoData = type { float, float, float, float, float, i32, float, float, float, float, float, i32, float, float, float, float, i32, i32, i32, float, float }

$_ZNK17btCollisionObject12getIslandTagEv = comdat any

$_ZN11btMultiBody15getBaseColliderEv = comdat any

$_ZNK11btMultiBody11getNumLinksEv = comdat any

$_ZN11btMultiBody7getLinkEi = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE21expandNonInitializingEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZNK17btCollisionObject14getCompanionIdEv = comdat any

$_ZNK11btRigidBody24getCenterOfMassTransformEv = comdat any

$_ZNK11btTransformmlERK9btVector3 = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE8allocateEiPPKS0_ = comdat any

$_ZN27btMultiBodySolverConstraintnwEmPv = comdat any

$_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE10deallocateEPS0_ = comdat any

@_ZTV22btMultiBodyPoint2Point = hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI22btMultiBodyPoint2Point to i8*), i8* bitcast (%class.btMultiBodyPoint2Point* (%class.btMultiBodyPoint2Point*)* @_ZN22btMultiBodyPoint2PointD1Ev to i8*), i8* bitcast (void (%class.btMultiBodyPoint2Point*)* @_ZN22btMultiBodyPoint2PointD0Ev to i8*), i8* bitcast (i32 (%class.btMultiBodyPoint2Point*)* @_ZNK22btMultiBodyPoint2Point12getIslandIdAEv to i8*), i8* bitcast (i32 (%class.btMultiBodyPoint2Point*)* @_ZNK22btMultiBodyPoint2Point12getIslandIdBEv to i8*), i8* bitcast (void (%class.btMultiBodyPoint2Point*, %class.btAlignedObjectArray.20*, %struct.btMultiBodyJacobianData*, %struct.btContactSolverInfo*)* @_ZN22btMultiBodyPoint2Point20createConstraintRowsER20btAlignedObjectArrayI27btMultiBodySolverConstraintER23btMultiBodyJacobianDataRK19btContactSolverInfo to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS22btMultiBodyPoint2Point = hidden constant [25 x i8] c"22btMultiBodyPoint2Point\00", align 1
@_ZTI21btMultiBodyConstraint = external constant i8*
@_ZTI22btMultiBodyPoint2Point = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([25 x i8], [25 x i8]* @_ZTS22btMultiBodyPoint2Point, i32 0, i32 0), i8* bitcast (i8** @_ZTI21btMultiBodyConstraint to i8*) }, align 4

@_ZN22btMultiBodyPoint2PointC1EP11btMultiBodyiP11btRigidBodyRK9btVector3S6_ = hidden unnamed_addr alias %class.btMultiBodyPoint2Point* (%class.btMultiBodyPoint2Point*, %class.btMultiBody*, i32, %class.btRigidBody*, %class.btVector3*, %class.btVector3*), %class.btMultiBodyPoint2Point* (%class.btMultiBodyPoint2Point*, %class.btMultiBody*, i32, %class.btRigidBody*, %class.btVector3*, %class.btVector3*)* @_ZN22btMultiBodyPoint2PointC2EP11btMultiBodyiP11btRigidBodyRK9btVector3S6_
@_ZN22btMultiBodyPoint2PointC1EP11btMultiBodyiS1_iRK9btVector3S4_ = hidden unnamed_addr alias %class.btMultiBodyPoint2Point* (%class.btMultiBodyPoint2Point*, %class.btMultiBody*, i32, %class.btMultiBody*, i32, %class.btVector3*, %class.btVector3*), %class.btMultiBodyPoint2Point* (%class.btMultiBodyPoint2Point*, %class.btMultiBody*, i32, %class.btMultiBody*, i32, %class.btVector3*, %class.btVector3*)* @_ZN22btMultiBodyPoint2PointC2EP11btMultiBodyiS1_iRK9btVector3S4_
@_ZN22btMultiBodyPoint2PointD1Ev = hidden unnamed_addr alias %class.btMultiBodyPoint2Point* (%class.btMultiBodyPoint2Point*), %class.btMultiBodyPoint2Point* (%class.btMultiBodyPoint2Point*)* @_ZN22btMultiBodyPoint2PointD2Ev

define hidden %class.btMultiBodyPoint2Point* @_ZN22btMultiBodyPoint2PointC2EP11btMultiBodyiP11btRigidBodyRK9btVector3S6_(%class.btMultiBodyPoint2Point* returned %this, %class.btMultiBody* %body, i32 %link, %class.btRigidBody* %bodyB, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotInA, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotInB) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btMultiBodyPoint2Point*, align 4
  %body.addr = alloca %class.btMultiBody*, align 4
  %link.addr = alloca i32, align 4
  %bodyB.addr = alloca %class.btRigidBody*, align 4
  %pivotInA.addr = alloca %class.btVector3*, align 4
  %pivotInB.addr = alloca %class.btVector3*, align 4
  store %class.btMultiBodyPoint2Point* %this, %class.btMultiBodyPoint2Point** %this.addr, align 4, !tbaa !2
  store %class.btMultiBody* %body, %class.btMultiBody** %body.addr, align 4, !tbaa !2
  store i32 %link, i32* %link.addr, align 4, !tbaa !6
  store %class.btRigidBody* %bodyB, %class.btRigidBody** %bodyB.addr, align 4, !tbaa !2
  store %class.btVector3* %pivotInA, %class.btVector3** %pivotInA.addr, align 4, !tbaa !2
  store %class.btVector3* %pivotInB, %class.btVector3** %pivotInB.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyPoint2Point*, %class.btMultiBodyPoint2Point** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyPoint2Point* %this1 to %class.btMultiBodyConstraint*
  %1 = load %class.btMultiBody*, %class.btMultiBody** %body.addr, align 4, !tbaa !2
  %2 = load i32, i32* %link.addr, align 4, !tbaa !6
  %call = call %class.btMultiBodyConstraint* @_ZN21btMultiBodyConstraintC2EP11btMultiBodyS1_iiib(%class.btMultiBodyConstraint* %0, %class.btMultiBody* %1, %class.btMultiBody* null, i32 %2, i32 -1, i32 3, i1 zeroext false)
  %3 = bitcast %class.btMultiBodyPoint2Point* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV22btMultiBodyPoint2Point, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %3, align 4, !tbaa !8
  %m_rigidBodyA = getelementptr inbounds %class.btMultiBodyPoint2Point, %class.btMultiBodyPoint2Point* %this1, i32 0, i32 1
  store %class.btRigidBody* null, %class.btRigidBody** %m_rigidBodyA, align 4, !tbaa !10
  %m_rigidBodyB = getelementptr inbounds %class.btMultiBodyPoint2Point, %class.btMultiBodyPoint2Point* %this1, i32 0, i32 2
  %4 = load %class.btRigidBody*, %class.btRigidBody** %bodyB.addr, align 4, !tbaa !2
  store %class.btRigidBody* %4, %class.btRigidBody** %m_rigidBodyB, align 4, !tbaa !13
  %m_pivotInA = getelementptr inbounds %class.btMultiBodyPoint2Point, %class.btMultiBodyPoint2Point* %this1, i32 0, i32 3
  %5 = load %class.btVector3*, %class.btVector3** %pivotInA.addr, align 4, !tbaa !2
  %6 = bitcast %class.btVector3* %m_pivotInA to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !14
  %m_pivotInB = getelementptr inbounds %class.btMultiBodyPoint2Point, %class.btMultiBodyPoint2Point* %this1, i32 0, i32 4
  %8 = load %class.btVector3*, %class.btVector3** %pivotInB.addr, align 4, !tbaa !2
  %9 = bitcast %class.btVector3* %m_pivotInB to i8*
  %10 = bitcast %class.btVector3* %8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false), !tbaa.struct !14
  ret %class.btMultiBodyPoint2Point* %this1
}

declare %class.btMultiBodyConstraint* @_ZN21btMultiBodyConstraintC2EP11btMultiBodyS1_iiib(%class.btMultiBodyConstraint* returned, %class.btMultiBody*, %class.btMultiBody*, i32, i32, i32, i1 zeroext) unnamed_addr #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

define hidden %class.btMultiBodyPoint2Point* @_ZN22btMultiBodyPoint2PointC2EP11btMultiBodyiS1_iRK9btVector3S4_(%class.btMultiBodyPoint2Point* returned %this, %class.btMultiBody* %bodyA, i32 %linkA, %class.btMultiBody* %bodyB, i32 %linkB, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotInA, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotInB) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btMultiBodyPoint2Point*, align 4
  %bodyA.addr = alloca %class.btMultiBody*, align 4
  %linkA.addr = alloca i32, align 4
  %bodyB.addr = alloca %class.btMultiBody*, align 4
  %linkB.addr = alloca i32, align 4
  %pivotInA.addr = alloca %class.btVector3*, align 4
  %pivotInB.addr = alloca %class.btVector3*, align 4
  store %class.btMultiBodyPoint2Point* %this, %class.btMultiBodyPoint2Point** %this.addr, align 4, !tbaa !2
  store %class.btMultiBody* %bodyA, %class.btMultiBody** %bodyA.addr, align 4, !tbaa !2
  store i32 %linkA, i32* %linkA.addr, align 4, !tbaa !6
  store %class.btMultiBody* %bodyB, %class.btMultiBody** %bodyB.addr, align 4, !tbaa !2
  store i32 %linkB, i32* %linkB.addr, align 4, !tbaa !6
  store %class.btVector3* %pivotInA, %class.btVector3** %pivotInA.addr, align 4, !tbaa !2
  store %class.btVector3* %pivotInB, %class.btVector3** %pivotInB.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyPoint2Point*, %class.btMultiBodyPoint2Point** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyPoint2Point* %this1 to %class.btMultiBodyConstraint*
  %1 = load %class.btMultiBody*, %class.btMultiBody** %bodyA.addr, align 4, !tbaa !2
  %2 = load %class.btMultiBody*, %class.btMultiBody** %bodyB.addr, align 4, !tbaa !2
  %3 = load i32, i32* %linkA.addr, align 4, !tbaa !6
  %4 = load i32, i32* %linkB.addr, align 4, !tbaa !6
  %call = call %class.btMultiBodyConstraint* @_ZN21btMultiBodyConstraintC2EP11btMultiBodyS1_iiib(%class.btMultiBodyConstraint* %0, %class.btMultiBody* %1, %class.btMultiBody* %2, i32 %3, i32 %4, i32 3, i1 zeroext false)
  %5 = bitcast %class.btMultiBodyPoint2Point* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV22btMultiBodyPoint2Point, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %5, align 4, !tbaa !8
  %m_rigidBodyA = getelementptr inbounds %class.btMultiBodyPoint2Point, %class.btMultiBodyPoint2Point* %this1, i32 0, i32 1
  store %class.btRigidBody* null, %class.btRigidBody** %m_rigidBodyA, align 4, !tbaa !10
  %m_rigidBodyB = getelementptr inbounds %class.btMultiBodyPoint2Point, %class.btMultiBodyPoint2Point* %this1, i32 0, i32 2
  store %class.btRigidBody* null, %class.btRigidBody** %m_rigidBodyB, align 4, !tbaa !13
  %m_pivotInA = getelementptr inbounds %class.btMultiBodyPoint2Point, %class.btMultiBodyPoint2Point* %this1, i32 0, i32 3
  %6 = load %class.btVector3*, %class.btVector3** %pivotInA.addr, align 4, !tbaa !2
  %7 = bitcast %class.btVector3* %m_pivotInA to i8*
  %8 = bitcast %class.btVector3* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !14
  %m_pivotInB = getelementptr inbounds %class.btMultiBodyPoint2Point, %class.btMultiBodyPoint2Point* %this1, i32 0, i32 4
  %9 = load %class.btVector3*, %class.btVector3** %pivotInB.addr, align 4, !tbaa !2
  %10 = bitcast %class.btVector3* %m_pivotInB to i8*
  %11 = bitcast %class.btVector3* %9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !14
  ret %class.btMultiBodyPoint2Point* %this1
}

; Function Attrs: nounwind
declare %class.btMultiBodyConstraint* @_ZN21btMultiBodyConstraintD2Ev(%class.btMultiBodyConstraint* returned) unnamed_addr #3

; Function Attrs: nounwind
define hidden %class.btMultiBodyPoint2Point* @_ZN22btMultiBodyPoint2PointD2Ev(%class.btMultiBodyPoint2Point* returned %this) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btMultiBodyPoint2Point*, align 4
  store %class.btMultiBodyPoint2Point* %this, %class.btMultiBodyPoint2Point** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyPoint2Point*, %class.btMultiBodyPoint2Point** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyPoint2Point* %this1 to %class.btMultiBodyConstraint*
  %call = call %class.btMultiBodyConstraint* @_ZN21btMultiBodyConstraintD2Ev(%class.btMultiBodyConstraint* %0) #8
  ret %class.btMultiBodyPoint2Point* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN22btMultiBodyPoint2PointD0Ev(%class.btMultiBodyPoint2Point* %this) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btMultiBodyPoint2Point*, align 4
  store %class.btMultiBodyPoint2Point* %this, %class.btMultiBodyPoint2Point** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyPoint2Point*, %class.btMultiBodyPoint2Point** %this.addr, align 4
  %call = call %class.btMultiBodyPoint2Point* @_ZN22btMultiBodyPoint2PointD1Ev(%class.btMultiBodyPoint2Point* %this1) #8
  %0 = bitcast %class.btMultiBodyPoint2Point* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #5

define hidden i32 @_ZNK22btMultiBodyPoint2Point12getIslandIdAEv(%class.btMultiBodyPoint2Point* %this) unnamed_addr #0 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btMultiBodyPoint2Point*, align 4
  %col = alloca %class.btMultiBodyLinkCollider*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btMultiBodyPoint2Point* %this, %class.btMultiBodyPoint2Point** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyPoint2Point*, %class.btMultiBodyPoint2Point** %this.addr, align 4
  %m_rigidBodyA = getelementptr inbounds %class.btMultiBodyPoint2Point, %class.btMultiBodyPoint2Point* %this1, i32 0, i32 1
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rigidBodyA, align 4, !tbaa !10
  %tobool = icmp ne %class.btRigidBody* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_rigidBodyA2 = getelementptr inbounds %class.btMultiBodyPoint2Point, %class.btMultiBodyPoint2Point* %this1, i32 0, i32 1
  %1 = load %class.btRigidBody*, %class.btRigidBody** %m_rigidBodyA2, align 4, !tbaa !10
  %2 = bitcast %class.btRigidBody* %1 to %class.btCollisionObject*
  %call = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %2)
  store i32 %call, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = bitcast %class.btMultiBodyPoint2Point* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %3, i32 0, i32 1
  %4 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA, align 4, !tbaa !16
  %tobool3 = icmp ne %class.btMultiBody* %4, null
  br i1 %tobool3, label %if.then4, label %if.end24

if.then4:                                         ; preds = %if.end
  %5 = bitcast %class.btMultiBodyLinkCollider** %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = bitcast %class.btMultiBodyPoint2Point* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA5 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %6, i32 0, i32 1
  %7 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA5, align 4, !tbaa !16
  %call6 = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %7)
  store %class.btMultiBodyLinkCollider* %call6, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %8 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %tobool7 = icmp ne %class.btMultiBodyLinkCollider* %8, null
  br i1 %tobool7, label %if.then8, label %if.end10

if.then8:                                         ; preds = %if.then4
  %9 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %10 = bitcast %class.btMultiBodyLinkCollider* %9 to %class.btCollisionObject*
  %call9 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %10)
  store i32 %call9, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup22

if.end10:                                         ; preds = %if.then4
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end10
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %13 = bitcast %class.btMultiBodyPoint2Point* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA11 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %13, i32 0, i32 1
  %14 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA11, align 4, !tbaa !16
  %call12 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %14)
  %cmp = icmp slt i32 %12, %call12
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body:                                         ; preds = %for.cond
  %15 = bitcast %class.btMultiBodyPoint2Point* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA13 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %15, i32 0, i32 1
  %16 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA13, align 4, !tbaa !16
  %17 = load i32, i32* %i, align 4, !tbaa !6
  %call14 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %16, i32 %17)
  %m_collider = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call14, i32 0, i32 15
  %18 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider, align 4, !tbaa !22
  %tobool15 = icmp ne %class.btMultiBodyLinkCollider* %18, null
  br i1 %tobool15, label %if.then16, label %if.end21

if.then16:                                        ; preds = %for.body
  %19 = bitcast %class.btMultiBodyPoint2Point* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA17 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %19, i32 0, i32 1
  %20 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA17, align 4, !tbaa !16
  %21 = load i32, i32* %i, align 4, !tbaa !6
  %call18 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %20, i32 %21)
  %m_collider19 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call18, i32 0, i32 15
  %22 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider19, align 4, !tbaa !22
  %23 = bitcast %class.btMultiBodyLinkCollider* %22 to %class.btCollisionObject*
  %call20 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %23)
  store i32 %call20, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end21:                                         ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end21
  %24 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %24, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

cleanup:                                          ; preds = %if.then16, %for.cond.cleanup
  %25 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup22 [
    i32 2, label %for.end
  ]

for.end:                                          ; preds = %cleanup
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup22

cleanup22:                                        ; preds = %for.end, %cleanup, %if.then8
  %26 = bitcast %class.btMultiBodyLinkCollider** %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #8
  %cleanup.dest23 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest23, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %return
  ]

cleanup.cont:                                     ; preds = %cleanup22
  br label %if.end24

if.end24:                                         ; preds = %cleanup.cont, %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end24, %cleanup22, %if.then
  %27 = load i32, i32* %retval, align 4
  ret i32 %27

unreachable:                                      ; preds = %cleanup22
  unreachable
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_islandTag1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 13
  %0 = load i32, i32* %m_islandTag1, align 4, !tbaa !25
  ret i32 %0
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: nounwind
define linkonce_odr hidden %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_baseCollider = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 0
  %0 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_baseCollider, align 4, !tbaa !29
  ret %class.btMultiBodyLinkCollider* %0
}

define linkonce_odr hidden i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %call = call i32 @_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv(%class.btAlignedObjectArray* %links)
  ret i32 %call
}

define linkonce_odr hidden nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %this, i32 %index) #0 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %index.addr = alloca i32, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !6
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %0 = load i32, i32* %index.addr, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links, i32 %0)
  ret %struct.btMultibodyLink* %call
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

define hidden i32 @_ZNK22btMultiBodyPoint2Point12getIslandIdBEv(%class.btMultiBodyPoint2Point* %this) unnamed_addr #0 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btMultiBodyPoint2Point*, align 4
  %col = alloca %class.btMultiBodyLinkCollider*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btMultiBodyPoint2Point* %this, %class.btMultiBodyPoint2Point** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyPoint2Point*, %class.btMultiBodyPoint2Point** %this.addr, align 4
  %m_rigidBodyB = getelementptr inbounds %class.btMultiBodyPoint2Point, %class.btMultiBodyPoint2Point* %this1, i32 0, i32 2
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rigidBodyB, align 4, !tbaa !13
  %tobool = icmp ne %class.btRigidBody* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_rigidBodyB2 = getelementptr inbounds %class.btMultiBodyPoint2Point, %class.btMultiBodyPoint2Point* %this1, i32 0, i32 2
  %1 = load %class.btRigidBody*, %class.btRigidBody** %m_rigidBodyB2, align 4, !tbaa !13
  %2 = bitcast %class.btRigidBody* %1 to %class.btCollisionObject*
  %call = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %2)
  store i32 %call, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = bitcast %class.btMultiBodyPoint2Point* %this1 to %class.btMultiBodyConstraint*
  %m_bodyB = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %3, i32 0, i32 2
  %4 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB, align 4, !tbaa !39
  %tobool3 = icmp ne %class.btMultiBody* %4, null
  br i1 %tobool3, label %if.then4, label %if.end21

if.then4:                                         ; preds = %if.end
  %5 = bitcast %class.btMultiBodyLinkCollider** %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = bitcast %class.btMultiBodyPoint2Point* %this1 to %class.btMultiBodyConstraint*
  %m_bodyB5 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %6, i32 0, i32 2
  %7 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB5, align 4, !tbaa !39
  %call6 = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %7)
  store %class.btMultiBodyLinkCollider* %call6, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %8 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %tobool7 = icmp ne %class.btMultiBodyLinkCollider* %8, null
  br i1 %tobool7, label %if.then8, label %if.end10

if.then8:                                         ; preds = %if.then4
  %9 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %10 = bitcast %class.btMultiBodyLinkCollider* %9 to %class.btCollisionObject*
  %call9 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %10)
  store i32 %call9, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup19

if.end10:                                         ; preds = %if.then4
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end10
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %13 = bitcast %class.btMultiBodyPoint2Point* %this1 to %class.btMultiBodyConstraint*
  %m_bodyB11 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %13, i32 0, i32 2
  %14 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB11, align 4, !tbaa !39
  %call12 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %14)
  %cmp = icmp slt i32 %12, %call12
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body:                                         ; preds = %for.cond
  %15 = bitcast %class.btMultiBodyPoint2Point* %this1 to %class.btMultiBodyConstraint*
  %m_bodyB13 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %15, i32 0, i32 2
  %16 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB13, align 4, !tbaa !39
  %17 = load i32, i32* %i, align 4, !tbaa !6
  %call14 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %16, i32 %17)
  %m_collider = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call14, i32 0, i32 15
  %18 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider, align 4, !tbaa !22
  store %class.btMultiBodyLinkCollider* %18, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %19 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %tobool15 = icmp ne %class.btMultiBodyLinkCollider* %19, null
  br i1 %tobool15, label %if.then16, label %if.end18

if.then16:                                        ; preds = %for.body
  %20 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %21 = bitcast %class.btMultiBodyLinkCollider* %20 to %class.btCollisionObject*
  %call17 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %21)
  store i32 %call17, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end18:                                         ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end18
  %22 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %22, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

cleanup:                                          ; preds = %if.then16, %for.cond.cleanup
  %23 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup19 [
    i32 2, label %for.end
  ]

for.end:                                          ; preds = %cleanup
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup19

cleanup19:                                        ; preds = %for.end, %cleanup, %if.then8
  %24 = bitcast %class.btMultiBodyLinkCollider** %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #8
  %cleanup.dest20 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest20, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %return
  ]

cleanup.cont:                                     ; preds = %cleanup19
  br label %if.end21

if.end21:                                         ; preds = %cleanup.cont, %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end21, %cleanup19, %if.then
  %25 = load i32, i32* %retval, align 4
  ret i32 %25

unreachable:                                      ; preds = %cleanup19
  unreachable
}

define hidden void @_ZN22btMultiBodyPoint2Point20createConstraintRowsER20btAlignedObjectArrayI27btMultiBodySolverConstraintER23btMultiBodyJacobianDataRK19btContactSolverInfo(%class.btMultiBodyPoint2Point* %this, %class.btAlignedObjectArray.20* nonnull align 4 dereferenceable(17) %constraintRows, %struct.btMultiBodyJacobianData* nonnull align 4 dereferenceable(128) %data, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %infoGlobal) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btMultiBodyPoint2Point*, align 4
  %constraintRows.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %data.addr = alloca %struct.btMultiBodyJacobianData*, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %i = alloca i32, align 4
  %constraintRow = alloca %struct.btMultiBodySolverConstraint*, align 4
  %contactNormalOnB = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %penetration = alloca float, align 4
  %pivotAworld = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca %class.btVector3, align 4
  %pivotBworld = alloca %class.btVector3, align 4
  %ref.tmp25 = alloca %class.btVector3, align 4
  %ref.tmp32 = alloca %class.btVector3, align 4
  %position = alloca float, align 4
  %ref.tmp37 = alloca %class.btVector3, align 4
  %relaxation = alloca float, align 4
  store %class.btMultiBodyPoint2Point* %this, %class.btMultiBodyPoint2Point** %this.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.20* %constraintRows, %class.btAlignedObjectArray.20** %constraintRows.addr, align 4, !tbaa !2
  store %struct.btMultiBodyJacobianData* %data, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyPoint2Point*, %class.btMultiBodyPoint2Point** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %struct.btMultiBodySolverConstraint** %constraintRow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %constraintRows.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(184) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE21expandNonInitializingEv(%class.btAlignedObjectArray.20* %4)
  store %struct.btMultiBodySolverConstraint* %call, %struct.btMultiBodySolverConstraint** %constraintRow, align 4, !tbaa !2
  %5 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_fixedBodyId = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %5, i32 0, i32 7
  %6 = load i32, i32* %m_fixedBodyId, align 4, !tbaa !40
  %7 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4, !tbaa !2
  %m_solverBodyIdA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %7, i32 0, i32 22
  store i32 %6, i32* %m_solverBodyIdA, align 4, !tbaa !42
  %8 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_fixedBodyId2 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %8, i32 0, i32 7
  %9 = load i32, i32* %m_fixedBodyId2, align 4, !tbaa !40
  %10 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4, !tbaa !2
  %m_solverBodyIdB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %10, i32 0, i32 25
  store i32 %9, i32* %m_solverBodyIdB, align 4, !tbaa !44
  %11 = bitcast %class.btVector3* %contactNormalOnB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #8
  %12 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !45
  %13 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !45
  %14 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #8
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !45
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %contactNormalOnB, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %15 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  %call6 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %contactNormalOnB)
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %call6, i32 %18
  store float -1.000000e+00, float* %arrayidx, align 4, !tbaa !45
  %19 = bitcast float* %penetration to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  store float 0.000000e+00, float* %penetration, align 4, !tbaa !45
  %20 = bitcast %class.btVector3* %pivotAworld to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %20) #8
  %m_pivotInA = getelementptr inbounds %class.btMultiBodyPoint2Point, %class.btMultiBodyPoint2Point* %this1, i32 0, i32 3
  %21 = bitcast %class.btVector3* %pivotAworld to i8*
  %22 = bitcast %class.btVector3* %m_pivotInA to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false), !tbaa.struct !14
  %m_rigidBodyA = getelementptr inbounds %class.btMultiBodyPoint2Point, %class.btMultiBodyPoint2Point* %this1, i32 0, i32 1
  %23 = load %class.btRigidBody*, %class.btRigidBody** %m_rigidBodyA, align 4, !tbaa !10
  %tobool = icmp ne %class.btRigidBody* %23, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %m_rigidBodyA7 = getelementptr inbounds %class.btMultiBodyPoint2Point, %class.btMultiBodyPoint2Point* %this1, i32 0, i32 1
  %24 = load %class.btRigidBody*, %class.btRigidBody** %m_rigidBodyA7, align 4, !tbaa !10
  %25 = bitcast %class.btRigidBody* %24 to %class.btCollisionObject*
  %call8 = call i32 @_ZNK17btCollisionObject14getCompanionIdEv(%class.btCollisionObject* %25)
  %26 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4, !tbaa !2
  %m_solverBodyIdA9 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %26, i32 0, i32 22
  store i32 %call8, i32* %m_solverBodyIdA9, align 4, !tbaa !42
  %27 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %27) #8
  %m_rigidBodyA11 = getelementptr inbounds %class.btMultiBodyPoint2Point, %class.btMultiBodyPoint2Point* %this1, i32 0, i32 1
  %28 = load %class.btRigidBody*, %class.btRigidBody** %m_rigidBodyA11, align 4, !tbaa !10
  %call12 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %28)
  %m_pivotInA13 = getelementptr inbounds %class.btMultiBodyPoint2Point, %class.btMultiBodyPoint2Point* %this1, i32 0, i32 3
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp10, %class.btTransform* %call12, %class.btVector3* nonnull align 4 dereferenceable(16) %m_pivotInA13)
  %29 = bitcast %class.btVector3* %pivotAworld to i8*
  %30 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %29, i8* align 4 %30, i32 16, i1 false), !tbaa.struct !14
  %31 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %31) #8
  br label %if.end19

if.else:                                          ; preds = %for.body
  %32 = bitcast %class.btMultiBodyPoint2Point* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %32, i32 0, i32 1
  %33 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA, align 4, !tbaa !16
  %tobool14 = icmp ne %class.btMultiBody* %33, null
  br i1 %tobool14, label %if.then15, label %if.end

if.then15:                                        ; preds = %if.else
  %34 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %34) #8
  %35 = bitcast %class.btMultiBodyPoint2Point* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA17 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %35, i32 0, i32 1
  %36 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA17, align 4, !tbaa !16
  %37 = bitcast %class.btMultiBodyPoint2Point* %this1 to %class.btMultiBodyConstraint*
  %m_linkA = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %37, i32 0, i32 3
  %38 = load i32, i32* %m_linkA, align 4, !tbaa !46
  %m_pivotInA18 = getelementptr inbounds %class.btMultiBodyPoint2Point, %class.btMultiBodyPoint2Point* %this1, i32 0, i32 3
  call void @_ZNK11btMultiBody15localPosToWorldEiRK9btVector3(%class.btVector3* sret align 4 %ref.tmp16, %class.btMultiBody* %36, i32 %38, %class.btVector3* nonnull align 4 dereferenceable(16) %m_pivotInA18)
  %39 = bitcast %class.btVector3* %pivotAworld to i8*
  %40 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %39, i8* align 4 %40, i32 16, i1 false), !tbaa.struct !14
  %41 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %41) #8
  br label %if.end

if.end:                                           ; preds = %if.then15, %if.else
  br label %if.end19

if.end19:                                         ; preds = %if.end, %if.then
  %42 = bitcast %class.btVector3* %pivotBworld to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %42) #8
  %m_pivotInB = getelementptr inbounds %class.btMultiBodyPoint2Point, %class.btMultiBodyPoint2Point* %this1, i32 0, i32 4
  %43 = bitcast %class.btVector3* %pivotBworld to i8*
  %44 = bitcast %class.btVector3* %m_pivotInB to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %43, i8* align 4 %44, i32 16, i1 false), !tbaa.struct !14
  %m_rigidBodyB = getelementptr inbounds %class.btMultiBodyPoint2Point, %class.btMultiBodyPoint2Point* %this1, i32 0, i32 2
  %45 = load %class.btRigidBody*, %class.btRigidBody** %m_rigidBodyB, align 4, !tbaa !13
  %tobool20 = icmp ne %class.btRigidBody* %45, null
  br i1 %tobool20, label %if.then21, label %if.else29

if.then21:                                        ; preds = %if.end19
  %m_rigidBodyB22 = getelementptr inbounds %class.btMultiBodyPoint2Point, %class.btMultiBodyPoint2Point* %this1, i32 0, i32 2
  %46 = load %class.btRigidBody*, %class.btRigidBody** %m_rigidBodyB22, align 4, !tbaa !13
  %47 = bitcast %class.btRigidBody* %46 to %class.btCollisionObject*
  %call23 = call i32 @_ZNK17btCollisionObject14getCompanionIdEv(%class.btCollisionObject* %47)
  %48 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4, !tbaa !2
  %m_solverBodyIdB24 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %48, i32 0, i32 25
  store i32 %call23, i32* %m_solverBodyIdB24, align 4, !tbaa !44
  %49 = bitcast %class.btVector3* %ref.tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %49) #8
  %m_rigidBodyB26 = getelementptr inbounds %class.btMultiBodyPoint2Point, %class.btMultiBodyPoint2Point* %this1, i32 0, i32 2
  %50 = load %class.btRigidBody*, %class.btRigidBody** %m_rigidBodyB26, align 4, !tbaa !13
  %call27 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %50)
  %m_pivotInB28 = getelementptr inbounds %class.btMultiBodyPoint2Point, %class.btMultiBodyPoint2Point* %this1, i32 0, i32 4
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp25, %class.btTransform* %call27, %class.btVector3* nonnull align 4 dereferenceable(16) %m_pivotInB28)
  %51 = bitcast %class.btVector3* %pivotBworld to i8*
  %52 = bitcast %class.btVector3* %ref.tmp25 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %51, i8* align 4 %52, i32 16, i1 false), !tbaa.struct !14
  %53 = bitcast %class.btVector3* %ref.tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %53) #8
  br label %if.end36

if.else29:                                        ; preds = %if.end19
  %54 = bitcast %class.btMultiBodyPoint2Point* %this1 to %class.btMultiBodyConstraint*
  %m_bodyB = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %54, i32 0, i32 2
  %55 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB, align 4, !tbaa !39
  %tobool30 = icmp ne %class.btMultiBody* %55, null
  br i1 %tobool30, label %if.then31, label %if.end35

if.then31:                                        ; preds = %if.else29
  %56 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %56) #8
  %57 = bitcast %class.btMultiBodyPoint2Point* %this1 to %class.btMultiBodyConstraint*
  %m_bodyB33 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %57, i32 0, i32 2
  %58 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB33, align 4, !tbaa !39
  %59 = bitcast %class.btMultiBodyPoint2Point* %this1 to %class.btMultiBodyConstraint*
  %m_linkB = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %59, i32 0, i32 4
  %60 = load i32, i32* %m_linkB, align 4, !tbaa !47
  %m_pivotInB34 = getelementptr inbounds %class.btMultiBodyPoint2Point, %class.btMultiBodyPoint2Point* %this1, i32 0, i32 4
  call void @_ZNK11btMultiBody15localPosToWorldEiRK9btVector3(%class.btVector3* sret align 4 %ref.tmp32, %class.btMultiBody* %58, i32 %60, %class.btVector3* nonnull align 4 dereferenceable(16) %m_pivotInB34)
  %61 = bitcast %class.btVector3* %pivotBworld to i8*
  %62 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %61, i8* align 4 %62, i32 16, i1 false), !tbaa.struct !14
  %63 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %63) #8
  br label %if.end35

if.end35:                                         ; preds = %if.then31, %if.else29
  br label %if.end36

if.end36:                                         ; preds = %if.end35, %if.then21
  %64 = bitcast float* %position to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #8
  %65 = bitcast %class.btVector3* %ref.tmp37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %65) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp37, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotAworld, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotBworld)
  %call38 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp37, %class.btVector3* nonnull align 4 dereferenceable(16) %contactNormalOnB)
  %66 = bitcast %class.btVector3* %ref.tmp37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %66) #8
  store float %call38, float* %position, align 4, !tbaa !45
  %67 = bitcast float* %relaxation to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #8
  store float 1.000000e+00, float* %relaxation, align 4, !tbaa !45
  %68 = bitcast %class.btMultiBodyPoint2Point* %this1 to %class.btMultiBodyConstraint*
  %69 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4, !tbaa !2
  %70 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %71 = load float, float* %position, align 4, !tbaa !45
  %72 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  call void @_ZN21btMultiBodyConstraint28fillMultiBodyConstraintMixedER27btMultiBodySolverConstraintR23btMultiBodyJacobianDataRK9btVector3S6_S6_fRK19btContactSolverInfoRfbff(%class.btMultiBodyConstraint* %68, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(184) %69, %struct.btMultiBodyJacobianData* nonnull align 4 dereferenceable(128) %70, %class.btVector3* nonnull align 4 dereferenceable(16) %contactNormalOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotAworld, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotBworld, float %71, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %72, float* nonnull align 4 dereferenceable(4) %relaxation, i1 zeroext false, float 0.000000e+00, float 0.000000e+00)
  %73 = bitcast %class.btMultiBodyPoint2Point* %this1 to %class.btMultiBodyConstraint*
  %m_maxAppliedImpulse = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %73, i32 0, i32 10
  %74 = load float, float* %m_maxAppliedImpulse, align 4, !tbaa !48
  %fneg = fneg float %74
  %75 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4, !tbaa !2
  %m_lowerLimit = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %75, i32 0, i32 16
  store float %fneg, float* %m_lowerLimit, align 4, !tbaa !49
  %76 = bitcast %class.btMultiBodyPoint2Point* %this1 to %class.btMultiBodyConstraint*
  %m_maxAppliedImpulse39 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %76, i32 0, i32 10
  %77 = load float, float* %m_maxAppliedImpulse39, align 4, !tbaa !48
  %78 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4, !tbaa !2
  %m_upperLimit = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %78, i32 0, i32 17
  store float %77, float* %m_upperLimit, align 4, !tbaa !50
  %79 = bitcast float* %relaxation to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #8
  %80 = bitcast float* %position to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #8
  %81 = bitcast %class.btVector3* %pivotBworld to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %81) #8
  %82 = bitcast %class.btVector3* %pivotAworld to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %82) #8
  %83 = bitcast float* %penetration to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #8
  %84 = bitcast %class.btVector3* %contactNormalOnB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %84) #8
  %85 = bitcast %struct.btMultiBodySolverConstraint** %constraintRow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #8
  br label %for.inc

for.inc:                                          ; preds = %if.end36
  %86 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %86, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(184) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE21expandNonInitializingEv(%class.btAlignedObjectArray.20* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.20* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !6
  %1 = load i32, i32* %sz, align 4, !tbaa !6
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE8capacityEv(%class.btAlignedObjectArray.20* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.20* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE9allocSizeEi(%class.btAlignedObjectArray.20* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7reserveEi(%class.btAlignedObjectArray.20* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4, !tbaa !51
  %inc = add nsw i32 %2, 1
  store i32 %inc, i32* %m_size, align 4, !tbaa !51
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %3 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4, !tbaa !54
  %4 = load i32, i32* %sz, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %3, i32 %4
  %5 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  ret %struct.btMultiBodySolverConstraint* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !45
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !45
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !45
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !45
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !45
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !45
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !45
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK17btCollisionObject14getCompanionIdEv(%class.btCollisionObject* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_companionId = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 14
  %0 = load i32, i32* %m_companionId, align 4, !tbaa !55
  ret i32 %0
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #7 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

declare void @_ZNK11btMultiBody15localPosToWorldEiRK9btVector3(%class.btVector3* sret align 4, %class.btMultiBody*, i32, %class.btVector3* nonnull align 4 dereferenceable(16)) #1

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #7 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !45
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !45
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !45
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !45
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !45
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !45
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !45
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !45
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !45
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !45
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !45
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !45
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !45
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !45
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !45
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

declare void @_ZN21btMultiBodyConstraint28fillMultiBodyConstraintMixedER27btMultiBodySolverConstraintR23btMultiBodyJacobianDataRK9btVector3S6_S6_fRK19btContactSolverInfoRfbff(%class.btMultiBodyConstraint*, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(184), %struct.btMultiBodyJacobianData* nonnull align 4 dereferenceable(128), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), float, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84), float* nonnull align 4 dereferenceable(4), i1 zeroext, float, float) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv(%class.btAlignedObjectArray* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !56
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %this, i32 %n) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btMultibodyLink*, %struct.btMultibodyLink** %m_data, align 4, !tbaa !57
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %0, i32 %1
  ret %struct.btMultibodyLink* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #7 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #7 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !45
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !45
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !45
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !45
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !45
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !45
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !45
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !45
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !45
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #7 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4, !tbaa !45
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !45
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !45
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #6 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.20* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !51
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE8capacityEv(%class.btAlignedObjectArray.20* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !58
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7reserveEi(%class.btAlignedObjectArray.20* %this, i32 %_Count) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btMultiBodySolverConstraint*, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE8capacityEv(%class.btAlignedObjectArray.20* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btMultiBodySolverConstraint** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE8allocateEi(%class.btAlignedObjectArray.20* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.btMultiBodySolverConstraint*
  store %struct.btMultiBodySolverConstraint* %3, %struct.btMultiBodySolverConstraint** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.20* %this1)
  %4 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4copyEiiPS0_(%class.btAlignedObjectArray.20* %this1, i32 0, i32 %call3, %struct.btMultiBodySolverConstraint* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.20* %this1)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7destroyEii(%class.btAlignedObjectArray.20* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE10deallocateEv(%class.btAlignedObjectArray.20* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !59
  %5 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  store %struct.btMultiBodySolverConstraint* %5, %struct.btMultiBodySolverConstraint** %m_data, align 4, !tbaa !54
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !58
  %7 = bitcast %struct.btMultiBodySolverConstraint** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE9allocSizeEi(%class.btAlignedObjectArray.20* %this, i32 %size) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE8allocateEi(%class.btAlignedObjectArray.20* %this, i32 %size) #7 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call %struct.btMultiBodySolverConstraint* @_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.21* %m_allocator, i32 %1, %struct.btMultiBodySolverConstraint** null)
  %2 = bitcast %struct.btMultiBodySolverConstraint* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4copyEiiPS0_(%class.btAlignedObjectArray.20* %this, i32 %start, i32 %end, %struct.btMultiBodySolverConstraint* %dest) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btMultiBodySolverConstraint*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store %struct.btMultiBodySolverConstraint* %dest, %struct.btMultiBodySolverConstraint** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %4, i32 %5
  %6 = bitcast %struct.btMultiBodySolverConstraint* %arrayidx to i8*
  %call = call i8* @_ZN27btMultiBodySolverConstraintnwEmPv(i32 184, i8* %6)
  %7 = bitcast i8* %call to %struct.btMultiBodySolverConstraint*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %8 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4, !tbaa !54
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %8, i32 %9
  %10 = bitcast %struct.btMultiBodySolverConstraint* %7 to i8*
  %11 = bitcast %struct.btMultiBodySolverConstraint* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 184, i1 false), !tbaa.struct !60
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7destroyEii(%class.btAlignedObjectArray.20* %this, i32 %first, i32 %last) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %4 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4, !tbaa !54
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE10deallocateEv(%class.btAlignedObjectArray.20* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %0 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4, !tbaa !54
  %tobool = icmp ne %struct.btMultiBodySolverConstraint* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !59, !range !61
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %2 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data4, align 4, !tbaa !54
  call void @_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE10deallocateEPS0_(%class.btAlignedAllocator.21* %m_allocator, %struct.btMultiBodySolverConstraint* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  store %struct.btMultiBodySolverConstraint* null, %struct.btMultiBodySolverConstraint** %m_data5, align 4, !tbaa !54
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden %struct.btMultiBodySolverConstraint* @_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.21* %this, i32 %n, %struct.btMultiBodySolverConstraint** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.21*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btMultiBodySolverConstraint**, align 4
  store %class.btAlignedAllocator.21* %this, %class.btAlignedAllocator.21** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store %struct.btMultiBodySolverConstraint** %hint, %struct.btMultiBodySolverConstraint*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.21*, %class.btAlignedAllocator.21** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 184, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btMultiBodySolverConstraint*
  ret %struct.btMultiBodySolverConstraint* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN27btMultiBodySolverConstraintnwEmPv(i32 %0, i8* %ptr) #6 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !62
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE10deallocateEPS0_(%class.btAlignedAllocator.21* %this, %struct.btMultiBodySolverConstraint* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.21*, align 4
  %ptr.addr = alloca %struct.btMultiBodySolverConstraint*, align 4
  store %class.btAlignedAllocator.21* %this, %class.btAlignedAllocator.21** %this.addr, align 4, !tbaa !2
  store %struct.btMultiBodySolverConstraint* %ptr, %struct.btMultiBodySolverConstraint** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.21*, %class.btAlignedAllocator.21** %this.addr, align 4
  %0 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btMultiBodySolverConstraint* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind }
attributes #9 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"vtable pointer", !5, i64 0}
!10 = !{!11, !3, i64 64}
!11 = !{!"_ZTS22btMultiBodyPoint2Point", !3, i64 64, !3, i64 68, !12, i64 72, !12, i64 88}
!12 = !{!"_ZTS9btVector3", !4, i64 0}
!13 = !{!11, !3, i64 68}
!14 = !{i64 0, i64 16, !15}
!15 = !{!4, !4, i64 0}
!16 = !{!17, !3, i64 4}
!17 = !{!"_ZTS21btMultiBodyConstraint", !3, i64 4, !3, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !18, i64 36, !19, i64 40, !20, i64 44}
!18 = !{!"bool", !4, i64 0}
!19 = !{!"float", !4, i64 0}
!20 = !{!"_ZTS20btAlignedObjectArrayIfE", !21, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !18, i64 16}
!21 = !{!"_ZTS18btAlignedAllocatorIfLj16EE"}
!22 = !{!23, !3, i64 180}
!23 = !{!"_ZTS15btMultibodyLink", !19, i64 0, !19, i64 4, !12, i64 8, !7, i64 24, !24, i64 28, !12, i64 44, !12, i64 60, !12, i64 76, !12, i64 92, !18, i64 108, !24, i64 112, !12, i64 128, !12, i64 144, !12, i64 160, !19, i64 176, !3, i64 180, !7, i64 184}
!24 = !{!"_ZTS12btQuaternion"}
!25 = !{!26, !7, i64 208}
!26 = !{!"_ZTS17btCollisionObject", !27, i64 4, !27, i64 68, !12, i64 132, !12, i64 148, !12, i64 164, !7, i64 180, !19, i64 184, !3, i64 188, !3, i64 192, !3, i64 196, !3, i64 200, !7, i64 204, !7, i64 208, !7, i64 212, !7, i64 216, !19, i64 220, !19, i64 224, !19, i64 228, !19, i64 232, !7, i64 236, !4, i64 240, !19, i64 244, !19, i64 248, !19, i64 252, !7, i64 256, !7, i64 260}
!27 = !{!"_ZTS11btTransform", !28, i64 0, !12, i64 48}
!28 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!29 = !{!30, !3, i64 0}
!30 = !{!"_ZTS11btMultiBody", !3, i64 0, !12, i64 4, !24, i64 20, !19, i64 36, !12, i64 40, !12, i64 56, !12, i64 72, !31, i64 88, !33, i64 108, !20, i64 128, !35, i64 148, !37, i64 168, !28, i64 188, !28, i64 236, !28, i64 284, !28, i64 332, !18, i64 380, !18, i64 381, !18, i64 382, !19, i64 384, !7, i64 388, !19, i64 392, !19, i64 396, !18, i64 400, !19, i64 404, !18, i64 408}
!31 = !{!"_ZTS20btAlignedObjectArrayI15btMultibodyLinkE", !32, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !18, i64 16}
!32 = !{!"_ZTS18btAlignedAllocatorI15btMultibodyLinkLj16EE"}
!33 = !{!"_ZTS20btAlignedObjectArrayIP23btMultiBodyLinkColliderE", !34, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !18, i64 16}
!34 = !{!"_ZTS18btAlignedAllocatorIP23btMultiBodyLinkColliderLj16EE"}
!35 = !{!"_ZTS20btAlignedObjectArrayI9btVector3E", !36, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !18, i64 16}
!36 = !{!"_ZTS18btAlignedAllocatorI9btVector3Lj16EE"}
!37 = !{!"_ZTS20btAlignedObjectArrayI11btMatrix3x3E", !38, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !18, i64 16}
!38 = !{!"_ZTS18btAlignedAllocatorI11btMatrix3x3Lj16EE"}
!39 = !{!17, !3, i64 8}
!40 = !{!41, !7, i64 124}
!41 = !{!"_ZTS23btMultiBodyJacobianData", !20, i64 0, !20, i64 20, !20, i64 40, !20, i64 60, !35, i64 80, !37, i64 100, !3, i64 120, !7, i64 124}
!42 = !{!43, !7, i64 160}
!43 = !{!"_ZTS27btMultiBodySolverConstraint", !7, i64 0, !12, i64 4, !12, i64 20, !7, i64 36, !7, i64 40, !12, i64 44, !12, i64 60, !7, i64 76, !12, i64 80, !12, i64 96, !19, i64 112, !19, i64 116, !19, i64 120, !19, i64 124, !19, i64 128, !19, i64 132, !19, i64 136, !19, i64 140, !19, i64 144, !4, i64 148, !7, i64 152, !7, i64 156, !7, i64 160, !3, i64 164, !7, i64 168, !7, i64 172, !3, i64 176, !7, i64 180}
!44 = !{!43, !7, i64 172}
!45 = !{!19, !19, i64 0}
!46 = !{!17, !7, i64 12}
!47 = !{!17, !7, i64 16}
!48 = !{!17, !19, i64 40}
!49 = !{!43, !19, i64 136}
!50 = !{!43, !19, i64 140}
!51 = !{!52, !7, i64 4}
!52 = !{!"_ZTS20btAlignedObjectArrayI27btMultiBodySolverConstraintE", !53, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !18, i64 16}
!53 = !{!"_ZTS18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE"}
!54 = !{!52, !3, i64 12}
!55 = !{!26, !7, i64 212}
!56 = !{!31, !7, i64 4}
!57 = !{!31, !3, i64 12}
!58 = !{!52, !7, i64 8}
!59 = !{!52, !18, i64 16}
!60 = !{i64 0, i64 4, !6, i64 4, i64 16, !15, i64 20, i64 16, !15, i64 36, i64 4, !6, i64 40, i64 4, !6, i64 44, i64 16, !15, i64 60, i64 16, !15, i64 76, i64 4, !6, i64 80, i64 16, !15, i64 96, i64 16, !15, i64 112, i64 4, !45, i64 116, i64 4, !45, i64 120, i64 4, !45, i64 124, i64 4, !45, i64 128, i64 4, !45, i64 132, i64 4, !45, i64 136, i64 4, !45, i64 140, i64 4, !45, i64 144, i64 4, !45, i64 148, i64 4, !2, i64 148, i64 4, !45, i64 152, i64 4, !6, i64 156, i64 4, !6, i64 160, i64 4, !6, i64 164, i64 4, !2, i64 168, i64 4, !6, i64 172, i64 4, !6, i64 176, i64 4, !2, i64 180, i64 4, !6}
!61 = !{i8 0, i8 2}
!62 = !{!63, !63, i64 0}
!63 = !{!"long", !4, i64 0}
