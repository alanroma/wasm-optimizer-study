; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/Featherstone/btMultiBody.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/Featherstone/btMultiBody.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btMultiBody = type <{ %class.btMultiBodyLinkCollider*, %class.btVector3, %class.btQuaternion, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btAlignedObjectArray, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3, i8, i8, i8, i8, float, i32, float, float, i8, [3 x i8], float, i8, [3 x i8] }>
%class.btMultiBodyLinkCollider = type { %class.btCollisionObject, %class.btMultiBody*, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%struct.btBroadphaseProxy = type opaque
%class.btCollisionShape = type opaque
%union.anon = type { i8* }
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }
%class.btVector3 = type { [4 x float] }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btMultibodyLink*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btMultibodyLink = type { float, float, %class.btVector3, i32, %class.btQuaternion, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i8, %class.btQuaternion, %class.btVector3, %class.btVector3, %class.btVector3, float, %class.btMultiBodyLinkCollider*, i32 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %class.btMultiBodyLinkCollider**, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, %class.btMatrix3x3*, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btAlignedObjectArray.16 = type <{ %class.btAlignedAllocator.17, [3 x i8], i32, i32, %class.btQuaternion*, i8, [3 x i8] }>
%class.btAlignedAllocator.17 = type { i8 }

$_ZN9btVector3C2Ev = comdat any

$_ZN12btQuaternionC2ERKfS1_S1_S1_ = comdat any

$_ZN20btAlignedObjectArrayI15btMultibodyLinkEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP23btMultiBodyLinkColliderEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIfEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EC2Ev = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3EC2Ev = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN20btAlignedObjectArrayI15btMultibodyLinkE6resizeEiRKS0_ = comdat any

$_ZN15btMultibodyLinkC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_ = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3E6resizeEiRKS0_ = comdat any

$_ZN20btAlignedObjectArrayIfE6resizeEiRKf = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3ED2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3ED2Ev = comdat any

$_ZN20btAlignedObjectArrayIfED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP23btMultiBodyLinkColliderED2Ev = comdat any

$_ZN20btAlignedObjectArrayI15btMultibodyLinkED2Ev = comdat any

$_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi = comdat any

$_ZN15btMultibodyLink11updateCacheEv = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi = comdat any

$_ZNK20btAlignedObjectArrayIfEixEi = comdat any

$_ZN20btAlignedObjectArrayIfEixEi = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_Z10quatRotateRK12btQuaternionRK9btVector3 = comdat any

$_ZNK12btQuaternion7inverseEv = comdat any

$_ZNK11btMultiBody17getWorldToBaseRotEv = comdat any

$_ZNK11btMultiBody10getBasePosEv = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK11btMultiBody11getNumLinksEv = comdat any

$_ZNK11btMultiBody12getBaseOmegaEv = comdat any

$_ZNK11btMultiBody10getBaseVelEv = comdat any

$_ZN11btMatrix3x3C2ERK12btQuaternion = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EixEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZN20btAlignedObjectArrayI12btQuaternionEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI12btQuaternionE6resizeEiRKS0_ = comdat any

$_ZN12btQuaternionC2Ev = comdat any

$_ZN20btAlignedObjectArrayI12btQuaternionEixEi = comdat any

$_ZmlRK12btQuaternionS1_ = comdat any

$_ZN20btAlignedObjectArrayI12btQuaternionED2Ev = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3EixEi = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZngRK9btVector3 = comdat any

$_ZNK9btVector34normEv = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_ZmiRK11btMatrix3x3S1_ = comdat any

$_Z18vecMulVecTransposeRK9btVector3S1_ = comdat any

$_ZN11btMatrix3x3pLERKS_ = comdat any

$_ZplRK11btMatrix3x3S1_ = comdat any

$_ZN11btMultiBody13applyDeltaVeeEPKff = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZmlRK11btMatrix3x3RKf = comdat any

$_ZNK11btMatrix3x37inverseEv = comdat any

$_ZN11btMatrix3x3ixEi = comdat any

$_ZNK20btAlignedObjectArrayI11btMatrix3x3EixEi = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3EixEi = comdat any

$_Z4fabsf = comdat any

$_ZdvRK9btVector3RKf = comdat any

$_ZN12btQuaternionC2ERK9btVector3RKf = comdat any

$_ZN12btQuaternion9normalizeEv = comdat any

$_ZN10btQuadWordC2ERKfS1_S1_S1_ = comdat any

$_ZmlRK12btQuaternionRK9btVector3 = comdat any

$_ZN12btQuaternionmLERKS_ = comdat any

$_ZNK10btQuadWord4getXEv = comdat any

$_ZNK10btQuadWord4getYEv = comdat any

$_ZNK10btQuadWord4getZEv = comdat any

$_ZNK10btQuadWord1wEv = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK10btQuadWord1yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZNK10btQuadWord1zEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK10btQuadWord1xEv = comdat any

$_ZN10btQuadWord8setValueERKfS1_S1_S1_ = comdat any

$_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv = comdat any

$_ZN11btMatrix3x311setRotationERK12btQuaternion = comdat any

$_ZNK12btQuaternion7length2Ev = comdat any

$_ZNK12btQuaternion3dotERKS_ = comdat any

$_ZN10btQuadWordC2Ev = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_Z6btSqrtf = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZNK11btMatrix3x35cofacEiiii = comdat any

$_ZN12btQuaternion11setRotationERK9btVector3RKf = comdat any

$_Z5btSinf = comdat any

$_Z5btCosf = comdat any

$_ZNK12btQuaternion6lengthEv = comdat any

$_ZN12btQuaterniondVERKf = comdat any

$_ZN12btQuaternionmLERKf = comdat any

$_ZN20btAlignedObjectArrayI15btMultibodyLinkE7reserveEi = comdat any

$_ZN15btMultibodyLinknwEmPv = comdat any

$_ZNK20btAlignedObjectArrayI15btMultibodyLinkE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI15btMultibodyLinkE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI15btMultibodyLinkE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI15btMultibodyLinkE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI15btMultibodyLinkLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI15btMultibodyLinkLj16EE10deallocateEPS0_ = comdat any

$_ZN18btAlignedAllocatorI15btMultibodyLinkLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI15btMultibodyLinkE4initEv = comdat any

$_ZN18btAlignedAllocatorIP23btMultiBodyLinkColliderLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP23btMultiBodyLinkColliderE4initEv = comdat any

$_ZN18btAlignedAllocatorIfLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIfE4initEv = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E4initEv = comdat any

$_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3E4initEv = comdat any

$_ZN20btAlignedObjectArrayI15btMultibodyLinkE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP23btMultiBodyLinkColliderE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP23btMultiBodyLinkColliderE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIP23btMultiBodyLinkColliderE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP23btMultiBodyLinkColliderE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP23btMultiBodyLinkColliderLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIfE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIfE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIfE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIfE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E5clearEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3E5clearEv = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3E7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayI11btMatrix3x3E4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3E10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7reserveEi = comdat any

$_ZN9btVector3nwEmPv = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3E7reserveEi = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZNK20btAlignedObjectArrayI11btMatrix3x3E8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3E8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI11btMatrix3x3E4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAlignedObjectArrayIfE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIfE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIfE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIfE4copyEiiPf = comdat any

$_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf = comdat any

$_ZN18btAlignedAllocatorI12btQuaternionLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI12btQuaternionE4initEv = comdat any

$_ZN20btAlignedObjectArrayI12btQuaternionE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI12btQuaternionE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayI12btQuaternionE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI12btQuaternionE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI12btQuaternionLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI12btQuaternionE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayI12btQuaternionE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI12btQuaternionE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI12btQuaternionE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI12btQuaternionLj16EE8allocateEiPPKS0_ = comdat any

@gDisableDeactivation = external global i8, align 1

@_ZN11btMultiBodyC1EifRK9btVector3bb = hidden unnamed_addr alias %class.btMultiBody* (%class.btMultiBody*, i32, float, %class.btVector3*, i1, i1), %class.btMultiBody* (%class.btMultiBody*, i32, float, %class.btVector3*, i1, i1)* @_ZN11btMultiBodyC2EifRK9btVector3bb
@_ZN11btMultiBodyD1Ev = hidden unnamed_addr alias %class.btMultiBody* (%class.btMultiBody*), %class.btMultiBody* (%class.btMultiBody*)* @_ZN11btMultiBodyD2Ev

define hidden %class.btMultiBody* @_ZN11btMultiBodyC2EifRK9btVector3bb(%class.btMultiBody* returned %this, i32 %n_links, float %mass, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia, i1 zeroext %fixed_base_, i1 zeroext %can_sleep_) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %n_links.addr = alloca i32, align 4
  %mass.addr = alloca float, align 4
  %inertia.addr = alloca %class.btVector3*, align 4
  %fixed_base_.addr = alloca i8, align 1
  %can_sleep_.addr = alloca i8, align 1
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp22 = alloca %struct.btMultibodyLink, align 4
  %ref.tmp25 = alloca %class.btVector3, align 4
  %ref.tmp28 = alloca %class.btMatrix3x3, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp35 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp39 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  %ref.tmp41 = alloca float, align 4
  %ref.tmp43 = alloca float, align 4
  %ref.tmp44 = alloca float, align 4
  %ref.tmp45 = alloca float, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store i32 %n_links, i32* %n_links.addr, align 4, !tbaa !6
  store float %mass, float* %mass.addr, align 4, !tbaa !8
  store %class.btVector3* %inertia, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %frombool = zext i1 %fixed_base_ to i8
  store i8 %frombool, i8* %fixed_base_.addr, align 1, !tbaa !10
  %frombool1 = zext i1 %can_sleep_ to i8
  store i8 %frombool1, i8* %can_sleep_.addr, align 1, !tbaa !10
  %this2 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_baseCollider = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 0
  store %class.btMultiBodyLinkCollider* null, %class.btMultiBodyLinkCollider** %m_baseCollider, align 4, !tbaa !12
  %base_pos = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 1
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %base_pos)
  %base_quat = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !8
  %1 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !8
  %2 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  store float 1.000000e+00, float* %ref.tmp5, align 4, !tbaa !8
  %call6 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %base_quat, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %4 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #7
  %5 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #7
  %6 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #7
  %7 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  %base_mass = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 3
  %8 = load float, float* %mass.addr, align 4, !tbaa !8
  store float %8, float* %base_mass, align 4, !tbaa !27
  %base_inertia = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 4
  %9 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %10 = bitcast %class.btVector3* %base_inertia to i8*
  %11 = bitcast %class.btVector3* %9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !28
  %base_force = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 5
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %base_force)
  %base_torque = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 6
  %call8 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %base_torque)
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 7
  %call9 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEC2Ev(%class.btAlignedObjectArray* %links)
  %m_colliders = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 8
  %call10 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIP23btMultiBodyLinkColliderEC2Ev(%class.btAlignedObjectArray.0* %m_colliders)
  %m_real_buf = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 9
  %call11 = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray.4* %m_real_buf)
  %vector_buf = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 10
  %call12 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.8* %vector_buf)
  %matrix_buf = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 11
  %call13 = call %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayI11btMatrix3x3EC2Ev(%class.btAlignedObjectArray.12* %matrix_buf)
  %cached_inertia_top_left = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 12
  %call14 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %cached_inertia_top_left)
  %cached_inertia_top_right = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 13
  %call15 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %cached_inertia_top_right)
  %cached_inertia_lower_left = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 14
  %call16 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %cached_inertia_lower_left)
  %cached_inertia_lower_right = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 15
  %call17 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %cached_inertia_lower_right)
  %fixed_base = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 16
  %12 = load i8, i8* %fixed_base_.addr, align 1, !tbaa !10, !range !30
  %tobool = trunc i8 %12 to i1
  %frombool18 = zext i1 %tobool to i8
  store i8 %frombool18, i8* %fixed_base, align 4, !tbaa !31
  %awake = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 17
  store i8 1, i8* %awake, align 1, !tbaa !32
  %can_sleep = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 18
  %13 = load i8, i8* %can_sleep_.addr, align 1, !tbaa !10, !range !30
  %tobool19 = trunc i8 %13 to i1
  %frombool20 = zext i1 %tobool19 to i8
  store i8 %frombool20, i8* %can_sleep, align 2, !tbaa !33
  %sleep_timer = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 20
  store float 0.000000e+00, float* %sleep_timer, align 4, !tbaa !34
  %m_linearDamping = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 22
  store float 0x3FA47AE140000000, float* %m_linearDamping, align 4, !tbaa !35
  %m_angularDamping = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 23
  store float 0x3FA47AE140000000, float* %m_angularDamping, align 4, !tbaa !36
  %m_useGyroTerm = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 24
  store i8 1, i8* %m_useGyroTerm, align 4, !tbaa !37
  %m_maxAppliedImpulse = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 26
  store float 1.000000e+03, float* %m_maxAppliedImpulse, align 4, !tbaa !38
  %m_hasSelfCollision = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 27
  store i8 1, i8* %m_hasSelfCollision, align 4, !tbaa !39
  %links21 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 7
  %14 = load i32, i32* %n_links.addr, align 4, !tbaa !6
  %15 = bitcast %struct.btMultibodyLink* %ref.tmp22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 188, i8* %15) #7
  %call23 = call %struct.btMultibodyLink* @_ZN15btMultibodyLinkC2Ev(%struct.btMultibodyLink* %ref.tmp22)
  call void @_ZN20btAlignedObjectArrayI15btMultibodyLinkE6resizeEiRKS0_(%class.btAlignedObjectArray* %links21, i32 %14, %struct.btMultibodyLink* nonnull align 4 dereferenceable(188) %ref.tmp22)
  %16 = bitcast %struct.btMultibodyLink* %ref.tmp22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 188, i8* %16) #7
  %vector_buf24 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 10
  %17 = load i32, i32* %n_links.addr, align 4, !tbaa !6
  %mul = mul nsw i32 2, %17
  %18 = bitcast %class.btVector3* %ref.tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %18) #7
  %call26 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp25)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.8* %vector_buf24, i32 %mul, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp25)
  %19 = bitcast %class.btVector3* %ref.tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %19) #7
  %matrix_buf27 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 11
  %20 = load i32, i32* %n_links.addr, align 4, !tbaa !6
  %add = add nsw i32 %20, 1
  %21 = bitcast %class.btMatrix3x3* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %21) #7
  %call29 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %ref.tmp28)
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E6resizeEiRKS0_(%class.btAlignedObjectArray.12* %matrix_buf27, i32 %add, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp28)
  %22 = bitcast %class.btMatrix3x3* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %22) #7
  %m_real_buf30 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 9
  %23 = load i32, i32* %n_links.addr, align 4, !tbaa !6
  %mul31 = mul nsw i32 2, %23
  %add32 = add nsw i32 6, %mul31
  %24 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #7
  store float 0.000000e+00, float* %ref.tmp33, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.4* %m_real_buf30, i32 %add32, float* nonnull align 4 dereferenceable(4) %ref.tmp33)
  %25 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #7
  %base_pos34 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 1
  %26 = bitcast float* %ref.tmp35 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #7
  store float 0.000000e+00, float* %ref.tmp35, align 4, !tbaa !8
  %27 = bitcast float* %ref.tmp36 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #7
  store float 0.000000e+00, float* %ref.tmp36, align 4, !tbaa !8
  %28 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #7
  store float 0.000000e+00, float* %ref.tmp37, align 4, !tbaa !8
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %base_pos34, float* nonnull align 4 dereferenceable(4) %ref.tmp35, float* nonnull align 4 dereferenceable(4) %ref.tmp36, float* nonnull align 4 dereferenceable(4) %ref.tmp37)
  %29 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #7
  %30 = bitcast float* %ref.tmp36 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #7
  %31 = bitcast float* %ref.tmp35 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #7
  %base_force38 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 5
  %32 = bitcast float* %ref.tmp39 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #7
  store float 0.000000e+00, float* %ref.tmp39, align 4, !tbaa !8
  %33 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #7
  store float 0.000000e+00, float* %ref.tmp40, align 4, !tbaa !8
  %34 = bitcast float* %ref.tmp41 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #7
  store float 0.000000e+00, float* %ref.tmp41, align 4, !tbaa !8
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %base_force38, float* nonnull align 4 dereferenceable(4) %ref.tmp39, float* nonnull align 4 dereferenceable(4) %ref.tmp40, float* nonnull align 4 dereferenceable(4) %ref.tmp41)
  %35 = bitcast float* %ref.tmp41 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #7
  %36 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #7
  %37 = bitcast float* %ref.tmp39 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #7
  %base_torque42 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this2, i32 0, i32 6
  %38 = bitcast float* %ref.tmp43 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #7
  store float 0.000000e+00, float* %ref.tmp43, align 4, !tbaa !8
  %39 = bitcast float* %ref.tmp44 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #7
  store float 0.000000e+00, float* %ref.tmp44, align 4, !tbaa !8
  %40 = bitcast float* %ref.tmp45 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #7
  store float 0.000000e+00, float* %ref.tmp45, align 4, !tbaa !8
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %base_torque42, float* nonnull align 4 dereferenceable(4) %ref.tmp43, float* nonnull align 4 dereferenceable(4) %ref.tmp44, float* nonnull align 4 dereferenceable(4) %ref.tmp45)
  %41 = bitcast float* %ref.tmp45 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #7
  %42 = bitcast float* %ref.tmp44 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #7
  %43 = bitcast float* %ref.tmp43 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #7
  ret %class.btMultiBody* %this2
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  store float* %_w, float** %_w.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = load float*, float** %_x.addr, align 4, !tbaa !2
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float*, float** %_z.addr, align 4, !tbaa !2
  %4 = load float*, float** %_w.addr, align 4, !tbaa !2
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4)
  ret %class.btQuaternion* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI15btMultibodyLinkLj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI15btMultibodyLinkE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIP23btMultiBodyLinkColliderEC2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorIP23btMultiBodyLinkColliderLj16EEC2Ev(%class.btAlignedAllocator.1* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP23btMultiBodyLinkColliderE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray.4* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorIfLj16EEC2Ev(%class.btAlignedAllocator.5* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.4* %this1)
  ret %class.btAlignedObjectArray.4* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.8* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.9* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator.9* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray.8* %this1)
  ret %class.btAlignedObjectArray.8* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayI11btMatrix3x3EC2Ev(%class.btAlignedObjectArray.12* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.13* @_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EEC2Ev(%class.btAlignedAllocator.13* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E4initEv(%class.btAlignedObjectArray.12* %this1)
  ret %class.btAlignedObjectArray.12* %this1
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI15btMultibodyLinkE6resizeEiRKS0_(%class.btAlignedObjectArray* %this, i32 %newsize, %struct.btMultibodyLink* nonnull align 4 dereferenceable(188) %fillData) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %struct.btMultibodyLink*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !6
  store %struct.btMultibodyLink* %fillData, %struct.btMultibodyLink** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !6
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %2 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  store i32 %4, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %struct.btMultibodyLink*, %struct.btMultibodyLink** %m_data, align 4, !tbaa !40
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end17

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv(%class.btAlignedObjectArray* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayI15btMultibodyLinkE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = load i32, i32* %curSize, align 4, !tbaa !6
  store i32 %14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc14, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !6
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  br label %for.end16

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %18 = load %struct.btMultibodyLink*, %struct.btMultibodyLink** %m_data11, align 4, !tbaa !40
  %19 = load i32, i32* %i6, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %18, i32 %19
  %20 = bitcast %struct.btMultibodyLink* %arrayidx12 to i8*
  %call13 = call i8* @_ZN15btMultibodyLinknwEmPv(i32 188, i8* %20)
  %21 = bitcast i8* %call13 to %struct.btMultibodyLink*
  %22 = load %struct.btMultibodyLink*, %struct.btMultibodyLink** %fillData.addr, align 4, !tbaa !2
  %23 = bitcast %struct.btMultibodyLink* %21 to i8*
  %24 = bitcast %struct.btMultibodyLink* %22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 188, i1 false)
  br label %for.inc14

for.inc14:                                        ; preds = %for.body10
  %25 = load i32, i32* %i6, align 4, !tbaa !6
  %inc15 = add nsw i32 %25, 1
  store i32 %inc15, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.end16:                                        ; preds = %for.cond.cleanup9
  br label %if.end17

if.end17:                                         ; preds = %for.end16, %for.end
  %26 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %26, i32* %m_size, align 4, !tbaa !41
  %27 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #7
  ret void
}

define linkonce_odr hidden %struct.btMultibodyLink* @_ZN15btMultibodyLinkC2Ev(%struct.btMultibodyLink* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %struct.btMultibodyLink*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp24 = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp35 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp39 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  %ref.tmp41 = alloca float, align 4
  %ref.tmp43 = alloca float, align 4
  %ref.tmp44 = alloca float, align 4
  %ref.tmp45 = alloca float, align 4
  %ref.tmp47 = alloca float, align 4
  %ref.tmp48 = alloca float, align 4
  %ref.tmp49 = alloca float, align 4
  store %struct.btMultibodyLink* %this, %struct.btMultibodyLink** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btMultibodyLink*, %struct.btMultibodyLink** %this.addr, align 4
  %joint_pos = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 0
  store float 0.000000e+00, float* %joint_pos, align 4, !tbaa !42
  %mass = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 1
  store float 1.000000e+00, float* %mass, align 4, !tbaa !44
  %inertia = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 2
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %inertia)
  %parent = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 3
  store i32 -1, i32* %parent, align 4, !tbaa !45
  %zero_rot_parent_to_this = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !8
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !8
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !8
  %call5 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %zero_rot_parent_to_this, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #7
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #7
  %6 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #7
  %7 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  %axis_top = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 5
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %axis_top)
  %axis_bottom = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 6
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %axis_bottom)
  %d_vector = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 7
  %call8 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %d_vector)
  %e_vector = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 8
  %call9 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %e_vector)
  %is_revolute = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 9
  store i8 0, i8* %is_revolute, align 4, !tbaa !46
  %cached_rot_parent_to_this = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 10
  %8 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  store float 1.000000e+00, float* %ref.tmp10, align 4, !tbaa !8
  %9 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  store float 0.000000e+00, float* %ref.tmp11, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  store float 0.000000e+00, float* %ref.tmp12, align 4, !tbaa !8
  %11 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #7
  store float 0.000000e+00, float* %ref.tmp13, align 4, !tbaa !8
  %call14 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %cached_rot_parent_to_this, float* nonnull align 4 dereferenceable(4) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp13)
  %12 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #7
  %13 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #7
  %14 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #7
  %15 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #7
  %cached_r_vector = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 11
  %call15 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %cached_r_vector)
  %applied_force = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 12
  %call16 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %applied_force)
  %applied_torque = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 13
  %call17 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %applied_torque)
  %joint_torque = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 14
  store float 0.000000e+00, float* %joint_torque, align 4, !tbaa !47
  %m_collider = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 15
  store %class.btMultiBodyLinkCollider* null, %class.btMultiBodyLinkCollider** %m_collider, align 4, !tbaa !48
  %m_flags = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 16
  store i32 0, i32* %m_flags, align 4, !tbaa !49
  %inertia18 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 2
  %16 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #7
  store float 1.000000e+00, float* %ref.tmp19, align 4, !tbaa !8
  %17 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #7
  store float 1.000000e+00, float* %ref.tmp20, align 4, !tbaa !8
  %18 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #7
  store float 1.000000e+00, float* %ref.tmp21, align 4, !tbaa !8
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %inertia18, float* nonnull align 4 dereferenceable(4) %ref.tmp19, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %19 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #7
  %20 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #7
  %21 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #7
  %axis_top22 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 5
  %22 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #7
  store float 0.000000e+00, float* %ref.tmp23, align 4, !tbaa !8
  %23 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #7
  store float 0.000000e+00, float* %ref.tmp24, align 4, !tbaa !8
  %24 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #7
  store float 0.000000e+00, float* %ref.tmp25, align 4, !tbaa !8
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %axis_top22, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp24, float* nonnull align 4 dereferenceable(4) %ref.tmp25)
  %25 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #7
  %26 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #7
  %27 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #7
  %axis_bottom26 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 6
  %28 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #7
  store float 1.000000e+00, float* %ref.tmp27, align 4, !tbaa !8
  %29 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #7
  store float 0.000000e+00, float* %ref.tmp28, align 4, !tbaa !8
  %30 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #7
  store float 0.000000e+00, float* %ref.tmp29, align 4, !tbaa !8
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %axis_bottom26, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp29)
  %31 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #7
  %32 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #7
  %33 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #7
  %d_vector30 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 7
  %34 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #7
  store float 0.000000e+00, float* %ref.tmp31, align 4, !tbaa !8
  %35 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #7
  store float 0.000000e+00, float* %ref.tmp32, align 4, !tbaa !8
  %36 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #7
  store float 0.000000e+00, float* %ref.tmp33, align 4, !tbaa !8
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %d_vector30, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp32, float* nonnull align 4 dereferenceable(4) %ref.tmp33)
  %37 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #7
  %38 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #7
  %39 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #7
  %e_vector34 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 8
  %40 = bitcast float* %ref.tmp35 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #7
  store float 0.000000e+00, float* %ref.tmp35, align 4, !tbaa !8
  %41 = bitcast float* %ref.tmp36 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #7
  store float 0.000000e+00, float* %ref.tmp36, align 4, !tbaa !8
  %42 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #7
  store float 0.000000e+00, float* %ref.tmp37, align 4, !tbaa !8
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %e_vector34, float* nonnull align 4 dereferenceable(4) %ref.tmp35, float* nonnull align 4 dereferenceable(4) %ref.tmp36, float* nonnull align 4 dereferenceable(4) %ref.tmp37)
  %43 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #7
  %44 = bitcast float* %ref.tmp36 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #7
  %45 = bitcast float* %ref.tmp35 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #7
  %cached_r_vector38 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 11
  %46 = bitcast float* %ref.tmp39 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #7
  store float 0.000000e+00, float* %ref.tmp39, align 4, !tbaa !8
  %47 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #7
  store float 0.000000e+00, float* %ref.tmp40, align 4, !tbaa !8
  %48 = bitcast float* %ref.tmp41 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #7
  store float 0.000000e+00, float* %ref.tmp41, align 4, !tbaa !8
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %cached_r_vector38, float* nonnull align 4 dereferenceable(4) %ref.tmp39, float* nonnull align 4 dereferenceable(4) %ref.tmp40, float* nonnull align 4 dereferenceable(4) %ref.tmp41)
  %49 = bitcast float* %ref.tmp41 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #7
  %50 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #7
  %51 = bitcast float* %ref.tmp39 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #7
  %applied_force42 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 12
  %52 = bitcast float* %ref.tmp43 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #7
  store float 0.000000e+00, float* %ref.tmp43, align 4, !tbaa !8
  %53 = bitcast float* %ref.tmp44 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #7
  store float 0.000000e+00, float* %ref.tmp44, align 4, !tbaa !8
  %54 = bitcast float* %ref.tmp45 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #7
  store float 0.000000e+00, float* %ref.tmp45, align 4, !tbaa !8
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %applied_force42, float* nonnull align 4 dereferenceable(4) %ref.tmp43, float* nonnull align 4 dereferenceable(4) %ref.tmp44, float* nonnull align 4 dereferenceable(4) %ref.tmp45)
  %55 = bitcast float* %ref.tmp45 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #7
  %56 = bitcast float* %ref.tmp44 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #7
  %57 = bitcast float* %ref.tmp43 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #7
  %applied_torque46 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 13
  %58 = bitcast float* %ref.tmp47 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #7
  store float 0.000000e+00, float* %ref.tmp47, align 4, !tbaa !8
  %59 = bitcast float* %ref.tmp48 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #7
  store float 0.000000e+00, float* %ref.tmp48, align 4, !tbaa !8
  %60 = bitcast float* %ref.tmp49 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #7
  store float 0.000000e+00, float* %ref.tmp49, align 4, !tbaa !8
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %applied_torque46, float* nonnull align 4 dereferenceable(4) %ref.tmp47, float* nonnull align 4 dereferenceable(4) %ref.tmp48, float* nonnull align 4 dereferenceable(4) %ref.tmp49)
  %61 = bitcast float* %ref.tmp49 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #7
  %62 = bitcast float* %ref.tmp48 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #7
  %63 = bitcast float* %ref.tmp47 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #7
  ret %struct.btMultibodyLink* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.8* %this, i32 %newsize, %class.btVector3* nonnull align 4 dereferenceable(16) %fillData) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btVector3*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !6
  store %class.btVector3* %fillData, %class.btVector3** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !6
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %2 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  store i32 %4, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %8 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !50
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end17

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray.8* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = load i32, i32* %curSize, align 4, !tbaa !6
  store i32 %14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc14, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !6
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  br label %for.end16

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %18 = load %class.btVector3*, %class.btVector3** %m_data11, align 4, !tbaa !50
  %19 = load i32, i32* %i6, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds %class.btVector3, %class.btVector3* %18, i32 %19
  %20 = bitcast %class.btVector3* %arrayidx12 to i8*
  %call13 = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %20)
  %21 = bitcast i8* %call13 to %class.btVector3*
  %22 = load %class.btVector3*, %class.btVector3** %fillData.addr, align 4, !tbaa !2
  %23 = bitcast %class.btVector3* %21 to i8*
  %24 = bitcast %class.btVector3* %22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false), !tbaa.struct !28
  br label %for.inc14

for.inc14:                                        ; preds = %for.body10
  %25 = load i32, i32* %i6, align 4, !tbaa !6
  %inc15 = add nsw i32 %25, 1
  store i32 %inc15, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.end16:                                        ; preds = %for.cond.cleanup9
  br label %if.end17

if.end17:                                         ; preds = %for.end16, %for.end
  %26 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  store i32 %26, i32* %m_size, align 4, !tbaa !51
  %27 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btMatrix3x3E6resizeEiRKS0_(%class.btAlignedObjectArray.12* %this, i32 %newsize, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %fillData) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btMatrix3x3*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !6
  store %class.btMatrix3x3* %fillData, %class.btMatrix3x3** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK20btAlignedObjectArrayI11btMatrix3x3E4sizeEv(%class.btAlignedObjectArray.12* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !6
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %2 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  store i32 %4, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m_data, align 4, !tbaa !52
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end17

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI11btMatrix3x3E4sizeEv(%class.btAlignedObjectArray.12* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E7reserveEi(%class.btAlignedObjectArray.12* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = load i32, i32* %curSize, align 4, !tbaa !6
  store i32 %14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc14, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !6
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  br label %for.end16

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m_data11, align 4, !tbaa !52
  %19 = load i32, i32* %i6, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %18, i32 %19
  %20 = bitcast %class.btMatrix3x3* %arrayidx12 to i8*
  %21 = bitcast i8* %20 to %class.btMatrix3x3*
  %22 = load %class.btMatrix3x3*, %class.btMatrix3x3** %fillData.addr, align 4, !tbaa !2
  %call13 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %21, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %22)
  br label %for.inc14

for.inc14:                                        ; preds = %for.body10
  %23 = load i32, i32* %i6, align 4, !tbaa !6
  %inc15 = add nsw i32 %23, 1
  store i32 %inc15, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.end16:                                        ; preds = %for.cond.cleanup9
  br label %if.end17

if.end17:                                         ; preds = %for.end16, %for.end
  %24 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  store i32 %24, i32* %m_size, align 4, !tbaa !53
  %25 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.4* %this, i32 %newsize, float* nonnull align 4 dereferenceable(4) %fillData) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca float*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !6
  store float* %fillData, float** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !6
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %2 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  store i32 %4, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %8 = load float*, float** %m_data, align 4, !tbaa !54
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray.4* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = load i32, i32* %curSize, align 4, !tbaa !6
  store i32 %14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !6
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %18 = load float*, float** %m_data11, align 4, !tbaa !54
  %19 = load i32, i32* %i6, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds float, float* %18, i32 %19
  %20 = bitcast float* %arrayidx12 to i8*
  %21 = bitcast i8* %20 to float*
  %22 = load float*, float** %fillData.addr, align 4, !tbaa !2
  %23 = load float, float* %22, align 4, !tbaa !8
  store float %23, float* %21, align 4, !tbaa !8
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %24 = load i32, i32* %i6, align 4, !tbaa !6
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %25 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 %25, i32* %m_size, align 4, !tbaa !55
  %26 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !8
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !8
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !8
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !8
  ret void
}

; Function Attrs: nounwind
define hidden %class.btMultiBody* @_ZN11btMultiBodyD2Ev(%class.btMultiBody* returned %this) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %matrix_buf = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 11
  %call = call %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayI11btMatrix3x3ED2Ev(%class.btAlignedObjectArray.12* %matrix_buf) #7
  %vector_buf = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 10
  %call2 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.8* %vector_buf) #7
  %m_real_buf = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 9
  %call3 = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.4* %m_real_buf) #7
  %m_colliders = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 8
  %call4 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIP23btMultiBodyLinkColliderED2Ev(%class.btAlignedObjectArray.0* %m_colliders) #7
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %call5 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI15btMultibodyLinkED2Ev(%class.btAlignedObjectArray* %links) #7
  ret %class.btMultiBody* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayI11btMatrix3x3ED2Ev(%class.btAlignedObjectArray.12* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E5clearEv(%class.btAlignedObjectArray.12* %this1)
  ret %class.btAlignedObjectArray.12* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.8* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray.8* %this1)
  ret %class.btAlignedObjectArray.8* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.4* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIfE5clearEv(%class.btAlignedObjectArray.4* %this1)
  ret %class.btAlignedObjectArray.4* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIP23btMultiBodyLinkColliderED2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP23btMultiBodyLinkColliderE5clearEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI15btMultibodyLinkED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI15btMultibodyLinkE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

define hidden void @_ZN11btMultiBody14setupPrismaticEifRK9btVector3iRK12btQuaternionS2_S2_b(%class.btMultiBody* %this, i32 %i, float %mass, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia, i32 %parent, %class.btQuaternion* nonnull align 4 dereferenceable(16) %rot_parent_to_this, %class.btVector3* nonnull align 4 dereferenceable(16) %joint_axis, %class.btVector3* nonnull align 4 dereferenceable(16) %r_vector_when_q_zero, i1 zeroext %disableParentCollision) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %i.addr = alloca i32, align 4
  %mass.addr = alloca float, align 4
  %inertia.addr = alloca %class.btVector3*, align 4
  %parent.addr = alloca i32, align 4
  %rot_parent_to_this.addr = alloca %class.btQuaternion*, align 4
  %joint_axis.addr = alloca %class.btVector3*, align 4
  %r_vector_when_q_zero.addr = alloca %class.btVector3*, align 4
  %disableParentCollision.addr = alloca i8, align 1
  %ref.tmp = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  store float %mass, float* %mass.addr, align 4, !tbaa !8
  store %class.btVector3* %inertia, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  store i32 %parent, i32* %parent.addr, align 4, !tbaa !6
  store %class.btQuaternion* %rot_parent_to_this, %class.btQuaternion** %rot_parent_to_this.addr, align 4, !tbaa !2
  store %class.btVector3* %joint_axis, %class.btVector3** %joint_axis.addr, align 4, !tbaa !2
  store %class.btVector3* %r_vector_when_q_zero, %class.btVector3** %r_vector_when_q_zero.addr, align 4, !tbaa !2
  %frombool = zext i1 %disableParentCollision to i8
  store i8 %frombool, i8* %disableParentCollision.addr, align 1, !tbaa !10
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = load float, float* %mass.addr, align 4, !tbaa !8
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %1 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links, i32 %1)
  %mass2 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call, i32 0, i32 1
  store float %0, float* %mass2, align 4, !tbaa !44
  %2 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %links3 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %3 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call4 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links3, i32 %3)
  %inertia5 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call4, i32 0, i32 2
  %4 = bitcast %class.btVector3* %inertia5 to i8*
  %5 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !28
  %6 = load i32, i32* %parent.addr, align 4, !tbaa !6
  %links6 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %7 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call7 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links6, i32 %7)
  %parent8 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call7, i32 0, i32 3
  store i32 %6, i32* %parent8, align 4, !tbaa !45
  %8 = load %class.btQuaternion*, %class.btQuaternion** %rot_parent_to_this.addr, align 4, !tbaa !2
  %links9 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %9 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call10 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links9, i32 %9)
  %zero_rot_parent_to_this = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call10, i32 0, i32 4
  %10 = bitcast %class.btQuaternion* %zero_rot_parent_to_this to i8*
  %11 = bitcast %class.btQuaternion* %8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  %links11 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %12 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call12 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links11, i32 %12)
  %axis_top = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call12, i32 0, i32 5
  %13 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !8
  %14 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #7
  store float 0.000000e+00, float* %ref.tmp13, align 4, !tbaa !8
  %15 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #7
  store float 0.000000e+00, float* %ref.tmp14, align 4, !tbaa !8
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %axis_top, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp14)
  %16 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #7
  %17 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  %18 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #7
  %19 = load %class.btVector3*, %class.btVector3** %joint_axis.addr, align 4, !tbaa !2
  %links15 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %20 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call16 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links15, i32 %20)
  %axis_bottom = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call16, i32 0, i32 6
  %21 = bitcast %class.btVector3* %axis_bottom to i8*
  %22 = bitcast %class.btVector3* %19 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false), !tbaa.struct !28
  %23 = load %class.btVector3*, %class.btVector3** %r_vector_when_q_zero.addr, align 4, !tbaa !2
  %links17 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %24 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call18 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links17, i32 %24)
  %e_vector = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call18, i32 0, i32 8
  %25 = bitcast %class.btVector3* %e_vector to i8*
  %26 = bitcast %class.btVector3* %23 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %25, i8* align 4 %26, i32 16, i1 false), !tbaa.struct !28
  %links19 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %27 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call20 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links19, i32 %27)
  %is_revolute = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call20, i32 0, i32 9
  store i8 0, i8* %is_revolute, align 4, !tbaa !46
  %28 = load %class.btQuaternion*, %class.btQuaternion** %rot_parent_to_this.addr, align 4, !tbaa !2
  %links21 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %29 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call22 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links21, i32 %29)
  %cached_rot_parent_to_this = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call22, i32 0, i32 10
  %30 = bitcast %class.btQuaternion* %cached_rot_parent_to_this to i8*
  %31 = bitcast %class.btQuaternion* %28 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %30, i8* align 4 %31, i32 16, i1 false)
  %32 = load i8, i8* %disableParentCollision.addr, align 1, !tbaa !10, !range !30
  %tobool = trunc i8 %32 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %links23 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %33 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call24 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links23, i32 %33)
  %m_flags = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call24, i32 0, i32 16
  %34 = load i32, i32* %m_flags, align 4, !tbaa !49
  %or = or i32 %34, 1
  store i32 %or, i32* %m_flags, align 4, !tbaa !49
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %links25 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %35 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call26 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links25, i32 %35)
  call void @_ZN15btMultibodyLink11updateCacheEv(%struct.btMultibodyLink* %call26)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btMultibodyLink*, %struct.btMultibodyLink** %m_data, align 4, !tbaa !40
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %0, i32 %1
  ret %struct.btMultibodyLink* %arrayidx
}

define linkonce_odr hidden void @_ZN15btMultibodyLink11updateCacheEv(%struct.btMultibodyLink* %this) #0 comdat {
entry:
  %this.addr = alloca %struct.btMultibodyLink*, align 4
  %ref.tmp = alloca %class.btQuaternion, align 4
  %ref.tmp2 = alloca %class.btQuaternion, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  store %struct.btMultibodyLink* %this, %struct.btMultibodyLink** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btMultibodyLink*, %struct.btMultibodyLink** %this.addr, align 4
  %is_revolute = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 9
  %0 = load i8, i8* %is_revolute, align 4, !tbaa !46, !range !30
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #7
  %2 = bitcast %class.btQuaternion* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #7
  %axis_top = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 5
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %joint_pos = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 0
  %4 = load float, float* %joint_pos, align 4, !tbaa !42
  %fneg = fneg float %4
  store float %fneg, float* %ref.tmp3, align 4, !tbaa !8
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %axis_top, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %zero_rot_parent_to_this = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 4
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %ref.tmp, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp2, %class.btQuaternion* nonnull align 4 dereferenceable(16) %zero_rot_parent_to_this)
  %cached_rot_parent_to_this = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 10
  %5 = bitcast %class.btQuaternion* %cached_rot_parent_to_this to i8*
  %6 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false)
  %7 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  %8 = bitcast %class.btQuaternion* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #7
  %9 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #7
  %10 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #7
  %d_vector = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 7
  %11 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #7
  %cached_rot_parent_to_this6 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 10
  %e_vector = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 8
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %ref.tmp5, %class.btQuaternion* nonnull align 4 dereferenceable(16) %cached_rot_parent_to_this6, %class.btVector3* nonnull align 4 dereferenceable(16) %e_vector)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %d_vector, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %cached_r_vector = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 11
  %12 = bitcast %class.btVector3* %cached_r_vector to i8*
  %13 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false), !tbaa.struct !28
  %14 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %14) #7
  %15 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %15) #7
  br label %if.end

if.else:                                          ; preds = %entry
  %16 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #7
  %e_vector8 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 8
  %17 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %17) #7
  %joint_pos10 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 0
  %axis_bottom = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 6
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp9, float* nonnull align 4 dereferenceable(4) %joint_pos10, %class.btVector3* nonnull align 4 dereferenceable(16) %axis_bottom)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp7, %class.btVector3* nonnull align 4 dereferenceable(16) %e_vector8, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9)
  %cached_r_vector11 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 11
  %18 = bitcast %class.btVector3* %cached_r_vector11 to i8*
  %19 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %18, i8* align 4 %19, i32 16, i1 false), !tbaa.struct !28
  %20 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %20) #7
  %21 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %21) #7
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

define hidden void @_ZN11btMultiBody13setupRevoluteEifRK9btVector3iRK12btQuaternionS2_S2_S2_b(%class.btMultiBody* %this, i32 %i, float %mass, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia, i32 %parent, %class.btQuaternion* nonnull align 4 dereferenceable(16) %zero_rot_parent_to_this, %class.btVector3* nonnull align 4 dereferenceable(16) %joint_axis, %class.btVector3* nonnull align 4 dereferenceable(16) %parent_axis_position, %class.btVector3* nonnull align 4 dereferenceable(16) %my_axis_position, i1 zeroext %disableParentCollision) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %i.addr = alloca i32, align 4
  %mass.addr = alloca float, align 4
  %inertia.addr = alloca %class.btVector3*, align 4
  %parent.addr = alloca i32, align 4
  %zero_rot_parent_to_this.addr = alloca %class.btQuaternion*, align 4
  %joint_axis.addr = alloca %class.btVector3*, align 4
  %parent_axis_position.addr = alloca %class.btVector3*, align 4
  %my_axis_position.addr = alloca %class.btVector3*, align 4
  %disableParentCollision.addr = alloca i8, align 1
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  store float %mass, float* %mass.addr, align 4, !tbaa !8
  store %class.btVector3* %inertia, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  store i32 %parent, i32* %parent.addr, align 4, !tbaa !6
  store %class.btQuaternion* %zero_rot_parent_to_this, %class.btQuaternion** %zero_rot_parent_to_this.addr, align 4, !tbaa !2
  store %class.btVector3* %joint_axis, %class.btVector3** %joint_axis.addr, align 4, !tbaa !2
  store %class.btVector3* %parent_axis_position, %class.btVector3** %parent_axis_position.addr, align 4, !tbaa !2
  store %class.btVector3* %my_axis_position, %class.btVector3** %my_axis_position.addr, align 4, !tbaa !2
  %frombool = zext i1 %disableParentCollision to i8
  store i8 %frombool, i8* %disableParentCollision.addr, align 1, !tbaa !10
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = load float, float* %mass.addr, align 4, !tbaa !8
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %1 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links, i32 %1)
  %mass2 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call, i32 0, i32 1
  store float %0, float* %mass2, align 4, !tbaa !44
  %2 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %links3 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %3 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call4 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links3, i32 %3)
  %inertia5 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call4, i32 0, i32 2
  %4 = bitcast %class.btVector3* %inertia5 to i8*
  %5 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !28
  %6 = load i32, i32* %parent.addr, align 4, !tbaa !6
  %links6 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %7 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call7 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links6, i32 %7)
  %parent8 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call7, i32 0, i32 3
  store i32 %6, i32* %parent8, align 4, !tbaa !45
  %8 = load %class.btQuaternion*, %class.btQuaternion** %zero_rot_parent_to_this.addr, align 4, !tbaa !2
  %links9 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %9 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call10 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links9, i32 %9)
  %zero_rot_parent_to_this11 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call10, i32 0, i32 4
  %10 = bitcast %class.btQuaternion* %zero_rot_parent_to_this11 to i8*
  %11 = bitcast %class.btQuaternion* %8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  %12 = load %class.btVector3*, %class.btVector3** %joint_axis.addr, align 4, !tbaa !2
  %links12 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %13 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call13 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links12, i32 %13)
  %axis_top = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call13, i32 0, i32 5
  %14 = bitcast %class.btVector3* %axis_top to i8*
  %15 = bitcast %class.btVector3* %12 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %15, i32 16, i1 false), !tbaa.struct !28
  %16 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #7
  %17 = load %class.btVector3*, %class.btVector3** %joint_axis.addr, align 4, !tbaa !2
  %18 = load %class.btVector3*, %class.btVector3** %my_axis_position.addr, align 4, !tbaa !2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %18)
  %links14 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %19 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call15 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links14, i32 %19)
  %axis_bottom = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call15, i32 0, i32 6
  %20 = bitcast %class.btVector3* %axis_bottom to i8*
  %21 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %20, i8* align 4 %21, i32 16, i1 false), !tbaa.struct !28
  %22 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %22) #7
  %23 = load %class.btVector3*, %class.btVector3** %my_axis_position.addr, align 4, !tbaa !2
  %links16 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %24 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call17 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links16, i32 %24)
  %d_vector = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call17, i32 0, i32 7
  %25 = bitcast %class.btVector3* %d_vector to i8*
  %26 = bitcast %class.btVector3* %23 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %25, i8* align 4 %26, i32 16, i1 false), !tbaa.struct !28
  %27 = load %class.btVector3*, %class.btVector3** %parent_axis_position.addr, align 4, !tbaa !2
  %links18 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %28 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call19 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links18, i32 %28)
  %e_vector = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call19, i32 0, i32 8
  %29 = bitcast %class.btVector3* %e_vector to i8*
  %30 = bitcast %class.btVector3* %27 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %29, i8* align 4 %30, i32 16, i1 false), !tbaa.struct !28
  %links20 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %31 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call21 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links20, i32 %31)
  %is_revolute = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call21, i32 0, i32 9
  store i8 1, i8* %is_revolute, align 4, !tbaa !46
  %32 = load i8, i8* %disableParentCollision.addr, align 1, !tbaa !10, !range !30
  %tobool = trunc i8 %32 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %links22 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %33 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call23 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links22, i32 %33)
  %m_flags = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call23, i32 0, i32 16
  %34 = load i32, i32* %m_flags, align 4, !tbaa !49
  %or = or i32 %34, 1
  store i32 %or, i32* %m_flags, align 4, !tbaa !49
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %links24 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %35 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call25 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links24, i32 %35)
  call void @_ZN15btMultibodyLink11updateCacheEv(%struct.btMultibodyLink* %call25)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %1 = load float, float* %arrayidx, align 4, !tbaa !8
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %3 = load float, float* %arrayidx3, align 4, !tbaa !8
  %mul = fmul float %1, %3
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %4 = load float, float* %arrayidx5, align 4, !tbaa !8
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %6 = load float, float* %arrayidx7, align 4, !tbaa !8
  %mul8 = fmul float %4, %6
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4, !tbaa !8
  %7 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %8 = load float, float* %arrayidx11, align 4, !tbaa !8
  %9 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %10 = load float, float* %arrayidx13, align 4, !tbaa !8
  %mul14 = fmul float %8, %10
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %11 = load float, float* %arrayidx16, align 4, !tbaa !8
  %12 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %13 = load float, float* %arrayidx18, align 4, !tbaa !8
  %mul19 = fmul float %11, %13
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4, !tbaa !8
  %14 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #7
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %15 = load float, float* %arrayidx23, align 4, !tbaa !8
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %17 = load float, float* %arrayidx25, align 4, !tbaa !8
  %mul26 = fmul float %15, %17
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %18 = load float, float* %arrayidx28, align 4, !tbaa !8
  %19 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %19, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %20 = load float, float* %arrayidx30, align 4, !tbaa !8
  %mul31 = fmul float %18, %20
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %21 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #7
  %22 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #7
  %23 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #7
  ret void
}

define hidden i32 @_ZNK11btMultiBody9getParentEi(%class.btMultiBody* %this, i32 %i) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %0 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links, i32 %0)
  %parent = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call, i32 0, i32 3
  %1 = load i32, i32* %parent, align 4, !tbaa !45
  ret i32 %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btMultibodyLink*, %struct.btMultibodyLink** %m_data, align 4, !tbaa !40
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %0, i32 %1
  ret %struct.btMultibodyLink* %arrayidx
}

define hidden float @_ZNK11btMultiBody11getLinkMassEi(%class.btMultiBody* %this, i32 %i) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %0 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links, i32 %0)
  %mass = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call, i32 0, i32 1
  %1 = load float, float* %mass, align 4, !tbaa !44
  ret float %1
}

define hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMultiBody14getLinkInertiaEi(%class.btMultiBody* %this, i32 %i) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %0 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links, i32 %0)
  %inertia = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call, i32 0, i32 2
  ret %class.btVector3* %inertia
}

define hidden float @_ZNK11btMultiBody11getJointPosEi(%class.btMultiBody* %this, i32 %i) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %0 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links, i32 %0)
  %joint_pos = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call, i32 0, i32 0
  %1 = load float, float* %joint_pos, align 4, !tbaa !42
  ret float %1
}

define hidden float @_ZNK11btMultiBody11getJointVelEi(%class.btMultiBody* %this, i32 %i) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_real_buf = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 9
  %0 = load i32, i32* %i.addr, align 4, !tbaa !6
  %add = add nsw i32 6, %0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_real_buf, i32 %add)
  %1 = load float, float* %call, align 4, !tbaa !8
  ret float %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4, !tbaa !54
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

define hidden void @_ZN11btMultiBody11setJointPosEif(%class.btMultiBody* %this, i32 %i, float %q) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %i.addr = alloca i32, align 4
  %q.addr = alloca float, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  store float %q, float* %q.addr, align 4, !tbaa !8
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = load float, float* %q.addr, align 4, !tbaa !8
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %1 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links, i32 %1)
  %joint_pos = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call, i32 0, i32 0
  store float %0, float* %joint_pos, align 4, !tbaa !42
  %links2 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %2 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call3 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links2, i32 %2)
  call void @_ZN15btMultibodyLink11updateCacheEv(%struct.btMultibodyLink* %call3)
  ret void
}

define hidden void @_ZN11btMultiBody11setJointVelEif(%class.btMultiBody* %this, i32 %i, float %qdot) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %i.addr = alloca i32, align 4
  %qdot.addr = alloca float, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  store float %qdot, float* %qdot.addr, align 4, !tbaa !8
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = load float, float* %qdot.addr, align 4, !tbaa !8
  %m_real_buf = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 9
  %1 = load i32, i32* %i.addr, align 4, !tbaa !6
  %add = add nsw i32 6, %1
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_real_buf, i32 %add)
  store float %0, float* %call, align 4, !tbaa !8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4, !tbaa !54
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

define hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMultiBody10getRVectorEi(%class.btMultiBody* %this, i32 %i) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %0 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links, i32 %0)
  %cached_r_vector = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call, i32 0, i32 11
  ret %class.btVector3* %cached_r_vector
}

define hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZNK11btMultiBody19getParentToLocalRotEi(%class.btMultiBody* %this, i32 %i) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %0 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links, i32 %0)
  %cached_rot_parent_to_this = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call, i32 0, i32 10
  ret %class.btQuaternion* %cached_rot_parent_to_this
}

define hidden void @_ZNK11btMultiBody15localPosToWorldEiRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMultiBody* %this, i32 %i, %class.btVector3* nonnull align 4 dereferenceable(16) %local_pos) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %i.addr = alloca i32, align 4
  %local_pos.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btQuaternion, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca %class.btQuaternion, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  store %class.btVector3* %local_pos, %class.btVector3** %local_pos.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %local_pos.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVector3* %agg.result to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !28
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %3 = load i32, i32* %i.addr, align 4, !tbaa !6
  %cmp = icmp ne i32 %3, -1
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %4 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMultiBody10getRVectorEi(%class.btMultiBody* %this1, i32 %4)
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #7
  %6 = bitcast %class.btQuaternion* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #7
  %7 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call4 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZNK11btMultiBody19getParentToLocalRotEi(%class.btMultiBody* %this1, i32 %7)
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp3, %class.btQuaternion* %call4)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %agg.result)
  %8 = bitcast %class.btVector3* %agg.result to i8*
  %9 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false), !tbaa.struct !28
  %10 = bitcast %class.btQuaternion* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %10) #7
  %11 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %11) #7
  %12 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call5 = call i32 @_ZNK11btMultiBody9getParentEi(%class.btMultiBody* %this1, i32 %12)
  store i32 %call5, i32* %i.addr, align 4, !tbaa !6
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %13 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #7
  %14 = bitcast %class.btQuaternion* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %14) #7
  %call8 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZNK11btMultiBody17getWorldToBaseRotEv(%class.btMultiBody* %this1)
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp7, %class.btQuaternion* %call8)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %ref.tmp6, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp7, %class.btVector3* nonnull align 4 dereferenceable(16) %agg.result)
  %15 = bitcast %class.btVector3* %agg.result to i8*
  %16 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 16, i1 false), !tbaa.struct !28
  %17 = bitcast %class.btQuaternion* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %17) #7
  %18 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #7
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMultiBody10getBasePosEv(%class.btMultiBody* %this1)
  %call10 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !8
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !8
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !8
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !8
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !8
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !8
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %rotation, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %rotation.addr = alloca %class.btQuaternion*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %q = alloca %class.btQuaternion, align 4
  %ref.tmp = alloca %class.btQuaternion, align 4
  store %class.btQuaternion* %rotation, %class.btQuaternion** %rotation.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast %class.btQuaternion* %q to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #7
  %1 = load %class.btQuaternion*, %class.btQuaternion** %rotation.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  call void @_ZmlRK12btQuaternionRK9btVector3(%class.btQuaternion* sret align 4 %q, %class.btQuaternion* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  %3 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #7
  %4 = load %class.btQuaternion*, %class.btQuaternion** %rotation.addr, align 4, !tbaa !2
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp, %class.btQuaternion* %4)
  %call = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKS_(%class.btQuaternion* %q, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp)
  %5 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #7
  %6 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getXEv(%class.btQuadWord* %6)
  %7 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getYEv(%class.btQuadWord* %7)
  %8 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getZEv(%class.btQuadWord* %8)
  %call4 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %call1, float* nonnull align 4 dereferenceable(4) %call2, float* nonnull align 4 dereferenceable(4) %call3)
  %9 = bitcast %class.btQuaternion* %q to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #7
  ret void
}

define linkonce_odr hidden void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats3 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %4, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [4 x float], [4 x float]* %m_floats3, i32 0, i32 1
  %5 = load float, float* %arrayidx4, align 4, !tbaa !8
  %fneg5 = fneg float %5
  store float %fneg5, float* %ref.tmp2, align 4, !tbaa !8
  %6 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats7 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %7, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 2
  %8 = load float, float* %arrayidx8, align 4, !tbaa !8
  %fneg9 = fneg float %8
  store float %fneg9, float* %ref.tmp6, align 4, !tbaa !8
  %9 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats10 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %9, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 3
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %10 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #7
  %11 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #7
  %12 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #7
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZNK11btMultiBody17getWorldToBaseRotEv(%class.btMultiBody* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %base_quat = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 2
  ret %class.btQuaternion* %base_quat
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMultiBody10getBasePosEv(%class.btMultiBody* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %base_pos = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 1
  ret %class.btVector3* %base_pos
}

define hidden void @_ZNK11btMultiBody15worldPosToLocalEiRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMultiBody* %this, i32 %i, %class.btVector3* nonnull align 4 dereferenceable(16) %world_pos) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %i.addr = alloca i32, align 4
  %world_pos.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  store %class.btVector3* %world_pos, %class.btVector3** %world_pos.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = load i32, i32* %i.addr, align 4, !tbaa !6
  %cmp = icmp eq i32 %0, -1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %call = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZNK11btMultiBody17getWorldToBaseRotEv(%class.btMultiBody* %this1)
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #7
  %2 = load %class.btVector3*, %class.btVector3** %world_pos.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMultiBody10getBasePosEv(%class.btMultiBody* %this1)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %call2)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %3) #7
  br label %return

if.else:                                          ; preds = %entry
  %4 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #7
  %5 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call4 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZNK11btMultiBody19getParentToLocalRotEi(%class.btMultiBody* %this1, i32 %5)
  %6 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #7
  %7 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call6 = call i32 @_ZNK11btMultiBody9getParentEi(%class.btMultiBody* %this1, i32 %7)
  %8 = load %class.btVector3*, %class.btVector3** %world_pos.addr, align 4, !tbaa !2
  call void @_ZNK11btMultiBody15worldPosToLocalEiRK9btVector3(%class.btVector3* sret align 4 %ref.tmp5, %class.btMultiBody* %this1, i32 %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %ref.tmp3, %class.btQuaternion* nonnull align 4 dereferenceable(16) %call4, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %9 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMultiBody10getRVectorEi(%class.btMultiBody* %this1, i32 %9)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %call7)
  %10 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %10) #7
  %11 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %11) #7
  br label %return

return:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #3 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !8
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !8
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !8
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !8
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !8
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #7
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #7
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  ret void
}

define hidden void @_ZNK11btMultiBody15localDirToWorldEiRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMultiBody* %this, i32 %i, %class.btVector3* nonnull align 4 dereferenceable(16) %local_dir) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %i.addr = alloca i32, align 4
  %local_dir.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btQuaternion, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btQuaternion, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  store %class.btVector3* %local_dir, %class.btVector3** %local_dir.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %local_dir.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVector3* %agg.result to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !28
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %3 = load i32, i32* %i.addr, align 4, !tbaa !6
  %cmp = icmp ne i32 %3, -1
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #7
  %5 = bitcast %class.btQuaternion* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #7
  %6 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZNK11btMultiBody19getParentToLocalRotEi(%class.btMultiBody* %this1, i32 %6)
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp2, %class.btQuaternion* %call)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %agg.result)
  %7 = bitcast %class.btVector3* %agg.result to i8*
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !28
  %9 = bitcast %class.btQuaternion* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #7
  %10 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %10) #7
  %11 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call3 = call i32 @_ZNK11btMultiBody9getParentEi(%class.btMultiBody* %this1, i32 %11)
  store i32 %call3, i32* %i.addr, align 4, !tbaa !6
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %12 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %12) #7
  %13 = bitcast %class.btQuaternion* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #7
  %call6 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZNK11btMultiBody17getWorldToBaseRotEv(%class.btMultiBody* %this1)
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp5, %class.btQuaternion* %call6)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %ref.tmp4, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %agg.result)
  %14 = bitcast %class.btVector3* %agg.result to i8*
  %15 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %15, i32 16, i1 false), !tbaa.struct !28
  %16 = bitcast %class.btQuaternion* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %16) #7
  %17 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %17) #7
  ret void
}

define hidden void @_ZNK11btMultiBody15worldDirToLocalEiRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMultiBody* %this, i32 %i, %class.btVector3* nonnull align 4 dereferenceable(16) %world_dir) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %i.addr = alloca i32, align 4
  %world_dir.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  store %class.btVector3* %world_dir, %class.btVector3** %world_dir.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = load i32, i32* %i.addr, align 4, !tbaa !6
  %cmp = icmp eq i32 %0, -1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %call = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZNK11btMultiBody17getWorldToBaseRotEv(%class.btMultiBody* %this1)
  %1 = load %class.btVector3*, %class.btVector3** %world_dir.addr, align 4, !tbaa !2
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  br label %return

if.else:                                          ; preds = %entry
  %2 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call2 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZNK11btMultiBody19getParentToLocalRotEi(%class.btMultiBody* %this1, i32 %2)
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #7
  %4 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call3 = call i32 @_ZNK11btMultiBody9getParentEi(%class.btMultiBody* %this1, i32 %4)
  %5 = load %class.btVector3*, %class.btVector3** %world_dir.addr, align 4, !tbaa !2
  call void @_ZNK11btMultiBody15worldDirToLocalEiRK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMultiBody* %this1, i32 %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %call2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %6) #7
  br label %return

return:                                           ; preds = %if.else, %if.then
  ret void
}

define hidden void @_ZNK11btMultiBody22compTreeLinkVelocitiesEP9btVector3S1_(%class.btMultiBody* %this, %class.btVector3* %omega, %class.btVector3* %vel) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %omega.addr = alloca %class.btVector3*, align 4
  %vel.addr = alloca %class.btVector3*, align 4
  %num_links = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  %parent = alloca i32, align 4
  %ref.tmp9 = alloca %class.btMatrix3x3, align 4
  %ref.tmp22 = alloca %class.btVector3, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp30 = alloca %class.btVector3, align 4
  %ref.tmp31 = alloca float, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %omega, %class.btVector3** %omega.addr, align 4, !tbaa !2
  store %class.btVector3* %vel, %class.btVector3** %vel.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = bitcast i32* %num_links to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %this1)
  store i32 %call, i32* %num_links, align 4, !tbaa !6
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #7
  %base_quat = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 2
  %2 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #7
  call void @_ZNK11btMultiBody12getBaseOmegaEv(%class.btVector3* sret align 4 %ref.tmp2, %class.btMultiBody* %this1)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btQuaternion* nonnull align 4 dereferenceable(16) %base_quat, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %3 = load %class.btVector3*, %class.btVector3** %omega.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0
  %4 = bitcast %class.btVector3* %arrayidx to i8*
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !28
  %6 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %6) #7
  %7 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %7) #7
  %8 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #7
  %base_quat4 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 2
  %9 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #7
  call void @_ZNK11btMultiBody10getBaseVelEv(%class.btVector3* sret align 4 %ref.tmp5, %class.btMultiBody* %this1)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %ref.tmp3, %class.btQuaternion* nonnull align 4 dereferenceable(16) %base_quat4, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %10 = load %class.btVector3*, %class.btVector3** %vel.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0
  %11 = bitcast %class.btVector3* %arrayidx6 to i8*
  %12 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 16, i1 false), !tbaa.struct !28
  %13 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %13) #7
  %14 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %14) #7
  %15 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %16 = load i32, i32* %i, align 4, !tbaa !6
  %17 = load i32, i32* %num_links, align 4, !tbaa !6
  %cmp = icmp slt i32 %16, %17
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %18 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %19 = bitcast i32* %parent to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #7
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %call7 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links, i32 %20)
  %parent8 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call7, i32 0, i32 3
  %21 = load i32, i32* %parent8, align 4, !tbaa !45
  store i32 %21, i32* %parent, align 4, !tbaa !6
  %22 = bitcast %class.btMatrix3x3* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %22) #7
  %links10 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %23 = load i32, i32* %i, align 4, !tbaa !6
  %call11 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links10, i32 %23)
  %cached_rot_parent_to_this = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call11, i32 0, i32 10
  %call12 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* %ref.tmp9, %class.btQuaternion* nonnull align 4 dereferenceable(16) %cached_rot_parent_to_this)
  %links13 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %24 = load i32, i32* %i, align 4, !tbaa !6
  %call14 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links13, i32 %24)
  %cached_r_vector = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call14, i32 0, i32 11
  %25 = load %class.btVector3*, %class.btVector3** %omega.addr, align 4, !tbaa !2
  %26 = load i32, i32* %parent, align 4, !tbaa !6
  %add = add nsw i32 %26, 1
  %arrayidx15 = getelementptr inbounds %class.btVector3, %class.btVector3* %25, i32 %add
  %27 = load %class.btVector3*, %class.btVector3** %vel.addr, align 4, !tbaa !2
  %28 = load i32, i32* %parent, align 4, !tbaa !6
  %add16 = add nsw i32 %28, 1
  %arrayidx17 = getelementptr inbounds %class.btVector3, %class.btVector3* %27, i32 %add16
  %29 = load %class.btVector3*, %class.btVector3** %omega.addr, align 4, !tbaa !2
  %30 = load i32, i32* %i, align 4, !tbaa !6
  %add18 = add nsw i32 %30, 1
  %arrayidx19 = getelementptr inbounds %class.btVector3, %class.btVector3* %29, i32 %add18
  %31 = load %class.btVector3*, %class.btVector3** %vel.addr, align 4, !tbaa !2
  %32 = load i32, i32* %i, align 4, !tbaa !6
  %add20 = add nsw i32 %32, 1
  %arrayidx21 = getelementptr inbounds %class.btVector3, %class.btVector3* %31, i32 %add20
  call void @_ZN12_GLOBAL__N_116SpatialTransformERK11btMatrix3x3RK9btVector3S5_S5_RS3_S6_(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %cached_r_vector, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx15, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx17, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx19, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx21)
  %33 = bitcast %class.btMatrix3x3* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %33) #7
  %34 = bitcast %class.btVector3* %ref.tmp22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %34) #7
  %35 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #7
  %36 = load i32, i32* %i, align 4, !tbaa !6
  %call24 = call float @_ZNK11btMultiBody11getJointVelEi(%class.btMultiBody* %this1, i32 %36)
  store float %call24, float* %ref.tmp23, align 4, !tbaa !8
  %links25 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %37 = load i32, i32* %i, align 4, !tbaa !6
  %call26 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links25, i32 %37)
  %axis_top = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call26, i32 0, i32 5
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp22, float* nonnull align 4 dereferenceable(4) %ref.tmp23, %class.btVector3* nonnull align 4 dereferenceable(16) %axis_top)
  %38 = load %class.btVector3*, %class.btVector3** %omega.addr, align 4, !tbaa !2
  %39 = load i32, i32* %i, align 4, !tbaa !6
  %add27 = add nsw i32 %39, 1
  %arrayidx28 = getelementptr inbounds %class.btVector3, %class.btVector3* %38, i32 %add27
  %call29 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %arrayidx28, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp22)
  %40 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #7
  %41 = bitcast %class.btVector3* %ref.tmp22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %41) #7
  %42 = bitcast %class.btVector3* %ref.tmp30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %42) #7
  %43 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #7
  %44 = load i32, i32* %i, align 4, !tbaa !6
  %call32 = call float @_ZNK11btMultiBody11getJointVelEi(%class.btMultiBody* %this1, i32 %44)
  store float %call32, float* %ref.tmp31, align 4, !tbaa !8
  %links33 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %45 = load i32, i32* %i, align 4, !tbaa !6
  %call34 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links33, i32 %45)
  %axis_bottom = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call34, i32 0, i32 6
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp30, float* nonnull align 4 dereferenceable(4) %ref.tmp31, %class.btVector3* nonnull align 4 dereferenceable(16) %axis_bottom)
  %46 = load %class.btVector3*, %class.btVector3** %vel.addr, align 4, !tbaa !2
  %47 = load i32, i32* %i, align 4, !tbaa !6
  %add35 = add nsw i32 %47, 1
  %arrayidx36 = getelementptr inbounds %class.btVector3, %class.btVector3* %46, i32 %add35
  %call37 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %arrayidx36, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp30)
  %48 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #7
  %49 = bitcast %class.btVector3* %ref.tmp30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %49) #7
  %50 = bitcast i32* %parent to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %51 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %51, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %52 = bitcast i32* %num_links to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #7
  ret void
}

define linkonce_odr hidden i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %call = call i32 @_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv(%class.btAlignedObjectArray* %links)
  ret i32 %call
}

define linkonce_odr hidden void @_ZNK11btMultiBody12getBaseOmegaEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btMultiBody* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_real_buf = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 9
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_real_buf, i32 0)
  %m_real_buf2 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 9
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_real_buf2, i32 1)
  %m_real_buf4 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 9
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_real_buf4, i32 2)
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call3, float* nonnull align 4 dereferenceable(4) %call5)
  ret void
}

define linkonce_odr hidden void @_ZNK11btMultiBody10getBaseVelEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btMultiBody* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_real_buf = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 9
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_real_buf, i32 3)
  %m_real_buf2 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 9
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_real_buf2, i32 4)
  %m_real_buf4 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 9
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_real_buf4, i32 5)
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call3, float* nonnull align 4 dereferenceable(4) %call5)
  ret void
}

define internal void @_ZN12_GLOBAL__N_116SpatialTransformERK11btMatrix3x3RK9btVector3S5_S5_RS3_S6_(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %rotation_matrix, %class.btVector3* nonnull align 4 dereferenceable(16) %displacement, %class.btVector3* nonnull align 4 dereferenceable(16) %top_in, %class.btVector3* nonnull align 4 dereferenceable(16) %bottom_in, %class.btVector3* nonnull align 4 dereferenceable(16) %top_out, %class.btVector3* nonnull align 4 dereferenceable(16) %bottom_out) #0 {
entry:
  %rotation_matrix.addr = alloca %class.btMatrix3x3*, align 4
  %displacement.addr = alloca %class.btVector3*, align 4
  %top_in.addr = alloca %class.btVector3*, align 4
  %bottom_in.addr = alloca %class.btVector3*, align 4
  %top_out.addr = alloca %class.btVector3*, align 4
  %bottom_out.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  store %class.btMatrix3x3* %rotation_matrix, %class.btMatrix3x3** %rotation_matrix.addr, align 4, !tbaa !2
  store %class.btVector3* %displacement, %class.btVector3** %displacement.addr, align 4, !tbaa !2
  store %class.btVector3* %top_in, %class.btVector3** %top_in.addr, align 4, !tbaa !2
  store %class.btVector3* %bottom_in, %class.btVector3** %bottom_in.addr, align 4, !tbaa !2
  store %class.btVector3* %top_out, %class.btVector3** %top_out.addr, align 4, !tbaa !2
  store %class.btVector3* %bottom_out, %class.btVector3** %bottom_out.addr, align 4, !tbaa !2
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #7
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rotation_matrix.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %top_in.addr, align 4, !tbaa !2
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  %3 = load %class.btVector3*, %class.btVector3** %top_out.addr, align 4, !tbaa !2
  %4 = bitcast %class.btVector3* %3 to i8*
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !28
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %6) #7
  %7 = bitcast %class.btVector3* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #7
  %8 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #7
  %9 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #7
  %10 = load %class.btVector3*, %class.btVector3** %displacement.addr, align 4, !tbaa !2
  %11 = load %class.btVector3*, %class.btVector3** %top_out.addr, align 4, !tbaa !2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* %10, %class.btVector3* nonnull align 4 dereferenceable(16) %11)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  %12 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %12) #7
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rotation_matrix.addr, align 4, !tbaa !2
  %14 = load %class.btVector3*, %class.btVector3** %bottom_in.addr, align 4, !tbaa !2
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp4, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %13, %class.btVector3* nonnull align 4 dereferenceable(16) %14)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  %15 = load %class.btVector3*, %class.btVector3** %bottom_out.addr, align 4, !tbaa !2
  %16 = bitcast %class.btVector3* %15 to i8*
  %17 = bitcast %class.btVector3* %ref.tmp1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false), !tbaa.struct !28
  %18 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #7
  %19 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %19) #7
  %20 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %20) #7
  %21 = bitcast %class.btVector3* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %21) #7
  ret void
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* returned %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0)
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

define hidden float @_ZNK11btMultiBody16getKineticEnergyEv(%class.btMultiBody* %this) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %num_links = alloca i32, align 4
  %omega = alloca %class.btAlignedObjectArray.8, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %vel = alloca %class.btAlignedObjectArray.8, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %result = alloca float, align 4
  %ref.tmp14 = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  %ref.tmp28 = alloca %class.btVector3, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = bitcast i32* %num_links to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %this1)
  store i32 %call, i32* %num_links, align 4, !tbaa !6
  %1 = bitcast %class.btAlignedObjectArray.8* %omega to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %1) #7
  %call2 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.8* %omega)
  %2 = load i32, i32* %num_links, align 4, !tbaa !6
  %add = add nsw i32 %2, 1
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #7
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.8* %omega, i32 %add, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #7
  %5 = bitcast %class.btAlignedObjectArray.8* %vel to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %5) #7
  %call4 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.8* %vel)
  %6 = load i32, i32* %num_links, align 4, !tbaa !6
  %add5 = add nsw i32 %6, 1
  %7 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #7
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp6)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.8* %vel, i32 %add5, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp6)
  %8 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #7
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %omega, i32 0)
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vel, i32 0)
  call void @_ZNK11btMultiBody22compTreeLinkVelocitiesEP9btVector3S1_(%class.btMultiBody* %this1, %class.btVector3* %call8, %class.btVector3* %call9)
  %9 = bitcast float* %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %base_mass = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 3
  %10 = load float, float* %base_mass, align 4, !tbaa !27
  %call10 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vel, i32 0)
  %call11 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vel, i32 0)
  %call12 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call10, %class.btVector3* nonnull align 4 dereferenceable(16) %call11)
  %mul = fmul float %10, %call12
  store float %mul, float* %result, align 4, !tbaa !8
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %omega, i32 0)
  %11 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #7
  %base_inertia = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 4
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %omega, i32 0)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp14, %class.btVector3* nonnull align 4 dereferenceable(16) %base_inertia, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  %call16 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call13, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp14)
  %12 = load float, float* %result, align 4, !tbaa !8
  %add17 = fadd float %12, %call16
  store float %add17, float* %result, align 4, !tbaa !8
  %13 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %13) #7
  %14 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %15 = load i32, i32* %i, align 4, !tbaa !6
  %16 = load i32, i32* %num_links, align 4, !tbaa !6
  %cmp = icmp slt i32 %15, %16
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %17 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %call18 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links, i32 %18)
  %mass = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call18, i32 0, i32 1
  %19 = load float, float* %mass, align 4, !tbaa !44
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %add19 = add nsw i32 %20, 1
  %call20 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vel, i32 %add19)
  %21 = load i32, i32* %i, align 4, !tbaa !6
  %add21 = add nsw i32 %21, 1
  %call22 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vel, i32 %add21)
  %call23 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call20, %class.btVector3* nonnull align 4 dereferenceable(16) %call22)
  %mul24 = fmul float %19, %call23
  %22 = load float, float* %result, align 4, !tbaa !8
  %add25 = fadd float %22, %mul24
  store float %add25, float* %result, align 4, !tbaa !8
  %23 = load i32, i32* %i, align 4, !tbaa !6
  %add26 = add nsw i32 %23, 1
  %call27 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %omega, i32 %add26)
  %24 = bitcast %class.btVector3* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %24) #7
  %links29 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %25 = load i32, i32* %i, align 4, !tbaa !6
  %call30 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links29, i32 %25)
  %inertia = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call30, i32 0, i32 2
  %26 = load i32, i32* %i, align 4, !tbaa !6
  %add31 = add nsw i32 %26, 1
  %call32 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %omega, i32 %add31)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp28, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia, %class.btVector3* nonnull align 4 dereferenceable(16) %call32)
  %call33 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call27, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp28)
  %27 = load float, float* %result, align 4, !tbaa !8
  %add34 = fadd float %27, %call33
  store float %add34, float* %result, align 4, !tbaa !8
  %28 = bitcast %class.btVector3* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %28) #7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %29 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %29, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %30 = load float, float* %result, align 4, !tbaa !8
  %mul35 = fmul float 5.000000e-01, %30
  %31 = bitcast float* %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #7
  %call36 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.8* %vel) #7
  %32 = bitcast %class.btAlignedObjectArray.8* %vel to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %32) #7
  %call37 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.8* %omega) #7
  %33 = bitcast %class.btAlignedObjectArray.8* %omega to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %33) #7
  %34 = bitcast i32* %num_links to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #7
  ret float %mul35
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !50
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %1
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !8
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !8
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !8
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #3 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !8
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !8
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !8
  %mul8 = fmul float %7, %9
  store float %mul8, float* %ref.tmp3, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !8
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !8
  %mul14 = fmul float %12, %14
  store float %mul14, float* %ref.tmp9, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #7
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #7
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  ret void
}

define hidden void @_ZNK11btMultiBody18getAngularMomentumEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btMultiBody* %this) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %num_links = alloca i32, align 4
  %omega = alloca %class.btAlignedObjectArray.8, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %vel = alloca %class.btAlignedObjectArray.8, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %rot_from_world = alloca %class.btAlignedObjectArray.16, align 4
  %ref.tmp10 = alloca %class.btQuaternion, align 4
  %ref.tmp15 = alloca %class.btQuaternion, align 4
  %ref.tmp17 = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  %ref.tmp19 = alloca %class.btQuaternion, align 4
  %ref.tmp27 = alloca %class.btVector3, align 4
  %ref.tmp28 = alloca %class.btQuaternion, align 4
  %ref.tmp31 = alloca %class.btVector3, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = bitcast i32* %num_links to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %this1)
  store i32 %call, i32* %num_links, align 4, !tbaa !6
  %1 = bitcast %class.btAlignedObjectArray.8* %omega to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %1) #7
  %call2 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.8* %omega)
  %2 = load i32, i32* %num_links, align 4, !tbaa !6
  %add = add nsw i32 %2, 1
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #7
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.8* %omega, i32 %add, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #7
  %5 = bitcast %class.btAlignedObjectArray.8* %vel to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %5) #7
  %call4 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.8* %vel)
  %6 = load i32, i32* %num_links, align 4, !tbaa !6
  %add5 = add nsw i32 %6, 1
  %7 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #7
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp6)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.8* %vel, i32 %add5, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp6)
  %8 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #7
  %9 = bitcast %class.btAlignedObjectArray.16* %rot_from_world to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %9) #7
  %call8 = call %class.btAlignedObjectArray.16* @_ZN20btAlignedObjectArrayI12btQuaternionEC2Ev(%class.btAlignedObjectArray.16* %rot_from_world)
  %10 = load i32, i32* %num_links, align 4, !tbaa !6
  %add9 = add nsw i32 %10, 1
  %11 = bitcast %class.btQuaternion* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #7
  %call11 = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %ref.tmp10)
  call void @_ZN20btAlignedObjectArrayI12btQuaternionE6resizeEiRKS0_(%class.btAlignedObjectArray.16* %rot_from_world, i32 %add9, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp10)
  %12 = bitcast %class.btQuaternion* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %12) #7
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %omega, i32 0)
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vel, i32 0)
  call void @_ZNK11btMultiBody22compTreeLinkVelocitiesEP9btVector3S1_(%class.btMultiBody* %this1, %class.btVector3* %call12, %class.btVector3* %call13)
  %base_quat = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 2
  %call14 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN20btAlignedObjectArrayI12btQuaternionEixEi(%class.btAlignedObjectArray.16* %rot_from_world, i32 0)
  %13 = bitcast %class.btQuaternion* %call14 to i8*
  %14 = bitcast %class.btQuaternion* %base_quat to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 16, i1 false)
  %15 = bitcast %class.btQuaternion* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %15) #7
  %call16 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN20btAlignedObjectArrayI12btQuaternionEixEi(%class.btAlignedObjectArray.16* %rot_from_world, i32 0)
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp15, %class.btQuaternion* %call16)
  %16 = bitcast %class.btVector3* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #7
  %base_inertia = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 4
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %omega, i32 0)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp17, %class.btVector3* nonnull align 4 dereferenceable(16) %base_inertia, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp15, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp17)
  %17 = bitcast %class.btVector3* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %17) #7
  %18 = bitcast %class.btQuaternion* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #7
  %19 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %21 = load i32, i32* %num_links, align 4, !tbaa !6
  %cmp = icmp slt i32 %20, %21
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %22 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %23 = bitcast %class.btQuaternion* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %23) #7
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %24 = load i32, i32* %i, align 4, !tbaa !6
  %call20 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links, i32 %24)
  %cached_rot_parent_to_this = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call20, i32 0, i32 10
  %links21 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %25 = load i32, i32* %i, align 4, !tbaa !6
  %call22 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links21, i32 %25)
  %parent = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call22, i32 0, i32 3
  %26 = load i32, i32* %parent, align 4, !tbaa !45
  %add23 = add nsw i32 %26, 1
  %call24 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN20btAlignedObjectArrayI12btQuaternionEixEi(%class.btAlignedObjectArray.16* %rot_from_world, i32 %add23)
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %ref.tmp19, %class.btQuaternion* nonnull align 4 dereferenceable(16) %cached_rot_parent_to_this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %call24)
  %27 = load i32, i32* %i, align 4, !tbaa !6
  %add25 = add nsw i32 %27, 1
  %call26 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN20btAlignedObjectArrayI12btQuaternionEixEi(%class.btAlignedObjectArray.16* %rot_from_world, i32 %add25)
  %28 = bitcast %class.btQuaternion* %call26 to i8*
  %29 = bitcast %class.btQuaternion* %ref.tmp19 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %29, i32 16, i1 false)
  %30 = bitcast %class.btQuaternion* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %30) #7
  %31 = bitcast %class.btVector3* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %31) #7
  %32 = bitcast %class.btQuaternion* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %32) #7
  %33 = load i32, i32* %i, align 4, !tbaa !6
  %add29 = add nsw i32 %33, 1
  %call30 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN20btAlignedObjectArrayI12btQuaternionEixEi(%class.btAlignedObjectArray.16* %rot_from_world, i32 %add29)
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp28, %class.btQuaternion* %call30)
  %34 = bitcast %class.btVector3* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %34) #7
  %links32 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %35 = load i32, i32* %i, align 4, !tbaa !6
  %call33 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links32, i32 %35)
  %inertia = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call33, i32 0, i32 2
  %36 = load i32, i32* %i, align 4, !tbaa !6
  %add34 = add nsw i32 %36, 1
  %call35 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %omega, i32 %add34)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp31, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia, %class.btVector3* nonnull align 4 dereferenceable(16) %call35)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %ref.tmp27, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp28, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp31)
  %call36 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp27)
  %37 = bitcast %class.btVector3* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %37) #7
  %38 = bitcast %class.btQuaternion* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %38) #7
  %39 = bitcast %class.btVector3* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %39) #7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %40 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %40, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %call37 = call %class.btAlignedObjectArray.16* @_ZN20btAlignedObjectArrayI12btQuaternionED2Ev(%class.btAlignedObjectArray.16* %rot_from_world) #7
  %41 = bitcast %class.btAlignedObjectArray.16* %rot_from_world to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %41) #7
  %call38 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.8* %vel) #7
  %42 = bitcast %class.btAlignedObjectArray.8* %vel to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %42) #7
  %call39 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.8* %omega) #7
  %43 = bitcast %class.btAlignedObjectArray.8* %omega to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %43) #7
  %44 = bitcast i32* %num_links to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #7
  ret void
}

define linkonce_odr hidden %class.btAlignedObjectArray.16* @_ZN20btAlignedObjectArrayI12btQuaternionEC2Ev(%class.btAlignedObjectArray.16* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.17* @_ZN18btAlignedAllocatorI12btQuaternionLj16EEC2Ev(%class.btAlignedAllocator.17* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI12btQuaternionE4initEv(%class.btAlignedObjectArray.16* %this1)
  ret %class.btAlignedObjectArray.16* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btQuaternionE6resizeEiRKS0_(%class.btAlignedObjectArray.16* %this, i32 %newsize, %class.btQuaternion* nonnull align 4 dereferenceable(16) %fillData) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btQuaternion*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !6
  store %class.btQuaternion* %fillData, %class.btQuaternion** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK20btAlignedObjectArrayI12btQuaternionE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !6
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %2 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  store i32 %4, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %8 = load %class.btQuaternion*, %class.btQuaternion** %m_data, align 4, !tbaa !56
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btQuaternion, %class.btQuaternion* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI12btQuaternionE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayI12btQuaternionE7reserveEi(%class.btAlignedObjectArray.16* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = load i32, i32* %curSize, align 4, !tbaa !6
  store i32 %14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !6
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %18 = load %class.btQuaternion*, %class.btQuaternion** %m_data11, align 4, !tbaa !56
  %19 = load i32, i32* %i6, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds %class.btQuaternion, %class.btQuaternion* %18, i32 %19
  %20 = bitcast %class.btQuaternion* %arrayidx12 to i8*
  %21 = bitcast i8* %20 to %class.btQuaternion*
  %22 = load %class.btQuaternion*, %class.btQuaternion** %fillData.addr, align 4, !tbaa !2
  %23 = bitcast %class.btQuaternion* %21 to i8*
  %24 = bitcast %class.btQuaternion* %22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false)
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %25 = load i32, i32* %i6, align 4, !tbaa !6
  %inc14 = add nsw i32 %25, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %26 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  store i32 %26, i32* %m_size, align 4, !tbaa !59
  %27 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #7
  ret void
}

define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* %0)
  ret %class.btQuaternion* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN20btAlignedObjectArrayI12btQuaternionEixEi(%class.btAlignedObjectArray.16* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %m_data, align 4, !tbaa !56
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btQuaternion, %class.btQuaternion* %0, i32 %1
  ret %class.btQuaternion* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q2) #3 comdat {
entry:
  %q1.addr = alloca %class.btQuaternion*, align 4
  %q2.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp44 = alloca float, align 4
  store %class.btQuaternion* %q1, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q2, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %2 = bitcast %class.btQuaternion* %1 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %2)
  %3 = load float, float* %call, align 4, !tbaa !8
  %4 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %5 = bitcast %class.btQuaternion* %4 to %class.btQuadWord*
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %5)
  %6 = load float, float* %call1, align 4, !tbaa !8
  %mul = fmul float %3, %6
  %7 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %8 = bitcast %class.btQuaternion* %7 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %8)
  %9 = load float, float* %call2, align 4, !tbaa !8
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %11)
  %12 = load float, float* %call3, align 4, !tbaa !8
  %mul4 = fmul float %9, %12
  %add = fadd float %mul, %mul4
  %13 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %14 = bitcast %class.btQuaternion* %13 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %14)
  %15 = load float, float* %call5, align 4, !tbaa !8
  %16 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %17 = bitcast %class.btQuaternion* %16 to %class.btQuadWord*
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %17)
  %18 = load float, float* %call6, align 4, !tbaa !8
  %mul7 = fmul float %15, %18
  %add8 = fadd float %add, %mul7
  %19 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %20 = bitcast %class.btQuaternion* %19 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %20)
  %21 = load float, float* %call9, align 4, !tbaa !8
  %22 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %23 = bitcast %class.btQuaternion* %22 to %class.btQuadWord*
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %23)
  %24 = load float, float* %call10, align 4, !tbaa !8
  %mul11 = fmul float %21, %24
  %sub = fsub float %add8, %mul11
  store float %sub, float* %ref.tmp, align 4, !tbaa !8
  %25 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #7
  %26 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %27 = bitcast %class.btQuaternion* %26 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %27)
  %28 = load float, float* %call13, align 4, !tbaa !8
  %29 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %30 = bitcast %class.btQuaternion* %29 to %class.btQuadWord*
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %30)
  %31 = load float, float* %call14, align 4, !tbaa !8
  %mul15 = fmul float %28, %31
  %32 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %33 = bitcast %class.btQuaternion* %32 to %class.btQuadWord*
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %33)
  %34 = load float, float* %call16, align 4, !tbaa !8
  %35 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %36 = bitcast %class.btQuaternion* %35 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %36)
  %37 = load float, float* %call17, align 4, !tbaa !8
  %mul18 = fmul float %34, %37
  %add19 = fadd float %mul15, %mul18
  %38 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %39 = bitcast %class.btQuaternion* %38 to %class.btQuadWord*
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %39)
  %40 = load float, float* %call20, align 4, !tbaa !8
  %41 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %42 = bitcast %class.btQuaternion* %41 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %42)
  %43 = load float, float* %call21, align 4, !tbaa !8
  %mul22 = fmul float %40, %43
  %add23 = fadd float %add19, %mul22
  %44 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %45 = bitcast %class.btQuaternion* %44 to %class.btQuadWord*
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %45)
  %46 = load float, float* %call24, align 4, !tbaa !8
  %47 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %48 = bitcast %class.btQuaternion* %47 to %class.btQuadWord*
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %48)
  %49 = load float, float* %call25, align 4, !tbaa !8
  %mul26 = fmul float %46, %49
  %sub27 = fsub float %add23, %mul26
  store float %sub27, float* %ref.tmp12, align 4, !tbaa !8
  %50 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #7
  %51 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %52 = bitcast %class.btQuaternion* %51 to %class.btQuadWord*
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %52)
  %53 = load float, float* %call29, align 4, !tbaa !8
  %54 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %55 = bitcast %class.btQuaternion* %54 to %class.btQuadWord*
  %call30 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %55)
  %56 = load float, float* %call30, align 4, !tbaa !8
  %mul31 = fmul float %53, %56
  %57 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %58 = bitcast %class.btQuaternion* %57 to %class.btQuadWord*
  %call32 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %58)
  %59 = load float, float* %call32, align 4, !tbaa !8
  %60 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %61 = bitcast %class.btQuaternion* %60 to %class.btQuadWord*
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %61)
  %62 = load float, float* %call33, align 4, !tbaa !8
  %mul34 = fmul float %59, %62
  %add35 = fadd float %mul31, %mul34
  %63 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %64 = bitcast %class.btQuaternion* %63 to %class.btQuadWord*
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %64)
  %65 = load float, float* %call36, align 4, !tbaa !8
  %66 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %67 = bitcast %class.btQuaternion* %66 to %class.btQuadWord*
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %67)
  %68 = load float, float* %call37, align 4, !tbaa !8
  %mul38 = fmul float %65, %68
  %add39 = fadd float %add35, %mul38
  %69 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %70 = bitcast %class.btQuaternion* %69 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %70)
  %71 = load float, float* %call40, align 4, !tbaa !8
  %72 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %73 = bitcast %class.btQuaternion* %72 to %class.btQuadWord*
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %73)
  %74 = load float, float* %call41, align 4, !tbaa !8
  %mul42 = fmul float %71, %74
  %sub43 = fsub float %add39, %mul42
  store float %sub43, float* %ref.tmp28, align 4, !tbaa !8
  %75 = bitcast float* %ref.tmp44 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #7
  %76 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %77 = bitcast %class.btQuaternion* %76 to %class.btQuadWord*
  %call45 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %77)
  %78 = load float, float* %call45, align 4, !tbaa !8
  %79 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %80 = bitcast %class.btQuaternion* %79 to %class.btQuadWord*
  %call46 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %80)
  %81 = load float, float* %call46, align 4, !tbaa !8
  %mul47 = fmul float %78, %81
  %82 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %83 = bitcast %class.btQuaternion* %82 to %class.btQuadWord*
  %call48 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %83)
  %84 = load float, float* %call48, align 4, !tbaa !8
  %85 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %86 = bitcast %class.btQuaternion* %85 to %class.btQuadWord*
  %call49 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %86)
  %87 = load float, float* %call49, align 4, !tbaa !8
  %mul50 = fmul float %84, %87
  %sub51 = fsub float %mul47, %mul50
  %88 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %89 = bitcast %class.btQuaternion* %88 to %class.btQuadWord*
  %call52 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %89)
  %90 = load float, float* %call52, align 4, !tbaa !8
  %91 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %92 = bitcast %class.btQuaternion* %91 to %class.btQuadWord*
  %call53 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %92)
  %93 = load float, float* %call53, align 4, !tbaa !8
  %mul54 = fmul float %90, %93
  %sub55 = fsub float %sub51, %mul54
  %94 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %95 = bitcast %class.btQuaternion* %94 to %class.btQuadWord*
  %call56 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %95)
  %96 = load float, float* %call56, align 4, !tbaa !8
  %97 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %98 = bitcast %class.btQuaternion* %97 to %class.btQuadWord*
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %98)
  %99 = load float, float* %call57, align 4, !tbaa !8
  %mul58 = fmul float %96, %99
  %sub59 = fsub float %sub55, %mul58
  store float %sub59, float* %ref.tmp44, align 4, !tbaa !8
  %call60 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp44)
  %100 = bitcast float* %ref.tmp44 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #7
  %101 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #7
  %102 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #7
  %103 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #7
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.16* @_ZN20btAlignedObjectArrayI12btQuaternionED2Ev(%class.btAlignedObjectArray.16* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI12btQuaternionE5clearEv(%class.btAlignedObjectArray.16* %this1)
  ret %class.btAlignedObjectArray.16* %this1
}

define hidden void @_ZN11btMultiBody21clearForcesAndTorquesEv(%class.btMultiBody* %this) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %i = alloca i32, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %base_force = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 5
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !8
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !8
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !8
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %base_force, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #7
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #7
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #7
  %base_torque = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 6
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !8
  %7 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  store float 0.000000e+00, float* %ref.tmp5, align 4, !tbaa !8
  %8 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !8
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %base_torque, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %9 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #7
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #7
  %11 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #7
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %13 = load i32, i32* %i, align 4, !tbaa !6
  %call = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %this1)
  %cmp = icmp slt i32 %13, %call
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %14 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %15 = load i32, i32* %i, align 4, !tbaa !6
  %call7 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links, i32 %15)
  %applied_force = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call7, i32 0, i32 12
  %16 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #7
  store float 0.000000e+00, float* %ref.tmp8, align 4, !tbaa !8
  %17 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #7
  store float 0.000000e+00, float* %ref.tmp9, align 4, !tbaa !8
  %18 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #7
  store float 0.000000e+00, float* %ref.tmp10, align 4, !tbaa !8
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %applied_force, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp10)
  %19 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #7
  %20 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #7
  %21 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #7
  %links11 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %22 = load i32, i32* %i, align 4, !tbaa !6
  %call12 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links11, i32 %22)
  %applied_torque = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call12, i32 0, i32 13
  %23 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #7
  store float 0.000000e+00, float* %ref.tmp13, align 4, !tbaa !8
  %24 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #7
  store float 0.000000e+00, float* %ref.tmp14, align 4, !tbaa !8
  %25 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #7
  store float 0.000000e+00, float* %ref.tmp15, align 4, !tbaa !8
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %applied_torque, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp15)
  %26 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #7
  %27 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #7
  %28 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #7
  %links16 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %29 = load i32, i32* %i, align 4, !tbaa !6
  %call17 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links16, i32 %29)
  %joint_torque = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call17, i32 0, i32 14
  store float 0.000000e+00, float* %joint_torque, align 4, !tbaa !47
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %30 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %30, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

define hidden void @_ZN11btMultiBody15clearVelocitiesEv(%class.btMultiBody* %this) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %i = alloca i32, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %call = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %this1)
  %add = add nsw i32 6, %call
  %cmp = icmp slt i32 %1, %add
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_real_buf = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 9
  %3 = load i32, i32* %i, align 4, !tbaa !6
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_real_buf, i32 %3)
  store float 0.000000e+00, float* %call2, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

define hidden void @_ZN11btMultiBody12addLinkForceEiRK9btVector3(%class.btMultiBody* %this, i32 %i, %class.btVector3* nonnull align 4 dereferenceable(16) %f) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %i.addr = alloca i32, align 4
  %f.addr = alloca %class.btVector3*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  store %class.btVector3* %f, %class.btVector3** %f.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %f.addr, align 4, !tbaa !2
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %1 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links, i32 %1)
  %applied_force = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call, i32 0, i32 12
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %applied_force, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

define hidden void @_ZN11btMultiBody13addLinkTorqueEiRK9btVector3(%class.btMultiBody* %this, i32 %i, %class.btVector3* nonnull align 4 dereferenceable(16) %t) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %i.addr = alloca i32, align 4
  %t.addr = alloca %class.btVector3*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  store %class.btVector3* %t, %class.btVector3** %t.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %t.addr, align 4, !tbaa !2
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %1 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links, i32 %1)
  %applied_torque = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call, i32 0, i32 13
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %applied_torque, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

define hidden void @_ZN11btMultiBody14addJointTorqueEif(%class.btMultiBody* %this, i32 %i, float %Q) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %i.addr = alloca i32, align 4
  %Q.addr = alloca float, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  store float %Q, float* %Q.addr, align 4, !tbaa !8
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = load float, float* %Q.addr, align 4, !tbaa !8
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %1 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links, i32 %1)
  %joint_torque = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call, i32 0, i32 14
  %2 = load float, float* %joint_torque, align 4, !tbaa !47
  %add = fadd float %2, %0
  store float %add, float* %joint_torque, align 4, !tbaa !47
  ret void
}

define hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMultiBody12getLinkForceEi(%class.btMultiBody* %this, i32 %i) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %0 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links, i32 %0)
  %applied_force = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call, i32 0, i32 12
  ret %class.btVector3* %applied_force
}

define hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMultiBody13getLinkTorqueEi(%class.btMultiBody* %this, i32 %i) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %0 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links, i32 %0)
  %applied_torque = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call, i32 0, i32 13
  ret %class.btVector3* %applied_torque
}

define hidden float @_ZNK11btMultiBody14getJointTorqueEi(%class.btMultiBody* %this, i32 %i) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %0 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links, i32 %0)
  %joint_torque = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call, i32 0, i32 14
  %1 = load float, float* %joint_torque, align 4, !tbaa !47
  ret float %1
}

define hidden void @_ZN11btMultiBody14stepVelocitiesEfR20btAlignedObjectArrayIfERS0_I9btVector3ERS0_I11btMatrix3x3E(%class.btMultiBody* %this, float %dt, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17) %scratch_r, %class.btAlignedObjectArray.8* nonnull align 4 dereferenceable(17) %scratch_v, %class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17) %scratch_m) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %dt.addr = alloca float, align 4
  %scratch_r.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %scratch_v.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %scratch_m.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %num_links = alloca i32, align 4
  %DAMPING_K1_LINEAR = alloca float, align 4
  %DAMPING_K2_LINEAR = alloca float, align 4
  %DAMPING_K1_ANGULAR = alloca float, align 4
  %DAMPING_K2_ANGULAR = alloca float, align 4
  %base_vel = alloca %class.btVector3, align 4
  %base_omega = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btMatrix3x3, align 4
  %r_ptr = alloca float*, align 4
  %output = alloca float*, align 4
  %v_ptr = alloca %class.btVector3*, align 4
  %vel_top_angular = alloca %class.btVector3*, align 4
  %vel_bottom_linear = alloca %class.btVector3*, align 4
  %zero_acc_top_angular = alloca %class.btVector3*, align 4
  %zero_acc_bottom_linear = alloca %class.btVector3*, align 4
  %coriolis_top_angular = alloca %class.btVector3*, align 4
  %coriolis_bottom_linear = alloca %class.btVector3*, align 4
  %inertia_top_left = alloca %class.btMatrix3x3*, align 4
  %inertia_top_right = alloca %class.btMatrix3x3*, align 4
  %inertia_bottom_left = alloca %class.btMatrix3x3*, align 4
  %rot_from_parent = alloca %class.btMatrix3x3*, align 4
  %rot_from_world = alloca %class.btMatrix3x3*, align 4
  %h_top = alloca %class.btVector3*, align 4
  %h_bottom = alloca %class.btVector3*, align 4
  %accel_top = alloca %class.btVector3*, align 4
  %accel_bottom = alloca %class.btVector3*, align 4
  %Y = alloca float*, align 4
  %D = alloca float*, align 4
  %joint_accel = alloca float*, align 4
  %ref.tmp55 = alloca %class.btMatrix3x3, align 4
  %ref.tmp58 = alloca %class.btVector3, align 4
  %ref.tmp61 = alloca %class.btVector3, align 4
  %ref.tmp64 = alloca %class.btVector3, align 4
  %ref.tmp65 = alloca float, align 4
  %ref.tmp66 = alloca float, align 4
  %ref.tmp67 = alloca float, align 4
  %ref.tmp71 = alloca %class.btVector3, align 4
  %ref.tmp72 = alloca %class.btVector3, align 4
  %ref.tmp74 = alloca %class.btVector3, align 4
  %ref.tmp75 = alloca %class.btVector3, align 4
  %ref.tmp76 = alloca float, align 4
  %ref.tmp82 = alloca %class.btVector3, align 4
  %ref.tmp83 = alloca %class.btVector3, align 4
  %ref.tmp88 = alloca %class.btVector3, align 4
  %ref.tmp90 = alloca %class.btVector3, align 4
  %ref.tmp94 = alloca %class.btVector3, align 4
  %ref.tmp95 = alloca %class.btVector3, align 4
  %ref.tmp98 = alloca float, align 4
  %ref.tmp106 = alloca %class.btMatrix3x3, align 4
  %ref.tmp107 = alloca float, align 4
  %ref.tmp108 = alloca float, align 4
  %ref.tmp109 = alloca float, align 4
  %ref.tmp110 = alloca float, align 4
  %ref.tmp111 = alloca float, align 4
  %ref.tmp112 = alloca float, align 4
  %ref.tmp113 = alloca float, align 4
  %ref.tmp114 = alloca float, align 4
  %ref.tmp115 = alloca float, align 4
  %ref.tmp121 = alloca float, align 4
  %ref.tmp122 = alloca float, align 4
  %ref.tmp123 = alloca float, align 4
  %ref.tmp125 = alloca float, align 4
  %ref.tmp126 = alloca float, align 4
  %ref.tmp127 = alloca float, align 4
  %ref.tmp133 = alloca float, align 4
  %ref.tmp134 = alloca float, align 4
  %ref.tmp135 = alloca float, align 4
  %ref.tmp139 = alloca float, align 4
  %ref.tmp140 = alloca float, align 4
  %ref.tmp141 = alloca float, align 4
  %i = alloca i32, align 4
  %parent = alloca i32, align 4
  %ref.tmp151 = alloca %class.btMatrix3x3, align 4
  %ref.tmp158 = alloca %class.btMatrix3x3, align 4
  %ref.tmp178 = alloca %class.btVector3, align 4
  %ref.tmp179 = alloca %class.btVector3, align 4
  %ref.tmp182 = alloca %class.btVector3, align 4
  %ref.tmp188 = alloca %class.btVector3, align 4
  %ref.tmp189 = alloca %class.btVector3, align 4
  %ref.tmp190 = alloca float, align 4
  %ref.tmp191 = alloca %class.btVector3, align 4
  %ref.tmp196 = alloca float, align 4
  %ref.tmp203 = alloca %class.btVector3, align 4
  %ref.tmp204 = alloca %class.btVector3, align 4
  %ref.tmp209 = alloca float, align 4
  %ref.tmp212 = alloca %class.btVector3, align 4
  %ref.tmp213 = alloca float, align 4
  %ref.tmp217 = alloca %class.btVector3, align 4
  %ref.tmp227 = alloca %class.btVector3, align 4
  %ref.tmp228 = alloca float, align 4
  %ref.tmp229 = alloca float, align 4
  %ref.tmp230 = alloca float, align 4
  %ref.tmp234 = alloca %class.btVector3, align 4
  %ref.tmp235 = alloca float, align 4
  %ref.tmp243 = alloca %class.btVector3, align 4
  %ref.tmp244 = alloca float, align 4
  %ref.tmp252 = alloca %class.btVector3, align 4
  %ref.tmp253 = alloca %class.btVector3, align 4
  %ref.tmp260 = alloca %class.btVector3, align 4
  %ref.tmp261 = alloca float, align 4
  %ref.tmp275 = alloca %class.btVector3, align 4
  %ref.tmp276 = alloca %class.btVector3, align 4
  %ref.tmp286 = alloca %class.btVector3, align 4
  %ref.tmp289 = alloca %class.btVector3, align 4
  %ref.tmp298 = alloca %class.btVector3, align 4
  %ref.tmp299 = alloca %class.btVector3, align 4
  %ref.tmp305 = alloca float, align 4
  %ref.tmp314 = alloca %class.btMatrix3x3, align 4
  %ref.tmp315 = alloca float, align 4
  %ref.tmp316 = alloca float, align 4
  %ref.tmp317 = alloca float, align 4
  %ref.tmp318 = alloca float, align 4
  %ref.tmp319 = alloca float, align 4
  %ref.tmp320 = alloca float, align 4
  %ref.tmp321 = alloca float, align 4
  %ref.tmp322 = alloca float, align 4
  %ref.tmp323 = alloca float, align 4
  %ref.tmp333 = alloca float, align 4
  %ref.tmp334 = alloca float, align 4
  %ref.tmp335 = alloca float, align 4
  %ref.tmp339 = alloca float, align 4
  %ref.tmp340 = alloca float, align 4
  %ref.tmp341 = alloca float, align 4
  %ref.tmp352 = alloca float, align 4
  %ref.tmp353 = alloca float, align 4
  %ref.tmp354 = alloca float, align 4
  %ref.tmp360 = alloca float, align 4
  %ref.tmp361 = alloca float, align 4
  %ref.tmp362 = alloca float, align 4
  %i368 = alloca i32, align 4
  %ref.tmp373 = alloca %class.btVector3, align 4
  %ref.tmp374 = alloca %class.btVector3, align 4
  %ref.tmp380 = alloca %class.btVector3, align 4
  %ref.tmp387 = alloca %class.btVector3, align 4
  %ref.tmp388 = alloca %class.btVector3, align 4
  %ref.tmp394 = alloca %class.btVector3, align 4
  %ref.tmp395 = alloca %class.btMatrix3x3, align 4
  %val = alloca float, align 4
  %parent433 = alloca i32, align 4
  %one_over_di = alloca float, align 4
  %TL = alloca %class.btMatrix3x3, align 4
  %ref.tmp440 = alloca %class.btMatrix3x3, align 4
  %ref.tmp441 = alloca %class.btVector3, align 4
  %TR = alloca %class.btMatrix3x3, align 4
  %ref.tmp446 = alloca %class.btMatrix3x3, align 4
  %ref.tmp447 = alloca %class.btVector3, align 4
  %BL = alloca %class.btMatrix3x3, align 4
  %ref.tmp452 = alloca %class.btMatrix3x3, align 4
  %ref.tmp453 = alloca %class.btVector3, align 4
  %r_cross = alloca %class.btMatrix3x3, align 4
  %ref.tmp457 = alloca float, align 4
  %ref.tmp458 = alloca float, align 4
  %ref.tmp474 = alloca float, align 4
  %ref.tmp475 = alloca float, align 4
  %ref.tmp482 = alloca float, align 4
  %ref.tmp494 = alloca float, align 4
  %ref.tmp495 = alloca %class.btMatrix3x3, align 4
  %ref.tmp496 = alloca %class.btMatrix3x3, align 4
  %ref.tmp497 = alloca %class.btMatrix3x3, align 4
  %ref.tmp500 = alloca %class.btMatrix3x3, align 4
  %ref.tmp501 = alloca %class.btMatrix3x3, align 4
  %ref.tmp507 = alloca %class.btMatrix3x3, align 4
  %ref.tmp508 = alloca %class.btMatrix3x3, align 4
  %ref.tmp509 = alloca %class.btMatrix3x3, align 4
  %ref.tmp517 = alloca %class.btMatrix3x3, align 4
  %ref.tmp518 = alloca %class.btMatrix3x3, align 4
  %ref.tmp519 = alloca %class.btMatrix3x3, align 4
  %ref.tmp522 = alloca %class.btMatrix3x3, align 4
  %ref.tmp523 = alloca %class.btMatrix3x3, align 4
  %ref.tmp524 = alloca %class.btMatrix3x3, align 4
  %ref.tmp525 = alloca %class.btMatrix3x3, align 4
  %ref.tmp526 = alloca %class.btMatrix3x3, align 4
  %ref.tmp527 = alloca %class.btMatrix3x3, align 4
  %ref.tmp528 = alloca %class.btMatrix3x3, align 4
  %in_top = alloca %class.btVector3, align 4
  %in_bottom = alloca %class.btVector3, align 4
  %out_top = alloca %class.btVector3, align 4
  %out_bottom = alloca %class.btVector3, align 4
  %Y_over_D = alloca float, align 4
  %ref.tmp540 = alloca %class.btVector3, align 4
  %ref.tmp541 = alloca %class.btVector3, align 4
  %ref.tmp542 = alloca %class.btVector3, align 4
  %ref.tmp545 = alloca %class.btVector3, align 4
  %ref.tmp549 = alloca %class.btVector3, align 4
  %ref.tmp553 = alloca %class.btVector3, align 4
  %ref.tmp555 = alloca %class.btVector3, align 4
  %ref.tmp556 = alloca %class.btVector3, align 4
  %ref.tmp557 = alloca %class.btVector3, align 4
  %ref.tmp560 = alloca %class.btVector3, align 4
  %ref.tmp564 = alloca %class.btVector3, align 4
  %ref.tmp565 = alloca %class.btMatrix3x3, align 4
  %ref.tmp569 = alloca %class.btVector3, align 4
  %ref.tmp587 = alloca %class.btVector3, align 4
  %ref.tmp588 = alloca float, align 4
  %ref.tmp589 = alloca float, align 4
  %ref.tmp590 = alloca float, align 4
  %ref.tmp603 = alloca %class.btMatrix3x3, align 4
  %rhs_top = alloca %class.btVector3, align 4
  %rhs_bot = alloca %class.btVector3, align 4
  %result = alloca [6 x float], align 16
  %i627 = alloca i32, align 4
  %i647 = alloca i32, align 4
  %parent652 = alloca i32, align 4
  %ref.tmp681 = alloca %class.btVector3, align 4
  %ref.tmp683 = alloca %class.btVector3, align 4
  %ref.tmp691 = alloca %class.btVector3, align 4
  %ref.tmp693 = alloca %class.btVector3, align 4
  %omegadot_out = alloca %class.btVector3, align 4
  %ref.tmp704 = alloca %class.btMatrix3x3, align 4
  %vdot_out = alloca %class.btVector3, align 4
  %ref.tmp716 = alloca %class.btMatrix3x3, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store float %dt, float* %dt.addr, align 4, !tbaa !8
  store %class.btAlignedObjectArray.4* %scratch_r, %class.btAlignedObjectArray.4** %scratch_r.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.8* %scratch_v, %class.btAlignedObjectArray.8** %scratch_v.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.12* %scratch_m, %class.btAlignedObjectArray.12** %scratch_m.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = bitcast i32* %num_links to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %this1)
  store i32 %call, i32* %num_links, align 4, !tbaa !6
  %1 = bitcast float* %DAMPING_K1_LINEAR to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %m_linearDamping = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 22
  %2 = load float, float* %m_linearDamping, align 4, !tbaa !35
  store float %2, float* %DAMPING_K1_LINEAR, align 4, !tbaa !8
  %3 = bitcast float* %DAMPING_K2_LINEAR to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %m_linearDamping2 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 22
  %4 = load float, float* %m_linearDamping2, align 4, !tbaa !35
  store float %4, float* %DAMPING_K2_LINEAR, align 4, !tbaa !8
  %5 = bitcast float* %DAMPING_K1_ANGULAR to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %m_angularDamping = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 23
  %6 = load float, float* %m_angularDamping, align 4, !tbaa !36
  store float %6, float* %DAMPING_K1_ANGULAR, align 4, !tbaa !8
  %7 = bitcast float* %DAMPING_K2_ANGULAR to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %m_angularDamping3 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 23
  %8 = load float, float* %m_angularDamping3, align 4, !tbaa !36
  store float %8, float* %DAMPING_K2_ANGULAR, align 4, !tbaa !8
  %9 = bitcast %class.btVector3* %base_vel to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #7
  call void @_ZNK11btMultiBody10getBaseVelEv(%class.btVector3* sret align 4 %base_vel, %class.btMultiBody* %this1)
  %10 = bitcast %class.btVector3* %base_omega to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #7
  call void @_ZNK11btMultiBody12getBaseOmegaEv(%class.btVector3* sret align 4 %base_omega, %class.btMultiBody* %this1)
  %11 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %scratch_r.addr, align 4, !tbaa !2
  %12 = load i32, i32* %num_links, align 4, !tbaa !6
  %mul = mul nsw i32 2, %12
  %add = add nsw i32 %mul, 6
  %13 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.4* %11, i32 %add, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %14 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #7
  %15 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %scratch_v.addr, align 4, !tbaa !2
  %16 = load i32, i32* %num_links, align 4, !tbaa !6
  %mul4 = mul nsw i32 8, %16
  %add5 = add nsw i32 %mul4, 6
  %17 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %17) #7
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp6)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.8* %15, i32 %add5, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp6)
  %18 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #7
  %19 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %scratch_m.addr, align 4, !tbaa !2
  %20 = load i32, i32* %num_links, align 4, !tbaa !6
  %mul8 = mul nsw i32 4, %20
  %add9 = add nsw i32 %mul8, 4
  %21 = bitcast %class.btMatrix3x3* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %21) #7
  %call11 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %ref.tmp10)
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E6resizeEiRKS0_(%class.btAlignedObjectArray.12* %19, i32 %add9, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp10)
  %22 = bitcast %class.btMatrix3x3* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %22) #7
  %23 = bitcast float** %r_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #7
  %24 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %scratch_r.addr, align 4, !tbaa !2
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %24, i32 0)
  store float* %call12, float** %r_ptr, align 4, !tbaa !2
  %25 = bitcast float** %output to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #7
  %26 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %scratch_r.addr, align 4, !tbaa !2
  %27 = load i32, i32* %num_links, align 4, !tbaa !6
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %26, i32 %27)
  store float* %call13, float** %output, align 4, !tbaa !2
  %28 = bitcast %class.btVector3** %v_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #7
  %29 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %scratch_v.addr, align 4, !tbaa !2
  %call14 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %29, i32 0)
  store %class.btVector3* %call14, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %30 = bitcast %class.btVector3** %vel_top_angular to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #7
  %31 = load %class.btVector3*, %class.btVector3** %v_ptr, align 4, !tbaa !2
  store %class.btVector3* %31, %class.btVector3** %vel_top_angular, align 4, !tbaa !2
  %32 = load i32, i32* %num_links, align 4, !tbaa !6
  %add15 = add nsw i32 %32, 1
  %33 = load %class.btVector3*, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds %class.btVector3, %class.btVector3* %33, i32 %add15
  store %class.btVector3* %add.ptr, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %34 = bitcast %class.btVector3** %vel_bottom_linear to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #7
  %35 = load %class.btVector3*, %class.btVector3** %v_ptr, align 4, !tbaa !2
  store %class.btVector3* %35, %class.btVector3** %vel_bottom_linear, align 4, !tbaa !2
  %36 = load i32, i32* %num_links, align 4, !tbaa !6
  %add16 = add nsw i32 %36, 1
  %37 = load %class.btVector3*, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %add.ptr17 = getelementptr inbounds %class.btVector3, %class.btVector3* %37, i32 %add16
  store %class.btVector3* %add.ptr17, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %38 = bitcast %class.btVector3** %zero_acc_top_angular to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #7
  %39 = load %class.btVector3*, %class.btVector3** %v_ptr, align 4, !tbaa !2
  store %class.btVector3* %39, %class.btVector3** %zero_acc_top_angular, align 4, !tbaa !2
  %40 = load i32, i32* %num_links, align 4, !tbaa !6
  %add18 = add nsw i32 %40, 1
  %41 = load %class.btVector3*, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %add.ptr19 = getelementptr inbounds %class.btVector3, %class.btVector3* %41, i32 %add18
  store %class.btVector3* %add.ptr19, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %42 = bitcast %class.btVector3** %zero_acc_bottom_linear to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #7
  %43 = load %class.btVector3*, %class.btVector3** %v_ptr, align 4, !tbaa !2
  store %class.btVector3* %43, %class.btVector3** %zero_acc_bottom_linear, align 4, !tbaa !2
  %44 = load i32, i32* %num_links, align 4, !tbaa !6
  %add20 = add nsw i32 %44, 1
  %45 = load %class.btVector3*, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %add.ptr21 = getelementptr inbounds %class.btVector3, %class.btVector3* %45, i32 %add20
  store %class.btVector3* %add.ptr21, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %46 = bitcast %class.btVector3** %coriolis_top_angular to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #7
  %47 = load %class.btVector3*, %class.btVector3** %v_ptr, align 4, !tbaa !2
  store %class.btVector3* %47, %class.btVector3** %coriolis_top_angular, align 4, !tbaa !2
  %48 = load i32, i32* %num_links, align 4, !tbaa !6
  %49 = load %class.btVector3*, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %add.ptr22 = getelementptr inbounds %class.btVector3, %class.btVector3* %49, i32 %48
  store %class.btVector3* %add.ptr22, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %50 = bitcast %class.btVector3** %coriolis_bottom_linear to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #7
  %51 = load %class.btVector3*, %class.btVector3** %v_ptr, align 4, !tbaa !2
  store %class.btVector3* %51, %class.btVector3** %coriolis_bottom_linear, align 4, !tbaa !2
  %52 = load i32, i32* %num_links, align 4, !tbaa !6
  %53 = load %class.btVector3*, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %add.ptr23 = getelementptr inbounds %class.btVector3, %class.btVector3* %53, i32 %52
  store %class.btVector3* %add.ptr23, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %54 = bitcast %class.btMatrix3x3** %inertia_top_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #7
  %55 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %scratch_m.addr, align 4, !tbaa !2
  %56 = load i32, i32* %num_links, align 4, !tbaa !6
  %add24 = add nsw i32 %56, 1
  %call25 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN20btAlignedObjectArrayI11btMatrix3x3EixEi(%class.btAlignedObjectArray.12* %55, i32 %add24)
  store %class.btMatrix3x3* %call25, %class.btMatrix3x3** %inertia_top_left, align 4, !tbaa !2
  %57 = bitcast %class.btMatrix3x3** %inertia_top_right to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %57) #7
  %58 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %scratch_m.addr, align 4, !tbaa !2
  %59 = load i32, i32* %num_links, align 4, !tbaa !6
  %mul26 = mul nsw i32 2, %59
  %add27 = add nsw i32 %mul26, 2
  %call28 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN20btAlignedObjectArrayI11btMatrix3x3EixEi(%class.btAlignedObjectArray.12* %58, i32 %add27)
  store %class.btMatrix3x3* %call28, %class.btMatrix3x3** %inertia_top_right, align 4, !tbaa !2
  %60 = bitcast %class.btMatrix3x3** %inertia_bottom_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #7
  %61 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %scratch_m.addr, align 4, !tbaa !2
  %62 = load i32, i32* %num_links, align 4, !tbaa !6
  %mul29 = mul nsw i32 3, %62
  %add30 = add nsw i32 %mul29, 3
  %call31 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN20btAlignedObjectArrayI11btMatrix3x3EixEi(%class.btAlignedObjectArray.12* %61, i32 %add30)
  store %class.btMatrix3x3* %call31, %class.btMatrix3x3** %inertia_bottom_left, align 4, !tbaa !2
  %63 = bitcast %class.btMatrix3x3** %rot_from_parent to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #7
  %matrix_buf = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 11
  %call32 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN20btAlignedObjectArrayI11btMatrix3x3EixEi(%class.btAlignedObjectArray.12* %matrix_buf, i32 0)
  store %class.btMatrix3x3* %call32, %class.btMatrix3x3** %rot_from_parent, align 4, !tbaa !2
  %64 = bitcast %class.btMatrix3x3** %rot_from_world to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #7
  %65 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %scratch_m.addr, align 4, !tbaa !2
  %call33 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN20btAlignedObjectArrayI11btMatrix3x3EixEi(%class.btAlignedObjectArray.12* %65, i32 0)
  store %class.btMatrix3x3* %call33, %class.btMatrix3x3** %rot_from_world, align 4, !tbaa !2
  %66 = bitcast %class.btVector3** %h_top to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #7
  %67 = load i32, i32* %num_links, align 4, !tbaa !6
  %cmp = icmp sgt i32 %67, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %vector_buf = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 10
  %call34 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vector_buf, i32 0)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %class.btVector3* [ %call34, %cond.true ], [ null, %cond.false ]
  store %class.btVector3* %cond, %class.btVector3** %h_top, align 4, !tbaa !2
  %68 = bitcast %class.btVector3** %h_bottom to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #7
  %69 = load i32, i32* %num_links, align 4, !tbaa !6
  %cmp35 = icmp sgt i32 %69, 0
  br i1 %cmp35, label %cond.true36, label %cond.false39

cond.true36:                                      ; preds = %cond.end
  %vector_buf37 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 10
  %70 = load i32, i32* %num_links, align 4, !tbaa !6
  %call38 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vector_buf37, i32 %70)
  br label %cond.end40

cond.false39:                                     ; preds = %cond.end
  br label %cond.end40

cond.end40:                                       ; preds = %cond.false39, %cond.true36
  %cond41 = phi %class.btVector3* [ %call38, %cond.true36 ], [ null, %cond.false39 ]
  store %class.btVector3* %cond41, %class.btVector3** %h_bottom, align 4, !tbaa !2
  %71 = bitcast %class.btVector3** %accel_top to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #7
  %72 = load %class.btVector3*, %class.btVector3** %v_ptr, align 4, !tbaa !2
  store %class.btVector3* %72, %class.btVector3** %accel_top, align 4, !tbaa !2
  %73 = load i32, i32* %num_links, align 4, !tbaa !6
  %add42 = add nsw i32 %73, 1
  %74 = load %class.btVector3*, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %add.ptr43 = getelementptr inbounds %class.btVector3, %class.btVector3* %74, i32 %add42
  store %class.btVector3* %add.ptr43, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %75 = bitcast %class.btVector3** %accel_bottom to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #7
  %76 = load %class.btVector3*, %class.btVector3** %v_ptr, align 4, !tbaa !2
  store %class.btVector3* %76, %class.btVector3** %accel_bottom, align 4, !tbaa !2
  %77 = load i32, i32* %num_links, align 4, !tbaa !6
  %add44 = add nsw i32 %77, 1
  %78 = load %class.btVector3*, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %add.ptr45 = getelementptr inbounds %class.btVector3, %class.btVector3* %78, i32 %add44
  store %class.btVector3* %add.ptr45, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %79 = bitcast float** %Y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #7
  %80 = load float*, float** %r_ptr, align 4, !tbaa !2
  store float* %80, float** %Y, align 4, !tbaa !2
  %81 = load i32, i32* %num_links, align 4, !tbaa !6
  %82 = load float*, float** %r_ptr, align 4, !tbaa !2
  %add.ptr46 = getelementptr inbounds float, float* %82, i32 %81
  store float* %add.ptr46, float** %r_ptr, align 4, !tbaa !2
  %83 = bitcast float** %D to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %83) #7
  %84 = load i32, i32* %num_links, align 4, !tbaa !6
  %cmp47 = icmp sgt i32 %84, 0
  br i1 %cmp47, label %cond.true48, label %cond.false51

cond.true48:                                      ; preds = %cond.end40
  %m_real_buf = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 9
  %85 = load i32, i32* %num_links, align 4, !tbaa !6
  %add49 = add nsw i32 6, %85
  %call50 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_real_buf, i32 %add49)
  br label %cond.end52

cond.false51:                                     ; preds = %cond.end40
  br label %cond.end52

cond.end52:                                       ; preds = %cond.false51, %cond.true48
  %cond53 = phi float* [ %call50, %cond.true48 ], [ null, %cond.false51 ]
  store float* %cond53, float** %D, align 4, !tbaa !2
  %86 = bitcast float** %joint_accel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %86) #7
  %87 = load float*, float** %output, align 4, !tbaa !2
  %add.ptr54 = getelementptr inbounds float, float* %87, i32 6
  store float* %add.ptr54, float** %joint_accel, align 4, !tbaa !2
  %88 = bitcast %class.btMatrix3x3* %ref.tmp55 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %88) #7
  %base_quat = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 2
  %call56 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* %ref.tmp55, %class.btQuaternion* nonnull align 4 dereferenceable(16) %base_quat)
  %89 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_parent, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %89, i32 0
  %call57 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %arrayidx, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp55)
  %90 = bitcast %class.btMatrix3x3* %ref.tmp55 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %90) #7
  %91 = bitcast %class.btVector3* %ref.tmp58 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %91) #7
  %92 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_parent, align 4, !tbaa !2
  %arrayidx59 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %92, i32 0
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp58, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx59, %class.btVector3* nonnull align 4 dereferenceable(16) %base_omega)
  %93 = load %class.btVector3*, %class.btVector3** %vel_top_angular, align 4, !tbaa !2
  %arrayidx60 = getelementptr inbounds %class.btVector3, %class.btVector3* %93, i32 0
  %94 = bitcast %class.btVector3* %arrayidx60 to i8*
  %95 = bitcast %class.btVector3* %ref.tmp58 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %94, i8* align 4 %95, i32 16, i1 false), !tbaa.struct !28
  %96 = bitcast %class.btVector3* %ref.tmp58 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %96) #7
  %97 = bitcast %class.btVector3* %ref.tmp61 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %97) #7
  %98 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_parent, align 4, !tbaa !2
  %arrayidx62 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %98, i32 0
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp61, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx62, %class.btVector3* nonnull align 4 dereferenceable(16) %base_vel)
  %99 = load %class.btVector3*, %class.btVector3** %vel_bottom_linear, align 4, !tbaa !2
  %arrayidx63 = getelementptr inbounds %class.btVector3, %class.btVector3* %99, i32 0
  %100 = bitcast %class.btVector3* %arrayidx63 to i8*
  %101 = bitcast %class.btVector3* %ref.tmp61 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %100, i8* align 4 %101, i32 16, i1 false), !tbaa.struct !28
  %102 = bitcast %class.btVector3* %ref.tmp61 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %102) #7
  %fixed_base = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 16
  %103 = load i8, i8* %fixed_base, align 4, !tbaa !31, !range !30
  %tobool = trunc i8 %103 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %cond.end52
  %104 = bitcast %class.btVector3* %ref.tmp64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %104) #7
  %105 = bitcast float* %ref.tmp65 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %105) #7
  store float 0.000000e+00, float* %ref.tmp65, align 4, !tbaa !8
  %106 = bitcast float* %ref.tmp66 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %106) #7
  store float 0.000000e+00, float* %ref.tmp66, align 4, !tbaa !8
  %107 = bitcast float* %ref.tmp67 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %107) #7
  store float 0.000000e+00, float* %ref.tmp67, align 4, !tbaa !8
  %call68 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp64, float* nonnull align 4 dereferenceable(4) %ref.tmp65, float* nonnull align 4 dereferenceable(4) %ref.tmp66, float* nonnull align 4 dereferenceable(4) %ref.tmp67)
  %108 = load %class.btVector3*, %class.btVector3** %zero_acc_bottom_linear, align 4, !tbaa !2
  %arrayidx69 = getelementptr inbounds %class.btVector3, %class.btVector3* %108, i32 0
  %109 = bitcast %class.btVector3* %arrayidx69 to i8*
  %110 = bitcast %class.btVector3* %ref.tmp64 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %109, i8* align 4 %110, i32 16, i1 false), !tbaa.struct !28
  %111 = load %class.btVector3*, %class.btVector3** %zero_acc_top_angular, align 4, !tbaa !2
  %arrayidx70 = getelementptr inbounds %class.btVector3, %class.btVector3* %111, i32 0
  %112 = bitcast %class.btVector3* %arrayidx70 to i8*
  %113 = bitcast %class.btVector3* %arrayidx69 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %112, i8* align 4 %113, i32 16, i1 false), !tbaa.struct !28
  %114 = bitcast float* %ref.tmp67 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #7
  %115 = bitcast float* %ref.tmp66 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #7
  %116 = bitcast float* %ref.tmp65 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #7
  %117 = bitcast %class.btVector3* %ref.tmp64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %117) #7
  br label %if.end105

if.else:                                          ; preds = %cond.end52
  %118 = bitcast %class.btVector3* %ref.tmp71 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %118) #7
  %119 = bitcast %class.btVector3* %ref.tmp72 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %119) #7
  %120 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_parent, align 4, !tbaa !2
  %arrayidx73 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %120, i32 0
  %121 = bitcast %class.btVector3* %ref.tmp74 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %121) #7
  %base_force = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 5
  %122 = bitcast %class.btVector3* %ref.tmp75 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %122) #7
  %123 = bitcast float* %ref.tmp76 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %123) #7
  %base_mass = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 3
  %124 = load float, float* %base_mass, align 4, !tbaa !27
  %125 = load float, float* %DAMPING_K1_LINEAR, align 4, !tbaa !8
  %126 = load float, float* %DAMPING_K2_LINEAR, align 4, !tbaa !8
  %call77 = call float @_ZNK9btVector34normEv(%class.btVector3* %base_vel)
  %mul78 = fmul float %126, %call77
  %add79 = fadd float %125, %mul78
  %mul80 = fmul float %124, %add79
  store float %mul80, float* %ref.tmp76, align 4, !tbaa !8
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp75, float* nonnull align 4 dereferenceable(4) %ref.tmp76, %class.btVector3* nonnull align 4 dereferenceable(16) %base_vel)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp74, %class.btVector3* nonnull align 4 dereferenceable(16) %base_force, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp75)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp72, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx73, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp74)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp71, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp72)
  %127 = load %class.btVector3*, %class.btVector3** %zero_acc_top_angular, align 4, !tbaa !2
  %arrayidx81 = getelementptr inbounds %class.btVector3, %class.btVector3* %127, i32 0
  %128 = bitcast %class.btVector3* %arrayidx81 to i8*
  %129 = bitcast %class.btVector3* %ref.tmp71 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %128, i8* align 4 %129, i32 16, i1 false), !tbaa.struct !28
  %130 = bitcast float* %ref.tmp76 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #7
  %131 = bitcast %class.btVector3* %ref.tmp75 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %131) #7
  %132 = bitcast %class.btVector3* %ref.tmp74 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %132) #7
  %133 = bitcast %class.btVector3* %ref.tmp72 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %133) #7
  %134 = bitcast %class.btVector3* %ref.tmp71 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %134) #7
  %135 = bitcast %class.btVector3* %ref.tmp82 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %135) #7
  %136 = bitcast %class.btVector3* %ref.tmp83 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %136) #7
  %137 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_parent, align 4, !tbaa !2
  %arrayidx84 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %137, i32 0
  %base_torque = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 6
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp83, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx84, %class.btVector3* nonnull align 4 dereferenceable(16) %base_torque)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp82, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp83)
  %138 = load %class.btVector3*, %class.btVector3** %zero_acc_bottom_linear, align 4, !tbaa !2
  %arrayidx85 = getelementptr inbounds %class.btVector3, %class.btVector3* %138, i32 0
  %139 = bitcast %class.btVector3* %arrayidx85 to i8*
  %140 = bitcast %class.btVector3* %ref.tmp82 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %139, i8* align 4 %140, i32 16, i1 false), !tbaa.struct !28
  %141 = bitcast %class.btVector3* %ref.tmp83 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %141) #7
  %142 = bitcast %class.btVector3* %ref.tmp82 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %142) #7
  %m_useGyroTerm = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 24
  %143 = load i8, i8* %m_useGyroTerm, align 4, !tbaa !37, !range !30
  %tobool86 = trunc i8 %143 to i1
  br i1 %tobool86, label %if.then87, label %if.end

if.then87:                                        ; preds = %if.else
  %144 = bitcast %class.btVector3* %ref.tmp88 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %144) #7
  %145 = load %class.btVector3*, %class.btVector3** %vel_top_angular, align 4, !tbaa !2
  %arrayidx89 = getelementptr inbounds %class.btVector3, %class.btVector3* %145, i32 0
  %146 = bitcast %class.btVector3* %ref.tmp90 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %146) #7
  %base_inertia = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 4
  %147 = load %class.btVector3*, %class.btVector3** %vel_top_angular, align 4, !tbaa !2
  %arrayidx91 = getelementptr inbounds %class.btVector3, %class.btVector3* %147, i32 0
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp90, %class.btVector3* nonnull align 4 dereferenceable(16) %base_inertia, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx91)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp88, %class.btVector3* %arrayidx89, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp90)
  %148 = load %class.btVector3*, %class.btVector3** %zero_acc_bottom_linear, align 4, !tbaa !2
  %arrayidx92 = getelementptr inbounds %class.btVector3, %class.btVector3* %148, i32 0
  %call93 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %arrayidx92, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp88)
  %149 = bitcast %class.btVector3* %ref.tmp90 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %149) #7
  %150 = bitcast %class.btVector3* %ref.tmp88 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %150) #7
  br label %if.end

if.end:                                           ; preds = %if.then87, %if.else
  %151 = bitcast %class.btVector3* %ref.tmp94 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %151) #7
  %152 = bitcast %class.btVector3* %ref.tmp95 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %152) #7
  %base_inertia96 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 4
  %153 = load %class.btVector3*, %class.btVector3** %vel_top_angular, align 4, !tbaa !2
  %arrayidx97 = getelementptr inbounds %class.btVector3, %class.btVector3* %153, i32 0
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp95, %class.btVector3* nonnull align 4 dereferenceable(16) %base_inertia96, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx97)
  %154 = bitcast float* %ref.tmp98 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %154) #7
  %155 = load float, float* %DAMPING_K1_ANGULAR, align 4, !tbaa !8
  %156 = load float, float* %DAMPING_K2_ANGULAR, align 4, !tbaa !8
  %157 = load %class.btVector3*, %class.btVector3** %vel_top_angular, align 4, !tbaa !2
  %arrayidx99 = getelementptr inbounds %class.btVector3, %class.btVector3* %157, i32 0
  %call100 = call float @_ZNK9btVector34normEv(%class.btVector3* %arrayidx99)
  %mul101 = fmul float %156, %call100
  %add102 = fadd float %155, %mul101
  store float %add102, float* %ref.tmp98, align 4, !tbaa !8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp94, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp95, float* nonnull align 4 dereferenceable(4) %ref.tmp98)
  %158 = load %class.btVector3*, %class.btVector3** %zero_acc_bottom_linear, align 4, !tbaa !2
  %arrayidx103 = getelementptr inbounds %class.btVector3, %class.btVector3* %158, i32 0
  %call104 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %arrayidx103, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp94)
  %159 = bitcast float* %ref.tmp98 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %159) #7
  %160 = bitcast %class.btVector3* %ref.tmp95 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %160) #7
  %161 = bitcast %class.btVector3* %ref.tmp94 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %161) #7
  br label %if.end105

if.end105:                                        ; preds = %if.end, %if.then
  %162 = bitcast %class.btMatrix3x3* %ref.tmp106 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %162) #7
  %163 = bitcast float* %ref.tmp107 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %163) #7
  store float 0.000000e+00, float* %ref.tmp107, align 4, !tbaa !8
  %164 = bitcast float* %ref.tmp108 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %164) #7
  store float 0.000000e+00, float* %ref.tmp108, align 4, !tbaa !8
  %165 = bitcast float* %ref.tmp109 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %165) #7
  store float 0.000000e+00, float* %ref.tmp109, align 4, !tbaa !8
  %166 = bitcast float* %ref.tmp110 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %166) #7
  store float 0.000000e+00, float* %ref.tmp110, align 4, !tbaa !8
  %167 = bitcast float* %ref.tmp111 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %167) #7
  store float 0.000000e+00, float* %ref.tmp111, align 4, !tbaa !8
  %168 = bitcast float* %ref.tmp112 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %168) #7
  store float 0.000000e+00, float* %ref.tmp112, align 4, !tbaa !8
  %169 = bitcast float* %ref.tmp113 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %169) #7
  store float 0.000000e+00, float* %ref.tmp113, align 4, !tbaa !8
  %170 = bitcast float* %ref.tmp114 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %170) #7
  store float 0.000000e+00, float* %ref.tmp114, align 4, !tbaa !8
  %171 = bitcast float* %ref.tmp115 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %171) #7
  store float 0.000000e+00, float* %ref.tmp115, align 4, !tbaa !8
  %call116 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %ref.tmp106, float* nonnull align 4 dereferenceable(4) %ref.tmp107, float* nonnull align 4 dereferenceable(4) %ref.tmp108, float* nonnull align 4 dereferenceable(4) %ref.tmp109, float* nonnull align 4 dereferenceable(4) %ref.tmp110, float* nonnull align 4 dereferenceable(4) %ref.tmp111, float* nonnull align 4 dereferenceable(4) %ref.tmp112, float* nonnull align 4 dereferenceable(4) %ref.tmp113, float* nonnull align 4 dereferenceable(4) %ref.tmp114, float* nonnull align 4 dereferenceable(4) %ref.tmp115)
  %172 = load %class.btMatrix3x3*, %class.btMatrix3x3** %inertia_top_left, align 4, !tbaa !2
  %arrayidx117 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %172, i32 0
  %call118 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %arrayidx117, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp106)
  %173 = bitcast float* %ref.tmp115 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %173) #7
  %174 = bitcast float* %ref.tmp114 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %174) #7
  %175 = bitcast float* %ref.tmp113 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %175) #7
  %176 = bitcast float* %ref.tmp112 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %176) #7
  %177 = bitcast float* %ref.tmp111 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %177) #7
  %178 = bitcast float* %ref.tmp110 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %178) #7
  %179 = bitcast float* %ref.tmp109 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %179) #7
  %180 = bitcast float* %ref.tmp108 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %180) #7
  %181 = bitcast float* %ref.tmp107 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %181) #7
  %182 = bitcast %class.btMatrix3x3* %ref.tmp106 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %182) #7
  %183 = load %class.btMatrix3x3*, %class.btMatrix3x3** %inertia_top_right, align 4, !tbaa !2
  %arrayidx119 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %183, i32 0
  %base_mass120 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 3
  %184 = bitcast float* %ref.tmp121 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %184) #7
  store float 0.000000e+00, float* %ref.tmp121, align 4, !tbaa !8
  %185 = bitcast float* %ref.tmp122 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %185) #7
  store float 0.000000e+00, float* %ref.tmp122, align 4, !tbaa !8
  %186 = bitcast float* %ref.tmp123 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %186) #7
  store float 0.000000e+00, float* %ref.tmp123, align 4, !tbaa !8
  %base_mass124 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 3
  %187 = bitcast float* %ref.tmp125 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %187) #7
  store float 0.000000e+00, float* %ref.tmp125, align 4, !tbaa !8
  %188 = bitcast float* %ref.tmp126 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %188) #7
  store float 0.000000e+00, float* %ref.tmp126, align 4, !tbaa !8
  %189 = bitcast float* %ref.tmp127 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %189) #7
  store float 0.000000e+00, float* %ref.tmp127, align 4, !tbaa !8
  %base_mass128 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 3
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %arrayidx119, float* nonnull align 4 dereferenceable(4) %base_mass120, float* nonnull align 4 dereferenceable(4) %ref.tmp121, float* nonnull align 4 dereferenceable(4) %ref.tmp122, float* nonnull align 4 dereferenceable(4) %ref.tmp123, float* nonnull align 4 dereferenceable(4) %base_mass124, float* nonnull align 4 dereferenceable(4) %ref.tmp125, float* nonnull align 4 dereferenceable(4) %ref.tmp126, float* nonnull align 4 dereferenceable(4) %ref.tmp127, float* nonnull align 4 dereferenceable(4) %base_mass128)
  %190 = bitcast float* %ref.tmp127 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %190) #7
  %191 = bitcast float* %ref.tmp126 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %191) #7
  %192 = bitcast float* %ref.tmp125 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %192) #7
  %193 = bitcast float* %ref.tmp123 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %193) #7
  %194 = bitcast float* %ref.tmp122 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %194) #7
  %195 = bitcast float* %ref.tmp121 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %195) #7
  %196 = load %class.btMatrix3x3*, %class.btMatrix3x3** %inertia_bottom_left, align 4, !tbaa !2
  %arrayidx129 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %196, i32 0
  %base_inertia130 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 4
  %call131 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %base_inertia130)
  %arrayidx132 = getelementptr inbounds float, float* %call131, i32 0
  %197 = bitcast float* %ref.tmp133 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %197) #7
  store float 0.000000e+00, float* %ref.tmp133, align 4, !tbaa !8
  %198 = bitcast float* %ref.tmp134 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %198) #7
  store float 0.000000e+00, float* %ref.tmp134, align 4, !tbaa !8
  %199 = bitcast float* %ref.tmp135 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %199) #7
  store float 0.000000e+00, float* %ref.tmp135, align 4, !tbaa !8
  %base_inertia136 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 4
  %call137 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %base_inertia136)
  %arrayidx138 = getelementptr inbounds float, float* %call137, i32 1
  %200 = bitcast float* %ref.tmp139 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %200) #7
  store float 0.000000e+00, float* %ref.tmp139, align 4, !tbaa !8
  %201 = bitcast float* %ref.tmp140 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %201) #7
  store float 0.000000e+00, float* %ref.tmp140, align 4, !tbaa !8
  %202 = bitcast float* %ref.tmp141 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %202) #7
  store float 0.000000e+00, float* %ref.tmp141, align 4, !tbaa !8
  %base_inertia142 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 4
  %call143 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %base_inertia142)
  %arrayidx144 = getelementptr inbounds float, float* %call143, i32 2
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %arrayidx129, float* nonnull align 4 dereferenceable(4) %arrayidx132, float* nonnull align 4 dereferenceable(4) %ref.tmp133, float* nonnull align 4 dereferenceable(4) %ref.tmp134, float* nonnull align 4 dereferenceable(4) %ref.tmp135, float* nonnull align 4 dereferenceable(4) %arrayidx138, float* nonnull align 4 dereferenceable(4) %ref.tmp139, float* nonnull align 4 dereferenceable(4) %ref.tmp140, float* nonnull align 4 dereferenceable(4) %ref.tmp141, float* nonnull align 4 dereferenceable(4) %arrayidx144)
  %203 = bitcast float* %ref.tmp141 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %203) #7
  %204 = bitcast float* %ref.tmp140 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %204) #7
  %205 = bitcast float* %ref.tmp139 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %205) #7
  %206 = bitcast float* %ref.tmp135 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %206) #7
  %207 = bitcast float* %ref.tmp134 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %207) #7
  %208 = bitcast float* %ref.tmp133 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %208) #7
  %209 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_parent, align 4, !tbaa !2
  %arrayidx145 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %209, i32 0
  %210 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_world, align 4, !tbaa !2
  %arrayidx146 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %210, i32 0
  %call147 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %arrayidx146, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx145)
  %211 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %211) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end105
  %212 = load i32, i32* %i, align 4, !tbaa !6
  %213 = load i32, i32* %num_links, align 4, !tbaa !6
  %cmp148 = icmp slt i32 %212, %213
  br i1 %cmp148, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %214 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %214) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %215 = bitcast i32* %parent to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %215) #7
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %216 = load i32, i32* %i, align 4, !tbaa !6
  %call149 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links, i32 %216)
  %parent150 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call149, i32 0, i32 3
  %217 = load i32, i32* %parent150, align 4, !tbaa !45
  store i32 %217, i32* %parent, align 4, !tbaa !6
  %218 = bitcast %class.btMatrix3x3* %ref.tmp151 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %218) #7
  %links152 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %219 = load i32, i32* %i, align 4, !tbaa !6
  %call153 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links152, i32 %219)
  %cached_rot_parent_to_this = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call153, i32 0, i32 10
  %call154 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* %ref.tmp151, %class.btQuaternion* nonnull align 4 dereferenceable(16) %cached_rot_parent_to_this)
  %220 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_parent, align 4, !tbaa !2
  %221 = load i32, i32* %i, align 4, !tbaa !6
  %add155 = add nsw i32 %221, 1
  %arrayidx156 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %220, i32 %add155
  %call157 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %arrayidx156, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp151)
  %222 = bitcast %class.btMatrix3x3* %ref.tmp151 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %222) #7
  %223 = bitcast %class.btMatrix3x3* %ref.tmp158 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %223) #7
  %224 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_parent, align 4, !tbaa !2
  %225 = load i32, i32* %i, align 4, !tbaa !6
  %add159 = add nsw i32 %225, 1
  %arrayidx160 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %224, i32 %add159
  %226 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_world, align 4, !tbaa !2
  %227 = load i32, i32* %parent, align 4, !tbaa !6
  %add161 = add nsw i32 %227, 1
  %arrayidx162 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %226, i32 %add161
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp158, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx160, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx162)
  %228 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_world, align 4, !tbaa !2
  %229 = load i32, i32* %i, align 4, !tbaa !6
  %add163 = add nsw i32 %229, 1
  %arrayidx164 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %228, i32 %add163
  %call165 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %arrayidx164, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp158)
  %230 = bitcast %class.btMatrix3x3* %ref.tmp158 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %230) #7
  %231 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_parent, align 4, !tbaa !2
  %232 = load i32, i32* %i, align 4, !tbaa !6
  %add166 = add nsw i32 %232, 1
  %arrayidx167 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %231, i32 %add166
  %links168 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %233 = load i32, i32* %i, align 4, !tbaa !6
  %call169 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links168, i32 %233)
  %cached_r_vector = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call169, i32 0, i32 11
  %234 = load %class.btVector3*, %class.btVector3** %vel_top_angular, align 4, !tbaa !2
  %235 = load i32, i32* %parent, align 4, !tbaa !6
  %add170 = add nsw i32 %235, 1
  %arrayidx171 = getelementptr inbounds %class.btVector3, %class.btVector3* %234, i32 %add170
  %236 = load %class.btVector3*, %class.btVector3** %vel_bottom_linear, align 4, !tbaa !2
  %237 = load i32, i32* %parent, align 4, !tbaa !6
  %add172 = add nsw i32 %237, 1
  %arrayidx173 = getelementptr inbounds %class.btVector3, %class.btVector3* %236, i32 %add172
  %238 = load %class.btVector3*, %class.btVector3** %vel_top_angular, align 4, !tbaa !2
  %239 = load i32, i32* %i, align 4, !tbaa !6
  %add174 = add nsw i32 %239, 1
  %arrayidx175 = getelementptr inbounds %class.btVector3, %class.btVector3* %238, i32 %add174
  %240 = load %class.btVector3*, %class.btVector3** %vel_bottom_linear, align 4, !tbaa !2
  %241 = load i32, i32* %i, align 4, !tbaa !6
  %add176 = add nsw i32 %241, 1
  %arrayidx177 = getelementptr inbounds %class.btVector3, %class.btVector3* %240, i32 %add176
  call void @_ZN12_GLOBAL__N_116SpatialTransformERK11btMatrix3x3RK9btVector3S5_S5_RS3_S6_(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx167, %class.btVector3* nonnull align 4 dereferenceable(16) %cached_r_vector, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx171, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx173, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx175, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx177)
  %242 = bitcast %class.btVector3* %ref.tmp178 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %242) #7
  %243 = bitcast %class.btVector3* %ref.tmp179 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %243) #7
  %244 = load %class.btVector3*, %class.btVector3** %vel_top_angular, align 4, !tbaa !2
  %245 = load i32, i32* %i, align 4, !tbaa !6
  %add180 = add nsw i32 %245, 1
  %arrayidx181 = getelementptr inbounds %class.btVector3, %class.btVector3* %244, i32 %add180
  %246 = bitcast %class.btVector3* %ref.tmp182 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %246) #7
  %247 = load %class.btVector3*, %class.btVector3** %vel_top_angular, align 4, !tbaa !2
  %248 = load i32, i32* %i, align 4, !tbaa !6
  %add183 = add nsw i32 %248, 1
  %arrayidx184 = getelementptr inbounds %class.btVector3, %class.btVector3* %247, i32 %add183
  %links185 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %249 = load i32, i32* %i, align 4, !tbaa !6
  %call186 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links185, i32 %249)
  %cached_r_vector187 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call186, i32 0, i32 11
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp182, %class.btVector3* %arrayidx184, %class.btVector3* nonnull align 4 dereferenceable(16) %cached_r_vector187)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp179, %class.btVector3* %arrayidx181, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp182)
  %250 = bitcast %class.btVector3* %ref.tmp188 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %250) #7
  %251 = bitcast %class.btVector3* %ref.tmp189 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %251) #7
  %252 = bitcast float* %ref.tmp190 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %252) #7
  store float 2.000000e+00, float* %ref.tmp190, align 4, !tbaa !8
  %253 = bitcast %class.btVector3* %ref.tmp191 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %253) #7
  %254 = load %class.btVector3*, %class.btVector3** %vel_top_angular, align 4, !tbaa !2
  %255 = load i32, i32* %i, align 4, !tbaa !6
  %add192 = add nsw i32 %255, 1
  %arrayidx193 = getelementptr inbounds %class.btVector3, %class.btVector3* %254, i32 %add192
  %links194 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %256 = load i32, i32* %i, align 4, !tbaa !6
  %call195 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links194, i32 %256)
  %axis_bottom = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call195, i32 0, i32 6
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp191, %class.btVector3* %arrayidx193, %class.btVector3* nonnull align 4 dereferenceable(16) %axis_bottom)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp189, float* nonnull align 4 dereferenceable(4) %ref.tmp190, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp191)
  %257 = bitcast float* %ref.tmp196 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %257) #7
  %258 = load i32, i32* %i, align 4, !tbaa !6
  %call197 = call float @_ZNK11btMultiBody11getJointVelEi(%class.btMultiBody* %this1, i32 %258)
  store float %call197, float* %ref.tmp196, align 4, !tbaa !8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp188, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp189, float* nonnull align 4 dereferenceable(4) %ref.tmp196)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp178, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp179, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp188)
  %259 = load %class.btVector3*, %class.btVector3** %coriolis_bottom_linear, align 4, !tbaa !2
  %260 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx198 = getelementptr inbounds %class.btVector3, %class.btVector3* %259, i32 %260
  %261 = bitcast %class.btVector3* %arrayidx198 to i8*
  %262 = bitcast %class.btVector3* %ref.tmp178 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %261, i8* align 4 %262, i32 16, i1 false), !tbaa.struct !28
  %263 = bitcast float* %ref.tmp196 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %263) #7
  %264 = bitcast %class.btVector3* %ref.tmp191 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %264) #7
  %265 = bitcast float* %ref.tmp190 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %265) #7
  %266 = bitcast %class.btVector3* %ref.tmp189 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %266) #7
  %267 = bitcast %class.btVector3* %ref.tmp188 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %267) #7
  %268 = bitcast %class.btVector3* %ref.tmp182 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %268) #7
  %269 = bitcast %class.btVector3* %ref.tmp179 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %269) #7
  %270 = bitcast %class.btVector3* %ref.tmp178 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %270) #7
  %links199 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %271 = load i32, i32* %i, align 4, !tbaa !6
  %call200 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links199, i32 %271)
  %is_revolute = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call200, i32 0, i32 9
  %272 = load i8, i8* %is_revolute, align 4, !tbaa !46, !range !30
  %tobool201 = trunc i8 %272 to i1
  br i1 %tobool201, label %if.then202, label %if.else226

if.then202:                                       ; preds = %for.body
  %273 = bitcast %class.btVector3* %ref.tmp203 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %273) #7
  %274 = bitcast %class.btVector3* %ref.tmp204 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %274) #7
  %275 = load %class.btVector3*, %class.btVector3** %vel_top_angular, align 4, !tbaa !2
  %276 = load i32, i32* %i, align 4, !tbaa !6
  %add205 = add nsw i32 %276, 1
  %arrayidx206 = getelementptr inbounds %class.btVector3, %class.btVector3* %275, i32 %add205
  %links207 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %277 = load i32, i32* %i, align 4, !tbaa !6
  %call208 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links207, i32 %277)
  %axis_top = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call208, i32 0, i32 5
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp204, %class.btVector3* %arrayidx206, %class.btVector3* nonnull align 4 dereferenceable(16) %axis_top)
  %278 = bitcast float* %ref.tmp209 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %278) #7
  %279 = load i32, i32* %i, align 4, !tbaa !6
  %call210 = call float @_ZNK11btMultiBody11getJointVelEi(%class.btMultiBody* %this1, i32 %279)
  store float %call210, float* %ref.tmp209, align 4, !tbaa !8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp203, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp204, float* nonnull align 4 dereferenceable(4) %ref.tmp209)
  %280 = load %class.btVector3*, %class.btVector3** %coriolis_top_angular, align 4, !tbaa !2
  %281 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx211 = getelementptr inbounds %class.btVector3, %class.btVector3* %280, i32 %281
  %282 = bitcast %class.btVector3* %arrayidx211 to i8*
  %283 = bitcast %class.btVector3* %ref.tmp203 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %282, i8* align 4 %283, i32 16, i1 false), !tbaa.struct !28
  %284 = bitcast float* %ref.tmp209 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %284) #7
  %285 = bitcast %class.btVector3* %ref.tmp204 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %285) #7
  %286 = bitcast %class.btVector3* %ref.tmp203 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %286) #7
  %287 = bitcast %class.btVector3* %ref.tmp212 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %287) #7
  %288 = bitcast float* %ref.tmp213 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %288) #7
  %289 = load i32, i32* %i, align 4, !tbaa !6
  %call214 = call float @_ZNK11btMultiBody11getJointVelEi(%class.btMultiBody* %this1, i32 %289)
  %290 = load i32, i32* %i, align 4, !tbaa !6
  %call215 = call float @_ZNK11btMultiBody11getJointVelEi(%class.btMultiBody* %this1, i32 %290)
  %mul216 = fmul float %call214, %call215
  store float %mul216, float* %ref.tmp213, align 4, !tbaa !8
  %291 = bitcast %class.btVector3* %ref.tmp217 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %291) #7
  %links218 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %292 = load i32, i32* %i, align 4, !tbaa !6
  %call219 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links218, i32 %292)
  %axis_top220 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call219, i32 0, i32 5
  %links221 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %293 = load i32, i32* %i, align 4, !tbaa !6
  %call222 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links221, i32 %293)
  %axis_bottom223 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call222, i32 0, i32 6
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp217, %class.btVector3* %axis_top220, %class.btVector3* nonnull align 4 dereferenceable(16) %axis_bottom223)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp212, float* nonnull align 4 dereferenceable(4) %ref.tmp213, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp217)
  %294 = load %class.btVector3*, %class.btVector3** %coriolis_bottom_linear, align 4, !tbaa !2
  %295 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx224 = getelementptr inbounds %class.btVector3, %class.btVector3* %294, i32 %295
  %call225 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %arrayidx224, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp212)
  %296 = bitcast %class.btVector3* %ref.tmp217 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %296) #7
  %297 = bitcast float* %ref.tmp213 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %297) #7
  %298 = bitcast %class.btVector3* %ref.tmp212 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %298) #7
  br label %if.end233

if.else226:                                       ; preds = %for.body
  %299 = bitcast %class.btVector3* %ref.tmp227 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %299) #7
  %300 = bitcast float* %ref.tmp228 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %300) #7
  store float 0.000000e+00, float* %ref.tmp228, align 4, !tbaa !8
  %301 = bitcast float* %ref.tmp229 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %301) #7
  store float 0.000000e+00, float* %ref.tmp229, align 4, !tbaa !8
  %302 = bitcast float* %ref.tmp230 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %302) #7
  store float 0.000000e+00, float* %ref.tmp230, align 4, !tbaa !8
  %call231 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp227, float* nonnull align 4 dereferenceable(4) %ref.tmp228, float* nonnull align 4 dereferenceable(4) %ref.tmp229, float* nonnull align 4 dereferenceable(4) %ref.tmp230)
  %303 = load %class.btVector3*, %class.btVector3** %coriolis_top_angular, align 4, !tbaa !2
  %304 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx232 = getelementptr inbounds %class.btVector3, %class.btVector3* %303, i32 %304
  %305 = bitcast %class.btVector3* %arrayidx232 to i8*
  %306 = bitcast %class.btVector3* %ref.tmp227 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %305, i8* align 4 %306, i32 16, i1 false), !tbaa.struct !28
  %307 = bitcast float* %ref.tmp230 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %307) #7
  %308 = bitcast float* %ref.tmp229 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %308) #7
  %309 = bitcast float* %ref.tmp228 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %309) #7
  %310 = bitcast %class.btVector3* %ref.tmp227 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %310) #7
  br label %if.end233

if.end233:                                        ; preds = %if.else226, %if.then202
  %311 = bitcast %class.btVector3* %ref.tmp234 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %311) #7
  %312 = bitcast float* %ref.tmp235 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %312) #7
  %313 = load i32, i32* %i, align 4, !tbaa !6
  %call236 = call float @_ZNK11btMultiBody11getJointVelEi(%class.btMultiBody* %this1, i32 %313)
  store float %call236, float* %ref.tmp235, align 4, !tbaa !8
  %links237 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %314 = load i32, i32* %i, align 4, !tbaa !6
  %call238 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links237, i32 %314)
  %axis_top239 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call238, i32 0, i32 5
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp234, float* nonnull align 4 dereferenceable(4) %ref.tmp235, %class.btVector3* nonnull align 4 dereferenceable(16) %axis_top239)
  %315 = load %class.btVector3*, %class.btVector3** %vel_top_angular, align 4, !tbaa !2
  %316 = load i32, i32* %i, align 4, !tbaa !6
  %add240 = add nsw i32 %316, 1
  %arrayidx241 = getelementptr inbounds %class.btVector3, %class.btVector3* %315, i32 %add240
  %call242 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %arrayidx241, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp234)
  %317 = bitcast float* %ref.tmp235 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %317) #7
  %318 = bitcast %class.btVector3* %ref.tmp234 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %318) #7
  %319 = bitcast %class.btVector3* %ref.tmp243 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %319) #7
  %320 = bitcast float* %ref.tmp244 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %320) #7
  %321 = load i32, i32* %i, align 4, !tbaa !6
  %call245 = call float @_ZNK11btMultiBody11getJointVelEi(%class.btMultiBody* %this1, i32 %321)
  store float %call245, float* %ref.tmp244, align 4, !tbaa !8
  %links246 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %322 = load i32, i32* %i, align 4, !tbaa !6
  %call247 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links246, i32 %322)
  %axis_bottom248 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call247, i32 0, i32 6
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp243, float* nonnull align 4 dereferenceable(4) %ref.tmp244, %class.btVector3* nonnull align 4 dereferenceable(16) %axis_bottom248)
  %323 = load %class.btVector3*, %class.btVector3** %vel_bottom_linear, align 4, !tbaa !2
  %324 = load i32, i32* %i, align 4, !tbaa !6
  %add249 = add nsw i32 %324, 1
  %arrayidx250 = getelementptr inbounds %class.btVector3, %class.btVector3* %323, i32 %add249
  %call251 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %arrayidx250, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp243)
  %325 = bitcast float* %ref.tmp244 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %325) #7
  %326 = bitcast %class.btVector3* %ref.tmp243 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %326) #7
  %327 = bitcast %class.btVector3* %ref.tmp252 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %327) #7
  %328 = bitcast %class.btVector3* %ref.tmp253 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %328) #7
  %329 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_world, align 4, !tbaa !2
  %330 = load i32, i32* %i, align 4, !tbaa !6
  %add254 = add nsw i32 %330, 1
  %arrayidx255 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %329, i32 %add254
  %links256 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %331 = load i32, i32* %i, align 4, !tbaa !6
  %call257 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links256, i32 %331)
  %applied_force = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call257, i32 0, i32 12
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp253, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx255, %class.btVector3* nonnull align 4 dereferenceable(16) %applied_force)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp252, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp253)
  %332 = load %class.btVector3*, %class.btVector3** %zero_acc_top_angular, align 4, !tbaa !2
  %333 = load i32, i32* %i, align 4, !tbaa !6
  %add258 = add nsw i32 %333, 1
  %arrayidx259 = getelementptr inbounds %class.btVector3, %class.btVector3* %332, i32 %add258
  %334 = bitcast %class.btVector3* %arrayidx259 to i8*
  %335 = bitcast %class.btVector3* %ref.tmp252 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %334, i8* align 4 %335, i32 16, i1 false), !tbaa.struct !28
  %336 = bitcast %class.btVector3* %ref.tmp253 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %336) #7
  %337 = bitcast %class.btVector3* %ref.tmp252 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %337) #7
  %338 = bitcast %class.btVector3* %ref.tmp260 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %338) #7
  %339 = bitcast float* %ref.tmp261 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %339) #7
  %links262 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %340 = load i32, i32* %i, align 4, !tbaa !6
  %call263 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links262, i32 %340)
  %mass = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call263, i32 0, i32 1
  %341 = load float, float* %mass, align 4, !tbaa !44
  %342 = load float, float* %DAMPING_K1_LINEAR, align 4, !tbaa !8
  %343 = load float, float* %DAMPING_K2_LINEAR, align 4, !tbaa !8
  %344 = load %class.btVector3*, %class.btVector3** %vel_bottom_linear, align 4, !tbaa !2
  %345 = load i32, i32* %i, align 4, !tbaa !6
  %add264 = add nsw i32 %345, 1
  %arrayidx265 = getelementptr inbounds %class.btVector3, %class.btVector3* %344, i32 %add264
  %call266 = call float @_ZNK9btVector34normEv(%class.btVector3* %arrayidx265)
  %mul267 = fmul float %343, %call266
  %add268 = fadd float %342, %mul267
  %mul269 = fmul float %341, %add268
  store float %mul269, float* %ref.tmp261, align 4, !tbaa !8
  %346 = load %class.btVector3*, %class.btVector3** %vel_bottom_linear, align 4, !tbaa !2
  %347 = load i32, i32* %i, align 4, !tbaa !6
  %add270 = add nsw i32 %347, 1
  %arrayidx271 = getelementptr inbounds %class.btVector3, %class.btVector3* %346, i32 %add270
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp260, float* nonnull align 4 dereferenceable(4) %ref.tmp261, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx271)
  %348 = load %class.btVector3*, %class.btVector3** %zero_acc_top_angular, align 4, !tbaa !2
  %349 = load i32, i32* %i, align 4, !tbaa !6
  %add272 = add nsw i32 %349, 1
  %arrayidx273 = getelementptr inbounds %class.btVector3, %class.btVector3* %348, i32 %add272
  %call274 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %arrayidx273, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp260)
  %350 = bitcast float* %ref.tmp261 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %350) #7
  %351 = bitcast %class.btVector3* %ref.tmp260 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %351) #7
  %352 = bitcast %class.btVector3* %ref.tmp275 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %352) #7
  %353 = bitcast %class.btVector3* %ref.tmp276 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %353) #7
  %354 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_world, align 4, !tbaa !2
  %355 = load i32, i32* %i, align 4, !tbaa !6
  %add277 = add nsw i32 %355, 1
  %arrayidx278 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %354, i32 %add277
  %links279 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %356 = load i32, i32* %i, align 4, !tbaa !6
  %call280 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links279, i32 %356)
  %applied_torque = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call280, i32 0, i32 13
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp276, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx278, %class.btVector3* nonnull align 4 dereferenceable(16) %applied_torque)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp275, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp276)
  %357 = load %class.btVector3*, %class.btVector3** %zero_acc_bottom_linear, align 4, !tbaa !2
  %358 = load i32, i32* %i, align 4, !tbaa !6
  %add281 = add nsw i32 %358, 1
  %arrayidx282 = getelementptr inbounds %class.btVector3, %class.btVector3* %357, i32 %add281
  %359 = bitcast %class.btVector3* %arrayidx282 to i8*
  %360 = bitcast %class.btVector3* %ref.tmp275 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %359, i8* align 4 %360, i32 16, i1 false), !tbaa.struct !28
  %361 = bitcast %class.btVector3* %ref.tmp276 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %361) #7
  %362 = bitcast %class.btVector3* %ref.tmp275 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %362) #7
  %m_useGyroTerm283 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 24
  %363 = load i8, i8* %m_useGyroTerm283, align 4, !tbaa !37, !range !30
  %tobool284 = trunc i8 %363 to i1
  br i1 %tobool284, label %if.then285, label %if.end297

if.then285:                                       ; preds = %if.end233
  %364 = bitcast %class.btVector3* %ref.tmp286 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %364) #7
  %365 = load %class.btVector3*, %class.btVector3** %vel_top_angular, align 4, !tbaa !2
  %366 = load i32, i32* %i, align 4, !tbaa !6
  %add287 = add nsw i32 %366, 1
  %arrayidx288 = getelementptr inbounds %class.btVector3, %class.btVector3* %365, i32 %add287
  %367 = bitcast %class.btVector3* %ref.tmp289 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %367) #7
  %links290 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %368 = load i32, i32* %i, align 4, !tbaa !6
  %call291 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links290, i32 %368)
  %inertia = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call291, i32 0, i32 2
  %369 = load %class.btVector3*, %class.btVector3** %vel_top_angular, align 4, !tbaa !2
  %370 = load i32, i32* %i, align 4, !tbaa !6
  %add292 = add nsw i32 %370, 1
  %arrayidx293 = getelementptr inbounds %class.btVector3, %class.btVector3* %369, i32 %add292
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp289, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx293)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp286, %class.btVector3* %arrayidx288, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp289)
  %371 = load %class.btVector3*, %class.btVector3** %zero_acc_bottom_linear, align 4, !tbaa !2
  %372 = load i32, i32* %i, align 4, !tbaa !6
  %add294 = add nsw i32 %372, 1
  %arrayidx295 = getelementptr inbounds %class.btVector3, %class.btVector3* %371, i32 %add294
  %call296 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %arrayidx295, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp286)
  %373 = bitcast %class.btVector3* %ref.tmp289 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %373) #7
  %374 = bitcast %class.btVector3* %ref.tmp286 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %374) #7
  br label %if.end297

if.end297:                                        ; preds = %if.then285, %if.end233
  %375 = bitcast %class.btVector3* %ref.tmp298 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %375) #7
  %376 = bitcast %class.btVector3* %ref.tmp299 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %376) #7
  %links300 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %377 = load i32, i32* %i, align 4, !tbaa !6
  %call301 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links300, i32 %377)
  %inertia302 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call301, i32 0, i32 2
  %378 = load %class.btVector3*, %class.btVector3** %vel_top_angular, align 4, !tbaa !2
  %379 = load i32, i32* %i, align 4, !tbaa !6
  %add303 = add nsw i32 %379, 1
  %arrayidx304 = getelementptr inbounds %class.btVector3, %class.btVector3* %378, i32 %add303
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp299, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia302, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx304)
  %380 = bitcast float* %ref.tmp305 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %380) #7
  %381 = load float, float* %DAMPING_K1_ANGULAR, align 4, !tbaa !8
  %382 = load float, float* %DAMPING_K2_ANGULAR, align 4, !tbaa !8
  %383 = load %class.btVector3*, %class.btVector3** %vel_top_angular, align 4, !tbaa !2
  %384 = load i32, i32* %i, align 4, !tbaa !6
  %add306 = add nsw i32 %384, 1
  %arrayidx307 = getelementptr inbounds %class.btVector3, %class.btVector3* %383, i32 %add306
  %call308 = call float @_ZNK9btVector34normEv(%class.btVector3* %arrayidx307)
  %mul309 = fmul float %382, %call308
  %add310 = fadd float %381, %mul309
  store float %add310, float* %ref.tmp305, align 4, !tbaa !8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp298, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp299, float* nonnull align 4 dereferenceable(4) %ref.tmp305)
  %385 = load %class.btVector3*, %class.btVector3** %zero_acc_bottom_linear, align 4, !tbaa !2
  %386 = load i32, i32* %i, align 4, !tbaa !6
  %add311 = add nsw i32 %386, 1
  %arrayidx312 = getelementptr inbounds %class.btVector3, %class.btVector3* %385, i32 %add311
  %call313 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %arrayidx312, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp298)
  %387 = bitcast float* %ref.tmp305 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %387) #7
  %388 = bitcast %class.btVector3* %ref.tmp299 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %388) #7
  %389 = bitcast %class.btVector3* %ref.tmp298 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %389) #7
  %390 = bitcast %class.btMatrix3x3* %ref.tmp314 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %390) #7
  %391 = bitcast float* %ref.tmp315 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %391) #7
  store float 0.000000e+00, float* %ref.tmp315, align 4, !tbaa !8
  %392 = bitcast float* %ref.tmp316 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %392) #7
  store float 0.000000e+00, float* %ref.tmp316, align 4, !tbaa !8
  %393 = bitcast float* %ref.tmp317 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %393) #7
  store float 0.000000e+00, float* %ref.tmp317, align 4, !tbaa !8
  %394 = bitcast float* %ref.tmp318 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %394) #7
  store float 0.000000e+00, float* %ref.tmp318, align 4, !tbaa !8
  %395 = bitcast float* %ref.tmp319 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %395) #7
  store float 0.000000e+00, float* %ref.tmp319, align 4, !tbaa !8
  %396 = bitcast float* %ref.tmp320 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %396) #7
  store float 0.000000e+00, float* %ref.tmp320, align 4, !tbaa !8
  %397 = bitcast float* %ref.tmp321 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %397) #7
  store float 0.000000e+00, float* %ref.tmp321, align 4, !tbaa !8
  %398 = bitcast float* %ref.tmp322 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %398) #7
  store float 0.000000e+00, float* %ref.tmp322, align 4, !tbaa !8
  %399 = bitcast float* %ref.tmp323 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %399) #7
  store float 0.000000e+00, float* %ref.tmp323, align 4, !tbaa !8
  %call324 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %ref.tmp314, float* nonnull align 4 dereferenceable(4) %ref.tmp315, float* nonnull align 4 dereferenceable(4) %ref.tmp316, float* nonnull align 4 dereferenceable(4) %ref.tmp317, float* nonnull align 4 dereferenceable(4) %ref.tmp318, float* nonnull align 4 dereferenceable(4) %ref.tmp319, float* nonnull align 4 dereferenceable(4) %ref.tmp320, float* nonnull align 4 dereferenceable(4) %ref.tmp321, float* nonnull align 4 dereferenceable(4) %ref.tmp322, float* nonnull align 4 dereferenceable(4) %ref.tmp323)
  %400 = load %class.btMatrix3x3*, %class.btMatrix3x3** %inertia_top_left, align 4, !tbaa !2
  %401 = load i32, i32* %i, align 4, !tbaa !6
  %add325 = add nsw i32 %401, 1
  %arrayidx326 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %400, i32 %add325
  %call327 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %arrayidx326, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp314)
  %402 = bitcast float* %ref.tmp323 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %402) #7
  %403 = bitcast float* %ref.tmp322 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %403) #7
  %404 = bitcast float* %ref.tmp321 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %404) #7
  %405 = bitcast float* %ref.tmp320 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %405) #7
  %406 = bitcast float* %ref.tmp319 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %406) #7
  %407 = bitcast float* %ref.tmp318 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %407) #7
  %408 = bitcast float* %ref.tmp317 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %408) #7
  %409 = bitcast float* %ref.tmp316 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %409) #7
  %410 = bitcast float* %ref.tmp315 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %410) #7
  %411 = bitcast %class.btMatrix3x3* %ref.tmp314 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %411) #7
  %412 = load %class.btMatrix3x3*, %class.btMatrix3x3** %inertia_top_right, align 4, !tbaa !2
  %413 = load i32, i32* %i, align 4, !tbaa !6
  %add328 = add nsw i32 %413, 1
  %arrayidx329 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %412, i32 %add328
  %links330 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %414 = load i32, i32* %i, align 4, !tbaa !6
  %call331 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links330, i32 %414)
  %mass332 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call331, i32 0, i32 1
  %415 = bitcast float* %ref.tmp333 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %415) #7
  store float 0.000000e+00, float* %ref.tmp333, align 4, !tbaa !8
  %416 = bitcast float* %ref.tmp334 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %416) #7
  store float 0.000000e+00, float* %ref.tmp334, align 4, !tbaa !8
  %417 = bitcast float* %ref.tmp335 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %417) #7
  store float 0.000000e+00, float* %ref.tmp335, align 4, !tbaa !8
  %links336 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %418 = load i32, i32* %i, align 4, !tbaa !6
  %call337 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links336, i32 %418)
  %mass338 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call337, i32 0, i32 1
  %419 = bitcast float* %ref.tmp339 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %419) #7
  store float 0.000000e+00, float* %ref.tmp339, align 4, !tbaa !8
  %420 = bitcast float* %ref.tmp340 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %420) #7
  store float 0.000000e+00, float* %ref.tmp340, align 4, !tbaa !8
  %421 = bitcast float* %ref.tmp341 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %421) #7
  store float 0.000000e+00, float* %ref.tmp341, align 4, !tbaa !8
  %links342 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %422 = load i32, i32* %i, align 4, !tbaa !6
  %call343 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links342, i32 %422)
  %mass344 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call343, i32 0, i32 1
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %arrayidx329, float* nonnull align 4 dereferenceable(4) %mass332, float* nonnull align 4 dereferenceable(4) %ref.tmp333, float* nonnull align 4 dereferenceable(4) %ref.tmp334, float* nonnull align 4 dereferenceable(4) %ref.tmp335, float* nonnull align 4 dereferenceable(4) %mass338, float* nonnull align 4 dereferenceable(4) %ref.tmp339, float* nonnull align 4 dereferenceable(4) %ref.tmp340, float* nonnull align 4 dereferenceable(4) %ref.tmp341, float* nonnull align 4 dereferenceable(4) %mass344)
  %423 = bitcast float* %ref.tmp341 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %423) #7
  %424 = bitcast float* %ref.tmp340 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %424) #7
  %425 = bitcast float* %ref.tmp339 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %425) #7
  %426 = bitcast float* %ref.tmp335 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %426) #7
  %427 = bitcast float* %ref.tmp334 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %427) #7
  %428 = bitcast float* %ref.tmp333 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %428) #7
  %429 = load %class.btMatrix3x3*, %class.btMatrix3x3** %inertia_bottom_left, align 4, !tbaa !2
  %430 = load i32, i32* %i, align 4, !tbaa !6
  %add345 = add nsw i32 %430, 1
  %arrayidx346 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %429, i32 %add345
  %links347 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %431 = load i32, i32* %i, align 4, !tbaa !6
  %call348 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links347, i32 %431)
  %inertia349 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call348, i32 0, i32 2
  %call350 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %inertia349)
  %arrayidx351 = getelementptr inbounds float, float* %call350, i32 0
  %432 = bitcast float* %ref.tmp352 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %432) #7
  store float 0.000000e+00, float* %ref.tmp352, align 4, !tbaa !8
  %433 = bitcast float* %ref.tmp353 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %433) #7
  store float 0.000000e+00, float* %ref.tmp353, align 4, !tbaa !8
  %434 = bitcast float* %ref.tmp354 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %434) #7
  store float 0.000000e+00, float* %ref.tmp354, align 4, !tbaa !8
  %links355 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %435 = load i32, i32* %i, align 4, !tbaa !6
  %call356 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links355, i32 %435)
  %inertia357 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call356, i32 0, i32 2
  %call358 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %inertia357)
  %arrayidx359 = getelementptr inbounds float, float* %call358, i32 1
  %436 = bitcast float* %ref.tmp360 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %436) #7
  store float 0.000000e+00, float* %ref.tmp360, align 4, !tbaa !8
  %437 = bitcast float* %ref.tmp361 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %437) #7
  store float 0.000000e+00, float* %ref.tmp361, align 4, !tbaa !8
  %438 = bitcast float* %ref.tmp362 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %438) #7
  store float 0.000000e+00, float* %ref.tmp362, align 4, !tbaa !8
  %links363 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %439 = load i32, i32* %i, align 4, !tbaa !6
  %call364 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links363, i32 %439)
  %inertia365 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call364, i32 0, i32 2
  %call366 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %inertia365)
  %arrayidx367 = getelementptr inbounds float, float* %call366, i32 2
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %arrayidx346, float* nonnull align 4 dereferenceable(4) %arrayidx351, float* nonnull align 4 dereferenceable(4) %ref.tmp352, float* nonnull align 4 dereferenceable(4) %ref.tmp353, float* nonnull align 4 dereferenceable(4) %ref.tmp354, float* nonnull align 4 dereferenceable(4) %arrayidx359, float* nonnull align 4 dereferenceable(4) %ref.tmp360, float* nonnull align 4 dereferenceable(4) %ref.tmp361, float* nonnull align 4 dereferenceable(4) %ref.tmp362, float* nonnull align 4 dereferenceable(4) %arrayidx367)
  %440 = bitcast float* %ref.tmp362 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %440) #7
  %441 = bitcast float* %ref.tmp361 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %441) #7
  %442 = bitcast float* %ref.tmp360 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %442) #7
  %443 = bitcast float* %ref.tmp354 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %443) #7
  %444 = bitcast float* %ref.tmp353 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %444) #7
  %445 = bitcast float* %ref.tmp352 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %445) #7
  %446 = bitcast i32* %parent to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %446) #7
  br label %for.inc

for.inc:                                          ; preds = %if.end297
  %447 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %447, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %448 = bitcast i32* %i368 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %448) #7
  %449 = load i32, i32* %num_links, align 4, !tbaa !6
  %sub = sub nsw i32 %449, 1
  store i32 %sub, i32* %i368, align 4, !tbaa !6
  br label %for.cond369

for.cond369:                                      ; preds = %for.inc582, %for.end
  %450 = load i32, i32* %i368, align 4, !tbaa !6
  %cmp370 = icmp sge i32 %450, 0
  br i1 %cmp370, label %for.body372, label %for.cond.cleanup371

for.cond.cleanup371:                              ; preds = %for.cond369
  %451 = bitcast i32* %i368 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %451) #7
  br label %for.end583

for.body372:                                      ; preds = %for.cond369
  %452 = bitcast %class.btVector3* %ref.tmp373 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %452) #7
  %453 = bitcast %class.btVector3* %ref.tmp374 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %453) #7
  %454 = load %class.btMatrix3x3*, %class.btMatrix3x3** %inertia_top_left, align 4, !tbaa !2
  %455 = load i32, i32* %i368, align 4, !tbaa !6
  %add375 = add nsw i32 %455, 1
  %arrayidx376 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %454, i32 %add375
  %links377 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %456 = load i32, i32* %i368, align 4, !tbaa !6
  %call378 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links377, i32 %456)
  %axis_top379 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call378, i32 0, i32 5
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp374, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx376, %class.btVector3* nonnull align 4 dereferenceable(16) %axis_top379)
  %457 = bitcast %class.btVector3* %ref.tmp380 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %457) #7
  %458 = load %class.btMatrix3x3*, %class.btMatrix3x3** %inertia_top_right, align 4, !tbaa !2
  %459 = load i32, i32* %i368, align 4, !tbaa !6
  %add381 = add nsw i32 %459, 1
  %arrayidx382 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %458, i32 %add381
  %links383 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %460 = load i32, i32* %i368, align 4, !tbaa !6
  %call384 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links383, i32 %460)
  %axis_bottom385 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call384, i32 0, i32 6
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp380, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx382, %class.btVector3* nonnull align 4 dereferenceable(16) %axis_bottom385)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp373, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp374, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp380)
  %461 = load %class.btVector3*, %class.btVector3** %h_top, align 4, !tbaa !2
  %462 = load i32, i32* %i368, align 4, !tbaa !6
  %arrayidx386 = getelementptr inbounds %class.btVector3, %class.btVector3* %461, i32 %462
  %463 = bitcast %class.btVector3* %arrayidx386 to i8*
  %464 = bitcast %class.btVector3* %ref.tmp373 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %463, i8* align 4 %464, i32 16, i1 false), !tbaa.struct !28
  %465 = bitcast %class.btVector3* %ref.tmp380 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %465) #7
  %466 = bitcast %class.btVector3* %ref.tmp374 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %466) #7
  %467 = bitcast %class.btVector3* %ref.tmp373 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %467) #7
  %468 = bitcast %class.btVector3* %ref.tmp387 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %468) #7
  %469 = bitcast %class.btVector3* %ref.tmp388 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %469) #7
  %470 = load %class.btMatrix3x3*, %class.btMatrix3x3** %inertia_bottom_left, align 4, !tbaa !2
  %471 = load i32, i32* %i368, align 4, !tbaa !6
  %add389 = add nsw i32 %471, 1
  %arrayidx390 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %470, i32 %add389
  %links391 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %472 = load i32, i32* %i368, align 4, !tbaa !6
  %call392 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links391, i32 %472)
  %axis_top393 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call392, i32 0, i32 5
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp388, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx390, %class.btVector3* nonnull align 4 dereferenceable(16) %axis_top393)
  %473 = bitcast %class.btVector3* %ref.tmp394 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %473) #7
  %474 = bitcast %class.btMatrix3x3* %ref.tmp395 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %474) #7
  %475 = load %class.btMatrix3x3*, %class.btMatrix3x3** %inertia_top_left, align 4, !tbaa !2
  %476 = load i32, i32* %i368, align 4, !tbaa !6
  %add396 = add nsw i32 %476, 1
  %arrayidx397 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %475, i32 %add396
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp395, %class.btMatrix3x3* %arrayidx397)
  %links398 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %477 = load i32, i32* %i368, align 4, !tbaa !6
  %call399 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links398, i32 %477)
  %axis_bottom400 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call399, i32 0, i32 6
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp394, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp395, %class.btVector3* nonnull align 4 dereferenceable(16) %axis_bottom400)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp387, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp388, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp394)
  %478 = load %class.btVector3*, %class.btVector3** %h_bottom, align 4, !tbaa !2
  %479 = load i32, i32* %i368, align 4, !tbaa !6
  %arrayidx401 = getelementptr inbounds %class.btVector3, %class.btVector3* %478, i32 %479
  %480 = bitcast %class.btVector3* %arrayidx401 to i8*
  %481 = bitcast %class.btVector3* %ref.tmp387 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %480, i8* align 4 %481, i32 16, i1 false), !tbaa.struct !28
  %482 = bitcast %class.btMatrix3x3* %ref.tmp395 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %482) #7
  %483 = bitcast %class.btVector3* %ref.tmp394 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %483) #7
  %484 = bitcast %class.btVector3* %ref.tmp388 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %484) #7
  %485 = bitcast %class.btVector3* %ref.tmp387 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %485) #7
  %486 = bitcast float* %val to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %486) #7
  %links402 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %487 = load i32, i32* %i368, align 4, !tbaa !6
  %call403 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links402, i32 %487)
  %axis_top404 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call403, i32 0, i32 5
  %links405 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %488 = load i32, i32* %i368, align 4, !tbaa !6
  %call406 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links405, i32 %488)
  %axis_bottom407 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call406, i32 0, i32 6
  %489 = load %class.btVector3*, %class.btVector3** %h_top, align 4, !tbaa !2
  %490 = load i32, i32* %i368, align 4, !tbaa !6
  %arrayidx408 = getelementptr inbounds %class.btVector3, %class.btVector3* %489, i32 %490
  %491 = load %class.btVector3*, %class.btVector3** %h_bottom, align 4, !tbaa !2
  %492 = load i32, i32* %i368, align 4, !tbaa !6
  %arrayidx409 = getelementptr inbounds %class.btVector3, %class.btVector3* %491, i32 %492
  %call410 = call float @_ZN12_GLOBAL__N_117SpatialDotProductERK9btVector3S2_S2_S2_(%class.btVector3* nonnull align 4 dereferenceable(16) %axis_top404, %class.btVector3* nonnull align 4 dereferenceable(16) %axis_bottom407, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx408, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx409)
  store float %call410, float* %val, align 4, !tbaa !8
  %493 = load float, float* %val, align 4, !tbaa !8
  %494 = load float*, float** %D, align 4, !tbaa !2
  %495 = load i32, i32* %i368, align 4, !tbaa !6
  %arrayidx411 = getelementptr inbounds float, float* %494, i32 %495
  store float %493, float* %arrayidx411, align 4, !tbaa !8
  %links412 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %496 = load i32, i32* %i368, align 4, !tbaa !6
  %call413 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links412, i32 %496)
  %joint_torque = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call413, i32 0, i32 14
  %497 = load float, float* %joint_torque, align 4, !tbaa !47
  %links414 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %498 = load i32, i32* %i368, align 4, !tbaa !6
  %call415 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links414, i32 %498)
  %axis_top416 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call415, i32 0, i32 5
  %links417 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %499 = load i32, i32* %i368, align 4, !tbaa !6
  %call418 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links417, i32 %499)
  %axis_bottom419 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call418, i32 0, i32 6
  %500 = load %class.btVector3*, %class.btVector3** %zero_acc_top_angular, align 4, !tbaa !2
  %501 = load i32, i32* %i368, align 4, !tbaa !6
  %add420 = add nsw i32 %501, 1
  %arrayidx421 = getelementptr inbounds %class.btVector3, %class.btVector3* %500, i32 %add420
  %502 = load %class.btVector3*, %class.btVector3** %zero_acc_bottom_linear, align 4, !tbaa !2
  %503 = load i32, i32* %i368, align 4, !tbaa !6
  %add422 = add nsw i32 %503, 1
  %arrayidx423 = getelementptr inbounds %class.btVector3, %class.btVector3* %502, i32 %add422
  %call424 = call float @_ZN12_GLOBAL__N_117SpatialDotProductERK9btVector3S2_S2_S2_(%class.btVector3* nonnull align 4 dereferenceable(16) %axis_top416, %class.btVector3* nonnull align 4 dereferenceable(16) %axis_bottom419, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx421, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx423)
  %sub425 = fsub float %497, %call424
  %504 = load %class.btVector3*, %class.btVector3** %h_top, align 4, !tbaa !2
  %505 = load i32, i32* %i368, align 4, !tbaa !6
  %arrayidx426 = getelementptr inbounds %class.btVector3, %class.btVector3* %504, i32 %505
  %506 = load %class.btVector3*, %class.btVector3** %h_bottom, align 4, !tbaa !2
  %507 = load i32, i32* %i368, align 4, !tbaa !6
  %arrayidx427 = getelementptr inbounds %class.btVector3, %class.btVector3* %506, i32 %507
  %508 = load %class.btVector3*, %class.btVector3** %coriolis_top_angular, align 4, !tbaa !2
  %509 = load i32, i32* %i368, align 4, !tbaa !6
  %arrayidx428 = getelementptr inbounds %class.btVector3, %class.btVector3* %508, i32 %509
  %510 = load %class.btVector3*, %class.btVector3** %coriolis_bottom_linear, align 4, !tbaa !2
  %511 = load i32, i32* %i368, align 4, !tbaa !6
  %arrayidx429 = getelementptr inbounds %class.btVector3, %class.btVector3* %510, i32 %511
  %call430 = call float @_ZN12_GLOBAL__N_117SpatialDotProductERK9btVector3S2_S2_S2_(%class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx426, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx427, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx428, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx429)
  %sub431 = fsub float %sub425, %call430
  %512 = load float*, float** %Y, align 4, !tbaa !2
  %513 = load i32, i32* %i368, align 4, !tbaa !6
  %arrayidx432 = getelementptr inbounds float, float* %512, i32 %513
  store float %sub431, float* %arrayidx432, align 4, !tbaa !8
  %514 = bitcast i32* %parent433 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %514) #7
  %links434 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %515 = load i32, i32* %i368, align 4, !tbaa !6
  %call435 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links434, i32 %515)
  %parent436 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call435, i32 0, i32 3
  %516 = load i32, i32* %parent436, align 4, !tbaa !45
  store i32 %516, i32* %parent433, align 4, !tbaa !6
  %517 = bitcast float* %one_over_di to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %517) #7
  %518 = load float*, float** %D, align 4, !tbaa !2
  %519 = load i32, i32* %i368, align 4, !tbaa !6
  %arrayidx437 = getelementptr inbounds float, float* %518, i32 %519
  %520 = load float, float* %arrayidx437, align 4, !tbaa !8
  %div = fdiv float 1.000000e+00, %520
  store float %div, float* %one_over_di, align 4, !tbaa !8
  %521 = bitcast %class.btMatrix3x3* %TL to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %521) #7
  %522 = load %class.btMatrix3x3*, %class.btMatrix3x3** %inertia_top_left, align 4, !tbaa !2
  %523 = load i32, i32* %i368, align 4, !tbaa !6
  %add438 = add nsw i32 %523, 1
  %arrayidx439 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %522, i32 %add438
  %524 = bitcast %class.btMatrix3x3* %ref.tmp440 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %524) #7
  %525 = bitcast %class.btVector3* %ref.tmp441 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %525) #7
  %526 = load %class.btVector3*, %class.btVector3** %h_top, align 4, !tbaa !2
  %527 = load i32, i32* %i368, align 4, !tbaa !6
  %arrayidx442 = getelementptr inbounds %class.btVector3, %class.btVector3* %526, i32 %527
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp441, float* nonnull align 4 dereferenceable(4) %one_over_di, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx442)
  %528 = load %class.btVector3*, %class.btVector3** %h_bottom, align 4, !tbaa !2
  %529 = load i32, i32* %i368, align 4, !tbaa !6
  %arrayidx443 = getelementptr inbounds %class.btVector3, %class.btVector3* %528, i32 %529
  call void @_Z18vecMulVecTransposeRK9btVector3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp440, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp441, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx443)
  call void @_ZmiRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %TL, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx439, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp440)
  %530 = bitcast %class.btVector3* %ref.tmp441 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %530) #7
  %531 = bitcast %class.btMatrix3x3* %ref.tmp440 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %531) #7
  %532 = bitcast %class.btMatrix3x3* %TR to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %532) #7
  %533 = load %class.btMatrix3x3*, %class.btMatrix3x3** %inertia_top_right, align 4, !tbaa !2
  %534 = load i32, i32* %i368, align 4, !tbaa !6
  %add444 = add nsw i32 %534, 1
  %arrayidx445 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %533, i32 %add444
  %535 = bitcast %class.btMatrix3x3* %ref.tmp446 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %535) #7
  %536 = bitcast %class.btVector3* %ref.tmp447 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %536) #7
  %537 = load %class.btVector3*, %class.btVector3** %h_top, align 4, !tbaa !2
  %538 = load i32, i32* %i368, align 4, !tbaa !6
  %arrayidx448 = getelementptr inbounds %class.btVector3, %class.btVector3* %537, i32 %538
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp447, float* nonnull align 4 dereferenceable(4) %one_over_di, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx448)
  %539 = load %class.btVector3*, %class.btVector3** %h_top, align 4, !tbaa !2
  %540 = load i32, i32* %i368, align 4, !tbaa !6
  %arrayidx449 = getelementptr inbounds %class.btVector3, %class.btVector3* %539, i32 %540
  call void @_Z18vecMulVecTransposeRK9btVector3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp446, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp447, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx449)
  call void @_ZmiRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %TR, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx445, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp446)
  %541 = bitcast %class.btVector3* %ref.tmp447 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %541) #7
  %542 = bitcast %class.btMatrix3x3* %ref.tmp446 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %542) #7
  %543 = bitcast %class.btMatrix3x3* %BL to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %543) #7
  %544 = load %class.btMatrix3x3*, %class.btMatrix3x3** %inertia_bottom_left, align 4, !tbaa !2
  %545 = load i32, i32* %i368, align 4, !tbaa !6
  %add450 = add nsw i32 %545, 1
  %arrayidx451 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %544, i32 %add450
  %546 = bitcast %class.btMatrix3x3* %ref.tmp452 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %546) #7
  %547 = bitcast %class.btVector3* %ref.tmp453 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %547) #7
  %548 = load %class.btVector3*, %class.btVector3** %h_bottom, align 4, !tbaa !2
  %549 = load i32, i32* %i368, align 4, !tbaa !6
  %arrayidx454 = getelementptr inbounds %class.btVector3, %class.btVector3* %548, i32 %549
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp453, float* nonnull align 4 dereferenceable(4) %one_over_di, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx454)
  %550 = load %class.btVector3*, %class.btVector3** %h_bottom, align 4, !tbaa !2
  %551 = load i32, i32* %i368, align 4, !tbaa !6
  %arrayidx455 = getelementptr inbounds %class.btVector3, %class.btVector3* %550, i32 %551
  call void @_Z18vecMulVecTransposeRK9btVector3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp452, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp453, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx455)
  call void @_ZmiRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %BL, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx451, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp452)
  %552 = bitcast %class.btVector3* %ref.tmp453 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %552) #7
  %553 = bitcast %class.btMatrix3x3* %ref.tmp452 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %553) #7
  %554 = bitcast %class.btMatrix3x3* %r_cross to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %554) #7
  %call456 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %r_cross)
  %555 = bitcast float* %ref.tmp457 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %555) #7
  store float 0.000000e+00, float* %ref.tmp457, align 4, !tbaa !8
  %556 = bitcast float* %ref.tmp458 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %556) #7
  %links459 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %557 = load i32, i32* %i368, align 4, !tbaa !6
  %call460 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links459, i32 %557)
  %cached_r_vector461 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call460, i32 0, i32 11
  %call462 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %cached_r_vector461)
  %arrayidx463 = getelementptr inbounds float, float* %call462, i32 2
  %558 = load float, float* %arrayidx463, align 4, !tbaa !8
  %fneg = fneg float %558
  store float %fneg, float* %ref.tmp458, align 4, !tbaa !8
  %links464 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %559 = load i32, i32* %i368, align 4, !tbaa !6
  %call465 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links464, i32 %559)
  %cached_r_vector466 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call465, i32 0, i32 11
  %call467 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %cached_r_vector466)
  %arrayidx468 = getelementptr inbounds float, float* %call467, i32 1
  %links469 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %560 = load i32, i32* %i368, align 4, !tbaa !6
  %call470 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links469, i32 %560)
  %cached_r_vector471 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call470, i32 0, i32 11
  %call472 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %cached_r_vector471)
  %arrayidx473 = getelementptr inbounds float, float* %call472, i32 2
  %561 = bitcast float* %ref.tmp474 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %561) #7
  store float 0.000000e+00, float* %ref.tmp474, align 4, !tbaa !8
  %562 = bitcast float* %ref.tmp475 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %562) #7
  %links476 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %563 = load i32, i32* %i368, align 4, !tbaa !6
  %call477 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links476, i32 %563)
  %cached_r_vector478 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call477, i32 0, i32 11
  %call479 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %cached_r_vector478)
  %arrayidx480 = getelementptr inbounds float, float* %call479, i32 0
  %564 = load float, float* %arrayidx480, align 4, !tbaa !8
  %fneg481 = fneg float %564
  store float %fneg481, float* %ref.tmp475, align 4, !tbaa !8
  %565 = bitcast float* %ref.tmp482 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %565) #7
  %links483 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %566 = load i32, i32* %i368, align 4, !tbaa !6
  %call484 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links483, i32 %566)
  %cached_r_vector485 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call484, i32 0, i32 11
  %call486 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %cached_r_vector485)
  %arrayidx487 = getelementptr inbounds float, float* %call486, i32 1
  %567 = load float, float* %arrayidx487, align 4, !tbaa !8
  %fneg488 = fneg float %567
  store float %fneg488, float* %ref.tmp482, align 4, !tbaa !8
  %links489 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %568 = load i32, i32* %i368, align 4, !tbaa !6
  %call490 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links489, i32 %568)
  %cached_r_vector491 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call490, i32 0, i32 11
  %call492 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %cached_r_vector491)
  %arrayidx493 = getelementptr inbounds float, float* %call492, i32 0
  %569 = bitcast float* %ref.tmp494 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %569) #7
  store float 0.000000e+00, float* %ref.tmp494, align 4, !tbaa !8
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %r_cross, float* nonnull align 4 dereferenceable(4) %ref.tmp457, float* nonnull align 4 dereferenceable(4) %ref.tmp458, float* nonnull align 4 dereferenceable(4) %arrayidx468, float* nonnull align 4 dereferenceable(4) %arrayidx473, float* nonnull align 4 dereferenceable(4) %ref.tmp474, float* nonnull align 4 dereferenceable(4) %ref.tmp475, float* nonnull align 4 dereferenceable(4) %ref.tmp482, float* nonnull align 4 dereferenceable(4) %arrayidx493, float* nonnull align 4 dereferenceable(4) %ref.tmp494)
  %570 = bitcast float* %ref.tmp494 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %570) #7
  %571 = bitcast float* %ref.tmp482 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %571) #7
  %572 = bitcast float* %ref.tmp475 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %572) #7
  %573 = bitcast float* %ref.tmp474 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %573) #7
  %574 = bitcast float* %ref.tmp458 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %574) #7
  %575 = bitcast float* %ref.tmp457 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %575) #7
  %576 = bitcast %class.btMatrix3x3* %ref.tmp495 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %576) #7
  %577 = bitcast %class.btMatrix3x3* %ref.tmp496 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %577) #7
  %578 = bitcast %class.btMatrix3x3* %ref.tmp497 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %578) #7
  %579 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_parent, align 4, !tbaa !2
  %580 = load i32, i32* %i368, align 4, !tbaa !6
  %add498 = add nsw i32 %580, 1
  %arrayidx499 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %579, i32 %add498
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp497, %class.btMatrix3x3* %arrayidx499)
  %581 = bitcast %class.btMatrix3x3* %ref.tmp500 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %581) #7
  %582 = bitcast %class.btMatrix3x3* %ref.tmp501 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %582) #7
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp501, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %TR, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %r_cross)
  call void @_ZmiRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp500, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %TL, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp501)
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp496, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp497, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp500)
  %583 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_parent, align 4, !tbaa !2
  %584 = load i32, i32* %i368, align 4, !tbaa !6
  %add502 = add nsw i32 %584, 1
  %arrayidx503 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %583, i32 %add502
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp495, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp496, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx503)
  %585 = load %class.btMatrix3x3*, %class.btMatrix3x3** %inertia_top_left, align 4, !tbaa !2
  %586 = load i32, i32* %parent433, align 4, !tbaa !6
  %add504 = add nsw i32 %586, 1
  %arrayidx505 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %585, i32 %add504
  %call506 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3pLERKS_(%class.btMatrix3x3* %arrayidx505, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp495)
  %587 = bitcast %class.btMatrix3x3* %ref.tmp501 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %587) #7
  %588 = bitcast %class.btMatrix3x3* %ref.tmp500 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %588) #7
  %589 = bitcast %class.btMatrix3x3* %ref.tmp497 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %589) #7
  %590 = bitcast %class.btMatrix3x3* %ref.tmp496 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %590) #7
  %591 = bitcast %class.btMatrix3x3* %ref.tmp495 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %591) #7
  %592 = bitcast %class.btMatrix3x3* %ref.tmp507 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %592) #7
  %593 = bitcast %class.btMatrix3x3* %ref.tmp508 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %593) #7
  %594 = bitcast %class.btMatrix3x3* %ref.tmp509 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %594) #7
  %595 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_parent, align 4, !tbaa !2
  %596 = load i32, i32* %i368, align 4, !tbaa !6
  %add510 = add nsw i32 %596, 1
  %arrayidx511 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %595, i32 %add510
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp509, %class.btMatrix3x3* %arrayidx511)
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp508, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp509, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %TR)
  %597 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_parent, align 4, !tbaa !2
  %598 = load i32, i32* %i368, align 4, !tbaa !6
  %add512 = add nsw i32 %598, 1
  %arrayidx513 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %597, i32 %add512
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp507, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp508, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx513)
  %599 = load %class.btMatrix3x3*, %class.btMatrix3x3** %inertia_top_right, align 4, !tbaa !2
  %600 = load i32, i32* %parent433, align 4, !tbaa !6
  %add514 = add nsw i32 %600, 1
  %arrayidx515 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %599, i32 %add514
  %call516 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3pLERKS_(%class.btMatrix3x3* %arrayidx515, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp507)
  %601 = bitcast %class.btMatrix3x3* %ref.tmp509 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %601) #7
  %602 = bitcast %class.btMatrix3x3* %ref.tmp508 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %602) #7
  %603 = bitcast %class.btMatrix3x3* %ref.tmp507 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %603) #7
  %604 = bitcast %class.btMatrix3x3* %ref.tmp517 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %604) #7
  %605 = bitcast %class.btMatrix3x3* %ref.tmp518 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %605) #7
  %606 = bitcast %class.btMatrix3x3* %ref.tmp519 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %606) #7
  %607 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_parent, align 4, !tbaa !2
  %608 = load i32, i32* %i368, align 4, !tbaa !6
  %add520 = add nsw i32 %608, 1
  %arrayidx521 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %607, i32 %add520
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp519, %class.btMatrix3x3* %arrayidx521)
  %609 = bitcast %class.btMatrix3x3* %ref.tmp522 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %609) #7
  %610 = bitcast %class.btMatrix3x3* %ref.tmp523 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %610) #7
  %611 = bitcast %class.btMatrix3x3* %ref.tmp524 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %611) #7
  %612 = bitcast %class.btMatrix3x3* %ref.tmp525 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %612) #7
  %613 = bitcast %class.btMatrix3x3* %ref.tmp526 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %613) #7
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp526, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %TR, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %r_cross)
  call void @_ZmiRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp525, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %TL, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp526)
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp524, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %r_cross, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp525)
  call void @_ZplRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp523, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp524, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %BL)
  %614 = bitcast %class.btMatrix3x3* %ref.tmp527 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %614) #7
  %615 = bitcast %class.btMatrix3x3* %ref.tmp528 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %615) #7
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp528, %class.btMatrix3x3* %TL)
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp527, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp528, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %r_cross)
  call void @_ZmiRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp522, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp523, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp527)
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp518, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp519, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp522)
  %616 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_parent, align 4, !tbaa !2
  %617 = load i32, i32* %i368, align 4, !tbaa !6
  %add529 = add nsw i32 %617, 1
  %arrayidx530 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %616, i32 %add529
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp517, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp518, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx530)
  %618 = load %class.btMatrix3x3*, %class.btMatrix3x3** %inertia_bottom_left, align 4, !tbaa !2
  %619 = load i32, i32* %parent433, align 4, !tbaa !6
  %add531 = add nsw i32 %619, 1
  %arrayidx532 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %618, i32 %add531
  %call533 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3pLERKS_(%class.btMatrix3x3* %arrayidx532, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp517)
  %620 = bitcast %class.btMatrix3x3* %ref.tmp528 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %620) #7
  %621 = bitcast %class.btMatrix3x3* %ref.tmp527 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %621) #7
  %622 = bitcast %class.btMatrix3x3* %ref.tmp526 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %622) #7
  %623 = bitcast %class.btMatrix3x3* %ref.tmp525 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %623) #7
  %624 = bitcast %class.btMatrix3x3* %ref.tmp524 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %624) #7
  %625 = bitcast %class.btMatrix3x3* %ref.tmp523 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %625) #7
  %626 = bitcast %class.btMatrix3x3* %ref.tmp522 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %626) #7
  %627 = bitcast %class.btMatrix3x3* %ref.tmp519 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %627) #7
  %628 = bitcast %class.btMatrix3x3* %ref.tmp518 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %628) #7
  %629 = bitcast %class.btMatrix3x3* %ref.tmp517 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %629) #7
  %630 = bitcast %class.btVector3* %in_top to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %630) #7
  %call534 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %in_top)
  %631 = bitcast %class.btVector3* %in_bottom to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %631) #7
  %call535 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %in_bottom)
  %632 = bitcast %class.btVector3* %out_top to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %632) #7
  %call536 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %out_top)
  %633 = bitcast %class.btVector3* %out_bottom to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %633) #7
  %call537 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %out_bottom)
  %634 = bitcast float* %Y_over_D to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %634) #7
  %635 = load float*, float** %Y, align 4, !tbaa !2
  %636 = load i32, i32* %i368, align 4, !tbaa !6
  %arrayidx538 = getelementptr inbounds float, float* %635, i32 %636
  %637 = load float, float* %arrayidx538, align 4, !tbaa !8
  %638 = load float, float* %one_over_di, align 4, !tbaa !8
  %mul539 = fmul float %637, %638
  store float %mul539, float* %Y_over_D, align 4, !tbaa !8
  %639 = bitcast %class.btVector3* %ref.tmp540 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %639) #7
  %640 = bitcast %class.btVector3* %ref.tmp541 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %640) #7
  %641 = bitcast %class.btVector3* %ref.tmp542 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %641) #7
  %642 = load %class.btVector3*, %class.btVector3** %zero_acc_top_angular, align 4, !tbaa !2
  %643 = load i32, i32* %i368, align 4, !tbaa !6
  %add543 = add nsw i32 %643, 1
  %arrayidx544 = getelementptr inbounds %class.btVector3, %class.btVector3* %642, i32 %add543
  %644 = bitcast %class.btVector3* %ref.tmp545 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %644) #7
  %645 = load %class.btMatrix3x3*, %class.btMatrix3x3** %inertia_top_left, align 4, !tbaa !2
  %646 = load i32, i32* %i368, align 4, !tbaa !6
  %add546 = add nsw i32 %646, 1
  %arrayidx547 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %645, i32 %add546
  %647 = load %class.btVector3*, %class.btVector3** %coriolis_top_angular, align 4, !tbaa !2
  %648 = load i32, i32* %i368, align 4, !tbaa !6
  %arrayidx548 = getelementptr inbounds %class.btVector3, %class.btVector3* %647, i32 %648
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp545, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx547, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx548)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp542, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx544, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp545)
  %649 = bitcast %class.btVector3* %ref.tmp549 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %649) #7
  %650 = load %class.btMatrix3x3*, %class.btMatrix3x3** %inertia_top_right, align 4, !tbaa !2
  %651 = load i32, i32* %i368, align 4, !tbaa !6
  %add550 = add nsw i32 %651, 1
  %arrayidx551 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %650, i32 %add550
  %652 = load %class.btVector3*, %class.btVector3** %coriolis_bottom_linear, align 4, !tbaa !2
  %653 = load i32, i32* %i368, align 4, !tbaa !6
  %arrayidx552 = getelementptr inbounds %class.btVector3, %class.btVector3* %652, i32 %653
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp549, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx551, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx552)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp541, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp542, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp549)
  %654 = bitcast %class.btVector3* %ref.tmp553 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %654) #7
  %655 = load %class.btVector3*, %class.btVector3** %h_top, align 4, !tbaa !2
  %656 = load i32, i32* %i368, align 4, !tbaa !6
  %arrayidx554 = getelementptr inbounds %class.btVector3, %class.btVector3* %655, i32 %656
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp553, float* nonnull align 4 dereferenceable(4) %Y_over_D, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx554)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp540, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp541, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp553)
  %657 = bitcast %class.btVector3* %in_top to i8*
  %658 = bitcast %class.btVector3* %ref.tmp540 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %657, i8* align 4 %658, i32 16, i1 false), !tbaa.struct !28
  %659 = bitcast %class.btVector3* %ref.tmp553 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %659) #7
  %660 = bitcast %class.btVector3* %ref.tmp549 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %660) #7
  %661 = bitcast %class.btVector3* %ref.tmp545 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %661) #7
  %662 = bitcast %class.btVector3* %ref.tmp542 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %662) #7
  %663 = bitcast %class.btVector3* %ref.tmp541 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %663) #7
  %664 = bitcast %class.btVector3* %ref.tmp540 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %664) #7
  %665 = bitcast %class.btVector3* %ref.tmp555 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %665) #7
  %666 = bitcast %class.btVector3* %ref.tmp556 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %666) #7
  %667 = bitcast %class.btVector3* %ref.tmp557 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %667) #7
  %668 = load %class.btVector3*, %class.btVector3** %zero_acc_bottom_linear, align 4, !tbaa !2
  %669 = load i32, i32* %i368, align 4, !tbaa !6
  %add558 = add nsw i32 %669, 1
  %arrayidx559 = getelementptr inbounds %class.btVector3, %class.btVector3* %668, i32 %add558
  %670 = bitcast %class.btVector3* %ref.tmp560 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %670) #7
  %671 = load %class.btMatrix3x3*, %class.btMatrix3x3** %inertia_bottom_left, align 4, !tbaa !2
  %672 = load i32, i32* %i368, align 4, !tbaa !6
  %add561 = add nsw i32 %672, 1
  %arrayidx562 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %671, i32 %add561
  %673 = load %class.btVector3*, %class.btVector3** %coriolis_top_angular, align 4, !tbaa !2
  %674 = load i32, i32* %i368, align 4, !tbaa !6
  %arrayidx563 = getelementptr inbounds %class.btVector3, %class.btVector3* %673, i32 %674
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp560, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx562, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx563)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp557, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx559, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp560)
  %675 = bitcast %class.btVector3* %ref.tmp564 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %675) #7
  %676 = bitcast %class.btMatrix3x3* %ref.tmp565 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %676) #7
  %677 = load %class.btMatrix3x3*, %class.btMatrix3x3** %inertia_top_left, align 4, !tbaa !2
  %678 = load i32, i32* %i368, align 4, !tbaa !6
  %add566 = add nsw i32 %678, 1
  %arrayidx567 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %677, i32 %add566
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp565, %class.btMatrix3x3* %arrayidx567)
  %679 = load %class.btVector3*, %class.btVector3** %coriolis_bottom_linear, align 4, !tbaa !2
  %680 = load i32, i32* %i368, align 4, !tbaa !6
  %arrayidx568 = getelementptr inbounds %class.btVector3, %class.btVector3* %679, i32 %680
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp564, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp565, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx568)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp556, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp557, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp564)
  %681 = bitcast %class.btVector3* %ref.tmp569 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %681) #7
  %682 = load %class.btVector3*, %class.btVector3** %h_bottom, align 4, !tbaa !2
  %683 = load i32, i32* %i368, align 4, !tbaa !6
  %arrayidx570 = getelementptr inbounds %class.btVector3, %class.btVector3* %682, i32 %683
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp569, float* nonnull align 4 dereferenceable(4) %Y_over_D, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx570)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp555, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp556, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp569)
  %684 = bitcast %class.btVector3* %in_bottom to i8*
  %685 = bitcast %class.btVector3* %ref.tmp555 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %684, i8* align 4 %685, i32 16, i1 false), !tbaa.struct !28
  %686 = bitcast %class.btVector3* %ref.tmp569 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %686) #7
  %687 = bitcast %class.btMatrix3x3* %ref.tmp565 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %687) #7
  %688 = bitcast %class.btVector3* %ref.tmp564 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %688) #7
  %689 = bitcast %class.btVector3* %ref.tmp560 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %689) #7
  %690 = bitcast %class.btVector3* %ref.tmp557 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %690) #7
  %691 = bitcast %class.btVector3* %ref.tmp556 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %691) #7
  %692 = bitcast %class.btVector3* %ref.tmp555 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %692) #7
  %693 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_parent, align 4, !tbaa !2
  %694 = load i32, i32* %i368, align 4, !tbaa !6
  %add571 = add nsw i32 %694, 1
  %arrayidx572 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %693, i32 %add571
  %links573 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %695 = load i32, i32* %i368, align 4, !tbaa !6
  %call574 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links573, i32 %695)
  %cached_r_vector575 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call574, i32 0, i32 11
  call void @_ZN12_GLOBAL__N_123InverseSpatialTransformERK11btMatrix3x3RK9btVector3S5_S5_RS3_S6_(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx572, %class.btVector3* nonnull align 4 dereferenceable(16) %cached_r_vector575, %class.btVector3* nonnull align 4 dereferenceable(16) %in_top, %class.btVector3* nonnull align 4 dereferenceable(16) %in_bottom, %class.btVector3* nonnull align 4 dereferenceable(16) %out_top, %class.btVector3* nonnull align 4 dereferenceable(16) %out_bottom)
  %696 = load %class.btVector3*, %class.btVector3** %zero_acc_top_angular, align 4, !tbaa !2
  %697 = load i32, i32* %parent433, align 4, !tbaa !6
  %add576 = add nsw i32 %697, 1
  %arrayidx577 = getelementptr inbounds %class.btVector3, %class.btVector3* %696, i32 %add576
  %call578 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %arrayidx577, %class.btVector3* nonnull align 4 dereferenceable(16) %out_top)
  %698 = load %class.btVector3*, %class.btVector3** %zero_acc_bottom_linear, align 4, !tbaa !2
  %699 = load i32, i32* %parent433, align 4, !tbaa !6
  %add579 = add nsw i32 %699, 1
  %arrayidx580 = getelementptr inbounds %class.btVector3, %class.btVector3* %698, i32 %add579
  %call581 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %arrayidx580, %class.btVector3* nonnull align 4 dereferenceable(16) %out_bottom)
  %700 = bitcast float* %Y_over_D to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %700) #7
  %701 = bitcast %class.btVector3* %out_bottom to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %701) #7
  %702 = bitcast %class.btVector3* %out_top to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %702) #7
  %703 = bitcast %class.btVector3* %in_bottom to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %703) #7
  %704 = bitcast %class.btVector3* %in_top to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %704) #7
  %705 = bitcast %class.btMatrix3x3* %r_cross to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %705) #7
  %706 = bitcast %class.btMatrix3x3* %BL to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %706) #7
  %707 = bitcast %class.btMatrix3x3* %TR to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %707) #7
  %708 = bitcast %class.btMatrix3x3* %TL to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %708) #7
  %709 = bitcast float* %one_over_di to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %709) #7
  %710 = bitcast i32* %parent433 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %710) #7
  %711 = bitcast float* %val to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %711) #7
  br label %for.inc582

for.inc582:                                       ; preds = %for.body372
  %712 = load i32, i32* %i368, align 4, !tbaa !6
  %dec = add nsw i32 %712, -1
  store i32 %dec, i32* %i368, align 4, !tbaa !6
  br label %for.cond369

for.end583:                                       ; preds = %for.cond.cleanup371
  %fixed_base584 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 16
  %713 = load i8, i8* %fixed_base584, align 4, !tbaa !31, !range !30
  %tobool585 = trunc i8 %713 to i1
  br i1 %tobool585, label %if.then586, label %if.else594

if.then586:                                       ; preds = %for.end583
  %714 = bitcast %class.btVector3* %ref.tmp587 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %714) #7
  %715 = bitcast float* %ref.tmp588 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %715) #7
  store float 0.000000e+00, float* %ref.tmp588, align 4, !tbaa !8
  %716 = bitcast float* %ref.tmp589 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %716) #7
  store float 0.000000e+00, float* %ref.tmp589, align 4, !tbaa !8
  %717 = bitcast float* %ref.tmp590 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %717) #7
  store float 0.000000e+00, float* %ref.tmp590, align 4, !tbaa !8
  %call591 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp587, float* nonnull align 4 dereferenceable(4) %ref.tmp588, float* nonnull align 4 dereferenceable(4) %ref.tmp589, float* nonnull align 4 dereferenceable(4) %ref.tmp590)
  %718 = load %class.btVector3*, %class.btVector3** %accel_bottom, align 4, !tbaa !2
  %arrayidx592 = getelementptr inbounds %class.btVector3, %class.btVector3* %718, i32 0
  %719 = bitcast %class.btVector3* %arrayidx592 to i8*
  %720 = bitcast %class.btVector3* %ref.tmp587 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %719, i8* align 4 %720, i32 16, i1 false), !tbaa.struct !28
  %721 = load %class.btVector3*, %class.btVector3** %accel_top, align 4, !tbaa !2
  %arrayidx593 = getelementptr inbounds %class.btVector3, %class.btVector3* %721, i32 0
  %722 = bitcast %class.btVector3* %arrayidx593 to i8*
  %723 = bitcast %class.btVector3* %arrayidx592 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %722, i8* align 4 %723, i32 16, i1 false), !tbaa.struct !28
  %724 = bitcast float* %ref.tmp590 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %724) #7
  %725 = bitcast float* %ref.tmp589 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %725) #7
  %726 = bitcast float* %ref.tmp588 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %726) #7
  %727 = bitcast %class.btVector3* %ref.tmp587 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %727) #7
  br label %if.end646

if.else594:                                       ; preds = %for.end583
  %728 = load i32, i32* %num_links, align 4, !tbaa !6
  %cmp595 = icmp sgt i32 %728, 0
  br i1 %cmp595, label %if.then596, label %if.end606

if.then596:                                       ; preds = %if.else594
  %729 = load %class.btMatrix3x3*, %class.btMatrix3x3** %inertia_top_left, align 4, !tbaa !2
  %arrayidx597 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %729, i32 0
  %cached_inertia_top_left = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 12
  %call598 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %cached_inertia_top_left, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx597)
  %730 = load %class.btMatrix3x3*, %class.btMatrix3x3** %inertia_top_right, align 4, !tbaa !2
  %arrayidx599 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %730, i32 0
  %cached_inertia_top_right = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 13
  %call600 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %cached_inertia_top_right, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx599)
  %731 = load %class.btMatrix3x3*, %class.btMatrix3x3** %inertia_bottom_left, align 4, !tbaa !2
  %arrayidx601 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %731, i32 0
  %cached_inertia_lower_left = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 14
  %call602 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %cached_inertia_lower_left, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx601)
  %732 = bitcast %class.btMatrix3x3* %ref.tmp603 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %732) #7
  %733 = load %class.btMatrix3x3*, %class.btMatrix3x3** %inertia_top_left, align 4, !tbaa !2
  %arrayidx604 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %733, i32 0
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp603, %class.btMatrix3x3* %arrayidx604)
  %cached_inertia_lower_right = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 15
  %call605 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %cached_inertia_lower_right, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp603)
  %734 = bitcast %class.btMatrix3x3* %ref.tmp603 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %734) #7
  br label %if.end606

if.end606:                                        ; preds = %if.then596, %if.else594
  %735 = bitcast %class.btVector3* %rhs_top to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %735) #7
  %736 = load %class.btVector3*, %class.btVector3** %zero_acc_top_angular, align 4, !tbaa !2
  %arrayidx607 = getelementptr inbounds %class.btVector3, %class.btVector3* %736, i32 0
  %call608 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx607)
  %arrayidx609 = getelementptr inbounds float, float* %call608, i32 0
  %737 = load %class.btVector3*, %class.btVector3** %zero_acc_top_angular, align 4, !tbaa !2
  %arrayidx610 = getelementptr inbounds %class.btVector3, %class.btVector3* %737, i32 0
  %call611 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx610)
  %arrayidx612 = getelementptr inbounds float, float* %call611, i32 1
  %738 = load %class.btVector3*, %class.btVector3** %zero_acc_top_angular, align 4, !tbaa !2
  %arrayidx613 = getelementptr inbounds %class.btVector3, %class.btVector3* %738, i32 0
  %call614 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx613)
  %arrayidx615 = getelementptr inbounds float, float* %call614, i32 2
  %call616 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %rhs_top, float* nonnull align 4 dereferenceable(4) %arrayidx609, float* nonnull align 4 dereferenceable(4) %arrayidx612, float* nonnull align 4 dereferenceable(4) %arrayidx615)
  %739 = bitcast %class.btVector3* %rhs_bot to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %739) #7
  %740 = load %class.btVector3*, %class.btVector3** %zero_acc_bottom_linear, align 4, !tbaa !2
  %arrayidx617 = getelementptr inbounds %class.btVector3, %class.btVector3* %740, i32 0
  %call618 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx617)
  %arrayidx619 = getelementptr inbounds float, float* %call618, i32 0
  %741 = load %class.btVector3*, %class.btVector3** %zero_acc_bottom_linear, align 4, !tbaa !2
  %arrayidx620 = getelementptr inbounds %class.btVector3, %class.btVector3* %741, i32 0
  %call621 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx620)
  %arrayidx622 = getelementptr inbounds float, float* %call621, i32 1
  %742 = load %class.btVector3*, %class.btVector3** %zero_acc_bottom_linear, align 4, !tbaa !2
  %arrayidx623 = getelementptr inbounds %class.btVector3, %class.btVector3* %742, i32 0
  %call624 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx623)
  %arrayidx625 = getelementptr inbounds float, float* %call624, i32 2
  %call626 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %rhs_bot, float* nonnull align 4 dereferenceable(4) %arrayidx619, float* nonnull align 4 dereferenceable(4) %arrayidx622, float* nonnull align 4 dereferenceable(4) %arrayidx625)
  %743 = bitcast [6 x float]* %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %743) #7
  %arraydecay = getelementptr inbounds [6 x float], [6 x float]* %result, i32 0, i32 0
  call void @_ZNK11btMultiBody12solveImatrixERK9btVector3S2_Pf(%class.btMultiBody* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %rhs_top, %class.btVector3* nonnull align 4 dereferenceable(16) %rhs_bot, float* %arraydecay)
  %744 = bitcast i32* %i627 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %744) #7
  store i32 0, i32* %i627, align 4, !tbaa !6
  br label %for.cond628

for.cond628:                                      ; preds = %for.inc643, %if.end606
  %745 = load i32, i32* %i627, align 4, !tbaa !6
  %cmp629 = icmp slt i32 %745, 3
  br i1 %cmp629, label %for.body631, label %for.cond.cleanup630

for.cond.cleanup630:                              ; preds = %for.cond628
  %746 = bitcast i32* %i627 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %746) #7
  br label %for.end645

for.body631:                                      ; preds = %for.cond628
  %747 = load i32, i32* %i627, align 4, !tbaa !6
  %arrayidx632 = getelementptr inbounds [6 x float], [6 x float]* %result, i32 0, i32 %747
  %748 = load float, float* %arrayidx632, align 4, !tbaa !8
  %fneg633 = fneg float %748
  %749 = load %class.btVector3*, %class.btVector3** %accel_top, align 4, !tbaa !2
  %arrayidx634 = getelementptr inbounds %class.btVector3, %class.btVector3* %749, i32 0
  %call635 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx634)
  %750 = load i32, i32* %i627, align 4, !tbaa !6
  %arrayidx636 = getelementptr inbounds float, float* %call635, i32 %750
  store float %fneg633, float* %arrayidx636, align 4, !tbaa !8
  %751 = load i32, i32* %i627, align 4, !tbaa !6
  %add637 = add nsw i32 %751, 3
  %arrayidx638 = getelementptr inbounds [6 x float], [6 x float]* %result, i32 0, i32 %add637
  %752 = load float, float* %arrayidx638, align 4, !tbaa !8
  %fneg639 = fneg float %752
  %753 = load %class.btVector3*, %class.btVector3** %accel_bottom, align 4, !tbaa !2
  %arrayidx640 = getelementptr inbounds %class.btVector3, %class.btVector3* %753, i32 0
  %call641 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx640)
  %754 = load i32, i32* %i627, align 4, !tbaa !6
  %arrayidx642 = getelementptr inbounds float, float* %call641, i32 %754
  store float %fneg639, float* %arrayidx642, align 4, !tbaa !8
  br label %for.inc643

for.inc643:                                       ; preds = %for.body631
  %755 = load i32, i32* %i627, align 4, !tbaa !6
  %inc644 = add nsw i32 %755, 1
  store i32 %inc644, i32* %i627, align 4, !tbaa !6
  br label %for.cond628

for.end645:                                       ; preds = %for.cond.cleanup630
  %756 = bitcast [6 x float]* %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %756) #7
  %757 = bitcast %class.btVector3* %rhs_bot to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %757) #7
  %758 = bitcast %class.btVector3* %rhs_top to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %758) #7
  br label %if.end646

if.end646:                                        ; preds = %for.end645, %if.then586
  %759 = bitcast i32* %i647 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %759) #7
  store i32 0, i32* %i647, align 4, !tbaa !6
  br label %for.cond648

for.cond648:                                      ; preds = %for.inc701, %if.end646
  %760 = load i32, i32* %i647, align 4, !tbaa !6
  %761 = load i32, i32* %num_links, align 4, !tbaa !6
  %cmp649 = icmp slt i32 %760, %761
  br i1 %cmp649, label %for.body651, label %for.cond.cleanup650

for.cond.cleanup650:                              ; preds = %for.cond648
  %762 = bitcast i32* %i647 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %762) #7
  br label %for.end703

for.body651:                                      ; preds = %for.cond648
  %763 = bitcast i32* %parent652 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %763) #7
  %links653 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %764 = load i32, i32* %i647, align 4, !tbaa !6
  %call654 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links653, i32 %764)
  %parent655 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call654, i32 0, i32 3
  %765 = load i32, i32* %parent655, align 4, !tbaa !45
  store i32 %765, i32* %parent652, align 4, !tbaa !6
  %766 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_parent, align 4, !tbaa !2
  %767 = load i32, i32* %i647, align 4, !tbaa !6
  %add656 = add nsw i32 %767, 1
  %arrayidx657 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %766, i32 %add656
  %links658 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %768 = load i32, i32* %i647, align 4, !tbaa !6
  %call659 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links658, i32 %768)
  %cached_r_vector660 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call659, i32 0, i32 11
  %769 = load %class.btVector3*, %class.btVector3** %accel_top, align 4, !tbaa !2
  %770 = load i32, i32* %parent652, align 4, !tbaa !6
  %add661 = add nsw i32 %770, 1
  %arrayidx662 = getelementptr inbounds %class.btVector3, %class.btVector3* %769, i32 %add661
  %771 = load %class.btVector3*, %class.btVector3** %accel_bottom, align 4, !tbaa !2
  %772 = load i32, i32* %parent652, align 4, !tbaa !6
  %add663 = add nsw i32 %772, 1
  %arrayidx664 = getelementptr inbounds %class.btVector3, %class.btVector3* %771, i32 %add663
  %773 = load %class.btVector3*, %class.btVector3** %accel_top, align 4, !tbaa !2
  %774 = load i32, i32* %i647, align 4, !tbaa !6
  %add665 = add nsw i32 %774, 1
  %arrayidx666 = getelementptr inbounds %class.btVector3, %class.btVector3* %773, i32 %add665
  %775 = load %class.btVector3*, %class.btVector3** %accel_bottom, align 4, !tbaa !2
  %776 = load i32, i32* %i647, align 4, !tbaa !6
  %add667 = add nsw i32 %776, 1
  %arrayidx668 = getelementptr inbounds %class.btVector3, %class.btVector3* %775, i32 %add667
  call void @_ZN12_GLOBAL__N_116SpatialTransformERK11btMatrix3x3RK9btVector3S5_S5_RS3_S6_(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx657, %class.btVector3* nonnull align 4 dereferenceable(16) %cached_r_vector660, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx662, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx664, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx666, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx668)
  %777 = load float*, float** %Y, align 4, !tbaa !2
  %778 = load i32, i32* %i647, align 4, !tbaa !6
  %arrayidx669 = getelementptr inbounds float, float* %777, i32 %778
  %779 = load float, float* %arrayidx669, align 4, !tbaa !8
  %780 = load %class.btVector3*, %class.btVector3** %h_top, align 4, !tbaa !2
  %781 = load i32, i32* %i647, align 4, !tbaa !6
  %arrayidx670 = getelementptr inbounds %class.btVector3, %class.btVector3* %780, i32 %781
  %782 = load %class.btVector3*, %class.btVector3** %h_bottom, align 4, !tbaa !2
  %783 = load i32, i32* %i647, align 4, !tbaa !6
  %arrayidx671 = getelementptr inbounds %class.btVector3, %class.btVector3* %782, i32 %783
  %784 = load %class.btVector3*, %class.btVector3** %accel_top, align 4, !tbaa !2
  %785 = load i32, i32* %i647, align 4, !tbaa !6
  %add672 = add nsw i32 %785, 1
  %arrayidx673 = getelementptr inbounds %class.btVector3, %class.btVector3* %784, i32 %add672
  %786 = load %class.btVector3*, %class.btVector3** %accel_bottom, align 4, !tbaa !2
  %787 = load i32, i32* %i647, align 4, !tbaa !6
  %add674 = add nsw i32 %787, 1
  %arrayidx675 = getelementptr inbounds %class.btVector3, %class.btVector3* %786, i32 %add674
  %call676 = call float @_ZN12_GLOBAL__N_117SpatialDotProductERK9btVector3S2_S2_S2_(%class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx670, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx671, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx673, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx675)
  %sub677 = fsub float %779, %call676
  %788 = load float*, float** %D, align 4, !tbaa !2
  %789 = load i32, i32* %i647, align 4, !tbaa !6
  %arrayidx678 = getelementptr inbounds float, float* %788, i32 %789
  %790 = load float, float* %arrayidx678, align 4, !tbaa !8
  %div679 = fdiv float %sub677, %790
  %791 = load float*, float** %joint_accel, align 4, !tbaa !2
  %792 = load i32, i32* %i647, align 4, !tbaa !6
  %arrayidx680 = getelementptr inbounds float, float* %791, i32 %792
  store float %div679, float* %arrayidx680, align 4, !tbaa !8
  %793 = bitcast %class.btVector3* %ref.tmp681 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %793) #7
  %794 = load %class.btVector3*, %class.btVector3** %coriolis_top_angular, align 4, !tbaa !2
  %795 = load i32, i32* %i647, align 4, !tbaa !6
  %arrayidx682 = getelementptr inbounds %class.btVector3, %class.btVector3* %794, i32 %795
  %796 = bitcast %class.btVector3* %ref.tmp683 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %796) #7
  %797 = load float*, float** %joint_accel, align 4, !tbaa !2
  %798 = load i32, i32* %i647, align 4, !tbaa !6
  %arrayidx684 = getelementptr inbounds float, float* %797, i32 %798
  %links685 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %799 = load i32, i32* %i647, align 4, !tbaa !6
  %call686 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links685, i32 %799)
  %axis_top687 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call686, i32 0, i32 5
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp683, float* nonnull align 4 dereferenceable(4) %arrayidx684, %class.btVector3* nonnull align 4 dereferenceable(16) %axis_top687)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp681, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx682, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp683)
  %800 = load %class.btVector3*, %class.btVector3** %accel_top, align 4, !tbaa !2
  %801 = load i32, i32* %i647, align 4, !tbaa !6
  %add688 = add nsw i32 %801, 1
  %arrayidx689 = getelementptr inbounds %class.btVector3, %class.btVector3* %800, i32 %add688
  %call690 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %arrayidx689, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp681)
  %802 = bitcast %class.btVector3* %ref.tmp683 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %802) #7
  %803 = bitcast %class.btVector3* %ref.tmp681 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %803) #7
  %804 = bitcast %class.btVector3* %ref.tmp691 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %804) #7
  %805 = load %class.btVector3*, %class.btVector3** %coriolis_bottom_linear, align 4, !tbaa !2
  %806 = load i32, i32* %i647, align 4, !tbaa !6
  %arrayidx692 = getelementptr inbounds %class.btVector3, %class.btVector3* %805, i32 %806
  %807 = bitcast %class.btVector3* %ref.tmp693 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %807) #7
  %808 = load float*, float** %joint_accel, align 4, !tbaa !2
  %809 = load i32, i32* %i647, align 4, !tbaa !6
  %arrayidx694 = getelementptr inbounds float, float* %808, i32 %809
  %links695 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %810 = load i32, i32* %i647, align 4, !tbaa !6
  %call696 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links695, i32 %810)
  %axis_bottom697 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call696, i32 0, i32 6
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp693, float* nonnull align 4 dereferenceable(4) %arrayidx694, %class.btVector3* nonnull align 4 dereferenceable(16) %axis_bottom697)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp691, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx692, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp693)
  %811 = load %class.btVector3*, %class.btVector3** %accel_bottom, align 4, !tbaa !2
  %812 = load i32, i32* %i647, align 4, !tbaa !6
  %add698 = add nsw i32 %812, 1
  %arrayidx699 = getelementptr inbounds %class.btVector3, %class.btVector3* %811, i32 %add698
  %call700 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %arrayidx699, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp691)
  %813 = bitcast %class.btVector3* %ref.tmp693 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %813) #7
  %814 = bitcast %class.btVector3* %ref.tmp691 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %814) #7
  %815 = bitcast i32* %parent652 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %815) #7
  br label %for.inc701

for.inc701:                                       ; preds = %for.body651
  %816 = load i32, i32* %i647, align 4, !tbaa !6
  %inc702 = add nsw i32 %816, 1
  store i32 %inc702, i32* %i647, align 4, !tbaa !6
  br label %for.cond648

for.end703:                                       ; preds = %for.cond.cleanup650
  %817 = bitcast %class.btVector3* %omegadot_out to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %817) #7
  %818 = bitcast %class.btMatrix3x3* %ref.tmp704 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %818) #7
  %819 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_parent, align 4, !tbaa !2
  %arrayidx705 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %819, i32 0
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp704, %class.btMatrix3x3* %arrayidx705)
  %820 = load %class.btVector3*, %class.btVector3** %accel_top, align 4, !tbaa !2
  %arrayidx706 = getelementptr inbounds %class.btVector3, %class.btVector3* %820, i32 0
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %omegadot_out, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp704, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx706)
  %821 = bitcast %class.btMatrix3x3* %ref.tmp704 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %821) #7
  %call707 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %omegadot_out)
  %arrayidx708 = getelementptr inbounds float, float* %call707, i32 0
  %822 = load float, float* %arrayidx708, align 4, !tbaa !8
  %823 = load float*, float** %output, align 4, !tbaa !2
  %arrayidx709 = getelementptr inbounds float, float* %823, i32 0
  store float %822, float* %arrayidx709, align 4, !tbaa !8
  %call710 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %omegadot_out)
  %arrayidx711 = getelementptr inbounds float, float* %call710, i32 1
  %824 = load float, float* %arrayidx711, align 4, !tbaa !8
  %825 = load float*, float** %output, align 4, !tbaa !2
  %arrayidx712 = getelementptr inbounds float, float* %825, i32 1
  store float %824, float* %arrayidx712, align 4, !tbaa !8
  %call713 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %omegadot_out)
  %arrayidx714 = getelementptr inbounds float, float* %call713, i32 2
  %826 = load float, float* %arrayidx714, align 4, !tbaa !8
  %827 = load float*, float** %output, align 4, !tbaa !2
  %arrayidx715 = getelementptr inbounds float, float* %827, i32 2
  store float %826, float* %arrayidx715, align 4, !tbaa !8
  %828 = bitcast %class.btVector3* %vdot_out to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %828) #7
  %829 = bitcast %class.btMatrix3x3* %ref.tmp716 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %829) #7
  %830 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_parent, align 4, !tbaa !2
  %arrayidx717 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %830, i32 0
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp716, %class.btMatrix3x3* %arrayidx717)
  %831 = load %class.btVector3*, %class.btVector3** %accel_bottom, align 4, !tbaa !2
  %arrayidx718 = getelementptr inbounds %class.btVector3, %class.btVector3* %831, i32 0
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %vdot_out, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp716, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx718)
  %832 = bitcast %class.btMatrix3x3* %ref.tmp716 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %832) #7
  %call719 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %vdot_out)
  %arrayidx720 = getelementptr inbounds float, float* %call719, i32 0
  %833 = load float, float* %arrayidx720, align 4, !tbaa !8
  %834 = load float*, float** %output, align 4, !tbaa !2
  %arrayidx721 = getelementptr inbounds float, float* %834, i32 3
  store float %833, float* %arrayidx721, align 4, !tbaa !8
  %call722 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %vdot_out)
  %arrayidx723 = getelementptr inbounds float, float* %call722, i32 1
  %835 = load float, float* %arrayidx723, align 4, !tbaa !8
  %836 = load float*, float** %output, align 4, !tbaa !2
  %arrayidx724 = getelementptr inbounds float, float* %836, i32 4
  store float %835, float* %arrayidx724, align 4, !tbaa !8
  %call725 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %vdot_out)
  %arrayidx726 = getelementptr inbounds float, float* %call725, i32 2
  %837 = load float, float* %arrayidx726, align 4, !tbaa !8
  %838 = load float*, float** %output, align 4, !tbaa !2
  %arrayidx727 = getelementptr inbounds float, float* %838, i32 5
  store float %837, float* %arrayidx727, align 4, !tbaa !8
  %839 = load float*, float** %output, align 4, !tbaa !2
  %840 = load float, float* %dt.addr, align 4, !tbaa !8
  call void @_ZN11btMultiBody13applyDeltaVeeEPKff(%class.btMultiBody* %this1, float* %839, float %840)
  %841 = bitcast %class.btVector3* %vdot_out to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %841) #7
  %842 = bitcast %class.btVector3* %omegadot_out to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %842) #7
  %843 = bitcast float** %joint_accel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %843) #7
  %844 = bitcast float** %D to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %844) #7
  %845 = bitcast float** %Y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %845) #7
  %846 = bitcast %class.btVector3** %accel_bottom to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %846) #7
  %847 = bitcast %class.btVector3** %accel_top to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %847) #7
  %848 = bitcast %class.btVector3** %h_bottom to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %848) #7
  %849 = bitcast %class.btVector3** %h_top to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %849) #7
  %850 = bitcast %class.btMatrix3x3** %rot_from_world to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %850) #7
  %851 = bitcast %class.btMatrix3x3** %rot_from_parent to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %851) #7
  %852 = bitcast %class.btMatrix3x3** %inertia_bottom_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %852) #7
  %853 = bitcast %class.btMatrix3x3** %inertia_top_right to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %853) #7
  %854 = bitcast %class.btMatrix3x3** %inertia_top_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %854) #7
  %855 = bitcast %class.btVector3** %coriolis_bottom_linear to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %855) #7
  %856 = bitcast %class.btVector3** %coriolis_top_angular to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %856) #7
  %857 = bitcast %class.btVector3** %zero_acc_bottom_linear to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %857) #7
  %858 = bitcast %class.btVector3** %zero_acc_top_angular to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %858) #7
  %859 = bitcast %class.btVector3** %vel_bottom_linear to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %859) #7
  %860 = bitcast %class.btVector3** %vel_top_angular to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %860) #7
  %861 = bitcast %class.btVector3** %v_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %861) #7
  %862 = bitcast float** %output to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %862) #7
  %863 = bitcast float** %r_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %863) #7
  %864 = bitcast %class.btVector3* %base_omega to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %864) #7
  %865 = bitcast %class.btVector3* %base_vel to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %865) #7
  %866 = bitcast float* %DAMPING_K2_ANGULAR to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %866) #7
  %867 = bitcast float* %DAMPING_K1_ANGULAR to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %867) #7
  %868 = bitcast float* %DAMPING_K2_LINEAR to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %868) #7
  %869 = bitcast float* %DAMPING_K1_LINEAR to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %869) #7
  %870 = bitcast i32* %num_links to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %870) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN20btAlignedObjectArrayI11btMatrix3x3EixEi(%class.btAlignedObjectArray.12* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m_data, align 4, !tbaa !52
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 %1
  ret %class.btMatrix3x3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !28
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !28
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !28
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %1, i32 0)
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call1, float* %ref.tmp, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 1)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !8
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 2)
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !8
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #7
  %10 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #7
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !8
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !8
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !8
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !8
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !8
  %fneg4 = fneg float %5
  store float %fneg4, float* %ref.tmp1, align 4, !tbaa !8
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %8 = load float, float* %arrayidx7, align 4, !tbaa !8
  %fneg8 = fneg float %8
  store float %fneg8, float* %ref.tmp5, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #7
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #7
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector34normEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  ret float %call
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #3 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !8
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !8
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !8
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !8
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !8
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #7
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #7
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  ret void
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #3 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %3, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4, !tbaa !8
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !8
  %7 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %9, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %8, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4, !tbaa !8
  %13 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %15, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %14, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4, !tbaa !8
  %16 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #7
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4, !tbaa !8
  %19 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #7
  %20 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %21 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %21, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %20, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4, !tbaa !8
  %22 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #7
  %23 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %24 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %24, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %23, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4, !tbaa !8
  %25 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #7
  %26 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %27 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %27, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %26, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4, !tbaa !8
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  %28 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #7
  %29 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #7
  %30 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #7
  %31 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #7
  %32 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #7
  %33 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #7
  %34 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #7
  %35 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #7
  %36 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #3 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !8
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !8
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !8
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !8
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !8
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #7
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #7
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #3 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

; Function Attrs: nounwind
define internal float @_ZN12_GLOBAL__N_117SpatialDotProductERK9btVector3S2_S2_S2_(%class.btVector3* nonnull align 4 dereferenceable(16) %a_top, %class.btVector3* nonnull align 4 dereferenceable(16) %a_bottom, %class.btVector3* nonnull align 4 dereferenceable(16) %b_top, %class.btVector3* nonnull align 4 dereferenceable(16) %b_bottom) #4 {
entry:
  %a_top.addr = alloca %class.btVector3*, align 4
  %a_bottom.addr = alloca %class.btVector3*, align 4
  %b_top.addr = alloca %class.btVector3*, align 4
  %b_bottom.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %a_top, %class.btVector3** %a_top.addr, align 4, !tbaa !2
  store %class.btVector3* %a_bottom, %class.btVector3** %a_bottom.addr, align 4, !tbaa !2
  store %class.btVector3* %b_top, %class.btVector3** %b_top.addr, align 4, !tbaa !2
  store %class.btVector3* %b_bottom, %class.btVector3** %b_bottom.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %a_bottom.addr, align 4, !tbaa !2
  %1 = load %class.btVector3*, %class.btVector3** %b_top.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %2 = load %class.btVector3*, %class.btVector3** %a_top.addr, align 4, !tbaa !2
  %3 = load %class.btVector3*, %class.btVector3** %b_bottom.addr, align 4, !tbaa !2
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  %add = fadd float %call, %call1
  ret float %add
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #3 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp45 = alloca float, align 4
  %ref.tmp53 = alloca float, align 4
  %ref.tmp61 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 0)
  %call1 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call)
  %arrayidx = getelementptr inbounds float, float* %call1, i32 0
  %3 = load float, float* %arrayidx, align 4, !tbaa !8
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 0)
  %call3 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call2)
  %arrayidx4 = getelementptr inbounds float, float* %call3, i32 0
  %5 = load float, float* %arrayidx4, align 4, !tbaa !8
  %sub = fsub float %3, %5
  store float %sub, float* %ref.tmp, align 4, !tbaa !8
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 0)
  %call7 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call6)
  %arrayidx8 = getelementptr inbounds float, float* %call7, i32 1
  %8 = load float, float* %arrayidx8, align 4, !tbaa !8
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %9, i32 0)
  %call10 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call9)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 1
  %10 = load float, float* %arrayidx11, align 4, !tbaa !8
  %sub12 = fsub float %8, %10
  store float %sub12, float* %ref.tmp5, align 4, !tbaa !8
  %11 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #7
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call14 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 0)
  %call15 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call14)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 2
  %13 = load float, float* %arrayidx16, align 4, !tbaa !8
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %call17 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %14, i32 0)
  %call18 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call17)
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 2
  %15 = load float, float* %arrayidx19, align 4, !tbaa !8
  %sub20 = fsub float %13, %15
  store float %sub20, float* %ref.tmp13, align 4, !tbaa !8
  %16 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #7
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call22 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %17, i32 1)
  %call23 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call22)
  %arrayidx24 = getelementptr inbounds float, float* %call23, i32 0
  %18 = load float, float* %arrayidx24, align 4, !tbaa !8
  %19 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %call25 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %19, i32 1)
  %call26 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call25)
  %arrayidx27 = getelementptr inbounds float, float* %call26, i32 0
  %20 = load float, float* %arrayidx27, align 4, !tbaa !8
  %sub28 = fsub float %18, %20
  store float %sub28, float* %ref.tmp21, align 4, !tbaa !8
  %21 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #7
  %22 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call30 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %22, i32 1)
  %call31 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call30)
  %arrayidx32 = getelementptr inbounds float, float* %call31, i32 1
  %23 = load float, float* %arrayidx32, align 4, !tbaa !8
  %24 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %call33 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %24, i32 1)
  %call34 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call33)
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 1
  %25 = load float, float* %arrayidx35, align 4, !tbaa !8
  %sub36 = fsub float %23, %25
  store float %sub36, float* %ref.tmp29, align 4, !tbaa !8
  %26 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #7
  %27 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call38 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %27, i32 1)
  %call39 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call38)
  %arrayidx40 = getelementptr inbounds float, float* %call39, i32 2
  %28 = load float, float* %arrayidx40, align 4, !tbaa !8
  %29 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %call41 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %29, i32 1)
  %call42 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call41)
  %arrayidx43 = getelementptr inbounds float, float* %call42, i32 2
  %30 = load float, float* %arrayidx43, align 4, !tbaa !8
  %sub44 = fsub float %28, %30
  store float %sub44, float* %ref.tmp37, align 4, !tbaa !8
  %31 = bitcast float* %ref.tmp45 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #7
  %32 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call46 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %32, i32 2)
  %call47 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call46)
  %arrayidx48 = getelementptr inbounds float, float* %call47, i32 0
  %33 = load float, float* %arrayidx48, align 4, !tbaa !8
  %34 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %call49 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %34, i32 2)
  %call50 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call49)
  %arrayidx51 = getelementptr inbounds float, float* %call50, i32 0
  %35 = load float, float* %arrayidx51, align 4, !tbaa !8
  %sub52 = fsub float %33, %35
  store float %sub52, float* %ref.tmp45, align 4, !tbaa !8
  %36 = bitcast float* %ref.tmp53 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #7
  %37 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call54 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %37, i32 2)
  %call55 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call54)
  %arrayidx56 = getelementptr inbounds float, float* %call55, i32 1
  %38 = load float, float* %arrayidx56, align 4, !tbaa !8
  %39 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %call57 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %39, i32 2)
  %call58 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call57)
  %arrayidx59 = getelementptr inbounds float, float* %call58, i32 1
  %40 = load float, float* %arrayidx59, align 4, !tbaa !8
  %sub60 = fsub float %38, %40
  store float %sub60, float* %ref.tmp53, align 4, !tbaa !8
  %41 = bitcast float* %ref.tmp61 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #7
  %42 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call62 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %42, i32 2)
  %call63 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call62)
  %arrayidx64 = getelementptr inbounds float, float* %call63, i32 2
  %43 = load float, float* %arrayidx64, align 4, !tbaa !8
  %44 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %call65 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %44, i32 2)
  %call66 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call65)
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 2
  %45 = load float, float* %arrayidx67, align 4, !tbaa !8
  %sub68 = fsub float %43, %45
  store float %sub68, float* %ref.tmp61, align 4, !tbaa !8
  %call69 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp21, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp37, float* nonnull align 4 dereferenceable(4) %ref.tmp45, float* nonnull align 4 dereferenceable(4) %ref.tmp53, float* nonnull align 4 dereferenceable(4) %ref.tmp61)
  %46 = bitcast float* %ref.tmp61 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #7
  %47 = bitcast float* %ref.tmp53 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #7
  %48 = bitcast float* %ref.tmp45 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #7
  %49 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #7
  %50 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #7
  %51 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #7
  %52 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #7
  %53 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #7
  %54 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_Z18vecMulVecTransposeRK9btVector3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1Transposed) #3 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1Transposed.addr = alloca %class.btVector3*, align 4
  %row0 = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %row1 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  %row2 = alloca %class.btVector3, align 4
  %ref.tmp24 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1Transposed, %class.btVector3** %v1Transposed.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVector3* %row0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #7
  %2 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %3)
  %4 = load float, float* %call, align 4, !tbaa !8
  %5 = load %class.btVector3*, %class.btVector3** %v1Transposed.addr, align 4, !tbaa !2
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %5)
  %6 = load float, float* %call1, align 4, !tbaa !8
  %mul = fmul float %4, %6
  store float %mul, float* %ref.tmp, align 4, !tbaa !8
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %8 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %8)
  %9 = load float, float* %call3, align 4, !tbaa !8
  %10 = load %class.btVector3*, %class.btVector3** %v1Transposed.addr, align 4, !tbaa !2
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %10)
  %11 = load float, float* %call4, align 4, !tbaa !8
  %mul5 = fmul float %9, %11
  store float %mul5, float* %ref.tmp2, align 4, !tbaa !8
  %12 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #7
  %13 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %13)
  %14 = load float, float* %call7, align 4, !tbaa !8
  %15 = load %class.btVector3*, %class.btVector3** %v1Transposed.addr, align 4, !tbaa !2
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %15)
  %16 = load float, float* %call8, align 4, !tbaa !8
  %mul9 = fmul float %14, %16
  store float %mul9, float* %ref.tmp6, align 4, !tbaa !8
  %call10 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %row0, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %17 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  %18 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #7
  %19 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #7
  %20 = bitcast %class.btVector3* %row1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %20) #7
  %21 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #7
  %22 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %22)
  %23 = load float, float* %call12, align 4, !tbaa !8
  %24 = load %class.btVector3*, %class.btVector3** %v1Transposed.addr, align 4, !tbaa !2
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %24)
  %25 = load float, float* %call13, align 4, !tbaa !8
  %mul14 = fmul float %23, %25
  store float %mul14, float* %ref.tmp11, align 4, !tbaa !8
  %26 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #7
  %27 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %27)
  %28 = load float, float* %call16, align 4, !tbaa !8
  %29 = load %class.btVector3*, %class.btVector3** %v1Transposed.addr, align 4, !tbaa !2
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %29)
  %30 = load float, float* %call17, align 4, !tbaa !8
  %mul18 = fmul float %28, %30
  store float %mul18, float* %ref.tmp15, align 4, !tbaa !8
  %31 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #7
  %32 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %32)
  %33 = load float, float* %call20, align 4, !tbaa !8
  %34 = load %class.btVector3*, %class.btVector3** %v1Transposed.addr, align 4, !tbaa !2
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %34)
  %35 = load float, float* %call21, align 4, !tbaa !8
  %mul22 = fmul float %33, %35
  store float %mul22, float* %ref.tmp19, align 4, !tbaa !8
  %call23 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %row1, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp19)
  %36 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #7
  %37 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #7
  %38 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #7
  %39 = bitcast %class.btVector3* %row2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %39) #7
  %40 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #7
  %41 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %41)
  %42 = load float, float* %call25, align 4, !tbaa !8
  %43 = load %class.btVector3*, %class.btVector3** %v1Transposed.addr, align 4, !tbaa !2
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %43)
  %44 = load float, float* %call26, align 4, !tbaa !8
  %mul27 = fmul float %42, %44
  store float %mul27, float* %ref.tmp24, align 4, !tbaa !8
  %45 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #7
  %46 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %46)
  %47 = load float, float* %call29, align 4, !tbaa !8
  %48 = load %class.btVector3*, %class.btVector3** %v1Transposed.addr, align 4, !tbaa !2
  %call30 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %48)
  %49 = load float, float* %call30, align 4, !tbaa !8
  %mul31 = fmul float %47, %49
  store float %mul31, float* %ref.tmp28, align 4, !tbaa !8
  %50 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #7
  %51 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %51)
  %52 = load float, float* %call33, align 4, !tbaa !8
  %53 = load %class.btVector3*, %class.btVector3** %v1Transposed.addr, align 4, !tbaa !2
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %53)
  %54 = load float, float* %call34, align 4, !tbaa !8
  %mul35 = fmul float %52, %54
  store float %mul35, float* %ref.tmp32, align 4, !tbaa !8
  %call36 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %row2, float* nonnull align 4 dereferenceable(4) %ref.tmp24, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  %55 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #7
  %56 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #7
  %57 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #7
  %call37 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %row0)
  %arrayidx = getelementptr inbounds float, float* %call37, i32 0
  %call38 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %row0)
  %arrayidx39 = getelementptr inbounds float, float* %call38, i32 1
  %call40 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %row0)
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 2
  %call42 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %row1)
  %arrayidx43 = getelementptr inbounds float, float* %call42, i32 0
  %call44 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %row1)
  %arrayidx45 = getelementptr inbounds float, float* %call44, i32 1
  %call46 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %row1)
  %arrayidx47 = getelementptr inbounds float, float* %call46, i32 2
  %call48 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %row2)
  %arrayidx49 = getelementptr inbounds float, float* %call48, i32 0
  %call50 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %row2)
  %arrayidx51 = getelementptr inbounds float, float* %call50, i32 1
  %call52 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %row2)
  %arrayidx53 = getelementptr inbounds float, float* %call52, i32 2
  %call54 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx39, float* nonnull align 4 dereferenceable(4) %arrayidx41, float* nonnull align 4 dereferenceable(4) %arrayidx43, float* nonnull align 4 dereferenceable(4) %arrayidx45, float* nonnull align 4 dereferenceable(4) %arrayidx47, float* nonnull align 4 dereferenceable(4) %arrayidx49, float* nonnull align 4 dereferenceable(4) %arrayidx51, float* nonnull align 4 dereferenceable(4) %arrayidx53)
  %58 = bitcast %class.btVector3* %row2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %58) #7
  %59 = bitcast %class.btVector3* %row1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %59) #7
  %60 = bitcast %class.btVector3* %row0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %60) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3pLERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp47 = alloca float, align 4
  %ref.tmp57 = alloca float, align 4
  %ref.tmp67 = alloca float, align 4
  %ref.tmp77 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx)
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 0
  %1 = load float, float* %arrayidx2, align 4, !tbaa !8
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %2, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx4)
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 0
  %3 = load float, float* %arrayidx6, align 4, !tbaa !8
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4, !tbaa !8
  %4 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx9)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 1
  %5 = load float, float* %arrayidx11, align 4, !tbaa !8
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %m_el12 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el12, i32 0, i32 0
  %call14 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx13)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 1
  %7 = load float, float* %arrayidx15, align 4, !tbaa !8
  %add16 = fadd float %5, %7
  store float %add16, float* %ref.tmp7, align 4, !tbaa !8
  %8 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  %m_el18 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx19 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el18, i32 0, i32 0
  %call20 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx19)
  %arrayidx21 = getelementptr inbounds float, float* %call20, i32 2
  %9 = load float, float* %arrayidx21, align 4, !tbaa !8
  %10 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %m_el22 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %10, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el22, i32 0, i32 0
  %call24 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx23)
  %arrayidx25 = getelementptr inbounds float, float* %call24, i32 2
  %11 = load float, float* %arrayidx25, align 4, !tbaa !8
  %add26 = fadd float %9, %11
  store float %add26, float* %ref.tmp17, align 4, !tbaa !8
  %12 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #7
  %m_el28 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx29 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el28, i32 0, i32 1
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx29)
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 0
  %13 = load float, float* %arrayidx31, align 4, !tbaa !8
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %m_el32 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %14, i32 0, i32 0
  %arrayidx33 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el32, i32 0, i32 1
  %call34 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx33)
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 0
  %15 = load float, float* %arrayidx35, align 4, !tbaa !8
  %add36 = fadd float %13, %15
  store float %add36, float* %ref.tmp27, align 4, !tbaa !8
  %16 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #7
  %m_el38 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx39 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el38, i32 0, i32 1
  %call40 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx39)
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 1
  %17 = load float, float* %arrayidx41, align 4, !tbaa !8
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %m_el42 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %18, i32 0, i32 0
  %arrayidx43 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el42, i32 0, i32 1
  %call44 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx43)
  %arrayidx45 = getelementptr inbounds float, float* %call44, i32 1
  %19 = load float, float* %arrayidx45, align 4, !tbaa !8
  %add46 = fadd float %17, %19
  store float %add46, float* %ref.tmp37, align 4, !tbaa !8
  %20 = bitcast float* %ref.tmp47 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #7
  %m_el48 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx49 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el48, i32 0, i32 1
  %call50 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx49)
  %arrayidx51 = getelementptr inbounds float, float* %call50, i32 2
  %21 = load float, float* %arrayidx51, align 4, !tbaa !8
  %22 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %m_el52 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %22, i32 0, i32 0
  %arrayidx53 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el52, i32 0, i32 1
  %call54 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx53)
  %arrayidx55 = getelementptr inbounds float, float* %call54, i32 2
  %23 = load float, float* %arrayidx55, align 4, !tbaa !8
  %add56 = fadd float %21, %23
  store float %add56, float* %ref.tmp47, align 4, !tbaa !8
  %24 = bitcast float* %ref.tmp57 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #7
  %m_el58 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx59 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el58, i32 0, i32 2
  %call60 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx59)
  %arrayidx61 = getelementptr inbounds float, float* %call60, i32 0
  %25 = load float, float* %arrayidx61, align 4, !tbaa !8
  %26 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %m_el62 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %26, i32 0, i32 0
  %arrayidx63 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el62, i32 0, i32 2
  %call64 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx63)
  %arrayidx65 = getelementptr inbounds float, float* %call64, i32 0
  %27 = load float, float* %arrayidx65, align 4, !tbaa !8
  %add66 = fadd float %25, %27
  store float %add66, float* %ref.tmp57, align 4, !tbaa !8
  %28 = bitcast float* %ref.tmp67 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #7
  %m_el68 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx69 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el68, i32 0, i32 2
  %call70 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx69)
  %arrayidx71 = getelementptr inbounds float, float* %call70, i32 1
  %29 = load float, float* %arrayidx71, align 4, !tbaa !8
  %30 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %m_el72 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %30, i32 0, i32 0
  %arrayidx73 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el72, i32 0, i32 2
  %call74 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx73)
  %arrayidx75 = getelementptr inbounds float, float* %call74, i32 1
  %31 = load float, float* %arrayidx75, align 4, !tbaa !8
  %add76 = fadd float %29, %31
  store float %add76, float* %ref.tmp67, align 4, !tbaa !8
  %32 = bitcast float* %ref.tmp77 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #7
  %m_el78 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx79 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el78, i32 0, i32 2
  %call80 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx79)
  %arrayidx81 = getelementptr inbounds float, float* %call80, i32 2
  %33 = load float, float* %arrayidx81, align 4, !tbaa !8
  %34 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %m_el82 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %34, i32 0, i32 0
  %arrayidx83 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el82, i32 0, i32 2
  %call84 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx83)
  %arrayidx85 = getelementptr inbounds float, float* %call84, i32 2
  %35 = load float, float* %arrayidx85, align 4, !tbaa !8
  %add86 = fadd float %33, %35
  store float %add86, float* %ref.tmp77, align 4, !tbaa !8
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp37, float* nonnull align 4 dereferenceable(4) %ref.tmp47, float* nonnull align 4 dereferenceable(4) %ref.tmp57, float* nonnull align 4 dereferenceable(4) %ref.tmp67, float* nonnull align 4 dereferenceable(4) %ref.tmp77)
  %36 = bitcast float* %ref.tmp77 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #7
  %37 = bitcast float* %ref.tmp67 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #7
  %38 = bitcast float* %ref.tmp57 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #7
  %39 = bitcast float* %ref.tmp47 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #7
  %40 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #7
  %41 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #7
  %42 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #7
  %43 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #7
  %44 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #7
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #3 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp45 = alloca float, align 4
  %ref.tmp53 = alloca float, align 4
  %ref.tmp61 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 0)
  %call1 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call)
  %arrayidx = getelementptr inbounds float, float* %call1, i32 0
  %3 = load float, float* %arrayidx, align 4, !tbaa !8
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 0)
  %call3 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call2)
  %arrayidx4 = getelementptr inbounds float, float* %call3, i32 0
  %5 = load float, float* %arrayidx4, align 4, !tbaa !8
  %add = fadd float %3, %5
  store float %add, float* %ref.tmp, align 4, !tbaa !8
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 0)
  %call7 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call6)
  %arrayidx8 = getelementptr inbounds float, float* %call7, i32 1
  %8 = load float, float* %arrayidx8, align 4, !tbaa !8
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %9, i32 0)
  %call10 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call9)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 1
  %10 = load float, float* %arrayidx11, align 4, !tbaa !8
  %add12 = fadd float %8, %10
  store float %add12, float* %ref.tmp5, align 4, !tbaa !8
  %11 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #7
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call14 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 0)
  %call15 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call14)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 2
  %13 = load float, float* %arrayidx16, align 4, !tbaa !8
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %call17 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %14, i32 0)
  %call18 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call17)
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 2
  %15 = load float, float* %arrayidx19, align 4, !tbaa !8
  %add20 = fadd float %13, %15
  store float %add20, float* %ref.tmp13, align 4, !tbaa !8
  %16 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #7
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call22 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %17, i32 1)
  %call23 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call22)
  %arrayidx24 = getelementptr inbounds float, float* %call23, i32 0
  %18 = load float, float* %arrayidx24, align 4, !tbaa !8
  %19 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %call25 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %19, i32 1)
  %call26 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call25)
  %arrayidx27 = getelementptr inbounds float, float* %call26, i32 0
  %20 = load float, float* %arrayidx27, align 4, !tbaa !8
  %add28 = fadd float %18, %20
  store float %add28, float* %ref.tmp21, align 4, !tbaa !8
  %21 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #7
  %22 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call30 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %22, i32 1)
  %call31 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call30)
  %arrayidx32 = getelementptr inbounds float, float* %call31, i32 1
  %23 = load float, float* %arrayidx32, align 4, !tbaa !8
  %24 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %call33 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %24, i32 1)
  %call34 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call33)
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 1
  %25 = load float, float* %arrayidx35, align 4, !tbaa !8
  %add36 = fadd float %23, %25
  store float %add36, float* %ref.tmp29, align 4, !tbaa !8
  %26 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #7
  %27 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call38 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %27, i32 1)
  %call39 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call38)
  %arrayidx40 = getelementptr inbounds float, float* %call39, i32 2
  %28 = load float, float* %arrayidx40, align 4, !tbaa !8
  %29 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %call41 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %29, i32 1)
  %call42 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call41)
  %arrayidx43 = getelementptr inbounds float, float* %call42, i32 2
  %30 = load float, float* %arrayidx43, align 4, !tbaa !8
  %add44 = fadd float %28, %30
  store float %add44, float* %ref.tmp37, align 4, !tbaa !8
  %31 = bitcast float* %ref.tmp45 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #7
  %32 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call46 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %32, i32 2)
  %call47 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call46)
  %arrayidx48 = getelementptr inbounds float, float* %call47, i32 0
  %33 = load float, float* %arrayidx48, align 4, !tbaa !8
  %34 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %call49 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %34, i32 2)
  %call50 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call49)
  %arrayidx51 = getelementptr inbounds float, float* %call50, i32 0
  %35 = load float, float* %arrayidx51, align 4, !tbaa !8
  %add52 = fadd float %33, %35
  store float %add52, float* %ref.tmp45, align 4, !tbaa !8
  %36 = bitcast float* %ref.tmp53 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #7
  %37 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call54 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %37, i32 2)
  %call55 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call54)
  %arrayidx56 = getelementptr inbounds float, float* %call55, i32 1
  %38 = load float, float* %arrayidx56, align 4, !tbaa !8
  %39 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %call57 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %39, i32 2)
  %call58 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call57)
  %arrayidx59 = getelementptr inbounds float, float* %call58, i32 1
  %40 = load float, float* %arrayidx59, align 4, !tbaa !8
  %add60 = fadd float %38, %40
  store float %add60, float* %ref.tmp53, align 4, !tbaa !8
  %41 = bitcast float* %ref.tmp61 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #7
  %42 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call62 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %42, i32 2)
  %call63 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call62)
  %arrayidx64 = getelementptr inbounds float, float* %call63, i32 2
  %43 = load float, float* %arrayidx64, align 4, !tbaa !8
  %44 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %call65 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %44, i32 2)
  %call66 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call65)
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 2
  %45 = load float, float* %arrayidx67, align 4, !tbaa !8
  %add68 = fadd float %43, %45
  store float %add68, float* %ref.tmp61, align 4, !tbaa !8
  %call69 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp21, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp37, float* nonnull align 4 dereferenceable(4) %ref.tmp45, float* nonnull align 4 dereferenceable(4) %ref.tmp53, float* nonnull align 4 dereferenceable(4) %ref.tmp61)
  %46 = bitcast float* %ref.tmp61 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #7
  %47 = bitcast float* %ref.tmp53 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #7
  %48 = bitcast float* %ref.tmp45 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #7
  %49 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #7
  %50 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #7
  %51 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #7
  %52 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #7
  %53 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #7
  %54 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #7
  ret void
}

define internal void @_ZN12_GLOBAL__N_123InverseSpatialTransformERK11btMatrix3x3RK9btVector3S5_S5_RS3_S6_(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %rotation_matrix, %class.btVector3* nonnull align 4 dereferenceable(16) %displacement, %class.btVector3* nonnull align 4 dereferenceable(16) %top_in, %class.btVector3* nonnull align 4 dereferenceable(16) %bottom_in, %class.btVector3* nonnull align 4 dereferenceable(16) %top_out, %class.btVector3* nonnull align 4 dereferenceable(16) %bottom_out) #0 {
entry:
  %rotation_matrix.addr = alloca %class.btMatrix3x3*, align 4
  %displacement.addr = alloca %class.btVector3*, align 4
  %top_in.addr = alloca %class.btVector3*, align 4
  %bottom_in.addr = alloca %class.btVector3*, align 4
  %top_out.addr = alloca %class.btVector3*, align 4
  %bottom_out.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca %class.btMatrix3x3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btMatrix3x3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  store %class.btMatrix3x3* %rotation_matrix, %class.btMatrix3x3** %rotation_matrix.addr, align 4, !tbaa !2
  store %class.btVector3* %displacement, %class.btVector3** %displacement.addr, align 4, !tbaa !2
  store %class.btVector3* %top_in, %class.btVector3** %top_in.addr, align 4, !tbaa !2
  store %class.btVector3* %bottom_in, %class.btVector3** %bottom_in.addr, align 4, !tbaa !2
  store %class.btVector3* %top_out, %class.btVector3** %top_out.addr, align 4, !tbaa !2
  store %class.btVector3* %bottom_out, %class.btVector3** %bottom_out.addr, align 4, !tbaa !2
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #7
  %1 = bitcast %class.btMatrix3x3* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %1) #7
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rotation_matrix.addr, align 4, !tbaa !2
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp1, %class.btMatrix3x3* %2)
  %3 = load %class.btVector3*, %class.btVector3** %top_in.addr, align 4, !tbaa !2
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  %4 = load %class.btVector3*, %class.btVector3** %top_out.addr, align 4, !tbaa !2
  %5 = bitcast %class.btVector3* %4 to i8*
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false), !tbaa.struct !28
  %7 = bitcast %class.btMatrix3x3* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %7) #7
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #7
  %9 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #7
  %10 = bitcast %class.btMatrix3x3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %10) #7
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rotation_matrix.addr, align 4, !tbaa !2
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp3, %class.btMatrix3x3* %11)
  %12 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %12) #7
  %13 = load %class.btVector3*, %class.btVector3** %bottom_in.addr, align 4, !tbaa !2
  %14 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %14) #7
  %15 = load %class.btVector3*, %class.btVector3** %displacement.addr, align 4, !tbaa !2
  %16 = load %class.btVector3*, %class.btVector3** %top_in.addr, align 4, !tbaa !2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* %15, %class.btVector3* nonnull align 4 dereferenceable(16) %16)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %13, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  %17 = load %class.btVector3*, %class.btVector3** %bottom_out.addr, align 4, !tbaa !2
  %18 = bitcast %class.btVector3* %17 to i8*
  %19 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %18, i8* align 4 %19, i32 16, i1 false), !tbaa.struct !28
  %20 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %20) #7
  %21 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %21) #7
  %22 = bitcast %class.btMatrix3x3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %22) #7
  %23 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %23) #7
  ret void
}

define hidden void @_ZNK11btMultiBody12solveImatrixERK9btVector3S2_Pf(%class.btMultiBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %rhs_top, %class.btVector3* nonnull align 4 dereferenceable(16) %rhs_bot, float* %result) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %rhs_top.addr = alloca %class.btVector3*, align 4
  %rhs_bot.addr = alloca %class.btVector3*, align 4
  %result.addr = alloca float*, align 4
  %num_links = alloca i32, align 4
  %Binv = alloca %class.btMatrix3x3, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp34 = alloca float, align 4
  %tmp = alloca %class.btMatrix3x3, align 4
  %invIupper_right = alloca %class.btMatrix3x3, align 4
  %ref.tmp35 = alloca %class.btMatrix3x3, align 4
  %ref.tmp36 = alloca %class.btMatrix3x3, align 4
  %ref.tmp37 = alloca %class.btMatrix3x3, align 4
  %invI_upper_left = alloca %class.btMatrix3x3, align 4
  %invI_lower_right = alloca %class.btMatrix3x3, align 4
  %ref.tmp40 = alloca %class.btMatrix3x3, align 4
  %invI_lower_left = alloca %class.btMatrix3x3, align 4
  %vtop = alloca %class.btVector3, align 4
  %tmp59 = alloca %class.btVector3, align 4
  %ref.tmp61 = alloca %class.btVector3, align 4
  %vbot = alloca %class.btVector3, align 4
  %ref.tmp63 = alloca %class.btVector3, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %rhs_top, %class.btVector3** %rhs_top.addr, align 4, !tbaa !2
  store %class.btVector3* %rhs_bot, %class.btVector3** %rhs_bot.addr, align 4, !tbaa !2
  store float* %result, float** %result.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = bitcast i32* %num_links to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %this1)
  store i32 %call, i32* %num_links, align 4, !tbaa !6
  %1 = load i32, i32* %num_links, align 4, !tbaa !6
  %cmp = icmp eq i32 %1, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load %class.btVector3*, %class.btVector3** %rhs_bot.addr, align 4, !tbaa !2
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %2)
  %arrayidx = getelementptr inbounds float, float* %call2, i32 0
  %3 = load float, float* %arrayidx, align 4, !tbaa !8
  %base_inertia = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 4
  %call3 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %base_inertia)
  %arrayidx4 = getelementptr inbounds float, float* %call3, i32 0
  %4 = load float, float* %arrayidx4, align 4, !tbaa !8
  %div = fdiv float %3, %4
  %5 = load float*, float** %result.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds float, float* %5, i32 0
  store float %div, float* %arrayidx5, align 4, !tbaa !8
  %6 = load %class.btVector3*, %class.btVector3** %rhs_bot.addr, align 4, !tbaa !2
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %6)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 1
  %7 = load float, float* %arrayidx7, align 4, !tbaa !8
  %base_inertia8 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 4
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %base_inertia8)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 1
  %8 = load float, float* %arrayidx10, align 4, !tbaa !8
  %div11 = fdiv float %7, %8
  %9 = load float*, float** %result.addr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds float, float* %9, i32 1
  store float %div11, float* %arrayidx12, align 4, !tbaa !8
  %10 = load %class.btVector3*, %class.btVector3** %rhs_bot.addr, align 4, !tbaa !2
  %call13 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %10)
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 2
  %11 = load float, float* %arrayidx14, align 4, !tbaa !8
  %base_inertia15 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 4
  %call16 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %base_inertia15)
  %arrayidx17 = getelementptr inbounds float, float* %call16, i32 2
  %12 = load float, float* %arrayidx17, align 4, !tbaa !8
  %div18 = fdiv float %11, %12
  %13 = load float*, float** %result.addr, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds float, float* %13, i32 2
  store float %div18, float* %arrayidx19, align 4, !tbaa !8
  %14 = load %class.btVector3*, %class.btVector3** %rhs_top.addr, align 4, !tbaa !2
  %call20 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %14)
  %arrayidx21 = getelementptr inbounds float, float* %call20, i32 0
  %15 = load float, float* %arrayidx21, align 4, !tbaa !8
  %base_mass = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 3
  %16 = load float, float* %base_mass, align 4, !tbaa !27
  %div22 = fdiv float %15, %16
  %17 = load float*, float** %result.addr, align 4, !tbaa !2
  %arrayidx23 = getelementptr inbounds float, float* %17, i32 3
  store float %div22, float* %arrayidx23, align 4, !tbaa !8
  %18 = load %class.btVector3*, %class.btVector3** %rhs_top.addr, align 4, !tbaa !2
  %call24 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %18)
  %arrayidx25 = getelementptr inbounds float, float* %call24, i32 1
  %19 = load float, float* %arrayidx25, align 4, !tbaa !8
  %base_mass26 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 3
  %20 = load float, float* %base_mass26, align 4, !tbaa !27
  %div27 = fdiv float %19, %20
  %21 = load float*, float** %result.addr, align 4, !tbaa !2
  %arrayidx28 = getelementptr inbounds float, float* %21, i32 4
  store float %div27, float* %arrayidx28, align 4, !tbaa !8
  %22 = load %class.btVector3*, %class.btVector3** %rhs_top.addr, align 4, !tbaa !2
  %call29 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %22)
  %arrayidx30 = getelementptr inbounds float, float* %call29, i32 2
  %23 = load float, float* %arrayidx30, align 4, !tbaa !8
  %base_mass31 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 3
  %24 = load float, float* %base_mass31, align 4, !tbaa !27
  %div32 = fdiv float %23, %24
  %25 = load float*, float** %result.addr, align 4, !tbaa !2
  %arrayidx33 = getelementptr inbounds float, float* %25, i32 5
  store float %div32, float* %arrayidx33, align 4, !tbaa !8
  br label %if.end

if.else:                                          ; preds = %entry
  %26 = bitcast %class.btMatrix3x3* %Binv to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %26) #7
  %27 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %27) #7
  %cached_inertia_top_right = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 13
  call void @_ZNK11btMatrix3x37inverseEv(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* %cached_inertia_top_right)
  %28 = bitcast float* %ref.tmp34 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #7
  store float -1.000000e+00, float* %ref.tmp34, align 4, !tbaa !8
  call void @_ZmlRK11btMatrix3x3RKf(%class.btMatrix3x3* sret align 4 %Binv, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp34)
  %29 = bitcast float* %ref.tmp34 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #7
  %30 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %30) #7
  %31 = bitcast %class.btMatrix3x3* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %31) #7
  %cached_inertia_lower_right = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 15
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %cached_inertia_lower_right, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %Binv)
  %32 = bitcast %class.btMatrix3x3* %invIupper_right to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %32) #7
  %33 = bitcast %class.btMatrix3x3* %ref.tmp35 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %33) #7
  %34 = bitcast %class.btMatrix3x3* %ref.tmp36 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %34) #7
  %cached_inertia_top_left = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 12
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp36, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %cached_inertia_top_left)
  %cached_inertia_lower_left = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 14
  call void @_ZplRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp35, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp36, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %cached_inertia_lower_left)
  call void @_ZNK11btMatrix3x37inverseEv(%class.btMatrix3x3* sret align 4 %invIupper_right, %class.btMatrix3x3* %ref.tmp35)
  %35 = bitcast %class.btMatrix3x3* %ref.tmp36 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %35) #7
  %36 = bitcast %class.btMatrix3x3* %ref.tmp35 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %36) #7
  %37 = bitcast %class.btMatrix3x3* %ref.tmp37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %37) #7
  %cached_inertia_lower_right38 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 15
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp37, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %invIupper_right, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %cached_inertia_lower_right38)
  %call39 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp37)
  %38 = bitcast %class.btMatrix3x3* %ref.tmp37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %38) #7
  %39 = bitcast %class.btMatrix3x3* %invI_upper_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %39) #7
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %invI_upper_left, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %Binv)
  %40 = bitcast %class.btMatrix3x3* %invI_lower_right to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %40) #7
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %invI_lower_right, %class.btMatrix3x3* %invI_upper_left)
  %41 = bitcast %class.btMatrix3x3* %ref.tmp40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %41) #7
  %cached_inertia_top_left41 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 12
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp40, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %cached_inertia_top_left41, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %invI_upper_left)
  %call42 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp40)
  %42 = bitcast %class.btMatrix3x3* %ref.tmp40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %42) #7
  %call43 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %tmp, i32 0)
  %call44 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call43)
  %arrayidx45 = getelementptr inbounds float, float* %call44, i32 0
  %43 = load float, float* %arrayidx45, align 4, !tbaa !8
  %conv = fpext float %43 to double
  %sub = fsub double %conv, 1.000000e+00
  %conv46 = fptrunc double %sub to float
  store float %conv46, float* %arrayidx45, align 4, !tbaa !8
  %call47 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %tmp, i32 1)
  %call48 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call47)
  %arrayidx49 = getelementptr inbounds float, float* %call48, i32 1
  %44 = load float, float* %arrayidx49, align 4, !tbaa !8
  %conv50 = fpext float %44 to double
  %sub51 = fsub double %conv50, 1.000000e+00
  %conv52 = fptrunc double %sub51 to float
  store float %conv52, float* %arrayidx49, align 4, !tbaa !8
  %call53 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %tmp, i32 2)
  %call54 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call53)
  %arrayidx55 = getelementptr inbounds float, float* %call54, i32 2
  %45 = load float, float* %arrayidx55, align 4, !tbaa !8
  %conv56 = fpext float %45 to double
  %sub57 = fsub double %conv56, 1.000000e+00
  %conv58 = fptrunc double %sub57 to float
  store float %conv58, float* %arrayidx55, align 4, !tbaa !8
  %46 = bitcast %class.btMatrix3x3* %invI_lower_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %46) #7
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %invI_lower_left, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %Binv, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %tmp)
  %47 = bitcast %class.btVector3* %vtop to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %47) #7
  %48 = load %class.btVector3*, %class.btVector3** %rhs_top.addr, align 4, !tbaa !2
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %vtop, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %invI_upper_left, %class.btVector3* nonnull align 4 dereferenceable(16) %48)
  %49 = bitcast %class.btVector3* %tmp59 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %49) #7
  %call60 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %tmp59)
  %50 = bitcast %class.btVector3* %ref.tmp61 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %50) #7
  %51 = load %class.btVector3*, %class.btVector3** %rhs_bot.addr, align 4, !tbaa !2
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp61, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %invIupper_right, %class.btVector3* nonnull align 4 dereferenceable(16) %51)
  %52 = bitcast %class.btVector3* %tmp59 to i8*
  %53 = bitcast %class.btVector3* %ref.tmp61 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %52, i8* align 4 %53, i32 16, i1 false), !tbaa.struct !28
  %54 = bitcast %class.btVector3* %ref.tmp61 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %54) #7
  %call62 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %vtop, %class.btVector3* nonnull align 4 dereferenceable(16) %tmp59)
  %55 = bitcast %class.btVector3* %vbot to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %55) #7
  %56 = load %class.btVector3*, %class.btVector3** %rhs_top.addr, align 4, !tbaa !2
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %vbot, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %invI_lower_left, %class.btVector3* nonnull align 4 dereferenceable(16) %56)
  %57 = bitcast %class.btVector3* %ref.tmp63 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %57) #7
  %58 = load %class.btVector3*, %class.btVector3** %rhs_bot.addr, align 4, !tbaa !2
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp63, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %invI_lower_right, %class.btVector3* nonnull align 4 dereferenceable(16) %58)
  %59 = bitcast %class.btVector3* %tmp59 to i8*
  %60 = bitcast %class.btVector3* %ref.tmp63 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %59, i8* align 4 %60, i32 16, i1 false), !tbaa.struct !28
  %61 = bitcast %class.btVector3* %ref.tmp63 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %61) #7
  %call64 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %vbot, %class.btVector3* nonnull align 4 dereferenceable(16) %tmp59)
  %call65 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %vtop)
  %arrayidx66 = getelementptr inbounds float, float* %call65, i32 0
  %62 = load float, float* %arrayidx66, align 4, !tbaa !8
  %63 = load float*, float** %result.addr, align 4, !tbaa !2
  %arrayidx67 = getelementptr inbounds float, float* %63, i32 0
  store float %62, float* %arrayidx67, align 4, !tbaa !8
  %call68 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %vtop)
  %arrayidx69 = getelementptr inbounds float, float* %call68, i32 1
  %64 = load float, float* %arrayidx69, align 4, !tbaa !8
  %65 = load float*, float** %result.addr, align 4, !tbaa !2
  %arrayidx70 = getelementptr inbounds float, float* %65, i32 1
  store float %64, float* %arrayidx70, align 4, !tbaa !8
  %call71 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %vtop)
  %arrayidx72 = getelementptr inbounds float, float* %call71, i32 2
  %66 = load float, float* %arrayidx72, align 4, !tbaa !8
  %67 = load float*, float** %result.addr, align 4, !tbaa !2
  %arrayidx73 = getelementptr inbounds float, float* %67, i32 2
  store float %66, float* %arrayidx73, align 4, !tbaa !8
  %call74 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %vbot)
  %arrayidx75 = getelementptr inbounds float, float* %call74, i32 0
  %68 = load float, float* %arrayidx75, align 4, !tbaa !8
  %69 = load float*, float** %result.addr, align 4, !tbaa !2
  %arrayidx76 = getelementptr inbounds float, float* %69, i32 3
  store float %68, float* %arrayidx76, align 4, !tbaa !8
  %call77 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %vbot)
  %arrayidx78 = getelementptr inbounds float, float* %call77, i32 1
  %70 = load float, float* %arrayidx78, align 4, !tbaa !8
  %71 = load float*, float** %result.addr, align 4, !tbaa !2
  %arrayidx79 = getelementptr inbounds float, float* %71, i32 4
  store float %70, float* %arrayidx79, align 4, !tbaa !8
  %call80 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %vbot)
  %arrayidx81 = getelementptr inbounds float, float* %call80, i32 2
  %72 = load float, float* %arrayidx81, align 4, !tbaa !8
  %73 = load float*, float** %result.addr, align 4, !tbaa !2
  %arrayidx82 = getelementptr inbounds float, float* %73, i32 5
  store float %72, float* %arrayidx82, align 4, !tbaa !8
  %74 = bitcast %class.btVector3* %vbot to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %74) #7
  %75 = bitcast %class.btVector3* %tmp59 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %75) #7
  %76 = bitcast %class.btVector3* %vtop to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %76) #7
  %77 = bitcast %class.btMatrix3x3* %invI_lower_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %77) #7
  %78 = bitcast %class.btMatrix3x3* %invI_lower_right to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %78) #7
  %79 = bitcast %class.btMatrix3x3* %invI_upper_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %79) #7
  %80 = bitcast %class.btMatrix3x3* %invIupper_right to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %80) #7
  %81 = bitcast %class.btMatrix3x3* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %81) #7
  %82 = bitcast %class.btMatrix3x3* %Binv to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %82) #7
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %83 = bitcast i32* %num_links to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #7
  ret void
}

define linkonce_odr hidden void @_ZN11btMultiBody13applyDeltaVeeEPKff(%class.btMultiBody* %this, float* %delta_vee, float %multiplier) #0 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %delta_vee.addr = alloca float*, align 4
  %multiplier.addr = alloca float, align 4
  %sum = alloca float, align 4
  %i = alloca i32, align 4
  %l = alloca float, align 4
  %i10 = alloca i32, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store float* %delta_vee, float** %delta_vee.addr, align 4, !tbaa !2
  store float %multiplier, float* %multiplier.addr, align 4, !tbaa !8
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = bitcast float* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  store float 0.000000e+00, float* %sum, align 4, !tbaa !8
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %call = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %this1)
  %add = add nsw i32 6, %call
  %cmp = icmp slt i32 %2, %add
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load float*, float** %delta_vee.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %4, i32 %5
  %6 = load float, float* %arrayidx, align 4, !tbaa !8
  %7 = load float, float* %multiplier.addr, align 4, !tbaa !8
  %mul = fmul float %6, %7
  %8 = load float*, float** %delta_vee.addr, align 4, !tbaa !2
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds float, float* %8, i32 %9
  %10 = load float, float* %arrayidx2, align 4, !tbaa !8
  %mul3 = fmul float %mul, %10
  %11 = load float, float* %multiplier.addr, align 4, !tbaa !8
  %mul4 = fmul float %mul3, %11
  %12 = load float, float* %sum, align 4, !tbaa !8
  %add5 = fadd float %12, %mul4
  store float %add5, float* %sum, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %13 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %14 = bitcast float* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #7
  %15 = load float, float* %sum, align 4, !tbaa !8
  %call6 = call float @_Z6btSqrtf(float %15)
  store float %call6, float* %l, align 4, !tbaa !8
  %16 = load float, float* %l, align 4, !tbaa !8
  %m_maxAppliedImpulse = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 26
  %17 = load float, float* %m_maxAppliedImpulse, align 4, !tbaa !38
  %cmp7 = fcmp ogt float %16, %17
  br i1 %cmp7, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %m_maxAppliedImpulse8 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 26
  %18 = load float, float* %m_maxAppliedImpulse8, align 4, !tbaa !38
  %19 = load float, float* %l, align 4, !tbaa !8
  %div = fdiv float %18, %19
  %20 = load float, float* %multiplier.addr, align 4, !tbaa !8
  %mul9 = fmul float %20, %div
  store float %mul9, float* %multiplier.addr, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %21 = bitcast i32* %i10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #7
  store i32 0, i32* %i10, align 4, !tbaa !6
  br label %for.cond11

for.cond11:                                       ; preds = %for.inc27, %if.end
  %22 = load i32, i32* %i10, align 4, !tbaa !6
  %call12 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %this1)
  %add13 = add nsw i32 6, %call12
  %cmp14 = icmp slt i32 %22, %add13
  br i1 %cmp14, label %for.body16, label %for.cond.cleanup15

for.cond.cleanup15:                               ; preds = %for.cond11
  %23 = bitcast i32* %i10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #7
  br label %for.end29

for.body16:                                       ; preds = %for.cond11
  %24 = load float*, float** %delta_vee.addr, align 4, !tbaa !2
  %25 = load i32, i32* %i10, align 4, !tbaa !6
  %arrayidx17 = getelementptr inbounds float, float* %24, i32 %25
  %26 = load float, float* %arrayidx17, align 4, !tbaa !8
  %27 = load float, float* %multiplier.addr, align 4, !tbaa !8
  %mul18 = fmul float %26, %27
  %28 = load float*, float** %delta_vee.addr, align 4, !tbaa !2
  %29 = load i32, i32* %i10, align 4, !tbaa !6
  %arrayidx19 = getelementptr inbounds float, float* %28, i32 %29
  %30 = load float, float* %arrayidx19, align 4, !tbaa !8
  %mul20 = fmul float %mul18, %30
  %31 = load float, float* %multiplier.addr, align 4, !tbaa !8
  %mul21 = fmul float %mul20, %31
  %32 = load float, float* %sum, align 4, !tbaa !8
  %add22 = fadd float %32, %mul21
  store float %add22, float* %sum, align 4, !tbaa !8
  %33 = load float*, float** %delta_vee.addr, align 4, !tbaa !2
  %34 = load i32, i32* %i10, align 4, !tbaa !6
  %arrayidx23 = getelementptr inbounds float, float* %33, i32 %34
  %35 = load float, float* %arrayidx23, align 4, !tbaa !8
  %36 = load float, float* %multiplier.addr, align 4, !tbaa !8
  %mul24 = fmul float %35, %36
  %m_real_buf = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 9
  %37 = load i32, i32* %i10, align 4, !tbaa !6
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_real_buf, i32 %37)
  %38 = load float, float* %call25, align 4, !tbaa !8
  %add26 = fadd float %38, %mul24
  store float %add26, float* %call25, align 4, !tbaa !8
  br label %for.inc27

for.inc27:                                        ; preds = %for.body16
  %39 = load i32, i32* %i10, align 4, !tbaa !6
  %inc28 = add nsw i32 %39, 1
  store i32 %inc28, i32* %i10, align 4, !tbaa !6
  br label %for.cond11

for.end29:                                        ; preds = %for.cond.cleanup15
  %40 = bitcast float* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #7
  %41 = bitcast float* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RKf(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, float* nonnull align 4 dereferenceable(4) %k) #3 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %k.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp22 = alloca float, align 4
  %ref.tmp26 = alloca float, align 4
  %ref.tmp30 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  store float* %k, float** %k.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 0)
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call)
  %3 = load float, float* %call1, align 4, !tbaa !8
  %4 = load float*, float** %k.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !8
  %mul = fmul float %3, %5
  store float %mul, float* %ref.tmp, align 4, !tbaa !8
  %6 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 0)
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call3)
  %8 = load float, float* %call4, align 4, !tbaa !8
  %9 = load float*, float** %k.addr, align 4, !tbaa !2
  %10 = load float, float* %9, align 4, !tbaa !8
  %mul5 = fmul float %8, %10
  store float %mul5, float* %ref.tmp2, align 4, !tbaa !8
  %11 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #7
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 0)
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call7)
  %13 = load float, float* %call8, align 4, !tbaa !8
  %14 = load float*, float** %k.addr, align 4, !tbaa !2
  %15 = load float, float* %14, align 4, !tbaa !8
  %mul9 = fmul float %13, %15
  store float %mul9, float* %ref.tmp6, align 4, !tbaa !8
  %16 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #7
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %17, i32 1)
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call11)
  %18 = load float, float* %call12, align 4, !tbaa !8
  %19 = load float*, float** %k.addr, align 4, !tbaa !2
  %20 = load float, float* %19, align 4, !tbaa !8
  %mul13 = fmul float %18, %20
  store float %mul13, float* %ref.tmp10, align 4, !tbaa !8
  %21 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #7
  %22 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %22, i32 1)
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call15)
  %23 = load float, float* %call16, align 4, !tbaa !8
  %24 = load float*, float** %k.addr, align 4, !tbaa !2
  %25 = load float, float* %24, align 4, !tbaa !8
  %mul17 = fmul float %23, %25
  store float %mul17, float* %ref.tmp14, align 4, !tbaa !8
  %26 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #7
  %27 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call19 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %27, i32 1)
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call19)
  %28 = load float, float* %call20, align 4, !tbaa !8
  %29 = load float*, float** %k.addr, align 4, !tbaa !2
  %30 = load float, float* %29, align 4, !tbaa !8
  %mul21 = fmul float %28, %30
  store float %mul21, float* %ref.tmp18, align 4, !tbaa !8
  %31 = bitcast float* %ref.tmp22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #7
  %32 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call23 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %32, i32 2)
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call23)
  %33 = load float, float* %call24, align 4, !tbaa !8
  %34 = load float*, float** %k.addr, align 4, !tbaa !2
  %35 = load float, float* %34, align 4, !tbaa !8
  %mul25 = fmul float %33, %35
  store float %mul25, float* %ref.tmp22, align 4, !tbaa !8
  %36 = bitcast float* %ref.tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #7
  %37 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call27 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %37, i32 2)
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call27)
  %38 = load float, float* %call28, align 4, !tbaa !8
  %39 = load float*, float** %k.addr, align 4, !tbaa !2
  %40 = load float, float* %39, align 4, !tbaa !8
  %mul29 = fmul float %38, %40
  store float %mul29, float* %ref.tmp26, align 4, !tbaa !8
  %41 = bitcast float* %ref.tmp30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #7
  %42 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call31 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %42, i32 2)
  %call32 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call31)
  %43 = load float, float* %call32, align 4, !tbaa !8
  %44 = load float*, float** %k.addr, align 4, !tbaa !2
  %45 = load float, float* %44, align 4, !tbaa !8
  %mul33 = fmul float %43, %45
  store float %mul33, float* %ref.tmp30, align 4, !tbaa !8
  %call34 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp22, float* nonnull align 4 dereferenceable(4) %ref.tmp26, float* nonnull align 4 dereferenceable(4) %ref.tmp30)
  %46 = bitcast float* %ref.tmp30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #7
  %47 = bitcast float* %ref.tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #7
  %48 = bitcast float* %ref.tmp22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #7
  %49 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #7
  %50 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #7
  %51 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #7
  %52 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #7
  %53 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #7
  %54 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x37inverseEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #3 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %co = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %det = alloca float, align 4
  %s = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp26 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %1 = bitcast %class.btVector3* %co to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #7
  %2 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %call = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 1, i32 2, i32 2)
  store float %call, float* %ref.tmp, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %call3 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 2, i32 2, i32 0)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !8
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %call5 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 0, i32 2, i32 1)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !8
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %co, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %5 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #7
  %6 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #7
  %7 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  %8 = bitcast float* %det to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this1, i32 0)
  %call8 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call7, %class.btVector3* nonnull align 4 dereferenceable(16) %co)
  store float %call8, float* %det, align 4, !tbaa !8
  %9 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %10 = load float, float* %det, align 4, !tbaa !8
  %div = fdiv float 1.000000e+00, %10
  store float %div, float* %s, align 4, !tbaa !8
  %11 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #7
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %co)
  %12 = load float, float* %call10, align 4, !tbaa !8
  %13 = load float, float* %s, align 4, !tbaa !8
  %mul = fmul float %12, %13
  store float %mul, float* %ref.tmp9, align 4, !tbaa !8
  %14 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #7
  %call12 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 2, i32 2, i32 1)
  %15 = load float, float* %s, align 4, !tbaa !8
  %mul13 = fmul float %call12, %15
  store float %mul13, float* %ref.tmp11, align 4, !tbaa !8
  %16 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #7
  %call15 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 1, i32 1, i32 2)
  %17 = load float, float* %s, align 4, !tbaa !8
  %mul16 = fmul float %call15, %17
  store float %mul16, float* %ref.tmp14, align 4, !tbaa !8
  %18 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #7
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %co)
  %19 = load float, float* %call18, align 4, !tbaa !8
  %20 = load float, float* %s, align 4, !tbaa !8
  %mul19 = fmul float %19, %20
  store float %mul19, float* %ref.tmp17, align 4, !tbaa !8
  %21 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #7
  %call21 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 0, i32 2, i32 2)
  %22 = load float, float* %s, align 4, !tbaa !8
  %mul22 = fmul float %call21, %22
  store float %mul22, float* %ref.tmp20, align 4, !tbaa !8
  %23 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #7
  %call24 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 2, i32 1, i32 0)
  %24 = load float, float* %s, align 4, !tbaa !8
  %mul25 = fmul float %call24, %24
  store float %mul25, float* %ref.tmp23, align 4, !tbaa !8
  %25 = bitcast float* %ref.tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #7
  %call27 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %co)
  %26 = load float, float* %call27, align 4, !tbaa !8
  %27 = load float, float* %s, align 4, !tbaa !8
  %mul28 = fmul float %26, %27
  store float %mul28, float* %ref.tmp26, align 4, !tbaa !8
  %28 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #7
  %call30 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 1, i32 2, i32 0)
  %29 = load float, float* %s, align 4, !tbaa !8
  %mul31 = fmul float %call30, %29
  store float %mul31, float* %ref.tmp29, align 4, !tbaa !8
  %30 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #7
  %call33 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 0, i32 1, i32 1)
  %31 = load float, float* %s, align 4, !tbaa !8
  %mul34 = fmul float %call33, %31
  store float %mul34, float* %ref.tmp32, align 4, !tbaa !8
  %call35 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp26, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  %32 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #7
  %33 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #7
  %34 = bitcast float* %ref.tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #7
  %35 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #7
  %36 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #7
  %37 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #7
  %38 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #7
  %39 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #7
  %40 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #7
  %41 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #7
  %42 = bitcast float* %det to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #7
  %43 = bitcast %class.btVector3* %co to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %43) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

define hidden void @_ZNK11btMultiBody22calcAccelerationDeltasEPKfPfR20btAlignedObjectArrayIfERS3_I9btVector3E(%class.btMultiBody* %this, float* %force, float* %output, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17) %scratch_r, %class.btAlignedObjectArray.8* nonnull align 4 dereferenceable(17) %scratch_v) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %force.addr = alloca float*, align 4
  %output.addr = alloca float*, align 4
  %scratch_r.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %scratch_v.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %num_links = alloca i32, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %r_ptr = alloca float*, align 4
  %v_ptr = alloca %class.btVector3*, align 4
  %zero_acc_top_angular = alloca %class.btVector3*, align 4
  %zero_acc_bottom_linear = alloca %class.btVector3*, align 4
  %rot_from_parent = alloca %class.btMatrix3x3*, align 4
  %h_top = alloca %class.btVector3*, align 4
  %h_bottom = alloca %class.btVector3*, align 4
  %accel_top = alloca %class.btVector3*, align 4
  %accel_bottom = alloca %class.btVector3*, align 4
  %Y = alloca float*, align 4
  %D = alloca float*, align 4
  %input_force = alloca %class.btVector3, align 4
  %input_torque = alloca %class.btVector3, align 4
  %ref.tmp42 = alloca %class.btVector3, align 4
  %ref.tmp43 = alloca float, align 4
  %ref.tmp44 = alloca float, align 4
  %ref.tmp45 = alloca float, align 4
  %ref.tmp49 = alloca %class.btVector3, align 4
  %ref.tmp50 = alloca %class.btVector3, align 4
  %ref.tmp53 = alloca %class.btVector3, align 4
  %ref.tmp54 = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  %ref.tmp58 = alloca %class.btVector3, align 4
  %ref.tmp59 = alloca float, align 4
  %ref.tmp60 = alloca float, align 4
  %ref.tmp61 = alloca float, align 4
  %i67 = alloca i32, align 4
  %parent = alloca i32, align 4
  %in_top = alloca %class.btVector3, align 4
  %in_bottom = alloca %class.btVector3, align 4
  %out_top = alloca %class.btVector3, align 4
  %out_bottom = alloca %class.btVector3, align 4
  %Y_over_D = alloca float, align 4
  %ref.tmp94 = alloca %class.btVector3, align 4
  %ref.tmp97 = alloca %class.btVector3, align 4
  %ref.tmp99 = alloca %class.btVector3, align 4
  %ref.tmp102 = alloca %class.btVector3, align 4
  %joint_accel = alloca float*, align 4
  %ref.tmp120 = alloca %class.btVector3, align 4
  %ref.tmp121 = alloca float, align 4
  %ref.tmp122 = alloca float, align 4
  %ref.tmp123 = alloca float, align 4
  %rhs_top = alloca %class.btVector3, align 4
  %rhs_bot = alloca %class.btVector3, align 4
  %result = alloca [6 x float], align 16
  %i148 = alloca i32, align 4
  %i168 = alloca i32, align 4
  %parent173 = alloca i32, align 4
  %ref.tmp202 = alloca %class.btVector3, align 4
  %ref.tmp210 = alloca %class.btVector3, align 4
  %omegadot_out = alloca %class.btVector3, align 4
  %ref.tmp222 = alloca %class.btVector3, align 4
  %ref.tmp223 = alloca %class.btMatrix3x3, align 4
  %vdot_out = alloca %class.btVector3, align 4
  %ref.tmp236 = alloca %class.btVector3, align 4
  %ref.tmp237 = alloca %class.btMatrix3x3, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store float* %force, float** %force.addr, align 4, !tbaa !2
  store float* %output, float** %output.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.4* %scratch_r, %class.btAlignedObjectArray.4** %scratch_r.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.8* %scratch_v, %class.btAlignedObjectArray.8** %scratch_v.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = bitcast i32* %num_links to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %this1)
  store i32 %call, i32* %num_links, align 4, !tbaa !6
  %1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %scratch_r.addr, align 4, !tbaa !2
  %2 = load i32, i32* %num_links, align 4, !tbaa !6
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.4* %1, i32 %2, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %4 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #7
  %5 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %scratch_v.addr, align 4, !tbaa !2
  %6 = load i32, i32* %num_links, align 4, !tbaa !6
  %mul = mul nsw i32 4, %6
  %add = add nsw i32 %mul, 4
  %7 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #7
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp2)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.8* %5, i32 %add, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %8 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #7
  %9 = bitcast float** %r_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %10 = load i32, i32* %num_links, align 4, !tbaa !6
  %cmp = icmp eq i32 %10, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %11 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %scratch_r.addr, align 4, !tbaa !2
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %11, i32 0)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float* [ null, %cond.true ], [ %call4, %cond.false ]
  store float* %cond, float** %r_ptr, align 4, !tbaa !2
  %12 = bitcast %class.btVector3** %v_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #7
  %13 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %scratch_v.addr, align 4, !tbaa !2
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %13, i32 0)
  store %class.btVector3* %call5, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %14 = bitcast %class.btVector3** %zero_acc_top_angular to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #7
  %15 = load %class.btVector3*, %class.btVector3** %v_ptr, align 4, !tbaa !2
  store %class.btVector3* %15, %class.btVector3** %zero_acc_top_angular, align 4, !tbaa !2
  %16 = load i32, i32* %num_links, align 4, !tbaa !6
  %add6 = add nsw i32 %16, 1
  %17 = load %class.btVector3*, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds %class.btVector3, %class.btVector3* %17, i32 %add6
  store %class.btVector3* %add.ptr, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %18 = bitcast %class.btVector3** %zero_acc_bottom_linear to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #7
  %19 = load %class.btVector3*, %class.btVector3** %v_ptr, align 4, !tbaa !2
  store %class.btVector3* %19, %class.btVector3** %zero_acc_bottom_linear, align 4, !tbaa !2
  %20 = load i32, i32* %num_links, align 4, !tbaa !6
  %add7 = add nsw i32 %20, 1
  %21 = load %class.btVector3*, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %add.ptr8 = getelementptr inbounds %class.btVector3, %class.btVector3* %21, i32 %add7
  store %class.btVector3* %add.ptr8, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %22 = bitcast %class.btMatrix3x3** %rot_from_parent to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #7
  %matrix_buf = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 11
  %call9 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK20btAlignedObjectArrayI11btMatrix3x3EixEi(%class.btAlignedObjectArray.12* %matrix_buf, i32 0)
  store %class.btMatrix3x3* %call9, %class.btMatrix3x3** %rot_from_parent, align 4, !tbaa !2
  %23 = bitcast %class.btVector3** %h_top to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #7
  %24 = load i32, i32* %num_links, align 4, !tbaa !6
  %cmp10 = icmp sgt i32 %24, 0
  br i1 %cmp10, label %cond.true11, label %cond.false13

cond.true11:                                      ; preds = %cond.end
  %vector_buf = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 10
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vector_buf, i32 0)
  br label %cond.end14

cond.false13:                                     ; preds = %cond.end
  br label %cond.end14

cond.end14:                                       ; preds = %cond.false13, %cond.true11
  %cond15 = phi %class.btVector3* [ %call12, %cond.true11 ], [ null, %cond.false13 ]
  store %class.btVector3* %cond15, %class.btVector3** %h_top, align 4, !tbaa !2
  %25 = bitcast %class.btVector3** %h_bottom to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #7
  %26 = load i32, i32* %num_links, align 4, !tbaa !6
  %cmp16 = icmp sgt i32 %26, 0
  br i1 %cmp16, label %cond.true17, label %cond.false20

cond.true17:                                      ; preds = %cond.end14
  %vector_buf18 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 10
  %27 = load i32, i32* %num_links, align 4, !tbaa !6
  %call19 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vector_buf18, i32 %27)
  br label %cond.end21

cond.false20:                                     ; preds = %cond.end14
  br label %cond.end21

cond.end21:                                       ; preds = %cond.false20, %cond.true17
  %cond22 = phi %class.btVector3* [ %call19, %cond.true17 ], [ null, %cond.false20 ]
  store %class.btVector3* %cond22, %class.btVector3** %h_bottom, align 4, !tbaa !2
  %28 = bitcast %class.btVector3** %accel_top to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #7
  %29 = load %class.btVector3*, %class.btVector3** %v_ptr, align 4, !tbaa !2
  store %class.btVector3* %29, %class.btVector3** %accel_top, align 4, !tbaa !2
  %30 = load i32, i32* %num_links, align 4, !tbaa !6
  %add23 = add nsw i32 %30, 1
  %31 = load %class.btVector3*, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %add.ptr24 = getelementptr inbounds %class.btVector3, %class.btVector3* %31, i32 %add23
  store %class.btVector3* %add.ptr24, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %32 = bitcast %class.btVector3** %accel_bottom to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #7
  %33 = load %class.btVector3*, %class.btVector3** %v_ptr, align 4, !tbaa !2
  store %class.btVector3* %33, %class.btVector3** %accel_bottom, align 4, !tbaa !2
  %34 = load i32, i32* %num_links, align 4, !tbaa !6
  %add25 = add nsw i32 %34, 1
  %35 = load %class.btVector3*, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %add.ptr26 = getelementptr inbounds %class.btVector3, %class.btVector3* %35, i32 %add25
  store %class.btVector3* %add.ptr26, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %36 = bitcast float** %Y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #7
  %37 = load float*, float** %r_ptr, align 4, !tbaa !2
  store float* %37, float** %Y, align 4, !tbaa !2
  %38 = load i32, i32* %num_links, align 4, !tbaa !6
  %39 = load float*, float** %r_ptr, align 4, !tbaa !2
  %add.ptr27 = getelementptr inbounds float, float* %39, i32 %38
  store float* %add.ptr27, float** %r_ptr, align 4, !tbaa !2
  %40 = bitcast float** %D to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #7
  %41 = load i32, i32* %num_links, align 4, !tbaa !6
  %cmp28 = icmp sgt i32 %41, 0
  br i1 %cmp28, label %cond.true29, label %cond.false32

cond.true29:                                      ; preds = %cond.end21
  %m_real_buf = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 9
  %42 = load i32, i32* %num_links, align 4, !tbaa !6
  %add30 = add nsw i32 6, %42
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_real_buf, i32 %add30)
  br label %cond.end33

cond.false32:                                     ; preds = %cond.end21
  br label %cond.end33

cond.end33:                                       ; preds = %cond.false32, %cond.true29
  %cond34 = phi float* [ %call31, %cond.true29 ], [ null, %cond.false32 ]
  store float* %cond34, float** %D, align 4, !tbaa !2
  %43 = bitcast %class.btVector3* %input_force to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %43) #7
  %44 = load float*, float** %force.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds float, float* %44, i32 3
  %45 = load float*, float** %force.addr, align 4, !tbaa !2
  %arrayidx35 = getelementptr inbounds float, float* %45, i32 4
  %46 = load float*, float** %force.addr, align 4, !tbaa !2
  %arrayidx36 = getelementptr inbounds float, float* %46, i32 5
  %call37 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %input_force, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx35, float* nonnull align 4 dereferenceable(4) %arrayidx36)
  %47 = bitcast %class.btVector3* %input_torque to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %47) #7
  %48 = load float*, float** %force.addr, align 4, !tbaa !2
  %arrayidx38 = getelementptr inbounds float, float* %48, i32 0
  %49 = load float*, float** %force.addr, align 4, !tbaa !2
  %arrayidx39 = getelementptr inbounds float, float* %49, i32 1
  %50 = load float*, float** %force.addr, align 4, !tbaa !2
  %arrayidx40 = getelementptr inbounds float, float* %50, i32 2
  %call41 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %input_torque, float* nonnull align 4 dereferenceable(4) %arrayidx38, float* nonnull align 4 dereferenceable(4) %arrayidx39, float* nonnull align 4 dereferenceable(4) %arrayidx40)
  %fixed_base = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 16
  %51 = load i8, i8* %fixed_base, align 4, !tbaa !31, !range !30
  %tobool = trunc i8 %51 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %cond.end33
  %52 = bitcast %class.btVector3* %ref.tmp42 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %52) #7
  %53 = bitcast float* %ref.tmp43 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #7
  store float 0.000000e+00, float* %ref.tmp43, align 4, !tbaa !8
  %54 = bitcast float* %ref.tmp44 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #7
  store float 0.000000e+00, float* %ref.tmp44, align 4, !tbaa !8
  %55 = bitcast float* %ref.tmp45 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #7
  store float 0.000000e+00, float* %ref.tmp45, align 4, !tbaa !8
  %call46 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp42, float* nonnull align 4 dereferenceable(4) %ref.tmp43, float* nonnull align 4 dereferenceable(4) %ref.tmp44, float* nonnull align 4 dereferenceable(4) %ref.tmp45)
  %56 = load %class.btVector3*, %class.btVector3** %zero_acc_bottom_linear, align 4, !tbaa !2
  %arrayidx47 = getelementptr inbounds %class.btVector3, %class.btVector3* %56, i32 0
  %57 = bitcast %class.btVector3* %arrayidx47 to i8*
  %58 = bitcast %class.btVector3* %ref.tmp42 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %57, i8* align 4 %58, i32 16, i1 false), !tbaa.struct !28
  %59 = load %class.btVector3*, %class.btVector3** %zero_acc_top_angular, align 4, !tbaa !2
  %arrayidx48 = getelementptr inbounds %class.btVector3, %class.btVector3* %59, i32 0
  %60 = bitcast %class.btVector3* %arrayidx48 to i8*
  %61 = bitcast %class.btVector3* %arrayidx47 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %60, i8* align 4 %61, i32 16, i1 false), !tbaa.struct !28
  %62 = bitcast float* %ref.tmp45 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #7
  %63 = bitcast float* %ref.tmp44 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #7
  %64 = bitcast float* %ref.tmp43 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #7
  %65 = bitcast %class.btVector3* %ref.tmp42 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %65) #7
  br label %if.end

if.else:                                          ; preds = %cond.end33
  %66 = bitcast %class.btVector3* %ref.tmp49 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %66) #7
  %67 = bitcast %class.btVector3* %ref.tmp50 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %67) #7
  %68 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_parent, align 4, !tbaa !2
  %arrayidx51 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %68, i32 0
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp50, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx51, %class.btVector3* nonnull align 4 dereferenceable(16) %input_force)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp49, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp50)
  %69 = load %class.btVector3*, %class.btVector3** %zero_acc_top_angular, align 4, !tbaa !2
  %arrayidx52 = getelementptr inbounds %class.btVector3, %class.btVector3* %69, i32 0
  %70 = bitcast %class.btVector3* %arrayidx52 to i8*
  %71 = bitcast %class.btVector3* %ref.tmp49 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %70, i8* align 4 %71, i32 16, i1 false), !tbaa.struct !28
  %72 = bitcast %class.btVector3* %ref.tmp50 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %72) #7
  %73 = bitcast %class.btVector3* %ref.tmp49 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %73) #7
  %74 = bitcast %class.btVector3* %ref.tmp53 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %74) #7
  %75 = bitcast %class.btVector3* %ref.tmp54 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %75) #7
  %76 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_parent, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %76, i32 0
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp54, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx55, %class.btVector3* nonnull align 4 dereferenceable(16) %input_torque)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp53, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp54)
  %77 = load %class.btVector3*, %class.btVector3** %zero_acc_bottom_linear, align 4, !tbaa !2
  %arrayidx56 = getelementptr inbounds %class.btVector3, %class.btVector3* %77, i32 0
  %78 = bitcast %class.btVector3* %arrayidx56 to i8*
  %79 = bitcast %class.btVector3* %ref.tmp53 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %78, i8* align 4 %79, i32 16, i1 false), !tbaa.struct !28
  %80 = bitcast %class.btVector3* %ref.tmp54 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %80) #7
  %81 = bitcast %class.btVector3* %ref.tmp53 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %81) #7
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %82 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %83 = load i32, i32* %i, align 4, !tbaa !6
  %84 = load i32, i32* %num_links, align 4, !tbaa !6
  %cmp57 = icmp slt i32 %83, %84
  br i1 %cmp57, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %85 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %86 = bitcast %class.btVector3* %ref.tmp58 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %86) #7
  %87 = bitcast float* %ref.tmp59 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %87) #7
  store float 0.000000e+00, float* %ref.tmp59, align 4, !tbaa !8
  %88 = bitcast float* %ref.tmp60 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #7
  store float 0.000000e+00, float* %ref.tmp60, align 4, !tbaa !8
  %89 = bitcast float* %ref.tmp61 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %89) #7
  store float 0.000000e+00, float* %ref.tmp61, align 4, !tbaa !8
  %call62 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp58, float* nonnull align 4 dereferenceable(4) %ref.tmp59, float* nonnull align 4 dereferenceable(4) %ref.tmp60, float* nonnull align 4 dereferenceable(4) %ref.tmp61)
  %90 = load %class.btVector3*, %class.btVector3** %zero_acc_bottom_linear, align 4, !tbaa !2
  %91 = load i32, i32* %i, align 4, !tbaa !6
  %add63 = add nsw i32 %91, 1
  %arrayidx64 = getelementptr inbounds %class.btVector3, %class.btVector3* %90, i32 %add63
  %92 = bitcast %class.btVector3* %arrayidx64 to i8*
  %93 = bitcast %class.btVector3* %ref.tmp58 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %92, i8* align 4 %93, i32 16, i1 false), !tbaa.struct !28
  %94 = load %class.btVector3*, %class.btVector3** %zero_acc_top_angular, align 4, !tbaa !2
  %95 = load i32, i32* %i, align 4, !tbaa !6
  %add65 = add nsw i32 %95, 1
  %arrayidx66 = getelementptr inbounds %class.btVector3, %class.btVector3* %94, i32 %add65
  %96 = bitcast %class.btVector3* %arrayidx66 to i8*
  %97 = bitcast %class.btVector3* %arrayidx64 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %96, i8* align 4 %97, i32 16, i1 false), !tbaa.struct !28
  %98 = bitcast float* %ref.tmp61 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #7
  %99 = bitcast float* %ref.tmp60 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #7
  %100 = bitcast float* %ref.tmp59 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #7
  %101 = bitcast %class.btVector3* %ref.tmp58 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %101) #7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %102 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %102, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %103 = bitcast i32* %i67 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %103) #7
  %104 = load i32, i32* %num_links, align 4, !tbaa !6
  %sub = sub nsw i32 %104, 1
  store i32 %sub, i32* %i67, align 4, !tbaa !6
  br label %for.cond68

for.cond68:                                       ; preds = %for.inc114, %for.end
  %105 = load i32, i32* %i67, align 4, !tbaa !6
  %cmp69 = icmp sge i32 %105, 0
  br i1 %cmp69, label %for.body71, label %for.cond.cleanup70

for.cond.cleanup70:                               ; preds = %for.cond68
  %106 = bitcast i32* %i67 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #7
  br label %for.end115

for.body71:                                       ; preds = %for.cond68
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %107 = load i32, i32* %i67, align 4, !tbaa !6
  %call72 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links, i32 %107)
  %axis_top = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call72, i32 0, i32 5
  %links73 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %108 = load i32, i32* %i67, align 4, !tbaa !6
  %call74 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links73, i32 %108)
  %axis_bottom = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call74, i32 0, i32 6
  %109 = load %class.btVector3*, %class.btVector3** %zero_acc_top_angular, align 4, !tbaa !2
  %110 = load i32, i32* %i67, align 4, !tbaa !6
  %add75 = add nsw i32 %110, 1
  %arrayidx76 = getelementptr inbounds %class.btVector3, %class.btVector3* %109, i32 %add75
  %111 = load %class.btVector3*, %class.btVector3** %zero_acc_bottom_linear, align 4, !tbaa !2
  %112 = load i32, i32* %i67, align 4, !tbaa !6
  %add77 = add nsw i32 %112, 1
  %arrayidx78 = getelementptr inbounds %class.btVector3, %class.btVector3* %111, i32 %add77
  %call79 = call float @_ZN12_GLOBAL__N_117SpatialDotProductERK9btVector3S2_S2_S2_(%class.btVector3* nonnull align 4 dereferenceable(16) %axis_top, %class.btVector3* nonnull align 4 dereferenceable(16) %axis_bottom, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx76, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx78)
  %fneg = fneg float %call79
  %113 = load float*, float** %Y, align 4, !tbaa !2
  %114 = load i32, i32* %i67, align 4, !tbaa !6
  %arrayidx80 = getelementptr inbounds float, float* %113, i32 %114
  store float %fneg, float* %arrayidx80, align 4, !tbaa !8
  %115 = load float*, float** %force.addr, align 4, !tbaa !2
  %116 = load i32, i32* %i67, align 4, !tbaa !6
  %add81 = add nsw i32 6, %116
  %arrayidx82 = getelementptr inbounds float, float* %115, i32 %add81
  %117 = load float, float* %arrayidx82, align 4, !tbaa !8
  %118 = load float*, float** %Y, align 4, !tbaa !2
  %119 = load i32, i32* %i67, align 4, !tbaa !6
  %arrayidx83 = getelementptr inbounds float, float* %118, i32 %119
  %120 = load float, float* %arrayidx83, align 4, !tbaa !8
  %add84 = fadd float %120, %117
  store float %add84, float* %arrayidx83, align 4, !tbaa !8
  %121 = bitcast i32* %parent to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %121) #7
  %links85 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %122 = load i32, i32* %i67, align 4, !tbaa !6
  %call86 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links85, i32 %122)
  %parent87 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call86, i32 0, i32 3
  %123 = load i32, i32* %parent87, align 4, !tbaa !45
  store i32 %123, i32* %parent, align 4, !tbaa !6
  %124 = bitcast %class.btVector3* %in_top to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %124) #7
  %call88 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %in_top)
  %125 = bitcast %class.btVector3* %in_bottom to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %125) #7
  %call89 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %in_bottom)
  %126 = bitcast %class.btVector3* %out_top to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %126) #7
  %call90 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %out_top)
  %127 = bitcast %class.btVector3* %out_bottom to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %127) #7
  %call91 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %out_bottom)
  %128 = bitcast float* %Y_over_D to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %128) #7
  %129 = load float*, float** %Y, align 4, !tbaa !2
  %130 = load i32, i32* %i67, align 4, !tbaa !6
  %arrayidx92 = getelementptr inbounds float, float* %129, i32 %130
  %131 = load float, float* %arrayidx92, align 4, !tbaa !8
  %132 = load float*, float** %D, align 4, !tbaa !2
  %133 = load i32, i32* %i67, align 4, !tbaa !6
  %arrayidx93 = getelementptr inbounds float, float* %132, i32 %133
  %134 = load float, float* %arrayidx93, align 4, !tbaa !8
  %div = fdiv float %131, %134
  store float %div, float* %Y_over_D, align 4, !tbaa !8
  %135 = bitcast %class.btVector3* %ref.tmp94 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %135) #7
  %136 = load %class.btVector3*, %class.btVector3** %zero_acc_top_angular, align 4, !tbaa !2
  %137 = load i32, i32* %i67, align 4, !tbaa !6
  %add95 = add nsw i32 %137, 1
  %arrayidx96 = getelementptr inbounds %class.btVector3, %class.btVector3* %136, i32 %add95
  %138 = bitcast %class.btVector3* %ref.tmp97 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %138) #7
  %139 = load %class.btVector3*, %class.btVector3** %h_top, align 4, !tbaa !2
  %140 = load i32, i32* %i67, align 4, !tbaa !6
  %arrayidx98 = getelementptr inbounds %class.btVector3, %class.btVector3* %139, i32 %140
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp97, float* nonnull align 4 dereferenceable(4) %Y_over_D, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx98)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp94, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx96, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp97)
  %141 = bitcast %class.btVector3* %in_top to i8*
  %142 = bitcast %class.btVector3* %ref.tmp94 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %141, i8* align 4 %142, i32 16, i1 false), !tbaa.struct !28
  %143 = bitcast %class.btVector3* %ref.tmp97 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %143) #7
  %144 = bitcast %class.btVector3* %ref.tmp94 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %144) #7
  %145 = bitcast %class.btVector3* %ref.tmp99 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %145) #7
  %146 = load %class.btVector3*, %class.btVector3** %zero_acc_bottom_linear, align 4, !tbaa !2
  %147 = load i32, i32* %i67, align 4, !tbaa !6
  %add100 = add nsw i32 %147, 1
  %arrayidx101 = getelementptr inbounds %class.btVector3, %class.btVector3* %146, i32 %add100
  %148 = bitcast %class.btVector3* %ref.tmp102 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %148) #7
  %149 = load %class.btVector3*, %class.btVector3** %h_bottom, align 4, !tbaa !2
  %150 = load i32, i32* %i67, align 4, !tbaa !6
  %arrayidx103 = getelementptr inbounds %class.btVector3, %class.btVector3* %149, i32 %150
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp102, float* nonnull align 4 dereferenceable(4) %Y_over_D, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx103)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp99, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx101, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp102)
  %151 = bitcast %class.btVector3* %in_bottom to i8*
  %152 = bitcast %class.btVector3* %ref.tmp99 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %151, i8* align 4 %152, i32 16, i1 false), !tbaa.struct !28
  %153 = bitcast %class.btVector3* %ref.tmp102 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %153) #7
  %154 = bitcast %class.btVector3* %ref.tmp99 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %154) #7
  %155 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_parent, align 4, !tbaa !2
  %156 = load i32, i32* %i67, align 4, !tbaa !6
  %add104 = add nsw i32 %156, 1
  %arrayidx105 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %155, i32 %add104
  %links106 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %157 = load i32, i32* %i67, align 4, !tbaa !6
  %call107 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links106, i32 %157)
  %cached_r_vector = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call107, i32 0, i32 11
  call void @_ZN12_GLOBAL__N_123InverseSpatialTransformERK11btMatrix3x3RK9btVector3S5_S5_RS3_S6_(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx105, %class.btVector3* nonnull align 4 dereferenceable(16) %cached_r_vector, %class.btVector3* nonnull align 4 dereferenceable(16) %in_top, %class.btVector3* nonnull align 4 dereferenceable(16) %in_bottom, %class.btVector3* nonnull align 4 dereferenceable(16) %out_top, %class.btVector3* nonnull align 4 dereferenceable(16) %out_bottom)
  %158 = load %class.btVector3*, %class.btVector3** %zero_acc_top_angular, align 4, !tbaa !2
  %159 = load i32, i32* %parent, align 4, !tbaa !6
  %add108 = add nsw i32 %159, 1
  %arrayidx109 = getelementptr inbounds %class.btVector3, %class.btVector3* %158, i32 %add108
  %call110 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %arrayidx109, %class.btVector3* nonnull align 4 dereferenceable(16) %out_top)
  %160 = load %class.btVector3*, %class.btVector3** %zero_acc_bottom_linear, align 4, !tbaa !2
  %161 = load i32, i32* %parent, align 4, !tbaa !6
  %add111 = add nsw i32 %161, 1
  %arrayidx112 = getelementptr inbounds %class.btVector3, %class.btVector3* %160, i32 %add111
  %call113 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %arrayidx112, %class.btVector3* nonnull align 4 dereferenceable(16) %out_bottom)
  %162 = bitcast float* %Y_over_D to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %162) #7
  %163 = bitcast %class.btVector3* %out_bottom to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %163) #7
  %164 = bitcast %class.btVector3* %out_top to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %164) #7
  %165 = bitcast %class.btVector3* %in_bottom to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %165) #7
  %166 = bitcast %class.btVector3* %in_top to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %166) #7
  %167 = bitcast i32* %parent to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %167) #7
  br label %for.inc114

for.inc114:                                       ; preds = %for.body71
  %168 = load i32, i32* %i67, align 4, !tbaa !6
  %dec = add nsw i32 %168, -1
  store i32 %dec, i32* %i67, align 4, !tbaa !6
  br label %for.cond68

for.end115:                                       ; preds = %for.cond.cleanup70
  %169 = bitcast float** %joint_accel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %169) #7
  %170 = load float*, float** %output.addr, align 4, !tbaa !2
  %add.ptr116 = getelementptr inbounds float, float* %170, i32 6
  store float* %add.ptr116, float** %joint_accel, align 4, !tbaa !2
  %fixed_base117 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 16
  %171 = load i8, i8* %fixed_base117, align 4, !tbaa !31, !range !30
  %tobool118 = trunc i8 %171 to i1
  br i1 %tobool118, label %if.then119, label %if.else127

if.then119:                                       ; preds = %for.end115
  %172 = bitcast %class.btVector3* %ref.tmp120 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %172) #7
  %173 = bitcast float* %ref.tmp121 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %173) #7
  store float 0.000000e+00, float* %ref.tmp121, align 4, !tbaa !8
  %174 = bitcast float* %ref.tmp122 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %174) #7
  store float 0.000000e+00, float* %ref.tmp122, align 4, !tbaa !8
  %175 = bitcast float* %ref.tmp123 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %175) #7
  store float 0.000000e+00, float* %ref.tmp123, align 4, !tbaa !8
  %call124 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp120, float* nonnull align 4 dereferenceable(4) %ref.tmp121, float* nonnull align 4 dereferenceable(4) %ref.tmp122, float* nonnull align 4 dereferenceable(4) %ref.tmp123)
  %176 = load %class.btVector3*, %class.btVector3** %accel_bottom, align 4, !tbaa !2
  %arrayidx125 = getelementptr inbounds %class.btVector3, %class.btVector3* %176, i32 0
  %177 = bitcast %class.btVector3* %arrayidx125 to i8*
  %178 = bitcast %class.btVector3* %ref.tmp120 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %177, i8* align 4 %178, i32 16, i1 false), !tbaa.struct !28
  %179 = load %class.btVector3*, %class.btVector3** %accel_top, align 4, !tbaa !2
  %arrayidx126 = getelementptr inbounds %class.btVector3, %class.btVector3* %179, i32 0
  %180 = bitcast %class.btVector3* %arrayidx126 to i8*
  %181 = bitcast %class.btVector3* %arrayidx125 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %180, i8* align 4 %181, i32 16, i1 false), !tbaa.struct !28
  %182 = bitcast float* %ref.tmp123 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %182) #7
  %183 = bitcast float* %ref.tmp122 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %183) #7
  %184 = bitcast float* %ref.tmp121 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %184) #7
  %185 = bitcast %class.btVector3* %ref.tmp120 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %185) #7
  br label %if.end167

if.else127:                                       ; preds = %for.end115
  %186 = bitcast %class.btVector3* %rhs_top to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %186) #7
  %187 = load %class.btVector3*, %class.btVector3** %zero_acc_top_angular, align 4, !tbaa !2
  %arrayidx128 = getelementptr inbounds %class.btVector3, %class.btVector3* %187, i32 0
  %call129 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx128)
  %arrayidx130 = getelementptr inbounds float, float* %call129, i32 0
  %188 = load %class.btVector3*, %class.btVector3** %zero_acc_top_angular, align 4, !tbaa !2
  %arrayidx131 = getelementptr inbounds %class.btVector3, %class.btVector3* %188, i32 0
  %call132 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx131)
  %arrayidx133 = getelementptr inbounds float, float* %call132, i32 1
  %189 = load %class.btVector3*, %class.btVector3** %zero_acc_top_angular, align 4, !tbaa !2
  %arrayidx134 = getelementptr inbounds %class.btVector3, %class.btVector3* %189, i32 0
  %call135 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx134)
  %arrayidx136 = getelementptr inbounds float, float* %call135, i32 2
  %call137 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %rhs_top, float* nonnull align 4 dereferenceable(4) %arrayidx130, float* nonnull align 4 dereferenceable(4) %arrayidx133, float* nonnull align 4 dereferenceable(4) %arrayidx136)
  %190 = bitcast %class.btVector3* %rhs_bot to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %190) #7
  %191 = load %class.btVector3*, %class.btVector3** %zero_acc_bottom_linear, align 4, !tbaa !2
  %arrayidx138 = getelementptr inbounds %class.btVector3, %class.btVector3* %191, i32 0
  %call139 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx138)
  %arrayidx140 = getelementptr inbounds float, float* %call139, i32 0
  %192 = load %class.btVector3*, %class.btVector3** %zero_acc_bottom_linear, align 4, !tbaa !2
  %arrayidx141 = getelementptr inbounds %class.btVector3, %class.btVector3* %192, i32 0
  %call142 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx141)
  %arrayidx143 = getelementptr inbounds float, float* %call142, i32 1
  %193 = load %class.btVector3*, %class.btVector3** %zero_acc_bottom_linear, align 4, !tbaa !2
  %arrayidx144 = getelementptr inbounds %class.btVector3, %class.btVector3* %193, i32 0
  %call145 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx144)
  %arrayidx146 = getelementptr inbounds float, float* %call145, i32 2
  %call147 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %rhs_bot, float* nonnull align 4 dereferenceable(4) %arrayidx140, float* nonnull align 4 dereferenceable(4) %arrayidx143, float* nonnull align 4 dereferenceable(4) %arrayidx146)
  %194 = bitcast [6 x float]* %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %194) #7
  %arraydecay = getelementptr inbounds [6 x float], [6 x float]* %result, i32 0, i32 0
  call void @_ZNK11btMultiBody12solveImatrixERK9btVector3S2_Pf(%class.btMultiBody* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %rhs_top, %class.btVector3* nonnull align 4 dereferenceable(16) %rhs_bot, float* %arraydecay)
  %195 = bitcast i32* %i148 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %195) #7
  store i32 0, i32* %i148, align 4, !tbaa !6
  br label %for.cond149

for.cond149:                                      ; preds = %for.inc164, %if.else127
  %196 = load i32, i32* %i148, align 4, !tbaa !6
  %cmp150 = icmp slt i32 %196, 3
  br i1 %cmp150, label %for.body152, label %for.cond.cleanup151

for.cond.cleanup151:                              ; preds = %for.cond149
  %197 = bitcast i32* %i148 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %197) #7
  br label %for.end166

for.body152:                                      ; preds = %for.cond149
  %198 = load i32, i32* %i148, align 4, !tbaa !6
  %arrayidx153 = getelementptr inbounds [6 x float], [6 x float]* %result, i32 0, i32 %198
  %199 = load float, float* %arrayidx153, align 4, !tbaa !8
  %fneg154 = fneg float %199
  %200 = load %class.btVector3*, %class.btVector3** %accel_top, align 4, !tbaa !2
  %arrayidx155 = getelementptr inbounds %class.btVector3, %class.btVector3* %200, i32 0
  %call156 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx155)
  %201 = load i32, i32* %i148, align 4, !tbaa !6
  %arrayidx157 = getelementptr inbounds float, float* %call156, i32 %201
  store float %fneg154, float* %arrayidx157, align 4, !tbaa !8
  %202 = load i32, i32* %i148, align 4, !tbaa !6
  %add158 = add nsw i32 %202, 3
  %arrayidx159 = getelementptr inbounds [6 x float], [6 x float]* %result, i32 0, i32 %add158
  %203 = load float, float* %arrayidx159, align 4, !tbaa !8
  %fneg160 = fneg float %203
  %204 = load %class.btVector3*, %class.btVector3** %accel_bottom, align 4, !tbaa !2
  %arrayidx161 = getelementptr inbounds %class.btVector3, %class.btVector3* %204, i32 0
  %call162 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx161)
  %205 = load i32, i32* %i148, align 4, !tbaa !6
  %arrayidx163 = getelementptr inbounds float, float* %call162, i32 %205
  store float %fneg160, float* %arrayidx163, align 4, !tbaa !8
  br label %for.inc164

for.inc164:                                       ; preds = %for.body152
  %206 = load i32, i32* %i148, align 4, !tbaa !6
  %inc165 = add nsw i32 %206, 1
  store i32 %inc165, i32* %i148, align 4, !tbaa !6
  br label %for.cond149

for.end166:                                       ; preds = %for.cond.cleanup151
  %207 = bitcast [6 x float]* %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %207) #7
  %208 = bitcast %class.btVector3* %rhs_bot to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %208) #7
  %209 = bitcast %class.btVector3* %rhs_top to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %209) #7
  br label %if.end167

if.end167:                                        ; preds = %for.end166, %if.then119
  %210 = bitcast i32* %i168 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %210) #7
  store i32 0, i32* %i168, align 4, !tbaa !6
  br label %for.cond169

for.cond169:                                      ; preds = %for.inc218, %if.end167
  %211 = load i32, i32* %i168, align 4, !tbaa !6
  %212 = load i32, i32* %num_links, align 4, !tbaa !6
  %cmp170 = icmp slt i32 %211, %212
  br i1 %cmp170, label %for.body172, label %for.cond.cleanup171

for.cond.cleanup171:                              ; preds = %for.cond169
  %213 = bitcast i32* %i168 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %213) #7
  br label %for.end220

for.body172:                                      ; preds = %for.cond169
  %214 = bitcast i32* %parent173 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %214) #7
  %links174 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %215 = load i32, i32* %i168, align 4, !tbaa !6
  %call175 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links174, i32 %215)
  %parent176 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call175, i32 0, i32 3
  %216 = load i32, i32* %parent176, align 4, !tbaa !45
  store i32 %216, i32* %parent173, align 4, !tbaa !6
  %217 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_parent, align 4, !tbaa !2
  %218 = load i32, i32* %i168, align 4, !tbaa !6
  %add177 = add nsw i32 %218, 1
  %arrayidx178 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %217, i32 %add177
  %links179 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %219 = load i32, i32* %i168, align 4, !tbaa !6
  %call180 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links179, i32 %219)
  %cached_r_vector181 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call180, i32 0, i32 11
  %220 = load %class.btVector3*, %class.btVector3** %accel_top, align 4, !tbaa !2
  %221 = load i32, i32* %parent173, align 4, !tbaa !6
  %add182 = add nsw i32 %221, 1
  %arrayidx183 = getelementptr inbounds %class.btVector3, %class.btVector3* %220, i32 %add182
  %222 = load %class.btVector3*, %class.btVector3** %accel_bottom, align 4, !tbaa !2
  %223 = load i32, i32* %parent173, align 4, !tbaa !6
  %add184 = add nsw i32 %223, 1
  %arrayidx185 = getelementptr inbounds %class.btVector3, %class.btVector3* %222, i32 %add184
  %224 = load %class.btVector3*, %class.btVector3** %accel_top, align 4, !tbaa !2
  %225 = load i32, i32* %i168, align 4, !tbaa !6
  %add186 = add nsw i32 %225, 1
  %arrayidx187 = getelementptr inbounds %class.btVector3, %class.btVector3* %224, i32 %add186
  %226 = load %class.btVector3*, %class.btVector3** %accel_bottom, align 4, !tbaa !2
  %227 = load i32, i32* %i168, align 4, !tbaa !6
  %add188 = add nsw i32 %227, 1
  %arrayidx189 = getelementptr inbounds %class.btVector3, %class.btVector3* %226, i32 %add188
  call void @_ZN12_GLOBAL__N_116SpatialTransformERK11btMatrix3x3RK9btVector3S5_S5_RS3_S6_(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx178, %class.btVector3* nonnull align 4 dereferenceable(16) %cached_r_vector181, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx183, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx185, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx187, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx189)
  %228 = load float*, float** %Y, align 4, !tbaa !2
  %229 = load i32, i32* %i168, align 4, !tbaa !6
  %arrayidx190 = getelementptr inbounds float, float* %228, i32 %229
  %230 = load float, float* %arrayidx190, align 4, !tbaa !8
  %231 = load %class.btVector3*, %class.btVector3** %h_top, align 4, !tbaa !2
  %232 = load i32, i32* %i168, align 4, !tbaa !6
  %arrayidx191 = getelementptr inbounds %class.btVector3, %class.btVector3* %231, i32 %232
  %233 = load %class.btVector3*, %class.btVector3** %h_bottom, align 4, !tbaa !2
  %234 = load i32, i32* %i168, align 4, !tbaa !6
  %arrayidx192 = getelementptr inbounds %class.btVector3, %class.btVector3* %233, i32 %234
  %235 = load %class.btVector3*, %class.btVector3** %accel_top, align 4, !tbaa !2
  %236 = load i32, i32* %i168, align 4, !tbaa !6
  %add193 = add nsw i32 %236, 1
  %arrayidx194 = getelementptr inbounds %class.btVector3, %class.btVector3* %235, i32 %add193
  %237 = load %class.btVector3*, %class.btVector3** %accel_bottom, align 4, !tbaa !2
  %238 = load i32, i32* %i168, align 4, !tbaa !6
  %add195 = add nsw i32 %238, 1
  %arrayidx196 = getelementptr inbounds %class.btVector3, %class.btVector3* %237, i32 %add195
  %call197 = call float @_ZN12_GLOBAL__N_117SpatialDotProductERK9btVector3S2_S2_S2_(%class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx191, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx192, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx194, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx196)
  %sub198 = fsub float %230, %call197
  %239 = load float*, float** %D, align 4, !tbaa !2
  %240 = load i32, i32* %i168, align 4, !tbaa !6
  %arrayidx199 = getelementptr inbounds float, float* %239, i32 %240
  %241 = load float, float* %arrayidx199, align 4, !tbaa !8
  %div200 = fdiv float %sub198, %241
  %242 = load float*, float** %joint_accel, align 4, !tbaa !2
  %243 = load i32, i32* %i168, align 4, !tbaa !6
  %arrayidx201 = getelementptr inbounds float, float* %242, i32 %243
  store float %div200, float* %arrayidx201, align 4, !tbaa !8
  %244 = bitcast %class.btVector3* %ref.tmp202 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %244) #7
  %245 = load float*, float** %joint_accel, align 4, !tbaa !2
  %246 = load i32, i32* %i168, align 4, !tbaa !6
  %arrayidx203 = getelementptr inbounds float, float* %245, i32 %246
  %links204 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %247 = load i32, i32* %i168, align 4, !tbaa !6
  %call205 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links204, i32 %247)
  %axis_top206 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call205, i32 0, i32 5
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp202, float* nonnull align 4 dereferenceable(4) %arrayidx203, %class.btVector3* nonnull align 4 dereferenceable(16) %axis_top206)
  %248 = load %class.btVector3*, %class.btVector3** %accel_top, align 4, !tbaa !2
  %249 = load i32, i32* %i168, align 4, !tbaa !6
  %add207 = add nsw i32 %249, 1
  %arrayidx208 = getelementptr inbounds %class.btVector3, %class.btVector3* %248, i32 %add207
  %call209 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %arrayidx208, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp202)
  %250 = bitcast %class.btVector3* %ref.tmp202 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %250) #7
  %251 = bitcast %class.btVector3* %ref.tmp210 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %251) #7
  %252 = load float*, float** %joint_accel, align 4, !tbaa !2
  %253 = load i32, i32* %i168, align 4, !tbaa !6
  %arrayidx211 = getelementptr inbounds float, float* %252, i32 %253
  %links212 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %254 = load i32, i32* %i168, align 4, !tbaa !6
  %call213 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links212, i32 %254)
  %axis_bottom214 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call213, i32 0, i32 6
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp210, float* nonnull align 4 dereferenceable(4) %arrayidx211, %class.btVector3* nonnull align 4 dereferenceable(16) %axis_bottom214)
  %255 = load %class.btVector3*, %class.btVector3** %accel_bottom, align 4, !tbaa !2
  %256 = load i32, i32* %i168, align 4, !tbaa !6
  %add215 = add nsw i32 %256, 1
  %arrayidx216 = getelementptr inbounds %class.btVector3, %class.btVector3* %255, i32 %add215
  %call217 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %arrayidx216, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp210)
  %257 = bitcast %class.btVector3* %ref.tmp210 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %257) #7
  %258 = bitcast i32* %parent173 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %258) #7
  br label %for.inc218

for.inc218:                                       ; preds = %for.body172
  %259 = load i32, i32* %i168, align 4, !tbaa !6
  %inc219 = add nsw i32 %259, 1
  store i32 %inc219, i32* %i168, align 4, !tbaa !6
  br label %for.cond169

for.end220:                                       ; preds = %for.cond.cleanup171
  %260 = bitcast %class.btVector3* %omegadot_out to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %260) #7
  %call221 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %omegadot_out)
  %261 = bitcast %class.btVector3* %ref.tmp222 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %261) #7
  %262 = bitcast %class.btMatrix3x3* %ref.tmp223 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %262) #7
  %263 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_parent, align 4, !tbaa !2
  %arrayidx224 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %263, i32 0
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp223, %class.btMatrix3x3* %arrayidx224)
  %264 = load %class.btVector3*, %class.btVector3** %accel_top, align 4, !tbaa !2
  %arrayidx225 = getelementptr inbounds %class.btVector3, %class.btVector3* %264, i32 0
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp222, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp223, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx225)
  %265 = bitcast %class.btVector3* %omegadot_out to i8*
  %266 = bitcast %class.btVector3* %ref.tmp222 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %265, i8* align 4 %266, i32 16, i1 false), !tbaa.struct !28
  %267 = bitcast %class.btMatrix3x3* %ref.tmp223 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %267) #7
  %268 = bitcast %class.btVector3* %ref.tmp222 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %268) #7
  %call226 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %omegadot_out)
  %arrayidx227 = getelementptr inbounds float, float* %call226, i32 0
  %269 = load float, float* %arrayidx227, align 4, !tbaa !8
  %270 = load float*, float** %output.addr, align 4, !tbaa !2
  %arrayidx228 = getelementptr inbounds float, float* %270, i32 0
  store float %269, float* %arrayidx228, align 4, !tbaa !8
  %call229 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %omegadot_out)
  %arrayidx230 = getelementptr inbounds float, float* %call229, i32 1
  %271 = load float, float* %arrayidx230, align 4, !tbaa !8
  %272 = load float*, float** %output.addr, align 4, !tbaa !2
  %arrayidx231 = getelementptr inbounds float, float* %272, i32 1
  store float %271, float* %arrayidx231, align 4, !tbaa !8
  %call232 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %omegadot_out)
  %arrayidx233 = getelementptr inbounds float, float* %call232, i32 2
  %273 = load float, float* %arrayidx233, align 4, !tbaa !8
  %274 = load float*, float** %output.addr, align 4, !tbaa !2
  %arrayidx234 = getelementptr inbounds float, float* %274, i32 2
  store float %273, float* %arrayidx234, align 4, !tbaa !8
  %275 = bitcast %class.btVector3* %vdot_out to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %275) #7
  %call235 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vdot_out)
  %276 = bitcast %class.btVector3* %ref.tmp236 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %276) #7
  %277 = bitcast %class.btMatrix3x3* %ref.tmp237 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %277) #7
  %278 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_parent, align 4, !tbaa !2
  %arrayidx238 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %278, i32 0
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp237, %class.btMatrix3x3* %arrayidx238)
  %279 = load %class.btVector3*, %class.btVector3** %accel_bottom, align 4, !tbaa !2
  %arrayidx239 = getelementptr inbounds %class.btVector3, %class.btVector3* %279, i32 0
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp236, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp237, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx239)
  %280 = bitcast %class.btVector3* %vdot_out to i8*
  %281 = bitcast %class.btVector3* %ref.tmp236 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %280, i8* align 4 %281, i32 16, i1 false), !tbaa.struct !28
  %282 = bitcast %class.btMatrix3x3* %ref.tmp237 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %282) #7
  %283 = bitcast %class.btVector3* %ref.tmp236 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %283) #7
  %call240 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %vdot_out)
  %arrayidx241 = getelementptr inbounds float, float* %call240, i32 0
  %284 = load float, float* %arrayidx241, align 4, !tbaa !8
  %285 = load float*, float** %output.addr, align 4, !tbaa !2
  %arrayidx242 = getelementptr inbounds float, float* %285, i32 3
  store float %284, float* %arrayidx242, align 4, !tbaa !8
  %call243 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %vdot_out)
  %arrayidx244 = getelementptr inbounds float, float* %call243, i32 1
  %286 = load float, float* %arrayidx244, align 4, !tbaa !8
  %287 = load float*, float** %output.addr, align 4, !tbaa !2
  %arrayidx245 = getelementptr inbounds float, float* %287, i32 4
  store float %286, float* %arrayidx245, align 4, !tbaa !8
  %call246 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %vdot_out)
  %arrayidx247 = getelementptr inbounds float, float* %call246, i32 2
  %288 = load float, float* %arrayidx247, align 4, !tbaa !8
  %289 = load float*, float** %output.addr, align 4, !tbaa !2
  %arrayidx248 = getelementptr inbounds float, float* %289, i32 5
  store float %288, float* %arrayidx248, align 4, !tbaa !8
  %290 = bitcast %class.btVector3* %vdot_out to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %290) #7
  %291 = bitcast %class.btVector3* %omegadot_out to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %291) #7
  %292 = bitcast float** %joint_accel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %292) #7
  %293 = bitcast %class.btVector3* %input_torque to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %293) #7
  %294 = bitcast %class.btVector3* %input_force to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %294) #7
  %295 = bitcast float** %D to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %295) #7
  %296 = bitcast float** %Y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %296) #7
  %297 = bitcast %class.btVector3** %accel_bottom to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %297) #7
  %298 = bitcast %class.btVector3** %accel_top to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %298) #7
  %299 = bitcast %class.btVector3** %h_bottom to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %299) #7
  %300 = bitcast %class.btVector3** %h_top to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %300) #7
  %301 = bitcast %class.btMatrix3x3** %rot_from_parent to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %301) #7
  %302 = bitcast %class.btVector3** %zero_acc_bottom_linear to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %302) #7
  %303 = bitcast %class.btVector3** %zero_acc_top_angular to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %303) #7
  %304 = bitcast %class.btVector3** %v_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %304) #7
  %305 = bitcast float** %r_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %305) #7
  %306 = bitcast i32* %num_links to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %306) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK20btAlignedObjectArrayI11btMatrix3x3EixEi(%class.btAlignedObjectArray.12* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m_data, align 4, !tbaa !52
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 %1
  ret %class.btMatrix3x3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !50
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %1
  ret %class.btVector3* %arrayidx
}

define hidden void @_ZN11btMultiBody13stepPositionsEf(%class.btMultiBody* %this, float %dt) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %dt.addr = alloca float, align 4
  %num_links = alloca i32, align 4
  %v = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %base_omega = alloca %class.btVector3, align 4
  %omega_norm = alloca float, align 4
  %omega_times_dt = alloca float, align 4
  %SMALL_ROTATION_ANGLE = alloca float, align 4
  %xsq = alloca float, align 4
  %sin_term = alloca float, align 4
  %cos_term = alloca float, align 4
  %ref.tmp9 = alloca %class.btQuaternion, align 4
  %ref.tmp10 = alloca %class.btQuaternion, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp24 = alloca %class.btQuaternion, align 4
  %ref.tmp26 = alloca %class.btQuaternion, align 4
  %ref.tmp27 = alloca %class.btVector3, align 4
  %ref.tmp28 = alloca float, align 4
  %i = alloca i32, align 4
  %jointVel = alloca float, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store float %dt, float* %dt.addr, align 4, !tbaa !8
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = bitcast i32* %num_links to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %this1)
  store i32 %call, i32* %num_links, align 4, !tbaa !6
  %1 = bitcast %class.btVector3* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #7
  call void @_ZNK11btMultiBody10getBaseVelEv(%class.btVector3* sret align 4 %v, %class.btMultiBody* %this1)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #7
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp, float* nonnull align 4 dereferenceable(4) %dt.addr, %class.btVector3* nonnull align 4 dereferenceable(16) %v)
  %base_pos = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 1
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %base_pos, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %3) #7
  %4 = bitcast %class.btVector3* %base_omega to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #7
  call void @_ZNK11btMultiBody12getBaseOmegaEv(%class.btVector3* sret align 4 %base_omega, %class.btMultiBody* %this1)
  %5 = bitcast float* %omega_norm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %call3 = call float @_ZNK9btVector34normEv(%class.btVector3* %base_omega)
  store float %call3, float* %omega_norm, align 4, !tbaa !8
  %6 = bitcast float* %omega_times_dt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = load float, float* %omega_norm, align 4, !tbaa !8
  %8 = load float, float* %dt.addr, align 4, !tbaa !8
  %mul = fmul float %7, %8
  store float %mul, float* %omega_times_dt, align 4, !tbaa !8
  %9 = bitcast float* %SMALL_ROTATION_ANGLE to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  store float 0x3F947AE140000000, float* %SMALL_ROTATION_ANGLE, align 4, !tbaa !8
  %10 = load float, float* %omega_times_dt, align 4, !tbaa !8
  %call4 = call float @_Z4fabsf(float %10) #7
  %cmp = fcmp olt float %call4, 0x3F947AE140000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %11 = bitcast float* %xsq to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #7
  %12 = load float, float* %omega_times_dt, align 4, !tbaa !8
  %13 = load float, float* %omega_times_dt, align 4, !tbaa !8
  %mul5 = fmul float %12, %13
  store float %mul5, float* %xsq, align 4, !tbaa !8
  %14 = bitcast float* %sin_term to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #7
  %15 = load float, float* %dt.addr, align 4, !tbaa !8
  %16 = load float, float* %xsq, align 4, !tbaa !8
  %div = fdiv float %16, 4.800000e+01
  %sub = fsub float %div, 5.000000e-01
  %mul6 = fmul float %15, %sub
  store float %mul6, float* %sin_term, align 4, !tbaa !8
  %17 = bitcast float* %cos_term to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #7
  %18 = load float, float* %xsq, align 4, !tbaa !8
  %div7 = fdiv float %18, 8.000000e+00
  %sub8 = fsub float 1.000000e+00, %div7
  store float %sub8, float* %cos_term, align 4, !tbaa !8
  %19 = bitcast %class.btQuaternion* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %19) #7
  %base_quat = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 2
  %20 = bitcast %class.btQuaternion* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %20) #7
  %21 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #7
  %22 = load float, float* %sin_term, align 4, !tbaa !8
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %base_omega)
  %arrayidx = getelementptr inbounds float, float* %call12, i32 0
  %23 = load float, float* %arrayidx, align 4, !tbaa !8
  %mul13 = fmul float %22, %23
  store float %mul13, float* %ref.tmp11, align 4, !tbaa !8
  %24 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #7
  %25 = load float, float* %sin_term, align 4, !tbaa !8
  %call15 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %base_omega)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 1
  %26 = load float, float* %arrayidx16, align 4, !tbaa !8
  %mul17 = fmul float %25, %26
  store float %mul17, float* %ref.tmp14, align 4, !tbaa !8
  %27 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #7
  %28 = load float, float* %sin_term, align 4, !tbaa !8
  %call19 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %base_omega)
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 2
  %29 = load float, float* %arrayidx20, align 4, !tbaa !8
  %mul21 = fmul float %28, %29
  store float %mul21, float* %ref.tmp18, align 4, !tbaa !8
  %call22 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %cos_term)
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %ref.tmp9, %class.btQuaternion* nonnull align 4 dereferenceable(16) %base_quat, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp10)
  %base_quat23 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 2
  %30 = bitcast %class.btQuaternion* %base_quat23 to i8*
  %31 = bitcast %class.btQuaternion* %ref.tmp9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %30, i8* align 4 %31, i32 16, i1 false)
  %32 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #7
  %33 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #7
  %34 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #7
  %35 = bitcast %class.btQuaternion* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %35) #7
  %36 = bitcast %class.btQuaternion* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %36) #7
  %37 = bitcast float* %cos_term to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #7
  %38 = bitcast float* %sin_term to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #7
  %39 = bitcast float* %xsq to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #7
  br label %if.end

if.else:                                          ; preds = %entry
  %40 = bitcast %class.btQuaternion* %ref.tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %40) #7
  %base_quat25 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 2
  %41 = bitcast %class.btQuaternion* %ref.tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %41) #7
  %42 = bitcast %class.btVector3* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %42) #7
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp27, %class.btVector3* nonnull align 4 dereferenceable(16) %base_omega, float* nonnull align 4 dereferenceable(4) %omega_norm)
  %43 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #7
  %44 = load float, float* %omega_times_dt, align 4, !tbaa !8
  %fneg = fneg float %44
  store float %fneg, float* %ref.tmp28, align 4, !tbaa !8
  %call29 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %ref.tmp26, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp28)
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %ref.tmp24, %class.btQuaternion* nonnull align 4 dereferenceable(16) %base_quat25, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp26)
  %base_quat30 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 2
  %45 = bitcast %class.btQuaternion* %base_quat30 to i8*
  %46 = bitcast %class.btQuaternion* %ref.tmp24 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %45, i8* align 4 %46, i32 16, i1 false)
  %47 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #7
  %48 = bitcast %class.btVector3* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %48) #7
  %49 = bitcast %class.btQuaternion* %ref.tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %49) #7
  %50 = bitcast %class.btQuaternion* %ref.tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %50) #7
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %base_quat31 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 2
  %call32 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %base_quat31)
  %51 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %52 = load i32, i32* %i, align 4, !tbaa !6
  %53 = load i32, i32* %num_links, align 4, !tbaa !6
  %cmp33 = icmp slt i32 %52, %53
  br i1 %cmp33, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %54 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %55 = bitcast float* %jointVel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #7
  %56 = load i32, i32* %i, align 4, !tbaa !6
  %call34 = call float @_ZNK11btMultiBody11getJointVelEi(%class.btMultiBody* %this1, i32 %56)
  store float %call34, float* %jointVel, align 4, !tbaa !8
  %57 = load float, float* %dt.addr, align 4, !tbaa !8
  %58 = load float, float* %jointVel, align 4, !tbaa !8
  %mul35 = fmul float %57, %58
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %59 = load i32, i32* %i, align 4, !tbaa !6
  %call36 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links, i32 %59)
  %joint_pos = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call36, i32 0, i32 0
  %60 = load float, float* %joint_pos, align 4, !tbaa !42
  %add = fadd float %60, %mul35
  store float %add, float* %joint_pos, align 4, !tbaa !42
  %links37 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %61 = load i32, i32* %i, align 4, !tbaa !6
  %call38 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links37, i32 %61)
  call void @_ZN15btMultibodyLink11updateCacheEv(%struct.btMultibodyLink* %call38)
  %62 = bitcast float* %jointVel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %63 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %63, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %64 = bitcast float* %SMALL_ROTATION_ANGLE to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #7
  %65 = bitcast float* %omega_times_dt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #7
  %66 = bitcast float* %omega_norm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #7
  %67 = bitcast %class.btVector3* %base_omega to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %67) #7
  %68 = bitcast %class.btVector3* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %68) #7
  %69 = bitcast i32* %num_links to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z4fabsf(float %__lcpp_x) #1 comdat {
entry:
  %__lcpp_x.addr = alloca float, align 4
  store float %__lcpp_x, float* %__lcpp_x.addr, align 4, !tbaa !8
  %0 = load float, float* %__lcpp_x.addr, align 4, !tbaa !8
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZdvRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #3 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load float*, float** %s.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !8
  %div = fdiv float 1.000000e+00, %3
  store float %div, float* %ref.tmp, align 4, !tbaa !8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %4 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #7
  ret void
}

define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %_axis, float* nonnull align 4 dereferenceable(4) %_angle) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %_axis.addr = alloca %class.btVector3*, align 4
  %_angle.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %_axis, %class.btVector3** %_axis.addr, align 4, !tbaa !2
  store float* %_angle, float** %_angle.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* %0)
  %1 = load %class.btVector3*, %class.btVector3** %_axis.addr, align 4, !tbaa !2
  %2 = load float*, float** %_angle.addr, align 4, !tbaa !2
  call void @_ZN12btQuaternion11setRotationERK9btVector3RKf(%class.btQuaternion* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1, float* nonnull align 4 dereferenceable(4) %2)
  ret %class.btQuaternion* %this1
}

define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call float @_ZNK12btQuaternion6lengthEv(%class.btQuaternion* %this1)
  store float %call, float* %ref.tmp, align 4, !tbaa !8
  %call2 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaterniondVERKf(%class.btQuaternion* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1) #7
  ret %class.btQuaternion* %call2
}

define hidden void @_ZNK11btMultiBody19fillContactJacobianEiRK9btVector3S2_PfR20btAlignedObjectArrayIfERS4_IS0_ERS4_I11btMatrix3x3E(%class.btMultiBody* %this, i32 %link, %class.btVector3* nonnull align 4 dereferenceable(16) %contact_point, %class.btVector3* nonnull align 4 dereferenceable(16) %normal, float* %jac, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17) %scratch_r, %class.btAlignedObjectArray.8* nonnull align 4 dereferenceable(17) %scratch_v, %class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17) %scratch_m) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %link.addr = alloca i32, align 4
  %contact_point.addr = alloca %class.btVector3*, align 4
  %normal.addr = alloca %class.btVector3*, align 4
  %jac.addr = alloca float*, align 4
  %scratch_r.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %scratch_v.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %scratch_m.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %num_links = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btMatrix3x3, align 4
  %v_ptr = alloca %class.btVector3*, align 4
  %p_minus_com = alloca %class.btVector3*, align 4
  %n_local = alloca %class.btVector3*, align 4
  %ref.tmp10 = alloca float, align 4
  %results = alloca float*, align 4
  %rot_from_world = alloca %class.btMatrix3x3*, align 4
  %p_minus_com_world = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca %class.btMatrix3x3, align 4
  %ref.tmp16 = alloca %class.btVector3, align 4
  %ref.tmp19 = alloca %class.btVector3, align 4
  %omega_coeffs = alloca %class.btVector3, align 4
  %ref.tmp23 = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  %i47 = alloca i32, align 4
  %parent = alloca i32, align 4
  %mtx = alloca %class.btMatrix3x3, align 4
  %ref.tmp57 = alloca %class.btMatrix3x3, align 4
  %ref.tmp63 = alloca %class.btVector3, align 4
  %ref.tmp68 = alloca %class.btVector3, align 4
  %ref.tmp69 = alloca %class.btVector3, align 4
  %ref.tmp81 = alloca %class.btVector3, align 4
  %ref.tmp82 = alloca %class.btVector3, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store i32 %link, i32* %link.addr, align 4, !tbaa !6
  store %class.btVector3* %contact_point, %class.btVector3** %contact_point.addr, align 4, !tbaa !2
  store %class.btVector3* %normal, %class.btVector3** %normal.addr, align 4, !tbaa !2
  store float* %jac, float** %jac.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.4* %scratch_r, %class.btAlignedObjectArray.4** %scratch_r.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.8* %scratch_v, %class.btAlignedObjectArray.8** %scratch_v.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.12* %scratch_m, %class.btAlignedObjectArray.12** %scratch_m.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = bitcast i32* %num_links to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %this1)
  store i32 %call, i32* %num_links, align 4, !tbaa !6
  %1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %scratch_v.addr, align 4, !tbaa !2
  %2 = load i32, i32* %num_links, align 4, !tbaa !6
  %mul = mul nsw i32 2, %2
  %add = add nsw i32 %mul, 2
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #7
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.8* %1, i32 %add, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #7
  %5 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %scratch_m.addr, align 4, !tbaa !2
  %6 = load i32, i32* %num_links, align 4, !tbaa !6
  %add3 = add nsw i32 %6, 1
  %7 = bitcast %class.btMatrix3x3* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %7) #7
  %call5 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %ref.tmp4)
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E6resizeEiRKS0_(%class.btAlignedObjectArray.12* %5, i32 %add3, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp4)
  %8 = bitcast %class.btMatrix3x3* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %8) #7
  %9 = bitcast %class.btVector3** %v_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %10 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %scratch_v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %10, i32 0)
  store %class.btVector3* %call6, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %11 = bitcast %class.btVector3** %p_minus_com to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #7
  %12 = load %class.btVector3*, %class.btVector3** %v_ptr, align 4, !tbaa !2
  store %class.btVector3* %12, %class.btVector3** %p_minus_com, align 4, !tbaa !2
  %13 = load i32, i32* %num_links, align 4, !tbaa !6
  %add7 = add nsw i32 %13, 1
  %14 = load %class.btVector3*, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds %class.btVector3, %class.btVector3* %14, i32 %add7
  store %class.btVector3* %add.ptr, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %15 = bitcast %class.btVector3** %n_local to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #7
  %16 = load %class.btVector3*, %class.btVector3** %v_ptr, align 4, !tbaa !2
  store %class.btVector3* %16, %class.btVector3** %n_local, align 4, !tbaa !2
  %17 = load i32, i32* %num_links, align 4, !tbaa !6
  %add8 = add nsw i32 %17, 1
  %18 = load %class.btVector3*, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %add.ptr9 = getelementptr inbounds %class.btVector3, %class.btVector3* %18, i32 %add8
  store %class.btVector3* %add.ptr9, %class.btVector3** %v_ptr, align 4, !tbaa !2
  %19 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %scratch_r.addr, align 4, !tbaa !2
  %20 = load i32, i32* %num_links, align 4, !tbaa !6
  %21 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #7
  store float 0.000000e+00, float* %ref.tmp10, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.4* %19, i32 %20, float* nonnull align 4 dereferenceable(4) %ref.tmp10)
  %22 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #7
  %23 = bitcast float** %results to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #7
  %24 = load i32, i32* %num_links, align 4, !tbaa !6
  %cmp = icmp sgt i32 %24, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %25 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %scratch_r.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %25, i32 0)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float* [ %call11, %cond.true ], [ null, %cond.false ]
  store float* %cond, float** %results, align 4, !tbaa !2
  %26 = bitcast %class.btMatrix3x3** %rot_from_world to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #7
  %27 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %scratch_m.addr, align 4, !tbaa !2
  %call12 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN20btAlignedObjectArrayI11btMatrix3x3EixEi(%class.btAlignedObjectArray.12* %27, i32 0)
  store %class.btMatrix3x3* %call12, %class.btMatrix3x3** %rot_from_world, align 4, !tbaa !2
  %28 = bitcast %class.btVector3* %p_minus_com_world to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %28) #7
  %29 = load %class.btVector3*, %class.btVector3** %contact_point.addr, align 4, !tbaa !2
  %base_pos = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %p_minus_com_world, %class.btVector3* nonnull align 4 dereferenceable(16) %29, %class.btVector3* nonnull align 4 dereferenceable(16) %base_pos)
  %30 = bitcast %class.btMatrix3x3* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %30) #7
  %base_quat = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 2
  %call14 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* %ref.tmp13, %class.btQuaternion* nonnull align 4 dereferenceable(16) %base_quat)
  %31 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_world, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %31, i32 0
  %call15 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %arrayidx, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp13)
  %32 = bitcast %class.btMatrix3x3* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %32) #7
  %33 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %33) #7
  %34 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_world, align 4, !tbaa !2
  %arrayidx17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %34, i32 0
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp16, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx17, %class.btVector3* nonnull align 4 dereferenceable(16) %p_minus_com_world)
  %35 = load %class.btVector3*, %class.btVector3** %p_minus_com, align 4, !tbaa !2
  %arrayidx18 = getelementptr inbounds %class.btVector3, %class.btVector3* %35, i32 0
  %36 = bitcast %class.btVector3* %arrayidx18 to i8*
  %37 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %36, i8* align 4 %37, i32 16, i1 false), !tbaa.struct !28
  %38 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %38) #7
  %39 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %39) #7
  %40 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_world, align 4, !tbaa !2
  %arrayidx20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %40, i32 0
  %41 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp19, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx20, %class.btVector3* nonnull align 4 dereferenceable(16) %41)
  %42 = load %class.btVector3*, %class.btVector3** %n_local, align 4, !tbaa !2
  %arrayidx21 = getelementptr inbounds %class.btVector3, %class.btVector3* %42, i32 0
  %43 = bitcast %class.btVector3* %arrayidx21 to i8*
  %44 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %43, i8* align 4 %44, i32 16, i1 false), !tbaa.struct !28
  %45 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %45) #7
  %46 = bitcast %class.btVector3* %omega_coeffs to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %46) #7
  %call22 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %omega_coeffs)
  %47 = bitcast %class.btVector3* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %47) #7
  %48 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp23, %class.btVector3* %p_minus_com_world, %class.btVector3* nonnull align 4 dereferenceable(16) %48)
  %49 = bitcast %class.btVector3* %omega_coeffs to i8*
  %50 = bitcast %class.btVector3* %ref.tmp23 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %49, i8* align 4 %50, i32 16, i1 false), !tbaa.struct !28
  %51 = bitcast %class.btVector3* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %51) #7
  %call24 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %omega_coeffs)
  %arrayidx25 = getelementptr inbounds float, float* %call24, i32 0
  %52 = load float, float* %arrayidx25, align 4, !tbaa !8
  %53 = load float*, float** %jac.addr, align 4, !tbaa !2
  %arrayidx26 = getelementptr inbounds float, float* %53, i32 0
  store float %52, float* %arrayidx26, align 4, !tbaa !8
  %call27 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %omega_coeffs)
  %arrayidx28 = getelementptr inbounds float, float* %call27, i32 1
  %54 = load float, float* %arrayidx28, align 4, !tbaa !8
  %55 = load float*, float** %jac.addr, align 4, !tbaa !2
  %arrayidx29 = getelementptr inbounds float, float* %55, i32 1
  store float %54, float* %arrayidx29, align 4, !tbaa !8
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %omega_coeffs)
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 2
  %56 = load float, float* %arrayidx31, align 4, !tbaa !8
  %57 = load float*, float** %jac.addr, align 4, !tbaa !2
  %arrayidx32 = getelementptr inbounds float, float* %57, i32 2
  store float %56, float* %arrayidx32, align 4, !tbaa !8
  %58 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %call33 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %58)
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 0
  %59 = load float, float* %arrayidx34, align 4, !tbaa !8
  %60 = load float*, float** %jac.addr, align 4, !tbaa !2
  %arrayidx35 = getelementptr inbounds float, float* %60, i32 3
  store float %59, float* %arrayidx35, align 4, !tbaa !8
  %61 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %call36 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %61)
  %arrayidx37 = getelementptr inbounds float, float* %call36, i32 1
  %62 = load float, float* %arrayidx37, align 4, !tbaa !8
  %63 = load float*, float** %jac.addr, align 4, !tbaa !2
  %arrayidx38 = getelementptr inbounds float, float* %63, i32 4
  store float %62, float* %arrayidx38, align 4, !tbaa !8
  %64 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %call39 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %64)
  %arrayidx40 = getelementptr inbounds float, float* %call39, i32 2
  %65 = load float, float* %arrayidx40, align 4, !tbaa !8
  %66 = load float*, float** %jac.addr, align 4, !tbaa !2
  %arrayidx41 = getelementptr inbounds float, float* %66, i32 5
  store float %65, float* %arrayidx41, align 4, !tbaa !8
  %67 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #7
  store i32 6, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end
  %68 = load i32, i32* %i, align 4, !tbaa !6
  %69 = load i32, i32* %num_links, align 4, !tbaa !6
  %add42 = add nsw i32 6, %69
  %cmp43 = icmp slt i32 %68, %add42
  br i1 %cmp43, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %70 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %71 = load float*, float** %jac.addr, align 4, !tbaa !2
  %72 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx44 = getelementptr inbounds float, float* %71, i32 %72
  store float 0.000000e+00, float* %arrayidx44, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %73 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %73, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %74 = load i32, i32* %num_links, align 4, !tbaa !6
  %cmp45 = icmp sgt i32 %74, 0
  br i1 %cmp45, label %land.lhs.true, label %if.end108

land.lhs.true:                                    ; preds = %for.end
  %75 = load i32, i32* %link.addr, align 4, !tbaa !6
  %cmp46 = icmp sgt i32 %75, -1
  br i1 %cmp46, label %if.then, label %if.end108

if.then:                                          ; preds = %land.lhs.true
  %76 = bitcast i32* %i47 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #7
  store i32 0, i32* %i47, align 4, !tbaa !6
  br label %for.cond48

for.cond48:                                       ; preds = %for.inc98, %if.then
  %77 = load i32, i32* %i47, align 4, !tbaa !6
  %78 = load i32, i32* %num_links, align 4, !tbaa !6
  %cmp49 = icmp slt i32 %77, %78
  br i1 %cmp49, label %for.body51, label %for.cond.cleanup50

for.cond.cleanup50:                               ; preds = %for.cond48
  %79 = bitcast i32* %i47 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #7
  br label %for.end100

for.body51:                                       ; preds = %for.cond48
  %80 = bitcast i32* %parent to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #7
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %81 = load i32, i32* %i47, align 4, !tbaa !6
  %call52 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links, i32 %81)
  %parent53 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call52, i32 0, i32 3
  %82 = load i32, i32* %parent53, align 4, !tbaa !45
  store i32 %82, i32* %parent, align 4, !tbaa !6
  %83 = bitcast %class.btMatrix3x3* %mtx to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %83) #7
  %links54 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %84 = load i32, i32* %i47, align 4, !tbaa !6
  %call55 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links54, i32 %84)
  %cached_rot_parent_to_this = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call55, i32 0, i32 10
  %call56 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* %mtx, %class.btQuaternion* nonnull align 4 dereferenceable(16) %cached_rot_parent_to_this)
  %85 = bitcast %class.btMatrix3x3* %ref.tmp57 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %85) #7
  %86 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_world, align 4, !tbaa !2
  %87 = load i32, i32* %parent, align 4, !tbaa !6
  %add58 = add nsw i32 %87, 1
  %arrayidx59 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %86, i32 %add58
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp57, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %mtx, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx59)
  %88 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot_from_world, align 4, !tbaa !2
  %89 = load i32, i32* %i47, align 4, !tbaa !6
  %add60 = add nsw i32 %89, 1
  %arrayidx61 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %88, i32 %add60
  %call62 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %arrayidx61, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp57)
  %90 = bitcast %class.btMatrix3x3* %ref.tmp57 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %90) #7
  %91 = bitcast %class.btVector3* %ref.tmp63 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %91) #7
  %92 = load %class.btVector3*, %class.btVector3** %n_local, align 4, !tbaa !2
  %93 = load i32, i32* %parent, align 4, !tbaa !6
  %add64 = add nsw i32 %93, 1
  %arrayidx65 = getelementptr inbounds %class.btVector3, %class.btVector3* %92, i32 %add64
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp63, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %mtx, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx65)
  %94 = load %class.btVector3*, %class.btVector3** %n_local, align 4, !tbaa !2
  %95 = load i32, i32* %i47, align 4, !tbaa !6
  %add66 = add nsw i32 %95, 1
  %arrayidx67 = getelementptr inbounds %class.btVector3, %class.btVector3* %94, i32 %add66
  %96 = bitcast %class.btVector3* %arrayidx67 to i8*
  %97 = bitcast %class.btVector3* %ref.tmp63 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %96, i8* align 4 %97, i32 16, i1 false), !tbaa.struct !28
  %98 = bitcast %class.btVector3* %ref.tmp63 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %98) #7
  %99 = bitcast %class.btVector3* %ref.tmp68 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %99) #7
  %100 = bitcast %class.btVector3* %ref.tmp69 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %100) #7
  %101 = load %class.btVector3*, %class.btVector3** %p_minus_com, align 4, !tbaa !2
  %102 = load i32, i32* %parent, align 4, !tbaa !6
  %add70 = add nsw i32 %102, 1
  %arrayidx71 = getelementptr inbounds %class.btVector3, %class.btVector3* %101, i32 %add70
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp69, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %mtx, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx71)
  %links72 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %103 = load i32, i32* %i47, align 4, !tbaa !6
  %call73 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links72, i32 %103)
  %cached_r_vector = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call73, i32 0, i32 11
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp68, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp69, %class.btVector3* nonnull align 4 dereferenceable(16) %cached_r_vector)
  %104 = load %class.btVector3*, %class.btVector3** %p_minus_com, align 4, !tbaa !2
  %105 = load i32, i32* %i47, align 4, !tbaa !6
  %add74 = add nsw i32 %105, 1
  %arrayidx75 = getelementptr inbounds %class.btVector3, %class.btVector3* %104, i32 %add74
  %106 = bitcast %class.btVector3* %arrayidx75 to i8*
  %107 = bitcast %class.btVector3* %ref.tmp68 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %106, i8* align 4 %107, i32 16, i1 false), !tbaa.struct !28
  %108 = bitcast %class.btVector3* %ref.tmp69 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %108) #7
  %109 = bitcast %class.btVector3* %ref.tmp68 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %109) #7
  %links76 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %110 = load i32, i32* %i47, align 4, !tbaa !6
  %call77 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links76, i32 %110)
  %is_revolute = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call77, i32 0, i32 9
  %111 = load i8, i8* %is_revolute, align 4, !tbaa !46, !range !30
  %tobool = trunc i8 %111 to i1
  br i1 %tobool, label %if.then78, label %if.else

if.then78:                                        ; preds = %for.body51
  %112 = load %class.btVector3*, %class.btVector3** %n_local, align 4, !tbaa !2
  %113 = load i32, i32* %i47, align 4, !tbaa !6
  %add79 = add nsw i32 %113, 1
  %arrayidx80 = getelementptr inbounds %class.btVector3, %class.btVector3* %112, i32 %add79
  %114 = bitcast %class.btVector3* %ref.tmp81 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %114) #7
  %115 = bitcast %class.btVector3* %ref.tmp82 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %115) #7
  %links83 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %116 = load i32, i32* %i47, align 4, !tbaa !6
  %call84 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links83, i32 %116)
  %axis_top = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call84, i32 0, i32 5
  %117 = load %class.btVector3*, %class.btVector3** %p_minus_com, align 4, !tbaa !2
  %118 = load i32, i32* %i47, align 4, !tbaa !6
  %add85 = add nsw i32 %118, 1
  %arrayidx86 = getelementptr inbounds %class.btVector3, %class.btVector3* %117, i32 %add85
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp82, %class.btVector3* %axis_top, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx86)
  %links87 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %119 = load i32, i32* %i47, align 4, !tbaa !6
  %call88 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links87, i32 %119)
  %axis_bottom = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call88, i32 0, i32 6
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp81, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp82, %class.btVector3* nonnull align 4 dereferenceable(16) %axis_bottom)
  %call89 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %arrayidx80, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp81)
  %120 = load float*, float** %results, align 4, !tbaa !2
  %121 = load i32, i32* %i47, align 4, !tbaa !6
  %arrayidx90 = getelementptr inbounds float, float* %120, i32 %121
  store float %call89, float* %arrayidx90, align 4, !tbaa !8
  %122 = bitcast %class.btVector3* %ref.tmp82 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %122) #7
  %123 = bitcast %class.btVector3* %ref.tmp81 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %123) #7
  br label %if.end

if.else:                                          ; preds = %for.body51
  %124 = load %class.btVector3*, %class.btVector3** %n_local, align 4, !tbaa !2
  %125 = load i32, i32* %i47, align 4, !tbaa !6
  %add91 = add nsw i32 %125, 1
  %arrayidx92 = getelementptr inbounds %class.btVector3, %class.btVector3* %124, i32 %add91
  %links93 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %126 = load i32, i32* %i47, align 4, !tbaa !6
  %call94 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links93, i32 %126)
  %axis_bottom95 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call94, i32 0, i32 6
  %call96 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %arrayidx92, %class.btVector3* nonnull align 4 dereferenceable(16) %axis_bottom95)
  %127 = load float*, float** %results, align 4, !tbaa !2
  %128 = load i32, i32* %i47, align 4, !tbaa !6
  %arrayidx97 = getelementptr inbounds float, float* %127, i32 %128
  store float %call96, float* %arrayidx97, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then78
  %129 = bitcast %class.btMatrix3x3* %mtx to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %129) #7
  %130 = bitcast i32* %parent to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #7
  br label %for.inc98

for.inc98:                                        ; preds = %if.end
  %131 = load i32, i32* %i47, align 4, !tbaa !6
  %inc99 = add nsw i32 %131, 1
  store i32 %inc99, i32* %i47, align 4, !tbaa !6
  br label %for.cond48

for.end100:                                       ; preds = %for.cond.cleanup50
  br label %while.cond

while.cond:                                       ; preds = %while.body, %for.end100
  %132 = load i32, i32* %link.addr, align 4, !tbaa !6
  %cmp101 = icmp ne i32 %132, -1
  br i1 %cmp101, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %133 = load float*, float** %results, align 4, !tbaa !2
  %134 = load i32, i32* %link.addr, align 4, !tbaa !6
  %arrayidx102 = getelementptr inbounds float, float* %133, i32 %134
  %135 = load float, float* %arrayidx102, align 4, !tbaa !8
  %136 = load float*, float** %jac.addr, align 4, !tbaa !2
  %137 = load i32, i32* %link.addr, align 4, !tbaa !6
  %add103 = add nsw i32 6, %137
  %arrayidx104 = getelementptr inbounds float, float* %136, i32 %add103
  store float %135, float* %arrayidx104, align 4, !tbaa !8
  %links105 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %138 = load i32, i32* %link.addr, align 4, !tbaa !6
  %call106 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZNK20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links105, i32 %138)
  %parent107 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call106, i32 0, i32 3
  %139 = load i32, i32* %parent107, align 4, !tbaa !45
  store i32 %139, i32* %link.addr, align 4, !tbaa !6
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %if.end108

if.end108:                                        ; preds = %while.end, %land.lhs.true, %for.end
  %140 = bitcast %class.btVector3* %omega_coeffs to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %140) #7
  %141 = bitcast %class.btVector3* %p_minus_com_world to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %141) #7
  %142 = bitcast %class.btMatrix3x3** %rot_from_world to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #7
  %143 = bitcast float** %results to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #7
  %144 = bitcast %class.btVector3** %n_local to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #7
  %145 = bitcast %class.btVector3** %p_minus_com to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #7
  %146 = bitcast %class.btVector3** %v_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #7
  %147 = bitcast i32* %num_links to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #7
  ret void
}

; Function Attrs: nounwind
define hidden void @_ZN11btMultiBody6wakeUpEv(%class.btMultiBody* %this) #4 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %awake = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 17
  store i8 1, i8* %awake, align 1, !tbaa !32
  ret void
}

; Function Attrs: nounwind
define hidden void @_ZN11btMultiBody9goToSleepEv(%class.btMultiBody* %this) #4 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %awake = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 17
  store i8 0, i8* %awake, align 1, !tbaa !32
  ret void
}

define hidden void @_ZN11btMultiBody29checkMotionAndSleepIfRequiredEf(%class.btMultiBody* %this, float %timestep) #0 {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %timestep.addr = alloca float, align 4
  %num_links = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %motion = alloca float, align 4
  %i = alloca i32, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store float %timestep, float* %timestep.addr, align 4, !tbaa !8
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = bitcast i32* %num_links to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %this1)
  store i32 %call, i32* %num_links, align 4, !tbaa !6
  %can_sleep = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 18
  %1 = load i8, i8* %can_sleep, align 2, !tbaa !33, !range !30
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %2 = load i8, i8* @gDisableDeactivation, align 1, !tbaa !10, !range !30
  %tobool2 = trunc i8 %2 to i1
  br i1 %tobool2, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  %awake = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 17
  store i8 1, i8* %awake, align 1, !tbaa !32
  %sleep_timer = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 20
  store float 0.000000e+00, float* %sleep_timer, align 4, !tbaa !34
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %lor.lhs.false
  %3 = bitcast float* %motion to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  store float 0.000000e+00, float* %motion, align 4, !tbaa !8
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %num_links, align 4, !tbaa !6
  %add = add nsw i32 6, %6
  %cmp = icmp slt i32 %5, %add
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_real_buf = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 9
  %8 = load i32, i32* %i, align 4, !tbaa !6
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_real_buf, i32 %8)
  %9 = load float, float* %call3, align 4, !tbaa !8
  %m_real_buf4 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 9
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_real_buf4, i32 %10)
  %11 = load float, float* %call5, align 4, !tbaa !8
  %mul = fmul float %9, %11
  %12 = load float, float* %motion, align 4, !tbaa !8
  %add6 = fadd float %12, %mul
  store float %add6, float* %motion, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %13 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %14 = load float, float* %motion, align 4, !tbaa !8
  %cmp7 = fcmp olt float %14, 0x3FA99999A0000000
  br i1 %cmp7, label %if.then8, label %if.else

if.then8:                                         ; preds = %for.end
  %15 = load float, float* %timestep.addr, align 4, !tbaa !8
  %sleep_timer9 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 20
  %16 = load float, float* %sleep_timer9, align 4, !tbaa !34
  %add10 = fadd float %16, %15
  store float %add10, float* %sleep_timer9, align 4, !tbaa !34
  %sleep_timer11 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 20
  %17 = load float, float* %sleep_timer11, align 4, !tbaa !34
  %cmp12 = fcmp ogt float %17, 2.000000e+00
  br i1 %cmp12, label %if.then13, label %if.end14

if.then13:                                        ; preds = %if.then8
  call void @_ZN11btMultiBody9goToSleepEv(%class.btMultiBody* %this1)
  br label %if.end14

if.end14:                                         ; preds = %if.then13, %if.then8
  br label %if.end20

if.else:                                          ; preds = %for.end
  %sleep_timer15 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 20
  store float 0.000000e+00, float* %sleep_timer15, align 4, !tbaa !34
  %awake16 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 17
  %18 = load i8, i8* %awake16, align 1, !tbaa !32, !range !30
  %tobool17 = trunc i8 %18 to i1
  br i1 %tobool17, label %if.end19, label %if.then18

if.then18:                                        ; preds = %if.else
  call void @_ZN11btMultiBody6wakeUpEv(%class.btMultiBody* %this1)
  br label %if.end19

if.end19:                                         ; preds = %if.then18, %if.else
  br label %if.end20

if.end20:                                         ; preds = %if.end19, %if.end14
  %19 = bitcast float* %motion to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end20, %if.then
  %20 = bitcast i32* %num_links to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  store float* %_w, float** %_w.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !8
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !8
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !8
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !8
  %6 = load float*, float** %_w.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4, !tbaa !8
  ret %class.btQuadWord* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK12btQuaternionRK9btVector3(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q, %class.btVector3* nonnull align 4 dereferenceable(16) %w) #3 comdat {
entry:
  %q.addr = alloca %class.btQuaternion*, align 4
  %w.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  store %class.btVector3* %w, %class.btVector3** %w.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %2 = bitcast %class.btQuaternion* %1 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %2)
  %3 = load float, float* %call, align 4, !tbaa !8
  %4 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %4)
  %5 = load float, float* %call1, align 4, !tbaa !8
  %mul = fmul float %3, %5
  %6 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %7)
  %8 = load float, float* %call2, align 4, !tbaa !8
  %9 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %9)
  %10 = load float, float* %call3, align 4, !tbaa !8
  %mul4 = fmul float %8, %10
  %add = fadd float %mul, %mul4
  %11 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %12 = bitcast %class.btQuaternion* %11 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %12)
  %13 = load float, float* %call5, align 4, !tbaa !8
  %14 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %14)
  %15 = load float, float* %call6, align 4, !tbaa !8
  %mul7 = fmul float %13, %15
  %sub = fsub float %add, %mul7
  store float %sub, float* %ref.tmp, align 4, !tbaa !8
  %16 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #7
  %17 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %18 = bitcast %class.btQuaternion* %17 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %18)
  %19 = load float, float* %call9, align 4, !tbaa !8
  %20 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %20)
  %21 = load float, float* %call10, align 4, !tbaa !8
  %mul11 = fmul float %19, %21
  %22 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %23 = bitcast %class.btQuaternion* %22 to %class.btQuadWord*
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %23)
  %24 = load float, float* %call12, align 4, !tbaa !8
  %25 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %25)
  %26 = load float, float* %call13, align 4, !tbaa !8
  %mul14 = fmul float %24, %26
  %add15 = fadd float %mul11, %mul14
  %27 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %28 = bitcast %class.btQuaternion* %27 to %class.btQuadWord*
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %28)
  %29 = load float, float* %call16, align 4, !tbaa !8
  %30 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %30)
  %31 = load float, float* %call17, align 4, !tbaa !8
  %mul18 = fmul float %29, %31
  %sub19 = fsub float %add15, %mul18
  store float %sub19, float* %ref.tmp8, align 4, !tbaa !8
  %32 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #7
  %33 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %34 = bitcast %class.btQuaternion* %33 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %34)
  %35 = load float, float* %call21, align 4, !tbaa !8
  %36 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %36)
  %37 = load float, float* %call22, align 4, !tbaa !8
  %mul23 = fmul float %35, %37
  %38 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %39 = bitcast %class.btQuaternion* %38 to %class.btQuadWord*
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %39)
  %40 = load float, float* %call24, align 4, !tbaa !8
  %41 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %41)
  %42 = load float, float* %call25, align 4, !tbaa !8
  %mul26 = fmul float %40, %42
  %add27 = fadd float %mul23, %mul26
  %43 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %44 = bitcast %class.btQuaternion* %43 to %class.btQuadWord*
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %44)
  %45 = load float, float* %call28, align 4, !tbaa !8
  %46 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %46)
  %47 = load float, float* %call29, align 4, !tbaa !8
  %mul30 = fmul float %45, %47
  %sub31 = fsub float %add27, %mul30
  store float %sub31, float* %ref.tmp20, align 4, !tbaa !8
  %48 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #7
  %49 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %50 = bitcast %class.btQuaternion* %49 to %class.btQuadWord*
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %50)
  %51 = load float, float* %call33, align 4, !tbaa !8
  %fneg = fneg float %51
  %52 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %52)
  %53 = load float, float* %call34, align 4, !tbaa !8
  %mul35 = fmul float %fneg, %53
  %54 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %55 = bitcast %class.btQuaternion* %54 to %class.btQuadWord*
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %55)
  %56 = load float, float* %call36, align 4, !tbaa !8
  %57 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %57)
  %58 = load float, float* %call37, align 4, !tbaa !8
  %mul38 = fmul float %56, %58
  %sub39 = fsub float %mul35, %mul38
  %59 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %60 = bitcast %class.btQuaternion* %59 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %60)
  %61 = load float, float* %call40, align 4, !tbaa !8
  %62 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %62)
  %63 = load float, float* %call41, align 4, !tbaa !8
  %mul42 = fmul float %61, %63
  %sub43 = fsub float %sub39, %mul42
  store float %sub43, float* %ref.tmp32, align 4, !tbaa !8
  %call44 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  %64 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #7
  %65 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #7
  %66 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #7
  %67 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #7
  ret void
}

define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp58 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %2, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  %3 = load float, float* %arrayidx, align 4, !tbaa !8
  %4 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %5 = bitcast %class.btQuaternion* %4 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %5)
  %6 = load float, float* %call, align 4, !tbaa !8
  %mul = fmul float %3, %6
  %7 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %7, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %8 = load float, float* %arrayidx3, align 4, !tbaa !8
  %9 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %10 = bitcast %class.btQuaternion* %9 to %class.btQuadWord*
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 3
  %11 = load float, float* %arrayidx5, align 4, !tbaa !8
  %mul6 = fmul float %8, %11
  %add = fadd float %mul, %mul6
  %12 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats7 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %12, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 1
  %13 = load float, float* %arrayidx8, align 4, !tbaa !8
  %14 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %15 = bitcast %class.btQuaternion* %14 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %15)
  %16 = load float, float* %call9, align 4, !tbaa !8
  %mul10 = fmul float %13, %16
  %add11 = fadd float %add, %mul10
  %17 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats12 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %17, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %18 = load float, float* %arrayidx13, align 4, !tbaa !8
  %19 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %20 = bitcast %class.btQuaternion* %19 to %class.btQuadWord*
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %20)
  %21 = load float, float* %call14, align 4, !tbaa !8
  %mul15 = fmul float %18, %21
  %sub = fsub float %add11, %mul15
  store float %sub, float* %ref.tmp, align 4, !tbaa !8
  %22 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #7
  %23 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats17 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %23, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 3
  %24 = load float, float* %arrayidx18, align 4, !tbaa !8
  %25 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %26 = bitcast %class.btQuaternion* %25 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %26)
  %27 = load float, float* %call19, align 4, !tbaa !8
  %mul20 = fmul float %24, %27
  %28 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats21 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %28, i32 0, i32 0
  %arrayidx22 = getelementptr inbounds [4 x float], [4 x float]* %m_floats21, i32 0, i32 1
  %29 = load float, float* %arrayidx22, align 4, !tbaa !8
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %m_floats23 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %31, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [4 x float], [4 x float]* %m_floats23, i32 0, i32 3
  %32 = load float, float* %arrayidx24, align 4, !tbaa !8
  %mul25 = fmul float %29, %32
  %add26 = fadd float %mul20, %mul25
  %33 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats27 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %33, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 2
  %34 = load float, float* %arrayidx28, align 4, !tbaa !8
  %35 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %36 = bitcast %class.btQuaternion* %35 to %class.btQuadWord*
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %36)
  %37 = load float, float* %call29, align 4, !tbaa !8
  %mul30 = fmul float %34, %37
  %add31 = fadd float %add26, %mul30
  %38 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats32 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %38, i32 0, i32 0
  %arrayidx33 = getelementptr inbounds [4 x float], [4 x float]* %m_floats32, i32 0, i32 0
  %39 = load float, float* %arrayidx33, align 4, !tbaa !8
  %40 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %41 = bitcast %class.btQuaternion* %40 to %class.btQuadWord*
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %41)
  %42 = load float, float* %call34, align 4, !tbaa !8
  %mul35 = fmul float %39, %42
  %sub36 = fsub float %add31, %mul35
  store float %sub36, float* %ref.tmp16, align 4, !tbaa !8
  %43 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #7
  %44 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats38 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %44, i32 0, i32 0
  %arrayidx39 = getelementptr inbounds [4 x float], [4 x float]* %m_floats38, i32 0, i32 3
  %45 = load float, float* %arrayidx39, align 4, !tbaa !8
  %46 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %47 = bitcast %class.btQuaternion* %46 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %47)
  %48 = load float, float* %call40, align 4, !tbaa !8
  %mul41 = fmul float %45, %48
  %49 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats42 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %49, i32 0, i32 0
  %arrayidx43 = getelementptr inbounds [4 x float], [4 x float]* %m_floats42, i32 0, i32 2
  %50 = load float, float* %arrayidx43, align 4, !tbaa !8
  %51 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %52 = bitcast %class.btQuaternion* %51 to %class.btQuadWord*
  %m_floats44 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %52, i32 0, i32 0
  %arrayidx45 = getelementptr inbounds [4 x float], [4 x float]* %m_floats44, i32 0, i32 3
  %53 = load float, float* %arrayidx45, align 4, !tbaa !8
  %mul46 = fmul float %50, %53
  %add47 = fadd float %mul41, %mul46
  %54 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats48 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %54, i32 0, i32 0
  %arrayidx49 = getelementptr inbounds [4 x float], [4 x float]* %m_floats48, i32 0, i32 0
  %55 = load float, float* %arrayidx49, align 4, !tbaa !8
  %56 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %57 = bitcast %class.btQuaternion* %56 to %class.btQuadWord*
  %call50 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %57)
  %58 = load float, float* %call50, align 4, !tbaa !8
  %mul51 = fmul float %55, %58
  %add52 = fadd float %add47, %mul51
  %59 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats53 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %59, i32 0, i32 0
  %arrayidx54 = getelementptr inbounds [4 x float], [4 x float]* %m_floats53, i32 0, i32 1
  %60 = load float, float* %arrayidx54, align 4, !tbaa !8
  %61 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %62 = bitcast %class.btQuaternion* %61 to %class.btQuadWord*
  %call55 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %62)
  %63 = load float, float* %call55, align 4, !tbaa !8
  %mul56 = fmul float %60, %63
  %sub57 = fsub float %add52, %mul56
  store float %sub57, float* %ref.tmp37, align 4, !tbaa !8
  %64 = bitcast float* %ref.tmp58 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #7
  %65 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats59 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %65, i32 0, i32 0
  %arrayidx60 = getelementptr inbounds [4 x float], [4 x float]* %m_floats59, i32 0, i32 3
  %66 = load float, float* %arrayidx60, align 4, !tbaa !8
  %67 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %68 = bitcast %class.btQuaternion* %67 to %class.btQuadWord*
  %m_floats61 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %68, i32 0, i32 0
  %arrayidx62 = getelementptr inbounds [4 x float], [4 x float]* %m_floats61, i32 0, i32 3
  %69 = load float, float* %arrayidx62, align 4, !tbaa !8
  %mul63 = fmul float %66, %69
  %70 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats64 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %70, i32 0, i32 0
  %arrayidx65 = getelementptr inbounds [4 x float], [4 x float]* %m_floats64, i32 0, i32 0
  %71 = load float, float* %arrayidx65, align 4, !tbaa !8
  %72 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %73 = bitcast %class.btQuaternion* %72 to %class.btQuadWord*
  %call66 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %73)
  %74 = load float, float* %call66, align 4, !tbaa !8
  %mul67 = fmul float %71, %74
  %sub68 = fsub float %mul63, %mul67
  %75 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats69 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %75, i32 0, i32 0
  %arrayidx70 = getelementptr inbounds [4 x float], [4 x float]* %m_floats69, i32 0, i32 1
  %76 = load float, float* %arrayidx70, align 4, !tbaa !8
  %77 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %78 = bitcast %class.btQuaternion* %77 to %class.btQuadWord*
  %call71 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %78)
  %79 = load float, float* %call71, align 4, !tbaa !8
  %mul72 = fmul float %76, %79
  %sub73 = fsub float %sub68, %mul72
  %80 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats74 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %80, i32 0, i32 0
  %arrayidx75 = getelementptr inbounds [4 x float], [4 x float]* %m_floats74, i32 0, i32 2
  %81 = load float, float* %arrayidx75, align 4, !tbaa !8
  %82 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %83 = bitcast %class.btQuaternion* %82 to %class.btQuadWord*
  %call76 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %83)
  %84 = load float, float* %call76, align 4, !tbaa !8
  %mul77 = fmul float %81, %84
  %sub78 = fsub float %sub73, %mul77
  store float %sub78, float* %ref.tmp58, align 4, !tbaa !8
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp37, float* nonnull align 4 dereferenceable(4) %ref.tmp58)
  %85 = bitcast float* %ref.tmp58 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #7
  %86 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #7
  %87 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #7
  %88 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #7
  ret %class.btQuaternion* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getXEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getYEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getZEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  store float* %_w, float** %_w.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !8
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !8
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !8
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !8
  %6 = load float*, float** %_w.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4, !tbaa !8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !41
  ret i32 %0
}

define linkonce_odr hidden void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %xs = alloca float, align 4
  %ys = alloca float, align 4
  %zs = alloca float, align 4
  %wx = alloca float, align 4
  %wy = alloca float, align 4
  %wz = alloca float, align 4
  %xx = alloca float, align 4
  %xy = alloca float, align 4
  %xz = alloca float, align 4
  %yy = alloca float, align 4
  %yz = alloca float, align 4
  %zz = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp34 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast float* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %1)
  store float %call, float* %d, align 4, !tbaa !8
  %2 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load float, float* %d, align 4, !tbaa !8
  %div = fdiv float 2.000000e+00, %3
  store float %div, float* %s, align 4, !tbaa !8
  %4 = bitcast float* %xs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %6 = bitcast %class.btQuaternion* %5 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %6)
  %7 = load float, float* %call2, align 4, !tbaa !8
  %8 = load float, float* %s, align 4, !tbaa !8
  %mul = fmul float %7, %8
  store float %mul, float* %xs, align 4, !tbaa !8
  %9 = bitcast float* %ys to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %11)
  %12 = load float, float* %call3, align 4, !tbaa !8
  %13 = load float, float* %s, align 4, !tbaa !8
  %mul4 = fmul float %12, %13
  store float %mul4, float* %ys, align 4, !tbaa !8
  %14 = bitcast float* %zs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #7
  %15 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %16 = bitcast %class.btQuaternion* %15 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %16)
  %17 = load float, float* %call5, align 4, !tbaa !8
  %18 = load float, float* %s, align 4, !tbaa !8
  %mul6 = fmul float %17, %18
  store float %mul6, float* %zs, align 4, !tbaa !8
  %19 = bitcast float* %wx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #7
  %20 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %21 = bitcast %class.btQuaternion* %20 to %class.btQuadWord*
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %21)
  %22 = load float, float* %call7, align 4, !tbaa !8
  %23 = load float, float* %xs, align 4, !tbaa !8
  %mul8 = fmul float %22, %23
  store float %mul8, float* %wx, align 4, !tbaa !8
  %24 = bitcast float* %wy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #7
  %25 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %26 = bitcast %class.btQuaternion* %25 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %26)
  %27 = load float, float* %call9, align 4, !tbaa !8
  %28 = load float, float* %ys, align 4, !tbaa !8
  %mul10 = fmul float %27, %28
  store float %mul10, float* %wy, align 4, !tbaa !8
  %29 = bitcast float* %wz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #7
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %31)
  %32 = load float, float* %call11, align 4, !tbaa !8
  %33 = load float, float* %zs, align 4, !tbaa !8
  %mul12 = fmul float %32, %33
  store float %mul12, float* %wz, align 4, !tbaa !8
  %34 = bitcast float* %xx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #7
  %35 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %36 = bitcast %class.btQuaternion* %35 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %36)
  %37 = load float, float* %call13, align 4, !tbaa !8
  %38 = load float, float* %xs, align 4, !tbaa !8
  %mul14 = fmul float %37, %38
  store float %mul14, float* %xx, align 4, !tbaa !8
  %39 = bitcast float* %xy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #7
  %40 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %41 = bitcast %class.btQuaternion* %40 to %class.btQuadWord*
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %41)
  %42 = load float, float* %call15, align 4, !tbaa !8
  %43 = load float, float* %ys, align 4, !tbaa !8
  %mul16 = fmul float %42, %43
  store float %mul16, float* %xy, align 4, !tbaa !8
  %44 = bitcast float* %xz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #7
  %45 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %46 = bitcast %class.btQuaternion* %45 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %46)
  %47 = load float, float* %call17, align 4, !tbaa !8
  %48 = load float, float* %zs, align 4, !tbaa !8
  %mul18 = fmul float %47, %48
  store float %mul18, float* %xz, align 4, !tbaa !8
  %49 = bitcast float* %yy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #7
  %50 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %51 = bitcast %class.btQuaternion* %50 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %51)
  %52 = load float, float* %call19, align 4, !tbaa !8
  %53 = load float, float* %ys, align 4, !tbaa !8
  %mul20 = fmul float %52, %53
  store float %mul20, float* %yy, align 4, !tbaa !8
  %54 = bitcast float* %yz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #7
  %55 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %56 = bitcast %class.btQuaternion* %55 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %56)
  %57 = load float, float* %call21, align 4, !tbaa !8
  %58 = load float, float* %zs, align 4, !tbaa !8
  %mul22 = fmul float %57, %58
  store float %mul22, float* %yz, align 4, !tbaa !8
  %59 = bitcast float* %zz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #7
  %60 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %61 = bitcast %class.btQuaternion* %60 to %class.btQuadWord*
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %61)
  %62 = load float, float* %call23, align 4, !tbaa !8
  %63 = load float, float* %zs, align 4, !tbaa !8
  %mul24 = fmul float %62, %63
  store float %mul24, float* %zz, align 4, !tbaa !8
  %64 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #7
  %65 = load float, float* %yy, align 4, !tbaa !8
  %66 = load float, float* %zz, align 4, !tbaa !8
  %add = fadd float %65, %66
  %sub = fsub float 1.000000e+00, %add
  store float %sub, float* %ref.tmp, align 4, !tbaa !8
  %67 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #7
  %68 = load float, float* %xy, align 4, !tbaa !8
  %69 = load float, float* %wz, align 4, !tbaa !8
  %sub26 = fsub float %68, %69
  store float %sub26, float* %ref.tmp25, align 4, !tbaa !8
  %70 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #7
  %71 = load float, float* %xz, align 4, !tbaa !8
  %72 = load float, float* %wy, align 4, !tbaa !8
  %add28 = fadd float %71, %72
  store float %add28, float* %ref.tmp27, align 4, !tbaa !8
  %73 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #7
  %74 = load float, float* %xy, align 4, !tbaa !8
  %75 = load float, float* %wz, align 4, !tbaa !8
  %add30 = fadd float %74, %75
  store float %add30, float* %ref.tmp29, align 4, !tbaa !8
  %76 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #7
  %77 = load float, float* %xx, align 4, !tbaa !8
  %78 = load float, float* %zz, align 4, !tbaa !8
  %add32 = fadd float %77, %78
  %sub33 = fsub float 1.000000e+00, %add32
  store float %sub33, float* %ref.tmp31, align 4, !tbaa !8
  %79 = bitcast float* %ref.tmp34 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #7
  %80 = load float, float* %yz, align 4, !tbaa !8
  %81 = load float, float* %wx, align 4, !tbaa !8
  %sub35 = fsub float %80, %81
  store float %sub35, float* %ref.tmp34, align 4, !tbaa !8
  %82 = bitcast float* %ref.tmp36 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #7
  %83 = load float, float* %xz, align 4, !tbaa !8
  %84 = load float, float* %wy, align 4, !tbaa !8
  %sub37 = fsub float %83, %84
  store float %sub37, float* %ref.tmp36, align 4, !tbaa !8
  %85 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %85) #7
  %86 = load float, float* %yz, align 4, !tbaa !8
  %87 = load float, float* %wx, align 4, !tbaa !8
  %add39 = fadd float %86, %87
  store float %add39, float* %ref.tmp38, align 4, !tbaa !8
  %88 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #7
  %89 = load float, float* %xx, align 4, !tbaa !8
  %90 = load float, float* %yy, align 4, !tbaa !8
  %add41 = fadd float %89, %90
  %sub42 = fsub float 1.000000e+00, %add41
  store float %sub42, float* %ref.tmp40, align 4, !tbaa !8
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp34, float* nonnull align 4 dereferenceable(4) %ref.tmp36, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp40)
  %91 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #7
  %92 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #7
  %93 = bitcast float* %ref.tmp36 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #7
  %94 = bitcast float* %ref.tmp34 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #7
  %95 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #7
  %96 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #7
  %97 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #7
  %98 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #7
  %99 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #7
  %100 = bitcast float* %zz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #7
  %101 = bitcast float* %yz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #7
  %102 = bitcast float* %yy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #7
  %103 = bitcast float* %xz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #7
  %104 = bitcast float* %xy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #7
  %105 = bitcast float* %xx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #7
  %106 = bitcast float* %wz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #7
  %107 = bitcast float* %wy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #7
  %108 = bitcast float* %wx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #7
  %109 = bitcast float* %zs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #7
  %110 = bitcast float* %ys to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #7
  %111 = bitcast float* %xs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #7
  %112 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #7
  %113 = bitcast float* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #7
  ret void
}

define linkonce_odr hidden float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #4 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !8
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call, align 4, !tbaa !8
  %mul = fmul float %1, %4
  %5 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %5, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %6 = load float, float* %arrayidx3, align 4, !tbaa !8
  %7 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %8 = bitcast %class.btQuaternion* %7 to %class.btQuadWord*
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %8)
  %9 = load float, float* %call4, align 4, !tbaa !8
  %mul5 = fmul float %6, %9
  %add = fadd float %mul, %mul5
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %11 = load float, float* %arrayidx7, align 4, !tbaa !8
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %13)
  %14 = load float, float* %call8, align 4, !tbaa !8
  %mul9 = fmul float %11, %14
  %add10 = fadd float %add, %mul9
  %15 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats11 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %15, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 3
  %16 = load float, float* %arrayidx12, align 4, !tbaa !8
  %17 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %18 = bitcast %class.btQuaternion* %17 to %class.btQuadWord*
  %m_floats13 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %18, i32 0, i32 0
  %arrayidx14 = getelementptr inbounds [4 x float], [4 x float]* %m_floats13, i32 0, i32 3
  %19 = load float, float* %arrayidx14, align 4, !tbaa !8
  %mul15 = fmul float %16, %19
  %add16 = fadd float %add10, %mul15
  ret float %add16
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  ret %class.btQuadWord* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !8
  %0 = load float, float* %y.addr, align 4, !tbaa !8
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #5

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !8
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !8
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !8
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !8
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !8
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !8
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !8
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !8
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !8
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this, i32 %r1, i32 %c1, i32 %r2, i32 %c2) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %r1.addr = alloca i32, align 4
  %c1.addr = alloca i32, align 4
  %r2.addr = alloca i32, align 4
  %c2.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %r1, i32* %r1.addr, align 4, !tbaa !6
  store i32 %c1, i32* %c1.addr, align 4, !tbaa !6
  store i32 %r2, i32* %r2.addr, align 4, !tbaa !6
  store i32 %c2, i32* %c2.addr, align 4, !tbaa !6
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %r1.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx)
  %1 = load i32, i32* %c1.addr, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 %1
  %2 = load float, float* %arrayidx2, align 4, !tbaa !8
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %3 = load i32, i32* %r2.addr, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 %3
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx4)
  %4 = load i32, i32* %c2.addr, align 4, !tbaa !6
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %4
  %5 = load float, float* %arrayidx6, align 4, !tbaa !8
  %mul = fmul float %2, %5
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %6 = load i32, i32* %r1.addr, align 4, !tbaa !6
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 %6
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx8)
  %7 = load i32, i32* %c2.addr, align 4, !tbaa !6
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %7
  %8 = load float, float* %arrayidx10, align 4, !tbaa !8
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %9 = load i32, i32* %r2.addr, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 %9
  %call13 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx12)
  %10 = load i32, i32* %c1.addr, align 4, !tbaa !6
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 %10
  %11 = load float, float* %arrayidx14, align 4, !tbaa !8
  %mul15 = fmul float %8, %11
  %sub = fsub float %mul, %mul15
  ret float %sub
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #5

define linkonce_odr hidden void @_ZN12btQuaternion11setRotationERK9btVector3RKf(%class.btQuaternion* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, float* nonnull align 4 dereferenceable(4) %_angle) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %axis.addr = alloca %class.btVector3*, align 4
  %_angle.addr = alloca float*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %axis, %class.btVector3** %axis.addr, align 4, !tbaa !2
  store float* %_angle, float** %_angle.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast float* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %1)
  store float %call, float* %d, align 4, !tbaa !8
  %2 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load float*, float** %_angle.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !8
  %mul = fmul float %4, 5.000000e-01
  %call2 = call float @_Z5btSinf(float %mul)
  %5 = load float, float* %d, align 4, !tbaa !8
  %div = fdiv float %call2, %5
  store float %div, float* %s, align 4, !tbaa !8
  %6 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %7 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %8 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %8)
  %9 = load float, float* %call3, align 4, !tbaa !8
  %10 = load float, float* %s, align 4, !tbaa !8
  %mul4 = fmul float %9, %10
  store float %mul4, float* %ref.tmp, align 4, !tbaa !8
  %11 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #7
  %12 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %12)
  %13 = load float, float* %call6, align 4, !tbaa !8
  %14 = load float, float* %s, align 4, !tbaa !8
  %mul7 = fmul float %13, %14
  store float %mul7, float* %ref.tmp5, align 4, !tbaa !8
  %15 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #7
  %16 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %16)
  %17 = load float, float* %call9, align 4, !tbaa !8
  %18 = load float, float* %s, align 4, !tbaa !8
  %mul10 = fmul float %17, %18
  store float %mul10, float* %ref.tmp8, align 4, !tbaa !8
  %19 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #7
  %20 = load float*, float** %_angle.addr, align 4, !tbaa !2
  %21 = load float, float* %20, align 4, !tbaa !8
  %mul12 = fmul float %21, 5.000000e-01
  %call13 = call float @_Z5btCosf(float %mul12)
  store float %call13, float* %ref.tmp11, align 4, !tbaa !8
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %6, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11)
  %22 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #7
  %23 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #7
  %24 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #7
  %25 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #7
  %26 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #7
  %27 = bitcast float* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z5btSinf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !8
  %0 = load float, float* %x.addr, align 4, !tbaa !8
  %1 = call float @llvm.sin.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z5btCosf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !8
  %0 = load float, float* %x.addr, align 4, !tbaa !8
  %1 = call float @llvm.cos.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sin.f32(float) #5

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.cos.f32(float) #5

define linkonce_odr hidden float @_ZNK12btQuaternion6lengthEv(%class.btQuaternion* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaterniondVERKf(%class.btQuaternion* %this, float* nonnull align 4 dereferenceable(4) %s) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !8
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !8
  %call = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKf(%class.btQuaternion* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #7
  ret %class.btQuaternion* %call
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKf(%class.btQuaternion* %this, float* nonnull align 4 dereferenceable(4) %s) #4 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %s.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %2 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %2, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %3 = load float, float* %arrayidx, align 4, !tbaa !8
  %mul = fmul float %3, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !8
  %4 = load float*, float** %s.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !8
  %6 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !8
  %mul4 = fmul float %7, %5
  store float %mul4, float* %arrayidx3, align 4, !tbaa !8
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !8
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats5 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %11 = load float, float* %arrayidx6, align 4, !tbaa !8
  %mul7 = fmul float %11, %9
  store float %mul7, float* %arrayidx6, align 4, !tbaa !8
  %12 = load float*, float** %s.addr, align 4, !tbaa !2
  %13 = load float, float* %12, align 4, !tbaa !8
  %14 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats8 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %14, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 3
  %15 = load float, float* %arrayidx9, align 4, !tbaa !8
  %mul10 = fmul float %15, %13
  store float %mul10, float* %arrayidx9, align 4, !tbaa !8
  ret %class.btQuaternion* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI15btMultibodyLinkE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btMultibodyLink*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI15btMultibodyLinkE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btMultibodyLink** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayI15btMultibodyLinkE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.btMultibodyLink*
  store %struct.btMultibodyLink* %3, %struct.btMultibodyLink** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv(%class.btAlignedObjectArray* %this1)
  %4 = load %struct.btMultibodyLink*, %struct.btMultibodyLink** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %struct.btMultibodyLink* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI15btMultibodyLinkE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI15btMultibodyLinkE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !60
  %5 = load %struct.btMultibodyLink*, %struct.btMultibodyLink** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btMultibodyLink* %5, %struct.btMultibodyLink** %m_data, align 4, !tbaa !40
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !61
  %7 = bitcast %struct.btMultibodyLink** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN15btMultibodyLinknwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !62
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI15btMultibodyLinkE8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !61
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI15btMultibodyLinkE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #3 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call %struct.btMultibodyLink* @_ZN18btAlignedAllocatorI15btMultibodyLinkLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %struct.btMultibodyLink** null)
  %2 = bitcast %struct.btMultibodyLink* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %struct.btMultibodyLink* %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btMultibodyLink*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store %struct.btMultibodyLink* %dest, %struct.btMultibodyLink** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.btMultibodyLink*, %struct.btMultibodyLink** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %4, i32 %5
  %6 = bitcast %struct.btMultibodyLink* %arrayidx to i8*
  %call = call i8* @_ZN15btMultibodyLinknwEmPv(i32 188, i8* %6)
  %7 = bitcast i8* %call to %struct.btMultibodyLink*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %struct.btMultibodyLink*, %struct.btMultibodyLink** %m_data, align 4, !tbaa !40
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %8, i32 %9
  %10 = bitcast %struct.btMultibodyLink* %7 to i8*
  %11 = bitcast %struct.btMultibodyLink* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 188, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI15btMultibodyLinkE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %struct.btMultibodyLink*, %struct.btMultibodyLink** %m_data, align 4, !tbaa !40
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI15btMultibodyLinkE10deallocateEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btMultibodyLink*, %struct.btMultibodyLink** %m_data, align 4, !tbaa !40
  %tobool = icmp ne %struct.btMultibodyLink* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !60, !range !30
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btMultibodyLink*, %struct.btMultibodyLink** %m_data4, align 4, !tbaa !40
  call void @_ZN18btAlignedAllocatorI15btMultibodyLinkLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %struct.btMultibodyLink* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btMultibodyLink* null, %struct.btMultibodyLink** %m_data5, align 4, !tbaa !40
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden %struct.btMultibodyLink* @_ZN18btAlignedAllocatorI15btMultibodyLinkLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %struct.btMultibodyLink** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btMultibodyLink**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store %struct.btMultibodyLink** %hint, %struct.btMultibodyLink*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 188, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btMultibodyLink*
  ret %struct.btMultibodyLink* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #6

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI15btMultibodyLinkLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %struct.btMultibodyLink* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %struct.btMultibodyLink*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store %struct.btMultibodyLink* %ptr, %struct.btMultibodyLink** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %struct.btMultibodyLink*, %struct.btMultibodyLink** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btMultibodyLink* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #6

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI15btMultibodyLinkLj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI15btMultibodyLinkE4initEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !60
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btMultibodyLink* null, %struct.btMultibodyLink** %m_data, align 4, !tbaa !40
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !41
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !61
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorIP23btMultiBodyLinkColliderLj16EEC2Ev(%class.btAlignedAllocator.1* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  ret %class.btAlignedAllocator.1* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP23btMultiBodyLinkColliderE4initEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !64
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %class.btMultiBodyLinkCollider** null, %class.btMultiBodyLinkCollider*** %m_data, align 4, !tbaa !65
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !66
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !67
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorIfLj16EEC2Ev(%class.btAlignedAllocator.5* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  ret %class.btAlignedAllocator.5* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !68
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store float* null, float** %m_data, align 4, !tbaa !54
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !55
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !69
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.9* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator.9* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  ret %class.btAlignedAllocator.9* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !70
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data, align 4, !tbaa !50
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !51
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !71
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.13* @_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EEC2Ev(%class.btAlignedAllocator.13* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.13*, align 4
  store %class.btAlignedAllocator.13* %this, %class.btAlignedAllocator.13** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.13*, %class.btAlignedAllocator.13** %this.addr, align 4
  ret %class.btAlignedAllocator.13* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btMatrix3x3E4initEv(%class.btAlignedObjectArray.12* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !72
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store %class.btMatrix3x3* null, %class.btMatrix3x3** %m_data, align 4, !tbaa !52
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !53
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !73
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI15btMultibodyLinkE5clearEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI15btMultibodyLinkE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI15btMultibodyLinkE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI15btMultibodyLinkE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP23btMultiBodyLinkColliderE5clearEv(%class.btAlignedObjectArray.0* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP23btMultiBodyLinkColliderE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIP23btMultiBodyLinkColliderE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP23btMultiBodyLinkColliderE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIP23btMultiBodyLinkColliderE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP23btMultiBodyLinkColliderE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %4 = load %class.btMultiBodyLinkCollider**, %class.btMultiBodyLinkCollider*** %m_data, align 4, !tbaa !65
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP23btMultiBodyLinkColliderE4sizeEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !66
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP23btMultiBodyLinkColliderE10deallocateEv(%class.btAlignedObjectArray.0* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %class.btMultiBodyLinkCollider**, %class.btMultiBodyLinkCollider*** %m_data, align 4, !tbaa !65
  %tobool = icmp ne %class.btMultiBodyLinkCollider** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !64, !range !30
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %class.btMultiBodyLinkCollider**, %class.btMultiBodyLinkCollider*** %m_data4, align 4, !tbaa !65
  call void @_ZN18btAlignedAllocatorIP23btMultiBodyLinkColliderLj16EE10deallocateEPS1_(%class.btAlignedAllocator.1* %m_allocator, %class.btMultiBodyLinkCollider** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %class.btMultiBodyLinkCollider** null, %class.btMultiBodyLinkCollider*** %m_data5, align 4, !tbaa !65
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP23btMultiBodyLinkColliderLj16EE10deallocateEPS1_(%class.btAlignedAllocator.1* %this, %class.btMultiBodyLinkCollider** %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca %class.btMultiBodyLinkCollider**, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  store %class.btMultiBodyLinkCollider** %ptr, %class.btMultiBodyLinkCollider*** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load %class.btMultiBodyLinkCollider**, %class.btMultiBodyLinkCollider*** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btMultiBodyLinkCollider** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE5clearEv(%class.btAlignedObjectArray.4* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.4* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.4* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %4 = load float*, float** %m_data, align 4, !tbaa !54
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !55
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.4* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4, !tbaa !54
  %tobool = icmp ne float* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !68, !range !30
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load float*, float** %m_data4, align 4, !tbaa !54
  call void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator.5* %m_allocator, float* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store float* null, float** %m_data5, align 4, !tbaa !54
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator.5* %this, float* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %ptr.addr = alloca float*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4, !tbaa !2
  store float* %ptr, float** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load float*, float** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast float* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray.8* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray.8* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.8* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %4 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !50
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !51
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.8* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !50
  %tobool = icmp ne %class.btVector3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !70, !range !30
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %2 = load %class.btVector3*, %class.btVector3** %m_data4, align 4, !tbaa !50
  call void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.9* %m_allocator, %class.btVector3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data5, align 4, !tbaa !50
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.9* %this, %class.btVector3* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %ptr.addr = alloca %class.btVector3*, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %ptr, %class.btVector3** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVector3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btMatrix3x3E5clearEv(%class.btAlignedObjectArray.12* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI11btMatrix3x3E4sizeEv(%class.btAlignedObjectArray.12* %this1)
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E7destroyEii(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E10deallocateEv(%class.btAlignedObjectArray.12* %this1)
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E4initEv(%class.btAlignedObjectArray.12* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btMatrix3x3E7destroyEii(%class.btAlignedObjectArray.12* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m_data, align 4, !tbaa !52
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI11btMatrix3x3E4sizeEv(%class.btAlignedObjectArray.12* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !53
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btMatrix3x3E10deallocateEv(%class.btAlignedObjectArray.12* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m_data, align 4, !tbaa !52
  %tobool = icmp ne %class.btMatrix3x3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !72, !range !30
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m_data4, align 4, !tbaa !52
  call void @_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.13* %m_allocator, %class.btMatrix3x3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store %class.btMatrix3x3* null, %class.btMatrix3x3** %m_data5, align 4, !tbaa !52
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.13* %this, %class.btMatrix3x3* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.13*, align 4
  %ptr.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btAlignedAllocator.13* %this, %class.btAlignedAllocator.13** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %ptr, %class.btMatrix3x3** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.13*, %class.btAlignedAllocator.13** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btMatrix3x3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray.8* %this, i32 %_Count) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btVector3*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray.8* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btVector3** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray.8* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btVector3*
  store %class.btVector3* %3, %class.btVector3** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %this1)
  %4 = load %class.btVector3*, %class.btVector3** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call3, %class.btVector3* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.8* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !70
  %5 = load %class.btVector3*, %class.btVector3** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %class.btVector3* %5, %class.btVector3** %m_data, align 4, !tbaa !50
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !71
  %7 = bitcast %class.btVector3** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN9btVector3nwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !62
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !71
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray.8* %this, i32 %size) #3 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.9* %m_allocator, i32 %1, %class.btVector3** null)
  %2 = bitcast %class.btVector3* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray.8* %this, i32 %start, i32 %end, %class.btVector3* %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store %class.btVector3* %dest, %class.btVector3** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btVector3*, %class.btVector3** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 %5
  %6 = bitcast %class.btVector3* %arrayidx to i8*
  %call = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %6)
  %7 = bitcast i8* %call to %class.btVector3*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %8 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !50
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 %9
  %10 = bitcast %class.btVector3* %7 to i8*
  %11 = bitcast %class.btVector3* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !28
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #7
  ret void
}

define linkonce_odr hidden %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.9* %this, i32 %n, %class.btVector3** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btVector3**, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store %class.btVector3** %hint, %class.btVector3*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btVector3*
  ret %class.btVector3* %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btMatrix3x3E7reserveEi(%class.btAlignedObjectArray.12* %this, i32 %_Count) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btMatrix3x3*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI11btMatrix3x3E8capacityEv(%class.btAlignedObjectArray.12* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btMatrix3x3** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayI11btMatrix3x3E8allocateEi(%class.btAlignedObjectArray.12* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btMatrix3x3*
  store %class.btMatrix3x3* %3, %class.btMatrix3x3** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI11btMatrix3x3E4sizeEv(%class.btAlignedObjectArray.12* %this1)
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI11btMatrix3x3E4copyEiiPS0_(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call3, %class.btMatrix3x3* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI11btMatrix3x3E4sizeEv(%class.btAlignedObjectArray.12* %this1)
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E7destroyEii(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E10deallocateEv(%class.btAlignedObjectArray.12* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !72
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store %class.btMatrix3x3* %5, %class.btMatrix3x3** %m_data, align 4, !tbaa !52
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !73
  %7 = bitcast %class.btMatrix3x3** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #3 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !28
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !28
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !28
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI11btMatrix3x3E8capacityEv(%class.btAlignedObjectArray.12* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !73
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI11btMatrix3x3E8allocateEi(%class.btAlignedObjectArray.12* %this, i32 %size) #3 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call %class.btMatrix3x3* @_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.13* %m_allocator, i32 %1, %class.btMatrix3x3** null)
  %2 = bitcast %class.btMatrix3x3* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI11btMatrix3x3E4copyEiiPS0_(%class.btAlignedObjectArray.12* %this, i32 %start, i32 %end, %class.btMatrix3x3* %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btMatrix3x3*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store %class.btMatrix3x3* %dest, %class.btMatrix3x3** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %4, i32 %5
  %6 = bitcast %class.btMatrix3x3* %arrayidx to i8*
  %7 = bitcast i8* %6 to %class.btMatrix3x3*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m_data, align 4, !tbaa !52
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %8, i32 %9
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %7, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #7
  ret void
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.13* %this, i32 %n, %class.btMatrix3x3** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.13*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btMatrix3x3**, align 4
  store %class.btAlignedAllocator.13* %this, %class.btAlignedAllocator.13** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store %class.btMatrix3x3** %hint, %class.btMatrix3x3*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.13*, %class.btAlignedAllocator.13** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 48, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btMatrix3x3*
  ret %class.btMatrix3x3* %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray.4* %this, i32 %_Count) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca float*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast float** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray.4* %this1, i32 %2)
  %3 = bitcast i8* %call2 to float*
  store float* %3, float** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %4 = load float*, float** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call3, float* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !68
  %5 = load float*, float** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store float* %5, float** %m_data, align 4, !tbaa !54
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !69
  %7 = bitcast float** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !69
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray.4* %this, i32 %size) #3 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator.5* %m_allocator, i32 %1, float** null)
  %2 = bitcast float* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.4* %this, i32 %start, i32 %end, float* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca float*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store float* %dest, float** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load float*, float** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %4, i32 %5
  %6 = bitcast float* %arrayidx to i8*
  %7 = bitcast i8* %6 to float*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %8 = load float*, float** %m_data, align 4, !tbaa !54
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds float, float* %8, i32 %9
  %10 = load float, float* %arrayidx2, align 4, !tbaa !8
  store float %10, float* %7, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #7
  ret void
}

define linkonce_odr hidden float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator.5* %this, i32 %n, float** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca float**, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store float** %hint, float*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to float*
  ret float* %1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.17* @_ZN18btAlignedAllocatorI12btQuaternionLj16EEC2Ev(%class.btAlignedAllocator.17* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.17*, align 4
  store %class.btAlignedAllocator.17* %this, %class.btAlignedAllocator.17** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.17*, %class.btAlignedAllocator.17** %this.addr, align 4
  ret %class.btAlignedAllocator.17* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btQuaternionE4initEv(%class.btAlignedObjectArray.16* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !74
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  store %class.btQuaternion* null, %class.btQuaternion** %m_data, align 4, !tbaa !56
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !59
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !75
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btQuaternionE5clearEv(%class.btAlignedObjectArray.16* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI12btQuaternionE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  call void @_ZN20btAlignedObjectArrayI12btQuaternionE7destroyEii(%class.btAlignedObjectArray.16* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI12btQuaternionE10deallocateEv(%class.btAlignedObjectArray.16* %this1)
  call void @_ZN20btAlignedObjectArrayI12btQuaternionE4initEv(%class.btAlignedObjectArray.16* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btQuaternionE7destroyEii(%class.btAlignedObjectArray.16* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %4 = load %class.btQuaternion*, %class.btQuaternion** %m_data, align 4, !tbaa !56
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btQuaternion, %class.btQuaternion* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI12btQuaternionE4sizeEv(%class.btAlignedObjectArray.16* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !59
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btQuaternionE10deallocateEv(%class.btAlignedObjectArray.16* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %m_data, align 4, !tbaa !56
  %tobool = icmp ne %class.btQuaternion* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !74, !range !30
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %2 = load %class.btQuaternion*, %class.btQuaternion** %m_data4, align 4, !tbaa !56
  call void @_ZN18btAlignedAllocatorI12btQuaternionLj16EE10deallocateEPS0_(%class.btAlignedAllocator.17* %m_allocator, %class.btQuaternion* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  store %class.btQuaternion* null, %class.btQuaternion** %m_data5, align 4, !tbaa !56
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI12btQuaternionLj16EE10deallocateEPS0_(%class.btAlignedAllocator.17* %this, %class.btQuaternion* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.17*, align 4
  %ptr.addr = alloca %class.btQuaternion*, align 4
  store %class.btAlignedAllocator.17* %this, %class.btAlignedAllocator.17** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %ptr, %class.btQuaternion** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.17*, %class.btAlignedAllocator.17** %this.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btQuaternion* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btQuaternionE7reserveEi(%class.btAlignedObjectArray.16* %this, i32 %_Count) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btQuaternion*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI12btQuaternionE8capacityEv(%class.btAlignedObjectArray.16* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btQuaternion** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayI12btQuaternionE8allocateEi(%class.btAlignedObjectArray.16* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btQuaternion*
  store %class.btQuaternion* %3, %class.btQuaternion** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI12btQuaternionE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  %4 = load %class.btQuaternion*, %class.btQuaternion** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI12btQuaternionE4copyEiiPS0_(%class.btAlignedObjectArray.16* %this1, i32 0, i32 %call3, %class.btQuaternion* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI12btQuaternionE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  call void @_ZN20btAlignedObjectArrayI12btQuaternionE7destroyEii(%class.btAlignedObjectArray.16* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI12btQuaternionE10deallocateEv(%class.btAlignedObjectArray.16* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !74
  %5 = load %class.btQuaternion*, %class.btQuaternion** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  store %class.btQuaternion* %5, %class.btQuaternion** %m_data, align 4, !tbaa !56
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !75
  %7 = bitcast %class.btQuaternion** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI12btQuaternionE8capacityEv(%class.btAlignedObjectArray.16* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !75
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI12btQuaternionE8allocateEi(%class.btAlignedObjectArray.16* %this, i32 %size) #3 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call %class.btQuaternion* @_ZN18btAlignedAllocatorI12btQuaternionLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.17* %m_allocator, i32 %1, %class.btQuaternion** null)
  %2 = bitcast %class.btQuaternion* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI12btQuaternionE4copyEiiPS0_(%class.btAlignedObjectArray.16* %this, i32 %start, i32 %end, %class.btQuaternion* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btQuaternion*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store %class.btQuaternion* %dest, %class.btQuaternion** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btQuaternion*, %class.btQuaternion** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btQuaternion, %class.btQuaternion* %4, i32 %5
  %6 = bitcast %class.btQuaternion* %arrayidx to i8*
  %7 = bitcast i8* %6 to %class.btQuaternion*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %8 = load %class.btQuaternion*, %class.btQuaternion** %m_data, align 4, !tbaa !56
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds %class.btQuaternion, %class.btQuaternion* %8, i32 %9
  %10 = bitcast %class.btQuaternion* %7 to i8*
  %11 = bitcast %class.btQuaternion* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #7
  ret void
}

define linkonce_odr hidden %class.btQuaternion* @_ZN18btAlignedAllocatorI12btQuaternionLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.17* %this, i32 %n, %class.btQuaternion** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.17*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btQuaternion**, align 4
  store %class.btAlignedAllocator.17* %this, %class.btAlignedAllocator.17** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store %class.btQuaternion** %hint, %class.btQuaternion*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.17*, %class.btAlignedAllocator.17** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btQuaternion*
  ret %class.btQuaternion* %1
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind readnone speculatable willreturn }
attributes #6 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"float", !4, i64 0}
!10 = !{!11, !11, i64 0}
!11 = !{!"bool", !4, i64 0}
!12 = !{!13, !3, i64 0}
!13 = !{!"_ZTS11btMultiBody", !3, i64 0, !14, i64 4, !15, i64 20, !9, i64 36, !14, i64 40, !14, i64 56, !14, i64 72, !16, i64 88, !18, i64 108, !20, i64 128, !22, i64 148, !24, i64 168, !26, i64 188, !26, i64 236, !26, i64 284, !26, i64 332, !11, i64 380, !11, i64 381, !11, i64 382, !9, i64 384, !7, i64 388, !9, i64 392, !9, i64 396, !11, i64 400, !9, i64 404, !11, i64 408}
!14 = !{!"_ZTS9btVector3", !4, i64 0}
!15 = !{!"_ZTS12btQuaternion"}
!16 = !{!"_ZTS20btAlignedObjectArrayI15btMultibodyLinkE", !17, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !11, i64 16}
!17 = !{!"_ZTS18btAlignedAllocatorI15btMultibodyLinkLj16EE"}
!18 = !{!"_ZTS20btAlignedObjectArrayIP23btMultiBodyLinkColliderE", !19, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !11, i64 16}
!19 = !{!"_ZTS18btAlignedAllocatorIP23btMultiBodyLinkColliderLj16EE"}
!20 = !{!"_ZTS20btAlignedObjectArrayIfE", !21, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !11, i64 16}
!21 = !{!"_ZTS18btAlignedAllocatorIfLj16EE"}
!22 = !{!"_ZTS20btAlignedObjectArrayI9btVector3E", !23, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !11, i64 16}
!23 = !{!"_ZTS18btAlignedAllocatorI9btVector3Lj16EE"}
!24 = !{!"_ZTS20btAlignedObjectArrayI11btMatrix3x3E", !25, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !11, i64 16}
!25 = !{!"_ZTS18btAlignedAllocatorI11btMatrix3x3Lj16EE"}
!26 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!27 = !{!13, !9, i64 36}
!28 = !{i64 0, i64 16, !29}
!29 = !{!4, !4, i64 0}
!30 = !{i8 0, i8 2}
!31 = !{!13, !11, i64 380}
!32 = !{!13, !11, i64 381}
!33 = !{!13, !11, i64 382}
!34 = !{!13, !9, i64 384}
!35 = !{!13, !9, i64 392}
!36 = !{!13, !9, i64 396}
!37 = !{!13, !11, i64 400}
!38 = !{!13, !9, i64 404}
!39 = !{!13, !11, i64 408}
!40 = !{!16, !3, i64 12}
!41 = !{!16, !7, i64 4}
!42 = !{!43, !9, i64 0}
!43 = !{!"_ZTS15btMultibodyLink", !9, i64 0, !9, i64 4, !14, i64 8, !7, i64 24, !15, i64 28, !14, i64 44, !14, i64 60, !14, i64 76, !14, i64 92, !11, i64 108, !15, i64 112, !14, i64 128, !14, i64 144, !14, i64 160, !9, i64 176, !3, i64 180, !7, i64 184}
!44 = !{!43, !9, i64 4}
!45 = !{!43, !7, i64 24}
!46 = !{!43, !11, i64 108}
!47 = !{!43, !9, i64 176}
!48 = !{!43, !3, i64 180}
!49 = !{!43, !7, i64 184}
!50 = !{!22, !3, i64 12}
!51 = !{!22, !7, i64 4}
!52 = !{!24, !3, i64 12}
!53 = !{!24, !7, i64 4}
!54 = !{!20, !3, i64 12}
!55 = !{!20, !7, i64 4}
!56 = !{!57, !3, i64 12}
!57 = !{!"_ZTS20btAlignedObjectArrayI12btQuaternionE", !58, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !11, i64 16}
!58 = !{!"_ZTS18btAlignedAllocatorI12btQuaternionLj16EE"}
!59 = !{!57, !7, i64 4}
!60 = !{!16, !11, i64 16}
!61 = !{!16, !7, i64 8}
!62 = !{!63, !63, i64 0}
!63 = !{!"long", !4, i64 0}
!64 = !{!18, !11, i64 16}
!65 = !{!18, !3, i64 12}
!66 = !{!18, !7, i64 4}
!67 = !{!18, !7, i64 8}
!68 = !{!20, !11, i64 16}
!69 = !{!20, !7, i64 8}
!70 = !{!22, !11, i64 16}
!71 = !{!22, !7, i64 8}
!72 = !{!24, !11, i64 16}
!73 = !{!24, !7, i64 8}
!74 = !{!57, !11, i64 16}
!75 = !{!57, !7, i64 8}
