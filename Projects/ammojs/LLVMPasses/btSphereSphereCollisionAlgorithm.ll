; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionDispatch/btSphereSphereCollisionAlgorithm.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionDispatch/btSphereSphereCollisionAlgorithm.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btSphereSphereCollisionAlgorithm = type { %class.btActivatingCollisionAlgorithm, i8, %class.btPersistentManifold* }
%class.btActivatingCollisionAlgorithm = type { %class.btCollisionAlgorithm }
%class.btCollisionAlgorithm = type { i32 (...)**, %class.btDispatcher* }
%class.btDispatcher = type { i32 (...)** }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, i32, i32, i32, i32, i8*, i8, float, float, float, float, float, float, float, i32, %class.btVector3, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%union.anon = type { i8* }
%struct.btCollisionAlgorithmConstructionInfo = type { %class.btDispatcher*, %class.btPersistentManifold* }
%struct.btCollisionObjectWrapper = type { %struct.btCollisionObjectWrapper*, %class.btCollisionShape*, %class.btCollisionObject*, %class.btTransform*, i32, i32 }
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btIDebugDraw = type opaque
%class.btManifoldResult = type { %"struct.btDiscreteCollisionDetectorInterface::Result", %class.btPersistentManifold*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i32, i32, i32, i32 }
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }
%class.btSphereShape = type { %class.btConvexInternalShape }
%class.btConvexInternalShape = type { %class.btConvexShape, %class.btVector3, %class.btVector3, float, float }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btPersistentManifold**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }

$_ZNK24btCollisionObjectWrapper18getCollisionObjectEv = comdat any

$_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold = comdat any

$_ZNK24btCollisionObjectWrapper17getCollisionShapeEv = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK24btCollisionObjectWrapper17getWorldTransformEv = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZNK13btSphereShape9getRadiusEv = comdat any

$_ZN16btManifoldResult20refreshContactPointsEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZdvRK9btVector3RKf = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZN32btSphereSphereCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE = comdat any

$_Z6btSqrtf = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK20btPersistentManifold14getNumContactsEv = comdat any

$_ZNK20btPersistentManifold8getBody0Ev = comdat any

$_ZNK17btCollisionObject17getWorldTransformEv = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_ = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_ = comdat any

@_ZTV32btSphereSphereCollisionAlgorithm = hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI32btSphereSphereCollisionAlgorithm to i8*), i8* bitcast (%class.btSphereSphereCollisionAlgorithm* (%class.btSphereSphereCollisionAlgorithm*)* @_ZN32btSphereSphereCollisionAlgorithmD1Ev to i8*), i8* bitcast (void (%class.btSphereSphereCollisionAlgorithm*)* @_ZN32btSphereSphereCollisionAlgorithmD0Ev to i8*), i8* bitcast (void (%class.btSphereSphereCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)* @_ZN32btSphereSphereCollisionAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResult to i8*), i8* bitcast (float (%class.btSphereSphereCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)* @_ZN32btSphereSphereCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult to i8*), i8* bitcast (void (%class.btSphereSphereCollisionAlgorithm*, %class.btAlignedObjectArray*)* @_ZN32btSphereSphereCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS32btSphereSphereCollisionAlgorithm = hidden constant [35 x i8] c"32btSphereSphereCollisionAlgorithm\00", align 1
@_ZTI30btActivatingCollisionAlgorithm = external constant i8*
@_ZTI32btSphereSphereCollisionAlgorithm = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([35 x i8], [35 x i8]* @_ZTS32btSphereSphereCollisionAlgorithm, i32 0, i32 0), i8* bitcast (i8** @_ZTI30btActivatingCollisionAlgorithm to i8*) }, align 4

@_ZN32btSphereSphereCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_ = hidden unnamed_addr alias %class.btSphereSphereCollisionAlgorithm* (%class.btSphereSphereCollisionAlgorithm*, %class.btPersistentManifold*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*), %class.btSphereSphereCollisionAlgorithm* (%class.btSphereSphereCollisionAlgorithm*, %class.btPersistentManifold*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN32btSphereSphereCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_
@_ZN32btSphereSphereCollisionAlgorithmD1Ev = hidden unnamed_addr alias %class.btSphereSphereCollisionAlgorithm* (%class.btSphereSphereCollisionAlgorithm*), %class.btSphereSphereCollisionAlgorithm* (%class.btSphereSphereCollisionAlgorithm*)* @_ZN32btSphereSphereCollisionAlgorithmD2Ev

define hidden %class.btSphereSphereCollisionAlgorithm* @_ZN32btSphereSphereCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_(%class.btSphereSphereCollisionAlgorithm* returned %this, %class.btPersistentManifold* %mf, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %col0Wrap, %struct.btCollisionObjectWrapper* %col1Wrap) unnamed_addr #0 {
entry:
  %retval = alloca %class.btSphereSphereCollisionAlgorithm*, align 4
  %this.addr = alloca %class.btSphereSphereCollisionAlgorithm*, align 4
  %mf.addr = alloca %class.btPersistentManifold*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %col0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %col1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %class.btSphereSphereCollisionAlgorithm* %this, %class.btSphereSphereCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %class.btPersistentManifold* %mf, %class.btPersistentManifold** %mf.addr, align 4, !tbaa !2
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %col0Wrap, %struct.btCollisionObjectWrapper** %col0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %col1Wrap, %struct.btCollisionObjectWrapper** %col1Wrap.addr, align 4, !tbaa !2
  %this1 = load %class.btSphereSphereCollisionAlgorithm*, %class.btSphereSphereCollisionAlgorithm** %this.addr, align 4
  store %class.btSphereSphereCollisionAlgorithm* %this1, %class.btSphereSphereCollisionAlgorithm** %retval, align 4
  %0 = bitcast %class.btSphereSphereCollisionAlgorithm* %this1 to %class.btActivatingCollisionAlgorithm*
  %1 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col0Wrap.addr, align 4, !tbaa !2
  %3 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col1Wrap.addr, align 4, !tbaa !2
  %call = call %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%class.btActivatingCollisionAlgorithm* %0, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %1, %struct.btCollisionObjectWrapper* %2, %struct.btCollisionObjectWrapper* %3)
  %4 = bitcast %class.btSphereSphereCollisionAlgorithm* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV32btSphereSphereCollisionAlgorithm, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %4, align 4, !tbaa !6
  %m_ownManifold = getelementptr inbounds %class.btSphereSphereCollisionAlgorithm, %class.btSphereSphereCollisionAlgorithm* %this1, i32 0, i32 1
  store i8 0, i8* %m_ownManifold, align 4, !tbaa !8
  %m_manifoldPtr = getelementptr inbounds %class.btSphereSphereCollisionAlgorithm, %class.btSphereSphereCollisionAlgorithm* %this1, i32 0, i32 2
  %5 = load %class.btPersistentManifold*, %class.btPersistentManifold** %mf.addr, align 4, !tbaa !2
  store %class.btPersistentManifold* %5, %class.btPersistentManifold** %m_manifoldPtr, align 4, !tbaa !11
  %m_manifoldPtr2 = getelementptr inbounds %class.btSphereSphereCollisionAlgorithm, %class.btSphereSphereCollisionAlgorithm* %this1, i32 0, i32 2
  %6 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr2, align 4, !tbaa !11
  %tobool = icmp ne %class.btPersistentManifold* %6, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %7 = bitcast %class.btSphereSphereCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %m_dispatcher = getelementptr inbounds %class.btCollisionAlgorithm, %class.btCollisionAlgorithm* %7, i32 0, i32 1
  %8 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4, !tbaa !12
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col0Wrap.addr, align 4, !tbaa !2
  %call3 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %9)
  %10 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col1Wrap.addr, align 4, !tbaa !2
  %call4 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %10)
  %11 = bitcast %class.btDispatcher* %8 to %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)***
  %vtable = load %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)**, %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*** %11, align 4, !tbaa !6
  %vfn = getelementptr inbounds %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*, %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)** %vtable, i64 3
  %12 = load %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*, %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)** %vfn, align 4
  %call5 = call %class.btPersistentManifold* %12(%class.btDispatcher* %8, %class.btCollisionObject* %call3, %class.btCollisionObject* %call4)
  %m_manifoldPtr6 = getelementptr inbounds %class.btSphereSphereCollisionAlgorithm, %class.btSphereSphereCollisionAlgorithm* %this1, i32 0, i32 2
  store %class.btPersistentManifold* %call5, %class.btPersistentManifold** %m_manifoldPtr6, align 4, !tbaa !11
  %m_ownManifold7 = getelementptr inbounds %class.btSphereSphereCollisionAlgorithm, %class.btSphereSphereCollisionAlgorithm* %this1, i32 0, i32 1
  store i8 1, i8* %m_ownManifold7, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %13 = load %class.btSphereSphereCollisionAlgorithm*, %class.btSphereSphereCollisionAlgorithm** %retval, align 4
  ret %class.btSphereSphereCollisionAlgorithm* %13
}

declare %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%class.btActivatingCollisionAlgorithm* returned, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8), %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*) unnamed_addr #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_collisionObject = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_collisionObject, align 4, !tbaa !14
  ret %class.btCollisionObject* %0
}

; Function Attrs: nounwind
define hidden %class.btSphereSphereCollisionAlgorithm* @_ZN32btSphereSphereCollisionAlgorithmD2Ev(%class.btSphereSphereCollisionAlgorithm* returned %this) unnamed_addr #3 {
entry:
  %retval = alloca %class.btSphereSphereCollisionAlgorithm*, align 4
  %this.addr = alloca %class.btSphereSphereCollisionAlgorithm*, align 4
  store %class.btSphereSphereCollisionAlgorithm* %this, %class.btSphereSphereCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSphereSphereCollisionAlgorithm*, %class.btSphereSphereCollisionAlgorithm** %this.addr, align 4
  store %class.btSphereSphereCollisionAlgorithm* %this1, %class.btSphereSphereCollisionAlgorithm** %retval, align 4
  %0 = bitcast %class.btSphereSphereCollisionAlgorithm* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV32btSphereSphereCollisionAlgorithm, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  %m_ownManifold = getelementptr inbounds %class.btSphereSphereCollisionAlgorithm, %class.btSphereSphereCollisionAlgorithm* %this1, i32 0, i32 1
  %1 = load i8, i8* %m_ownManifold, align 4, !tbaa !8, !range !17
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then, label %if.end5

if.then:                                          ; preds = %entry
  %m_manifoldPtr = getelementptr inbounds %class.btSphereSphereCollisionAlgorithm, %class.btSphereSphereCollisionAlgorithm* %this1, i32 0, i32 2
  %2 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr, align 4, !tbaa !11
  %tobool2 = icmp ne %class.btPersistentManifold* %2, null
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %3 = bitcast %class.btSphereSphereCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %m_dispatcher = getelementptr inbounds %class.btCollisionAlgorithm, %class.btCollisionAlgorithm* %3, i32 0, i32 1
  %4 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4, !tbaa !12
  %m_manifoldPtr4 = getelementptr inbounds %class.btSphereSphereCollisionAlgorithm, %class.btSphereSphereCollisionAlgorithm* %this1, i32 0, i32 2
  %5 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr4, align 4, !tbaa !11
  %6 = bitcast %class.btDispatcher* %4 to void (%class.btDispatcher*, %class.btPersistentManifold*)***
  %vtable = load void (%class.btDispatcher*, %class.btPersistentManifold*)**, void (%class.btDispatcher*, %class.btPersistentManifold*)*** %6, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btDispatcher*, %class.btPersistentManifold*)*, void (%class.btDispatcher*, %class.btPersistentManifold*)** %vtable, i64 4
  %7 = load void (%class.btDispatcher*, %class.btPersistentManifold*)*, void (%class.btDispatcher*, %class.btPersistentManifold*)** %vfn, align 4
  call void %7(%class.btDispatcher* %4, %class.btPersistentManifold* %5)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  br label %if.end5

if.end5:                                          ; preds = %if.end, %entry
  %8 = bitcast %class.btSphereSphereCollisionAlgorithm* %this1 to %class.btActivatingCollisionAlgorithm*
  %call = call %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmD2Ev(%class.btActivatingCollisionAlgorithm* %8) #9
  %9 = load %class.btSphereSphereCollisionAlgorithm*, %class.btSphereSphereCollisionAlgorithm** %retval, align 4
  ret %class.btSphereSphereCollisionAlgorithm* %9
}

; Function Attrs: nounwind
declare %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmD2Ev(%class.btActivatingCollisionAlgorithm* returned) unnamed_addr #4

; Function Attrs: nounwind
define hidden void @_ZN32btSphereSphereCollisionAlgorithmD0Ev(%class.btSphereSphereCollisionAlgorithm* %this) unnamed_addr #3 {
entry:
  %this.addr = alloca %class.btSphereSphereCollisionAlgorithm*, align 4
  store %class.btSphereSphereCollisionAlgorithm* %this, %class.btSphereSphereCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSphereSphereCollisionAlgorithm*, %class.btSphereSphereCollisionAlgorithm** %this.addr, align 4
  %call = call %class.btSphereSphereCollisionAlgorithm* @_ZN32btSphereSphereCollisionAlgorithmD1Ev(%class.btSphereSphereCollisionAlgorithm* %this1) #9
  %0 = bitcast %class.btSphereSphereCollisionAlgorithm* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #5

define hidden void @_ZN32btSphereSphereCollisionAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResult(%class.btSphereSphereCollisionAlgorithm* %this, %struct.btCollisionObjectWrapper* %col0Wrap, %struct.btCollisionObjectWrapper* %col1Wrap, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo, %class.btManifoldResult* %resultOut) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSphereSphereCollisionAlgorithm*, align 4
  %col0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %col1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %resultOut.addr = alloca %class.btManifoldResult*, align 4
  %sphere0 = alloca %class.btSphereShape*, align 4
  %sphere1 = alloca %class.btSphereShape*, align 4
  %diff = alloca %class.btVector3, align 4
  %len = alloca float, align 4
  %radius0 = alloca float, align 4
  %radius1 = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %dist = alloca float, align 4
  %normalOnSurfaceB = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp19 = alloca %class.btVector3, align 4
  %pos1 = alloca %class.btVector3, align 4
  %ref.tmp23 = alloca %class.btVector3, align 4
  store %class.btSphereSphereCollisionAlgorithm* %this, %class.btSphereSphereCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %col0Wrap, %struct.btCollisionObjectWrapper** %col0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %col1Wrap, %struct.btCollisionObjectWrapper** %col1Wrap.addr, align 4, !tbaa !2
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  store %class.btManifoldResult* %resultOut, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  %this1 = load %class.btSphereSphereCollisionAlgorithm*, %class.btSphereSphereCollisionAlgorithm** %this.addr, align 4
  %0 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  %m_manifoldPtr = getelementptr inbounds %class.btSphereSphereCollisionAlgorithm, %class.btSphereSphereCollisionAlgorithm* %this1, i32 0, i32 2
  %1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr, align 4, !tbaa !11
  %tobool = icmp ne %class.btPersistentManifold* %1, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %cleanup.cont

if.end:                                           ; preds = %entry
  %2 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  %m_manifoldPtr2 = getelementptr inbounds %class.btSphereSphereCollisionAlgorithm, %class.btSphereSphereCollisionAlgorithm* %this1, i32 0, i32 2
  %3 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr2, align 4, !tbaa !11
  call void @_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold(%class.btManifoldResult* %2, %class.btPersistentManifold* %3)
  %4 = bitcast %class.btSphereShape** %sphere0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col0Wrap.addr, align 4, !tbaa !2
  %call = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %5)
  %6 = bitcast %class.btCollisionShape* %call to %class.btSphereShape*
  store %class.btSphereShape* %6, %class.btSphereShape** %sphere0, align 4, !tbaa !2
  %7 = bitcast %class.btSphereShape** %sphere1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col1Wrap.addr, align 4, !tbaa !2
  %call3 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %8)
  %9 = bitcast %class.btCollisionShape* %call3 to %class.btSphereShape*
  store %class.btSphereShape* %9, %class.btSphereShape** %sphere1, align 4, !tbaa !2
  %10 = bitcast %class.btVector3* %diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #9
  %11 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col0Wrap.addr, align 4, !tbaa !2
  %call4 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %11)
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %call4)
  %12 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col1Wrap.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %12)
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %call6)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %diff, %class.btVector3* nonnull align 4 dereferenceable(16) %call5, %class.btVector3* nonnull align 4 dereferenceable(16) %call7)
  %13 = bitcast float* %len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %call8 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %diff)
  store float %call8, float* %len, align 4, !tbaa !18
  %14 = bitcast float* %radius0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #9
  %15 = load %class.btSphereShape*, %class.btSphereShape** %sphere0, align 4, !tbaa !2
  %call9 = call float @_ZNK13btSphereShape9getRadiusEv(%class.btSphereShape* %15)
  store float %call9, float* %radius0, align 4, !tbaa !18
  %16 = bitcast float* %radius1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #9
  %17 = load %class.btSphereShape*, %class.btSphereShape** %sphere1, align 4, !tbaa !2
  %call10 = call float @_ZNK13btSphereShape9getRadiusEv(%class.btSphereShape* %17)
  store float %call10, float* %radius1, align 4, !tbaa !18
  %18 = load float, float* %len, align 4, !tbaa !18
  %19 = load float, float* %radius0, align 4, !tbaa !18
  %20 = load float, float* %radius1, align 4, !tbaa !18
  %add = fadd float %19, %20
  %cmp = fcmp ogt float %18, %add
  br i1 %cmp, label %if.then11, label %if.end12

if.then11:                                        ; preds = %if.end
  %21 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  call void @_ZN16btManifoldResult20refreshContactPointsEv(%class.btManifoldResult* %21)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end12:                                         ; preds = %if.end
  %22 = bitcast float* %dist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #9
  %23 = load float, float* %len, align 4, !tbaa !18
  %24 = load float, float* %radius0, align 4, !tbaa !18
  %25 = load float, float* %radius1, align 4, !tbaa !18
  %add13 = fadd float %24, %25
  %sub = fsub float %23, %add13
  store float %sub, float* %dist, align 4, !tbaa !18
  %26 = bitcast %class.btVector3* %normalOnSurfaceB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %26) #9
  %27 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #9
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !18
  %28 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #9
  store float 0.000000e+00, float* %ref.tmp14, align 4, !tbaa !18
  %29 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #9
  store float 0.000000e+00, float* %ref.tmp15, align 4, !tbaa !18
  %call16 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %normalOnSurfaceB, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp15)
  %30 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #9
  %31 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #9
  %32 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #9
  %33 = load float, float* %len, align 4, !tbaa !18
  %cmp17 = fcmp ogt float %33, 0x3E80000000000000
  br i1 %cmp17, label %if.then18, label %if.end20

if.then18:                                        ; preds = %if.end12
  %34 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %34) #9
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp19, %class.btVector3* nonnull align 4 dereferenceable(16) %diff, float* nonnull align 4 dereferenceable(4) %len)
  %35 = bitcast %class.btVector3* %normalOnSurfaceB to i8*
  %36 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %35, i8* align 4 %36, i32 16, i1 false), !tbaa.struct !20
  %37 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %37) #9
  br label %if.end20

if.end20:                                         ; preds = %if.then18, %if.end12
  %38 = bitcast %class.btVector3* %pos1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %38) #9
  %39 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col1Wrap.addr, align 4, !tbaa !2
  %call21 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %39)
  %call22 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %call21)
  %40 = bitcast %class.btVector3* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %40) #9
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp23, float* nonnull align 4 dereferenceable(4) %radius1, %class.btVector3* nonnull align 4 dereferenceable(16) %normalOnSurfaceB)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %call22, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp23)
  %41 = bitcast %class.btVector3* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %41) #9
  %42 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  %43 = load float, float* %dist, align 4, !tbaa !18
  %44 = bitcast %class.btManifoldResult* %42 to void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)***
  %vtable = load void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)**, void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)*** %44, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)** %vtable, i64 4
  %45 = load void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)** %vfn, align 4
  call void %45(%class.btManifoldResult* %42, %class.btVector3* nonnull align 4 dereferenceable(16) %normalOnSurfaceB, %class.btVector3* nonnull align 4 dereferenceable(16) %pos1, float %43)
  %46 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  call void @_ZN16btManifoldResult20refreshContactPointsEv(%class.btManifoldResult* %46)
  %47 = bitcast %class.btVector3* %pos1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %47) #9
  %48 = bitcast %class.btVector3* %normalOnSurfaceB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %48) #9
  %49 = bitcast float* %dist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #9
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end20, %if.then11
  %50 = bitcast float* %radius1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #9
  %51 = bitcast float* %radius0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #9
  %52 = bitcast float* %len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #9
  %53 = bitcast %class.btVector3* %diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %53) #9
  %54 = bitcast %class.btSphereShape** %sphere1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #9
  %55 = bitcast %class.btSphereShape** %sphere0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #9
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %if.then, %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold(%class.btManifoldResult* %this, %class.btPersistentManifold* %manifoldPtr) #3 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %manifoldPtr.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4, !tbaa !2
  store %class.btPersistentManifold* %manifoldPtr, %class.btPersistentManifold** %manifoldPtr.addr, align 4, !tbaa !2
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifoldPtr.addr, align 4, !tbaa !2
  %m_manifoldPtr = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  store %class.btPersistentManifold* %0, %class.btPersistentManifold** %m_manifoldPtr, align 4, !tbaa !22
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #6

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_shape = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 1
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %m_shape, align 4, !tbaa !24
  ret %class.btCollisionShape* %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #7 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !18
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !18
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !18
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !18
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !18
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !18
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !18
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !18
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !18
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 3
  %0 = load %class.btTransform*, %class.btTransform** %m_worldTransform, align 4, !tbaa !25
  ret %class.btTransform* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

define linkonce_odr hidden float @_ZNK13btSphereShape9getRadiusEv(%class.btSphereShape* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btSphereShape*, align 4
  store %class.btSphereShape* %this, %class.btSphereShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSphereShape*, %class.btSphereShape** %this.addr, align 4
  %0 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %0, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %m_implicitShapeDimensions)
  %1 = load float, float* %call, align 4, !tbaa !18
  %2 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %2, i32 0, i32 1
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %m_localScaling)
  %3 = load float, float* %call2, align 4, !tbaa !18
  %mul = fmul float %1, %3
  ret float %mul
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN16btManifoldResult20refreshContactPointsEv(%class.btManifoldResult* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %isSwapped = alloca i8, align 1
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %m_manifoldPtr = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr, align 4, !tbaa !22
  %call = call i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %isSwapped) #9
  %m_manifoldPtr2 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr2, align 4, !tbaa !22
  %call3 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %1)
  %m_body0Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap, align 4, !tbaa !26
  %call4 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %2)
  %cmp = icmp ne %class.btCollisionObject* %call3, %call4
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %isSwapped, align 1, !tbaa !27
  %3 = load i8, i8* %isSwapped, align 1, !tbaa !27, !range !17
  %tobool5 = trunc i8 %3 to i1
  br i1 %tobool5, label %if.then6, label %if.else

if.then6:                                         ; preds = %if.end
  %m_manifoldPtr7 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %4 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr7, align 4, !tbaa !22
  %m_body1Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  %5 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body1Wrap, align 4, !tbaa !28
  %call8 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %5)
  %call9 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call8)
  %m_body0Wrap10 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %6 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap10, align 4, !tbaa !26
  %call11 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %6)
  %call12 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call11)
  call void @_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_(%class.btPersistentManifold* %4, %class.btTransform* nonnull align 4 dereferenceable(64) %call9, %class.btTransform* nonnull align 4 dereferenceable(64) %call12)
  br label %if.end20

if.else:                                          ; preds = %if.end
  %m_manifoldPtr13 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %7 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr13, align 4, !tbaa !22
  %m_body0Wrap14 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap14, align 4, !tbaa !26
  %call15 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %8)
  %call16 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call15)
  %m_body1Wrap17 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body1Wrap17, align 4, !tbaa !28
  %call18 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %9)
  %call19 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call18)
  call void @_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_(%class.btPersistentManifold* %7, %class.btTransform* nonnull align 4 dereferenceable(64) %call16, %class.btTransform* nonnull align 4 dereferenceable(64) %call19)
  br label %if.end20

if.end20:                                         ; preds = %if.else, %if.then6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %isSwapped) #9
  br label %return

return:                                           ; preds = %if.end20, %if.then
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !18
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !18
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !18
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !18
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !18
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !18
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !18
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #6

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZdvRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #7 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load float*, float** %s.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !18
  %div = fdiv float 1.000000e+00, %3
  store float %div, float* %ref.tmp, align 4, !tbaa !18
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %4 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #6

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #7 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !18
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !18
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !18
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !18
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !18
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !18
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !18
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !18
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !18
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #7 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: nounwind
define hidden float @_ZN32btSphereSphereCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult(%class.btSphereSphereCollisionAlgorithm* %this, %class.btCollisionObject* %col0, %class.btCollisionObject* %col1, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo, %class.btManifoldResult* %resultOut) unnamed_addr #3 {
entry:
  %this.addr = alloca %class.btSphereSphereCollisionAlgorithm*, align 4
  %col0.addr = alloca %class.btCollisionObject*, align 4
  %col1.addr = alloca %class.btCollisionObject*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %resultOut.addr = alloca %class.btManifoldResult*, align 4
  store %class.btSphereSphereCollisionAlgorithm* %this, %class.btSphereSphereCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %col0, %class.btCollisionObject** %col0.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %col1, %class.btCollisionObject** %col1.addr, align 4, !tbaa !2
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  store %class.btManifoldResult* %resultOut, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  %this1 = load %class.btSphereSphereCollisionAlgorithm*, %class.btSphereSphereCollisionAlgorithm** %this.addr, align 4
  %0 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  ret float 1.000000e+00
}

define linkonce_odr hidden void @_ZN32btSphereSphereCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE(%class.btSphereSphereCollisionAlgorithm* %this, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %manifoldArray) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btSphereSphereCollisionAlgorithm*, align 4
  %manifoldArray.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btSphereSphereCollisionAlgorithm* %this, %class.btSphereSphereCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray* %manifoldArray, %class.btAlignedObjectArray** %manifoldArray.addr, align 4, !tbaa !2
  %this1 = load %class.btSphereSphereCollisionAlgorithm*, %class.btSphereSphereCollisionAlgorithm** %this.addr, align 4
  %m_manifoldPtr = getelementptr inbounds %class.btSphereSphereCollisionAlgorithm, %class.btSphereSphereCollisionAlgorithm* %this1, i32 0, i32 2
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr, align 4, !tbaa !11
  %tobool = icmp ne %class.btPersistentManifold* %0, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %m_ownManifold = getelementptr inbounds %class.btSphereSphereCollisionAlgorithm, %class.btSphereSphereCollisionAlgorithm* %this1, i32 0, i32 1
  %1 = load i8, i8* %m_ownManifold, align 4, !tbaa !8, !range !17
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %2 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %manifoldArray.addr, align 4, !tbaa !2
  %m_manifoldPtr3 = getelementptr inbounds %class.btSphereSphereCollisionAlgorithm, %class.btSphereSphereCollisionAlgorithm* %this1, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_(%class.btAlignedObjectArray* %2, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %m_manifoldPtr3)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #2 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !18
  %0 = load float, float* %y.addr, align 4, !tbaa !18
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #8

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !18
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !18
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !18
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !18
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !18
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !18
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_cachedPoints = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 4
  %0 = load i32, i32* %m_cachedPoints, align 4, !tbaa !29
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_body0 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body0, align 4, !tbaa !31
  ret %class.btCollisionObject* %0
}

declare void @_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_(%class.btPersistentManifold*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btTransform* nonnull align 4 dereferenceable(64)) #1

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #7 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !18
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !18
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !18
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !18
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !18
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !18
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !18
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !18
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !18
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_(%class.btAlignedObjectArray* %this, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %_Val) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %class.btPersistentManifold**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store %class.btPersistentManifold** %_Val, %class.btPersistentManifold*** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !32
  %1 = load i32, i32* %sz, align 4, !tbaa !32
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !33
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !36
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %2, i32 %3
  %4 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %5 = bitcast i8* %4 to %class.btPersistentManifold**
  %6 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %_Val.addr, align 4, !tbaa !2
  %7 = load %class.btPersistentManifold*, %class.btPersistentManifold** %6, align 4, !tbaa !2
  store %class.btPersistentManifold* %7, %class.btPersistentManifold** %5, align 4, !tbaa !2
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size5, align 4, !tbaa !36
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size5, align 4, !tbaa !36
  %9 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !36
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !37
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !32
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !32
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btPersistentManifold*** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !32
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btPersistentManifold**
  store %class.btPersistentManifold** %3, %class.btPersistentManifold*** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray* %this1)
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %class.btPersistentManifold** %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !38
  %5 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btPersistentManifold** %5, %class.btPersistentManifold*** %m_data, align 4, !tbaa !33
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !32
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !37
  %7 = bitcast %class.btPersistentManifold*** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !32
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !32
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !32
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #7 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !32
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !32
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !32
  %call = call %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator* %m_allocator, i32 %1, %class.btPersistentManifold*** null)
  %2 = bitcast %class.btPersistentManifold** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %class.btPersistentManifold** %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btPersistentManifold**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !32
  store i32 %end, i32* %end.addr, align 4, !tbaa !32
  store %class.btPersistentManifold** %dest, %class.btPersistentManifold*** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %start.addr, align 4, !tbaa !32
  store i32 %1, i32* %i, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !32
  %3 = load i32, i32* %end.addr, align 4, !tbaa !32
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !32
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %4, i32 %5
  %6 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %7 = bitcast i8* %6 to %class.btPersistentManifold**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !33
  %9 = load i32, i32* %i, align 4, !tbaa !32
  %arrayidx2 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %8, i32 %9
  %10 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx2, align 4, !tbaa !2
  store %class.btPersistentManifold* %10, %class.btPersistentManifold** %7, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !32
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !32
  store i32 %last, i32* %last.addr, align 4, !tbaa !32
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %first.addr, align 4, !tbaa !32
  store i32 %1, i32* %i, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !32
  %3 = load i32, i32* %last.addr, align 4, !tbaa !32
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !33
  %5 = load i32, i32* %i, align 4, !tbaa !32
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !32
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !33
  %tobool = icmp ne %class.btPersistentManifold** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !38, !range !17
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data4, align 4, !tbaa !33
  call void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator* %m_allocator, %class.btPersistentManifold** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data5, align 4, !tbaa !33
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator* %this, i32 %n, %class.btPersistentManifold*** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btPersistentManifold***, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !32
  store %class.btPersistentManifold*** %hint, %class.btPersistentManifold**** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !32
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btPersistentManifold**
  ret %class.btPersistentManifold** %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #1

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator* %this, %class.btPersistentManifold** %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store %class.btPersistentManifold** %ptr, %class.btPersistentManifold*** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btPersistentManifold** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { argmemonly nounwind willreturn }
attributes #7 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind readnone speculatable willreturn }
attributes #9 = { nounwind }
attributes #10 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{!9, !10, i64 8}
!9 = !{!"_ZTS32btSphereSphereCollisionAlgorithm", !10, i64 8, !3, i64 12}
!10 = !{!"bool", !4, i64 0}
!11 = !{!9, !3, i64 12}
!12 = !{!13, !3, i64 4}
!13 = !{!"_ZTS20btCollisionAlgorithm", !3, i64 4}
!14 = !{!15, !3, i64 8}
!15 = !{!"_ZTS24btCollisionObjectWrapper", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !16, i64 16, !16, i64 20}
!16 = !{!"int", !4, i64 0}
!17 = !{i8 0, i8 2}
!18 = !{!19, !19, i64 0}
!19 = !{!"float", !4, i64 0}
!20 = !{i64 0, i64 16, !21}
!21 = !{!4, !4, i64 0}
!22 = !{!23, !3, i64 4}
!23 = !{!"_ZTS16btManifoldResult", !3, i64 4, !3, i64 8, !3, i64 12, !16, i64 16, !16, i64 20, !16, i64 24, !16, i64 28}
!24 = !{!15, !3, i64 4}
!25 = !{!15, !3, i64 12}
!26 = !{!23, !3, i64 8}
!27 = !{!10, !10, i64 0}
!28 = !{!23, !3, i64 12}
!29 = !{!30, !16, i64 748}
!30 = !{!"_ZTS20btPersistentManifold", !4, i64 4, !3, i64 740, !3, i64 744, !16, i64 748, !19, i64 752, !19, i64 756, !16, i64 760, !16, i64 764, !16, i64 768}
!31 = !{!30, !3, i64 740}
!32 = !{!16, !16, i64 0}
!33 = !{!34, !3, i64 12}
!34 = !{!"_ZTS20btAlignedObjectArrayIP20btPersistentManifoldE", !35, i64 0, !16, i64 4, !16, i64 8, !3, i64 12, !10, i64 16}
!35 = !{!"_ZTS18btAlignedAllocatorIP20btPersistentManifoldLj16EE"}
!36 = !{!34, !16, i64 4}
!37 = !{!34, !16, i64 8}
!38 = !{!34, !10, i64 16}
