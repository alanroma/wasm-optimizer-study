; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btTriangleCallback.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btTriangleCallback.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btTriangleCallback = type { i32 (...)** }
%class.btInternalTriangleIndexCallback = type { i32 (...)** }

@_ZTV18btTriangleCallback = hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI18btTriangleCallback to i8*), i8* bitcast (%class.btTriangleCallback* (%class.btTriangleCallback*)* @_ZN18btTriangleCallbackD1Ev to i8*), i8* bitcast (void (%class.btTriangleCallback*)* @_ZN18btTriangleCallbackD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, align 4
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS18btTriangleCallback = hidden constant [21 x i8] c"18btTriangleCallback\00", align 1
@_ZTI18btTriangleCallback = hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([21 x i8], [21 x i8]* @_ZTS18btTriangleCallback, i32 0, i32 0) }, align 4
@_ZTV31btInternalTriangleIndexCallback = hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI31btInternalTriangleIndexCallback to i8*), i8* bitcast (%class.btInternalTriangleIndexCallback* (%class.btInternalTriangleIndexCallback*)* @_ZN31btInternalTriangleIndexCallbackD1Ev to i8*), i8* bitcast (void (%class.btInternalTriangleIndexCallback*)* @_ZN31btInternalTriangleIndexCallbackD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, align 4
@_ZTS31btInternalTriangleIndexCallback = hidden constant [34 x i8] c"31btInternalTriangleIndexCallback\00", align 1
@_ZTI31btInternalTriangleIndexCallback = hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([34 x i8], [34 x i8]* @_ZTS31btInternalTriangleIndexCallback, i32 0, i32 0) }, align 4

@_ZN18btTriangleCallbackD1Ev = hidden unnamed_addr alias %class.btTriangleCallback* (%class.btTriangleCallback*), %class.btTriangleCallback* (%class.btTriangleCallback*)* @_ZN18btTriangleCallbackD2Ev
@_ZN31btInternalTriangleIndexCallbackD1Ev = hidden unnamed_addr alias %class.btInternalTriangleIndexCallback* (%class.btInternalTriangleIndexCallback*), %class.btInternalTriangleIndexCallback* (%class.btInternalTriangleIndexCallback*)* @_ZN31btInternalTriangleIndexCallbackD2Ev

; Function Attrs: nounwind
define hidden %class.btTriangleCallback* @_ZN18btTriangleCallbackD2Ev(%class.btTriangleCallback* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btTriangleCallback*, align 4
  store %class.btTriangleCallback* %this, %class.btTriangleCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleCallback*, %class.btTriangleCallback** %this.addr, align 4
  ret %class.btTriangleCallback* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN18btTriangleCallbackD0Ev(%class.btTriangleCallback* %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btTriangleCallback*, align 4
  store %class.btTriangleCallback* %this, %class.btTriangleCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleCallback*, %class.btTriangleCallback** %this.addr, align 4
  call void @llvm.trap() #2
  unreachable
}

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #1

; Function Attrs: nounwind
define hidden %class.btInternalTriangleIndexCallback* @_ZN31btInternalTriangleIndexCallbackD2Ev(%class.btInternalTriangleIndexCallback* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btInternalTriangleIndexCallback*, align 4
  store %class.btInternalTriangleIndexCallback* %this, %class.btInternalTriangleIndexCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btInternalTriangleIndexCallback*, %class.btInternalTriangleIndexCallback** %this.addr, align 4
  ret %class.btInternalTriangleIndexCallback* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN31btInternalTriangleIndexCallbackD0Ev(%class.btInternalTriangleIndexCallback* %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btInternalTriangleIndexCallback*, align 4
  store %class.btInternalTriangleIndexCallback* %this, %class.btInternalTriangleIndexCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btInternalTriangleIndexCallback*, %class.btInternalTriangleIndexCallback** %this.addr, align 4
  call void @llvm.trap() #2
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { cold noreturn nounwind }
attributes #2 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
