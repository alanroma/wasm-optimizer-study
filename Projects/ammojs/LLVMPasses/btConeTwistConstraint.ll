; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/ConstraintSolver/btConeTwistConstraint.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/ConstraintSolver/btConeTwistConstraint.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btVector3 = type { [4 x float] }
%class.btConeTwistConstraint = type { %class.btTypedConstraint, [3 x %class.btJacobianEntry], %class.btTransform, %class.btTransform, float, float, float, float, float, float, float, float, %class.btVector3, %class.btVector3, float, float, float, float, float, float, float, float, i8, i8, i8, i8, float, float, %class.btVector3, i8, i8, %class.btQuaternion, float, %class.btVector3, i32, float, float, float }
%class.btTypedConstraint = type { i32 (...)**, %struct.btTypedObject, i32, %union.anon, float, i8, i8, i32, %class.btRigidBody*, %class.btRigidBody*, float, float, %struct.btJointFeedback* }
%struct.btTypedObject = type { i32 }
%union.anon = type { i32 }
%struct.btJointFeedback = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%class.btJacobianEntry = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon.0, float, float, float, i32, i32 }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%union.anon.0 = type { i8* }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%"struct.btTypedConstraint::btConstraintInfo1" = type { i32, i32 }
%"struct.btTypedConstraint::btConstraintInfo2" = type { float, float, float*, float*, float*, float*, i32, float*, float*, float*, float*, i32*, i32, float }
%struct.btSolverBody = type { %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btRigidBody* }
%class.btAlignedObjectArray.1 = type opaque
%class.btSerializer = type opaque
%struct.btConeTwistConstraintData = type { %struct.btTypedConstraintData, %struct.btTransformFloatData, %struct.btTransformFloatData, float, float, float, float, float, float, float, [4 x i8] }
%struct.btTypedConstraintData = type { %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData*, i8*, i32, i32, i32, i32, float, float, i32, i32, float, i32 }
%struct.btRigidBodyFloatData = type { %struct.btCollisionObjectFloatData, %struct.btMatrix3x3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, float, float, float, float, float, float, float, float, i32 }
%struct.btCollisionObjectFloatData = type { i8*, i8*, %struct.btCollisionShapeData*, i8*, %struct.btTransformFloatData, %struct.btTransformFloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, float, float, float, float, float, float, float, i32, i32, i32, i32, i32, i32, i32, [4 x i8] }
%struct.btCollisionShapeData = type opaque
%struct.btMatrix3x3FloatData = type { [3 x %struct.btVector3FloatData] }
%struct.btVector3FloatData = type { [4 x float] }
%struct.btTransformFloatData = type { %struct.btMatrix3x3FloatData, %struct.btVector3FloatData }

$_ZN15btJacobianEntryC2Ev = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN12btQuaternionC2Ev = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZN11btTransform9setOriginERK9btVector3 = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN21btConeTwistConstraint8setLimitEffffff = comdat any

$_ZNK11btRigidBody24getCenterOfMassTransformEv = comdat any

$_ZNK11btRigidBody24getInvInertiaTensorWorldEv = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZN11btTransform9getOriginEv = comdat any

$_ZngRK9btVector3 = comdat any

$_ZNK9btVector322getSkewSymmetricMatrixEPS_S0_S0_ = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZNK11btTransformmlERKS_ = comdat any

$_ZN11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x39getColumnEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK11btTransformmlERK9btVector3 = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZNK9btVector310normalizedEv = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_ = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_ZNK11btRigidBody23getCenterOfMassPositionEv = comdat any

$_ZNK11btRigidBody22getInvInertiaDiagLocalEv = comdat any

$_ZNK11btRigidBody10getInvMassEv = comdat any

$_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f = comdat any

$_ZNK12btSolverBody39internalGetVelocityInLocalPointObsoleteERK9btVector3RS0_ = comdat any

$_ZNK15btJacobianEntry11getDiagonalEv = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f = comdat any

$_ZNK12btSolverBody26internalGetAngularVelocityER9btVector3 = comdat any

$_ZN11btTransform11setIdentityEv = comdat any

$_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_ = comdat any

$_ZN11btTransformC2ERK12btQuaternionRK9btVector3 = comdat any

$_ZNK11btTransform7inverseEv = comdat any

$_ZN15btTransformUtil17calculateVelocityERK11btTransformS2_fR9btVector3S4_ = comdat any

$_ZNK21btConeTwistConstraint13getRigidBodyAEv = comdat any

$_ZNK11btRigidBody32computeAngularImpulseDenominatorERK9btVector3 = comdat any

$_ZNK21btConeTwistConstraint13getRigidBodyBEv = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZdvRK9btVector3RKf = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_Z5btMaxIfERKT_S2_S2_ = comdat any

$_Z11btAtan2Fastff = comdat any

$_Z6btFabsf = comdat any

$_Z15shortestArcQuatRK9btVector3S1_ = comdat any

$_Z10quatRotateRK12btQuaternionRK9btVector3 = comdat any

$_ZNK11btTransform11getRotationEv = comdat any

$_ZNK10btQuadWord1xEv = comdat any

$_ZNK10btQuadWord1yEv = comdat any

$_ZNK10btQuadWord1zEv = comdat any

$_Z11btFuzzyZerof = comdat any

$_ZNK12btQuaternion8getAngleEv = comdat any

$_ZmlRK12btQuaternionS1_ = comdat any

$_ZNK12btQuaternion7inverseEv = comdat any

$_ZN12btQuaternion9normalizeEv = comdat any

$_Z32computeAngularImpulseDenominatorRK9btVector3RK11btMatrix3x3 = comdat any

$_Z7btAtan2ff = comdat any

$_Z5btCosf = comdat any

$_Z5btSinf = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_Z4fabsf = comdat any

$_Z4sqrtf = comdat any

$_ZN12btQuaternionC2ERK9btVector3RKf = comdat any

$_ZNK12btQuaternionngEv = comdat any

$_ZN9btVector34setZEf = comdat any

$_ZN9btVector34setYEf = comdat any

$_ZN17btTypedConstraintD2Ev = comdat any

$_ZN21btConeTwistConstraintD0Ev = comdat any

$_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif = comdat any

$_ZNK21btConeTwistConstraint28calculateSerializeBufferSizeEv = comdat any

$_ZNK21btConeTwistConstraint9serializeEPvP12btSerializer = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZN10btQuadWordC2Ev = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZN11btMatrix3x311setIdentityEv = comdat any

$_ZN12btQuaternionC2ERKfS1_S1_S1_ = comdat any

$_ZN11btTransform11setRotationERK12btQuaternion = comdat any

$_ZN10btQuadWordC2ERKfS1_S1_S1_ = comdat any

$_ZN11btMatrix3x311setRotationERK12btQuaternion = comdat any

$_ZNK12btQuaternion7length2Ev = comdat any

$_ZNK10btQuadWord1wEv = comdat any

$_ZNK12btQuaternion3dotERKS_ = comdat any

$_ZN11btMatrix3x3C2ERK12btQuaternion = comdat any

$_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf = comdat any

$_ZNK11btMatrix3x37inverseEv = comdat any

$_ZNK11btMatrix3x311getRotationER12btQuaternion = comdat any

$_Z6btSqrtf = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZNK11btMatrix3x35cofacEiiii = comdat any

$_ZN10btQuadWord8setValueERKfS1_S1_S1_ = comdat any

$_ZmlRK9btVector3RK11btMatrix3x3 = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZmlRK12btQuaternionRK9btVector3 = comdat any

$_ZN12btQuaternionmLERKS_ = comdat any

$_ZNK10btQuadWord4getXEv = comdat any

$_ZNK10btQuadWord4getYEv = comdat any

$_ZNK10btQuadWord4getZEv = comdat any

$_Z6btAcosf = comdat any

$_ZNK12btQuaternion6lengthEv = comdat any

$_ZN12btQuaterniondVERKf = comdat any

$_ZN12btQuaternionmLERKf = comdat any

$_ZN12btQuaternion11setRotationERK9btVector3RKf = comdat any

$_ZN21btConeTwistConstraintdlEPv = comdat any

$_ZNK11btTransform9serializeER20btTransformFloatData = comdat any

$_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData = comdat any

$_ZNK9btVector39serializeER18btVector3FloatData = comdat any

@_ZTV21btConeTwistConstraint = hidden unnamed_addr constant { [14 x i8*] } { [14 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI21btConeTwistConstraint to i8*), i8* bitcast (%class.btTypedConstraint* (%class.btTypedConstraint*)* @_ZN17btTypedConstraintD2Ev to i8*), i8* bitcast (void (%class.btConeTwistConstraint*)* @_ZN21btConeTwistConstraintD0Ev to i8*), i8* bitcast (void (%class.btConeTwistConstraint*)* @_ZN21btConeTwistConstraint13buildJacobianEv to i8*), i8* bitcast (void (%class.btTypedConstraint*, %class.btAlignedObjectArray.1*, i32, i32, float)* @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif to i8*), i8* bitcast (void (%class.btConeTwistConstraint*, %"struct.btTypedConstraint::btConstraintInfo1"*)* @_ZN21btConeTwistConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E to i8*), i8* bitcast (void (%class.btConeTwistConstraint*, %"struct.btTypedConstraint::btConstraintInfo2"*)* @_ZN21btConeTwistConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E to i8*), i8* bitcast (void (%class.btConeTwistConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)* @_ZN21btConeTwistConstraint23solveConstraintObsoleteER12btSolverBodyS1_f to i8*), i8* bitcast (void (%class.btConeTwistConstraint*, i32, float, i32)* @_ZN21btConeTwistConstraint8setParamEifi to i8*), i8* bitcast (float (%class.btConeTwistConstraint*, i32, i32)* @_ZNK21btConeTwistConstraint8getParamEii to i8*), i8* bitcast (i32 (%class.btConeTwistConstraint*)* @_ZNK21btConeTwistConstraint28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btConeTwistConstraint*, i8*, %class.btSerializer*)* @_ZNK21btConeTwistConstraint9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btConeTwistConstraint*, %class.btTransform*, %class.btTransform*)* @_ZN21btConeTwistConstraint9setFramesERK11btTransformS2_ to i8*)] }, align 4
@_ZZN21btConeTwistConstraint23solveConstraintObsoleteER12btSolverBodyS1_fE9bDoTorque = internal global i8 1, align 1
@_ZL6vTwist = internal global %class.btVector3 zeroinitializer, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS21btConeTwistConstraint = hidden constant [24 x i8] c"21btConeTwistConstraint\00", align 1
@_ZTI17btTypedConstraint = external constant i8*
@_ZTI21btConeTwistConstraint = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([24 x i8], [24 x i8]* @_ZTS21btConeTwistConstraint, i32 0, i32 0), i8* bitcast (i8** @_ZTI17btTypedConstraint to i8*) }, align 4
@_ZTV17btTypedConstraint = external unnamed_addr constant { [13 x i8*] }, align 4
@.str = private unnamed_addr constant [26 x i8] c"btConeTwistConstraintData\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btConeTwistConstraint.cpp, i8* null }]

@_ZN21btConeTwistConstraintC1ER11btRigidBodyS1_RK11btTransformS4_ = hidden unnamed_addr alias %class.btConeTwistConstraint* (%class.btConeTwistConstraint*, %class.btRigidBody*, %class.btRigidBody*, %class.btTransform*, %class.btTransform*), %class.btConeTwistConstraint* (%class.btConeTwistConstraint*, %class.btRigidBody*, %class.btRigidBody*, %class.btTransform*, %class.btTransform*)* @_ZN21btConeTwistConstraintC2ER11btRigidBodyS1_RK11btTransformS4_
@_ZN21btConeTwistConstraintC1ER11btRigidBodyRK11btTransform = hidden unnamed_addr alias %class.btConeTwistConstraint* (%class.btConeTwistConstraint*, %class.btRigidBody*, %class.btTransform*), %class.btConeTwistConstraint* (%class.btConeTwistConstraint*, %class.btRigidBody*, %class.btTransform*)* @_ZN21btConeTwistConstraintC2ER11btRigidBodyRK11btTransform

define hidden %class.btConeTwistConstraint* @_ZN21btConeTwistConstraintC2ER11btRigidBodyS1_RK11btTransformS4_(%class.btConeTwistConstraint* returned %this, %class.btRigidBody* nonnull align 4 dereferenceable(616) %rbA, %class.btRigidBody* nonnull align 4 dereferenceable(616) %rbB, %class.btTransform* nonnull align 4 dereferenceable(64) %rbAFrame, %class.btTransform* nonnull align 4 dereferenceable(64) %rbBFrame) unnamed_addr #0 {
entry:
  %retval = alloca %class.btConeTwistConstraint*, align 4
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %rbA.addr = alloca %class.btRigidBody*, align 4
  %rbB.addr = alloca %class.btRigidBody*, align 4
  %rbAFrame.addr = alloca %class.btTransform*, align 4
  %rbBFrame.addr = alloca %class.btTransform*, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4, !tbaa !2
  store %class.btRigidBody* %rbA, %class.btRigidBody** %rbA.addr, align 4, !tbaa !2
  store %class.btRigidBody* %rbB, %class.btRigidBody** %rbB.addr, align 4, !tbaa !2
  store %class.btTransform* %rbAFrame, %class.btTransform** %rbAFrame.addr, align 4, !tbaa !2
  store %class.btTransform* %rbBFrame, %class.btTransform** %rbBFrame.addr, align 4, !tbaa !2
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  store %class.btConeTwistConstraint* %this1, %class.btConeTwistConstraint** %retval, align 4
  %0 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %1 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4, !tbaa !2
  %2 = load %class.btRigidBody*, %class.btRigidBody** %rbB.addr, align 4, !tbaa !2
  %call = call %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_(%class.btTypedConstraint* %0, i32 5, %class.btRigidBody* nonnull align 4 dereferenceable(616) %1, %class.btRigidBody* nonnull align 4 dereferenceable(616) %2)
  %3 = bitcast %class.btConeTwistConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [14 x i8*] }, { [14 x i8*] }* @_ZTV21btConeTwistConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %3, align 4, !tbaa !6
  %m_jac = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jac, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btJacobianEntry* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btJacobianEntry* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_rbAFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  %4 = load %class.btTransform*, %class.btTransform** %rbAFrame.addr, align 4, !tbaa !2
  %call3 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_rbAFrame, %class.btTransform* nonnull align 4 dereferenceable(64) %4)
  %m_rbBFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  %5 = load %class.btTransform*, %class.btTransform** %rbBFrame.addr, align 4, !tbaa !2
  %call4 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_rbBFrame, %class.btTransform* nonnull align 4 dereferenceable(64) %5)
  %m_swingAxis = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_swingAxis)
  %m_twistAxis = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 13
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_twistAxis)
  %m_angularOnly = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 22
  store i8 0, i8* %m_angularOnly, align 4, !tbaa !8
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 25
  store i8 0, i8* %m_useSolveConstraintObsolete, align 1, !tbaa !17
  %m_twistAxisA = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 28
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_twistAxisA)
  %m_qTarget = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 31
  %call8 = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %m_qTarget)
  %m_accMotorImpulse = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 33
  %call9 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_accMotorImpulse)
  call void @_ZN21btConeTwistConstraint4initEv(%class.btConeTwistConstraint* %this1)
  %6 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %retval, align 4
  ret %class.btConeTwistConstraint* %6
}

declare %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_(%class.btTypedConstraint* returned, i32, %class.btRigidBody* nonnull align 4 dereferenceable(616), %class.btRigidBody* nonnull align 4 dereferenceable(616)) unnamed_addr #1

define linkonce_odr hidden %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btJacobianEntry*, align 4
  store %class.btJacobianEntry* %this, %class.btJacobianEntry** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btJacobianEntry*, %class.btJacobianEntry** %this.addr, align 4
  %m_linearJointAxis = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_linearJointAxis)
  %m_aJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aJ)
  %m_bJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_bJ)
  %m_0MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_0MinvJt)
  %m_1MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_1MinvJt)
  ret %class.btJacobianEntry* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !18
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* %0)
  ret %class.btQuaternion* %this1
}

define hidden void @_ZN21btConeTwistConstraint4initEv(%class.btConeTwistConstraint* %this) #0 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %m_angularOnly = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 22
  store i8 0, i8* %m_angularOnly, align 4, !tbaa !8
  %m_solveTwistLimit = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 23
  store i8 0, i8* %m_solveTwistLimit, align 1, !tbaa !20
  %m_solveSwingLimit = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 24
  store i8 0, i8* %m_solveSwingLimit, align 2, !tbaa !21
  %m_bMotorEnabled = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 29
  store i8 0, i8* %m_bMotorEnabled, align 4, !tbaa !22
  %m_maxMotorImpulse = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 32
  store float -1.000000e+00, float* %m_maxMotorImpulse, align 4, !tbaa !23
  call void @_ZN21btConeTwistConstraint8setLimitEffffff(%class.btConeTwistConstraint* %this1, float 0x43ABC16D60000000, float 0x43ABC16D60000000, float 0x43ABC16D60000000, float 1.000000e+00, float 0x3FD3333340000000, float 1.000000e+00)
  %m_damping = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 7
  store float 0x3F847AE140000000, float* %m_damping, align 4, !tbaa !24
  %m_fixThresh = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 11
  store float 0x3FA99999A0000000, float* %m_fixThresh, align 4, !tbaa !25
  %m_flags = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 34
  store i32 0, i32* %m_flags, align 4, !tbaa !26
  %m_linCFM = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 35
  store float 0.000000e+00, float* %m_linCFM, align 4, !tbaa !27
  %m_linERP = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 36
  store float 0x3FE6666660000000, float* %m_linERP, align 4, !tbaa !28
  %m_angCFM = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 37
  store float 0.000000e+00, float* %m_angCFM, align 4, !tbaa !29
  ret void
}

define hidden %class.btConeTwistConstraint* @_ZN21btConeTwistConstraintC2ER11btRigidBodyRK11btTransform(%class.btConeTwistConstraint* returned %this, %class.btRigidBody* nonnull align 4 dereferenceable(616) %rbA, %class.btTransform* nonnull align 4 dereferenceable(64) %rbAFrame) unnamed_addr #0 {
entry:
  %retval = alloca %class.btConeTwistConstraint*, align 4
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %rbA.addr = alloca %class.btRigidBody*, align 4
  %rbAFrame.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4, !tbaa !2
  store %class.btRigidBody* %rbA, %class.btRigidBody** %rbA.addr, align 4, !tbaa !2
  store %class.btTransform* %rbAFrame, %class.btTransform** %rbAFrame.addr, align 4, !tbaa !2
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  store %class.btConeTwistConstraint* %this1, %class.btConeTwistConstraint** %retval, align 4
  %0 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %1 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4, !tbaa !2
  %call = call %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBody(%class.btTypedConstraint* %0, i32 5, %class.btRigidBody* nonnull align 4 dereferenceable(616) %1)
  %2 = bitcast %class.btConeTwistConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [14 x i8*] }, { [14 x i8*] }* @_ZTV21btConeTwistConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %2, align 4, !tbaa !6
  %m_jac = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jac, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btJacobianEntry* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btJacobianEntry* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_rbAFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  %3 = load %class.btTransform*, %class.btTransform** %rbAFrame.addr, align 4, !tbaa !2
  %call3 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_rbAFrame, %class.btTransform* nonnull align 4 dereferenceable(64) %3)
  %m_rbBFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  %call4 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_rbBFrame)
  %m_swingAxis = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_swingAxis)
  %m_twistAxis = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 13
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_twistAxis)
  %m_angularOnly = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 22
  store i8 0, i8* %m_angularOnly, align 4, !tbaa !8
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 25
  store i8 0, i8* %m_useSolveConstraintObsolete, align 1, !tbaa !17
  %m_twistAxisA = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 28
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_twistAxisA)
  %m_qTarget = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 31
  %call8 = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %m_qTarget)
  %m_accMotorImpulse = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 33
  %call9 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_accMotorImpulse)
  %m_rbAFrame10 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  %m_rbBFrame11 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  %call12 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_rbBFrame11, %class.btTransform* nonnull align 4 dereferenceable(64) %m_rbAFrame10)
  %m_rbBFrame13 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #8
  %5 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  store float 0.000000e+00, float* %ref.tmp14, align 4, !tbaa !30
  %6 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  store float 0.000000e+00, float* %ref.tmp15, align 4, !tbaa !30
  %7 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  store float 0.000000e+00, float* %ref.tmp16, align 4, !tbaa !30
  %call17 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp16)
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %m_rbBFrame13, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %8 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  %9 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %11) #8
  call void @_ZN21btConeTwistConstraint4initEv(%class.btConeTwistConstraint* %this1)
  %12 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %retval, align 4
  ret %class.btConeTwistConstraint* %12
}

declare %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBody(%class.btTypedConstraint* returned, i32, %class.btRigidBody* nonnull align 4 dereferenceable(616)) unnamed_addr #1

define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !18
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %origin) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %origin.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %origin, %class.btVector3** %origin.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %origin.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_origin to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !18
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #4

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !30
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !30
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !30
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !30
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !30
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !30
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !30
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #4

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN21btConeTwistConstraint8setLimitEffffff(%class.btConeTwistConstraint* %this, float %_swingSpan1, float %_swingSpan2, float %_twistSpan, float %_softness, float %_biasFactor, float %_relaxationFactor) #5 comdat {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %_swingSpan1.addr = alloca float, align 4
  %_swingSpan2.addr = alloca float, align 4
  %_twistSpan.addr = alloca float, align 4
  %_softness.addr = alloca float, align 4
  %_biasFactor.addr = alloca float, align 4
  %_relaxationFactor.addr = alloca float, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4, !tbaa !2
  store float %_swingSpan1, float* %_swingSpan1.addr, align 4, !tbaa !30
  store float %_swingSpan2, float* %_swingSpan2.addr, align 4, !tbaa !30
  store float %_twistSpan, float* %_twistSpan.addr, align 4, !tbaa !30
  store float %_softness, float* %_softness.addr, align 4, !tbaa !30
  store float %_biasFactor, float* %_biasFactor.addr, align 4, !tbaa !30
  store float %_relaxationFactor, float* %_relaxationFactor.addr, align 4, !tbaa !30
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = load float, float* %_swingSpan1.addr, align 4, !tbaa !30
  %m_swingSpan1 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  store float %0, float* %m_swingSpan1, align 4, !tbaa !31
  %1 = load float, float* %_swingSpan2.addr, align 4, !tbaa !30
  %m_swingSpan2 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  store float %1, float* %m_swingSpan2, align 4, !tbaa !32
  %2 = load float, float* %_twistSpan.addr, align 4, !tbaa !30
  %m_twistSpan = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  store float %2, float* %m_twistSpan, align 4, !tbaa !33
  %3 = load float, float* %_softness.addr, align 4, !tbaa !30
  %m_limitSoftness = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 4
  store float %3, float* %m_limitSoftness, align 4, !tbaa !34
  %4 = load float, float* %_biasFactor.addr, align 4, !tbaa !30
  %m_biasFactor = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 5
  store float %4, float* %m_biasFactor, align 4, !tbaa !35
  %5 = load float, float* %_relaxationFactor.addr, align 4, !tbaa !30
  %m_relaxationFactor = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 6
  store float %5, float* %m_relaxationFactor, align 4, !tbaa !36
  ret void
}

define hidden void @_ZN21btConeTwistConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E(%class.btConeTwistConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo1"* %info) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo1"*, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4, !tbaa !2
  store %"struct.btTypedConstraint::btConstraintInfo1"* %info, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 25
  %0 = load i8, i8* %m_useSolveConstraintObsolete, align 1, !tbaa !17, !range !37
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %m_numConstraintRows = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %1, i32 0, i32 0
  store i32 0, i32* %m_numConstraintRows, align 4, !tbaa !38
  %2 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %nub = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %2, i32 0, i32 1
  store i32 0, i32* %nub, align 4, !tbaa !40
  br label %if.end28

if.else:                                          ; preds = %entry
  %3 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %m_numConstraintRows2 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %3, i32 0, i32 0
  store i32 3, i32* %m_numConstraintRows2, align 4, !tbaa !38
  %4 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %nub3 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %4, i32 0, i32 1
  store i32 3, i32* %nub3, align 4, !tbaa !40
  %5 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %5, i32 0, i32 8
  %6 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4, !tbaa !41
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %6)
  %7 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %7, i32 0, i32 9
  %8 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4, !tbaa !43
  %call4 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %8)
  %9 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA5 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %9, i32 0, i32 8
  %10 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA5, align 4, !tbaa !41
  %call6 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %10)
  %11 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB7 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %11, i32 0, i32 9
  %12 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB7, align 4, !tbaa !43
  %call8 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %12)
  call void @_ZN21btConeTwistConstraint14calcAngleInfo2ERK11btTransformS2_RK11btMatrix3x3S5_(%class.btConeTwistConstraint* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %call, %class.btTransform* nonnull align 4 dereferenceable(64) %call4, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call6, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call8)
  %m_solveSwingLimit = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 24
  %13 = load i8, i8* %m_solveSwingLimit, align 2, !tbaa !21, !range !37
  %tobool9 = trunc i8 %13 to i1
  br i1 %tobool9, label %if.then10, label %if.end20

if.then10:                                        ; preds = %if.else
  %14 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %m_numConstraintRows11 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %14, i32 0, i32 0
  %15 = load i32, i32* %m_numConstraintRows11, align 4, !tbaa !38
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %m_numConstraintRows11, align 4, !tbaa !38
  %16 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %nub12 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %16, i32 0, i32 1
  %17 = load i32, i32* %nub12, align 4, !tbaa !40
  %dec = add nsw i32 %17, -1
  store i32 %dec, i32* %nub12, align 4, !tbaa !40
  %m_swingSpan1 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %18 = load float, float* %m_swingSpan1, align 4, !tbaa !31
  %m_fixThresh = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 11
  %19 = load float, float* %m_fixThresh, align 4, !tbaa !25
  %cmp = fcmp olt float %18, %19
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then10
  %m_swingSpan2 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %20 = load float, float* %m_swingSpan2, align 4, !tbaa !32
  %m_fixThresh13 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 11
  %21 = load float, float* %m_fixThresh13, align 4, !tbaa !25
  %cmp14 = fcmp olt float %20, %21
  br i1 %cmp14, label %if.then15, label %if.end

if.then15:                                        ; preds = %land.lhs.true
  %22 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %m_numConstraintRows16 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %22, i32 0, i32 0
  %23 = load i32, i32* %m_numConstraintRows16, align 4, !tbaa !38
  %inc17 = add nsw i32 %23, 1
  store i32 %inc17, i32* %m_numConstraintRows16, align 4, !tbaa !38
  %24 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %nub18 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %24, i32 0, i32 1
  %25 = load i32, i32* %nub18, align 4, !tbaa !40
  %dec19 = add nsw i32 %25, -1
  store i32 %dec19, i32* %nub18, align 4, !tbaa !40
  br label %if.end

if.end:                                           ; preds = %if.then15, %land.lhs.true, %if.then10
  br label %if.end20

if.end20:                                         ; preds = %if.end, %if.else
  %m_solveTwistLimit = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 23
  %26 = load i8, i8* %m_solveTwistLimit, align 1, !tbaa !20, !range !37
  %tobool21 = trunc i8 %26 to i1
  br i1 %tobool21, label %if.then22, label %if.end27

if.then22:                                        ; preds = %if.end20
  %27 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %m_numConstraintRows23 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %27, i32 0, i32 0
  %28 = load i32, i32* %m_numConstraintRows23, align 4, !tbaa !38
  %inc24 = add nsw i32 %28, 1
  store i32 %inc24, i32* %m_numConstraintRows23, align 4, !tbaa !38
  %29 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %nub25 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %29, i32 0, i32 1
  %30 = load i32, i32* %nub25, align 4, !tbaa !40
  %dec26 = add nsw i32 %30, -1
  store i32 %dec26, i32* %nub25, align 4, !tbaa !40
  br label %if.end27

if.end27:                                         ; preds = %if.then22, %if.end20
  br label %if.end28

if.end28:                                         ; preds = %if.end27, %if.then
  ret void
}

define hidden void @_ZN21btConeTwistConstraint14calcAngleInfo2ERK11btTransformS2_RK11btMatrix3x3S5_(%class.btConeTwistConstraint* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %invInertiaWorldA, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %invInertiaWorldB) #0 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %invInertiaWorldA.addr = alloca %class.btMatrix3x3*, align 4
  %invInertiaWorldB.addr = alloca %class.btMatrix3x3*, align 4
  %trPose = alloca %class.btTransform, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %trA = alloca %class.btTransform, align 4
  %trB = alloca %class.btTransform, align 4
  %trDeltaAB = alloca %class.btTransform, align 4
  %ref.tmp7 = alloca %class.btTransform, align 4
  %ref.tmp8 = alloca %class.btTransform, align 4
  %qDeltaAB = alloca %class.btQuaternion, align 4
  %swingAxis = alloca %class.btVector3, align 4
  %swingAxisLen2 = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %qA = alloca %class.btQuaternion, align 4
  %ref.tmp32 = alloca %class.btQuaternion, align 4
  %ref.tmp33 = alloca %class.btQuaternion, align 4
  %qB = alloca %class.btQuaternion, align 4
  %ref.tmp35 = alloca %class.btQuaternion, align 4
  %ref.tmp36 = alloca %class.btQuaternion, align 4
  %qAB = alloca %class.btQuaternion, align 4
  %ref.tmp38 = alloca %class.btQuaternion, align 4
  %vConeNoTwist = alloca %class.btVector3, align 4
  %qABCone = alloca %class.btQuaternion, align 4
  %qABTwist = alloca %class.btQuaternion, align 4
  %ref.tmp41 = alloca %class.btQuaternion, align 4
  %swingAngle = alloca float, align 4
  %swingLimit = alloca float, align 4
  %swingAxis47 = alloca %class.btVector3, align 4
  %ref.tmp68 = alloca %class.btVector3, align 4
  %ref.tmp69 = alloca %class.btVector3, align 4
  %ref.tmp71 = alloca float, align 4
  %ref.tmp72 = alloca float, align 4
  %ref.tmp73 = alloca float, align 4
  %ivA = alloca %class.btVector3, align 4
  %ref.tmp81 = alloca %class.btVector3, align 4
  %jvA = alloca %class.btVector3, align 4
  %ref.tmp85 = alloca %class.btVector3, align 4
  %kvA = alloca %class.btVector3, align 4
  %ref.tmp89 = alloca %class.btVector3, align 4
  %ivB = alloca %class.btVector3, align 4
  %ref.tmp93 = alloca %class.btVector3, align 4
  %target = alloca %class.btVector3, align 4
  %x = alloca float, align 4
  %y = alloca float, align 4
  %z = alloca float, align 4
  %ref.tmp112 = alloca %class.btVector3, align 4
  %ref.tmp113 = alloca %class.btVector3, align 4
  %span2 = alloca float, align 4
  %span1 = alloca float, align 4
  %ref.tmp223 = alloca %class.btVector3, align 4
  %ref.tmp224 = alloca %class.btVector3, align 4
  %twistAxis = alloca %class.btVector3, align 4
  %ref.tmp268 = alloca %class.btVector3, align 4
  %ref.tmp269 = alloca %class.btVector3, align 4
  %ref.tmp280 = alloca %class.btVector3, align 4
  %ref.tmp281 = alloca %class.btVector3, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4, !tbaa !2
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %invInertiaWorldA, %class.btMatrix3x3** %invInertiaWorldA.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %invInertiaWorldB, %class.btMatrix3x3** %invInertiaWorldB.addr, align 4, !tbaa !2
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %m_swingCorrection = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 17
  store float 0.000000e+00, float* %m_swingCorrection, align 4, !tbaa !44
  %m_twistLimitSign = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 16
  store float 0.000000e+00, float* %m_twistLimitSign, align 4, !tbaa !45
  %m_solveTwistLimit = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 23
  store i8 0, i8* %m_solveTwistLimit, align 1, !tbaa !20
  %m_solveSwingLimit = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 24
  store i8 0, i8* %m_solveSwingLimit, align 2, !tbaa !21
  %m_bMotorEnabled = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 29
  %0 = load i8, i8* %m_bMotorEnabled, align 4, !tbaa !22, !range !37
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %land.lhs.true, label %if.end31

land.lhs.true:                                    ; preds = %entry
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 25
  %1 = load i8, i8* %m_useSolveConstraintObsolete, align 1, !tbaa !17, !range !37
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.end31, label %if.then

if.then:                                          ; preds = %land.lhs.true
  %2 = bitcast %class.btTransform* %trPose to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %2) #8
  %m_qTarget = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 31
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #8
  %4 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !30
  %5 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !30
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  store float 0.000000e+00, float* %ref.tmp5, align 4, !tbaa !30
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %call6 = call %class.btTransform* @_ZN11btTransformC2ERK12btQuaternionRK9btVector3(%class.btTransform* %trPose, %class.btQuaternion* nonnull align 4 dereferenceable(16) %m_qTarget, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %7 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  %8 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  %9 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %10) #8
  %11 = bitcast %class.btTransform* %trA to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %11) #8
  %12 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %m_rbAFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %trA, %class.btTransform* %12, %class.btTransform* nonnull align 4 dereferenceable(64) %m_rbAFrame)
  %13 = bitcast %class.btTransform* %trB to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %13) #8
  %14 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %m_rbBFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %trB, %class.btTransform* %14, %class.btTransform* nonnull align 4 dereferenceable(64) %m_rbBFrame)
  %15 = bitcast %class.btTransform* %trDeltaAB to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %15) #8
  %16 = bitcast %class.btTransform* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %16) #8
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp7, %class.btTransform* %trB, %class.btTransform* nonnull align 4 dereferenceable(64) %trPose)
  %17 = bitcast %class.btTransform* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %17) #8
  call void @_ZNK11btTransform7inverseEv(%class.btTransform* sret align 4 %ref.tmp8, %class.btTransform* %trA)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %trDeltaAB, %class.btTransform* %ref.tmp7, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp8)
  %18 = bitcast %class.btTransform* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %18) #8
  %19 = bitcast %class.btTransform* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %19) #8
  %20 = bitcast %class.btQuaternion* %qDeltaAB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %20) #8
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %qDeltaAB, %class.btTransform* %trDeltaAB)
  %21 = bitcast %class.btVector3* %swingAxis to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %21) #8
  %22 = bitcast %class.btQuaternion* %qDeltaAB to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %22)
  %23 = bitcast %class.btQuaternion* %qDeltaAB to %class.btQuadWord*
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %23)
  %24 = bitcast %class.btQuaternion* %qDeltaAB to %class.btQuadWord*
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %24)
  %call12 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %swingAxis, float* nonnull align 4 dereferenceable(4) %call9, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call11)
  %25 = bitcast float* %swingAxisLen2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  %call13 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %swingAxis)
  store float %call13, float* %swingAxisLen2, align 4, !tbaa !30
  %26 = load float, float* %swingAxisLen2, align 4, !tbaa !30
  %call14 = call zeroext i1 @_Z11btFuzzyZerof(float %26)
  br i1 %call14, label %if.then15, label %if.end

if.then15:                                        ; preds = %if.then
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  %m_swingAxis = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %27 = bitcast %class.btVector3* %m_swingAxis to i8*
  %28 = bitcast %class.btVector3* %swingAxis to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %27, i8* align 4 %28, i32 16, i1 false), !tbaa.struct !18
  %m_swingAxis16 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %call17 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %m_swingAxis16)
  %call18 = call float @_ZNK12btQuaternion8getAngleEv(%class.btQuaternion* %qDeltaAB)
  %m_swingCorrection19 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 17
  store float %call18, float* %m_swingCorrection19, align 4, !tbaa !44
  %m_swingCorrection20 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 17
  %29 = load float, float* %m_swingCorrection20, align 4, !tbaa !44
  %call21 = call zeroext i1 @_Z11btFuzzyZerof(float %29)
  br i1 %call21, label %if.end24, label %if.then22

if.then22:                                        ; preds = %if.end
  %m_solveSwingLimit23 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 24
  store i8 1, i8* %m_solveSwingLimit23, align 2, !tbaa !21
  br label %if.end24

if.end24:                                         ; preds = %if.then22, %if.end
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end24, %if.then15
  %30 = bitcast float* %swingAxisLen2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #8
  %31 = bitcast %class.btVector3* %swingAxis to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %31) #8
  %32 = bitcast %class.btQuaternion* %qDeltaAB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %32) #8
  %33 = bitcast %class.btTransform* %trDeltaAB to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %33) #8
  %34 = bitcast %class.btTransform* %trB to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %34) #8
  %35 = bitcast %class.btTransform* %trA to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %35) #8
  %36 = bitcast %class.btTransform* %trPose to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %36) #8
  br label %return

if.end31:                                         ; preds = %land.lhs.true, %entry
  %37 = bitcast %class.btQuaternion* %qA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %37) #8
  %38 = bitcast %class.btQuaternion* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %38) #8
  %39 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp32, %class.btTransform* %39)
  %40 = bitcast %class.btQuaternion* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %40) #8
  %m_rbAFrame34 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp33, %class.btTransform* %m_rbAFrame34)
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %qA, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp32, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp33)
  %41 = bitcast %class.btQuaternion* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %41) #8
  %42 = bitcast %class.btQuaternion* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %42) #8
  %43 = bitcast %class.btQuaternion* %qB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %43) #8
  %44 = bitcast %class.btQuaternion* %ref.tmp35 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %44) #8
  %45 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp35, %class.btTransform* %45)
  %46 = bitcast %class.btQuaternion* %ref.tmp36 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %46) #8
  %m_rbBFrame37 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp36, %class.btTransform* %m_rbBFrame37)
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %qB, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp35, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp36)
  %47 = bitcast %class.btQuaternion* %ref.tmp36 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %47) #8
  %48 = bitcast %class.btQuaternion* %ref.tmp35 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %48) #8
  %49 = bitcast %class.btQuaternion* %qAB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %49) #8
  %50 = bitcast %class.btQuaternion* %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %50) #8
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp38, %class.btQuaternion* %qB)
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %qAB, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp38, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qA)
  %51 = bitcast %class.btQuaternion* %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %51) #8
  %52 = bitcast %class.btVector3* %vConeNoTwist to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %52) #8
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %vConeNoTwist, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qAB, %class.btVector3* nonnull align 4 dereferenceable(16) @_ZL6vTwist)
  %call39 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %vConeNoTwist)
  %53 = bitcast %class.btQuaternion* %qABCone to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %53) #8
  call void @_Z15shortestArcQuatRK9btVector3S1_(%class.btQuaternion* sret align 4 %qABCone, %class.btVector3* nonnull align 4 dereferenceable(16) @_ZL6vTwist, %class.btVector3* nonnull align 4 dereferenceable(16) %vConeNoTwist)
  %call40 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %qABCone)
  %54 = bitcast %class.btQuaternion* %qABTwist to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %54) #8
  %55 = bitcast %class.btQuaternion* %ref.tmp41 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %55) #8
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp41, %class.btQuaternion* %qABCone)
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %qABTwist, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp41, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qAB)
  %56 = bitcast %class.btQuaternion* %ref.tmp41 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %56) #8
  %call42 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %qABTwist)
  %m_swingSpan1 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %57 = load float, float* %m_swingSpan1, align 4, !tbaa !31
  %m_fixThresh = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 11
  %58 = load float, float* %m_fixThresh, align 4, !tbaa !25
  %cmp = fcmp oge float %57, %58
  br i1 %cmp, label %land.lhs.true43, label %if.else

land.lhs.true43:                                  ; preds = %if.end31
  %m_swingSpan2 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %59 = load float, float* %m_swingSpan2, align 4, !tbaa !32
  %m_fixThresh44 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 11
  %60 = load float, float* %m_fixThresh44, align 4, !tbaa !25
  %cmp45 = fcmp oge float %59, %60
  br i1 %cmp45, label %if.then46, label %if.else

if.then46:                                        ; preds = %land.lhs.true43
  %61 = bitcast float* %swingAngle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %61) #8
  %62 = bitcast float* %swingLimit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #8
  store float 0.000000e+00, float* %swingLimit, align 4, !tbaa !30
  %63 = bitcast %class.btVector3* %swingAxis47 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %63) #8
  %call48 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %swingAxis47)
  call void @_ZN21btConeTwistConstraint20computeConeLimitInfoERK12btQuaternionRfR9btVector3S3_(%class.btConeTwistConstraint* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qABCone, float* nonnull align 4 dereferenceable(4) %swingAngle, %class.btVector3* nonnull align 4 dereferenceable(16) %swingAxis47, float* nonnull align 4 dereferenceable(4) %swingLimit)
  %64 = load float, float* %swingAngle, align 4, !tbaa !30
  %65 = load float, float* %swingLimit, align 4, !tbaa !30
  %m_limitSoftness = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 4
  %66 = load float, float* %m_limitSoftness, align 4, !tbaa !34
  %mul = fmul float %65, %66
  %cmp49 = fcmp ogt float %64, %mul
  br i1 %cmp49, label %if.then50, label %if.end79

if.then50:                                        ; preds = %if.then46
  %m_solveSwingLimit51 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 24
  store i8 1, i8* %m_solveSwingLimit51, align 2, !tbaa !21
  %m_swingLimitRatio = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 26
  store float 1.000000e+00, float* %m_swingLimitRatio, align 4, !tbaa !46
  %67 = load float, float* %swingAngle, align 4, !tbaa !30
  %68 = load float, float* %swingLimit, align 4, !tbaa !30
  %cmp52 = fcmp olt float %67, %68
  br i1 %cmp52, label %land.lhs.true53, label %if.end63

land.lhs.true53:                                  ; preds = %if.then50
  %m_limitSoftness54 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 4
  %69 = load float, float* %m_limitSoftness54, align 4, !tbaa !34
  %cmp55 = fcmp olt float %69, 0x3FEFFFFFC0000000
  br i1 %cmp55, label %if.then56, label %if.end63

if.then56:                                        ; preds = %land.lhs.true53
  %70 = load float, float* %swingAngle, align 4, !tbaa !30
  %71 = load float, float* %swingLimit, align 4, !tbaa !30
  %m_limitSoftness57 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 4
  %72 = load float, float* %m_limitSoftness57, align 4, !tbaa !34
  %mul58 = fmul float %71, %72
  %sub = fsub float %70, %mul58
  %73 = load float, float* %swingLimit, align 4, !tbaa !30
  %74 = load float, float* %swingLimit, align 4, !tbaa !30
  %m_limitSoftness59 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 4
  %75 = load float, float* %m_limitSoftness59, align 4, !tbaa !34
  %mul60 = fmul float %74, %75
  %sub61 = fsub float %73, %mul60
  %div = fdiv float %sub, %sub61
  %m_swingLimitRatio62 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 26
  store float %div, float* %m_swingLimitRatio62, align 4, !tbaa !46
  br label %if.end63

if.end63:                                         ; preds = %if.then56, %land.lhs.true53, %if.then50
  %76 = load float, float* %swingAngle, align 4, !tbaa !30
  %77 = load float, float* %swingLimit, align 4, !tbaa !30
  %m_limitSoftness64 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 4
  %78 = load float, float* %m_limitSoftness64, align 4, !tbaa !34
  %mul65 = fmul float %77, %78
  %sub66 = fsub float %76, %mul65
  %m_swingCorrection67 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 17
  store float %sub66, float* %m_swingCorrection67, align 4, !tbaa !44
  call void @_ZNK21btConeTwistConstraint33adjustSwingAxisToUseEllipseNormalER9btVector3(%class.btConeTwistConstraint* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %swingAxis47)
  %79 = bitcast %class.btVector3* %ref.tmp68 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %79) #8
  %80 = bitcast %class.btVector3* %ref.tmp69 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %80) #8
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp69, %class.btVector3* nonnull align 4 dereferenceable(16) %swingAxis47)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %ref.tmp68, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qB, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp69)
  %m_swingAxis70 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %81 = bitcast %class.btVector3* %m_swingAxis70 to i8*
  %82 = bitcast %class.btVector3* %ref.tmp68 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %81, i8* align 4 %82, i32 16, i1 false), !tbaa.struct !18
  %83 = bitcast %class.btVector3* %ref.tmp69 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %83) #8
  %84 = bitcast %class.btVector3* %ref.tmp68 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %84) #8
  %m_twistAxisA = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 28
  %85 = bitcast float* %ref.tmp71 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %85) #8
  store float 0.000000e+00, float* %ref.tmp71, align 4, !tbaa !30
  %86 = bitcast float* %ref.tmp72 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %86) #8
  store float 0.000000e+00, float* %ref.tmp72, align 4, !tbaa !30
  %87 = bitcast float* %ref.tmp73 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %87) #8
  store float 0.000000e+00, float* %ref.tmp73, align 4, !tbaa !30
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_twistAxisA, float* nonnull align 4 dereferenceable(4) %ref.tmp71, float* nonnull align 4 dereferenceable(4) %ref.tmp72, float* nonnull align 4 dereferenceable(4) %ref.tmp73)
  %88 = bitcast float* %ref.tmp73 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #8
  %89 = bitcast float* %ref.tmp72 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #8
  %90 = bitcast float* %ref.tmp71 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #8
  %m_swingAxis74 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %91 = load %class.btMatrix3x3*, %class.btMatrix3x3** %invInertiaWorldA.addr, align 4, !tbaa !2
  %call75 = call float @_Z32computeAngularImpulseDenominatorRK9btVector3RK11btMatrix3x3(%class.btVector3* nonnull align 4 dereferenceable(16) %m_swingAxis74, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %91)
  %m_swingAxis76 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %92 = load %class.btMatrix3x3*, %class.btMatrix3x3** %invInertiaWorldB.addr, align 4, !tbaa !2
  %call77 = call float @_Z32computeAngularImpulseDenominatorRK9btVector3RK11btMatrix3x3(%class.btVector3* nonnull align 4 dereferenceable(16) %m_swingAxis76, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %92)
  %add = fadd float %call75, %call77
  %div78 = fdiv float 1.000000e+00, %add
  %m_kSwing = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 14
  store float %div78, float* %m_kSwing, align 4, !tbaa !47
  br label %if.end79

if.end79:                                         ; preds = %if.end63, %if.then46
  %93 = bitcast %class.btVector3* %swingAxis47 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %93) #8
  %94 = bitcast float* %swingLimit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #8
  %95 = bitcast float* %swingAngle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #8
  br label %if.end232

if.else:                                          ; preds = %land.lhs.true43, %if.end31
  %96 = bitcast %class.btVector3* %ivA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %96) #8
  %97 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %call80 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %97)
  %98 = bitcast %class.btVector3* %ref.tmp81 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %98) #8
  %m_rbAFrame82 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  %call83 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbAFrame82)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp81, %class.btMatrix3x3* %call83, i32 0)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ivA, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call80, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp81)
  %99 = bitcast %class.btVector3* %ref.tmp81 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %99) #8
  %100 = bitcast %class.btVector3* %jvA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %100) #8
  %101 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %call84 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %101)
  %102 = bitcast %class.btVector3* %ref.tmp85 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %102) #8
  %m_rbAFrame86 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  %call87 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbAFrame86)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp85, %class.btMatrix3x3* %call87, i32 1)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %jvA, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call84, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp85)
  %103 = bitcast %class.btVector3* %ref.tmp85 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %103) #8
  %104 = bitcast %class.btVector3* %kvA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %104) #8
  %105 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %call88 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %105)
  %106 = bitcast %class.btVector3* %ref.tmp89 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %106) #8
  %m_rbAFrame90 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  %call91 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbAFrame90)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp89, %class.btMatrix3x3* %call91, i32 2)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %kvA, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call88, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp89)
  %107 = bitcast %class.btVector3* %ref.tmp89 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %107) #8
  %108 = bitcast %class.btVector3* %ivB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %108) #8
  %109 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %call92 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %109)
  %110 = bitcast %class.btVector3* %ref.tmp93 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %110) #8
  %m_rbBFrame94 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  %call95 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbBFrame94)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp93, %class.btMatrix3x3* %call95, i32 0)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ivB, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call92, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp93)
  %111 = bitcast %class.btVector3* %ref.tmp93 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %111) #8
  %112 = bitcast %class.btVector3* %target to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %112) #8
  %call96 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %target)
  %113 = bitcast float* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %113) #8
  %call97 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ivB, %class.btVector3* nonnull align 4 dereferenceable(16) %ivA)
  store float %call97, float* %x, align 4, !tbaa !30
  %114 = bitcast float* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %114) #8
  %call98 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ivB, %class.btVector3* nonnull align 4 dereferenceable(16) %jvA)
  store float %call98, float* %y, align 4, !tbaa !30
  %115 = bitcast float* %z to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %115) #8
  %call99 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ivB, %class.btVector3* nonnull align 4 dereferenceable(16) %kvA)
  store float %call99, float* %z, align 4, !tbaa !30
  %m_swingSpan1100 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %116 = load float, float* %m_swingSpan1100, align 4, !tbaa !31
  %m_fixThresh101 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 11
  %117 = load float, float* %m_fixThresh101, align 4, !tbaa !25
  %cmp102 = fcmp olt float %116, %117
  br i1 %cmp102, label %land.lhs.true103, label %if.else116

land.lhs.true103:                                 ; preds = %if.else
  %m_swingSpan2104 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %118 = load float, float* %m_swingSpan2104, align 4, !tbaa !32
  %m_fixThresh105 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 11
  %119 = load float, float* %m_fixThresh105, align 4, !tbaa !25
  %cmp106 = fcmp olt float %118, %119
  br i1 %cmp106, label %if.then107, label %if.else116

if.then107:                                       ; preds = %land.lhs.true103
  %120 = load float, float* %y, align 4, !tbaa !30
  %call108 = call zeroext i1 @_Z11btFuzzyZerof(float %120)
  br i1 %call108, label %lor.lhs.false, label %if.then110

lor.lhs.false:                                    ; preds = %if.then107
  %121 = load float, float* %z, align 4, !tbaa !30
  %call109 = call zeroext i1 @_Z11btFuzzyZerof(float %121)
  br i1 %call109, label %if.end115, label %if.then110

if.then110:                                       ; preds = %lor.lhs.false, %if.then107
  %m_solveSwingLimit111 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 24
  store i8 1, i8* %m_solveSwingLimit111, align 2, !tbaa !21
  %122 = bitcast %class.btVector3* %ref.tmp112 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %122) #8
  %123 = bitcast %class.btVector3* %ref.tmp113 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %123) #8
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp113, %class.btVector3* %ivB, %class.btVector3* nonnull align 4 dereferenceable(16) %ivA)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp112, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp113)
  %m_swingAxis114 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %124 = bitcast %class.btVector3* %m_swingAxis114 to i8*
  %125 = bitcast %class.btVector3* %ref.tmp112 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %124, i8* align 4 %125, i32 16, i1 false), !tbaa.struct !18
  %126 = bitcast %class.btVector3* %ref.tmp113 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %126) #8
  %127 = bitcast %class.btVector3* %ref.tmp112 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %127) #8
  br label %if.end115

if.end115:                                        ; preds = %if.then110, %lor.lhs.false
  br label %if.end231

if.else116:                                       ; preds = %land.lhs.true103, %if.else
  %m_swingSpan1117 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %128 = load float, float* %m_swingSpan1117, align 4, !tbaa !31
  %m_fixThresh118 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 11
  %129 = load float, float* %m_fixThresh118, align 4, !tbaa !25
  %cmp119 = fcmp olt float %128, %129
  br i1 %cmp119, label %if.then120, label %if.else151

if.then120:                                       ; preds = %if.else116
  %130 = load float, float* %x, align 4, !tbaa !30
  %call121 = call zeroext i1 @_Z11btFuzzyZerof(float %130)
  br i1 %call121, label %lor.lhs.false122, label %if.then124

lor.lhs.false122:                                 ; preds = %if.then120
  %131 = load float, float* %z, align 4, !tbaa !30
  %call123 = call zeroext i1 @_Z11btFuzzyZerof(float %131)
  br i1 %call123, label %if.end150, label %if.then124

if.then124:                                       ; preds = %lor.lhs.false122, %if.then120
  %m_solveSwingLimit125 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 24
  store i8 1, i8* %m_solveSwingLimit125, align 2, !tbaa !21
  %m_swingSpan2126 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %132 = load float, float* %m_swingSpan2126, align 4, !tbaa !32
  %m_fixThresh127 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 11
  %133 = load float, float* %m_fixThresh127, align 4, !tbaa !25
  %cmp128 = fcmp oge float %132, %133
  br i1 %cmp128, label %if.then129, label %if.end149

if.then129:                                       ; preds = %if.then124
  store float 0.000000e+00, float* %y, align 4, !tbaa !30
  %134 = bitcast float* %span2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %134) #8
  %135 = load float, float* %z, align 4, !tbaa !30
  %136 = load float, float* %x, align 4, !tbaa !30
  %call130 = call float @_Z7btAtan2ff(float %135, float %136)
  store float %call130, float* %span2, align 4, !tbaa !30
  %137 = load float, float* %span2, align 4, !tbaa !30
  %m_swingSpan2131 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %138 = load float, float* %m_swingSpan2131, align 4, !tbaa !32
  %cmp132 = fcmp ogt float %137, %138
  br i1 %cmp132, label %if.then133, label %if.else138

if.then133:                                       ; preds = %if.then129
  %m_swingSpan2134 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %139 = load float, float* %m_swingSpan2134, align 4, !tbaa !32
  %call135 = call float @_Z5btCosf(float %139)
  store float %call135, float* %x, align 4, !tbaa !30
  %m_swingSpan2136 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %140 = load float, float* %m_swingSpan2136, align 4, !tbaa !32
  %call137 = call float @_Z5btSinf(float %140)
  store float %call137, float* %z, align 4, !tbaa !30
  br label %if.end148

if.else138:                                       ; preds = %if.then129
  %141 = load float, float* %span2, align 4, !tbaa !30
  %m_swingSpan2139 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %142 = load float, float* %m_swingSpan2139, align 4, !tbaa !32
  %fneg = fneg float %142
  %cmp140 = fcmp olt float %141, %fneg
  br i1 %cmp140, label %if.then141, label %if.end147

if.then141:                                       ; preds = %if.else138
  %m_swingSpan2142 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %143 = load float, float* %m_swingSpan2142, align 4, !tbaa !32
  %call143 = call float @_Z5btCosf(float %143)
  store float %call143, float* %x, align 4, !tbaa !30
  %m_swingSpan2144 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %144 = load float, float* %m_swingSpan2144, align 4, !tbaa !32
  %call145 = call float @_Z5btSinf(float %144)
  %fneg146 = fneg float %call145
  store float %fneg146, float* %z, align 4, !tbaa !30
  br label %if.end147

if.end147:                                        ; preds = %if.then141, %if.else138
  br label %if.end148

if.end148:                                        ; preds = %if.end147, %if.then133
  %145 = bitcast float* %span2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #8
  br label %if.end149

if.end149:                                        ; preds = %if.end148, %if.then124
  br label %if.end150

if.end150:                                        ; preds = %if.end149, %lor.lhs.false122
  br label %if.end183

if.else151:                                       ; preds = %if.else116
  %146 = load float, float* %x, align 4, !tbaa !30
  %call152 = call zeroext i1 @_Z11btFuzzyZerof(float %146)
  br i1 %call152, label %lor.lhs.false153, label %if.then155

lor.lhs.false153:                                 ; preds = %if.else151
  %147 = load float, float* %y, align 4, !tbaa !30
  %call154 = call zeroext i1 @_Z11btFuzzyZerof(float %147)
  br i1 %call154, label %if.end182, label %if.then155

if.then155:                                       ; preds = %lor.lhs.false153, %if.else151
  %m_solveSwingLimit156 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 24
  store i8 1, i8* %m_solveSwingLimit156, align 2, !tbaa !21
  %m_swingSpan1157 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %148 = load float, float* %m_swingSpan1157, align 4, !tbaa !31
  %m_fixThresh158 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 11
  %149 = load float, float* %m_fixThresh158, align 4, !tbaa !25
  %cmp159 = fcmp oge float %148, %149
  br i1 %cmp159, label %if.then160, label %if.end181

if.then160:                                       ; preds = %if.then155
  store float 0.000000e+00, float* %z, align 4, !tbaa !30
  %150 = bitcast float* %span1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %150) #8
  %151 = load float, float* %y, align 4, !tbaa !30
  %152 = load float, float* %x, align 4, !tbaa !30
  %call161 = call float @_Z7btAtan2ff(float %151, float %152)
  store float %call161, float* %span1, align 4, !tbaa !30
  %153 = load float, float* %span1, align 4, !tbaa !30
  %m_swingSpan1162 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %154 = load float, float* %m_swingSpan1162, align 4, !tbaa !31
  %cmp163 = fcmp ogt float %153, %154
  br i1 %cmp163, label %if.then164, label %if.else169

if.then164:                                       ; preds = %if.then160
  %m_swingSpan1165 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %155 = load float, float* %m_swingSpan1165, align 4, !tbaa !31
  %call166 = call float @_Z5btCosf(float %155)
  store float %call166, float* %x, align 4, !tbaa !30
  %m_swingSpan1167 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %156 = load float, float* %m_swingSpan1167, align 4, !tbaa !31
  %call168 = call float @_Z5btSinf(float %156)
  store float %call168, float* %y, align 4, !tbaa !30
  br label %if.end180

if.else169:                                       ; preds = %if.then160
  %157 = load float, float* %span1, align 4, !tbaa !30
  %m_swingSpan1170 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %158 = load float, float* %m_swingSpan1170, align 4, !tbaa !31
  %fneg171 = fneg float %158
  %cmp172 = fcmp olt float %157, %fneg171
  br i1 %cmp172, label %if.then173, label %if.end179

if.then173:                                       ; preds = %if.else169
  %m_swingSpan1174 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %159 = load float, float* %m_swingSpan1174, align 4, !tbaa !31
  %call175 = call float @_Z5btCosf(float %159)
  store float %call175, float* %x, align 4, !tbaa !30
  %m_swingSpan1176 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %160 = load float, float* %m_swingSpan1176, align 4, !tbaa !31
  %call177 = call float @_Z5btSinf(float %160)
  %fneg178 = fneg float %call177
  store float %fneg178, float* %y, align 4, !tbaa !30
  br label %if.end179

if.end179:                                        ; preds = %if.then173, %if.else169
  br label %if.end180

if.end180:                                        ; preds = %if.end179, %if.then164
  %161 = bitcast float* %span1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %161) #8
  br label %if.end181

if.end181:                                        ; preds = %if.end180, %if.then155
  br label %if.end182

if.end182:                                        ; preds = %if.end181, %lor.lhs.false153
  br label %if.end183

if.end183:                                        ; preds = %if.end182, %if.end150
  %162 = load float, float* %x, align 4, !tbaa !30
  %call184 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ivA)
  %arrayidx = getelementptr inbounds float, float* %call184, i32 0
  %163 = load float, float* %arrayidx, align 4, !tbaa !30
  %mul185 = fmul float %162, %163
  %164 = load float, float* %y, align 4, !tbaa !30
  %call186 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %jvA)
  %arrayidx187 = getelementptr inbounds float, float* %call186, i32 0
  %165 = load float, float* %arrayidx187, align 4, !tbaa !30
  %mul188 = fmul float %164, %165
  %add189 = fadd float %mul185, %mul188
  %166 = load float, float* %z, align 4, !tbaa !30
  %call190 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %kvA)
  %arrayidx191 = getelementptr inbounds float, float* %call190, i32 0
  %167 = load float, float* %arrayidx191, align 4, !tbaa !30
  %mul192 = fmul float %166, %167
  %add193 = fadd float %add189, %mul192
  %call194 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %target)
  %arrayidx195 = getelementptr inbounds float, float* %call194, i32 0
  store float %add193, float* %arrayidx195, align 4, !tbaa !30
  %168 = load float, float* %x, align 4, !tbaa !30
  %call196 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ivA)
  %arrayidx197 = getelementptr inbounds float, float* %call196, i32 1
  %169 = load float, float* %arrayidx197, align 4, !tbaa !30
  %mul198 = fmul float %168, %169
  %170 = load float, float* %y, align 4, !tbaa !30
  %call199 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %jvA)
  %arrayidx200 = getelementptr inbounds float, float* %call199, i32 1
  %171 = load float, float* %arrayidx200, align 4, !tbaa !30
  %mul201 = fmul float %170, %171
  %add202 = fadd float %mul198, %mul201
  %172 = load float, float* %z, align 4, !tbaa !30
  %call203 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %kvA)
  %arrayidx204 = getelementptr inbounds float, float* %call203, i32 1
  %173 = load float, float* %arrayidx204, align 4, !tbaa !30
  %mul205 = fmul float %172, %173
  %add206 = fadd float %add202, %mul205
  %call207 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %target)
  %arrayidx208 = getelementptr inbounds float, float* %call207, i32 1
  store float %add206, float* %arrayidx208, align 4, !tbaa !30
  %174 = load float, float* %x, align 4, !tbaa !30
  %call209 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ivA)
  %arrayidx210 = getelementptr inbounds float, float* %call209, i32 2
  %175 = load float, float* %arrayidx210, align 4, !tbaa !30
  %mul211 = fmul float %174, %175
  %176 = load float, float* %y, align 4, !tbaa !30
  %call212 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %jvA)
  %arrayidx213 = getelementptr inbounds float, float* %call212, i32 2
  %177 = load float, float* %arrayidx213, align 4, !tbaa !30
  %mul214 = fmul float %176, %177
  %add215 = fadd float %mul211, %mul214
  %178 = load float, float* %z, align 4, !tbaa !30
  %call216 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %kvA)
  %arrayidx217 = getelementptr inbounds float, float* %call216, i32 2
  %179 = load float, float* %arrayidx217, align 4, !tbaa !30
  %mul218 = fmul float %178, %179
  %add219 = fadd float %add215, %mul218
  %call220 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %target)
  %arrayidx221 = getelementptr inbounds float, float* %call220, i32 2
  store float %add219, float* %arrayidx221, align 4, !tbaa !30
  %call222 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %target)
  %180 = bitcast %class.btVector3* %ref.tmp223 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %180) #8
  %181 = bitcast %class.btVector3* %ref.tmp224 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %181) #8
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp224, %class.btVector3* %ivB, %class.btVector3* nonnull align 4 dereferenceable(16) %target)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp223, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp224)
  %m_swingAxis225 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %182 = bitcast %class.btVector3* %m_swingAxis225 to i8*
  %183 = bitcast %class.btVector3* %ref.tmp223 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %182, i8* align 4 %183, i32 16, i1 false), !tbaa.struct !18
  %184 = bitcast %class.btVector3* %ref.tmp224 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %184) #8
  %185 = bitcast %class.btVector3* %ref.tmp223 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %185) #8
  %m_swingAxis226 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %call227 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %m_swingAxis226)
  %m_swingCorrection228 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 17
  store float %call227, float* %m_swingCorrection228, align 4, !tbaa !44
  %m_swingAxis229 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %call230 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %m_swingAxis229)
  br label %if.end231

if.end231:                                        ; preds = %if.end183, %if.end115
  %186 = bitcast float* %z to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %186) #8
  %187 = bitcast float* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %187) #8
  %188 = bitcast float* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %188) #8
  %189 = bitcast %class.btVector3* %target to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %189) #8
  %190 = bitcast %class.btVector3* %ivB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %190) #8
  %191 = bitcast %class.btVector3* %kvA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %191) #8
  %192 = bitcast %class.btVector3* %jvA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %192) #8
  %193 = bitcast %class.btVector3* %ivA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %193) #8
  br label %if.end232

if.end232:                                        ; preds = %if.end231, %if.end79
  %m_twistSpan = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %194 = load float, float* %m_twistSpan, align 4, !tbaa !33
  %cmp233 = fcmp oge float %194, 0.000000e+00
  br i1 %cmp233, label %if.then234, label %if.else284

if.then234:                                       ; preds = %if.end232
  %195 = bitcast %class.btVector3* %twistAxis to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %195) #8
  %call235 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %twistAxis)
  %m_twistAngle = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 19
  call void @_ZN21btConeTwistConstraint21computeTwistLimitInfoERK12btQuaternionRfR9btVector3(%class.btConeTwistConstraint* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qABTwist, float* nonnull align 4 dereferenceable(4) %m_twistAngle, %class.btVector3* nonnull align 4 dereferenceable(16) %twistAxis)
  %m_twistAngle236 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 19
  %196 = load float, float* %m_twistAngle236, align 4, !tbaa !48
  %m_twistSpan237 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %197 = load float, float* %m_twistSpan237, align 4, !tbaa !33
  %m_limitSoftness238 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 4
  %198 = load float, float* %m_limitSoftness238, align 4, !tbaa !34
  %mul239 = fmul float %197, %198
  %cmp240 = fcmp ogt float %196, %mul239
  br i1 %cmp240, label %if.then241, label %if.end276

if.then241:                                       ; preds = %if.then234
  %m_solveTwistLimit242 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 23
  store i8 1, i8* %m_solveTwistLimit242, align 1, !tbaa !20
  %m_twistLimitRatio = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 27
  store float 1.000000e+00, float* %m_twistLimitRatio, align 4, !tbaa !49
  %m_twistAngle243 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 19
  %199 = load float, float* %m_twistAngle243, align 4, !tbaa !48
  %m_twistSpan244 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %200 = load float, float* %m_twistSpan244, align 4, !tbaa !33
  %cmp245 = fcmp olt float %199, %200
  br i1 %cmp245, label %land.lhs.true246, label %if.end262

land.lhs.true246:                                 ; preds = %if.then241
  %m_limitSoftness247 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 4
  %201 = load float, float* %m_limitSoftness247, align 4, !tbaa !34
  %cmp248 = fcmp olt float %201, 0x3FEFFFFFC0000000
  br i1 %cmp248, label %if.then249, label %if.end262

if.then249:                                       ; preds = %land.lhs.true246
  %m_twistAngle250 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 19
  %202 = load float, float* %m_twistAngle250, align 4, !tbaa !48
  %m_twistSpan251 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %203 = load float, float* %m_twistSpan251, align 4, !tbaa !33
  %m_limitSoftness252 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 4
  %204 = load float, float* %m_limitSoftness252, align 4, !tbaa !34
  %mul253 = fmul float %203, %204
  %sub254 = fsub float %202, %mul253
  %m_twistSpan255 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %205 = load float, float* %m_twistSpan255, align 4, !tbaa !33
  %m_twistSpan256 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %206 = load float, float* %m_twistSpan256, align 4, !tbaa !33
  %m_limitSoftness257 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 4
  %207 = load float, float* %m_limitSoftness257, align 4, !tbaa !34
  %mul258 = fmul float %206, %207
  %sub259 = fsub float %205, %mul258
  %div260 = fdiv float %sub254, %sub259
  %m_twistLimitRatio261 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 27
  store float %div260, float* %m_twistLimitRatio261, align 4, !tbaa !49
  br label %if.end262

if.end262:                                        ; preds = %if.then249, %land.lhs.true246, %if.then241
  %m_twistAngle263 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 19
  %208 = load float, float* %m_twistAngle263, align 4, !tbaa !48
  %m_twistSpan264 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %209 = load float, float* %m_twistSpan264, align 4, !tbaa !33
  %m_limitSoftness265 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 4
  %210 = load float, float* %m_limitSoftness265, align 4, !tbaa !34
  %mul266 = fmul float %209, %210
  %sub267 = fsub float %208, %mul266
  %m_twistCorrection = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 18
  store float %sub267, float* %m_twistCorrection, align 4, !tbaa !50
  %211 = bitcast %class.btVector3* %ref.tmp268 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %211) #8
  %212 = bitcast %class.btVector3* %ref.tmp269 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %212) #8
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp269, %class.btVector3* nonnull align 4 dereferenceable(16) %twistAxis)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %ref.tmp268, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qB, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp269)
  %m_twistAxis = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 13
  %213 = bitcast %class.btVector3* %m_twistAxis to i8*
  %214 = bitcast %class.btVector3* %ref.tmp268 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %213, i8* align 4 %214, i32 16, i1 false), !tbaa.struct !18
  %215 = bitcast %class.btVector3* %ref.tmp269 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %215) #8
  %216 = bitcast %class.btVector3* %ref.tmp268 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %216) #8
  %m_twistAxis270 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 13
  %217 = load %class.btMatrix3x3*, %class.btMatrix3x3** %invInertiaWorldA.addr, align 4, !tbaa !2
  %call271 = call float @_Z32computeAngularImpulseDenominatorRK9btVector3RK11btMatrix3x3(%class.btVector3* nonnull align 4 dereferenceable(16) %m_twistAxis270, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %217)
  %m_twistAxis272 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 13
  %218 = load %class.btMatrix3x3*, %class.btMatrix3x3** %invInertiaWorldB.addr, align 4, !tbaa !2
  %call273 = call float @_Z32computeAngularImpulseDenominatorRK9btVector3RK11btMatrix3x3(%class.btVector3* nonnull align 4 dereferenceable(16) %m_twistAxis272, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %218)
  %add274 = fadd float %call271, %call273
  %div275 = fdiv float 1.000000e+00, %add274
  %m_kTwist = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 15
  store float %div275, float* %m_kTwist, align 4, !tbaa !51
  br label %if.end276

if.end276:                                        ; preds = %if.end262, %if.then234
  %m_solveSwingLimit277 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 24
  %219 = load i8, i8* %m_solveSwingLimit277, align 2, !tbaa !21, !range !37
  %tobool278 = trunc i8 %219 to i1
  br i1 %tobool278, label %if.then279, label %if.end283

if.then279:                                       ; preds = %if.end276
  %220 = bitcast %class.btVector3* %ref.tmp280 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %220) #8
  %221 = bitcast %class.btVector3* %ref.tmp281 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %221) #8
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp281, %class.btVector3* nonnull align 4 dereferenceable(16) %twistAxis)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %ref.tmp280, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qA, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp281)
  %m_twistAxisA282 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 28
  %222 = bitcast %class.btVector3* %m_twistAxisA282 to i8*
  %223 = bitcast %class.btVector3* %ref.tmp280 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %222, i8* align 4 %223, i32 16, i1 false), !tbaa.struct !18
  %224 = bitcast %class.btVector3* %ref.tmp281 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %224) #8
  %225 = bitcast %class.btVector3* %ref.tmp280 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %225) #8
  br label %if.end283

if.end283:                                        ; preds = %if.then279, %if.end276
  %226 = bitcast %class.btVector3* %twistAxis to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %226) #8
  br label %if.end286

if.else284:                                       ; preds = %if.end232
  %m_twistAngle285 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 19
  store float 0.000000e+00, float* %m_twistAngle285, align 4, !tbaa !48
  br label %if.end286

if.end286:                                        ; preds = %if.else284, %if.end283
  %227 = bitcast %class.btQuaternion* %qABTwist to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %227) #8
  %228 = bitcast %class.btQuaternion* %qABCone to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %228) #8
  %229 = bitcast %class.btVector3* %vConeNoTwist to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %229) #8
  %230 = bitcast %class.btQuaternion* %qAB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %230) #8
  %231 = bitcast %class.btQuaternion* %qB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %231) #8
  %232 = bitcast %class.btQuaternion* %qA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %232) #8
  br label %return

return:                                           ; preds = %if.end286, %cleanup
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_invInertiaTensorWorld = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 1
  ret %class.btMatrix3x3* %m_invInertiaTensorWorld
}

; Function Attrs: nounwind
define hidden void @_ZN21btConeTwistConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E(%class.btConeTwistConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo1"* %info) #5 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo1"*, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4, !tbaa !2
  store %"struct.btTypedConstraint::btConstraintInfo1"* %info, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %m_numConstraintRows = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %0, i32 0, i32 0
  store i32 6, i32* %m_numConstraintRows, align 4, !tbaa !38
  %1 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %nub = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %1, i32 0, i32 1
  store i32 0, i32* %nub, align 4, !tbaa !40
  ret void
}

define hidden void @_ZN21btConeTwistConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E(%class.btConeTwistConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo2"* %info) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo2"*, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4, !tbaa !2
  store %"struct.btTypedConstraint::btConstraintInfo2"* %info, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %1 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %1, i32 0, i32 8
  %2 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4, !tbaa !41
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %2)
  %3 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %3, i32 0, i32 9
  %4 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4, !tbaa !43
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %4)
  %5 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA3 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %5, i32 0, i32 8
  %6 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA3, align 4, !tbaa !41
  %call4 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %6)
  %7 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB5 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %7, i32 0, i32 9
  %8 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB5, align 4, !tbaa !43
  %call6 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %8)
  call void @_ZN21btConeTwistConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK11btMatrix3x3S8_(%class.btConeTwistConstraint* %this1, %"struct.btTypedConstraint::btConstraintInfo2"* %0, %class.btTransform* nonnull align 4 dereferenceable(64) %call, %class.btTransform* nonnull align 4 dereferenceable(64) %call2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call4, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call6)
  ret void
}

define hidden void @_ZN21btConeTwistConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK11btMatrix3x3S8_(%class.btConeTwistConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo2"* %info, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %invInertiaWorldA, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %invInertiaWorldB) #0 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo2"*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %invInertiaWorldA.addr = alloca %class.btMatrix3x3*, align 4
  %invInertiaWorldB.addr = alloca %class.btMatrix3x3*, align 4
  %a1 = alloca %class.btVector3, align 4
  %angular0 = alloca %class.btVector3*, align 4
  %angular1 = alloca %class.btVector3*, align 4
  %angular2 = alloca %class.btVector3*, align 4
  %a1neg = alloca %class.btVector3, align 4
  %a2 = alloca %class.btVector3, align 4
  %angular027 = alloca %class.btVector3*, align 4
  %angular128 = alloca %class.btVector3*, align 4
  %angular232 = alloca %class.btVector3*, align 4
  %linERP = alloca float, align 4
  %k = alloca float, align 4
  %j = alloca i32, align 4
  %row = alloca i32, align 4
  %srow = alloca i32, align 4
  %ax1 = alloca %class.btVector3, align 4
  %J1 = alloca float*, align 4
  %J2 = alloca float*, align 4
  %trA = alloca %class.btTransform, align 4
  %p = alloca %class.btVector3, align 4
  %q = alloca %class.btVector3, align 4
  %srow1 = alloca i32, align 4
  %fact = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp156 = alloca %class.btVector3, align 4
  %k187 = alloca float, align 4
  %ref.tmp210 = alloca %class.btVector3, align 4
  %ref.tmp211 = alloca %class.btVector3, align 4
  %J1214 = alloca float*, align 4
  %J2216 = alloca float*, align 4
  %k245 = alloca float, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4, !tbaa !2
  store %"struct.btTypedConstraint::btConstraintInfo2"* %info, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4, !tbaa !2
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %invInertiaWorldA, %class.btMatrix3x3** %invInertiaWorldA.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %invInertiaWorldB, %class.btMatrix3x3** %invInertiaWorldB.addr, align 4, !tbaa !2
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %1 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %invInertiaWorldA.addr, align 4, !tbaa !2
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %invInertiaWorldB.addr, align 4, !tbaa !2
  call void @_ZN21btConeTwistConstraint14calcAngleInfo2ERK11btTransformS2_RK11btMatrix3x3S5_(%class.btConeTwistConstraint* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %0, %class.btTransform* nonnull align 4 dereferenceable(64) %1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %3)
  %4 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1linearAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %4, i32 0, i32 2
  %5 = load float*, float** %m_J1linearAxis, align 4, !tbaa !52
  %arrayidx = getelementptr inbounds float, float* %5, i32 0
  store float 1.000000e+00, float* %arrayidx, align 4, !tbaa !30
  %6 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1linearAxis2 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %6, i32 0, i32 2
  %7 = load float*, float** %m_J1linearAxis2, align 4, !tbaa !52
  %8 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %8, i32 0, i32 6
  %9 = load i32, i32* %rowskip, align 4, !tbaa !54
  %add = add nsw i32 %9, 1
  %arrayidx3 = getelementptr inbounds float, float* %7, i32 %add
  store float 1.000000e+00, float* %arrayidx3, align 4, !tbaa !30
  %10 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1linearAxis4 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %10, i32 0, i32 2
  %11 = load float*, float** %m_J1linearAxis4, align 4, !tbaa !52
  %12 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip5 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %12, i32 0, i32 6
  %13 = load i32, i32* %rowskip5, align 4, !tbaa !54
  %mul = mul nsw i32 2, %13
  %add6 = add nsw i32 %mul, 2
  %arrayidx7 = getelementptr inbounds float, float* %11, i32 %add6
  store float 1.000000e+00, float* %arrayidx7, align 4, !tbaa !30
  %14 = bitcast %class.btVector3* %a1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %14) #8
  %15 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %15)
  %m_rbAFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_rbAFrame)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %a1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call8)
  %16 = bitcast %class.btVector3** %angular0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  %17 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %17, i32 0, i32 3
  %18 = load float*, float** %m_J1angularAxis, align 4, !tbaa !55
  %19 = bitcast float* %18 to %class.btVector3*
  store %class.btVector3* %19, %class.btVector3** %angular0, align 4, !tbaa !2
  %20 = bitcast %class.btVector3** %angular1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #8
  %21 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis9 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %21, i32 0, i32 3
  %22 = load float*, float** %m_J1angularAxis9, align 4, !tbaa !55
  %23 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip10 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %23, i32 0, i32 6
  %24 = load i32, i32* %rowskip10, align 4, !tbaa !54
  %add.ptr = getelementptr inbounds float, float* %22, i32 %24
  %25 = bitcast float* %add.ptr to %class.btVector3*
  store %class.btVector3* %25, %class.btVector3** %angular1, align 4, !tbaa !2
  %26 = bitcast %class.btVector3** %angular2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #8
  %27 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis11 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %27, i32 0, i32 3
  %28 = load float*, float** %m_J1angularAxis11, align 4, !tbaa !55
  %29 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip12 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %29, i32 0, i32 6
  %30 = load i32, i32* %rowskip12, align 4, !tbaa !54
  %mul13 = mul nsw i32 2, %30
  %add.ptr14 = getelementptr inbounds float, float* %28, i32 %mul13
  %31 = bitcast float* %add.ptr14 to %class.btVector3*
  store %class.btVector3* %31, %class.btVector3** %angular2, align 4, !tbaa !2
  %32 = bitcast %class.btVector3* %a1neg to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %32) #8
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %a1neg, %class.btVector3* nonnull align 4 dereferenceable(16) %a1)
  %33 = load %class.btVector3*, %class.btVector3** %angular0, align 4, !tbaa !2
  %34 = load %class.btVector3*, %class.btVector3** %angular1, align 4, !tbaa !2
  %35 = load %class.btVector3*, %class.btVector3** %angular2, align 4, !tbaa !2
  call void @_ZNK9btVector322getSkewSymmetricMatrixEPS_S0_S0_(%class.btVector3* %a1neg, %class.btVector3* %33, %class.btVector3* %34, %class.btVector3* %35)
  %36 = bitcast %class.btVector3* %a1neg to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %36) #8
  %37 = bitcast %class.btVector3** %angular2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #8
  %38 = bitcast %class.btVector3** %angular1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #8
  %39 = bitcast %class.btVector3** %angular0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #8
  %40 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2linearAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %40, i32 0, i32 4
  %41 = load float*, float** %m_J2linearAxis, align 4, !tbaa !56
  %arrayidx15 = getelementptr inbounds float, float* %41, i32 0
  store float -1.000000e+00, float* %arrayidx15, align 4, !tbaa !30
  %42 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2linearAxis16 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %42, i32 0, i32 4
  %43 = load float*, float** %m_J2linearAxis16, align 4, !tbaa !56
  %44 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip17 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %44, i32 0, i32 6
  %45 = load i32, i32* %rowskip17, align 4, !tbaa !54
  %add18 = add nsw i32 %45, 1
  %arrayidx19 = getelementptr inbounds float, float* %43, i32 %add18
  store float -1.000000e+00, float* %arrayidx19, align 4, !tbaa !30
  %46 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2linearAxis20 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %46, i32 0, i32 4
  %47 = load float*, float** %m_J2linearAxis20, align 4, !tbaa !56
  %48 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip21 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %48, i32 0, i32 6
  %49 = load i32, i32* %rowskip21, align 4, !tbaa !54
  %mul22 = mul nsw i32 2, %49
  %add23 = add nsw i32 %mul22, 2
  %arrayidx24 = getelementptr inbounds float, float* %47, i32 %add23
  store float -1.000000e+00, float* %arrayidx24, align 4, !tbaa !30
  %50 = bitcast %class.btVector3* %a2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %50) #8
  %51 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %call25 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %51)
  %m_rbBFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  %call26 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_rbBFrame)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %a2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call25, %class.btVector3* nonnull align 4 dereferenceable(16) %call26)
  %52 = bitcast %class.btVector3** %angular027 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #8
  %53 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %53, i32 0, i32 5
  %54 = load float*, float** %m_J2angularAxis, align 4, !tbaa !57
  %55 = bitcast float* %54 to %class.btVector3*
  store %class.btVector3* %55, %class.btVector3** %angular027, align 4, !tbaa !2
  %56 = bitcast %class.btVector3** %angular128 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #8
  %57 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis29 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %57, i32 0, i32 5
  %58 = load float*, float** %m_J2angularAxis29, align 4, !tbaa !57
  %59 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip30 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %59, i32 0, i32 6
  %60 = load i32, i32* %rowskip30, align 4, !tbaa !54
  %add.ptr31 = getelementptr inbounds float, float* %58, i32 %60
  %61 = bitcast float* %add.ptr31 to %class.btVector3*
  store %class.btVector3* %61, %class.btVector3** %angular128, align 4, !tbaa !2
  %62 = bitcast %class.btVector3** %angular232 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #8
  %63 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis33 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %63, i32 0, i32 5
  %64 = load float*, float** %m_J2angularAxis33, align 4, !tbaa !57
  %65 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip34 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %65, i32 0, i32 6
  %66 = load i32, i32* %rowskip34, align 4, !tbaa !54
  %mul35 = mul nsw i32 2, %66
  %add.ptr36 = getelementptr inbounds float, float* %64, i32 %mul35
  %67 = bitcast float* %add.ptr36 to %class.btVector3*
  store %class.btVector3* %67, %class.btVector3** %angular232, align 4, !tbaa !2
  %68 = load %class.btVector3*, %class.btVector3** %angular027, align 4, !tbaa !2
  %69 = load %class.btVector3*, %class.btVector3** %angular128, align 4, !tbaa !2
  %70 = load %class.btVector3*, %class.btVector3** %angular232, align 4, !tbaa !2
  call void @_ZNK9btVector322getSkewSymmetricMatrixEPS_S0_S0_(%class.btVector3* %a2, %class.btVector3* %68, %class.btVector3* %69, %class.btVector3* %70)
  %71 = bitcast %class.btVector3** %angular232 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #8
  %72 = bitcast %class.btVector3** %angular128 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #8
  %73 = bitcast %class.btVector3** %angular027 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #8
  %74 = bitcast float* %linERP to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %74) #8
  %m_flags = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 34
  %75 = load i32, i32* %m_flags, align 4, !tbaa !26
  %and = and i32 %75, 2
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_linERP = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 36
  %76 = load float, float* %m_linERP, align 4, !tbaa !28
  br label %cond.end

cond.false:                                       ; preds = %entry
  %77 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %erp = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %77, i32 0, i32 1
  %78 = load float, float* %erp, align 4, !tbaa !58
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %76, %cond.true ], [ %78, %cond.false ]
  store float %cond, float* %linERP, align 4, !tbaa !30
  %79 = bitcast float* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #8
  %80 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %fps = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %80, i32 0, i32 0
  %81 = load float, float* %fps, align 4, !tbaa !59
  %82 = load float, float* %linERP, align 4, !tbaa !30
  %mul37 = fmul float %81, %82
  store float %mul37, float* %k, align 4, !tbaa !30
  %83 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %83) #8
  store i32 0, i32* %j, align 4, !tbaa !60
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end
  %84 = load i32, i32* %j, align 4, !tbaa !60
  %cmp = icmp slt i32 %84, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %85 = load float, float* %k, align 4, !tbaa !30
  %call38 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %a2)
  %86 = load i32, i32* %j, align 4, !tbaa !60
  %arrayidx39 = getelementptr inbounds float, float* %call38, i32 %86
  %87 = load float, float* %arrayidx39, align 4, !tbaa !30
  %88 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4, !tbaa !2
  %call40 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %88)
  %call41 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call40)
  %89 = load i32, i32* %j, align 4, !tbaa !60
  %arrayidx42 = getelementptr inbounds float, float* %call41, i32 %89
  %90 = load float, float* %arrayidx42, align 4, !tbaa !30
  %add43 = fadd float %87, %90
  %call44 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %a1)
  %91 = load i32, i32* %j, align 4, !tbaa !60
  %arrayidx45 = getelementptr inbounds float, float* %call44, i32 %91
  %92 = load float, float* %arrayidx45, align 4, !tbaa !30
  %sub = fsub float %add43, %92
  %93 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %call46 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %93)
  %call47 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call46)
  %94 = load i32, i32* %j, align 4, !tbaa !60
  %arrayidx48 = getelementptr inbounds float, float* %call47, i32 %94
  %95 = load float, float* %arrayidx48, align 4, !tbaa !30
  %sub49 = fsub float %sub, %95
  %mul50 = fmul float %85, %sub49
  %96 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %96, i32 0, i32 7
  %97 = load float*, float** %m_constraintError, align 4, !tbaa !61
  %98 = load i32, i32* %j, align 4, !tbaa !60
  %99 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip51 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %99, i32 0, i32 6
  %100 = load i32, i32* %rowskip51, align 4, !tbaa !54
  %mul52 = mul nsw i32 %98, %100
  %arrayidx53 = getelementptr inbounds float, float* %97, i32 %mul52
  store float %mul50, float* %arrayidx53, align 4, !tbaa !30
  %101 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_lowerLimit = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %101, i32 0, i32 9
  %102 = load float*, float** %m_lowerLimit, align 4, !tbaa !62
  %103 = load i32, i32* %j, align 4, !tbaa !60
  %104 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip54 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %104, i32 0, i32 6
  %105 = load i32, i32* %rowskip54, align 4, !tbaa !54
  %mul55 = mul nsw i32 %103, %105
  %arrayidx56 = getelementptr inbounds float, float* %102, i32 %mul55
  store float 0xC7EFFFFFE0000000, float* %arrayidx56, align 4, !tbaa !30
  %106 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_upperLimit = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %106, i32 0, i32 10
  %107 = load float*, float** %m_upperLimit, align 4, !tbaa !63
  %108 = load i32, i32* %j, align 4, !tbaa !60
  %109 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip57 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %109, i32 0, i32 6
  %110 = load i32, i32* %rowskip57, align 4, !tbaa !54
  %mul58 = mul nsw i32 %108, %110
  %arrayidx59 = getelementptr inbounds float, float* %107, i32 %mul58
  store float 0x47EFFFFFE0000000, float* %arrayidx59, align 4, !tbaa !30
  %m_flags60 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 34
  %111 = load i32, i32* %m_flags60, align 4, !tbaa !26
  %and61 = and i32 %111, 1
  %tobool62 = icmp ne i32 %and61, 0
  br i1 %tobool62, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %m_linCFM = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 35
  %112 = load float, float* %m_linCFM, align 4, !tbaa !27
  %113 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %cfm = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %113, i32 0, i32 8
  %114 = load float*, float** %cfm, align 4, !tbaa !64
  %115 = load i32, i32* %j, align 4, !tbaa !60
  %116 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip63 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %116, i32 0, i32 6
  %117 = load i32, i32* %rowskip63, align 4, !tbaa !54
  %mul64 = mul nsw i32 %115, %117
  %arrayidx65 = getelementptr inbounds float, float* %114, i32 %mul64
  store float %112, float* %arrayidx65, align 4, !tbaa !30
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %118 = load i32, i32* %j, align 4, !tbaa !60
  %inc = add nsw i32 %118, 1
  store i32 %inc, i32* %j, align 4, !tbaa !60
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %119 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %119) #8
  store i32 3, i32* %row, align 4, !tbaa !60
  %120 = bitcast i32* %srow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %120) #8
  %121 = load i32, i32* %row, align 4, !tbaa !60
  %122 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip66 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %122, i32 0, i32 6
  %123 = load i32, i32* %rowskip66, align 4, !tbaa !54
  %mul67 = mul nsw i32 %121, %123
  store i32 %mul67, i32* %srow, align 4, !tbaa !60
  %124 = bitcast %class.btVector3* %ax1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %124) #8
  %call68 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ax1)
  %m_solveSwingLimit = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 24
  %125 = load i8, i8* %m_solveSwingLimit, align 2, !tbaa !21, !range !37
  %tobool69 = trunc i8 %125 to i1
  br i1 %tobool69, label %if.then70, label %if.end207

if.then70:                                        ; preds = %for.end
  %126 = bitcast float** %J1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %126) #8
  %127 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis71 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %127, i32 0, i32 3
  %128 = load float*, float** %m_J1angularAxis71, align 4, !tbaa !55
  store float* %128, float** %J1, align 4, !tbaa !2
  %129 = bitcast float** %J2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %129) #8
  %130 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis72 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %130, i32 0, i32 5
  %131 = load float*, float** %m_J2angularAxis72, align 4, !tbaa !57
  store float* %131, float** %J2, align 4, !tbaa !2
  %m_swingSpan1 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %132 = load float, float* %m_swingSpan1, align 4, !tbaa !31
  %m_fixThresh = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 11
  %133 = load float, float* %m_fixThresh, align 4, !tbaa !25
  %cmp73 = fcmp olt float %132, %133
  br i1 %cmp73, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %if.then70
  %m_swingSpan2 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %134 = load float, float* %m_swingSpan2, align 4, !tbaa !32
  %m_fixThresh74 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 11
  %135 = load float, float* %m_fixThresh74, align 4, !tbaa !25
  %cmp75 = fcmp olt float %134, %135
  br i1 %cmp75, label %if.then76, label %if.else

if.then76:                                        ; preds = %land.lhs.true
  %136 = bitcast %class.btTransform* %trA to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %136) #8
  %137 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4, !tbaa !2
  %m_rbAFrame77 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %trA, %class.btTransform* %137, %class.btTransform* nonnull align 4 dereferenceable(64) %m_rbAFrame77)
  %138 = bitcast %class.btVector3* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %138) #8
  %call78 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %trA)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %p, %class.btMatrix3x3* %call78, i32 1)
  %139 = bitcast %class.btVector3* %q to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %139) #8
  %call79 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %trA)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %q, %class.btMatrix3x3* %call79, i32 2)
  %140 = bitcast i32* %srow1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %140) #8
  %141 = load i32, i32* %srow, align 4, !tbaa !60
  %142 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip80 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %142, i32 0, i32 6
  %143 = load i32, i32* %rowskip80, align 4, !tbaa !54
  %add81 = add nsw i32 %141, %143
  store i32 %add81, i32* %srow1, align 4, !tbaa !60
  %call82 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx83 = getelementptr inbounds float, float* %call82, i32 0
  %144 = load float, float* %arrayidx83, align 4, !tbaa !30
  %145 = load float*, float** %J1, align 4, !tbaa !2
  %146 = load i32, i32* %srow, align 4, !tbaa !60
  %add84 = add nsw i32 %146, 0
  %arrayidx85 = getelementptr inbounds float, float* %145, i32 %add84
  store float %144, float* %arrayidx85, align 4, !tbaa !30
  %call86 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 1
  %147 = load float, float* %arrayidx87, align 4, !tbaa !30
  %148 = load float*, float** %J1, align 4, !tbaa !2
  %149 = load i32, i32* %srow, align 4, !tbaa !60
  %add88 = add nsw i32 %149, 1
  %arrayidx89 = getelementptr inbounds float, float* %148, i32 %add88
  store float %147, float* %arrayidx89, align 4, !tbaa !30
  %call90 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx91 = getelementptr inbounds float, float* %call90, i32 2
  %150 = load float, float* %arrayidx91, align 4, !tbaa !30
  %151 = load float*, float** %J1, align 4, !tbaa !2
  %152 = load i32, i32* %srow, align 4, !tbaa !60
  %add92 = add nsw i32 %152, 2
  %arrayidx93 = getelementptr inbounds float, float* %151, i32 %add92
  store float %150, float* %arrayidx93, align 4, !tbaa !30
  %call94 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx95 = getelementptr inbounds float, float* %call94, i32 0
  %153 = load float, float* %arrayidx95, align 4, !tbaa !30
  %154 = load float*, float** %J1, align 4, !tbaa !2
  %155 = load i32, i32* %srow1, align 4, !tbaa !60
  %add96 = add nsw i32 %155, 0
  %arrayidx97 = getelementptr inbounds float, float* %154, i32 %add96
  store float %153, float* %arrayidx97, align 4, !tbaa !30
  %call98 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx99 = getelementptr inbounds float, float* %call98, i32 1
  %156 = load float, float* %arrayidx99, align 4, !tbaa !30
  %157 = load float*, float** %J1, align 4, !tbaa !2
  %158 = load i32, i32* %srow1, align 4, !tbaa !60
  %add100 = add nsw i32 %158, 1
  %arrayidx101 = getelementptr inbounds float, float* %157, i32 %add100
  store float %156, float* %arrayidx101, align 4, !tbaa !30
  %call102 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx103 = getelementptr inbounds float, float* %call102, i32 2
  %159 = load float, float* %arrayidx103, align 4, !tbaa !30
  %160 = load float*, float** %J1, align 4, !tbaa !2
  %161 = load i32, i32* %srow1, align 4, !tbaa !60
  %add104 = add nsw i32 %161, 2
  %arrayidx105 = getelementptr inbounds float, float* %160, i32 %add104
  store float %159, float* %arrayidx105, align 4, !tbaa !30
  %call106 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx107 = getelementptr inbounds float, float* %call106, i32 0
  %162 = load float, float* %arrayidx107, align 4, !tbaa !30
  %fneg = fneg float %162
  %163 = load float*, float** %J2, align 4, !tbaa !2
  %164 = load i32, i32* %srow, align 4, !tbaa !60
  %add108 = add nsw i32 %164, 0
  %arrayidx109 = getelementptr inbounds float, float* %163, i32 %add108
  store float %fneg, float* %arrayidx109, align 4, !tbaa !30
  %call110 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx111 = getelementptr inbounds float, float* %call110, i32 1
  %165 = load float, float* %arrayidx111, align 4, !tbaa !30
  %fneg112 = fneg float %165
  %166 = load float*, float** %J2, align 4, !tbaa !2
  %167 = load i32, i32* %srow, align 4, !tbaa !60
  %add113 = add nsw i32 %167, 1
  %arrayidx114 = getelementptr inbounds float, float* %166, i32 %add113
  store float %fneg112, float* %arrayidx114, align 4, !tbaa !30
  %call115 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx116 = getelementptr inbounds float, float* %call115, i32 2
  %168 = load float, float* %arrayidx116, align 4, !tbaa !30
  %fneg117 = fneg float %168
  %169 = load float*, float** %J2, align 4, !tbaa !2
  %170 = load i32, i32* %srow, align 4, !tbaa !60
  %add118 = add nsw i32 %170, 2
  %arrayidx119 = getelementptr inbounds float, float* %169, i32 %add118
  store float %fneg117, float* %arrayidx119, align 4, !tbaa !30
  %call120 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx121 = getelementptr inbounds float, float* %call120, i32 0
  %171 = load float, float* %arrayidx121, align 4, !tbaa !30
  %fneg122 = fneg float %171
  %172 = load float*, float** %J2, align 4, !tbaa !2
  %173 = load i32, i32* %srow1, align 4, !tbaa !60
  %add123 = add nsw i32 %173, 0
  %arrayidx124 = getelementptr inbounds float, float* %172, i32 %add123
  store float %fneg122, float* %arrayidx124, align 4, !tbaa !30
  %call125 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx126 = getelementptr inbounds float, float* %call125, i32 1
  %174 = load float, float* %arrayidx126, align 4, !tbaa !30
  %fneg127 = fneg float %174
  %175 = load float*, float** %J2, align 4, !tbaa !2
  %176 = load i32, i32* %srow1, align 4, !tbaa !60
  %add128 = add nsw i32 %176, 1
  %arrayidx129 = getelementptr inbounds float, float* %175, i32 %add128
  store float %fneg127, float* %arrayidx129, align 4, !tbaa !30
  %call130 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx131 = getelementptr inbounds float, float* %call130, i32 2
  %177 = load float, float* %arrayidx131, align 4, !tbaa !30
  %fneg132 = fneg float %177
  %178 = load float*, float** %J2, align 4, !tbaa !2
  %179 = load i32, i32* %srow1, align 4, !tbaa !60
  %add133 = add nsw i32 %179, 2
  %arrayidx134 = getelementptr inbounds float, float* %178, i32 %add133
  store float %fneg132, float* %arrayidx134, align 4, !tbaa !30
  %180 = bitcast float* %fact to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %180) #8
  %181 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %fps135 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %181, i32 0, i32 0
  %182 = load float, float* %fps135, align 4, !tbaa !59
  %m_relaxationFactor = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 6
  %183 = load float, float* %m_relaxationFactor, align 4, !tbaa !36
  %mul136 = fmul float %182, %183
  store float %mul136, float* %fact, align 4, !tbaa !30
  %184 = load float, float* %fact, align 4, !tbaa !30
  %m_swingAxis = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %call137 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_swingAxis, %class.btVector3* nonnull align 4 dereferenceable(16) %p)
  %mul138 = fmul float %184, %call137
  %185 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError139 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %185, i32 0, i32 7
  %186 = load float*, float** %m_constraintError139, align 4, !tbaa !61
  %187 = load i32, i32* %srow, align 4, !tbaa !60
  %arrayidx140 = getelementptr inbounds float, float* %186, i32 %187
  store float %mul138, float* %arrayidx140, align 4, !tbaa !30
  %188 = load float, float* %fact, align 4, !tbaa !30
  %m_swingAxis141 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %call142 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_swingAxis141, %class.btVector3* nonnull align 4 dereferenceable(16) %q)
  %mul143 = fmul float %188, %call142
  %189 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError144 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %189, i32 0, i32 7
  %190 = load float*, float** %m_constraintError144, align 4, !tbaa !61
  %191 = load i32, i32* %srow1, align 4, !tbaa !60
  %arrayidx145 = getelementptr inbounds float, float* %190, i32 %191
  store float %mul143, float* %arrayidx145, align 4, !tbaa !30
  %192 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_lowerLimit146 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %192, i32 0, i32 9
  %193 = load float*, float** %m_lowerLimit146, align 4, !tbaa !62
  %194 = load i32, i32* %srow, align 4, !tbaa !60
  %arrayidx147 = getelementptr inbounds float, float* %193, i32 %194
  store float 0xC7EFFFFFE0000000, float* %arrayidx147, align 4, !tbaa !30
  %195 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_upperLimit148 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %195, i32 0, i32 10
  %196 = load float*, float** %m_upperLimit148, align 4, !tbaa !63
  %197 = load i32, i32* %srow, align 4, !tbaa !60
  %arrayidx149 = getelementptr inbounds float, float* %196, i32 %197
  store float 0x47EFFFFFE0000000, float* %arrayidx149, align 4, !tbaa !30
  %198 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_lowerLimit150 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %198, i32 0, i32 9
  %199 = load float*, float** %m_lowerLimit150, align 4, !tbaa !62
  %200 = load i32, i32* %srow1, align 4, !tbaa !60
  %arrayidx151 = getelementptr inbounds float, float* %199, i32 %200
  store float 0xC7EFFFFFE0000000, float* %arrayidx151, align 4, !tbaa !30
  %201 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_upperLimit152 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %201, i32 0, i32 10
  %202 = load float*, float** %m_upperLimit152, align 4, !tbaa !63
  %203 = load i32, i32* %srow1, align 4, !tbaa !60
  %arrayidx153 = getelementptr inbounds float, float* %202, i32 %203
  store float 0x47EFFFFFE0000000, float* %arrayidx153, align 4, !tbaa !30
  %204 = load i32, i32* %srow1, align 4, !tbaa !60
  %205 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip154 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %205, i32 0, i32 6
  %206 = load i32, i32* %rowskip154, align 4, !tbaa !54
  %add155 = add nsw i32 %204, %206
  store i32 %add155, i32* %srow, align 4, !tbaa !60
  %207 = bitcast float* %fact to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %207) #8
  %208 = bitcast i32* %srow1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %208) #8
  %209 = bitcast %class.btVector3* %q to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %209) #8
  %210 = bitcast %class.btVector3* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %210) #8
  %211 = bitcast %class.btTransform* %trA to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %211) #8
  br label %if.end206

if.else:                                          ; preds = %land.lhs.true, %if.then70
  %212 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %212) #8
  %213 = bitcast %class.btVector3* %ref.tmp156 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %213) #8
  %m_swingAxis157 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %m_relaxationFactor158 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 6
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp156, %class.btVector3* nonnull align 4 dereferenceable(16) %m_swingAxis157, float* nonnull align 4 dereferenceable(4) %m_relaxationFactor158)
  %m_relaxationFactor159 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 6
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp156, float* nonnull align 4 dereferenceable(4) %m_relaxationFactor159)
  %214 = bitcast %class.btVector3* %ax1 to i8*
  %215 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %214, i8* align 4 %215, i32 16, i1 false), !tbaa.struct !18
  %216 = bitcast %class.btVector3* %ref.tmp156 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %216) #8
  %217 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %217) #8
  %call160 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx161 = getelementptr inbounds float, float* %call160, i32 0
  %218 = load float, float* %arrayidx161, align 4, !tbaa !30
  %219 = load float*, float** %J1, align 4, !tbaa !2
  %220 = load i32, i32* %srow, align 4, !tbaa !60
  %add162 = add nsw i32 %220, 0
  %arrayidx163 = getelementptr inbounds float, float* %219, i32 %add162
  store float %218, float* %arrayidx163, align 4, !tbaa !30
  %call164 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx165 = getelementptr inbounds float, float* %call164, i32 1
  %221 = load float, float* %arrayidx165, align 4, !tbaa !30
  %222 = load float*, float** %J1, align 4, !tbaa !2
  %223 = load i32, i32* %srow, align 4, !tbaa !60
  %add166 = add nsw i32 %223, 1
  %arrayidx167 = getelementptr inbounds float, float* %222, i32 %add166
  store float %221, float* %arrayidx167, align 4, !tbaa !30
  %call168 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx169 = getelementptr inbounds float, float* %call168, i32 2
  %224 = load float, float* %arrayidx169, align 4, !tbaa !30
  %225 = load float*, float** %J1, align 4, !tbaa !2
  %226 = load i32, i32* %srow, align 4, !tbaa !60
  %add170 = add nsw i32 %226, 2
  %arrayidx171 = getelementptr inbounds float, float* %225, i32 %add170
  store float %224, float* %arrayidx171, align 4, !tbaa !30
  %call172 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx173 = getelementptr inbounds float, float* %call172, i32 0
  %227 = load float, float* %arrayidx173, align 4, !tbaa !30
  %fneg174 = fneg float %227
  %228 = load float*, float** %J2, align 4, !tbaa !2
  %229 = load i32, i32* %srow, align 4, !tbaa !60
  %add175 = add nsw i32 %229, 0
  %arrayidx176 = getelementptr inbounds float, float* %228, i32 %add175
  store float %fneg174, float* %arrayidx176, align 4, !tbaa !30
  %call177 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx178 = getelementptr inbounds float, float* %call177, i32 1
  %230 = load float, float* %arrayidx178, align 4, !tbaa !30
  %fneg179 = fneg float %230
  %231 = load float*, float** %J2, align 4, !tbaa !2
  %232 = load i32, i32* %srow, align 4, !tbaa !60
  %add180 = add nsw i32 %232, 1
  %arrayidx181 = getelementptr inbounds float, float* %231, i32 %add180
  store float %fneg179, float* %arrayidx181, align 4, !tbaa !30
  %call182 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx183 = getelementptr inbounds float, float* %call182, i32 2
  %233 = load float, float* %arrayidx183, align 4, !tbaa !30
  %fneg184 = fneg float %233
  %234 = load float*, float** %J2, align 4, !tbaa !2
  %235 = load i32, i32* %srow, align 4, !tbaa !60
  %add185 = add nsw i32 %235, 2
  %arrayidx186 = getelementptr inbounds float, float* %234, i32 %add185
  store float %fneg184, float* %arrayidx186, align 4, !tbaa !30
  %236 = bitcast float* %k187 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %236) #8
  %237 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %fps188 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %237, i32 0, i32 0
  %238 = load float, float* %fps188, align 4, !tbaa !59
  %m_biasFactor = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 5
  %239 = load float, float* %m_biasFactor, align 4, !tbaa !35
  %mul189 = fmul float %238, %239
  store float %mul189, float* %k187, align 4, !tbaa !30
  %240 = load float, float* %k187, align 4, !tbaa !30
  %m_swingCorrection = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 17
  %241 = load float, float* %m_swingCorrection, align 4, !tbaa !44
  %mul190 = fmul float %240, %241
  %242 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError191 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %242, i32 0, i32 7
  %243 = load float*, float** %m_constraintError191, align 4, !tbaa !61
  %244 = load i32, i32* %srow, align 4, !tbaa !60
  %arrayidx192 = getelementptr inbounds float, float* %243, i32 %244
  store float %mul190, float* %arrayidx192, align 4, !tbaa !30
  %m_flags193 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 34
  %245 = load i32, i32* %m_flags193, align 4, !tbaa !26
  %and194 = and i32 %245, 4
  %tobool195 = icmp ne i32 %and194, 0
  br i1 %tobool195, label %if.then196, label %if.end199

if.then196:                                       ; preds = %if.else
  %m_angCFM = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 37
  %246 = load float, float* %m_angCFM, align 4, !tbaa !29
  %247 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %cfm197 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %247, i32 0, i32 8
  %248 = load float*, float** %cfm197, align 4, !tbaa !64
  %249 = load i32, i32* %srow, align 4, !tbaa !60
  %arrayidx198 = getelementptr inbounds float, float* %248, i32 %249
  store float %246, float* %arrayidx198, align 4, !tbaa !30
  br label %if.end199

if.end199:                                        ; preds = %if.then196, %if.else
  %250 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_lowerLimit200 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %250, i32 0, i32 9
  %251 = load float*, float** %m_lowerLimit200, align 4, !tbaa !62
  %252 = load i32, i32* %srow, align 4, !tbaa !60
  %arrayidx201 = getelementptr inbounds float, float* %251, i32 %252
  store float 0.000000e+00, float* %arrayidx201, align 4, !tbaa !30
  %253 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_upperLimit202 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %253, i32 0, i32 10
  %254 = load float*, float** %m_upperLimit202, align 4, !tbaa !63
  %255 = load i32, i32* %srow, align 4, !tbaa !60
  %arrayidx203 = getelementptr inbounds float, float* %254, i32 %255
  store float 0x47EFFFFFE0000000, float* %arrayidx203, align 4, !tbaa !30
  %256 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip204 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %256, i32 0, i32 6
  %257 = load i32, i32* %rowskip204, align 4, !tbaa !54
  %258 = load i32, i32* %srow, align 4, !tbaa !60
  %add205 = add nsw i32 %258, %257
  store i32 %add205, i32* %srow, align 4, !tbaa !60
  %259 = bitcast float* %k187 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %259) #8
  br label %if.end206

if.end206:                                        ; preds = %if.end199, %if.then76
  %260 = bitcast float** %J2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %260) #8
  %261 = bitcast float** %J1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %261) #8
  br label %if.end207

if.end207:                                        ; preds = %if.end206, %for.end
  %m_solveTwistLimit = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 23
  %262 = load i8, i8* %m_solveTwistLimit, align 1, !tbaa !20, !range !37
  %tobool208 = trunc i8 %262 to i1
  br i1 %tobool208, label %if.then209, label %if.end283

if.then209:                                       ; preds = %if.end207
  %263 = bitcast %class.btVector3* %ref.tmp210 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %263) #8
  %264 = bitcast %class.btVector3* %ref.tmp211 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %264) #8
  %m_twistAxis = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 13
  %m_relaxationFactor212 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 6
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp211, %class.btVector3* nonnull align 4 dereferenceable(16) %m_twistAxis, float* nonnull align 4 dereferenceable(4) %m_relaxationFactor212)
  %m_relaxationFactor213 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 6
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp210, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp211, float* nonnull align 4 dereferenceable(4) %m_relaxationFactor213)
  %265 = bitcast %class.btVector3* %ax1 to i8*
  %266 = bitcast %class.btVector3* %ref.tmp210 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %265, i8* align 4 %266, i32 16, i1 false), !tbaa.struct !18
  %267 = bitcast %class.btVector3* %ref.tmp211 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %267) #8
  %268 = bitcast %class.btVector3* %ref.tmp210 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %268) #8
  %269 = bitcast float** %J1214 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %269) #8
  %270 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis215 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %270, i32 0, i32 3
  %271 = load float*, float** %m_J1angularAxis215, align 4, !tbaa !55
  store float* %271, float** %J1214, align 4, !tbaa !2
  %272 = bitcast float** %J2216 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %272) #8
  %273 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis217 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %273, i32 0, i32 5
  %274 = load float*, float** %m_J2angularAxis217, align 4, !tbaa !57
  store float* %274, float** %J2216, align 4, !tbaa !2
  %call218 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx219 = getelementptr inbounds float, float* %call218, i32 0
  %275 = load float, float* %arrayidx219, align 4, !tbaa !30
  %276 = load float*, float** %J1214, align 4, !tbaa !2
  %277 = load i32, i32* %srow, align 4, !tbaa !60
  %add220 = add nsw i32 %277, 0
  %arrayidx221 = getelementptr inbounds float, float* %276, i32 %add220
  store float %275, float* %arrayidx221, align 4, !tbaa !30
  %call222 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx223 = getelementptr inbounds float, float* %call222, i32 1
  %278 = load float, float* %arrayidx223, align 4, !tbaa !30
  %279 = load float*, float** %J1214, align 4, !tbaa !2
  %280 = load i32, i32* %srow, align 4, !tbaa !60
  %add224 = add nsw i32 %280, 1
  %arrayidx225 = getelementptr inbounds float, float* %279, i32 %add224
  store float %278, float* %arrayidx225, align 4, !tbaa !30
  %call226 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx227 = getelementptr inbounds float, float* %call226, i32 2
  %281 = load float, float* %arrayidx227, align 4, !tbaa !30
  %282 = load float*, float** %J1214, align 4, !tbaa !2
  %283 = load i32, i32* %srow, align 4, !tbaa !60
  %add228 = add nsw i32 %283, 2
  %arrayidx229 = getelementptr inbounds float, float* %282, i32 %add228
  store float %281, float* %arrayidx229, align 4, !tbaa !30
  %call230 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx231 = getelementptr inbounds float, float* %call230, i32 0
  %284 = load float, float* %arrayidx231, align 4, !tbaa !30
  %fneg232 = fneg float %284
  %285 = load float*, float** %J2216, align 4, !tbaa !2
  %286 = load i32, i32* %srow, align 4, !tbaa !60
  %add233 = add nsw i32 %286, 0
  %arrayidx234 = getelementptr inbounds float, float* %285, i32 %add233
  store float %fneg232, float* %arrayidx234, align 4, !tbaa !30
  %call235 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx236 = getelementptr inbounds float, float* %call235, i32 1
  %287 = load float, float* %arrayidx236, align 4, !tbaa !30
  %fneg237 = fneg float %287
  %288 = load float*, float** %J2216, align 4, !tbaa !2
  %289 = load i32, i32* %srow, align 4, !tbaa !60
  %add238 = add nsw i32 %289, 1
  %arrayidx239 = getelementptr inbounds float, float* %288, i32 %add238
  store float %fneg237, float* %arrayidx239, align 4, !tbaa !30
  %call240 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx241 = getelementptr inbounds float, float* %call240, i32 2
  %290 = load float, float* %arrayidx241, align 4, !tbaa !30
  %fneg242 = fneg float %290
  %291 = load float*, float** %J2216, align 4, !tbaa !2
  %292 = load i32, i32* %srow, align 4, !tbaa !60
  %add243 = add nsw i32 %292, 2
  %arrayidx244 = getelementptr inbounds float, float* %291, i32 %add243
  store float %fneg242, float* %arrayidx244, align 4, !tbaa !30
  %293 = bitcast float* %k245 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %293) #8
  %294 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %fps246 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %294, i32 0, i32 0
  %295 = load float, float* %fps246, align 4, !tbaa !59
  %m_biasFactor247 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 5
  %296 = load float, float* %m_biasFactor247, align 4, !tbaa !35
  %mul248 = fmul float %295, %296
  store float %mul248, float* %k245, align 4, !tbaa !30
  %297 = load float, float* %k245, align 4, !tbaa !30
  %m_twistCorrection = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 18
  %298 = load float, float* %m_twistCorrection, align 4, !tbaa !50
  %mul249 = fmul float %297, %298
  %299 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError250 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %299, i32 0, i32 7
  %300 = load float*, float** %m_constraintError250, align 4, !tbaa !61
  %301 = load i32, i32* %srow, align 4, !tbaa !60
  %arrayidx251 = getelementptr inbounds float, float* %300, i32 %301
  store float %mul249, float* %arrayidx251, align 4, !tbaa !30
  %m_flags252 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 34
  %302 = load i32, i32* %m_flags252, align 4, !tbaa !26
  %and253 = and i32 %302, 4
  %tobool254 = icmp ne i32 %and253, 0
  br i1 %tobool254, label %if.then255, label %if.end259

if.then255:                                       ; preds = %if.then209
  %m_angCFM256 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 37
  %303 = load float, float* %m_angCFM256, align 4, !tbaa !29
  %304 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %cfm257 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %304, i32 0, i32 8
  %305 = load float*, float** %cfm257, align 4, !tbaa !64
  %306 = load i32, i32* %srow, align 4, !tbaa !60
  %arrayidx258 = getelementptr inbounds float, float* %305, i32 %306
  store float %303, float* %arrayidx258, align 4, !tbaa !30
  br label %if.end259

if.end259:                                        ; preds = %if.then255, %if.then209
  %m_twistSpan = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %307 = load float, float* %m_twistSpan, align 4, !tbaa !33
  %cmp260 = fcmp ogt float %307, 0.000000e+00
  br i1 %cmp260, label %if.then261, label %if.else275

if.then261:                                       ; preds = %if.end259
  %m_twistCorrection262 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 18
  %308 = load float, float* %m_twistCorrection262, align 4, !tbaa !50
  %cmp263 = fcmp ogt float %308, 0.000000e+00
  br i1 %cmp263, label %if.then264, label %if.else269

if.then264:                                       ; preds = %if.then261
  %309 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_lowerLimit265 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %309, i32 0, i32 9
  %310 = load float*, float** %m_lowerLimit265, align 4, !tbaa !62
  %311 = load i32, i32* %srow, align 4, !tbaa !60
  %arrayidx266 = getelementptr inbounds float, float* %310, i32 %311
  store float 0.000000e+00, float* %arrayidx266, align 4, !tbaa !30
  %312 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_upperLimit267 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %312, i32 0, i32 10
  %313 = load float*, float** %m_upperLimit267, align 4, !tbaa !63
  %314 = load i32, i32* %srow, align 4, !tbaa !60
  %arrayidx268 = getelementptr inbounds float, float* %313, i32 %314
  store float 0x47EFFFFFE0000000, float* %arrayidx268, align 4, !tbaa !30
  br label %if.end274

if.else269:                                       ; preds = %if.then261
  %315 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_lowerLimit270 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %315, i32 0, i32 9
  %316 = load float*, float** %m_lowerLimit270, align 4, !tbaa !62
  %317 = load i32, i32* %srow, align 4, !tbaa !60
  %arrayidx271 = getelementptr inbounds float, float* %316, i32 %317
  store float 0xC7EFFFFFE0000000, float* %arrayidx271, align 4, !tbaa !30
  %318 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_upperLimit272 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %318, i32 0, i32 10
  %319 = load float*, float** %m_upperLimit272, align 4, !tbaa !63
  %320 = load i32, i32* %srow, align 4, !tbaa !60
  %arrayidx273 = getelementptr inbounds float, float* %319, i32 %320
  store float 0.000000e+00, float* %arrayidx273, align 4, !tbaa !30
  br label %if.end274

if.end274:                                        ; preds = %if.else269, %if.then264
  br label %if.end280

if.else275:                                       ; preds = %if.end259
  %321 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_lowerLimit276 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %321, i32 0, i32 9
  %322 = load float*, float** %m_lowerLimit276, align 4, !tbaa !62
  %323 = load i32, i32* %srow, align 4, !tbaa !60
  %arrayidx277 = getelementptr inbounds float, float* %322, i32 %323
  store float 0xC7EFFFFFE0000000, float* %arrayidx277, align 4, !tbaa !30
  %324 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_upperLimit278 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %324, i32 0, i32 10
  %325 = load float*, float** %m_upperLimit278, align 4, !tbaa !63
  %326 = load i32, i32* %srow, align 4, !tbaa !60
  %arrayidx279 = getelementptr inbounds float, float* %325, i32 %326
  store float 0x47EFFFFFE0000000, float* %arrayidx279, align 4, !tbaa !30
  br label %if.end280

if.end280:                                        ; preds = %if.else275, %if.end274
  %327 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip281 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %327, i32 0, i32 6
  %328 = load i32, i32* %rowskip281, align 4, !tbaa !54
  %329 = load i32, i32* %srow, align 4, !tbaa !60
  %add282 = add nsw i32 %329, %328
  store i32 %add282, i32* %srow, align 4, !tbaa !60
  %330 = bitcast float* %k245 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %330) #8
  %331 = bitcast float** %J2216 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %331) #8
  %332 = bitcast float** %J1214 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %332) #8
  br label %if.end283

if.end283:                                        ; preds = %if.end280, %if.end207
  %333 = bitcast %class.btVector3* %ax1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %333) #8
  %334 = bitcast i32* %srow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %334) #8
  %335 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %335) #8
  %336 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %336) #8
  %337 = bitcast float* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %337) #8
  %338 = bitcast float* %linERP to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %338) #8
  %339 = bitcast %class.btVector3* %a2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %339) #8
  %340 = bitcast %class.btVector3* %a1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %340) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %1, i32 0)
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call1, float* %ref.tmp, align 4, !tbaa !30
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 1)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !30
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 2)
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !30
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !30
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp, align 4, !tbaa !30
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !30
  %fneg4 = fneg float %5
  store float %fneg4, float* %ref.tmp1, align 4, !tbaa !30
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %8 = load float, float* %arrayidx7, align 4, !tbaa !30
  %fneg8 = fneg float %8
  store float %fneg8, float* %ref.tmp5, align 4, !tbaa !30
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret void
}

define linkonce_odr hidden void @_ZNK9btVector322getSkewSymmetricMatrixEPS_S0_S0_(%class.btVector3* %this, %class.btVector3* %v0, %class.btVector3* %v1, %class.btVector3* %v2) #0 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !30
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this1)
  %3 = load float, float* %call, align 4, !tbaa !30
  %fneg = fneg float %3
  store float %fneg, float* %ref.tmp2, align 4, !tbaa !30
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this1)
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %0, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %call3)
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this1)
  %7 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  store float 0.000000e+00, float* %ref.tmp5, align 4, !tbaa !30
  %8 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this1)
  %9 = load float, float* %call7, align 4, !tbaa !30
  %fneg8 = fneg float %9
  store float %fneg8, float* %ref.tmp6, align 4, !tbaa !30
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %6, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %10 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  %12 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %13 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this1)
  %14 = load float, float* %call10, align 4, !tbaa !30
  %fneg11 = fneg float %14
  store float %fneg11, float* %ref.tmp9, align 4, !tbaa !30
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this1)
  %15 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  store float 0.000000e+00, float* %ref.tmp13, align 4, !tbaa !30
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %12, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %call12, float* nonnull align 4 dereferenceable(4) %ref.tmp13)
  %16 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformmlERKS_(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %1 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %1) #8
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %2 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %2, i32 0, i32 0
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %3 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #8
  %4 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %4, i32 0, i32 1
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp3, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  %5 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #8
  %6 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %6) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this, i32 %i) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !60
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx)
  %0 = load i32, i32* %i.addr, align 4, !tbaa !60
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 %0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx4)
  %1 = load i32, i32* %i.addr, align 4, !tbaa !60
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 2
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx8)
  %2 = load i32, i32* %i.addr, align 4, !tbaa !60
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %2
  %call11 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %arrayidx2, float* nonnull align 4 dereferenceable(4) %arrayidx6, float* nonnull align 4 dereferenceable(4) %arrayidx10)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !30
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !30
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !30
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !30
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !30
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !30
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !30
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !30
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !30
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !30
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !30
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !30
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !30
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !30
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !30
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

define hidden void @_ZN21btConeTwistConstraint13buildJacobianEv(%class.btConeTwistConstraint* %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %pivotAInW = alloca %class.btVector3, align 4
  %pivotBInW = alloca %class.btVector3, align 4
  %relPos = alloca %class.btVector3, align 4
  %normal = alloca [3 x %class.btVector3], align 16
  %ref.tmp14 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %i = alloca i32, align 4
  %ref.tmp24 = alloca %class.btMatrix3x3, align 4
  %ref.tmp28 = alloca %class.btMatrix3x3, align 4
  %ref.tmp32 = alloca %class.btVector3, align 4
  %ref.tmp35 = alloca %class.btVector3, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 25
  %0 = load i8, i8* %m_useSolveConstraintObsolete, align 1, !tbaa !17, !range !37
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end57

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_appliedImpulse = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %1, i32 0, i32 10
  store float 0.000000e+00, float* %m_appliedImpulse, align 4, !tbaa !65
  %m_accTwistLimitImpulse = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 21
  store float 0.000000e+00, float* %m_accTwistLimitImpulse, align 4, !tbaa !66
  %m_accSwingLimitImpulse = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 20
  store float 0.000000e+00, float* %m_accSwingLimitImpulse, align 4, !tbaa !67
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #8
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !30
  %4 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !30
  %5 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !30
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %m_accMotorImpulse = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 33
  %6 = bitcast %class.btVector3* %m_accMotorImpulse to i8*
  %7 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !18
  %8 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  %9 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %11) #8
  %m_angularOnly = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 22
  %12 = load i8, i8* %m_angularOnly, align 4, !tbaa !8, !range !37
  %tobool5 = trunc i8 %12 to i1
  br i1 %tobool5, label %if.end48, label %if.then6

if.then6:                                         ; preds = %if.then
  %13 = bitcast %class.btVector3* %pivotAInW to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #8
  %14 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %14, i32 0, i32 8
  %15 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4, !tbaa !41
  %call7 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %15)
  %m_rbAFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_rbAFrame)
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %pivotAInW, %class.btTransform* %call7, %class.btVector3* nonnull align 4 dereferenceable(16) %call8)
  %16 = bitcast %class.btVector3* %pivotBInW to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #8
  %17 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %17, i32 0, i32 9
  %18 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4, !tbaa !43
  %call9 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %18)
  %m_rbBFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  %call10 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_rbBFrame)
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %pivotBInW, %class.btTransform* %call9, %class.btVector3* nonnull align 4 dereferenceable(16) %call10)
  %19 = bitcast %class.btVector3* %relPos to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %19) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %relPos, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotBInW, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotAInW)
  %20 = bitcast [3 x %class.btVector3]* %normal to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %20) #8
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %normal, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %if.then6
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %if.then6 ], [ %arrayctor.next, %arrayctor.loop ]
  %call11 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %call12 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %relPos)
  %cmp = fcmp ogt float %call12, 0x3E80000000000000
  br i1 %cmp, label %if.then13, label %if.else

if.then13:                                        ; preds = %arrayctor.cont
  %21 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %21) #8
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %ref.tmp14, %class.btVector3* %relPos)
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %normal, i32 0, i32 0
  %22 = bitcast %class.btVector3* %arrayidx to i8*
  %23 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %22, i8* align 4 %23, i32 16, i1 false), !tbaa.struct !18
  %24 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %24) #8
  br label %if.end

if.else:                                          ; preds = %arrayctor.cont
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %normal, i32 0, i32 0
  %25 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  store float 1.000000e+00, float* %ref.tmp16, align 4, !tbaa !30
  %26 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #8
  store float 0.000000e+00, float* %ref.tmp17, align 4, !tbaa !30
  %27 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #8
  store float 0.000000e+00, float* %ref.tmp18, align 4, !tbaa !30
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx15, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp18)
  %28 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #8
  %29 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #8
  %30 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then13
  %arrayidx19 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %normal, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %normal, i32 0, i32 1
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %normal, i32 0, i32 2
  call void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx19, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx20, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx21)
  %31 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #8
  store i32 0, i32* %i, align 4, !tbaa !60
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %32 = load i32, i32* %i, align 4, !tbaa !60
  %cmp22 = icmp slt i32 %32, 3
  br i1 %cmp22, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %33 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_jac = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 1
  %34 = load i32, i32* %i, align 4, !tbaa !60
  %arrayidx23 = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jac, i32 0, i32 %34
  %35 = bitcast %class.btJacobianEntry* %arrayidx23 to i8*
  %36 = bitcast i8* %35 to %class.btJacobianEntry*
  %37 = bitcast %class.btMatrix3x3* %ref.tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %37) #8
  %38 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA25 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %38, i32 0, i32 8
  %39 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA25, align 4, !tbaa !41
  %call26 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %39)
  %call27 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call26)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp24, %class.btMatrix3x3* %call27)
  %40 = bitcast %class.btMatrix3x3* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %40) #8
  %41 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB29 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %41, i32 0, i32 9
  %42 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB29, align 4, !tbaa !43
  %call30 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %42)
  %call31 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call30)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp28, %class.btMatrix3x3* %call31)
  %43 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %43) #8
  %44 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA33 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %44, i32 0, i32 8
  %45 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA33, align 4, !tbaa !41
  %call34 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %45)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp32, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotAInW, %class.btVector3* nonnull align 4 dereferenceable(16) %call34)
  %46 = bitcast %class.btVector3* %ref.tmp35 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %46) #8
  %47 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB36 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %47, i32 0, i32 9
  %48 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB36, align 4, !tbaa !43
  %call37 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %48)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp35, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotBInW, %class.btVector3* nonnull align 4 dereferenceable(16) %call37)
  %49 = load i32, i32* %i, align 4, !tbaa !60
  %arrayidx38 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %normal, i32 0, i32 %49
  %50 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA39 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %50, i32 0, i32 8
  %51 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA39, align 4, !tbaa !41
  %call40 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %51)
  %52 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA41 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %52, i32 0, i32 8
  %53 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA41, align 4, !tbaa !41
  %call42 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %53)
  %54 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB43 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %54, i32 0, i32 9
  %55 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB43, align 4, !tbaa !43
  %call44 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %55)
  %56 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB45 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %56, i32 0, i32 9
  %57 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB45, align 4, !tbaa !43
  %call46 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %57)
  %call47 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f(%class.btJacobianEntry* %36, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp24, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp28, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp32, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp35, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx38, %class.btVector3* nonnull align 4 dereferenceable(16) %call40, float %call42, %class.btVector3* nonnull align 4 dereferenceable(16) %call44, float %call46)
  %58 = bitcast %class.btVector3* %ref.tmp35 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %58) #8
  %59 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %59) #8
  %60 = bitcast %class.btMatrix3x3* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %60) #8
  %61 = bitcast %class.btMatrix3x3* %ref.tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %61) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %62 = load i32, i32* %i, align 4, !tbaa !60
  %inc = add nsw i32 %62, 1
  store i32 %inc, i32* %i, align 4, !tbaa !60
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %63 = bitcast [3 x %class.btVector3]* %normal to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %63) #8
  %64 = bitcast %class.btVector3* %relPos to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %64) #8
  %65 = bitcast %class.btVector3* %pivotBInW to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %65) #8
  %66 = bitcast %class.btVector3* %pivotAInW to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %66) #8
  br label %if.end48

if.end48:                                         ; preds = %for.end, %if.then
  %67 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA49 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %67, i32 0, i32 8
  %68 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA49, align 4, !tbaa !41
  %call50 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %68)
  %69 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB51 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %69, i32 0, i32 9
  %70 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB51, align 4, !tbaa !43
  %call52 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %70)
  %71 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA53 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %71, i32 0, i32 8
  %72 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA53, align 4, !tbaa !41
  %call54 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %72)
  %73 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB55 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %73, i32 0, i32 9
  %74 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB55, align 4, !tbaa !43
  %call56 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %74)
  call void @_ZN21btConeTwistConstraint14calcAngleInfo2ERK11btTransformS2_RK11btMatrix3x3S5_(%class.btConeTwistConstraint* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %call50, %class.btTransform* nonnull align 4 dereferenceable(64) %call52, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call54, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call56)
  br label %if.end57

if.end57:                                         ; preds = %if.end48, %entry
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !30
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !30
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !30
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !30
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !30
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !30
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !30
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !30
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !30
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector310normalizedEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %norm = alloca %class.btVector3, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast %class.btVector3* %norm to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = bitcast %class.btVector3* %norm to i8*
  %2 = bitcast %class.btVector3* %this1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !18
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %norm)
  %3 = bitcast %class.btVector3* %agg.result to i8*
  %4 = bitcast %class.btVector3* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false), !tbaa.struct !18
  %5 = bitcast %class.btVector3* %norm to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !30
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !30
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !30
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !30
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !30
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !30
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !30
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %n, %class.btVector3* nonnull align 4 dereferenceable(16) %p, %class.btVector3* nonnull align 4 dereferenceable(16) %q) #3 comdat {
entry:
  %n.addr = alloca %class.btVector3*, align 4
  %p.addr = alloca %class.btVector3*, align 4
  %q.addr = alloca %class.btVector3*, align 4
  %a = alloca float, align 4
  %k = alloca float, align 4
  %a42 = alloca float, align 4
  %k54 = alloca float, align 4
  store %class.btVector3* %n, %class.btVector3** %n.addr, align 4, !tbaa !2
  store %class.btVector3* %p, %class.btVector3** %p.addr, align 4, !tbaa !2
  store %class.btVector3* %q, %class.btVector3** %q.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %0)
  %arrayidx = getelementptr inbounds float, float* %call, i32 2
  %1 = load float, float* %arrayidx, align 4, !tbaa !30
  %call1 = call float @_Z6btFabsf(float %1)
  %cmp = fcmp ogt float %call1, 0x3FE6A09E60000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = bitcast float* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %3)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 1
  %4 = load float, float* %arrayidx3, align 4, !tbaa !30
  %5 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call4 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %5)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 1
  %6 = load float, float* %arrayidx5, align 4, !tbaa !30
  %mul = fmul float %4, %6
  %7 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %7)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 2
  %8 = load float, float* %arrayidx7, align 4, !tbaa !30
  %9 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %9)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 2
  %10 = load float, float* %arrayidx9, align 4, !tbaa !30
  %mul10 = fmul float %8, %10
  %add = fadd float %mul, %mul10
  store float %add, float* %a, align 4, !tbaa !30
  %11 = bitcast float* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  %12 = load float, float* %a, align 4, !tbaa !30
  %call11 = call float @_Z6btSqrtf(float %12)
  %div = fdiv float 1.000000e+00, %call11
  store float %div, float* %k, align 4, !tbaa !30
  %13 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %13)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 0
  store float 0.000000e+00, float* %arrayidx13, align 4, !tbaa !30
  %14 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call14 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %14)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 2
  %15 = load float, float* %arrayidx15, align 4, !tbaa !30
  %fneg = fneg float %15
  %16 = load float, float* %k, align 4, !tbaa !30
  %mul16 = fmul float %fneg, %16
  %17 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %17)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 1
  store float %mul16, float* %arrayidx18, align 4, !tbaa !30
  %18 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call19 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %18)
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 1
  %19 = load float, float* %arrayidx20, align 4, !tbaa !30
  %20 = load float, float* %k, align 4, !tbaa !30
  %mul21 = fmul float %19, %20
  %21 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call22 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %21)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 2
  store float %mul21, float* %arrayidx23, align 4, !tbaa !30
  %22 = load float, float* %a, align 4, !tbaa !30
  %23 = load float, float* %k, align 4, !tbaa !30
  %mul24 = fmul float %22, %23
  %24 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %24)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 0
  store float %mul24, float* %arrayidx26, align 4, !tbaa !30
  %25 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call27 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %25)
  %arrayidx28 = getelementptr inbounds float, float* %call27, i32 0
  %26 = load float, float* %arrayidx28, align 4, !tbaa !30
  %fneg29 = fneg float %26
  %27 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %27)
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 2
  %28 = load float, float* %arrayidx31, align 4, !tbaa !30
  %mul32 = fmul float %fneg29, %28
  %29 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %29)
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 1
  store float %mul32, float* %arrayidx34, align 4, !tbaa !30
  %30 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call35 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %30)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 0
  %31 = load float, float* %arrayidx36, align 4, !tbaa !30
  %32 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call37 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %32)
  %arrayidx38 = getelementptr inbounds float, float* %call37, i32 1
  %33 = load float, float* %arrayidx38, align 4, !tbaa !30
  %mul39 = fmul float %31, %33
  %34 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %call40 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %34)
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 2
  store float %mul39, float* %arrayidx41, align 4, !tbaa !30
  %35 = bitcast float* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  %36 = bitcast float* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #8
  br label %if.end

if.else:                                          ; preds = %entry
  %37 = bitcast float* %a42 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #8
  %38 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call43 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %38)
  %arrayidx44 = getelementptr inbounds float, float* %call43, i32 0
  %39 = load float, float* %arrayidx44, align 4, !tbaa !30
  %40 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call45 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %40)
  %arrayidx46 = getelementptr inbounds float, float* %call45, i32 0
  %41 = load float, float* %arrayidx46, align 4, !tbaa !30
  %mul47 = fmul float %39, %41
  %42 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call48 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %42)
  %arrayidx49 = getelementptr inbounds float, float* %call48, i32 1
  %43 = load float, float* %arrayidx49, align 4, !tbaa !30
  %44 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call50 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %44)
  %arrayidx51 = getelementptr inbounds float, float* %call50, i32 1
  %45 = load float, float* %arrayidx51, align 4, !tbaa !30
  %mul52 = fmul float %43, %45
  %add53 = fadd float %mul47, %mul52
  store float %add53, float* %a42, align 4, !tbaa !30
  %46 = bitcast float* %k54 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #8
  %47 = load float, float* %a42, align 4, !tbaa !30
  %call55 = call float @_Z6btSqrtf(float %47)
  %div56 = fdiv float 1.000000e+00, %call55
  store float %div56, float* %k54, align 4, !tbaa !30
  %48 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call57 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %48)
  %arrayidx58 = getelementptr inbounds float, float* %call57, i32 1
  %49 = load float, float* %arrayidx58, align 4, !tbaa !30
  %fneg59 = fneg float %49
  %50 = load float, float* %k54, align 4, !tbaa !30
  %mul60 = fmul float %fneg59, %50
  %51 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call61 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %51)
  %arrayidx62 = getelementptr inbounds float, float* %call61, i32 0
  store float %mul60, float* %arrayidx62, align 4, !tbaa !30
  %52 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call63 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %52)
  %arrayidx64 = getelementptr inbounds float, float* %call63, i32 0
  %53 = load float, float* %arrayidx64, align 4, !tbaa !30
  %54 = load float, float* %k54, align 4, !tbaa !30
  %mul65 = fmul float %53, %54
  %55 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call66 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %55)
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 1
  store float %mul65, float* %arrayidx67, align 4, !tbaa !30
  %56 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call68 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %56)
  %arrayidx69 = getelementptr inbounds float, float* %call68, i32 2
  store float 0.000000e+00, float* %arrayidx69, align 4, !tbaa !30
  %57 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call70 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %57)
  %arrayidx71 = getelementptr inbounds float, float* %call70, i32 2
  %58 = load float, float* %arrayidx71, align 4, !tbaa !30
  %fneg72 = fneg float %58
  %59 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call73 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %59)
  %arrayidx74 = getelementptr inbounds float, float* %call73, i32 1
  %60 = load float, float* %arrayidx74, align 4, !tbaa !30
  %mul75 = fmul float %fneg72, %60
  %61 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %call76 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %61)
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 0
  store float %mul75, float* %arrayidx77, align 4, !tbaa !30
  %62 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call78 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %62)
  %arrayidx79 = getelementptr inbounds float, float* %call78, i32 2
  %63 = load float, float* %arrayidx79, align 4, !tbaa !30
  %64 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call80 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %64)
  %arrayidx81 = getelementptr inbounds float, float* %call80, i32 0
  %65 = load float, float* %arrayidx81, align 4, !tbaa !30
  %mul82 = fmul float %63, %65
  %66 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %call83 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %66)
  %arrayidx84 = getelementptr inbounds float, float* %call83, i32 1
  store float %mul82, float* %arrayidx84, align 4, !tbaa !30
  %67 = load float, float* %a42, align 4, !tbaa !30
  %68 = load float, float* %k54, align 4, !tbaa !30
  %mul85 = fmul float %67, %68
  %69 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %call86 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %69)
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 2
  store float %mul85, float* %arrayidx87, align 4, !tbaa !30
  %70 = bitcast float* %k54 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #8
  %71 = bitcast float* %a42 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %m_worldTransform)
  ret %class.btVector3* %call
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_invInertiaLocal = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 8
  ret %class.btVector3* %m_invInertiaLocal
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %0 = load float, float* %m_inverseMass, align 4, !tbaa !68
  ret float %0
}

define linkonce_odr hidden %class.btJacobianEntry* @_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f(%class.btJacobianEntry* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %world2A, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %world2B, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2, %class.btVector3* nonnull align 4 dereferenceable(16) %jointAxis, %class.btVector3* nonnull align 4 dereferenceable(16) %inertiaInvA, float %massInvA, %class.btVector3* nonnull align 4 dereferenceable(16) %inertiaInvB, float %massInvB) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btJacobianEntry*, align 4
  %world2A.addr = alloca %class.btMatrix3x3*, align 4
  %world2B.addr = alloca %class.btMatrix3x3*, align 4
  %rel_pos1.addr = alloca %class.btVector3*, align 4
  %rel_pos2.addr = alloca %class.btVector3*, align 4
  %jointAxis.addr = alloca %class.btVector3*, align 4
  %inertiaInvA.addr = alloca %class.btVector3*, align 4
  %massInvA.addr = alloca float, align 4
  %inertiaInvB.addr = alloca %class.btVector3*, align 4
  %massInvB.addr = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca %class.btVector3, align 4
  store %class.btJacobianEntry* %this, %class.btJacobianEntry** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %world2A, %class.btMatrix3x3** %world2A.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %world2B, %class.btMatrix3x3** %world2B.addr, align 4, !tbaa !2
  store %class.btVector3* %rel_pos1, %class.btVector3** %rel_pos1.addr, align 4, !tbaa !2
  store %class.btVector3* %rel_pos2, %class.btVector3** %rel_pos2.addr, align 4, !tbaa !2
  store %class.btVector3* %jointAxis, %class.btVector3** %jointAxis.addr, align 4, !tbaa !2
  store %class.btVector3* %inertiaInvA, %class.btVector3** %inertiaInvA.addr, align 4, !tbaa !2
  store float %massInvA, float* %massInvA.addr, align 4, !tbaa !30
  store %class.btVector3* %inertiaInvB, %class.btVector3** %inertiaInvB.addr, align 4, !tbaa !2
  store float %massInvB, float* %massInvB.addr, align 4, !tbaa !30
  %this1 = load %class.btJacobianEntry*, %class.btJacobianEntry** %this.addr, align 4
  %m_linearJointAxis = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %jointAxis.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVector3* %m_linearJointAxis to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !18
  %m_aJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aJ)
  %m_bJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_bJ)
  %m_0MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_0MinvJt)
  %m_1MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_1MinvJt)
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #8
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %world2A.addr, align 4, !tbaa !2
  %5 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %rel_pos1.addr, align 4, !tbaa !2
  %m_linearJointAxis6 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* %6, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearJointAxis6)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %m_aJ7 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %7 = bitcast %class.btVector3* %m_aJ7 to i8*
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !18
  %9 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #8
  %10 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %10) #8
  %11 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #8
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %world2B.addr, align 4, !tbaa !2
  %13 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #8
  %14 = load %class.btVector3*, %class.btVector3** %rel_pos2.addr, align 4, !tbaa !2
  %15 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %15) #8
  %m_linearJointAxis11 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp10, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearJointAxis11)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* %14, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp10)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp8, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %12, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9)
  %m_bJ12 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %16 = bitcast %class.btVector3* %m_bJ12 to i8*
  %17 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false), !tbaa.struct !18
  %18 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #8
  %19 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %19) #8
  %20 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %20) #8
  %21 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %21) #8
  %22 = load %class.btVector3*, %class.btVector3** %inertiaInvA.addr, align 4, !tbaa !2
  %m_aJ14 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp13, %class.btVector3* nonnull align 4 dereferenceable(16) %22, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aJ14)
  %m_0MinvJt15 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %23 = bitcast %class.btVector3* %m_0MinvJt15 to i8*
  %24 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false), !tbaa.struct !18
  %25 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %25) #8
  %26 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %26) #8
  %27 = load %class.btVector3*, %class.btVector3** %inertiaInvB.addr, align 4, !tbaa !2
  %m_bJ17 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp16, %class.btVector3* nonnull align 4 dereferenceable(16) %27, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bJ17)
  %m_1MinvJt18 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %28 = bitcast %class.btVector3* %m_1MinvJt18 to i8*
  %29 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %29, i32 16, i1 false), !tbaa.struct !18
  %30 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %30) #8
  %31 = load float, float* %massInvA.addr, align 4, !tbaa !30
  %m_0MinvJt19 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %m_aJ20 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %call21 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_0MinvJt19, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aJ20)
  %add = fadd float %31, %call21
  %32 = load float, float* %massInvB.addr, align 4, !tbaa !30
  %add22 = fadd float %add, %32
  %m_1MinvJt23 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %m_bJ24 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %call25 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_1MinvJt23, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bJ24)
  %add26 = fadd float %add22, %call25
  %m_Adiag = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 5
  store float %add26, float* %m_Adiag, align 4, !tbaa !72
  ret %class.btJacobianEntry* %this1
}

define hidden void @_ZN21btConeTwistConstraint23solveConstraintObsoleteER12btSolverBodyS1_f(%class.btConeTwistConstraint* %this, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %bodyA, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %bodyB, float %timeStep) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %bodyA.addr = alloca %struct.btSolverBody*, align 4
  %bodyB.addr = alloca %struct.btSolverBody*, align 4
  %timeStep.addr = alloca float, align 4
  %pivotAInW = alloca %class.btVector3, align 4
  %pivotBInW = alloca %class.btVector3, align 4
  %tau = alloca float, align 4
  %rel_pos1 = alloca %class.btVector3, align 4
  %rel_pos2 = alloca %class.btVector3, align 4
  %vel1 = alloca %class.btVector3, align 4
  %vel2 = alloca %class.btVector3, align 4
  %vel = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  %normal = alloca %class.btVector3*, align 4
  %jacDiagABInv = alloca float, align 4
  %rel_vel = alloca float, align 4
  %depth = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %impulse = alloca float, align 4
  %ftorqueAxis1 = alloca %class.btVector3, align 4
  %ftorqueAxis2 = alloca %class.btVector3, align 4
  %ref.tmp21 = alloca %class.btVector3, align 4
  %ref.tmp22 = alloca float, align 4
  %ref.tmp25 = alloca %class.btVector3, align 4
  %ref.tmp28 = alloca %class.btVector3, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp32 = alloca %class.btVector3, align 4
  %trACur = alloca %class.btTransform, align 4
  %trBCur = alloca %class.btTransform, align 4
  %omegaA = alloca %class.btVector3, align 4
  %omegaB = alloca %class.btVector3, align 4
  %trAPred = alloca %class.btTransform, align 4
  %zerovec = alloca %class.btVector3, align 4
  %ref.tmp47 = alloca float, align 4
  %ref.tmp48 = alloca float, align 4
  %ref.tmp49 = alloca float, align 4
  %trBPred = alloca %class.btTransform, align 4
  %trPose = alloca %class.btTransform, align 4
  %ref.tmp52 = alloca %class.btVector3, align 4
  %ref.tmp53 = alloca float, align 4
  %ref.tmp54 = alloca float, align 4
  %ref.tmp55 = alloca float, align 4
  %trABDes = alloca %class.btTransform, align 4
  %ref.tmp58 = alloca %class.btTransform, align 4
  %ref.tmp60 = alloca %class.btTransform, align 4
  %trADes = alloca %class.btTransform, align 4
  %trBDes = alloca %class.btTransform, align 4
  %ref.tmp62 = alloca %class.btTransform, align 4
  %omegaADes = alloca %class.btVector3, align 4
  %omegaBDes = alloca %class.btVector3, align 4
  %dOmegaA = alloca %class.btVector3, align 4
  %dOmegaB = alloca %class.btVector3, align 4
  %axisA = alloca %class.btVector3, align 4
  %axisB = alloca %class.btVector3, align 4
  %kAxisAInv = alloca float, align 4
  %kAxisBInv = alloca float, align 4
  %ref.tmp70 = alloca %class.btVector3, align 4
  %ref.tmp77 = alloca %class.btVector3, align 4
  %avgAxis = alloca %class.btVector3, align 4
  %ref.tmp81 = alloca %class.btVector3, align 4
  %ref.tmp82 = alloca %class.btVector3, align 4
  %kInvCombined = alloca float, align 4
  %impulse93 = alloca %class.btVector3, align 4
  %ref.tmp94 = alloca %class.btVector3, align 4
  %ref.tmp95 = alloca %class.btVector3, align 4
  %ref.tmp96 = alloca %class.btVector3, align 4
  %ref.tmp97 = alloca float, align 4
  %fMaxImpulse = alloca float, align 4
  %newUnclampedAccImpulse = alloca %class.btVector3, align 4
  %newUnclampedMag = alloca float, align 4
  %ref.tmp111 = alloca %class.btVector3, align 4
  %impulseMag = alloca float, align 4
  %impulseAxis = alloca %class.btVector3, align 4
  %ref.tmp118 = alloca %class.btVector3, align 4
  %ref.tmp119 = alloca float, align 4
  %ref.tmp120 = alloca float, align 4
  %ref.tmp121 = alloca float, align 4
  %ref.tmp123 = alloca %class.btVector3, align 4
  %ref.tmp126 = alloca %class.btVector3, align 4
  %ref.tmp127 = alloca float, align 4
  %ref.tmp128 = alloca float, align 4
  %ref.tmp129 = alloca float, align 4
  %ref.tmp131 = alloca %class.btVector3, align 4
  %angVelA = alloca %class.btVector3, align 4
  %angVelB = alloca %class.btVector3, align 4
  %relVel = alloca %class.btVector3, align 4
  %relVelAxis = alloca %class.btVector3, align 4
  %m_kDamping = alloca float, align 4
  %impulse149 = alloca %class.btVector3, align 4
  %ref.tmp150 = alloca float, align 4
  %impulseMag153 = alloca float, align 4
  %impulseAxis155 = alloca %class.btVector3, align 4
  %ref.tmp156 = alloca %class.btVector3, align 4
  %ref.tmp157 = alloca float, align 4
  %ref.tmp158 = alloca float, align 4
  %ref.tmp159 = alloca float, align 4
  %ref.tmp161 = alloca %class.btVector3, align 4
  %ref.tmp164 = alloca %class.btVector3, align 4
  %ref.tmp165 = alloca float, align 4
  %ref.tmp166 = alloca float, align 4
  %ref.tmp167 = alloca float, align 4
  %ref.tmp169 = alloca %class.btVector3, align 4
  %angVelA176 = alloca %class.btVector3, align 4
  %angVelB178 = alloca %class.btVector3, align 4
  %amplitude = alloca float, align 4
  %relSwingVel = alloca float, align 4
  %ref.tmp185 = alloca %class.btVector3, align 4
  %impulseMag194 = alloca float, align 4
  %temp = alloca float, align 4
  %ref.tmp196 = alloca float, align 4
  %ref.tmp199 = alloca float, align 4
  %impulse204 = alloca %class.btVector3, align 4
  %impulseTwistCouple = alloca %class.btVector3, align 4
  %ref.tmp206 = alloca float, align 4
  %impulseNoTwistCouple = alloca %class.btVector3, align 4
  %noTwistSwingAxis = alloca %class.btVector3, align 4
  %ref.tmp210 = alloca %class.btVector3, align 4
  %ref.tmp211 = alloca float, align 4
  %ref.tmp212 = alloca float, align 4
  %ref.tmp213 = alloca float, align 4
  %ref.tmp215 = alloca %class.btVector3, align 4
  %ref.tmp218 = alloca %class.btVector3, align 4
  %ref.tmp219 = alloca float, align 4
  %ref.tmp220 = alloca float, align 4
  %ref.tmp221 = alloca float, align 4
  %ref.tmp223 = alloca %class.btVector3, align 4
  %amplitude230 = alloca float, align 4
  %relTwistVel = alloca float, align 4
  %ref.tmp235 = alloca %class.btVector3, align 4
  %impulseMag245 = alloca float, align 4
  %temp247 = alloca float, align 4
  %ref.tmp248 = alloca float, align 4
  %ref.tmp251 = alloca float, align 4
  %ref.tmp256 = alloca %class.btVector3, align 4
  %ref.tmp257 = alloca float, align 4
  %ref.tmp258 = alloca float, align 4
  %ref.tmp259 = alloca float, align 4
  %ref.tmp261 = alloca %class.btVector3, align 4
  %ref.tmp265 = alloca %class.btVector3, align 4
  %ref.tmp266 = alloca float, align 4
  %ref.tmp267 = alloca float, align 4
  %ref.tmp268 = alloca float, align 4
  %ref.tmp270 = alloca %class.btVector3, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4, !tbaa !2
  store %struct.btSolverBody* %bodyA, %struct.btSolverBody** %bodyA.addr, align 4, !tbaa !2
  store %struct.btSolverBody* %bodyB, %struct.btSolverBody** %bodyB.addr, align 4, !tbaa !2
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !30
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 25
  %0 = load i8, i8* %m_useSolveConstraintObsolete, align 1, !tbaa !17, !range !37
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end276

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btVector3* %pivotAInW to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #8
  %2 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %2, i32 0, i32 8
  %3 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4, !tbaa !41
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %3)
  %m_rbAFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_rbAFrame)
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %pivotAInW, %class.btTransform* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call2)
  %4 = bitcast %class.btVector3* %pivotBInW to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #8
  %5 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %5, i32 0, i32 9
  %6 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4, !tbaa !43
  %call3 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %6)
  %m_rbBFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_rbBFrame)
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %pivotBInW, %class.btTransform* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call4)
  %7 = bitcast float* %tau to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  store float 0x3FD3333340000000, float* %tau, align 4, !tbaa !30
  %m_angularOnly = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 22
  %8 = load i8, i8* %m_angularOnly, align 4, !tbaa !8, !range !37
  %tobool5 = trunc i8 %8 to i1
  br i1 %tobool5, label %if.end, label %if.then6

if.then6:                                         ; preds = %if.then
  %9 = bitcast %class.btVector3* %rel_pos1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #8
  %10 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA7 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %10, i32 0, i32 8
  %11 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA7, align 4, !tbaa !41
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %11)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotAInW, %class.btVector3* nonnull align 4 dereferenceable(16) %call8)
  %12 = bitcast %class.btVector3* %rel_pos2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %12) #8
  %13 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB9 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %13, i32 0, i32 9
  %14 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB9, align 4, !tbaa !43
  %call10 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %14)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %rel_pos2, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotBInW, %class.btVector3* nonnull align 4 dereferenceable(16) %call10)
  %15 = bitcast %class.btVector3* %vel1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %15) #8
  %call11 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vel1)
  %16 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA.addr, align 4, !tbaa !2
  call void @_ZNK12btSolverBody39internalGetVelocityInLocalPointObsoleteERK9btVector3RS0_(%struct.btSolverBody* %16, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %vel1)
  %17 = bitcast %class.btVector3* %vel2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %17) #8
  %call12 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vel2)
  %18 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB.addr, align 4, !tbaa !2
  call void @_ZNK12btSolverBody39internalGetVelocityInLocalPointObsoleteERK9btVector3RS0_(%struct.btSolverBody* %18, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2, %class.btVector3* nonnull align 4 dereferenceable(16) %vel2)
  %19 = bitcast %class.btVector3* %vel to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %19) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %vel, %class.btVector3* nonnull align 4 dereferenceable(16) %vel1, %class.btVector3* nonnull align 4 dereferenceable(16) %vel2)
  %20 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #8
  store i32 0, i32* %i, align 4, !tbaa !60
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then6
  %21 = load i32, i32* %i, align 4, !tbaa !60
  %cmp = icmp slt i32 %21, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %22 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %23 = bitcast %class.btVector3** %normal to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #8
  %m_jac = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 1
  %24 = load i32, i32* %i, align 4, !tbaa !60
  %arrayidx = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jac, i32 0, i32 %24
  %m_linearJointAxis = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayidx, i32 0, i32 0
  store %class.btVector3* %m_linearJointAxis, %class.btVector3** %normal, align 4, !tbaa !2
  %25 = bitcast float* %jacDiagABInv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  %m_jac13 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 1
  %26 = load i32, i32* %i, align 4, !tbaa !60
  %arrayidx14 = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jac13, i32 0, i32 %26
  %call15 = call float @_ZNK15btJacobianEntry11getDiagonalEv(%class.btJacobianEntry* %arrayidx14)
  %div = fdiv float 1.000000e+00, %call15
  store float %div, float* %jacDiagABInv, align 4, !tbaa !30
  %27 = bitcast float* %rel_vel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #8
  %28 = load %class.btVector3*, %class.btVector3** %normal, align 4, !tbaa !2
  %call16 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %28, %class.btVector3* nonnull align 4 dereferenceable(16) %vel)
  store float %call16, float* %rel_vel, align 4, !tbaa !30
  %29 = bitcast float* %depth to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #8
  %30 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %30) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotAInW, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotBInW)
  %31 = load %class.btVector3*, %class.btVector3** %normal, align 4, !tbaa !2
  %call17 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %31)
  %fneg = fneg float %call17
  %32 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %32) #8
  store float %fneg, float* %depth, align 4, !tbaa !30
  %33 = bitcast float* %impulse to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #8
  %34 = load float, float* %depth, align 4, !tbaa !30
  %35 = load float, float* %tau, align 4, !tbaa !30
  %mul = fmul float %34, %35
  %36 = load float, float* %timeStep.addr, align 4, !tbaa !30
  %div18 = fdiv float %mul, %36
  %37 = load float, float* %jacDiagABInv, align 4, !tbaa !30
  %mul19 = fmul float %div18, %37
  %38 = load float, float* %rel_vel, align 4, !tbaa !30
  %39 = load float, float* %jacDiagABInv, align 4, !tbaa !30
  %mul20 = fmul float %38, %39
  %sub = fsub float %mul19, %mul20
  store float %sub, float* %impulse, align 4, !tbaa !30
  %40 = load float, float* %impulse, align 4, !tbaa !30
  %41 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_appliedImpulse = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %41, i32 0, i32 10
  %42 = load float, float* %m_appliedImpulse, align 4, !tbaa !65
  %add = fadd float %42, %40
  store float %add, float* %m_appliedImpulse, align 4, !tbaa !65
  %43 = bitcast %class.btVector3* %ftorqueAxis1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %43) #8
  %44 = load %class.btVector3*, %class.btVector3** %normal, align 4, !tbaa !2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ftorqueAxis1, %class.btVector3* %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %44)
  %45 = bitcast %class.btVector3* %ftorqueAxis2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %45) #8
  %46 = load %class.btVector3*, %class.btVector3** %normal, align 4, !tbaa !2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ftorqueAxis2, %class.btVector3* %rel_pos2, %class.btVector3* nonnull align 4 dereferenceable(16) %46)
  %47 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA.addr, align 4, !tbaa !2
  %48 = bitcast %class.btVector3* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %48) #8
  %49 = load %class.btVector3*, %class.btVector3** %normal, align 4, !tbaa !2
  %50 = bitcast float* %ref.tmp22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #8
  %51 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA23 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %51, i32 0, i32 8
  %52 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA23, align 4, !tbaa !41
  %call24 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %52)
  store float %call24, float* %ref.tmp22, align 4, !tbaa !30
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp21, %class.btVector3* nonnull align 4 dereferenceable(16) %49, float* nonnull align 4 dereferenceable(4) %ref.tmp22)
  %53 = bitcast %class.btVector3* %ref.tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %53) #8
  %54 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA26 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %54, i32 0, i32 8
  %55 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA26, align 4, !tbaa !41
  %call27 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %55)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp25, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call27, %class.btVector3* nonnull align 4 dereferenceable(16) %ftorqueAxis1)
  %56 = load float, float* %impulse, align 4, !tbaa !30
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %47, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp21, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp25, float %56)
  %57 = bitcast %class.btVector3* %ref.tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %57) #8
  %58 = bitcast float* %ref.tmp22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #8
  %59 = bitcast %class.btVector3* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %59) #8
  %60 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB.addr, align 4, !tbaa !2
  %61 = bitcast %class.btVector3* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %61) #8
  %62 = load %class.btVector3*, %class.btVector3** %normal, align 4, !tbaa !2
  %63 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #8
  %64 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB30 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %64, i32 0, i32 9
  %65 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB30, align 4, !tbaa !43
  %call31 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %65)
  store float %call31, float* %ref.tmp29, align 4, !tbaa !30
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp28, %class.btVector3* nonnull align 4 dereferenceable(16) %62, float* nonnull align 4 dereferenceable(4) %ref.tmp29)
  %66 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %66) #8
  %67 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB33 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %67, i32 0, i32 9
  %68 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB33, align 4, !tbaa !43
  %call34 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %68)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp32, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call34, %class.btVector3* nonnull align 4 dereferenceable(16) %ftorqueAxis2)
  %69 = load float, float* %impulse, align 4, !tbaa !30
  %fneg35 = fneg float %69
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %60, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp28, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp32, float %fneg35)
  %70 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %70) #8
  %71 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #8
  %72 = bitcast %class.btVector3* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %72) #8
  %73 = bitcast %class.btVector3* %ftorqueAxis2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %73) #8
  %74 = bitcast %class.btVector3* %ftorqueAxis1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %74) #8
  %75 = bitcast float* %impulse to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #8
  %76 = bitcast float* %depth to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #8
  %77 = bitcast float* %rel_vel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #8
  %78 = bitcast float* %jacDiagABInv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #8
  %79 = bitcast %class.btVector3** %normal to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %80 = load i32, i32* %i, align 4, !tbaa !60
  %inc = add nsw i32 %80, 1
  store i32 %inc, i32* %i, align 4, !tbaa !60
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %81 = bitcast %class.btVector3* %vel to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %81) #8
  %82 = bitcast %class.btVector3* %vel2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %82) #8
  %83 = bitcast %class.btVector3* %vel1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %83) #8
  %84 = bitcast %class.btVector3* %rel_pos2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %84) #8
  %85 = bitcast %class.btVector3* %rel_pos1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %85) #8
  br label %if.end

if.end:                                           ; preds = %for.end, %if.then
  %m_bMotorEnabled = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 29
  %86 = load i8, i8* %m_bMotorEnabled, align 4, !tbaa !22, !range !37
  %tobool36 = trunc i8 %86 to i1
  br i1 %tobool36, label %if.then37, label %if.else

if.then37:                                        ; preds = %if.end
  %87 = bitcast %class.btTransform* %trACur to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %87) #8
  %88 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA38 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %88, i32 0, i32 8
  %89 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA38, align 4, !tbaa !41
  %call39 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %89)
  %call40 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %trACur, %class.btTransform* nonnull align 4 dereferenceable(64) %call39)
  %90 = bitcast %class.btTransform* %trBCur to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %90) #8
  %91 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB41 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %91, i32 0, i32 9
  %92 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB41, align 4, !tbaa !43
  %call42 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %92)
  %call43 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %trBCur, %class.btTransform* nonnull align 4 dereferenceable(64) %call42)
  %93 = bitcast %class.btVector3* %omegaA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %93) #8
  %call44 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %omegaA)
  %94 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA.addr, align 4, !tbaa !2
  call void @_ZNK12btSolverBody26internalGetAngularVelocityER9btVector3(%struct.btSolverBody* %94, %class.btVector3* nonnull align 4 dereferenceable(16) %omegaA)
  %95 = bitcast %class.btVector3* %omegaB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %95) #8
  %call45 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %omegaB)
  %96 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB.addr, align 4, !tbaa !2
  call void @_ZNK12btSolverBody26internalGetAngularVelocityER9btVector3(%struct.btSolverBody* %96, %class.btVector3* nonnull align 4 dereferenceable(16) %omegaB)
  %97 = bitcast %class.btTransform* %trAPred to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %97) #8
  %call46 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %trAPred)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %trAPred)
  %98 = bitcast %class.btVector3* %zerovec to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %98) #8
  %99 = bitcast float* %ref.tmp47 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %99) #8
  store float 0.000000e+00, float* %ref.tmp47, align 4, !tbaa !30
  %100 = bitcast float* %ref.tmp48 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %100) #8
  store float 0.000000e+00, float* %ref.tmp48, align 4, !tbaa !30
  %101 = bitcast float* %ref.tmp49 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %101) #8
  store float 0.000000e+00, float* %ref.tmp49, align 4, !tbaa !30
  %call50 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %zerovec, float* nonnull align 4 dereferenceable(4) %ref.tmp47, float* nonnull align 4 dereferenceable(4) %ref.tmp48, float* nonnull align 4 dereferenceable(4) %ref.tmp49)
  %102 = bitcast float* %ref.tmp49 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #8
  %103 = bitcast float* %ref.tmp48 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #8
  %104 = bitcast float* %ref.tmp47 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #8
  %105 = load float, float* %timeStep.addr, align 4, !tbaa !30
  call void @_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_(%class.btTransform* nonnull align 4 dereferenceable(64) %trACur, %class.btVector3* nonnull align 4 dereferenceable(16) %zerovec, %class.btVector3* nonnull align 4 dereferenceable(16) %omegaA, float %105, %class.btTransform* nonnull align 4 dereferenceable(64) %trAPred)
  %106 = bitcast %class.btTransform* %trBPred to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %106) #8
  %call51 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %trBPred)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %trBPred)
  %107 = load float, float* %timeStep.addr, align 4, !tbaa !30
  call void @_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_(%class.btTransform* nonnull align 4 dereferenceable(64) %trBCur, %class.btVector3* nonnull align 4 dereferenceable(16) %zerovec, %class.btVector3* nonnull align 4 dereferenceable(16) %omegaB, float %107, %class.btTransform* nonnull align 4 dereferenceable(64) %trBPred)
  %108 = bitcast %class.btTransform* %trPose to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %108) #8
  %m_qTarget = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 31
  %109 = bitcast %class.btVector3* %ref.tmp52 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %109) #8
  %110 = bitcast float* %ref.tmp53 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %110) #8
  store float 0.000000e+00, float* %ref.tmp53, align 4, !tbaa !30
  %111 = bitcast float* %ref.tmp54 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %111) #8
  store float 0.000000e+00, float* %ref.tmp54, align 4, !tbaa !30
  %112 = bitcast float* %ref.tmp55 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %112) #8
  store float 0.000000e+00, float* %ref.tmp55, align 4, !tbaa !30
  %call56 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp52, float* nonnull align 4 dereferenceable(4) %ref.tmp53, float* nonnull align 4 dereferenceable(4) %ref.tmp54, float* nonnull align 4 dereferenceable(4) %ref.tmp55)
  %call57 = call %class.btTransform* @_ZN11btTransformC2ERK12btQuaternionRK9btVector3(%class.btTransform* %trPose, %class.btQuaternion* nonnull align 4 dereferenceable(16) %m_qTarget, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp52)
  %113 = bitcast float* %ref.tmp55 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #8
  %114 = bitcast float* %ref.tmp54 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #8
  %115 = bitcast float* %ref.tmp53 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #8
  %116 = bitcast %class.btVector3* %ref.tmp52 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %116) #8
  %117 = bitcast %class.btTransform* %trABDes to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %117) #8
  %118 = bitcast %class.btTransform* %ref.tmp58 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %118) #8
  %m_rbBFrame59 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp58, %class.btTransform* %m_rbBFrame59, %class.btTransform* nonnull align 4 dereferenceable(64) %trPose)
  %119 = bitcast %class.btTransform* %ref.tmp60 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %119) #8
  %m_rbAFrame61 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  call void @_ZNK11btTransform7inverseEv(%class.btTransform* sret align 4 %ref.tmp60, %class.btTransform* %m_rbAFrame61)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %trABDes, %class.btTransform* %ref.tmp58, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp60)
  %120 = bitcast %class.btTransform* %ref.tmp60 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %120) #8
  %121 = bitcast %class.btTransform* %ref.tmp58 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %121) #8
  %122 = bitcast %class.btTransform* %trADes to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %122) #8
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %trADes, %class.btTransform* %trBPred, %class.btTransform* nonnull align 4 dereferenceable(64) %trABDes)
  %123 = bitcast %class.btTransform* %trBDes to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %123) #8
  %124 = bitcast %class.btTransform* %ref.tmp62 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %124) #8
  call void @_ZNK11btTransform7inverseEv(%class.btTransform* sret align 4 %ref.tmp62, %class.btTransform* %trABDes)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %trBDes, %class.btTransform* %trAPred, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp62)
  %125 = bitcast %class.btTransform* %ref.tmp62 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %125) #8
  %126 = bitcast %class.btVector3* %omegaADes to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %126) #8
  %call63 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %omegaADes)
  %127 = bitcast %class.btVector3* %omegaBDes to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %127) #8
  %call64 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %omegaBDes)
  %128 = load float, float* %timeStep.addr, align 4, !tbaa !30
  call void @_ZN15btTransformUtil17calculateVelocityERK11btTransformS2_fR9btVector3S4_(%class.btTransform* nonnull align 4 dereferenceable(64) %trACur, %class.btTransform* nonnull align 4 dereferenceable(64) %trADes, float %128, %class.btVector3* nonnull align 4 dereferenceable(16) %zerovec, %class.btVector3* nonnull align 4 dereferenceable(16) %omegaADes)
  %129 = load float, float* %timeStep.addr, align 4, !tbaa !30
  call void @_ZN15btTransformUtil17calculateVelocityERK11btTransformS2_fR9btVector3S4_(%class.btTransform* nonnull align 4 dereferenceable(64) %trBCur, %class.btTransform* nonnull align 4 dereferenceable(64) %trBDes, float %129, %class.btVector3* nonnull align 4 dereferenceable(16) %zerovec, %class.btVector3* nonnull align 4 dereferenceable(16) %omegaBDes)
  %130 = bitcast %class.btVector3* %dOmegaA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %130) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %dOmegaA, %class.btVector3* nonnull align 4 dereferenceable(16) %omegaADes, %class.btVector3* nonnull align 4 dereferenceable(16) %omegaA)
  %131 = bitcast %class.btVector3* %dOmegaB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %131) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %dOmegaB, %class.btVector3* nonnull align 4 dereferenceable(16) %omegaBDes, %class.btVector3* nonnull align 4 dereferenceable(16) %omegaB)
  %132 = bitcast %class.btVector3* %axisA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %132) #8
  %call65 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %axisA)
  %133 = bitcast %class.btVector3* %axisB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %133) #8
  %call66 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %axisB)
  %134 = bitcast float* %kAxisAInv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %134) #8
  store float 0.000000e+00, float* %kAxisAInv, align 4, !tbaa !30
  %135 = bitcast float* %kAxisBInv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %135) #8
  store float 0.000000e+00, float* %kAxisBInv, align 4, !tbaa !30
  %call67 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %dOmegaA)
  %cmp68 = fcmp ogt float %call67, 0x3E80000000000000
  br i1 %cmp68, label %if.then69, label %if.end73

if.then69:                                        ; preds = %if.then37
  %136 = bitcast %class.btVector3* %ref.tmp70 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %136) #8
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %ref.tmp70, %class.btVector3* %dOmegaA)
  %137 = bitcast %class.btVector3* %axisA to i8*
  %138 = bitcast %class.btVector3* %ref.tmp70 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %137, i8* align 4 %138, i32 16, i1 false), !tbaa.struct !18
  %139 = bitcast %class.btVector3* %ref.tmp70 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %139) #8
  %call71 = call nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyAEv(%class.btConeTwistConstraint* %this1)
  %call72 = call float @_ZNK11btRigidBody32computeAngularImpulseDenominatorERK9btVector3(%class.btRigidBody* %call71, %class.btVector3* nonnull align 4 dereferenceable(16) %axisA)
  store float %call72, float* %kAxisAInv, align 4, !tbaa !30
  br label %if.end73

if.end73:                                         ; preds = %if.then69, %if.then37
  %call74 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %dOmegaB)
  %cmp75 = fcmp ogt float %call74, 0x3E80000000000000
  br i1 %cmp75, label %if.then76, label %if.end80

if.then76:                                        ; preds = %if.end73
  %140 = bitcast %class.btVector3* %ref.tmp77 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %140) #8
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %ref.tmp77, %class.btVector3* %dOmegaB)
  %141 = bitcast %class.btVector3* %axisB to i8*
  %142 = bitcast %class.btVector3* %ref.tmp77 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %141, i8* align 4 %142, i32 16, i1 false), !tbaa.struct !18
  %143 = bitcast %class.btVector3* %ref.tmp77 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %143) #8
  %call78 = call nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyBEv(%class.btConeTwistConstraint* %this1)
  %call79 = call float @_ZNK11btRigidBody32computeAngularImpulseDenominatorERK9btVector3(%class.btRigidBody* %call78, %class.btVector3* nonnull align 4 dereferenceable(16) %axisB)
  store float %call79, float* %kAxisBInv, align 4, !tbaa !30
  br label %if.end80

if.end80:                                         ; preds = %if.then76, %if.end73
  %144 = bitcast %class.btVector3* %avgAxis to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %144) #8
  %145 = bitcast %class.btVector3* %ref.tmp81 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %145) #8
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp81, float* nonnull align 4 dereferenceable(4) %kAxisAInv, %class.btVector3* nonnull align 4 dereferenceable(16) %axisA)
  %146 = bitcast %class.btVector3* %ref.tmp82 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %146) #8
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp82, float* nonnull align 4 dereferenceable(4) %kAxisBInv, %class.btVector3* nonnull align 4 dereferenceable(16) %axisB)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %avgAxis, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp81, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp82)
  %147 = bitcast %class.btVector3* %ref.tmp82 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %147) #8
  %148 = bitcast %class.btVector3* %ref.tmp81 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %148) #8
  %149 = load i8, i8* @_ZZN21btConeTwistConstraint23solveConstraintObsoleteER12btSolverBodyS1_fE9bDoTorque, align 1, !tbaa !74, !range !37
  %tobool83 = trunc i8 %149 to i1
  br i1 %tobool83, label %land.lhs.true, label %if.end135

land.lhs.true:                                    ; preds = %if.end80
  %call84 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %avgAxis)
  %cmp85 = fcmp ogt float %call84, 0x3E80000000000000
  br i1 %cmp85, label %if.then86, label %if.end135

if.then86:                                        ; preds = %land.lhs.true
  %call87 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %avgAxis)
  %call88 = call nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyAEv(%class.btConeTwistConstraint* %this1)
  %call89 = call float @_ZNK11btRigidBody32computeAngularImpulseDenominatorERK9btVector3(%class.btRigidBody* %call88, %class.btVector3* nonnull align 4 dereferenceable(16) %avgAxis)
  store float %call89, float* %kAxisAInv, align 4, !tbaa !30
  %call90 = call nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyBEv(%class.btConeTwistConstraint* %this1)
  %call91 = call float @_ZNK11btRigidBody32computeAngularImpulseDenominatorERK9btVector3(%class.btRigidBody* %call90, %class.btVector3* nonnull align 4 dereferenceable(16) %avgAxis)
  store float %call91, float* %kAxisBInv, align 4, !tbaa !30
  %150 = bitcast float* %kInvCombined to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %150) #8
  %151 = load float, float* %kAxisAInv, align 4, !tbaa !30
  %152 = load float, float* %kAxisBInv, align 4, !tbaa !30
  %add92 = fadd float %151, %152
  store float %add92, float* %kInvCombined, align 4, !tbaa !30
  %153 = bitcast %class.btVector3* %impulse93 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %153) #8
  %154 = bitcast %class.btVector3* %ref.tmp94 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %154) #8
  %155 = bitcast %class.btVector3* %ref.tmp95 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %155) #8
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp95, float* nonnull align 4 dereferenceable(4) %kAxisAInv, %class.btVector3* nonnull align 4 dereferenceable(16) %dOmegaA)
  %156 = bitcast %class.btVector3* %ref.tmp96 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %156) #8
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp96, float* nonnull align 4 dereferenceable(4) %kAxisBInv, %class.btVector3* nonnull align 4 dereferenceable(16) %dOmegaB)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp94, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp95, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp96)
  %157 = bitcast float* %ref.tmp97 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %157) #8
  %158 = load float, float* %kInvCombined, align 4, !tbaa !30
  %159 = load float, float* %kInvCombined, align 4, !tbaa !30
  %mul98 = fmul float %158, %159
  store float %mul98, float* %ref.tmp97, align 4, !tbaa !30
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %impulse93, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp94, float* nonnull align 4 dereferenceable(4) %ref.tmp97)
  %160 = bitcast float* %ref.tmp97 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %160) #8
  %161 = bitcast %class.btVector3* %ref.tmp96 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %161) #8
  %162 = bitcast %class.btVector3* %ref.tmp95 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %162) #8
  %163 = bitcast %class.btVector3* %ref.tmp94 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %163) #8
  %m_maxMotorImpulse = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 32
  %164 = load float, float* %m_maxMotorImpulse, align 4, !tbaa !23
  %cmp99 = fcmp oge float %164, 0.000000e+00
  br i1 %cmp99, label %if.then100, label %if.end116

if.then100:                                       ; preds = %if.then86
  %165 = bitcast float* %fMaxImpulse to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %165) #8
  %m_maxMotorImpulse101 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 32
  %166 = load float, float* %m_maxMotorImpulse101, align 4, !tbaa !23
  store float %166, float* %fMaxImpulse, align 4, !tbaa !30
  %m_bNormalizedMotorStrength = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 30
  %167 = load i8, i8* %m_bNormalizedMotorStrength, align 1, !tbaa !75, !range !37
  %tobool102 = trunc i8 %167 to i1
  br i1 %tobool102, label %if.then103, label %if.end105

if.then103:                                       ; preds = %if.then100
  %168 = load float, float* %fMaxImpulse, align 4, !tbaa !30
  %169 = load float, float* %kAxisAInv, align 4, !tbaa !30
  %div104 = fdiv float %168, %169
  store float %div104, float* %fMaxImpulse, align 4, !tbaa !30
  br label %if.end105

if.end105:                                        ; preds = %if.then103, %if.then100
  %170 = bitcast %class.btVector3* %newUnclampedAccImpulse to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %170) #8
  %m_accMotorImpulse = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 33
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %newUnclampedAccImpulse, %class.btVector3* nonnull align 4 dereferenceable(16) %m_accMotorImpulse, %class.btVector3* nonnull align 4 dereferenceable(16) %impulse93)
  %171 = bitcast float* %newUnclampedMag to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %171) #8
  %call106 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %newUnclampedAccImpulse)
  store float %call106, float* %newUnclampedMag, align 4, !tbaa !30
  %172 = load float, float* %newUnclampedMag, align 4, !tbaa !30
  %173 = load float, float* %fMaxImpulse, align 4, !tbaa !30
  %cmp107 = fcmp ogt float %172, %173
  br i1 %cmp107, label %if.then108, label %if.end113

if.then108:                                       ; preds = %if.end105
  %call109 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %newUnclampedAccImpulse)
  %call110 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %newUnclampedAccImpulse, float* nonnull align 4 dereferenceable(4) %fMaxImpulse)
  %174 = bitcast %class.btVector3* %ref.tmp111 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %174) #8
  %m_accMotorImpulse112 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 33
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp111, %class.btVector3* nonnull align 4 dereferenceable(16) %newUnclampedAccImpulse, %class.btVector3* nonnull align 4 dereferenceable(16) %m_accMotorImpulse112)
  %175 = bitcast %class.btVector3* %impulse93 to i8*
  %176 = bitcast %class.btVector3* %ref.tmp111 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %175, i8* align 4 %176, i32 16, i1 false), !tbaa.struct !18
  %177 = bitcast %class.btVector3* %ref.tmp111 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %177) #8
  br label %if.end113

if.end113:                                        ; preds = %if.then108, %if.end105
  %m_accMotorImpulse114 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 33
  %call115 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_accMotorImpulse114, %class.btVector3* nonnull align 4 dereferenceable(16) %impulse93)
  %178 = bitcast float* %newUnclampedMag to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %178) #8
  %179 = bitcast %class.btVector3* %newUnclampedAccImpulse to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %179) #8
  %180 = bitcast float* %fMaxImpulse to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %180) #8
  br label %if.end116

if.end116:                                        ; preds = %if.end113, %if.then86
  %181 = bitcast float* %impulseMag to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %181) #8
  %call117 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %impulse93)
  store float %call117, float* %impulseMag, align 4, !tbaa !30
  %182 = bitcast %class.btVector3* %impulseAxis to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %182) #8
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %impulseAxis, %class.btVector3* nonnull align 4 dereferenceable(16) %impulse93, float* nonnull align 4 dereferenceable(4) %impulseMag)
  %183 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA.addr, align 4, !tbaa !2
  %184 = bitcast %class.btVector3* %ref.tmp118 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %184) #8
  %185 = bitcast float* %ref.tmp119 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %185) #8
  store float 0.000000e+00, float* %ref.tmp119, align 4, !tbaa !30
  %186 = bitcast float* %ref.tmp120 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %186) #8
  store float 0.000000e+00, float* %ref.tmp120, align 4, !tbaa !30
  %187 = bitcast float* %ref.tmp121 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %187) #8
  store float 0.000000e+00, float* %ref.tmp121, align 4, !tbaa !30
  %call122 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp118, float* nonnull align 4 dereferenceable(4) %ref.tmp119, float* nonnull align 4 dereferenceable(4) %ref.tmp120, float* nonnull align 4 dereferenceable(4) %ref.tmp121)
  %188 = bitcast %class.btVector3* %ref.tmp123 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %188) #8
  %189 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA124 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %189, i32 0, i32 8
  %190 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA124, align 4, !tbaa !41
  %call125 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %190)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp123, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call125, %class.btVector3* nonnull align 4 dereferenceable(16) %impulseAxis)
  %191 = load float, float* %impulseMag, align 4, !tbaa !30
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %183, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp118, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp123, float %191)
  %192 = bitcast %class.btVector3* %ref.tmp123 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %192) #8
  %193 = bitcast float* %ref.tmp121 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %193) #8
  %194 = bitcast float* %ref.tmp120 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %194) #8
  %195 = bitcast float* %ref.tmp119 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %195) #8
  %196 = bitcast %class.btVector3* %ref.tmp118 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %196) #8
  %197 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB.addr, align 4, !tbaa !2
  %198 = bitcast %class.btVector3* %ref.tmp126 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %198) #8
  %199 = bitcast float* %ref.tmp127 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %199) #8
  store float 0.000000e+00, float* %ref.tmp127, align 4, !tbaa !30
  %200 = bitcast float* %ref.tmp128 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %200) #8
  store float 0.000000e+00, float* %ref.tmp128, align 4, !tbaa !30
  %201 = bitcast float* %ref.tmp129 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %201) #8
  store float 0.000000e+00, float* %ref.tmp129, align 4, !tbaa !30
  %call130 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp126, float* nonnull align 4 dereferenceable(4) %ref.tmp127, float* nonnull align 4 dereferenceable(4) %ref.tmp128, float* nonnull align 4 dereferenceable(4) %ref.tmp129)
  %202 = bitcast %class.btVector3* %ref.tmp131 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %202) #8
  %203 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB132 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %203, i32 0, i32 9
  %204 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB132, align 4, !tbaa !43
  %call133 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %204)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp131, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call133, %class.btVector3* nonnull align 4 dereferenceable(16) %impulseAxis)
  %205 = load float, float* %impulseMag, align 4, !tbaa !30
  %fneg134 = fneg float %205
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %197, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp126, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp131, float %fneg134)
  %206 = bitcast %class.btVector3* %ref.tmp131 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %206) #8
  %207 = bitcast float* %ref.tmp129 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %207) #8
  %208 = bitcast float* %ref.tmp128 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %208) #8
  %209 = bitcast float* %ref.tmp127 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %209) #8
  %210 = bitcast %class.btVector3* %ref.tmp126 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %210) #8
  %211 = bitcast %class.btVector3* %impulseAxis to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %211) #8
  %212 = bitcast float* %impulseMag to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %212) #8
  %213 = bitcast %class.btVector3* %impulse93 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %213) #8
  %214 = bitcast float* %kInvCombined to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %214) #8
  br label %if.end135

if.end135:                                        ; preds = %if.end116, %land.lhs.true, %if.end80
  %215 = bitcast %class.btVector3* %avgAxis to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %215) #8
  %216 = bitcast float* %kAxisBInv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %216) #8
  %217 = bitcast float* %kAxisAInv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %217) #8
  %218 = bitcast %class.btVector3* %axisB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %218) #8
  %219 = bitcast %class.btVector3* %axisA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %219) #8
  %220 = bitcast %class.btVector3* %dOmegaB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %220) #8
  %221 = bitcast %class.btVector3* %dOmegaA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %221) #8
  %222 = bitcast %class.btVector3* %omegaBDes to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %222) #8
  %223 = bitcast %class.btVector3* %omegaADes to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %223) #8
  %224 = bitcast %class.btTransform* %trBDes to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %224) #8
  %225 = bitcast %class.btTransform* %trADes to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %225) #8
  %226 = bitcast %class.btTransform* %trABDes to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %226) #8
  %227 = bitcast %class.btTransform* %trPose to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %227) #8
  %228 = bitcast %class.btTransform* %trBPred to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %228) #8
  %229 = bitcast %class.btVector3* %zerovec to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %229) #8
  %230 = bitcast %class.btTransform* %trAPred to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %230) #8
  %231 = bitcast %class.btVector3* %omegaB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %231) #8
  %232 = bitcast %class.btVector3* %omegaA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %232) #8
  %233 = bitcast %class.btTransform* %trBCur to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %233) #8
  %234 = bitcast %class.btTransform* %trACur to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %234) #8
  br label %if.end175

if.else:                                          ; preds = %if.end
  %m_damping = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 7
  %235 = load float, float* %m_damping, align 4, !tbaa !24
  %cmp136 = fcmp ogt float %235, 0x3E80000000000000
  br i1 %cmp136, label %if.then137, label %if.end174

if.then137:                                       ; preds = %if.else
  %236 = bitcast %class.btVector3* %angVelA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %236) #8
  %call138 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %angVelA)
  %237 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA.addr, align 4, !tbaa !2
  call void @_ZNK12btSolverBody26internalGetAngularVelocityER9btVector3(%struct.btSolverBody* %237, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelA)
  %238 = bitcast %class.btVector3* %angVelB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %238) #8
  %call139 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %angVelB)
  %239 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB.addr, align 4, !tbaa !2
  call void @_ZNK12btSolverBody26internalGetAngularVelocityER9btVector3(%struct.btSolverBody* %239, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelB)
  %240 = bitcast %class.btVector3* %relVel to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %240) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %relVel, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelB, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelA)
  %call140 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %relVel)
  %cmp141 = fcmp ogt float %call140, 0x3E80000000000000
  br i1 %cmp141, label %if.then142, label %if.end173

if.then142:                                       ; preds = %if.then137
  %241 = bitcast %class.btVector3* %relVelAxis to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %241) #8
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %relVelAxis, %class.btVector3* %relVel)
  %242 = bitcast float* %m_kDamping to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %242) #8
  %call143 = call nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyAEv(%class.btConeTwistConstraint* %this1)
  %call144 = call float @_ZNK11btRigidBody32computeAngularImpulseDenominatorERK9btVector3(%class.btRigidBody* %call143, %class.btVector3* nonnull align 4 dereferenceable(16) %relVelAxis)
  %call145 = call nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyBEv(%class.btConeTwistConstraint* %this1)
  %call146 = call float @_ZNK11btRigidBody32computeAngularImpulseDenominatorERK9btVector3(%class.btRigidBody* %call145, %class.btVector3* nonnull align 4 dereferenceable(16) %relVelAxis)
  %add147 = fadd float %call144, %call146
  %div148 = fdiv float 1.000000e+00, %add147
  store float %div148, float* %m_kDamping, align 4, !tbaa !30
  %243 = bitcast %class.btVector3* %impulse149 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %243) #8
  %244 = bitcast float* %ref.tmp150 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %244) #8
  %m_damping151 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 7
  %245 = load float, float* %m_damping151, align 4, !tbaa !24
  %246 = load float, float* %m_kDamping, align 4, !tbaa !30
  %mul152 = fmul float %245, %246
  store float %mul152, float* %ref.tmp150, align 4, !tbaa !30
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %impulse149, float* nonnull align 4 dereferenceable(4) %ref.tmp150, %class.btVector3* nonnull align 4 dereferenceable(16) %relVel)
  %247 = bitcast float* %ref.tmp150 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %247) #8
  %248 = bitcast float* %impulseMag153 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %248) #8
  %call154 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %impulse149)
  store float %call154, float* %impulseMag153, align 4, !tbaa !30
  %249 = bitcast %class.btVector3* %impulseAxis155 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %249) #8
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %impulseAxis155, %class.btVector3* nonnull align 4 dereferenceable(16) %impulse149, float* nonnull align 4 dereferenceable(4) %impulseMag153)
  %250 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA.addr, align 4, !tbaa !2
  %251 = bitcast %class.btVector3* %ref.tmp156 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %251) #8
  %252 = bitcast float* %ref.tmp157 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %252) #8
  store float 0.000000e+00, float* %ref.tmp157, align 4, !tbaa !30
  %253 = bitcast float* %ref.tmp158 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %253) #8
  store float 0.000000e+00, float* %ref.tmp158, align 4, !tbaa !30
  %254 = bitcast float* %ref.tmp159 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %254) #8
  store float 0.000000e+00, float* %ref.tmp159, align 4, !tbaa !30
  %call160 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp156, float* nonnull align 4 dereferenceable(4) %ref.tmp157, float* nonnull align 4 dereferenceable(4) %ref.tmp158, float* nonnull align 4 dereferenceable(4) %ref.tmp159)
  %255 = bitcast %class.btVector3* %ref.tmp161 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %255) #8
  %256 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA162 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %256, i32 0, i32 8
  %257 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA162, align 4, !tbaa !41
  %call163 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %257)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp161, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call163, %class.btVector3* nonnull align 4 dereferenceable(16) %impulseAxis155)
  %258 = load float, float* %impulseMag153, align 4, !tbaa !30
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %250, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp156, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp161, float %258)
  %259 = bitcast %class.btVector3* %ref.tmp161 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %259) #8
  %260 = bitcast float* %ref.tmp159 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %260) #8
  %261 = bitcast float* %ref.tmp158 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %261) #8
  %262 = bitcast float* %ref.tmp157 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %262) #8
  %263 = bitcast %class.btVector3* %ref.tmp156 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %263) #8
  %264 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB.addr, align 4, !tbaa !2
  %265 = bitcast %class.btVector3* %ref.tmp164 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %265) #8
  %266 = bitcast float* %ref.tmp165 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %266) #8
  store float 0.000000e+00, float* %ref.tmp165, align 4, !tbaa !30
  %267 = bitcast float* %ref.tmp166 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %267) #8
  store float 0.000000e+00, float* %ref.tmp166, align 4, !tbaa !30
  %268 = bitcast float* %ref.tmp167 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %268) #8
  store float 0.000000e+00, float* %ref.tmp167, align 4, !tbaa !30
  %call168 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp164, float* nonnull align 4 dereferenceable(4) %ref.tmp165, float* nonnull align 4 dereferenceable(4) %ref.tmp166, float* nonnull align 4 dereferenceable(4) %ref.tmp167)
  %269 = bitcast %class.btVector3* %ref.tmp169 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %269) #8
  %270 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB170 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %270, i32 0, i32 9
  %271 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB170, align 4, !tbaa !43
  %call171 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %271)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp169, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call171, %class.btVector3* nonnull align 4 dereferenceable(16) %impulseAxis155)
  %272 = load float, float* %impulseMag153, align 4, !tbaa !30
  %fneg172 = fneg float %272
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %264, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp164, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp169, float %fneg172)
  %273 = bitcast %class.btVector3* %ref.tmp169 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %273) #8
  %274 = bitcast float* %ref.tmp167 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %274) #8
  %275 = bitcast float* %ref.tmp166 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %275) #8
  %276 = bitcast float* %ref.tmp165 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %276) #8
  %277 = bitcast %class.btVector3* %ref.tmp164 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %277) #8
  %278 = bitcast %class.btVector3* %impulseAxis155 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %278) #8
  %279 = bitcast float* %impulseMag153 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %279) #8
  %280 = bitcast %class.btVector3* %impulse149 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %280) #8
  %281 = bitcast float* %m_kDamping to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %281) #8
  %282 = bitcast %class.btVector3* %relVelAxis to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %282) #8
  br label %if.end173

if.end173:                                        ; preds = %if.then142, %if.then137
  %283 = bitcast %class.btVector3* %relVel to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %283) #8
  %284 = bitcast %class.btVector3* %angVelB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %284) #8
  %285 = bitcast %class.btVector3* %angVelA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %285) #8
  br label %if.end174

if.end174:                                        ; preds = %if.end173, %if.else
  br label %if.end175

if.end175:                                        ; preds = %if.end174, %if.end135
  %286 = bitcast %class.btVector3* %angVelA176 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %286) #8
  %call177 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %angVelA176)
  %287 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA.addr, align 4, !tbaa !2
  call void @_ZNK12btSolverBody26internalGetAngularVelocityER9btVector3(%struct.btSolverBody* %287, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelA176)
  %288 = bitcast %class.btVector3* %angVelB178 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %288) #8
  %call179 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %angVelB178)
  %289 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB.addr, align 4, !tbaa !2
  call void @_ZNK12btSolverBody26internalGetAngularVelocityER9btVector3(%struct.btSolverBody* %289, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelB178)
  %m_solveSwingLimit = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 24
  %290 = load i8, i8* %m_solveSwingLimit, align 2, !tbaa !21, !range !37
  %tobool180 = trunc i8 %290 to i1
  br i1 %tobool180, label %if.then181, label %if.end227

if.then181:                                       ; preds = %if.end175
  %291 = bitcast float* %amplitude to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %291) #8
  %m_swingLimitRatio = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 26
  %292 = load float, float* %m_swingLimitRatio, align 4, !tbaa !46
  %m_swingCorrection = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 17
  %293 = load float, float* %m_swingCorrection, align 4, !tbaa !44
  %mul182 = fmul float %292, %293
  %m_biasFactor = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 5
  %294 = load float, float* %m_biasFactor, align 4, !tbaa !35
  %mul183 = fmul float %mul182, %294
  %295 = load float, float* %timeStep.addr, align 4, !tbaa !30
  %div184 = fdiv float %mul183, %295
  store float %div184, float* %amplitude, align 4, !tbaa !30
  %296 = bitcast float* %relSwingVel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %296) #8
  %297 = bitcast %class.btVector3* %ref.tmp185 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %297) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp185, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelB178, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelA176)
  %m_swingAxis = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %call186 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp185, %class.btVector3* nonnull align 4 dereferenceable(16) %m_swingAxis)
  %298 = bitcast %class.btVector3* %ref.tmp185 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %298) #8
  store float %call186, float* %relSwingVel, align 4, !tbaa !30
  %299 = load float, float* %relSwingVel, align 4, !tbaa !30
  %cmp187 = fcmp ogt float %299, 0.000000e+00
  br i1 %cmp187, label %if.then188, label %if.end193

if.then188:                                       ; preds = %if.then181
  %m_swingLimitRatio189 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 26
  %300 = load float, float* %m_swingLimitRatio189, align 4, !tbaa !46
  %301 = load float, float* %relSwingVel, align 4, !tbaa !30
  %mul190 = fmul float %300, %301
  %m_relaxationFactor = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 6
  %302 = load float, float* %m_relaxationFactor, align 4, !tbaa !36
  %mul191 = fmul float %mul190, %302
  %303 = load float, float* %amplitude, align 4, !tbaa !30
  %add192 = fadd float %303, %mul191
  store float %add192, float* %amplitude, align 4, !tbaa !30
  br label %if.end193

if.end193:                                        ; preds = %if.then188, %if.then181
  %304 = bitcast float* %impulseMag194 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %304) #8
  %305 = load float, float* %amplitude, align 4, !tbaa !30
  %m_kSwing = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 14
  %306 = load float, float* %m_kSwing, align 4, !tbaa !47
  %mul195 = fmul float %305, %306
  store float %mul195, float* %impulseMag194, align 4, !tbaa !30
  %307 = bitcast float* %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %307) #8
  %m_accSwingLimitImpulse = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 20
  %308 = load float, float* %m_accSwingLimitImpulse, align 4, !tbaa !67
  store float %308, float* %temp, align 4, !tbaa !30
  %309 = bitcast float* %ref.tmp196 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %309) #8
  %m_accSwingLimitImpulse197 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 20
  %310 = load float, float* %m_accSwingLimitImpulse197, align 4, !tbaa !67
  %311 = load float, float* %impulseMag194, align 4, !tbaa !30
  %add198 = fadd float %310, %311
  store float %add198, float* %ref.tmp196, align 4, !tbaa !30
  %312 = bitcast float* %ref.tmp199 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %312) #8
  store float 0.000000e+00, float* %ref.tmp199, align 4, !tbaa !30
  %call200 = call nonnull align 4 dereferenceable(4) float* @_Z5btMaxIfERKT_S2_S2_(float* nonnull align 4 dereferenceable(4) %ref.tmp196, float* nonnull align 4 dereferenceable(4) %ref.tmp199)
  %313 = load float, float* %call200, align 4, !tbaa !30
  %m_accSwingLimitImpulse201 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 20
  store float %313, float* %m_accSwingLimitImpulse201, align 4, !tbaa !67
  %314 = bitcast float* %ref.tmp199 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %314) #8
  %315 = bitcast float* %ref.tmp196 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %315) #8
  %m_accSwingLimitImpulse202 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 20
  %316 = load float, float* %m_accSwingLimitImpulse202, align 4, !tbaa !67
  %317 = load float, float* %temp, align 4, !tbaa !30
  %sub203 = fsub float %316, %317
  store float %sub203, float* %impulseMag194, align 4, !tbaa !30
  %318 = bitcast %class.btVector3* %impulse204 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %318) #8
  %m_swingAxis205 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %impulse204, %class.btVector3* nonnull align 4 dereferenceable(16) %m_swingAxis205, float* nonnull align 4 dereferenceable(4) %impulseMag194)
  %319 = bitcast %class.btVector3* %impulseTwistCouple to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %319) #8
  %320 = bitcast float* %ref.tmp206 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %320) #8
  %m_twistAxisA = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 28
  %call207 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %impulse204, %class.btVector3* nonnull align 4 dereferenceable(16) %m_twistAxisA)
  store float %call207, float* %ref.tmp206, align 4, !tbaa !30
  %m_twistAxisA208 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 28
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %impulseTwistCouple, float* nonnull align 4 dereferenceable(4) %ref.tmp206, %class.btVector3* nonnull align 4 dereferenceable(16) %m_twistAxisA208)
  %321 = bitcast float* %ref.tmp206 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %321) #8
  %322 = bitcast %class.btVector3* %impulseNoTwistCouple to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %322) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %impulseNoTwistCouple, %class.btVector3* nonnull align 4 dereferenceable(16) %impulse204, %class.btVector3* nonnull align 4 dereferenceable(16) %impulseTwistCouple)
  %323 = bitcast %class.btVector3* %impulse204 to i8*
  %324 = bitcast %class.btVector3* %impulseNoTwistCouple to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %323, i8* align 4 %324, i32 16, i1 false), !tbaa.struct !18
  %325 = bitcast %class.btVector3* %impulseNoTwistCouple to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %325) #8
  %326 = bitcast %class.btVector3* %impulseTwistCouple to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %326) #8
  %call209 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %impulse204)
  store float %call209, float* %impulseMag194, align 4, !tbaa !30
  %327 = bitcast %class.btVector3* %noTwistSwingAxis to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %327) #8
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %noTwistSwingAxis, %class.btVector3* nonnull align 4 dereferenceable(16) %impulse204, float* nonnull align 4 dereferenceable(4) %impulseMag194)
  %328 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA.addr, align 4, !tbaa !2
  %329 = bitcast %class.btVector3* %ref.tmp210 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %329) #8
  %330 = bitcast float* %ref.tmp211 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %330) #8
  store float 0.000000e+00, float* %ref.tmp211, align 4, !tbaa !30
  %331 = bitcast float* %ref.tmp212 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %331) #8
  store float 0.000000e+00, float* %ref.tmp212, align 4, !tbaa !30
  %332 = bitcast float* %ref.tmp213 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %332) #8
  store float 0.000000e+00, float* %ref.tmp213, align 4, !tbaa !30
  %call214 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp210, float* nonnull align 4 dereferenceable(4) %ref.tmp211, float* nonnull align 4 dereferenceable(4) %ref.tmp212, float* nonnull align 4 dereferenceable(4) %ref.tmp213)
  %333 = bitcast %class.btVector3* %ref.tmp215 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %333) #8
  %334 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA216 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %334, i32 0, i32 8
  %335 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA216, align 4, !tbaa !41
  %call217 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %335)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp215, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call217, %class.btVector3* nonnull align 4 dereferenceable(16) %noTwistSwingAxis)
  %336 = load float, float* %impulseMag194, align 4, !tbaa !30
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %328, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp210, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp215, float %336)
  %337 = bitcast %class.btVector3* %ref.tmp215 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %337) #8
  %338 = bitcast float* %ref.tmp213 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %338) #8
  %339 = bitcast float* %ref.tmp212 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %339) #8
  %340 = bitcast float* %ref.tmp211 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %340) #8
  %341 = bitcast %class.btVector3* %ref.tmp210 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %341) #8
  %342 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB.addr, align 4, !tbaa !2
  %343 = bitcast %class.btVector3* %ref.tmp218 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %343) #8
  %344 = bitcast float* %ref.tmp219 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %344) #8
  store float 0.000000e+00, float* %ref.tmp219, align 4, !tbaa !30
  %345 = bitcast float* %ref.tmp220 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %345) #8
  store float 0.000000e+00, float* %ref.tmp220, align 4, !tbaa !30
  %346 = bitcast float* %ref.tmp221 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %346) #8
  store float 0.000000e+00, float* %ref.tmp221, align 4, !tbaa !30
  %call222 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp218, float* nonnull align 4 dereferenceable(4) %ref.tmp219, float* nonnull align 4 dereferenceable(4) %ref.tmp220, float* nonnull align 4 dereferenceable(4) %ref.tmp221)
  %347 = bitcast %class.btVector3* %ref.tmp223 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %347) #8
  %348 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB224 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %348, i32 0, i32 9
  %349 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB224, align 4, !tbaa !43
  %call225 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %349)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp223, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call225, %class.btVector3* nonnull align 4 dereferenceable(16) %noTwistSwingAxis)
  %350 = load float, float* %impulseMag194, align 4, !tbaa !30
  %fneg226 = fneg float %350
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %342, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp218, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp223, float %fneg226)
  %351 = bitcast %class.btVector3* %ref.tmp223 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %351) #8
  %352 = bitcast float* %ref.tmp221 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %352) #8
  %353 = bitcast float* %ref.tmp220 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %353) #8
  %354 = bitcast float* %ref.tmp219 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %354) #8
  %355 = bitcast %class.btVector3* %ref.tmp218 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %355) #8
  %356 = bitcast %class.btVector3* %noTwistSwingAxis to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %356) #8
  %357 = bitcast %class.btVector3* %impulse204 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %357) #8
  %358 = bitcast float* %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %358) #8
  %359 = bitcast float* %impulseMag194 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %359) #8
  %360 = bitcast float* %relSwingVel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %360) #8
  %361 = bitcast float* %amplitude to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %361) #8
  br label %if.end227

if.end227:                                        ; preds = %if.end193, %if.end175
  %m_solveTwistLimit = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 23
  %362 = load i8, i8* %m_solveTwistLimit, align 1, !tbaa !20, !range !37
  %tobool228 = trunc i8 %362 to i1
  br i1 %tobool228, label %if.then229, label %if.end275

if.then229:                                       ; preds = %if.end227
  %363 = bitcast float* %amplitude230 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %363) #8
  %m_twistLimitRatio = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 27
  %364 = load float, float* %m_twistLimitRatio, align 4, !tbaa !49
  %m_twistCorrection = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 18
  %365 = load float, float* %m_twistCorrection, align 4, !tbaa !50
  %mul231 = fmul float %364, %365
  %m_biasFactor232 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 5
  %366 = load float, float* %m_biasFactor232, align 4, !tbaa !35
  %mul233 = fmul float %mul231, %366
  %367 = load float, float* %timeStep.addr, align 4, !tbaa !30
  %div234 = fdiv float %mul233, %367
  store float %div234, float* %amplitude230, align 4, !tbaa !30
  %368 = bitcast float* %relTwistVel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %368) #8
  %369 = bitcast %class.btVector3* %ref.tmp235 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %369) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp235, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelB178, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelA176)
  %m_twistAxis = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 13
  %call236 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp235, %class.btVector3* nonnull align 4 dereferenceable(16) %m_twistAxis)
  %370 = bitcast %class.btVector3* %ref.tmp235 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %370) #8
  store float %call236, float* %relTwistVel, align 4, !tbaa !30
  %371 = load float, float* %relTwistVel, align 4, !tbaa !30
  %cmp237 = fcmp ogt float %371, 0.000000e+00
  br i1 %cmp237, label %if.then238, label %if.end244

if.then238:                                       ; preds = %if.then229
  %m_twistLimitRatio239 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 27
  %372 = load float, float* %m_twistLimitRatio239, align 4, !tbaa !49
  %373 = load float, float* %relTwistVel, align 4, !tbaa !30
  %mul240 = fmul float %372, %373
  %m_relaxationFactor241 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 6
  %374 = load float, float* %m_relaxationFactor241, align 4, !tbaa !36
  %mul242 = fmul float %mul240, %374
  %375 = load float, float* %amplitude230, align 4, !tbaa !30
  %add243 = fadd float %375, %mul242
  store float %add243, float* %amplitude230, align 4, !tbaa !30
  br label %if.end244

if.end244:                                        ; preds = %if.then238, %if.then229
  %376 = bitcast float* %impulseMag245 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %376) #8
  %377 = load float, float* %amplitude230, align 4, !tbaa !30
  %m_kTwist = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 15
  %378 = load float, float* %m_kTwist, align 4, !tbaa !51
  %mul246 = fmul float %377, %378
  store float %mul246, float* %impulseMag245, align 4, !tbaa !30
  %379 = bitcast float* %temp247 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %379) #8
  %m_accTwistLimitImpulse = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 21
  %380 = load float, float* %m_accTwistLimitImpulse, align 4, !tbaa !66
  store float %380, float* %temp247, align 4, !tbaa !30
  %381 = bitcast float* %ref.tmp248 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %381) #8
  %m_accTwistLimitImpulse249 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 21
  %382 = load float, float* %m_accTwistLimitImpulse249, align 4, !tbaa !66
  %383 = load float, float* %impulseMag245, align 4, !tbaa !30
  %add250 = fadd float %382, %383
  store float %add250, float* %ref.tmp248, align 4, !tbaa !30
  %384 = bitcast float* %ref.tmp251 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %384) #8
  store float 0.000000e+00, float* %ref.tmp251, align 4, !tbaa !30
  %call252 = call nonnull align 4 dereferenceable(4) float* @_Z5btMaxIfERKT_S2_S2_(float* nonnull align 4 dereferenceable(4) %ref.tmp248, float* nonnull align 4 dereferenceable(4) %ref.tmp251)
  %385 = load float, float* %call252, align 4, !tbaa !30
  %m_accTwistLimitImpulse253 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 21
  store float %385, float* %m_accTwistLimitImpulse253, align 4, !tbaa !66
  %386 = bitcast float* %ref.tmp251 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %386) #8
  %387 = bitcast float* %ref.tmp248 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %387) #8
  %m_accTwistLimitImpulse254 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 21
  %388 = load float, float* %m_accTwistLimitImpulse254, align 4, !tbaa !66
  %389 = load float, float* %temp247, align 4, !tbaa !30
  %sub255 = fsub float %388, %389
  store float %sub255, float* %impulseMag245, align 4, !tbaa !30
  %390 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA.addr, align 4, !tbaa !2
  %391 = bitcast %class.btVector3* %ref.tmp256 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %391) #8
  %392 = bitcast float* %ref.tmp257 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %392) #8
  store float 0.000000e+00, float* %ref.tmp257, align 4, !tbaa !30
  %393 = bitcast float* %ref.tmp258 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %393) #8
  store float 0.000000e+00, float* %ref.tmp258, align 4, !tbaa !30
  %394 = bitcast float* %ref.tmp259 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %394) #8
  store float 0.000000e+00, float* %ref.tmp259, align 4, !tbaa !30
  %call260 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp256, float* nonnull align 4 dereferenceable(4) %ref.tmp257, float* nonnull align 4 dereferenceable(4) %ref.tmp258, float* nonnull align 4 dereferenceable(4) %ref.tmp259)
  %395 = bitcast %class.btVector3* %ref.tmp261 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %395) #8
  %396 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA262 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %396, i32 0, i32 8
  %397 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA262, align 4, !tbaa !41
  %call263 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %397)
  %m_twistAxis264 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 13
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp261, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call263, %class.btVector3* nonnull align 4 dereferenceable(16) %m_twistAxis264)
  %398 = load float, float* %impulseMag245, align 4, !tbaa !30
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %390, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp256, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp261, float %398)
  %399 = bitcast %class.btVector3* %ref.tmp261 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %399) #8
  %400 = bitcast float* %ref.tmp259 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %400) #8
  %401 = bitcast float* %ref.tmp258 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %401) #8
  %402 = bitcast float* %ref.tmp257 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %402) #8
  %403 = bitcast %class.btVector3* %ref.tmp256 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %403) #8
  %404 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB.addr, align 4, !tbaa !2
  %405 = bitcast %class.btVector3* %ref.tmp265 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %405) #8
  %406 = bitcast float* %ref.tmp266 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %406) #8
  store float 0.000000e+00, float* %ref.tmp266, align 4, !tbaa !30
  %407 = bitcast float* %ref.tmp267 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %407) #8
  store float 0.000000e+00, float* %ref.tmp267, align 4, !tbaa !30
  %408 = bitcast float* %ref.tmp268 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %408) #8
  store float 0.000000e+00, float* %ref.tmp268, align 4, !tbaa !30
  %call269 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp265, float* nonnull align 4 dereferenceable(4) %ref.tmp266, float* nonnull align 4 dereferenceable(4) %ref.tmp267, float* nonnull align 4 dereferenceable(4) %ref.tmp268)
  %409 = bitcast %class.btVector3* %ref.tmp270 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %409) #8
  %410 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB271 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %410, i32 0, i32 9
  %411 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB271, align 4, !tbaa !43
  %call272 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %411)
  %m_twistAxis273 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 13
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp270, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call272, %class.btVector3* nonnull align 4 dereferenceable(16) %m_twistAxis273)
  %412 = load float, float* %impulseMag245, align 4, !tbaa !30
  %fneg274 = fneg float %412
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %404, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp265, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp270, float %fneg274)
  %413 = bitcast %class.btVector3* %ref.tmp270 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %413) #8
  %414 = bitcast float* %ref.tmp268 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %414) #8
  %415 = bitcast float* %ref.tmp267 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %415) #8
  %416 = bitcast float* %ref.tmp266 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %416) #8
  %417 = bitcast %class.btVector3* %ref.tmp265 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %417) #8
  %418 = bitcast float* %temp247 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %418) #8
  %419 = bitcast float* %impulseMag245 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %419) #8
  %420 = bitcast float* %relTwistVel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %420) #8
  %421 = bitcast float* %amplitude230 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %421) #8
  br label %if.end275

if.end275:                                        ; preds = %if.end244, %if.end227
  %422 = bitcast %class.btVector3* %angVelB178 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %422) #8
  %423 = bitcast %class.btVector3* %angVelA176 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %423) #8
  %424 = bitcast float* %tau to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %424) #8
  %425 = bitcast %class.btVector3* %pivotBInW to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %425) #8
  %426 = bitcast %class.btVector3* %pivotAInW to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %426) #8
  br label %if.end276

if.end276:                                        ; preds = %if.end275, %entry
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK12btSolverBody39internalGetVelocityInLocalPointObsoleteERK9btVector3RS0_(%struct.btSolverBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos, %class.btVector3* nonnull align 4 dereferenceable(16) %velocity) #2 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  %rel_pos.addr = alloca %class.btVector3*, align 4
  %velocity.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %rel_pos, %class.btVector3** %rel_pos.addr, align 4, !tbaa !2
  store %class.btVector3* %velocity, %class.btVector3** %velocity.addr, align 4, !tbaa !2
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #8
  %m_linearVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 8
  %m_deltaLinearVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %m_deltaLinearVelocity)
  %2 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #8
  %3 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #8
  %m_angularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 9
  %m_deltaAngularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %m_deltaAngularVelocity)
  %4 = load %class.btVector3*, %class.btVector3** %rel_pos.addr, align 4, !tbaa !2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %4)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  %5 = load %class.btVector3*, %class.btVector3** %velocity.addr, align 4, !tbaa !2
  %6 = bitcast %class.btVector3* %5 to i8*
  %7 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !18
  %8 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #8
  %9 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #8
  %10 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %10) #8
  %11 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %11) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK15btJacobianEntry11getDiagonalEv(%class.btJacobianEntry* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btJacobianEntry*, align 4
  store %class.btJacobianEntry* %this, %class.btJacobianEntry** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btJacobianEntry*, %class.btJacobianEntry** %this.addr, align 4
  %m_Adiag = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 5
  %0 = load float, float* %m_Adiag, align 4, !tbaa !72
  ret float %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %1 = load float, float* %arrayidx, align 4, !tbaa !30
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %3 = load float, float* %arrayidx3, align 4, !tbaa !30
  %mul = fmul float %1, %3
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %4 = load float, float* %arrayidx5, align 4, !tbaa !30
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %6 = load float, float* %arrayidx7, align 4, !tbaa !30
  %mul8 = fmul float %4, %6
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4, !tbaa !30
  %7 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %8 = load float, float* %arrayidx11, align 4, !tbaa !30
  %9 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %10 = load float, float* %arrayidx13, align 4, !tbaa !30
  %mul14 = fmul float %8, %10
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %11 = load float, float* %arrayidx16, align 4, !tbaa !30
  %12 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %13 = load float, float* %arrayidx18, align 4, !tbaa !30
  %mul19 = fmul float %11, %13
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4, !tbaa !30
  %14 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #8
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %15 = load float, float* %arrayidx23, align 4, !tbaa !30
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %17 = load float, float* %arrayidx25, align 4, !tbaa !30
  %mul26 = fmul float %15, %17
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %18 = load float, float* %arrayidx28, align 4, !tbaa !30
  %19 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %19, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %20 = load float, float* %arrayidx30, align 4, !tbaa !30
  %mul31 = fmul float %18, %20
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4, !tbaa !30
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %21 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  %22 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #8
  %23 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %linearComponent, %class.btVector3* nonnull align 4 dereferenceable(16) %angularComponent, float %impulseMagnitude) #2 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  %linearComponent.addr = alloca %class.btVector3*, align 4
  %angularComponent.addr = alloca %class.btVector3*, align 4
  %impulseMagnitude.addr = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %linearComponent, %class.btVector3** %linearComponent.addr, align 4, !tbaa !2
  store %class.btVector3* %angularComponent, %class.btVector3** %angularComponent.addr, align 4, !tbaa !2
  store float %impulseMagnitude, float* %impulseMagnitude.addr, align 4, !tbaa !30
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 12
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody, align 4, !tbaa !76
  %tobool = icmp ne %class.btRigidBody* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #8
  %2 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #8
  %3 = load %class.btVector3*, %class.btVector3** %linearComponent.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, float* nonnull align 4 dereferenceable(4) %impulseMagnitude.addr)
  %m_linearFactor = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 4
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearFactor)
  %m_deltaLinearVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_deltaLinearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %4 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #8
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #8
  %6 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #8
  %7 = load %class.btVector3*, %class.btVector3** %angularComponent.addr, align 4, !tbaa !2
  %8 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #8
  %m_angularFactor = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 3
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp4, float* nonnull align 4 dereferenceable(4) %impulseMagnitude.addr, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularFactor)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %7, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  %m_deltaAngularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 2
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_deltaAngularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  %9 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #8
  %10 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %10) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK12btSolverBody26internalGetAngularVelocityER9btVector3(%struct.btSolverBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %angVel) #2 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  %angVel.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %angVel, %class.btVector3** %angVel.addr, align 4, !tbaa !2
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %m_angularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 9
  %m_deltaAngularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %m_deltaAngularVelocity)
  %1 = load %class.btVector3*, %class.btVector3** %angVel.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %1 to i8*
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !18
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #8
  ret void
}

define linkonce_odr hidden void @_ZN11btTransform11setIdentityEv(%class.btTransform* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !30
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !30
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !30
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_origin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  ret void
}

define linkonce_odr hidden void @_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_(%class.btTransform* nonnull align 4 dereferenceable(64) %curTrans, %class.btVector3* nonnull align 4 dereferenceable(16) %linvel, %class.btVector3* nonnull align 4 dereferenceable(16) %angvel, float %timeStep, %class.btTransform* nonnull align 4 dereferenceable(64) %predictedTransform) #0 comdat {
entry:
  %curTrans.addr = alloca %class.btTransform*, align 4
  %linvel.addr = alloca %class.btVector3*, align 4
  %angvel.addr = alloca %class.btVector3*, align 4
  %timeStep.addr = alloca float, align 4
  %predictedTransform.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca %class.btVector3, align 4
  %axis = alloca %class.btVector3, align 4
  %fAngle = alloca float, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp14 = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca float, align 4
  %dorn = alloca %class.btQuaternion, align 4
  %ref.tmp24 = alloca float, align 4
  %orn0 = alloca %class.btQuaternion, align 4
  %predictedOrn = alloca %class.btQuaternion, align 4
  store %class.btTransform* %curTrans, %class.btTransform** %curTrans.addr, align 4, !tbaa !2
  store %class.btVector3* %linvel, %class.btVector3** %linvel.addr, align 4, !tbaa !2
  store %class.btVector3* %angvel, %class.btVector3** %angvel.addr, align 4, !tbaa !2
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !30
  store %class.btTransform* %predictedTransform, %class.btTransform** %predictedTransform.addr, align 4, !tbaa !2
  %0 = load %class.btTransform*, %class.btTransform** %predictedTransform.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #8
  %2 = load %class.btTransform*, %class.btTransform** %curTrans.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %2)
  %3 = bitcast %class.btVector3* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #8
  %4 = load %class.btVector3*, %class.btVector3** %linvel.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp1, %class.btVector3* nonnull align 4 dereferenceable(16) %4, float* nonnull align 4 dereferenceable(4) %timeStep.addr)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1)
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %5 = bitcast %class.btVector3* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #8
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %6) #8
  %7 = bitcast %class.btVector3* %axis to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #8
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %axis)
  %8 = bitcast float* %fAngle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  %9 = load %class.btVector3*, %class.btVector3** %angvel.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %9)
  store float %call3, float* %fAngle, align 4, !tbaa !30
  %10 = load float, float* %fAngle, align 4, !tbaa !30
  %11 = load float, float* %timeStep.addr, align 4, !tbaa !30
  %mul = fmul float %10, %11
  %cmp = fcmp ogt float %mul, 0x3FE921FB60000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %12 = load float, float* %timeStep.addr, align 4, !tbaa !30
  %div = fdiv float 0x3FE921FB60000000, %12
  store float %div, float* %fAngle, align 4, !tbaa !30
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %13 = load float, float* %fAngle, align 4, !tbaa !30
  %cmp4 = fcmp olt float %13, 0x3F50624DE0000000
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %14 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %14) #8
  %15 = load %class.btVector3*, %class.btVector3** %angvel.addr, align 4, !tbaa !2
  %16 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  %17 = load float, float* %timeStep.addr, align 4, !tbaa !30
  %mul8 = fmul float 5.000000e-01, %17
  %18 = load float, float* %timeStep.addr, align 4, !tbaa !30
  %19 = load float, float* %timeStep.addr, align 4, !tbaa !30
  %mul9 = fmul float %18, %19
  %20 = load float, float* %timeStep.addr, align 4, !tbaa !30
  %mul10 = fmul float %mul9, %20
  %mul11 = fmul float %mul10, 0x3F95555560000000
  %21 = load float, float* %fAngle, align 4, !tbaa !30
  %mul12 = fmul float %mul11, %21
  %22 = load float, float* %fAngle, align 4, !tbaa !30
  %mul13 = fmul float %mul12, %22
  %sub = fsub float %mul8, %mul13
  store float %sub, float* %ref.tmp7, align 4, !tbaa !30
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %15, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %23 = bitcast %class.btVector3* %axis to i8*
  %24 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false), !tbaa.struct !18
  %25 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #8
  %26 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %26) #8
  br label %if.end20

if.else:                                          ; preds = %if.end
  %27 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %27) #8
  %28 = load %class.btVector3*, %class.btVector3** %angvel.addr, align 4, !tbaa !2
  %29 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #8
  %30 = load float, float* %fAngle, align 4, !tbaa !30
  %mul16 = fmul float 5.000000e-01, %30
  %31 = load float, float* %timeStep.addr, align 4, !tbaa !30
  %mul17 = fmul float %mul16, %31
  %call18 = call float @_Z5btSinf(float %mul17)
  %32 = load float, float* %fAngle, align 4, !tbaa !30
  %div19 = fdiv float %call18, %32
  store float %div19, float* %ref.tmp15, align 4, !tbaa !30
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp14, %class.btVector3* nonnull align 4 dereferenceable(16) %28, float* nonnull align 4 dereferenceable(4) %ref.tmp15)
  %33 = bitcast %class.btVector3* %axis to i8*
  %34 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %33, i8* align 4 %34, i32 16, i1 false), !tbaa.struct !18
  %35 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  %36 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %36) #8
  br label %if.end20

if.end20:                                         ; preds = %if.else, %if.then5
  %37 = bitcast %class.btQuaternion* %dorn to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %37) #8
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %axis)
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %axis)
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %axis)
  %38 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #8
  %39 = load float, float* %fAngle, align 4, !tbaa !30
  %40 = load float, float* %timeStep.addr, align 4, !tbaa !30
  %mul25 = fmul float %39, %40
  %mul26 = fmul float %mul25, 5.000000e-01
  %call27 = call float @_Z5btCosf(float %mul26)
  store float %call27, float* %ref.tmp24, align 4, !tbaa !30
  %call28 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %dorn, float* nonnull align 4 dereferenceable(4) %call21, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call23, float* nonnull align 4 dereferenceable(4) %ref.tmp24)
  %41 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #8
  %42 = bitcast %class.btQuaternion* %orn0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %42) #8
  %43 = load %class.btTransform*, %class.btTransform** %curTrans.addr, align 4, !tbaa !2
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %orn0, %class.btTransform* %43)
  %44 = bitcast %class.btQuaternion* %predictedOrn to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %44) #8
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %predictedOrn, %class.btQuaternion* nonnull align 4 dereferenceable(16) %dorn, %class.btQuaternion* nonnull align 4 dereferenceable(16) %orn0)
  %call29 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %predictedOrn)
  %45 = load %class.btTransform*, %class.btTransform** %predictedTransform.addr, align 4, !tbaa !2
  call void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %45, %class.btQuaternion* nonnull align 4 dereferenceable(16) %predictedOrn)
  %46 = bitcast %class.btQuaternion* %predictedOrn to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %46) #8
  %47 = bitcast %class.btQuaternion* %orn0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %47) #8
  %48 = bitcast %class.btQuaternion* %dorn to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %48) #8
  %49 = bitcast float* %fAngle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #8
  %50 = bitcast %class.btVector3* %axis to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %50) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERK12btQuaternionRK9btVector3(%class.btTransform* returned %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q, %class.btVector3* nonnull align 4 dereferenceable(16) %c) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* %m_basis, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !18
  ret %class.btTransform* %this1
}

define linkonce_odr hidden void @_ZNK11btTransform7inverseEv(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %inv = alloca %class.btMatrix3x3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %1 = bitcast %class.btMatrix3x3* %inv to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %1) #8
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %inv, %class.btMatrix3x3* %m_basis)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #8
  %3 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #8
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %4 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #8
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #8
  %6 = bitcast %class.btMatrix3x3* %inv to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %6) #8
  ret void
}

define linkonce_odr hidden void @_ZN15btTransformUtil17calculateVelocityERK11btTransformS2_fR9btVector3S4_(%class.btTransform* nonnull align 4 dereferenceable(64) %transform0, %class.btTransform* nonnull align 4 dereferenceable(64) %transform1, float %timeStep, %class.btVector3* nonnull align 4 dereferenceable(16) %linVel, %class.btVector3* nonnull align 4 dereferenceable(16) %angVel) #0 comdat {
entry:
  %transform0.addr = alloca %class.btTransform*, align 4
  %transform1.addr = alloca %class.btTransform*, align 4
  %timeStep.addr = alloca float, align 4
  %linVel.addr = alloca %class.btVector3*, align 4
  %angVel.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca %class.btVector3, align 4
  %axis = alloca %class.btVector3, align 4
  %angle = alloca float, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  store %class.btTransform* %transform0, %class.btTransform** %transform0.addr, align 4, !tbaa !2
  store %class.btTransform* %transform1, %class.btTransform** %transform1.addr, align 4, !tbaa !2
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !30
  store %class.btVector3* %linVel, %class.btVector3** %linVel.addr, align 4, !tbaa !2
  store %class.btVector3* %angVel, %class.btVector3** %angVel.addr, align 4, !tbaa !2
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = bitcast %class.btVector3* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #8
  %2 = load %class.btTransform*, %class.btTransform** %transform1.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %2)
  %3 = load %class.btTransform*, %class.btTransform** %transform0.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %3)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp1, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call2)
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %timeStep.addr)
  %4 = load %class.btVector3*, %class.btVector3** %linVel.addr, align 4, !tbaa !2
  %5 = bitcast %class.btVector3* %4 to i8*
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false), !tbaa.struct !18
  %7 = bitcast %class.btVector3* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %7) #8
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #8
  %9 = bitcast %class.btVector3* %axis to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #8
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %axis)
  %10 = bitcast float* %angle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btTransform*, %class.btTransform** %transform0.addr, align 4, !tbaa !2
  %12 = load %class.btTransform*, %class.btTransform** %transform1.addr, align 4, !tbaa !2
  call void @_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf(%class.btTransform* nonnull align 4 dereferenceable(64) %11, %class.btTransform* nonnull align 4 dereferenceable(64) %12, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, float* nonnull align 4 dereferenceable(4) %angle)
  %13 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #8
  %14 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %14) #8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, float* nonnull align 4 dereferenceable(4) %angle)
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %timeStep.addr)
  %15 = load %class.btVector3*, %class.btVector3** %angVel.addr, align 4, !tbaa !2
  %16 = bitcast %class.btVector3* %15 to i8*
  %17 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false), !tbaa.struct !18
  %18 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #8
  %19 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %19) #8
  %20 = bitcast float* %angle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #8
  %21 = bitcast %class.btVector3* %axis to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %21) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyAEv(%class.btConeTwistConstraint* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %0, i32 0, i32 8
  %1 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4, !tbaa !41
  ret %class.btRigidBody* %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK11btRigidBody32computeAngularImpulseDenominatorERK9btVector3(%class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %axis) #2 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %axis.addr = alloca %class.btVector3*, align 4
  %vec = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %axis, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btVector3* %vec to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %this1)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %vec, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call)
  %2 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %call2 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %vec)
  %3 = bitcast %class.btVector3* %vec to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %3) #8
  ret float %call2
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyBEv(%class.btConeTwistConstraint* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %0, i32 0, i32 9
  %1 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4, !tbaa !43
  ret %class.btRigidBody* %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !30
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !30
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !30
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !30
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !30
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !30
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !30
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !30
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !30
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4, !tbaa !30
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1) #8
  ret %class.btVector3* %call2
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZdvRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load float*, float** %s.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !30
  %div = fdiv float 1.000000e+00, %3
  store float %div, float* %ref.tmp, align 4, !tbaa !30
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %4 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !30
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !30
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !30
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !30
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !30
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !30
  %6 = load float*, float** %s.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !30
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !30
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !30
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !30
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !30
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !30
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !30
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !30
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !30
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !30
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !30
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !30
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_Z5btMaxIfERKT_S2_S2_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #3 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store float* %b, float** %b.addr, align 4, !tbaa !2
  %0 = load float*, float** %a.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !30
  %2 = load float*, float** %b.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !30
  %cmp = fcmp ogt float %1, %3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load float*, float** %a.addr, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %entry
  %5 = load float*, float** %b.addr, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi float* [ %4, %cond.true ], [ %5, %cond.false ]
  ret float* %cond-lvalue
}

; Function Attrs: nounwind
define hidden void @_ZN21btConeTwistConstraint9updateRHSEf(%class.btConeTwistConstraint* %this, float %timeStep) #5 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %timeStep.addr = alloca float, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4, !tbaa !2
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !30
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  ret void
}

define hidden void @_ZN21btConeTwistConstraint13calcAngleInfoEv(%class.btConeTwistConstraint* %this) #0 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %b1Axis1 = alloca %class.btVector3, align 4
  %b1Axis2 = alloca %class.btVector3, align 4
  %b1Axis3 = alloca %class.btVector3, align 4
  %b2Axis1 = alloca %class.btVector3, align 4
  %b2Axis2 = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca %class.btVector3, align 4
  %swing1 = alloca float, align 4
  %swing2 = alloca float, align 4
  %swx = alloca float, align 4
  %swy = alloca float, align 4
  %thresh = alloca float, align 4
  %fact = alloca float, align 4
  %ref.tmp17 = alloca %class.btVector3, align 4
  %ref.tmp21 = alloca %class.btVector3, align 4
  %ref.tmp34 = alloca %class.btVector3, align 4
  %ref.tmp38 = alloca %class.btVector3, align 4
  %RMaxAngle1Sq = alloca float, align 4
  %RMaxAngle2Sq = alloca float, align 4
  %EllipseAngle = alloca float, align 4
  %ref.tmp72 = alloca %class.btVector3, align 4
  %ref.tmp73 = alloca %class.btVector3, align 4
  %ref.tmp74 = alloca %class.btVector3, align 4
  %ref.tmp75 = alloca float, align 4
  %ref.tmp77 = alloca %class.btVector3, align 4
  %ref.tmp78 = alloca float, align 4
  %swingAxisSign = alloca float, align 4
  %b2Axis289 = alloca %class.btVector3, align 4
  %ref.tmp93 = alloca %class.btVector3, align 4
  %rotationArc = alloca %class.btQuaternion, align 4
  %TwistRef = alloca %class.btVector3, align 4
  %twist = alloca float, align 4
  %lockedFreeFactor = alloca float, align 4
  %ref.tmp110 = alloca %class.btVector3, align 4
  %ref.tmp111 = alloca %class.btVector3, align 4
  %ref.tmp112 = alloca float, align 4
  %ref.tmp115 = alloca float, align 4
  %ref.tmp126 = alloca %class.btVector3, align 4
  %ref.tmp127 = alloca %class.btVector3, align 4
  %ref.tmp128 = alloca float, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %m_swingCorrection = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 17
  store float 0.000000e+00, float* %m_swingCorrection, align 4, !tbaa !44
  %m_twistLimitSign = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 16
  store float 0.000000e+00, float* %m_twistLimitSign, align 4, !tbaa !45
  %m_solveTwistLimit = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 23
  store i8 0, i8* %m_solveTwistLimit, align 1, !tbaa !20
  %m_solveSwingLimit = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 24
  store i8 0, i8* %m_solveSwingLimit, align 2, !tbaa !21
  %0 = bitcast %class.btVector3* %b1Axis1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %b1Axis1)
  %1 = bitcast %class.btVector3* %b1Axis2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #8
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %b1Axis2)
  %2 = bitcast %class.btVector3* %b1Axis3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #8
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %b1Axis3)
  %3 = bitcast %class.btVector3* %b2Axis1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #8
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %b2Axis1)
  %4 = bitcast %class.btVector3* %b2Axis2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #8
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %b2Axis2)
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #8
  %call6 = call nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyAEv(%class.btConeTwistConstraint* %this1)
  %call7 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %call6)
  %call8 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call7)
  %6 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #8
  %m_rbAFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbAFrame)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp9, %class.btMatrix3x3* %call10, i32 0)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call8, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9)
  %7 = bitcast %class.btVector3* %b1Axis1 to i8*
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !18
  %9 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #8
  %10 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %10) #8
  %11 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #8
  %call12 = call nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyBEv(%class.btConeTwistConstraint* %this1)
  %call13 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %call12)
  %call14 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call13)
  %12 = bitcast %class.btVector3* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %12) #8
  %m_rbBFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  %call16 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbBFrame)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp15, %class.btMatrix3x3* %call16, i32 0)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp11, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call14, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp15)
  %13 = bitcast %class.btVector3* %b2Axis1 to i8*
  %14 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 16, i1 false), !tbaa.struct !18
  %15 = bitcast %class.btVector3* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %15) #8
  %16 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %16) #8
  %17 = bitcast float* %swing1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #8
  store float 0.000000e+00, float* %swing1, align 4, !tbaa !30
  %18 = bitcast float* %swing2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #8
  store float 0.000000e+00, float* %swing2, align 4, !tbaa !30
  %19 = bitcast float* %swx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  store float 0.000000e+00, float* %swx, align 4, !tbaa !30
  %20 = bitcast float* %swy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #8
  store float 0.000000e+00, float* %swy, align 4, !tbaa !30
  %21 = bitcast float* %thresh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #8
  store float 1.000000e+01, float* %thresh, align 4, !tbaa !30
  %22 = bitcast float* %fact to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #8
  %m_swingSpan1 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %23 = load float, float* %m_swingSpan1, align 4, !tbaa !31
  %cmp = fcmp oge float %23, 0x3FA99999A0000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %24 = bitcast %class.btVector3* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %24) #8
  %call18 = call nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyAEv(%class.btConeTwistConstraint* %this1)
  %call19 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %call18)
  %call20 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call19)
  %25 = bitcast %class.btVector3* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %25) #8
  %m_rbAFrame22 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  %call23 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbAFrame22)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp21, %class.btMatrix3x3* %call23, i32 1)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp17, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call20, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp21)
  %26 = bitcast %class.btVector3* %b1Axis2 to i8*
  %27 = bitcast %class.btVector3* %ref.tmp17 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %26, i8* align 4 %27, i32 16, i1 false), !tbaa.struct !18
  %28 = bitcast %class.btVector3* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %28) #8
  %29 = bitcast %class.btVector3* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #8
  %call24 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %b2Axis1, %class.btVector3* nonnull align 4 dereferenceable(16) %b1Axis1)
  store float %call24, float* %swx, align 4, !tbaa !30
  %call25 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %b2Axis1, %class.btVector3* nonnull align 4 dereferenceable(16) %b1Axis2)
  store float %call25, float* %swy, align 4, !tbaa !30
  %30 = load float, float* %swy, align 4, !tbaa !30
  %31 = load float, float* %swx, align 4, !tbaa !30
  %call26 = call float @_Z11btAtan2Fastff(float %30, float %31)
  store float %call26, float* %swing1, align 4, !tbaa !30
  %32 = load float, float* %swy, align 4, !tbaa !30
  %33 = load float, float* %swy, align 4, !tbaa !30
  %mul = fmul float %32, %33
  %34 = load float, float* %swx, align 4, !tbaa !30
  %35 = load float, float* %swx, align 4, !tbaa !30
  %mul27 = fmul float %34, %35
  %add = fadd float %mul, %mul27
  %36 = load float, float* %thresh, align 4, !tbaa !30
  %mul28 = fmul float %add, %36
  %37 = load float, float* %thresh, align 4, !tbaa !30
  %mul29 = fmul float %mul28, %37
  store float %mul29, float* %fact, align 4, !tbaa !30
  %38 = load float, float* %fact, align 4, !tbaa !30
  %39 = load float, float* %fact, align 4, !tbaa !30
  %add30 = fadd float %39, 1.000000e+00
  %div = fdiv float %38, %add30
  store float %div, float* %fact, align 4, !tbaa !30
  %40 = load float, float* %fact, align 4, !tbaa !30
  %41 = load float, float* %swing1, align 4, !tbaa !30
  %mul31 = fmul float %41, %40
  store float %mul31, float* %swing1, align 4, !tbaa !30
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_swingSpan2 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %42 = load float, float* %m_swingSpan2, align 4, !tbaa !32
  %cmp32 = fcmp oge float %42, 0x3FA99999A0000000
  br i1 %cmp32, label %if.then33, label %if.end52

if.then33:                                        ; preds = %if.end
  %43 = bitcast %class.btVector3* %ref.tmp34 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %43) #8
  %call35 = call nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyAEv(%class.btConeTwistConstraint* %this1)
  %call36 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %call35)
  %call37 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call36)
  %44 = bitcast %class.btVector3* %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %44) #8
  %m_rbAFrame39 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  %call40 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbAFrame39)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp38, %class.btMatrix3x3* %call40, i32 2)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp34, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call37, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp38)
  %45 = bitcast %class.btVector3* %b1Axis3 to i8*
  %46 = bitcast %class.btVector3* %ref.tmp34 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %45, i8* align 4 %46, i32 16, i1 false), !tbaa.struct !18
  %47 = bitcast %class.btVector3* %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %47) #8
  %48 = bitcast %class.btVector3* %ref.tmp34 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %48) #8
  %call41 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %b2Axis1, %class.btVector3* nonnull align 4 dereferenceable(16) %b1Axis1)
  store float %call41, float* %swx, align 4, !tbaa !30
  %call42 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %b2Axis1, %class.btVector3* nonnull align 4 dereferenceable(16) %b1Axis3)
  store float %call42, float* %swy, align 4, !tbaa !30
  %49 = load float, float* %swy, align 4, !tbaa !30
  %50 = load float, float* %swx, align 4, !tbaa !30
  %call43 = call float @_Z11btAtan2Fastff(float %49, float %50)
  store float %call43, float* %swing2, align 4, !tbaa !30
  %51 = load float, float* %swy, align 4, !tbaa !30
  %52 = load float, float* %swy, align 4, !tbaa !30
  %mul44 = fmul float %51, %52
  %53 = load float, float* %swx, align 4, !tbaa !30
  %54 = load float, float* %swx, align 4, !tbaa !30
  %mul45 = fmul float %53, %54
  %add46 = fadd float %mul44, %mul45
  %55 = load float, float* %thresh, align 4, !tbaa !30
  %mul47 = fmul float %add46, %55
  %56 = load float, float* %thresh, align 4, !tbaa !30
  %mul48 = fmul float %mul47, %56
  store float %mul48, float* %fact, align 4, !tbaa !30
  %57 = load float, float* %fact, align 4, !tbaa !30
  %58 = load float, float* %fact, align 4, !tbaa !30
  %add49 = fadd float %58, 1.000000e+00
  %div50 = fdiv float %57, %add49
  store float %div50, float* %fact, align 4, !tbaa !30
  %59 = load float, float* %fact, align 4, !tbaa !30
  %60 = load float, float* %swing2, align 4, !tbaa !30
  %mul51 = fmul float %60, %59
  store float %mul51, float* %swing2, align 4, !tbaa !30
  br label %if.end52

if.end52:                                         ; preds = %if.then33, %if.end
  %61 = bitcast float* %RMaxAngle1Sq to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %61) #8
  %m_swingSpan153 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %62 = load float, float* %m_swingSpan153, align 4, !tbaa !31
  %m_swingSpan154 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %63 = load float, float* %m_swingSpan154, align 4, !tbaa !31
  %mul55 = fmul float %62, %63
  %div56 = fdiv float 1.000000e+00, %mul55
  store float %div56, float* %RMaxAngle1Sq, align 4, !tbaa !30
  %64 = bitcast float* %RMaxAngle2Sq to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #8
  %m_swingSpan257 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %65 = load float, float* %m_swingSpan257, align 4, !tbaa !32
  %m_swingSpan258 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %66 = load float, float* %m_swingSpan258, align 4, !tbaa !32
  %mul59 = fmul float %65, %66
  %div60 = fdiv float 1.000000e+00, %mul59
  store float %div60, float* %RMaxAngle2Sq, align 4, !tbaa !30
  %67 = bitcast float* %EllipseAngle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #8
  %68 = load float, float* %swing1, align 4, !tbaa !30
  %69 = load float, float* %swing1, align 4, !tbaa !30
  %mul61 = fmul float %68, %69
  %call62 = call float @_Z6btFabsf(float %mul61)
  %70 = load float, float* %RMaxAngle1Sq, align 4, !tbaa !30
  %mul63 = fmul float %call62, %70
  %71 = load float, float* %swing2, align 4, !tbaa !30
  %72 = load float, float* %swing2, align 4, !tbaa !30
  %mul64 = fmul float %71, %72
  %call65 = call float @_Z6btFabsf(float %mul64)
  %73 = load float, float* %RMaxAngle2Sq, align 4, !tbaa !30
  %mul66 = fmul float %call65, %73
  %add67 = fadd float %mul63, %mul66
  store float %add67, float* %EllipseAngle, align 4, !tbaa !30
  %74 = load float, float* %EllipseAngle, align 4, !tbaa !30
  %cmp68 = fcmp ogt float %74, 1.000000e+00
  br i1 %cmp68, label %if.then69, label %if.end86

if.then69:                                        ; preds = %if.end52
  %75 = load float, float* %EllipseAngle, align 4, !tbaa !30
  %sub = fsub float %75, 1.000000e+00
  %m_swingCorrection70 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 17
  store float %sub, float* %m_swingCorrection70, align 4, !tbaa !44
  %m_solveSwingLimit71 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 24
  store i8 1, i8* %m_solveSwingLimit71, align 2, !tbaa !21
  %76 = bitcast %class.btVector3* %ref.tmp72 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %76) #8
  %77 = bitcast %class.btVector3* %ref.tmp73 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %77) #8
  %78 = bitcast %class.btVector3* %ref.tmp74 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %78) #8
  %79 = bitcast float* %ref.tmp75 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #8
  %call76 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %b2Axis1, %class.btVector3* nonnull align 4 dereferenceable(16) %b1Axis2)
  store float %call76, float* %ref.tmp75, align 4, !tbaa !30
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp74, %class.btVector3* nonnull align 4 dereferenceable(16) %b1Axis2, float* nonnull align 4 dereferenceable(4) %ref.tmp75)
  %80 = bitcast %class.btVector3* %ref.tmp77 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %80) #8
  %81 = bitcast float* %ref.tmp78 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %81) #8
  %call79 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %b2Axis1, %class.btVector3* nonnull align 4 dereferenceable(16) %b1Axis3)
  store float %call79, float* %ref.tmp78, align 4, !tbaa !30
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp77, %class.btVector3* nonnull align 4 dereferenceable(16) %b1Axis3, float* nonnull align 4 dereferenceable(4) %ref.tmp78)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp73, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp74, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp77)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp72, %class.btVector3* %b2Axis1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp73)
  %m_swingAxis = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %82 = bitcast %class.btVector3* %m_swingAxis to i8*
  %83 = bitcast %class.btVector3* %ref.tmp72 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %82, i8* align 4 %83, i32 16, i1 false), !tbaa.struct !18
  %84 = bitcast float* %ref.tmp78 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #8
  %85 = bitcast %class.btVector3* %ref.tmp77 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %85) #8
  %86 = bitcast float* %ref.tmp75 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #8
  %87 = bitcast %class.btVector3* %ref.tmp74 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %87) #8
  %88 = bitcast %class.btVector3* %ref.tmp73 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %88) #8
  %89 = bitcast %class.btVector3* %ref.tmp72 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %89) #8
  %m_swingAxis80 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %call81 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %m_swingAxis80)
  %90 = bitcast float* %swingAxisSign to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %90) #8
  %call82 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %b2Axis1, %class.btVector3* nonnull align 4 dereferenceable(16) %b1Axis1)
  %cmp83 = fcmp oge float %call82, 0.000000e+00
  %91 = zext i1 %cmp83 to i64
  %cond = select i1 %cmp83, float 1.000000e+00, float -1.000000e+00
  store float %cond, float* %swingAxisSign, align 4, !tbaa !30
  %m_swingAxis84 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %call85 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %m_swingAxis84, float* nonnull align 4 dereferenceable(4) %swingAxisSign)
  %92 = bitcast float* %swingAxisSign to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #8
  br label %if.end86

if.end86:                                         ; preds = %if.then69, %if.end52
  %m_twistSpan = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %93 = load float, float* %m_twistSpan, align 4, !tbaa !33
  %cmp87 = fcmp oge float %93, 0.000000e+00
  br i1 %cmp87, label %if.then88, label %if.end134

if.then88:                                        ; preds = %if.end86
  %94 = bitcast %class.btVector3* %b2Axis289 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %94) #8
  %call90 = call nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyBEv(%class.btConeTwistConstraint* %this1)
  %call91 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %call90)
  %call92 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call91)
  %95 = bitcast %class.btVector3* %ref.tmp93 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %95) #8
  %m_rbBFrame94 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  %call95 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbBFrame94)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp93, %class.btMatrix3x3* %call95, i32 1)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %b2Axis289, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call92, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp93)
  %96 = bitcast %class.btVector3* %ref.tmp93 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %96) #8
  %97 = bitcast %class.btQuaternion* %rotationArc to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %97) #8
  call void @_Z15shortestArcQuatRK9btVector3S1_(%class.btQuaternion* sret align 4 %rotationArc, %class.btVector3* nonnull align 4 dereferenceable(16) %b2Axis1, %class.btVector3* nonnull align 4 dereferenceable(16) %b1Axis1)
  %98 = bitcast %class.btVector3* %TwistRef to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %98) #8
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %TwistRef, %class.btQuaternion* nonnull align 4 dereferenceable(16) %rotationArc, %class.btVector3* nonnull align 4 dereferenceable(16) %b2Axis289)
  %99 = bitcast float* %twist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %99) #8
  %call96 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %TwistRef, %class.btVector3* nonnull align 4 dereferenceable(16) %b1Axis3)
  %call97 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %TwistRef, %class.btVector3* nonnull align 4 dereferenceable(16) %b1Axis2)
  %call98 = call float @_Z11btAtan2Fastff(float %call96, float %call97)
  store float %call98, float* %twist, align 4, !tbaa !30
  %100 = load float, float* %twist, align 4, !tbaa !30
  %m_twistAngle = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 19
  store float %100, float* %m_twistAngle, align 4, !tbaa !48
  %101 = bitcast float* %lockedFreeFactor to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %101) #8
  %m_twistSpan99 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %102 = load float, float* %m_twistSpan99, align 4, !tbaa !33
  %cmp100 = fcmp ogt float %102, 0x3FA99999A0000000
  %103 = zext i1 %cmp100 to i64
  %cond101 = select i1 %cmp100, float 1.000000e+00, float 0.000000e+00
  store float %cond101, float* %lockedFreeFactor, align 4, !tbaa !30
  %104 = load float, float* %twist, align 4, !tbaa !30
  %m_twistSpan102 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %105 = load float, float* %m_twistSpan102, align 4, !tbaa !33
  %fneg = fneg float %105
  %106 = load float, float* %lockedFreeFactor, align 4, !tbaa !30
  %mul103 = fmul float %fneg, %106
  %cmp104 = fcmp ole float %104, %mul103
  br i1 %cmp104, label %if.then105, label %if.else

if.then105:                                       ; preds = %if.then88
  %107 = load float, float* %twist, align 4, !tbaa !30
  %m_twistSpan106 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %108 = load float, float* %m_twistSpan106, align 4, !tbaa !33
  %add107 = fadd float %107, %108
  %fneg108 = fneg float %add107
  %m_twistCorrection = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 18
  store float %fneg108, float* %m_twistCorrection, align 4, !tbaa !50
  %m_solveTwistLimit109 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 23
  store i8 1, i8* %m_solveTwistLimit109, align 1, !tbaa !20
  %109 = bitcast %class.btVector3* %ref.tmp110 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %109) #8
  %110 = bitcast %class.btVector3* %ref.tmp111 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %110) #8
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp111, %class.btVector3* nonnull align 4 dereferenceable(16) %b2Axis1, %class.btVector3* nonnull align 4 dereferenceable(16) %b1Axis1)
  %111 = bitcast float* %ref.tmp112 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %111) #8
  store float 5.000000e-01, float* %ref.tmp112, align 4, !tbaa !30
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp110, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp111, float* nonnull align 4 dereferenceable(4) %ref.tmp112)
  %m_twistAxis = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 13
  %112 = bitcast %class.btVector3* %m_twistAxis to i8*
  %113 = bitcast %class.btVector3* %ref.tmp110 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %112, i8* align 4 %113, i32 16, i1 false), !tbaa.struct !18
  %114 = bitcast float* %ref.tmp112 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #8
  %115 = bitcast %class.btVector3* %ref.tmp111 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %115) #8
  %116 = bitcast %class.btVector3* %ref.tmp110 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %116) #8
  %m_twistAxis113 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 13
  %call114 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %m_twistAxis113)
  %117 = bitcast float* %ref.tmp115 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %117) #8
  store float -1.000000e+00, float* %ref.tmp115, align 4, !tbaa !30
  %m_twistAxis116 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 13
  %call117 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %m_twistAxis116, float* nonnull align 4 dereferenceable(4) %ref.tmp115)
  %118 = bitcast float* %ref.tmp115 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #8
  br label %if.end133

if.else:                                          ; preds = %if.then88
  %119 = load float, float* %twist, align 4, !tbaa !30
  %m_twistSpan118 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %120 = load float, float* %m_twistSpan118, align 4, !tbaa !33
  %121 = load float, float* %lockedFreeFactor, align 4, !tbaa !30
  %mul119 = fmul float %120, %121
  %cmp120 = fcmp ogt float %119, %mul119
  br i1 %cmp120, label %if.then121, label %if.end132

if.then121:                                       ; preds = %if.else
  %122 = load float, float* %twist, align 4, !tbaa !30
  %m_twistSpan122 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %123 = load float, float* %m_twistSpan122, align 4, !tbaa !33
  %sub123 = fsub float %122, %123
  %m_twistCorrection124 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 18
  store float %sub123, float* %m_twistCorrection124, align 4, !tbaa !50
  %m_solveTwistLimit125 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 23
  store i8 1, i8* %m_solveTwistLimit125, align 1, !tbaa !20
  %124 = bitcast %class.btVector3* %ref.tmp126 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %124) #8
  %125 = bitcast %class.btVector3* %ref.tmp127 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %125) #8
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp127, %class.btVector3* nonnull align 4 dereferenceable(16) %b2Axis1, %class.btVector3* nonnull align 4 dereferenceable(16) %b1Axis1)
  %126 = bitcast float* %ref.tmp128 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %126) #8
  store float 5.000000e-01, float* %ref.tmp128, align 4, !tbaa !30
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp126, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp127, float* nonnull align 4 dereferenceable(4) %ref.tmp128)
  %m_twistAxis129 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 13
  %127 = bitcast %class.btVector3* %m_twistAxis129 to i8*
  %128 = bitcast %class.btVector3* %ref.tmp126 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %127, i8* align 4 %128, i32 16, i1 false), !tbaa.struct !18
  %129 = bitcast float* %ref.tmp128 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #8
  %130 = bitcast %class.btVector3* %ref.tmp127 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %130) #8
  %131 = bitcast %class.btVector3* %ref.tmp126 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %131) #8
  %m_twistAxis130 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 13
  %call131 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %m_twistAxis130)
  br label %if.end132

if.end132:                                        ; preds = %if.then121, %if.else
  br label %if.end133

if.end133:                                        ; preds = %if.end132, %if.then105
  %132 = bitcast float* %lockedFreeFactor to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #8
  %133 = bitcast float* %twist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %133) #8
  %134 = bitcast %class.btVector3* %TwistRef to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %134) #8
  %135 = bitcast %class.btQuaternion* %rotationArc to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %135) #8
  %136 = bitcast %class.btVector3* %b2Axis289 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %136) #8
  br label %if.end134

if.end134:                                        ; preds = %if.end133, %if.end86
  %137 = bitcast float* %EllipseAngle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %137) #8
  %138 = bitcast float* %RMaxAngle2Sq to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %138) #8
  %139 = bitcast float* %RMaxAngle1Sq to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #8
  %140 = bitcast float* %fact to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #8
  %141 = bitcast float* %thresh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #8
  %142 = bitcast float* %swy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #8
  %143 = bitcast float* %swx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #8
  %144 = bitcast float* %swing2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #8
  %145 = bitcast float* %swing1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #8
  %146 = bitcast %class.btVector3* %b2Axis2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %146) #8
  %147 = bitcast %class.btVector3* %b2Axis1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %147) #8
  %148 = bitcast %class.btVector3* %b1Axis3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %148) #8
  %149 = bitcast %class.btVector3* %b1Axis2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %149) #8
  %150 = bitcast %class.btVector3* %b1Axis1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %150) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_Z11btAtan2Fastff(float %y, float %x) #2 comdat {
entry:
  %y.addr = alloca float, align 4
  %x.addr = alloca float, align 4
  %coeff_1 = alloca float, align 4
  %coeff_2 = alloca float, align 4
  %abs_y = alloca float, align 4
  %angle = alloca float, align 4
  %r = alloca float, align 4
  %r3 = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !30
  store float %x, float* %x.addr, align 4, !tbaa !30
  %0 = bitcast float* %coeff_1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 0x3FE921FB60000000, float* %coeff_1, align 4, !tbaa !30
  %1 = bitcast float* %coeff_2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load float, float* %coeff_1, align 4, !tbaa !30
  %mul = fmul float 3.000000e+00, %2
  store float %mul, float* %coeff_2, align 4, !tbaa !30
  %3 = bitcast float* %abs_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load float, float* %y.addr, align 4, !tbaa !30
  %call = call float @_Z6btFabsf(float %4)
  store float %call, float* %abs_y, align 4, !tbaa !30
  %5 = bitcast float* %angle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load float, float* %x.addr, align 4, !tbaa !30
  %cmp = fcmp oge float %6, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %7 = bitcast float* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %8 = load float, float* %x.addr, align 4, !tbaa !30
  %9 = load float, float* %abs_y, align 4, !tbaa !30
  %sub = fsub float %8, %9
  %10 = load float, float* %x.addr, align 4, !tbaa !30
  %11 = load float, float* %abs_y, align 4, !tbaa !30
  %add = fadd float %10, %11
  %div = fdiv float %sub, %add
  store float %div, float* %r, align 4, !tbaa !30
  %12 = load float, float* %coeff_1, align 4, !tbaa !30
  %13 = load float, float* %coeff_1, align 4, !tbaa !30
  %14 = load float, float* %r, align 4, !tbaa !30
  %mul1 = fmul float %13, %14
  %sub2 = fsub float %12, %mul1
  store float %sub2, float* %angle, align 4, !tbaa !30
  %15 = bitcast float* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  br label %if.end

if.else:                                          ; preds = %entry
  %16 = bitcast float* %r3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  %17 = load float, float* %x.addr, align 4, !tbaa !30
  %18 = load float, float* %abs_y, align 4, !tbaa !30
  %add4 = fadd float %17, %18
  %19 = load float, float* %abs_y, align 4, !tbaa !30
  %20 = load float, float* %x.addr, align 4, !tbaa !30
  %sub5 = fsub float %19, %20
  %div6 = fdiv float %add4, %sub5
  store float %div6, float* %r3, align 4, !tbaa !30
  %21 = load float, float* %coeff_2, align 4, !tbaa !30
  %22 = load float, float* %coeff_1, align 4, !tbaa !30
  %23 = load float, float* %r3, align 4, !tbaa !30
  %mul7 = fmul float %22, %23
  %sub8 = fsub float %21, %mul7
  store float %sub8, float* %angle, align 4, !tbaa !30
  %24 = bitcast float* %r3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %25 = load float, float* %y.addr, align 4, !tbaa !30
  %cmp9 = fcmp olt float %25, 0.000000e+00
  br i1 %cmp9, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %26 = load float, float* %angle, align 4, !tbaa !30
  %fneg = fneg float %26
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %27 = load float, float* %angle, align 4, !tbaa !30
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %fneg, %cond.true ], [ %27, %cond.false ]
  %28 = bitcast float* %angle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #8
  %29 = bitcast float* %abs_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #8
  %30 = bitcast float* %coeff_2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #8
  %31 = bitcast float* %coeff_1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #8
  ret float %cond
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btFabsf(float %x) #3 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !30
  %0 = load float, float* %x.addr, align 4, !tbaa !30
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_Z15shortestArcQuatRK9btVector3S1_(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1) #2 comdat {
entry:
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %c = alloca %class.btVector3, align 4
  %d = alloca float, align 4
  %n = alloca %class.btVector3, align 4
  %unused = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %s = alloca float, align 4
  %rs = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %0 = bitcast %class.btVector3* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %c, %class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  %3 = bitcast float* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %5 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call, float* %d, align 4, !tbaa !30
  %6 = load float, float* %d, align 4, !tbaa !30
  %conv = fpext float %6 to double
  %cmp = fcmp olt double %conv, 0xBFEFFFFFC0000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = bitcast %class.btVector3* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #8
  %call1 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %n)
  %8 = bitcast %class.btVector3* %unused to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #8
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %unused)
  %9 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  call void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %9, %class.btVector3* nonnull align 4 dereferenceable(16) %n, %class.btVector3* nonnull align 4 dereferenceable(16) %unused)
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %n)
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %n)
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %n)
  %10 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !30
  %call6 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %call3, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call5, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  store i32 1, i32* %cleanup.dest.slot, align 4
  %12 = bitcast %class.btVector3* %unused to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %12) #8
  %13 = bitcast %class.btVector3* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %13) #8
  br label %cleanup

if.end:                                           ; preds = %entry
  %14 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #8
  %15 = load float, float* %d, align 4, !tbaa !30
  %add = fadd float 1.000000e+00, %15
  %mul = fmul float %add, 2.000000e+00
  %call7 = call float @_Z6btSqrtf(float %mul)
  store float %call7, float* %s, align 4, !tbaa !30
  %16 = bitcast float* %rs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  %17 = load float, float* %s, align 4, !tbaa !30
  %div = fdiv float 1.000000e+00, %17
  store float %div, float* %rs, align 4, !tbaa !30
  %18 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #8
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %c)
  %19 = load float, float* %call9, align 4, !tbaa !30
  %20 = load float, float* %rs, align 4, !tbaa !30
  %mul10 = fmul float %19, %20
  store float %mul10, float* %ref.tmp8, align 4, !tbaa !30
  %21 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #8
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %c)
  %22 = load float, float* %call12, align 4, !tbaa !30
  %23 = load float, float* %rs, align 4, !tbaa !30
  %mul13 = fmul float %22, %23
  store float %mul13, float* %ref.tmp11, align 4, !tbaa !30
  %24 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #8
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %c)
  %25 = load float, float* %call15, align 4, !tbaa !30
  %26 = load float, float* %rs, align 4, !tbaa !30
  %mul16 = fmul float %25, %26
  store float %mul16, float* %ref.tmp14, align 4, !tbaa !30
  %27 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #8
  %28 = load float, float* %s, align 4, !tbaa !30
  %mul18 = fmul float %28, 5.000000e-01
  store float %mul18, float* %ref.tmp17, align 4, !tbaa !30
  %call19 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  %29 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #8
  %30 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #8
  %31 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #8
  %32 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #8
  store i32 1, i32* %cleanup.dest.slot, align 4
  %33 = bitcast float* %rs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #8
  %34 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #8
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %35 = bitcast float* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  %36 = bitcast %class.btVector3* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %36) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %rotation, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %rotation.addr = alloca %class.btQuaternion*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %q = alloca %class.btQuaternion, align 4
  %ref.tmp = alloca %class.btQuaternion, align 4
  store %class.btQuaternion* %rotation, %class.btQuaternion** %rotation.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast %class.btQuaternion* %q to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btQuaternion*, %class.btQuaternion** %rotation.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  call void @_ZmlRK12btQuaternionRK9btVector3(%class.btQuaternion* sret align 4 %q, %class.btQuaternion* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  %3 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #8
  %4 = load %class.btQuaternion*, %class.btQuaternion** %rotation.addr, align 4, !tbaa !2
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp, %class.btQuaternion* %4)
  %call = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKS_(%class.btQuaternion* %q, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp)
  %5 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #8
  %6 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getXEv(%class.btQuadWord* %6)
  %7 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getYEv(%class.btQuadWord* %7)
  %8 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getZEv(%class.btQuadWord* %8)
  %call4 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %call1, float* nonnull align 4 dereferenceable(4) %call2, float* nonnull align 4 dereferenceable(4) %call3)
  %9 = bitcast %class.btQuaternion* %q to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #8
  ret void
}

define internal void @__cxx_global_var_init() #0 {
entry:
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !30
  %1 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 0.000000e+00, float* %ref.tmp1, align 4, !tbaa !30
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !30
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* @_ZL6vTwist, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  %4 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  ret void
}

define linkonce_odr hidden void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btTransform* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %agg.result)
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %m_basis, %class.btQuaternion* nonnull align 4 dereferenceable(16) %agg.result)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_Z11btFuzzyZerof(float %x) #3 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !30
  %0 = load float, float* %x.addr, align 4, !tbaa !30
  %call = call float @_Z6btFabsf(float %0)
  %cmp = fcmp olt float %call, 0x3E80000000000000
  ret i1 %cmp
}

define linkonce_odr hidden float @_ZNK12btQuaternion8getAngleEv(%class.btQuaternion* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %s = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  %2 = load float, float* %arrayidx, align 4, !tbaa !30
  %call = call float @_Z6btAcosf(float %2)
  %mul = fmul float 2.000000e+00, %call
  store float %mul, float* %s, align 4, !tbaa !30
  %3 = load float, float* %s, align 4, !tbaa !30
  %4 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  ret float %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q2) #2 comdat {
entry:
  %q1.addr = alloca %class.btQuaternion*, align 4
  %q2.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp44 = alloca float, align 4
  store %class.btQuaternion* %q1, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q2, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %2 = bitcast %class.btQuaternion* %1 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %2)
  %3 = load float, float* %call, align 4, !tbaa !30
  %4 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %5 = bitcast %class.btQuaternion* %4 to %class.btQuadWord*
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %5)
  %6 = load float, float* %call1, align 4, !tbaa !30
  %mul = fmul float %3, %6
  %7 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %8 = bitcast %class.btQuaternion* %7 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %8)
  %9 = load float, float* %call2, align 4, !tbaa !30
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %11)
  %12 = load float, float* %call3, align 4, !tbaa !30
  %mul4 = fmul float %9, %12
  %add = fadd float %mul, %mul4
  %13 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %14 = bitcast %class.btQuaternion* %13 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %14)
  %15 = load float, float* %call5, align 4, !tbaa !30
  %16 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %17 = bitcast %class.btQuaternion* %16 to %class.btQuadWord*
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %17)
  %18 = load float, float* %call6, align 4, !tbaa !30
  %mul7 = fmul float %15, %18
  %add8 = fadd float %add, %mul7
  %19 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %20 = bitcast %class.btQuaternion* %19 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %20)
  %21 = load float, float* %call9, align 4, !tbaa !30
  %22 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %23 = bitcast %class.btQuaternion* %22 to %class.btQuadWord*
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %23)
  %24 = load float, float* %call10, align 4, !tbaa !30
  %mul11 = fmul float %21, %24
  %sub = fsub float %add8, %mul11
  store float %sub, float* %ref.tmp, align 4, !tbaa !30
  %25 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  %26 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %27 = bitcast %class.btQuaternion* %26 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %27)
  %28 = load float, float* %call13, align 4, !tbaa !30
  %29 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %30 = bitcast %class.btQuaternion* %29 to %class.btQuadWord*
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %30)
  %31 = load float, float* %call14, align 4, !tbaa !30
  %mul15 = fmul float %28, %31
  %32 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %33 = bitcast %class.btQuaternion* %32 to %class.btQuadWord*
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %33)
  %34 = load float, float* %call16, align 4, !tbaa !30
  %35 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %36 = bitcast %class.btQuaternion* %35 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %36)
  %37 = load float, float* %call17, align 4, !tbaa !30
  %mul18 = fmul float %34, %37
  %add19 = fadd float %mul15, %mul18
  %38 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %39 = bitcast %class.btQuaternion* %38 to %class.btQuadWord*
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %39)
  %40 = load float, float* %call20, align 4, !tbaa !30
  %41 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %42 = bitcast %class.btQuaternion* %41 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %42)
  %43 = load float, float* %call21, align 4, !tbaa !30
  %mul22 = fmul float %40, %43
  %add23 = fadd float %add19, %mul22
  %44 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %45 = bitcast %class.btQuaternion* %44 to %class.btQuadWord*
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %45)
  %46 = load float, float* %call24, align 4, !tbaa !30
  %47 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %48 = bitcast %class.btQuaternion* %47 to %class.btQuadWord*
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %48)
  %49 = load float, float* %call25, align 4, !tbaa !30
  %mul26 = fmul float %46, %49
  %sub27 = fsub float %add23, %mul26
  store float %sub27, float* %ref.tmp12, align 4, !tbaa !30
  %50 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #8
  %51 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %52 = bitcast %class.btQuaternion* %51 to %class.btQuadWord*
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %52)
  %53 = load float, float* %call29, align 4, !tbaa !30
  %54 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %55 = bitcast %class.btQuaternion* %54 to %class.btQuadWord*
  %call30 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %55)
  %56 = load float, float* %call30, align 4, !tbaa !30
  %mul31 = fmul float %53, %56
  %57 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %58 = bitcast %class.btQuaternion* %57 to %class.btQuadWord*
  %call32 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %58)
  %59 = load float, float* %call32, align 4, !tbaa !30
  %60 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %61 = bitcast %class.btQuaternion* %60 to %class.btQuadWord*
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %61)
  %62 = load float, float* %call33, align 4, !tbaa !30
  %mul34 = fmul float %59, %62
  %add35 = fadd float %mul31, %mul34
  %63 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %64 = bitcast %class.btQuaternion* %63 to %class.btQuadWord*
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %64)
  %65 = load float, float* %call36, align 4, !tbaa !30
  %66 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %67 = bitcast %class.btQuaternion* %66 to %class.btQuadWord*
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %67)
  %68 = load float, float* %call37, align 4, !tbaa !30
  %mul38 = fmul float %65, %68
  %add39 = fadd float %add35, %mul38
  %69 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %70 = bitcast %class.btQuaternion* %69 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %70)
  %71 = load float, float* %call40, align 4, !tbaa !30
  %72 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %73 = bitcast %class.btQuaternion* %72 to %class.btQuadWord*
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %73)
  %74 = load float, float* %call41, align 4, !tbaa !30
  %mul42 = fmul float %71, %74
  %sub43 = fsub float %add39, %mul42
  store float %sub43, float* %ref.tmp28, align 4, !tbaa !30
  %75 = bitcast float* %ref.tmp44 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #8
  %76 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %77 = bitcast %class.btQuaternion* %76 to %class.btQuadWord*
  %call45 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %77)
  %78 = load float, float* %call45, align 4, !tbaa !30
  %79 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %80 = bitcast %class.btQuaternion* %79 to %class.btQuadWord*
  %call46 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %80)
  %81 = load float, float* %call46, align 4, !tbaa !30
  %mul47 = fmul float %78, %81
  %82 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %83 = bitcast %class.btQuaternion* %82 to %class.btQuadWord*
  %call48 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %83)
  %84 = load float, float* %call48, align 4, !tbaa !30
  %85 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %86 = bitcast %class.btQuaternion* %85 to %class.btQuadWord*
  %call49 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %86)
  %87 = load float, float* %call49, align 4, !tbaa !30
  %mul50 = fmul float %84, %87
  %sub51 = fsub float %mul47, %mul50
  %88 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %89 = bitcast %class.btQuaternion* %88 to %class.btQuadWord*
  %call52 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %89)
  %90 = load float, float* %call52, align 4, !tbaa !30
  %91 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %92 = bitcast %class.btQuaternion* %91 to %class.btQuadWord*
  %call53 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %92)
  %93 = load float, float* %call53, align 4, !tbaa !30
  %mul54 = fmul float %90, %93
  %sub55 = fsub float %sub51, %mul54
  %94 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %95 = bitcast %class.btQuaternion* %94 to %class.btQuadWord*
  %call56 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %95)
  %96 = load float, float* %call56, align 4, !tbaa !30
  %97 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %98 = bitcast %class.btQuaternion* %97 to %class.btQuadWord*
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %98)
  %99 = load float, float* %call57, align 4, !tbaa !30
  %mul58 = fmul float %96, %99
  %sub59 = fsub float %sub55, %mul58
  store float %sub59, float* %ref.tmp44, align 4, !tbaa !30
  %call60 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp44)
  %100 = bitcast float* %ref.tmp44 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #8
  %101 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #8
  %102 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #8
  %103 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #8
  ret void
}

define linkonce_odr hidden void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !30
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp, align 4, !tbaa !30
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats3 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %4, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [4 x float], [4 x float]* %m_floats3, i32 0, i32 1
  %5 = load float, float* %arrayidx4, align 4, !tbaa !30
  %fneg5 = fneg float %5
  store float %fneg5, float* %ref.tmp2, align 4, !tbaa !30
  %6 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats7 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %7, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 2
  %8 = load float, float* %arrayidx8, align 4, !tbaa !30
  %fneg9 = fneg float %8
  store float %fneg9, float* %ref.tmp6, align 4, !tbaa !30
  %9 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats10 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %9, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 3
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %10 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  %12 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  ret void
}

define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call float @_ZNK12btQuaternion6lengthEv(%class.btQuaternion* %this1)
  store float %call, float* %ref.tmp, align 4, !tbaa !30
  %call2 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaterniondVERKf(%class.btQuaternion* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1) #8
  ret %class.btQuaternion* %call2
}

define hidden void @_ZN21btConeTwistConstraint20computeConeLimitInfoERK12btQuaternionRfR9btVector3S3_(%class.btConeTwistConstraint* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qCone, float* nonnull align 4 dereferenceable(4) %swingAngle, %class.btVector3* nonnull align 4 dereferenceable(16) %vSwingAxis, float* nonnull align 4 dereferenceable(4) %swingLimit) #0 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %qCone.addr = alloca %class.btQuaternion*, align 4
  %swingAngle.addr = alloca float*, align 4
  %vSwingAxis.addr = alloca %class.btVector3*, align 4
  %swingLimit.addr = alloca float*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %xEllipse = alloca float, align 4
  %yEllipse = alloca float, align 4
  %surfaceSlope2 = alloca float, align 4
  %norm = alloca float, align 4
  %swingLimit2 = alloca float, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %qCone, %class.btQuaternion** %qCone.addr, align 4, !tbaa !2
  store float* %swingAngle, float** %swingAngle.addr, align 4, !tbaa !2
  store %class.btVector3* %vSwingAxis, %class.btVector3** %vSwingAxis.addr, align 4, !tbaa !2
  store float* %swingLimit, float** %swingLimit.addr, align 4, !tbaa !2
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %qCone.addr, align 4, !tbaa !2
  %call = call float @_ZNK12btQuaternion8getAngleEv(%class.btQuaternion* %0)
  %1 = load float*, float** %swingAngle.addr, align 4, !tbaa !2
  store float %call, float* %1, align 4, !tbaa !30
  %2 = load float*, float** %swingAngle.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !30
  %cmp = fcmp ogt float %3, 0x3E80000000000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #8
  %5 = load %class.btQuaternion*, %class.btQuaternion** %qCone.addr, align 4, !tbaa !2
  %6 = bitcast %class.btQuaternion* %5 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %6)
  %7 = load %class.btQuaternion*, %class.btQuaternion** %qCone.addr, align 4, !tbaa !2
  %8 = bitcast %class.btQuaternion* %7 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %8)
  %9 = load %class.btQuaternion*, %class.btQuaternion** %qCone.addr, align 4, !tbaa !2
  %10 = bitcast %class.btQuaternion* %9 to %class.btQuadWord*
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %10)
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %call2, float* nonnull align 4 dereferenceable(4) %call3, float* nonnull align 4 dereferenceable(4) %call4)
  %11 = load %class.btVector3*, %class.btVector3** %vSwingAxis.addr, align 4, !tbaa !2
  %12 = bitcast %class.btVector3* %11 to i8*
  %13 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false), !tbaa.struct !18
  %14 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %14) #8
  %15 = load %class.btVector3*, %class.btVector3** %vSwingAxis.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %15)
  %16 = bitcast float* %xEllipse to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  %17 = load %class.btVector3*, %class.btVector3** %vSwingAxis.addr, align 4, !tbaa !2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %17)
  %18 = load float, float* %call7, align 4, !tbaa !30
  store float %18, float* %xEllipse, align 4, !tbaa !30
  %19 = bitcast float* %yEllipse to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  %20 = load %class.btVector3*, %class.btVector3** %vSwingAxis.addr, align 4, !tbaa !2
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %20)
  %21 = load float, float* %call8, align 4, !tbaa !30
  %fneg = fneg float %21
  store float %fneg, float* %yEllipse, align 4, !tbaa !30
  %m_swingSpan1 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %22 = load float, float* %m_swingSpan1, align 4, !tbaa !31
  %23 = load float*, float** %swingLimit.addr, align 4, !tbaa !2
  store float %22, float* %23, align 4, !tbaa !30
  %24 = load float, float* %xEllipse, align 4, !tbaa !30
  %call9 = call float @_Z4fabsf(float %24) #8
  %cmp10 = fcmp ogt float %call9, 0x3E80000000000000
  br i1 %cmp10, label %if.then11, label %if.end

if.then11:                                        ; preds = %if.then
  %25 = bitcast float* %surfaceSlope2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  %26 = load float, float* %yEllipse, align 4, !tbaa !30
  %27 = load float, float* %yEllipse, align 4, !tbaa !30
  %mul = fmul float %26, %27
  %28 = load float, float* %xEllipse, align 4, !tbaa !30
  %29 = load float, float* %xEllipse, align 4, !tbaa !30
  %mul12 = fmul float %28, %29
  %div = fdiv float %mul, %mul12
  store float %div, float* %surfaceSlope2, align 4, !tbaa !30
  %30 = bitcast float* %norm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #8
  %m_swingSpan2 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %31 = load float, float* %m_swingSpan2, align 4, !tbaa !32
  %m_swingSpan213 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %32 = load float, float* %m_swingSpan213, align 4, !tbaa !32
  %mul14 = fmul float %31, %32
  %div15 = fdiv float 1.000000e+00, %mul14
  store float %div15, float* %norm, align 4, !tbaa !30
  %33 = load float, float* %surfaceSlope2, align 4, !tbaa !30
  %m_swingSpan116 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %34 = load float, float* %m_swingSpan116, align 4, !tbaa !31
  %m_swingSpan117 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %35 = load float, float* %m_swingSpan117, align 4, !tbaa !31
  %mul18 = fmul float %34, %35
  %div19 = fdiv float %33, %mul18
  %36 = load float, float* %norm, align 4, !tbaa !30
  %add = fadd float %36, %div19
  store float %add, float* %norm, align 4, !tbaa !30
  %37 = bitcast float* %swingLimit2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #8
  %38 = load float, float* %surfaceSlope2, align 4, !tbaa !30
  %add20 = fadd float 1.000000e+00, %38
  %39 = load float, float* %norm, align 4, !tbaa !30
  %div21 = fdiv float %add20, %39
  store float %div21, float* %swingLimit2, align 4, !tbaa !30
  %40 = load float, float* %swingLimit2, align 4, !tbaa !30
  %call22 = call float @_Z4sqrtf(float %40) #8
  %41 = load float*, float** %swingLimit.addr, align 4, !tbaa !2
  store float %call22, float* %41, align 4, !tbaa !30
  %42 = bitcast float* %swingLimit2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #8
  %43 = bitcast float* %norm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #8
  %44 = bitcast float* %surfaceSlope2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #8
  br label %if.end

if.end:                                           ; preds = %if.then11, %if.then
  %45 = bitcast float* %yEllipse to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #8
  %46 = bitcast float* %xEllipse to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #8
  br label %if.end26

if.else:                                          ; preds = %entry
  %47 = load float*, float** %swingAngle.addr, align 4, !tbaa !2
  %48 = load float, float* %47, align 4, !tbaa !30
  %cmp23 = fcmp olt float %48, 0.000000e+00
  br i1 %cmp23, label %if.then24, label %if.end25

if.then24:                                        ; preds = %if.else
  br label %if.end25

if.end25:                                         ; preds = %if.then24, %if.else
  br label %if.end26

if.end26:                                         ; preds = %if.end25, %if.end
  ret void
}

define hidden void @_ZNK21btConeTwistConstraint33adjustSwingAxisToUseEllipseNormalER9btVector3(%class.btConeTwistConstraint* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %vSwingAxis) #0 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %vSwingAxis.addr = alloca %class.btVector3*, align 4
  %y = alloca float, align 4
  %z = alloca float, align 4
  %grad = alloca float, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %vSwingAxis, %class.btVector3** %vSwingAxis.addr, align 4, !tbaa !2
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = bitcast float* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %vSwingAxis.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %1)
  %2 = load float, float* %call, align 4, !tbaa !30
  %fneg = fneg float %2
  store float %fneg, float* %y, align 4, !tbaa !30
  %3 = bitcast float* %z to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %class.btVector3*, %class.btVector3** %vSwingAxis.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call2, align 4, !tbaa !30
  store float %5, float* %z, align 4, !tbaa !30
  %6 = load float, float* %z, align 4, !tbaa !30
  %call3 = call float @_Z4fabsf(float %6) #8
  %cmp = fcmp ogt float %call3, 0x3E80000000000000
  br i1 %cmp, label %if.then, label %if.end14

if.then:                                          ; preds = %entry
  %7 = bitcast float* %grad to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %8 = load float, float* %y, align 4, !tbaa !30
  %9 = load float, float* %z, align 4, !tbaa !30
  %div = fdiv float %8, %9
  store float %div, float* %grad, align 4, !tbaa !30
  %m_swingSpan2 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %10 = load float, float* %m_swingSpan2, align 4, !tbaa !32
  %m_swingSpan1 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %11 = load float, float* %m_swingSpan1, align 4, !tbaa !31
  %div4 = fdiv float %10, %11
  %12 = load float, float* %grad, align 4, !tbaa !30
  %mul = fmul float %12, %div4
  store float %mul, float* %grad, align 4, !tbaa !30
  %13 = load float, float* %y, align 4, !tbaa !30
  %cmp5 = fcmp ogt float %13, 0.000000e+00
  br i1 %cmp5, label %if.then6, label %if.else

if.then6:                                         ; preds = %if.then
  %14 = load float, float* %grad, align 4, !tbaa !30
  %15 = load float, float* %z, align 4, !tbaa !30
  %mul7 = fmul float %14, %15
  %call8 = call float @_Z4fabsf(float %mul7) #8
  store float %call8, float* %y, align 4, !tbaa !30
  br label %if.end

if.else:                                          ; preds = %if.then
  %16 = load float, float* %grad, align 4, !tbaa !30
  %17 = load float, float* %z, align 4, !tbaa !30
  %mul9 = fmul float %16, %17
  %call10 = call float @_Z4fabsf(float %mul9) #8
  %fneg11 = fneg float %call10
  store float %fneg11, float* %y, align 4, !tbaa !30
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then6
  %18 = load %class.btVector3*, %class.btVector3** %vSwingAxis.addr, align 4, !tbaa !2
  %19 = load float, float* %y, align 4, !tbaa !30
  %fneg12 = fneg float %19
  call void @_ZN9btVector34setZEf(%class.btVector3* %18, float %fneg12)
  %20 = load %class.btVector3*, %class.btVector3** %vSwingAxis.addr, align 4, !tbaa !2
  %21 = load float, float* %z, align 4, !tbaa !30
  call void @_ZN9btVector34setYEf(%class.btVector3* %20, float %21)
  %22 = load %class.btVector3*, %class.btVector3** %vSwingAxis.addr, align 4, !tbaa !2
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %22)
  %23 = bitcast float* %grad to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #8
  br label %if.end14

if.end14:                                         ; preds = %if.end, %entry
  %24 = bitcast float* %z to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #8
  %25 = bitcast float* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_Z32computeAngularImpulseDenominatorRK9btVector3RK11btMatrix3x3(%class.btVector3* nonnull align 4 dereferenceable(16) %axis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %invInertiaWorld) #2 comdat {
entry:
  %axis.addr = alloca %class.btVector3*, align 4
  %invInertiaWorld.addr = alloca %class.btMatrix3x3*, align 4
  %vec = alloca %class.btVector3, align 4
  store %class.btVector3* %axis, %class.btVector3** %axis.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %invInertiaWorld, %class.btMatrix3x3** %invInertiaWorld.addr, align 4, !tbaa !2
  %0 = bitcast %class.btVector3* %vec to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %invInertiaWorld.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %vec, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %2)
  %3 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %vec)
  %4 = bitcast %class.btVector3* %vec to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #8
  ret float %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z7btAtan2ff(float %x, float %y) #3 comdat {
entry:
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !30
  store float %y, float* %y.addr, align 4, !tbaa !30
  %0 = load float, float* %x.addr, align 4, !tbaa !30
  %1 = load float, float* %y.addr, align 4, !tbaa !30
  %call = call float @atan2f(float %0, float %1) #9
  ret float %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z5btCosf(float %x) #3 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !30
  %0 = load float, float* %x.addr, align 4, !tbaa !30
  %1 = call float @llvm.cos.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z5btSinf(float %x) #3 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !30
  %0 = load float, float* %x.addr, align 4, !tbaa !30
  %1 = call float @llvm.sin.f32(float %0)
  ret float %1
}

define hidden void @_ZN21btConeTwistConstraint21computeTwistLimitInfoERK12btQuaternionRfR9btVector3(%class.btConeTwistConstraint* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qTwist, float* nonnull align 4 dereferenceable(4) %twistAngle, %class.btVector3* nonnull align 4 dereferenceable(16) %vTwistAxis) #0 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %qTwist.addr = alloca %class.btQuaternion*, align 4
  %twistAngle.addr = alloca float*, align 4
  %vTwistAxis.addr = alloca %class.btVector3*, align 4
  %qMinTwist = alloca %class.btQuaternion, align 4
  %ref.tmp = alloca %class.btQuaternion, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %qTwist, %class.btQuaternion** %qTwist.addr, align 4, !tbaa !2
  store float* %twistAngle, float** %twistAngle.addr, align 4, !tbaa !2
  store %class.btVector3* %vTwistAxis, %class.btVector3** %vTwistAxis.addr, align 4, !tbaa !2
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %qMinTwist to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btQuaternion*, %class.btQuaternion** %qTwist.addr, align 4, !tbaa !2
  %2 = bitcast %class.btQuaternion* %qMinTwist to i8*
  %3 = bitcast %class.btQuaternion* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  %4 = load %class.btQuaternion*, %class.btQuaternion** %qTwist.addr, align 4, !tbaa !2
  %call = call float @_ZNK12btQuaternion8getAngleEv(%class.btQuaternion* %4)
  %5 = load float*, float** %twistAngle.addr, align 4, !tbaa !2
  store float %call, float* %5, align 4, !tbaa !30
  %6 = load float*, float** %twistAngle.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !30
  %cmp = fcmp ogt float %7, 0x400921FB60000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %8 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #8
  %9 = load %class.btQuaternion*, %class.btQuaternion** %qTwist.addr, align 4, !tbaa !2
  call void @_ZNK12btQuaternionngEv(%class.btQuaternion* sret align 4 %ref.tmp, %class.btQuaternion* %9)
  %10 = bitcast %class.btQuaternion* %qMinTwist to i8*
  %11 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  %12 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %12) #8
  %call2 = call float @_ZNK12btQuaternion8getAngleEv(%class.btQuaternion* %qMinTwist)
  %13 = load float*, float** %twistAngle.addr, align 4, !tbaa !2
  store float %call2, float* %13, align 4, !tbaa !30
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %14 = load float*, float** %twistAngle.addr, align 4, !tbaa !2
  %15 = load float, float* %14, align 4, !tbaa !30
  %cmp3 = fcmp olt float %15, 0.000000e+00
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  br label %if.end5

if.end5:                                          ; preds = %if.then4, %if.end
  %16 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #8
  %17 = bitcast %class.btQuaternion* %qMinTwist to %class.btQuadWord*
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %17)
  %18 = bitcast %class.btQuaternion* %qMinTwist to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %18)
  %19 = bitcast %class.btQuaternion* %qMinTwist to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %19)
  %call10 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp6, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call8, float* nonnull align 4 dereferenceable(4) %call9)
  %20 = load %class.btVector3*, %class.btVector3** %vTwistAxis.addr, align 4, !tbaa !2
  %21 = bitcast %class.btVector3* %20 to i8*
  %22 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false), !tbaa.struct !18
  %23 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %23) #8
  %24 = load float*, float** %twistAngle.addr, align 4, !tbaa !2
  %25 = load float, float* %24, align 4, !tbaa !30
  %cmp11 = fcmp ogt float %25, 0x3E80000000000000
  br i1 %cmp11, label %if.then12, label %if.end14

if.then12:                                        ; preds = %if.end5
  %26 = load %class.btVector3*, %class.btVector3** %vTwistAxis.addr, align 4, !tbaa !2
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %26)
  br label %if.end14

if.end14:                                         ; preds = %if.then12, %if.end5
  %27 = bitcast %class.btQuaternion* %qMinTwist to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %27) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z4fabsf(float %__lcpp_x) #3 comdat {
entry:
  %__lcpp_x.addr = alloca float, align 4
  store float %__lcpp_x, float* %__lcpp_x.addr, align 4, !tbaa !30
  %0 = load float, float* %__lcpp_x.addr, align 4, !tbaa !30
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z4sqrtf(float %__lcpp_x) #3 comdat {
entry:
  %__lcpp_x.addr = alloca float, align 4
  store float %__lcpp_x, float* %__lcpp_x.addr, align 4, !tbaa !30
  %0 = load float, float* %__lcpp_x.addr, align 4, !tbaa !30
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

define hidden void @_ZNK21btConeTwistConstraint16GetPointForAngleEff(%class.btVector3* noalias sret align 4 %agg.result, %class.btConeTwistConstraint* %this, float %fAngleInRadians, float %fLength) #0 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %fAngleInRadians.addr = alloca float, align 4
  %fLength.addr = alloca float, align 4
  %xEllipse = alloca float, align 4
  %yEllipse = alloca float, align 4
  %swingLimit = alloca float, align 4
  %surfaceSlope2 = alloca float, align 4
  %norm = alloca float, align 4
  %swingLimit2 = alloca float, align 4
  %vSwingAxis = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %qSwing = alloca %class.btQuaternion, align 4
  %vPointInConstraintSpace = alloca %class.btVector3, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4, !tbaa !2
  store float %fAngleInRadians, float* %fAngleInRadians.addr, align 4, !tbaa !30
  store float %fLength, float* %fLength.addr, align 4, !tbaa !30
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = bitcast float* %xEllipse to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load float, float* %fAngleInRadians.addr, align 4, !tbaa !30
  %call = call float @_Z5btCosf(float %1)
  store float %call, float* %xEllipse, align 4, !tbaa !30
  %2 = bitcast float* %yEllipse to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load float, float* %fAngleInRadians.addr, align 4, !tbaa !30
  %call2 = call float @_Z5btSinf(float %3)
  store float %call2, float* %yEllipse, align 4, !tbaa !30
  %4 = bitcast float* %swingLimit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %m_swingSpan1 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %5 = load float, float* %m_swingSpan1, align 4, !tbaa !31
  store float %5, float* %swingLimit, align 4, !tbaa !30
  %6 = load float, float* %xEllipse, align 4, !tbaa !30
  %call3 = call float @_Z4fabsf(float %6) #8
  %cmp = fcmp ogt float %call3, 0x3E80000000000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = bitcast float* %surfaceSlope2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %8 = load float, float* %yEllipse, align 4, !tbaa !30
  %9 = load float, float* %yEllipse, align 4, !tbaa !30
  %mul = fmul float %8, %9
  %10 = load float, float* %xEllipse, align 4, !tbaa !30
  %11 = load float, float* %xEllipse, align 4, !tbaa !30
  %mul4 = fmul float %10, %11
  %div = fdiv float %mul, %mul4
  store float %div, float* %surfaceSlope2, align 4, !tbaa !30
  %12 = bitcast float* %norm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  %m_swingSpan2 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %13 = load float, float* %m_swingSpan2, align 4, !tbaa !32
  %m_swingSpan25 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %14 = load float, float* %m_swingSpan25, align 4, !tbaa !32
  %mul6 = fmul float %13, %14
  %div7 = fdiv float 1.000000e+00, %mul6
  store float %div7, float* %norm, align 4, !tbaa !30
  %15 = load float, float* %surfaceSlope2, align 4, !tbaa !30
  %m_swingSpan18 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %16 = load float, float* %m_swingSpan18, align 4, !tbaa !31
  %m_swingSpan19 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %17 = load float, float* %m_swingSpan19, align 4, !tbaa !31
  %mul10 = fmul float %16, %17
  %div11 = fdiv float %15, %mul10
  %18 = load float, float* %norm, align 4, !tbaa !30
  %add = fadd float %18, %div11
  store float %add, float* %norm, align 4, !tbaa !30
  %19 = bitcast float* %swingLimit2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  %20 = load float, float* %surfaceSlope2, align 4, !tbaa !30
  %add12 = fadd float 1.000000e+00, %20
  %21 = load float, float* %norm, align 4, !tbaa !30
  %div13 = fdiv float %add12, %21
  store float %div13, float* %swingLimit2, align 4, !tbaa !30
  %22 = load float, float* %swingLimit2, align 4, !tbaa !30
  %call14 = call float @_Z4sqrtf(float %22) #8
  store float %call14, float* %swingLimit, align 4, !tbaa !30
  %23 = bitcast float* %swingLimit2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #8
  %24 = bitcast float* %norm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #8
  %25 = bitcast float* %surfaceSlope2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %26 = bitcast %class.btVector3* %vSwingAxis to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %26) #8
  %27 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !30
  %28 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #8
  %29 = load float, float* %yEllipse, align 4, !tbaa !30
  %fneg = fneg float %29
  store float %fneg, float* %ref.tmp15, align 4, !tbaa !30
  %call16 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %vSwingAxis, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %xEllipse, float* nonnull align 4 dereferenceable(4) %ref.tmp15)
  %30 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #8
  %31 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #8
  %32 = bitcast %class.btQuaternion* %qSwing to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %32) #8
  %call17 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %qSwing, %class.btVector3* nonnull align 4 dereferenceable(16) %vSwingAxis, float* nonnull align 4 dereferenceable(4) %swingLimit)
  %33 = bitcast %class.btVector3* %vPointInConstraintSpace to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %33) #8
  %34 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #8
  store float 0.000000e+00, float* %ref.tmp18, align 4, !tbaa !30
  %35 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #8
  store float 0.000000e+00, float* %ref.tmp19, align 4, !tbaa !30
  %call20 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %vPointInConstraintSpace, float* nonnull align 4 dereferenceable(4) %fLength.addr, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp19)
  %36 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #8
  %37 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #8
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qSwing, %class.btVector3* nonnull align 4 dereferenceable(16) %vPointInConstraintSpace)
  %38 = bitcast %class.btVector3* %vPointInConstraintSpace to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %38) #8
  %39 = bitcast %class.btQuaternion* %qSwing to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %39) #8
  %40 = bitcast %class.btVector3* %vSwingAxis to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %40) #8
  %41 = bitcast float* %swingLimit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #8
  %42 = bitcast float* %yEllipse to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #8
  %43 = bitcast float* %xEllipse to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #8
  ret void
}

define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %_axis, float* nonnull align 4 dereferenceable(4) %_angle) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %_axis.addr = alloca %class.btVector3*, align 4
  %_angle.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %_axis, %class.btVector3** %_axis.addr, align 4, !tbaa !2
  store float* %_angle, float** %_angle.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* %0)
  %1 = load %class.btVector3*, %class.btVector3** %_axis.addr, align 4, !tbaa !2
  %2 = load float*, float** %_angle.addr, align 4, !tbaa !2
  call void @_ZN12btQuaternion11setRotationERK9btVector3RKf(%class.btQuaternion* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1, float* nonnull align 4 dereferenceable(4) %2)
  ret %class.btQuaternion* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK12btQuaternionngEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q2 = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion** %q2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store %class.btQuaternion* %this1, %class.btQuaternion** %q2, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q2, align 4, !tbaa !2
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call, align 4, !tbaa !30
  %fneg = fneg float %4
  store float %fneg, float* %ref.tmp, align 4, !tbaa !30
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btQuaternion*, %class.btQuaternion** %q2, align 4, !tbaa !2
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %7)
  %8 = load float, float* %call3, align 4, !tbaa !30
  %fneg4 = fneg float %8
  store float %fneg4, float* %ref.tmp2, align 4, !tbaa !30
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q2, align 4, !tbaa !2
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %11)
  %12 = load float, float* %call6, align 4, !tbaa !30
  %fneg7 = fneg float %12
  store float %fneg7, float* %ref.tmp5, align 4, !tbaa !30
  %13 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load %class.btQuaternion*, %class.btQuaternion** %q2, align 4, !tbaa !2
  %15 = bitcast %class.btQuaternion* %14 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %15, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  %16 = load float, float* %arrayidx, align 4, !tbaa !30
  %fneg9 = fneg float %16
  store float %fneg9, float* %ref.tmp8, align 4, !tbaa !30
  %call10 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  %17 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  %18 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  %19 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  %20 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #8
  %21 = bitcast %class.btQuaternion** %q2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector34setZEf(%class.btVector3* %this, float %_z) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_z.addr = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float %_z, float* %_z.addr, align 4, !tbaa !30
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float, float* %_z.addr, align 4, !tbaa !30
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  store float %0, float* %arrayidx, align 4, !tbaa !30
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector34setYEf(%class.btVector3* %this, float %_y) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_y.addr = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float %_y, float* %_y.addr, align 4, !tbaa !30
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float, float* %_y.addr, align 4, !tbaa !30
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  store float %0, float* %arrayidx, align 4, !tbaa !30
  ret void
}

define hidden void @_ZN21btConeTwistConstraint14setMotorTargetERK12btQuaternion(%class.btConeTwistConstraint* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #0 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %trACur = alloca %class.btTransform, align 4
  %trBCur = alloca %class.btTransform, align 4
  %qConstraint = alloca %class.btQuaternion, align 4
  %ref.tmp = alloca %class.btQuaternion, align 4
  %ref.tmp5 = alloca %class.btQuaternion, align 4
  %ref.tmp6 = alloca %class.btQuaternion, align 4
  %ref.tmp7 = alloca %class.btQuaternion, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = bitcast %class.btTransform* %trACur to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %0) #8
  %1 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %1, i32 0, i32 8
  %2 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4, !tbaa !41
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %2)
  %call2 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %trACur, %class.btTransform* nonnull align 4 dereferenceable(64) %call)
  %3 = bitcast %class.btTransform* %trBCur to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %3) #8
  %4 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %4, i32 0, i32 9
  %5 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4, !tbaa !43
  %call3 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %5)
  %call4 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %trBCur, %class.btTransform* nonnull align 4 dereferenceable(64) %call3)
  %6 = bitcast %class.btQuaternion* %qConstraint to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #8
  %7 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #8
  %8 = bitcast %class.btQuaternion* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #8
  %9 = bitcast %class.btQuaternion* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #8
  %m_rbBFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp6, %class.btTransform* %m_rbBFrame)
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp5, %class.btQuaternion* %ref.tmp6)
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %ref.tmp, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp5, %class.btQuaternion* nonnull align 4 dereferenceable(16) %10)
  %11 = bitcast %class.btQuaternion* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #8
  %m_rbAFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp7, %class.btTransform* %m_rbAFrame)
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %qConstraint, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp7)
  %12 = bitcast %class.btQuaternion* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %12) #8
  %13 = bitcast %class.btQuaternion* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %13) #8
  %14 = bitcast %class.btQuaternion* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %14) #8
  %15 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %15) #8
  call void @_ZN21btConeTwistConstraint31setMotorTargetInConstraintSpaceERK12btQuaternion(%class.btConeTwistConstraint* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qConstraint)
  %16 = bitcast %class.btQuaternion* %qConstraint to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %16) #8
  %17 = bitcast %class.btTransform* %trBCur to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %17) #8
  %18 = bitcast %class.btTransform* %trACur to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %18) #8
  ret void
}

define hidden void @_ZN21btConeTwistConstraint31setMotorTargetInConstraintSpaceERK12btQuaternion(%class.btConeTwistConstraint* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #0 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %softness = alloca float, align 4
  %vTwisted = alloca %class.btVector3, align 4
  %qTargetCone = alloca %class.btQuaternion, align 4
  %qTargetTwist = alloca %class.btQuaternion, align 4
  %ref.tmp = alloca %class.btQuaternion, align 4
  %swingAngle = alloca float, align 4
  %swingLimit = alloca float, align 4
  %swingAxis = alloca %class.btVector3, align 4
  %ref.tmp19 = alloca %class.btQuaternion, align 4
  %twistAngle = alloca float, align 4
  %twistAxis = alloca %class.btVector3, align 4
  %ref.tmp46 = alloca %class.btQuaternion, align 4
  %ref.tmp50 = alloca %class.btQuaternion, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %m_qTarget = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 31
  %1 = bitcast %class.btQuaternion* %m_qTarget to i8*
  %2 = bitcast %class.btQuaternion* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = bitcast float* %softness to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store float 1.000000e+00, float* %softness, align 4, !tbaa !30
  %4 = bitcast %class.btVector3* %vTwisted to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #8
  %m_qTarget2 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 31
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %vTwisted, %class.btQuaternion* nonnull align 4 dereferenceable(16) %m_qTarget2, %class.btVector3* nonnull align 4 dereferenceable(16) @_ZL6vTwist)
  %5 = bitcast %class.btQuaternion* %qTargetCone to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #8
  call void @_Z15shortestArcQuatRK9btVector3S1_(%class.btQuaternion* sret align 4 %qTargetCone, %class.btVector3* nonnull align 4 dereferenceable(16) @_ZL6vTwist, %class.btVector3* nonnull align 4 dereferenceable(16) %vTwisted)
  %call = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %qTargetCone)
  %6 = bitcast %class.btQuaternion* %qTargetTwist to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #8
  %7 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #8
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp, %class.btQuaternion* %qTargetCone)
  %m_qTarget3 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 31
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %qTargetTwist, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btQuaternion* nonnull align 4 dereferenceable(16) %m_qTarget3)
  %8 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #8
  %call4 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %qTargetTwist)
  %m_swingSpan1 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %9 = load float, float* %m_swingSpan1, align 4, !tbaa !31
  %cmp = fcmp oge float %9, 0x3FA99999A0000000
  br i1 %cmp, label %land.lhs.true, label %if.end22

land.lhs.true:                                    ; preds = %entry
  %m_swingSpan2 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %10 = load float, float* %m_swingSpan2, align 4, !tbaa !32
  %cmp5 = fcmp oge float %10, 0x3FA99999A0000000
  br i1 %cmp5, label %if.then, label %if.end22

if.then:                                          ; preds = %land.lhs.true
  %11 = bitcast float* %swingAngle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  %12 = bitcast float* %swingLimit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  %13 = bitcast %class.btVector3* %swingAxis to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #8
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %swingAxis)
  call void @_ZN21btConeTwistConstraint20computeConeLimitInfoERK12btQuaternionRfR9btVector3S3_(%class.btConeTwistConstraint* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qTargetCone, float* nonnull align 4 dereferenceable(4) %swingAngle, %class.btVector3* nonnull align 4 dereferenceable(16) %swingAxis, float* nonnull align 4 dereferenceable(4) %swingLimit)
  %14 = load float, float* %swingAngle, align 4, !tbaa !30
  %call7 = call float @_Z4fabsf(float %14) #8
  %cmp8 = fcmp ogt float %call7, 0x3E80000000000000
  br i1 %cmp8, label %if.then9, label %if.end21

if.then9:                                         ; preds = %if.then
  %15 = load float, float* %swingAngle, align 4, !tbaa !30
  %16 = load float, float* %swingLimit, align 4, !tbaa !30
  %17 = load float, float* %softness, align 4, !tbaa !30
  %mul = fmul float %16, %17
  %cmp10 = fcmp ogt float %15, %mul
  br i1 %cmp10, label %if.then11, label %if.else

if.then11:                                        ; preds = %if.then9
  %18 = load float, float* %swingLimit, align 4, !tbaa !30
  %19 = load float, float* %softness, align 4, !tbaa !30
  %mul12 = fmul float %18, %19
  store float %mul12, float* %swingAngle, align 4, !tbaa !30
  br label %if.end18

if.else:                                          ; preds = %if.then9
  %20 = load float, float* %swingAngle, align 4, !tbaa !30
  %21 = load float, float* %swingLimit, align 4, !tbaa !30
  %fneg = fneg float %21
  %22 = load float, float* %softness, align 4, !tbaa !30
  %mul13 = fmul float %fneg, %22
  %cmp14 = fcmp olt float %20, %mul13
  br i1 %cmp14, label %if.then15, label %if.end

if.then15:                                        ; preds = %if.else
  %23 = load float, float* %swingLimit, align 4, !tbaa !30
  %fneg16 = fneg float %23
  %24 = load float, float* %softness, align 4, !tbaa !30
  %mul17 = fmul float %fneg16, %24
  store float %mul17, float* %swingAngle, align 4, !tbaa !30
  br label %if.end

if.end:                                           ; preds = %if.then15, %if.else
  br label %if.end18

if.end18:                                         ; preds = %if.end, %if.then11
  %25 = bitcast %class.btQuaternion* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %25) #8
  %call20 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %ref.tmp19, %class.btVector3* nonnull align 4 dereferenceable(16) %swingAxis, float* nonnull align 4 dereferenceable(4) %swingAngle)
  %26 = bitcast %class.btQuaternion* %qTargetCone to i8*
  %27 = bitcast %class.btQuaternion* %ref.tmp19 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %26, i8* align 4 %27, i32 16, i1 false)
  %28 = bitcast %class.btQuaternion* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %28) #8
  br label %if.end21

if.end21:                                         ; preds = %if.end18, %if.then
  %29 = bitcast %class.btVector3* %swingAxis to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #8
  %30 = bitcast float* %swingLimit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #8
  %31 = bitcast float* %swingAngle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #8
  br label %if.end22

if.end22:                                         ; preds = %if.end21, %land.lhs.true, %entry
  %m_twistSpan = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %32 = load float, float* %m_twistSpan, align 4, !tbaa !33
  %cmp23 = fcmp oge float %32, 0x3FA99999A0000000
  br i1 %cmp23, label %if.then24, label %if.end49

if.then24:                                        ; preds = %if.end22
  %33 = bitcast float* %twistAngle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #8
  %34 = bitcast %class.btVector3* %twistAxis to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %34) #8
  %call25 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %twistAxis)
  call void @_ZN21btConeTwistConstraint21computeTwistLimitInfoERK12btQuaternionRfR9btVector3(%class.btConeTwistConstraint* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qTargetTwist, float* nonnull align 4 dereferenceable(4) %twistAngle, %class.btVector3* nonnull align 4 dereferenceable(16) %twistAxis)
  %35 = load float, float* %twistAngle, align 4, !tbaa !30
  %call26 = call float @_Z4fabsf(float %35) #8
  %cmp27 = fcmp ogt float %call26, 0x3E80000000000000
  br i1 %cmp27, label %if.then28, label %if.end48

if.then28:                                        ; preds = %if.then24
  %36 = load float, float* %twistAngle, align 4, !tbaa !30
  %m_twistSpan29 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %37 = load float, float* %m_twistSpan29, align 4, !tbaa !33
  %38 = load float, float* %softness, align 4, !tbaa !30
  %mul30 = fmul float %37, %38
  %cmp31 = fcmp ogt float %36, %mul30
  br i1 %cmp31, label %if.then32, label %if.else35

if.then32:                                        ; preds = %if.then28
  %m_twistSpan33 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %39 = load float, float* %m_twistSpan33, align 4, !tbaa !33
  %40 = load float, float* %softness, align 4, !tbaa !30
  %mul34 = fmul float %39, %40
  store float %mul34, float* %twistAngle, align 4, !tbaa !30
  br label %if.end45

if.else35:                                        ; preds = %if.then28
  %41 = load float, float* %twistAngle, align 4, !tbaa !30
  %m_twistSpan36 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %42 = load float, float* %m_twistSpan36, align 4, !tbaa !33
  %fneg37 = fneg float %42
  %43 = load float, float* %softness, align 4, !tbaa !30
  %mul38 = fmul float %fneg37, %43
  %cmp39 = fcmp olt float %41, %mul38
  br i1 %cmp39, label %if.then40, label %if.end44

if.then40:                                        ; preds = %if.else35
  %m_twistSpan41 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %44 = load float, float* %m_twistSpan41, align 4, !tbaa !33
  %fneg42 = fneg float %44
  %45 = load float, float* %softness, align 4, !tbaa !30
  %mul43 = fmul float %fneg42, %45
  store float %mul43, float* %twistAngle, align 4, !tbaa !30
  br label %if.end44

if.end44:                                         ; preds = %if.then40, %if.else35
  br label %if.end45

if.end45:                                         ; preds = %if.end44, %if.then32
  %46 = bitcast %class.btQuaternion* %ref.tmp46 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %46) #8
  %call47 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %ref.tmp46, %class.btVector3* nonnull align 4 dereferenceable(16) %twistAxis, float* nonnull align 4 dereferenceable(4) %twistAngle)
  %47 = bitcast %class.btQuaternion* %qTargetTwist to i8*
  %48 = bitcast %class.btQuaternion* %ref.tmp46 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %47, i8* align 4 %48, i32 16, i1 false)
  %49 = bitcast %class.btQuaternion* %ref.tmp46 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %49) #8
  br label %if.end48

if.end48:                                         ; preds = %if.end45, %if.then24
  %50 = bitcast %class.btVector3* %twistAxis to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %50) #8
  %51 = bitcast float* %twistAngle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #8
  br label %if.end49

if.end49:                                         ; preds = %if.end48, %if.end22
  %52 = bitcast %class.btQuaternion* %ref.tmp50 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %52) #8
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %ref.tmp50, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qTargetCone, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qTargetTwist)
  %m_qTarget51 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 31
  %53 = bitcast %class.btQuaternion* %m_qTarget51 to i8*
  %54 = bitcast %class.btQuaternion* %ref.tmp50 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %53, i8* align 4 %54, i32 16, i1 false)
  %55 = bitcast %class.btQuaternion* %ref.tmp50 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %55) #8
  %56 = bitcast %class.btQuaternion* %qTargetTwist to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %56) #8
  %57 = bitcast %class.btQuaternion* %qTargetCone to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %57) #8
  %58 = bitcast %class.btVector3* %vTwisted to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %58) #8
  %59 = bitcast float* %softness to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #8
  ret void
}

; Function Attrs: nounwind
define hidden void @_ZN21btConeTwistConstraint8setParamEifi(%class.btConeTwistConstraint* %this, i32 %num, float %value, i32 %axis) unnamed_addr #5 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %num.addr = alloca i32, align 4
  %value.addr = alloca float, align 4
  %axis.addr = alloca i32, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4, !tbaa !2
  store i32 %num, i32* %num.addr, align 4, !tbaa !60
  store float %value, float* %value.addr, align 4, !tbaa !30
  store i32 %axis, i32* %axis.addr, align 4, !tbaa !60
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = load i32, i32* %num.addr, align 4, !tbaa !60
  switch i32 %0, label %sw.default [
    i32 1, label %sw.bb
    i32 2, label %sw.bb
    i32 3, label %sw.bb3
    i32 4, label %sw.bb3
  ]

sw.bb:                                            ; preds = %entry, %entry
  %1 = load i32, i32* %axis.addr, align 4, !tbaa !60
  %cmp = icmp sge i32 %1, 0
  br i1 %cmp, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %sw.bb
  %2 = load i32, i32* %axis.addr, align 4, !tbaa !60
  %cmp2 = icmp slt i32 %2, 3
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %3 = load float, float* %value.addr, align 4, !tbaa !30
  %m_linERP = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 36
  store float %3, float* %m_linERP, align 4, !tbaa !28
  %m_flags = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 34
  %4 = load i32, i32* %m_flags, align 4, !tbaa !26
  %or = or i32 %4, 2
  store i32 %or, i32* %m_flags, align 4, !tbaa !26
  br label %if.end

if.else:                                          ; preds = %land.lhs.true, %sw.bb
  %5 = load float, float* %value.addr, align 4, !tbaa !30
  %m_biasFactor = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 5
  store float %5, float* %m_biasFactor, align 4, !tbaa !35
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  br label %sw.epilog

sw.bb3:                                           ; preds = %entry, %entry
  %6 = load i32, i32* %axis.addr, align 4, !tbaa !60
  %cmp4 = icmp sge i32 %6, 0
  br i1 %cmp4, label %land.lhs.true5, label %if.else10

land.lhs.true5:                                   ; preds = %sw.bb3
  %7 = load i32, i32* %axis.addr, align 4, !tbaa !60
  %cmp6 = icmp slt i32 %7, 3
  br i1 %cmp6, label %if.then7, label %if.else10

if.then7:                                         ; preds = %land.lhs.true5
  %8 = load float, float* %value.addr, align 4, !tbaa !30
  %m_linCFM = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 35
  store float %8, float* %m_linCFM, align 4, !tbaa !27
  %m_flags8 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 34
  %9 = load i32, i32* %m_flags8, align 4, !tbaa !26
  %or9 = or i32 %9, 1
  store i32 %or9, i32* %m_flags8, align 4, !tbaa !26
  br label %if.end13

if.else10:                                        ; preds = %land.lhs.true5, %sw.bb3
  %10 = load float, float* %value.addr, align 4, !tbaa !30
  %m_angCFM = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 37
  store float %10, float* %m_angCFM, align 4, !tbaa !29
  %m_flags11 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 34
  %11 = load i32, i32* %m_flags11, align 4, !tbaa !26
  %or12 = or i32 %11, 4
  store i32 %or12, i32* %m_flags11, align 4, !tbaa !26
  br label %if.end13

if.end13:                                         ; preds = %if.else10, %if.then7
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %if.end13, %if.end
  ret void
}

; Function Attrs: nounwind
define hidden float @_ZNK21btConeTwistConstraint8getParamEii(%class.btConeTwistConstraint* %this, i32 %num, i32 %axis) unnamed_addr #5 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %num.addr = alloca i32, align 4
  %axis.addr = alloca i32, align 4
  %retVal = alloca float, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4, !tbaa !2
  store i32 %num, i32* %num.addr, align 4, !tbaa !60
  store i32 %axis, i32* %axis.addr, align 4, !tbaa !60
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = bitcast float* %retVal to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 0.000000e+00, float* %retVal, align 4, !tbaa !30
  %1 = load i32, i32* %num.addr, align 4, !tbaa !60
  switch i32 %1, label %sw.default [
    i32 1, label %sw.bb
    i32 2, label %sw.bb
    i32 3, label %sw.bb9
    i32 4, label %sw.bb9
  ]

sw.bb:                                            ; preds = %entry, %entry
  %2 = load i32, i32* %axis.addr, align 4, !tbaa !60
  %cmp = icmp sge i32 %2, 0
  br i1 %cmp, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %sw.bb
  %3 = load i32, i32* %axis.addr, align 4, !tbaa !60
  %cmp2 = icmp slt i32 %3, 3
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %m_linERP = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 36
  %4 = load float, float* %m_linERP, align 4, !tbaa !28
  store float %4, float* %retVal, align 4, !tbaa !30
  br label %if.end8

if.else:                                          ; preds = %land.lhs.true, %sw.bb
  %5 = load i32, i32* %axis.addr, align 4, !tbaa !60
  %cmp3 = icmp sge i32 %5, 3
  br i1 %cmp3, label %land.lhs.true4, label %if.else7

land.lhs.true4:                                   ; preds = %if.else
  %6 = load i32, i32* %axis.addr, align 4, !tbaa !60
  %cmp5 = icmp slt i32 %6, 6
  br i1 %cmp5, label %if.then6, label %if.else7

if.then6:                                         ; preds = %land.lhs.true4
  %m_biasFactor = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 5
  %7 = load float, float* %m_biasFactor, align 4, !tbaa !35
  store float %7, float* %retVal, align 4, !tbaa !30
  br label %if.end

if.else7:                                         ; preds = %land.lhs.true4, %if.else
  br label %if.end

if.end:                                           ; preds = %if.else7, %if.then6
  br label %if.end8

if.end8:                                          ; preds = %if.end, %if.then
  br label %sw.epilog

sw.bb9:                                           ; preds = %entry, %entry
  %8 = load i32, i32* %axis.addr, align 4, !tbaa !60
  %cmp10 = icmp sge i32 %8, 0
  br i1 %cmp10, label %land.lhs.true11, label %if.else14

land.lhs.true11:                                  ; preds = %sw.bb9
  %9 = load i32, i32* %axis.addr, align 4, !tbaa !60
  %cmp12 = icmp slt i32 %9, 3
  br i1 %cmp12, label %if.then13, label %if.else14

if.then13:                                        ; preds = %land.lhs.true11
  %m_linCFM = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 35
  %10 = load float, float* %m_linCFM, align 4, !tbaa !27
  store float %10, float* %retVal, align 4, !tbaa !30
  br label %if.end21

if.else14:                                        ; preds = %land.lhs.true11, %sw.bb9
  %11 = load i32, i32* %axis.addr, align 4, !tbaa !60
  %cmp15 = icmp sge i32 %11, 3
  br i1 %cmp15, label %land.lhs.true16, label %if.else19

land.lhs.true16:                                  ; preds = %if.else14
  %12 = load i32, i32* %axis.addr, align 4, !tbaa !60
  %cmp17 = icmp slt i32 %12, 6
  br i1 %cmp17, label %if.then18, label %if.else19

if.then18:                                        ; preds = %land.lhs.true16
  %m_angCFM = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 37
  %13 = load float, float* %m_angCFM, align 4, !tbaa !29
  store float %13, float* %retVal, align 4, !tbaa !30
  br label %if.end20

if.else19:                                        ; preds = %land.lhs.true16, %if.else14
  br label %if.end20

if.end20:                                         ; preds = %if.else19, %if.then18
  br label %if.end21

if.end21:                                         ; preds = %if.end20, %if.then13
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %if.end21, %if.end8
  %14 = load float, float* %retVal, align 4, !tbaa !30
  %15 = bitcast float* %retVal to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  ret float %14
}

define hidden void @_ZN21btConeTwistConstraint9setFramesERK11btTransformS2_(%class.btConeTwistConstraint* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %frameA, %class.btTransform* nonnull align 4 dereferenceable(64) %frameB) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %frameA.addr = alloca %class.btTransform*, align 4
  %frameB.addr = alloca %class.btTransform*, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %frameA, %class.btTransform** %frameA.addr, align 4, !tbaa !2
  store %class.btTransform* %frameB, %class.btTransform** %frameB.addr, align 4, !tbaa !2
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %frameA.addr, align 4, !tbaa !2
  %m_rbAFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_rbAFrame, %class.btTransform* nonnull align 4 dereferenceable(64) %0)
  %1 = load %class.btTransform*, %class.btTransform** %frameB.addr, align 4, !tbaa !2
  %m_rbBFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_rbBFrame, %class.btTransform* nonnull align 4 dereferenceable(64) %1)
  %2 = bitcast %class.btConeTwistConstraint* %this1 to void (%class.btConeTwistConstraint*)***
  %vtable = load void (%class.btConeTwistConstraint*)**, void (%class.btConeTwistConstraint*)*** %2, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btConeTwistConstraint*)*, void (%class.btConeTwistConstraint*)** %vtable, i64 2
  %3 = load void (%class.btConeTwistConstraint*)*, void (%class.btConeTwistConstraint*)** %vfn, align 4
  call void %3(%class.btConeTwistConstraint* %this1)
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btTypedConstraint* @_ZN17btTypedConstraintD2Ev(%class.btTypedConstraint* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = bitcast %class.btTypedConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [13 x i8*] }, { [13 x i8*] }* @_ZTV17btTypedConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  ret %class.btTypedConstraint* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN21btConeTwistConstraintD0Ev(%class.btConeTwistConstraint* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %call = call %class.btConeTwistConstraint* bitcast (%class.btTypedConstraint* (%class.btTypedConstraint*)* @_ZN17btTypedConstraintD2Ev to %class.btConeTwistConstraint* (%class.btConeTwistConstraint*)*)(%class.btConeTwistConstraint* %this1) #8
  %0 = bitcast %class.btConeTwistConstraint* %this1 to i8*
  call void @_ZN21btConeTwistConstraintdlEPv(i8* %0) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif(%class.btTypedConstraint* %this, %class.btAlignedObjectArray.1* nonnull align 1 %ca, i32 %solverBodyA, i32 %solverBodyB, float %timeStep) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  %ca.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %solverBodyA.addr = alloca i32, align 4
  %solverBodyB.addr = alloca i32, align 4
  %timeStep.addr = alloca float, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.1* %ca, %class.btAlignedObjectArray.1** %ca.addr, align 4, !tbaa !2
  store i32 %solverBodyA, i32* %solverBodyA.addr, align 4, !tbaa !60
  store i32 %solverBodyB, i32* %solverBodyB.addr, align 4, !tbaa !60
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !30
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %ca.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK21btConeTwistConstraint28calculateSerializeBufferSizeEv(%class.btConeTwistConstraint* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  ret i32 212
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZNK21btConeTwistConstraint9serializeEPvP12btSerializer(%class.btConeTwistConstraint* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %cone = alloca %struct.btConeTwistConstraintData*, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4, !tbaa !2
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = bitcast %struct.btConeTwistConstraintData** %cone to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i8*, i8** %dataBuffer.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.btConeTwistConstraintData*
  store %struct.btConeTwistConstraintData* %2, %struct.btConeTwistConstraintData** %cone, align 4, !tbaa !2
  %3 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %4 = load %struct.btConeTwistConstraintData*, %struct.btConeTwistConstraintData** %cone, align 4, !tbaa !2
  %m_typeConstraintData = getelementptr inbounds %struct.btConeTwistConstraintData, %struct.btConeTwistConstraintData* %4, i32 0, i32 0
  %5 = bitcast %struct.btTypedConstraintData* %m_typeConstraintData to i8*
  %6 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %call = call i8* @_ZNK17btTypedConstraint9serializeEPvP12btSerializer(%class.btTypedConstraint* %3, i8* %5, %class.btSerializer* %6)
  %m_rbAFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  %7 = load %struct.btConeTwistConstraintData*, %struct.btConeTwistConstraintData** %cone, align 4, !tbaa !2
  %m_rbAFrame2 = getelementptr inbounds %struct.btConeTwistConstraintData, %struct.btConeTwistConstraintData* %7, i32 0, i32 1
  call void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %m_rbAFrame, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %m_rbAFrame2)
  %m_rbBFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  %8 = load %struct.btConeTwistConstraintData*, %struct.btConeTwistConstraintData** %cone, align 4, !tbaa !2
  %m_rbBFrame3 = getelementptr inbounds %struct.btConeTwistConstraintData, %struct.btConeTwistConstraintData* %8, i32 0, i32 2
  call void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %m_rbBFrame, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %m_rbBFrame3)
  %m_swingSpan1 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %9 = load float, float* %m_swingSpan1, align 4, !tbaa !31
  %10 = load %struct.btConeTwistConstraintData*, %struct.btConeTwistConstraintData** %cone, align 4, !tbaa !2
  %m_swingSpan14 = getelementptr inbounds %struct.btConeTwistConstraintData, %struct.btConeTwistConstraintData* %10, i32 0, i32 3
  store float %9, float* %m_swingSpan14, align 4, !tbaa !78
  %m_swingSpan2 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %11 = load float, float* %m_swingSpan2, align 4, !tbaa !32
  %12 = load %struct.btConeTwistConstraintData*, %struct.btConeTwistConstraintData** %cone, align 4, !tbaa !2
  %m_swingSpan25 = getelementptr inbounds %struct.btConeTwistConstraintData, %struct.btConeTwistConstraintData* %12, i32 0, i32 4
  store float %11, float* %m_swingSpan25, align 4, !tbaa !84
  %m_twistSpan = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %13 = load float, float* %m_twistSpan, align 4, !tbaa !33
  %14 = load %struct.btConeTwistConstraintData*, %struct.btConeTwistConstraintData** %cone, align 4, !tbaa !2
  %m_twistSpan6 = getelementptr inbounds %struct.btConeTwistConstraintData, %struct.btConeTwistConstraintData* %14, i32 0, i32 5
  store float %13, float* %m_twistSpan6, align 4, !tbaa !85
  %m_limitSoftness = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 4
  %15 = load float, float* %m_limitSoftness, align 4, !tbaa !34
  %16 = load %struct.btConeTwistConstraintData*, %struct.btConeTwistConstraintData** %cone, align 4, !tbaa !2
  %m_limitSoftness7 = getelementptr inbounds %struct.btConeTwistConstraintData, %struct.btConeTwistConstraintData* %16, i32 0, i32 6
  store float %15, float* %m_limitSoftness7, align 4, !tbaa !86
  %m_biasFactor = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 5
  %17 = load float, float* %m_biasFactor, align 4, !tbaa !35
  %18 = load %struct.btConeTwistConstraintData*, %struct.btConeTwistConstraintData** %cone, align 4, !tbaa !2
  %m_biasFactor8 = getelementptr inbounds %struct.btConeTwistConstraintData, %struct.btConeTwistConstraintData* %18, i32 0, i32 7
  store float %17, float* %m_biasFactor8, align 4, !tbaa !87
  %m_relaxationFactor = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 6
  %19 = load float, float* %m_relaxationFactor, align 4, !tbaa !36
  %20 = load %struct.btConeTwistConstraintData*, %struct.btConeTwistConstraintData** %cone, align 4, !tbaa !2
  %m_relaxationFactor9 = getelementptr inbounds %struct.btConeTwistConstraintData, %struct.btConeTwistConstraintData* %20, i32 0, i32 8
  store float %19, float* %m_relaxationFactor9, align 4, !tbaa !88
  %m_damping = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 7
  %21 = load float, float* %m_damping, align 4, !tbaa !24
  %22 = load %struct.btConeTwistConstraintData*, %struct.btConeTwistConstraintData** %cone, align 4, !tbaa !2
  %m_damping10 = getelementptr inbounds %struct.btConeTwistConstraintData, %struct.btConeTwistConstraintData* %22, i32 0, i32 9
  store float %21, float* %m_damping10, align 4, !tbaa !89
  %23 = bitcast %struct.btConeTwistConstraintData** %cone to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #8
  ret i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !18
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !18
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !18
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  ret %class.btQuadWord* %this1
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !18
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !18
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !18
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !60
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !60
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %3, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4, !tbaa !30
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !30
  %7 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %9, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %8, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !30
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4, !tbaa !30
  %13 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %15, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %14, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4, !tbaa !30
  %16 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4, !tbaa !30
  %19 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  %20 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %21 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %21, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %20, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4, !tbaa !30
  %22 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #8
  %23 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %24 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %24, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %23, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4, !tbaa !30
  %25 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  %26 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %27 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %27, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %26, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4, !tbaa !30
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  %28 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #8
  %29 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #8
  %30 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #8
  %31 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #8
  %32 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #8
  %33 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #8
  %34 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #8
  %35 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  %36 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %b.addr = alloca %class.btMatrix3x3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %b, %class.btMatrix3x3** %b.addr, align 4, !tbaa !2
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %b.addr, align 4, !tbaa !2
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !18
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !30
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !30
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !30
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !30
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !30
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !30
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !30
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !30
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !30
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !30
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !30
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !30
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !30
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !30
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !30
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !30
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !30
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !30
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4, !tbaa !30
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !30
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !30
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !30
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !30
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !30
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !30
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !30
  %mul8 = fmul float %7, %9
  store float %mul8, float* %ref.tmp3, align 4, !tbaa !30
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !30
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !30
  %mul14 = fmul float %12, %14
  store float %mul14, float* %ref.tmp9, align 4, !tbaa !30
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

define linkonce_odr hidden void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !30
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !30
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !30
  %3 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !30
  %4 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  store float 1.000000e+00, float* %ref.tmp5, align 4, !tbaa !30
  %5 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !30
  %6 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !30
  %7 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  store float 0.000000e+00, float* %ref.tmp8, align 4, !tbaa !30
  %8 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  store float 1.000000e+00, float* %ref.tmp9, align 4, !tbaa !30
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %9 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  %12 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  %13 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  %14 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #8
  %15 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  store float* %_w, float** %_w.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = load float*, float** %_x.addr, align 4, !tbaa !2
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float*, float** %_z.addr, align 4, !tbaa !2
  %4 = load float*, float** %_w.addr, align 4, !tbaa !2
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4)
  ret %class.btQuaternion* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %m_basis, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  store float* %_w, float** %_w.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !30
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !30
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !30
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !30
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !30
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !30
  %6 = load float*, float** %_w.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !30
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4, !tbaa !30
  ret %class.btQuadWord* %this1
}

define linkonce_odr hidden void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %xs = alloca float, align 4
  %ys = alloca float, align 4
  %zs = alloca float, align 4
  %wx = alloca float, align 4
  %wy = alloca float, align 4
  %wz = alloca float, align 4
  %xx = alloca float, align 4
  %xy = alloca float, align 4
  %xz = alloca float, align 4
  %yy = alloca float, align 4
  %yz = alloca float, align 4
  %zz = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp34 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast float* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %1)
  store float %call, float* %d, align 4, !tbaa !30
  %2 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load float, float* %d, align 4, !tbaa !30
  %div = fdiv float 2.000000e+00, %3
  store float %div, float* %s, align 4, !tbaa !30
  %4 = bitcast float* %xs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %6 = bitcast %class.btQuaternion* %5 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %6)
  %7 = load float, float* %call2, align 4, !tbaa !30
  %8 = load float, float* %s, align 4, !tbaa !30
  %mul = fmul float %7, %8
  store float %mul, float* %xs, align 4, !tbaa !30
  %9 = bitcast float* %ys to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %11)
  %12 = load float, float* %call3, align 4, !tbaa !30
  %13 = load float, float* %s, align 4, !tbaa !30
  %mul4 = fmul float %12, %13
  store float %mul4, float* %ys, align 4, !tbaa !30
  %14 = bitcast float* %zs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #8
  %15 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %16 = bitcast %class.btQuaternion* %15 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %16)
  %17 = load float, float* %call5, align 4, !tbaa !30
  %18 = load float, float* %s, align 4, !tbaa !30
  %mul6 = fmul float %17, %18
  store float %mul6, float* %zs, align 4, !tbaa !30
  %19 = bitcast float* %wx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  %20 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %21 = bitcast %class.btQuaternion* %20 to %class.btQuadWord*
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %21)
  %22 = load float, float* %call7, align 4, !tbaa !30
  %23 = load float, float* %xs, align 4, !tbaa !30
  %mul8 = fmul float %22, %23
  store float %mul8, float* %wx, align 4, !tbaa !30
  %24 = bitcast float* %wy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #8
  %25 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %26 = bitcast %class.btQuaternion* %25 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %26)
  %27 = load float, float* %call9, align 4, !tbaa !30
  %28 = load float, float* %ys, align 4, !tbaa !30
  %mul10 = fmul float %27, %28
  store float %mul10, float* %wy, align 4, !tbaa !30
  %29 = bitcast float* %wz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #8
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %31)
  %32 = load float, float* %call11, align 4, !tbaa !30
  %33 = load float, float* %zs, align 4, !tbaa !30
  %mul12 = fmul float %32, %33
  store float %mul12, float* %wz, align 4, !tbaa !30
  %34 = bitcast float* %xx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #8
  %35 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %36 = bitcast %class.btQuaternion* %35 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %36)
  %37 = load float, float* %call13, align 4, !tbaa !30
  %38 = load float, float* %xs, align 4, !tbaa !30
  %mul14 = fmul float %37, %38
  store float %mul14, float* %xx, align 4, !tbaa !30
  %39 = bitcast float* %xy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #8
  %40 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %41 = bitcast %class.btQuaternion* %40 to %class.btQuadWord*
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %41)
  %42 = load float, float* %call15, align 4, !tbaa !30
  %43 = load float, float* %ys, align 4, !tbaa !30
  %mul16 = fmul float %42, %43
  store float %mul16, float* %xy, align 4, !tbaa !30
  %44 = bitcast float* %xz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #8
  %45 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %46 = bitcast %class.btQuaternion* %45 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %46)
  %47 = load float, float* %call17, align 4, !tbaa !30
  %48 = load float, float* %zs, align 4, !tbaa !30
  %mul18 = fmul float %47, %48
  store float %mul18, float* %xz, align 4, !tbaa !30
  %49 = bitcast float* %yy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #8
  %50 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %51 = bitcast %class.btQuaternion* %50 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %51)
  %52 = load float, float* %call19, align 4, !tbaa !30
  %53 = load float, float* %ys, align 4, !tbaa !30
  %mul20 = fmul float %52, %53
  store float %mul20, float* %yy, align 4, !tbaa !30
  %54 = bitcast float* %yz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #8
  %55 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %56 = bitcast %class.btQuaternion* %55 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %56)
  %57 = load float, float* %call21, align 4, !tbaa !30
  %58 = load float, float* %zs, align 4, !tbaa !30
  %mul22 = fmul float %57, %58
  store float %mul22, float* %yz, align 4, !tbaa !30
  %59 = bitcast float* %zz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #8
  %60 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %61 = bitcast %class.btQuaternion* %60 to %class.btQuadWord*
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %61)
  %62 = load float, float* %call23, align 4, !tbaa !30
  %63 = load float, float* %zs, align 4, !tbaa !30
  %mul24 = fmul float %62, %63
  store float %mul24, float* %zz, align 4, !tbaa !30
  %64 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #8
  %65 = load float, float* %yy, align 4, !tbaa !30
  %66 = load float, float* %zz, align 4, !tbaa !30
  %add = fadd float %65, %66
  %sub = fsub float 1.000000e+00, %add
  store float %sub, float* %ref.tmp, align 4, !tbaa !30
  %67 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #8
  %68 = load float, float* %xy, align 4, !tbaa !30
  %69 = load float, float* %wz, align 4, !tbaa !30
  %sub26 = fsub float %68, %69
  store float %sub26, float* %ref.tmp25, align 4, !tbaa !30
  %70 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #8
  %71 = load float, float* %xz, align 4, !tbaa !30
  %72 = load float, float* %wy, align 4, !tbaa !30
  %add28 = fadd float %71, %72
  store float %add28, float* %ref.tmp27, align 4, !tbaa !30
  %73 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #8
  %74 = load float, float* %xy, align 4, !tbaa !30
  %75 = load float, float* %wz, align 4, !tbaa !30
  %add30 = fadd float %74, %75
  store float %add30, float* %ref.tmp29, align 4, !tbaa !30
  %76 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #8
  %77 = load float, float* %xx, align 4, !tbaa !30
  %78 = load float, float* %zz, align 4, !tbaa !30
  %add32 = fadd float %77, %78
  %sub33 = fsub float 1.000000e+00, %add32
  store float %sub33, float* %ref.tmp31, align 4, !tbaa !30
  %79 = bitcast float* %ref.tmp34 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #8
  %80 = load float, float* %yz, align 4, !tbaa !30
  %81 = load float, float* %wx, align 4, !tbaa !30
  %sub35 = fsub float %80, %81
  store float %sub35, float* %ref.tmp34, align 4, !tbaa !30
  %82 = bitcast float* %ref.tmp36 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #8
  %83 = load float, float* %xz, align 4, !tbaa !30
  %84 = load float, float* %wy, align 4, !tbaa !30
  %sub37 = fsub float %83, %84
  store float %sub37, float* %ref.tmp36, align 4, !tbaa !30
  %85 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %85) #8
  %86 = load float, float* %yz, align 4, !tbaa !30
  %87 = load float, float* %wx, align 4, !tbaa !30
  %add39 = fadd float %86, %87
  store float %add39, float* %ref.tmp38, align 4, !tbaa !30
  %88 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #8
  %89 = load float, float* %xx, align 4, !tbaa !30
  %90 = load float, float* %yy, align 4, !tbaa !30
  %add41 = fadd float %89, %90
  %sub42 = fsub float 1.000000e+00, %add41
  store float %sub42, float* %ref.tmp40, align 4, !tbaa !30
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp34, float* nonnull align 4 dereferenceable(4) %ref.tmp36, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp40)
  %91 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #8
  %92 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #8
  %93 = bitcast float* %ref.tmp36 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #8
  %94 = bitcast float* %ref.tmp34 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #8
  %95 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #8
  %96 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #8
  %97 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #8
  %98 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #8
  %99 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #8
  %100 = bitcast float* %zz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #8
  %101 = bitcast float* %yz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #8
  %102 = bitcast float* %yy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #8
  %103 = bitcast float* %xz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #8
  %104 = bitcast float* %xy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #8
  %105 = bitcast float* %xx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #8
  %106 = bitcast float* %wz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #8
  %107 = bitcast float* %wy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #8
  %108 = bitcast float* %wx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #8
  %109 = bitcast float* %zs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #8
  %110 = bitcast float* %ys to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #8
  %111 = bitcast float* %xs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #8
  %112 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #8
  %113 = bitcast float* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #8
  ret void
}

define linkonce_odr hidden float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

define linkonce_odr hidden float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !30
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call, align 4, !tbaa !30
  %mul = fmul float %1, %4
  %5 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %5, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %6 = load float, float* %arrayidx3, align 4, !tbaa !30
  %7 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %8 = bitcast %class.btQuaternion* %7 to %class.btQuadWord*
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %8)
  %9 = load float, float* %call4, align 4, !tbaa !30
  %mul5 = fmul float %6, %9
  %add = fadd float %mul, %mul5
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %11 = load float, float* %arrayidx7, align 4, !tbaa !30
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %13)
  %14 = load float, float* %call8, align 4, !tbaa !30
  %mul9 = fmul float %11, %14
  %add10 = fadd float %add, %mul9
  %15 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats11 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %15, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 3
  %16 = load float, float* %arrayidx12, align 4, !tbaa !30
  %17 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %18 = bitcast %class.btQuaternion* %17 to %class.btQuadWord*
  %m_floats13 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %18, i32 0, i32 0
  %arrayidx14 = getelementptr inbounds [4 x float], [4 x float]* %m_floats13, i32 0, i32 3
  %19 = load float, float* %arrayidx14, align 4, !tbaa !30
  %mul15 = fmul float %16, %19
  %add16 = fadd float %add10, %mul15
  ret float %add16
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* returned %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0)
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %1
}

define linkonce_odr hidden void @_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf(%class.btTransform* nonnull align 4 dereferenceable(64) %transform0, %class.btTransform* nonnull align 4 dereferenceable(64) %transform1, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, float* nonnull align 4 dereferenceable(4) %angle) #0 comdat {
entry:
  %transform0.addr = alloca %class.btTransform*, align 4
  %transform1.addr = alloca %class.btTransform*, align 4
  %axis.addr = alloca %class.btVector3*, align 4
  %angle.addr = alloca float*, align 4
  %dmat = alloca %class.btMatrix3x3, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %dorn = alloca %class.btQuaternion, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %len = alloca float, align 4
  %ref.tmp12 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  store %class.btTransform* %transform0, %class.btTransform** %transform0.addr, align 4, !tbaa !2
  store %class.btTransform* %transform1, %class.btTransform** %transform1.addr, align 4, !tbaa !2
  store %class.btVector3* %axis, %class.btVector3** %axis.addr, align 4, !tbaa !2
  store float* %angle, float** %angle.addr, align 4, !tbaa !2
  %0 = bitcast %class.btMatrix3x3* %dmat to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %0) #8
  %1 = load %class.btTransform*, %class.btTransform** %transform1.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %1)
  %2 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %2) #8
  %3 = load %class.btTransform*, %class.btTransform** %transform0.addr, align 4, !tbaa !2
  %call1 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %3)
  call void @_ZNK11btMatrix3x37inverseEv(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* %call1)
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %dmat, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp)
  %4 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %4) #8
  %5 = bitcast %class.btQuaternion* %dorn to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #8
  %call2 = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %dorn)
  call void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %dmat, %class.btQuaternion* nonnull align 4 dereferenceable(16) %dorn)
  %call3 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %dorn)
  %call4 = call float @_ZNK12btQuaternion8getAngleEv(%class.btQuaternion* %dorn)
  %6 = load float*, float** %angle.addr, align 4, !tbaa !2
  store float %call4, float* %6, align 4, !tbaa !30
  %7 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #8
  %8 = bitcast %class.btQuaternion* %dorn to %class.btQuadWord*
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %8)
  %9 = bitcast %class.btQuaternion* %dorn to %class.btQuadWord*
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %9)
  %10 = bitcast %class.btQuaternion* %dorn to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %10)
  %call9 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp5, float* nonnull align 4 dereferenceable(4) %call6, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call8)
  %11 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %12 = bitcast %class.btVector3* %11 to i8*
  %13 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false), !tbaa.struct !18
  %14 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %14) #8
  %15 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %15)
  %arrayidx = getelementptr inbounds float, float* %call10, i32 3
  store float 0.000000e+00, float* %arrayidx, align 4, !tbaa !30
  %16 = bitcast float* %len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  %17 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %call11 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %17)
  store float %call11, float* %len, align 4, !tbaa !30
  %18 = load float, float* %len, align 4, !tbaa !30
  %cmp = fcmp olt float %18, 0x3D10000000000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %19 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %19) #8
  %20 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #8
  store float 1.000000e+00, float* %ref.tmp13, align 4, !tbaa !30
  %21 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #8
  store float 0.000000e+00, float* %ref.tmp14, align 4, !tbaa !30
  %22 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #8
  store float 0.000000e+00, float* %ref.tmp15, align 4, !tbaa !30
  %call16 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp15)
  %23 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %24 = bitcast %class.btVector3* %23 to i8*
  %25 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %24, i8* align 4 %25, i32 16, i1 false), !tbaa.struct !18
  %26 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #8
  %27 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #8
  %28 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #8
  %29 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #8
  br label %if.end

if.else:                                          ; preds = %entry
  %30 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #8
  %31 = load float, float* %len, align 4, !tbaa !30
  %call18 = call float @_Z6btSqrtf(float %31)
  store float %call18, float* %ref.tmp17, align 4, !tbaa !30
  %32 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %call19 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %32, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  %33 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %34 = bitcast float* %len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #8
  %35 = bitcast %class.btQuaternion* %dorn to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %35) #8
  %36 = bitcast %class.btMatrix3x3* %dmat to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %36) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x37inverseEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %co = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %det = alloca float, align 4
  %s = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp26 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %1 = bitcast %class.btVector3* %co to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #8
  %2 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %call = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 1, i32 2, i32 2)
  store float %call, float* %ref.tmp, align 4, !tbaa !30
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %call3 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 2, i32 2, i32 0)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !30
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %call5 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 0, i32 2, i32 1)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !30
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %co, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %5 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  %6 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %7 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  %8 = bitcast float* %det to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this1, i32 0)
  %call8 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call7, %class.btVector3* nonnull align 4 dereferenceable(16) %co)
  store float %call8, float* %det, align 4, !tbaa !30
  %9 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  %10 = load float, float* %det, align 4, !tbaa !30
  %div = fdiv float 1.000000e+00, %10
  store float %div, float* %s, align 4, !tbaa !30
  %11 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %co)
  %12 = load float, float* %call10, align 4, !tbaa !30
  %13 = load float, float* %s, align 4, !tbaa !30
  %mul = fmul float %12, %13
  store float %mul, float* %ref.tmp9, align 4, !tbaa !30
  %14 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #8
  %call12 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 2, i32 2, i32 1)
  %15 = load float, float* %s, align 4, !tbaa !30
  %mul13 = fmul float %call12, %15
  store float %mul13, float* %ref.tmp11, align 4, !tbaa !30
  %16 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  %call15 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 1, i32 1, i32 2)
  %17 = load float, float* %s, align 4, !tbaa !30
  %mul16 = fmul float %call15, %17
  store float %mul16, float* %ref.tmp14, align 4, !tbaa !30
  %18 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #8
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %co)
  %19 = load float, float* %call18, align 4, !tbaa !30
  %20 = load float, float* %s, align 4, !tbaa !30
  %mul19 = fmul float %19, %20
  store float %mul19, float* %ref.tmp17, align 4, !tbaa !30
  %21 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #8
  %call21 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 0, i32 2, i32 2)
  %22 = load float, float* %s, align 4, !tbaa !30
  %mul22 = fmul float %call21, %22
  store float %mul22, float* %ref.tmp20, align 4, !tbaa !30
  %23 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #8
  %call24 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 2, i32 1, i32 0)
  %24 = load float, float* %s, align 4, !tbaa !30
  %mul25 = fmul float %call24, %24
  store float %mul25, float* %ref.tmp23, align 4, !tbaa !30
  %25 = bitcast float* %ref.tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  %call27 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %co)
  %26 = load float, float* %call27, align 4, !tbaa !30
  %27 = load float, float* %s, align 4, !tbaa !30
  %mul28 = fmul float %26, %27
  store float %mul28, float* %ref.tmp26, align 4, !tbaa !30
  %28 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #8
  %call30 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 1, i32 2, i32 0)
  %29 = load float, float* %s, align 4, !tbaa !30
  %mul31 = fmul float %call30, %29
  store float %mul31, float* %ref.tmp29, align 4, !tbaa !30
  %30 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #8
  %call33 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 0, i32 1, i32 1)
  %31 = load float, float* %s, align 4, !tbaa !30
  %mul34 = fmul float %call33, %31
  store float %mul34, float* %ref.tmp32, align 4, !tbaa !30
  %call35 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp26, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  %32 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #8
  %33 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #8
  %34 = bitcast float* %ref.tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #8
  %35 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  %36 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #8
  %37 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #8
  %38 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #8
  %39 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #8
  %40 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #8
  %41 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #8
  %42 = bitcast float* %det to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #8
  %43 = bitcast %class.btVector3* %co to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %43) #8
  ret void
}

define linkonce_odr hidden void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %trace = alloca float, align 4
  %temp = alloca [4 x float], align 16
  %s = alloca float, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %s64 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast float* %trace to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %1 = load float, float* %call, align 4, !tbaa !30
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx3)
  %2 = load float, float* %call4, align 4, !tbaa !30
  %add = fadd float %1, %2
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx6)
  %3 = load float, float* %call7, align 4, !tbaa !30
  %add8 = fadd float %add, %3
  store float %add8, float* %trace, align 4, !tbaa !30
  %4 = bitcast [4 x float]* %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #8
  %5 = load float, float* %trace, align 4, !tbaa !30
  %cmp = fcmp ogt float %5, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %6 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load float, float* %trace, align 4, !tbaa !30
  %add9 = fadd float %7, 1.000000e+00
  %call10 = call float @_Z6btSqrtf(float %add9)
  store float %call10, float* %s, align 4, !tbaa !30
  %8 = load float, float* %s, align 4, !tbaa !30
  %mul = fmul float %8, 5.000000e-01
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  store float %mul, float* %arrayidx11, align 4, !tbaa !30
  %9 = load float, float* %s, align 4, !tbaa !30
  %div = fdiv float 5.000000e-01, %9
  store float %div, float* %s, align 4, !tbaa !30
  %m_el12 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el12, i32 0, i32 2
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx13)
  %10 = load float, float* %call14, align 4, !tbaa !30
  %m_el15 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el15, i32 0, i32 1
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx16)
  %11 = load float, float* %call17, align 4, !tbaa !30
  %sub = fsub float %10, %11
  %12 = load float, float* %s, align 4, !tbaa !30
  %mul18 = fmul float %sub, %12
  %arrayidx19 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 0
  store float %mul18, float* %arrayidx19, align 16, !tbaa !30
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 0
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %13 = load float, float* %call22, align 4, !tbaa !30
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx24)
  %14 = load float, float* %call25, align 4, !tbaa !30
  %sub26 = fsub float %13, %14
  %15 = load float, float* %s, align 4, !tbaa !30
  %mul27 = fmul float %sub26, %15
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 1
  store float %mul27, float* %arrayidx28, align 4, !tbaa !30
  %m_el29 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el29, i32 0, i32 1
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx30)
  %16 = load float, float* %call31, align 4, !tbaa !30
  %m_el32 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx33 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el32, i32 0, i32 0
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx33)
  %17 = load float, float* %call34, align 4, !tbaa !30
  %sub35 = fsub float %16, %17
  %18 = load float, float* %s, align 4, !tbaa !30
  %mul36 = fmul float %sub35, %18
  %arrayidx37 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 2
  store float %mul36, float* %arrayidx37, align 8, !tbaa !30
  %19 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  br label %if.end

if.else:                                          ; preds = %entry
  %20 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #8
  %m_el38 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx39 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el38, i32 0, i32 0
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx39)
  %21 = load float, float* %call40, align 4, !tbaa !30
  %m_el41 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx42 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el41, i32 0, i32 1
  %call43 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx42)
  %22 = load float, float* %call43, align 4, !tbaa !30
  %cmp44 = fcmp olt float %21, %22
  br i1 %cmp44, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else
  %m_el45 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx46 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el45, i32 0, i32 1
  %call47 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx46)
  %23 = load float, float* %call47, align 4, !tbaa !30
  %m_el48 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx49 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el48, i32 0, i32 2
  %call50 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx49)
  %24 = load float, float* %call50, align 4, !tbaa !30
  %cmp51 = fcmp olt float %23, %24
  %25 = zext i1 %cmp51 to i64
  %cond = select i1 %cmp51, i32 2, i32 1
  br label %cond.end

cond.false:                                       ; preds = %if.else
  %m_el52 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx53 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el52, i32 0, i32 0
  %call54 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx53)
  %26 = load float, float* %call54, align 4, !tbaa !30
  %m_el55 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx56 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el55, i32 0, i32 2
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx56)
  %27 = load float, float* %call57, align 4, !tbaa !30
  %cmp58 = fcmp olt float %26, %27
  %28 = zext i1 %cmp58 to i64
  %cond59 = select i1 %cmp58, i32 2, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond60 = phi i32 [ %cond, %cond.true ], [ %cond59, %cond.false ]
  store i32 %cond60, i32* %i, align 4, !tbaa !60
  %29 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #8
  %30 = load i32, i32* %i, align 4, !tbaa !60
  %add61 = add nsw i32 %30, 1
  %rem = srem i32 %add61, 3
  store i32 %rem, i32* %j, align 4, !tbaa !60
  %31 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #8
  %32 = load i32, i32* %i, align 4, !tbaa !60
  %add62 = add nsw i32 %32, 2
  %rem63 = srem i32 %add62, 3
  store i32 %rem63, i32* %k, align 4, !tbaa !60
  %33 = bitcast float* %s64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #8
  %m_el65 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %34 = load i32, i32* %i, align 4, !tbaa !60
  %arrayidx66 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el65, i32 0, i32 %34
  %call67 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx66)
  %35 = load i32, i32* %i, align 4, !tbaa !60
  %arrayidx68 = getelementptr inbounds float, float* %call67, i32 %35
  %36 = load float, float* %arrayidx68, align 4, !tbaa !30
  %m_el69 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %37 = load i32, i32* %j, align 4, !tbaa !60
  %arrayidx70 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el69, i32 0, i32 %37
  %call71 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx70)
  %38 = load i32, i32* %j, align 4, !tbaa !60
  %arrayidx72 = getelementptr inbounds float, float* %call71, i32 %38
  %39 = load float, float* %arrayidx72, align 4, !tbaa !30
  %sub73 = fsub float %36, %39
  %m_el74 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %40 = load i32, i32* %k, align 4, !tbaa !60
  %arrayidx75 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el74, i32 0, i32 %40
  %call76 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx75)
  %41 = load i32, i32* %k, align 4, !tbaa !60
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 %41
  %42 = load float, float* %arrayidx77, align 4, !tbaa !30
  %sub78 = fsub float %sub73, %42
  %add79 = fadd float %sub78, 1.000000e+00
  %call80 = call float @_Z6btSqrtf(float %add79)
  store float %call80, float* %s64, align 4, !tbaa !30
  %43 = load float, float* %s64, align 4, !tbaa !30
  %mul81 = fmul float %43, 5.000000e-01
  %44 = load i32, i32* %i, align 4, !tbaa !60
  %arrayidx82 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %44
  store float %mul81, float* %arrayidx82, align 4, !tbaa !30
  %45 = load float, float* %s64, align 4, !tbaa !30
  %div83 = fdiv float 5.000000e-01, %45
  store float %div83, float* %s64, align 4, !tbaa !30
  %m_el84 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %46 = load i32, i32* %k, align 4, !tbaa !60
  %arrayidx85 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el84, i32 0, i32 %46
  %call86 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx85)
  %47 = load i32, i32* %j, align 4, !tbaa !60
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 %47
  %48 = load float, float* %arrayidx87, align 4, !tbaa !30
  %m_el88 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %49 = load i32, i32* %j, align 4, !tbaa !60
  %arrayidx89 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el88, i32 0, i32 %49
  %call90 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx89)
  %50 = load i32, i32* %k, align 4, !tbaa !60
  %arrayidx91 = getelementptr inbounds float, float* %call90, i32 %50
  %51 = load float, float* %arrayidx91, align 4, !tbaa !30
  %sub92 = fsub float %48, %51
  %52 = load float, float* %s64, align 4, !tbaa !30
  %mul93 = fmul float %sub92, %52
  %arrayidx94 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  store float %mul93, float* %arrayidx94, align 4, !tbaa !30
  %m_el95 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %53 = load i32, i32* %j, align 4, !tbaa !60
  %arrayidx96 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el95, i32 0, i32 %53
  %call97 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx96)
  %54 = load i32, i32* %i, align 4, !tbaa !60
  %arrayidx98 = getelementptr inbounds float, float* %call97, i32 %54
  %55 = load float, float* %arrayidx98, align 4, !tbaa !30
  %m_el99 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %56 = load i32, i32* %i, align 4, !tbaa !60
  %arrayidx100 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el99, i32 0, i32 %56
  %call101 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx100)
  %57 = load i32, i32* %j, align 4, !tbaa !60
  %arrayidx102 = getelementptr inbounds float, float* %call101, i32 %57
  %58 = load float, float* %arrayidx102, align 4, !tbaa !30
  %add103 = fadd float %55, %58
  %59 = load float, float* %s64, align 4, !tbaa !30
  %mul104 = fmul float %add103, %59
  %60 = load i32, i32* %j, align 4, !tbaa !60
  %arrayidx105 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %60
  store float %mul104, float* %arrayidx105, align 4, !tbaa !30
  %m_el106 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %61 = load i32, i32* %k, align 4, !tbaa !60
  %arrayidx107 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el106, i32 0, i32 %61
  %call108 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx107)
  %62 = load i32, i32* %i, align 4, !tbaa !60
  %arrayidx109 = getelementptr inbounds float, float* %call108, i32 %62
  %63 = load float, float* %arrayidx109, align 4, !tbaa !30
  %m_el110 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %64 = load i32, i32* %i, align 4, !tbaa !60
  %arrayidx111 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el110, i32 0, i32 %64
  %call112 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx111)
  %65 = load i32, i32* %k, align 4, !tbaa !60
  %arrayidx113 = getelementptr inbounds float, float* %call112, i32 %65
  %66 = load float, float* %arrayidx113, align 4, !tbaa !30
  %add114 = fadd float %63, %66
  %67 = load float, float* %s64, align 4, !tbaa !30
  %mul115 = fmul float %add114, %67
  %68 = load i32, i32* %k, align 4, !tbaa !60
  %arrayidx116 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %68
  store float %mul115, float* %arrayidx116, align 4, !tbaa !30
  %69 = bitcast float* %s64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #8
  %70 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #8
  %71 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #8
  %72 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #8
  br label %if.end

if.end:                                           ; preds = %cond.end, %if.then
  %73 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %74 = bitcast %class.btQuaternion* %73 to %class.btQuadWord*
  %arrayidx117 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 0
  %arrayidx118 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 1
  %arrayidx119 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 2
  %arrayidx120 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %74, float* nonnull align 4 dereferenceable(4) %arrayidx117, float* nonnull align 4 dereferenceable(4) %arrayidx118, float* nonnull align 4 dereferenceable(4) %arrayidx119, float* nonnull align 4 dereferenceable(4) %arrayidx120)
  %75 = bitcast [4 x float]* %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %75) #8
  %76 = bitcast float* %trace to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #3 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !30
  %0 = load float, float* %y.addr, align 4, !tbaa !30
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !30
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !30
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  ret %class.btVector3* %call
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this, i32 %r1, i32 %c1, i32 %r2, i32 %c2) #5 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %r1.addr = alloca i32, align 4
  %c1.addr = alloca i32, align 4
  %r2.addr = alloca i32, align 4
  %c2.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %r1, i32* %r1.addr, align 4, !tbaa !60
  store i32 %c1, i32* %c1.addr, align 4, !tbaa !60
  store i32 %r2, i32* %r2.addr, align 4, !tbaa !60
  store i32 %c2, i32* %c2.addr, align 4, !tbaa !60
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %r1.addr, align 4, !tbaa !60
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx)
  %1 = load i32, i32* %c1.addr, align 4, !tbaa !60
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 %1
  %2 = load float, float* %arrayidx2, align 4, !tbaa !30
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %3 = load i32, i32* %r2.addr, align 4, !tbaa !60
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 %3
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx4)
  %4 = load i32, i32* %c2.addr, align 4, !tbaa !60
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %4
  %5 = load float, float* %arrayidx6, align 4, !tbaa !30
  %mul = fmul float %2, %5
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %6 = load i32, i32* %r1.addr, align 4, !tbaa !60
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 %6
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx8)
  %7 = load i32, i32* %c2.addr, align 4, !tbaa !60
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %7
  %8 = load float, float* %arrayidx10, align 4, !tbaa !30
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %9 = load i32, i32* %r2.addr, align 4, !tbaa !60
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 %9
  %call13 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx12)
  %10 = load i32, i32* %c1.addr, align 4, !tbaa !60
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 %10
  %11 = load float, float* %arrayidx14, align 4, !tbaa !30
  %mul15 = fmul float %8, %11
  %sub = fsub float %mul, %mul15
  ret float %sub
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  store float* %_w, float** %_w.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !30
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !30
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !30
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !30
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !30
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !30
  %6 = load float*, float** %_w.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !30
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4, !tbaa !30
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #6

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call, float* %ref.tmp, align 4, !tbaa !30
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call2, float* %ref.tmp1, align 4, !tbaa !30
  %6 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call4, float* %ref.tmp3, align 4, !tbaa !30
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %9 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #6

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK12btQuaternionRK9btVector3(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q, %class.btVector3* nonnull align 4 dereferenceable(16) %w) #2 comdat {
entry:
  %q.addr = alloca %class.btQuaternion*, align 4
  %w.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  store %class.btVector3* %w, %class.btVector3** %w.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %2 = bitcast %class.btQuaternion* %1 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %2)
  %3 = load float, float* %call, align 4, !tbaa !30
  %4 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %4)
  %5 = load float, float* %call1, align 4, !tbaa !30
  %mul = fmul float %3, %5
  %6 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %7)
  %8 = load float, float* %call2, align 4, !tbaa !30
  %9 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %9)
  %10 = load float, float* %call3, align 4, !tbaa !30
  %mul4 = fmul float %8, %10
  %add = fadd float %mul, %mul4
  %11 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %12 = bitcast %class.btQuaternion* %11 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %12)
  %13 = load float, float* %call5, align 4, !tbaa !30
  %14 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %14)
  %15 = load float, float* %call6, align 4, !tbaa !30
  %mul7 = fmul float %13, %15
  %sub = fsub float %add, %mul7
  store float %sub, float* %ref.tmp, align 4, !tbaa !30
  %16 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  %17 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %18 = bitcast %class.btQuaternion* %17 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %18)
  %19 = load float, float* %call9, align 4, !tbaa !30
  %20 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %20)
  %21 = load float, float* %call10, align 4, !tbaa !30
  %mul11 = fmul float %19, %21
  %22 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %23 = bitcast %class.btQuaternion* %22 to %class.btQuadWord*
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %23)
  %24 = load float, float* %call12, align 4, !tbaa !30
  %25 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %25)
  %26 = load float, float* %call13, align 4, !tbaa !30
  %mul14 = fmul float %24, %26
  %add15 = fadd float %mul11, %mul14
  %27 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %28 = bitcast %class.btQuaternion* %27 to %class.btQuadWord*
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %28)
  %29 = load float, float* %call16, align 4, !tbaa !30
  %30 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %30)
  %31 = load float, float* %call17, align 4, !tbaa !30
  %mul18 = fmul float %29, %31
  %sub19 = fsub float %add15, %mul18
  store float %sub19, float* %ref.tmp8, align 4, !tbaa !30
  %32 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #8
  %33 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %34 = bitcast %class.btQuaternion* %33 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %34)
  %35 = load float, float* %call21, align 4, !tbaa !30
  %36 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %36)
  %37 = load float, float* %call22, align 4, !tbaa !30
  %mul23 = fmul float %35, %37
  %38 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %39 = bitcast %class.btQuaternion* %38 to %class.btQuadWord*
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %39)
  %40 = load float, float* %call24, align 4, !tbaa !30
  %41 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %41)
  %42 = load float, float* %call25, align 4, !tbaa !30
  %mul26 = fmul float %40, %42
  %add27 = fadd float %mul23, %mul26
  %43 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %44 = bitcast %class.btQuaternion* %43 to %class.btQuadWord*
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %44)
  %45 = load float, float* %call28, align 4, !tbaa !30
  %46 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %46)
  %47 = load float, float* %call29, align 4, !tbaa !30
  %mul30 = fmul float %45, %47
  %sub31 = fsub float %add27, %mul30
  store float %sub31, float* %ref.tmp20, align 4, !tbaa !30
  %48 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #8
  %49 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %50 = bitcast %class.btQuaternion* %49 to %class.btQuadWord*
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %50)
  %51 = load float, float* %call33, align 4, !tbaa !30
  %fneg = fneg float %51
  %52 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %52)
  %53 = load float, float* %call34, align 4, !tbaa !30
  %mul35 = fmul float %fneg, %53
  %54 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %55 = bitcast %class.btQuaternion* %54 to %class.btQuadWord*
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %55)
  %56 = load float, float* %call36, align 4, !tbaa !30
  %57 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %57)
  %58 = load float, float* %call37, align 4, !tbaa !30
  %mul38 = fmul float %56, %58
  %sub39 = fsub float %mul35, %mul38
  %59 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %60 = bitcast %class.btQuaternion* %59 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %60)
  %61 = load float, float* %call40, align 4, !tbaa !30
  %62 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %62)
  %63 = load float, float* %call41, align 4, !tbaa !30
  %mul42 = fmul float %61, %63
  %sub43 = fsub float %sub39, %mul42
  store float %sub43, float* %ref.tmp32, align 4, !tbaa !30
  %call44 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  %64 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #8
  %65 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #8
  %66 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #8
  %67 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #8
  ret void
}

define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp58 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %2, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  %3 = load float, float* %arrayidx, align 4, !tbaa !30
  %4 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %5 = bitcast %class.btQuaternion* %4 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %5)
  %6 = load float, float* %call, align 4, !tbaa !30
  %mul = fmul float %3, %6
  %7 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %7, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %8 = load float, float* %arrayidx3, align 4, !tbaa !30
  %9 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %10 = bitcast %class.btQuaternion* %9 to %class.btQuadWord*
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 3
  %11 = load float, float* %arrayidx5, align 4, !tbaa !30
  %mul6 = fmul float %8, %11
  %add = fadd float %mul, %mul6
  %12 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats7 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %12, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 1
  %13 = load float, float* %arrayidx8, align 4, !tbaa !30
  %14 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %15 = bitcast %class.btQuaternion* %14 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %15)
  %16 = load float, float* %call9, align 4, !tbaa !30
  %mul10 = fmul float %13, %16
  %add11 = fadd float %add, %mul10
  %17 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats12 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %17, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %18 = load float, float* %arrayidx13, align 4, !tbaa !30
  %19 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %20 = bitcast %class.btQuaternion* %19 to %class.btQuadWord*
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %20)
  %21 = load float, float* %call14, align 4, !tbaa !30
  %mul15 = fmul float %18, %21
  %sub = fsub float %add11, %mul15
  store float %sub, float* %ref.tmp, align 4, !tbaa !30
  %22 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #8
  %23 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats17 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %23, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 3
  %24 = load float, float* %arrayidx18, align 4, !tbaa !30
  %25 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %26 = bitcast %class.btQuaternion* %25 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %26)
  %27 = load float, float* %call19, align 4, !tbaa !30
  %mul20 = fmul float %24, %27
  %28 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats21 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %28, i32 0, i32 0
  %arrayidx22 = getelementptr inbounds [4 x float], [4 x float]* %m_floats21, i32 0, i32 1
  %29 = load float, float* %arrayidx22, align 4, !tbaa !30
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %m_floats23 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %31, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [4 x float], [4 x float]* %m_floats23, i32 0, i32 3
  %32 = load float, float* %arrayidx24, align 4, !tbaa !30
  %mul25 = fmul float %29, %32
  %add26 = fadd float %mul20, %mul25
  %33 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats27 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %33, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 2
  %34 = load float, float* %arrayidx28, align 4, !tbaa !30
  %35 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %36 = bitcast %class.btQuaternion* %35 to %class.btQuadWord*
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %36)
  %37 = load float, float* %call29, align 4, !tbaa !30
  %mul30 = fmul float %34, %37
  %add31 = fadd float %add26, %mul30
  %38 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats32 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %38, i32 0, i32 0
  %arrayidx33 = getelementptr inbounds [4 x float], [4 x float]* %m_floats32, i32 0, i32 0
  %39 = load float, float* %arrayidx33, align 4, !tbaa !30
  %40 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %41 = bitcast %class.btQuaternion* %40 to %class.btQuadWord*
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %41)
  %42 = load float, float* %call34, align 4, !tbaa !30
  %mul35 = fmul float %39, %42
  %sub36 = fsub float %add31, %mul35
  store float %sub36, float* %ref.tmp16, align 4, !tbaa !30
  %43 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #8
  %44 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats38 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %44, i32 0, i32 0
  %arrayidx39 = getelementptr inbounds [4 x float], [4 x float]* %m_floats38, i32 0, i32 3
  %45 = load float, float* %arrayidx39, align 4, !tbaa !30
  %46 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %47 = bitcast %class.btQuaternion* %46 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %47)
  %48 = load float, float* %call40, align 4, !tbaa !30
  %mul41 = fmul float %45, %48
  %49 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats42 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %49, i32 0, i32 0
  %arrayidx43 = getelementptr inbounds [4 x float], [4 x float]* %m_floats42, i32 0, i32 2
  %50 = load float, float* %arrayidx43, align 4, !tbaa !30
  %51 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %52 = bitcast %class.btQuaternion* %51 to %class.btQuadWord*
  %m_floats44 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %52, i32 0, i32 0
  %arrayidx45 = getelementptr inbounds [4 x float], [4 x float]* %m_floats44, i32 0, i32 3
  %53 = load float, float* %arrayidx45, align 4, !tbaa !30
  %mul46 = fmul float %50, %53
  %add47 = fadd float %mul41, %mul46
  %54 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats48 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %54, i32 0, i32 0
  %arrayidx49 = getelementptr inbounds [4 x float], [4 x float]* %m_floats48, i32 0, i32 0
  %55 = load float, float* %arrayidx49, align 4, !tbaa !30
  %56 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %57 = bitcast %class.btQuaternion* %56 to %class.btQuadWord*
  %call50 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %57)
  %58 = load float, float* %call50, align 4, !tbaa !30
  %mul51 = fmul float %55, %58
  %add52 = fadd float %add47, %mul51
  %59 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats53 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %59, i32 0, i32 0
  %arrayidx54 = getelementptr inbounds [4 x float], [4 x float]* %m_floats53, i32 0, i32 1
  %60 = load float, float* %arrayidx54, align 4, !tbaa !30
  %61 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %62 = bitcast %class.btQuaternion* %61 to %class.btQuadWord*
  %call55 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %62)
  %63 = load float, float* %call55, align 4, !tbaa !30
  %mul56 = fmul float %60, %63
  %sub57 = fsub float %add52, %mul56
  store float %sub57, float* %ref.tmp37, align 4, !tbaa !30
  %64 = bitcast float* %ref.tmp58 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #8
  %65 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats59 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %65, i32 0, i32 0
  %arrayidx60 = getelementptr inbounds [4 x float], [4 x float]* %m_floats59, i32 0, i32 3
  %66 = load float, float* %arrayidx60, align 4, !tbaa !30
  %67 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %68 = bitcast %class.btQuaternion* %67 to %class.btQuadWord*
  %m_floats61 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %68, i32 0, i32 0
  %arrayidx62 = getelementptr inbounds [4 x float], [4 x float]* %m_floats61, i32 0, i32 3
  %69 = load float, float* %arrayidx62, align 4, !tbaa !30
  %mul63 = fmul float %66, %69
  %70 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats64 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %70, i32 0, i32 0
  %arrayidx65 = getelementptr inbounds [4 x float], [4 x float]* %m_floats64, i32 0, i32 0
  %71 = load float, float* %arrayidx65, align 4, !tbaa !30
  %72 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %73 = bitcast %class.btQuaternion* %72 to %class.btQuadWord*
  %call66 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %73)
  %74 = load float, float* %call66, align 4, !tbaa !30
  %mul67 = fmul float %71, %74
  %sub68 = fsub float %mul63, %mul67
  %75 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats69 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %75, i32 0, i32 0
  %arrayidx70 = getelementptr inbounds [4 x float], [4 x float]* %m_floats69, i32 0, i32 1
  %76 = load float, float* %arrayidx70, align 4, !tbaa !30
  %77 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %78 = bitcast %class.btQuaternion* %77 to %class.btQuadWord*
  %call71 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %78)
  %79 = load float, float* %call71, align 4, !tbaa !30
  %mul72 = fmul float %76, %79
  %sub73 = fsub float %sub68, %mul72
  %80 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats74 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %80, i32 0, i32 0
  %arrayidx75 = getelementptr inbounds [4 x float], [4 x float]* %m_floats74, i32 0, i32 2
  %81 = load float, float* %arrayidx75, align 4, !tbaa !30
  %82 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %83 = bitcast %class.btQuaternion* %82 to %class.btQuadWord*
  %call76 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %83)
  %84 = load float, float* %call76, align 4, !tbaa !30
  %mul77 = fmul float %81, %84
  %sub78 = fsub float %sub73, %mul77
  store float %sub78, float* %ref.tmp58, align 4, !tbaa !30
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp37, float* nonnull align 4 dereferenceable(4) %ref.tmp58)
  %85 = bitcast float* %ref.tmp58 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #8
  %86 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #8
  %87 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #8
  %88 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #8
  ret %class.btQuaternion* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getXEv(%class.btQuadWord* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getYEv(%class.btQuadWord* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getZEv(%class.btQuadWord* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btAcosf(float %x) #3 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !30
  %0 = load float, float* %x.addr, align 4, !tbaa !30
  %cmp = fcmp olt float %0, -1.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store float -1.000000e+00, float* %x.addr, align 4, !tbaa !30
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %1 = load float, float* %x.addr, align 4, !tbaa !30
  %cmp1 = fcmp ogt float %1, 1.000000e+00
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store float 1.000000e+00, float* %x.addr, align 4, !tbaa !30
  br label %if.end3

if.end3:                                          ; preds = %if.then2, %if.end
  %2 = load float, float* %x.addr, align 4, !tbaa !30
  %call = call float @acosf(float %2) #9
  ret float %call
}

; Function Attrs: nounwind readnone
declare float @acosf(float) #7

define linkonce_odr hidden float @_ZNK12btQuaternion6lengthEv(%class.btQuaternion* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaterniondVERKf(%class.btQuaternion* %this, float* nonnull align 4 dereferenceable(4) %s) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !30
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !30
  %call = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKf(%class.btQuaternion* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  ret %class.btQuaternion* %call
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKf(%class.btQuaternion* %this, float* nonnull align 4 dereferenceable(4) %s) #5 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %s.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !30
  %2 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %2, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %3 = load float, float* %arrayidx, align 4, !tbaa !30
  %mul = fmul float %3, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !30
  %4 = load float*, float** %s.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !30
  %6 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !30
  %mul4 = fmul float %7, %5
  store float %mul4, float* %arrayidx3, align 4, !tbaa !30
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !30
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats5 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %11 = load float, float* %arrayidx6, align 4, !tbaa !30
  %mul7 = fmul float %11, %9
  store float %mul7, float* %arrayidx6, align 4, !tbaa !30
  %12 = load float*, float** %s.addr, align 4, !tbaa !2
  %13 = load float, float* %12, align 4, !tbaa !30
  %14 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats8 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %14, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 3
  %15 = load float, float* %arrayidx9, align 4, !tbaa !30
  %mul10 = fmul float %15, %13
  store float %mul10, float* %arrayidx9, align 4, !tbaa !30
  ret %class.btQuaternion* %this1
}

; Function Attrs: nounwind readnone
declare float @atan2f(float, float) #7

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.cos.f32(float) #6

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sin.f32(float) #6

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN12btQuaternion11setRotationERK9btVector3RKf(%class.btQuaternion* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, float* nonnull align 4 dereferenceable(4) %_angle) #5 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %axis.addr = alloca %class.btVector3*, align 4
  %_angle.addr = alloca float*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %axis, %class.btVector3** %axis.addr, align 4, !tbaa !2
  store float* %_angle, float** %_angle.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast float* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %1)
  store float %call, float* %d, align 4, !tbaa !30
  %2 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load float*, float** %_angle.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !30
  %mul = fmul float %4, 5.000000e-01
  %call2 = call float @_Z5btSinf(float %mul)
  %5 = load float, float* %d, align 4, !tbaa !30
  %div = fdiv float %call2, %5
  store float %div, float* %s, align 4, !tbaa !30
  %6 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %7 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %8 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %8)
  %9 = load float, float* %call3, align 4, !tbaa !30
  %10 = load float, float* %s, align 4, !tbaa !30
  %mul4 = fmul float %9, %10
  store float %mul4, float* %ref.tmp, align 4, !tbaa !30
  %11 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  %12 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %12)
  %13 = load float, float* %call6, align 4, !tbaa !30
  %14 = load float, float* %s, align 4, !tbaa !30
  %mul7 = fmul float %13, %14
  store float %mul7, float* %ref.tmp5, align 4, !tbaa !30
  %15 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  %16 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %16)
  %17 = load float, float* %call9, align 4, !tbaa !30
  %18 = load float, float* %s, align 4, !tbaa !30
  %mul10 = fmul float %17, %18
  store float %mul10, float* %ref.tmp8, align 4, !tbaa !30
  %19 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  %20 = load float*, float** %_angle.addr, align 4, !tbaa !2
  %21 = load float, float* %20, align 4, !tbaa !30
  %mul12 = fmul float %21, 5.000000e-01
  %call13 = call float @_Z5btCosf(float %mul12)
  store float %call13, float* %ref.tmp11, align 4, !tbaa !30
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %6, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11)
  %22 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #8
  %23 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #8
  %24 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #8
  %25 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #8
  %26 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #8
  %27 = bitcast float* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN21btConeTwistConstraintdlEPv(i8* %ptr) #3 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

declare i8* @_ZNK17btTypedConstraint9serializeEPvP12btSerializer(%class.btTypedConstraint*, i8*, %class.btSerializer*) unnamed_addr #1

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %this, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %dataOut) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %dataOut.addr = alloca %struct.btTransformFloatData*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %struct.btTransformFloatData* %dataOut, %struct.btTransformFloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %struct.btTransformFloatData*, %struct.btTransformFloatData** %dataOut.addr, align 4, !tbaa !2
  %m_basis2 = getelementptr inbounds %struct.btTransformFloatData, %struct.btTransformFloatData* %0, i32 0, i32 0
  call void @_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData(%class.btMatrix3x3* %m_basis, %struct.btMatrix3x3FloatData* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %struct.btTransformFloatData*, %struct.btTransformFloatData** %dataOut.addr, align 4, !tbaa !2
  %m_origin3 = getelementptr inbounds %struct.btTransformFloatData, %struct.btTransformFloatData* %1, i32 0, i32 1
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_origin, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_origin3)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData(%class.btMatrix3x3* %this, %struct.btMatrix3x3FloatData* nonnull align 4 dereferenceable(48) %dataOut) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %dataOut.addr = alloca %struct.btMatrix3x3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %struct.btMatrix3x3FloatData* %dataOut, %struct.btMatrix3x3FloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %i, align 4, !tbaa !60
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !60
  %cmp = icmp slt i32 %1, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %3 = load i32, i32* %i, align 4, !tbaa !60
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %3
  %4 = load %struct.btMatrix3x3FloatData*, %struct.btMatrix3x3FloatData** %dataOut.addr, align 4, !tbaa !2
  %m_el2 = getelementptr inbounds %struct.btMatrix3x3FloatData, %struct.btMatrix3x3FloatData* %4, i32 0, i32 0
  %5 = load i32, i32* %i, align 4, !tbaa !60
  %arrayidx3 = getelementptr inbounds [3 x %struct.btVector3FloatData], [3 x %struct.btVector3FloatData]* %m_el2, i32 0, i32 %5
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %arrayidx, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %arrayidx3)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !60
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !60
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %i, align 4, !tbaa !60
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !60
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %3 = load i32, i32* %i, align 4, !tbaa !60
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %3
  %4 = load float, float* %arrayidx, align 4, !tbaa !30
  %5 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %5, i32 0, i32 0
  %6 = load i32, i32* %i, align 4, !tbaa !60
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %6
  store float %4, float* %arrayidx3, align 4, !tbaa !30
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !60
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !60
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

define internal void @_GLOBAL__sub_I_btConeTwistConstraint.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind readnone speculatable willreturn }
attributes #7 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind }
attributes #9 = { nounwind readnone }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{!9, !14, i64 524}
!9 = !{!"_ZTS21btConeTwistConstraint", !4, i64 48, !10, i64 300, !10, i64 364, !13, i64 428, !13, i64 432, !13, i64 436, !13, i64 440, !13, i64 444, !13, i64 448, !13, i64 452, !13, i64 456, !12, i64 460, !12, i64 476, !13, i64 492, !13, i64 496, !13, i64 500, !13, i64 504, !13, i64 508, !13, i64 512, !13, i64 516, !13, i64 520, !14, i64 524, !14, i64 525, !14, i64 526, !14, i64 527, !13, i64 528, !13, i64 532, !12, i64 536, !14, i64 552, !14, i64 553, !15, i64 556, !13, i64 572, !12, i64 576, !16, i64 592, !13, i64 596, !13, i64 600, !13, i64 604}
!10 = !{!"_ZTS11btTransform", !11, i64 0, !12, i64 48}
!11 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!12 = !{!"_ZTS9btVector3", !4, i64 0}
!13 = !{!"float", !4, i64 0}
!14 = !{!"bool", !4, i64 0}
!15 = !{!"_ZTS12btQuaternion"}
!16 = !{!"int", !4, i64 0}
!17 = !{!9, !14, i64 527}
!18 = !{i64 0, i64 16, !19}
!19 = !{!4, !4, i64 0}
!20 = !{!9, !14, i64 525}
!21 = !{!9, !14, i64 526}
!22 = !{!9, !14, i64 552}
!23 = !{!9, !13, i64 572}
!24 = !{!9, !13, i64 440}
!25 = !{!9, !13, i64 456}
!26 = !{!9, !16, i64 592}
!27 = !{!9, !13, i64 596}
!28 = !{!9, !13, i64 600}
!29 = !{!9, !13, i64 604}
!30 = !{!13, !13, i64 0}
!31 = !{!9, !13, i64 444}
!32 = !{!9, !13, i64 448}
!33 = !{!9, !13, i64 452}
!34 = !{!9, !13, i64 428}
!35 = !{!9, !13, i64 432}
!36 = !{!9, !13, i64 436}
!37 = !{i8 0, i8 2}
!38 = !{!39, !16, i64 0}
!39 = !{!"_ZTSN17btTypedConstraint17btConstraintInfo1E", !16, i64 0, !16, i64 4}
!40 = !{!39, !16, i64 4}
!41 = !{!42, !3, i64 28}
!42 = !{!"_ZTS17btTypedConstraint", !16, i64 8, !4, i64 12, !13, i64 16, !14, i64 20, !14, i64 21, !16, i64 24, !3, i64 28, !3, i64 32, !13, i64 36, !13, i64 40, !3, i64 44}
!43 = !{!42, !3, i64 32}
!44 = !{!9, !13, i64 504}
!45 = !{!9, !13, i64 500}
!46 = !{!9, !13, i64 528}
!47 = !{!9, !13, i64 492}
!48 = !{!9, !13, i64 512}
!49 = !{!9, !13, i64 532}
!50 = !{!9, !13, i64 508}
!51 = !{!9, !13, i64 496}
!52 = !{!53, !3, i64 8}
!53 = !{!"_ZTSN17btTypedConstraint17btConstraintInfo2E", !13, i64 0, !13, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !16, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !3, i64 44, !16, i64 48, !13, i64 52}
!54 = !{!53, !16, i64 24}
!55 = !{!53, !3, i64 12}
!56 = !{!53, !3, i64 16}
!57 = !{!53, !3, i64 20}
!58 = !{!53, !13, i64 4}
!59 = !{!53, !13, i64 0}
!60 = !{!16, !16, i64 0}
!61 = !{!53, !3, i64 28}
!62 = !{!53, !3, i64 36}
!63 = !{!53, !3, i64 40}
!64 = !{!53, !3, i64 32}
!65 = !{!42, !13, i64 36}
!66 = !{!9, !13, i64 520}
!67 = !{!9, !13, i64 516}
!68 = !{!69, !13, i64 344}
!69 = !{!"_ZTS11btRigidBody", !11, i64 264, !12, i64 312, !12, i64 328, !13, i64 344, !12, i64 348, !12, i64 364, !12, i64 380, !12, i64 396, !12, i64 412, !12, i64 428, !13, i64 444, !13, i64 448, !14, i64 452, !13, i64 456, !13, i64 460, !13, i64 464, !13, i64 468, !13, i64 472, !13, i64 476, !3, i64 480, !70, i64 484, !16, i64 504, !16, i64 508, !12, i64 512, !12, i64 528, !12, i64 544, !12, i64 560, !12, i64 576, !12, i64 592, !16, i64 608, !16, i64 612}
!70 = !{!"_ZTS20btAlignedObjectArrayIP17btTypedConstraintE", !71, i64 0, !16, i64 4, !16, i64 8, !3, i64 12, !14, i64 16}
!71 = !{!"_ZTS18btAlignedAllocatorIP17btTypedConstraintLj16EE"}
!72 = !{!73, !13, i64 80}
!73 = !{!"_ZTS15btJacobianEntry", !12, i64 0, !12, i64 16, !12, i64 32, !12, i64 48, !12, i64 64, !13, i64 80}
!74 = !{!14, !14, i64 0}
!75 = !{!9, !14, i64 553}
!76 = !{!77, !3, i64 240}
!77 = !{!"_ZTS12btSolverBody", !10, i64 0, !12, i64 64, !12, i64 80, !12, i64 96, !12, i64 112, !12, i64 128, !12, i64 144, !12, i64 160, !12, i64 176, !12, i64 192, !12, i64 208, !12, i64 224, !3, i64 240}
!78 = !{!79, !13, i64 180}
!79 = !{!"_ZTS25btConeTwistConstraintData", !80, i64 0, !81, i64 52, !81, i64 116, !13, i64 180, !13, i64 184, !13, i64 188, !13, i64 192, !13, i64 196, !13, i64 200, !13, i64 204, !4, i64 208}
!80 = !{!"_ZTS21btTypedConstraintData", !3, i64 0, !3, i64 4, !3, i64 8, !16, i64 12, !16, i64 16, !16, i64 20, !16, i64 24, !13, i64 28, !13, i64 32, !16, i64 36, !16, i64 40, !13, i64 44, !16, i64 48}
!81 = !{!"_ZTS20btTransformFloatData", !82, i64 0, !83, i64 48}
!82 = !{!"_ZTS20btMatrix3x3FloatData", !4, i64 0}
!83 = !{!"_ZTS18btVector3FloatData", !4, i64 0}
!84 = !{!79, !13, i64 184}
!85 = !{!79, !13, i64 188}
!86 = !{!79, !13, i64 192}
!87 = !{!79, !13, i64 196}
!88 = !{!79, !13, i64 200}
!89 = !{!79, !13, i64 204}
