; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btBvhTriangleMeshShape.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btBvhTriangleMeshShape.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btBvhTriangleMeshShape = type <{ %class.btTriangleMeshShape, %class.btOptimizedBvh*, %struct.btTriangleInfoMap*, i8, i8, [11 x i8], [3 x i8] }>
%class.btTriangleMeshShape = type { %class.btConcaveShape, %class.btVector3, %class.btVector3, %class.btStridingMeshInterface* }
%class.btConcaveShape = type { %class.btCollisionShape, float }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%class.btVector3 = type { [4 x float] }
%class.btOptimizedBvh = type { %class.btQuantizedBvh }
%class.btQuantizedBvh = type { i32 (...)**, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32, i8, [3 x i8], %class.btAlignedObjectArray, %class.btAlignedObjectArray, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0, i32, %class.btAlignedObjectArray.4, i32 }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btOptimizedBvhNode*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btOptimizedBvhNode = type { %class.btVector3, %class.btVector3, i32, i32, i32, [20 x i8] }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.btQuantizedBvhNode*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btQuantizedBvhNode = type { [3 x i16], [3 x i16], i32 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %class.btBvhSubtreeInfo*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btBvhSubtreeInfo = type { [3 x i16], [3 x i16], i32, i32, [3 x i32] }
%struct.btTriangleInfoMap = type { i32 (...)**, %class.btHashMap, float, float, float, float, float, float }
%class.btHashMap = type { %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.16 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, %struct.btTriangleInfo*, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%struct.btTriangleInfo = type { i32, float, float, float }
%class.btAlignedObjectArray.16 = type <{ %class.btAlignedAllocator.17, [3 x i8], i32, i32, %class.btHashInt*, i8, [3 x i8] }>
%class.btAlignedAllocator.17 = type { i8 }
%class.btHashInt = type { i32 }
%class.btStridingMeshInterface = type { i32 (...)**, %class.btVector3 }
%class.btTriangleCallback = type { i32 (...)** }
%struct.MyNodeOverlapCallback = type { %class.btNodeOverlapCallback, %class.btStridingMeshInterface*, %class.btTriangleCallback* }
%class.btNodeOverlapCallback = type { i32 (...)** }
%struct.MyNodeOverlapCallback.20 = type { %class.btNodeOverlapCallback, %class.btStridingMeshInterface*, %class.btTriangleCallback* }
%struct.MyNodeOverlapCallback.21 = type { %class.btNodeOverlapCallback, %class.btStridingMeshInterface*, %class.btTriangleCallback*, [3 x %class.btVector3] }
%class.btSerializer = type { i32 (...)** }
%struct.btTriangleMeshShapeData = type { %struct.btCollisionShapeData, %struct.btStridingMeshInterfaceData, %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhDoubleData*, %struct.btTriangleInfoMapData*, float, [4 x i8] }
%struct.btCollisionShapeData = type { i8*, i32, [4 x i8] }
%struct.btStridingMeshInterfaceData = type { %struct.btMeshPartData*, %struct.btVector3FloatData, i32, [4 x i8] }
%struct.btMeshPartData = type { %struct.btVector3FloatData*, %struct.btVector3DoubleData*, %struct.btIntIndexData*, %struct.btShortIntIndexTripletData*, %struct.btCharIndexTripletData*, %struct.btShortIntIndexData*, i32, i32 }
%struct.btVector3DoubleData = type { [4 x double] }
%struct.btIntIndexData = type { i32 }
%struct.btShortIntIndexTripletData = type { [3 x i16], [2 x i8] }
%struct.btCharIndexTripletData = type { [3 x i8], i8 }
%struct.btShortIntIndexData = type { i16, [2 x i8] }
%struct.btVector3FloatData = type { [4 x float] }
%struct.btQuantizedBvhFloatData = type { %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, i32, i32, i32, i32, %struct.btOptimizedBvhNodeFloatData*, %struct.btQuantizedBvhNodeData*, %struct.btBvhSubtreeInfoData*, i32, i32 }
%struct.btOptimizedBvhNodeFloatData = type { %struct.btVector3FloatData, %struct.btVector3FloatData, i32, i32, i32, [4 x i8] }
%struct.btQuantizedBvhNodeData = type { [3 x i16], [3 x i16], i32 }
%struct.btBvhSubtreeInfoData = type { i32, i32, [3 x i16], [3 x i16] }
%struct.btQuantizedBvhDoubleData = type { %struct.btVector3DoubleData, %struct.btVector3DoubleData, %struct.btVector3DoubleData, i32, i32, i32, i32, %struct.btOptimizedBvhNodeDoubleData*, %struct.btQuantizedBvhNodeData*, i32, i32, %struct.btBvhSubtreeInfoData* }
%struct.btOptimizedBvhNodeDoubleData = type { %struct.btVector3DoubleData, %struct.btVector3DoubleData, i32, i32, i32, [4 x i8] }
%struct.btTriangleInfoMapData = type { i32*, i32*, %struct.btTriangleInfoData*, i32*, float, float, float, float, float, i32, i32, i32, i32, [4 x i8] }
%struct.btTriangleInfoData = type { i32, float, float, float }
%class.btChunk = type { i32, i32, i8*, i32, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }

$_ZN14btOptimizedBvhnwEmPv = comdat any

$_ZN9btVector36setMinERKS_ = comdat any

$_ZN9btVector36setMaxERKS_ = comdat any

$_ZN22btBvhTriangleMeshShapedlEPv = comdat any

$_ZN21btNodeOverlapCallbackD2Ev = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZNK22btBvhTriangleMeshShape7getNameEv = comdat any

$_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZN14btConcaveShape9setMarginEf = comdat any

$_ZNK14btConcaveShape9getMarginEv = comdat any

$_ZNK22btBvhTriangleMeshShape28calculateSerializeBufferSizeEv = comdat any

$_ZNK19btTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3 = comdat any

$_Z8btSetMinIfEvRT_RKS0_ = comdat any

$_ZNK9btVector31wEv = comdat any

$_Z8btSetMaxIfEvRT_RKS0_ = comdat any

$_ZN21btNodeOverlapCallbackC2Ev = comdat any

$_ZN21btNodeOverlapCallbackD0Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK23btStridingMeshInterface10getScalingEv = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZTS21btNodeOverlapCallback = comdat any

$_ZTI21btNodeOverlapCallback = comdat any

$_ZTV21btNodeOverlapCallback = comdat any

@_ZTV22btBvhTriangleMeshShape = hidden unnamed_addr constant { [23 x i8*] } { [23 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI22btBvhTriangleMeshShape to i8*), i8* bitcast (%class.btBvhTriangleMeshShape* (%class.btBvhTriangleMeshShape*)* @_ZN22btBvhTriangleMeshShapeD1Ev to i8*), i8* bitcast (void (%class.btBvhTriangleMeshShape*)* @_ZN22btBvhTriangleMeshShapeD0Ev to i8*), i8* bitcast (void (%class.btTriangleMeshShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK19btTriangleMeshShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btBvhTriangleMeshShape*, %class.btVector3*)* @_ZN22btBvhTriangleMeshShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btTriangleMeshShape*)* @_ZNK19btTriangleMeshShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btTriangleMeshShape*, float, %class.btVector3*)* @_ZNK19btTriangleMeshShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btBvhTriangleMeshShape*)* @_ZNK22btBvhTriangleMeshShape7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btConcaveShape*, float)* @_ZN14btConcaveShape9setMarginEf to i8*), i8* bitcast (float (%class.btConcaveShape*)* @_ZNK14btConcaveShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btBvhTriangleMeshShape*)* @_ZNK22btBvhTriangleMeshShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btBvhTriangleMeshShape*, i8*, %class.btSerializer*)* @_ZNK22btBvhTriangleMeshShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)* @_ZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_ to i8*), i8* bitcast (void (%class.btVector3*, %class.btTriangleMeshShape*, %class.btVector3*)* @_ZNK19btTriangleMeshShape24localGetSupportingVertexERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btTriangleMeshShape*, %class.btVector3*)* @_ZNK19btTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3 to i8*), i8* bitcast (void (%class.btBvhTriangleMeshShape*, %class.btSerializer*)* @_ZNK22btBvhTriangleMeshShape18serializeSingleBvhEP12btSerializer to i8*), i8* bitcast (void (%class.btBvhTriangleMeshShape*, %class.btSerializer*)* @_ZNK22btBvhTriangleMeshShape30serializeSingleTriangleInfoMapEP12btSerializer to i8*)] }, align 4
@.str = private unnamed_addr constant [24 x i8] c"btTriangleMeshShapeData\00", align 1
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS22btBvhTriangleMeshShape = hidden constant [25 x i8] c"22btBvhTriangleMeshShape\00", align 1
@_ZTI19btTriangleMeshShape = external constant i8*
@_ZTI22btBvhTriangleMeshShape = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([25 x i8], [25 x i8]* @_ZTS22btBvhTriangleMeshShape, i32 0, i32 0), i8* bitcast (i8** @_ZTI19btTriangleMeshShape to i8*) }, align 4
@_ZTVZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback = internal unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback to i8*), i8* bitcast (%class.btNodeOverlapCallback* (%class.btNodeOverlapCallback*)* @_ZN21btNodeOverlapCallbackD2Ev to i8*), i8* bitcast (void (%struct.MyNodeOverlapCallback*)* @_ZZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackD0Ev to i8*), i8* bitcast (void (%struct.MyNodeOverlapCallback*, i32, i32)* @_ZZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallback11processNodeEii to i8*)] }, align 4
@_ZTSZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback = internal constant [104 x i8] c"ZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS21btNodeOverlapCallback = linkonce_odr hidden constant [24 x i8] c"21btNodeOverlapCallback\00", comdat, align 1
@_ZTI21btNodeOverlapCallback = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([24 x i8], [24 x i8]* @_ZTS21btNodeOverlapCallback, i32 0, i32 0) }, comdat, align 4
@_ZTIZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback = internal constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([104 x i8], [104 x i8]* @_ZTSZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI21btNodeOverlapCallback to i8*) }, align 4
@_ZTV21btNodeOverlapCallback = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI21btNodeOverlapCallback to i8*), i8* bitcast (%class.btNodeOverlapCallback* (%class.btNodeOverlapCallback*)* @_ZN21btNodeOverlapCallbackD2Ev to i8*), i8* bitcast (void (%class.btNodeOverlapCallback*)* @_ZN21btNodeOverlapCallbackD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTVZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback = internal unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback to i8*), i8* bitcast (%class.btNodeOverlapCallback* (%class.btNodeOverlapCallback*)* @_ZN21btNodeOverlapCallbackD2Ev to i8*), i8* bitcast (void (%struct.MyNodeOverlapCallback.20*)* @_ZZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_EN21MyNodeOverlapCallbackD0Ev to i8*), i8* bitcast (void (%struct.MyNodeOverlapCallback.20*, i32, i32)* @_ZZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_EN21MyNodeOverlapCallback11processNodeEii to i8*)] }, align 4
@_ZTSZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback = internal constant [113 x i8] c"ZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback\00", align 1
@_ZTIZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback = internal constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([113 x i8], [113 x i8]* @_ZTSZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI21btNodeOverlapCallback to i8*) }, align 4
@_ZTVZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback = internal unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback to i8*), i8* bitcast (%class.btNodeOverlapCallback* (%class.btNodeOverlapCallback*)* @_ZN21btNodeOverlapCallbackD2Ev to i8*), i8* bitcast (void (%struct.MyNodeOverlapCallback.21*)* @_ZZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackD0Ev to i8*), i8* bitcast (void (%struct.MyNodeOverlapCallback.21*, i32, i32)* @_ZZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallback11processNodeEii to i8*)] }, align 4
@_ZTSZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback = internal constant [110 x i8] c"ZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback\00", align 1
@_ZTIZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback = internal constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([110 x i8], [110 x i8]* @_ZTSZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI21btNodeOverlapCallback to i8*) }, align 4
@.str.1 = private unnamed_addr constant [16 x i8] c"BVHTRIANGLEMESH\00", align 1

@_ZN22btBvhTriangleMeshShapeC1EP23btStridingMeshInterfacebb = hidden unnamed_addr alias %class.btBvhTriangleMeshShape* (%class.btBvhTriangleMeshShape*, %class.btStridingMeshInterface*, i1, i1), %class.btBvhTriangleMeshShape* (%class.btBvhTriangleMeshShape*, %class.btStridingMeshInterface*, i1, i1)* @_ZN22btBvhTriangleMeshShapeC2EP23btStridingMeshInterfacebb
@_ZN22btBvhTriangleMeshShapeC1EP23btStridingMeshInterfacebRK9btVector3S4_b = hidden unnamed_addr alias %class.btBvhTriangleMeshShape* (%class.btBvhTriangleMeshShape*, %class.btStridingMeshInterface*, i1, %class.btVector3*, %class.btVector3*, i1), %class.btBvhTriangleMeshShape* (%class.btBvhTriangleMeshShape*, %class.btStridingMeshInterface*, i1, %class.btVector3*, %class.btVector3*, i1)* @_ZN22btBvhTriangleMeshShapeC2EP23btStridingMeshInterfacebRK9btVector3S4_b
@_ZN22btBvhTriangleMeshShapeD1Ev = hidden unnamed_addr alias %class.btBvhTriangleMeshShape* (%class.btBvhTriangleMeshShape*), %class.btBvhTriangleMeshShape* (%class.btBvhTriangleMeshShape*)* @_ZN22btBvhTriangleMeshShapeD2Ev

define hidden %class.btBvhTriangleMeshShape* @_ZN22btBvhTriangleMeshShapeC2EP23btStridingMeshInterfacebb(%class.btBvhTriangleMeshShape* returned %this, %class.btStridingMeshInterface* %meshInterface, i1 zeroext %useQuantizedAabbCompression, i1 zeroext %buildBvh) unnamed_addr #0 {
entry:
  %retval = alloca %class.btBvhTriangleMeshShape*, align 4
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %meshInterface.addr = alloca %class.btStridingMeshInterface*, align 4
  %useQuantizedAabbCompression.addr = alloca i8, align 1
  %buildBvh.addr = alloca i8, align 1
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  store %class.btStridingMeshInterface* %meshInterface, %class.btStridingMeshInterface** %meshInterface.addr, align 4, !tbaa !2
  %frombool = zext i1 %useQuantizedAabbCompression to i8
  store i8 %frombool, i8* %useQuantizedAabbCompression.addr, align 1, !tbaa !6
  %frombool1 = zext i1 %buildBvh to i8
  store i8 %frombool1, i8* %buildBvh.addr, align 1, !tbaa !6
  %this2 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  store %class.btBvhTriangleMeshShape* %this2, %class.btBvhTriangleMeshShape** %retval, align 4
  %0 = bitcast %class.btBvhTriangleMeshShape* %this2 to %class.btTriangleMeshShape*
  %1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface.addr, align 4, !tbaa !2
  %call = call %class.btTriangleMeshShape* @_ZN19btTriangleMeshShapeC2EP23btStridingMeshInterface(%class.btTriangleMeshShape* %0, %class.btStridingMeshInterface* %1)
  %2 = bitcast %class.btBvhTriangleMeshShape* %this2 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [23 x i8*] }, { [23 x i8*] }* @_ZTV22btBvhTriangleMeshShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %2, align 4, !tbaa !8
  %m_bvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this2, i32 0, i32 1
  store %class.btOptimizedBvh* null, %class.btOptimizedBvh** %m_bvh, align 4, !tbaa !10
  %m_triangleInfoMap = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this2, i32 0, i32 2
  store %struct.btTriangleInfoMap* null, %struct.btTriangleInfoMap** %m_triangleInfoMap, align 4, !tbaa !12
  %m_useQuantizedAabbCompression = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this2, i32 0, i32 3
  %3 = load i8, i8* %useQuantizedAabbCompression.addr, align 1, !tbaa !6, !range !13
  %tobool = trunc i8 %3 to i1
  %frombool3 = zext i1 %tobool to i8
  store i8 %frombool3, i8* %m_useQuantizedAabbCompression, align 4, !tbaa !14
  %m_ownsBvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this2, i32 0, i32 4
  store i8 0, i8* %m_ownsBvh, align 1, !tbaa !15
  %4 = bitcast %class.btBvhTriangleMeshShape* %this2 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %4, i32 0, i32 1
  store i32 21, i32* %m_shapeType, align 4, !tbaa !16
  %5 = load i8, i8* %buildBvh.addr, align 1, !tbaa !6, !range !13
  %tobool4 = trunc i8 %5 to i1
  br i1 %tobool4, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZN22btBvhTriangleMeshShape17buildOptimizedBvhEv(%class.btBvhTriangleMeshShape* %this2)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %6 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %retval, align 4
  ret %class.btBvhTriangleMeshShape* %6
}

declare %class.btTriangleMeshShape* @_ZN19btTriangleMeshShapeC2EP23btStridingMeshInterface(%class.btTriangleMeshShape* returned, %class.btStridingMeshInterface*) unnamed_addr #1

define hidden void @_ZN22btBvhTriangleMeshShape17buildOptimizedBvhEv(%class.btBvhTriangleMeshShape* %this) #0 {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %mem = alloca i8*, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %m_ownsBvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 4
  %0 = load i8, i8* %m_ownsBvh, align 1, !tbaa !15, !range !13
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_bvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %1 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh, align 4, !tbaa !10
  %2 = bitcast %class.btOptimizedBvh* %1 to %class.btOptimizedBvh* (%class.btOptimizedBvh*)***
  %vtable = load %class.btOptimizedBvh* (%class.btOptimizedBvh*)**, %class.btOptimizedBvh* (%class.btOptimizedBvh*)*** %2, align 4, !tbaa !8
  %vfn = getelementptr inbounds %class.btOptimizedBvh* (%class.btOptimizedBvh*)*, %class.btOptimizedBvh* (%class.btOptimizedBvh*)** %vtable, i64 0
  %3 = load %class.btOptimizedBvh* (%class.btOptimizedBvh*)*, %class.btOptimizedBvh* (%class.btOptimizedBvh*)** %vfn, align 4
  %call = call %class.btOptimizedBvh* %3(%class.btOptimizedBvh* %1) #9
  %m_bvh2 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %4 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh2, align 4, !tbaa !10
  %5 = bitcast %class.btOptimizedBvh* %4 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %5)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %6 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %call3 = call i8* @_Z22btAlignedAllocInternalmi(i32 172, i32 16)
  store i8* %call3, i8** %mem, align 4, !tbaa !2
  %7 = load i8*, i8** %mem, align 4, !tbaa !2
  %call4 = call i8* @_ZN14btOptimizedBvhnwEmPv(i32 172, i8* %7)
  %8 = bitcast i8* %call4 to %class.btOptimizedBvh*
  %call5 = call %class.btOptimizedBvh* @_ZN14btOptimizedBvhC1Ev(%class.btOptimizedBvh* %8)
  %m_bvh6 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  store %class.btOptimizedBvh* %8, %class.btOptimizedBvh** %m_bvh6, align 4, !tbaa !10
  %m_bvh7 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %9 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh7, align 4, !tbaa !10
  %10 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %m_meshInterface = getelementptr inbounds %class.btTriangleMeshShape, %class.btTriangleMeshShape* %10, i32 0, i32 3
  %11 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface, align 4, !tbaa !19
  %m_useQuantizedAabbCompression = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 3
  %12 = load i8, i8* %m_useQuantizedAabbCompression, align 4, !tbaa !14, !range !13
  %tobool8 = trunc i8 %12 to i1
  %13 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %m_localAabbMin = getelementptr inbounds %class.btTriangleMeshShape, %class.btTriangleMeshShape* %13, i32 0, i32 1
  %14 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %m_localAabbMax = getelementptr inbounds %class.btTriangleMeshShape, %class.btTriangleMeshShape* %14, i32 0, i32 2
  call void @_ZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_(%class.btOptimizedBvh* %9, %class.btStridingMeshInterface* %11, i1 zeroext %tobool8, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localAabbMax)
  %m_ownsBvh9 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 4
  store i8 1, i8* %m_ownsBvh9, align 1, !tbaa !15
  %15 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  ret void
}

define hidden %class.btBvhTriangleMeshShape* @_ZN22btBvhTriangleMeshShapeC2EP23btStridingMeshInterfacebRK9btVector3S4_b(%class.btBvhTriangleMeshShape* returned %this, %class.btStridingMeshInterface* %meshInterface, i1 zeroext %useQuantizedAabbCompression, %class.btVector3* nonnull align 4 dereferenceable(16) %bvhAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %bvhAabbMax, i1 zeroext %buildBvh) unnamed_addr #0 {
entry:
  %retval = alloca %class.btBvhTriangleMeshShape*, align 4
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %meshInterface.addr = alloca %class.btStridingMeshInterface*, align 4
  %useQuantizedAabbCompression.addr = alloca i8, align 1
  %bvhAabbMin.addr = alloca %class.btVector3*, align 4
  %bvhAabbMax.addr = alloca %class.btVector3*, align 4
  %buildBvh.addr = alloca i8, align 1
  %mem = alloca i8*, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  store %class.btStridingMeshInterface* %meshInterface, %class.btStridingMeshInterface** %meshInterface.addr, align 4, !tbaa !2
  %frombool = zext i1 %useQuantizedAabbCompression to i8
  store i8 %frombool, i8* %useQuantizedAabbCompression.addr, align 1, !tbaa !6
  store %class.btVector3* %bvhAabbMin, %class.btVector3** %bvhAabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %bvhAabbMax, %class.btVector3** %bvhAabbMax.addr, align 4, !tbaa !2
  %frombool1 = zext i1 %buildBvh to i8
  store i8 %frombool1, i8* %buildBvh.addr, align 1, !tbaa !6
  %this2 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  store %class.btBvhTriangleMeshShape* %this2, %class.btBvhTriangleMeshShape** %retval, align 4
  %0 = bitcast %class.btBvhTriangleMeshShape* %this2 to %class.btTriangleMeshShape*
  %1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface.addr, align 4, !tbaa !2
  %call = call %class.btTriangleMeshShape* @_ZN19btTriangleMeshShapeC2EP23btStridingMeshInterface(%class.btTriangleMeshShape* %0, %class.btStridingMeshInterface* %1)
  %2 = bitcast %class.btBvhTriangleMeshShape* %this2 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [23 x i8*] }, { [23 x i8*] }* @_ZTV22btBvhTriangleMeshShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %2, align 4, !tbaa !8
  %m_bvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this2, i32 0, i32 1
  store %class.btOptimizedBvh* null, %class.btOptimizedBvh** %m_bvh, align 4, !tbaa !10
  %m_triangleInfoMap = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this2, i32 0, i32 2
  store %struct.btTriangleInfoMap* null, %struct.btTriangleInfoMap** %m_triangleInfoMap, align 4, !tbaa !12
  %m_useQuantizedAabbCompression = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this2, i32 0, i32 3
  %3 = load i8, i8* %useQuantizedAabbCompression.addr, align 1, !tbaa !6, !range !13
  %tobool = trunc i8 %3 to i1
  %frombool3 = zext i1 %tobool to i8
  store i8 %frombool3, i8* %m_useQuantizedAabbCompression, align 4, !tbaa !14
  %m_ownsBvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this2, i32 0, i32 4
  store i8 0, i8* %m_ownsBvh, align 1, !tbaa !15
  %4 = bitcast %class.btBvhTriangleMeshShape* %this2 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %4, i32 0, i32 1
  store i32 21, i32* %m_shapeType, align 4, !tbaa !16
  %5 = load i8, i8* %buildBvh.addr, align 1, !tbaa !6, !range !13
  %tobool4 = trunc i8 %5 to i1
  br i1 %tobool4, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %call5 = call i8* @_Z22btAlignedAllocInternalmi(i32 172, i32 16)
  store i8* %call5, i8** %mem, align 4, !tbaa !2
  %7 = load i8*, i8** %mem, align 4, !tbaa !2
  %call6 = call i8* @_ZN14btOptimizedBvhnwEmPv(i32 172, i8* %7)
  %8 = bitcast i8* %call6 to %class.btOptimizedBvh*
  %call7 = call %class.btOptimizedBvh* @_ZN14btOptimizedBvhC1Ev(%class.btOptimizedBvh* %8)
  %m_bvh8 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this2, i32 0, i32 1
  store %class.btOptimizedBvh* %8, %class.btOptimizedBvh** %m_bvh8, align 4, !tbaa !10
  %m_bvh9 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this2, i32 0, i32 1
  %9 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh9, align 4, !tbaa !10
  %10 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface.addr, align 4, !tbaa !2
  %m_useQuantizedAabbCompression10 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this2, i32 0, i32 3
  %11 = load i8, i8* %m_useQuantizedAabbCompression10, align 4, !tbaa !14, !range !13
  %tobool11 = trunc i8 %11 to i1
  %12 = load %class.btVector3*, %class.btVector3** %bvhAabbMin.addr, align 4, !tbaa !2
  %13 = load %class.btVector3*, %class.btVector3** %bvhAabbMax.addr, align 4, !tbaa !2
  call void @_ZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_(%class.btOptimizedBvh* %9, %class.btStridingMeshInterface* %10, i1 zeroext %tobool11, %class.btVector3* nonnull align 4 dereferenceable(16) %12, %class.btVector3* nonnull align 4 dereferenceable(16) %13)
  %m_ownsBvh12 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this2, i32 0, i32 4
  store i8 1, i8* %m_ownsBvh12, align 1, !tbaa !15
  %14 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %15 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %retval, align 4
  ret %class.btBvhTriangleMeshShape* %15
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN14btOptimizedBvhnwEmPv(i32 %0, i8* %ptr) #3 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !22
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

declare %class.btOptimizedBvh* @_ZN14btOptimizedBvhC1Ev(%class.btOptimizedBvh* returned) unnamed_addr #1

declare void @_ZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_(%class.btOptimizedBvh*, %class.btStridingMeshInterface*, i1 zeroext, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

define hidden void @_ZN22btBvhTriangleMeshShape16partialRefitTreeERK9btVector3S2_(%class.btBvhTriangleMeshShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) #0 {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %m_bvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %0 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh, align 4, !tbaa !10
  %1 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %m_meshInterface = getelementptr inbounds %class.btTriangleMeshShape, %class.btTriangleMeshShape* %1, i32 0, i32 3
  %2 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface, align 4, !tbaa !19
  %3 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %4 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  call void @_ZN14btOptimizedBvh12refitPartialEP23btStridingMeshInterfaceRK9btVector3S4_(%class.btOptimizedBvh* %0, %class.btStridingMeshInterface* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %4)
  %5 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %m_localAabbMin = getelementptr inbounds %class.btTriangleMeshShape, %class.btTriangleMeshShape* %5, i32 0, i32 1
  %6 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %m_localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %6)
  %7 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %m_localAabbMax = getelementptr inbounds %class.btTriangleMeshShape, %class.btTriangleMeshShape* %7, i32 0, i32 2
  %8 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %m_localAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  ret void
}

declare void @_ZN14btOptimizedBvh12refitPartialEP23btStridingMeshInterfaceRK9btVector3S4_(%class.btOptimizedBvh*, %class.btStridingMeshInterface*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) #1

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN9btVector36setMinERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7)
  %m_floats8 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 3
  %3 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %3)
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN9btVector36setMaxERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7)
  %m_floats8 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 3
  %3 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %3)
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %call)
  ret void
}

define hidden void @_ZN22btBvhTriangleMeshShape9refitTreeERK9btVector3S2_(%class.btBvhTriangleMeshShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) #0 {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %m_bvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %0 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh, align 4, !tbaa !10
  %1 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %m_meshInterface = getelementptr inbounds %class.btTriangleMeshShape, %class.btTriangleMeshShape* %1, i32 0, i32 3
  %2 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface, align 4, !tbaa !19
  %3 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %4 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  call void @_ZN14btOptimizedBvh5refitEP23btStridingMeshInterfaceRK9btVector3S4_(%class.btOptimizedBvh* %0, %class.btStridingMeshInterface* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %4)
  %5 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  call void @_ZN19btTriangleMeshShape15recalcLocalAabbEv(%class.btTriangleMeshShape* %5)
  ret void
}

declare void @_ZN14btOptimizedBvh5refitEP23btStridingMeshInterfaceRK9btVector3S4_(%class.btOptimizedBvh*, %class.btStridingMeshInterface*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) #1

declare void @_ZN19btTriangleMeshShape15recalcLocalAabbEv(%class.btTriangleMeshShape*) #1

; Function Attrs: nounwind
define hidden %class.btBvhTriangleMeshShape* @_ZN22btBvhTriangleMeshShapeD2Ev(%class.btBvhTriangleMeshShape* returned %this) unnamed_addr #5 {
entry:
  %retval = alloca %class.btBvhTriangleMeshShape*, align 4
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  store %class.btBvhTriangleMeshShape* %this1, %class.btBvhTriangleMeshShape** %retval, align 4
  %0 = bitcast %class.btBvhTriangleMeshShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [23 x i8*] }, { [23 x i8*] }* @_ZTV22btBvhTriangleMeshShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !8
  %m_ownsBvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 4
  %1 = load i8, i8* %m_ownsBvh, align 1, !tbaa !15, !range !13
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_bvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %2 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh, align 4, !tbaa !10
  %3 = bitcast %class.btOptimizedBvh* %2 to %class.btOptimizedBvh* (%class.btOptimizedBvh*)***
  %vtable = load %class.btOptimizedBvh* (%class.btOptimizedBvh*)**, %class.btOptimizedBvh* (%class.btOptimizedBvh*)*** %3, align 4, !tbaa !8
  %vfn = getelementptr inbounds %class.btOptimizedBvh* (%class.btOptimizedBvh*)*, %class.btOptimizedBvh* (%class.btOptimizedBvh*)** %vtable, i64 0
  %4 = load %class.btOptimizedBvh* (%class.btOptimizedBvh*)*, %class.btOptimizedBvh* (%class.btOptimizedBvh*)** %vfn, align 4
  %call = call %class.btOptimizedBvh* %4(%class.btOptimizedBvh* %2) #9
  %m_bvh2 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %5 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh2, align 4, !tbaa !10
  %6 = bitcast %class.btOptimizedBvh* %5 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %6)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %7 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %call3 = call %class.btTriangleMeshShape* @_ZN19btTriangleMeshShapeD2Ev(%class.btTriangleMeshShape* %7) #9
  %8 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %retval, align 4
  ret %class.btBvhTriangleMeshShape* %8
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

; Function Attrs: nounwind
declare %class.btTriangleMeshShape* @_ZN19btTriangleMeshShapeD2Ev(%class.btTriangleMeshShape* returned) unnamed_addr #6

; Function Attrs: nounwind
define hidden void @_ZN22btBvhTriangleMeshShapeD0Ev(%class.btBvhTriangleMeshShape* %this) unnamed_addr #5 {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %call = call %class.btBvhTriangleMeshShape* @_ZN22btBvhTriangleMeshShapeD1Ev(%class.btBvhTriangleMeshShape* %this1) #9
  %0 = bitcast %class.btBvhTriangleMeshShape* %this1 to i8*
  call void @_ZN22btBvhTriangleMeshShapedlEPv(i8* %0) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN22btBvhTriangleMeshShapedlEPv(i8* %ptr) #3 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

define hidden void @_ZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_(%class.btBvhTriangleMeshShape* %this, %class.btTriangleCallback* %callback, %class.btVector3* nonnull align 4 dereferenceable(16) %raySource, %class.btVector3* nonnull align 4 dereferenceable(16) %rayTarget) #0 {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %callback.addr = alloca %class.btTriangleCallback*, align 4
  %raySource.addr = alloca %class.btVector3*, align 4
  %rayTarget.addr = alloca %class.btVector3*, align 4
  %myNodeCallback = alloca %struct.MyNodeOverlapCallback, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  store %class.btTriangleCallback* %callback, %class.btTriangleCallback** %callback.addr, align 4, !tbaa !2
  store %class.btVector3* %raySource, %class.btVector3** %raySource.addr, align 4, !tbaa !2
  store %class.btVector3* %rayTarget, %class.btVector3** %rayTarget.addr, align 4, !tbaa !2
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %0 = bitcast %struct.MyNodeOverlapCallback* %myNodeCallback to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %0) #9
  %1 = load %class.btTriangleCallback*, %class.btTriangleCallback** %callback.addr, align 4, !tbaa !2
  %2 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %m_meshInterface = getelementptr inbounds %class.btTriangleMeshShape, %class.btTriangleMeshShape* %2, i32 0, i32 3
  %3 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface, align 4, !tbaa !19
  %call = call %struct.MyNodeOverlapCallback* @_ZZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackC2ES1_P23btStridingMeshInterface(%struct.MyNodeOverlapCallback* %myNodeCallback, %class.btTriangleCallback* %1, %class.btStridingMeshInterface* %3)
  %m_bvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %4 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh, align 4, !tbaa !10
  %5 = bitcast %class.btOptimizedBvh* %4 to %class.btQuantizedBvh*
  %6 = bitcast %struct.MyNodeOverlapCallback* %myNodeCallback to %class.btNodeOverlapCallback*
  %7 = load %class.btVector3*, %class.btVector3** %raySource.addr, align 4, !tbaa !2
  %8 = load %class.btVector3*, %class.btVector3** %rayTarget.addr, align 4, !tbaa !2
  call void @_ZNK14btQuantizedBvh25reportRayOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_(%class.btQuantizedBvh* %5, %class.btNodeOverlapCallback* %6, %class.btVector3* nonnull align 4 dereferenceable(16) %7, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  %call2 = call %struct.MyNodeOverlapCallback* bitcast (%class.btNodeOverlapCallback* (%class.btNodeOverlapCallback*)* @_ZN21btNodeOverlapCallbackD2Ev to %struct.MyNodeOverlapCallback* (%struct.MyNodeOverlapCallback*)*)(%struct.MyNodeOverlapCallback* %myNodeCallback) #9
  %9 = bitcast %struct.MyNodeOverlapCallback* %myNodeCallback to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %9) #9
  ret void
}

; Function Attrs: nounwind
define internal %struct.MyNodeOverlapCallback* @_ZZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackC2ES1_P23btStridingMeshInterface(%struct.MyNodeOverlapCallback* returned %this, %class.btTriangleCallback* %callback, %class.btStridingMeshInterface* %meshInterface) unnamed_addr #5 {
entry:
  %this.addr = alloca %struct.MyNodeOverlapCallback*, align 4
  %callback.addr = alloca %class.btTriangleCallback*, align 4
  %meshInterface.addr = alloca %class.btStridingMeshInterface*, align 4
  store %struct.MyNodeOverlapCallback* %this, %struct.MyNodeOverlapCallback** %this.addr, align 4, !tbaa !2
  store %class.btTriangleCallback* %callback, %class.btTriangleCallback** %callback.addr, align 4, !tbaa !2
  store %class.btStridingMeshInterface* %meshInterface, %class.btStridingMeshInterface** %meshInterface.addr, align 4, !tbaa !2
  %this1 = load %struct.MyNodeOverlapCallback*, %struct.MyNodeOverlapCallback** %this.addr, align 4
  %0 = bitcast %struct.MyNodeOverlapCallback* %this1 to %class.btNodeOverlapCallback*
  %call = call %class.btNodeOverlapCallback* @_ZN21btNodeOverlapCallbackC2Ev(%class.btNodeOverlapCallback* %0) #9
  %1 = bitcast %struct.MyNodeOverlapCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !8
  %m_meshInterface = getelementptr inbounds %struct.MyNodeOverlapCallback, %struct.MyNodeOverlapCallback* %this1, i32 0, i32 1
  %2 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface.addr, align 4, !tbaa !2
  store %class.btStridingMeshInterface* %2, %class.btStridingMeshInterface** %m_meshInterface, align 4, !tbaa !24
  %m_callback = getelementptr inbounds %struct.MyNodeOverlapCallback, %struct.MyNodeOverlapCallback* %this1, i32 0, i32 2
  %3 = load %class.btTriangleCallback*, %class.btTriangleCallback** %callback.addr, align 4, !tbaa !2
  store %class.btTriangleCallback* %3, %class.btTriangleCallback** %m_callback, align 4, !tbaa !26
  ret %struct.MyNodeOverlapCallback* %this1
}

declare void @_ZNK14btQuantizedBvh25reportRayOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_(%class.btQuantizedBvh*, %class.btNodeOverlapCallback*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) #1

; Function Attrs: nounwind
define linkonce_odr hidden %class.btNodeOverlapCallback* @_ZN21btNodeOverlapCallbackD2Ev(%class.btNodeOverlapCallback* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btNodeOverlapCallback*, align 4
  store %class.btNodeOverlapCallback* %this, %class.btNodeOverlapCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %this.addr, align 4
  ret %class.btNodeOverlapCallback* %this1
}

define hidden void @_ZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_(%class.btBvhTriangleMeshShape* %this, %class.btTriangleCallback* %callback, %class.btVector3* nonnull align 4 dereferenceable(16) %raySource, %class.btVector3* nonnull align 4 dereferenceable(16) %rayTarget, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) #0 {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %callback.addr = alloca %class.btTriangleCallback*, align 4
  %raySource.addr = alloca %class.btVector3*, align 4
  %rayTarget.addr = alloca %class.btVector3*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %myNodeCallback = alloca %struct.MyNodeOverlapCallback.20, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  store %class.btTriangleCallback* %callback, %class.btTriangleCallback** %callback.addr, align 4, !tbaa !2
  store %class.btVector3* %raySource, %class.btVector3** %raySource.addr, align 4, !tbaa !2
  store %class.btVector3* %rayTarget, %class.btVector3** %rayTarget.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %0 = bitcast %struct.MyNodeOverlapCallback.20* %myNodeCallback to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %0) #9
  %1 = load %class.btTriangleCallback*, %class.btTriangleCallback** %callback.addr, align 4, !tbaa !2
  %2 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %m_meshInterface = getelementptr inbounds %class.btTriangleMeshShape, %class.btTriangleMeshShape* %2, i32 0, i32 3
  %3 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface, align 4, !tbaa !19
  %call = call %struct.MyNodeOverlapCallback.20* @_ZZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_EN21MyNodeOverlapCallbackC2ES1_P23btStridingMeshInterface(%struct.MyNodeOverlapCallback.20* %myNodeCallback, %class.btTriangleCallback* %1, %class.btStridingMeshInterface* %3)
  %m_bvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %4 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh, align 4, !tbaa !10
  %5 = bitcast %class.btOptimizedBvh* %4 to %class.btQuantizedBvh*
  %6 = bitcast %struct.MyNodeOverlapCallback.20* %myNodeCallback to %class.btNodeOverlapCallback*
  %7 = load %class.btVector3*, %class.btVector3** %raySource.addr, align 4, !tbaa !2
  %8 = load %class.btVector3*, %class.btVector3** %rayTarget.addr, align 4, !tbaa !2
  %9 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %10 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  call void @_ZNK14btQuantizedBvh29reportBoxCastOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_(%class.btQuantizedBvh* %5, %class.btNodeOverlapCallback* %6, %class.btVector3* nonnull align 4 dereferenceable(16) %7, %class.btVector3* nonnull align 4 dereferenceable(16) %8, %class.btVector3* nonnull align 4 dereferenceable(16) %9, %class.btVector3* nonnull align 4 dereferenceable(16) %10)
  %call2 = call %struct.MyNodeOverlapCallback.20* bitcast (%class.btNodeOverlapCallback* (%class.btNodeOverlapCallback*)* @_ZN21btNodeOverlapCallbackD2Ev to %struct.MyNodeOverlapCallback.20* (%struct.MyNodeOverlapCallback.20*)*)(%struct.MyNodeOverlapCallback.20* %myNodeCallback) #9
  %11 = bitcast %struct.MyNodeOverlapCallback.20* %myNodeCallback to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %11) #9
  ret void
}

; Function Attrs: nounwind
define internal %struct.MyNodeOverlapCallback.20* @_ZZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_EN21MyNodeOverlapCallbackC2ES1_P23btStridingMeshInterface(%struct.MyNodeOverlapCallback.20* returned %this, %class.btTriangleCallback* %callback, %class.btStridingMeshInterface* %meshInterface) unnamed_addr #5 {
entry:
  %this.addr = alloca %struct.MyNodeOverlapCallback.20*, align 4
  %callback.addr = alloca %class.btTriangleCallback*, align 4
  %meshInterface.addr = alloca %class.btStridingMeshInterface*, align 4
  store %struct.MyNodeOverlapCallback.20* %this, %struct.MyNodeOverlapCallback.20** %this.addr, align 4, !tbaa !2
  store %class.btTriangleCallback* %callback, %class.btTriangleCallback** %callback.addr, align 4, !tbaa !2
  store %class.btStridingMeshInterface* %meshInterface, %class.btStridingMeshInterface** %meshInterface.addr, align 4, !tbaa !2
  %this1 = load %struct.MyNodeOverlapCallback.20*, %struct.MyNodeOverlapCallback.20** %this.addr, align 4
  %0 = bitcast %struct.MyNodeOverlapCallback.20* %this1 to %class.btNodeOverlapCallback*
  %call = call %class.btNodeOverlapCallback* @_ZN21btNodeOverlapCallbackC2Ev(%class.btNodeOverlapCallback* %0) #9
  %1 = bitcast %struct.MyNodeOverlapCallback.20* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !8
  %m_meshInterface = getelementptr inbounds %struct.MyNodeOverlapCallback.20, %struct.MyNodeOverlapCallback.20* %this1, i32 0, i32 1
  %2 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface.addr, align 4, !tbaa !2
  store %class.btStridingMeshInterface* %2, %class.btStridingMeshInterface** %m_meshInterface, align 4, !tbaa !27
  %m_callback = getelementptr inbounds %struct.MyNodeOverlapCallback.20, %struct.MyNodeOverlapCallback.20* %this1, i32 0, i32 2
  %3 = load %class.btTriangleCallback*, %class.btTriangleCallback** %callback.addr, align 4, !tbaa !2
  store %class.btTriangleCallback* %3, %class.btTriangleCallback** %m_callback, align 4, !tbaa !29
  ret %struct.MyNodeOverlapCallback.20* %this1
}

declare void @_ZNK14btQuantizedBvh29reportBoxCastOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_(%class.btQuantizedBvh*, %class.btNodeOverlapCallback*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) #1

define hidden void @_ZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_(%class.btBvhTriangleMeshShape* %this, %class.btTriangleCallback* %callback, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %callback.addr = alloca %class.btTriangleCallback*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %myNodeCallback = alloca %struct.MyNodeOverlapCallback.21, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  store %class.btTriangleCallback* %callback, %class.btTriangleCallback** %callback.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %0 = bitcast %struct.MyNodeOverlapCallback.21* %myNodeCallback to i8*
  call void @llvm.lifetime.start.p0i8(i64 60, i8* %0) #9
  %1 = load %class.btTriangleCallback*, %class.btTriangleCallback** %callback.addr, align 4, !tbaa !2
  %2 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %m_meshInterface = getelementptr inbounds %class.btTriangleMeshShape, %class.btTriangleMeshShape* %2, i32 0, i32 3
  %3 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface, align 4, !tbaa !19
  %call = call %struct.MyNodeOverlapCallback.21* @_ZZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackC2ES1_P23btStridingMeshInterface(%struct.MyNodeOverlapCallback.21* %myNodeCallback, %class.btTriangleCallback* %1, %class.btStridingMeshInterface* %3)
  %m_bvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %4 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh, align 4, !tbaa !10
  %5 = bitcast %class.btOptimizedBvh* %4 to %class.btQuantizedBvh*
  %6 = bitcast %struct.MyNodeOverlapCallback.21* %myNodeCallback to %class.btNodeOverlapCallback*
  %7 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %8 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  call void @_ZNK14btQuantizedBvh26reportAabbOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_(%class.btQuantizedBvh* %5, %class.btNodeOverlapCallback* %6, %class.btVector3* nonnull align 4 dereferenceable(16) %7, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  %call2 = call %struct.MyNodeOverlapCallback.21* bitcast (%class.btNodeOverlapCallback* (%class.btNodeOverlapCallback*)* @_ZN21btNodeOverlapCallbackD2Ev to %struct.MyNodeOverlapCallback.21* (%struct.MyNodeOverlapCallback.21*)*)(%struct.MyNodeOverlapCallback.21* %myNodeCallback) #9
  %9 = bitcast %struct.MyNodeOverlapCallback.21* %myNodeCallback to i8*
  call void @llvm.lifetime.end.p0i8(i64 60, i8* %9) #9
  ret void
}

define internal %struct.MyNodeOverlapCallback.21* @_ZZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackC2ES1_P23btStridingMeshInterface(%struct.MyNodeOverlapCallback.21* returned %this, %class.btTriangleCallback* %callback, %class.btStridingMeshInterface* %meshInterface) unnamed_addr #0 {
entry:
  %retval = alloca %struct.MyNodeOverlapCallback.21*, align 4
  %this.addr = alloca %struct.MyNodeOverlapCallback.21*, align 4
  %callback.addr = alloca %class.btTriangleCallback*, align 4
  %meshInterface.addr = alloca %class.btStridingMeshInterface*, align 4
  store %struct.MyNodeOverlapCallback.21* %this, %struct.MyNodeOverlapCallback.21** %this.addr, align 4, !tbaa !2
  store %class.btTriangleCallback* %callback, %class.btTriangleCallback** %callback.addr, align 4, !tbaa !2
  store %class.btStridingMeshInterface* %meshInterface, %class.btStridingMeshInterface** %meshInterface.addr, align 4, !tbaa !2
  %this1 = load %struct.MyNodeOverlapCallback.21*, %struct.MyNodeOverlapCallback.21** %this.addr, align 4
  store %struct.MyNodeOverlapCallback.21* %this1, %struct.MyNodeOverlapCallback.21** %retval, align 4
  %0 = bitcast %struct.MyNodeOverlapCallback.21* %this1 to %class.btNodeOverlapCallback*
  %call = call %class.btNodeOverlapCallback* @_ZN21btNodeOverlapCallbackC2Ev(%class.btNodeOverlapCallback* %0) #9
  %1 = bitcast %struct.MyNodeOverlapCallback.21* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !8
  %m_meshInterface = getelementptr inbounds %struct.MyNodeOverlapCallback.21, %struct.MyNodeOverlapCallback.21* %this1, i32 0, i32 1
  %2 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface.addr, align 4, !tbaa !2
  store %class.btStridingMeshInterface* %2, %class.btStridingMeshInterface** %m_meshInterface, align 4, !tbaa !30
  %m_callback = getelementptr inbounds %struct.MyNodeOverlapCallback.21, %struct.MyNodeOverlapCallback.21* %this1, i32 0, i32 2
  %3 = load %class.btTriangleCallback*, %class.btTriangleCallback** %callback.addr, align 4, !tbaa !2
  store %class.btTriangleCallback* %3, %class.btTriangleCallback** %m_callback, align 4, !tbaa !32
  %m_triangle = getelementptr inbounds %struct.MyNodeOverlapCallback.21, %struct.MyNodeOverlapCallback.21* %this1, i32 0, i32 3
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_triangle, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %4 = load %struct.MyNodeOverlapCallback.21*, %struct.MyNodeOverlapCallback.21** %retval, align 4
  ret %struct.MyNodeOverlapCallback.21* %4
}

declare void @_ZNK14btQuantizedBvh26reportAabbOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_(%class.btQuantizedBvh*, %class.btNodeOverlapCallback*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) #1

define hidden void @_ZN22btBvhTriangleMeshShape15setLocalScalingERK9btVector3(%class.btBvhTriangleMeshShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %scaling) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %scaling.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %scaling, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %2 = bitcast %class.btTriangleMeshShape* %1 to %class.btVector3* (%class.btTriangleMeshShape*)***
  %vtable = load %class.btVector3* (%class.btTriangleMeshShape*)**, %class.btVector3* (%class.btTriangleMeshShape*)*** %2, align 4, !tbaa !8
  %vfn = getelementptr inbounds %class.btVector3* (%class.btTriangleMeshShape*)*, %class.btVector3* (%class.btTriangleMeshShape*)** %vtable, i64 7
  %3 = load %class.btVector3* (%class.btTriangleMeshShape*)*, %class.btVector3* (%class.btTriangleMeshShape*)** %vfn, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* %3(%class.btTriangleMeshShape* %1)
  %4 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %4)
  %call2 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ref.tmp)
  %cmp = fcmp ogt float %call2, 0x3E80000000000000
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #9
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %7 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  call void @_ZN19btTriangleMeshShape15setLocalScalingERK9btVector3(%class.btTriangleMeshShape* %6, %class.btVector3* nonnull align 4 dereferenceable(16) %7)
  call void @_ZN22btBvhTriangleMeshShape17buildOptimizedBvhEv(%class.btBvhTriangleMeshShape* %this1)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !33
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !33
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !33
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !33
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !33
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !33
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !33
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !33
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !33
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

declare void @_ZN19btTriangleMeshShape15setLocalScalingERK9btVector3(%class.btTriangleMeshShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

define hidden void @_ZN22btBvhTriangleMeshShape15setOptimizedBvhEP14btOptimizedBvhRK9btVector3(%class.btBvhTriangleMeshShape* %this, %class.btOptimizedBvh* %bvh, %class.btVector3* nonnull align 4 dereferenceable(16) %scaling) #0 {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %bvh.addr = alloca %class.btOptimizedBvh*, align 4
  %scaling.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  store %class.btOptimizedBvh* %bvh, %class.btOptimizedBvh** %bvh.addr, align 4, !tbaa !2
  store %class.btVector3* %scaling, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %0 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %bvh.addr, align 4, !tbaa !2
  %m_bvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  store %class.btOptimizedBvh* %0, %class.btOptimizedBvh** %m_bvh, align 4, !tbaa !10
  %m_ownsBvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 4
  store i8 0, i8* %m_ownsBvh, align 1, !tbaa !15
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #9
  %2 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %3 = bitcast %class.btTriangleMeshShape* %2 to %class.btVector3* (%class.btTriangleMeshShape*)***
  %vtable = load %class.btVector3* (%class.btTriangleMeshShape*)**, %class.btVector3* (%class.btTriangleMeshShape*)*** %3, align 4, !tbaa !8
  %vfn = getelementptr inbounds %class.btVector3* (%class.btTriangleMeshShape*)*, %class.btVector3* (%class.btTriangleMeshShape*)** %vtable, i64 7
  %4 = load %class.btVector3* (%class.btTriangleMeshShape*)*, %class.btVector3* (%class.btTriangleMeshShape*)** %vfn, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* %4(%class.btTriangleMeshShape* %2)
  %5 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  %call2 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ref.tmp)
  %cmp = fcmp ogt float %call2, 0x3E80000000000000
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %6) #9
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %8 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  call void @_ZN19btTriangleMeshShape15setLocalScalingERK9btVector3(%class.btTriangleMeshShape* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

define hidden i8* @_ZNK22btBvhTriangleMeshShape9serializeEPvP12btSerializer(%class.btBvhTriangleMeshShape* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %trimeshData = alloca %struct.btTriangleMeshShapeData*, align 4
  %chunk = alloca i8*, align 4
  %sz = alloca i32, align 4
  %chunk25 = alloca %class.btChunk*, align 4
  %structType = alloca i8*, align 4
  %chunk48 = alloca i8*, align 4
  %sz62 = alloca i32, align 4
  %chunk67 = alloca %class.btChunk*, align 4
  %structType71 = alloca i8*, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %0 = bitcast %struct.btTriangleMeshShapeData** %trimeshData to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i8*, i8** %dataBuffer.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.btTriangleMeshShapeData*
  store %struct.btTriangleMeshShapeData* %2, %struct.btTriangleMeshShapeData** %trimeshData, align 4, !tbaa !2
  %3 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btCollisionShape*
  %4 = load %struct.btTriangleMeshShapeData*, %struct.btTriangleMeshShapeData** %trimeshData, align 4, !tbaa !2
  %m_collisionShapeData = getelementptr inbounds %struct.btTriangleMeshShapeData, %struct.btTriangleMeshShapeData* %4, i32 0, i32 0
  %5 = bitcast %struct.btCollisionShapeData* %m_collisionShapeData to i8*
  %6 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %call = call i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape* %3, i8* %5, %class.btSerializer* %6)
  %7 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %m_meshInterface = getelementptr inbounds %class.btTriangleMeshShape, %class.btTriangleMeshShape* %7, i32 0, i32 3
  %8 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface, align 4, !tbaa !19
  %9 = load %struct.btTriangleMeshShapeData*, %struct.btTriangleMeshShapeData** %trimeshData, align 4, !tbaa !2
  %m_meshInterface2 = getelementptr inbounds %struct.btTriangleMeshShapeData, %struct.btTriangleMeshShapeData* %9, i32 0, i32 1
  %10 = bitcast %struct.btStridingMeshInterfaceData* %m_meshInterface2 to i8*
  %11 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %12 = bitcast %class.btStridingMeshInterface* %8 to i8* (%class.btStridingMeshInterface*, i8*, %class.btSerializer*)***
  %vtable = load i8* (%class.btStridingMeshInterface*, i8*, %class.btSerializer*)**, i8* (%class.btStridingMeshInterface*, i8*, %class.btSerializer*)*** %12, align 4, !tbaa !8
  %vfn = getelementptr inbounds i8* (%class.btStridingMeshInterface*, i8*, %class.btSerializer*)*, i8* (%class.btStridingMeshInterface*, i8*, %class.btSerializer*)** %vtable, i64 14
  %13 = load i8* (%class.btStridingMeshInterface*, i8*, %class.btSerializer*)*, i8* (%class.btStridingMeshInterface*, i8*, %class.btSerializer*)** %vfn, align 4
  %call3 = call i8* %13(%class.btStridingMeshInterface* %8, i8* %10, %class.btSerializer* %11)
  %14 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btConcaveShape*
  %m_collisionMargin = getelementptr inbounds %class.btConcaveShape, %class.btConcaveShape* %14, i32 0, i32 1
  %15 = load float, float* %m_collisionMargin, align 4, !tbaa !35
  %16 = load %struct.btTriangleMeshShapeData*, %struct.btTriangleMeshShapeData** %trimeshData, align 4, !tbaa !2
  %m_collisionMargin4 = getelementptr inbounds %struct.btTriangleMeshShapeData, %struct.btTriangleMeshShapeData* %16, i32 0, i32 5
  store float %15, float* %m_collisionMargin4, align 4, !tbaa !37
  %m_bvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %17 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh, align 4, !tbaa !10
  %tobool = icmp ne %class.btOptimizedBvh* %17, null
  br i1 %tobool, label %land.lhs.true, label %if.else36

land.lhs.true:                                    ; preds = %entry
  %18 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %19 = bitcast %class.btSerializer* %18 to i32 (%class.btSerializer*)***
  %vtable5 = load i32 (%class.btSerializer*)**, i32 (%class.btSerializer*)*** %19, align 4, !tbaa !8
  %vfn6 = getelementptr inbounds i32 (%class.btSerializer*)*, i32 (%class.btSerializer*)** %vtable5, i64 13
  %20 = load i32 (%class.btSerializer*)*, i32 (%class.btSerializer*)** %vfn6, align 4
  %call7 = call i32 %20(%class.btSerializer* %18)
  %and = and i32 %call7, 1
  %tobool8 = icmp ne i32 %and, 0
  br i1 %tobool8, label %if.else36, label %if.then

if.then:                                          ; preds = %land.lhs.true
  %21 = bitcast i8** %chunk to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #9
  %22 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %m_bvh9 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %23 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh9, align 4, !tbaa !10
  %24 = bitcast %class.btOptimizedBvh* %23 to i8*
  %25 = bitcast %class.btSerializer* %22 to i8* (%class.btSerializer*, i8*)***
  %vtable10 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %25, align 4, !tbaa !8
  %vfn11 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable10, i64 6
  %26 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn11, align 4
  %call12 = call i8* %26(%class.btSerializer* %22, i8* %24)
  store i8* %call12, i8** %chunk, align 4, !tbaa !2
  %27 = load i8*, i8** %chunk, align 4, !tbaa !2
  %tobool13 = icmp ne i8* %27, null
  br i1 %tobool13, label %if.then14, label %if.else

if.then14:                                        ; preds = %if.then
  %28 = load i8*, i8** %chunk, align 4, !tbaa !2
  %29 = bitcast i8* %28 to %struct.btQuantizedBvhFloatData*
  %30 = load %struct.btTriangleMeshShapeData*, %struct.btTriangleMeshShapeData** %trimeshData, align 4, !tbaa !2
  %m_quantizedFloatBvh = getelementptr inbounds %struct.btTriangleMeshShapeData, %struct.btTriangleMeshShapeData* %30, i32 0, i32 2
  store %struct.btQuantizedBvhFloatData* %29, %struct.btQuantizedBvhFloatData** %m_quantizedFloatBvh, align 4, !tbaa !42
  %31 = load %struct.btTriangleMeshShapeData*, %struct.btTriangleMeshShapeData** %trimeshData, align 4, !tbaa !2
  %m_quantizedDoubleBvh = getelementptr inbounds %struct.btTriangleMeshShapeData, %struct.btTriangleMeshShapeData* %31, i32 0, i32 3
  store %struct.btQuantizedBvhDoubleData* null, %struct.btQuantizedBvhDoubleData** %m_quantizedDoubleBvh, align 4, !tbaa !43
  br label %if.end

if.else:                                          ; preds = %if.then
  %32 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %m_bvh15 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %33 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh15, align 4, !tbaa !10
  %34 = bitcast %class.btOptimizedBvh* %33 to i8*
  %35 = bitcast %class.btSerializer* %32 to i8* (%class.btSerializer*, i8*)***
  %vtable16 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %35, align 4, !tbaa !8
  %vfn17 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable16, i64 7
  %36 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn17, align 4
  %call18 = call i8* %36(%class.btSerializer* %32, i8* %34)
  %37 = bitcast i8* %call18 to %struct.btQuantizedBvhFloatData*
  %38 = load %struct.btTriangleMeshShapeData*, %struct.btTriangleMeshShapeData** %trimeshData, align 4, !tbaa !2
  %m_quantizedFloatBvh19 = getelementptr inbounds %struct.btTriangleMeshShapeData, %struct.btTriangleMeshShapeData* %38, i32 0, i32 2
  store %struct.btQuantizedBvhFloatData* %37, %struct.btQuantizedBvhFloatData** %m_quantizedFloatBvh19, align 4, !tbaa !42
  %39 = load %struct.btTriangleMeshShapeData*, %struct.btTriangleMeshShapeData** %trimeshData, align 4, !tbaa !2
  %m_quantizedDoubleBvh20 = getelementptr inbounds %struct.btTriangleMeshShapeData, %struct.btTriangleMeshShapeData* %39, i32 0, i32 3
  store %struct.btQuantizedBvhDoubleData* null, %struct.btQuantizedBvhDoubleData** %m_quantizedDoubleBvh20, align 4, !tbaa !43
  %40 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #9
  %m_bvh21 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %41 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh21, align 4, !tbaa !10
  %42 = bitcast %class.btOptimizedBvh* %41 to %class.btQuantizedBvh*
  %43 = bitcast %class.btQuantizedBvh* %42 to i32 (%class.btQuantizedBvh*)***
  %vtable22 = load i32 (%class.btQuantizedBvh*)**, i32 (%class.btQuantizedBvh*)*** %43, align 4, !tbaa !8
  %vfn23 = getelementptr inbounds i32 (%class.btQuantizedBvh*)*, i32 (%class.btQuantizedBvh*)** %vtable22, i64 3
  %44 = load i32 (%class.btQuantizedBvh*)*, i32 (%class.btQuantizedBvh*)** %vfn23, align 4
  %call24 = call i32 %44(%class.btQuantizedBvh* %42)
  store i32 %call24, i32* %sz, align 4, !tbaa !44
  %45 = bitcast %class.btChunk** %chunk25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #9
  %46 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %47 = load i32, i32* %sz, align 4, !tbaa !44
  %48 = bitcast %class.btSerializer* %46 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable26 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %48, align 4, !tbaa !8
  %vfn27 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable26, i64 4
  %49 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn27, align 4
  %call28 = call %class.btChunk* %49(%class.btSerializer* %46, i32 %47, i32 1)
  store %class.btChunk* %call28, %class.btChunk** %chunk25, align 4, !tbaa !2
  %50 = bitcast i8** %structType to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #9
  %m_bvh29 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %51 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh29, align 4, !tbaa !10
  %52 = bitcast %class.btOptimizedBvh* %51 to %class.btQuantizedBvh*
  %53 = load %class.btChunk*, %class.btChunk** %chunk25, align 4, !tbaa !2
  %m_oldPtr = getelementptr inbounds %class.btChunk, %class.btChunk* %53, i32 0, i32 2
  %54 = load i8*, i8** %m_oldPtr, align 4, !tbaa !45
  %55 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %56 = bitcast %class.btQuantizedBvh* %52 to i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)***
  %vtable30 = load i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)**, i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)*** %56, align 4, !tbaa !8
  %vfn31 = getelementptr inbounds i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)*, i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)** %vtable30, i64 4
  %57 = load i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)*, i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)** %vfn31, align 4
  %call32 = call i8* %57(%class.btQuantizedBvh* %52, i8* %54, %class.btSerializer* %55)
  store i8* %call32, i8** %structType, align 4, !tbaa !2
  %58 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %59 = load %class.btChunk*, %class.btChunk** %chunk25, align 4, !tbaa !2
  %60 = load i8*, i8** %structType, align 4, !tbaa !2
  %m_bvh33 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %61 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh33, align 4, !tbaa !10
  %62 = bitcast %class.btOptimizedBvh* %61 to i8*
  %63 = bitcast %class.btSerializer* %58 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable34 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %63, align 4, !tbaa !8
  %vfn35 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable34, i64 5
  %64 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn35, align 4
  call void %64(%class.btSerializer* %58, %class.btChunk* %59, i8* %60, i32 1213612625, i8* %62)
  %65 = bitcast i8** %structType to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #9
  %66 = bitcast %class.btChunk** %chunk25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #9
  %67 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #9
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then14
  %68 = bitcast i8** %chunk to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #9
  br label %if.end39

if.else36:                                        ; preds = %land.lhs.true, %entry
  %69 = load %struct.btTriangleMeshShapeData*, %struct.btTriangleMeshShapeData** %trimeshData, align 4, !tbaa !2
  %m_quantizedFloatBvh37 = getelementptr inbounds %struct.btTriangleMeshShapeData, %struct.btTriangleMeshShapeData* %69, i32 0, i32 2
  store %struct.btQuantizedBvhFloatData* null, %struct.btQuantizedBvhFloatData** %m_quantizedFloatBvh37, align 4, !tbaa !42
  %70 = load %struct.btTriangleMeshShapeData*, %struct.btTriangleMeshShapeData** %trimeshData, align 4, !tbaa !2
  %m_quantizedDoubleBvh38 = getelementptr inbounds %struct.btTriangleMeshShapeData, %struct.btTriangleMeshShapeData* %70, i32 0, i32 3
  store %struct.btQuantizedBvhDoubleData* null, %struct.btQuantizedBvhDoubleData** %m_quantizedDoubleBvh38, align 4, !tbaa !43
  br label %if.end39

if.end39:                                         ; preds = %if.else36, %if.end
  %m_triangleInfoMap = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 2
  %71 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap, align 4, !tbaa !12
  %tobool40 = icmp ne %struct.btTriangleInfoMap* %71, null
  br i1 %tobool40, label %land.lhs.true41, label %if.else81

land.lhs.true41:                                  ; preds = %if.end39
  %72 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %73 = bitcast %class.btSerializer* %72 to i32 (%class.btSerializer*)***
  %vtable42 = load i32 (%class.btSerializer*)**, i32 (%class.btSerializer*)*** %73, align 4, !tbaa !8
  %vfn43 = getelementptr inbounds i32 (%class.btSerializer*)*, i32 (%class.btSerializer*)** %vtable42, i64 13
  %74 = load i32 (%class.btSerializer*)*, i32 (%class.btSerializer*)** %vfn43, align 4
  %call44 = call i32 %74(%class.btSerializer* %72)
  %and45 = and i32 %call44, 2
  %tobool46 = icmp ne i32 %and45, 0
  br i1 %tobool46, label %if.else81, label %if.then47

if.then47:                                        ; preds = %land.lhs.true41
  %75 = bitcast i8** %chunk48 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #9
  %76 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %m_triangleInfoMap49 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 2
  %77 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap49, align 4, !tbaa !12
  %78 = bitcast %struct.btTriangleInfoMap* %77 to i8*
  %79 = bitcast %class.btSerializer* %76 to i8* (%class.btSerializer*, i8*)***
  %vtable50 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %79, align 4, !tbaa !8
  %vfn51 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable50, i64 6
  %80 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn51, align 4
  %call52 = call i8* %80(%class.btSerializer* %76, i8* %78)
  store i8* %call52, i8** %chunk48, align 4, !tbaa !2
  %81 = load i8*, i8** %chunk48, align 4, !tbaa !2
  %tobool53 = icmp ne i8* %81, null
  br i1 %tobool53, label %if.then54, label %if.else56

if.then54:                                        ; preds = %if.then47
  %82 = load i8*, i8** %chunk48, align 4, !tbaa !2
  %83 = bitcast i8* %82 to %struct.btTriangleInfoMapData*
  %84 = load %struct.btTriangleMeshShapeData*, %struct.btTriangleMeshShapeData** %trimeshData, align 4, !tbaa !2
  %m_triangleInfoMap55 = getelementptr inbounds %struct.btTriangleMeshShapeData, %struct.btTriangleMeshShapeData* %84, i32 0, i32 4
  store %struct.btTriangleInfoMapData* %83, %struct.btTriangleInfoMapData** %m_triangleInfoMap55, align 4, !tbaa !47
  br label %if.end80

if.else56:                                        ; preds = %if.then47
  %85 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %m_triangleInfoMap57 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 2
  %86 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap57, align 4, !tbaa !12
  %87 = bitcast %struct.btTriangleInfoMap* %86 to i8*
  %88 = bitcast %class.btSerializer* %85 to i8* (%class.btSerializer*, i8*)***
  %vtable58 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %88, align 4, !tbaa !8
  %vfn59 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable58, i64 7
  %89 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn59, align 4
  %call60 = call i8* %89(%class.btSerializer* %85, i8* %87)
  %90 = bitcast i8* %call60 to %struct.btTriangleInfoMapData*
  %91 = load %struct.btTriangleMeshShapeData*, %struct.btTriangleMeshShapeData** %trimeshData, align 4, !tbaa !2
  %m_triangleInfoMap61 = getelementptr inbounds %struct.btTriangleMeshShapeData, %struct.btTriangleMeshShapeData* %91, i32 0, i32 4
  store %struct.btTriangleInfoMapData* %90, %struct.btTriangleInfoMapData** %m_triangleInfoMap61, align 4, !tbaa !47
  %92 = bitcast i32* %sz62 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %92) #9
  %m_triangleInfoMap63 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 2
  %93 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap63, align 4, !tbaa !12
  %94 = bitcast %struct.btTriangleInfoMap* %93 to i32 (%struct.btTriangleInfoMap*)***
  %vtable64 = load i32 (%struct.btTriangleInfoMap*)**, i32 (%struct.btTriangleInfoMap*)*** %94, align 4, !tbaa !8
  %vfn65 = getelementptr inbounds i32 (%struct.btTriangleInfoMap*)*, i32 (%struct.btTriangleInfoMap*)** %vtable64, i64 2
  %95 = load i32 (%struct.btTriangleInfoMap*)*, i32 (%struct.btTriangleInfoMap*)** %vfn65, align 4
  %call66 = call i32 %95(%struct.btTriangleInfoMap* %93)
  store i32 %call66, i32* %sz62, align 4, !tbaa !44
  %96 = bitcast %class.btChunk** %chunk67 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %96) #9
  %97 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %98 = load i32, i32* %sz62, align 4, !tbaa !44
  %99 = bitcast %class.btSerializer* %97 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable68 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %99, align 4, !tbaa !8
  %vfn69 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable68, i64 4
  %100 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn69, align 4
  %call70 = call %class.btChunk* %100(%class.btSerializer* %97, i32 %98, i32 1)
  store %class.btChunk* %call70, %class.btChunk** %chunk67, align 4, !tbaa !2
  %101 = bitcast i8** %structType71 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %101) #9
  %m_triangleInfoMap72 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 2
  %102 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap72, align 4, !tbaa !12
  %103 = load %class.btChunk*, %class.btChunk** %chunk67, align 4, !tbaa !2
  %m_oldPtr73 = getelementptr inbounds %class.btChunk, %class.btChunk* %103, i32 0, i32 2
  %104 = load i8*, i8** %m_oldPtr73, align 4, !tbaa !45
  %105 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %106 = bitcast %struct.btTriangleInfoMap* %102 to i8* (%struct.btTriangleInfoMap*, i8*, %class.btSerializer*)***
  %vtable74 = load i8* (%struct.btTriangleInfoMap*, i8*, %class.btSerializer*)**, i8* (%struct.btTriangleInfoMap*, i8*, %class.btSerializer*)*** %106, align 4, !tbaa !8
  %vfn75 = getelementptr inbounds i8* (%struct.btTriangleInfoMap*, i8*, %class.btSerializer*)*, i8* (%struct.btTriangleInfoMap*, i8*, %class.btSerializer*)** %vtable74, i64 3
  %107 = load i8* (%struct.btTriangleInfoMap*, i8*, %class.btSerializer*)*, i8* (%struct.btTriangleInfoMap*, i8*, %class.btSerializer*)** %vfn75, align 4
  %call76 = call i8* %107(%struct.btTriangleInfoMap* %102, i8* %104, %class.btSerializer* %105)
  store i8* %call76, i8** %structType71, align 4, !tbaa !2
  %108 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %109 = load %class.btChunk*, %class.btChunk** %chunk67, align 4, !tbaa !2
  %110 = load i8*, i8** %structType71, align 4, !tbaa !2
  %m_triangleInfoMap77 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 2
  %111 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap77, align 4, !tbaa !12
  %112 = bitcast %struct.btTriangleInfoMap* %111 to i8*
  %113 = bitcast %class.btSerializer* %108 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable78 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %113, align 4, !tbaa !8
  %vfn79 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable78, i64 5
  %114 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn79, align 4
  call void %114(%class.btSerializer* %108, %class.btChunk* %109, i8* %110, i32 1346456916, i8* %112)
  %115 = bitcast i8** %structType71 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #9
  %116 = bitcast %class.btChunk** %chunk67 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #9
  %117 = bitcast i32* %sz62 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #9
  br label %if.end80

if.end80:                                         ; preds = %if.else56, %if.then54
  %118 = bitcast i8** %chunk48 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #9
  br label %if.end83

if.else81:                                        ; preds = %land.lhs.true41, %if.end39
  %119 = load %struct.btTriangleMeshShapeData*, %struct.btTriangleMeshShapeData** %trimeshData, align 4, !tbaa !2
  %m_triangleInfoMap82 = getelementptr inbounds %struct.btTriangleMeshShapeData, %struct.btTriangleMeshShapeData* %119, i32 0, i32 4
  store %struct.btTriangleInfoMapData* null, %struct.btTriangleInfoMapData** %m_triangleInfoMap82, align 4, !tbaa !47
  br label %if.end83

if.end83:                                         ; preds = %if.else81, %if.end80
  %120 = bitcast %struct.btTriangleMeshShapeData** %trimeshData to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #9
  ret i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str, i32 0, i32 0)
}

declare i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape*, i8*, %class.btSerializer*) unnamed_addr #1

define hidden void @_ZNK22btBvhTriangleMeshShape18serializeSingleBvhEP12btSerializer(%class.btBvhTriangleMeshShape* %this, %class.btSerializer* %serializer) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %len = alloca i32, align 4
  %chunk = alloca %class.btChunk*, align 4
  %structType = alloca i8*, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %m_bvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %0 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh, align 4, !tbaa !10
  %tobool = icmp ne %class.btOptimizedBvh* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast i32* %len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %m_bvh2 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %2 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh2, align 4, !tbaa !10
  %3 = bitcast %class.btOptimizedBvh* %2 to %class.btQuantizedBvh*
  %4 = bitcast %class.btQuantizedBvh* %3 to i32 (%class.btQuantizedBvh*)***
  %vtable = load i32 (%class.btQuantizedBvh*)**, i32 (%class.btQuantizedBvh*)*** %4, align 4, !tbaa !8
  %vfn = getelementptr inbounds i32 (%class.btQuantizedBvh*)*, i32 (%class.btQuantizedBvh*)** %vtable, i64 3
  %5 = load i32 (%class.btQuantizedBvh*)*, i32 (%class.btQuantizedBvh*)** %vfn, align 4
  %call = call i32 %5(%class.btQuantizedBvh* %3)
  store i32 %call, i32* %len, align 4, !tbaa !44
  %6 = bitcast %class.btChunk** %chunk to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %7 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %8 = load i32, i32* %len, align 4, !tbaa !44
  %9 = bitcast %class.btSerializer* %7 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable3 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %9, align 4, !tbaa !8
  %vfn4 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable3, i64 4
  %10 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn4, align 4
  %call5 = call %class.btChunk* %10(%class.btSerializer* %7, i32 %8, i32 1)
  store %class.btChunk* %call5, %class.btChunk** %chunk, align 4, !tbaa !2
  %11 = bitcast i8** %structType to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  %m_bvh6 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %12 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh6, align 4, !tbaa !10
  %13 = bitcast %class.btOptimizedBvh* %12 to %class.btQuantizedBvh*
  %14 = load %class.btChunk*, %class.btChunk** %chunk, align 4, !tbaa !2
  %m_oldPtr = getelementptr inbounds %class.btChunk, %class.btChunk* %14, i32 0, i32 2
  %15 = load i8*, i8** %m_oldPtr, align 4, !tbaa !45
  %16 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %17 = bitcast %class.btQuantizedBvh* %13 to i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)***
  %vtable7 = load i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)**, i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)*** %17, align 4, !tbaa !8
  %vfn8 = getelementptr inbounds i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)*, i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)** %vtable7, i64 4
  %18 = load i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)*, i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)** %vfn8, align 4
  %call9 = call i8* %18(%class.btQuantizedBvh* %13, i8* %15, %class.btSerializer* %16)
  store i8* %call9, i8** %structType, align 4, !tbaa !2
  %19 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %20 = load %class.btChunk*, %class.btChunk** %chunk, align 4, !tbaa !2
  %21 = load i8*, i8** %structType, align 4, !tbaa !2
  %m_bvh10 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %22 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh10, align 4, !tbaa !10
  %23 = bitcast %class.btOptimizedBvh* %22 to i8*
  %24 = bitcast %class.btSerializer* %19 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable11 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %24, align 4, !tbaa !8
  %vfn12 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable11, i64 5
  %25 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn12, align 4
  call void %25(%class.btSerializer* %19, %class.btChunk* %20, i8* %21, i32 1213612625, i8* %23)
  %26 = bitcast i8** %structType to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #9
  %27 = bitcast %class.btChunk** %chunk to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #9
  %28 = bitcast i32* %len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

define hidden void @_ZNK22btBvhTriangleMeshShape30serializeSingleTriangleInfoMapEP12btSerializer(%class.btBvhTriangleMeshShape* %this, %class.btSerializer* %serializer) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %len = alloca i32, align 4
  %chunk = alloca %class.btChunk*, align 4
  %structType = alloca i8*, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %m_triangleInfoMap = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 2
  %0 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap, align 4, !tbaa !12
  %tobool = icmp ne %struct.btTriangleInfoMap* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast i32* %len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %m_triangleInfoMap2 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 2
  %2 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap2, align 4, !tbaa !12
  %3 = bitcast %struct.btTriangleInfoMap* %2 to i32 (%struct.btTriangleInfoMap*)***
  %vtable = load i32 (%struct.btTriangleInfoMap*)**, i32 (%struct.btTriangleInfoMap*)*** %3, align 4, !tbaa !8
  %vfn = getelementptr inbounds i32 (%struct.btTriangleInfoMap*)*, i32 (%struct.btTriangleInfoMap*)** %vtable, i64 2
  %4 = load i32 (%struct.btTriangleInfoMap*)*, i32 (%struct.btTriangleInfoMap*)** %vfn, align 4
  %call = call i32 %4(%struct.btTriangleInfoMap* %2)
  store i32 %call, i32* %len, align 4, !tbaa !44
  %5 = bitcast %class.btChunk** %chunk to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %7 = load i32, i32* %len, align 4, !tbaa !44
  %8 = bitcast %class.btSerializer* %6 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable3 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %8, align 4, !tbaa !8
  %vfn4 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable3, i64 4
  %9 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn4, align 4
  %call5 = call %class.btChunk* %9(%class.btSerializer* %6, i32 %7, i32 1)
  store %class.btChunk* %call5, %class.btChunk** %chunk, align 4, !tbaa !2
  %10 = bitcast i8** %structType to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %m_triangleInfoMap6 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 2
  %11 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap6, align 4, !tbaa !12
  %12 = load %class.btChunk*, %class.btChunk** %chunk, align 4, !tbaa !2
  %m_oldPtr = getelementptr inbounds %class.btChunk, %class.btChunk* %12, i32 0, i32 2
  %13 = load i8*, i8** %m_oldPtr, align 4, !tbaa !45
  %14 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %15 = bitcast %struct.btTriangleInfoMap* %11 to i8* (%struct.btTriangleInfoMap*, i8*, %class.btSerializer*)***
  %vtable7 = load i8* (%struct.btTriangleInfoMap*, i8*, %class.btSerializer*)**, i8* (%struct.btTriangleInfoMap*, i8*, %class.btSerializer*)*** %15, align 4, !tbaa !8
  %vfn8 = getelementptr inbounds i8* (%struct.btTriangleInfoMap*, i8*, %class.btSerializer*)*, i8* (%struct.btTriangleInfoMap*, i8*, %class.btSerializer*)** %vtable7, i64 3
  %16 = load i8* (%struct.btTriangleInfoMap*, i8*, %class.btSerializer*)*, i8* (%struct.btTriangleInfoMap*, i8*, %class.btSerializer*)** %vfn8, align 4
  %call9 = call i8* %16(%struct.btTriangleInfoMap* %11, i8* %13, %class.btSerializer* %14)
  store i8* %call9, i8** %structType, align 4, !tbaa !2
  %17 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %18 = load %class.btChunk*, %class.btChunk** %chunk, align 4, !tbaa !2
  %19 = load i8*, i8** %structType, align 4, !tbaa !2
  %m_triangleInfoMap10 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 2
  %20 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap10, align 4, !tbaa !12
  %21 = bitcast %struct.btTriangleInfoMap* %20 to i8*
  %22 = bitcast %class.btSerializer* %17 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable11 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %22, align 4, !tbaa !8
  %vfn12 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable11, i64 5
  %23 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn12, align 4
  call void %23(%class.btSerializer* %17, %class.btChunk* %18, i8* %19, i32 1346456916, i8* %21)
  %24 = bitcast i8** %structType to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #9
  %25 = bitcast %class.btChunk** %chunk to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #9
  %26 = bitcast i32* %len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

declare void @_ZNK19btTriangleMeshShape7getAabbERK11btTransformR9btVector3S4_(%class.btTriangleMeshShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

declare void @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf(%class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4)) unnamed_addr #1

declare float @_ZNK16btCollisionShape20getAngularMotionDiscEv(%class.btCollisionShape*) unnamed_addr #1

declare float @_ZNK16btCollisionShape27getContactBreakingThresholdEf(%class.btCollisionShape*, float) unnamed_addr #1

declare nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK19btTriangleMeshShape15getLocalScalingEv(%class.btTriangleMeshShape*) unnamed_addr #1

declare void @_ZNK19btTriangleMeshShape21calculateLocalInertiaEfR9btVector3(%class.btTriangleMeshShape*, float, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden i8* @_ZNK22btBvhTriangleMeshShape7getNameEv(%class.btBvhTriangleMeshShape* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  ret i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.1, i32 0, i32 0)
}

define linkonce_odr hidden void @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btCollisionShape* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !33
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store float 1.000000e+00, float* %ref.tmp2, align 4, !tbaa !33
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 1.000000e+00, float* %ref.tmp3, align 4, !tbaa !33
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN14btConcaveShape9setMarginEf(%class.btConcaveShape* %this, float %collisionMargin) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btConcaveShape*, align 4
  %collisionMargin.addr = alloca float, align 4
  store %class.btConcaveShape* %this, %class.btConcaveShape** %this.addr, align 4, !tbaa !2
  store float %collisionMargin, float* %collisionMargin.addr, align 4, !tbaa !33
  %this1 = load %class.btConcaveShape*, %class.btConcaveShape** %this.addr, align 4
  %0 = load float, float* %collisionMargin.addr, align 4, !tbaa !33
  %m_collisionMargin = getelementptr inbounds %class.btConcaveShape, %class.btConcaveShape* %this1, i32 0, i32 1
  store float %0, float* %m_collisionMargin, align 4, !tbaa !35
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK14btConcaveShape9getMarginEv(%class.btConcaveShape* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btConcaveShape*, align 4
  store %class.btConcaveShape* %this, %class.btConcaveShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConcaveShape*, %class.btConcaveShape** %this.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btConcaveShape, %class.btConcaveShape* %this1, i32 0, i32 1
  %0 = load float, float* %m_collisionMargin, align 4, !tbaa !35
  ret float %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK22btBvhTriangleMeshShape28calculateSerializeBufferSizeEv(%class.btBvhTriangleMeshShape* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  ret i32 60
}

declare void @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer(%class.btCollisionShape*, %class.btSerializer*) unnamed_addr #1

declare void @_ZNK19btTriangleMeshShape24localGetSupportingVertexERK9btVector3(%class.btVector3* sret align 4, %class.btTriangleMeshShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

define linkonce_odr hidden void @_ZNK19btTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTriangleMeshShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %vec) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleMeshShape*, align 4
  %vec.addr = alloca %class.btVector3*, align 4
  store %class.btTriangleMeshShape* %this, %class.btTriangleMeshShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %vec, %class.btVector3** %vec.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleMeshShape*, %class.btTriangleMeshShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %vec.addr, align 4, !tbaa !2
  %1 = bitcast %class.btTriangleMeshShape* %this1 to void (%class.btVector3*, %class.btTriangleMeshShape*, %class.btVector3*)***
  %vtable = load void (%class.btVector3*, %class.btTriangleMeshShape*, %class.btVector3*)**, void (%class.btVector3*, %class.btTriangleMeshShape*, %class.btVector3*)*** %1, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btVector3*, %class.btTriangleMeshShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btTriangleMeshShape*, %class.btVector3*)** %vtable, i64 17
  %2 = load void (%class.btVector3*, %class.btTriangleMeshShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btTriangleMeshShape*, %class.btVector3*)** %vfn, align 4
  call void %2(%class.btVector3* sret align 4 %agg.result, %class.btTriangleMeshShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #3 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store float* %b, float** %b.addr, align 4, !tbaa !2
  %0 = load float*, float** %b.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !33
  %2 = load float*, float** %a.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !33
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !33
  %6 = load float*, float** %a.addr, align 4, !tbaa !2
  store float %5, float* %6, align 4, !tbaa !33
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #3 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store float* %b, float** %b.addr, align 4, !tbaa !2
  %0 = load float*, float** %a.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !33
  %2 = load float*, float** %b.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !33
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !33
  %6 = load float*, float** %a.addr, align 4, !tbaa !2
  store float %5, float* %6, align 4, !tbaa !33
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btNodeOverlapCallback* @_ZN21btNodeOverlapCallbackC2Ev(%class.btNodeOverlapCallback* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btNodeOverlapCallback*, align 4
  store %class.btNodeOverlapCallback* %this, %class.btNodeOverlapCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %this.addr, align 4
  %0 = bitcast %class.btNodeOverlapCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV21btNodeOverlapCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !8
  ret %class.btNodeOverlapCallback* %this1
}

; Function Attrs: inlinehint nounwind
define internal void @_ZZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackD0Ev(%struct.MyNodeOverlapCallback* %this) unnamed_addr #3 {
entry:
  %this.addr = alloca %struct.MyNodeOverlapCallback*, align 4
  store %struct.MyNodeOverlapCallback* %this, %struct.MyNodeOverlapCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.MyNodeOverlapCallback*, %struct.MyNodeOverlapCallback** %this.addr, align 4
  %call = call %struct.MyNodeOverlapCallback* bitcast (%class.btNodeOverlapCallback* (%class.btNodeOverlapCallback*)* @_ZN21btNodeOverlapCallbackD2Ev to %struct.MyNodeOverlapCallback* (%struct.MyNodeOverlapCallback*)*)(%struct.MyNodeOverlapCallback* %this1) #9
  %0 = bitcast %struct.MyNodeOverlapCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

define internal void @_ZZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallback11processNodeEii(%struct.MyNodeOverlapCallback* %this, i32 %nodeSubPart, i32 %nodeTriangleIndex) unnamed_addr #0 {
entry:
  %this.addr = alloca %struct.MyNodeOverlapCallback*, align 4
  %nodeSubPart.addr = alloca i32, align 4
  %nodeTriangleIndex.addr = alloca i32, align 4
  %m_triangle = alloca [3 x %class.btVector3], align 16
  %vertexbase = alloca i8*, align 4
  %numverts = alloca i32, align 4
  %type = alloca i32, align 4
  %stride = alloca i32, align 4
  %indexbase = alloca i8*, align 4
  %indexstride = alloca i32, align 4
  %numfaces = alloca i32, align 4
  %indicestype = alloca i32, align 4
  %gfxbase = alloca i32*, align 4
  %meshScaling = alloca %class.btVector3*, align 4
  %j = alloca i32, align 4
  %graphicsindex = alloca i32, align 4
  %graphicsbase = alloca float*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %graphicsbase23 = alloca double*, align 4
  %ref.tmp26 = alloca %class.btVector3, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  store %struct.MyNodeOverlapCallback* %this, %struct.MyNodeOverlapCallback** %this.addr, align 4, !tbaa !2
  store i32 %nodeSubPart, i32* %nodeSubPart.addr, align 4, !tbaa !44
  store i32 %nodeTriangleIndex, i32* %nodeTriangleIndex.addr, align 4, !tbaa !44
  %this1 = load %struct.MyNodeOverlapCallback*, %struct.MyNodeOverlapCallback** %this.addr, align 4
  %0 = bitcast [3 x %class.btVector3]* %m_triangle to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %0) #9
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_triangle, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %1 = bitcast i8** %vertexbase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = bitcast i32* %numverts to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %3 = bitcast i32* %type to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = bitcast i32* %stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = bitcast i8** %indexbase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = bitcast i32* %indexstride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %7 = bitcast i32* %numfaces to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  %8 = bitcast i32* %indicestype to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  %m_meshInterface = getelementptr inbounds %struct.MyNodeOverlapCallback, %struct.MyNodeOverlapCallback* %this1, i32 0, i32 1
  %9 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface, align 4, !tbaa !24
  %10 = load i32, i32* %nodeSubPart.addr, align 4, !tbaa !44
  %11 = bitcast %class.btStridingMeshInterface* %9 to void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)***
  %vtable = load void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)**, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*** %11, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vtable, i64 4
  %12 = load void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vfn, align 4
  call void %12(%class.btStridingMeshInterface* %9, i8** %vertexbase, i32* nonnull align 4 dereferenceable(4) %numverts, i32* nonnull align 4 dereferenceable(4) %type, i32* nonnull align 4 dereferenceable(4) %stride, i8** %indexbase, i32* nonnull align 4 dereferenceable(4) %indexstride, i32* nonnull align 4 dereferenceable(4) %numfaces, i32* nonnull align 4 dereferenceable(4) %indicestype, i32 %10)
  %13 = bitcast i32** %gfxbase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %14 = load i8*, i8** %indexbase, align 4, !tbaa !2
  %15 = load i32, i32* %nodeTriangleIndex.addr, align 4, !tbaa !44
  %16 = load i32, i32* %indexstride, align 4, !tbaa !44
  %mul = mul nsw i32 %15, %16
  %add.ptr = getelementptr inbounds i8, i8* %14, i32 %mul
  %17 = bitcast i8* %add.ptr to i32*
  store i32* %17, i32** %gfxbase, align 4, !tbaa !2
  %18 = bitcast %class.btVector3** %meshScaling to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #9
  %m_meshInterface2 = getelementptr inbounds %struct.MyNodeOverlapCallback, %struct.MyNodeOverlapCallback* %this1, i32 0, i32 1
  %19 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface2, align 4, !tbaa !24
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btStridingMeshInterface10getScalingEv(%class.btStridingMeshInterface* %19)
  store %class.btVector3* %call3, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %20 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #9
  store i32 2, i32* %j, align 4, !tbaa !44
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %arrayctor.cont
  %21 = load i32, i32* %j, align 4, !tbaa !44
  %cmp = icmp sge i32 %21, 0
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %22 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %23 = bitcast i32* %graphicsindex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #9
  %24 = load i32, i32* %indicestype, align 4, !tbaa !48
  %cmp4 = icmp eq i32 %24, 3
  br i1 %cmp4, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %25 = load i32*, i32** %gfxbase, align 4, !tbaa !2
  %26 = bitcast i32* %25 to i16*
  %27 = load i32, i32* %j, align 4, !tbaa !44
  %arrayidx = getelementptr inbounds i16, i16* %26, i32 %27
  %28 = load i16, i16* %arrayidx, align 2, !tbaa !50
  %conv = zext i16 %28 to i32
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %29 = load i32*, i32** %gfxbase, align 4, !tbaa !2
  %30 = load i32, i32* %j, align 4, !tbaa !44
  %arrayidx5 = getelementptr inbounds i32, i32* %29, i32 %30
  %31 = load i32, i32* %arrayidx5, align 4, !tbaa !44
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv, %cond.true ], [ %31, %cond.false ]
  store i32 %cond, i32* %graphicsindex, align 4, !tbaa !44
  %32 = load i32, i32* %type, align 4, !tbaa !48
  %cmp6 = icmp eq i32 %32, 0
  br i1 %cmp6, label %if.then, label %if.else

if.then:                                          ; preds = %cond.end
  %33 = bitcast float** %graphicsbase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #9
  %34 = load i8*, i8** %vertexbase, align 4, !tbaa !2
  %35 = load i32, i32* %graphicsindex, align 4, !tbaa !44
  %36 = load i32, i32* %stride, align 4, !tbaa !44
  %mul7 = mul nsw i32 %35, %36
  %add.ptr8 = getelementptr inbounds i8, i8* %34, i32 %mul7
  %37 = bitcast i8* %add.ptr8 to float*
  store float* %37, float** %graphicsbase, align 4, !tbaa !2
  %38 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %38) #9
  %39 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #9
  %40 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds float, float* %40, i32 0
  %41 = load float, float* %arrayidx10, align 4, !tbaa !33
  %42 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %42)
  %43 = load float, float* %call11, align 4, !tbaa !33
  %mul12 = fmul float %41, %43
  store float %mul12, float* %ref.tmp9, align 4, !tbaa !33
  %44 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #9
  %45 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds float, float* %45, i32 1
  %46 = load float, float* %arrayidx14, align 4, !tbaa !33
  %47 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %47)
  %48 = load float, float* %call15, align 4, !tbaa !33
  %mul16 = fmul float %46, %48
  store float %mul16, float* %ref.tmp13, align 4, !tbaa !33
  %49 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #9
  %50 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx18 = getelementptr inbounds float, float* %50, i32 2
  %51 = load float, float* %arrayidx18, align 4, !tbaa !33
  %52 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %52)
  %53 = load float, float* %call19, align 4, !tbaa !33
  %mul20 = fmul float %51, %53
  store float %mul20, float* %ref.tmp17, align 4, !tbaa !33
  %call21 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  %54 = load i32, i32* %j, align 4, !tbaa !44
  %arrayidx22 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_triangle, i32 0, i32 %54
  %55 = bitcast %class.btVector3* %arrayidx22 to i8*
  %56 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %55, i8* align 4 %56, i32 16, i1 false), !tbaa.struct !52
  %57 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #9
  %58 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #9
  %59 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #9
  %60 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %60) #9
  %61 = bitcast float** %graphicsbase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #9
  br label %if.end

if.else:                                          ; preds = %cond.end
  %62 = bitcast double** %graphicsbase23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #9
  %63 = load i8*, i8** %vertexbase, align 4, !tbaa !2
  %64 = load i32, i32* %graphicsindex, align 4, !tbaa !44
  %65 = load i32, i32* %stride, align 4, !tbaa !44
  %mul24 = mul nsw i32 %64, %65
  %add.ptr25 = getelementptr inbounds i8, i8* %63, i32 %mul24
  %66 = bitcast i8* %add.ptr25 to double*
  store double* %66, double** %graphicsbase23, align 4, !tbaa !2
  %67 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %67) #9
  %68 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #9
  %69 = load double*, double** %graphicsbase23, align 4, !tbaa !2
  %arrayidx28 = getelementptr inbounds double, double* %69, i32 0
  %70 = load double, double* %arrayidx28, align 8, !tbaa !54
  %conv29 = fptrunc double %70 to float
  %71 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %call30 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %71)
  %72 = load float, float* %call30, align 4, !tbaa !33
  %mul31 = fmul float %conv29, %72
  store float %mul31, float* %ref.tmp27, align 4, !tbaa !33
  %73 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #9
  %74 = load double*, double** %graphicsbase23, align 4, !tbaa !2
  %arrayidx33 = getelementptr inbounds double, double* %74, i32 1
  %75 = load double, double* %arrayidx33, align 8, !tbaa !54
  %conv34 = fptrunc double %75 to float
  %76 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %call35 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %76)
  %77 = load float, float* %call35, align 4, !tbaa !33
  %mul36 = fmul float %conv34, %77
  store float %mul36, float* %ref.tmp32, align 4, !tbaa !33
  %78 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %78) #9
  %79 = load double*, double** %graphicsbase23, align 4, !tbaa !2
  %arrayidx38 = getelementptr inbounds double, double* %79, i32 2
  %80 = load double, double* %arrayidx38, align 8, !tbaa !54
  %conv39 = fptrunc double %80 to float
  %81 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %81)
  %82 = load float, float* %call40, align 4, !tbaa !33
  %mul41 = fmul float %conv39, %82
  store float %mul41, float* %ref.tmp37, align 4, !tbaa !33
  %call42 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp26, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp32, float* nonnull align 4 dereferenceable(4) %ref.tmp37)
  %83 = load i32, i32* %j, align 4, !tbaa !44
  %arrayidx43 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_triangle, i32 0, i32 %83
  %84 = bitcast %class.btVector3* %arrayidx43 to i8*
  %85 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %84, i8* align 4 %85, i32 16, i1 false), !tbaa.struct !52
  %86 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #9
  %87 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #9
  %88 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #9
  %89 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %89) #9
  %90 = bitcast double** %graphicsbase23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #9
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %91 = bitcast i32* %graphicsindex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #9
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %92 = load i32, i32* %j, align 4, !tbaa !44
  %dec = add nsw i32 %92, -1
  store i32 %dec, i32* %j, align 4, !tbaa !44
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %m_callback = getelementptr inbounds %struct.MyNodeOverlapCallback, %struct.MyNodeOverlapCallback* %this1, i32 0, i32 2
  %93 = load %class.btTriangleCallback*, %class.btTriangleCallback** %m_callback, align 4, !tbaa !26
  %arraydecay = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_triangle, i32 0, i32 0
  %94 = load i32, i32* %nodeSubPart.addr, align 4, !tbaa !44
  %95 = load i32, i32* %nodeTriangleIndex.addr, align 4, !tbaa !44
  %96 = bitcast %class.btTriangleCallback* %93 to void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)***
  %vtable44 = load void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)**, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*** %96, align 4, !tbaa !8
  %vfn45 = getelementptr inbounds void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)** %vtable44, i64 2
  %97 = load void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)** %vfn45, align 4
  call void %97(%class.btTriangleCallback* %93, %class.btVector3* %arraydecay, i32 %94, i32 %95)
  %m_meshInterface46 = getelementptr inbounds %struct.MyNodeOverlapCallback, %struct.MyNodeOverlapCallback* %this1, i32 0, i32 1
  %98 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface46, align 4, !tbaa !24
  %99 = load i32, i32* %nodeSubPart.addr, align 4, !tbaa !44
  %100 = bitcast %class.btStridingMeshInterface* %98 to void (%class.btStridingMeshInterface*, i32)***
  %vtable47 = load void (%class.btStridingMeshInterface*, i32)**, void (%class.btStridingMeshInterface*, i32)*** %100, align 4, !tbaa !8
  %vfn48 = getelementptr inbounds void (%class.btStridingMeshInterface*, i32)*, void (%class.btStridingMeshInterface*, i32)** %vtable47, i64 6
  %101 = load void (%class.btStridingMeshInterface*, i32)*, void (%class.btStridingMeshInterface*, i32)** %vfn48, align 4
  call void %101(%class.btStridingMeshInterface* %98, i32 %99)
  %102 = bitcast %class.btVector3** %meshScaling to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #9
  %103 = bitcast i32** %gfxbase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #9
  %104 = bitcast i32* %indicestype to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #9
  %105 = bitcast i32* %numfaces to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #9
  %106 = bitcast i32* %indexstride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #9
  %107 = bitcast i8** %indexbase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #9
  %108 = bitcast i32* %stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #9
  %109 = bitcast i32* %type to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #9
  %110 = bitcast i32* %numverts to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #9
  %111 = bitcast i8** %vertexbase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #9
  %112 = bitcast [3 x %class.btVector3]* %m_triangle to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %112) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN21btNodeOverlapCallbackD0Ev(%class.btNodeOverlapCallback* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btNodeOverlapCallback*, align 4
  store %class.btNodeOverlapCallback* %this, %class.btNodeOverlapCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %this.addr, align 4
  call void @llvm.trap() #11
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #7

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #8

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btStridingMeshInterface10getScalingEv(%class.btStridingMeshInterface* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  %m_scaling = getelementptr inbounds %class.btStridingMeshInterface, %class.btStridingMeshInterface* %this1, i32 0, i32 1
  ret %class.btVector3* %m_scaling
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !33
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !33
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !33
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !33
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !33
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !33
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !33
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: inlinehint nounwind
define internal void @_ZZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_EN21MyNodeOverlapCallbackD0Ev(%struct.MyNodeOverlapCallback.20* %this) unnamed_addr #3 {
entry:
  %this.addr = alloca %struct.MyNodeOverlapCallback.20*, align 4
  store %struct.MyNodeOverlapCallback.20* %this, %struct.MyNodeOverlapCallback.20** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.MyNodeOverlapCallback.20*, %struct.MyNodeOverlapCallback.20** %this.addr, align 4
  %call = call %struct.MyNodeOverlapCallback.20* bitcast (%class.btNodeOverlapCallback* (%class.btNodeOverlapCallback*)* @_ZN21btNodeOverlapCallbackD2Ev to %struct.MyNodeOverlapCallback.20* (%struct.MyNodeOverlapCallback.20*)*)(%struct.MyNodeOverlapCallback.20* %this1) #9
  %0 = bitcast %struct.MyNodeOverlapCallback.20* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

define internal void @_ZZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_EN21MyNodeOverlapCallback11processNodeEii(%struct.MyNodeOverlapCallback.20* %this, i32 %nodeSubPart, i32 %nodeTriangleIndex) unnamed_addr #0 {
entry:
  %this.addr = alloca %struct.MyNodeOverlapCallback.20*, align 4
  %nodeSubPart.addr = alloca i32, align 4
  %nodeTriangleIndex.addr = alloca i32, align 4
  %m_triangle = alloca [3 x %class.btVector3], align 16
  %vertexbase = alloca i8*, align 4
  %numverts = alloca i32, align 4
  %type = alloca i32, align 4
  %stride = alloca i32, align 4
  %indexbase = alloca i8*, align 4
  %indexstride = alloca i32, align 4
  %numfaces = alloca i32, align 4
  %indicestype = alloca i32, align 4
  %gfxbase = alloca i32*, align 4
  %meshScaling = alloca %class.btVector3*, align 4
  %j = alloca i32, align 4
  %graphicsindex = alloca i32, align 4
  %graphicsbase = alloca float*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %graphicsbase23 = alloca double*, align 4
  %ref.tmp26 = alloca %class.btVector3, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  store %struct.MyNodeOverlapCallback.20* %this, %struct.MyNodeOverlapCallback.20** %this.addr, align 4, !tbaa !2
  store i32 %nodeSubPart, i32* %nodeSubPart.addr, align 4, !tbaa !44
  store i32 %nodeTriangleIndex, i32* %nodeTriangleIndex.addr, align 4, !tbaa !44
  %this1 = load %struct.MyNodeOverlapCallback.20*, %struct.MyNodeOverlapCallback.20** %this.addr, align 4
  %0 = bitcast [3 x %class.btVector3]* %m_triangle to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %0) #9
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_triangle, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %1 = bitcast i8** %vertexbase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = bitcast i32* %numverts to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %3 = bitcast i32* %type to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = bitcast i32* %stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = bitcast i8** %indexbase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = bitcast i32* %indexstride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %7 = bitcast i32* %numfaces to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  %8 = bitcast i32* %indicestype to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  %m_meshInterface = getelementptr inbounds %struct.MyNodeOverlapCallback.20, %struct.MyNodeOverlapCallback.20* %this1, i32 0, i32 1
  %9 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface, align 4, !tbaa !27
  %10 = load i32, i32* %nodeSubPart.addr, align 4, !tbaa !44
  %11 = bitcast %class.btStridingMeshInterface* %9 to void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)***
  %vtable = load void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)**, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*** %11, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vtable, i64 4
  %12 = load void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vfn, align 4
  call void %12(%class.btStridingMeshInterface* %9, i8** %vertexbase, i32* nonnull align 4 dereferenceable(4) %numverts, i32* nonnull align 4 dereferenceable(4) %type, i32* nonnull align 4 dereferenceable(4) %stride, i8** %indexbase, i32* nonnull align 4 dereferenceable(4) %indexstride, i32* nonnull align 4 dereferenceable(4) %numfaces, i32* nonnull align 4 dereferenceable(4) %indicestype, i32 %10)
  %13 = bitcast i32** %gfxbase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %14 = load i8*, i8** %indexbase, align 4, !tbaa !2
  %15 = load i32, i32* %nodeTriangleIndex.addr, align 4, !tbaa !44
  %16 = load i32, i32* %indexstride, align 4, !tbaa !44
  %mul = mul nsw i32 %15, %16
  %add.ptr = getelementptr inbounds i8, i8* %14, i32 %mul
  %17 = bitcast i8* %add.ptr to i32*
  store i32* %17, i32** %gfxbase, align 4, !tbaa !2
  %18 = bitcast %class.btVector3** %meshScaling to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #9
  %m_meshInterface2 = getelementptr inbounds %struct.MyNodeOverlapCallback.20, %struct.MyNodeOverlapCallback.20* %this1, i32 0, i32 1
  %19 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface2, align 4, !tbaa !27
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btStridingMeshInterface10getScalingEv(%class.btStridingMeshInterface* %19)
  store %class.btVector3* %call3, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %20 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #9
  store i32 2, i32* %j, align 4, !tbaa !44
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %arrayctor.cont
  %21 = load i32, i32* %j, align 4, !tbaa !44
  %cmp = icmp sge i32 %21, 0
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %22 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %23 = bitcast i32* %graphicsindex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #9
  %24 = load i32, i32* %indicestype, align 4, !tbaa !48
  %cmp4 = icmp eq i32 %24, 3
  br i1 %cmp4, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %25 = load i32*, i32** %gfxbase, align 4, !tbaa !2
  %26 = bitcast i32* %25 to i16*
  %27 = load i32, i32* %j, align 4, !tbaa !44
  %arrayidx = getelementptr inbounds i16, i16* %26, i32 %27
  %28 = load i16, i16* %arrayidx, align 2, !tbaa !50
  %conv = zext i16 %28 to i32
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %29 = load i32*, i32** %gfxbase, align 4, !tbaa !2
  %30 = load i32, i32* %j, align 4, !tbaa !44
  %arrayidx5 = getelementptr inbounds i32, i32* %29, i32 %30
  %31 = load i32, i32* %arrayidx5, align 4, !tbaa !44
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv, %cond.true ], [ %31, %cond.false ]
  store i32 %cond, i32* %graphicsindex, align 4, !tbaa !44
  %32 = load i32, i32* %type, align 4, !tbaa !48
  %cmp6 = icmp eq i32 %32, 0
  br i1 %cmp6, label %if.then, label %if.else

if.then:                                          ; preds = %cond.end
  %33 = bitcast float** %graphicsbase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #9
  %34 = load i8*, i8** %vertexbase, align 4, !tbaa !2
  %35 = load i32, i32* %graphicsindex, align 4, !tbaa !44
  %36 = load i32, i32* %stride, align 4, !tbaa !44
  %mul7 = mul nsw i32 %35, %36
  %add.ptr8 = getelementptr inbounds i8, i8* %34, i32 %mul7
  %37 = bitcast i8* %add.ptr8 to float*
  store float* %37, float** %graphicsbase, align 4, !tbaa !2
  %38 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %38) #9
  %39 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #9
  %40 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds float, float* %40, i32 0
  %41 = load float, float* %arrayidx10, align 4, !tbaa !33
  %42 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %42)
  %43 = load float, float* %call11, align 4, !tbaa !33
  %mul12 = fmul float %41, %43
  store float %mul12, float* %ref.tmp9, align 4, !tbaa !33
  %44 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #9
  %45 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds float, float* %45, i32 1
  %46 = load float, float* %arrayidx14, align 4, !tbaa !33
  %47 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %47)
  %48 = load float, float* %call15, align 4, !tbaa !33
  %mul16 = fmul float %46, %48
  store float %mul16, float* %ref.tmp13, align 4, !tbaa !33
  %49 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #9
  %50 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx18 = getelementptr inbounds float, float* %50, i32 2
  %51 = load float, float* %arrayidx18, align 4, !tbaa !33
  %52 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %52)
  %53 = load float, float* %call19, align 4, !tbaa !33
  %mul20 = fmul float %51, %53
  store float %mul20, float* %ref.tmp17, align 4, !tbaa !33
  %call21 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  %54 = load i32, i32* %j, align 4, !tbaa !44
  %arrayidx22 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_triangle, i32 0, i32 %54
  %55 = bitcast %class.btVector3* %arrayidx22 to i8*
  %56 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %55, i8* align 4 %56, i32 16, i1 false), !tbaa.struct !52
  %57 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #9
  %58 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #9
  %59 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #9
  %60 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %60) #9
  %61 = bitcast float** %graphicsbase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #9
  br label %if.end

if.else:                                          ; preds = %cond.end
  %62 = bitcast double** %graphicsbase23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #9
  %63 = load i8*, i8** %vertexbase, align 4, !tbaa !2
  %64 = load i32, i32* %graphicsindex, align 4, !tbaa !44
  %65 = load i32, i32* %stride, align 4, !tbaa !44
  %mul24 = mul nsw i32 %64, %65
  %add.ptr25 = getelementptr inbounds i8, i8* %63, i32 %mul24
  %66 = bitcast i8* %add.ptr25 to double*
  store double* %66, double** %graphicsbase23, align 4, !tbaa !2
  %67 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %67) #9
  %68 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #9
  %69 = load double*, double** %graphicsbase23, align 4, !tbaa !2
  %arrayidx28 = getelementptr inbounds double, double* %69, i32 0
  %70 = load double, double* %arrayidx28, align 8, !tbaa !54
  %conv29 = fptrunc double %70 to float
  %71 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %call30 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %71)
  %72 = load float, float* %call30, align 4, !tbaa !33
  %mul31 = fmul float %conv29, %72
  store float %mul31, float* %ref.tmp27, align 4, !tbaa !33
  %73 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #9
  %74 = load double*, double** %graphicsbase23, align 4, !tbaa !2
  %arrayidx33 = getelementptr inbounds double, double* %74, i32 1
  %75 = load double, double* %arrayidx33, align 8, !tbaa !54
  %conv34 = fptrunc double %75 to float
  %76 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %call35 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %76)
  %77 = load float, float* %call35, align 4, !tbaa !33
  %mul36 = fmul float %conv34, %77
  store float %mul36, float* %ref.tmp32, align 4, !tbaa !33
  %78 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %78) #9
  %79 = load double*, double** %graphicsbase23, align 4, !tbaa !2
  %arrayidx38 = getelementptr inbounds double, double* %79, i32 2
  %80 = load double, double* %arrayidx38, align 8, !tbaa !54
  %conv39 = fptrunc double %80 to float
  %81 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %81)
  %82 = load float, float* %call40, align 4, !tbaa !33
  %mul41 = fmul float %conv39, %82
  store float %mul41, float* %ref.tmp37, align 4, !tbaa !33
  %call42 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp26, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp32, float* nonnull align 4 dereferenceable(4) %ref.tmp37)
  %83 = load i32, i32* %j, align 4, !tbaa !44
  %arrayidx43 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_triangle, i32 0, i32 %83
  %84 = bitcast %class.btVector3* %arrayidx43 to i8*
  %85 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %84, i8* align 4 %85, i32 16, i1 false), !tbaa.struct !52
  %86 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #9
  %87 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #9
  %88 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #9
  %89 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %89) #9
  %90 = bitcast double** %graphicsbase23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #9
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %91 = bitcast i32* %graphicsindex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #9
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %92 = load i32, i32* %j, align 4, !tbaa !44
  %dec = add nsw i32 %92, -1
  store i32 %dec, i32* %j, align 4, !tbaa !44
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %m_callback = getelementptr inbounds %struct.MyNodeOverlapCallback.20, %struct.MyNodeOverlapCallback.20* %this1, i32 0, i32 2
  %93 = load %class.btTriangleCallback*, %class.btTriangleCallback** %m_callback, align 4, !tbaa !29
  %arraydecay = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_triangle, i32 0, i32 0
  %94 = load i32, i32* %nodeSubPart.addr, align 4, !tbaa !44
  %95 = load i32, i32* %nodeTriangleIndex.addr, align 4, !tbaa !44
  %96 = bitcast %class.btTriangleCallback* %93 to void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)***
  %vtable44 = load void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)**, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*** %96, align 4, !tbaa !8
  %vfn45 = getelementptr inbounds void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)** %vtable44, i64 2
  %97 = load void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)** %vfn45, align 4
  call void %97(%class.btTriangleCallback* %93, %class.btVector3* %arraydecay, i32 %94, i32 %95)
  %m_meshInterface46 = getelementptr inbounds %struct.MyNodeOverlapCallback.20, %struct.MyNodeOverlapCallback.20* %this1, i32 0, i32 1
  %98 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface46, align 4, !tbaa !27
  %99 = load i32, i32* %nodeSubPart.addr, align 4, !tbaa !44
  %100 = bitcast %class.btStridingMeshInterface* %98 to void (%class.btStridingMeshInterface*, i32)***
  %vtable47 = load void (%class.btStridingMeshInterface*, i32)**, void (%class.btStridingMeshInterface*, i32)*** %100, align 4, !tbaa !8
  %vfn48 = getelementptr inbounds void (%class.btStridingMeshInterface*, i32)*, void (%class.btStridingMeshInterface*, i32)** %vtable47, i64 6
  %101 = load void (%class.btStridingMeshInterface*, i32)*, void (%class.btStridingMeshInterface*, i32)** %vfn48, align 4
  call void %101(%class.btStridingMeshInterface* %98, i32 %99)
  %102 = bitcast %class.btVector3** %meshScaling to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #9
  %103 = bitcast i32** %gfxbase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #9
  %104 = bitcast i32* %indicestype to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #9
  %105 = bitcast i32* %numfaces to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #9
  %106 = bitcast i32* %indexstride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #9
  %107 = bitcast i8** %indexbase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #9
  %108 = bitcast i32* %stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #9
  %109 = bitcast i32* %type to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #9
  %110 = bitcast i32* %numverts to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #9
  %111 = bitcast i8** %vertexbase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #9
  %112 = bitcast [3 x %class.btVector3]* %m_triangle to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %112) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @_ZZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackD0Ev(%struct.MyNodeOverlapCallback.21* %this) unnamed_addr #3 {
entry:
  %this.addr = alloca %struct.MyNodeOverlapCallback.21*, align 4
  store %struct.MyNodeOverlapCallback.21* %this, %struct.MyNodeOverlapCallback.21** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.MyNodeOverlapCallback.21*, %struct.MyNodeOverlapCallback.21** %this.addr, align 4
  %call = call %struct.MyNodeOverlapCallback.21* bitcast (%class.btNodeOverlapCallback* (%class.btNodeOverlapCallback*)* @_ZN21btNodeOverlapCallbackD2Ev to %struct.MyNodeOverlapCallback.21* (%struct.MyNodeOverlapCallback.21*)*)(%struct.MyNodeOverlapCallback.21* %this1) #9
  %0 = bitcast %struct.MyNodeOverlapCallback.21* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

define internal void @_ZZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallback11processNodeEii(%struct.MyNodeOverlapCallback.21* %this, i32 %nodeSubPart, i32 %nodeTriangleIndex) unnamed_addr #0 {
entry:
  %this.addr = alloca %struct.MyNodeOverlapCallback.21*, align 4
  %nodeSubPart.addr = alloca i32, align 4
  %nodeTriangleIndex.addr = alloca i32, align 4
  %vertexbase = alloca i8*, align 4
  %numverts = alloca i32, align 4
  %type = alloca i32, align 4
  %stride = alloca i32, align 4
  %indexbase = alloca i8*, align 4
  %indexstride = alloca i32, align 4
  %numfaces = alloca i32, align 4
  %indicestype = alloca i32, align 4
  %gfxbase = alloca i32*, align 4
  %meshScaling = alloca %class.btVector3*, align 4
  %j = alloca i32, align 4
  %graphicsindex = alloca i32, align 4
  %graphicsbase = alloca float*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %graphicsbase29 = alloca double*, align 4
  %ref.tmp32 = alloca %class.btVector3, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp43 = alloca float, align 4
  store %struct.MyNodeOverlapCallback.21* %this, %struct.MyNodeOverlapCallback.21** %this.addr, align 4, !tbaa !2
  store i32 %nodeSubPart, i32* %nodeSubPart.addr, align 4, !tbaa !44
  store i32 %nodeTriangleIndex, i32* %nodeTriangleIndex.addr, align 4, !tbaa !44
  %this1 = load %struct.MyNodeOverlapCallback.21*, %struct.MyNodeOverlapCallback.21** %this.addr, align 4
  %0 = bitcast i8** %vertexbase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = bitcast i32* %numverts to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = bitcast i32* %type to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %3 = bitcast i32* %stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = bitcast i8** %indexbase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = bitcast i32* %indexstride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = bitcast i32* %numfaces to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %7 = bitcast i32* %indicestype to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  %m_meshInterface = getelementptr inbounds %struct.MyNodeOverlapCallback.21, %struct.MyNodeOverlapCallback.21* %this1, i32 0, i32 1
  %8 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface, align 4, !tbaa !30
  %9 = load i32, i32* %nodeSubPart.addr, align 4, !tbaa !44
  %10 = bitcast %class.btStridingMeshInterface* %8 to void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)***
  %vtable = load void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)**, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*** %10, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vtable, i64 4
  %11 = load void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vfn, align 4
  call void %11(%class.btStridingMeshInterface* %8, i8** %vertexbase, i32* nonnull align 4 dereferenceable(4) %numverts, i32* nonnull align 4 dereferenceable(4) %type, i32* nonnull align 4 dereferenceable(4) %stride, i8** %indexbase, i32* nonnull align 4 dereferenceable(4) %indexstride, i32* nonnull align 4 dereferenceable(4) %numfaces, i32* nonnull align 4 dereferenceable(4) %indicestype, i32 %9)
  %12 = bitcast i32** %gfxbase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #9
  %13 = load i8*, i8** %indexbase, align 4, !tbaa !2
  %14 = load i32, i32* %nodeTriangleIndex.addr, align 4, !tbaa !44
  %15 = load i32, i32* %indexstride, align 4, !tbaa !44
  %mul = mul nsw i32 %14, %15
  %add.ptr = getelementptr inbounds i8, i8* %13, i32 %mul
  %16 = bitcast i8* %add.ptr to i32*
  store i32* %16, i32** %gfxbase, align 4, !tbaa !2
  %17 = bitcast %class.btVector3** %meshScaling to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #9
  %m_meshInterface2 = getelementptr inbounds %struct.MyNodeOverlapCallback.21, %struct.MyNodeOverlapCallback.21* %this1, i32 0, i32 1
  %18 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface2, align 4, !tbaa !30
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btStridingMeshInterface10getScalingEv(%class.btStridingMeshInterface* %18)
  store %class.btVector3* %call, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %19 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #9
  store i32 2, i32* %j, align 4, !tbaa !44
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %20 = load i32, i32* %j, align 4, !tbaa !44
  %cmp = icmp sge i32 %20, 0
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %21 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %22 = bitcast i32* %graphicsindex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #9
  %23 = load i32, i32* %indicestype, align 4, !tbaa !48
  %cmp3 = icmp eq i32 %23, 3
  br i1 %cmp3, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %24 = load i32*, i32** %gfxbase, align 4, !tbaa !2
  %25 = bitcast i32* %24 to i16*
  %26 = load i32, i32* %j, align 4, !tbaa !44
  %arrayidx = getelementptr inbounds i16, i16* %25, i32 %26
  %27 = load i16, i16* %arrayidx, align 2, !tbaa !50
  %conv = zext i16 %27 to i32
  br label %cond.end10

cond.false:                                       ; preds = %for.body
  %28 = load i32, i32* %indicestype, align 4, !tbaa !48
  %cmp4 = icmp eq i32 %28, 2
  br i1 %cmp4, label %cond.true5, label %cond.false7

cond.true5:                                       ; preds = %cond.false
  %29 = load i32*, i32** %gfxbase, align 4, !tbaa !2
  %30 = load i32, i32* %j, align 4, !tbaa !44
  %arrayidx6 = getelementptr inbounds i32, i32* %29, i32 %30
  %31 = load i32, i32* %arrayidx6, align 4, !tbaa !44
  br label %cond.end

cond.false7:                                      ; preds = %cond.false
  %32 = load i32*, i32** %gfxbase, align 4, !tbaa !2
  %33 = bitcast i32* %32 to i8*
  %34 = load i32, i32* %j, align 4, !tbaa !44
  %arrayidx8 = getelementptr inbounds i8, i8* %33, i32 %34
  %35 = load i8, i8* %arrayidx8, align 1, !tbaa !53
  %conv9 = zext i8 %35 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false7, %cond.true5
  %cond = phi i32 [ %31, %cond.true5 ], [ %conv9, %cond.false7 ]
  br label %cond.end10

cond.end10:                                       ; preds = %cond.end, %cond.true
  %cond11 = phi i32 [ %conv, %cond.true ], [ %cond, %cond.end ]
  store i32 %cond11, i32* %graphicsindex, align 4, !tbaa !44
  %36 = load i32, i32* %type, align 4, !tbaa !48
  %cmp12 = icmp eq i32 %36, 0
  br i1 %cmp12, label %if.then, label %if.else

if.then:                                          ; preds = %cond.end10
  %37 = bitcast float** %graphicsbase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #9
  %38 = load i8*, i8** %vertexbase, align 4, !tbaa !2
  %39 = load i32, i32* %graphicsindex, align 4, !tbaa !44
  %40 = load i32, i32* %stride, align 4, !tbaa !44
  %mul13 = mul nsw i32 %39, %40
  %add.ptr14 = getelementptr inbounds i8, i8* %38, i32 %mul13
  %41 = bitcast i8* %add.ptr14 to float*
  store float* %41, float** %graphicsbase, align 4, !tbaa !2
  %42 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %42) #9
  %43 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #9
  %44 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx16 = getelementptr inbounds float, float* %44, i32 0
  %45 = load float, float* %arrayidx16, align 4, !tbaa !33
  %46 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %46)
  %47 = load float, float* %call17, align 4, !tbaa !33
  %mul18 = fmul float %45, %47
  store float %mul18, float* %ref.tmp15, align 4, !tbaa !33
  %48 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #9
  %49 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx20 = getelementptr inbounds float, float* %49, i32 1
  %50 = load float, float* %arrayidx20, align 4, !tbaa !33
  %51 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %51)
  %52 = load float, float* %call21, align 4, !tbaa !33
  %mul22 = fmul float %50, %52
  store float %mul22, float* %ref.tmp19, align 4, !tbaa !33
  %53 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #9
  %54 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds float, float* %54, i32 2
  %55 = load float, float* %arrayidx24, align 4, !tbaa !33
  %56 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %56)
  %57 = load float, float* %call25, align 4, !tbaa !33
  %mul26 = fmul float %55, %57
  store float %mul26, float* %ref.tmp23, align 4, !tbaa !33
  %call27 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp19, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  %m_triangle = getelementptr inbounds %struct.MyNodeOverlapCallback.21, %struct.MyNodeOverlapCallback.21* %this1, i32 0, i32 3
  %58 = load i32, i32* %j, align 4, !tbaa !44
  %arrayidx28 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_triangle, i32 0, i32 %58
  %59 = bitcast %class.btVector3* %arrayidx28 to i8*
  %60 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %59, i8* align 4 %60, i32 16, i1 false), !tbaa.struct !52
  %61 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #9
  %62 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #9
  %63 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #9
  %64 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %64) #9
  %65 = bitcast float** %graphicsbase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #9
  br label %if.end

if.else:                                          ; preds = %cond.end10
  %66 = bitcast double** %graphicsbase29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #9
  %67 = load i8*, i8** %vertexbase, align 4, !tbaa !2
  %68 = load i32, i32* %graphicsindex, align 4, !tbaa !44
  %69 = load i32, i32* %stride, align 4, !tbaa !44
  %mul30 = mul nsw i32 %68, %69
  %add.ptr31 = getelementptr inbounds i8, i8* %67, i32 %mul30
  %70 = bitcast i8* %add.ptr31 to double*
  store double* %70, double** %graphicsbase29, align 4, !tbaa !2
  %71 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %71) #9
  %72 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %72) #9
  %73 = load double*, double** %graphicsbase29, align 4, !tbaa !2
  %arrayidx34 = getelementptr inbounds double, double* %73, i32 0
  %74 = load double, double* %arrayidx34, align 8, !tbaa !54
  %conv35 = fptrunc double %74 to float
  %75 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %75)
  %76 = load float, float* %call36, align 4, !tbaa !33
  %mul37 = fmul float %conv35, %76
  store float %mul37, float* %ref.tmp33, align 4, !tbaa !33
  %77 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %77) #9
  %78 = load double*, double** %graphicsbase29, align 4, !tbaa !2
  %arrayidx39 = getelementptr inbounds double, double* %78, i32 1
  %79 = load double, double* %arrayidx39, align 8, !tbaa !54
  %conv40 = fptrunc double %79 to float
  %80 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %80)
  %81 = load float, float* %call41, align 4, !tbaa !33
  %mul42 = fmul float %conv40, %81
  store float %mul42, float* %ref.tmp38, align 4, !tbaa !33
  %82 = bitcast float* %ref.tmp43 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #9
  %83 = load double*, double** %graphicsbase29, align 4, !tbaa !2
  %arrayidx44 = getelementptr inbounds double, double* %83, i32 2
  %84 = load double, double* %arrayidx44, align 8, !tbaa !54
  %conv45 = fptrunc double %84 to float
  %85 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %call46 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %85)
  %86 = load float, float* %call46, align 4, !tbaa !33
  %mul47 = fmul float %conv45, %86
  store float %mul47, float* %ref.tmp43, align 4, !tbaa !33
  %call48 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp32, float* nonnull align 4 dereferenceable(4) %ref.tmp33, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp43)
  %m_triangle49 = getelementptr inbounds %struct.MyNodeOverlapCallback.21, %struct.MyNodeOverlapCallback.21* %this1, i32 0, i32 3
  %87 = load i32, i32* %j, align 4, !tbaa !44
  %arrayidx50 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_triangle49, i32 0, i32 %87
  %88 = bitcast %class.btVector3* %arrayidx50 to i8*
  %89 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %88, i8* align 4 %89, i32 16, i1 false), !tbaa.struct !52
  %90 = bitcast float* %ref.tmp43 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #9
  %91 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #9
  %92 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #9
  %93 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %93) #9
  %94 = bitcast double** %graphicsbase29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #9
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %95 = bitcast i32* %graphicsindex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #9
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %96 = load i32, i32* %j, align 4, !tbaa !44
  %dec = add nsw i32 %96, -1
  store i32 %dec, i32* %j, align 4, !tbaa !44
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %m_callback = getelementptr inbounds %struct.MyNodeOverlapCallback.21, %struct.MyNodeOverlapCallback.21* %this1, i32 0, i32 2
  %97 = load %class.btTriangleCallback*, %class.btTriangleCallback** %m_callback, align 4, !tbaa !32
  %m_triangle51 = getelementptr inbounds %struct.MyNodeOverlapCallback.21, %struct.MyNodeOverlapCallback.21* %this1, i32 0, i32 3
  %arraydecay = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_triangle51, i32 0, i32 0
  %98 = load i32, i32* %nodeSubPart.addr, align 4, !tbaa !44
  %99 = load i32, i32* %nodeTriangleIndex.addr, align 4, !tbaa !44
  %100 = bitcast %class.btTriangleCallback* %97 to void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)***
  %vtable52 = load void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)**, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*** %100, align 4, !tbaa !8
  %vfn53 = getelementptr inbounds void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)** %vtable52, i64 2
  %101 = load void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)** %vfn53, align 4
  call void %101(%class.btTriangleCallback* %97, %class.btVector3* %arraydecay, i32 %98, i32 %99)
  %m_meshInterface54 = getelementptr inbounds %struct.MyNodeOverlapCallback.21, %struct.MyNodeOverlapCallback.21* %this1, i32 0, i32 1
  %102 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface54, align 4, !tbaa !30
  %103 = load i32, i32* %nodeSubPart.addr, align 4, !tbaa !44
  %104 = bitcast %class.btStridingMeshInterface* %102 to void (%class.btStridingMeshInterface*, i32)***
  %vtable55 = load void (%class.btStridingMeshInterface*, i32)**, void (%class.btStridingMeshInterface*, i32)*** %104, align 4, !tbaa !8
  %vfn56 = getelementptr inbounds void (%class.btStridingMeshInterface*, i32)*, void (%class.btStridingMeshInterface*, i32)** %vtable55, i64 6
  %105 = load void (%class.btStridingMeshInterface*, i32)*, void (%class.btStridingMeshInterface*, i32)** %vfn56, align 4
  call void %105(%class.btStridingMeshInterface* %102, i32 %103)
  %106 = bitcast %class.btVector3** %meshScaling to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #9
  %107 = bitcast i32** %gfxbase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #9
  %108 = bitcast i32* %indicestype to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #9
  %109 = bitcast i32* %numfaces to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #9
  %110 = bitcast i32* %indexstride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #9
  %111 = bitcast i8** %indexbase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #9
  %112 = bitcast i32* %stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #9
  %113 = bitcast i32* %type to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #9
  %114 = bitcast i32* %numverts to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #9
  %115 = bitcast i8** %vertexbase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !33
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !33
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !33
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !33
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !33
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !33
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { cold noreturn nounwind }
attributes #8 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { nounwind }
attributes #10 = { builtin nounwind }
attributes #11 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"bool", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"vtable pointer", !5, i64 0}
!10 = !{!11, !3, i64 52}
!11 = !{!"_ZTS22btBvhTriangleMeshShape", !3, i64 52, !3, i64 56, !7, i64 60, !7, i64 61, !4, i64 62}
!12 = !{!11, !3, i64 56}
!13 = !{i8 0, i8 2}
!14 = !{!11, !7, i64 60}
!15 = !{!11, !7, i64 61}
!16 = !{!17, !18, i64 4}
!17 = !{!"_ZTS16btCollisionShape", !18, i64 4, !3, i64 8}
!18 = !{!"int", !4, i64 0}
!19 = !{!20, !3, i64 48}
!20 = !{!"_ZTS19btTriangleMeshShape", !21, i64 16, !21, i64 32, !3, i64 48}
!21 = !{!"_ZTS9btVector3", !4, i64 0}
!22 = !{!23, !23, i64 0}
!23 = !{!"long", !4, i64 0}
!24 = !{!25, !3, i64 4}
!25 = !{!"_ZTSZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback", !3, i64 4, !3, i64 8}
!26 = !{!25, !3, i64 8}
!27 = !{!28, !3, i64 4}
!28 = !{!"_ZTSZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback", !3, i64 4, !3, i64 8}
!29 = !{!28, !3, i64 8}
!30 = !{!31, !3, i64 4}
!31 = !{!"_ZTSZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback", !3, i64 4, !3, i64 8, !4, i64 12}
!32 = !{!31, !3, i64 8}
!33 = !{!34, !34, i64 0}
!34 = !{!"float", !4, i64 0}
!35 = !{!36, !34, i64 12}
!36 = !{!"_ZTS14btConcaveShape", !34, i64 12}
!37 = !{!38, !34, i64 52}
!38 = !{!"_ZTS23btTriangleMeshShapeData", !39, i64 0, !40, i64 12, !3, i64 40, !3, i64 44, !3, i64 48, !34, i64 52, !4, i64 56}
!39 = !{!"_ZTS20btCollisionShapeData", !3, i64 0, !18, i64 4, !4, i64 8}
!40 = !{!"_ZTS27btStridingMeshInterfaceData", !3, i64 0, !41, i64 4, !18, i64 20, !4, i64 24}
!41 = !{!"_ZTS18btVector3FloatData", !4, i64 0}
!42 = !{!38, !3, i64 40}
!43 = !{!38, !3, i64 44}
!44 = !{!18, !18, i64 0}
!45 = !{!46, !3, i64 8}
!46 = !{!"_ZTS7btChunk", !18, i64 0, !18, i64 4, !3, i64 8, !18, i64 12, !18, i64 16}
!47 = !{!38, !3, i64 48}
!48 = !{!49, !49, i64 0}
!49 = !{!"_ZTS14PHY_ScalarType", !4, i64 0}
!50 = !{!51, !51, i64 0}
!51 = !{!"short", !4, i64 0}
!52 = !{i64 0, i64 16, !53}
!53 = !{!4, !4, i64 0}
!54 = !{!55, !55, i64 0}
!55 = !{!"double", !4, i64 0}
