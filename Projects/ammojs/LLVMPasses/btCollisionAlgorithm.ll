; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/BroadphaseCollision/btCollisionAlgorithm.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/BroadphaseCollision/btCollisionAlgorithm.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btCollisionAlgorithm = type { i32 (...)**, %class.btDispatcher* }
%class.btDispatcher = type { i32 (...)** }
%struct.btCollisionAlgorithmConstructionInfo = type { %class.btDispatcher*, %class.btPersistentManifold* }
%class.btPersistentManifold = type opaque

$_ZN20btCollisionAlgorithmD2Ev = comdat any

$_ZN20btCollisionAlgorithmD0Ev = comdat any

$_ZTV20btCollisionAlgorithm = comdat any

$_ZTS20btCollisionAlgorithm = comdat any

$_ZTI20btCollisionAlgorithm = comdat any

@_ZTV20btCollisionAlgorithm = linkonce_odr hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI20btCollisionAlgorithm to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)* @_ZN20btCollisionAlgorithmD2Ev to i8*), i8* bitcast (void (%class.btCollisionAlgorithm*)* @_ZN20btCollisionAlgorithmD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS20btCollisionAlgorithm = linkonce_odr hidden constant [23 x i8] c"20btCollisionAlgorithm\00", comdat, align 1
@_ZTI20btCollisionAlgorithm = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([23 x i8], [23 x i8]* @_ZTS20btCollisionAlgorithm, i32 0, i32 0) }, comdat, align 4

; Function Attrs: nounwind
define hidden %class.btCollisionAlgorithm* @_ZN20btCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo(%class.btCollisionAlgorithm* returned %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btCollisionAlgorithm*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  store %class.btCollisionAlgorithm* %this, %class.btCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast %class.btCollisionAlgorithm* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV20btCollisionAlgorithm, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  %1 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %1, i32 0, i32 0
  %2 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4, !tbaa !8
  %m_dispatcher = getelementptr inbounds %class.btCollisionAlgorithm, %class.btCollisionAlgorithm* %this1, i32 0, i32 1
  store %class.btDispatcher* %2, %class.btDispatcher** %m_dispatcher, align 4, !tbaa !10
  ret %class.btCollisionAlgorithm* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN20btCollisionAlgorithmD2Ev(%class.btCollisionAlgorithm* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionAlgorithm*, align 4
  store %class.btCollisionAlgorithm* %this, %class.btCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %this.addr, align 4
  ret %class.btCollisionAlgorithm* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN20btCollisionAlgorithmD0Ev(%class.btCollisionAlgorithm* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionAlgorithm*, align 4
  store %class.btCollisionAlgorithm* %this, %class.btCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %this.addr, align 4
  call void @llvm.trap() #2
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #1

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { cold noreturn nounwind }
attributes #2 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{!9, !3, i64 0}
!9 = !{!"_ZTS36btCollisionAlgorithmConstructionInfo", !3, i64 0, !3, i64 4}
!10 = !{!11, !3, i64 4}
!11 = !{!"_ZTS20btCollisionAlgorithm", !3, i64 4}
