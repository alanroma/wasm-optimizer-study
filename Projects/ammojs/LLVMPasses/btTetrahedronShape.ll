; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btTetrahedronShape.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btTetrahedronShape.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btBU_Simplex1to4 = type { %class.btPolyhedralConvexAabbCachingShape.base, i32, [4 x %class.btVector3] }
%class.btPolyhedralConvexAabbCachingShape.base = type <{ %class.btPolyhedralConvexShape, %class.btVector3, %class.btVector3, i8 }>
%class.btPolyhedralConvexShape = type { %class.btConvexInternalShape, %class.btConvexPolyhedron* }
%class.btConvexInternalShape = type { %class.btConvexShape, %class.btVector3, %class.btVector3, float, float }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%class.btConvexPolyhedron = type opaque
%class.btVector3 = type { [4 x float] }
%class.btPolyhedralConvexAabbCachingShape = type <{ %class.btPolyhedralConvexShape, %class.btVector3, %class.btVector3, i8, [3 x i8] }>
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btSerializer = type opaque
%struct.btConvexInternalShapeData = type { %struct.btCollisionShapeData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, i32 }
%struct.btCollisionShapeData = type { i8*, i32, [4 x i8] }
%struct.btVector3FloatData = type { [4 x float] }

$_ZN9btVector3C2Ev = comdat any

$_ZN16btBU_Simplex1to4D0Ev = comdat any

$_ZNK21btConvexInternalShape15getLocalScalingEv = comdat any

$_ZNK16btBU_Simplex1to47getNameEv = comdat any

$_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZN21btConvexInternalShape9setMarginEf = comdat any

$_ZNK21btConvexInternalShape9getMarginEv = comdat any

$_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv = comdat any

$_ZNK21btConvexInternalShape9serializeEPvP12btSerializer = comdat any

$_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv = comdat any

$_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3 = comdat any

$_ZN16btBU_Simplex1to4dlEPv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZNK9btVector314serializeFloatER18btVector3FloatData = comdat any

@_ZTV16btBU_Simplex1to4 = hidden unnamed_addr constant { [34 x i8*] } { [34 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI16btBU_Simplex1to4 to i8*), i8* bitcast (%class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD2Ev to i8*), i8* bitcast (void (%class.btBU_Simplex1to4*)* @_ZN16btBU_Simplex1to4D0Ev to i8*), i8* bitcast (void (%class.btBU_Simplex1to4*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK16btBU_Simplex1to47getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btPolyhedralConvexAabbCachingShape*, %class.btVector3*)* @_ZN34btPolyhedralConvexAabbCachingShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btPolyhedralConvexShape*, float, %class.btVector3*)* @_ZNK23btPolyhedralConvexShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btBU_Simplex1to4*)* @_ZNK16btBU_Simplex1to47getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, float)* @_ZN21btConvexInternalShape9setMarginEf to i8*), i8* bitcast (float (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btConvexInternalShape*, i8*, %class.btSerializer*)* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btVector3*, %class.btConvexInternalShape*, %class.btVector3*)* @_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btPolyhedralConvexShape*, %class.btVector3*)* @_ZNK23btPolyhedralConvexShape37localGetSupportingVertexWithoutMarginERK9btVector3 to i8*), i8* bitcast (void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, float*, float*)* @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_ to i8*), i8* bitcast (void (%class.btPolyhedralConvexShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK23btPolyhedralConvexShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, i32, %class.btVector3*)* @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3 to i8*), i8* bitcast (i1 (%class.btPolyhedralConvexShape*, i32)* @_ZN23btPolyhedralConvexShape28initializePolyhedralFeaturesEi to i8*), i8* bitcast (i32 (%class.btBU_Simplex1to4*)* @_ZNK16btBU_Simplex1to414getNumVerticesEv to i8*), i8* bitcast (i32 (%class.btBU_Simplex1to4*)* @_ZNK16btBU_Simplex1to411getNumEdgesEv to i8*), i8* bitcast (void (%class.btBU_Simplex1to4*, i32, %class.btVector3*, %class.btVector3*)* @_ZNK16btBU_Simplex1to47getEdgeEiR9btVector3S1_ to i8*), i8* bitcast (void (%class.btBU_Simplex1to4*, i32, %class.btVector3*)* @_ZNK16btBU_Simplex1to49getVertexEiR9btVector3 to i8*), i8* bitcast (i32 (%class.btBU_Simplex1to4*)* @_ZNK16btBU_Simplex1to412getNumPlanesEv to i8*), i8* bitcast (void (%class.btBU_Simplex1to4*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK16btBU_Simplex1to48getPlaneER9btVector3S1_i to i8*), i8* bitcast (i1 (%class.btBU_Simplex1to4*, %class.btVector3*, float)* @_ZNK16btBU_Simplex1to48isInsideERK9btVector3f to i8*), i8* bitcast (i32 (%class.btBU_Simplex1to4*, i32)* @_ZNK16btBU_Simplex1to48getIndexEi to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS16btBU_Simplex1to4 = hidden constant [19 x i8] c"16btBU_Simplex1to4\00", align 1
@_ZTI34btPolyhedralConvexAabbCachingShape = external constant i8*
@_ZTI16btBU_Simplex1to4 = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([19 x i8], [19 x i8]* @_ZTS16btBU_Simplex1to4, i32 0, i32 0), i8* bitcast (i8** @_ZTI34btPolyhedralConvexAabbCachingShape to i8*) }, align 4
@.str = private unnamed_addr constant [17 x i8] c"btBU_Simplex1to4\00", align 1
@.str.1 = private unnamed_addr constant [26 x i8] c"btConvexInternalShapeData\00", align 1

@_ZN16btBU_Simplex1to4C1Ev = hidden unnamed_addr alias %class.btBU_Simplex1to4* (%class.btBU_Simplex1to4*), %class.btBU_Simplex1to4* (%class.btBU_Simplex1to4*)* @_ZN16btBU_Simplex1to4C2Ev
@_ZN16btBU_Simplex1to4C1ERK9btVector3 = hidden unnamed_addr alias %class.btBU_Simplex1to4* (%class.btBU_Simplex1to4*, %class.btVector3*), %class.btBU_Simplex1to4* (%class.btBU_Simplex1to4*, %class.btVector3*)* @_ZN16btBU_Simplex1to4C2ERK9btVector3
@_ZN16btBU_Simplex1to4C1ERK9btVector3S2_ = hidden unnamed_addr alias %class.btBU_Simplex1to4* (%class.btBU_Simplex1to4*, %class.btVector3*, %class.btVector3*), %class.btBU_Simplex1to4* (%class.btBU_Simplex1to4*, %class.btVector3*, %class.btVector3*)* @_ZN16btBU_Simplex1to4C2ERK9btVector3S2_
@_ZN16btBU_Simplex1to4C1ERK9btVector3S2_S2_ = hidden unnamed_addr alias %class.btBU_Simplex1to4* (%class.btBU_Simplex1to4*, %class.btVector3*, %class.btVector3*, %class.btVector3*), %class.btBU_Simplex1to4* (%class.btBU_Simplex1to4*, %class.btVector3*, %class.btVector3*, %class.btVector3*)* @_ZN16btBU_Simplex1to4C2ERK9btVector3S2_S2_
@_ZN16btBU_Simplex1to4C1ERK9btVector3S2_S2_S2_ = hidden unnamed_addr alias %class.btBU_Simplex1to4* (%class.btBU_Simplex1to4*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*), %class.btBU_Simplex1to4* (%class.btBU_Simplex1to4*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*)* @_ZN16btBU_Simplex1to4C2ERK9btVector3S2_S2_S2_

define hidden %class.btBU_Simplex1to4* @_ZN16btBU_Simplex1to4C2Ev(%class.btBU_Simplex1to4* returned %this) unnamed_addr #0 {
entry:
  %retval = alloca %class.btBU_Simplex1to4*, align 4
  %this.addr = alloca %class.btBU_Simplex1to4*, align 4
  store %class.btBU_Simplex1to4* %this, %class.btBU_Simplex1to4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btBU_Simplex1to4*, %class.btBU_Simplex1to4** %this.addr, align 4
  store %class.btBU_Simplex1to4* %this1, %class.btBU_Simplex1to4** %retval, align 4
  %0 = bitcast %class.btBU_Simplex1to4* %this1 to %class.btPolyhedralConvexAabbCachingShape*
  %call = call %class.btPolyhedralConvexAabbCachingShape* @_ZN34btPolyhedralConvexAabbCachingShapeC2Ev(%class.btPolyhedralConvexAabbCachingShape* %0)
  %1 = bitcast %class.btBU_Simplex1to4* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [34 x i8*] }, { [34 x i8*] }* @_ZTV16btBU_Simplex1to4, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_numVertices = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 1
  store i32 0, i32* %m_numVertices, align 4, !tbaa !8
  %m_vertices = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 2
  %array.begin = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %m_vertices, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 4
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %2 = bitcast %class.btBU_Simplex1to4* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %2, i32 0, i32 1
  store i32 2, i32* %m_shapeType, align 4, !tbaa !11
  %3 = load %class.btBU_Simplex1to4*, %class.btBU_Simplex1to4** %retval, align 4
  ret %class.btBU_Simplex1to4* %3
}

declare %class.btPolyhedralConvexAabbCachingShape* @_ZN34btPolyhedralConvexAabbCachingShapeC2Ev(%class.btPolyhedralConvexAabbCachingShape* returned) unnamed_addr #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

define hidden %class.btBU_Simplex1to4* @_ZN16btBU_Simplex1to4C2ERK9btVector3(%class.btBU_Simplex1to4* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %pt0) unnamed_addr #0 {
entry:
  %retval = alloca %class.btBU_Simplex1to4*, align 4
  %this.addr = alloca %class.btBU_Simplex1to4*, align 4
  %pt0.addr = alloca %class.btVector3*, align 4
  store %class.btBU_Simplex1to4* %this, %class.btBU_Simplex1to4** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %pt0, %class.btVector3** %pt0.addr, align 4, !tbaa !2
  %this1 = load %class.btBU_Simplex1to4*, %class.btBU_Simplex1to4** %this.addr, align 4
  store %class.btBU_Simplex1to4* %this1, %class.btBU_Simplex1to4** %retval, align 4
  %0 = bitcast %class.btBU_Simplex1to4* %this1 to %class.btPolyhedralConvexAabbCachingShape*
  %call = call %class.btPolyhedralConvexAabbCachingShape* @_ZN34btPolyhedralConvexAabbCachingShapeC2Ev(%class.btPolyhedralConvexAabbCachingShape* %0)
  %1 = bitcast %class.btBU_Simplex1to4* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [34 x i8*] }, { [34 x i8*] }* @_ZTV16btBU_Simplex1to4, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_numVertices = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 1
  store i32 0, i32* %m_numVertices, align 4, !tbaa !8
  %m_vertices = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 2
  %array.begin = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %m_vertices, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 4
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %2 = bitcast %class.btBU_Simplex1to4* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %2, i32 0, i32 1
  store i32 2, i32* %m_shapeType, align 4, !tbaa !11
  %3 = load %class.btVector3*, %class.btVector3** %pt0.addr, align 4, !tbaa !2
  call void @_ZN16btBU_Simplex1to49addVertexERK9btVector3(%class.btBU_Simplex1to4* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  %4 = load %class.btBU_Simplex1to4*, %class.btBU_Simplex1to4** %retval, align 4
  ret %class.btBU_Simplex1to4* %4
}

define hidden void @_ZN16btBU_Simplex1to49addVertexERK9btVector3(%class.btBU_Simplex1to4* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %pt) #0 {
entry:
  %this.addr = alloca %class.btBU_Simplex1to4*, align 4
  %pt.addr = alloca %class.btVector3*, align 4
  store %class.btBU_Simplex1to4* %this, %class.btBU_Simplex1to4** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %pt, %class.btVector3** %pt.addr, align 4, !tbaa !2
  %this1 = load %class.btBU_Simplex1to4*, %class.btBU_Simplex1to4** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %pt.addr, align 4, !tbaa !2
  %m_vertices = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 2
  %m_numVertices = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 1
  %1 = load i32, i32* %m_numVertices, align 4, !tbaa !8
  %inc = add nsw i32 %1, 1
  store i32 %inc, i32* %m_numVertices, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %m_vertices, i32 0, i32 %1
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  %3 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !13
  %4 = bitcast %class.btBU_Simplex1to4* %this1 to %class.btPolyhedralConvexAabbCachingShape*
  call void @_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv(%class.btPolyhedralConvexAabbCachingShape* %4)
  ret void
}

define hidden %class.btBU_Simplex1to4* @_ZN16btBU_Simplex1to4C2ERK9btVector3S2_(%class.btBU_Simplex1to4* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %pt0, %class.btVector3* nonnull align 4 dereferenceable(16) %pt1) unnamed_addr #0 {
entry:
  %retval = alloca %class.btBU_Simplex1to4*, align 4
  %this.addr = alloca %class.btBU_Simplex1to4*, align 4
  %pt0.addr = alloca %class.btVector3*, align 4
  %pt1.addr = alloca %class.btVector3*, align 4
  store %class.btBU_Simplex1to4* %this, %class.btBU_Simplex1to4** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %pt0, %class.btVector3** %pt0.addr, align 4, !tbaa !2
  store %class.btVector3* %pt1, %class.btVector3** %pt1.addr, align 4, !tbaa !2
  %this1 = load %class.btBU_Simplex1to4*, %class.btBU_Simplex1to4** %this.addr, align 4
  store %class.btBU_Simplex1to4* %this1, %class.btBU_Simplex1to4** %retval, align 4
  %0 = bitcast %class.btBU_Simplex1to4* %this1 to %class.btPolyhedralConvexAabbCachingShape*
  %call = call %class.btPolyhedralConvexAabbCachingShape* @_ZN34btPolyhedralConvexAabbCachingShapeC2Ev(%class.btPolyhedralConvexAabbCachingShape* %0)
  %1 = bitcast %class.btBU_Simplex1to4* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [34 x i8*] }, { [34 x i8*] }* @_ZTV16btBU_Simplex1to4, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_numVertices = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 1
  store i32 0, i32* %m_numVertices, align 4, !tbaa !8
  %m_vertices = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 2
  %array.begin = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %m_vertices, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 4
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %2 = bitcast %class.btBU_Simplex1to4* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %2, i32 0, i32 1
  store i32 2, i32* %m_shapeType, align 4, !tbaa !11
  %3 = load %class.btVector3*, %class.btVector3** %pt0.addr, align 4, !tbaa !2
  call void @_ZN16btBU_Simplex1to49addVertexERK9btVector3(%class.btBU_Simplex1to4* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  %4 = load %class.btVector3*, %class.btVector3** %pt1.addr, align 4, !tbaa !2
  call void @_ZN16btBU_Simplex1to49addVertexERK9btVector3(%class.btBU_Simplex1to4* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %4)
  %5 = load %class.btBU_Simplex1to4*, %class.btBU_Simplex1to4** %retval, align 4
  ret %class.btBU_Simplex1to4* %5
}

define hidden %class.btBU_Simplex1to4* @_ZN16btBU_Simplex1to4C2ERK9btVector3S2_S2_(%class.btBU_Simplex1to4* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %pt0, %class.btVector3* nonnull align 4 dereferenceable(16) %pt1, %class.btVector3* nonnull align 4 dereferenceable(16) %pt2) unnamed_addr #0 {
entry:
  %retval = alloca %class.btBU_Simplex1to4*, align 4
  %this.addr = alloca %class.btBU_Simplex1to4*, align 4
  %pt0.addr = alloca %class.btVector3*, align 4
  %pt1.addr = alloca %class.btVector3*, align 4
  %pt2.addr = alloca %class.btVector3*, align 4
  store %class.btBU_Simplex1to4* %this, %class.btBU_Simplex1to4** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %pt0, %class.btVector3** %pt0.addr, align 4, !tbaa !2
  store %class.btVector3* %pt1, %class.btVector3** %pt1.addr, align 4, !tbaa !2
  store %class.btVector3* %pt2, %class.btVector3** %pt2.addr, align 4, !tbaa !2
  %this1 = load %class.btBU_Simplex1to4*, %class.btBU_Simplex1to4** %this.addr, align 4
  store %class.btBU_Simplex1to4* %this1, %class.btBU_Simplex1to4** %retval, align 4
  %0 = bitcast %class.btBU_Simplex1to4* %this1 to %class.btPolyhedralConvexAabbCachingShape*
  %call = call %class.btPolyhedralConvexAabbCachingShape* @_ZN34btPolyhedralConvexAabbCachingShapeC2Ev(%class.btPolyhedralConvexAabbCachingShape* %0)
  %1 = bitcast %class.btBU_Simplex1to4* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [34 x i8*] }, { [34 x i8*] }* @_ZTV16btBU_Simplex1to4, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_numVertices = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 1
  store i32 0, i32* %m_numVertices, align 4, !tbaa !8
  %m_vertices = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 2
  %array.begin = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %m_vertices, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 4
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %2 = bitcast %class.btBU_Simplex1to4* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %2, i32 0, i32 1
  store i32 2, i32* %m_shapeType, align 4, !tbaa !11
  %3 = load %class.btVector3*, %class.btVector3** %pt0.addr, align 4, !tbaa !2
  call void @_ZN16btBU_Simplex1to49addVertexERK9btVector3(%class.btBU_Simplex1to4* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  %4 = load %class.btVector3*, %class.btVector3** %pt1.addr, align 4, !tbaa !2
  call void @_ZN16btBU_Simplex1to49addVertexERK9btVector3(%class.btBU_Simplex1to4* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %4)
  %5 = load %class.btVector3*, %class.btVector3** %pt2.addr, align 4, !tbaa !2
  call void @_ZN16btBU_Simplex1to49addVertexERK9btVector3(%class.btBU_Simplex1to4* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  %6 = load %class.btBU_Simplex1to4*, %class.btBU_Simplex1to4** %retval, align 4
  ret %class.btBU_Simplex1to4* %6
}

define hidden %class.btBU_Simplex1to4* @_ZN16btBU_Simplex1to4C2ERK9btVector3S2_S2_S2_(%class.btBU_Simplex1to4* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %pt0, %class.btVector3* nonnull align 4 dereferenceable(16) %pt1, %class.btVector3* nonnull align 4 dereferenceable(16) %pt2, %class.btVector3* nonnull align 4 dereferenceable(16) %pt3) unnamed_addr #0 {
entry:
  %retval = alloca %class.btBU_Simplex1to4*, align 4
  %this.addr = alloca %class.btBU_Simplex1to4*, align 4
  %pt0.addr = alloca %class.btVector3*, align 4
  %pt1.addr = alloca %class.btVector3*, align 4
  %pt2.addr = alloca %class.btVector3*, align 4
  %pt3.addr = alloca %class.btVector3*, align 4
  store %class.btBU_Simplex1to4* %this, %class.btBU_Simplex1to4** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %pt0, %class.btVector3** %pt0.addr, align 4, !tbaa !2
  store %class.btVector3* %pt1, %class.btVector3** %pt1.addr, align 4, !tbaa !2
  store %class.btVector3* %pt2, %class.btVector3** %pt2.addr, align 4, !tbaa !2
  store %class.btVector3* %pt3, %class.btVector3** %pt3.addr, align 4, !tbaa !2
  %this1 = load %class.btBU_Simplex1to4*, %class.btBU_Simplex1to4** %this.addr, align 4
  store %class.btBU_Simplex1to4* %this1, %class.btBU_Simplex1to4** %retval, align 4
  %0 = bitcast %class.btBU_Simplex1to4* %this1 to %class.btPolyhedralConvexAabbCachingShape*
  %call = call %class.btPolyhedralConvexAabbCachingShape* @_ZN34btPolyhedralConvexAabbCachingShapeC2Ev(%class.btPolyhedralConvexAabbCachingShape* %0)
  %1 = bitcast %class.btBU_Simplex1to4* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [34 x i8*] }, { [34 x i8*] }* @_ZTV16btBU_Simplex1to4, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_numVertices = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 1
  store i32 0, i32* %m_numVertices, align 4, !tbaa !8
  %m_vertices = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 2
  %array.begin = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %m_vertices, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 4
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %2 = bitcast %class.btBU_Simplex1to4* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %2, i32 0, i32 1
  store i32 2, i32* %m_shapeType, align 4, !tbaa !11
  %3 = load %class.btVector3*, %class.btVector3** %pt0.addr, align 4, !tbaa !2
  call void @_ZN16btBU_Simplex1to49addVertexERK9btVector3(%class.btBU_Simplex1to4* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  %4 = load %class.btVector3*, %class.btVector3** %pt1.addr, align 4, !tbaa !2
  call void @_ZN16btBU_Simplex1to49addVertexERK9btVector3(%class.btBU_Simplex1to4* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %4)
  %5 = load %class.btVector3*, %class.btVector3** %pt2.addr, align 4, !tbaa !2
  call void @_ZN16btBU_Simplex1to49addVertexERK9btVector3(%class.btBU_Simplex1to4* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  %6 = load %class.btVector3*, %class.btVector3** %pt3.addr, align 4, !tbaa !2
  call void @_ZN16btBU_Simplex1to49addVertexERK9btVector3(%class.btBU_Simplex1to4* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %6)
  %7 = load %class.btBU_Simplex1to4*, %class.btBU_Simplex1to4** %retval, align 4
  ret %class.btBU_Simplex1to4* %7
}

define hidden void @_ZNK16btBU_Simplex1to47getAabbERK11btTransformR9btVector3S4_(%class.btBU_Simplex1to4* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btBU_Simplex1to4*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btBU_Simplex1to4* %this, %class.btBU_Simplex1to4** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btBU_Simplex1to4*, %class.btBU_Simplex1to4** %this.addr, align 4
  %0 = bitcast %class.btBU_Simplex1to4* %this1 to %class.btPolyhedralConvexAabbCachingShape*
  %1 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %3 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  call void @_ZNK34btPolyhedralConvexAabbCachingShape7getAabbERK11btTransformR9btVector3S4_(%class.btPolyhedralConvexAabbCachingShape* %0, %class.btTransform* nonnull align 4 dereferenceable(64) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  ret void
}

declare void @_ZNK34btPolyhedralConvexAabbCachingShape7getAabbERK11btTransformR9btVector3S4_(%class.btPolyhedralConvexAabbCachingShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

declare void @_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv(%class.btPolyhedralConvexAabbCachingShape*) #1

; Function Attrs: nounwind
define hidden i32 @_ZNK16btBU_Simplex1to414getNumVerticesEv(%class.btBU_Simplex1to4* %this) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btBU_Simplex1to4*, align 4
  store %class.btBU_Simplex1to4* %this, %class.btBU_Simplex1to4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btBU_Simplex1to4*, %class.btBU_Simplex1to4** %this.addr, align 4
  %m_numVertices = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_numVertices, align 4, !tbaa !8
  ret i32 %0
}

; Function Attrs: nounwind
define hidden i32 @_ZNK16btBU_Simplex1to411getNumEdgesEv(%class.btBU_Simplex1to4* %this) unnamed_addr #4 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btBU_Simplex1to4*, align 4
  store %class.btBU_Simplex1to4* %this, %class.btBU_Simplex1to4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btBU_Simplex1to4*, %class.btBU_Simplex1to4** %this.addr, align 4
  %m_numVertices = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_numVertices, align 4, !tbaa !8
  switch i32 %0, label %sw.epilog [
    i32 0, label %sw.bb
    i32 1, label %sw.bb2
    i32 2, label %sw.bb3
    i32 3, label %sw.bb4
    i32 4, label %sw.bb5
  ]

sw.bb:                                            ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

sw.bb2:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

sw.bb3:                                           ; preds = %entry
  store i32 1, i32* %retval, align 4
  br label %return

sw.bb4:                                           ; preds = %entry
  store i32 3, i32* %retval, align 4
  br label %return

sw.bb5:                                           ; preds = %entry
  store i32 6, i32* %retval, align 4
  br label %return

sw.epilog:                                        ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %sw.epilog, %sw.bb5, %sw.bb4, %sw.bb3, %sw.bb2, %sw.bb
  %1 = load i32, i32* %retval, align 4
  ret i32 %1
}

; Function Attrs: nounwind
define hidden void @_ZNK16btBU_Simplex1to47getEdgeEiR9btVector3S1_(%class.btBU_Simplex1to4* %this, i32 %i, %class.btVector3* nonnull align 4 dereferenceable(16) %pa, %class.btVector3* nonnull align 4 dereferenceable(16) %pb) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btBU_Simplex1to4*, align 4
  %i.addr = alloca i32, align 4
  %pa.addr = alloca %class.btVector3*, align 4
  %pb.addr = alloca %class.btVector3*, align 4
  store %class.btBU_Simplex1to4* %this, %class.btBU_Simplex1to4** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !15
  store %class.btVector3* %pa, %class.btVector3** %pa.addr, align 4, !tbaa !2
  store %class.btVector3* %pb, %class.btVector3** %pb.addr, align 4, !tbaa !2
  %this1 = load %class.btBU_Simplex1to4*, %class.btBU_Simplex1to4** %this.addr, align 4
  %m_numVertices = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_numVertices, align 4, !tbaa !8
  switch i32 %0, label %sw.epilog52 [
    i32 2, label %sw.bb
    i32 3, label %sw.bb4
    i32 4, label %sw.bb20
  ]

sw.bb:                                            ; preds = %entry
  %m_vertices = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 2
  %arrayidx = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %m_vertices, i32 0, i32 0
  %1 = load %class.btVector3*, %class.btVector3** %pa.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %1 to i8*
  %3 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !13
  %m_vertices2 = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 2
  %arrayidx3 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %m_vertices2, i32 0, i32 1
  %4 = load %class.btVector3*, %class.btVector3** %pb.addr, align 4, !tbaa !2
  %5 = bitcast %class.btVector3* %4 to i8*
  %6 = bitcast %class.btVector3* %arrayidx3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false), !tbaa.struct !13
  br label %sw.epilog52

sw.bb4:                                           ; preds = %entry
  %7 = load i32, i32* %i.addr, align 4, !tbaa !15
  switch i32 %7, label %sw.epilog [
    i32 0, label %sw.bb5
    i32 1, label %sw.bb10
    i32 2, label %sw.bb15
  ]

sw.bb5:                                           ; preds = %sw.bb4
  %m_vertices6 = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 2
  %arrayidx7 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %m_vertices6, i32 0, i32 0
  %8 = load %class.btVector3*, %class.btVector3** %pa.addr, align 4, !tbaa !2
  %9 = bitcast %class.btVector3* %8 to i8*
  %10 = bitcast %class.btVector3* %arrayidx7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false), !tbaa.struct !13
  %m_vertices8 = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 2
  %arrayidx9 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %m_vertices8, i32 0, i32 1
  %11 = load %class.btVector3*, %class.btVector3** %pb.addr, align 4, !tbaa !2
  %12 = bitcast %class.btVector3* %11 to i8*
  %13 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false), !tbaa.struct !13
  br label %sw.epilog

sw.bb10:                                          ; preds = %sw.bb4
  %m_vertices11 = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 2
  %arrayidx12 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %m_vertices11, i32 0, i32 1
  %14 = load %class.btVector3*, %class.btVector3** %pa.addr, align 4, !tbaa !2
  %15 = bitcast %class.btVector3* %14 to i8*
  %16 = bitcast %class.btVector3* %arrayidx12 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 16, i1 false), !tbaa.struct !13
  %m_vertices13 = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 2
  %arrayidx14 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %m_vertices13, i32 0, i32 2
  %17 = load %class.btVector3*, %class.btVector3** %pb.addr, align 4, !tbaa !2
  %18 = bitcast %class.btVector3* %17 to i8*
  %19 = bitcast %class.btVector3* %arrayidx14 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %18, i8* align 4 %19, i32 16, i1 false), !tbaa.struct !13
  br label %sw.epilog

sw.bb15:                                          ; preds = %sw.bb4
  %m_vertices16 = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 2
  %arrayidx17 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %m_vertices16, i32 0, i32 2
  %20 = load %class.btVector3*, %class.btVector3** %pa.addr, align 4, !tbaa !2
  %21 = bitcast %class.btVector3* %20 to i8*
  %22 = bitcast %class.btVector3* %arrayidx17 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false), !tbaa.struct !13
  %m_vertices18 = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 2
  %arrayidx19 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %m_vertices18, i32 0, i32 0
  %23 = load %class.btVector3*, %class.btVector3** %pb.addr, align 4, !tbaa !2
  %24 = bitcast %class.btVector3* %23 to i8*
  %25 = bitcast %class.btVector3* %arrayidx19 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %24, i8* align 4 %25, i32 16, i1 false), !tbaa.struct !13
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.bb4, %sw.bb15, %sw.bb10, %sw.bb5
  br label %sw.epilog52

sw.bb20:                                          ; preds = %entry
  %26 = load i32, i32* %i.addr, align 4, !tbaa !15
  switch i32 %26, label %sw.epilog51 [
    i32 0, label %sw.bb21
    i32 1, label %sw.bb26
    i32 2, label %sw.bb31
    i32 3, label %sw.bb36
    i32 4, label %sw.bb41
    i32 5, label %sw.bb46
  ]

sw.bb21:                                          ; preds = %sw.bb20
  %m_vertices22 = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 2
  %arrayidx23 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %m_vertices22, i32 0, i32 0
  %27 = load %class.btVector3*, %class.btVector3** %pa.addr, align 4, !tbaa !2
  %28 = bitcast %class.btVector3* %27 to i8*
  %29 = bitcast %class.btVector3* %arrayidx23 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %29, i32 16, i1 false), !tbaa.struct !13
  %m_vertices24 = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 2
  %arrayidx25 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %m_vertices24, i32 0, i32 1
  %30 = load %class.btVector3*, %class.btVector3** %pb.addr, align 4, !tbaa !2
  %31 = bitcast %class.btVector3* %30 to i8*
  %32 = bitcast %class.btVector3* %arrayidx25 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %31, i8* align 4 %32, i32 16, i1 false), !tbaa.struct !13
  br label %sw.epilog51

sw.bb26:                                          ; preds = %sw.bb20
  %m_vertices27 = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 2
  %arrayidx28 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %m_vertices27, i32 0, i32 1
  %33 = load %class.btVector3*, %class.btVector3** %pa.addr, align 4, !tbaa !2
  %34 = bitcast %class.btVector3* %33 to i8*
  %35 = bitcast %class.btVector3* %arrayidx28 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %34, i8* align 4 %35, i32 16, i1 false), !tbaa.struct !13
  %m_vertices29 = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 2
  %arrayidx30 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %m_vertices29, i32 0, i32 2
  %36 = load %class.btVector3*, %class.btVector3** %pb.addr, align 4, !tbaa !2
  %37 = bitcast %class.btVector3* %36 to i8*
  %38 = bitcast %class.btVector3* %arrayidx30 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %37, i8* align 4 %38, i32 16, i1 false), !tbaa.struct !13
  br label %sw.epilog51

sw.bb31:                                          ; preds = %sw.bb20
  %m_vertices32 = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 2
  %arrayidx33 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %m_vertices32, i32 0, i32 2
  %39 = load %class.btVector3*, %class.btVector3** %pa.addr, align 4, !tbaa !2
  %40 = bitcast %class.btVector3* %39 to i8*
  %41 = bitcast %class.btVector3* %arrayidx33 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %40, i8* align 4 %41, i32 16, i1 false), !tbaa.struct !13
  %m_vertices34 = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 2
  %arrayidx35 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %m_vertices34, i32 0, i32 0
  %42 = load %class.btVector3*, %class.btVector3** %pb.addr, align 4, !tbaa !2
  %43 = bitcast %class.btVector3* %42 to i8*
  %44 = bitcast %class.btVector3* %arrayidx35 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %43, i8* align 4 %44, i32 16, i1 false), !tbaa.struct !13
  br label %sw.epilog51

sw.bb36:                                          ; preds = %sw.bb20
  %m_vertices37 = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 2
  %arrayidx38 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %m_vertices37, i32 0, i32 0
  %45 = load %class.btVector3*, %class.btVector3** %pa.addr, align 4, !tbaa !2
  %46 = bitcast %class.btVector3* %45 to i8*
  %47 = bitcast %class.btVector3* %arrayidx38 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %46, i8* align 4 %47, i32 16, i1 false), !tbaa.struct !13
  %m_vertices39 = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 2
  %arrayidx40 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %m_vertices39, i32 0, i32 3
  %48 = load %class.btVector3*, %class.btVector3** %pb.addr, align 4, !tbaa !2
  %49 = bitcast %class.btVector3* %48 to i8*
  %50 = bitcast %class.btVector3* %arrayidx40 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %49, i8* align 4 %50, i32 16, i1 false), !tbaa.struct !13
  br label %sw.epilog51

sw.bb41:                                          ; preds = %sw.bb20
  %m_vertices42 = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 2
  %arrayidx43 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %m_vertices42, i32 0, i32 1
  %51 = load %class.btVector3*, %class.btVector3** %pa.addr, align 4, !tbaa !2
  %52 = bitcast %class.btVector3* %51 to i8*
  %53 = bitcast %class.btVector3* %arrayidx43 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %52, i8* align 4 %53, i32 16, i1 false), !tbaa.struct !13
  %m_vertices44 = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 2
  %arrayidx45 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %m_vertices44, i32 0, i32 3
  %54 = load %class.btVector3*, %class.btVector3** %pb.addr, align 4, !tbaa !2
  %55 = bitcast %class.btVector3* %54 to i8*
  %56 = bitcast %class.btVector3* %arrayidx45 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %55, i8* align 4 %56, i32 16, i1 false), !tbaa.struct !13
  br label %sw.epilog51

sw.bb46:                                          ; preds = %sw.bb20
  %m_vertices47 = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 2
  %arrayidx48 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %m_vertices47, i32 0, i32 2
  %57 = load %class.btVector3*, %class.btVector3** %pa.addr, align 4, !tbaa !2
  %58 = bitcast %class.btVector3* %57 to i8*
  %59 = bitcast %class.btVector3* %arrayidx48 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %58, i8* align 4 %59, i32 16, i1 false), !tbaa.struct !13
  %m_vertices49 = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 2
  %arrayidx50 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %m_vertices49, i32 0, i32 3
  %60 = load %class.btVector3*, %class.btVector3** %pb.addr, align 4, !tbaa !2
  %61 = bitcast %class.btVector3* %60 to i8*
  %62 = bitcast %class.btVector3* %arrayidx50 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %61, i8* align 4 %62, i32 16, i1 false), !tbaa.struct !13
  br label %sw.epilog51

sw.epilog51:                                      ; preds = %sw.bb20, %sw.bb46, %sw.bb41, %sw.bb36, %sw.bb31, %sw.bb26, %sw.bb21
  br label %sw.epilog52

sw.epilog52:                                      ; preds = %sw.epilog51, %entry, %sw.epilog, %sw.bb
  ret void
}

; Function Attrs: nounwind
define hidden void @_ZNK16btBU_Simplex1to49getVertexEiR9btVector3(%class.btBU_Simplex1to4* %this, i32 %i, %class.btVector3* nonnull align 4 dereferenceable(16) %vtx) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btBU_Simplex1to4*, align 4
  %i.addr = alloca i32, align 4
  %vtx.addr = alloca %class.btVector3*, align 4
  store %class.btBU_Simplex1to4* %this, %class.btBU_Simplex1to4** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !15
  store %class.btVector3* %vtx, %class.btVector3** %vtx.addr, align 4, !tbaa !2
  %this1 = load %class.btBU_Simplex1to4*, %class.btBU_Simplex1to4** %this.addr, align 4
  %m_vertices = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 2
  %0 = load i32, i32* %i.addr, align 4, !tbaa !15
  %arrayidx = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %m_vertices, i32 0, i32 %0
  %1 = load %class.btVector3*, %class.btVector3** %vtx.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %1 to i8*
  %3 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !13
  ret void
}

; Function Attrs: nounwind
define hidden i32 @_ZNK16btBU_Simplex1to412getNumPlanesEv(%class.btBU_Simplex1to4* %this) unnamed_addr #4 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btBU_Simplex1to4*, align 4
  store %class.btBU_Simplex1to4* %this, %class.btBU_Simplex1to4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btBU_Simplex1to4*, %class.btBU_Simplex1to4** %this.addr, align 4
  %m_numVertices = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_numVertices, align 4, !tbaa !8
  switch i32 %0, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb2
    i32 2, label %sw.bb3
    i32 3, label %sw.bb4
    i32 4, label %sw.bb5
  ]

sw.bb:                                            ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

sw.bb2:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

sw.bb3:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

sw.bb4:                                           ; preds = %entry
  store i32 2, i32* %retval, align 4
  br label %return

sw.bb5:                                           ; preds = %entry
  store i32 4, i32* %retval, align 4
  br label %return

sw.default:                                       ; preds = %entry
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %sw.epilog, %sw.bb5, %sw.bb4, %sw.bb3, %sw.bb2, %sw.bb
  %1 = load i32, i32* %retval, align 4
  ret i32 %1
}

; Function Attrs: nounwind
define hidden void @_ZNK16btBU_Simplex1to48getPlaneER9btVector3S1_i(%class.btBU_Simplex1to4* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, i32 %2) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btBU_Simplex1to4*, align 4
  %.addr = alloca %class.btVector3*, align 4
  %.addr1 = alloca %class.btVector3*, align 4
  %.addr2 = alloca i32, align 4
  store %class.btBU_Simplex1to4* %this, %class.btBU_Simplex1to4** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %0, %class.btVector3** %.addr, align 4, !tbaa !2
  store %class.btVector3* %1, %class.btVector3** %.addr1, align 4, !tbaa !2
  store i32 %2, i32* %.addr2, align 4, !tbaa !15
  %this3 = load %class.btBU_Simplex1to4*, %class.btBU_Simplex1to4** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define hidden i32 @_ZNK16btBU_Simplex1to48getIndexEi(%class.btBU_Simplex1to4* %this, i32 %0) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btBU_Simplex1to4*, align 4
  %.addr = alloca i32, align 4
  store %class.btBU_Simplex1to4* %this, %class.btBU_Simplex1to4** %this.addr, align 4, !tbaa !2
  store i32 %0, i32* %.addr, align 4, !tbaa !15
  %this1 = load %class.btBU_Simplex1to4*, %class.btBU_Simplex1to4** %this.addr, align 4
  ret i32 0
}

; Function Attrs: nounwind
define hidden zeroext i1 @_ZNK16btBU_Simplex1to48isInsideERK9btVector3f(%class.btBU_Simplex1to4* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float %1) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btBU_Simplex1to4*, align 4
  %.addr = alloca %class.btVector3*, align 4
  %.addr1 = alloca float, align 4
  store %class.btBU_Simplex1to4* %this, %class.btBU_Simplex1to4** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %0, %class.btVector3** %.addr, align 4, !tbaa !2
  store float %1, float* %.addr1, align 4, !tbaa !16
  %this2 = load %class.btBU_Simplex1to4*, %class.btBU_Simplex1to4** %this.addr, align 4
  ret i1 false
}

; Function Attrs: nounwind
declare %class.btPolyhedralConvexShape* @_ZN23btPolyhedralConvexShapeD2Ev(%class.btPolyhedralConvexShape* returned) unnamed_addr #5

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN16btBU_Simplex1to4D0Ev(%class.btBU_Simplex1to4* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btBU_Simplex1to4*, align 4
  store %class.btBU_Simplex1to4* %this, %class.btBU_Simplex1to4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btBU_Simplex1to4*, %class.btBU_Simplex1to4** %this.addr, align 4
  %call = call %class.btBU_Simplex1to4* bitcast (%class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD2Ev to %class.btBU_Simplex1to4* (%class.btBU_Simplex1to4*)*)(%class.btBU_Simplex1to4* %this1) #7
  %0 = bitcast %class.btBU_Simplex1to4* %this1 to i8*
  call void @_ZN16btBU_Simplex1to4dlEPv(i8* %0) #7
  ret void
}

declare void @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf(%class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4)) unnamed_addr #1

declare float @_ZNK16btCollisionShape20getAngularMotionDiscEv(%class.btCollisionShape*) unnamed_addr #1

declare float @_ZNK16btCollisionShape27getContactBreakingThresholdEf(%class.btCollisionShape*, float) unnamed_addr #1

declare void @_ZN34btPolyhedralConvexAabbCachingShape15setLocalScalingERK9btVector3(%class.btPolyhedralConvexAabbCachingShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btConvexInternalShape15getLocalScalingEv(%class.btConvexInternalShape* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 1
  ret %class.btVector3* %m_localScaling
}

declare void @_ZNK23btPolyhedralConvexShape21calculateLocalInertiaEfR9btVector3(%class.btPolyhedralConvexShape*, float, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden i8* @_ZNK16btBU_Simplex1to47getNameEv(%class.btBU_Simplex1to4* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btBU_Simplex1to4*, align 4
  store %class.btBU_Simplex1to4* %this, %class.btBU_Simplex1to4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btBU_Simplex1to4*, %class.btBU_Simplex1to4** %this.addr, align 4
  ret i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str, i32 0, i32 0)
}

define linkonce_odr hidden void @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btCollisionShape* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !16
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  store float 1.000000e+00, float* %ref.tmp2, align 4, !tbaa !16
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  store float 1.000000e+00, float* %ref.tmp3, align 4, !tbaa !16
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #7
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #7
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #7
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN21btConvexInternalShape9setMarginEf(%class.btConvexInternalShape* %this, float %margin) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %margin.addr = alloca float, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  store float %margin, float* %margin.addr, align 4, !tbaa !16
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load float, float* %margin.addr, align 4, !tbaa !16
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  store float %0, float* %m_collisionMargin, align 4, !tbaa !18
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK21btConvexInternalShape9getMarginEv(%class.btConvexInternalShape* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  %0 = load float, float* %m_collisionMargin, align 4, !tbaa !18
  ret float %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv(%class.btConvexInternalShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  ret i32 52
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer(%class.btConvexInternalShape* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %shapeData = alloca %struct.btConvexInternalShapeData*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = bitcast %struct.btConvexInternalShapeData** %shapeData to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i8*, i8** %dataBuffer.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.btConvexInternalShapeData*
  store %struct.btConvexInternalShapeData* %2, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %3 = bitcast %class.btConvexInternalShape* %this1 to %class.btCollisionShape*
  %4 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_collisionShapeData = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %4, i32 0, i32 0
  %5 = bitcast %struct.btCollisionShapeData* %m_collisionShapeData to i8*
  %6 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %call = call i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape* %3, i8* %5, %class.btSerializer* %6)
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 2
  %7 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_implicitShapeDimensions2 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %7, i32 0, i32 2
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_implicitShapeDimensions, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_implicitShapeDimensions2)
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 1
  %8 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_localScaling3 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %8, i32 0, i32 1
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_localScaling, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_localScaling3)
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  %9 = load float, float* %m_collisionMargin, align 4, !tbaa !18
  %10 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_collisionMargin4 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %10, i32 0, i32 3
  store float %9, float* %m_collisionMargin4, align 4, !tbaa !21
  %11 = bitcast %struct.btConvexInternalShapeData** %shapeData to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #7
  ret i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.1, i32 0, i32 0)
}

declare void @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer(%class.btCollisionShape*, %class.btSerializer*) unnamed_addr #1

declare void @_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3(%class.btVector3* sret align 4, %class.btConvexInternalShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

declare void @_ZNK23btPolyhedralConvexShape37localGetSupportingVertexWithoutMarginERK9btVector3(%class.btVector3* sret align 4, %class.btPolyhedralConvexShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

declare void @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_(%class.btConvexShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4), float* nonnull align 4 dereferenceable(4)) unnamed_addr #1

declare void @_ZNK23btPolyhedralConvexShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i(%class.btPolyhedralConvexShape*, %class.btVector3*, %class.btVector3*, i32) unnamed_addr #1

declare void @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_(%class.btConvexInternalShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv(%class.btConvexInternalShape* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  ret i32 0
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3(%class.btConvexInternalShape* %this, i32 %index, %class.btVector3* nonnull align 4 dereferenceable(16) %penetrationVector) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %index.addr = alloca i32, align 4
  %penetrationVector.addr = alloca %class.btVector3*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !15
  store %class.btVector3* %penetrationVector, %class.btVector3** %penetrationVector.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %penetrationVector.addr, align 4, !tbaa !2
  ret void
}

declare zeroext i1 @_ZN23btPolyhedralConvexShape28initializePolyhedralFeaturesEi(%class.btPolyhedralConvexShape*, i32) unnamed_addr #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN16btBU_Simplex1to4dlEPv(i8* %ptr) #2 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !16
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !16
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !16
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !16
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !16
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !16
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !16
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #3

declare i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape*, i8*, %class.btSerializer*) unnamed_addr #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  store i32 0, i32* %i, align 4, !tbaa !15
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !15
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %3 = load i32, i32* %i, align 4, !tbaa !15
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %3
  %4 = load float, float* %arrayidx, align 4, !tbaa !16
  %5 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %5, i32 0, i32 0
  %6 = load i32, i32* %i, align 4, !tbaa !15
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %6
  store float %4, float* %arrayidx3, align 4, !tbaa !16
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !15
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !15
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{!9, !10, i64 92}
!9 = !{!"_ZTS16btBU_Simplex1to4", !10, i64 92, !4, i64 96}
!10 = !{!"int", !4, i64 0}
!11 = !{!12, !10, i64 4}
!12 = !{!"_ZTS16btCollisionShape", !10, i64 4, !3, i64 8}
!13 = !{i64 0, i64 16, !14}
!14 = !{!4, !4, i64 0}
!15 = !{!10, !10, i64 0}
!16 = !{!17, !17, i64 0}
!17 = !{!"float", !4, i64 0}
!18 = !{!19, !17, i64 44}
!19 = !{!"_ZTS21btConvexInternalShape", !20, i64 12, !20, i64 28, !17, i64 44, !17, i64 48}
!20 = !{!"_ZTS9btVector3", !4, i64 0}
!21 = !{!22, !17, i64 44}
!22 = !{!"_ZTS25btConvexInternalShapeData", !23, i64 0, !24, i64 12, !24, i64 28, !17, i64 44, !10, i64 48}
!23 = !{!"_ZTS20btCollisionShapeData", !3, i64 0, !10, i64 4, !4, i64 8}
!24 = !{!"_ZTS18btVector3FloatData", !4, i64 0}
