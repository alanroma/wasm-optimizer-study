; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btCompoundShape.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btCompoundShape.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btCompoundShape = type { %class.btCollisionShape, %class.btAlignedObjectArray, %class.btVector3, %class.btVector3, %struct.btDbvt*, i32, float, %class.btVector3 }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btCompoundShapeChild*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btCompoundShapeChild = type { %class.btTransform, %class.btCollisionShape*, i32, float, %struct.btDbvtNode* }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btDbvtNode = type { %struct.btDbvtAabbMm, %struct.btDbvtNode*, %union.anon }
%struct.btDbvtAabbMm = type { %class.btVector3, %class.btVector3 }
%union.anon = type { [2 x %struct.btDbvtNode*] }
%struct.btDbvt = type { %struct.btDbvtNode*, %struct.btDbvtNode*, i32, i32, i32, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.4 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %"struct.btDbvt::sStkNN"*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%"struct.btDbvt::sStkNN" = type { %struct.btDbvtNode*, %struct.btDbvtNode* }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %struct.btDbvtNode**, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btVector3 = type { [4 x float] }
%class.btSerializer = type { i32 (...)** }
%struct.btCompoundShapeData = type { %struct.btCollisionShapeData, %struct.btCompoundShapeChildData*, i32, float }
%struct.btCollisionShapeData = type { i8*, i32, [4 x i8] }
%struct.btCompoundShapeChildData = type { %struct.btTransformFloatData, %struct.btCollisionShapeData*, i32, float }
%struct.btTransformFloatData = type { %struct.btMatrix3x3FloatData, %struct.btVector3FloatData }
%struct.btMatrix3x3FloatData = type { [3 x %struct.btVector3FloatData] }
%struct.btVector3FloatData = type { [4 x float] }
%class.btChunk = type { i32, i32, i8*, i32, i32 }

$_ZN16btCollisionShapeC2Ev = comdat any

$_ZN20btAlignedObjectArrayI20btCompoundShapeChildEC2Ev = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN20btAlignedObjectArrayI20btCompoundShapeChildED2Ev = comdat any

$_ZN16btCollisionShapeD2Ev = comdat any

$_ZN15btCompoundShapedlEPv = comdat any

$_ZN20btCompoundShapeChildC2Ev = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZNK16btCollisionShape12getShapeTypeEv = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN12btDbvtAabbMm6FromMMERK9btVector3S2_ = comdat any

$_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI20btCompoundShapeChildE9push_backERKS0_ = comdat any

$_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi = comdat any

$_ZN20btAlignedObjectArrayI20btCompoundShapeChildE4swapEii = comdat any

$_ZN20btAlignedObjectArrayI20btCompoundShapeChildE8pop_backEv = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x38absoluteEv = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZN11btMatrix3x3ixEi = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN11btTransform11setIdentityEv = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZN11btTransform9setOriginERK9btVector3 = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZN11btMatrix3x311diagonalizeERS_fi = comdat any

$_ZN11btTransform8getBasisEv = comdat any

$_ZN15btCompoundShape17getChildTransformEi = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZdvRK9btVector3S1_ = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZN11btTransform9getOriginEv = comdat any

$_ZNK11btTransform14serializeFloatER20btTransformFloatData = comdat any

$_ZNK15btCompoundShape15getLocalScalingEv = comdat any

$_ZNK15btCompoundShape7getNameEv = comdat any

$_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZN15btCompoundShape9setMarginEf = comdat any

$_ZNK15btCompoundShape9getMarginEv = comdat any

$_ZNK15btCompoundShape28calculateSerializeBufferSizeEv = comdat any

$_ZN12btDbvtAabbMmC2Ev = comdat any

$_Z6btFabsf = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN11btMatrix3x311setIdentityEv = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_Z6btSqrtf = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZNK11btMatrix3x314serializeFloatER20btMatrix3x3FloatData = comdat any

$_ZNK9btVector314serializeFloatER18btVector3FloatData = comdat any

$_ZN18btAlignedAllocatorI20btCompoundShapeChildLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI20btCompoundShapeChildE4initEv = comdat any

$_ZN20btAlignedObjectArrayI20btCompoundShapeChildE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI20btCompoundShapeChildE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI20btCompoundShapeChildE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI20btCompoundShapeChildLj16EE10deallocateEPS0_ = comdat any

$_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI20btCompoundShapeChildE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI20btCompoundShapeChildE9allocSizeEi = comdat any

$_ZN20btCompoundShapeChildnwEmPv = comdat any

$_ZN20btCompoundShapeChildC2ERKS_ = comdat any

$_ZN20btAlignedObjectArrayI20btCompoundShapeChildE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI20btCompoundShapeChildLj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btCompoundShapeChildaSERKS_ = comdat any

@_ZTV15btCompoundShape = hidden unnamed_addr constant { [20 x i8*] } { [20 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI15btCompoundShape to i8*), i8* bitcast (%class.btCompoundShape* (%class.btCompoundShape*)* @_ZN15btCompoundShapeD1Ev to i8*), i8* bitcast (void (%class.btCompoundShape*)* @_ZN15btCompoundShapeD0Ev to i8*), i8* bitcast (void (%class.btCompoundShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK15btCompoundShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btCompoundShape*, %class.btVector3*)* @_ZN15btCompoundShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btCompoundShape*)* @_ZNK15btCompoundShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btCompoundShape*, float, %class.btVector3*)* @_ZNK15btCompoundShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btCompoundShape*)* @_ZNK15btCompoundShape7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btCompoundShape*, float)* @_ZN15btCompoundShape9setMarginEf to i8*), i8* bitcast (float (%class.btCompoundShape*)* @_ZNK15btCompoundShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btCompoundShape*)* @_ZNK15btCompoundShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btCompoundShape*, i8*, %class.btSerializer*)* @_ZNK15btCompoundShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btCompoundShape*, %class.btCollisionShape*)* @_ZN15btCompoundShape16removeChildShapeEP16btCollisionShape to i8*), i8* bitcast (void (%class.btCompoundShape*)* @_ZN15btCompoundShape20recalculateLocalAabbEv to i8*)] }, align 4
@.str = private unnamed_addr constant [25 x i8] c"btCompoundShapeChildData\00", align 1
@.str.1 = private unnamed_addr constant [20 x i8] c"btCompoundShapeData\00", align 1
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS15btCompoundShape = hidden constant [18 x i8] c"15btCompoundShape\00", align 1
@_ZTI16btCollisionShape = external constant i8*
@_ZTI15btCompoundShape = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([18 x i8], [18 x i8]* @_ZTS15btCompoundShape, i32 0, i32 0), i8* bitcast (i8** @_ZTI16btCollisionShape to i8*) }, align 4
@_ZTV16btCollisionShape = external unnamed_addr constant { [18 x i8*] }, align 4
@.str.2 = private unnamed_addr constant [9 x i8] c"Compound\00", align 1

@_ZN15btCompoundShapeC1Eb = hidden unnamed_addr alias %class.btCompoundShape* (%class.btCompoundShape*, i1), %class.btCompoundShape* (%class.btCompoundShape*, i1)* @_ZN15btCompoundShapeC2Eb
@_ZN15btCompoundShapeD1Ev = hidden unnamed_addr alias %class.btCompoundShape* (%class.btCompoundShape*), %class.btCompoundShape* (%class.btCompoundShape*)* @_ZN15btCompoundShapeD2Ev

define hidden %class.btCompoundShape* @_ZN15btCompoundShapeC2Eb(%class.btCompoundShape* returned %this, i1 zeroext %enableDynamicAabbTree) unnamed_addr #0 {
entry:
  %retval = alloca %class.btCompoundShape*, align 4
  %this.addr = alloca %class.btCompoundShape*, align 4
  %enableDynamicAabbTree.addr = alloca i8, align 1
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %mem = alloca i8*, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  %frombool = zext i1 %enableDynamicAabbTree to i8
  store i8 %frombool, i8* %enableDynamicAabbTree.addr, align 1, !tbaa !6
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  store %class.btCompoundShape* %this1, %class.btCompoundShape** %retval, align 4
  %0 = bitcast %class.btCompoundShape* %this1 to %class.btCollisionShape*
  %call = call %class.btCollisionShape* @_ZN16btCollisionShapeC2Ev(%class.btCollisionShape* %0)
  %1 = bitcast %class.btCompoundShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [20 x i8*] }, { [20 x i8*] }* @_ZTV15btCompoundShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !8
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEC2Ev(%class.btAlignedObjectArray* %m_children)
  %m_localAabbMin = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 2
  %2 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 0x43ABC16D60000000, float* %ref.tmp, align 4, !tbaa !10
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store float 0x43ABC16D60000000, float* %ref.tmp3, align 4, !tbaa !10
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  store float 0x43ABC16D60000000, float* %ref.tmp4, align 4, !tbaa !10
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %m_localAabbMin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %5 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  %6 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %7 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  %m_localAabbMax = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 3
  %8 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  store float 0xC3ABC16D60000000, float* %ref.tmp6, align 4, !tbaa !10
  %9 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  store float 0xC3ABC16D60000000, float* %ref.tmp7, align 4, !tbaa !10
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  store float 0xC3ABC16D60000000, float* %ref.tmp8, align 4, !tbaa !10
  %call9 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %m_localAabbMax, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  %11 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  %12 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  %13 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  %m_dynamicAabbTree = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  store %struct.btDbvt* null, %struct.btDbvt** %m_dynamicAabbTree, align 4, !tbaa !12
  %m_updateRevision = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 5
  store i32 1, i32* %m_updateRevision, align 4, !tbaa !18
  %m_collisionMargin = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 6
  store float 0.000000e+00, float* %m_collisionMargin, align 4, !tbaa !19
  %m_localScaling = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 7
  %14 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #8
  store float 1.000000e+00, float* %ref.tmp10, align 4, !tbaa !10
  %15 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  store float 1.000000e+00, float* %ref.tmp11, align 4, !tbaa !10
  %16 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  store float 1.000000e+00, float* %ref.tmp12, align 4, !tbaa !10
  %call13 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %m_localScaling, float* nonnull align 4 dereferenceable(4) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp12)
  %17 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  %18 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  %19 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  %20 = bitcast %class.btCompoundShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %20, i32 0, i32 1
  store i32 31, i32* %m_shapeType, align 4, !tbaa !20
  %21 = load i8, i8* %enableDynamicAabbTree.addr, align 1, !tbaa !6, !range !22
  %tobool = trunc i8 %21 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %22 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #8
  %call14 = call i8* @_Z22btAlignedAllocInternalmi(i32 60, i32 16)
  store i8* %call14, i8** %mem, align 4, !tbaa !2
  %23 = load i8*, i8** %mem, align 4, !tbaa !2
  %24 = bitcast i8* %23 to %struct.btDbvt*
  %call15 = call %struct.btDbvt* @_ZN6btDbvtC1Ev(%struct.btDbvt* %24)
  %m_dynamicAabbTree16 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  store %struct.btDbvt* %24, %struct.btDbvt** %m_dynamicAabbTree16, align 4, !tbaa !12
  %25 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %26 = load %class.btCompoundShape*, %class.btCompoundShape** %retval, align 4
  ret %class.btCompoundShape* %26
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btCollisionShape* @_ZN16btCollisionShapeC2Ev(%class.btCollisionShape* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %0 = bitcast %class.btCollisionShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [18 x i8*] }, { [18 x i8*] }* @_ZTV16btCollisionShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !8
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 1
  store i32 35, i32* %m_shapeType, align 4, !tbaa !20
  %m_userPointer = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 2
  store i8* null, i8** %m_userPointer, align 4, !tbaa !23
  ret %class.btCollisionShape* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI20btCompoundShapeChildLj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !10
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !10
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !10
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !10
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !10
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !10
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !10
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #4

declare %struct.btDbvt* @_ZN6btDbvtC1Ev(%struct.btDbvt* returned) unnamed_addr #4

; Function Attrs: nounwind
define hidden %class.btCompoundShape* @_ZN15btCompoundShapeD2Ev(%class.btCompoundShape* returned %this) unnamed_addr #1 {
entry:
  %retval = alloca %class.btCompoundShape*, align 4
  %this.addr = alloca %class.btCompoundShape*, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  store %class.btCompoundShape* %this1, %class.btCompoundShape** %retval, align 4
  %0 = bitcast %class.btCompoundShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [20 x i8*] }, { [20 x i8*] }* @_ZTV15btCompoundShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !8
  %m_dynamicAabbTree = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  %1 = load %struct.btDbvt*, %struct.btDbvt** %m_dynamicAabbTree, align 4, !tbaa !12
  %tobool = icmp ne %struct.btDbvt* %1, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_dynamicAabbTree2 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  %2 = load %struct.btDbvt*, %struct.btDbvt** %m_dynamicAabbTree2, align 4, !tbaa !12
  %call = call %struct.btDbvt* @_ZN6btDbvtD1Ev(%struct.btDbvt* %2) #8
  %m_dynamicAabbTree3 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  %3 = load %struct.btDbvt*, %struct.btDbvt** %m_dynamicAabbTree3, align 4, !tbaa !12
  %4 = bitcast %struct.btDbvt* %3 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %call4 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildED2Ev(%class.btAlignedObjectArray* %m_children) #8
  %5 = bitcast %class.btCompoundShape* %this1 to %class.btCollisionShape*
  %call5 = call %class.btCollisionShape* @_ZN16btCollisionShapeD2Ev(%class.btCollisionShape* %5) #8
  %6 = load %class.btCompoundShape*, %class.btCompoundShape** %retval, align 4
  ret %class.btCompoundShape* %6
}

; Function Attrs: nounwind
declare %struct.btDbvt* @_ZN6btDbvtD1Ev(%struct.btDbvt* returned) unnamed_addr #5

declare void @_Z21btAlignedFreeInternalPv(i8*) #4

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btCollisionShape* @_ZN16btCollisionShapeD2Ev(%class.btCollisionShape* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  ret %class.btCollisionShape* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN15btCompoundShapeD0Ev(%class.btCompoundShape* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %call = call %class.btCompoundShape* @_ZN15btCompoundShapeD1Ev(%class.btCompoundShape* %this1) #8
  %0 = bitcast %class.btCompoundShape* %this1 to i8*
  call void @_ZN15btCompoundShapedlEPv(i8* %0) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN15btCompoundShapedlEPv(i8* %ptr) #3 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

define hidden void @_ZN15btCompoundShape13addChildShapeERK11btTransformP16btCollisionShape(%class.btCompoundShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %localTransform, %class.btCollisionShape* %shape) #0 {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %localTransform.addr = alloca %class.btTransform*, align 4
  %shape.addr = alloca %class.btCollisionShape*, align 4
  %child = alloca %struct.btCompoundShapeChild, align 4
  %localAabbMin = alloca %class.btVector3, align 4
  %localAabbMax = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  %bounds = alloca %struct.btDbvtAabbMm, align 4
  %index = alloca i32, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %localTransform, %class.btTransform** %localTransform.addr, align 4, !tbaa !2
  store %class.btCollisionShape* %shape, %class.btCollisionShape** %shape.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_updateRevision = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 5
  %0 = load i32, i32* %m_updateRevision, align 4, !tbaa !18
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %m_updateRevision, align 4, !tbaa !18
  %1 = bitcast %struct.btCompoundShapeChild* %child to i8*
  call void @llvm.lifetime.start.p0i8(i64 80, i8* %1) #8
  %call = call %struct.btCompoundShapeChild* @_ZN20btCompoundShapeChildC2Ev(%struct.btCompoundShapeChild* %child)
  %m_node = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %child, i32 0, i32 4
  store %struct.btDbvtNode* null, %struct.btDbvtNode** %m_node, align 4, !tbaa !24
  %2 = load %class.btTransform*, %class.btTransform** %localTransform.addr, align 4, !tbaa !2
  %m_transform = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %child, i32 0, i32 0
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_transform, %class.btTransform* nonnull align 4 dereferenceable(64) %2)
  %3 = load %class.btCollisionShape*, %class.btCollisionShape** %shape.addr, align 4, !tbaa !2
  %m_childShape = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %child, i32 0, i32 1
  store %class.btCollisionShape* %3, %class.btCollisionShape** %m_childShape, align 4, !tbaa !28
  %4 = load %class.btCollisionShape*, %class.btCollisionShape** %shape.addr, align 4, !tbaa !2
  %call3 = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %4)
  %m_childShapeType = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %child, i32 0, i32 2
  store i32 %call3, i32* %m_childShapeType, align 4, !tbaa !29
  %5 = load %class.btCollisionShape*, %class.btCollisionShape** %shape.addr, align 4, !tbaa !2
  %6 = bitcast %class.btCollisionShape* %5 to float (%class.btCollisionShape*)***
  %vtable = load float (%class.btCollisionShape*)**, float (%class.btCollisionShape*)*** %6, align 4, !tbaa !8
  %vfn = getelementptr inbounds float (%class.btCollisionShape*)*, float (%class.btCollisionShape*)** %vtable, i64 12
  %7 = load float (%class.btCollisionShape*)*, float (%class.btCollisionShape*)** %vfn, align 4
  %call4 = call float %7(%class.btCollisionShape* %5)
  %m_childMargin = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %child, i32 0, i32 3
  store float %call4, float* %m_childMargin, align 4, !tbaa !30
  %8 = bitcast %class.btVector3* %localAabbMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #8
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %localAabbMin)
  %9 = bitcast %class.btVector3* %localAabbMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #8
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %localAabbMax)
  %10 = load %class.btCollisionShape*, %class.btCollisionShape** %shape.addr, align 4, !tbaa !2
  %11 = load %class.btTransform*, %class.btTransform** %localTransform.addr, align 4, !tbaa !2
  %12 = bitcast %class.btCollisionShape* %10 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable7 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %12, align 4, !tbaa !8
  %vfn8 = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable7, i64 2
  %13 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn8, align 4
  call void %13(%class.btCollisionShape* %10, %class.btTransform* nonnull align 4 dereferenceable(64) %11, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax)
  %14 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #8
  store i32 0, i32* %i, align 4, !tbaa !31
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %15 = load i32, i32* %i, align 4, !tbaa !31
  %cmp = icmp slt i32 %15, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %16 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_localAabbMin = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 2
  %call9 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_localAabbMin)
  %17 = load i32, i32* %i, align 4, !tbaa !31
  %arrayidx = getelementptr inbounds float, float* %call9, i32 %17
  %18 = load float, float* %arrayidx, align 4, !tbaa !10
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAabbMin)
  %19 = load i32, i32* %i, align 4, !tbaa !31
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 %19
  %20 = load float, float* %arrayidx11, align 4, !tbaa !10
  %cmp12 = fcmp ogt float %18, %20
  br i1 %cmp12, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %call13 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAabbMin)
  %21 = load i32, i32* %i, align 4, !tbaa !31
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 %21
  %22 = load float, float* %arrayidx14, align 4, !tbaa !10
  %m_localAabbMin15 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 2
  %call16 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_localAabbMin15)
  %23 = load i32, i32* %i, align 4, !tbaa !31
  %arrayidx17 = getelementptr inbounds float, float* %call16, i32 %23
  store float %22, float* %arrayidx17, align 4, !tbaa !10
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %m_localAabbMax = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 3
  %call18 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_localAabbMax)
  %24 = load i32, i32* %i, align 4, !tbaa !31
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 %24
  %25 = load float, float* %arrayidx19, align 4, !tbaa !10
  %call20 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAabbMax)
  %26 = load i32, i32* %i, align 4, !tbaa !31
  %arrayidx21 = getelementptr inbounds float, float* %call20, i32 %26
  %27 = load float, float* %arrayidx21, align 4, !tbaa !10
  %cmp22 = fcmp olt float %25, %27
  br i1 %cmp22, label %if.then23, label %if.end29

if.then23:                                        ; preds = %if.end
  %call24 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAabbMax)
  %28 = load i32, i32* %i, align 4, !tbaa !31
  %arrayidx25 = getelementptr inbounds float, float* %call24, i32 %28
  %29 = load float, float* %arrayidx25, align 4, !tbaa !10
  %m_localAabbMax26 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 3
  %call27 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_localAabbMax26)
  %30 = load i32, i32* %i, align 4, !tbaa !31
  %arrayidx28 = getelementptr inbounds float, float* %call27, i32 %30
  store float %29, float* %arrayidx28, align 4, !tbaa !10
  br label %if.end29

if.end29:                                         ; preds = %if.then23, %if.end
  br label %for.inc

for.inc:                                          ; preds = %if.end29
  %31 = load i32, i32* %i, align 4, !tbaa !31
  %inc30 = add nsw i32 %31, 1
  store i32 %inc30, i32* %i, align 4, !tbaa !31
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %m_dynamicAabbTree = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  %32 = load %struct.btDbvt*, %struct.btDbvt** %m_dynamicAabbTree, align 4, !tbaa !12
  %tobool = icmp ne %struct.btDbvt* %32, null
  br i1 %tobool, label %if.then31, label %if.end36

if.then31:                                        ; preds = %for.end
  %33 = bitcast %struct.btDbvtAabbMm* %bounds to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %33) #8
  call void @_ZN12btDbvtAabbMm6FromMMERK9btVector3S2_(%struct.btDbvtAabbMm* sret align 4 %bounds, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax)
  %34 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #8
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %call32 = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray* %m_children)
  store i32 %call32, i32* %index, align 4, !tbaa !31
  %m_dynamicAabbTree33 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  %35 = load %struct.btDbvt*, %struct.btDbvt** %m_dynamicAabbTree33, align 4, !tbaa !12
  %36 = load i32, i32* %index, align 4, !tbaa !31
  %37 = inttoptr i32 %36 to i8*
  %call34 = call %struct.btDbvtNode* @_ZN6btDbvt6insertERK12btDbvtAabbMmPv(%struct.btDbvt* %35, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %bounds, i8* %37)
  %m_node35 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %child, i32 0, i32 4
  store %struct.btDbvtNode* %call34, %struct.btDbvtNode** %m_node35, align 4, !tbaa !24
  %38 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #8
  %39 = bitcast %struct.btDbvtAabbMm* %bounds to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %39) #8
  br label %if.end36

if.end36:                                         ; preds = %if.then31, %for.end
  %m_children37 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE9push_backERKS0_(%class.btAlignedObjectArray* %m_children37, %struct.btCompoundShapeChild* nonnull align 4 dereferenceable(80) %child)
  %40 = bitcast %class.btVector3* %localAabbMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %40) #8
  %41 = bitcast %class.btVector3* %localAabbMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %41) #8
  %42 = bitcast %struct.btCompoundShapeChild* %child to i8*
  call void @llvm.lifetime.end.p0i8(i64 80, i8* %42) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden %struct.btCompoundShapeChild* @_ZN20btCompoundShapeChildC2Ev(%struct.btCompoundShapeChild* returned %this) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %struct.btCompoundShapeChild*, align 4
  store %struct.btCompoundShapeChild* %this, %struct.btCompoundShapeChild** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %this.addr, align 4
  %m_transform = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %this1, i32 0, i32 0
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_transform)
  ret %struct.btCompoundShapeChild* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #6 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !32
  ret %class.btTransform* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_shapeType, align 4, !tbaa !20
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN12btDbvtAabbMm6FromMMERK9btVector3S2_(%struct.btDbvtAabbMm* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %mi, %class.btVector3* nonnull align 4 dereferenceable(16) %mx) #6 comdat {
entry:
  %mi.addr = alloca %class.btVector3*, align 4
  %mx.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %mi, %class.btVector3** %mi.addr, align 4, !tbaa !2
  store %class.btVector3* %mx, %class.btVector3** %mx.addr, align 4, !tbaa !2
  %call = call %struct.btDbvtAabbMm* @_ZN12btDbvtAabbMmC2Ev(%struct.btDbvtAabbMm* %agg.result)
  %0 = load %class.btVector3*, %class.btVector3** %mi.addr, align 4, !tbaa !2
  %mi1 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %agg.result, i32 0, i32 0
  %1 = bitcast %class.btVector3* %mi1 to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !32
  %3 = load %class.btVector3*, %class.btVector3** %mx.addr, align 4, !tbaa !2
  %mx2 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %agg.result, i32 0, i32 1
  %4 = bitcast %class.btVector3* %mx2 to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !32
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !34
  ret i32 %0
}

declare %struct.btDbvtNode* @_ZN6btDbvt6insertERK12btDbvtAabbMmPv(%struct.btDbvt*, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32), i8*) #4

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE9push_backERKS0_(%class.btAlignedObjectArray* %this, %struct.btCompoundShapeChild* nonnull align 4 dereferenceable(80) %_Val) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %struct.btCompoundShapeChild*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store %struct.btCompoundShapeChild* %_Val, %struct.btCompoundShapeChild** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !31
  %1 = load i32, i32* %sz, align 4, !tbaa !31
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data, align 4, !tbaa !35
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !34
  %arrayidx = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %2, i32 %3
  %4 = bitcast %struct.btCompoundShapeChild* %arrayidx to i8*
  %call5 = call i8* @_ZN20btCompoundShapeChildnwEmPv(i32 80, i8* %4)
  %5 = bitcast i8* %call5 to %struct.btCompoundShapeChild*
  %6 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %_Val.addr, align 4, !tbaa !2
  %call6 = call %struct.btCompoundShapeChild* @_ZN20btCompoundShapeChildC2ERKS_(%struct.btCompoundShapeChild* %5, %struct.btCompoundShapeChild* nonnull align 4 dereferenceable(80) %6)
  %m_size7 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size7, align 4, !tbaa !34
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size7, align 4, !tbaa !34
  %8 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  ret void
}

define hidden void @_ZN15btCompoundShape20updateChildTransformEiRK11btTransformb(%class.btCompoundShape* %this, i32 %childIndex, %class.btTransform* nonnull align 4 dereferenceable(64) %newChildTransform, i1 zeroext %shouldRecalculateLocalAabb) #0 {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %childIndex.addr = alloca i32, align 4
  %newChildTransform.addr = alloca %class.btTransform*, align 4
  %shouldRecalculateLocalAabb.addr = alloca i8, align 1
  %localAabbMin = alloca %class.btVector3, align 4
  %localAabbMax = alloca %class.btVector3, align 4
  %bounds = alloca %struct.btDbvtAabbMm, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  store i32 %childIndex, i32* %childIndex.addr, align 4, !tbaa !31
  store %class.btTransform* %newChildTransform, %class.btTransform** %newChildTransform.addr, align 4, !tbaa !2
  %frombool = zext i1 %shouldRecalculateLocalAabb to i8
  store i8 %frombool, i8* %shouldRecalculateLocalAabb.addr, align 1, !tbaa !6
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %newChildTransform.addr, align 4, !tbaa !2
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %1 = load i32, i32* %childIndex.addr, align 4, !tbaa !31
  %call = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children, i32 %1)
  %m_transform = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call, i32 0, i32 0
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_transform, %class.btTransform* nonnull align 4 dereferenceable(64) %0)
  %m_dynamicAabbTree = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  %2 = load %struct.btDbvt*, %struct.btDbvt** %m_dynamicAabbTree, align 4, !tbaa !12
  %tobool = icmp ne %struct.btDbvt* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = bitcast %class.btVector3* %localAabbMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #8
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %localAabbMin)
  %4 = bitcast %class.btVector3* %localAabbMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #8
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %localAabbMax)
  %m_children5 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %5 = load i32, i32* %childIndex.addr, align 4, !tbaa !31
  %call6 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children5, i32 %5)
  %m_childShape = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call6, i32 0, i32 1
  %6 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape, align 4, !tbaa !28
  %7 = load %class.btTransform*, %class.btTransform** %newChildTransform.addr, align 4, !tbaa !2
  %8 = bitcast %class.btCollisionShape* %6 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %8, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %9 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %9(%class.btCollisionShape* %6, %class.btTransform* nonnull align 4 dereferenceable(64) %7, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax)
  %10 = bitcast %struct.btDbvtAabbMm* %bounds to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %10) #8
  call void @_ZN12btDbvtAabbMm6FromMMERK9btVector3S2_(%struct.btDbvtAabbMm* sret align 4 %bounds, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax)
  %m_dynamicAabbTree7 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  %11 = load %struct.btDbvt*, %struct.btDbvt** %m_dynamicAabbTree7, align 4, !tbaa !12
  %m_children8 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %12 = load i32, i32* %childIndex.addr, align 4, !tbaa !31
  %call9 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children8, i32 %12)
  %m_node = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call9, i32 0, i32 4
  %13 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_node, align 4, !tbaa !24
  call void @_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMm(%struct.btDbvt* %11, %struct.btDbvtNode* %13, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %bounds)
  %14 = bitcast %struct.btDbvtAabbMm* %bounds to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %14) #8
  %15 = bitcast %class.btVector3* %localAabbMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %15) #8
  %16 = bitcast %class.btVector3* %localAabbMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %16) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %17 = load i8, i8* %shouldRecalculateLocalAabb.addr, align 1, !tbaa !6, !range !22
  %tobool10 = trunc i8 %17 to i1
  br i1 %tobool10, label %if.then11, label %if.end14

if.then11:                                        ; preds = %if.end
  %18 = bitcast %class.btCompoundShape* %this1 to void (%class.btCompoundShape*)***
  %vtable12 = load void (%class.btCompoundShape*)**, void (%class.btCompoundShape*)*** %18, align 4, !tbaa !8
  %vfn13 = getelementptr inbounds void (%class.btCompoundShape*)*, void (%class.btCompoundShape*)** %vtable12, i64 17
  %19 = load void (%class.btCompoundShape*)*, void (%class.btCompoundShape*)** %vfn13, align 4
  call void %19(%class.btCompoundShape* %this1)
  br label %if.end14

if.end14:                                         ; preds = %if.then11, %if.end
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !31
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data, align 4, !tbaa !35
  %1 = load i32, i32* %n.addr, align 4, !tbaa !31
  %arrayidx = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %0, i32 %1
  ret %struct.btCompoundShapeChild* %arrayidx
}

declare void @_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMm(%struct.btDbvt*, %struct.btDbvtNode*, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32)) #4

define hidden void @_ZN15btCompoundShape23removeChildShapeByIndexEi(%class.btCompoundShape* %this, i32 %childShapeIndex) #0 {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %childShapeIndex.addr = alloca i32, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  store i32 %childShapeIndex, i32* %childShapeIndex.addr, align 4, !tbaa !31
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_updateRevision = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 5
  %0 = load i32, i32* %m_updateRevision, align 4, !tbaa !18
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %m_updateRevision, align 4, !tbaa !18
  %m_dynamicAabbTree = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  %1 = load %struct.btDbvt*, %struct.btDbvt** %m_dynamicAabbTree, align 4, !tbaa !12
  %tobool = icmp ne %struct.btDbvt* %1, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_dynamicAabbTree2 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  %2 = load %struct.btDbvt*, %struct.btDbvt** %m_dynamicAabbTree2, align 4, !tbaa !12
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %3 = load i32, i32* %childShapeIndex.addr, align 4, !tbaa !31
  %call = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children, i32 %3)
  %m_node = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call, i32 0, i32 4
  %4 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_node, align 4, !tbaa !24
  call void @_ZN6btDbvt6removeEP10btDbvtNode(%struct.btDbvt* %2, %struct.btDbvtNode* %4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_children3 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %5 = load i32, i32* %childShapeIndex.addr, align 4, !tbaa !31
  %m_children4 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %call5 = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray* %m_children4)
  %sub = sub nsw i32 %call5, 1
  call void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE4swapEii(%class.btAlignedObjectArray* %m_children3, i32 %5, i32 %sub)
  %m_dynamicAabbTree6 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  %6 = load %struct.btDbvt*, %struct.btDbvt** %m_dynamicAabbTree6, align 4, !tbaa !12
  %tobool7 = icmp ne %struct.btDbvt* %6, null
  br i1 %tobool7, label %if.then8, label %if.end12

if.then8:                                         ; preds = %if.end
  %7 = load i32, i32* %childShapeIndex.addr, align 4, !tbaa !31
  %m_children9 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %8 = load i32, i32* %childShapeIndex.addr, align 4, !tbaa !31
  %call10 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children9, i32 %8)
  %m_node11 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call10, i32 0, i32 4
  %9 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_node11, align 4, !tbaa !24
  %10 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %9, i32 0, i32 2
  %dataAsInt = bitcast %union.anon* %10 to i32*
  store i32 %7, i32* %dataAsInt, align 4, !tbaa !33
  br label %if.end12

if.end12:                                         ; preds = %if.then8, %if.end
  %m_children13 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE8pop_backEv(%class.btAlignedObjectArray* %m_children13)
  ret void
}

declare void @_ZN6btDbvt6removeEP10btDbvtNode(%struct.btDbvt*, %struct.btDbvtNode*) #4

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE4swapEii(%class.btAlignedObjectArray* %this, i32 %index0, i32 %index1) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %struct.btCompoundShapeChild, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %index0, i32* %index0.addr, align 4, !tbaa !31
  store i32 %index1, i32* %index1.addr, align 4, !tbaa !31
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast %struct.btCompoundShapeChild* %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 80, i8* %0) #8
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data, align 4, !tbaa !35
  %2 = load i32, i32* %index0.addr, align 4, !tbaa !31
  %arrayidx = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %1, i32 %2
  %call = call %struct.btCompoundShapeChild* @_ZN20btCompoundShapeChildC2ERKS_(%struct.btCompoundShapeChild* %temp, %struct.btCompoundShapeChild* nonnull align 4 dereferenceable(80) %arrayidx)
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data2, align 4, !tbaa !35
  %4 = load i32, i32* %index1.addr, align 4, !tbaa !31
  %arrayidx3 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %3, i32 %4
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %5 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data4, align 4, !tbaa !35
  %6 = load i32, i32* %index0.addr, align 4, !tbaa !31
  %arrayidx5 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %5, i32 %6
  %call6 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btCompoundShapeChildaSERKS_(%struct.btCompoundShapeChild* %arrayidx5, %struct.btCompoundShapeChild* nonnull align 4 dereferenceable(80) %arrayidx3)
  %m_data7 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data7, align 4, !tbaa !35
  %8 = load i32, i32* %index1.addr, align 4, !tbaa !31
  %arrayidx8 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %7, i32 %8
  %call9 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btCompoundShapeChildaSERKS_(%struct.btCompoundShapeChild* %arrayidx8, %struct.btCompoundShapeChild* nonnull align 4 dereferenceable(80) %temp)
  %9 = bitcast %struct.btCompoundShapeChild* %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 80, i8* %9) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE8pop_backEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !34
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4, !tbaa !34
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data, align 4, !tbaa !35
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4, !tbaa !34
  %arrayidx = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %1, i32 %2
  ret void
}

define hidden void @_ZN15btCompoundShape16removeChildShapeEP16btCollisionShape(%class.btCompoundShape* %this, %class.btCollisionShape* %shape) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %shape.addr = alloca %class.btCollisionShape*, align 4
  %i = alloca i32, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  store %class.btCollisionShape* %shape, %class.btCollisionShape** %shape.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_updateRevision = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 5
  %0 = load i32, i32* %m_updateRevision, align 4, !tbaa !18
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %m_updateRevision, align 4, !tbaa !18
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray* %m_children)
  %sub = sub nsw i32 %call, 1
  store i32 %sub, i32* %i, align 4, !tbaa !31
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !31
  %cmp = icmp sge i32 %2, 0
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_children2 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %4 = load i32, i32* %i, align 4, !tbaa !31
  %call3 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children2, i32 %4)
  %m_childShape = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call3, i32 0, i32 1
  %5 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape, align 4, !tbaa !28
  %6 = load %class.btCollisionShape*, %class.btCollisionShape** %shape.addr, align 4, !tbaa !2
  %cmp4 = icmp eq %class.btCollisionShape* %5, %6
  br i1 %cmp4, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !31
  call void @_ZN15btCompoundShape23removeChildShapeByIndexEi(%class.btCompoundShape* %this1, i32 %7)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %8 = load i32, i32* %i, align 4, !tbaa !31
  %dec = add nsw i32 %8, -1
  store i32 %dec, i32* %i, align 4, !tbaa !31
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %9 = bitcast %class.btCompoundShape* %this1 to void (%class.btCompoundShape*)***
  %vtable = load void (%class.btCompoundShape*)**, void (%class.btCompoundShape*)*** %9, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btCompoundShape*)*, void (%class.btCompoundShape*)** %vtable, i64 17
  %10 = load void (%class.btCompoundShape*)*, void (%class.btCompoundShape*)** %vfn, align 4
  call void %10(%class.btCompoundShape* %this1)
  ret void
}

define hidden void @_ZN15btCompoundShape20recalculateLocalAabbEv(%class.btCompoundShape* %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %localAabbMin = alloca %class.btVector3, align 4
  %localAabbMax = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 0x43ABC16D60000000, float* %ref.tmp2, align 4, !tbaa !10
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 0x43ABC16D60000000, float* %ref.tmp3, align 4, !tbaa !10
  %3 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store float 0x43ABC16D60000000, float* %ref.tmp4, align 4, !tbaa !10
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %m_localAabbMin = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 2
  %4 = bitcast %class.btVector3* %m_localAabbMin to i8*
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !32
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %7 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  %8 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  %9 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #8
  %10 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #8
  %11 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  store float 0xC3ABC16D60000000, float* %ref.tmp6, align 4, !tbaa !10
  %12 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  store float 0xC3ABC16D60000000, float* %ref.tmp7, align 4, !tbaa !10
  %13 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  store float 0xC3ABC16D60000000, float* %ref.tmp8, align 4, !tbaa !10
  %call9 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  %m_localAabbMax = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 3
  %14 = bitcast %class.btVector3* %m_localAabbMax to i8*
  %15 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %15, i32 16, i1 false), !tbaa.struct !32
  %16 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  %18 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  %19 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %19) #8
  %20 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #8
  store i32 0, i32* %j, align 4, !tbaa !31
  br label %for.cond

for.cond:                                         ; preds = %for.inc44, %entry
  %21 = load i32, i32* %j, align 4, !tbaa !31
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %call10 = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray* %m_children)
  %cmp = icmp slt i32 %21, %call10
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %22 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #8
  br label %for.end46

for.body:                                         ; preds = %for.cond
  %23 = bitcast %class.btVector3* %localAabbMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %23) #8
  %call11 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %localAabbMin)
  %24 = bitcast %class.btVector3* %localAabbMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %24) #8
  %call12 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %localAabbMax)
  %m_children13 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %25 = load i32, i32* %j, align 4, !tbaa !31
  %call14 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children13, i32 %25)
  %m_childShape = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call14, i32 0, i32 1
  %26 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape, align 4, !tbaa !28
  %m_children15 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %27 = load i32, i32* %j, align 4, !tbaa !31
  %call16 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children15, i32 %27)
  %m_transform = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call16, i32 0, i32 0
  %28 = bitcast %class.btCollisionShape* %26 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %28, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %29 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %29(%class.btCollisionShape* %26, %class.btTransform* nonnull align 4 dereferenceable(64) %m_transform, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax)
  %30 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #8
  store i32 0, i32* %i, align 4, !tbaa !31
  br label %for.cond17

for.cond17:                                       ; preds = %for.inc, %for.body
  %31 = load i32, i32* %i, align 4, !tbaa !31
  %cmp18 = icmp slt i32 %31, 3
  br i1 %cmp18, label %for.body20, label %for.cond.cleanup19

for.cond.cleanup19:                               ; preds = %for.cond17
  store i32 5, i32* %cleanup.dest.slot, align 4
  %32 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #8
  br label %for.end

for.body20:                                       ; preds = %for.cond17
  %m_localAabbMin21 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 2
  %call22 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_localAabbMin21)
  %33 = load i32, i32* %i, align 4, !tbaa !31
  %arrayidx = getelementptr inbounds float, float* %call22, i32 %33
  %34 = load float, float* %arrayidx, align 4, !tbaa !10
  %call23 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAabbMin)
  %35 = load i32, i32* %i, align 4, !tbaa !31
  %arrayidx24 = getelementptr inbounds float, float* %call23, i32 %35
  %36 = load float, float* %arrayidx24, align 4, !tbaa !10
  %cmp25 = fcmp ogt float %34, %36
  br i1 %cmp25, label %if.then, label %if.end

if.then:                                          ; preds = %for.body20
  %call26 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAabbMin)
  %37 = load i32, i32* %i, align 4, !tbaa !31
  %arrayidx27 = getelementptr inbounds float, float* %call26, i32 %37
  %38 = load float, float* %arrayidx27, align 4, !tbaa !10
  %m_localAabbMin28 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 2
  %call29 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_localAabbMin28)
  %39 = load i32, i32* %i, align 4, !tbaa !31
  %arrayidx30 = getelementptr inbounds float, float* %call29, i32 %39
  store float %38, float* %arrayidx30, align 4, !tbaa !10
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body20
  %m_localAabbMax31 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 3
  %call32 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_localAabbMax31)
  %40 = load i32, i32* %i, align 4, !tbaa !31
  %arrayidx33 = getelementptr inbounds float, float* %call32, i32 %40
  %41 = load float, float* %arrayidx33, align 4, !tbaa !10
  %call34 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAabbMax)
  %42 = load i32, i32* %i, align 4, !tbaa !31
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 %42
  %43 = load float, float* %arrayidx35, align 4, !tbaa !10
  %cmp36 = fcmp olt float %41, %43
  br i1 %cmp36, label %if.then37, label %if.end43

if.then37:                                        ; preds = %if.end
  %call38 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAabbMax)
  %44 = load i32, i32* %i, align 4, !tbaa !31
  %arrayidx39 = getelementptr inbounds float, float* %call38, i32 %44
  %45 = load float, float* %arrayidx39, align 4, !tbaa !10
  %m_localAabbMax40 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 3
  %call41 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_localAabbMax40)
  %46 = load i32, i32* %i, align 4, !tbaa !31
  %arrayidx42 = getelementptr inbounds float, float* %call41, i32 %46
  store float %45, float* %arrayidx42, align 4, !tbaa !10
  br label %if.end43

if.end43:                                         ; preds = %if.then37, %if.end
  br label %for.inc

for.inc:                                          ; preds = %if.end43
  %47 = load i32, i32* %i, align 4, !tbaa !31
  %inc = add nsw i32 %47, 1
  store i32 %inc, i32* %i, align 4, !tbaa !31
  br label %for.cond17

for.end:                                          ; preds = %for.cond.cleanup19
  %48 = bitcast %class.btVector3* %localAabbMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %48) #8
  %49 = bitcast %class.btVector3* %localAabbMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %49) #8
  br label %for.inc44

for.inc44:                                        ; preds = %for.end
  %50 = load i32, i32* %j, align 4, !tbaa !31
  %inc45 = add nsw i32 %50, 1
  store i32 %inc45, i32* %j, align 4, !tbaa !31
  br label %for.cond

for.end46:                                        ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

define hidden void @_ZNK15btCompoundShape7getAabbERK11btTransformR9btVector3S4_(%class.btCompoundShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %trans, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %trans.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %localHalfExtents = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %localCenter = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %abs_b = alloca %class.btMatrix3x3, align 4
  %center = alloca %class.btVector3, align 4
  %extent = alloca %class.btVector3, align 4
  %ref.tmp30 = alloca %class.btVector3, align 4
  %ref.tmp31 = alloca %class.btVector3, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %trans, %class.btTransform** %trans.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %localHalfExtents to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 5.000000e-01, float* %ref.tmp, align 4, !tbaa !10
  %2 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #8
  %m_localAabbMax = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 3
  %m_localAabbMin = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localAabbMin)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %localHalfExtents, float* nonnull align 4 dereferenceable(4) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %3 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %3) #8
  %4 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast %class.btVector3* %localCenter to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #8
  %6 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  store float 5.000000e-01, float* %ref.tmp3, align 4, !tbaa !10
  %7 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #8
  %m_localAabbMax5 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 3
  %m_localAabbMin6 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localAabbMax5, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localAabbMin6)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %localCenter, float* nonnull align 4 dereferenceable(4) %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  %8 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #8
  %9 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray* %m_children)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %10 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !10
  %11 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  store float 0.000000e+00, float* %ref.tmp8, align 4, !tbaa !10
  %12 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  store float 0.000000e+00, float* %ref.tmp9, align 4, !tbaa !10
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %localHalfExtents, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %13 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  %14 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #8
  %15 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  store float 0.000000e+00, float* %ref.tmp10, align 4, !tbaa !10
  %17 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #8
  store float 0.000000e+00, float* %ref.tmp11, align 4, !tbaa !10
  %18 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #8
  store float 0.000000e+00, float* %ref.tmp12, align 4, !tbaa !10
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %localCenter, float* nonnull align 4 dereferenceable(4) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp12)
  %19 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  %20 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #8
  %21 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %22 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %22) #8
  %23 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #8
  %24 = bitcast %class.btCompoundShape* %this1 to float (%class.btCompoundShape*)***
  %vtable = load float (%class.btCompoundShape*)**, float (%class.btCompoundShape*)*** %24, align 4, !tbaa !8
  %vfn = getelementptr inbounds float (%class.btCompoundShape*)*, float (%class.btCompoundShape*)** %vtable, i64 12
  %25 = load float (%class.btCompoundShape*)*, float (%class.btCompoundShape*)** %vfn, align 4
  %call15 = call float %25(%class.btCompoundShape* %this1)
  store float %call15, float* %ref.tmp14, align 4, !tbaa !10
  %26 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #8
  %27 = bitcast %class.btCompoundShape* %this1 to float (%class.btCompoundShape*)***
  %vtable17 = load float (%class.btCompoundShape*)**, float (%class.btCompoundShape*)*** %27, align 4, !tbaa !8
  %vfn18 = getelementptr inbounds float (%class.btCompoundShape*)*, float (%class.btCompoundShape*)** %vtable17, i64 12
  %28 = load float (%class.btCompoundShape*)*, float (%class.btCompoundShape*)** %vfn18, align 4
  %call19 = call float %28(%class.btCompoundShape* %this1)
  store float %call19, float* %ref.tmp16, align 4, !tbaa !10
  %29 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #8
  %30 = bitcast %class.btCompoundShape* %this1 to float (%class.btCompoundShape*)***
  %vtable21 = load float (%class.btCompoundShape*)**, float (%class.btCompoundShape*)*** %30, align 4, !tbaa !8
  %vfn22 = getelementptr inbounds float (%class.btCompoundShape*)*, float (%class.btCompoundShape*)** %vtable21, i64 12
  %31 = load float (%class.btCompoundShape*)*, float (%class.btCompoundShape*)** %vfn22, align 4
  %call23 = call float %31(%class.btCompoundShape* %this1)
  store float %call23, float* %ref.tmp20, align 4, !tbaa !10
  %call24 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp20)
  %call25 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %localHalfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp13)
  %32 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #8
  %33 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #8
  %34 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #8
  %35 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %35) #8
  %36 = bitcast %class.btMatrix3x3* %abs_b to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %36) #8
  %37 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4, !tbaa !2
  %call26 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %37)
  call void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* sret align 4 %abs_b, %class.btMatrix3x3* %call26)
  %38 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %38) #8
  %39 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4, !tbaa !2
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %center, %class.btTransform* %39, %class.btVector3* nonnull align 4 dereferenceable(16) %localCenter)
  %40 = bitcast %class.btVector3* %extent to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %40) #8
  %call27 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 0)
  %call28 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 1)
  %call29 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %extent, %class.btVector3* %localHalfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %call27, %class.btVector3* nonnull align 4 dereferenceable(16) %call28, %class.btVector3* nonnull align 4 dereferenceable(16) %call29)
  %41 = bitcast %class.btVector3* %ref.tmp30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %41) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp30, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %42 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %43 = bitcast %class.btVector3* %42 to i8*
  %44 = bitcast %class.btVector3* %ref.tmp30 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %43, i8* align 4 %44, i32 16, i1 false), !tbaa.struct !32
  %45 = bitcast %class.btVector3* %ref.tmp30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %45) #8
  %46 = bitcast %class.btVector3* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %46) #8
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp31, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %47 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %48 = bitcast %class.btVector3* %47 to i8*
  %49 = bitcast %class.btVector3* %ref.tmp31 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %48, i8* align 4 %49, i32 16, i1 false), !tbaa.struct !32
  %50 = bitcast %class.btVector3* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %50) #8
  %51 = bitcast %class.btVector3* %extent to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %51) #8
  %52 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %52) #8
  %53 = bitcast %class.btMatrix3x3* %abs_b to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %53) #8
  %54 = bitcast %class.btVector3* %localCenter to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %54) #8
  %55 = bitcast %class.btVector3* %localHalfExtents to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %55) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #6 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #6 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !10
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !10
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !10
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !10
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !10
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !10
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !10
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !10
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !10
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #6 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !10
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !10
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !10
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !10
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !10
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !10
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !10
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !10
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !10
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !10
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !10
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !10
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !10
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !10
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !10
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !10
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !10
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !10
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !10
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !10
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !10
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !10
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !10
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !10
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #6 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %2 = load float, float* %call, align 4, !tbaa !10
  %call2 = call float @_Z6btFabsf(float %2)
  store float %call2, float* %ref.tmp, align 4, !tbaa !10
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 0
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx5)
  %4 = load float, float* %call6, align 4, !tbaa !10
  %call7 = call float @_Z6btFabsf(float %4)
  store float %call7, float* %ref.tmp3, align 4, !tbaa !10
  %5 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 0
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx10)
  %6 = load float, float* %call11, align 4, !tbaa !10
  %call12 = call float @_Z6btFabsf(float %6)
  store float %call12, float* %ref.tmp8, align 4, !tbaa !10
  %7 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 1
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx15)
  %8 = load float, float* %call16, align 4, !tbaa !10
  %call17 = call float @_Z6btFabsf(float %8)
  store float %call17, float* %ref.tmp13, align 4, !tbaa !10
  %9 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  %m_el19 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el19, i32 0, i32 1
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx20)
  %10 = load float, float* %call21, align 4, !tbaa !10
  %call22 = call float @_Z6btFabsf(float %10)
  store float %call22, float* %ref.tmp18, align 4, !tbaa !10
  %11 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  %m_el24 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el24, i32 0, i32 1
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx25)
  %12 = load float, float* %call26, align 4, !tbaa !10
  %call27 = call float @_Z6btFabsf(float %12)
  store float %call27, float* %ref.tmp23, align 4, !tbaa !10
  %13 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %m_el29 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el29, i32 0, i32 2
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx30)
  %14 = load float, float* %call31, align 4, !tbaa !10
  %call32 = call float @_Z6btFabsf(float %14)
  store float %call32, float* %ref.tmp28, align 4, !tbaa !10
  %15 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  %m_el34 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx35 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el34, i32 0, i32 2
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx35)
  %16 = load float, float* %call36, align 4, !tbaa !10
  %call37 = call float @_Z6btFabsf(float %16)
  store float %call37, float* %ref.tmp33, align 4, !tbaa !10
  %17 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #8
  %m_el39 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx40 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el39, i32 0, i32 2
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx40)
  %18 = load float, float* %call41, align 4, !tbaa !10
  %call42 = call float @_Z6btFabsf(float %18)
  store float %call42, float* %ref.tmp38, align 4, !tbaa !10
  %call43 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp33, float* nonnull align 4 dereferenceable(4) %ref.tmp38)
  %19 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  %20 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #8
  %21 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  %22 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #8
  %23 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #8
  %24 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #8
  %25 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #8
  %26 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #8
  %27 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #6 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4, !tbaa !10
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !10
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !10
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !31
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !31
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

define hidden void @_ZNK15btCompoundShape21calculateLocalInertiaEfR9btVector3(%class.btCompoundShape* %this, float %mass, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %mass.addr = alloca float, align 4
  %inertia.addr = alloca %class.btVector3*, align 4
  %ident = alloca %class.btTransform, align 4
  %aabbMin = alloca %class.btVector3, align 4
  %aabbMax = alloca %class.btVector3, align 4
  %halfExtents = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca float, align 4
  %lx = alloca float, align 4
  %ly = alloca float, align 4
  %lz = alloca float, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  store float %mass, float* %mass.addr, align 4, !tbaa !10
  store %class.btVector3* %inertia, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %0 = bitcast %class.btTransform* %ident to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %0) #8
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %ident)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %ident)
  %1 = bitcast %class.btVector3* %aabbMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #8
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin)
  %2 = bitcast %class.btVector3* %aabbMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #8
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax)
  %3 = bitcast %class.btCompoundShape* %this1 to void (%class.btCompoundShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btCompoundShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCompoundShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %3, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btCompoundShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCompoundShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %4 = load void (%class.btCompoundShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCompoundShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %4(%class.btCompoundShape* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %ident, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax)
  %5 = bitcast %class.btVector3* %halfExtents to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #8
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin)
  %7 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  store float 5.000000e-01, float* %ref.tmp4, align 4, !tbaa !10
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %halfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %8 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  %9 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #8
  %10 = bitcast float* %lx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %halfExtents)
  %11 = load float, float* %call5, align 4, !tbaa !10
  %mul = fmul float 2.000000e+00, %11
  store float %mul, float* %lx, align 4, !tbaa !10
  %12 = bitcast float* %ly to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %halfExtents)
  %13 = load float, float* %call6, align 4, !tbaa !10
  %mul7 = fmul float 2.000000e+00, %13
  store float %mul7, float* %ly, align 4, !tbaa !10
  %14 = bitcast float* %lz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #8
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %halfExtents)
  %15 = load float, float* %call8, align 4, !tbaa !10
  %mul9 = fmul float 2.000000e+00, %15
  store float %mul9, float* %lz, align 4, !tbaa !10
  %16 = load float, float* %mass.addr, align 4, !tbaa !10
  %div = fdiv float %16, 1.200000e+01
  %17 = load float, float* %ly, align 4, !tbaa !10
  %18 = load float, float* %ly, align 4, !tbaa !10
  %mul10 = fmul float %17, %18
  %19 = load float, float* %lz, align 4, !tbaa !10
  %20 = load float, float* %lz, align 4, !tbaa !10
  %mul11 = fmul float %19, %20
  %add = fadd float %mul10, %mul11
  %mul12 = fmul float %div, %add
  %21 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %call13 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %21)
  %arrayidx = getelementptr inbounds float, float* %call13, i32 0
  store float %mul12, float* %arrayidx, align 4, !tbaa !10
  %22 = load float, float* %mass.addr, align 4, !tbaa !10
  %div14 = fdiv float %22, 1.200000e+01
  %23 = load float, float* %lx, align 4, !tbaa !10
  %24 = load float, float* %lx, align 4, !tbaa !10
  %mul15 = fmul float %23, %24
  %25 = load float, float* %lz, align 4, !tbaa !10
  %26 = load float, float* %lz, align 4, !tbaa !10
  %mul16 = fmul float %25, %26
  %add17 = fadd float %mul15, %mul16
  %mul18 = fmul float %div14, %add17
  %27 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %call19 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %27)
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 1
  store float %mul18, float* %arrayidx20, align 4, !tbaa !10
  %28 = load float, float* %mass.addr, align 4, !tbaa !10
  %div21 = fdiv float %28, 1.200000e+01
  %29 = load float, float* %lx, align 4, !tbaa !10
  %30 = load float, float* %lx, align 4, !tbaa !10
  %mul22 = fmul float %29, %30
  %31 = load float, float* %ly, align 4, !tbaa !10
  %32 = load float, float* %ly, align 4, !tbaa !10
  %mul23 = fmul float %31, %32
  %add24 = fadd float %mul22, %mul23
  %mul25 = fmul float %div21, %add24
  %33 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %call26 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %33)
  %arrayidx27 = getelementptr inbounds float, float* %call26, i32 2
  store float %mul25, float* %arrayidx27, align 4, !tbaa !10
  %34 = bitcast float* %lz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #8
  %35 = bitcast float* %ly to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  %36 = bitcast float* %lx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #8
  %37 = bitcast %class.btVector3* %halfExtents to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %37) #8
  %38 = bitcast %class.btVector3* %aabbMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %38) #8
  %39 = bitcast %class.btVector3* %aabbMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %39) #8
  %40 = bitcast %class.btTransform* %ident to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %40) #8
  ret void
}

define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

define linkonce_odr hidden void @_ZN11btTransform11setIdentityEv(%class.btTransform* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !10
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !10
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !10
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_origin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #6 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !10
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !10
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !10
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !10
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !10
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !10
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !10
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !10
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !10
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

define hidden void @_ZNK15btCompoundShape31calculatePrincipalAxisTransformEPfR11btTransformR9btVector3(%class.btCompoundShape* %this, float* %masses, %class.btTransform* nonnull align 4 dereferenceable(64) %principal, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia) #0 {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %masses.addr = alloca float*, align 4
  %principal.addr = alloca %class.btTransform*, align 4
  %inertia.addr = alloca %class.btVector3*, align 4
  %n = alloca i32, align 4
  %totalMass = alloca float, align 4
  %center = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %k = alloca i32, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %tensor = alloca %class.btMatrix3x3, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %i = alloca %class.btVector3, align 4
  %t = alloca %class.btTransform*, align 4
  %o = alloca %class.btVector3, align 4
  %j = alloca %class.btMatrix3x3, align 4
  %ref.tmp46 = alloca %class.btMatrix3x3, align 4
  %o2 = alloca float, align 4
  %ref.tmp60 = alloca float, align 4
  %ref.tmp61 = alloca float, align 4
  %ref.tmp63 = alloca float, align 4
  %ref.tmp64 = alloca float, align 4
  %ref.tmp66 = alloca float, align 4
  %ref.tmp67 = alloca float, align 4
  %ref.tmp68 = alloca %class.btVector3, align 4
  %ref.tmp69 = alloca float, align 4
  %ref.tmp73 = alloca %class.btVector3, align 4
  %ref.tmp74 = alloca float, align 4
  %ref.tmp79 = alloca %class.btVector3, align 4
  %ref.tmp80 = alloca float, align 4
  %ref.tmp85 = alloca %class.btVector3, align 4
  %ref.tmp90 = alloca %class.btVector3, align 4
  %ref.tmp95 = alloca %class.btVector3, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  store float* %masses, float** %masses.addr, align 4, !tbaa !2
  store %class.btTransform* %principal, %class.btTransform** %principal.addr, align 4, !tbaa !2
  store %class.btVector3* %inertia, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %0 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray* %m_children)
  store i32 %call, i32* %n, align 4, !tbaa !31
  %1 = bitcast float* %totalMass to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 0.000000e+00, float* %totalMass, align 4, !tbaa !10
  %2 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #8
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !10
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !10
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !10
  %call4 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %center, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %6 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  %9 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  store i32 0, i32* %k, align 4, !tbaa !31
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %10 = load i32, i32* %k, align 4, !tbaa !31
  %11 = load i32, i32* %n, align 4, !tbaa !31
  %cmp = icmp slt i32 %10, %11
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %12 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %12) #8
  %m_children6 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %13 = load i32, i32* %k, align 4, !tbaa !31
  %call7 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children6, i32 %13)
  %m_transform = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call7, i32 0, i32 0
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %m_transform)
  %14 = load float*, float** %masses.addr, align 4, !tbaa !2
  %15 = load i32, i32* %k, align 4, !tbaa !31
  %arrayidx = getelementptr inbounds float, float* %14, i32 %15
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %call8, float* nonnull align 4 dereferenceable(4) %arrayidx)
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %center, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %16 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %16) #8
  %17 = load float*, float** %masses.addr, align 4, !tbaa !2
  %18 = load i32, i32* %k, align 4, !tbaa !31
  %arrayidx10 = getelementptr inbounds float, float* %17, i32 %18
  %19 = load float, float* %arrayidx10, align 4, !tbaa !10
  %20 = load float, float* %totalMass, align 4, !tbaa !10
  %add = fadd float %20, %19
  store float %add, float* %totalMass, align 4, !tbaa !10
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %21 = load i32, i32* %k, align 4, !tbaa !31
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %k, align 4, !tbaa !31
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call11 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %center, float* nonnull align 4 dereferenceable(4) %totalMass)
  %22 = load %class.btTransform*, %class.btTransform** %principal.addr, align 4, !tbaa !2
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %22, %class.btVector3* nonnull align 4 dereferenceable(16) %center)
  %23 = bitcast %class.btMatrix3x3* %tensor to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %23) #8
  %24 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #8
  store float 0.000000e+00, float* %ref.tmp12, align 4, !tbaa !10
  %25 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  store float 0.000000e+00, float* %ref.tmp13, align 4, !tbaa !10
  %26 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #8
  store float 0.000000e+00, float* %ref.tmp14, align 4, !tbaa !10
  %27 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #8
  store float 0.000000e+00, float* %ref.tmp15, align 4, !tbaa !10
  %28 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #8
  store float 0.000000e+00, float* %ref.tmp16, align 4, !tbaa !10
  %29 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #8
  store float 0.000000e+00, float* %ref.tmp17, align 4, !tbaa !10
  %30 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #8
  store float 0.000000e+00, float* %ref.tmp18, align 4, !tbaa !10
  %31 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #8
  store float 0.000000e+00, float* %ref.tmp19, align 4, !tbaa !10
  %32 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #8
  store float 0.000000e+00, float* %ref.tmp20, align 4, !tbaa !10
  %call21 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %tensor, float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp19, float* nonnull align 4 dereferenceable(4) %ref.tmp20)
  %33 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #8
  %34 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #8
  %35 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  %36 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #8
  %37 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #8
  %38 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #8
  %39 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #8
  %40 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #8
  %41 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #8
  store i32 0, i32* %k, align 4, !tbaa !31
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc100, %for.end
  %42 = load i32, i32* %k, align 4, !tbaa !31
  %43 = load i32, i32* %n, align 4, !tbaa !31
  %cmp23 = icmp slt i32 %42, %43
  br i1 %cmp23, label %for.body24, label %for.end102

for.body24:                                       ; preds = %for.cond22
  %44 = bitcast %class.btVector3* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %44) #8
  %call25 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %i)
  %m_children26 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %45 = load i32, i32* %k, align 4, !tbaa !31
  %call27 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children26, i32 %45)
  %m_childShape = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call27, i32 0, i32 1
  %46 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape, align 4, !tbaa !28
  %47 = load float*, float** %masses.addr, align 4, !tbaa !2
  %48 = load i32, i32* %k, align 4, !tbaa !31
  %arrayidx28 = getelementptr inbounds float, float* %47, i32 %48
  %49 = load float, float* %arrayidx28, align 4, !tbaa !10
  %50 = bitcast %class.btCollisionShape* %46 to void (%class.btCollisionShape*, float, %class.btVector3*)***
  %vtable = load void (%class.btCollisionShape*, float, %class.btVector3*)**, void (%class.btCollisionShape*, float, %class.btVector3*)*** %50, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btCollisionShape*, float, %class.btVector3*)*, void (%class.btCollisionShape*, float, %class.btVector3*)** %vtable, i64 8
  %51 = load void (%class.btCollisionShape*, float, %class.btVector3*)*, void (%class.btCollisionShape*, float, %class.btVector3*)** %vfn, align 4
  call void %51(%class.btCollisionShape* %46, float %49, %class.btVector3* nonnull align 4 dereferenceable(16) %i)
  %52 = bitcast %class.btTransform** %t to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #8
  %m_children29 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %53 = load i32, i32* %k, align 4, !tbaa !31
  %call30 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children29, i32 %53)
  %m_transform31 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call30, i32 0, i32 0
  store %class.btTransform* %m_transform31, %class.btTransform** %t, align 4, !tbaa !2
  %54 = bitcast %class.btVector3* %o to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %54) #8
  %55 = load %class.btTransform*, %class.btTransform** %t, align 4, !tbaa !2
  %call32 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %55)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %o, %class.btVector3* nonnull align 4 dereferenceable(16) %call32, %class.btVector3* nonnull align 4 dereferenceable(16) %center)
  %56 = bitcast %class.btMatrix3x3* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %56) #8
  %57 = load %class.btTransform*, %class.btTransform** %t, align 4, !tbaa !2
  %call33 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %57)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %j, %class.btMatrix3x3* %call33)
  %call34 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %i)
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 0
  %call36 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %j, i32 0)
  %call37 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %call36, float* nonnull align 4 dereferenceable(4) %arrayidx35)
  %call38 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %i)
  %arrayidx39 = getelementptr inbounds float, float* %call38, i32 1
  %call40 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %j, i32 1)
  %call41 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %call40, float* nonnull align 4 dereferenceable(4) %arrayidx39)
  %call42 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %i)
  %arrayidx43 = getelementptr inbounds float, float* %call42, i32 2
  %call44 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %j, i32 2)
  %call45 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %call44, float* nonnull align 4 dereferenceable(4) %arrayidx43)
  %58 = bitcast %class.btMatrix3x3* %ref.tmp46 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %58) #8
  %59 = load %class.btTransform*, %class.btTransform** %t, align 4, !tbaa !2
  %call47 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %59)
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp46, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call47, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %j)
  %call48 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %j, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp46)
  %60 = bitcast %class.btMatrix3x3* %ref.tmp46 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %60) #8
  %call49 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %j, i32 0)
  %call50 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %tensor, i32 0)
  %call51 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %call50, %class.btVector3* nonnull align 4 dereferenceable(16) %call49)
  %call52 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %j, i32 1)
  %call53 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %tensor, i32 1)
  %call54 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %call53, %class.btVector3* nonnull align 4 dereferenceable(16) %call52)
  %call55 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %j, i32 2)
  %call56 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %tensor, i32 2)
  %call57 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %call56, %class.btVector3* nonnull align 4 dereferenceable(16) %call55)
  %61 = bitcast float* %o2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %61) #8
  %call58 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %o)
  store float %call58, float* %o2, align 4, !tbaa !10
  %call59 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %j, i32 0)
  %62 = bitcast float* %ref.tmp60 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #8
  store float 0.000000e+00, float* %ref.tmp60, align 4, !tbaa !10
  %63 = bitcast float* %ref.tmp61 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #8
  store float 0.000000e+00, float* %ref.tmp61, align 4, !tbaa !10
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %call59, float* nonnull align 4 dereferenceable(4) %o2, float* nonnull align 4 dereferenceable(4) %ref.tmp60, float* nonnull align 4 dereferenceable(4) %ref.tmp61)
  %64 = bitcast float* %ref.tmp61 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #8
  %65 = bitcast float* %ref.tmp60 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #8
  %call62 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %j, i32 1)
  %66 = bitcast float* %ref.tmp63 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #8
  store float 0.000000e+00, float* %ref.tmp63, align 4, !tbaa !10
  %67 = bitcast float* %ref.tmp64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #8
  store float 0.000000e+00, float* %ref.tmp64, align 4, !tbaa !10
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %call62, float* nonnull align 4 dereferenceable(4) %ref.tmp63, float* nonnull align 4 dereferenceable(4) %o2, float* nonnull align 4 dereferenceable(4) %ref.tmp64)
  %68 = bitcast float* %ref.tmp64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #8
  %69 = bitcast float* %ref.tmp63 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #8
  %call65 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %j, i32 2)
  %70 = bitcast float* %ref.tmp66 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #8
  store float 0.000000e+00, float* %ref.tmp66, align 4, !tbaa !10
  %71 = bitcast float* %ref.tmp67 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #8
  store float 0.000000e+00, float* %ref.tmp67, align 4, !tbaa !10
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %call65, float* nonnull align 4 dereferenceable(4) %ref.tmp66, float* nonnull align 4 dereferenceable(4) %ref.tmp67, float* nonnull align 4 dereferenceable(4) %o2)
  %72 = bitcast float* %ref.tmp67 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #8
  %73 = bitcast float* %ref.tmp66 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #8
  %74 = bitcast %class.btVector3* %ref.tmp68 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %74) #8
  %75 = bitcast float* %ref.tmp69 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #8
  %call70 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %o)
  %76 = load float, float* %call70, align 4, !tbaa !10
  %fneg = fneg float %76
  store float %fneg, float* %ref.tmp69, align 4, !tbaa !10
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp68, %class.btVector3* nonnull align 4 dereferenceable(16) %o, float* nonnull align 4 dereferenceable(4) %ref.tmp69)
  %call71 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %j, i32 0)
  %call72 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %call71, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp68)
  %77 = bitcast float* %ref.tmp69 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #8
  %78 = bitcast %class.btVector3* %ref.tmp68 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %78) #8
  %79 = bitcast %class.btVector3* %ref.tmp73 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %79) #8
  %80 = bitcast float* %ref.tmp74 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #8
  %call75 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %o)
  %81 = load float, float* %call75, align 4, !tbaa !10
  %fneg76 = fneg float %81
  store float %fneg76, float* %ref.tmp74, align 4, !tbaa !10
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp73, %class.btVector3* nonnull align 4 dereferenceable(16) %o, float* nonnull align 4 dereferenceable(4) %ref.tmp74)
  %call77 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %j, i32 1)
  %call78 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %call77, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp73)
  %82 = bitcast float* %ref.tmp74 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #8
  %83 = bitcast %class.btVector3* %ref.tmp73 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %83) #8
  %84 = bitcast %class.btVector3* %ref.tmp79 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %84) #8
  %85 = bitcast float* %ref.tmp80 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %85) #8
  %call81 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %o)
  %86 = load float, float* %call81, align 4, !tbaa !10
  %fneg82 = fneg float %86
  store float %fneg82, float* %ref.tmp80, align 4, !tbaa !10
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp79, %class.btVector3* nonnull align 4 dereferenceable(16) %o, float* nonnull align 4 dereferenceable(4) %ref.tmp80)
  %call83 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %j, i32 2)
  %call84 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %call83, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp79)
  %87 = bitcast float* %ref.tmp80 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #8
  %88 = bitcast %class.btVector3* %ref.tmp79 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %88) #8
  %89 = bitcast %class.btVector3* %ref.tmp85 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %89) #8
  %90 = load float*, float** %masses.addr, align 4, !tbaa !2
  %91 = load i32, i32* %k, align 4, !tbaa !31
  %arrayidx86 = getelementptr inbounds float, float* %90, i32 %91
  %call87 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %j, i32 0)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp85, float* nonnull align 4 dereferenceable(4) %arrayidx86, %class.btVector3* nonnull align 4 dereferenceable(16) %call87)
  %call88 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %tensor, i32 0)
  %call89 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %call88, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp85)
  %92 = bitcast %class.btVector3* %ref.tmp85 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %92) #8
  %93 = bitcast %class.btVector3* %ref.tmp90 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %93) #8
  %94 = load float*, float** %masses.addr, align 4, !tbaa !2
  %95 = load i32, i32* %k, align 4, !tbaa !31
  %arrayidx91 = getelementptr inbounds float, float* %94, i32 %95
  %call92 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %j, i32 1)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp90, float* nonnull align 4 dereferenceable(4) %arrayidx91, %class.btVector3* nonnull align 4 dereferenceable(16) %call92)
  %call93 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %tensor, i32 1)
  %call94 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %call93, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp90)
  %96 = bitcast %class.btVector3* %ref.tmp90 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %96) #8
  %97 = bitcast %class.btVector3* %ref.tmp95 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %97) #8
  %98 = load float*, float** %masses.addr, align 4, !tbaa !2
  %99 = load i32, i32* %k, align 4, !tbaa !31
  %arrayidx96 = getelementptr inbounds float, float* %98, i32 %99
  %call97 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %j, i32 2)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp95, float* nonnull align 4 dereferenceable(4) %arrayidx96, %class.btVector3* nonnull align 4 dereferenceable(16) %call97)
  %call98 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %tensor, i32 2)
  %call99 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %call98, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp95)
  %100 = bitcast %class.btVector3* %ref.tmp95 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %100) #8
  %101 = bitcast float* %o2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #8
  %102 = bitcast %class.btMatrix3x3* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %102) #8
  %103 = bitcast %class.btVector3* %o to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %103) #8
  %104 = bitcast %class.btTransform** %t to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #8
  %105 = bitcast %class.btVector3* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %105) #8
  br label %for.inc100

for.inc100:                                       ; preds = %for.body24
  %106 = load i32, i32* %k, align 4, !tbaa !31
  %inc101 = add nsw i32 %106, 1
  store i32 %inc101, i32* %k, align 4, !tbaa !31
  br label %for.cond22

for.end102:                                       ; preds = %for.cond22
  %107 = load %class.btTransform*, %class.btTransform** %principal.addr, align 4, !tbaa !2
  %call103 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %107)
  call void @_ZN11btMatrix3x311diagonalizeERS_fi(%class.btMatrix3x3* %tensor, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call103, float 0x3EE4F8B580000000, i32 20)
  %108 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %call104 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %tensor, i32 0)
  %call105 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call104)
  %arrayidx106 = getelementptr inbounds float, float* %call105, i32 0
  %call107 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %tensor, i32 1)
  %call108 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call107)
  %arrayidx109 = getelementptr inbounds float, float* %call108, i32 1
  %call110 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %tensor, i32 2)
  %call111 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call110)
  %arrayidx112 = getelementptr inbounds float, float* %call111, i32 2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %108, float* nonnull align 4 dereferenceable(4) %arrayidx106, float* nonnull align 4 dereferenceable(4) %arrayidx109, float* nonnull align 4 dereferenceable(4) %arrayidx112)
  %109 = bitcast %class.btMatrix3x3* %tensor to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %109) #8
  %110 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #8
  %111 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %111) #8
  %112 = bitcast float* %totalMass to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #8
  %113 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !31
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data, align 4, !tbaa !35
  %1 = load i32, i32* %n.addr, align 4, !tbaa !31
  %arrayidx = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %0, i32 %1
  ret %struct.btCompoundShapeChild* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !10
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !10
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  ret %class.btVector3* %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %origin) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %origin.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %origin, %class.btVector3** %origin.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %origin.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_origin to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !32
  ret void
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #6 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !10
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !10
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !10
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !10
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !10
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !10
  %6 = load float*, float** %s.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !10
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !10
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !10
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #6 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %3, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4, !tbaa !10
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !10
  %7 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %9, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %8, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !10
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4, !tbaa !10
  %13 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %15, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %14, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4, !tbaa !10
  %16 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4, !tbaa !10
  %19 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  %20 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %21 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %21, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %20, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4, !tbaa !10
  %22 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #8
  %23 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %24 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %24, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %23, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4, !tbaa !10
  %25 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  %26 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %27 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %27, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %26, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4, !tbaa !10
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  %28 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #8
  %29 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #8
  %30 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #8
  %31 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #8
  %32 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #8
  %33 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #8
  %34 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #8
  %35 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  %36 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !32
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !32
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !32
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

define linkonce_odr hidden void @_ZN11btMatrix3x311diagonalizeERS_fi(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %rot, float %threshold, i32 %maxSteps) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %rot.addr = alloca %class.btMatrix3x3*, align 4
  %threshold.addr = alloca float, align 4
  %maxSteps.addr = alloca i32, align 4
  %step = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %p = alloca i32, align 4
  %q = alloca i32, align 4
  %r = alloca i32, align 4
  %max = alloca float, align 4
  %v = alloca float, align 4
  %t = alloca float, align 4
  %mpq = alloca float, align 4
  %theta = alloca float, align 4
  %theta2 = alloca float, align 4
  %cos = alloca float, align 4
  %sin = alloca float, align 4
  %mrp = alloca float, align 4
  %mrq = alloca float, align 4
  %i = alloca i32, align 4
  %row = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %rot, %class.btMatrix3x3** %rot.addr, align 4, !tbaa !2
  store float %threshold, float* %threshold.addr, align 4, !tbaa !10
  store i32 %maxSteps, i32* %maxSteps.addr, align 4, !tbaa !31
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %0)
  %1 = bitcast i32* %step to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %maxSteps.addr, align 4, !tbaa !31
  store i32 %2, i32* %step, align 4, !tbaa !31
  br label %for.cond

for.cond:                                         ; preds = %for.inc155, %entry
  %3 = load i32, i32* %step, align 4, !tbaa !31
  %cmp = icmp sgt i32 %3, 0
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup156

for.body:                                         ; preds = %for.cond
  %4 = bitcast i32* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  store i32 0, i32* %p, align 4, !tbaa !31
  %5 = bitcast i32* %q to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  store i32 1, i32* %q, align 4, !tbaa !31
  %6 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  store i32 2, i32* %r, align 4, !tbaa !31
  %7 = bitcast float* %max to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx)
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 1
  %8 = load float, float* %arrayidx2, align 4, !tbaa !10
  %call3 = call float @_Z6btFabsf(float %8)
  store float %call3, float* %max, align 4, !tbaa !10
  %9 = bitcast float* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 0
  %call6 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx5)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 2
  %10 = load float, float* %arrayidx7, align 4, !tbaa !10
  %call8 = call float @_Z6btFabsf(float %10)
  store float %call8, float* %v, align 4, !tbaa !10
  %11 = load float, float* %v, align 4, !tbaa !10
  %12 = load float, float* %max, align 4, !tbaa !10
  %cmp9 = fcmp ogt float %11, %12
  br i1 %cmp9, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i32 2, i32* %q, align 4, !tbaa !31
  store i32 1, i32* %r, align 4, !tbaa !31
  %13 = load float, float* %v, align 4, !tbaa !10
  store float %13, float* %max, align 4, !tbaa !10
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 1
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx11)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !10
  %call14 = call float @_Z6btFabsf(float %14)
  store float %call14, float* %v, align 4, !tbaa !10
  %15 = load float, float* %v, align 4, !tbaa !10
  %16 = load float, float* %max, align 4, !tbaa !10
  %cmp15 = fcmp ogt float %15, %16
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.end
  store i32 1, i32* %p, align 4, !tbaa !31
  store i32 2, i32* %q, align 4, !tbaa !31
  store i32 0, i32* %r, align 4, !tbaa !31
  %17 = load float, float* %v, align 4, !tbaa !10
  store float %17, float* %max, align 4, !tbaa !10
  br label %if.end17

if.end17:                                         ; preds = %if.then16, %if.end
  %18 = bitcast float* %t to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #8
  %19 = load float, float* %threshold.addr, align 4, !tbaa !10
  %m_el18 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx19 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el18, i32 0, i32 0
  %call20 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx19)
  %arrayidx21 = getelementptr inbounds float, float* %call20, i32 0
  %20 = load float, float* %arrayidx21, align 4, !tbaa !10
  %call22 = call float @_Z6btFabsf(float %20)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 1
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx24)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 1
  %21 = load float, float* %arrayidx26, align 4, !tbaa !10
  %call27 = call float @_Z6btFabsf(float %21)
  %add = fadd float %call22, %call27
  %m_el28 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx29 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el28, i32 0, i32 2
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx29)
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 2
  %22 = load float, float* %arrayidx31, align 4, !tbaa !10
  %call32 = call float @_Z6btFabsf(float %22)
  %add33 = fadd float %add, %call32
  %mul = fmul float %19, %add33
  store float %mul, float* %t, align 4, !tbaa !10
  %23 = load float, float* %max, align 4, !tbaa !10
  %24 = load float, float* %t, align 4, !tbaa !10
  %cmp34 = fcmp ole float %23, %24
  br i1 %cmp34, label %if.then35, label %if.end40

if.then35:                                        ; preds = %if.end17
  %25 = load float, float* %max, align 4, !tbaa !10
  %26 = load float, float* %t, align 4, !tbaa !10
  %mul36 = fmul float 0x3E80000000000000, %26
  %cmp37 = fcmp ole float %25, %mul36
  br i1 %cmp37, label %if.then38, label %if.end39

if.then38:                                        ; preds = %if.then35
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end39:                                         ; preds = %if.then35
  store i32 1, i32* %step, align 4, !tbaa !31
  br label %if.end40

if.end40:                                         ; preds = %if.end39, %if.end17
  %27 = bitcast float* %mpq to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #8
  %m_el41 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %28 = load i32, i32* %p, align 4, !tbaa !31
  %arrayidx42 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el41, i32 0, i32 %28
  %call43 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx42)
  %29 = load i32, i32* %q, align 4, !tbaa !31
  %arrayidx44 = getelementptr inbounds float, float* %call43, i32 %29
  %30 = load float, float* %arrayidx44, align 4, !tbaa !10
  store float %30, float* %mpq, align 4, !tbaa !10
  %31 = bitcast float* %theta to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #8
  %m_el45 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %32 = load i32, i32* %q, align 4, !tbaa !31
  %arrayidx46 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el45, i32 0, i32 %32
  %call47 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx46)
  %33 = load i32, i32* %q, align 4, !tbaa !31
  %arrayidx48 = getelementptr inbounds float, float* %call47, i32 %33
  %34 = load float, float* %arrayidx48, align 4, !tbaa !10
  %m_el49 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %35 = load i32, i32* %p, align 4, !tbaa !31
  %arrayidx50 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el49, i32 0, i32 %35
  %call51 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx50)
  %36 = load i32, i32* %p, align 4, !tbaa !31
  %arrayidx52 = getelementptr inbounds float, float* %call51, i32 %36
  %37 = load float, float* %arrayidx52, align 4, !tbaa !10
  %sub = fsub float %34, %37
  %38 = load float, float* %mpq, align 4, !tbaa !10
  %mul53 = fmul float 2.000000e+00, %38
  %div = fdiv float %sub, %mul53
  store float %div, float* %theta, align 4, !tbaa !10
  %39 = bitcast float* %theta2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #8
  %40 = load float, float* %theta, align 4, !tbaa !10
  %41 = load float, float* %theta, align 4, !tbaa !10
  %mul54 = fmul float %40, %41
  store float %mul54, float* %theta2, align 4, !tbaa !10
  %42 = bitcast float* %cos to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #8
  %43 = bitcast float* %sin to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #8
  %44 = load float, float* %theta2, align 4, !tbaa !10
  %45 = load float, float* %theta2, align 4, !tbaa !10
  %mul55 = fmul float %44, %45
  %cmp56 = fcmp olt float %mul55, 0x4194000000000000
  br i1 %cmp56, label %if.then57, label %if.else

if.then57:                                        ; preds = %if.end40
  %46 = load float, float* %theta, align 4, !tbaa !10
  %cmp58 = fcmp oge float %46, 0.000000e+00
  br i1 %cmp58, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then57
  %47 = load float, float* %theta, align 4, !tbaa !10
  %48 = load float, float* %theta2, align 4, !tbaa !10
  %add59 = fadd float 1.000000e+00, %48
  %call60 = call float @_Z6btSqrtf(float %add59)
  %add61 = fadd float %47, %call60
  %div62 = fdiv float 1.000000e+00, %add61
  br label %cond.end

cond.false:                                       ; preds = %if.then57
  %49 = load float, float* %theta, align 4, !tbaa !10
  %50 = load float, float* %theta2, align 4, !tbaa !10
  %add63 = fadd float 1.000000e+00, %50
  %call64 = call float @_Z6btSqrtf(float %add63)
  %sub65 = fsub float %49, %call64
  %div66 = fdiv float 1.000000e+00, %sub65
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %div62, %cond.true ], [ %div66, %cond.false ]
  store float %cond, float* %t, align 4, !tbaa !10
  %51 = load float, float* %t, align 4, !tbaa !10
  %52 = load float, float* %t, align 4, !tbaa !10
  %mul67 = fmul float %51, %52
  %add68 = fadd float 1.000000e+00, %mul67
  %call69 = call float @_Z6btSqrtf(float %add68)
  %div70 = fdiv float 1.000000e+00, %call69
  store float %div70, float* %cos, align 4, !tbaa !10
  %53 = load float, float* %cos, align 4, !tbaa !10
  %54 = load float, float* %t, align 4, !tbaa !10
  %mul71 = fmul float %53, %54
  store float %mul71, float* %sin, align 4, !tbaa !10
  br label %if.end80

if.else:                                          ; preds = %if.end40
  %55 = load float, float* %theta, align 4, !tbaa !10
  %56 = load float, float* %theta2, align 4, !tbaa !10
  %div72 = fdiv float 5.000000e-01, %56
  %add73 = fadd float 2.000000e+00, %div72
  %mul74 = fmul float %55, %add73
  %div75 = fdiv float 1.000000e+00, %mul74
  store float %div75, float* %t, align 4, !tbaa !10
  %57 = load float, float* %t, align 4, !tbaa !10
  %mul76 = fmul float 5.000000e-01, %57
  %58 = load float, float* %t, align 4, !tbaa !10
  %mul77 = fmul float %mul76, %58
  %sub78 = fsub float 1.000000e+00, %mul77
  store float %sub78, float* %cos, align 4, !tbaa !10
  %59 = load float, float* %cos, align 4, !tbaa !10
  %60 = load float, float* %t, align 4, !tbaa !10
  %mul79 = fmul float %59, %60
  store float %mul79, float* %sin, align 4, !tbaa !10
  br label %if.end80

if.end80:                                         ; preds = %if.else, %cond.end
  %m_el81 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %61 = load i32, i32* %q, align 4, !tbaa !31
  %arrayidx82 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el81, i32 0, i32 %61
  %call83 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx82)
  %62 = load i32, i32* %p, align 4, !tbaa !31
  %arrayidx84 = getelementptr inbounds float, float* %call83, i32 %62
  store float 0.000000e+00, float* %arrayidx84, align 4, !tbaa !10
  %m_el85 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %63 = load i32, i32* %p, align 4, !tbaa !31
  %arrayidx86 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el85, i32 0, i32 %63
  %call87 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx86)
  %64 = load i32, i32* %q, align 4, !tbaa !31
  %arrayidx88 = getelementptr inbounds float, float* %call87, i32 %64
  store float 0.000000e+00, float* %arrayidx88, align 4, !tbaa !10
  %65 = load float, float* %t, align 4, !tbaa !10
  %66 = load float, float* %mpq, align 4, !tbaa !10
  %mul89 = fmul float %65, %66
  %m_el90 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %67 = load i32, i32* %p, align 4, !tbaa !31
  %arrayidx91 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el90, i32 0, i32 %67
  %call92 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx91)
  %68 = load i32, i32* %p, align 4, !tbaa !31
  %arrayidx93 = getelementptr inbounds float, float* %call92, i32 %68
  %69 = load float, float* %arrayidx93, align 4, !tbaa !10
  %sub94 = fsub float %69, %mul89
  store float %sub94, float* %arrayidx93, align 4, !tbaa !10
  %70 = load float, float* %t, align 4, !tbaa !10
  %71 = load float, float* %mpq, align 4, !tbaa !10
  %mul95 = fmul float %70, %71
  %m_el96 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %72 = load i32, i32* %q, align 4, !tbaa !31
  %arrayidx97 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el96, i32 0, i32 %72
  %call98 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx97)
  %73 = load i32, i32* %q, align 4, !tbaa !31
  %arrayidx99 = getelementptr inbounds float, float* %call98, i32 %73
  %74 = load float, float* %arrayidx99, align 4, !tbaa !10
  %add100 = fadd float %74, %mul95
  store float %add100, float* %arrayidx99, align 4, !tbaa !10
  %75 = bitcast float* %mrp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #8
  %m_el101 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %76 = load i32, i32* %r, align 4, !tbaa !31
  %arrayidx102 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el101, i32 0, i32 %76
  %call103 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx102)
  %77 = load i32, i32* %p, align 4, !tbaa !31
  %arrayidx104 = getelementptr inbounds float, float* %call103, i32 %77
  %78 = load float, float* %arrayidx104, align 4, !tbaa !10
  store float %78, float* %mrp, align 4, !tbaa !10
  %79 = bitcast float* %mrq to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #8
  %m_el105 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %80 = load i32, i32* %r, align 4, !tbaa !31
  %arrayidx106 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el105, i32 0, i32 %80
  %call107 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx106)
  %81 = load i32, i32* %q, align 4, !tbaa !31
  %arrayidx108 = getelementptr inbounds float, float* %call107, i32 %81
  %82 = load float, float* %arrayidx108, align 4, !tbaa !10
  store float %82, float* %mrq, align 4, !tbaa !10
  %83 = load float, float* %cos, align 4, !tbaa !10
  %84 = load float, float* %mrp, align 4, !tbaa !10
  %mul109 = fmul float %83, %84
  %85 = load float, float* %sin, align 4, !tbaa !10
  %86 = load float, float* %mrq, align 4, !tbaa !10
  %mul110 = fmul float %85, %86
  %sub111 = fsub float %mul109, %mul110
  %m_el112 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %87 = load i32, i32* %p, align 4, !tbaa !31
  %arrayidx113 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el112, i32 0, i32 %87
  %call114 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx113)
  %88 = load i32, i32* %r, align 4, !tbaa !31
  %arrayidx115 = getelementptr inbounds float, float* %call114, i32 %88
  store float %sub111, float* %arrayidx115, align 4, !tbaa !10
  %m_el116 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %89 = load i32, i32* %r, align 4, !tbaa !31
  %arrayidx117 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el116, i32 0, i32 %89
  %call118 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx117)
  %90 = load i32, i32* %p, align 4, !tbaa !31
  %arrayidx119 = getelementptr inbounds float, float* %call118, i32 %90
  store float %sub111, float* %arrayidx119, align 4, !tbaa !10
  %91 = load float, float* %cos, align 4, !tbaa !10
  %92 = load float, float* %mrq, align 4, !tbaa !10
  %mul120 = fmul float %91, %92
  %93 = load float, float* %sin, align 4, !tbaa !10
  %94 = load float, float* %mrp, align 4, !tbaa !10
  %mul121 = fmul float %93, %94
  %add122 = fadd float %mul120, %mul121
  %m_el123 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %95 = load i32, i32* %q, align 4, !tbaa !31
  %arrayidx124 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el123, i32 0, i32 %95
  %call125 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx124)
  %96 = load i32, i32* %r, align 4, !tbaa !31
  %arrayidx126 = getelementptr inbounds float, float* %call125, i32 %96
  store float %add122, float* %arrayidx126, align 4, !tbaa !10
  %m_el127 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %97 = load i32, i32* %r, align 4, !tbaa !31
  %arrayidx128 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el127, i32 0, i32 %97
  %call129 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx128)
  %98 = load i32, i32* %q, align 4, !tbaa !31
  %arrayidx130 = getelementptr inbounds float, float* %call129, i32 %98
  store float %add122, float* %arrayidx130, align 4, !tbaa !10
  %99 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %99) #8
  store i32 0, i32* %i, align 4, !tbaa !31
  br label %for.cond131

for.cond131:                                      ; preds = %for.inc, %if.end80
  %100 = load i32, i32* %i, align 4, !tbaa !31
  %cmp132 = icmp slt i32 %100, 3
  br i1 %cmp132, label %for.body134, label %for.cond.cleanup133

for.cond.cleanup133:                              ; preds = %for.cond131
  store i32 5, i32* %cleanup.dest.slot, align 4
  %101 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #8
  br label %for.end

for.body134:                                      ; preds = %for.cond131
  %102 = bitcast %class.btVector3** %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %102) #8
  %103 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot.addr, align 4, !tbaa !2
  %104 = load i32, i32* %i, align 4, !tbaa !31
  %call135 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %103, i32 %104)
  store %class.btVector3* %call135, %class.btVector3** %row, align 4, !tbaa !2
  %105 = load %class.btVector3*, %class.btVector3** %row, align 4, !tbaa !2
  %call136 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %105)
  %106 = load i32, i32* %p, align 4, !tbaa !31
  %arrayidx137 = getelementptr inbounds float, float* %call136, i32 %106
  %107 = load float, float* %arrayidx137, align 4, !tbaa !10
  store float %107, float* %mrp, align 4, !tbaa !10
  %108 = load %class.btVector3*, %class.btVector3** %row, align 4, !tbaa !2
  %call138 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %108)
  %109 = load i32, i32* %q, align 4, !tbaa !31
  %arrayidx139 = getelementptr inbounds float, float* %call138, i32 %109
  %110 = load float, float* %arrayidx139, align 4, !tbaa !10
  store float %110, float* %mrq, align 4, !tbaa !10
  %111 = load float, float* %cos, align 4, !tbaa !10
  %112 = load float, float* %mrp, align 4, !tbaa !10
  %mul140 = fmul float %111, %112
  %113 = load float, float* %sin, align 4, !tbaa !10
  %114 = load float, float* %mrq, align 4, !tbaa !10
  %mul141 = fmul float %113, %114
  %sub142 = fsub float %mul140, %mul141
  %115 = load %class.btVector3*, %class.btVector3** %row, align 4, !tbaa !2
  %call143 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %115)
  %116 = load i32, i32* %p, align 4, !tbaa !31
  %arrayidx144 = getelementptr inbounds float, float* %call143, i32 %116
  store float %sub142, float* %arrayidx144, align 4, !tbaa !10
  %117 = load float, float* %cos, align 4, !tbaa !10
  %118 = load float, float* %mrq, align 4, !tbaa !10
  %mul145 = fmul float %117, %118
  %119 = load float, float* %sin, align 4, !tbaa !10
  %120 = load float, float* %mrp, align 4, !tbaa !10
  %mul146 = fmul float %119, %120
  %add147 = fadd float %mul145, %mul146
  %121 = load %class.btVector3*, %class.btVector3** %row, align 4, !tbaa !2
  %call148 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %121)
  %122 = load i32, i32* %q, align 4, !tbaa !31
  %arrayidx149 = getelementptr inbounds float, float* %call148, i32 %122
  store float %add147, float* %arrayidx149, align 4, !tbaa !10
  %123 = bitcast %class.btVector3** %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body134
  %124 = load i32, i32* %i, align 4, !tbaa !31
  %inc = add nsw i32 %124, 1
  store i32 %inc, i32* %i, align 4, !tbaa !31
  br label %for.cond131

for.end:                                          ; preds = %for.cond.cleanup133
  %125 = bitcast float* %mrq to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #8
  %126 = bitcast float* %mrp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #8
  %127 = bitcast float* %sin to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #8
  %128 = bitcast float* %cos to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #8
  %129 = bitcast float* %theta2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #8
  %130 = bitcast float* %theta to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #8
  %131 = bitcast float* %mpq to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %131) #8
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then38
  %132 = bitcast float* %t to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #8
  %133 = bitcast float* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %133) #8
  %134 = bitcast float* %max to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %134) #8
  %135 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %135) #8
  %136 = bitcast i32* %q to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %136) #8
  %137 = bitcast i32* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %137) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup156 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc155

for.inc155:                                       ; preds = %cleanup.cont
  %138 = load i32, i32* %step, align 4, !tbaa !31
  %dec = add nsw i32 %138, -1
  store i32 %dec, i32* %step, align 4, !tbaa !31
  br label %for.cond

cleanup156:                                       ; preds = %cleanup, %for.cond.cleanup
  %139 = bitcast i32* %step to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #8
  %cleanup.dest157 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest157, label %unreachable [
    i32 2, label %for.end158
    i32 1, label %for.end158
  ]

for.end158:                                       ; preds = %cleanup156, %cleanup156
  ret void

unreachable:                                      ; preds = %cleanup156
  unreachable
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

define hidden void @_ZN15btCompoundShape15setLocalScalingERK9btVector3(%class.btCompoundShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %scaling) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %scaling.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  %childTrans = alloca %class.btTransform, align 4
  %childScale = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %ref.tmp14 = alloca %class.btVector3, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %scaling, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %i, align 4, !tbaa !31
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !31
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray* %m_children)
  %cmp = icmp slt i32 %1, %call
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %class.btTransform* %childTrans to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %3) #8
  %4 = load i32, i32* %i, align 4, !tbaa !31
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN15btCompoundShape17getChildTransformEi(%class.btCompoundShape* %this1, i32 %4)
  %call3 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %childTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %call2)
  %5 = bitcast %class.btVector3* %childScale to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #8
  %m_children4 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %6 = load i32, i32* %i, align 4, !tbaa !31
  %call5 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children4, i32 %6)
  %m_childShape = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call5, i32 0, i32 1
  %7 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape, align 4, !tbaa !28
  %8 = bitcast %class.btCollisionShape* %7 to %class.btVector3* (%class.btCollisionShape*)***
  %vtable = load %class.btVector3* (%class.btCollisionShape*)**, %class.btVector3* (%class.btCollisionShape*)*** %8, align 4, !tbaa !8
  %vfn = getelementptr inbounds %class.btVector3* (%class.btCollisionShape*)*, %class.btVector3* (%class.btCollisionShape*)** %vtable, i64 7
  %9 = load %class.btVector3* (%class.btCollisionShape*)*, %class.btVector3* (%class.btCollisionShape*)** %vfn, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* %9(%class.btCollisionShape* %7)
  %10 = bitcast %class.btVector3* %childScale to i8*
  %11 = bitcast %class.btVector3* %call6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !32
  %12 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %12) #8
  %13 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #8
  %14 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp7, %class.btVector3* nonnull align 4 dereferenceable(16) %childScale, %class.btVector3* nonnull align 4 dereferenceable(16) %14)
  %m_localScaling = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 7
  call void @_ZdvRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp7, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling)
  %15 = bitcast %class.btVector3* %childScale to i8*
  %16 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 16, i1 false), !tbaa.struct !32
  %17 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %17) #8
  %18 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #8
  %m_children8 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %19 = load i32, i32* %i, align 4, !tbaa !31
  %call9 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children8, i32 %19)
  %m_childShape10 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call9, i32 0, i32 1
  %20 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape10, align 4, !tbaa !28
  %21 = bitcast %class.btCollisionShape* %20 to void (%class.btCollisionShape*, %class.btVector3*)***
  %vtable11 = load void (%class.btCollisionShape*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btVector3*)*** %21, align 4, !tbaa !8
  %vfn12 = getelementptr inbounds void (%class.btCollisionShape*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btVector3*)** %vtable11, i64 6
  %22 = load void (%class.btCollisionShape*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btVector3*)** %vfn12, align 4
  call void %22(%class.btCollisionShape* %20, %class.btVector3* nonnull align 4 dereferenceable(16) %childScale)
  %23 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %23) #8
  %24 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %24) #8
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %childTrans)
  %25 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp14, %class.btVector3* nonnull align 4 dereferenceable(16) %call15, %class.btVector3* nonnull align 4 dereferenceable(16) %25)
  %m_localScaling16 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 7
  call void @_ZdvRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp13, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp14, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling16)
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %childTrans, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp13)
  %26 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %26) #8
  %27 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %27) #8
  %28 = load i32, i32* %i, align 4, !tbaa !31
  call void @_ZN15btCompoundShape20updateChildTransformEiRK11btTransformb(%class.btCompoundShape* %this1, i32 %28, %class.btTransform* nonnull align 4 dereferenceable(64) %childTrans, i1 zeroext false)
  %29 = bitcast %class.btVector3* %childScale to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #8
  %30 = bitcast %class.btTransform* %childTrans to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %30) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %31 = load i32, i32* %i, align 4, !tbaa !31
  %inc = add nsw i32 %31, 1
  store i32 %inc, i32* %i, align 4, !tbaa !31
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %32 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  %m_localScaling17 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 7
  %33 = bitcast %class.btVector3* %m_localScaling17 to i8*
  %34 = bitcast %class.btVector3* %32 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %33, i8* align 4 %34, i32 16, i1 false), !tbaa.struct !32
  %35 = bitcast %class.btCompoundShape* %this1 to void (%class.btCompoundShape*)***
  %vtable18 = load void (%class.btCompoundShape*)**, void (%class.btCompoundShape*)*** %35, align 4, !tbaa !8
  %vfn19 = getelementptr inbounds void (%class.btCompoundShape*)*, void (%class.btCompoundShape*)** %vtable18, i64 17
  %36 = load void (%class.btCompoundShape*)*, void (%class.btCompoundShape*)** %vfn19, align 4
  call void %36(%class.btCompoundShape* %this1)
  ret void
}

define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN15btCompoundShape17getChildTransformEi(%class.btCompoundShape* %this, i32 %index) #0 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %index.addr = alloca i32, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !31
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %index.addr, align 4, !tbaa !31
  %call = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children, i32 %0)
  %m_transform = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call, i32 0, i32 0
  ret %class.btTransform* %m_transform
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !32
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZdvRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #6 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !10
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !10
  %div = fdiv float %2, %4
  store float %div, float* %ref.tmp, align 4, !tbaa !10
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !10
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !10
  %div8 = fdiv float %7, %9
  store float %div8, float* %ref.tmp3, align 4, !tbaa !10
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !10
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !10
  %div14 = fdiv float %12, %14
  store float %div14, float* %ref.tmp9, align 4, !tbaa !10
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #6 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !10
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !10
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !10
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !10
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !10
  %mul8 = fmul float %7, %9
  store float %mul8, float* %ref.tmp3, align 4, !tbaa !10
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !10
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !10
  %mul14 = fmul float %12, %14
  store float %mul14, float* %ref.tmp9, align 4, !tbaa !10
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

define hidden void @_ZN15btCompoundShape26createAabbTreeFromChildrenEv(%class.btCompoundShape* %this) #0 {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %mem = alloca i8*, align 4
  %index = alloca i32, align 4
  %child = alloca %struct.btCompoundShapeChild*, align 4
  %localAabbMin = alloca %class.btVector3, align 4
  %localAabbMax = alloca %class.btVector3, align 4
  %bounds = alloca %struct.btDbvtAabbMm, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_dynamicAabbTree = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  %0 = load %struct.btDbvt*, %struct.btDbvt** %m_dynamicAabbTree, align 4, !tbaa !12
  %tobool = icmp ne %struct.btDbvt* %0, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %1 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 60, i32 16)
  store i8* %call, i8** %mem, align 4, !tbaa !2
  %2 = load i8*, i8** %mem, align 4, !tbaa !2
  %3 = bitcast i8* %2 to %struct.btDbvt*
  %call2 = call %struct.btDbvt* @_ZN6btDbvtC1Ev(%struct.btDbvt* %3)
  %m_dynamicAabbTree3 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  store %struct.btDbvt* %3, %struct.btDbvt** %m_dynamicAabbTree3, align 4, !tbaa !12
  %4 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  store i32 0, i32* %index, align 4, !tbaa !31
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %index, align 4, !tbaa !31
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray* %m_children)
  %cmp = icmp slt i32 %5, %call4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %6 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %7 = bitcast %struct.btCompoundShapeChild** %child to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %m_children5 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %8 = load i32, i32* %index, align 4, !tbaa !31
  %call6 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children5, i32 %8)
  store %struct.btCompoundShapeChild* %call6, %struct.btCompoundShapeChild** %child, align 4, !tbaa !2
  %9 = bitcast %class.btVector3* %localAabbMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #8
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %localAabbMin)
  %10 = bitcast %class.btVector3* %localAabbMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #8
  %call8 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %localAabbMax)
  %11 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %child, align 4, !tbaa !2
  %m_childShape = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %11, i32 0, i32 1
  %12 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape, align 4, !tbaa !28
  %13 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %child, align 4, !tbaa !2
  %m_transform = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %13, i32 0, i32 0
  %14 = bitcast %class.btCollisionShape* %12 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %14, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %15 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %15(%class.btCollisionShape* %12, %class.btTransform* nonnull align 4 dereferenceable(64) %m_transform, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax)
  %16 = bitcast %struct.btDbvtAabbMm* %bounds to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %16) #8
  call void @_ZN12btDbvtAabbMm6FromMMERK9btVector3S2_(%struct.btDbvtAabbMm* sret align 4 %bounds, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax)
  %m_dynamicAabbTree9 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  %17 = load %struct.btDbvt*, %struct.btDbvt** %m_dynamicAabbTree9, align 4, !tbaa !12
  %18 = load i32, i32* %index, align 4, !tbaa !31
  %19 = inttoptr i32 %18 to i8*
  %call10 = call %struct.btDbvtNode* @_ZN6btDbvt6insertERK12btDbvtAabbMmPv(%struct.btDbvt* %17, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %bounds, i8* %19)
  %20 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %child, align 4, !tbaa !2
  %m_node = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %20, i32 0, i32 4
  store %struct.btDbvtNode* %call10, %struct.btDbvtNode** %m_node, align 4, !tbaa !24
  %21 = bitcast %struct.btDbvtAabbMm* %bounds to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %21) #8
  %22 = bitcast %class.btVector3* %localAabbMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %22) #8
  %23 = bitcast %class.btVector3* %localAabbMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %23) #8
  %24 = bitcast %struct.btCompoundShapeChild** %child to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %25 = load i32, i32* %index, align 4, !tbaa !31
  %inc = add nsw i32 %25, 1
  store i32 %inc, i32* %index, align 4, !tbaa !31
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %26 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #8
  br label %if.end

if.end:                                           ; preds = %for.end, %entry
  ret void
}

define hidden i8* @_ZNK15btCompoundShape9serializeEPvP12btSerializer(%class.btCompoundShape* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %shapeData = alloca %struct.btCompoundShapeData*, align 4
  %chunk = alloca %class.btChunk*, align 4
  %memPtr = alloca %struct.btCompoundShapeChildData*, align 4
  %i = alloca i32, align 4
  %chunk29 = alloca %class.btChunk*, align 4
  %structType = alloca i8*, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %0 = bitcast %struct.btCompoundShapeData** %shapeData to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i8*, i8** %dataBuffer.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.btCompoundShapeData*
  store %struct.btCompoundShapeData* %2, %struct.btCompoundShapeData** %shapeData, align 4, !tbaa !2
  %3 = bitcast %class.btCompoundShape* %this1 to %class.btCollisionShape*
  %4 = load %struct.btCompoundShapeData*, %struct.btCompoundShapeData** %shapeData, align 4, !tbaa !2
  %m_collisionShapeData = getelementptr inbounds %struct.btCompoundShapeData, %struct.btCompoundShapeData* %4, i32 0, i32 0
  %5 = bitcast %struct.btCollisionShapeData* %m_collisionShapeData to i8*
  %6 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %call = call i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape* %3, i8* %5, %class.btSerializer* %6)
  %m_collisionMargin = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 6
  %7 = load float, float* %m_collisionMargin, align 4, !tbaa !19
  %8 = load %struct.btCompoundShapeData*, %struct.btCompoundShapeData** %shapeData, align 4, !tbaa !2
  %m_collisionMargin2 = getelementptr inbounds %struct.btCompoundShapeData, %struct.btCompoundShapeData* %8, i32 0, i32 3
  store float %7, float* %m_collisionMargin2, align 4, !tbaa !36
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray* %m_children)
  %9 = load %struct.btCompoundShapeData*, %struct.btCompoundShapeData** %shapeData, align 4, !tbaa !2
  %m_numChildShapes = getelementptr inbounds %struct.btCompoundShapeData, %struct.btCompoundShapeData* %9, i32 0, i32 2
  store i32 %call3, i32* %m_numChildShapes, align 4, !tbaa !39
  %10 = load %struct.btCompoundShapeData*, %struct.btCompoundShapeData** %shapeData, align 4, !tbaa !2
  %m_childShapePtr = getelementptr inbounds %struct.btCompoundShapeData, %struct.btCompoundShapeData* %10, i32 0, i32 1
  store %struct.btCompoundShapeChildData* null, %struct.btCompoundShapeChildData** %m_childShapePtr, align 4, !tbaa !40
  %11 = load %struct.btCompoundShapeData*, %struct.btCompoundShapeData** %shapeData, align 4, !tbaa !2
  %m_numChildShapes4 = getelementptr inbounds %struct.btCompoundShapeData, %struct.btCompoundShapeData* %11, i32 0, i32 2
  %12 = load i32, i32* %m_numChildShapes4, align 4, !tbaa !39
  %tobool = icmp ne i32 %12, 0
  br i1 %tobool, label %if.then, label %if.end60

if.then:                                          ; preds = %entry
  %13 = bitcast %class.btChunk** %chunk to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %15 = load %struct.btCompoundShapeData*, %struct.btCompoundShapeData** %shapeData, align 4, !tbaa !2
  %m_numChildShapes5 = getelementptr inbounds %struct.btCompoundShapeData, %struct.btCompoundShapeData* %15, i32 0, i32 2
  %16 = load i32, i32* %m_numChildShapes5, align 4, !tbaa !39
  %17 = bitcast %class.btSerializer* %14 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %17, align 4, !tbaa !8
  %vfn = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable, i64 4
  %18 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn, align 4
  %call6 = call %class.btChunk* %18(%class.btSerializer* %14, i32 76, i32 %16)
  store %class.btChunk* %call6, %class.btChunk** %chunk, align 4, !tbaa !2
  %19 = bitcast %struct.btCompoundShapeChildData** %memPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  %20 = load %class.btChunk*, %class.btChunk** %chunk, align 4, !tbaa !2
  %m_oldPtr = getelementptr inbounds %class.btChunk, %class.btChunk* %20, i32 0, i32 2
  %21 = load i8*, i8** %m_oldPtr, align 4, !tbaa !41
  %22 = bitcast i8* %21 to %struct.btCompoundShapeChildData*
  store %struct.btCompoundShapeChildData* %22, %struct.btCompoundShapeChildData** %memPtr, align 4, !tbaa !2
  %23 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %24 = load %struct.btCompoundShapeChildData*, %struct.btCompoundShapeChildData** %memPtr, align 4, !tbaa !2
  %25 = bitcast %struct.btCompoundShapeChildData* %24 to i8*
  %26 = bitcast %class.btSerializer* %23 to i8* (%class.btSerializer*, i8*)***
  %vtable7 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %26, align 4, !tbaa !8
  %vfn8 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable7, i64 7
  %27 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn8, align 4
  %call9 = call i8* %27(%class.btSerializer* %23, i8* %25)
  %28 = bitcast i8* %call9 to %struct.btCompoundShapeChildData*
  %29 = load %struct.btCompoundShapeData*, %struct.btCompoundShapeData** %shapeData, align 4, !tbaa !2
  %m_childShapePtr10 = getelementptr inbounds %struct.btCompoundShapeData, %struct.btCompoundShapeData* %29, i32 0, i32 1
  store %struct.btCompoundShapeChildData* %28, %struct.btCompoundShapeChildData** %m_childShapePtr10, align 4, !tbaa !40
  %30 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #8
  store i32 0, i32* %i, align 4, !tbaa !31
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %31 = load i32, i32* %i, align 4, !tbaa !31
  %32 = load %struct.btCompoundShapeData*, %struct.btCompoundShapeData** %shapeData, align 4, !tbaa !2
  %m_numChildShapes11 = getelementptr inbounds %struct.btCompoundShapeData, %struct.btCompoundShapeData* %32, i32 0, i32 2
  %33 = load i32, i32* %m_numChildShapes11, align 4, !tbaa !39
  %cmp = icmp slt i32 %31, %33
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %34 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_children12 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %35 = load i32, i32* %i, align 4, !tbaa !31
  %call13 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children12, i32 %35)
  %m_childMargin = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call13, i32 0, i32 3
  %36 = load float, float* %m_childMargin, align 4, !tbaa !30
  %37 = load %struct.btCompoundShapeChildData*, %struct.btCompoundShapeChildData** %memPtr, align 4, !tbaa !2
  %m_childMargin14 = getelementptr inbounds %struct.btCompoundShapeChildData, %struct.btCompoundShapeChildData* %37, i32 0, i32 3
  store float %36, float* %m_childMargin14, align 4, !tbaa !43
  %38 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %m_children15 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %39 = load i32, i32* %i, align 4, !tbaa !31
  %call16 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children15, i32 %39)
  %m_childShape = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call16, i32 0, i32 1
  %40 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape, align 4, !tbaa !28
  %41 = bitcast %class.btCollisionShape* %40 to i8*
  %42 = bitcast %class.btSerializer* %38 to i8* (%class.btSerializer*, i8*)***
  %vtable17 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %42, align 4, !tbaa !8
  %vfn18 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable17, i64 7
  %43 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn18, align 4
  %call19 = call i8* %43(%class.btSerializer* %38, i8* %41)
  %44 = bitcast i8* %call19 to %struct.btCollisionShapeData*
  %45 = load %struct.btCompoundShapeChildData*, %struct.btCompoundShapeChildData** %memPtr, align 4, !tbaa !2
  %m_childShape20 = getelementptr inbounds %struct.btCompoundShapeChildData, %struct.btCompoundShapeChildData* %45, i32 0, i32 1
  store %struct.btCollisionShapeData* %44, %struct.btCollisionShapeData** %m_childShape20, align 4, !tbaa !48
  %46 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %m_children21 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %47 = load i32, i32* %i, align 4, !tbaa !31
  %call22 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children21, i32 %47)
  %m_childShape23 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call22, i32 0, i32 1
  %48 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape23, align 4, !tbaa !28
  %49 = bitcast %class.btCollisionShape* %48 to i8*
  %50 = bitcast %class.btSerializer* %46 to i8* (%class.btSerializer*, i8*)***
  %vtable24 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %50, align 4, !tbaa !8
  %vfn25 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable24, i64 6
  %51 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn25, align 4
  %call26 = call i8* %51(%class.btSerializer* %46, i8* %49)
  %tobool27 = icmp ne i8* %call26, null
  br i1 %tobool27, label %if.end, label %if.then28

if.then28:                                        ; preds = %for.body
  %52 = bitcast %class.btChunk** %chunk29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #8
  %53 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %m_children30 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %54 = load i32, i32* %i, align 4, !tbaa !31
  %call31 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children30, i32 %54)
  %m_childShape32 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call31, i32 0, i32 1
  %55 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape32, align 4, !tbaa !28
  %56 = bitcast %class.btCollisionShape* %55 to i32 (%class.btCollisionShape*)***
  %vtable33 = load i32 (%class.btCollisionShape*)**, i32 (%class.btCollisionShape*)*** %56, align 4, !tbaa !8
  %vfn34 = getelementptr inbounds i32 (%class.btCollisionShape*)*, i32 (%class.btCollisionShape*)** %vtable33, i64 13
  %57 = load i32 (%class.btCollisionShape*)*, i32 (%class.btCollisionShape*)** %vfn34, align 4
  %call35 = call i32 %57(%class.btCollisionShape* %55)
  %58 = bitcast %class.btSerializer* %53 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable36 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %58, align 4, !tbaa !8
  %vfn37 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable36, i64 4
  %59 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn37, align 4
  %call38 = call %class.btChunk* %59(%class.btSerializer* %53, i32 %call35, i32 1)
  store %class.btChunk* %call38, %class.btChunk** %chunk29, align 4, !tbaa !2
  %60 = bitcast i8** %structType to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #8
  %m_children39 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %61 = load i32, i32* %i, align 4, !tbaa !31
  %call40 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children39, i32 %61)
  %m_childShape41 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call40, i32 0, i32 1
  %62 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape41, align 4, !tbaa !28
  %63 = load %class.btChunk*, %class.btChunk** %chunk29, align 4, !tbaa !2
  %m_oldPtr42 = getelementptr inbounds %class.btChunk, %class.btChunk* %63, i32 0, i32 2
  %64 = load i8*, i8** %m_oldPtr42, align 4, !tbaa !41
  %65 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %66 = bitcast %class.btCollisionShape* %62 to i8* (%class.btCollisionShape*, i8*, %class.btSerializer*)***
  %vtable43 = load i8* (%class.btCollisionShape*, i8*, %class.btSerializer*)**, i8* (%class.btCollisionShape*, i8*, %class.btSerializer*)*** %66, align 4, !tbaa !8
  %vfn44 = getelementptr inbounds i8* (%class.btCollisionShape*, i8*, %class.btSerializer*)*, i8* (%class.btCollisionShape*, i8*, %class.btSerializer*)** %vtable43, i64 14
  %67 = load i8* (%class.btCollisionShape*, i8*, %class.btSerializer*)*, i8* (%class.btCollisionShape*, i8*, %class.btSerializer*)** %vfn44, align 4
  %call45 = call i8* %67(%class.btCollisionShape* %62, i8* %64, %class.btSerializer* %65)
  store i8* %call45, i8** %structType, align 4, !tbaa !2
  %68 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %69 = load %class.btChunk*, %class.btChunk** %chunk29, align 4, !tbaa !2
  %70 = load i8*, i8** %structType, align 4, !tbaa !2
  %m_children46 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %71 = load i32, i32* %i, align 4, !tbaa !31
  %call47 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children46, i32 %71)
  %m_childShape48 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call47, i32 0, i32 1
  %72 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape48, align 4, !tbaa !28
  %73 = bitcast %class.btCollisionShape* %72 to i8*
  %74 = bitcast %class.btSerializer* %68 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable49 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %74, align 4, !tbaa !8
  %vfn50 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable49, i64 5
  %75 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn50, align 4
  call void %75(%class.btSerializer* %68, %class.btChunk* %69, i8* %70, i32 1346455635, i8* %73)
  %76 = bitcast i8** %structType to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #8
  %77 = bitcast %class.btChunk** %chunk29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #8
  br label %if.end

if.end:                                           ; preds = %if.then28, %for.body
  %m_children51 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %78 = load i32, i32* %i, align 4, !tbaa !31
  %call52 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children51, i32 %78)
  %m_childShapeType = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call52, i32 0, i32 2
  %79 = load i32, i32* %m_childShapeType, align 4, !tbaa !29
  %80 = load %struct.btCompoundShapeChildData*, %struct.btCompoundShapeChildData** %memPtr, align 4, !tbaa !2
  %m_childShapeType53 = getelementptr inbounds %struct.btCompoundShapeChildData, %struct.btCompoundShapeChildData* %80, i32 0, i32 2
  store i32 %79, i32* %m_childShapeType53, align 4, !tbaa !49
  %m_children54 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %81 = load i32, i32* %i, align 4, !tbaa !31
  %call55 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children54, i32 %81)
  %m_transform = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call55, i32 0, i32 0
  %82 = load %struct.btCompoundShapeChildData*, %struct.btCompoundShapeChildData** %memPtr, align 4, !tbaa !2
  %m_transform56 = getelementptr inbounds %struct.btCompoundShapeChildData, %struct.btCompoundShapeChildData* %82, i32 0, i32 0
  call void @_ZNK11btTransform14serializeFloatER20btTransformFloatData(%class.btTransform* %m_transform, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %m_transform56)
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %83 = load i32, i32* %i, align 4, !tbaa !31
  %inc = add nsw i32 %83, 1
  store i32 %inc, i32* %i, align 4, !tbaa !31
  %84 = load %struct.btCompoundShapeChildData*, %struct.btCompoundShapeChildData** %memPtr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.btCompoundShapeChildData, %struct.btCompoundShapeChildData* %84, i32 1
  store %struct.btCompoundShapeChildData* %incdec.ptr, %struct.btCompoundShapeChildData** %memPtr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %85 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %86 = load %class.btChunk*, %class.btChunk** %chunk, align 4, !tbaa !2
  %87 = load %class.btChunk*, %class.btChunk** %chunk, align 4, !tbaa !2
  %m_oldPtr57 = getelementptr inbounds %class.btChunk, %class.btChunk* %87, i32 0, i32 2
  %88 = load i8*, i8** %m_oldPtr57, align 4, !tbaa !41
  %89 = bitcast %class.btSerializer* %85 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable58 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %89, align 4, !tbaa !8
  %vfn59 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable58, i64 5
  %90 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn59, align 4
  call void %90(%class.btSerializer* %85, %class.btChunk* %86, i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str, i32 0, i32 0), i32 1497453121, i8* %88)
  %91 = bitcast %struct.btCompoundShapeChildData** %memPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #8
  %92 = bitcast %class.btChunk** %chunk to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #8
  br label %if.end60

if.end60:                                         ; preds = %for.end, %entry
  %93 = bitcast %struct.btCompoundShapeData** %shapeData to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #8
  ret i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.1, i32 0, i32 0)
}

declare i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape*, i8*, %class.btSerializer*) unnamed_addr #4

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransform14serializeFloatER20btTransformFloatData(%class.btTransform* %this, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %dataOut) #6 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %dataOut.addr = alloca %struct.btTransformFloatData*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %struct.btTransformFloatData* %dataOut, %struct.btTransformFloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %struct.btTransformFloatData*, %struct.btTransformFloatData** %dataOut.addr, align 4, !tbaa !2
  %m_basis2 = getelementptr inbounds %struct.btTransformFloatData, %struct.btTransformFloatData* %0, i32 0, i32 0
  call void @_ZNK11btMatrix3x314serializeFloatER20btMatrix3x3FloatData(%class.btMatrix3x3* %m_basis, %struct.btMatrix3x3FloatData* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %struct.btTransformFloatData*, %struct.btTransformFloatData** %dataOut.addr, align 4, !tbaa !2
  %m_origin3 = getelementptr inbounds %struct.btTransformFloatData, %struct.btTransformFloatData* %1, i32 0, i32 1
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_origin, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_origin3)
  ret void
}

declare void @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf(%class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4)) unnamed_addr #4

declare float @_ZNK16btCollisionShape20getAngularMotionDiscEv(%class.btCollisionShape*) unnamed_addr #4

declare float @_ZNK16btCollisionShape27getContactBreakingThresholdEf(%class.btCollisionShape*, float) unnamed_addr #4

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btCompoundShape15getLocalScalingEv(%class.btCompoundShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_localScaling = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 7
  ret %class.btVector3* %m_localScaling
}

; Function Attrs: nounwind
define linkonce_odr hidden i8* @_ZNK15btCompoundShape7getNameEv(%class.btCompoundShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  ret i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.2, i32 0, i32 0)
}

define linkonce_odr hidden void @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btCollisionShape* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !10
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 1.000000e+00, float* %ref.tmp2, align 4, !tbaa !10
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 1.000000e+00, float* %ref.tmp3, align 4, !tbaa !10
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN15btCompoundShape9setMarginEf(%class.btCompoundShape* %this, float %margin) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %margin.addr = alloca float, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  store float %margin, float* %margin.addr, align 4, !tbaa !10
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %0 = load float, float* %margin.addr, align 4, !tbaa !10
  %m_collisionMargin = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 6
  store float %0, float* %m_collisionMargin, align 4, !tbaa !19
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK15btCompoundShape9getMarginEv(%class.btCompoundShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 6
  %0 = load float, float* %m_collisionMargin, align 4, !tbaa !19
  ret float %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK15btCompoundShape28calculateSerializeBufferSizeEv(%class.btCompoundShape* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  ret i32 24
}

declare void @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer(%class.btCollisionShape*, %class.btSerializer*) unnamed_addr #4

; Function Attrs: inlinehint
define linkonce_odr hidden %struct.btDbvtAabbMm* @_ZN12btDbvtAabbMmC2Ev(%struct.btDbvtAabbMm* returned %this) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %struct.btDbvtAabbMm*, align 4
  store %struct.btDbvtAabbMm* %this, %struct.btDbvtAabbMm** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %this.addr, align 4
  %mi = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %mi)
  %mx = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %mx)
  ret %struct.btDbvtAabbMm* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btFabsf(float %x) #3 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !10
  %0 = load float, float* %x.addr, align 4, !tbaa !10
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #7

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !31
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !31
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !10
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !10
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !10
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !10
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !10
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !10
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

define linkonce_odr hidden void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !10
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !10
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !10
  %3 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !10
  %4 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  store float 1.000000e+00, float* %ref.tmp5, align 4, !tbaa !10
  %5 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !10
  %6 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !10
  %7 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  store float 0.000000e+00, float* %ref.tmp8, align 4, !tbaa !10
  %8 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  store float 1.000000e+00, float* %ref.tmp9, align 4, !tbaa !10
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %9 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  %12 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  %13 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  %14 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #8
  %15 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !10
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !10
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !10
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !10
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !10
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !10
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !10
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !10
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !10
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !10
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !10
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !10
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !10
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !10
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !10
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !10
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !10
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !10
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #3 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !10
  %0 = load float, float* %y.addr, align 4, !tbaa !10
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #7

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #6 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !32
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !32
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !32
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x314serializeFloatER20btMatrix3x3FloatData(%class.btMatrix3x3* %this, %struct.btMatrix3x3FloatData* nonnull align 4 dereferenceable(48) %dataOut) #6 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %dataOut.addr = alloca %struct.btMatrix3x3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %struct.btMatrix3x3FloatData* %dataOut, %struct.btMatrix3x3FloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %i, align 4, !tbaa !31
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !31
  %cmp = icmp slt i32 %1, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %3 = load i32, i32* %i, align 4, !tbaa !31
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %3
  %4 = load %struct.btMatrix3x3FloatData*, %struct.btMatrix3x3FloatData** %dataOut.addr, align 4, !tbaa !2
  %m_el2 = getelementptr inbounds %struct.btMatrix3x3FloatData, %struct.btMatrix3x3FloatData* %4, i32 0, i32 0
  %5 = load i32, i32* %i, align 4, !tbaa !31
  %arrayidx3 = getelementptr inbounds [3 x %struct.btVector3FloatData], [3 x %struct.btVector3FloatData]* %m_el2, i32 0, i32 %5
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %arrayidx, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %arrayidx3)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !31
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !31
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %i, align 4, !tbaa !31
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !31
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %3 = load i32, i32* %i, align 4, !tbaa !31
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %3
  %4 = load float, float* %arrayidx, align 4, !tbaa !10
  %5 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %5, i32 0, i32 0
  %6 = load i32, i32* %i, align 4, !tbaa !31
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %6
  store float %4, float* %arrayidx3, align 4, !tbaa !10
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !31
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !31
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI20btCompoundShapeChildLj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE4initEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !50
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btCompoundShapeChild* null, %struct.btCompoundShapeChild** %m_data, align 4, !tbaa !35
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !34
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !51
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE5clearEv(%class.btAlignedObjectArray* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !31
  store i32 %last, i32* %last.addr, align 4, !tbaa !31
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %first.addr, align 4, !tbaa !31
  store i32 %1, i32* %i, align 4, !tbaa !31
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !31
  %3 = load i32, i32* %last.addr, align 4, !tbaa !31
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data, align 4, !tbaa !35
  %5 = load i32, i32* %i, align 4, !tbaa !31
  %arrayidx = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !31
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !31
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE10deallocateEv(%class.btAlignedObjectArray* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data, align 4, !tbaa !35
  %tobool = icmp ne %struct.btCompoundShapeChild* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !50, !range !22
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data4, align 4, !tbaa !35
  call void @_ZN18btAlignedAllocatorI20btCompoundShapeChildLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %struct.btCompoundShapeChild* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btCompoundShapeChild* null, %struct.btCompoundShapeChild** %m_data5, align 4, !tbaa !35
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI20btCompoundShapeChildLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %struct.btCompoundShapeChild* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %struct.btCompoundShapeChild*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store %struct.btCompoundShapeChild* %ptr, %struct.btCompoundShapeChild** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btCompoundShapeChild* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE8capacityEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !51
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btCompoundShapeChild*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !31
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !31
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btCompoundShapeChild** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !31
  %call2 = call i8* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.btCompoundShapeChild*
  store %struct.btCompoundShapeChild* %3, %struct.btCompoundShapeChild** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray* %this1)
  %4 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %struct.btCompoundShapeChild* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !50
  %5 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btCompoundShapeChild* %5, %struct.btCompoundShapeChild** %m_data, align 4, !tbaa !35
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !31
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !51
  %7 = bitcast %struct.btCompoundShapeChild** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !31
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !31
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !31
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN20btCompoundShapeChildnwEmPv(i32 %0, i8* %ptr) #3 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !52
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden %struct.btCompoundShapeChild* @_ZN20btCompoundShapeChildC2ERKS_(%struct.btCompoundShapeChild* returned %this, %struct.btCompoundShapeChild* nonnull align 4 dereferenceable(80) %0) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %struct.btCompoundShapeChild*, align 4
  %.addr = alloca %struct.btCompoundShapeChild*, align 4
  store %struct.btCompoundShapeChild* %this, %struct.btCompoundShapeChild** %this.addr, align 4, !tbaa !2
  store %struct.btCompoundShapeChild* %0, %struct.btCompoundShapeChild** %.addr, align 4, !tbaa !2
  %this1 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %this.addr, align 4
  %m_transform = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %this1, i32 0, i32 0
  %1 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %.addr, align 4, !tbaa !2
  %m_transform2 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %1, i32 0, i32 0
  %call = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_transform, %class.btTransform* nonnull align 4 dereferenceable(64) %m_transform2)
  %m_childShape = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %this1, i32 0, i32 1
  %2 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %.addr, align 4
  %m_childShape3 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %2, i32 0, i32 1
  %3 = bitcast %class.btCollisionShape** %m_childShape to i8*
  %4 = bitcast %class.btCollisionShape** %m_childShape3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 4 %3, i8* align 4 %4, i64 16, i1 false)
  ret %struct.btCompoundShapeChild* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #6 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !31
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !31
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !31
  %call = call %struct.btCompoundShapeChild* @_ZN18btAlignedAllocatorI20btCompoundShapeChildLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %struct.btCompoundShapeChild** null)
  %2 = bitcast %struct.btCompoundShapeChild* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %struct.btCompoundShapeChild* %dest) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btCompoundShapeChild*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !31
  store i32 %end, i32* %end.addr, align 4, !tbaa !31
  store %struct.btCompoundShapeChild* %dest, %struct.btCompoundShapeChild** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %start.addr, align 4, !tbaa !31
  store i32 %1, i32* %i, align 4, !tbaa !31
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !31
  %3 = load i32, i32* %end.addr, align 4, !tbaa !31
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !31
  %arrayidx = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %4, i32 %5
  %6 = bitcast %struct.btCompoundShapeChild* %arrayidx to i8*
  %call = call i8* @_ZN20btCompoundShapeChildnwEmPv(i32 80, i8* %6)
  %7 = bitcast i8* %call to %struct.btCompoundShapeChild*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data, align 4, !tbaa !35
  %9 = load i32, i32* %i, align 4, !tbaa !31
  %arrayidx2 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %8, i32 %9
  %call3 = call %struct.btCompoundShapeChild* @_ZN20btCompoundShapeChildC2ERKS_(%struct.btCompoundShapeChild* %7, %struct.btCompoundShapeChild* nonnull align 4 dereferenceable(80) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !31
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !31
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret void
}

define linkonce_odr hidden %struct.btCompoundShapeChild* @_ZN18btAlignedAllocatorI20btCompoundShapeChildLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %struct.btCompoundShapeChild** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btCompoundShapeChild**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !31
  store %struct.btCompoundShapeChild** %hint, %struct.btCompoundShapeChild*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !31
  %mul = mul i32 80, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btCompoundShapeChild*
  ret %struct.btCompoundShapeChild* %1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i64, i1 immarg) #2

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btCompoundShapeChildaSERKS_(%struct.btCompoundShapeChild* %this, %struct.btCompoundShapeChild* nonnull align 4 dereferenceable(80) %0) #6 comdat {
entry:
  %this.addr = alloca %struct.btCompoundShapeChild*, align 4
  %.addr = alloca %struct.btCompoundShapeChild*, align 4
  store %struct.btCompoundShapeChild* %this, %struct.btCompoundShapeChild** %this.addr, align 4, !tbaa !2
  store %struct.btCompoundShapeChild* %0, %struct.btCompoundShapeChild** %.addr, align 4, !tbaa !2
  %this1 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %this.addr, align 4
  %m_transform = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %this1, i32 0, i32 0
  %1 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %.addr, align 4, !tbaa !2
  %m_transform2 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_transform, %class.btTransform* nonnull align 4 dereferenceable(64) %m_transform2)
  %m_childShape = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %this1, i32 0, i32 1
  %2 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %.addr, align 4
  %m_childShape3 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %2, i32 0, i32 1
  %3 = bitcast %class.btCollisionShape** %m_childShape to i8*
  %4 = bitcast %class.btCollisionShape** %m_childShape3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 4 %3, i8* align 4 %4, i64 16, i1 false)
  ret %struct.btCompoundShapeChild* %this1
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind readnone speculatable willreturn }
attributes #8 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"bool", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"vtable pointer", !5, i64 0}
!10 = !{!11, !11, i64 0}
!11 = !{!"float", !4, i64 0}
!12 = !{!13, !3, i64 64}
!13 = !{!"_ZTS15btCompoundShape", !14, i64 12, !17, i64 32, !17, i64 48, !3, i64 64, !16, i64 68, !11, i64 72, !17, i64 76}
!14 = !{!"_ZTS20btAlignedObjectArrayI20btCompoundShapeChildE", !15, i64 0, !16, i64 4, !16, i64 8, !3, i64 12, !7, i64 16}
!15 = !{!"_ZTS18btAlignedAllocatorI20btCompoundShapeChildLj16EE"}
!16 = !{!"int", !4, i64 0}
!17 = !{!"_ZTS9btVector3", !4, i64 0}
!18 = !{!13, !16, i64 68}
!19 = !{!13, !11, i64 72}
!20 = !{!21, !16, i64 4}
!21 = !{!"_ZTS16btCollisionShape", !16, i64 4, !3, i64 8}
!22 = !{i8 0, i8 2}
!23 = !{!21, !3, i64 8}
!24 = !{!25, !3, i64 76}
!25 = !{!"_ZTS20btCompoundShapeChild", !26, i64 0, !3, i64 64, !16, i64 68, !11, i64 72, !3, i64 76}
!26 = !{!"_ZTS11btTransform", !27, i64 0, !17, i64 48}
!27 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!28 = !{!25, !3, i64 64}
!29 = !{!25, !16, i64 68}
!30 = !{!25, !11, i64 72}
!31 = !{!16, !16, i64 0}
!32 = !{i64 0, i64 16, !33}
!33 = !{!4, !4, i64 0}
!34 = !{!14, !16, i64 4}
!35 = !{!14, !3, i64 12}
!36 = !{!37, !11, i64 20}
!37 = !{!"_ZTS19btCompoundShapeData", !38, i64 0, !3, i64 12, !16, i64 16, !11, i64 20}
!38 = !{!"_ZTS20btCollisionShapeData", !3, i64 0, !16, i64 4, !4, i64 8}
!39 = !{!37, !16, i64 16}
!40 = !{!37, !3, i64 12}
!41 = !{!42, !3, i64 8}
!42 = !{!"_ZTS7btChunk", !16, i64 0, !16, i64 4, !3, i64 8, !16, i64 12, !16, i64 16}
!43 = !{!44, !11, i64 72}
!44 = !{!"_ZTS24btCompoundShapeChildData", !45, i64 0, !3, i64 64, !16, i64 68, !11, i64 72}
!45 = !{!"_ZTS20btTransformFloatData", !46, i64 0, !47, i64 48}
!46 = !{!"_ZTS20btMatrix3x3FloatData", !4, i64 0}
!47 = !{!"_ZTS18btVector3FloatData", !4, i64 0}
!48 = !{!44, !3, i64 64}
!49 = !{!44, !16, i64 68}
!50 = !{!14, !7, i64 16}
!51 = !{!14, !16, i64 8}
!52 = !{!53, !53, i64 0}
!53 = !{!"long", !4, i64 0}
