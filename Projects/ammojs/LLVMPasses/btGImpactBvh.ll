; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/Gimpact/btGImpactBvh.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/Gimpact/btGImpactBvh.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btBvhTree = type { i32, %class.GIM_BVH_TREE_NODE_ARRAY }
%class.GIM_BVH_TREE_NODE_ARRAY = type { %class.btAlignedObjectArray.base, [3 x i8] }
%class.btAlignedObjectArray.base = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.GIM_BVH_TREE_NODE*, i8 }>
%class.btAlignedAllocator = type { i8 }
%class.GIM_BVH_TREE_NODE = type { %class.btAABB, i32 }
%class.btAABB = type { %class.btVector3, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%class.GIM_BVH_DATA_ARRAY = type { %class.btAlignedObjectArray.base.3, [3 x i8] }
%class.btAlignedObjectArray.base.3 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.GIM_BVH_DATA*, i8 }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.GIM_BVH_DATA = type { %class.btAABB, i32 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.GIM_BVH_DATA*, i8, [3 x i8] }>
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.GIM_BVH_TREE_NODE*, i8, [3 x i8] }>
%class.btGImpactBvh = type { %class.btBvhTree, %class.btPrimitiveManagerBase* }
%class.btPrimitiveManagerBase = type { i32 (...)** }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btPairSet = type { %class.btAlignedObjectArray.base.11, [3 x i8] }
%class.btAlignedObjectArray.base.11 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, %struct.GIM_PAIR*, i8 }>
%class.btAlignedAllocator.9 = type { i8 }
%struct.GIM_PAIR = type { i32, i32 }
%class.BT_BOX_BOX_TRANSFORM_CACHE = type { %class.btVector3, %class.btMatrix3x3, %class.btMatrix3x3 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, %struct.GIM_PAIR*, i8, [3 x i8] }>

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEixEi = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZNK9btVector37maxAxisEv = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE4swapEii = comdat any

$_ZN9btBvhTree12setNodeBoundEiRK6btAABB = comdat any

$_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEEixEi = comdat any

$_ZN17GIM_BVH_TREE_NODE12setDataIndexEi = comdat any

$_ZN6btAABBC2Ev = comdat any

$_ZN6btAABB10invalidateEv = comdat any

$_ZN6btAABB5mergeERKS_ = comdat any

$_ZN17GIM_BVH_TREE_NODE14setEscapeIndexEi = comdat any

$_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE6resizeEiRKS0_ = comdat any

$_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE4sizeEv = comdat any

$_ZN17GIM_BVH_TREE_NODEC2Ev = comdat any

$_ZNK12btGImpactBvh12getNodeCountEv = comdat any

$_ZNK12btGImpactBvh10isLeafNodeEi = comdat any

$_ZNK12btGImpactBvh11getNodeDataEi = comdat any

$_ZN12btGImpactBvh12setNodeBoundEiRK6btAABB = comdat any

$_ZNK12btGImpactBvh11getLeftNodeEi = comdat any

$_ZNK12btGImpactBvh12getNodeBoundEiR6btAABB = comdat any

$_ZNK12btGImpactBvh12getRightNodeEi = comdat any

$_ZN18GIM_BVH_DATA_ARRAYC2Ev = comdat any

$_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE6resizeEiRKS0_ = comdat any

$_ZN12GIM_BVH_DATAC2Ev = comdat any

$_ZN20btAlignedObjectArrayI12GIM_BVH_DATAED2Ev = comdat any

$_ZNK6btAABB13has_collisionERKS_ = comdat any

$_ZN20btAlignedObjectArrayIiE9push_backERKi = comdat any

$_ZNK12btGImpactBvh18getEscapeNodeIndexEi = comdat any

$_ZNK20btAlignedObjectArrayIiE4sizeEv = comdat any

$_ZNK6btAABB11collide_rayERK9btVector3S2_ = comdat any

$_ZN26BT_BOX_BOX_TRANSFORM_CACHEC2Ev = comdat any

$_ZN26BT_BOX_BOX_TRANSFORM_CACHE19calc_from_homogenicERK11btTransformS2_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZNK9btBvhTree12getNodeCountEv = comdat any

$_ZNK9btBvhTree10isLeafNodeEi = comdat any

$_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEEixEi = comdat any

$_ZNK17GIM_BVH_TREE_NODE10isLeafNodeEv = comdat any

$_ZNK9btBvhTree11getNodeDataEi = comdat any

$_ZNK17GIM_BVH_TREE_NODE12getDataIndexEv = comdat any

$_ZNK9btBvhTree11getLeftNodeEi = comdat any

$_ZNK9btBvhTree12getNodeBoundEiR6btAABB = comdat any

$_ZNK9btBvhTree12getRightNodeEi = comdat any

$_ZNK17GIM_BVH_TREE_NODE14getEscapeIndexEv = comdat any

$_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEC2Ev = comdat any

$_ZN18btAlignedAllocatorI12GIM_BVH_DATALj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE4initEv = comdat any

$_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI12GIM_BVH_DATALj16EE10deallocateEPS0_ = comdat any

$_ZNK9btBvhTree18getEscapeNodeIndexEi = comdat any

$_ZNK6btAABB17get_center_extendER9btVector3S1_ = comdat any

$_Z6btFabsf = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZNK11btTransform7inverseEv = comdat any

$_ZNK11btTransformmlERKS_ = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZN11btTransform9getOriginEv = comdat any

$_ZN11btTransform8getBasisEv = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZN26BT_BOX_BOX_TRANSFORM_CACHE20calc_absolute_matrixEv = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZngRK9btVector3 = comdat any

$_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZN11btMatrix3x3ixEi = comdat any

$_Z15_node_collisionP12btGImpactBvhS0_RK26BT_BOX_BOX_TRANSFORM_CACHEiib = comdat any

$_ZN9btPairSet9push_pairEii = comdat any

$_ZNK6btAABB23overlapping_trans_cacheERKS_RK26BT_BOX_BOX_TRANSFORM_CACHEb = comdat any

$_Z15bt_mat3_dot_colRK11btMatrix3x3RK9btVector3i = comdat any

$_ZN20btAlignedObjectArrayI8GIM_PAIRE9push_backERKS0_ = comdat any

$_ZN8GIM_PAIRC2Eii = comdat any

$_ZNK20btAlignedObjectArrayI8GIM_PAIRE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayI8GIM_PAIRE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI8GIM_PAIRE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI8GIM_PAIRE9allocSizeEi = comdat any

$_ZN8GIM_PAIRC2ERKS_ = comdat any

$_ZN20btAlignedObjectArrayI8GIM_PAIRE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI8GIM_PAIRE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI8GIM_PAIRE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI8GIM_PAIRE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI8GIM_PAIRLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI8GIM_PAIRLj16EE10deallocateEPS0_ = comdat any

$_ZN12GIM_BVH_DATAC2ERKS_ = comdat any

$_ZN6btAABBC2ERKS_ = comdat any

$_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE7reserveEi = comdat any

$_ZN17GIM_BVH_TREE_NODEC2ERKS_ = comdat any

$_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI17GIM_BVH_TREE_NODELj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI17GIM_BVH_TREE_NODELj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI12GIM_BVH_DATALj16EE8allocateEiPPKS0_ = comdat any

$_ZNK20btAlignedObjectArrayIiE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIiE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIiE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIiE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIiE4copyEiiPi = comdat any

$_ZN20btAlignedObjectArrayIiE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIiE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi = comdat any

$_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi = comdat any

define hidden i32 @_ZN9btBvhTree20_calc_splitting_axisER18GIM_BVH_DATA_ARRAYii(%class.btBvhTree* %this, %class.GIM_BVH_DATA_ARRAY* nonnull align 4 dereferenceable(17) %primitive_boxes, i32 %startIndex, i32 %endIndex) #0 {
entry:
  %this.addr = alloca %class.btBvhTree*, align 4
  %primitive_boxes.addr = alloca %class.GIM_BVH_DATA_ARRAY*, align 4
  %startIndex.addr = alloca i32, align 4
  %endIndex.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %means = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %variance = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %numIndices = alloca i32, align 4
  %center = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp14 = alloca float, align 4
  %center19 = alloca %class.btVector3, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp21 = alloca %class.btVector3, align 4
  %diff2 = alloca %class.btVector3, align 4
  %ref.tmp28 = alloca %class.btVector3, align 4
  %ref.tmp33 = alloca float, align 4
  store %class.btBvhTree* %this, %class.btBvhTree** %this.addr, align 4, !tbaa !2
  store %class.GIM_BVH_DATA_ARRAY* %primitive_boxes, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4, !tbaa !2
  store i32 %startIndex, i32* %startIndex.addr, align 4, !tbaa !6
  store i32 %endIndex, i32* %endIndex.addr, align 4, !tbaa !6
  %this1 = load %class.btBvhTree*, %class.btBvhTree** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = bitcast %class.btVector3* %means to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #8
  %2 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !8
  %4 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %means, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  %6 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %7 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  %8 = bitcast %class.btVector3* %variance to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #8
  %9 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  store float 0.000000e+00, float* %ref.tmp5, align 4, !tbaa !8
  %11 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !8
  %call7 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %variance, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %12 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  %13 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  %14 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #8
  %15 = bitcast i32* %numIndices to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  %16 = load i32, i32* %endIndex.addr, align 4, !tbaa !6
  %17 = load i32, i32* %startIndex.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %16, %17
  store i32 %sub, i32* %numIndices, align 4, !tbaa !6
  %18 = load i32, i32* %startIndex.addr, align 4, !tbaa !6
  store i32 %18, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %19 = load i32, i32* %i, align 4, !tbaa !6
  %20 = load i32, i32* %endIndex.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %19, %20
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %21 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %21) #8
  %22 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #8
  store float 5.000000e-01, float* %ref.tmp8, align 4, !tbaa !8
  %23 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %23) #8
  %24 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4, !tbaa !2
  %25 = bitcast %class.GIM_BVH_DATA_ARRAY* %24 to %class.btAlignedObjectArray.0*
  %26 = load i32, i32* %i, align 4, !tbaa !6
  %call10 = call nonnull align 4 dereferenceable(36) %struct.GIM_BVH_DATA* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEixEi(%class.btAlignedObjectArray.0* %25, i32 %26)
  %m_bound = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %call10, i32 0, i32 0
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %m_bound, i32 0, i32 1
  %27 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4, !tbaa !2
  %28 = bitcast %class.GIM_BVH_DATA_ARRAY* %27 to %class.btAlignedObjectArray.0*
  %29 = load i32, i32* %i, align 4, !tbaa !6
  %call11 = call nonnull align 4 dereferenceable(36) %struct.GIM_BVH_DATA* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEixEi(%class.btAlignedObjectArray.0* %28, i32 %29)
  %m_bound12 = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %call11, i32 0, i32 0
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %m_bound12, i32 0, i32 0
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %m_max, %class.btVector3* nonnull align 4 dereferenceable(16) %m_min)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %center, float* nonnull align 4 dereferenceable(4) %ref.tmp8, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9)
  %30 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %30) #8
  %31 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #8
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %means, %class.btVector3* nonnull align 4 dereferenceable(16) %center)
  %32 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %32) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %33 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %33, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %34 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #8
  %35 = load i32, i32* %numIndices, align 4, !tbaa !6
  %conv = sitofp i32 %35 to float
  %div = fdiv float 1.000000e+00, %conv
  store float %div, float* %ref.tmp14, align 4, !tbaa !8
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %means, float* nonnull align 4 dereferenceable(4) %ref.tmp14)
  %36 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #8
  %37 = load i32, i32* %startIndex.addr, align 4, !tbaa !6
  store i32 %37, i32* %i, align 4, !tbaa !6
  br label %for.cond16

for.cond16:                                       ; preds = %for.inc30, %for.end
  %38 = load i32, i32* %i, align 4, !tbaa !6
  %39 = load i32, i32* %endIndex.addr, align 4, !tbaa !6
  %cmp17 = icmp slt i32 %38, %39
  br i1 %cmp17, label %for.body18, label %for.end32

for.body18:                                       ; preds = %for.cond16
  %40 = bitcast %class.btVector3* %center19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %40) #8
  %41 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #8
  store float 5.000000e-01, float* %ref.tmp20, align 4, !tbaa !8
  %42 = bitcast %class.btVector3* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %42) #8
  %43 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4, !tbaa !2
  %44 = bitcast %class.GIM_BVH_DATA_ARRAY* %43 to %class.btAlignedObjectArray.0*
  %45 = load i32, i32* %i, align 4, !tbaa !6
  %call22 = call nonnull align 4 dereferenceable(36) %struct.GIM_BVH_DATA* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEixEi(%class.btAlignedObjectArray.0* %44, i32 %45)
  %m_bound23 = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %call22, i32 0, i32 0
  %m_max24 = getelementptr inbounds %class.btAABB, %class.btAABB* %m_bound23, i32 0, i32 1
  %46 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4, !tbaa !2
  %47 = bitcast %class.GIM_BVH_DATA_ARRAY* %46 to %class.btAlignedObjectArray.0*
  %48 = load i32, i32* %i, align 4, !tbaa !6
  %call25 = call nonnull align 4 dereferenceable(36) %struct.GIM_BVH_DATA* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEixEi(%class.btAlignedObjectArray.0* %47, i32 %48)
  %m_bound26 = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %call25, i32 0, i32 0
  %m_min27 = getelementptr inbounds %class.btAABB, %class.btAABB* %m_bound26, i32 0, i32 0
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp21, %class.btVector3* nonnull align 4 dereferenceable(16) %m_max24, %class.btVector3* nonnull align 4 dereferenceable(16) %m_min27)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %center19, float* nonnull align 4 dereferenceable(4) %ref.tmp20, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp21)
  %49 = bitcast %class.btVector3* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %49) #8
  %50 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #8
  %51 = bitcast %class.btVector3* %diff2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %51) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %diff2, %class.btVector3* nonnull align 4 dereferenceable(16) %center19, %class.btVector3* nonnull align 4 dereferenceable(16) %means)
  %52 = bitcast %class.btVector3* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %52) #8
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp28, %class.btVector3* nonnull align 4 dereferenceable(16) %diff2, %class.btVector3* nonnull align 4 dereferenceable(16) %diff2)
  %53 = bitcast %class.btVector3* %diff2 to i8*
  %54 = bitcast %class.btVector3* %ref.tmp28 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %53, i8* align 4 %54, i32 16, i1 false), !tbaa.struct !10
  %55 = bitcast %class.btVector3* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %55) #8
  %call29 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %variance, %class.btVector3* nonnull align 4 dereferenceable(16) %diff2)
  %56 = bitcast %class.btVector3* %diff2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %56) #8
  %57 = bitcast %class.btVector3* %center19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %57) #8
  br label %for.inc30

for.inc30:                                        ; preds = %for.body18
  %58 = load i32, i32* %i, align 4, !tbaa !6
  %inc31 = add nsw i32 %58, 1
  store i32 %inc31, i32* %i, align 4, !tbaa !6
  br label %for.cond16

for.end32:                                        ; preds = %for.cond16
  %59 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #8
  %60 = load i32, i32* %numIndices, align 4, !tbaa !6
  %conv34 = sitofp i32 %60 to float
  %sub35 = fsub float %conv34, 1.000000e+00
  %div36 = fdiv float 1.000000e+00, %sub35
  store float %div36, float* %ref.tmp33, align 4, !tbaa !8
  %call37 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %variance, float* nonnull align 4 dereferenceable(4) %ref.tmp33)
  %61 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #8
  %call38 = call i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %variance)
  %62 = bitcast i32* %numIndices to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #8
  %63 = bitcast %class.btVector3* %variance to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %63) #8
  %64 = bitcast %class.btVector3* %means to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %64) #8
  %65 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #8
  ret i32 %call38
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !8
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !8
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !8
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !8
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #3 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !8
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !8
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !8
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !8
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !8
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(36) %struct.GIM_BVH_DATA* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %m_data, align 4, !tbaa !12
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %0, i32 %1
  ret %struct.GIM_BVH_DATA* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !8
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !8
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !8
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !8
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !8
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !8
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !8
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !8
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !8
  %6 = load float*, float** %s.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !8
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !8
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !8
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #3 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !8
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !8
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !8
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !8
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !8
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #3 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !8
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !8
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !8
  %mul8 = fmul float %7, %9
  store float %mul8, float* %ref.tmp3, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !8
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !8
  %mul14 = fmul float %12, %14
  store float %mul14, float* %ref.tmp9, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %1 = load float, float* %arrayidx3, align 4, !tbaa !8
  %cmp = fcmp olt float %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %2 = load float, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %3 = load float, float* %arrayidx7, align 4, !tbaa !8
  %cmp8 = fcmp olt float %2, %3
  %4 = zext i1 %cmp8 to i64
  %cond = select i1 %cmp8, i32 2, i32 1
  br label %cond.end

cond.false:                                       ; preds = %entry
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 0
  %5 = load float, float* %arrayidx10, align 4, !tbaa !8
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %6 = load float, float* %arrayidx12, align 4, !tbaa !8
  %cmp13 = fcmp olt float %5, %6
  %7 = zext i1 %cmp13 to i64
  %cond14 = select i1 %cmp13, i32 2, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond15 = phi i32 [ %cond, %cond.true ], [ %cond14, %cond.false ]
  ret i32 %cond15
}

define hidden i32 @_ZN9btBvhTree30_sort_and_calc_splitting_indexER18GIM_BVH_DATA_ARRAYiii(%class.btBvhTree* %this, %class.GIM_BVH_DATA_ARRAY* nonnull align 4 dereferenceable(17) %primitive_boxes, i32 %startIndex, i32 %endIndex, i32 %splitAxis) #0 {
entry:
  %this.addr = alloca %class.btBvhTree*, align 4
  %primitive_boxes.addr = alloca %class.GIM_BVH_DATA_ARRAY*, align 4
  %startIndex.addr = alloca i32, align 4
  %endIndex.addr = alloca i32, align 4
  %splitAxis.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %splitIndex = alloca i32, align 4
  %numIndices = alloca i32, align 4
  %splitValue = alloca float, align 4
  %means = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %center = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca float, align 4
  %center16 = alloca %class.btVector3, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp18 = alloca %class.btVector3, align 4
  %rangeBalancedIndices = alloca i32, align 4
  %unbalanced = alloca i8, align 1
  store %class.btBvhTree* %this, %class.btBvhTree** %this.addr, align 4, !tbaa !2
  store %class.GIM_BVH_DATA_ARRAY* %primitive_boxes, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4, !tbaa !2
  store i32 %startIndex, i32* %startIndex.addr, align 4, !tbaa !6
  store i32 %endIndex, i32* %endIndex.addr, align 4, !tbaa !6
  store i32 %splitAxis, i32* %splitAxis.addr, align 4, !tbaa !6
  %this1 = load %class.btBvhTree*, %class.btBvhTree** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = bitcast i32* %splitIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %startIndex.addr, align 4, !tbaa !6
  store i32 %2, i32* %splitIndex, align 4, !tbaa !6
  %3 = bitcast i32* %numIndices to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load i32, i32* %endIndex.addr, align 4, !tbaa !6
  %5 = load i32, i32* %startIndex.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %4, %5
  store i32 %sub, i32* %numIndices, align 4, !tbaa !6
  %6 = bitcast float* %splitValue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  store float 0.000000e+00, float* %splitValue, align 4, !tbaa !8
  %7 = bitcast %class.btVector3* %means to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #8
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !8
  %9 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %means, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %11 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  %12 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  %13 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  %14 = load i32, i32* %startIndex.addr, align 4, !tbaa !6
  store i32 %14, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %15 = load i32, i32* %i, align 4, !tbaa !6
  %16 = load i32, i32* %endIndex.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %15, %16
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %17 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %17) #8
  %18 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #8
  store float 5.000000e-01, float* %ref.tmp4, align 4, !tbaa !8
  %19 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %19) #8
  %20 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4, !tbaa !2
  %21 = bitcast %class.GIM_BVH_DATA_ARRAY* %20 to %class.btAlignedObjectArray.0*
  %22 = load i32, i32* %i, align 4, !tbaa !6
  %call6 = call nonnull align 4 dereferenceable(36) %struct.GIM_BVH_DATA* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEixEi(%class.btAlignedObjectArray.0* %21, i32 %22)
  %m_bound = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %call6, i32 0, i32 0
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %m_bound, i32 0, i32 1
  %23 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4, !tbaa !2
  %24 = bitcast %class.GIM_BVH_DATA_ARRAY* %23 to %class.btAlignedObjectArray.0*
  %25 = load i32, i32* %i, align 4, !tbaa !6
  %call7 = call nonnull align 4 dereferenceable(36) %struct.GIM_BVH_DATA* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEixEi(%class.btAlignedObjectArray.0* %24, i32 %25)
  %m_bound8 = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %call7, i32 0, i32 0
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %m_bound8, i32 0, i32 0
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %m_max, %class.btVector3* nonnull align 4 dereferenceable(16) %m_min)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %center, float* nonnull align 4 dereferenceable(4) %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %26 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %26) #8
  %27 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #8
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %means, %class.btVector3* nonnull align 4 dereferenceable(16) %center)
  %28 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %28) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %29 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %29, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %30 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #8
  %31 = load i32, i32* %numIndices, align 4, !tbaa !6
  %conv = sitofp i32 %31 to float
  %div = fdiv float 1.000000e+00, %conv
  store float %div, float* %ref.tmp10, align 4, !tbaa !8
  %call11 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %means, float* nonnull align 4 dereferenceable(4) %ref.tmp10)
  %32 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #8
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %means)
  %33 = load i32, i32* %splitAxis.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %call12, i32 %33
  %34 = load float, float* %arrayidx, align 4, !tbaa !8
  store float %34, float* %splitValue, align 4, !tbaa !8
  %35 = load i32, i32* %startIndex.addr, align 4, !tbaa !6
  store i32 %35, i32* %i, align 4, !tbaa !6
  br label %for.cond13

for.cond13:                                       ; preds = %for.inc29, %for.end
  %36 = load i32, i32* %i, align 4, !tbaa !6
  %37 = load i32, i32* %endIndex.addr, align 4, !tbaa !6
  %cmp14 = icmp slt i32 %36, %37
  br i1 %cmp14, label %for.body15, label %for.end31

for.body15:                                       ; preds = %for.cond13
  %38 = bitcast %class.btVector3* %center16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %38) #8
  %39 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #8
  store float 5.000000e-01, float* %ref.tmp17, align 4, !tbaa !8
  %40 = bitcast %class.btVector3* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %40) #8
  %41 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4, !tbaa !2
  %42 = bitcast %class.GIM_BVH_DATA_ARRAY* %41 to %class.btAlignedObjectArray.0*
  %43 = load i32, i32* %i, align 4, !tbaa !6
  %call19 = call nonnull align 4 dereferenceable(36) %struct.GIM_BVH_DATA* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEixEi(%class.btAlignedObjectArray.0* %42, i32 %43)
  %m_bound20 = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %call19, i32 0, i32 0
  %m_max21 = getelementptr inbounds %class.btAABB, %class.btAABB* %m_bound20, i32 0, i32 1
  %44 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4, !tbaa !2
  %45 = bitcast %class.GIM_BVH_DATA_ARRAY* %44 to %class.btAlignedObjectArray.0*
  %46 = load i32, i32* %i, align 4, !tbaa !6
  %call22 = call nonnull align 4 dereferenceable(36) %struct.GIM_BVH_DATA* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEixEi(%class.btAlignedObjectArray.0* %45, i32 %46)
  %m_bound23 = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %call22, i32 0, i32 0
  %m_min24 = getelementptr inbounds %class.btAABB, %class.btAABB* %m_bound23, i32 0, i32 0
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp18, %class.btVector3* nonnull align 4 dereferenceable(16) %m_max21, %class.btVector3* nonnull align 4 dereferenceable(16) %m_min24)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %center16, float* nonnull align 4 dereferenceable(4) %ref.tmp17, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp18)
  %47 = bitcast %class.btVector3* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %47) #8
  %48 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #8
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center16)
  %49 = load i32, i32* %splitAxis.addr, align 4, !tbaa !6
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 %49
  %50 = load float, float* %arrayidx26, align 4, !tbaa !8
  %51 = load float, float* %splitValue, align 4, !tbaa !8
  %cmp27 = fcmp ogt float %50, %51
  br i1 %cmp27, label %if.then, label %if.end

if.then:                                          ; preds = %for.body15
  %52 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4, !tbaa !2
  %53 = bitcast %class.GIM_BVH_DATA_ARRAY* %52 to %class.btAlignedObjectArray.0*
  %54 = load i32, i32* %i, align 4, !tbaa !6
  %55 = load i32, i32* %splitIndex, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE4swapEii(%class.btAlignedObjectArray.0* %53, i32 %54, i32 %55)
  %56 = load i32, i32* %splitIndex, align 4, !tbaa !6
  %inc28 = add nsw i32 %56, 1
  store i32 %inc28, i32* %splitIndex, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body15
  %57 = bitcast %class.btVector3* %center16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %57) #8
  br label %for.inc29

for.inc29:                                        ; preds = %if.end
  %58 = load i32, i32* %i, align 4, !tbaa !6
  %inc30 = add nsw i32 %58, 1
  store i32 %inc30, i32* %i, align 4, !tbaa !6
  br label %for.cond13

for.end31:                                        ; preds = %for.cond13
  %59 = bitcast i32* %rangeBalancedIndices to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #8
  %60 = load i32, i32* %numIndices, align 4, !tbaa !6
  %div32 = sdiv i32 %60, 3
  store i32 %div32, i32* %rangeBalancedIndices, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %unbalanced) #8
  %61 = load i32, i32* %splitIndex, align 4, !tbaa !6
  %62 = load i32, i32* %startIndex.addr, align 4, !tbaa !6
  %63 = load i32, i32* %rangeBalancedIndices, align 4, !tbaa !6
  %add = add nsw i32 %62, %63
  %cmp33 = icmp sle i32 %61, %add
  br i1 %cmp33, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %for.end31
  %64 = load i32, i32* %splitIndex, align 4, !tbaa !6
  %65 = load i32, i32* %endIndex.addr, align 4, !tbaa !6
  %sub34 = sub nsw i32 %65, 1
  %66 = load i32, i32* %rangeBalancedIndices, align 4, !tbaa !6
  %sub35 = sub nsw i32 %sub34, %66
  %cmp36 = icmp sge i32 %64, %sub35
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %for.end31
  %67 = phi i1 [ true, %for.end31 ], [ %cmp36, %lor.rhs ]
  %frombool = zext i1 %67 to i8
  store i8 %frombool, i8* %unbalanced, align 1, !tbaa !16
  %68 = load i8, i8* %unbalanced, align 1, !tbaa !16, !range !17
  %tobool = trunc i8 %68 to i1
  br i1 %tobool, label %if.then37, label %if.end39

if.then37:                                        ; preds = %lor.end
  %69 = load i32, i32* %startIndex.addr, align 4, !tbaa !6
  %70 = load i32, i32* %numIndices, align 4, !tbaa !6
  %shr = ashr i32 %70, 1
  %add38 = add nsw i32 %69, %shr
  store i32 %add38, i32* %splitIndex, align 4, !tbaa !6
  br label %if.end39

if.end39:                                         ; preds = %if.then37, %lor.end
  %71 = load i32, i32* %splitIndex, align 4, !tbaa !6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %unbalanced) #8
  %72 = bitcast i32* %rangeBalancedIndices to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #8
  %73 = bitcast %class.btVector3* %means to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %73) #8
  %74 = bitcast float* %splitValue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #8
  %75 = bitcast i32* %numIndices to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #8
  %76 = bitcast i32* %splitIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #8
  %77 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #8
  ret i32 %71
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE4swapEii(%class.btAlignedObjectArray.0* %this, i32 %index0, i32 %index1) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %struct.GIM_BVH_DATA, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %index0, i32* %index0.addr, align 4, !tbaa !6
  store i32 %index1, i32* %index1.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast %struct.GIM_BVH_DATA* %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 36, i8* %0) #8
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %1 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %m_data, align 4, !tbaa !12
  %2 = load i32, i32* %index0.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %1, i32 %2
  %call = call %struct.GIM_BVH_DATA* @_ZN12GIM_BVH_DATAC2ERKS_(%struct.GIM_BVH_DATA* %temp, %struct.GIM_BVH_DATA* nonnull align 4 dereferenceable(36) %arrayidx)
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %3 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %m_data2, align 4, !tbaa !12
  %4 = load i32, i32* %index1.addr, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %3, i32 %4
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %5 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %m_data4, align 4, !tbaa !12
  %6 = load i32, i32* %index0.addr, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %5, i32 %6
  %7 = bitcast %struct.GIM_BVH_DATA* %arrayidx5 to i8*
  %8 = bitcast %struct.GIM_BVH_DATA* %arrayidx3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 36, i1 false), !tbaa.struct !18
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %9 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %m_data6, align 4, !tbaa !12
  %10 = load i32, i32* %index1.addr, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %9, i32 %10
  %11 = bitcast %struct.GIM_BVH_DATA* %arrayidx7 to i8*
  %12 = bitcast %struct.GIM_BVH_DATA* %temp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 36, i1 false), !tbaa.struct !18
  %13 = bitcast %struct.GIM_BVH_DATA* %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 36, i8* %13) #8
  ret void
}

define hidden void @_ZN9btBvhTree15_build_sub_treeER18GIM_BVH_DATA_ARRAYii(%class.btBvhTree* %this, %class.GIM_BVH_DATA_ARRAY* nonnull align 4 dereferenceable(17) %primitive_boxes, i32 %startIndex, i32 %endIndex) #0 {
entry:
  %this.addr = alloca %class.btBvhTree*, align 4
  %primitive_boxes.addr = alloca %class.GIM_BVH_DATA_ARRAY*, align 4
  %startIndex.addr = alloca i32, align 4
  %endIndex.addr = alloca i32, align 4
  %curIndex = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %splitIndex = alloca i32, align 4
  %node_bound = alloca %class.btAABB, align 4
  %i = alloca i32, align 4
  store %class.btBvhTree* %this, %class.btBvhTree** %this.addr, align 4, !tbaa !2
  store %class.GIM_BVH_DATA_ARRAY* %primitive_boxes, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4, !tbaa !2
  store i32 %startIndex, i32* %startIndex.addr, align 4, !tbaa !6
  store i32 %endIndex, i32* %endIndex.addr, align 4, !tbaa !6
  %this1 = load %class.btBvhTree*, %class.btBvhTree** %this.addr, align 4
  %0 = bitcast i32* %curIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %m_num_nodes = getelementptr inbounds %class.btBvhTree, %class.btBvhTree* %this1, i32 0, i32 0
  %1 = load i32, i32* %m_num_nodes, align 4, !tbaa !19
  store i32 %1, i32* %curIndex, align 4, !tbaa !6
  %m_num_nodes2 = getelementptr inbounds %class.btBvhTree, %class.btBvhTree* %this1, i32 0, i32 0
  %2 = load i32, i32* %m_num_nodes2, align 4, !tbaa !19
  %inc = add nsw i32 %2, 1
  store i32 %inc, i32* %m_num_nodes2, align 4, !tbaa !19
  %3 = load i32, i32* %endIndex.addr, align 4, !tbaa !6
  %4 = load i32, i32* %startIndex.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %3, %4
  %cmp = icmp eq i32 %sub, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %curIndex, align 4, !tbaa !6
  %6 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4, !tbaa !2
  %7 = bitcast %class.GIM_BVH_DATA_ARRAY* %6 to %class.btAlignedObjectArray.0*
  %8 = load i32, i32* %startIndex.addr, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(36) %struct.GIM_BVH_DATA* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEixEi(%class.btAlignedObjectArray.0* %7, i32 %8)
  %m_bound = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %call, i32 0, i32 0
  call void @_ZN9btBvhTree12setNodeBoundEiRK6btAABB(%class.btBvhTree* %this1, i32 %5, %class.btAABB* nonnull align 4 dereferenceable(32) %m_bound)
  %m_node_array = getelementptr inbounds %class.btBvhTree, %class.btBvhTree* %this1, i32 0, i32 1
  %9 = bitcast %class.GIM_BVH_TREE_NODE_ARRAY* %m_node_array to %class.btAlignedObjectArray*
  %10 = load i32, i32* %curIndex, align 4, !tbaa !6
  %call3 = call nonnull align 4 dereferenceable(36) %class.GIM_BVH_TREE_NODE* @_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEEixEi(%class.btAlignedObjectArray* %9, i32 %10)
  %11 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4, !tbaa !2
  %12 = bitcast %class.GIM_BVH_DATA_ARRAY* %11 to %class.btAlignedObjectArray.0*
  %13 = load i32, i32* %startIndex.addr, align 4, !tbaa !6
  %call4 = call nonnull align 4 dereferenceable(36) %struct.GIM_BVH_DATA* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEixEi(%class.btAlignedObjectArray.0* %12, i32 %13)
  %m_data = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %call4, i32 0, i32 1
  %14 = load i32, i32* %m_data, align 4, !tbaa !22
  call void @_ZN17GIM_BVH_TREE_NODE12setDataIndexEi(%class.GIM_BVH_TREE_NODE* %call3, i32 %14)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %15 = bitcast i32* %splitIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  %16 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4, !tbaa !2
  %17 = load i32, i32* %startIndex.addr, align 4, !tbaa !6
  %18 = load i32, i32* %endIndex.addr, align 4, !tbaa !6
  %call5 = call i32 @_ZN9btBvhTree20_calc_splitting_axisER18GIM_BVH_DATA_ARRAYii(%class.btBvhTree* %this1, %class.GIM_BVH_DATA_ARRAY* nonnull align 4 dereferenceable(17) %16, i32 %17, i32 %18)
  store i32 %call5, i32* %splitIndex, align 4, !tbaa !6
  %19 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4, !tbaa !2
  %20 = load i32, i32* %startIndex.addr, align 4, !tbaa !6
  %21 = load i32, i32* %endIndex.addr, align 4, !tbaa !6
  %22 = load i32, i32* %splitIndex, align 4, !tbaa !6
  %call6 = call i32 @_ZN9btBvhTree30_sort_and_calc_splitting_indexER18GIM_BVH_DATA_ARRAYiii(%class.btBvhTree* %this1, %class.GIM_BVH_DATA_ARRAY* nonnull align 4 dereferenceable(17) %19, i32 %20, i32 %21, i32 %22)
  store i32 %call6, i32* %splitIndex, align 4, !tbaa !6
  %23 = bitcast %class.btAABB* %node_bound to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %23) #8
  %call7 = call %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* %node_bound)
  call void @_ZN6btAABB10invalidateEv(%class.btAABB* %node_bound)
  %24 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #8
  %25 = load i32, i32* %startIndex.addr, align 4, !tbaa !6
  store i32 %25, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %26 = load i32, i32* %i, align 4, !tbaa !6
  %27 = load i32, i32* %endIndex.addr, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %26, %27
  br i1 %cmp8, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %28 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %29 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4, !tbaa !2
  %30 = bitcast %class.GIM_BVH_DATA_ARRAY* %29 to %class.btAlignedObjectArray.0*
  %31 = load i32, i32* %i, align 4, !tbaa !6
  %call9 = call nonnull align 4 dereferenceable(36) %struct.GIM_BVH_DATA* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEixEi(%class.btAlignedObjectArray.0* %30, i32 %31)
  %m_bound10 = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %call9, i32 0, i32 0
  call void @_ZN6btAABB5mergeERKS_(%class.btAABB* %node_bound, %class.btAABB* nonnull align 4 dereferenceable(32) %m_bound10)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %32 = load i32, i32* %i, align 4, !tbaa !6
  %inc11 = add nsw i32 %32, 1
  store i32 %inc11, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %33 = load i32, i32* %curIndex, align 4, !tbaa !6
  call void @_ZN9btBvhTree12setNodeBoundEiRK6btAABB(%class.btBvhTree* %this1, i32 %33, %class.btAABB* nonnull align 4 dereferenceable(32) %node_bound)
  %34 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4, !tbaa !2
  %35 = load i32, i32* %startIndex.addr, align 4, !tbaa !6
  %36 = load i32, i32* %splitIndex, align 4, !tbaa !6
  call void @_ZN9btBvhTree15_build_sub_treeER18GIM_BVH_DATA_ARRAYii(%class.btBvhTree* %this1, %class.GIM_BVH_DATA_ARRAY* nonnull align 4 dereferenceable(17) %34, i32 %35, i32 %36)
  %37 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4, !tbaa !2
  %38 = load i32, i32* %splitIndex, align 4, !tbaa !6
  %39 = load i32, i32* %endIndex.addr, align 4, !tbaa !6
  call void @_ZN9btBvhTree15_build_sub_treeER18GIM_BVH_DATA_ARRAYii(%class.btBvhTree* %this1, %class.GIM_BVH_DATA_ARRAY* nonnull align 4 dereferenceable(17) %37, i32 %38, i32 %39)
  %m_node_array12 = getelementptr inbounds %class.btBvhTree, %class.btBvhTree* %this1, i32 0, i32 1
  %40 = bitcast %class.GIM_BVH_TREE_NODE_ARRAY* %m_node_array12 to %class.btAlignedObjectArray*
  %41 = load i32, i32* %curIndex, align 4, !tbaa !6
  %call13 = call nonnull align 4 dereferenceable(36) %class.GIM_BVH_TREE_NODE* @_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEEixEi(%class.btAlignedObjectArray* %40, i32 %41)
  %m_num_nodes14 = getelementptr inbounds %class.btBvhTree, %class.btBvhTree* %this1, i32 0, i32 0
  %42 = load i32, i32* %m_num_nodes14, align 4, !tbaa !19
  %43 = load i32, i32* %curIndex, align 4, !tbaa !6
  %sub15 = sub nsw i32 %42, %43
  call void @_ZN17GIM_BVH_TREE_NODE14setEscapeIndexEi(%class.GIM_BVH_TREE_NODE* %call13, i32 %sub15)
  %44 = bitcast %class.btAABB* %node_bound to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %44) #8
  %45 = bitcast i32* %splitIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #8
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then
  %46 = bitcast i32* %curIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN9btBvhTree12setNodeBoundEiRK6btAABB(%class.btBvhTree* %this, i32 %nodeindex, %class.btAABB* nonnull align 4 dereferenceable(32) %bound) #3 comdat {
entry:
  %this.addr = alloca %class.btBvhTree*, align 4
  %nodeindex.addr = alloca i32, align 4
  %bound.addr = alloca %class.btAABB*, align 4
  store %class.btBvhTree* %this, %class.btBvhTree** %this.addr, align 4, !tbaa !2
  store i32 %nodeindex, i32* %nodeindex.addr, align 4, !tbaa !6
  store %class.btAABB* %bound, %class.btAABB** %bound.addr, align 4, !tbaa !2
  %this1 = load %class.btBvhTree*, %class.btBvhTree** %this.addr, align 4
  %0 = load %class.btAABB*, %class.btAABB** %bound.addr, align 4, !tbaa !2
  %m_node_array = getelementptr inbounds %class.btBvhTree, %class.btBvhTree* %this1, i32 0, i32 1
  %1 = bitcast %class.GIM_BVH_TREE_NODE_ARRAY* %m_node_array to %class.btAlignedObjectArray*
  %2 = load i32, i32* %nodeindex.addr, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(36) %class.GIM_BVH_TREE_NODE* @_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEEixEi(%class.btAlignedObjectArray* %1, i32 %2)
  %m_bound = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %call, i32 0, i32 0
  %3 = bitcast %class.btAABB* %m_bound to i8*
  %4 = bitcast %class.btAABB* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 32, i1 false), !tbaa.struct !26
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(36) %class.GIM_BVH_TREE_NODE* @_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEEixEi(%class.btAlignedObjectArray* %this, i32 %n) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %m_data, align 4, !tbaa !27
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %0, i32 %1
  ret %class.GIM_BVH_TREE_NODE* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN17GIM_BVH_TREE_NODE12setDataIndexEi(%class.GIM_BVH_TREE_NODE* %this, i32 %index) #2 comdat {
entry:
  %this.addr = alloca %class.GIM_BVH_TREE_NODE*, align 4
  %index.addr = alloca i32, align 4
  store %class.GIM_BVH_TREE_NODE* %this, %class.GIM_BVH_TREE_NODE** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !6
  %this1 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %this.addr, align 4
  %0 = load i32, i32* %index.addr, align 4, !tbaa !6
  %m_escapeIndexOrDataIndex = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %this1, i32 0, i32 1
  store i32 %0, i32* %m_escapeIndexOrDataIndex, align 4, !tbaa !30
  ret void
}

define linkonce_odr hidden %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAABB*, align 4
  store %class.btAABB* %this, %class.btAABB** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAABB*, %class.btAABB** %this.addr, align 4
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_min)
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_max)
  ret %class.btAABB* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN6btAABB10invalidateEv(%class.btAABB* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAABB*, align 4
  store %class.btAABB* %this, %class.btAABB** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAABB*, %class.btAABB** %this.addr, align 4
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  store float 0x47EFFFFFE0000000, float* %arrayidx, align 4, !tbaa !8
  %m_min2 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call3 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min2)
  %arrayidx4 = getelementptr inbounds float, float* %call3, i32 1
  store float 0x47EFFFFFE0000000, float* %arrayidx4, align 4, !tbaa !8
  %m_min5 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call6 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min5)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 2
  store float 0x47EFFFFFE0000000, float* %arrayidx7, align 4, !tbaa !8
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call8 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 0
  store float 0xC7EFFFFFE0000000, float* %arrayidx9, align 4, !tbaa !8
  %m_max10 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call11 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max10)
  %arrayidx12 = getelementptr inbounds float, float* %call11, i32 1
  store float 0xC7EFFFFFE0000000, float* %arrayidx12, align 4, !tbaa !8
  %m_max13 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call14 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max13)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 2
  store float 0xC7EFFFFFE0000000, float* %arrayidx15, align 4, !tbaa !8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN6btAABB5mergeERKS_(%class.btAABB* %this, %class.btAABB* nonnull align 4 dereferenceable(32) %box) #3 comdat {
entry:
  %this.addr = alloca %class.btAABB*, align 4
  %box.addr = alloca %class.btAABB*, align 4
  store %class.btAABB* %this, %class.btAABB** %this.addr, align 4, !tbaa !2
  store %class.btAABB* %box, %class.btAABB** %box.addr, align 4, !tbaa !2
  %this1 = load %class.btAABB*, %class.btAABB** %this.addr, align 4
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !8
  %1 = load %class.btAABB*, %class.btAABB** %box.addr, align 4, !tbaa !2
  %m_min2 = getelementptr inbounds %class.btAABB, %class.btAABB* %1, i32 0, i32 0
  %call3 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min2)
  %arrayidx4 = getelementptr inbounds float, float* %call3, i32 0
  %2 = load float, float* %arrayidx4, align 4, !tbaa !8
  %cmp = fcmp ogt float %0, %2
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %3 = load %class.btAABB*, %class.btAABB** %box.addr, align 4, !tbaa !2
  %m_min5 = getelementptr inbounds %class.btAABB, %class.btAABB* %3, i32 0, i32 0
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min5)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 0
  %4 = load float, float* %arrayidx7, align 4, !tbaa !8
  br label %cond.end

cond.false:                                       ; preds = %entry
  %m_min8 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call9 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min8)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 0
  %5 = load float, float* %arrayidx10, align 4, !tbaa !8
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %4, %cond.true ], [ %5, %cond.false ]
  %m_min11 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min11)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 0
  store float %cond, float* %arrayidx13, align 4, !tbaa !8
  %m_min14 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call15 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min14)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 1
  %6 = load float, float* %arrayidx16, align 4, !tbaa !8
  %7 = load %class.btAABB*, %class.btAABB** %box.addr, align 4, !tbaa !2
  %m_min17 = getelementptr inbounds %class.btAABB, %class.btAABB* %7, i32 0, i32 0
  %call18 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min17)
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 1
  %8 = load float, float* %arrayidx19, align 4, !tbaa !8
  %cmp20 = fcmp ogt float %6, %8
  br i1 %cmp20, label %cond.true21, label %cond.false25

cond.true21:                                      ; preds = %cond.end
  %9 = load %class.btAABB*, %class.btAABB** %box.addr, align 4, !tbaa !2
  %m_min22 = getelementptr inbounds %class.btAABB, %class.btAABB* %9, i32 0, i32 0
  %call23 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min22)
  %arrayidx24 = getelementptr inbounds float, float* %call23, i32 1
  %10 = load float, float* %arrayidx24, align 4, !tbaa !8
  br label %cond.end29

cond.false25:                                     ; preds = %cond.end
  %m_min26 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call27 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min26)
  %arrayidx28 = getelementptr inbounds float, float* %call27, i32 1
  %11 = load float, float* %arrayidx28, align 4, !tbaa !8
  br label %cond.end29

cond.end29:                                       ; preds = %cond.false25, %cond.true21
  %cond30 = phi float [ %10, %cond.true21 ], [ %11, %cond.false25 ]
  %m_min31 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call32 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min31)
  %arrayidx33 = getelementptr inbounds float, float* %call32, i32 1
  store float %cond30, float* %arrayidx33, align 4, !tbaa !8
  %m_min34 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call35 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min34)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 2
  %12 = load float, float* %arrayidx36, align 4, !tbaa !8
  %13 = load %class.btAABB*, %class.btAABB** %box.addr, align 4, !tbaa !2
  %m_min37 = getelementptr inbounds %class.btAABB, %class.btAABB* %13, i32 0, i32 0
  %call38 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min37)
  %arrayidx39 = getelementptr inbounds float, float* %call38, i32 2
  %14 = load float, float* %arrayidx39, align 4, !tbaa !8
  %cmp40 = fcmp ogt float %12, %14
  br i1 %cmp40, label %cond.true41, label %cond.false45

cond.true41:                                      ; preds = %cond.end29
  %15 = load %class.btAABB*, %class.btAABB** %box.addr, align 4, !tbaa !2
  %m_min42 = getelementptr inbounds %class.btAABB, %class.btAABB* %15, i32 0, i32 0
  %call43 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min42)
  %arrayidx44 = getelementptr inbounds float, float* %call43, i32 2
  %16 = load float, float* %arrayidx44, align 4, !tbaa !8
  br label %cond.end49

cond.false45:                                     ; preds = %cond.end29
  %m_min46 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call47 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min46)
  %arrayidx48 = getelementptr inbounds float, float* %call47, i32 2
  %17 = load float, float* %arrayidx48, align 4, !tbaa !8
  br label %cond.end49

cond.end49:                                       ; preds = %cond.false45, %cond.true41
  %cond50 = phi float [ %16, %cond.true41 ], [ %17, %cond.false45 ]
  %m_min51 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call52 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min51)
  %arrayidx53 = getelementptr inbounds float, float* %call52, i32 2
  store float %cond50, float* %arrayidx53, align 4, !tbaa !8
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call54 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max)
  %arrayidx55 = getelementptr inbounds float, float* %call54, i32 0
  %18 = load float, float* %arrayidx55, align 4, !tbaa !8
  %19 = load %class.btAABB*, %class.btAABB** %box.addr, align 4, !tbaa !2
  %m_max56 = getelementptr inbounds %class.btAABB, %class.btAABB* %19, i32 0, i32 1
  %call57 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max56)
  %arrayidx58 = getelementptr inbounds float, float* %call57, i32 0
  %20 = load float, float* %arrayidx58, align 4, !tbaa !8
  %cmp59 = fcmp olt float %18, %20
  br i1 %cmp59, label %cond.true60, label %cond.false64

cond.true60:                                      ; preds = %cond.end49
  %21 = load %class.btAABB*, %class.btAABB** %box.addr, align 4, !tbaa !2
  %m_max61 = getelementptr inbounds %class.btAABB, %class.btAABB* %21, i32 0, i32 1
  %call62 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max61)
  %arrayidx63 = getelementptr inbounds float, float* %call62, i32 0
  %22 = load float, float* %arrayidx63, align 4, !tbaa !8
  br label %cond.end68

cond.false64:                                     ; preds = %cond.end49
  %m_max65 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call66 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max65)
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 0
  %23 = load float, float* %arrayidx67, align 4, !tbaa !8
  br label %cond.end68

cond.end68:                                       ; preds = %cond.false64, %cond.true60
  %cond69 = phi float [ %22, %cond.true60 ], [ %23, %cond.false64 ]
  %m_max70 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call71 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max70)
  %arrayidx72 = getelementptr inbounds float, float* %call71, i32 0
  store float %cond69, float* %arrayidx72, align 4, !tbaa !8
  %m_max73 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call74 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max73)
  %arrayidx75 = getelementptr inbounds float, float* %call74, i32 1
  %24 = load float, float* %arrayidx75, align 4, !tbaa !8
  %25 = load %class.btAABB*, %class.btAABB** %box.addr, align 4, !tbaa !2
  %m_max76 = getelementptr inbounds %class.btAABB, %class.btAABB* %25, i32 0, i32 1
  %call77 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max76)
  %arrayidx78 = getelementptr inbounds float, float* %call77, i32 1
  %26 = load float, float* %arrayidx78, align 4, !tbaa !8
  %cmp79 = fcmp olt float %24, %26
  br i1 %cmp79, label %cond.true80, label %cond.false84

cond.true80:                                      ; preds = %cond.end68
  %27 = load %class.btAABB*, %class.btAABB** %box.addr, align 4, !tbaa !2
  %m_max81 = getelementptr inbounds %class.btAABB, %class.btAABB* %27, i32 0, i32 1
  %call82 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max81)
  %arrayidx83 = getelementptr inbounds float, float* %call82, i32 1
  %28 = load float, float* %arrayidx83, align 4, !tbaa !8
  br label %cond.end88

cond.false84:                                     ; preds = %cond.end68
  %m_max85 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call86 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max85)
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 1
  %29 = load float, float* %arrayidx87, align 4, !tbaa !8
  br label %cond.end88

cond.end88:                                       ; preds = %cond.false84, %cond.true80
  %cond89 = phi float [ %28, %cond.true80 ], [ %29, %cond.false84 ]
  %m_max90 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call91 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max90)
  %arrayidx92 = getelementptr inbounds float, float* %call91, i32 1
  store float %cond89, float* %arrayidx92, align 4, !tbaa !8
  %m_max93 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call94 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max93)
  %arrayidx95 = getelementptr inbounds float, float* %call94, i32 2
  %30 = load float, float* %arrayidx95, align 4, !tbaa !8
  %31 = load %class.btAABB*, %class.btAABB** %box.addr, align 4, !tbaa !2
  %m_max96 = getelementptr inbounds %class.btAABB, %class.btAABB* %31, i32 0, i32 1
  %call97 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max96)
  %arrayidx98 = getelementptr inbounds float, float* %call97, i32 2
  %32 = load float, float* %arrayidx98, align 4, !tbaa !8
  %cmp99 = fcmp olt float %30, %32
  br i1 %cmp99, label %cond.true100, label %cond.false104

cond.true100:                                     ; preds = %cond.end88
  %33 = load %class.btAABB*, %class.btAABB** %box.addr, align 4, !tbaa !2
  %m_max101 = getelementptr inbounds %class.btAABB, %class.btAABB* %33, i32 0, i32 1
  %call102 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max101)
  %arrayidx103 = getelementptr inbounds float, float* %call102, i32 2
  %34 = load float, float* %arrayidx103, align 4, !tbaa !8
  br label %cond.end108

cond.false104:                                    ; preds = %cond.end88
  %m_max105 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call106 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max105)
  %arrayidx107 = getelementptr inbounds float, float* %call106, i32 2
  %35 = load float, float* %arrayidx107, align 4, !tbaa !8
  br label %cond.end108

cond.end108:                                      ; preds = %cond.false104, %cond.true100
  %cond109 = phi float [ %34, %cond.true100 ], [ %35, %cond.false104 ]
  %m_max110 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call111 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max110)
  %arrayidx112 = getelementptr inbounds float, float* %call111, i32 2
  store float %cond109, float* %arrayidx112, align 4, !tbaa !8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN17GIM_BVH_TREE_NODE14setEscapeIndexEi(%class.GIM_BVH_TREE_NODE* %this, i32 %index) #2 comdat {
entry:
  %this.addr = alloca %class.GIM_BVH_TREE_NODE*, align 4
  %index.addr = alloca i32, align 4
  store %class.GIM_BVH_TREE_NODE* %this, %class.GIM_BVH_TREE_NODE** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !6
  %this1 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %this.addr, align 4
  %0 = load i32, i32* %index.addr, align 4, !tbaa !6
  %sub = sub nsw i32 0, %0
  %m_escapeIndexOrDataIndex = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %this1, i32 0, i32 1
  store i32 %sub, i32* %m_escapeIndexOrDataIndex, align 4, !tbaa !30
  ret void
}

define hidden void @_ZN9btBvhTree10build_treeER18GIM_BVH_DATA_ARRAY(%class.btBvhTree* %this, %class.GIM_BVH_DATA_ARRAY* nonnull align 4 dereferenceable(17) %primitive_boxes) #0 {
entry:
  %this.addr = alloca %class.btBvhTree*, align 4
  %primitive_boxes.addr = alloca %class.GIM_BVH_DATA_ARRAY*, align 4
  %ref.tmp = alloca %class.GIM_BVH_TREE_NODE, align 4
  store %class.btBvhTree* %this, %class.btBvhTree** %this.addr, align 4, !tbaa !2
  store %class.GIM_BVH_DATA_ARRAY* %primitive_boxes, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4, !tbaa !2
  %this1 = load %class.btBvhTree*, %class.btBvhTree** %this.addr, align 4
  %m_num_nodes = getelementptr inbounds %class.btBvhTree, %class.btBvhTree* %this1, i32 0, i32 0
  store i32 0, i32* %m_num_nodes, align 4, !tbaa !19
  %m_node_array = getelementptr inbounds %class.btBvhTree, %class.btBvhTree* %this1, i32 0, i32 1
  %0 = bitcast %class.GIM_BVH_TREE_NODE_ARRAY* %m_node_array to %class.btAlignedObjectArray*
  %1 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4, !tbaa !2
  %2 = bitcast %class.GIM_BVH_DATA_ARRAY* %1 to %class.btAlignedObjectArray.0*
  %call = call i32 @_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE4sizeEv(%class.btAlignedObjectArray.0* %2)
  %mul = mul nsw i32 %call, 2
  %3 = bitcast %class.GIM_BVH_TREE_NODE* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 36, i8* %3) #8
  %call2 = call %class.GIM_BVH_TREE_NODE* @_ZN17GIM_BVH_TREE_NODEC2Ev(%class.GIM_BVH_TREE_NODE* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE6resizeEiRKS0_(%class.btAlignedObjectArray* %0, i32 %mul, %class.GIM_BVH_TREE_NODE* nonnull align 4 dereferenceable(36) %ref.tmp)
  %4 = bitcast %class.GIM_BVH_TREE_NODE* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 36, i8* %4) #8
  %5 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4, !tbaa !2
  %6 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4, !tbaa !2
  %7 = bitcast %class.GIM_BVH_DATA_ARRAY* %6 to %class.btAlignedObjectArray.0*
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE4sizeEv(%class.btAlignedObjectArray.0* %7)
  call void @_ZN9btBvhTree15_build_sub_treeER18GIM_BVH_DATA_ARRAYii(%class.btBvhTree* %this1, %class.GIM_BVH_DATA_ARRAY* nonnull align 4 dereferenceable(17) %5, i32 0, i32 %call3)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE6resizeEiRKS0_(%class.btAlignedObjectArray* %this, i32 %newsize, %class.GIM_BVH_TREE_NODE* nonnull align 4 dereferenceable(36) %fillData) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.GIM_BVH_TREE_NODE*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !6
  store %class.GIM_BVH_TREE_NODE* %fillData, %class.GIM_BVH_TREE_NODE** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !6
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %2 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  store i32 %4, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %m_data, align 4, !tbaa !27
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end17

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE4sizeEv(%class.btAlignedObjectArray* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load i32, i32* %curSize, align 4, !tbaa !6
  store i32 %14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc14, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !6
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  br label %for.end16

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %18 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %m_data11, align 4, !tbaa !27
  %19 = load i32, i32* %i6, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %18, i32 %19
  %20 = bitcast %class.GIM_BVH_TREE_NODE* %arrayidx12 to i8*
  %21 = bitcast i8* %20 to %class.GIM_BVH_TREE_NODE*
  %22 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %fillData.addr, align 4, !tbaa !2
  %call13 = call %class.GIM_BVH_TREE_NODE* @_ZN17GIM_BVH_TREE_NODEC2ERKS_(%class.GIM_BVH_TREE_NODE* %21, %class.GIM_BVH_TREE_NODE* nonnull align 4 dereferenceable(36) %22)
  br label %for.inc14

for.inc14:                                        ; preds = %for.body10
  %23 = load i32, i32* %i6, align 4, !tbaa !6
  %inc15 = add nsw i32 %23, 1
  store i32 %inc15, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.end16:                                        ; preds = %for.cond.cleanup9
  br label %if.end17

if.end17:                                         ; preds = %for.end16, %for.end
  %24 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %24, i32* %m_size, align 4, !tbaa !32
  %25 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE4sizeEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !33
  ret i32 %0
}

define linkonce_odr hidden %class.GIM_BVH_TREE_NODE* @_ZN17GIM_BVH_TREE_NODEC2Ev(%class.GIM_BVH_TREE_NODE* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.GIM_BVH_TREE_NODE*, align 4
  store %class.GIM_BVH_TREE_NODE* %this, %class.GIM_BVH_TREE_NODE** %this.addr, align 4, !tbaa !2
  %this1 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %this.addr, align 4
  %m_bound = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %this1, i32 0, i32 0
  %call = call %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* %m_bound)
  %m_escapeIndexOrDataIndex = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %this1, i32 0, i32 1
  store i32 0, i32* %m_escapeIndexOrDataIndex, align 4, !tbaa !30
  ret %class.GIM_BVH_TREE_NODE* %this1
}

define hidden void @_ZN12btGImpactBvh5refitEv(%class.btGImpactBvh* %this) #0 {
entry:
  %this.addr = alloca %class.btGImpactBvh*, align 4
  %nodecount = alloca i32, align 4
  %leafbox = alloca %class.btAABB, align 4
  %bound = alloca %class.btAABB, align 4
  %temp_box = alloca %class.btAABB, align 4
  %child_node = alloca i32, align 4
  store %class.btGImpactBvh* %this, %class.btGImpactBvh** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactBvh*, %class.btGImpactBvh** %this.addr, align 4
  %0 = bitcast i32* %nodecount to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK12btGImpactBvh12getNodeCountEv(%class.btGImpactBvh* %this1)
  store i32 %call, i32* %nodecount, align 4, !tbaa !6
  br label %while.cond

while.cond:                                       ; preds = %if.end14, %entry
  %1 = load i32, i32* %nodecount, align 4, !tbaa !6
  %dec = add nsw i32 %1, -1
  store i32 %dec, i32* %nodecount, align 4, !tbaa !6
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load i32, i32* %nodecount, align 4, !tbaa !6
  %call2 = call zeroext i1 @_ZNK12btGImpactBvh10isLeafNodeEi(%class.btGImpactBvh* %this1, i32 %2)
  br i1 %call2, label %if.then, label %if.else

if.then:                                          ; preds = %while.body
  %3 = bitcast %class.btAABB* %leafbox to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %3) #8
  %call3 = call %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* %leafbox)
  %m_primitive_manager = getelementptr inbounds %class.btGImpactBvh, %class.btGImpactBvh* %this1, i32 0, i32 1
  %4 = load %class.btPrimitiveManagerBase*, %class.btPrimitiveManagerBase** %m_primitive_manager, align 4, !tbaa !34
  %5 = load i32, i32* %nodecount, align 4, !tbaa !6
  %call4 = call i32 @_ZNK12btGImpactBvh11getNodeDataEi(%class.btGImpactBvh* %this1, i32 %5)
  %6 = bitcast %class.btPrimitiveManagerBase* %4 to void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)***
  %vtable = load void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)**, void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)*** %6, align 4, !tbaa !36
  %vfn = getelementptr inbounds void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)*, void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)** %vtable, i64 4
  %7 = load void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)*, void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)** %vfn, align 4
  call void %7(%class.btPrimitiveManagerBase* %4, i32 %call4, %class.btAABB* nonnull align 4 dereferenceable(32) %leafbox)
  %8 = load i32, i32* %nodecount, align 4, !tbaa !6
  call void @_ZN12btGImpactBvh12setNodeBoundEiRK6btAABB(%class.btGImpactBvh* %this1, i32 %8, %class.btAABB* nonnull align 4 dereferenceable(32) %leafbox)
  %9 = bitcast %class.btAABB* %leafbox to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %9) #8
  br label %if.end14

if.else:                                          ; preds = %while.body
  %10 = bitcast %class.btAABB* %bound to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %10) #8
  %call5 = call %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* %bound)
  call void @_ZN6btAABB10invalidateEv(%class.btAABB* %bound)
  %11 = bitcast %class.btAABB* %temp_box to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %11) #8
  %call6 = call %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* %temp_box)
  %12 = bitcast i32* %child_node to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  %13 = load i32, i32* %nodecount, align 4, !tbaa !6
  %call7 = call i32 @_ZNK12btGImpactBvh11getLeftNodeEi(%class.btGImpactBvh* %this1, i32 %13)
  store i32 %call7, i32* %child_node, align 4, !tbaa !6
  %14 = load i32, i32* %child_node, align 4, !tbaa !6
  %tobool8 = icmp ne i32 %14, 0
  br i1 %tobool8, label %if.then9, label %if.end

if.then9:                                         ; preds = %if.else
  %15 = load i32, i32* %child_node, align 4, !tbaa !6
  call void @_ZNK12btGImpactBvh12getNodeBoundEiR6btAABB(%class.btGImpactBvh* %this1, i32 %15, %class.btAABB* nonnull align 4 dereferenceable(32) %temp_box)
  call void @_ZN6btAABB5mergeERKS_(%class.btAABB* %bound, %class.btAABB* nonnull align 4 dereferenceable(32) %temp_box)
  br label %if.end

if.end:                                           ; preds = %if.then9, %if.else
  %16 = load i32, i32* %nodecount, align 4, !tbaa !6
  %call10 = call i32 @_ZNK12btGImpactBvh12getRightNodeEi(%class.btGImpactBvh* %this1, i32 %16)
  store i32 %call10, i32* %child_node, align 4, !tbaa !6
  %17 = load i32, i32* %child_node, align 4, !tbaa !6
  %tobool11 = icmp ne i32 %17, 0
  br i1 %tobool11, label %if.then12, label %if.end13

if.then12:                                        ; preds = %if.end
  %18 = load i32, i32* %child_node, align 4, !tbaa !6
  call void @_ZNK12btGImpactBvh12getNodeBoundEiR6btAABB(%class.btGImpactBvh* %this1, i32 %18, %class.btAABB* nonnull align 4 dereferenceable(32) %temp_box)
  call void @_ZN6btAABB5mergeERKS_(%class.btAABB* %bound, %class.btAABB* nonnull align 4 dereferenceable(32) %temp_box)
  br label %if.end13

if.end13:                                         ; preds = %if.then12, %if.end
  %19 = load i32, i32* %nodecount, align 4, !tbaa !6
  call void @_ZN12btGImpactBvh12setNodeBoundEiRK6btAABB(%class.btGImpactBvh* %this1, i32 %19, %class.btAABB* nonnull align 4 dereferenceable(32) %bound)
  %20 = bitcast i32* %child_node to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #8
  %21 = bitcast %class.btAABB* %temp_box to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %21) #8
  %22 = bitcast %class.btAABB* %bound to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %22) #8
  br label %if.end14

if.end14:                                         ; preds = %if.end13, %if.then
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %23 = bitcast i32* %nodecount to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden i32 @_ZNK12btGImpactBvh12getNodeCountEv(%class.btGImpactBvh* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btGImpactBvh*, align 4
  store %class.btGImpactBvh* %this, %class.btGImpactBvh** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactBvh*, %class.btGImpactBvh** %this.addr, align 4
  %m_box_tree = getelementptr inbounds %class.btGImpactBvh, %class.btGImpactBvh* %this1, i32 0, i32 0
  %call = call i32 @_ZNK9btBvhTree12getNodeCountEv(%class.btBvhTree* %m_box_tree)
  ret i32 %call
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_ZNK12btGImpactBvh10isLeafNodeEi(%class.btGImpactBvh* %this, i32 %nodeindex) #3 comdat {
entry:
  %this.addr = alloca %class.btGImpactBvh*, align 4
  %nodeindex.addr = alloca i32, align 4
  store %class.btGImpactBvh* %this, %class.btGImpactBvh** %this.addr, align 4, !tbaa !2
  store i32 %nodeindex, i32* %nodeindex.addr, align 4, !tbaa !6
  %this1 = load %class.btGImpactBvh*, %class.btGImpactBvh** %this.addr, align 4
  %m_box_tree = getelementptr inbounds %class.btGImpactBvh, %class.btGImpactBvh* %this1, i32 0, i32 0
  %0 = load i32, i32* %nodeindex.addr, align 4, !tbaa !6
  %call = call zeroext i1 @_ZNK9btBvhTree10isLeafNodeEi(%class.btBvhTree* %m_box_tree, i32 %0)
  ret i1 %call
}

; Function Attrs: inlinehint
define linkonce_odr hidden i32 @_ZNK12btGImpactBvh11getNodeDataEi(%class.btGImpactBvh* %this, i32 %nodeindex) #3 comdat {
entry:
  %this.addr = alloca %class.btGImpactBvh*, align 4
  %nodeindex.addr = alloca i32, align 4
  store %class.btGImpactBvh* %this, %class.btGImpactBvh** %this.addr, align 4, !tbaa !2
  store i32 %nodeindex, i32* %nodeindex.addr, align 4, !tbaa !6
  %this1 = load %class.btGImpactBvh*, %class.btGImpactBvh** %this.addr, align 4
  %m_box_tree = getelementptr inbounds %class.btGImpactBvh, %class.btGImpactBvh* %this1, i32 0, i32 0
  %0 = load i32, i32* %nodeindex.addr, align 4, !tbaa !6
  %call = call i32 @_ZNK9btBvhTree11getNodeDataEi(%class.btBvhTree* %m_box_tree, i32 %0)
  ret i32 %call
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN12btGImpactBvh12setNodeBoundEiRK6btAABB(%class.btGImpactBvh* %this, i32 %nodeindex, %class.btAABB* nonnull align 4 dereferenceable(32) %bound) #3 comdat {
entry:
  %this.addr = alloca %class.btGImpactBvh*, align 4
  %nodeindex.addr = alloca i32, align 4
  %bound.addr = alloca %class.btAABB*, align 4
  store %class.btGImpactBvh* %this, %class.btGImpactBvh** %this.addr, align 4, !tbaa !2
  store i32 %nodeindex, i32* %nodeindex.addr, align 4, !tbaa !6
  store %class.btAABB* %bound, %class.btAABB** %bound.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactBvh*, %class.btGImpactBvh** %this.addr, align 4
  %m_box_tree = getelementptr inbounds %class.btGImpactBvh, %class.btGImpactBvh* %this1, i32 0, i32 0
  %0 = load i32, i32* %nodeindex.addr, align 4, !tbaa !6
  %1 = load %class.btAABB*, %class.btAABB** %bound.addr, align 4, !tbaa !2
  call void @_ZN9btBvhTree12setNodeBoundEiRK6btAABB(%class.btBvhTree* %m_box_tree, i32 %0, %class.btAABB* nonnull align 4 dereferenceable(32) %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden i32 @_ZNK12btGImpactBvh11getLeftNodeEi(%class.btGImpactBvh* %this, i32 %nodeindex) #3 comdat {
entry:
  %this.addr = alloca %class.btGImpactBvh*, align 4
  %nodeindex.addr = alloca i32, align 4
  store %class.btGImpactBvh* %this, %class.btGImpactBvh** %this.addr, align 4, !tbaa !2
  store i32 %nodeindex, i32* %nodeindex.addr, align 4, !tbaa !6
  %this1 = load %class.btGImpactBvh*, %class.btGImpactBvh** %this.addr, align 4
  %m_box_tree = getelementptr inbounds %class.btGImpactBvh, %class.btGImpactBvh* %this1, i32 0, i32 0
  %0 = load i32, i32* %nodeindex.addr, align 4, !tbaa !6
  %call = call i32 @_ZNK9btBvhTree11getLeftNodeEi(%class.btBvhTree* %m_box_tree, i32 %0)
  ret i32 %call
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK12btGImpactBvh12getNodeBoundEiR6btAABB(%class.btGImpactBvh* %this, i32 %nodeindex, %class.btAABB* nonnull align 4 dereferenceable(32) %bound) #3 comdat {
entry:
  %this.addr = alloca %class.btGImpactBvh*, align 4
  %nodeindex.addr = alloca i32, align 4
  %bound.addr = alloca %class.btAABB*, align 4
  store %class.btGImpactBvh* %this, %class.btGImpactBvh** %this.addr, align 4, !tbaa !2
  store i32 %nodeindex, i32* %nodeindex.addr, align 4, !tbaa !6
  store %class.btAABB* %bound, %class.btAABB** %bound.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactBvh*, %class.btGImpactBvh** %this.addr, align 4
  %m_box_tree = getelementptr inbounds %class.btGImpactBvh, %class.btGImpactBvh* %this1, i32 0, i32 0
  %0 = load i32, i32* %nodeindex.addr, align 4, !tbaa !6
  %1 = load %class.btAABB*, %class.btAABB** %bound.addr, align 4, !tbaa !2
  call void @_ZNK9btBvhTree12getNodeBoundEiR6btAABB(%class.btBvhTree* %m_box_tree, i32 %0, %class.btAABB* nonnull align 4 dereferenceable(32) %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden i32 @_ZNK12btGImpactBvh12getRightNodeEi(%class.btGImpactBvh* %this, i32 %nodeindex) #3 comdat {
entry:
  %this.addr = alloca %class.btGImpactBvh*, align 4
  %nodeindex.addr = alloca i32, align 4
  store %class.btGImpactBvh* %this, %class.btGImpactBvh** %this.addr, align 4, !tbaa !2
  store i32 %nodeindex, i32* %nodeindex.addr, align 4, !tbaa !6
  %this1 = load %class.btGImpactBvh*, %class.btGImpactBvh** %this.addr, align 4
  %m_box_tree = getelementptr inbounds %class.btGImpactBvh, %class.btGImpactBvh* %this1, i32 0, i32 0
  %0 = load i32, i32* %nodeindex.addr, align 4, !tbaa !6
  %call = call i32 @_ZNK9btBvhTree12getRightNodeEi(%class.btBvhTree* %m_box_tree, i32 %0)
  ret i32 %call
}

define hidden void @_ZN12btGImpactBvh8buildSetEv(%class.btGImpactBvh* %this) #0 {
entry:
  %this.addr = alloca %class.btGImpactBvh*, align 4
  %primitive_boxes = alloca %class.GIM_BVH_DATA_ARRAY, align 4
  %ref.tmp = alloca %struct.GIM_BVH_DATA, align 4
  %i = alloca i32, align 4
  store %class.btGImpactBvh* %this, %class.btGImpactBvh** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactBvh*, %class.btGImpactBvh** %this.addr, align 4
  %0 = bitcast %class.GIM_BVH_DATA_ARRAY* %primitive_boxes to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %0) #8
  %call = call %class.GIM_BVH_DATA_ARRAY* @_ZN18GIM_BVH_DATA_ARRAYC2Ev(%class.GIM_BVH_DATA_ARRAY* %primitive_boxes)
  %1 = bitcast %class.GIM_BVH_DATA_ARRAY* %primitive_boxes to %class.btAlignedObjectArray.0*
  %m_primitive_manager = getelementptr inbounds %class.btGImpactBvh, %class.btGImpactBvh* %this1, i32 0, i32 1
  %2 = load %class.btPrimitiveManagerBase*, %class.btPrimitiveManagerBase** %m_primitive_manager, align 4, !tbaa !34
  %3 = bitcast %class.btPrimitiveManagerBase* %2 to i32 (%class.btPrimitiveManagerBase*)***
  %vtable = load i32 (%class.btPrimitiveManagerBase*)**, i32 (%class.btPrimitiveManagerBase*)*** %3, align 4, !tbaa !36
  %vfn = getelementptr inbounds i32 (%class.btPrimitiveManagerBase*)*, i32 (%class.btPrimitiveManagerBase*)** %vtable, i64 3
  %4 = load i32 (%class.btPrimitiveManagerBase*)*, i32 (%class.btPrimitiveManagerBase*)** %vfn, align 4
  %call2 = call i32 %4(%class.btPrimitiveManagerBase* %2)
  %5 = bitcast %struct.GIM_BVH_DATA* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 36, i8* %5) #8
  %6 = bitcast %struct.GIM_BVH_DATA* %ref.tmp to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %6, i8 0, i32 36, i1 false)
  %call3 = call %struct.GIM_BVH_DATA* @_ZN12GIM_BVH_DATAC2Ev(%struct.GIM_BVH_DATA* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE6resizeEiRKS0_(%class.btAlignedObjectArray.0* %1, i32 %call2, %struct.GIM_BVH_DATA* nonnull align 4 dereferenceable(36) %ref.tmp)
  %7 = bitcast %struct.GIM_BVH_DATA* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 36, i8* %7) #8
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %10 = bitcast %class.GIM_BVH_DATA_ARRAY* %primitive_boxes to %class.btAlignedObjectArray.0*
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE4sizeEv(%class.btAlignedObjectArray.0* %10)
  %cmp = icmp slt i32 %9, %call4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_primitive_manager5 = getelementptr inbounds %class.btGImpactBvh, %class.btGImpactBvh* %this1, i32 0, i32 1
  %12 = load %class.btPrimitiveManagerBase*, %class.btPrimitiveManagerBase** %m_primitive_manager5, align 4, !tbaa !34
  %13 = load i32, i32* %i, align 4, !tbaa !6
  %14 = bitcast %class.GIM_BVH_DATA_ARRAY* %primitive_boxes to %class.btAlignedObjectArray.0*
  %15 = load i32, i32* %i, align 4, !tbaa !6
  %call6 = call nonnull align 4 dereferenceable(36) %struct.GIM_BVH_DATA* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEixEi(%class.btAlignedObjectArray.0* %14, i32 %15)
  %m_bound = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %call6, i32 0, i32 0
  %16 = bitcast %class.btPrimitiveManagerBase* %12 to void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)***
  %vtable7 = load void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)**, void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)*** %16, align 4, !tbaa !36
  %vfn8 = getelementptr inbounds void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)*, void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)** %vtable7, i64 4
  %17 = load void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)*, void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)** %vfn8, align 4
  call void %17(%class.btPrimitiveManagerBase* %12, i32 %13, %class.btAABB* nonnull align 4 dereferenceable(32) %m_bound)
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %19 = bitcast %class.GIM_BVH_DATA_ARRAY* %primitive_boxes to %class.btAlignedObjectArray.0*
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %call9 = call nonnull align 4 dereferenceable(36) %struct.GIM_BVH_DATA* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEixEi(%class.btAlignedObjectArray.0* %19, i32 %20)
  %m_data = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %call9, i32 0, i32 1
  store i32 %18, i32* %m_data, align 4, !tbaa !22
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %21 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %m_box_tree = getelementptr inbounds %class.btGImpactBvh, %class.btGImpactBvh* %this1, i32 0, i32 0
  call void @_ZN9btBvhTree10build_treeER18GIM_BVH_DATA_ARRAY(%class.btBvhTree* %m_box_tree, %class.GIM_BVH_DATA_ARRAY* nonnull align 4 dereferenceable(17) %primitive_boxes)
  %call10 = call %class.GIM_BVH_DATA_ARRAY* bitcast (%class.btAlignedObjectArray.0* (%class.btAlignedObjectArray.0*)* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAED2Ev to %class.GIM_BVH_DATA_ARRAY* (%class.GIM_BVH_DATA_ARRAY*)*)(%class.GIM_BVH_DATA_ARRAY* %primitive_boxes) #8
  %22 = bitcast %class.GIM_BVH_DATA_ARRAY* %primitive_boxes to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %22) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.GIM_BVH_DATA_ARRAY* @_ZN18GIM_BVH_DATA_ARRAYC2Ev(%class.GIM_BVH_DATA_ARRAY* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.GIM_BVH_DATA_ARRAY*, align 4
  store %class.GIM_BVH_DATA_ARRAY* %this, %class.GIM_BVH_DATA_ARRAY** %this.addr, align 4, !tbaa !2
  %this1 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %this.addr, align 4
  %0 = bitcast %class.GIM_BVH_DATA_ARRAY* %this1 to %class.btAlignedObjectArray.0*
  %call = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEC2Ev(%class.btAlignedObjectArray.0* %0)
  ret %class.GIM_BVH_DATA_ARRAY* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE6resizeEiRKS0_(%class.btAlignedObjectArray.0* %this, i32 %newsize, %struct.GIM_BVH_DATA* nonnull align 4 dereferenceable(36) %fillData) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %struct.GIM_BVH_DATA*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !6
  store %struct.GIM_BVH_DATA* %fillData, %struct.GIM_BVH_DATA** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !6
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %2 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  store i32 %4, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %8 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %m_data, align 4, !tbaa !12
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end17

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load i32, i32* %curSize, align 4, !tbaa !6
  store i32 %14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc14, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !6
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  br label %for.end16

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %18 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %m_data11, align 4, !tbaa !12
  %19 = load i32, i32* %i6, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %18, i32 %19
  %20 = bitcast %struct.GIM_BVH_DATA* %arrayidx12 to i8*
  %21 = bitcast i8* %20 to %struct.GIM_BVH_DATA*
  %22 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %fillData.addr, align 4, !tbaa !2
  %call13 = call %struct.GIM_BVH_DATA* @_ZN12GIM_BVH_DATAC2ERKS_(%struct.GIM_BVH_DATA* %21, %struct.GIM_BVH_DATA* nonnull align 4 dereferenceable(36) %22)
  br label %for.inc14

for.inc14:                                        ; preds = %for.body10
  %23 = load i32, i32* %i6, align 4, !tbaa !6
  %inc15 = add nsw i32 %23, 1
  store i32 %inc15, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.end16:                                        ; preds = %for.cond.cleanup9
  br label %if.end17

if.end17:                                         ; preds = %for.end16, %for.end
  %24 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 %24, i32* %m_size, align 4, !tbaa !33
  %25 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #8
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #4

; Function Attrs: inlinehint
define linkonce_odr hidden %struct.GIM_BVH_DATA* @_ZN12GIM_BVH_DATAC2Ev(%struct.GIM_BVH_DATA* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %struct.GIM_BVH_DATA*, align 4
  store %struct.GIM_BVH_DATA* %this, %struct.GIM_BVH_DATA** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %this.addr, align 4
  %m_bound = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %this1, i32 0, i32 0
  %call = call %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* %m_bound)
  ret %struct.GIM_BVH_DATA* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAED2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE5clearEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

define hidden zeroext i1 @_ZNK12btGImpactBvh8boxQueryERK6btAABBR20btAlignedObjectArrayIiE(%class.btGImpactBvh* %this, %class.btAABB* nonnull align 4 dereferenceable(32) %box, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17) %collided_results) #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btGImpactBvh*, align 4
  %box.addr = alloca %class.btAABB*, align 4
  %collided_results.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %curIndex = alloca i32, align 4
  %numNodes = alloca i32, align 4
  %bound = alloca %class.btAABB, align 4
  %aabbOverlap = alloca i8, align 1
  %isleafnode = alloca i8, align 1
  %ref.tmp = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btGImpactBvh* %this, %class.btGImpactBvh** %this.addr, align 4, !tbaa !2
  store %class.btAABB* %box, %class.btAABB** %box.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.4* %collided_results, %class.btAlignedObjectArray.4** %collided_results.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactBvh*, %class.btGImpactBvh** %this.addr, align 4
  %0 = bitcast i32* %curIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %curIndex, align 4, !tbaa !6
  %1 = bitcast i32* %numNodes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %call = call i32 @_ZNK12btGImpactBvh12getNodeCountEv(%class.btGImpactBvh* %this1)
  store i32 %call, i32* %numNodes, align 4, !tbaa !6
  br label %while.cond

while.cond:                                       ; preds = %if.end12, %entry
  %2 = load i32, i32* %curIndex, align 4, !tbaa !6
  %3 = load i32, i32* %numNodes, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %4 = bitcast %class.btAABB* %bound to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %4) #8
  %call2 = call %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* %bound)
  %5 = load i32, i32* %curIndex, align 4, !tbaa !6
  call void @_ZNK12btGImpactBvh12getNodeBoundEiR6btAABB(%class.btGImpactBvh* %this1, i32 %5, %class.btAABB* nonnull align 4 dereferenceable(32) %bound)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %aabbOverlap) #8
  %6 = load %class.btAABB*, %class.btAABB** %box.addr, align 4, !tbaa !2
  %call3 = call zeroext i1 @_ZNK6btAABB13has_collisionERKS_(%class.btAABB* %bound, %class.btAABB* nonnull align 4 dereferenceable(32) %6)
  %frombool = zext i1 %call3 to i8
  store i8 %frombool, i8* %aabbOverlap, align 1, !tbaa !16
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %isleafnode) #8
  %7 = load i32, i32* %curIndex, align 4, !tbaa !6
  %call4 = call zeroext i1 @_ZNK12btGImpactBvh10isLeafNodeEi(%class.btGImpactBvh* %this1, i32 %7)
  %frombool5 = zext i1 %call4 to i8
  store i8 %frombool5, i8* %isleafnode, align 1, !tbaa !16
  %8 = load i8, i8* %isleafnode, align 1, !tbaa !16, !range !17
  %tobool = trunc i8 %8 to i1
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %while.body
  %9 = load i8, i8* %aabbOverlap, align 1, !tbaa !16, !range !17
  %tobool6 = trunc i8 %9 to i1
  br i1 %tobool6, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %10 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %collided_results.addr, align 4, !tbaa !2
  %11 = bitcast i32* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  %12 = load i32, i32* %curIndex, align 4, !tbaa !6
  %call7 = call i32 @_ZNK12btGImpactBvh11getNodeDataEi(%class.btGImpactBvh* %this1, i32 %12)
  store i32 %call7, i32* %ref.tmp, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.4* %10, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  %13 = bitcast i32* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %while.body
  %14 = load i8, i8* %aabbOverlap, align 1, !tbaa !16, !range !17
  %tobool8 = trunc i8 %14 to i1
  br i1 %tobool8, label %if.then10, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %15 = load i8, i8* %isleafnode, align 1, !tbaa !16, !range !17
  %tobool9 = trunc i8 %15 to i1
  br i1 %tobool9, label %if.then10, label %if.else

if.then10:                                        ; preds = %lor.lhs.false, %if.end
  %16 = load i32, i32* %curIndex, align 4, !tbaa !6
  %inc = add nsw i32 %16, 1
  store i32 %inc, i32* %curIndex, align 4, !tbaa !6
  br label %if.end12

if.else:                                          ; preds = %lor.lhs.false
  %17 = load i32, i32* %curIndex, align 4, !tbaa !6
  %call11 = call i32 @_ZNK12btGImpactBvh18getEscapeNodeIndexEi(%class.btGImpactBvh* %this1, i32 %17)
  %18 = load i32, i32* %curIndex, align 4, !tbaa !6
  %add = add nsw i32 %18, %call11
  store i32 %add, i32* %curIndex, align 4, !tbaa !6
  br label %if.end12

if.end12:                                         ; preds = %if.else, %if.then10
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %isleafnode) #8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %aabbOverlap) #8
  %19 = bitcast %class.btAABB* %bound to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %19) #8
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %20 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %collided_results.addr, align 4, !tbaa !2
  %call13 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %20)
  %cmp14 = icmp sgt i32 %call13, 0
  br i1 %cmp14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %while.end
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end16:                                         ; preds = %while.end
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end16, %if.then15
  %21 = bitcast i32* %numNodes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  %22 = bitcast i32* %curIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #8
  %23 = load i1, i1* %retval, align 1
  ret i1 %23
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZNK6btAABB13has_collisionERKS_(%class.btAABB* %this, %class.btAABB* nonnull align 4 dereferenceable(32) %other) #2 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btAABB*, align 4
  %other.addr = alloca %class.btAABB*, align 4
  store %class.btAABB* %this, %class.btAABB** %this.addr, align 4, !tbaa !2
  store %class.btAABB* %other, %class.btAABB** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btAABB*, %class.btAABB** %this.addr, align 4
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !8
  %1 = load %class.btAABB*, %class.btAABB** %other.addr, align 4, !tbaa !2
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %1, i32 0, i32 1
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !8
  %cmp = fcmp ogt float %0, %2
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %m_max4 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max4)
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 0
  %3 = load float, float* %arrayidx6, align 4, !tbaa !8
  %4 = load %class.btAABB*, %class.btAABB** %other.addr, align 4, !tbaa !2
  %m_min7 = getelementptr inbounds %class.btAABB, %class.btAABB* %4, i32 0, i32 0
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min7)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 0
  %5 = load float, float* %arrayidx9, align 4, !tbaa !8
  %cmp10 = fcmp olt float %3, %5
  br i1 %cmp10, label %if.then, label %lor.lhs.false11

lor.lhs.false11:                                  ; preds = %lor.lhs.false
  %m_min12 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call13 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min12)
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 1
  %6 = load float, float* %arrayidx14, align 4, !tbaa !8
  %7 = load %class.btAABB*, %class.btAABB** %other.addr, align 4, !tbaa !2
  %m_max15 = getelementptr inbounds %class.btAABB, %class.btAABB* %7, i32 0, i32 1
  %call16 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max15)
  %arrayidx17 = getelementptr inbounds float, float* %call16, i32 1
  %8 = load float, float* %arrayidx17, align 4, !tbaa !8
  %cmp18 = fcmp ogt float %6, %8
  br i1 %cmp18, label %if.then, label %lor.lhs.false19

lor.lhs.false19:                                  ; preds = %lor.lhs.false11
  %m_max20 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call21 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max20)
  %arrayidx22 = getelementptr inbounds float, float* %call21, i32 1
  %9 = load float, float* %arrayidx22, align 4, !tbaa !8
  %10 = load %class.btAABB*, %class.btAABB** %other.addr, align 4, !tbaa !2
  %m_min23 = getelementptr inbounds %class.btAABB, %class.btAABB* %10, i32 0, i32 0
  %call24 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min23)
  %arrayidx25 = getelementptr inbounds float, float* %call24, i32 1
  %11 = load float, float* %arrayidx25, align 4, !tbaa !8
  %cmp26 = fcmp olt float %9, %11
  br i1 %cmp26, label %if.then, label %lor.lhs.false27

lor.lhs.false27:                                  ; preds = %lor.lhs.false19
  %m_min28 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call29 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min28)
  %arrayidx30 = getelementptr inbounds float, float* %call29, i32 2
  %12 = load float, float* %arrayidx30, align 4, !tbaa !8
  %13 = load %class.btAABB*, %class.btAABB** %other.addr, align 4, !tbaa !2
  %m_max31 = getelementptr inbounds %class.btAABB, %class.btAABB* %13, i32 0, i32 1
  %call32 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max31)
  %arrayidx33 = getelementptr inbounds float, float* %call32, i32 2
  %14 = load float, float* %arrayidx33, align 4, !tbaa !8
  %cmp34 = fcmp ogt float %12, %14
  br i1 %cmp34, label %if.then, label %lor.lhs.false35

lor.lhs.false35:                                  ; preds = %lor.lhs.false27
  %m_max36 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call37 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max36)
  %arrayidx38 = getelementptr inbounds float, float* %call37, i32 2
  %15 = load float, float* %arrayidx38, align 4, !tbaa !8
  %16 = load %class.btAABB*, %class.btAABB** %other.addr, align 4, !tbaa !2
  %m_min39 = getelementptr inbounds %class.btAABB, %class.btAABB* %16, i32 0, i32 0
  %call40 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min39)
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 2
  %17 = load float, float* %arrayidx41, align 4, !tbaa !8
  %cmp42 = fcmp olt float %15, %17
  br i1 %cmp42, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false35, %lor.lhs.false27, %lor.lhs.false19, %lor.lhs.false11, %lor.lhs.false, %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %lor.lhs.false35
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %18 = load i1, i1* %retval, align 1
  ret i1 %18
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.4* %this, i32* nonnull align 4 dereferenceable(4) %_Val) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %_Val.addr = alloca i32*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32* %_Val, i32** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !6
  %1 = load i32, i32* %sz, align 4, !tbaa !6
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIiE9allocSizeEi(%class.btAlignedObjectArray.4* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.4* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data, align 4, !tbaa !38
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !41
  %arrayidx = getelementptr inbounds i32, i32* %2, i32 %3
  %4 = bitcast i32* %arrayidx to i8*
  %5 = bitcast i8* %4 to i32*
  %6 = load i32*, i32** %_Val.addr, align 4, !tbaa !2
  %7 = load i32, i32* %6, align 4, !tbaa !6
  store i32 %7, i32* %5, align 4, !tbaa !6
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size5, align 4, !tbaa !41
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size5, align 4, !tbaa !41
  %9 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden i32 @_ZNK12btGImpactBvh18getEscapeNodeIndexEi(%class.btGImpactBvh* %this, i32 %nodeindex) #3 comdat {
entry:
  %this.addr = alloca %class.btGImpactBvh*, align 4
  %nodeindex.addr = alloca i32, align 4
  store %class.btGImpactBvh* %this, %class.btGImpactBvh** %this.addr, align 4, !tbaa !2
  store i32 %nodeindex, i32* %nodeindex.addr, align 4, !tbaa !6
  %this1 = load %class.btGImpactBvh*, %class.btGImpactBvh** %this.addr, align 4
  %m_box_tree = getelementptr inbounds %class.btGImpactBvh, %class.btGImpactBvh* %this1, i32 0, i32 0
  %0 = load i32, i32* %nodeindex.addr, align 4, !tbaa !6
  %call = call i32 @_ZNK9btBvhTree18getEscapeNodeIndexEi(%class.btBvhTree* %m_box_tree, i32 %0)
  ret i32 %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !41
  ret i32 %0
}

define hidden zeroext i1 @_ZNK12btGImpactBvh8rayQueryERK9btVector3S2_R20btAlignedObjectArrayIiE(%class.btGImpactBvh* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %ray_dir, %class.btVector3* nonnull align 4 dereferenceable(16) %ray_origin, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17) %collided_results) #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btGImpactBvh*, align 4
  %ray_dir.addr = alloca %class.btVector3*, align 4
  %ray_origin.addr = alloca %class.btVector3*, align 4
  %collided_results.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %curIndex = alloca i32, align 4
  %numNodes = alloca i32, align 4
  %bound = alloca %class.btAABB, align 4
  %aabbOverlap = alloca i8, align 1
  %isleafnode = alloca i8, align 1
  %ref.tmp = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btGImpactBvh* %this, %class.btGImpactBvh** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %ray_dir, %class.btVector3** %ray_dir.addr, align 4, !tbaa !2
  store %class.btVector3* %ray_origin, %class.btVector3** %ray_origin.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.4* %collided_results, %class.btAlignedObjectArray.4** %collided_results.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactBvh*, %class.btGImpactBvh** %this.addr, align 4
  %0 = bitcast i32* %curIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %curIndex, align 4, !tbaa !6
  %1 = bitcast i32* %numNodes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %call = call i32 @_ZNK12btGImpactBvh12getNodeCountEv(%class.btGImpactBvh* %this1)
  store i32 %call, i32* %numNodes, align 4, !tbaa !6
  br label %while.cond

while.cond:                                       ; preds = %if.end12, %entry
  %2 = load i32, i32* %curIndex, align 4, !tbaa !6
  %3 = load i32, i32* %numNodes, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %4 = bitcast %class.btAABB* %bound to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %4) #8
  %call2 = call %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* %bound)
  %5 = load i32, i32* %curIndex, align 4, !tbaa !6
  call void @_ZNK12btGImpactBvh12getNodeBoundEiR6btAABB(%class.btGImpactBvh* %this1, i32 %5, %class.btAABB* nonnull align 4 dereferenceable(32) %bound)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %aabbOverlap) #8
  %6 = load %class.btVector3*, %class.btVector3** %ray_origin.addr, align 4, !tbaa !2
  %7 = load %class.btVector3*, %class.btVector3** %ray_dir.addr, align 4, !tbaa !2
  %call3 = call zeroext i1 @_ZNK6btAABB11collide_rayERK9btVector3S2_(%class.btAABB* %bound, %class.btVector3* nonnull align 4 dereferenceable(16) %6, %class.btVector3* nonnull align 4 dereferenceable(16) %7)
  %frombool = zext i1 %call3 to i8
  store i8 %frombool, i8* %aabbOverlap, align 1, !tbaa !16
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %isleafnode) #8
  %8 = load i32, i32* %curIndex, align 4, !tbaa !6
  %call4 = call zeroext i1 @_ZNK12btGImpactBvh10isLeafNodeEi(%class.btGImpactBvh* %this1, i32 %8)
  %frombool5 = zext i1 %call4 to i8
  store i8 %frombool5, i8* %isleafnode, align 1, !tbaa !16
  %9 = load i8, i8* %isleafnode, align 1, !tbaa !16, !range !17
  %tobool = trunc i8 %9 to i1
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %while.body
  %10 = load i8, i8* %aabbOverlap, align 1, !tbaa !16, !range !17
  %tobool6 = trunc i8 %10 to i1
  br i1 %tobool6, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %11 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %collided_results.addr, align 4, !tbaa !2
  %12 = bitcast i32* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  %13 = load i32, i32* %curIndex, align 4, !tbaa !6
  %call7 = call i32 @_ZNK12btGImpactBvh11getNodeDataEi(%class.btGImpactBvh* %this1, i32 %13)
  store i32 %call7, i32* %ref.tmp, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.4* %11, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  %14 = bitcast i32* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %while.body
  %15 = load i8, i8* %aabbOverlap, align 1, !tbaa !16, !range !17
  %tobool8 = trunc i8 %15 to i1
  br i1 %tobool8, label %if.then10, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %16 = load i8, i8* %isleafnode, align 1, !tbaa !16, !range !17
  %tobool9 = trunc i8 %16 to i1
  br i1 %tobool9, label %if.then10, label %if.else

if.then10:                                        ; preds = %lor.lhs.false, %if.end
  %17 = load i32, i32* %curIndex, align 4, !tbaa !6
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %curIndex, align 4, !tbaa !6
  br label %if.end12

if.else:                                          ; preds = %lor.lhs.false
  %18 = load i32, i32* %curIndex, align 4, !tbaa !6
  %call11 = call i32 @_ZNK12btGImpactBvh18getEscapeNodeIndexEi(%class.btGImpactBvh* %this1, i32 %18)
  %19 = load i32, i32* %curIndex, align 4, !tbaa !6
  %add = add nsw i32 %19, %call11
  store i32 %add, i32* %curIndex, align 4, !tbaa !6
  br label %if.end12

if.end12:                                         ; preds = %if.else, %if.then10
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %isleafnode) #8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %aabbOverlap) #8
  %20 = bitcast %class.btAABB* %bound to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %20) #8
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %21 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %collided_results.addr, align 4, !tbaa !2
  %call13 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %21)
  %cmp14 = icmp sgt i32 %call13, 0
  br i1 %cmp14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %while.end
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end16:                                         ; preds = %while.end
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end16, %if.then15
  %22 = bitcast i32* %numNodes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #8
  %23 = bitcast i32* %curIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #8
  %24 = load i1, i1* %retval, align 1
  ret i1 %24
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_ZNK6btAABB11collide_rayERK9btVector3S2_(%class.btAABB* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %vorigin, %class.btVector3* nonnull align 4 dereferenceable(16) %vdir) #3 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btAABB*, align 4
  %vorigin.addr = alloca %class.btVector3*, align 4
  %vdir.addr = alloca %class.btVector3*, align 4
  %extents = alloca %class.btVector3, align 4
  %center = alloca %class.btVector3, align 4
  %Dx = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %Dy = alloca float, align 4
  %Dz = alloca float, align 4
  %f = alloca float, align 4
  store %class.btAABB* %this, %class.btAABB** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %vorigin, %class.btVector3** %vorigin.addr, align 4, !tbaa !2
  store %class.btVector3* %vdir, %class.btVector3** %vdir.addr, align 4, !tbaa !2
  %this1 = load %class.btAABB*, %class.btAABB** %this.addr, align 4
  %0 = bitcast %class.btVector3* %extents to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %extents)
  %1 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #8
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %center)
  call void @_ZNK6btAABB17get_center_extendER9btVector3S1_(%class.btAABB* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extents)
  %2 = bitcast float* %Dx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load %class.btVector3*, %class.btVector3** %vorigin.addr, align 4, !tbaa !2
  %call3 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %3)
  %arrayidx = getelementptr inbounds float, float* %call3, i32 0
  %4 = load float, float* %arrayidx, align 4, !tbaa !8
  %call4 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 0
  %5 = load float, float* %arrayidx5, align 4, !tbaa !8
  %sub = fsub float %4, %5
  store float %sub, float* %Dx, align 4, !tbaa !8
  %6 = load float, float* %Dx, align 4, !tbaa !8
  %call6 = call float @_Z6btFabsf(float %6)
  %call7 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %extents)
  %arrayidx8 = getelementptr inbounds float, float* %call7, i32 0
  %7 = load float, float* %arrayidx8, align 4, !tbaa !8
  %cmp = fcmp ogt float %call6, %7
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %8 = load float, float* %Dx, align 4, !tbaa !8
  %9 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4, !tbaa !2
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %9)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 0
  %10 = load float, float* %arrayidx10, align 4, !tbaa !8
  %mul = fmul float %8, %10
  %cmp11 = fcmp oge float %mul, 0.000000e+00
  br i1 %cmp11, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup117

if.end:                                           ; preds = %land.lhs.true, %entry
  %11 = bitcast float* %Dy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  %12 = load %class.btVector3*, %class.btVector3** %vorigin.addr, align 4, !tbaa !2
  %call12 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %12)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 1
  %13 = load float, float* %arrayidx13, align 4, !tbaa !8
  %call14 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 1
  %14 = load float, float* %arrayidx15, align 4, !tbaa !8
  %sub16 = fsub float %13, %14
  store float %sub16, float* %Dy, align 4, !tbaa !8
  %15 = load float, float* %Dy, align 4, !tbaa !8
  %call17 = call float @_Z6btFabsf(float %15)
  %call18 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %extents)
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 1
  %16 = load float, float* %arrayidx19, align 4, !tbaa !8
  %cmp20 = fcmp ogt float %call17, %16
  br i1 %cmp20, label %land.lhs.true21, label %if.end27

land.lhs.true21:                                  ; preds = %if.end
  %17 = load float, float* %Dy, align 4, !tbaa !8
  %18 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4, !tbaa !2
  %call22 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %18)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 1
  %19 = load float, float* %arrayidx23, align 4, !tbaa !8
  %mul24 = fmul float %17, %19
  %cmp25 = fcmp oge float %mul24, 0.000000e+00
  br i1 %cmp25, label %if.then26, label %if.end27

if.then26:                                        ; preds = %land.lhs.true21
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup116

if.end27:                                         ; preds = %land.lhs.true21, %if.end
  %20 = bitcast float* %Dz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #8
  %21 = load %class.btVector3*, %class.btVector3** %vorigin.addr, align 4, !tbaa !2
  %call28 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %21)
  %arrayidx29 = getelementptr inbounds float, float* %call28, i32 2
  %22 = load float, float* %arrayidx29, align 4, !tbaa !8
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center)
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 2
  %23 = load float, float* %arrayidx31, align 4, !tbaa !8
  %sub32 = fsub float %22, %23
  store float %sub32, float* %Dz, align 4, !tbaa !8
  %24 = load float, float* %Dz, align 4, !tbaa !8
  %call33 = call float @_Z6btFabsf(float %24)
  %call34 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %extents)
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 2
  %25 = load float, float* %arrayidx35, align 4, !tbaa !8
  %cmp36 = fcmp ogt float %call33, %25
  br i1 %cmp36, label %land.lhs.true37, label %if.end43

land.lhs.true37:                                  ; preds = %if.end27
  %26 = load float, float* %Dz, align 4, !tbaa !8
  %27 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4, !tbaa !2
  %call38 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %27)
  %arrayidx39 = getelementptr inbounds float, float* %call38, i32 2
  %28 = load float, float* %arrayidx39, align 4, !tbaa !8
  %mul40 = fmul float %26, %28
  %cmp41 = fcmp oge float %mul40, 0.000000e+00
  br i1 %cmp41, label %if.then42, label %if.end43

if.then42:                                        ; preds = %land.lhs.true37
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup115

if.end43:                                         ; preds = %land.lhs.true37, %if.end27
  %29 = bitcast float* %f to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #8
  %30 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4, !tbaa !2
  %call44 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %30)
  %arrayidx45 = getelementptr inbounds float, float* %call44, i32 1
  %31 = load float, float* %arrayidx45, align 4, !tbaa !8
  %32 = load float, float* %Dz, align 4, !tbaa !8
  %mul46 = fmul float %31, %32
  %33 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4, !tbaa !2
  %call47 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %33)
  %arrayidx48 = getelementptr inbounds float, float* %call47, i32 2
  %34 = load float, float* %arrayidx48, align 4, !tbaa !8
  %35 = load float, float* %Dy, align 4, !tbaa !8
  %mul49 = fmul float %34, %35
  %sub50 = fsub float %mul46, %mul49
  store float %sub50, float* %f, align 4, !tbaa !8
  %36 = load float, float* %f, align 4, !tbaa !8
  %call51 = call float @_Z6btFabsf(float %36)
  %call52 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %extents)
  %arrayidx53 = getelementptr inbounds float, float* %call52, i32 1
  %37 = load float, float* %arrayidx53, align 4, !tbaa !8
  %38 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4, !tbaa !2
  %call54 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %38)
  %arrayidx55 = getelementptr inbounds float, float* %call54, i32 2
  %39 = load float, float* %arrayidx55, align 4, !tbaa !8
  %call56 = call float @_Z6btFabsf(float %39)
  %mul57 = fmul float %37, %call56
  %call58 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %extents)
  %arrayidx59 = getelementptr inbounds float, float* %call58, i32 2
  %40 = load float, float* %arrayidx59, align 4, !tbaa !8
  %41 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4, !tbaa !2
  %call60 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %41)
  %arrayidx61 = getelementptr inbounds float, float* %call60, i32 1
  %42 = load float, float* %arrayidx61, align 4, !tbaa !8
  %call62 = call float @_Z6btFabsf(float %42)
  %mul63 = fmul float %40, %call62
  %add = fadd float %mul57, %mul63
  %cmp64 = fcmp ogt float %call51, %add
  br i1 %cmp64, label %if.then65, label %if.end66

if.then65:                                        ; preds = %if.end43
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end66:                                         ; preds = %if.end43
  %43 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4, !tbaa !2
  %call67 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %43)
  %arrayidx68 = getelementptr inbounds float, float* %call67, i32 2
  %44 = load float, float* %arrayidx68, align 4, !tbaa !8
  %45 = load float, float* %Dx, align 4, !tbaa !8
  %mul69 = fmul float %44, %45
  %46 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4, !tbaa !2
  %call70 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %46)
  %arrayidx71 = getelementptr inbounds float, float* %call70, i32 0
  %47 = load float, float* %arrayidx71, align 4, !tbaa !8
  %48 = load float, float* %Dz, align 4, !tbaa !8
  %mul72 = fmul float %47, %48
  %sub73 = fsub float %mul69, %mul72
  store float %sub73, float* %f, align 4, !tbaa !8
  %49 = load float, float* %f, align 4, !tbaa !8
  %call74 = call float @_Z6btFabsf(float %49)
  %call75 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %extents)
  %arrayidx76 = getelementptr inbounds float, float* %call75, i32 0
  %50 = load float, float* %arrayidx76, align 4, !tbaa !8
  %51 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4, !tbaa !2
  %call77 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %51)
  %arrayidx78 = getelementptr inbounds float, float* %call77, i32 2
  %52 = load float, float* %arrayidx78, align 4, !tbaa !8
  %call79 = call float @_Z6btFabsf(float %52)
  %mul80 = fmul float %50, %call79
  %call81 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %extents)
  %arrayidx82 = getelementptr inbounds float, float* %call81, i32 2
  %53 = load float, float* %arrayidx82, align 4, !tbaa !8
  %54 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4, !tbaa !2
  %call83 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %54)
  %arrayidx84 = getelementptr inbounds float, float* %call83, i32 0
  %55 = load float, float* %arrayidx84, align 4, !tbaa !8
  %call85 = call float @_Z6btFabsf(float %55)
  %mul86 = fmul float %53, %call85
  %add87 = fadd float %mul80, %mul86
  %cmp88 = fcmp ogt float %call74, %add87
  br i1 %cmp88, label %if.then89, label %if.end90

if.then89:                                        ; preds = %if.end66
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end90:                                         ; preds = %if.end66
  %56 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4, !tbaa !2
  %call91 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %56)
  %arrayidx92 = getelementptr inbounds float, float* %call91, i32 0
  %57 = load float, float* %arrayidx92, align 4, !tbaa !8
  %58 = load float, float* %Dy, align 4, !tbaa !8
  %mul93 = fmul float %57, %58
  %59 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4, !tbaa !2
  %call94 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %59)
  %arrayidx95 = getelementptr inbounds float, float* %call94, i32 1
  %60 = load float, float* %arrayidx95, align 4, !tbaa !8
  %61 = load float, float* %Dx, align 4, !tbaa !8
  %mul96 = fmul float %60, %61
  %sub97 = fsub float %mul93, %mul96
  store float %sub97, float* %f, align 4, !tbaa !8
  %62 = load float, float* %f, align 4, !tbaa !8
  %call98 = call float @_Z6btFabsf(float %62)
  %call99 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %extents)
  %arrayidx100 = getelementptr inbounds float, float* %call99, i32 0
  %63 = load float, float* %arrayidx100, align 4, !tbaa !8
  %64 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4, !tbaa !2
  %call101 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %64)
  %arrayidx102 = getelementptr inbounds float, float* %call101, i32 1
  %65 = load float, float* %arrayidx102, align 4, !tbaa !8
  %call103 = call float @_Z6btFabsf(float %65)
  %mul104 = fmul float %63, %call103
  %call105 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %extents)
  %arrayidx106 = getelementptr inbounds float, float* %call105, i32 1
  %66 = load float, float* %arrayidx106, align 4, !tbaa !8
  %67 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4, !tbaa !2
  %call107 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %67)
  %arrayidx108 = getelementptr inbounds float, float* %call107, i32 0
  %68 = load float, float* %arrayidx108, align 4, !tbaa !8
  %call109 = call float @_Z6btFabsf(float %68)
  %mul110 = fmul float %66, %call109
  %add111 = fadd float %mul104, %mul110
  %cmp112 = fcmp ogt float %call98, %add111
  br i1 %cmp112, label %if.then113, label %if.end114

if.then113:                                       ; preds = %if.end90
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end114:                                        ; preds = %if.end90
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end114, %if.then113, %if.then89, %if.then65
  %69 = bitcast float* %f to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #8
  br label %cleanup115

cleanup115:                                       ; preds = %cleanup, %if.then42
  %70 = bitcast float* %Dz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #8
  br label %cleanup116

cleanup116:                                       ; preds = %cleanup115, %if.then26
  %71 = bitcast float* %Dy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #8
  br label %cleanup117

cleanup117:                                       ; preds = %cleanup116, %if.then
  %72 = bitcast float* %Dx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #8
  %73 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %73) #8
  %74 = bitcast %class.btVector3* %extents to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %74) #8
  %75 = load i1, i1* %retval, align 1
  ret i1 %75
}

define hidden void @_ZN12btGImpactBvh14find_collisionEPS_RK11btTransformS0_S3_R9btPairSet(%class.btGImpactBvh* %boxset0, %class.btTransform* nonnull align 4 dereferenceable(64) %trans0, %class.btGImpactBvh* %boxset1, %class.btTransform* nonnull align 4 dereferenceable(64) %trans1, %class.btPairSet* nonnull align 4 dereferenceable(17) %collision_pairs) #0 {
entry:
  %boxset0.addr = alloca %class.btGImpactBvh*, align 4
  %trans0.addr = alloca %class.btTransform*, align 4
  %boxset1.addr = alloca %class.btGImpactBvh*, align 4
  %trans1.addr = alloca %class.btTransform*, align 4
  %collision_pairs.addr = alloca %class.btPairSet*, align 4
  %trans_cache_1to0 = alloca %class.BT_BOX_BOX_TRANSFORM_CACHE, align 4
  store %class.btGImpactBvh* %boxset0, %class.btGImpactBvh** %boxset0.addr, align 4, !tbaa !2
  store %class.btTransform* %trans0, %class.btTransform** %trans0.addr, align 4, !tbaa !2
  store %class.btGImpactBvh* %boxset1, %class.btGImpactBvh** %boxset1.addr, align 4, !tbaa !2
  store %class.btTransform* %trans1, %class.btTransform** %trans1.addr, align 4, !tbaa !2
  store %class.btPairSet* %collision_pairs, %class.btPairSet** %collision_pairs.addr, align 4, !tbaa !2
  %0 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4, !tbaa !2
  %call = call i32 @_ZNK12btGImpactBvh12getNodeCountEv(%class.btGImpactBvh* %0)
  %cmp = icmp eq i32 %call, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4, !tbaa !2
  %call1 = call i32 @_ZNK12btGImpactBvh12getNodeCountEv(%class.btGImpactBvh* %1)
  %cmp2 = icmp eq i32 %call1, 0
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %2 = bitcast %class.BT_BOX_BOX_TRANSFORM_CACHE* %trans_cache_1to0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 112, i8* %2) #8
  %call3 = call %class.BT_BOX_BOX_TRANSFORM_CACHE* @_ZN26BT_BOX_BOX_TRANSFORM_CACHEC2Ev(%class.BT_BOX_BOX_TRANSFORM_CACHE* %trans_cache_1to0)
  %3 = load %class.btTransform*, %class.btTransform** %trans0.addr, align 4, !tbaa !2
  %4 = load %class.btTransform*, %class.btTransform** %trans1.addr, align 4, !tbaa !2
  call void @_ZN26BT_BOX_BOX_TRANSFORM_CACHE19calc_from_homogenicERK11btTransformS2_(%class.BT_BOX_BOX_TRANSFORM_CACHE* %trans_cache_1to0, %class.btTransform* nonnull align 4 dereferenceable(64) %3, %class.btTransform* nonnull align 4 dereferenceable(64) %4)
  %5 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4, !tbaa !2
  %6 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4, !tbaa !2
  %7 = load %class.btPairSet*, %class.btPairSet** %collision_pairs.addr, align 4, !tbaa !2
  call void @_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib(%class.btGImpactBvh* %5, %class.btGImpactBvh* %6, %class.btPairSet* %7, %class.BT_BOX_BOX_TRANSFORM_CACHE* nonnull align 4 dereferenceable(112) %trans_cache_1to0, i32 0, i32 0, i1 zeroext true)
  %8 = bitcast %class.BT_BOX_BOX_TRANSFORM_CACHE* %trans_cache_1to0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 112, i8* %8) #8
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

define linkonce_odr hidden %class.BT_BOX_BOX_TRANSFORM_CACHE* @_ZN26BT_BOX_BOX_TRANSFORM_CACHEC2Ev(%class.BT_BOX_BOX_TRANSFORM_CACHE* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.BT_BOX_BOX_TRANSFORM_CACHE*, align 4
  store %class.BT_BOX_BOX_TRANSFORM_CACHE* %this, %class.BT_BOX_BOX_TRANSFORM_CACHE** %this.addr, align 4, !tbaa !2
  %this1 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %this.addr, align 4
  %m_T1to0 = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_T1to0)
  %m_R1to0 = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %this1, i32 0, i32 1
  %call2 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_R1to0)
  %m_AR = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %this1, i32 0, i32 2
  %call3 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_AR)
  ret %class.BT_BOX_BOX_TRANSFORM_CACHE* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN26BT_BOX_BOX_TRANSFORM_CACHE19calc_from_homogenicERK11btTransformS2_(%class.BT_BOX_BOX_TRANSFORM_CACHE* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %trans0, %class.btTransform* nonnull align 4 dereferenceable(64) %trans1) #3 comdat {
entry:
  %this.addr = alloca %class.BT_BOX_BOX_TRANSFORM_CACHE*, align 4
  %trans0.addr = alloca %class.btTransform*, align 4
  %trans1.addr = alloca %class.btTransform*, align 4
  %temp_trans = alloca %class.btTransform, align 4
  %ref.tmp = alloca %class.btTransform, align 4
  store %class.BT_BOX_BOX_TRANSFORM_CACHE* %this, %class.BT_BOX_BOX_TRANSFORM_CACHE** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %trans0, %class.btTransform** %trans0.addr, align 4, !tbaa !2
  store %class.btTransform* %trans1, %class.btTransform** %trans1.addr, align 4, !tbaa !2
  %this1 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %this.addr, align 4
  %0 = bitcast %class.btTransform* %temp_trans to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %0) #8
  %1 = load %class.btTransform*, %class.btTransform** %trans0.addr, align 4, !tbaa !2
  call void @_ZNK11btTransform7inverseEv(%class.btTransform* sret align 4 %temp_trans, %class.btTransform* %1)
  %2 = bitcast %class.btTransform* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %2) #8
  %3 = load %class.btTransform*, %class.btTransform** %trans1.addr, align 4, !tbaa !2
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp, %class.btTransform* %temp_trans, %class.btTransform* nonnull align 4 dereferenceable(64) %3)
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %temp_trans, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp)
  %4 = bitcast %class.btTransform* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %4) #8
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %temp_trans)
  %m_T1to0 = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %this1, i32 0, i32 0
  %5 = bitcast %class.btVector3* %m_T1to0 to i8*
  %6 = bitcast %class.btVector3* %call2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false), !tbaa.struct !10
  %call3 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %temp_trans)
  %m_R1to0 = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %this1, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_R1to0, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call3)
  call void @_ZN26BT_BOX_BOX_TRANSFORM_CACHE20calc_absolute_matrixEv(%class.BT_BOX_BOX_TRANSFORM_CACHE* %this1)
  %7 = bitcast %class.btTransform* %temp_trans to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %7) #8
  ret void
}

define internal void @_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib(%class.btGImpactBvh* %boxset0, %class.btGImpactBvh* %boxset1, %class.btPairSet* %collision_pairs, %class.BT_BOX_BOX_TRANSFORM_CACHE* nonnull align 4 dereferenceable(112) %trans_cache_1to0, i32 %node0, i32 %node1, i1 zeroext %complete_primitive_tests) #0 {
entry:
  %boxset0.addr = alloca %class.btGImpactBvh*, align 4
  %boxset1.addr = alloca %class.btGImpactBvh*, align 4
  %collision_pairs.addr = alloca %class.btPairSet*, align 4
  %trans_cache_1to0.addr = alloca %class.BT_BOX_BOX_TRANSFORM_CACHE*, align 4
  %node0.addr = alloca i32, align 4
  %node1.addr = alloca i32, align 4
  %complete_primitive_tests.addr = alloca i8, align 1
  store %class.btGImpactBvh* %boxset0, %class.btGImpactBvh** %boxset0.addr, align 4, !tbaa !2
  store %class.btGImpactBvh* %boxset1, %class.btGImpactBvh** %boxset1.addr, align 4, !tbaa !2
  store %class.btPairSet* %collision_pairs, %class.btPairSet** %collision_pairs.addr, align 4, !tbaa !2
  store %class.BT_BOX_BOX_TRANSFORM_CACHE* %trans_cache_1to0, %class.BT_BOX_BOX_TRANSFORM_CACHE** %trans_cache_1to0.addr, align 4, !tbaa !2
  store i32 %node0, i32* %node0.addr, align 4, !tbaa !6
  store i32 %node1, i32* %node1.addr, align 4, !tbaa !6
  %frombool = zext i1 %complete_primitive_tests to i8
  store i8 %frombool, i8* %complete_primitive_tests.addr, align 1, !tbaa !16
  %0 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4, !tbaa !2
  %1 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4, !tbaa !2
  %2 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %trans_cache_1to0.addr, align 4, !tbaa !2
  %3 = load i32, i32* %node0.addr, align 4, !tbaa !6
  %4 = load i32, i32* %node1.addr, align 4, !tbaa !6
  %5 = load i8, i8* %complete_primitive_tests.addr, align 1, !tbaa !16, !range !17
  %tobool = trunc i8 %5 to i1
  %call = call zeroext i1 @_Z15_node_collisionP12btGImpactBvhS0_RK26BT_BOX_BOX_TRANSFORM_CACHEiib(%class.btGImpactBvh* %0, %class.btGImpactBvh* %1, %class.BT_BOX_BOX_TRANSFORM_CACHE* nonnull align 4 dereferenceable(112) %2, i32 %3, i32 %4, i1 zeroext %tobool)
  %conv = zext i1 %call to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %if.end25

if.end:                                           ; preds = %entry
  %6 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4, !tbaa !2
  %7 = load i32, i32* %node0.addr, align 4, !tbaa !6
  %call1 = call zeroext i1 @_ZNK12btGImpactBvh10isLeafNodeEi(%class.btGImpactBvh* %6, i32 %7)
  br i1 %call1, label %if.then2, label %if.else10

if.then2:                                         ; preds = %if.end
  %8 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4, !tbaa !2
  %9 = load i32, i32* %node1.addr, align 4, !tbaa !6
  %call3 = call zeroext i1 @_ZNK12btGImpactBvh10isLeafNodeEi(%class.btGImpactBvh* %8, i32 %9)
  br i1 %call3, label %if.then4, label %if.else

if.then4:                                         ; preds = %if.then2
  %10 = load %class.btPairSet*, %class.btPairSet** %collision_pairs.addr, align 4, !tbaa !2
  %11 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4, !tbaa !2
  %12 = load i32, i32* %node0.addr, align 4, !tbaa !6
  %call5 = call i32 @_ZNK12btGImpactBvh11getNodeDataEi(%class.btGImpactBvh* %11, i32 %12)
  %13 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4, !tbaa !2
  %14 = load i32, i32* %node1.addr, align 4, !tbaa !6
  %call6 = call i32 @_ZNK12btGImpactBvh11getNodeDataEi(%class.btGImpactBvh* %13, i32 %14)
  call void @_ZN9btPairSet9push_pairEii(%class.btPairSet* %10, i32 %call5, i32 %call6)
  br label %if.end25

if.else:                                          ; preds = %if.then2
  %15 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4, !tbaa !2
  %16 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4, !tbaa !2
  %17 = load %class.btPairSet*, %class.btPairSet** %collision_pairs.addr, align 4, !tbaa !2
  %18 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %trans_cache_1to0.addr, align 4, !tbaa !2
  %19 = load i32, i32* %node0.addr, align 4, !tbaa !6
  %20 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4, !tbaa !2
  %21 = load i32, i32* %node1.addr, align 4, !tbaa !6
  %call7 = call i32 @_ZNK12btGImpactBvh11getLeftNodeEi(%class.btGImpactBvh* %20, i32 %21)
  call void @_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib(%class.btGImpactBvh* %15, %class.btGImpactBvh* %16, %class.btPairSet* %17, %class.BT_BOX_BOX_TRANSFORM_CACHE* nonnull align 4 dereferenceable(112) %18, i32 %19, i32 %call7, i1 zeroext false)
  %22 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4, !tbaa !2
  %23 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4, !tbaa !2
  %24 = load %class.btPairSet*, %class.btPairSet** %collision_pairs.addr, align 4, !tbaa !2
  %25 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %trans_cache_1to0.addr, align 4, !tbaa !2
  %26 = load i32, i32* %node0.addr, align 4, !tbaa !6
  %27 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4, !tbaa !2
  %28 = load i32, i32* %node1.addr, align 4, !tbaa !6
  %call8 = call i32 @_ZNK12btGImpactBvh12getRightNodeEi(%class.btGImpactBvh* %27, i32 %28)
  call void @_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib(%class.btGImpactBvh* %22, %class.btGImpactBvh* %23, %class.btPairSet* %24, %class.BT_BOX_BOX_TRANSFORM_CACHE* nonnull align 4 dereferenceable(112) %25, i32 %26, i32 %call8, i1 zeroext false)
  br label %if.end9

if.end9:                                          ; preds = %if.else
  br label %if.end25

if.else10:                                        ; preds = %if.end
  %29 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4, !tbaa !2
  %30 = load i32, i32* %node1.addr, align 4, !tbaa !6
  %call11 = call zeroext i1 @_ZNK12btGImpactBvh10isLeafNodeEi(%class.btGImpactBvh* %29, i32 %30)
  br i1 %call11, label %if.then12, label %if.else15

if.then12:                                        ; preds = %if.else10
  %31 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4, !tbaa !2
  %32 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4, !tbaa !2
  %33 = load %class.btPairSet*, %class.btPairSet** %collision_pairs.addr, align 4, !tbaa !2
  %34 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %trans_cache_1to0.addr, align 4, !tbaa !2
  %35 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4, !tbaa !2
  %36 = load i32, i32* %node0.addr, align 4, !tbaa !6
  %call13 = call i32 @_ZNK12btGImpactBvh11getLeftNodeEi(%class.btGImpactBvh* %35, i32 %36)
  %37 = load i32, i32* %node1.addr, align 4, !tbaa !6
  call void @_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib(%class.btGImpactBvh* %31, %class.btGImpactBvh* %32, %class.btPairSet* %33, %class.BT_BOX_BOX_TRANSFORM_CACHE* nonnull align 4 dereferenceable(112) %34, i32 %call13, i32 %37, i1 zeroext false)
  %38 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4, !tbaa !2
  %39 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4, !tbaa !2
  %40 = load %class.btPairSet*, %class.btPairSet** %collision_pairs.addr, align 4, !tbaa !2
  %41 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %trans_cache_1to0.addr, align 4, !tbaa !2
  %42 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4, !tbaa !2
  %43 = load i32, i32* %node0.addr, align 4, !tbaa !6
  %call14 = call i32 @_ZNK12btGImpactBvh12getRightNodeEi(%class.btGImpactBvh* %42, i32 %43)
  %44 = load i32, i32* %node1.addr, align 4, !tbaa !6
  call void @_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib(%class.btGImpactBvh* %38, %class.btGImpactBvh* %39, %class.btPairSet* %40, %class.BT_BOX_BOX_TRANSFORM_CACHE* nonnull align 4 dereferenceable(112) %41, i32 %call14, i32 %44, i1 zeroext false)
  br label %if.end24

if.else15:                                        ; preds = %if.else10
  %45 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4, !tbaa !2
  %46 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4, !tbaa !2
  %47 = load %class.btPairSet*, %class.btPairSet** %collision_pairs.addr, align 4, !tbaa !2
  %48 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %trans_cache_1to0.addr, align 4, !tbaa !2
  %49 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4, !tbaa !2
  %50 = load i32, i32* %node0.addr, align 4, !tbaa !6
  %call16 = call i32 @_ZNK12btGImpactBvh11getLeftNodeEi(%class.btGImpactBvh* %49, i32 %50)
  %51 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4, !tbaa !2
  %52 = load i32, i32* %node1.addr, align 4, !tbaa !6
  %call17 = call i32 @_ZNK12btGImpactBvh11getLeftNodeEi(%class.btGImpactBvh* %51, i32 %52)
  call void @_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib(%class.btGImpactBvh* %45, %class.btGImpactBvh* %46, %class.btPairSet* %47, %class.BT_BOX_BOX_TRANSFORM_CACHE* nonnull align 4 dereferenceable(112) %48, i32 %call16, i32 %call17, i1 zeroext false)
  %53 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4, !tbaa !2
  %54 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4, !tbaa !2
  %55 = load %class.btPairSet*, %class.btPairSet** %collision_pairs.addr, align 4, !tbaa !2
  %56 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %trans_cache_1to0.addr, align 4, !tbaa !2
  %57 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4, !tbaa !2
  %58 = load i32, i32* %node0.addr, align 4, !tbaa !6
  %call18 = call i32 @_ZNK12btGImpactBvh11getLeftNodeEi(%class.btGImpactBvh* %57, i32 %58)
  %59 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4, !tbaa !2
  %60 = load i32, i32* %node1.addr, align 4, !tbaa !6
  %call19 = call i32 @_ZNK12btGImpactBvh12getRightNodeEi(%class.btGImpactBvh* %59, i32 %60)
  call void @_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib(%class.btGImpactBvh* %53, %class.btGImpactBvh* %54, %class.btPairSet* %55, %class.BT_BOX_BOX_TRANSFORM_CACHE* nonnull align 4 dereferenceable(112) %56, i32 %call18, i32 %call19, i1 zeroext false)
  %61 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4, !tbaa !2
  %62 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4, !tbaa !2
  %63 = load %class.btPairSet*, %class.btPairSet** %collision_pairs.addr, align 4, !tbaa !2
  %64 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %trans_cache_1to0.addr, align 4, !tbaa !2
  %65 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4, !tbaa !2
  %66 = load i32, i32* %node0.addr, align 4, !tbaa !6
  %call20 = call i32 @_ZNK12btGImpactBvh12getRightNodeEi(%class.btGImpactBvh* %65, i32 %66)
  %67 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4, !tbaa !2
  %68 = load i32, i32* %node1.addr, align 4, !tbaa !6
  %call21 = call i32 @_ZNK12btGImpactBvh11getLeftNodeEi(%class.btGImpactBvh* %67, i32 %68)
  call void @_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib(%class.btGImpactBvh* %61, %class.btGImpactBvh* %62, %class.btPairSet* %63, %class.BT_BOX_BOX_TRANSFORM_CACHE* nonnull align 4 dereferenceable(112) %64, i32 %call20, i32 %call21, i1 zeroext false)
  %69 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4, !tbaa !2
  %70 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4, !tbaa !2
  %71 = load %class.btPairSet*, %class.btPairSet** %collision_pairs.addr, align 4, !tbaa !2
  %72 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %trans_cache_1to0.addr, align 4, !tbaa !2
  %73 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4, !tbaa !2
  %74 = load i32, i32* %node0.addr, align 4, !tbaa !6
  %call22 = call i32 @_ZNK12btGImpactBvh12getRightNodeEi(%class.btGImpactBvh* %73, i32 %74)
  %75 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4, !tbaa !2
  %76 = load i32, i32* %node1.addr, align 4, !tbaa !6
  %call23 = call i32 @_ZNK12btGImpactBvh12getRightNodeEi(%class.btGImpactBvh* %75, i32 %76)
  call void @_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib(%class.btGImpactBvh* %69, %class.btGImpactBvh* %70, %class.btPairSet* %71, %class.BT_BOX_BOX_TRANSFORM_CACHE* nonnull align 4 dereferenceable(112) %72, i32 %call22, i32 %call23, i1 zeroext false)
  br label %if.end24

if.end24:                                         ; preds = %if.else15, %if.then12
  br label %if.end25

if.end25:                                         ; preds = %if.then, %if.then4, %if.end24, %if.end9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #3 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !8
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !8
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !8
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !8
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !8
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK9btBvhTree12getNodeCountEv(%class.btBvhTree* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btBvhTree*, align 4
  store %class.btBvhTree* %this, %class.btBvhTree** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btBvhTree*, %class.btBvhTree** %this.addr, align 4
  %m_num_nodes = getelementptr inbounds %class.btBvhTree, %class.btBvhTree* %this1, i32 0, i32 0
  %0 = load i32, i32* %m_num_nodes, align 4, !tbaa !19
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_ZNK9btBvhTree10isLeafNodeEi(%class.btBvhTree* %this, i32 %nodeindex) #3 comdat {
entry:
  %this.addr = alloca %class.btBvhTree*, align 4
  %nodeindex.addr = alloca i32, align 4
  store %class.btBvhTree* %this, %class.btBvhTree** %this.addr, align 4, !tbaa !2
  store i32 %nodeindex, i32* %nodeindex.addr, align 4, !tbaa !6
  %this1 = load %class.btBvhTree*, %class.btBvhTree** %this.addr, align 4
  %m_node_array = getelementptr inbounds %class.btBvhTree, %class.btBvhTree* %this1, i32 0, i32 1
  %0 = bitcast %class.GIM_BVH_TREE_NODE_ARRAY* %m_node_array to %class.btAlignedObjectArray*
  %1 = load i32, i32* %nodeindex.addr, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(36) %class.GIM_BVH_TREE_NODE* @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEEixEi(%class.btAlignedObjectArray* %0, i32 %1)
  %call2 = call zeroext i1 @_ZNK17GIM_BVH_TREE_NODE10isLeafNodeEv(%class.GIM_BVH_TREE_NODE* %call)
  ret i1 %call2
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(36) %class.GIM_BVH_TREE_NODE* @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEEixEi(%class.btAlignedObjectArray* %this, i32 %n) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %m_data, align 4, !tbaa !27
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %0, i32 %1
  ret %class.GIM_BVH_TREE_NODE* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZNK17GIM_BVH_TREE_NODE10isLeafNodeEv(%class.GIM_BVH_TREE_NODE* %this) #2 comdat {
entry:
  %this.addr = alloca %class.GIM_BVH_TREE_NODE*, align 4
  store %class.GIM_BVH_TREE_NODE* %this, %class.GIM_BVH_TREE_NODE** %this.addr, align 4, !tbaa !2
  %this1 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %this.addr, align 4
  %m_escapeIndexOrDataIndex = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_escapeIndexOrDataIndex, align 4, !tbaa !30
  %cmp = icmp sge i32 %0, 0
  ret i1 %cmp
}

; Function Attrs: inlinehint
define linkonce_odr hidden i32 @_ZNK9btBvhTree11getNodeDataEi(%class.btBvhTree* %this, i32 %nodeindex) #3 comdat {
entry:
  %this.addr = alloca %class.btBvhTree*, align 4
  %nodeindex.addr = alloca i32, align 4
  store %class.btBvhTree* %this, %class.btBvhTree** %this.addr, align 4, !tbaa !2
  store i32 %nodeindex, i32* %nodeindex.addr, align 4, !tbaa !6
  %this1 = load %class.btBvhTree*, %class.btBvhTree** %this.addr, align 4
  %m_node_array = getelementptr inbounds %class.btBvhTree, %class.btBvhTree* %this1, i32 0, i32 1
  %0 = bitcast %class.GIM_BVH_TREE_NODE_ARRAY* %m_node_array to %class.btAlignedObjectArray*
  %1 = load i32, i32* %nodeindex.addr, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(36) %class.GIM_BVH_TREE_NODE* @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEEixEi(%class.btAlignedObjectArray* %0, i32 %1)
  %call2 = call i32 @_ZNK17GIM_BVH_TREE_NODE12getDataIndexEv(%class.GIM_BVH_TREE_NODE* %call)
  ret i32 %call2
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK17GIM_BVH_TREE_NODE12getDataIndexEv(%class.GIM_BVH_TREE_NODE* %this) #2 comdat {
entry:
  %this.addr = alloca %class.GIM_BVH_TREE_NODE*, align 4
  store %class.GIM_BVH_TREE_NODE* %this, %class.GIM_BVH_TREE_NODE** %this.addr, align 4, !tbaa !2
  %this1 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %this.addr, align 4
  %m_escapeIndexOrDataIndex = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_escapeIndexOrDataIndex, align 4, !tbaa !30
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK9btBvhTree11getLeftNodeEi(%class.btBvhTree* %this, i32 %nodeindex) #2 comdat {
entry:
  %this.addr = alloca %class.btBvhTree*, align 4
  %nodeindex.addr = alloca i32, align 4
  store %class.btBvhTree* %this, %class.btBvhTree** %this.addr, align 4, !tbaa !2
  store i32 %nodeindex, i32* %nodeindex.addr, align 4, !tbaa !6
  %this1 = load %class.btBvhTree*, %class.btBvhTree** %this.addr, align 4
  %0 = load i32, i32* %nodeindex.addr, align 4, !tbaa !6
  %add = add nsw i32 %0, 1
  ret i32 %add
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK9btBvhTree12getNodeBoundEiR6btAABB(%class.btBvhTree* %this, i32 %nodeindex, %class.btAABB* nonnull align 4 dereferenceable(32) %bound) #2 comdat {
entry:
  %this.addr = alloca %class.btBvhTree*, align 4
  %nodeindex.addr = alloca i32, align 4
  %bound.addr = alloca %class.btAABB*, align 4
  store %class.btBvhTree* %this, %class.btBvhTree** %this.addr, align 4, !tbaa !2
  store i32 %nodeindex, i32* %nodeindex.addr, align 4, !tbaa !6
  store %class.btAABB* %bound, %class.btAABB** %bound.addr, align 4, !tbaa !2
  %this1 = load %class.btBvhTree*, %class.btBvhTree** %this.addr, align 4
  %m_node_array = getelementptr inbounds %class.btBvhTree, %class.btBvhTree* %this1, i32 0, i32 1
  %0 = bitcast %class.GIM_BVH_TREE_NODE_ARRAY* %m_node_array to %class.btAlignedObjectArray*
  %1 = load i32, i32* %nodeindex.addr, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(36) %class.GIM_BVH_TREE_NODE* @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEEixEi(%class.btAlignedObjectArray* %0, i32 %1)
  %m_bound = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %call, i32 0, i32 0
  %2 = load %class.btAABB*, %class.btAABB** %bound.addr, align 4, !tbaa !2
  %3 = bitcast %class.btAABB* %2 to i8*
  %4 = bitcast %class.btAABB* %m_bound to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 32, i1 false), !tbaa.struct !26
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden i32 @_ZNK9btBvhTree12getRightNodeEi(%class.btBvhTree* %this, i32 %nodeindex) #3 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btBvhTree*, align 4
  %nodeindex.addr = alloca i32, align 4
  store %class.btBvhTree* %this, %class.btBvhTree** %this.addr, align 4, !tbaa !2
  store i32 %nodeindex, i32* %nodeindex.addr, align 4, !tbaa !6
  %this1 = load %class.btBvhTree*, %class.btBvhTree** %this.addr, align 4
  %m_node_array = getelementptr inbounds %class.btBvhTree, %class.btBvhTree* %this1, i32 0, i32 1
  %0 = bitcast %class.GIM_BVH_TREE_NODE_ARRAY* %m_node_array to %class.btAlignedObjectArray*
  %1 = load i32, i32* %nodeindex.addr, align 4, !tbaa !6
  %add = add nsw i32 %1, 1
  %call = call nonnull align 4 dereferenceable(36) %class.GIM_BVH_TREE_NODE* @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEEixEi(%class.btAlignedObjectArray* %0, i32 %add)
  %call2 = call zeroext i1 @_ZNK17GIM_BVH_TREE_NODE10isLeafNodeEv(%class.GIM_BVH_TREE_NODE* %call)
  br i1 %call2, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %nodeindex.addr, align 4, !tbaa !6
  %add3 = add nsw i32 %2, 2
  store i32 %add3, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = load i32, i32* %nodeindex.addr, align 4, !tbaa !6
  %add4 = add nsw i32 %3, 1
  %m_node_array5 = getelementptr inbounds %class.btBvhTree, %class.btBvhTree* %this1, i32 0, i32 1
  %4 = bitcast %class.GIM_BVH_TREE_NODE_ARRAY* %m_node_array5 to %class.btAlignedObjectArray*
  %5 = load i32, i32* %nodeindex.addr, align 4, !tbaa !6
  %add6 = add nsw i32 %5, 1
  %call7 = call nonnull align 4 dereferenceable(36) %class.GIM_BVH_TREE_NODE* @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEEixEi(%class.btAlignedObjectArray* %4, i32 %add6)
  %call8 = call i32 @_ZNK17GIM_BVH_TREE_NODE14getEscapeIndexEv(%class.GIM_BVH_TREE_NODE* %call7)
  %add9 = add nsw i32 %add4, %call8
  store i32 %add9, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %6 = load i32, i32* %retval, align 4
  ret i32 %6
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK17GIM_BVH_TREE_NODE14getEscapeIndexEv(%class.GIM_BVH_TREE_NODE* %this) #2 comdat {
entry:
  %this.addr = alloca %class.GIM_BVH_TREE_NODE*, align 4
  store %class.GIM_BVH_TREE_NODE* %this, %class.GIM_BVH_TREE_NODE** %this.addr, align 4, !tbaa !2
  %this1 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %this.addr, align 4
  %m_escapeIndexOrDataIndex = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_escapeIndexOrDataIndex, align 4, !tbaa !30
  %sub = sub nsw i32 0, %0
  ret i32 %sub
}

define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEC2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorI12GIM_BVH_DATALj16EEC2Ev(%class.btAlignedAllocator.1* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorI12GIM_BVH_DATALj16EEC2Ev(%class.btAlignedAllocator.1* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  ret %class.btAlignedAllocator.1* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE4initEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !42
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.GIM_BVH_DATA* null, %struct.GIM_BVH_DATA** %m_data, align 4, !tbaa !12
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !33
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !43
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE5clearEv(%class.btAlignedObjectArray.0* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %4 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %m_data, align 4, !tbaa !12
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE10deallocateEv(%class.btAlignedObjectArray.0* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %m_data, align 4, !tbaa !12
  %tobool = icmp ne %struct.GIM_BVH_DATA* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !42, !range !17
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %m_data4, align 4, !tbaa !12
  call void @_ZN18btAlignedAllocatorI12GIM_BVH_DATALj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %m_allocator, %struct.GIM_BVH_DATA* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.GIM_BVH_DATA* null, %struct.GIM_BVH_DATA** %m_data5, align 4, !tbaa !12
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI12GIM_BVH_DATALj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %this, %struct.GIM_BVH_DATA* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca %struct.GIM_BVH_DATA*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  store %struct.GIM_BVH_DATA* %ptr, %struct.GIM_BVH_DATA** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.GIM_BVH_DATA* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #6

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK9btBvhTree18getEscapeNodeIndexEi(%class.btBvhTree* %this, i32 %nodeindex) #2 comdat {
entry:
  %this.addr = alloca %class.btBvhTree*, align 4
  %nodeindex.addr = alloca i32, align 4
  store %class.btBvhTree* %this, %class.btBvhTree** %this.addr, align 4, !tbaa !2
  store i32 %nodeindex, i32* %nodeindex.addr, align 4, !tbaa !6
  %this1 = load %class.btBvhTree*, %class.btBvhTree** %this.addr, align 4
  %m_node_array = getelementptr inbounds %class.btBvhTree, %class.btBvhTree* %this1, i32 0, i32 1
  %0 = bitcast %class.GIM_BVH_TREE_NODE_ARRAY* %m_node_array to %class.btAlignedObjectArray*
  %1 = load i32, i32* %nodeindex.addr, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(36) %class.GIM_BVH_TREE_NODE* @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEEixEi(%class.btAlignedObjectArray* %0, i32 %1)
  %call2 = call i32 @_ZNK17GIM_BVH_TREE_NODE14getEscapeIndexEv(%class.GIM_BVH_TREE_NODE* %call)
  ret i32 %call2
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK6btAABB17get_center_extendER9btVector3S1_(%class.btAABB* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extend) #3 comdat {
entry:
  %this.addr = alloca %class.btAABB*, align 4
  %center.addr = alloca %class.btVector3*, align 4
  %extend.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  store %class.btAABB* %this, %class.btAABB** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %center, %class.btVector3** %center.addr, align 4, !tbaa !2
  store %class.btVector3* %extend, %class.btVector3** %extend.addr, align 4, !tbaa !2
  %this1 = load %class.btAABB*, %class.btAABB** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #8
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_max, %class.btVector3* nonnull align 4 dereferenceable(16) %m_min)
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 5.000000e-01, float* %ref.tmp3, align 4, !tbaa !8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = load %class.btVector3*, %class.btVector3** %center.addr, align 4, !tbaa !2
  %4 = bitcast %class.btVector3* %3 to i8*
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !10
  %6 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %7 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %7) #8
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #8
  %9 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #8
  %m_max5 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %10 = load %class.btVector3*, %class.btVector3** %center.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %m_max5, %class.btVector3* nonnull align 4 dereferenceable(16) %10)
  %11 = load %class.btVector3*, %class.btVector3** %extend.addr, align 4, !tbaa !2
  %12 = bitcast %class.btVector3* %11 to i8*
  %13 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false), !tbaa.struct !10
  %14 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %14) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btFabsf(float %x) #2 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !8
  %0 = load float, float* %x.addr, align 4, !tbaa !8
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #7

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

define linkonce_odr hidden void @_ZNK11btTransform7inverseEv(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %inv = alloca %class.btMatrix3x3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %1 = bitcast %class.btMatrix3x3* %inv to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %1) #8
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %inv, %class.btMatrix3x3* %m_basis)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #8
  %3 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #8
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %4 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #8
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #8
  %6 = bitcast %class.btMatrix3x3* %inv to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %6) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformmlERKS_(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t) #3 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %1 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %1) #8
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %2 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %2, i32 0, i32 0
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %3 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #8
  %4 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %4, i32 0, i32 1
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp3, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  %5 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #8
  %6 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %6) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !10
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !10
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !10
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !10
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN26BT_BOX_BOX_TRANSFORM_CACHE20calc_absolute_matrixEv(%class.BT_BOX_BOX_TRANSFORM_CACHE* %this) #3 comdat {
entry:
  %this.addr = alloca %class.BT_BOX_BOX_TRANSFORM_CACHE*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store %class.BT_BOX_BOX_TRANSFORM_CACHE* %this, %class.BT_BOX_BOX_TRANSFORM_CACHE** %this.addr, align 4, !tbaa !2
  %this1 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc10, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, 3
  br i1 %cmp, label %for.body, label %for.end12

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc, %for.body
  %3 = load i32, i32* %j, align 4, !tbaa !6
  %cmp3 = icmp slt i32 %3, 3
  br i1 %cmp3, label %for.body4, label %for.end

for.body4:                                        ; preds = %for.cond2
  %m_R1to0 = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %this1, i32 0, i32 1
  %4 = load i32, i32* %i, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %m_R1to0, i32 %4)
  %call5 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call)
  %5 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %call5, i32 %5
  %6 = load float, float* %arrayidx, align 4, !tbaa !8
  %call6 = call float @_Z6btFabsf(float %6)
  %add = fadd float 0x3EB0C6F7A0000000, %call6
  %m_AR = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %this1, i32 0, i32 2
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %m_AR, i32 %7)
  %call8 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call7)
  %8 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 %8
  store float %add, float* %arrayidx9, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %9 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond2

for.end:                                          ; preds = %for.cond2
  br label %for.inc10

for.inc10:                                        ; preds = %for.end
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc11 = add nsw i32 %10, 1
  store i32 %inc11, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end12:                                        ; preds = %for.cond
  %11 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #3 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %1, i32 0)
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call1, float* %ref.tmp, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 1)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !8
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 2)
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !8
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !8
  %fneg4 = fneg float %5
  store float %fneg4, float* %ref.tmp1, align 4, !tbaa !8
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %8 = load float, float* %arrayidx7, align 4, !tbaa !8
  %fneg8 = fneg float %8
  store float %fneg8, float* %ref.tmp5, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %b.addr = alloca %class.btMatrix3x3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %b, %class.btMatrix3x3** %b.addr, align 4, !tbaa !2
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %b.addr, align 4, !tbaa !2
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !10
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !8
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !8
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !8
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !8
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !8
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !8
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #3 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !10
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !10
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !10
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #3 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %3, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4, !tbaa !8
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !8
  %7 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %9, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %8, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4, !tbaa !8
  %13 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %15, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %14, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4, !tbaa !8
  %16 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4, !tbaa !8
  %19 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  %20 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %21 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %21, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %20, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4, !tbaa !8
  %22 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #8
  %23 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %24 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %24, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %23, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4, !tbaa !8
  %25 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  %26 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %27 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %27, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %26, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4, !tbaa !8
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  %28 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #8
  %29 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #8
  %30 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #8
  %31 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #8
  %32 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #8
  %33 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #8
  %34 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #8
  %35 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  %36 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !8
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !8
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !8
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !8
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !8
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !8
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !8
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !8
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !8
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4, !tbaa !8
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !8
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !8
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_Z15_node_collisionP12btGImpactBvhS0_RK26BT_BOX_BOX_TRANSFORM_CACHEiib(%class.btGImpactBvh* %boxset0, %class.btGImpactBvh* %boxset1, %class.BT_BOX_BOX_TRANSFORM_CACHE* nonnull align 4 dereferenceable(112) %trans_cache_1to0, i32 %node0, i32 %node1, i1 zeroext %complete_primitive_tests) #3 comdat {
entry:
  %boxset0.addr = alloca %class.btGImpactBvh*, align 4
  %boxset1.addr = alloca %class.btGImpactBvh*, align 4
  %trans_cache_1to0.addr = alloca %class.BT_BOX_BOX_TRANSFORM_CACHE*, align 4
  %node0.addr = alloca i32, align 4
  %node1.addr = alloca i32, align 4
  %complete_primitive_tests.addr = alloca i8, align 1
  %box0 = alloca %class.btAABB, align 4
  %box1 = alloca %class.btAABB, align 4
  store %class.btGImpactBvh* %boxset0, %class.btGImpactBvh** %boxset0.addr, align 4, !tbaa !2
  store %class.btGImpactBvh* %boxset1, %class.btGImpactBvh** %boxset1.addr, align 4, !tbaa !2
  store %class.BT_BOX_BOX_TRANSFORM_CACHE* %trans_cache_1to0, %class.BT_BOX_BOX_TRANSFORM_CACHE** %trans_cache_1to0.addr, align 4, !tbaa !2
  store i32 %node0, i32* %node0.addr, align 4, !tbaa !6
  store i32 %node1, i32* %node1.addr, align 4, !tbaa !6
  %frombool = zext i1 %complete_primitive_tests to i8
  store i8 %frombool, i8* %complete_primitive_tests.addr, align 1, !tbaa !16
  %0 = bitcast %class.btAABB* %box0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %0) #8
  %call = call %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* %box0)
  %1 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4, !tbaa !2
  %2 = load i32, i32* %node0.addr, align 4, !tbaa !6
  call void @_ZNK12btGImpactBvh12getNodeBoundEiR6btAABB(%class.btGImpactBvh* %1, i32 %2, %class.btAABB* nonnull align 4 dereferenceable(32) %box0)
  %3 = bitcast %class.btAABB* %box1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %3) #8
  %call1 = call %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* %box1)
  %4 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4, !tbaa !2
  %5 = load i32, i32* %node1.addr, align 4, !tbaa !6
  call void @_ZNK12btGImpactBvh12getNodeBoundEiR6btAABB(%class.btGImpactBvh* %4, i32 %5, %class.btAABB* nonnull align 4 dereferenceable(32) %box1)
  %6 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %trans_cache_1to0.addr, align 4, !tbaa !2
  %7 = load i8, i8* %complete_primitive_tests.addr, align 1, !tbaa !16, !range !17
  %tobool = trunc i8 %7 to i1
  %call2 = call zeroext i1 @_ZNK6btAABB23overlapping_trans_cacheERKS_RK26BT_BOX_BOX_TRANSFORM_CACHEb(%class.btAABB* %box0, %class.btAABB* nonnull align 4 dereferenceable(32) %box1, %class.BT_BOX_BOX_TRANSFORM_CACHE* nonnull align 4 dereferenceable(112) %6, i1 zeroext %tobool)
  %8 = bitcast %class.btAABB* %box1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %8) #8
  %9 = bitcast %class.btAABB* %box0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %9) #8
  ret i1 %call2
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN9btPairSet9push_pairEii(%class.btPairSet* %this, i32 %index1, i32 %index2) #3 comdat {
entry:
  %this.addr = alloca %class.btPairSet*, align 4
  %index1.addr = alloca i32, align 4
  %index2.addr = alloca i32, align 4
  %ref.tmp = alloca %struct.GIM_PAIR, align 4
  store %class.btPairSet* %this, %class.btPairSet** %this.addr, align 4, !tbaa !2
  store i32 %index1, i32* %index1.addr, align 4, !tbaa !6
  store i32 %index2, i32* %index2.addr, align 4, !tbaa !6
  %this1 = load %class.btPairSet*, %class.btPairSet** %this.addr, align 4
  %0 = bitcast %class.btPairSet* %this1 to %class.btAlignedObjectArray.8*
  %1 = bitcast %struct.GIM_PAIR* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #8
  %2 = load i32, i32* %index1.addr, align 4, !tbaa !6
  %3 = load i32, i32* %index2.addr, align 4, !tbaa !6
  %call = call %struct.GIM_PAIR* @_ZN8GIM_PAIRC2Eii(%struct.GIM_PAIR* %ref.tmp, i32 %2, i32 %3)
  call void @_ZN20btAlignedObjectArrayI8GIM_PAIRE9push_backERKS0_(%class.btAlignedObjectArray.8* %0, %struct.GIM_PAIR* nonnull align 4 dereferenceable(8) %ref.tmp)
  %4 = bitcast %struct.GIM_PAIR* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %4) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_ZNK6btAABB23overlapping_trans_cacheERKS_RK26BT_BOX_BOX_TRANSFORM_CACHEb(%class.btAABB* %this, %class.btAABB* nonnull align 4 dereferenceable(32) %box, %class.BT_BOX_BOX_TRANSFORM_CACHE* nonnull align 4 dereferenceable(112) %transcache, i1 zeroext %fulltest) #3 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btAABB*, align 4
  %box.addr = alloca %class.btAABB*, align 4
  %transcache.addr = alloca %class.BT_BOX_BOX_TRANSFORM_CACHE*, align 4
  %fulltest.addr = alloca i8, align 1
  %ea = alloca %class.btVector3, align 4
  %eb = alloca %class.btVector3, align 4
  %ca = alloca %class.btVector3, align 4
  %cb = alloca %class.btVector3, align 4
  %T = alloca %class.btVector3, align 4
  %t = alloca float, align 4
  %t2 = alloca float, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %j = alloca i32, align 4
  %m = alloca i32, align 4
  %n = alloca i32, align 4
  %o = alloca i32, align 4
  %p = alloca i32, align 4
  %q = alloca i32, align 4
  %r = alloca i32, align 4
  store %class.btAABB* %this, %class.btAABB** %this.addr, align 4, !tbaa !2
  store %class.btAABB* %box, %class.btAABB** %box.addr, align 4, !tbaa !2
  store %class.BT_BOX_BOX_TRANSFORM_CACHE* %transcache, %class.BT_BOX_BOX_TRANSFORM_CACHE** %transcache.addr, align 4, !tbaa !2
  %frombool = zext i1 %fulltest to i8
  store i8 %frombool, i8* %fulltest.addr, align 1, !tbaa !16
  %this1 = load %class.btAABB*, %class.btAABB** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ea to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ea)
  %1 = bitcast %class.btVector3* %eb to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #8
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %eb)
  %2 = bitcast %class.btVector3* %ca to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #8
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ca)
  %3 = bitcast %class.btVector3* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #8
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %cb)
  call void @_ZNK6btAABB17get_center_extendER9btVector3S1_(%class.btAABB* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %ca, %class.btVector3* nonnull align 4 dereferenceable(16) %ea)
  %4 = load %class.btAABB*, %class.btAABB** %box.addr, align 4, !tbaa !2
  call void @_ZNK6btAABB17get_center_extendER9btVector3S1_(%class.btAABB* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %cb, %class.btVector3* nonnull align 4 dereferenceable(16) %eb)
  %5 = bitcast %class.btVector3* %T to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #8
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %T)
  %6 = bitcast float* %t to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = bitcast float* %t2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %9, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %10 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %transcache.addr, align 4, !tbaa !2
  %m_R1to0 = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %10, i32 0, i32 1
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_R1to0, i32 %11)
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %cb)
  %12 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %transcache.addr, align 4, !tbaa !2
  %m_T1to0 = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %12, i32 0, i32 0
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_T1to0)
  %13 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %call8, i32 %13
  %14 = load float, float* %arrayidx, align 4, !tbaa !8
  %add = fadd float %call7, %14
  %call9 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ca)
  %15 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %15
  %16 = load float, float* %arrayidx10, align 4, !tbaa !8
  %sub = fsub float %add, %16
  %call11 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %T)
  %17 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds float, float* %call11, i32 %17
  store float %sub, float* %arrayidx12, align 4, !tbaa !8
  %18 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %transcache.addr, align 4, !tbaa !2
  %m_AR = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %18, i32 0, i32 2
  %19 = load i32, i32* %i, align 4, !tbaa !6
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_AR, i32 %19)
  %call14 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call13, %class.btVector3* nonnull align 4 dereferenceable(16) %eb)
  %call15 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ea)
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 %20
  %21 = load float, float* %arrayidx16, align 4, !tbaa !8
  %add17 = fadd float %call14, %21
  store float %add17, float* %t, align 4, !tbaa !8
  %call18 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %T)
  %22 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 %22
  %23 = load float, float* %arrayidx19, align 4, !tbaa !8
  %call20 = call float @_Z6btFabsf(float %23)
  %24 = load float, float* %t, align 4, !tbaa !8
  %cmp21 = fcmp ogt float %call20, %24
  br i1 %cmp21, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup118

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %25 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %25, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc36, %for.end
  %26 = load i32, i32* %i, align 4, !tbaa !6
  %cmp23 = icmp slt i32 %26, 3
  br i1 %cmp23, label %for.body24, label %for.end38

for.body24:                                       ; preds = %for.cond22
  %27 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %transcache.addr, align 4, !tbaa !2
  %m_R1to025 = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %27, i32 0, i32 1
  %28 = load i32, i32* %i, align 4, !tbaa !6
  %call26 = call float @_Z15bt_mat3_dot_colRK11btMatrix3x3RK9btVector3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_R1to025, %class.btVector3* nonnull align 4 dereferenceable(16) %T, i32 %28)
  store float %call26, float* %t, align 4, !tbaa !8
  %29 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %transcache.addr, align 4, !tbaa !2
  %m_AR27 = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %29, i32 0, i32 2
  %30 = load i32, i32* %i, align 4, !tbaa !6
  %call28 = call float @_Z15bt_mat3_dot_colRK11btMatrix3x3RK9btVector3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_AR27, %class.btVector3* nonnull align 4 dereferenceable(16) %ea, i32 %30)
  %call29 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %eb)
  %31 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx30 = getelementptr inbounds float, float* %call29, i32 %31
  %32 = load float, float* %arrayidx30, align 4, !tbaa !8
  %add31 = fadd float %call28, %32
  store float %add31, float* %t2, align 4, !tbaa !8
  %33 = load float, float* %t, align 4, !tbaa !8
  %call32 = call float @_Z6btFabsf(float %33)
  %34 = load float, float* %t2, align 4, !tbaa !8
  %cmp33 = fcmp ogt float %call32, %34
  br i1 %cmp33, label %if.then34, label %if.end35

if.then34:                                        ; preds = %for.body24
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup118

if.end35:                                         ; preds = %for.body24
  br label %for.inc36

for.inc36:                                        ; preds = %if.end35
  %35 = load i32, i32* %i, align 4, !tbaa !6
  %inc37 = add nsw i32 %35, 1
  store i32 %inc37, i32* %i, align 4, !tbaa !6
  br label %for.cond22

for.end38:                                        ; preds = %for.cond22
  %36 = load i8, i8* %fulltest.addr, align 1, !tbaa !16, !range !17
  %tobool = trunc i8 %36 to i1
  br i1 %tobool, label %if.then39, label %if.end117

if.then39:                                        ; preds = %for.end38
  %37 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #8
  %38 = bitcast i32* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #8
  %39 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #8
  %40 = bitcast i32* %o to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #8
  %41 = bitcast i32* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #8
  %42 = bitcast i32* %q to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #8
  %43 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #8
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond40

for.cond40:                                       ; preds = %for.inc108, %if.then39
  %44 = load i32, i32* %i, align 4, !tbaa !6
  %cmp41 = icmp slt i32 %44, 3
  br i1 %cmp41, label %for.body42, label %for.end110

for.body42:                                       ; preds = %for.cond40
  %45 = load i32, i32* %i, align 4, !tbaa !6
  %add43 = add nsw i32 %45, 1
  %rem = srem i32 %add43, 3
  store i32 %rem, i32* %m, align 4, !tbaa !6
  %46 = load i32, i32* %i, align 4, !tbaa !6
  %add44 = add nsw i32 %46, 2
  %rem45 = srem i32 %add44, 3
  store i32 %rem45, i32* %n, align 4, !tbaa !6
  %47 = load i32, i32* %i, align 4, !tbaa !6
  %cmp46 = icmp eq i32 %47, 0
  %48 = zext i1 %cmp46 to i64
  %cond = select i1 %cmp46, i32 1, i32 0
  store i32 %cond, i32* %o, align 4, !tbaa !6
  %49 = load i32, i32* %i, align 4, !tbaa !6
  %cmp47 = icmp eq i32 %49, 2
  %50 = zext i1 %cmp47 to i64
  %cond48 = select i1 %cmp47, i32 1, i32 2
  store i32 %cond48, i32* %p, align 4, !tbaa !6
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond49

for.cond49:                                       ; preds = %for.inc105, %for.body42
  %51 = load i32, i32* %j, align 4, !tbaa !6
  %cmp50 = icmp slt i32 %51, 3
  br i1 %cmp50, label %for.body51, label %for.end107

for.body51:                                       ; preds = %for.cond49
  %52 = load i32, i32* %j, align 4, !tbaa !6
  %cmp52 = icmp eq i32 %52, 2
  %53 = zext i1 %cmp52 to i64
  %cond53 = select i1 %cmp52, i32 1, i32 2
  store i32 %cond53, i32* %q, align 4, !tbaa !6
  %54 = load i32, i32* %j, align 4, !tbaa !6
  %cmp54 = icmp eq i32 %54, 0
  %55 = zext i1 %cmp54 to i64
  %cond55 = select i1 %cmp54, i32 1, i32 0
  store i32 %cond55, i32* %r, align 4, !tbaa !6
  %call56 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %T)
  %56 = load i32, i32* %n, align 4, !tbaa !6
  %arrayidx57 = getelementptr inbounds float, float* %call56, i32 %56
  %57 = load float, float* %arrayidx57, align 4, !tbaa !8
  %58 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %transcache.addr, align 4, !tbaa !2
  %m_R1to058 = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %58, i32 0, i32 1
  %59 = load i32, i32* %m, align 4, !tbaa !6
  %call59 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_R1to058, i32 %59)
  %call60 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call59)
  %60 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx61 = getelementptr inbounds float, float* %call60, i32 %60
  %61 = load float, float* %arrayidx61, align 4, !tbaa !8
  %mul = fmul float %57, %61
  %call62 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %T)
  %62 = load i32, i32* %m, align 4, !tbaa !6
  %arrayidx63 = getelementptr inbounds float, float* %call62, i32 %62
  %63 = load float, float* %arrayidx63, align 4, !tbaa !8
  %64 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %transcache.addr, align 4, !tbaa !2
  %m_R1to064 = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %64, i32 0, i32 1
  %65 = load i32, i32* %n, align 4, !tbaa !6
  %call65 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_R1to064, i32 %65)
  %call66 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call65)
  %66 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 %66
  %67 = load float, float* %arrayidx67, align 4, !tbaa !8
  %mul68 = fmul float %63, %67
  %sub69 = fsub float %mul, %mul68
  store float %sub69, float* %t, align 4, !tbaa !8
  %call70 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ea)
  %68 = load i32, i32* %o, align 4, !tbaa !6
  %arrayidx71 = getelementptr inbounds float, float* %call70, i32 %68
  %69 = load float, float* %arrayidx71, align 4, !tbaa !8
  %70 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %transcache.addr, align 4, !tbaa !2
  %m_AR72 = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %70, i32 0, i32 2
  %71 = load i32, i32* %p, align 4, !tbaa !6
  %call73 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_AR72, i32 %71)
  %call74 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call73)
  %72 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx75 = getelementptr inbounds float, float* %call74, i32 %72
  %73 = load float, float* %arrayidx75, align 4, !tbaa !8
  %mul76 = fmul float %69, %73
  %call77 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ea)
  %74 = load i32, i32* %p, align 4, !tbaa !6
  %arrayidx78 = getelementptr inbounds float, float* %call77, i32 %74
  %75 = load float, float* %arrayidx78, align 4, !tbaa !8
  %76 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %transcache.addr, align 4, !tbaa !2
  %m_AR79 = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %76, i32 0, i32 2
  %77 = load i32, i32* %o, align 4, !tbaa !6
  %call80 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_AR79, i32 %77)
  %call81 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call80)
  %78 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx82 = getelementptr inbounds float, float* %call81, i32 %78
  %79 = load float, float* %arrayidx82, align 4, !tbaa !8
  %mul83 = fmul float %75, %79
  %add84 = fadd float %mul76, %mul83
  %call85 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %eb)
  %80 = load i32, i32* %r, align 4, !tbaa !6
  %arrayidx86 = getelementptr inbounds float, float* %call85, i32 %80
  %81 = load float, float* %arrayidx86, align 4, !tbaa !8
  %82 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %transcache.addr, align 4, !tbaa !2
  %m_AR87 = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %82, i32 0, i32 2
  %83 = load i32, i32* %i, align 4, !tbaa !6
  %call88 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_AR87, i32 %83)
  %call89 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call88)
  %84 = load i32, i32* %q, align 4, !tbaa !6
  %arrayidx90 = getelementptr inbounds float, float* %call89, i32 %84
  %85 = load float, float* %arrayidx90, align 4, !tbaa !8
  %mul91 = fmul float %81, %85
  %add92 = fadd float %add84, %mul91
  %call93 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %eb)
  %86 = load i32, i32* %q, align 4, !tbaa !6
  %arrayidx94 = getelementptr inbounds float, float* %call93, i32 %86
  %87 = load float, float* %arrayidx94, align 4, !tbaa !8
  %88 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %transcache.addr, align 4, !tbaa !2
  %m_AR95 = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %88, i32 0, i32 2
  %89 = load i32, i32* %i, align 4, !tbaa !6
  %call96 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_AR95, i32 %89)
  %call97 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call96)
  %90 = load i32, i32* %r, align 4, !tbaa !6
  %arrayidx98 = getelementptr inbounds float, float* %call97, i32 %90
  %91 = load float, float* %arrayidx98, align 4, !tbaa !8
  %mul99 = fmul float %87, %91
  %add100 = fadd float %add92, %mul99
  store float %add100, float* %t2, align 4, !tbaa !8
  %92 = load float, float* %t, align 4, !tbaa !8
  %call101 = call float @_Z6btFabsf(float %92)
  %93 = load float, float* %t2, align 4, !tbaa !8
  %cmp102 = fcmp ogt float %call101, %93
  br i1 %cmp102, label %if.then103, label %if.end104

if.then103:                                       ; preds = %for.body51
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end104:                                        ; preds = %for.body51
  br label %for.inc105

for.inc105:                                       ; preds = %if.end104
  %94 = load i32, i32* %j, align 4, !tbaa !6
  %inc106 = add nsw i32 %94, 1
  store i32 %inc106, i32* %j, align 4, !tbaa !6
  br label %for.cond49

for.end107:                                       ; preds = %for.cond49
  br label %for.inc108

for.inc108:                                       ; preds = %for.end107
  %95 = load i32, i32* %i, align 4, !tbaa !6
  %inc109 = add nsw i32 %95, 1
  store i32 %inc109, i32* %i, align 4, !tbaa !6
  br label %for.cond40

for.end110:                                       ; preds = %for.cond40
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end110, %if.then103
  %96 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #8
  %97 = bitcast i32* %q to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #8
  %98 = bitcast i32* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #8
  %99 = bitcast i32* %o to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #8
  %100 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #8
  %101 = bitcast i32* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #8
  %102 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup118 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end117

if.end117:                                        ; preds = %cleanup.cont, %for.end38
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup118

cleanup118:                                       ; preds = %if.end117, %cleanup, %if.then34, %if.then
  %103 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #8
  %104 = bitcast float* %t2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #8
  %105 = bitcast float* %t to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #8
  %106 = bitcast %class.btVector3* %T to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %106) #8
  %107 = bitcast %class.btVector3* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %107) #8
  %108 = bitcast %class.btVector3* %ca to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %108) #8
  %109 = bitcast %class.btVector3* %eb to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %109) #8
  %110 = bitcast %class.btVector3* %ea to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %110) #8
  %111 = load i1, i1* %retval, align 1
  ret i1 %111
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z15bt_mat3_dot_colRK11btMatrix3x3RK9btVector3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %mat, %class.btVector3* nonnull align 4 dereferenceable(16) %vec3, i32 %colindex) #2 comdat {
entry:
  %mat.addr = alloca %class.btMatrix3x3*, align 4
  %vec3.addr = alloca %class.btVector3*, align 4
  %colindex.addr = alloca i32, align 4
  store %class.btMatrix3x3* %mat, %class.btMatrix3x3** %mat.addr, align 4, !tbaa !2
  store %class.btVector3* %vec3, %class.btVector3** %vec3.addr, align 4, !tbaa !2
  store i32 %colindex, i32* %colindex.addr, align 4, !tbaa !6
  %0 = load %class.btVector3*, %class.btVector3** %vec3.addr, align 4, !tbaa !2
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %0)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !8
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4, !tbaa !2
  %call1 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 0)
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call1)
  %3 = load i32, i32* %colindex.addr, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 %3
  %4 = load float, float* %arrayidx3, align 4, !tbaa !8
  %mul = fmul float %1, %4
  %5 = load %class.btVector3*, %class.btVector3** %vec3.addr, align 4, !tbaa !2
  %call4 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %5)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 1
  %6 = load float, float* %arrayidx5, align 4, !tbaa !8
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 1)
  %call7 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call6)
  %8 = load i32, i32* %colindex.addr, align 4, !tbaa !6
  %arrayidx8 = getelementptr inbounds float, float* %call7, i32 %8
  %9 = load float, float* %arrayidx8, align 4, !tbaa !8
  %mul9 = fmul float %6, %9
  %add = fadd float %mul, %mul9
  %10 = load %class.btVector3*, %class.btVector3** %vec3.addr, align 4, !tbaa !2
  %call10 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %10)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 2
  %11 = load float, float* %arrayidx11, align 4, !tbaa !8
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4, !tbaa !2
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 2)
  %call13 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call12)
  %13 = load i32, i32* %colindex.addr, align 4, !tbaa !6
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 %13
  %14 = load float, float* %arrayidx14, align 4, !tbaa !8
  %mul15 = fmul float %11, %14
  %add16 = fadd float %add, %mul15
  ret float %add16
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI8GIM_PAIRE9push_backERKS0_(%class.btAlignedObjectArray.8* %this, %struct.GIM_PAIR* nonnull align 4 dereferenceable(8) %_Val) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %_Val.addr = alloca %struct.GIM_PAIR*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store %struct.GIM_PAIR* %_Val, %struct.GIM_PAIR** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayI8GIM_PAIRE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !6
  %1 = load i32, i32* %sz, align 4, !tbaa !6
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI8GIM_PAIRE8capacityEv(%class.btAlignedObjectArray.8* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI8GIM_PAIRE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI8GIM_PAIRE9allocSizeEi(%class.btAlignedObjectArray.8* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI8GIM_PAIRE7reserveEi(%class.btAlignedObjectArray.8* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %2 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %m_data, align 4, !tbaa !44
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !47
  %arrayidx = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %2, i32 %3
  %4 = bitcast %struct.GIM_PAIR* %arrayidx to i8*
  %5 = bitcast i8* %4 to %struct.GIM_PAIR*
  %6 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %_Val.addr, align 4, !tbaa !2
  %call5 = call %struct.GIM_PAIR* @_ZN8GIM_PAIRC2ERKS_(%struct.GIM_PAIR* %5, %struct.GIM_PAIR* nonnull align 4 dereferenceable(8) %6)
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size6, align 4, !tbaa !47
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size6, align 4, !tbaa !47
  %8 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.GIM_PAIR* @_ZN8GIM_PAIRC2Eii(%struct.GIM_PAIR* returned %this, i32 %index1, i32 %index2) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %struct.GIM_PAIR*, align 4
  %index1.addr = alloca i32, align 4
  %index2.addr = alloca i32, align 4
  store %struct.GIM_PAIR* %this, %struct.GIM_PAIR** %this.addr, align 4, !tbaa !2
  store i32 %index1, i32* %index1.addr, align 4, !tbaa !6
  store i32 %index2, i32* %index2.addr, align 4, !tbaa !6
  %this1 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %this.addr, align 4
  %0 = load i32, i32* %index1.addr, align 4, !tbaa !6
  %m_index1 = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %this1, i32 0, i32 0
  store i32 %0, i32* %m_index1, align 4, !tbaa !48
  %1 = load i32, i32* %index2.addr, align 4, !tbaa !6
  %m_index2 = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %this1, i32 0, i32 1
  store i32 %1, i32* %m_index2, align 4, !tbaa !50
  ret %struct.GIM_PAIR* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI8GIM_PAIRE4sizeEv(%class.btAlignedObjectArray.8* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !47
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI8GIM_PAIRE8capacityEv(%class.btAlignedObjectArray.8* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !51
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI8GIM_PAIRE7reserveEi(%class.btAlignedObjectArray.8* %this, i32 %_Count) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.GIM_PAIR*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI8GIM_PAIRE8capacityEv(%class.btAlignedObjectArray.8* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.GIM_PAIR** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayI8GIM_PAIRE8allocateEi(%class.btAlignedObjectArray.8* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.GIM_PAIR*
  store %struct.GIM_PAIR* %3, %struct.GIM_PAIR** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI8GIM_PAIRE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  %4 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI8GIM_PAIRE4copyEiiPS0_(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call3, %struct.GIM_PAIR* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI8GIM_PAIRE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayI8GIM_PAIRE7destroyEii(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI8GIM_PAIRE10deallocateEv(%class.btAlignedObjectArray.8* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !52
  %5 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %struct.GIM_PAIR* %5, %struct.GIM_PAIR** %m_data, align 4, !tbaa !44
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !51
  %7 = bitcast %struct.GIM_PAIR** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI8GIM_PAIRE9allocSizeEi(%class.btAlignedObjectArray.8* %this, i32 %size) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.GIM_PAIR* @_ZN8GIM_PAIRC2ERKS_(%struct.GIM_PAIR* returned %this, %struct.GIM_PAIR* nonnull align 4 dereferenceable(8) %p) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %struct.GIM_PAIR*, align 4
  %p.addr = alloca %struct.GIM_PAIR*, align 4
  store %struct.GIM_PAIR* %this, %struct.GIM_PAIR** %this.addr, align 4, !tbaa !2
  store %struct.GIM_PAIR* %p, %struct.GIM_PAIR** %p.addr, align 4, !tbaa !2
  %this1 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %this.addr, align 4
  %0 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %p.addr, align 4, !tbaa !2
  %m_index1 = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %0, i32 0, i32 0
  %1 = load i32, i32* %m_index1, align 4, !tbaa !48
  %m_index12 = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %this1, i32 0, i32 0
  store i32 %1, i32* %m_index12, align 4, !tbaa !48
  %2 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %p.addr, align 4, !tbaa !2
  %m_index2 = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %2, i32 0, i32 1
  %3 = load i32, i32* %m_index2, align 4, !tbaa !50
  %m_index23 = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %this1, i32 0, i32 1
  store i32 %3, i32* %m_index23, align 4, !tbaa !50
  ret %struct.GIM_PAIR* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI8GIM_PAIRE8allocateEi(%class.btAlignedObjectArray.8* %this, i32 %size) #3 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call %struct.GIM_PAIR* @_ZN18btAlignedAllocatorI8GIM_PAIRLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.9* %m_allocator, i32 %1, %struct.GIM_PAIR** null)
  %2 = bitcast %struct.GIM_PAIR* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI8GIM_PAIRE4copyEiiPS0_(%class.btAlignedObjectArray.8* %this, i32 %start, i32 %end, %struct.GIM_PAIR* %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.GIM_PAIR*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store %struct.GIM_PAIR* %dest, %struct.GIM_PAIR** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %4, i32 %5
  %6 = bitcast %struct.GIM_PAIR* %arrayidx to i8*
  %7 = bitcast i8* %6 to %struct.GIM_PAIR*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %8 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %m_data, align 4, !tbaa !44
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %8, i32 %9
  %call = call %struct.GIM_PAIR* @_ZN8GIM_PAIRC2ERKS_(%struct.GIM_PAIR* %7, %struct.GIM_PAIR* nonnull align 4 dereferenceable(8) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI8GIM_PAIRE7destroyEii(%class.btAlignedObjectArray.8* %this, i32 %first, i32 %last) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %4 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %m_data, align 4, !tbaa !44
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI8GIM_PAIRE10deallocateEv(%class.btAlignedObjectArray.8* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %m_data, align 4, !tbaa !44
  %tobool = icmp ne %struct.GIM_PAIR* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !52, !range !17
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %2 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %m_data4, align 4, !tbaa !44
  call void @_ZN18btAlignedAllocatorI8GIM_PAIRLj16EE10deallocateEPS0_(%class.btAlignedAllocator.9* %m_allocator, %struct.GIM_PAIR* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %struct.GIM_PAIR* null, %struct.GIM_PAIR** %m_data5, align 4, !tbaa !44
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden %struct.GIM_PAIR* @_ZN18btAlignedAllocatorI8GIM_PAIRLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.9* %this, i32 %n, %struct.GIM_PAIR** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.GIM_PAIR**, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store %struct.GIM_PAIR** %hint, %struct.GIM_PAIR*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 8, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.GIM_PAIR*
  ret %struct.GIM_PAIR* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #6

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI8GIM_PAIRLj16EE10deallocateEPS0_(%class.btAlignedAllocator.9* %this, %struct.GIM_PAIR* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %ptr.addr = alloca %struct.GIM_PAIR*, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4, !tbaa !2
  store %struct.GIM_PAIR* %ptr, %struct.GIM_PAIR** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.GIM_PAIR* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden %struct.GIM_BVH_DATA* @_ZN12GIM_BVH_DATAC2ERKS_(%struct.GIM_BVH_DATA* returned %this, %struct.GIM_BVH_DATA* nonnull align 4 dereferenceable(36) %0) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %struct.GIM_BVH_DATA*, align 4
  %.addr = alloca %struct.GIM_BVH_DATA*, align 4
  store %struct.GIM_BVH_DATA* %this, %struct.GIM_BVH_DATA** %this.addr, align 4, !tbaa !2
  store %struct.GIM_BVH_DATA* %0, %struct.GIM_BVH_DATA** %.addr, align 4, !tbaa !2
  %this1 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %this.addr, align 4
  %m_bound = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %this1, i32 0, i32 0
  %1 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %.addr, align 4, !tbaa !2
  %m_bound2 = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %1, i32 0, i32 0
  %call = call %class.btAABB* @_ZN6btAABBC2ERKS_(%class.btAABB* %m_bound, %class.btAABB* nonnull align 4 dereferenceable(32) %m_bound2)
  %m_data = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %this1, i32 0, i32 1
  %2 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %.addr, align 4, !tbaa !2
  %m_data3 = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %2, i32 0, i32 1
  %3 = load i32, i32* %m_data3, align 4, !tbaa !22
  store i32 %3, i32* %m_data, align 4, !tbaa !22
  ret %struct.GIM_BVH_DATA* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAABB* @_ZN6btAABBC2ERKS_(%class.btAABB* returned %this, %class.btAABB* nonnull align 4 dereferenceable(32) %other) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAABB*, align 4
  %other.addr = alloca %class.btAABB*, align 4
  store %class.btAABB* %this, %class.btAABB** %this.addr, align 4, !tbaa !2
  store %class.btAABB* %other, %class.btAABB** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btAABB*, %class.btAABB** %this.addr, align 4
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %0 = load %class.btAABB*, %class.btAABB** %other.addr, align 4, !tbaa !2
  %m_min2 = getelementptr inbounds %class.btAABB, %class.btAABB* %0, i32 0, i32 0
  %1 = bitcast %class.btVector3* %m_min to i8*
  %2 = bitcast %class.btVector3* %m_min2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !10
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %3 = load %class.btAABB*, %class.btAABB** %other.addr, align 4, !tbaa !2
  %m_max3 = getelementptr inbounds %class.btAABB, %class.btAABB* %3, i32 0, i32 1
  %4 = bitcast %class.btVector3* %m_max to i8*
  %5 = bitcast %class.btVector3* %m_max3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !10
  ret %class.btAABB* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE4sizeEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !32
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.GIM_BVH_TREE_NODE*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.GIM_BVH_TREE_NODE** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.GIM_BVH_TREE_NODE*
  store %class.GIM_BVH_TREE_NODE* %3, %class.GIM_BVH_TREE_NODE** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE4sizeEv(%class.btAlignedObjectArray* %this1)
  %4 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %class.GIM_BVH_TREE_NODE* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !53
  %5 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.GIM_BVH_TREE_NODE* %5, %class.GIM_BVH_TREE_NODE** %m_data, align 4, !tbaa !27
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !54
  %7 = bitcast %class.GIM_BVH_TREE_NODE** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.GIM_BVH_TREE_NODE* @_ZN17GIM_BVH_TREE_NODEC2ERKS_(%class.GIM_BVH_TREE_NODE* returned %this, %class.GIM_BVH_TREE_NODE* nonnull align 4 dereferenceable(36) %0) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.GIM_BVH_TREE_NODE*, align 4
  %.addr = alloca %class.GIM_BVH_TREE_NODE*, align 4
  store %class.GIM_BVH_TREE_NODE* %this, %class.GIM_BVH_TREE_NODE** %this.addr, align 4, !tbaa !2
  store %class.GIM_BVH_TREE_NODE* %0, %class.GIM_BVH_TREE_NODE** %.addr, align 4, !tbaa !2
  %this1 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %this.addr, align 4
  %m_bound = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %this1, i32 0, i32 0
  %1 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %.addr, align 4, !tbaa !2
  %m_bound2 = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %1, i32 0, i32 0
  %call = call %class.btAABB* @_ZN6btAABBC2ERKS_(%class.btAABB* %m_bound, %class.btAABB* nonnull align 4 dereferenceable(32) %m_bound2)
  %m_escapeIndexOrDataIndex = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %this1, i32 0, i32 1
  %2 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %.addr, align 4, !tbaa !2
  %m_escapeIndexOrDataIndex3 = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %2, i32 0, i32 1
  %3 = load i32, i32* %m_escapeIndexOrDataIndex3, align 4, !tbaa !30
  store i32 %3, i32* %m_escapeIndexOrDataIndex, align 4, !tbaa !30
  ret %class.GIM_BVH_TREE_NODE* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE8capacityEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !54
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #3 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call %class.GIM_BVH_TREE_NODE* @_ZN18btAlignedAllocatorI17GIM_BVH_TREE_NODELj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %class.GIM_BVH_TREE_NODE** null)
  %2 = bitcast %class.GIM_BVH_TREE_NODE* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %class.GIM_BVH_TREE_NODE* %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.GIM_BVH_TREE_NODE*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store %class.GIM_BVH_TREE_NODE* %dest, %class.GIM_BVH_TREE_NODE** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %4, i32 %5
  %6 = bitcast %class.GIM_BVH_TREE_NODE* %arrayidx to i8*
  %7 = bitcast i8* %6 to %class.GIM_BVH_TREE_NODE*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %m_data, align 4, !tbaa !27
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %8, i32 %9
  %call = call %class.GIM_BVH_TREE_NODE* @_ZN17GIM_BVH_TREE_NODEC2ERKS_(%class.GIM_BVH_TREE_NODE* %7, %class.GIM_BVH_TREE_NODE* nonnull align 4 dereferenceable(36) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %m_data, align 4, !tbaa !27
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE10deallocateEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %m_data, align 4, !tbaa !27
  %tobool = icmp ne %class.GIM_BVH_TREE_NODE* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !53, !range !17
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %m_data4, align 4, !tbaa !27
  call void @_ZN18btAlignedAllocatorI17GIM_BVH_TREE_NODELj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %class.GIM_BVH_TREE_NODE* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.GIM_BVH_TREE_NODE* null, %class.GIM_BVH_TREE_NODE** %m_data5, align 4, !tbaa !27
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden %class.GIM_BVH_TREE_NODE* @_ZN18btAlignedAllocatorI17GIM_BVH_TREE_NODELj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %class.GIM_BVH_TREE_NODE** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.GIM_BVH_TREE_NODE**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store %class.GIM_BVH_TREE_NODE** %hint, %class.GIM_BVH_TREE_NODE*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 36, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.GIM_BVH_TREE_NODE*
  ret %class.GIM_BVH_TREE_NODE* %1
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI17GIM_BVH_TREE_NODELj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %class.GIM_BVH_TREE_NODE* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.GIM_BVH_TREE_NODE*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store %class.GIM_BVH_TREE_NODE* %ptr, %class.GIM_BVH_TREE_NODE** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.GIM_BVH_TREE_NODE* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE7reserveEi(%class.btAlignedObjectArray.0* %this, i32 %_Count) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.GIM_BVH_DATA*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.GIM_BVH_DATA** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE8allocateEi(%class.btAlignedObjectArray.0* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.GIM_BVH_DATA*
  store %struct.GIM_BVH_DATA* %3, %struct.GIM_BVH_DATA** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %4 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE4copyEiiPS0_(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call3, %struct.GIM_BVH_DATA* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !42
  %5 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.GIM_BVH_DATA* %5, %struct.GIM_BVH_DATA** %m_data, align 4, !tbaa !12
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !43
  %7 = bitcast %struct.GIM_BVH_DATA** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE8capacityEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !43
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE8allocateEi(%class.btAlignedObjectArray.0* %this, i32 %size) #3 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call %struct.GIM_BVH_DATA* @_ZN18btAlignedAllocatorI12GIM_BVH_DATALj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %m_allocator, i32 %1, %struct.GIM_BVH_DATA** null)
  %2 = bitcast %struct.GIM_BVH_DATA* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE4copyEiiPS0_(%class.btAlignedObjectArray.0* %this, i32 %start, i32 %end, %struct.GIM_BVH_DATA* %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.GIM_BVH_DATA*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store %struct.GIM_BVH_DATA* %dest, %struct.GIM_BVH_DATA** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %4, i32 %5
  %6 = bitcast %struct.GIM_BVH_DATA* %arrayidx to i8*
  %7 = bitcast i8* %6 to %struct.GIM_BVH_DATA*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %8 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %m_data, align 4, !tbaa !12
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %8, i32 %9
  %call = call %struct.GIM_BVH_DATA* @_ZN12GIM_BVH_DATAC2ERKS_(%struct.GIM_BVH_DATA* %7, %struct.GIM_BVH_DATA* nonnull align 4 dereferenceable(36) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret void
}

define linkonce_odr hidden %struct.GIM_BVH_DATA* @_ZN18btAlignedAllocatorI12GIM_BVH_DATALj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %this, i32 %n, %struct.GIM_BVH_DATA** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.GIM_BVH_DATA**, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store %struct.GIM_BVH_DATA** %hint, %struct.GIM_BVH_DATA*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 36, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.GIM_BVH_DATA*
  ret %struct.GIM_BVH_DATA* %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.4* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !55
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.4* %this, i32 %_Count) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca i32*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast i32** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.4* %this1, i32 %2)
  %3 = bitcast i8* %call2 to i32*
  store i32* %3, i32** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %4 = load i32*, i32** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call3, i32* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !56
  %5 = load i32*, i32** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store i32* %5, i32** %m_data, align 4, !tbaa !38
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !55
  %7 = bitcast i32** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIiE9allocSizeEi(%class.btAlignedObjectArray.4* %this, i32 %size) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.4* %this, i32 %size) #3 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.5* %m_allocator, i32 %1, i32** null)
  %2 = bitcast i32* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.4* %this, i32 %start, i32 %end, i32* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store i32* %dest, i32** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i32*, i32** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  %6 = bitcast i32* %arrayidx to i8*
  %7 = bitcast i8* %6 to i32*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %8 = load i32*, i32** %m_data, align 4, !tbaa !38
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds i32, i32* %8, i32 %9
  %10 = load i32, i32* %arrayidx2, align 4, !tbaa !6
  store i32 %10, i32* %7, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.4* %this, i32 %first, i32 %last) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %4 = load i32*, i32** %m_data, align 4, !tbaa !38
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.4* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !38
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !56, !range !17
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4, !tbaa !38
  call void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.5* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4, !tbaa !38
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.5* %this, i32 %n, i32** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca i32**, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store i32** %hint, i32*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to i32*
  ret i32* %1
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.5* %this, i32* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4, !tbaa !2
  store i32* %ptr, i32** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn writeonly }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind readnone speculatable willreturn }
attributes #8 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"float", !4, i64 0}
!10 = !{i64 0, i64 16, !11}
!11 = !{!4, !4, i64 0}
!12 = !{!13, !3, i64 12}
!13 = !{!"_ZTS20btAlignedObjectArrayI12GIM_BVH_DATAE", !14, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !15, i64 16}
!14 = !{!"_ZTS18btAlignedAllocatorI12GIM_BVH_DATALj16EE"}
!15 = !{!"bool", !4, i64 0}
!16 = !{!15, !15, i64 0}
!17 = !{i8 0, i8 2}
!18 = !{i64 0, i64 16, !11, i64 16, i64 16, !11, i64 32, i64 4, !6}
!19 = !{!20, !7, i64 0}
!20 = !{!"_ZTS9btBvhTree", !7, i64 0, !21, i64 4}
!21 = !{!"_ZTS23GIM_BVH_TREE_NODE_ARRAY"}
!22 = !{!23, !7, i64 32}
!23 = !{!"_ZTS12GIM_BVH_DATA", !24, i64 0, !7, i64 32}
!24 = !{!"_ZTS6btAABB", !25, i64 0, !25, i64 16}
!25 = !{!"_ZTS9btVector3", !4, i64 0}
!26 = !{i64 0, i64 16, !11, i64 16, i64 16, !11}
!27 = !{!28, !3, i64 12}
!28 = !{!"_ZTS20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE", !29, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !15, i64 16}
!29 = !{!"_ZTS18btAlignedAllocatorI17GIM_BVH_TREE_NODELj16EE"}
!30 = !{!31, !7, i64 32}
!31 = !{!"_ZTS17GIM_BVH_TREE_NODE", !24, i64 0, !7, i64 32}
!32 = !{!28, !7, i64 4}
!33 = !{!13, !7, i64 4}
!34 = !{!35, !3, i64 24}
!35 = !{!"_ZTS12btGImpactBvh", !20, i64 0, !3, i64 24}
!36 = !{!37, !37, i64 0}
!37 = !{!"vtable pointer", !5, i64 0}
!38 = !{!39, !3, i64 12}
!39 = !{!"_ZTS20btAlignedObjectArrayIiE", !40, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !15, i64 16}
!40 = !{!"_ZTS18btAlignedAllocatorIiLj16EE"}
!41 = !{!39, !7, i64 4}
!42 = !{!13, !15, i64 16}
!43 = !{!13, !7, i64 8}
!44 = !{!45, !3, i64 12}
!45 = !{!"_ZTS20btAlignedObjectArrayI8GIM_PAIRE", !46, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !15, i64 16}
!46 = !{!"_ZTS18btAlignedAllocatorI8GIM_PAIRLj16EE"}
!47 = !{!45, !7, i64 4}
!48 = !{!49, !7, i64 0}
!49 = !{!"_ZTS8GIM_PAIR", !7, i64 0, !7, i64 4}
!50 = !{!49, !7, i64 4}
!51 = !{!45, !7, i64 8}
!52 = !{!45, !15, i64 16}
!53 = !{!28, !15, i64 16}
!54 = !{!28, !7, i64 8}
!55 = !{!39, !7, i64 8}
!56 = !{!39, !15, i64 16}
