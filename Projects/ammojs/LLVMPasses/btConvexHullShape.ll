; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btConvexHullShape.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btConvexHullShape.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btConvexHullShape = type { %class.btPolyhedralConvexAabbCachingShape.base, [3 x i8], %class.btAlignedObjectArray }
%class.btPolyhedralConvexAabbCachingShape.base = type <{ %class.btPolyhedralConvexShape, %class.btVector3, %class.btVector3, i8 }>
%class.btPolyhedralConvexShape = type { %class.btConvexInternalShape, %class.btConvexPolyhedron* }
%class.btConvexInternalShape = type { %class.btConvexShape, %class.btVector3, %class.btVector3, float, float }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%class.btConvexPolyhedron = type opaque
%class.btVector3 = type { [4 x float] }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btPolyhedralConvexAabbCachingShape = type <{ %class.btPolyhedralConvexShape, %class.btVector3, %class.btVector3, i8, [3 x i8] }>
%class.btSerializer = type { i32 (...)** }
%struct.btConvexHullShapeData = type { %struct.btConvexInternalShapeData, %struct.btVector3FloatData*, %struct.btVector3DoubleData*, i32, [4 x i8] }
%struct.btConvexInternalShapeData = type { %struct.btCollisionShapeData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, i32 }
%struct.btCollisionShapeData = type { i8*, i32, [4 x i8] }
%struct.btVector3FloatData = type { [4 x float] }
%struct.btVector3DoubleData = type { [4 x double] }
%class.btChunk = type { i32, i32, i8*, i32, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }

$_ZN20btAlignedObjectArrayI9btVector3EC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_ = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EixEi = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_ = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZNK9btVector36maxDotEPKS_lRf = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3EixEi = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZNK17btConvexHullShape14getScaledPointEi = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZNK21btConvexInternalShape9serializeEPvP12btSerializer = comdat any

$_ZNK9btVector39serializeER18btVector3FloatData = comdat any

$_ZNK11btTransformmlERK9btVector3 = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_Z6btSwapIfEvRT_S1_ = comdat any

$_Z6btSwapI9btVector3EvRT_S2_ = comdat any

$_ZN17btConvexHullShapeD2Ev = comdat any

$_ZN17btConvexHullShapeD0Ev = comdat any

$_ZNK21btConvexInternalShape15getLocalScalingEv = comdat any

$_ZNK17btConvexHullShape7getNameEv = comdat any

$_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZN21btConvexInternalShape9setMarginEf = comdat any

$_ZNK21btConvexInternalShape9getMarginEv = comdat any

$_ZNK17btConvexHullShape28calculateSerializeBufferSizeEv = comdat any

$_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv = comdat any

$_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3 = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_Z6btSqrtf = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK9btVector314serializeFloatER18btVector3FloatData = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZN20btAlignedObjectArrayI9btVector3ED2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E5clearEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E4initEv = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_ = comdat any

$_ZN17btConvexHullShapedlEPv = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7reserveEi = comdat any

$_ZN9btVector3nwEmPv = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi = comdat any

@_ZTV17btConvexHullShape = hidden unnamed_addr constant { [34 x i8*] } { [34 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI17btConvexHullShape to i8*), i8* bitcast (%class.btConvexHullShape* (%class.btConvexHullShape*)* @_ZN17btConvexHullShapeD2Ev to i8*), i8* bitcast (void (%class.btConvexHullShape*)* @_ZN17btConvexHullShapeD0Ev to i8*), i8* bitcast (void (%class.btPolyhedralConvexAabbCachingShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK34btPolyhedralConvexAabbCachingShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btConvexHullShape*, %class.btVector3*)* @_ZN17btConvexHullShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btPolyhedralConvexShape*, float, %class.btVector3*)* @_ZNK23btPolyhedralConvexShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btConvexHullShape*)* @_ZNK17btConvexHullShape7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, float)* @_ZN21btConvexInternalShape9setMarginEf to i8*), i8* bitcast (float (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btConvexHullShape*)* @_ZNK17btConvexHullShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btConvexHullShape*, i8*, %class.btSerializer*)* @_ZNK17btConvexHullShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btVector3*, %class.btConvexHullShape*, %class.btVector3*)* @_ZNK17btConvexHullShape24localGetSupportingVertexERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btConvexHullShape*, %class.btVector3*)* @_ZNK17btConvexHullShape37localGetSupportingVertexWithoutMarginERK9btVector3 to i8*), i8* bitcast (void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, float*, float*)* @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_ to i8*), i8* bitcast (void (%class.btConvexHullShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK17btConvexHullShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, i32, %class.btVector3*)* @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3 to i8*), i8* bitcast (i1 (%class.btPolyhedralConvexShape*, i32)* @_ZN23btPolyhedralConvexShape28initializePolyhedralFeaturesEi to i8*), i8* bitcast (i32 (%class.btConvexHullShape*)* @_ZNK17btConvexHullShape14getNumVerticesEv to i8*), i8* bitcast (i32 (%class.btConvexHullShape*)* @_ZNK17btConvexHullShape11getNumEdgesEv to i8*), i8* bitcast (void (%class.btConvexHullShape*, i32, %class.btVector3*, %class.btVector3*)* @_ZNK17btConvexHullShape7getEdgeEiR9btVector3S1_ to i8*), i8* bitcast (void (%class.btConvexHullShape*, i32, %class.btVector3*)* @_ZNK17btConvexHullShape9getVertexEiR9btVector3 to i8*), i8* bitcast (i32 (%class.btConvexHullShape*)* @_ZNK17btConvexHullShape12getNumPlanesEv to i8*), i8* bitcast (void (%class.btConvexHullShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK17btConvexHullShape8getPlaneER9btVector3S1_i to i8*), i8* bitcast (i1 (%class.btConvexHullShape*, %class.btVector3*, float)* @_ZNK17btConvexHullShape8isInsideERK9btVector3f to i8*), i8* bitcast (void (%class.btConvexHullShape*, %class.btTransform*, %class.btVector3*, float*, float*, %class.btVector3*, %class.btVector3*)* @_ZNK17btConvexHullShape7projectERK11btTransformRK9btVector3RfS6_RS3_S7_ to i8*)] }, align 4
@.str = private unnamed_addr constant [19 x i8] c"btVector3FloatData\00", align 1
@.str.1 = private unnamed_addr constant [22 x i8] c"btConvexHullShapeData\00", align 1
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS17btConvexHullShape = hidden constant [20 x i8] c"17btConvexHullShape\00", align 1
@_ZTI34btPolyhedralConvexAabbCachingShape = external constant i8*
@_ZTI17btConvexHullShape = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([20 x i8], [20 x i8]* @_ZTS17btConvexHullShape, i32 0, i32 0), i8* bitcast (i8** @_ZTI34btPolyhedralConvexAabbCachingShape to i8*) }, align 4
@.str.2 = private unnamed_addr constant [26 x i8] c"btConvexInternalShapeData\00", align 1
@.str.3 = private unnamed_addr constant [7 x i8] c"Convex\00", align 1

@_ZN17btConvexHullShapeC1EPKfii = hidden unnamed_addr alias %class.btConvexHullShape* (%class.btConvexHullShape*, float*, i32, i32), %class.btConvexHullShape* (%class.btConvexHullShape*, float*, i32, i32)* @_ZN17btConvexHullShapeC2EPKfii

define hidden %class.btConvexHullShape* @_ZN17btConvexHullShapeC2EPKfii(%class.btConvexHullShape* returned %this, float* %points, i32 %numPoints, i32 %stride) unnamed_addr #0 {
entry:
  %retval = alloca %class.btConvexHullShape*, align 4
  %this.addr = alloca %class.btConvexHullShape*, align 4
  %points.addr = alloca float*, align 4
  %numPoints.addr = alloca i32, align 4
  %stride.addr = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %pointsAddress = alloca i8*, align 4
  %i = alloca i32, align 4
  %point = alloca float*, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  store %class.btConvexHullShape* %this, %class.btConvexHullShape** %this.addr, align 4, !tbaa !2
  store float* %points, float** %points.addr, align 4, !tbaa !2
  store i32 %numPoints, i32* %numPoints.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  %this1 = load %class.btConvexHullShape*, %class.btConvexHullShape** %this.addr, align 4
  store %class.btConvexHullShape* %this1, %class.btConvexHullShape** %retval, align 4
  %0 = bitcast %class.btConvexHullShape* %this1 to %class.btPolyhedralConvexAabbCachingShape*
  %call = call %class.btPolyhedralConvexAabbCachingShape* @_ZN34btPolyhedralConvexAabbCachingShapeC2Ev(%class.btPolyhedralConvexAabbCachingShape* %0)
  %1 = bitcast %class.btConvexHullShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [34 x i8*] }, { [34 x i8*] }* @_ZTV17btConvexHullShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !8
  %m_unscaledPoints = getelementptr inbounds %class.btConvexHullShape, %class.btConvexHullShape* %this1, i32 0, i32 2
  %call2 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* %m_unscaledPoints)
  %2 = bitcast %class.btConvexHullShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %2, i32 0, i32 1
  store i32 4, i32* %m_shapeType, align 4, !tbaa !10
  %m_unscaledPoints3 = getelementptr inbounds %class.btConvexHullShape, %class.btConvexHullShape* %this1, i32 0, i32 2
  %3 = load i32, i32* %numPoints.addr, align 4, !tbaa !6
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #8
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray* %m_unscaledPoints3, i32 %3, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #8
  %6 = bitcast i8** %pointsAddress to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load float*, float** %points.addr, align 4, !tbaa !2
  %8 = bitcast float* %7 to i8*
  store i8* %8, i8** %pointsAddress, align 4, !tbaa !2
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %11 = load i32, i32* %numPoints.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %10, %11
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %13 = bitcast float** %point to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load i8*, i8** %pointsAddress, align 4, !tbaa !2
  %15 = bitcast i8* %14 to float*
  store float* %15, float** %point, align 4, !tbaa !2
  %16 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #8
  %17 = load float*, float** %point, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds float, float* %17, i32 0
  %18 = load float*, float** %point, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds float, float* %18, i32 1
  %19 = load float*, float** %point, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds float, float* %19, i32 2
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp5, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx6, float* nonnull align 4 dereferenceable(4) %arrayidx7)
  %m_unscaledPoints9 = getelementptr inbounds %class.btConvexHullShape, %class.btConvexHullShape* %this1, i32 0, i32 2
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %call10 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_unscaledPoints9, i32 %20)
  %21 = bitcast %class.btVector3* %call10 to i8*
  %22 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false), !tbaa.struct !12
  %23 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %23) #8
  %24 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %25 = load i8*, i8** %pointsAddress, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %25, i32 %24
  store i8* %add.ptr, i8** %pointsAddress, align 4, !tbaa !2
  %26 = bitcast float** %point to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %27 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %27, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %28 = bitcast %class.btConvexHullShape* %this1 to %class.btPolyhedralConvexAabbCachingShape*
  call void @_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv(%class.btPolyhedralConvexAabbCachingShape* %28)
  %29 = bitcast i8** %pointsAddress to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #8
  %30 = load %class.btConvexHullShape*, %class.btConvexHullShape** %retval, align 4
  ret %class.btConvexHullShape* %30
}

declare %class.btPolyhedralConvexAabbCachingShape* @_ZN34btPolyhedralConvexAabbCachingShapeC2Ev(%class.btPolyhedralConvexAabbCachingShape* returned) unnamed_addr #1

define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray* %this, i32 %newsize, %class.btVector3* nonnull align 4 dereferenceable(16) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btVector3*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !6
  store %class.btVector3* %fillData, %class.btVector3** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !6
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %2 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  store i32 %4, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !14
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end17

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load i32, i32* %curSize, align 4, !tbaa !6
  store i32 %14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc14, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !6
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  br label %for.end16

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %18 = load %class.btVector3*, %class.btVector3** %m_data11, align 4, !tbaa !14
  %19 = load i32, i32* %i6, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds %class.btVector3, %class.btVector3* %18, i32 %19
  %20 = bitcast %class.btVector3* %arrayidx12 to i8*
  %call13 = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %20)
  %21 = bitcast i8* %call13 to %class.btVector3*
  %22 = load %class.btVector3*, %class.btVector3** %fillData.addr, align 4, !tbaa !2
  %23 = bitcast %class.btVector3* %21 to i8*
  %24 = bitcast %class.btVector3* %22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false), !tbaa.struct !12
  br label %for.inc14

for.inc14:                                        ; preds = %for.body10
  %25 = load i32, i32* %i6, align 4, !tbaa !6
  %inc15 = add nsw i32 %25, 1
  store i32 %inc15, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.end16:                                        ; preds = %for.cond.cleanup9
  br label %if.end17

if.end17:                                         ; preds = %for.end16, %for.end
  %26 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %26, i32* %m_size, align 4, !tbaa !18
  %27 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #8
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !19
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !19
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !19
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !19
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !19
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !19
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !19
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %this, i32 %n) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !14
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %1
  ret %class.btVector3* %arrayidx
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

declare void @_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv(%class.btPolyhedralConvexAabbCachingShape*) #1

define hidden void @_ZN17btConvexHullShape15setLocalScalingERK9btVector3(%class.btConvexHullShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %scaling) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConvexHullShape*, align 4
  %scaling.addr = alloca %class.btVector3*, align 4
  store %class.btConvexHullShape* %this, %class.btConvexHullShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %scaling, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexHullShape*, %class.btConvexHullShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  %1 = bitcast %class.btConvexHullShape* %this1 to %class.btConvexInternalShape*
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_localScaling to i8*
  %3 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !12
  %4 = bitcast %class.btConvexHullShape* %this1 to %class.btPolyhedralConvexAabbCachingShape*
  call void @_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv(%class.btPolyhedralConvexAabbCachingShape* %4)
  ret void
}

define hidden void @_ZN17btConvexHullShape8addPointERK9btVector3b(%class.btConvexHullShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %point, i1 zeroext %recalculateLocalAabb) #0 {
entry:
  %this.addr = alloca %class.btConvexHullShape*, align 4
  %point.addr = alloca %class.btVector3*, align 4
  %recalculateLocalAabb.addr = alloca i8, align 1
  store %class.btConvexHullShape* %this, %class.btConvexHullShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %point, %class.btVector3** %point.addr, align 4, !tbaa !2
  %frombool = zext i1 %recalculateLocalAabb to i8
  store i8 %frombool, i8* %recalculateLocalAabb.addr, align 1, !tbaa !21
  %this1 = load %class.btConvexHullShape*, %class.btConvexHullShape** %this.addr, align 4
  %m_unscaledPoints = getelementptr inbounds %class.btConvexHullShape, %class.btConvexHullShape* %this1, i32 0, i32 2
  %0 = load %class.btVector3*, %class.btVector3** %point.addr, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray* %m_unscaledPoints, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  %1 = load i8, i8* %recalculateLocalAabb.addr, align 1, !tbaa !21, !range !22
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %class.btConvexHullShape* %this1 to %class.btPolyhedralConvexAabbCachingShape*
  call void @_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv(%class.btPolyhedralConvexAabbCachingShape* %2)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %class.btVector3*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %_Val, %class.btVector3** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !6
  %1 = load i32, i32* %sz, align 4, !tbaa !6
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !14
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 %3
  %4 = bitcast %class.btVector3* %arrayidx to i8*
  %call5 = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %4)
  %5 = bitcast i8* %call5 to %class.btVector3*
  %6 = load %class.btVector3*, %class.btVector3** %_Val.addr, align 4, !tbaa !2
  %7 = bitcast %class.btVector3* %5 to i8*
  %8 = bitcast %class.btVector3* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !12
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %9 = load i32, i32* %m_size6, align 4, !tbaa !18
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %m_size6, align 4, !tbaa !18
  %10 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  ret void
}

define hidden void @_ZNK17btConvexHullShape37localGetSupportingVertexWithoutMarginERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btConvexHullShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %vec) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConvexHullShape*, align 4
  %vec.addr = alloca %class.btVector3*, align 4
  %supVec = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %maxDot = alloca float, align 4
  %scaled = alloca %class.btVector3, align 4
  %index = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btConvexHullShape* %this, %class.btConvexHullShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %vec, %class.btVector3** %vec.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexHullShape*, %class.btConvexHullShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %supVec to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !19
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !19
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !19
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %supVec, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %4 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  %6 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %7 = bitcast float* %maxDot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  store float 0xC3ABC16D60000000, float* %maxDot, align 4, !tbaa !19
  %m_unscaledPoints = getelementptr inbounds %class.btConvexHullShape, %class.btConvexHullShape* %this1, i32 0, i32 2
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %m_unscaledPoints)
  %cmp = icmp slt i32 0, %call4
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %8 = bitcast %class.btVector3* %scaled to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #8
  %9 = load %class.btVector3*, %class.btVector3** %vec.addr, align 4, !tbaa !2
  %10 = bitcast %class.btConvexHullShape* %this1 to %class.btConvexInternalShape*
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %10, i32 0, i32 1
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %scaled, %class.btVector3* nonnull align 4 dereferenceable(16) %9, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling)
  %11 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  %m_unscaledPoints5 = getelementptr inbounds %class.btConvexHullShape, %class.btConvexHullShape* %this1, i32 0, i32 2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_unscaledPoints5, i32 0)
  %m_unscaledPoints7 = getelementptr inbounds %class.btConvexHullShape, %class.btConvexHullShape* %this1, i32 0, i32 2
  %call8 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %m_unscaledPoints7)
  %call9 = call i32 @_ZNK9btVector36maxDotEPKS_lRf(%class.btVector3* %scaled, %class.btVector3* %call6, i32 %call8, float* nonnull align 4 dereferenceable(4) %maxDot)
  store i32 %call9, i32* %index, align 4, !tbaa !6
  %m_unscaledPoints10 = getelementptr inbounds %class.btConvexHullShape, %class.btConvexHullShape* %this1, i32 0, i32 2
  %12 = load i32, i32* %index, align 4, !tbaa !6
  %call11 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_unscaledPoints10, i32 %12)
  %13 = bitcast %class.btConvexHullShape* %this1 to %class.btConvexInternalShape*
  %m_localScaling12 = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %13, i32 0, i32 1
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %call11, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling12)
  store i32 1, i32* %cleanup.dest.slot, align 4
  %14 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #8
  %15 = bitcast %class.btVector3* %scaled to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %15) #8
  br label %cleanup

if.end:                                           ; preds = %entry
  %16 = bitcast %class.btVector3* %agg.result to i8*
  %17 = bitcast %class.btVector3* %supVec to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false), !tbaa.struct !12
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %18 = bitcast float* %maxDot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  %19 = bitcast %class.btVector3* %supVec to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %19) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !18
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !19
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !19
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !19
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !19
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !19
  %mul8 = fmul float %7, %9
  store float %mul8, float* %ref.tmp3, align 4, !tbaa !19
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !19
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !19
  %mul14 = fmul float %12, %14
  store float %mul14, float* %ref.tmp9, align 4, !tbaa !19
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden i32 @_ZNK9btVector36maxDotEPKS_lRf(%class.btVector3* %this, %class.btVector3* %array, i32 %array_count, float* nonnull align 4 dereferenceable(4) %dotOut) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %array.addr = alloca %class.btVector3*, align 4
  %array_count.addr = alloca i32, align 4
  %dotOut.addr = alloca float*, align 4
  %maxDot = alloca float, align 4
  %i = alloca i32, align 4
  %ptIndex = alloca i32, align 4
  %dot = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %array, %class.btVector3** %array.addr, align 4, !tbaa !2
  store i32 %array_count, i32* %array_count.addr, align 4, !tbaa !23
  store float* %dotOut, float** %dotOut.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %maxDot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 0xC7EFFFFFE0000000, float* %maxDot, align 4, !tbaa !19
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store i32 0, i32* %i, align 4, !tbaa !6
  %2 = bitcast i32* %ptIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store i32 -1, i32* %ptIndex, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4, !tbaa !6
  %4 = load i32, i32* %array_count.addr, align 4, !tbaa !23
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = bitcast float* %dot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %array.addr, align 4, !tbaa !2
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 %7
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  store float %call, float* %dot, align 4, !tbaa !19
  %8 = load float, float* %dot, align 4, !tbaa !19
  %9 = load float, float* %maxDot, align 4, !tbaa !19
  %cmp2 = fcmp ogt float %8, %9
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %10 = load float, float* %dot, align 4, !tbaa !19
  store float %10, float* %maxDot, align 4, !tbaa !19
  %11 = load i32, i32* %i, align 4, !tbaa !6
  store i32 %11, i32* %ptIndex, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %12 = bitcast float* %dot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %13 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %14 = load float, float* %maxDot, align 4, !tbaa !19
  %15 = load float*, float** %dotOut.addr, align 4, !tbaa !2
  store float %14, float* %15, align 4, !tbaa !19
  %16 = load i32, i32* %ptIndex, align 4, !tbaa !6
  %17 = bitcast i32* %ptIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  %18 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  %19 = bitcast float* %maxDot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  ret i32 %16
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %this, i32 %n) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !14
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %1
  ret %class.btVector3* %arrayidx
}

define hidden void @_ZNK17btConvexHullShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i(%class.btConvexHullShape* %this, %class.btVector3* %vectors, %class.btVector3* %supportVerticesOut, i32 %numVectors) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConvexHullShape*, align 4
  %vectors.addr = alloca %class.btVector3*, align 4
  %supportVerticesOut.addr = alloca %class.btVector3*, align 4
  %numVectors.addr = alloca i32, align 4
  %newDot = alloca float, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %vec = alloca %class.btVector3, align 4
  %i10 = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btConvexHullShape* %this, %class.btConvexHullShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %vectors, %class.btVector3** %vectors.addr, align 4, !tbaa !2
  store %class.btVector3* %supportVerticesOut, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  store i32 %numVectors, i32* %numVectors.addr, align 4, !tbaa !6
  %this1 = load %class.btConvexHullShape*, %class.btConvexHullShape** %this.addr, align 4
  %0 = bitcast float* %newDot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %numVectors.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 %6
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx)
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 3
  store float 0xC3ABC16D60000000, float* %arrayidx2, align 4, !tbaa !19
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %8 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc23, %for.end
  %9 = load i32, i32* %j, align 4, !tbaa !6
  %10 = load i32, i32* %numVectors.addr, align 4, !tbaa !6
  %cmp4 = icmp slt i32 %9, %10
  br i1 %cmp4, label %for.body6, label %for.cond.cleanup5

for.cond.cleanup5:                                ; preds = %for.cond3
  %11 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  br label %for.end25

for.body6:                                        ; preds = %for.cond3
  %12 = bitcast %class.btVector3* %vec to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %12) #8
  %13 = load %class.btVector3*, %class.btVector3** %vectors.addr, align 4, !tbaa !2
  %14 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 %14
  %15 = bitcast %class.btConvexHullShape* %this1 to %class.btConvexInternalShape*
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %15, i32 0, i32 1
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %vec, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx7, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling)
  %m_unscaledPoints = getelementptr inbounds %class.btConvexHullShape, %class.btConvexHullShape* %this1, i32 0, i32 2
  %call8 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %m_unscaledPoints)
  %cmp9 = icmp slt i32 0, %call8
  br i1 %cmp9, label %if.then, label %if.else

if.then:                                          ; preds = %for.body6
  %16 = bitcast i32* %i10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  %m_unscaledPoints11 = getelementptr inbounds %class.btConvexHullShape, %class.btConvexHullShape* %this1, i32 0, i32 2
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_unscaledPoints11, i32 0)
  %m_unscaledPoints13 = getelementptr inbounds %class.btConvexHullShape, %class.btConvexHullShape* %this1, i32 0, i32 2
  %call14 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %m_unscaledPoints13)
  %call15 = call i32 @_ZNK9btVector36maxDotEPKS_lRf(%class.btVector3* %vec, %class.btVector3* %call12, i32 %call14, float* nonnull align 4 dereferenceable(4) %newDot)
  store i32 %call15, i32* %i10, align 4, !tbaa !6
  %17 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %17) #8
  %18 = load i32, i32* %i10, align 4, !tbaa !6
  call void @_ZNK17btConvexHullShape14getScaledPointEi(%class.btVector3* sret align 4 %ref.tmp, %class.btConvexHullShape* %this1, i32 %18)
  %19 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  %20 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx16 = getelementptr inbounds %class.btVector3, %class.btVector3* %19, i32 %20
  %21 = bitcast %class.btVector3* %arrayidx16 to i8*
  %22 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false), !tbaa.struct !12
  %23 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %23) #8
  %24 = load float, float* %newDot, align 4, !tbaa !19
  %25 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  %26 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx17 = getelementptr inbounds %class.btVector3, %class.btVector3* %25, i32 %26
  %call18 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx17)
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 3
  store float %24, float* %arrayidx19, align 4, !tbaa !19
  %27 = bitcast i32* %i10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #8
  br label %if.end

if.else:                                          ; preds = %for.body6
  %28 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  %29 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx20 = getelementptr inbounds %class.btVector3, %class.btVector3* %28, i32 %29
  %call21 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx20)
  %arrayidx22 = getelementptr inbounds float, float* %call21, i32 3
  store float 0xC3ABC16D60000000, float* %arrayidx22, align 4, !tbaa !19
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %30 = bitcast %class.btVector3* %vec to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %30) #8
  br label %for.inc23

for.inc23:                                        ; preds = %if.end
  %31 = load i32, i32* %j, align 4, !tbaa !6
  %inc24 = add nsw i32 %31, 1
  store i32 %inc24, i32* %j, align 4, !tbaa !6
  br label %for.cond3

for.end25:                                        ; preds = %for.cond.cleanup5
  %32 = bitcast float* %newDot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK17btConvexHullShape14getScaledPointEi(%class.btVector3* noalias sret align 4 %agg.result, %class.btConvexHullShape* %this, i32 %i) #2 comdat {
entry:
  %this.addr = alloca %class.btConvexHullShape*, align 4
  %i.addr = alloca i32, align 4
  store %class.btConvexHullShape* %this, %class.btConvexHullShape** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %this1 = load %class.btConvexHullShape*, %class.btConvexHullShape** %this.addr, align 4
  %m_unscaledPoints = getelementptr inbounds %class.btConvexHullShape, %class.btConvexHullShape* %this1, i32 0, i32 2
  %0 = load i32, i32* %i.addr, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_unscaledPoints, i32 %0)
  %1 = bitcast %class.btConvexHullShape* %this1 to %class.btConvexInternalShape*
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %1, i32 0, i32 1
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling)
  ret void
}

define hidden void @_ZNK17btConvexHullShape24localGetSupportingVertexERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btConvexHullShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %vec) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConvexHullShape*, align 4
  %vec.addr = alloca %class.btVector3*, align 4
  %vecnorm = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca float, align 4
  store %class.btConvexHullShape* %this, %class.btConvexHullShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %vec, %class.btVector3** %vec.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexHullShape*, %class.btConvexHullShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %vec.addr, align 4, !tbaa !2
  %1 = bitcast %class.btConvexHullShape* %this1 to void (%class.btVector3*, %class.btConvexHullShape*, %class.btVector3*)***
  %vtable = load void (%class.btVector3*, %class.btConvexHullShape*, %class.btVector3*)**, void (%class.btVector3*, %class.btConvexHullShape*, %class.btVector3*)*** %1, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btVector3*, %class.btConvexHullShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexHullShape*, %class.btVector3*)** %vtable, i64 17
  %2 = load void (%class.btVector3*, %class.btConvexHullShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexHullShape*, %class.btVector3*)** %vfn, align 4
  call void %2(%class.btVector3* sret align 4 %agg.result, %class.btConvexHullShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  %3 = bitcast %class.btConvexHullShape* %this1 to %class.btConvexInternalShape*
  %4 = bitcast %class.btConvexInternalShape* %3 to float (%class.btConvexInternalShape*)***
  %vtable2 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %4, align 4, !tbaa !8
  %vfn3 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable2, i64 12
  %5 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn3, align 4
  %call = call float %5(%class.btConvexInternalShape* %3)
  %cmp = fcmp une float %call, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end16

if.then:                                          ; preds = %entry
  %6 = bitcast %class.btVector3* %vecnorm to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #8
  %7 = load %class.btVector3*, %class.btVector3** %vec.addr, align 4, !tbaa !2
  %8 = bitcast %class.btVector3* %vecnorm to i8*
  %9 = bitcast %class.btVector3* %7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false), !tbaa.struct !12
  %call4 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %vecnorm)
  %cmp5 = fcmp olt float %call4, 0x3D10000000000000
  br i1 %cmp5, label %if.then6, label %if.end

if.then6:                                         ; preds = %if.then
  %10 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  store float -1.000000e+00, float* %ref.tmp, align 4, !tbaa !19
  %11 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  store float -1.000000e+00, float* %ref.tmp7, align 4, !tbaa !19
  %12 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  store float -1.000000e+00, float* %ref.tmp8, align 4, !tbaa !19
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %vecnorm, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  %13 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  %14 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #8
  %15 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  br label %if.end

if.end:                                           ; preds = %if.then6, %if.then
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %vecnorm)
  %16 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #8
  %17 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #8
  %18 = bitcast %class.btConvexHullShape* %this1 to %class.btConvexInternalShape*
  %19 = bitcast %class.btConvexInternalShape* %18 to float (%class.btConvexInternalShape*)***
  %vtable12 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %19, align 4, !tbaa !8
  %vfn13 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable12, i64 12
  %20 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn13, align 4
  %call14 = call float %20(%class.btConvexInternalShape* %18)
  store float %call14, float* %ref.tmp11, align 4, !tbaa !19
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11, %class.btVector3* nonnull align 4 dereferenceable(16) %vecnorm)
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp10)
  %21 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  %22 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %22) #8
  %23 = bitcast %class.btVector3* %vecnorm to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %23) #8
  br label %if.end16

if.end16:                                         ; preds = %if.end, %entry
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !19
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !19
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !19
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !19
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !19
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !19
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !19
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4, !tbaa !19
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1) #8
  ret %class.btVector3* %call2
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !19
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !19
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !19
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !19
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !19
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !19
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !19
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !19
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !19
  ret %class.btVector3* %this1
}

define hidden i32 @_ZNK17btConvexHullShape14getNumVerticesEv(%class.btConvexHullShape* %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConvexHullShape*, align 4
  store %class.btConvexHullShape* %this, %class.btConvexHullShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexHullShape*, %class.btConvexHullShape** %this.addr, align 4
  %m_unscaledPoints = getelementptr inbounds %class.btConvexHullShape, %class.btConvexHullShape* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %m_unscaledPoints)
  ret i32 %call
}

define hidden i32 @_ZNK17btConvexHullShape11getNumEdgesEv(%class.btConvexHullShape* %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConvexHullShape*, align 4
  store %class.btConvexHullShape* %this, %class.btConvexHullShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexHullShape*, %class.btConvexHullShape** %this.addr, align 4
  %m_unscaledPoints = getelementptr inbounds %class.btConvexHullShape, %class.btConvexHullShape* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %m_unscaledPoints)
  ret i32 %call
}

define hidden void @_ZNK17btConvexHullShape7getEdgeEiR9btVector3S1_(%class.btConvexHullShape* %this, i32 %i, %class.btVector3* nonnull align 4 dereferenceable(16) %pa, %class.btVector3* nonnull align 4 dereferenceable(16) %pb) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConvexHullShape*, align 4
  %i.addr = alloca i32, align 4
  %pa.addr = alloca %class.btVector3*, align 4
  %pb.addr = alloca %class.btVector3*, align 4
  %index0 = alloca i32, align 4
  %index1 = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  store %class.btConvexHullShape* %this, %class.btConvexHullShape** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  store %class.btVector3* %pa, %class.btVector3** %pa.addr, align 4, !tbaa !2
  store %class.btVector3* %pb, %class.btVector3** %pb.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexHullShape*, %class.btConvexHullShape** %this.addr, align 4
  %0 = bitcast i32* %index0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %i.addr, align 4, !tbaa !6
  %m_unscaledPoints = getelementptr inbounds %class.btConvexHullShape, %class.btConvexHullShape* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %m_unscaledPoints)
  %rem = srem i32 %1, %call
  store i32 %rem, i32* %index0, align 4, !tbaa !6
  %2 = bitcast i32* %index1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load i32, i32* %i.addr, align 4, !tbaa !6
  %add = add nsw i32 %3, 1
  %m_unscaledPoints2 = getelementptr inbounds %class.btConvexHullShape, %class.btConvexHullShape* %this1, i32 0, i32 2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %m_unscaledPoints2)
  %rem4 = srem i32 %add, %call3
  store i32 %rem4, i32* %index1, align 4, !tbaa !6
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #8
  %5 = load i32, i32* %index0, align 4, !tbaa !6
  call void @_ZNK17btConvexHullShape14getScaledPointEi(%class.btVector3* sret align 4 %ref.tmp, %class.btConvexHullShape* %this1, i32 %5)
  %6 = load %class.btVector3*, %class.btVector3** %pa.addr, align 4, !tbaa !2
  %7 = bitcast %class.btVector3* %6 to i8*
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !12
  %9 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #8
  %10 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #8
  %11 = load i32, i32* %index1, align 4, !tbaa !6
  call void @_ZNK17btConvexHullShape14getScaledPointEi(%class.btVector3* sret align 4 %ref.tmp5, %class.btConvexHullShape* %this1, i32 %11)
  %12 = load %class.btVector3*, %class.btVector3** %pb.addr, align 4, !tbaa !2
  %13 = bitcast %class.btVector3* %12 to i8*
  %14 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 16, i1 false), !tbaa.struct !12
  %15 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %15) #8
  %16 = bitcast i32* %index1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast i32* %index0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

define hidden void @_ZNK17btConvexHullShape9getVertexEiR9btVector3(%class.btConvexHullShape* %this, i32 %i, %class.btVector3* nonnull align 4 dereferenceable(16) %vtx) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConvexHullShape*, align 4
  %i.addr = alloca i32, align 4
  %vtx.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btConvexHullShape* %this, %class.btConvexHullShape** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  store %class.btVector3* %vtx, %class.btVector3** %vtx.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexHullShape*, %class.btConvexHullShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load i32, i32* %i.addr, align 4, !tbaa !6
  call void @_ZNK17btConvexHullShape14getScaledPointEi(%class.btVector3* sret align 4 %ref.tmp, %class.btConvexHullShape* %this1, i32 %1)
  %2 = load %class.btVector3*, %class.btVector3** %vtx.addr, align 4, !tbaa !2
  %3 = bitcast %class.btVector3* %2 to i8*
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false), !tbaa.struct !12
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #8
  ret void
}

; Function Attrs: nounwind
define hidden i32 @_ZNK17btConvexHullShape12getNumPlanesEv(%class.btConvexHullShape* %this) unnamed_addr #5 {
entry:
  %this.addr = alloca %class.btConvexHullShape*, align 4
  store %class.btConvexHullShape* %this, %class.btConvexHullShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexHullShape*, %class.btConvexHullShape** %this.addr, align 4
  ret i32 0
}

; Function Attrs: nounwind
define hidden void @_ZNK17btConvexHullShape8getPlaneER9btVector3S1_i(%class.btConvexHullShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, i32 %2) unnamed_addr #5 {
entry:
  %this.addr = alloca %class.btConvexHullShape*, align 4
  %.addr = alloca %class.btVector3*, align 4
  %.addr1 = alloca %class.btVector3*, align 4
  %.addr2 = alloca i32, align 4
  store %class.btConvexHullShape* %this, %class.btConvexHullShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %0, %class.btVector3** %.addr, align 4, !tbaa !2
  store %class.btVector3* %1, %class.btVector3** %.addr1, align 4, !tbaa !2
  store i32 %2, i32* %.addr2, align 4, !tbaa !6
  %this3 = load %class.btConvexHullShape*, %class.btConvexHullShape** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define hidden zeroext i1 @_ZNK17btConvexHullShape8isInsideERK9btVector3f(%class.btConvexHullShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float %1) unnamed_addr #5 {
entry:
  %this.addr = alloca %class.btConvexHullShape*, align 4
  %.addr = alloca %class.btVector3*, align 4
  %.addr1 = alloca float, align 4
  store %class.btConvexHullShape* %this, %class.btConvexHullShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %0, %class.btVector3** %.addr, align 4, !tbaa !2
  store float %1, float* %.addr1, align 4, !tbaa !19
  %this2 = load %class.btConvexHullShape*, %class.btConvexHullShape** %this.addr, align 4
  ret i1 false
}

define hidden i8* @_ZNK17btConvexHullShape9serializeEPvP12btSerializer(%class.btConvexHullShape* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConvexHullShape*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %shapeData = alloca %struct.btConvexHullShapeData*, align 4
  %numElem = alloca i32, align 4
  %sz = alloca i32, align 4
  %chunk = alloca %class.btChunk*, align 4
  %memPtr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btConvexHullShape* %this, %class.btConvexHullShape** %this.addr, align 4, !tbaa !2
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexHullShape*, %class.btConvexHullShape** %this.addr, align 4
  %0 = bitcast %struct.btConvexHullShapeData** %shapeData to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i8*, i8** %dataBuffer.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.btConvexHullShapeData*
  store %struct.btConvexHullShapeData* %2, %struct.btConvexHullShapeData** %shapeData, align 4, !tbaa !2
  %3 = bitcast %class.btConvexHullShape* %this1 to %class.btConvexInternalShape*
  %4 = load %struct.btConvexHullShapeData*, %struct.btConvexHullShapeData** %shapeData, align 4, !tbaa !2
  %m_convexInternalShapeData = getelementptr inbounds %struct.btConvexHullShapeData, %struct.btConvexHullShapeData* %4, i32 0, i32 0
  %5 = bitcast %struct.btConvexInternalShapeData* %m_convexInternalShapeData to i8*
  %6 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %call = call i8* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer(%class.btConvexInternalShape* %3, i8* %5, %class.btSerializer* %6)
  %7 = bitcast i32* %numElem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %m_unscaledPoints = getelementptr inbounds %class.btConvexHullShape, %class.btConvexHullShape* %this1, i32 0, i32 2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %m_unscaledPoints)
  store i32 %call2, i32* %numElem, align 4, !tbaa !6
  %8 = load i32, i32* %numElem, align 4, !tbaa !6
  %9 = load %struct.btConvexHullShapeData*, %struct.btConvexHullShapeData** %shapeData, align 4, !tbaa !2
  %m_numUnscaledPoints = getelementptr inbounds %struct.btConvexHullShapeData, %struct.btConvexHullShapeData* %9, i32 0, i32 3
  store i32 %8, i32* %m_numUnscaledPoints, align 4, !tbaa !25
  %10 = load i32, i32* %numElem, align 4, !tbaa !6
  %tobool = icmp ne i32 %10, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %11 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %m_unscaledPoints3 = getelementptr inbounds %class.btConvexHullShape, %class.btConvexHullShape* %this1, i32 0, i32 2
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_unscaledPoints3, i32 0)
  %12 = bitcast %class.btVector3* %call4 to i8*
  %13 = bitcast %class.btSerializer* %11 to i8* (%class.btSerializer*, i8*)***
  %vtable = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %13, align 4, !tbaa !8
  %vfn = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable, i64 7
  %14 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn, align 4
  %call5 = call i8* %14(%class.btSerializer* %11, i8* %12)
  %15 = bitcast i8* %call5 to %struct.btVector3FloatData*
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.btVector3FloatData* [ %15, %cond.true ], [ null, %cond.false ]
  %16 = load %struct.btConvexHullShapeData*, %struct.btConvexHullShapeData** %shapeData, align 4, !tbaa !2
  %m_unscaledPointsFloatPtr = getelementptr inbounds %struct.btConvexHullShapeData, %struct.btConvexHullShapeData* %16, i32 0, i32 1
  store %struct.btVector3FloatData* %cond, %struct.btVector3FloatData** %m_unscaledPointsFloatPtr, align 4, !tbaa !30
  %17 = load %struct.btConvexHullShapeData*, %struct.btConvexHullShapeData** %shapeData, align 4, !tbaa !2
  %m_unscaledPointsDoublePtr = getelementptr inbounds %struct.btConvexHullShapeData, %struct.btConvexHullShapeData* %17, i32 0, i32 2
  store %struct.btVector3DoubleData* null, %struct.btVector3DoubleData** %m_unscaledPointsDoublePtr, align 4, !tbaa !31
  %18 = load i32, i32* %numElem, align 4, !tbaa !6
  %tobool6 = icmp ne i32 %18, 0
  br i1 %tobool6, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end
  %19 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  store i32 16, i32* %sz, align 4, !tbaa !6
  %20 = bitcast %class.btChunk** %chunk to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #8
  %21 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %22 = load i32, i32* %sz, align 4, !tbaa !6
  %23 = load i32, i32* %numElem, align 4, !tbaa !6
  %24 = bitcast %class.btSerializer* %21 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable7 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %24, align 4, !tbaa !8
  %vfn8 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable7, i64 4
  %25 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn8, align 4
  %call9 = call %class.btChunk* %25(%class.btSerializer* %21, i32 %22, i32 %23)
  store %class.btChunk* %call9, %class.btChunk** %chunk, align 4, !tbaa !2
  %26 = bitcast %struct.btVector3FloatData** %memPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #8
  %27 = load %class.btChunk*, %class.btChunk** %chunk, align 4, !tbaa !2
  %m_oldPtr = getelementptr inbounds %class.btChunk, %class.btChunk* %27, i32 0, i32 2
  %28 = load i8*, i8** %m_oldPtr, align 4, !tbaa !32
  %29 = bitcast i8* %28 to %struct.btVector3FloatData*
  store %struct.btVector3FloatData* %29, %struct.btVector3FloatData** %memPtr, align 4, !tbaa !2
  %30 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #8
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %31 = load i32, i32* %i, align 4, !tbaa !6
  %32 = load i32, i32* %numElem, align 4, !tbaa !6
  %cmp = icmp slt i32 %31, %32
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %33 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_unscaledPoints10 = getelementptr inbounds %class.btConvexHullShape, %class.btConvexHullShape* %this1, i32 0, i32 2
  %34 = load i32, i32* %i, align 4, !tbaa !6
  %call11 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_unscaledPoints10, i32 %34)
  %35 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %memPtr, align 4, !tbaa !2
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %call11, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %35)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %36 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %36, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  %37 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %memPtr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %37, i32 1
  store %struct.btVector3FloatData* %incdec.ptr, %struct.btVector3FloatData** %memPtr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %38 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %39 = load %class.btChunk*, %class.btChunk** %chunk, align 4, !tbaa !2
  %m_unscaledPoints12 = getelementptr inbounds %class.btConvexHullShape, %class.btConvexHullShape* %this1, i32 0, i32 2
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_unscaledPoints12, i32 0)
  %40 = bitcast %class.btVector3* %call13 to i8*
  %41 = bitcast %class.btSerializer* %38 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable14 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %41, align 4, !tbaa !8
  %vfn15 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable14, i64 5
  %42 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn15, align 4
  call void %42(%class.btSerializer* %38, %class.btChunk* %39, i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str, i32 0, i32 0), i32 1497453121, i8* %40)
  %43 = bitcast %struct.btVector3FloatData** %memPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #8
  %44 = bitcast %class.btChunk** %chunk to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #8
  %45 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #8
  br label %if.end

if.end:                                           ; preds = %for.end, %cond.end
  %46 = bitcast i32* %numElem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #8
  %47 = bitcast %struct.btConvexHullShapeData** %shapeData to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #8
  ret i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.1, i32 0, i32 0)
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer(%class.btConvexInternalShape* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %shapeData = alloca %struct.btConvexInternalShapeData*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = bitcast %struct.btConvexInternalShapeData** %shapeData to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i8*, i8** %dataBuffer.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.btConvexInternalShapeData*
  store %struct.btConvexInternalShapeData* %2, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %3 = bitcast %class.btConvexInternalShape* %this1 to %class.btCollisionShape*
  %4 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_collisionShapeData = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %4, i32 0, i32 0
  %5 = bitcast %struct.btCollisionShapeData* %m_collisionShapeData to i8*
  %6 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %call = call i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape* %3, i8* %5, %class.btSerializer* %6)
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 2
  %7 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_implicitShapeDimensions2 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %7, i32 0, i32 2
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_implicitShapeDimensions, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_implicitShapeDimensions2)
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 1
  %8 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_localScaling3 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %8, i32 0, i32 1
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_localScaling, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_localScaling3)
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  %9 = load float, float* %m_collisionMargin, align 4, !tbaa !34
  %10 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_collisionMargin4 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %10, i32 0, i32 3
  store float %9, float* %m_collisionMargin4, align 4, !tbaa !37
  %11 = bitcast %struct.btConvexInternalShapeData** %shapeData to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.2, i32 0, i32 0)
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %3 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %3
  %4 = load float, float* %arrayidx, align 4, !tbaa !19
  %5 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %5, i32 0, i32 0
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %6
  store float %4, float* %arrayidx3, align 4, !tbaa !19
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

define hidden void @_ZNK17btConvexHullShape7projectERK11btTransformRK9btVector3RfS6_RS3_S7_(%class.btConvexHullShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %trans, %class.btVector3* nonnull align 4 dereferenceable(16) %dir, float* nonnull align 4 dereferenceable(4) %minProj, float* nonnull align 4 dereferenceable(4) %maxProj, %class.btVector3* nonnull align 4 dereferenceable(16) %witnesPtMin, %class.btVector3* nonnull align 4 dereferenceable(16) %witnesPtMax) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConvexHullShape*, align 4
  %trans.addr = alloca %class.btTransform*, align 4
  %dir.addr = alloca %class.btVector3*, align 4
  %minProj.addr = alloca float*, align 4
  %maxProj.addr = alloca float*, align 4
  %witnesPtMin.addr = alloca %class.btVector3*, align 4
  %witnesPtMax.addr = alloca %class.btVector3*, align 4
  %numVerts = alloca i32, align 4
  %i = alloca i32, align 4
  %vtx = alloca %class.btVector3, align 4
  %pt = alloca %class.btVector3, align 4
  %dp = alloca float, align 4
  store %class.btConvexHullShape* %this, %class.btConvexHullShape** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %trans, %class.btTransform** %trans.addr, align 4, !tbaa !2
  store %class.btVector3* %dir, %class.btVector3** %dir.addr, align 4, !tbaa !2
  store float* %minProj, float** %minProj.addr, align 4, !tbaa !2
  store float* %maxProj, float** %maxProj.addr, align 4, !tbaa !2
  store %class.btVector3* %witnesPtMin, %class.btVector3** %witnesPtMin.addr, align 4, !tbaa !2
  store %class.btVector3* %witnesPtMax, %class.btVector3** %witnesPtMax.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexHullShape*, %class.btConvexHullShape** %this.addr, align 4
  %0 = load float*, float** %minProj.addr, align 4, !tbaa !2
  store float 0x47EFFFFFE0000000, float* %0, align 4, !tbaa !19
  %1 = load float*, float** %maxProj.addr, align 4, !tbaa !2
  store float 0xC7EFFFFFE0000000, float* %1, align 4, !tbaa !19
  %2 = bitcast i32* %numVerts to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %m_unscaledPoints = getelementptr inbounds %class.btConvexHullShape, %class.btConvexHullShape* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %m_unscaledPoints)
  store i32 %call, i32* %numVerts, align 4, !tbaa !6
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %4 = load i32, i32* %i, align 4, !tbaa !6
  %5 = load i32, i32* %numVerts, align 4, !tbaa !6
  %cmp = icmp slt i32 %4, %5
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %6 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %7 = bitcast %class.btVector3* %vtx to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #8
  %m_unscaledPoints2 = getelementptr inbounds %class.btConvexHullShape, %class.btConvexHullShape* %this1, i32 0, i32 2
  %8 = load i32, i32* %i, align 4, !tbaa !6
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_unscaledPoints2, i32 %8)
  %9 = bitcast %class.btConvexHullShape* %this1 to %class.btConvexInternalShape*
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %9, i32 0, i32 1
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %vtx, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling)
  %10 = bitcast %class.btVector3* %pt to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #8
  %11 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4, !tbaa !2
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %pt, %class.btTransform* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %vtx)
  %12 = bitcast float* %dp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  %13 = load %class.btVector3*, %class.btVector3** %dir.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %pt, %class.btVector3* nonnull align 4 dereferenceable(16) %13)
  store float %call4, float* %dp, align 4, !tbaa !19
  %14 = load float, float* %dp, align 4, !tbaa !19
  %15 = load float*, float** %minProj.addr, align 4, !tbaa !2
  %16 = load float, float* %15, align 4, !tbaa !19
  %cmp5 = fcmp olt float %14, %16
  br i1 %cmp5, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %17 = load float, float* %dp, align 4, !tbaa !19
  %18 = load float*, float** %minProj.addr, align 4, !tbaa !2
  store float %17, float* %18, align 4, !tbaa !19
  %19 = load %class.btVector3*, %class.btVector3** %witnesPtMin.addr, align 4, !tbaa !2
  %20 = bitcast %class.btVector3* %19 to i8*
  %21 = bitcast %class.btVector3* %pt to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %20, i8* align 4 %21, i32 16, i1 false), !tbaa.struct !12
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %22 = load float, float* %dp, align 4, !tbaa !19
  %23 = load float*, float** %maxProj.addr, align 4, !tbaa !2
  %24 = load float, float* %23, align 4, !tbaa !19
  %cmp6 = fcmp ogt float %22, %24
  br i1 %cmp6, label %if.then7, label %if.end8

if.then7:                                         ; preds = %if.end
  %25 = load float, float* %dp, align 4, !tbaa !19
  %26 = load float*, float** %maxProj.addr, align 4, !tbaa !2
  store float %25, float* %26, align 4, !tbaa !19
  %27 = load %class.btVector3*, %class.btVector3** %witnesPtMax.addr, align 4, !tbaa !2
  %28 = bitcast %class.btVector3* %27 to i8*
  %29 = bitcast %class.btVector3* %pt to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %29, i32 16, i1 false), !tbaa.struct !12
  br label %if.end8

if.end8:                                          ; preds = %if.then7, %if.end
  %30 = bitcast float* %dp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #8
  %31 = bitcast %class.btVector3* %pt to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %31) #8
  %32 = bitcast %class.btVector3* %vtx to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %32) #8
  br label %for.inc

for.inc:                                          ; preds = %if.end8
  %33 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %33, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %34 = load float*, float** %minProj.addr, align 4, !tbaa !2
  %35 = load float, float* %34, align 4, !tbaa !19
  %36 = load float*, float** %maxProj.addr, align 4, !tbaa !2
  %37 = load float, float* %36, align 4, !tbaa !19
  %cmp9 = fcmp ogt float %35, %37
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %for.end
  %38 = load float*, float** %minProj.addr, align 4, !tbaa !2
  %39 = load float*, float** %maxProj.addr, align 4, !tbaa !2
  call void @_Z6btSwapIfEvRT_S1_(float* nonnull align 4 dereferenceable(4) %38, float* nonnull align 4 dereferenceable(4) %39)
  %40 = load %class.btVector3*, %class.btVector3** %witnesPtMin.addr, align 4, !tbaa !2
  %41 = load %class.btVector3*, %class.btVector3** %witnesPtMax.addr, align 4, !tbaa !2
  call void @_Z6btSwapI9btVector3EvRT_S2_(%class.btVector3* nonnull align 4 dereferenceable(16) %40, %class.btVector3* nonnull align 4 dereferenceable(16) %41)
  br label %if.end11

if.end11:                                         ; preds = %if.then10, %for.end
  %42 = bitcast i32* %numVerts to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !19
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !19
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !19
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !19
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !19
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !19
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z6btSwapIfEvRT_S1_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #4 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  %tmp = alloca float, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store float* %b, float** %b.addr, align 4, !tbaa !2
  %0 = bitcast float* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load float*, float** %a.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !19
  store float %2, float* %tmp, align 4, !tbaa !19
  %3 = load float*, float** %b.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !19
  %5 = load float*, float** %a.addr, align 4, !tbaa !2
  store float %4, float* %5, align 4, !tbaa !19
  %6 = load float, float* %tmp, align 4, !tbaa !19
  %7 = load float*, float** %b.addr, align 4, !tbaa !2
  store float %6, float* %7, align 4, !tbaa !19
  %8 = bitcast float* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z6btSwapI9btVector3EvRT_S2_(%class.btVector3* nonnull align 4 dereferenceable(16) %a, %class.btVector3* nonnull align 4 dereferenceable(16) %b) #4 comdat {
entry:
  %a.addr = alloca %class.btVector3*, align 4
  %b.addr = alloca %class.btVector3*, align 4
  %tmp = alloca %class.btVector3, align 4
  store %class.btVector3* %a, %class.btVector3** %a.addr, align 4, !tbaa !2
  store %class.btVector3* %b, %class.btVector3** %b.addr, align 4, !tbaa !2
  %0 = bitcast %class.btVector3* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %tmp to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !12
  %4 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  %5 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %6 = bitcast %class.btVector3* %5 to i8*
  %7 = bitcast %class.btVector3* %4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !12
  %8 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  %9 = bitcast %class.btVector3* %8 to i8*
  %10 = bitcast %class.btVector3* %tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false), !tbaa.struct !12
  %11 = bitcast %class.btVector3* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %11) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btConvexHullShape* @_ZN17btConvexHullShapeD2Ev(%class.btConvexHullShape* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btConvexHullShape*, align 4
  store %class.btConvexHullShape* %this, %class.btConvexHullShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexHullShape*, %class.btConvexHullShape** %this.addr, align 4
  %0 = bitcast %class.btConvexHullShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [34 x i8*] }, { [34 x i8*] }* @_ZTV17btConvexHullShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !8
  %m_unscaledPoints = getelementptr inbounds %class.btConvexHullShape, %class.btConvexHullShape* %this1, i32 0, i32 2
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* %m_unscaledPoints) #8
  %1 = bitcast %class.btConvexHullShape* %this1 to %class.btPolyhedralConvexAabbCachingShape*
  %call2 = call %class.btPolyhedralConvexAabbCachingShape* bitcast (%class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD2Ev to %class.btPolyhedralConvexAabbCachingShape* (%class.btPolyhedralConvexAabbCachingShape*)*)(%class.btPolyhedralConvexAabbCachingShape* %1) #8
  ret %class.btConvexHullShape* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN17btConvexHullShapeD0Ev(%class.btConvexHullShape* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btConvexHullShape*, align 4
  store %class.btConvexHullShape* %this, %class.btConvexHullShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexHullShape*, %class.btConvexHullShape** %this.addr, align 4
  %call = call %class.btConvexHullShape* @_ZN17btConvexHullShapeD2Ev(%class.btConvexHullShape* %this1) #8
  %0 = bitcast %class.btConvexHullShape* %this1 to i8*
  call void @_ZN17btConvexHullShapedlEPv(i8* %0) #8
  ret void
}

declare void @_ZNK34btPolyhedralConvexAabbCachingShape7getAabbERK11btTransformR9btVector3S4_(%class.btPolyhedralConvexAabbCachingShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

declare void @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf(%class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4)) unnamed_addr #1

declare float @_ZNK16btCollisionShape20getAngularMotionDiscEv(%class.btCollisionShape*) unnamed_addr #1

declare float @_ZNK16btCollisionShape27getContactBreakingThresholdEf(%class.btCollisionShape*, float) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btConvexInternalShape15getLocalScalingEv(%class.btConvexInternalShape* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 1
  ret %class.btVector3* %m_localScaling
}

declare void @_ZNK23btPolyhedralConvexShape21calculateLocalInertiaEfR9btVector3(%class.btPolyhedralConvexShape*, float, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden i8* @_ZNK17btConvexHullShape7getNameEv(%class.btConvexHullShape* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btConvexHullShape*, align 4
  store %class.btConvexHullShape* %this, %class.btConvexHullShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexHullShape*, %class.btConvexHullShape** %this.addr, align 4
  ret i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.3, i32 0, i32 0)
}

define linkonce_odr hidden void @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btCollisionShape* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !19
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 1.000000e+00, float* %ref.tmp2, align 4, !tbaa !19
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 1.000000e+00, float* %ref.tmp3, align 4, !tbaa !19
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN21btConvexInternalShape9setMarginEf(%class.btConvexInternalShape* %this, float %margin) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %margin.addr = alloca float, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  store float %margin, float* %margin.addr, align 4, !tbaa !19
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load float, float* %margin.addr, align 4, !tbaa !19
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  store float %0, float* %m_collisionMargin, align 4, !tbaa !34
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK21btConvexInternalShape9getMarginEv(%class.btConvexInternalShape* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  %0 = load float, float* %m_collisionMargin, align 4, !tbaa !34
  ret float %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK17btConvexHullShape28calculateSerializeBufferSizeEv(%class.btConvexHullShape* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btConvexHullShape*, align 4
  store %class.btConvexHullShape* %this, %class.btConvexHullShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexHullShape*, %class.btConvexHullShape** %this.addr, align 4
  ret i32 68
}

declare void @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer(%class.btCollisionShape*, %class.btSerializer*) unnamed_addr #1

declare void @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_(%class.btConvexShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4), float* nonnull align 4 dereferenceable(4)) unnamed_addr #1

declare void @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_(%class.btConvexInternalShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv(%class.btConvexInternalShape* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  ret i32 0
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3(%class.btConvexInternalShape* %this, i32 %index, %class.btVector3* nonnull align 4 dereferenceable(16) %penetrationVector) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %index.addr = alloca i32, align 4
  %penetrationVector.addr = alloca %class.btVector3*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !6
  store %class.btVector3* %penetrationVector, %class.btVector3** %penetrationVector.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %penetrationVector.addr, align 4, !tbaa !2
  ret void
}

declare zeroext i1 @_ZN23btPolyhedralConvexShape28initializePolyhedralFeaturesEi(%class.btPolyhedralConvexShape*, i32) unnamed_addr #1

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !19
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !19
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  ret %class.btVector3* %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #4 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !19
  %0 = load float, float* %y.addr, align 4, !tbaa !19
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #6

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !19
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !19
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !19
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !19
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !19
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !19
  %6 = load float*, float** %s.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !19
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !19
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !19
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !19
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !19
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !19
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !19
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !19
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !19
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !19
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !19
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !19
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

declare i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape*, i8*, %class.btSerializer*) unnamed_addr #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %3 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %3
  %4 = load float, float* %arrayidx, align 4, !tbaa !19
  %5 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %5, i32 0, i32 0
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %6
  store float %4, float* %arrayidx3, align 4, !tbaa !19
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !19
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !19
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !19
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !19
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !19
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !19
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !19
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !19
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !19
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4, !tbaa !19
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !19
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !19
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: nounwind
declare %class.btPolyhedralConvexShape* @_ZN23btPolyhedralConvexShapeD2Ev(%class.btPolyhedralConvexShape* returned) unnamed_addr #7

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !14
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !14
  %tobool = icmp ne %class.btVector3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !38, !range !22
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btVector3*, %class.btVector3** %m_data4, align 4, !tbaa !14
  call void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %class.btVector3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data5, align 4, !tbaa !14
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !38
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data, align 4, !tbaa !14
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !18
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !39
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %class.btVector3* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.btVector3*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %ptr, %class.btVector3** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVector3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN17btConvexHullShapedlEPv(i8* %ptr) #4 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btVector3*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btVector3** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btVector3*
  store %class.btVector3* %3, %class.btVector3** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  %4 = load %class.btVector3*, %class.btVector3** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %class.btVector3* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !38
  %5 = load %class.btVector3*, %class.btVector3** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* %5, %class.btVector3** %m_data, align 4, !tbaa !14
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !39
  %7 = bitcast %class.btVector3** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN9btVector3nwEmPv(i32 %0, i8* %ptr) #4 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !23
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !39
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %class.btVector3** null)
  %2 = bitcast %class.btVector3* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %class.btVector3* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store %class.btVector3* %dest, %class.btVector3** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btVector3*, %class.btVector3** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 %5
  %6 = bitcast %class.btVector3* %arrayidx to i8*
  %call = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %6)
  %7 = bitcast i8* %call to %class.btVector3*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !14
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 %9
  %10 = bitcast %class.btVector3* %7 to i8*
  %11 = bitcast %class.btVector3* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !12
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  ret void
}

define linkonce_odr hidden %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %class.btVector3** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btVector3**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store %class.btVector3** %hint, %class.btVector3*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btVector3*
  ret %class.btVector3* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind readnone speculatable willreturn }
attributes #7 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"vtable pointer", !5, i64 0}
!10 = !{!11, !7, i64 4}
!11 = !{!"_ZTS16btCollisionShape", !7, i64 4, !3, i64 8}
!12 = !{i64 0, i64 16, !13}
!13 = !{!4, !4, i64 0}
!14 = !{!15, !3, i64 12}
!15 = !{!"_ZTS20btAlignedObjectArrayI9btVector3E", !16, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !17, i64 16}
!16 = !{!"_ZTS18btAlignedAllocatorI9btVector3Lj16EE"}
!17 = !{!"bool", !4, i64 0}
!18 = !{!15, !7, i64 4}
!19 = !{!20, !20, i64 0}
!20 = !{!"float", !4, i64 0}
!21 = !{!17, !17, i64 0}
!22 = !{i8 0, i8 2}
!23 = !{!24, !24, i64 0}
!24 = !{!"long", !4, i64 0}
!25 = !{!26, !7, i64 60}
!26 = !{!"_ZTS21btConvexHullShapeData", !27, i64 0, !3, i64 52, !3, i64 56, !7, i64 60, !4, i64 64}
!27 = !{!"_ZTS25btConvexInternalShapeData", !28, i64 0, !29, i64 12, !29, i64 28, !20, i64 44, !7, i64 48}
!28 = !{!"_ZTS20btCollisionShapeData", !3, i64 0, !7, i64 4, !4, i64 8}
!29 = !{!"_ZTS18btVector3FloatData", !4, i64 0}
!30 = !{!26, !3, i64 52}
!31 = !{!26, !3, i64 56}
!32 = !{!33, !3, i64 8}
!33 = !{!"_ZTS7btChunk", !7, i64 0, !7, i64 4, !3, i64 8, !7, i64 12, !7, i64 16}
!34 = !{!35, !20, i64 44}
!35 = !{!"_ZTS21btConvexInternalShape", !36, i64 12, !36, i64 28, !20, i64 44, !20, i64 48}
!36 = !{!"_ZTS9btVector3", !4, i64 0}
!37 = !{!27, !20, i64 44}
!38 = !{!15, !17, i64 16}
!39 = !{!15, !7, i64 8}
