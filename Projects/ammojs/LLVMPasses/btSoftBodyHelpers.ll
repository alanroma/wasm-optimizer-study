; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletSoftBody/btSoftBodyHelpers.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletSoftBody/btSoftBodyHelpers.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btVector3 = type { [4 x float] }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btSoftBody = type { %class.btCollisionObject, %class.btAlignedObjectArray, %class.btSoftBodySolver*, %"struct.btSoftBody::Config", %"struct.btSoftBody::SolverState", %"struct.btSoftBody::Pose", i8*, %struct.btSoftBodyWorldInfo*, %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.25, %class.btAlignedObjectArray.29, %class.btAlignedObjectArray.33, %class.btAlignedObjectArray.37, %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.49, %class.btAlignedObjectArray.53, %class.btAlignedObjectArray.57, %class.btAlignedObjectArray.65, float, [2 x %class.btVector3], i8, %struct.btDbvt, %struct.btDbvt, %struct.btDbvt, %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.81, %class.btTransform, %class.btVector3, float, %class.btAlignedObjectArray.85 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%union.anon = type { i8* }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btSoftBodySolver = type opaque
%"struct.btSoftBody::Config" = type { i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, i32, i32, i32, i32, i32, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%"struct.btSoftBody::SolverState" = type { float, float, float, float, float }
%"struct.btSoftBody::Pose" = type { i8, i8, float, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12, %class.btVector3, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%struct.btSoftBodyWorldInfo = type { float, float, float, float, %class.btVector3, %class.btBroadphaseInterface*, %class.btDispatcher*, %class.btVector3, %struct.btSparseSdf }
%class.btBroadphaseInterface = type { i32 (...)** }
%class.btDispatcher = type { i32 (...)** }
%struct.btSparseSdf = type { %class.btAlignedObjectArray.16, float, i32, i32, i32, i32, i32 }
%class.btAlignedObjectArray.16 = type <{ %class.btAlignedAllocator.17, [3 x i8], i32, i32, %"struct.btSparseSdf<3>::Cell"**, i8, [3 x i8] }>
%class.btAlignedAllocator.17 = type { i8 }
%"struct.btSparseSdf<3>::Cell" = type opaque
%class.btAlignedObjectArray.20 = type <{ %class.btAlignedAllocator.21, [3 x i8], i32, i32, %"struct.btSoftBody::Note"*, i8, [3 x i8] }>
%class.btAlignedAllocator.21 = type { i8 }
%"struct.btSoftBody::Note" = type { %"struct.btSoftBody::Element", i8*, %class.btVector3, i32, [4 x %"struct.btSoftBody::Node"*], [4 x float] }
%"struct.btSoftBody::Element" = type { i8* }
%"struct.btSoftBody::Node" = type <{ %"struct.btSoftBody::Feature", %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, %struct.btDbvtNode*, i8, [3 x i8] }>
%"struct.btSoftBody::Feature" = type { %"struct.btSoftBody::Element", %"struct.btSoftBody::Material"* }
%"struct.btSoftBody::Material" = type { %"struct.btSoftBody::Element", float, float, float, i32 }
%struct.btDbvtNode = type { %struct.btDbvtAabbMm, %struct.btDbvtNode*, %union.anon.23 }
%struct.btDbvtAabbMm = type { %class.btVector3, %class.btVector3 }
%union.anon.23 = type { [2 x %struct.btDbvtNode*] }
%class.btAlignedObjectArray.25 = type <{ %class.btAlignedAllocator.26, [3 x i8], i32, i32, %"struct.btSoftBody::Node"*, i8, [3 x i8] }>
%class.btAlignedAllocator.26 = type { i8 }
%class.btAlignedObjectArray.29 = type <{ %class.btAlignedAllocator.30, [3 x i8], i32, i32, %"struct.btSoftBody::Link"*, i8, [3 x i8] }>
%class.btAlignedAllocator.30 = type { i8 }
%"struct.btSoftBody::Link" = type { %"struct.btSoftBody::Feature", [2 x %"struct.btSoftBody::Node"*], float, i8, float, float, float, %class.btVector3 }
%class.btAlignedObjectArray.33 = type <{ %class.btAlignedAllocator.34, [3 x i8], i32, i32, %"struct.btSoftBody::Face"*, i8, [3 x i8] }>
%class.btAlignedAllocator.34 = type { i8 }
%"struct.btSoftBody::Face" = type { %"struct.btSoftBody::Feature", [3 x %"struct.btSoftBody::Node"*], %class.btVector3, float, %struct.btDbvtNode* }
%class.btAlignedObjectArray.37 = type <{ %class.btAlignedAllocator.38, [3 x i8], i32, i32, %"struct.btSoftBody::Tetra"*, i8, [3 x i8] }>
%class.btAlignedAllocator.38 = type { i8 }
%"struct.btSoftBody::Tetra" = type { %"struct.btSoftBody::Feature", [4 x %"struct.btSoftBody::Node"*], float, %struct.btDbvtNode*, [4 x %class.btVector3], float, float }
%class.btAlignedObjectArray.41 = type <{ %class.btAlignedAllocator.42, [3 x i8], i32, i32, %"struct.btSoftBody::Anchor"*, i8, [3 x i8] }>
%class.btAlignedAllocator.42 = type { i8 }
%"struct.btSoftBody::Anchor" = type { %"struct.btSoftBody::Node"*, %class.btVector3, %class.btRigidBody*, float, %class.btMatrix3x3, %class.btVector3, float }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.44, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray.44 = type <{ %class.btAlignedAllocator.45, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.45 = type { i8 }
%class.btTypedConstraint = type opaque
%class.btAlignedObjectArray.49 = type <{ %class.btAlignedAllocator.50, [3 x i8], i32, i32, %"struct.btSoftBody::RContact"*, i8, [3 x i8] }>
%class.btAlignedAllocator.50 = type { i8 }
%"struct.btSoftBody::RContact" = type { %"struct.btSoftBody::sCti", %"struct.btSoftBody::Node"*, %class.btMatrix3x3, %class.btVector3, float, float, float }
%"struct.btSoftBody::sCti" = type { %class.btCollisionObject*, %class.btVector3, float }
%class.btAlignedObjectArray.53 = type <{ %class.btAlignedAllocator.54, [3 x i8], i32, i32, %"struct.btSoftBody::SContact"*, i8, [3 x i8] }>
%class.btAlignedAllocator.54 = type { i8 }
%"struct.btSoftBody::SContact" = type { %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Face"*, %class.btVector3, %class.btVector3, float, float, [2 x float] }
%class.btAlignedObjectArray.57 = type <{ %class.btAlignedAllocator.58, [3 x i8], i32, i32, %"struct.btSoftBody::Joint"**, i8, [3 x i8] }>
%class.btAlignedAllocator.58 = type { i8 }
%"struct.btSoftBody::Joint" = type <{ i32 (...)**, [2 x %"struct.btSoftBody::Body"], [2 x %class.btVector3], float, float, float, %class.btVector3, %class.btVector3, %class.btMatrix3x3, i8, [3 x i8] }>
%"struct.btSoftBody::Body" = type { %"struct.btSoftBody::Cluster"*, %class.btRigidBody*, %class.btCollisionObject* }
%"struct.btSoftBody::Cluster" = type { %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.60, %class.btAlignedObjectArray.8, %class.btTransform, float, float, %class.btMatrix3x3, %class.btMatrix3x3, %class.btVector3, [2 x %class.btVector3], [2 x %class.btVector3], i32, i32, %class.btVector3, %class.btVector3, %struct.btDbvtNode*, float, float, float, float, float, float, i8, i8, i32 }
%class.btAlignedObjectArray.60 = type <{ %class.btAlignedAllocator.61, [3 x i8], i32, i32, %"struct.btSoftBody::Node"**, i8, [3 x i8] }>
%class.btAlignedAllocator.61 = type { i8 }
%class.btAlignedObjectArray.65 = type <{ %class.btAlignedAllocator.66, [3 x i8], i32, i32, %"struct.btSoftBody::Material"**, i8, [3 x i8] }>
%class.btAlignedAllocator.66 = type { i8 }
%struct.btDbvt = type { %struct.btDbvtNode*, %struct.btDbvtNode*, i32, i32, i32, %class.btAlignedObjectArray.69, %class.btAlignedObjectArray.73 }
%class.btAlignedObjectArray.69 = type <{ %class.btAlignedAllocator.70, [3 x i8], i32, i32, %"struct.btDbvt::sStkNN"*, i8, [3 x i8] }>
%class.btAlignedAllocator.70 = type { i8 }
%"struct.btDbvt::sStkNN" = type { %struct.btDbvtNode*, %struct.btDbvtNode* }
%class.btAlignedObjectArray.73 = type <{ %class.btAlignedAllocator.74, [3 x i8], i32, i32, %struct.btDbvtNode**, i8, [3 x i8] }>
%class.btAlignedAllocator.74 = type { i8 }
%class.btAlignedObjectArray.77 = type <{ %class.btAlignedAllocator.78, [3 x i8], i32, i32, %"struct.btSoftBody::Cluster"**, i8, [3 x i8] }>
%class.btAlignedAllocator.78 = type { i8 }
%class.btAlignedObjectArray.81 = type <{ %class.btAlignedAllocator.82, [3 x i8], i32, i32, i8*, i8, [3 x i8] }>
%class.btAlignedAllocator.82 = type { i8 }
%class.btAlignedObjectArray.85 = type <{ %class.btAlignedAllocator.86, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.86 = type { i8 }
%class.btIDebugDraw = type { i32 (...)** }
%class.btConvexHullComputer = type { %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.85 }
%class.btAlignedObjectArray.89 = type <{ %class.btAlignedAllocator.90, [3 x i8], i32, i32, %"class.btConvexHullComputer::Edge"*, i8, [3 x i8] }>
%class.btAlignedAllocator.90 = type { i8 }
%"class.btConvexHullComputer::Edge" = type { i32, i32, i32 }
%"struct.btSoftBody::LJoint" = type { %"struct.btSoftBody::Joint.base", [2 x %class.btVector3] }
%"struct.btSoftBody::Joint.base" = type <{ i32 (...)**, [2 x %"struct.btSoftBody::Body"], [2 x %class.btVector3], float, float, float, %class.btVector3, %class.btVector3, %class.btMatrix3x3, i8 }>
%class.HullDesc = type { i32, i32, %class.btVector3*, i32, float, i32, i32 }
%class.HullResult = type { i8, i32, %class.btAlignedObjectArray.8, i32, i32, %class.btAlignedObjectArray.93 }
%class.btAlignedObjectArray.93 = type <{ %class.btAlignedAllocator.94, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.94 = type { i8 }
%class.HullLibrary = type { %class.btAlignedObjectArray.97, %class.btAlignedObjectArray.85 }
%class.btAlignedObjectArray.97 = type <{ %class.btAlignedAllocator.98, [3 x i8], i32, i32, %class.btHullTriangle**, i8, [3 x i8] }>
%class.btAlignedAllocator.98 = type { i8 }
%class.btHullTriangle = type opaque

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZNK20btAlignedObjectArrayIPN10btSoftBody7ClusterEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIPN10btSoftBody7ClusterEEixEi = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK9btVector310normalizedEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_ = comdat any

$_ZNK20btAlignedObjectArrayIPN10btSoftBody4NodeEE4sizeEv = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIPN10btSoftBody4NodeEEixEi = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EixEi = comdat any

$_ZN20btConvexHullComputerC2Ev = comdat any

$_ZN20btConvexHullComputer7computeEPKfiiff = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK20btAlignedObjectArrayIiE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIiEixEi = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEEixEi = comdat any

$_ZNK20btConvexHullComputer4Edge17getNextEdgeOfFaceEv = comdat any

$_ZNK20btConvexHullComputer4Edge15getSourceVertexEv = comdat any

$_ZNK20btConvexHullComputer4Edge15getTargetVertexEv = comdat any

$_ZN20btConvexHullComputerD2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3ED2Ev = comdat any

$_ZNK20btAlignedObjectArrayIN10btSoftBody4NodeEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIN10btSoftBody4NodeEEixEi = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK20btAlignedObjectArrayIN10btSoftBody4LinkEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIN10btSoftBody4LinkEEixEi = comdat any

$_ZNK20btAlignedObjectArrayIN10btSoftBody8RContactEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIN10btSoftBody8RContactEEixEi = comdat any

$_Z5btDotRK9btVector3S1_ = comdat any

$_Z7btCrossRK9btVector3S1_ = comdat any

$_ZNK9btVector37minAxisEv = comdat any

$_ZNK20btAlignedObjectArrayIN10btSoftBody4FaceEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIN10btSoftBody4FaceEEixEi = comdat any

$_ZdvRK9btVector3RKf = comdat any

$_ZNK20btAlignedObjectArrayIN10btSoftBody5TetraEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIN10btSoftBody5TetraEEixEi = comdat any

$_ZNK20btAlignedObjectArrayIN10btSoftBody6AnchorEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIN10btSoftBody6AnchorEEixEi = comdat any

$_ZN17btCollisionObject17getWorldTransformEv = comdat any

$_ZNK11btTransformmlERK9btVector3 = comdat any

$_ZNK20btAlignedObjectArrayIN10btSoftBody4NoteEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIN10btSoftBody4NoteEEixEi = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZNK20btAlignedObjectArrayIPN10btSoftBody5JointEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIPN10btSoftBody5JointEEixEi = comdat any

$_ZNK10btSoftBody4Body5xformEv = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZN9btVector3naEm = comdat any

$_Z4lerpRK9btVector3S1_RKf = comdat any

$_ZN17btCollisionObjectnwEm = comdat any

$_ZN9btVector3daEPv = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_Z5btMaxIiERKT_S2_S2_ = comdat any

$_ZN20btAlignedObjectArrayIbEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIbE6resizeEiRKb = comdat any

$_ZN20btAlignedObjectArrayIbEixEi = comdat any

$_ZN20btAlignedObjectArrayIbED2Ev = comdat any

$_ZN8HullDescC2E8HullFlagjPK9btVector3j = comdat any

$_ZN10HullResultC2Ev = comdat any

$_ZN11HullLibraryC2Ev = comdat any

$_ZN20btAlignedObjectArrayIjEixEi = comdat any

$_ZN11HullLibraryD2Ev = comdat any

$_ZN10HullResultD2Ev = comdat any

$_ZN9btVector34setXEf = comdat any

$_ZN9btVector34setYEf = comdat any

$_ZN9btVector34setZEf = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_Z6btSqrtf = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiEC2Ev = comdat any

$_ZN18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4initEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiE4initEv = comdat any

$_ZNK20btConvexHullComputer4Edge19getNextEdgeOfVertexEv = comdat any

$_ZN20btAlignedObjectArrayIiED2Ev = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEED2Ev = comdat any

$_ZN20btAlignedObjectArrayIiE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIiE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIiE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EE10deallocateEPS1_ = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZN11btTransform11getIdentityEv = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZNK17btCollisionObject17getWorldTransformEv = comdat any

$_ZN11btMatrix3x311getIdentityEv = comdat any

$_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3 = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZNK10btDbvtNode10isinternalEv = comdat any

$_ZNK12btDbvtAabbMm6CenterEv = comdat any

$_ZNK12btDbvtAabbMm7ExtentsEv = comdat any

$_ZNK10btDbvtNode6isleafEv = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZNK9btVector34lerpERKS_RKf = comdat any

$_Z5btCosf = comdat any

$_Z5btSinf = comdat any

$_ZN20btAlignedObjectArrayIjEC2Ev = comdat any

$_ZN18btAlignedAllocatorIjLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIjE4initEv = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleEC2Ev = comdat any

$_ZN18btAlignedAllocatorIP14btHullTriangleLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleE4initEv = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIP14btHullTriangleE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP14btHullTriangleLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIjED2Ev = comdat any

$_ZN20btAlignedObjectArrayIjE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIjE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIjE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIjE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIjLj16EE10deallocateEPj = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E4initEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E5clearEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7reserveEi = comdat any

$_ZN9btVector3nwEmPv = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorIbLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIbE4initEv = comdat any

$_ZN20btAlignedObjectArrayIbE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIbE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIbE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIbE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIbLj16EE10deallocateEPb = comdat any

$_ZN20btAlignedObjectArrayIbE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIbE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIbE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIbE4copyEiiPb = comdat any

$_ZN18btAlignedAllocatorIbLj16EE8allocateEiPPKb = comdat any

$_ZZNK10btSoftBody4Body5xformEvE8identity = comdat any

$_ZGVZNK10btSoftBody4Body5xformEvE8identity = comdat any

$_ZZN11btTransform11getIdentityEvE17identityTransform = comdat any

$_ZGVZN11btTransform11getIdentityEvE17identityTransform = comdat any

$_ZZN11btMatrix3x311getIdentityEvE14identityMatrix = comdat any

$_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix = comdat any

@_ZZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis = internal global [3 x %class.btVector3] zeroinitializer, align 16
@_ZGVZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis = internal global i32 0, align 4
@.str = private unnamed_addr constant [9 x i8] c" M(%.2f)\00", align 1
@.str.1 = private unnamed_addr constant [9 x i8] c" A(%.2f)\00", align 1
@_ZZN17btSoftBodyHelpers9DrawFrameEP10btSoftBodyP12btIDebugDrawE4ascl = internal constant float 1.000000e+01, align 4
@_ZZN17btSoftBodyHelpers9DrawFrameEP10btSoftBodyP12btIDebugDrawE4nscl = internal constant float 0x3FB99999A0000000, align 4
@.str.2 = private unnamed_addr constant [12 x i8] c"%d %d %d %d\00", align 1
@.str.3 = private unnamed_addr constant [12 x i8] c"%d %f %f %f\00", align 1
@.str.4 = private unnamed_addr constant [9 x i8] c"%d %d %d\00", align 1
@.str.5 = private unnamed_addr constant [15 x i8] c"%d %d %d %d %d\00", align 1
@.str.6 = private unnamed_addr constant [13 x i8] c"Nodes:  %u\0D\0A\00", align 1
@.str.7 = private unnamed_addr constant [13 x i8] c"Links:  %u\0D\0A\00", align 1
@.str.8 = private unnamed_addr constant [13 x i8] c"Faces:  %u\0D\0A\00", align 1
@.str.9 = private unnamed_addr constant [13 x i8] c"Tetras: %u\0D\0A\00", align 1
@_ZZNK10btSoftBody4Body5xformEvE8identity = linkonce_odr hidden global %class.btTransform zeroinitializer, comdat, align 4
@_ZGVZNK10btSoftBody4Body5xformEvE8identity = linkonce_odr hidden global i32 0, comdat, align 4
@_ZZN11btTransform11getIdentityEvE17identityTransform = linkonce_odr hidden global %class.btTransform zeroinitializer, comdat, align 4
@_ZGVZN11btTransform11getIdentityEvE17identityTransform = linkonce_odr hidden global i32 0, comdat, align 4
@_ZZN11btMatrix3x311getIdentityEvE14identityMatrix = linkonce_odr hidden global %class.btMatrix3x3 zeroinitializer, comdat, align 4
@_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix = linkonce_odr hidden global i32 0, comdat, align 4

define hidden void @_ZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawi(%class.btSoftBody* %psb, %class.btIDebugDraw* %idraw, i32 %drawflags) #0 {
entry:
  %psb.addr = alloca %class.btSoftBody*, align 4
  %idraw.addr = alloca %class.btIDebugDraw*, align 4
  %drawflags.addr = alloca i32, align 4
  %scl = alloca float, align 4
  %nscl = alloca float, align 4
  %lcolor = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ncolor = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ccolor = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %nj = alloca i32, align 4
  %color = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp22 = alloca float, align 4
  %ref.tmp27 = alloca %class.btVector3, align 4
  %ref.tmp28 = alloca %class.btVector3, align 4
  %ref.tmp29 = alloca float, align 4
  %vertices = alloca %class.btAlignedObjectArray.8, align 4
  %ref.tmp34 = alloca %class.btVector3, align 4
  %computer = alloca %class.btConvexHullComputer, align 4
  %stride = alloca i32, align 4
  %count = alloca i32, align 4
  %shrink = alloca float, align 4
  %shrinkClamp = alloca float, align 4
  %i50 = alloca i32, align 4
  %face = alloca i32, align 4
  %firstEdge = alloca %"class.btConvexHullComputer::Edge"*, align 4
  %edge = alloca %"class.btConvexHullComputer::Edge"*, align 4
  %v0 = alloca i32, align 4
  %v1 = alloca i32, align 4
  %v2 = alloca i32, align 4
  %n = alloca %"struct.btSoftBody::Node"*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %ref.tmp92 = alloca %class.btVector3, align 4
  %ref.tmp94 = alloca %class.btVector3, align 4
  %ref.tmp95 = alloca float, align 4
  %ref.tmp96 = alloca float, align 4
  %ref.tmp98 = alloca %class.btVector3, align 4
  %ref.tmp100 = alloca %class.btVector3, align 4
  %ref.tmp101 = alloca float, align 4
  %ref.tmp102 = alloca float, align 4
  %ref.tmp104 = alloca %class.btVector3, align 4
  %ref.tmp105 = alloca float, align 4
  %ref.tmp106 = alloca float, align 4
  %ref.tmp107 = alloca float, align 4
  %ref.tmp111 = alloca %class.btVector3, align 4
  %ref.tmp113 = alloca %class.btVector3, align 4
  %ref.tmp114 = alloca float, align 4
  %ref.tmp115 = alloca float, align 4
  %ref.tmp117 = alloca %class.btVector3, align 4
  %ref.tmp119 = alloca %class.btVector3, align 4
  %ref.tmp120 = alloca float, align 4
  %ref.tmp121 = alloca float, align 4
  %ref.tmp123 = alloca %class.btVector3, align 4
  %ref.tmp124 = alloca float, align 4
  %ref.tmp125 = alloca float, align 4
  %ref.tmp126 = alloca float, align 4
  %ref.tmp130 = alloca %class.btVector3, align 4
  %ref.tmp132 = alloca %class.btVector3, align 4
  %ref.tmp133 = alloca float, align 4
  %ref.tmp134 = alloca float, align 4
  %ref.tmp136 = alloca %class.btVector3, align 4
  %ref.tmp138 = alloca %class.btVector3, align 4
  %ref.tmp139 = alloca float, align 4
  %ref.tmp140 = alloca float, align 4
  %ref.tmp142 = alloca %class.btVector3, align 4
  %ref.tmp143 = alloca float, align 4
  %ref.tmp144 = alloca float, align 4
  %ref.tmp145 = alloca float, align 4
  %l = alloca %"struct.btSoftBody::Link"*, align 4
  %n189 = alloca %"struct.btSoftBody::Node"*, align 4
  %d = alloca %class.btVector3, align 4
  %ref.tmp200 = alloca %class.btVector3, align 4
  %ref.tmp205 = alloca %class.btVector3, align 4
  %ref.tmp207 = alloca %class.btVector3, align 4
  %ref.tmp208 = alloca float, align 4
  %ref.tmp222 = alloca float, align 4
  %ref.tmp223 = alloca float, align 4
  %ref.tmp224 = alloca float, align 4
  %ref.tmp226 = alloca float, align 4
  %ref.tmp227 = alloca float, align 4
  %ref.tmp228 = alloca float, align 4
  %ref.tmp230 = alloca float, align 4
  %ref.tmp231 = alloca float, align 4
  %ref.tmp232 = alloca float, align 4
  %c = alloca %"struct.btSoftBody::RContact"*, align 4
  %o = alloca %class.btVector3, align 4
  %ref.tmp241 = alloca %class.btVector3, align 4
  %ref.tmp242 = alloca float, align 4
  %x = alloca %class.btVector3, align 4
  %ref.tmp249 = alloca %class.btVector3, align 4
  %y = alloca %class.btVector3, align 4
  %ref.tmp256 = alloca %class.btVector3, align 4
  %ref.tmp259 = alloca %class.btVector3, align 4
  %ref.tmp260 = alloca %class.btVector3, align 4
  %ref.tmp261 = alloca %class.btVector3, align 4
  %ref.tmp262 = alloca %class.btVector3, align 4
  %ref.tmp265 = alloca %class.btVector3, align 4
  %ref.tmp266 = alloca %class.btVector3, align 4
  %ref.tmp267 = alloca %class.btVector3, align 4
  %ref.tmp268 = alloca %class.btVector3, align 4
  %ref.tmp271 = alloca %class.btVector3, align 4
  %ref.tmp272 = alloca %class.btVector3, align 4
  %ref.tmp273 = alloca %class.btVector3, align 4
  %ref.tmp276 = alloca float, align 4
  %ref.tmp277 = alloca %class.btVector3, align 4
  %ref.tmp278 = alloca float, align 4
  %ref.tmp279 = alloca float, align 4
  %ref.tmp280 = alloca float, align 4
  %scl291 = alloca float, align 4
  %alp = alloca float, align 4
  %col = alloca %class.btVector3, align 4
  %ref.tmp292 = alloca float, align 4
  %ref.tmp293 = alloca float, align 4
  %ref.tmp294 = alloca float, align 4
  %f = alloca %"struct.btSoftBody::Face"*, align 4
  %x308 = alloca [3 x %class.btVector3], align 16
  %c319 = alloca %class.btVector3, align 4
  %ref.tmp320 = alloca %class.btVector3, align 4
  %ref.tmp321 = alloca %class.btVector3, align 4
  %ref.tmp325 = alloca float, align 4
  %ref.tmp326 = alloca %class.btVector3, align 4
  %ref.tmp327 = alloca %class.btVector3, align 4
  %ref.tmp328 = alloca %class.btVector3, align 4
  %ref.tmp330 = alloca %class.btVector3, align 4
  %ref.tmp331 = alloca %class.btVector3, align 4
  %ref.tmp332 = alloca %class.btVector3, align 4
  %ref.tmp334 = alloca %class.btVector3, align 4
  %ref.tmp335 = alloca %class.btVector3, align 4
  %ref.tmp336 = alloca %class.btVector3, align 4
  %scl350 = alloca float, align 4
  %alp351 = alloca float, align 4
  %col352 = alloca %class.btVector3, align 4
  %ref.tmp353 = alloca float, align 4
  %ref.tmp354 = alloca float, align 4
  %ref.tmp355 = alloca float, align 4
  %i357 = alloca i32, align 4
  %t = alloca %"struct.btSoftBody::Tetra"*, align 4
  %x371 = alloca [4 x %class.btVector3], align 16
  %c388 = alloca %class.btVector3, align 4
  %ref.tmp389 = alloca %class.btVector3, align 4
  %ref.tmp390 = alloca %class.btVector3, align 4
  %ref.tmp391 = alloca %class.btVector3, align 4
  %ref.tmp396 = alloca float, align 4
  %ref.tmp397 = alloca %class.btVector3, align 4
  %ref.tmp398 = alloca %class.btVector3, align 4
  %ref.tmp399 = alloca %class.btVector3, align 4
  %ref.tmp401 = alloca %class.btVector3, align 4
  %ref.tmp402 = alloca %class.btVector3, align 4
  %ref.tmp403 = alloca %class.btVector3, align 4
  %ref.tmp405 = alloca %class.btVector3, align 4
  %ref.tmp406 = alloca %class.btVector3, align 4
  %ref.tmp407 = alloca %class.btVector3, align 4
  %ref.tmp411 = alloca %class.btVector3, align 4
  %ref.tmp412 = alloca %class.btVector3, align 4
  %ref.tmp413 = alloca %class.btVector3, align 4
  %ref.tmp415 = alloca %class.btVector3, align 4
  %ref.tmp416 = alloca %class.btVector3, align 4
  %ref.tmp417 = alloca %class.btVector3, align 4
  %ref.tmp419 = alloca %class.btVector3, align 4
  %ref.tmp420 = alloca %class.btVector3, align 4
  %ref.tmp421 = alloca %class.btVector3, align 4
  %ref.tmp425 = alloca %class.btVector3, align 4
  %ref.tmp426 = alloca %class.btVector3, align 4
  %ref.tmp427 = alloca %class.btVector3, align 4
  %ref.tmp429 = alloca %class.btVector3, align 4
  %ref.tmp430 = alloca %class.btVector3, align 4
  %ref.tmp431 = alloca %class.btVector3, align 4
  %ref.tmp433 = alloca %class.btVector3, align 4
  %ref.tmp434 = alloca %class.btVector3, align 4
  %ref.tmp435 = alloca %class.btVector3, align 4
  %ref.tmp439 = alloca %class.btVector3, align 4
  %ref.tmp440 = alloca %class.btVector3, align 4
  %ref.tmp441 = alloca %class.btVector3, align 4
  %ref.tmp443 = alloca %class.btVector3, align 4
  %ref.tmp444 = alloca %class.btVector3, align 4
  %ref.tmp445 = alloca %class.btVector3, align 4
  %ref.tmp447 = alloca %class.btVector3, align 4
  %ref.tmp448 = alloca %class.btVector3, align 4
  %ref.tmp449 = alloca %class.btVector3, align 4
  %a = alloca %"struct.btSoftBody::Anchor"*, align 4
  %q = alloca %class.btVector3, align 4
  %ref.tmp474 = alloca %class.btVector3, align 4
  %ref.tmp475 = alloca float, align 4
  %ref.tmp476 = alloca float, align 4
  %ref.tmp477 = alloca float, align 4
  %ref.tmp479 = alloca %class.btVector3, align 4
  %ref.tmp480 = alloca float, align 4
  %ref.tmp481 = alloca float, align 4
  %ref.tmp482 = alloca float, align 4
  %ref.tmp486 = alloca %class.btVector3, align 4
  %ref.tmp487 = alloca float, align 4
  %ref.tmp488 = alloca float, align 4
  %ref.tmp489 = alloca float, align 4
  %n501 = alloca %"struct.btSoftBody::Node"*, align 4
  %ref.tmp513 = alloca %class.btVector3, align 4
  %ref.tmp514 = alloca float, align 4
  %ref.tmp515 = alloca float, align 4
  %ref.tmp516 = alloca float, align 4
  %n533 = alloca %"struct.btSoftBody::Note"*, align 4
  %p = alloca %class.btVector3, align 4
  %j537 = alloca i32, align 4
  %ref.tmp542 = alloca %class.btVector3, align 4
  %pj = alloca %"struct.btSoftBody::Joint"*, align 4
  %pjl = alloca %"struct.btSoftBody::LJoint"*, align 4
  %a0 = alloca %class.btVector3, align 4
  %a1 = alloca %class.btVector3, align 4
  %ref.tmp594 = alloca %class.btVector3, align 4
  %ref.tmp595 = alloca float, align 4
  %ref.tmp596 = alloca float, align 4
  %ref.tmp597 = alloca float, align 4
  %ref.tmp605 = alloca %class.btVector3, align 4
  %ref.tmp606 = alloca float, align 4
  %ref.tmp607 = alloca float, align 4
  %ref.tmp608 = alloca float, align 4
  %ref.tmp612 = alloca %class.btVector3, align 4
  %ref.tmp613 = alloca float, align 4
  %ref.tmp614 = alloca float, align 4
  %ref.tmp615 = alloca float, align 4
  %ref.tmp617 = alloca %class.btVector3, align 4
  %ref.tmp618 = alloca float, align 4
  %ref.tmp619 = alloca float, align 4
  %ref.tmp620 = alloca float, align 4
  %o0 = alloca %class.btVector3, align 4
  %o1 = alloca %class.btVector3, align 4
  %a0631 = alloca %class.btVector3, align 4
  %a1638 = alloca %class.btVector3, align 4
  %ref.tmp645 = alloca %class.btVector3, align 4
  %ref.tmp646 = alloca %class.btVector3, align 4
  %ref.tmp647 = alloca float, align 4
  %ref.tmp648 = alloca %class.btVector3, align 4
  %ref.tmp649 = alloca float, align 4
  %ref.tmp650 = alloca float, align 4
  %ref.tmp651 = alloca float, align 4
  %ref.tmp655 = alloca %class.btVector3, align 4
  %ref.tmp656 = alloca %class.btVector3, align 4
  %ref.tmp657 = alloca float, align 4
  %ref.tmp658 = alloca %class.btVector3, align 4
  %ref.tmp659 = alloca float, align 4
  %ref.tmp660 = alloca float, align 4
  %ref.tmp661 = alloca float, align 4
  %ref.tmp665 = alloca %class.btVector3, align 4
  %ref.tmp666 = alloca %class.btVector3, align 4
  %ref.tmp667 = alloca float, align 4
  %ref.tmp668 = alloca %class.btVector3, align 4
  %ref.tmp669 = alloca float, align 4
  %ref.tmp670 = alloca float, align 4
  %ref.tmp671 = alloca float, align 4
  %ref.tmp675 = alloca %class.btVector3, align 4
  %ref.tmp676 = alloca %class.btVector3, align 4
  %ref.tmp677 = alloca float, align 4
  %ref.tmp678 = alloca %class.btVector3, align 4
  %ref.tmp679 = alloca float, align 4
  %ref.tmp680 = alloca float, align 4
  %ref.tmp681 = alloca float, align 4
  store %class.btSoftBody* %psb, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  store %class.btIDebugDraw* %idraw, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  store i32 %drawflags, i32* %drawflags.addr, align 4, !tbaa !6
  %0 = bitcast float* %scl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store float 0x3FB99999A0000000, float* %scl, align 4, !tbaa !8
  %1 = bitcast float* %nscl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store float 5.000000e-01, float* %nscl, align 4, !tbaa !8
  %2 = bitcast %class.btVector3* %lcolor to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #6
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !8
  %4 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  store float 0.000000e+00, float* %ref.tmp1, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %lcolor, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  %6 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #6
  %7 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #6
  %9 = bitcast %class.btVector3* %ncolor to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #6
  %10 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  store float 1.000000e+00, float* %ref.tmp3, align 4, !tbaa !8
  %11 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #6
  store float 1.000000e+00, float* %ref.tmp4, align 4, !tbaa !8
  %12 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  store float 1.000000e+00, float* %ref.tmp5, align 4, !tbaa !8
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ncolor, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %13 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #6
  %14 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #6
  %15 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast %class.btVector3* %ccolor to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #6
  %17 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #6
  store float 1.000000e+00, float* %ref.tmp7, align 4, !tbaa !8
  %18 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #6
  store float 0.000000e+00, float* %ref.tmp8, align 4, !tbaa !8
  %19 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #6
  store float 0.000000e+00, float* %ref.tmp9, align 4, !tbaa !8
  %call10 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ccolor, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %20 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #6
  %21 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #6
  %22 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #6
  %23 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #6
  %24 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #6
  %25 = bitcast i32* %nj to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #6
  %26 = load i32, i32* %drawflags.addr, align 4, !tbaa !6
  %and = and i32 %26, 256
  %cmp = icmp ne i32 0, %and
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  call void @srand(i32 1806)
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc75, %if.then
  %27 = load i32, i32* %i, align 4, !tbaa !6
  %28 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_clusters = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %28, i32 0, i32 24
  %call11 = call i32 @_ZNK20btAlignedObjectArrayIPN10btSoftBody7ClusterEE4sizeEv(%class.btAlignedObjectArray.77* %m_clusters)
  %cmp12 = icmp slt i32 %27, %call11
  br i1 %cmp12, label %for.body, label %for.end77

for.body:                                         ; preds = %for.cond
  %29 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_clusters13 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %29, i32 0, i32 24
  %30 = load i32, i32* %i, align 4, !tbaa !6
  %call14 = call nonnull align 4 dereferenceable(4) %"struct.btSoftBody::Cluster"** @_ZN20btAlignedObjectArrayIPN10btSoftBody7ClusterEEixEi(%class.btAlignedObjectArray.77* %m_clusters13, i32 %30)
  %31 = load %"struct.btSoftBody::Cluster"*, %"struct.btSoftBody::Cluster"** %call14, align 4, !tbaa !2
  %m_collide = getelementptr inbounds %"struct.btSoftBody::Cluster", %"struct.btSoftBody::Cluster"* %31, i32 0, i32 23
  %32 = load i8, i8* %m_collide, align 1, !tbaa !10, !range !22
  %tobool = trunc i8 %32 to i1
  br i1 %tobool, label %if.then15, label %if.end

if.then15:                                        ; preds = %for.body
  %33 = bitcast %class.btVector3* %color to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %33) #6
  %34 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #6
  %call17 = call i32 @rand()
  %conv = sitofp i32 %call17 to float
  %div = fdiv float %conv, 0x41E0000000000000
  store float %div, float* %ref.tmp16, align 4, !tbaa !8
  %35 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #6
  %call19 = call i32 @rand()
  %conv20 = sitofp i32 %call19 to float
  %div21 = fdiv float %conv20, 0x41E0000000000000
  store float %div21, float* %ref.tmp18, align 4, !tbaa !8
  %36 = bitcast float* %ref.tmp22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #6
  %call23 = call i32 @rand()
  %conv24 = sitofp i32 %call23 to float
  %div25 = fdiv float %conv24, 0x41E0000000000000
  store float %div25, float* %ref.tmp22, align 4, !tbaa !8
  %call26 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %color, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp22)
  %37 = bitcast float* %ref.tmp22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #6
  %38 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #6
  %39 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #6
  %40 = bitcast %class.btVector3* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %40) #6
  %41 = bitcast %class.btVector3* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %41) #6
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %ref.tmp28, %class.btVector3* %color)
  %42 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #6
  store float 7.500000e-01, float* %ref.tmp29, align 4, !tbaa !8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp27, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp29)
  %43 = bitcast %class.btVector3* %color to i8*
  %44 = bitcast %class.btVector3* %ref.tmp27 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %43, i8* align 4 %44, i32 16, i1 false), !tbaa.struct !23
  %45 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #6
  %46 = bitcast %class.btVector3* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %46) #6
  %47 = bitcast %class.btVector3* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %47) #6
  %48 = bitcast %class.btAlignedObjectArray.8* %vertices to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %48) #6
  %call30 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.8* %vertices)
  %49 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_clusters31 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %49, i32 0, i32 24
  %50 = load i32, i32* %i, align 4, !tbaa !6
  %call32 = call nonnull align 4 dereferenceable(4) %"struct.btSoftBody::Cluster"** @_ZN20btAlignedObjectArrayIPN10btSoftBody7ClusterEEixEi(%class.btAlignedObjectArray.77* %m_clusters31, i32 %50)
  %51 = load %"struct.btSoftBody::Cluster"*, %"struct.btSoftBody::Cluster"** %call32, align 4, !tbaa !2
  %m_nodes = getelementptr inbounds %"struct.btSoftBody::Cluster", %"struct.btSoftBody::Cluster"* %51, i32 0, i32 1
  %call33 = call i32 @_ZNK20btAlignedObjectArrayIPN10btSoftBody4NodeEE4sizeEv(%class.btAlignedObjectArray.60* %m_nodes)
  %52 = bitcast %class.btVector3* %ref.tmp34 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %52) #6
  %call35 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp34)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.8* %vertices, i32 %call33, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp34)
  %53 = bitcast %class.btVector3* %ref.tmp34 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %53) #6
  store i32 0, i32* %j, align 4, !tbaa !6
  %call36 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %vertices)
  store i32 %call36, i32* %nj, align 4, !tbaa !6
  br label %for.cond37

for.cond37:                                       ; preds = %for.inc, %if.then15
  %54 = load i32, i32* %j, align 4, !tbaa !6
  %55 = load i32, i32* %nj, align 4, !tbaa !6
  %cmp38 = icmp slt i32 %54, %55
  br i1 %cmp38, label %for.body39, label %for.end

for.body39:                                       ; preds = %for.cond37
  %56 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_clusters40 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %56, i32 0, i32 24
  %57 = load i32, i32* %i, align 4, !tbaa !6
  %call41 = call nonnull align 4 dereferenceable(4) %"struct.btSoftBody::Cluster"** @_ZN20btAlignedObjectArrayIPN10btSoftBody7ClusterEEixEi(%class.btAlignedObjectArray.77* %m_clusters40, i32 %57)
  %58 = load %"struct.btSoftBody::Cluster"*, %"struct.btSoftBody::Cluster"** %call41, align 4, !tbaa !2
  %m_nodes42 = getelementptr inbounds %"struct.btSoftBody::Cluster", %"struct.btSoftBody::Cluster"* %58, i32 0, i32 1
  %59 = load i32, i32* %j, align 4, !tbaa !6
  %call43 = call nonnull align 4 dereferenceable(4) %"struct.btSoftBody::Node"** @_ZN20btAlignedObjectArrayIPN10btSoftBody4NodeEEixEi(%class.btAlignedObjectArray.60* %m_nodes42, i32 %59)
  %60 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %call43, align 4, !tbaa !2
  %m_x = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %60, i32 0, i32 1
  %61 = load i32, i32* %j, align 4, !tbaa !6
  %call44 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vertices, i32 %61)
  %62 = bitcast %class.btVector3* %call44 to i8*
  %63 = bitcast %class.btVector3* %m_x to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %62, i8* align 4 %63, i32 16, i1 false), !tbaa.struct !23
  br label %for.inc

for.inc:                                          ; preds = %for.body39
  %64 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %64, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond37

for.end:                                          ; preds = %for.cond37
  %65 = bitcast %class.btConvexHullComputer* %computer to i8*
  call void @llvm.lifetime.start.p0i8(i64 60, i8* %65) #6
  %call45 = call %class.btConvexHullComputer* @_ZN20btConvexHullComputerC2Ev(%class.btConvexHullComputer* %computer)
  %66 = bitcast i32* %stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #6
  store i32 16, i32* %stride, align 4, !tbaa !6
  %67 = bitcast i32* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #6
  %call46 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %vertices)
  store i32 %call46, i32* %count, align 4, !tbaa !6
  %68 = bitcast float* %shrink to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #6
  store float 0.000000e+00, float* %shrink, align 4, !tbaa !8
  %69 = bitcast float* %shrinkClamp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #6
  store float 0.000000e+00, float* %shrinkClamp, align 4, !tbaa !8
  %call47 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vertices, i32 0)
  %call48 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %call47)
  %70 = load i32, i32* %stride, align 4, !tbaa !6
  %71 = load i32, i32* %count, align 4, !tbaa !6
  %72 = load float, float* %shrink, align 4, !tbaa !8
  %73 = load float, float* %shrinkClamp, align 4, !tbaa !8
  %call49 = call float @_ZN20btConvexHullComputer7computeEPKfiiff(%class.btConvexHullComputer* %computer, float* %call48, i32 %70, i32 %71, float %72, float %73)
  %74 = bitcast i32* %i50 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %74) #6
  store i32 0, i32* %i50, align 4, !tbaa !6
  br label %for.cond51

for.cond51:                                       ; preds = %for.inc70, %for.end
  %75 = load i32, i32* %i50, align 4, !tbaa !6
  %faces = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %computer, i32 0, i32 2
  %call52 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.85* %faces)
  %cmp53 = icmp slt i32 %75, %call52
  br i1 %cmp53, label %for.body54, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond51
  %76 = bitcast i32* %i50 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #6
  br label %for.end72

for.body54:                                       ; preds = %for.cond51
  %77 = bitcast i32* %face to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %77) #6
  %faces55 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %computer, i32 0, i32 2
  %78 = load i32, i32* %i50, align 4, !tbaa !6
  %call56 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.85* %faces55, i32 %78)
  %79 = load i32, i32* %call56, align 4, !tbaa !6
  store i32 %79, i32* %face, align 4, !tbaa !6
  %80 = bitcast %"class.btConvexHullComputer::Edge"** %firstEdge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #6
  %edges = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %computer, i32 0, i32 1
  %81 = load i32, i32* %face, align 4, !tbaa !6
  %call57 = call nonnull align 4 dereferenceable(12) %"class.btConvexHullComputer::Edge"* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEEixEi(%class.btAlignedObjectArray.89* %edges, i32 %81)
  store %"class.btConvexHullComputer::Edge"* %call57, %"class.btConvexHullComputer::Edge"** %firstEdge, align 4, !tbaa !2
  %82 = bitcast %"class.btConvexHullComputer::Edge"** %edge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #6
  %83 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %firstEdge, align 4, !tbaa !2
  %call58 = call %"class.btConvexHullComputer::Edge"* @_ZNK20btConvexHullComputer4Edge17getNextEdgeOfFaceEv(%"class.btConvexHullComputer::Edge"* %83)
  store %"class.btConvexHullComputer::Edge"* %call58, %"class.btConvexHullComputer::Edge"** %edge, align 4, !tbaa !2
  %84 = bitcast i32* %v0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %84) #6
  %85 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %firstEdge, align 4, !tbaa !2
  %call59 = call i32 @_ZNK20btConvexHullComputer4Edge15getSourceVertexEv(%"class.btConvexHullComputer::Edge"* %85)
  store i32 %call59, i32* %v0, align 4, !tbaa !6
  %86 = bitcast i32* %v1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %86) #6
  %87 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %firstEdge, align 4, !tbaa !2
  %call60 = call i32 @_ZNK20btConvexHullComputer4Edge15getTargetVertexEv(%"class.btConvexHullComputer::Edge"* %87)
  store i32 %call60, i32* %v1, align 4, !tbaa !6
  br label %while.cond

while.cond:                                       ; preds = %while.body, %for.body54
  %88 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %edge, align 4, !tbaa !2
  %89 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %firstEdge, align 4, !tbaa !2
  %cmp61 = icmp ne %"class.btConvexHullComputer::Edge"* %88, %89
  br i1 %cmp61, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %90 = bitcast i32* %v2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %90) #6
  %91 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %edge, align 4, !tbaa !2
  %call62 = call i32 @_ZNK20btConvexHullComputer4Edge15getTargetVertexEv(%"class.btConvexHullComputer::Edge"* %91)
  store i32 %call62, i32* %v2, align 4, !tbaa !6
  %92 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %vertices63 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %computer, i32 0, i32 0
  %93 = load i32, i32* %v0, align 4, !tbaa !6
  %call64 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vertices63, i32 %93)
  %vertices65 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %computer, i32 0, i32 0
  %94 = load i32, i32* %v1, align 4, !tbaa !6
  %call66 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vertices65, i32 %94)
  %vertices67 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %computer, i32 0, i32 0
  %95 = load i32, i32* %v2, align 4, !tbaa !6
  %call68 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vertices67, i32 %95)
  %96 = bitcast %class.btIDebugDraw* %92 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)***
  %vtable = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*** %96, align 4, !tbaa !25
  %vfn = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)** %vtable, i64 7
  %97 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)** %vfn, align 4
  call void %97(%class.btIDebugDraw* %92, %class.btVector3* nonnull align 4 dereferenceable(16) %call64, %class.btVector3* nonnull align 4 dereferenceable(16) %call66, %class.btVector3* nonnull align 4 dereferenceable(16) %call68, %class.btVector3* nonnull align 4 dereferenceable(16) %color, float 1.000000e+00)
  %98 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %edge, align 4, !tbaa !2
  %call69 = call %"class.btConvexHullComputer::Edge"* @_ZNK20btConvexHullComputer4Edge17getNextEdgeOfFaceEv(%"class.btConvexHullComputer::Edge"* %98)
  store %"class.btConvexHullComputer::Edge"* %call69, %"class.btConvexHullComputer::Edge"** %edge, align 4, !tbaa !2
  %99 = load i32, i32* %v1, align 4, !tbaa !6
  store i32 %99, i32* %v0, align 4, !tbaa !6
  %100 = load i32, i32* %v2, align 4, !tbaa !6
  store i32 %100, i32* %v1, align 4, !tbaa !6
  %101 = bitcast i32* %v2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #6
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %102 = bitcast i32* %v1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #6
  %103 = bitcast i32* %v0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #6
  %104 = bitcast %"class.btConvexHullComputer::Edge"** %edge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #6
  %105 = bitcast %"class.btConvexHullComputer::Edge"** %firstEdge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #6
  %106 = bitcast i32* %face to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #6
  br label %for.inc70

for.inc70:                                        ; preds = %while.end
  %107 = load i32, i32* %i50, align 4, !tbaa !6
  %inc71 = add nsw i32 %107, 1
  store i32 %inc71, i32* %i50, align 4, !tbaa !6
  br label %for.cond51

for.end72:                                        ; preds = %for.cond.cleanup
  %108 = bitcast float* %shrinkClamp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #6
  %109 = bitcast float* %shrink to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #6
  %110 = bitcast i32* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #6
  %111 = bitcast i32* %stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #6
  %call73 = call %class.btConvexHullComputer* @_ZN20btConvexHullComputerD2Ev(%class.btConvexHullComputer* %computer) #6
  %112 = bitcast %class.btConvexHullComputer* %computer to i8*
  call void @llvm.lifetime.end.p0i8(i64 60, i8* %112) #6
  %call74 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.8* %vertices) #6
  %113 = bitcast %class.btAlignedObjectArray.8* %vertices to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %113) #6
  %114 = bitcast %class.btVector3* %color to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %114) #6
  br label %if.end

if.end:                                           ; preds = %for.end72, %for.body
  br label %for.inc75

for.inc75:                                        ; preds = %if.end
  %115 = load i32, i32* %i, align 4, !tbaa !6
  %inc76 = add nsw i32 %115, 1
  store i32 %inc76, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end77:                                        ; preds = %for.cond
  br label %if.end461

if.else:                                          ; preds = %entry
  %116 = load i32, i32* %drawflags.addr, align 4, !tbaa !6
  %and78 = and i32 %116, 1
  %cmp79 = icmp ne i32 0, %and78
  br i1 %cmp79, label %if.then80, label %if.end152

if.then80:                                        ; preds = %if.else
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond81

for.cond81:                                       ; preds = %for.inc149, %if.then80
  %117 = load i32, i32* %i, align 4, !tbaa !6
  %118 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_nodes82 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %118, i32 0, i32 9
  %call83 = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4NodeEE4sizeEv(%class.btAlignedObjectArray.25* %m_nodes82)
  %cmp84 = icmp slt i32 %117, %call83
  br i1 %cmp84, label %for.body85, label %for.end151

for.body85:                                       ; preds = %for.cond81
  %119 = bitcast %"struct.btSoftBody::Node"** %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %119) #6
  %120 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_nodes86 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %120, i32 0, i32 9
  %121 = load i32, i32* %i, align 4, !tbaa !6
  %call87 = call nonnull align 4 dereferenceable(101) %"struct.btSoftBody::Node"* @_ZN20btAlignedObjectArrayIN10btSoftBody4NodeEEixEi(%class.btAlignedObjectArray.25* %m_nodes86, i32 %121)
  store %"struct.btSoftBody::Node"* %call87, %"struct.btSoftBody::Node"** %n, align 4, !tbaa !2
  %122 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n, align 4, !tbaa !2
  %123 = bitcast %"struct.btSoftBody::Node"* %122 to %"struct.btSoftBody::Feature"*
  %m_material = getelementptr inbounds %"struct.btSoftBody::Feature", %"struct.btSoftBody::Feature"* %123, i32 0, i32 1
  %124 = load %"struct.btSoftBody::Material"*, %"struct.btSoftBody::Material"** %m_material, align 4, !tbaa !27
  %m_flags = getelementptr inbounds %"struct.btSoftBody::Material", %"struct.btSoftBody::Material"* %124, i32 0, i32 4
  %125 = load i32, i32* %m_flags, align 4, !tbaa !29
  %and88 = and i32 %125, 1
  %cmp89 = icmp eq i32 0, %and88
  br i1 %cmp89, label %if.then90, label %if.end91

if.then90:                                        ; preds = %for.body85
  store i32 15, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end91:                                         ; preds = %for.body85
  %126 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %127 = bitcast %class.btVector3* %ref.tmp92 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %127) #6
  %128 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n, align 4, !tbaa !2
  %m_x93 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %128, i32 0, i32 1
  %129 = bitcast %class.btVector3* %ref.tmp94 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %129) #6
  %130 = bitcast float* %ref.tmp95 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %130) #6
  store float 0.000000e+00, float* %ref.tmp95, align 4, !tbaa !8
  %131 = bitcast float* %ref.tmp96 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %131) #6
  store float 0.000000e+00, float* %ref.tmp96, align 4, !tbaa !8
  %call97 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp94, float* nonnull align 4 dereferenceable(4) %scl, float* nonnull align 4 dereferenceable(4) %ref.tmp95, float* nonnull align 4 dereferenceable(4) %ref.tmp96)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp92, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x93, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp94)
  %132 = bitcast %class.btVector3* %ref.tmp98 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %132) #6
  %133 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n, align 4, !tbaa !2
  %m_x99 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %133, i32 0, i32 1
  %134 = bitcast %class.btVector3* %ref.tmp100 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %134) #6
  %135 = bitcast float* %ref.tmp101 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %135) #6
  store float 0.000000e+00, float* %ref.tmp101, align 4, !tbaa !8
  %136 = bitcast float* %ref.tmp102 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %136) #6
  store float 0.000000e+00, float* %ref.tmp102, align 4, !tbaa !8
  %call103 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp100, float* nonnull align 4 dereferenceable(4) %scl, float* nonnull align 4 dereferenceable(4) %ref.tmp101, float* nonnull align 4 dereferenceable(4) %ref.tmp102)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp98, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x99, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp100)
  %137 = bitcast %class.btVector3* %ref.tmp104 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %137) #6
  %138 = bitcast float* %ref.tmp105 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %138) #6
  store float 1.000000e+00, float* %ref.tmp105, align 4, !tbaa !8
  %139 = bitcast float* %ref.tmp106 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %139) #6
  store float 0.000000e+00, float* %ref.tmp106, align 4, !tbaa !8
  %140 = bitcast float* %ref.tmp107 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %140) #6
  store float 0.000000e+00, float* %ref.tmp107, align 4, !tbaa !8
  %call108 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp104, float* nonnull align 4 dereferenceable(4) %ref.tmp105, float* nonnull align 4 dereferenceable(4) %ref.tmp106, float* nonnull align 4 dereferenceable(4) %ref.tmp107)
  %141 = bitcast %class.btIDebugDraw* %126 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable109 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %141, align 4, !tbaa !25
  %vfn110 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable109, i64 2
  %142 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn110, align 4
  call void %142(%class.btIDebugDraw* %126, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp92, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp98, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp104)
  %143 = bitcast float* %ref.tmp107 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #6
  %144 = bitcast float* %ref.tmp106 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #6
  %145 = bitcast float* %ref.tmp105 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #6
  %146 = bitcast %class.btVector3* %ref.tmp104 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %146) #6
  %147 = bitcast float* %ref.tmp102 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #6
  %148 = bitcast float* %ref.tmp101 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #6
  %149 = bitcast %class.btVector3* %ref.tmp100 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %149) #6
  %150 = bitcast %class.btVector3* %ref.tmp98 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %150) #6
  %151 = bitcast float* %ref.tmp96 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #6
  %152 = bitcast float* %ref.tmp95 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %152) #6
  %153 = bitcast %class.btVector3* %ref.tmp94 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %153) #6
  %154 = bitcast %class.btVector3* %ref.tmp92 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %154) #6
  %155 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %156 = bitcast %class.btVector3* %ref.tmp111 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %156) #6
  %157 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n, align 4, !tbaa !2
  %m_x112 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %157, i32 0, i32 1
  %158 = bitcast %class.btVector3* %ref.tmp113 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %158) #6
  %159 = bitcast float* %ref.tmp114 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %159) #6
  store float 0.000000e+00, float* %ref.tmp114, align 4, !tbaa !8
  %160 = bitcast float* %ref.tmp115 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %160) #6
  store float 0.000000e+00, float* %ref.tmp115, align 4, !tbaa !8
  %call116 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp113, float* nonnull align 4 dereferenceable(4) %ref.tmp114, float* nonnull align 4 dereferenceable(4) %scl, float* nonnull align 4 dereferenceable(4) %ref.tmp115)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp111, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x112, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp113)
  %161 = bitcast %class.btVector3* %ref.tmp117 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %161) #6
  %162 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n, align 4, !tbaa !2
  %m_x118 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %162, i32 0, i32 1
  %163 = bitcast %class.btVector3* %ref.tmp119 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %163) #6
  %164 = bitcast float* %ref.tmp120 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %164) #6
  store float 0.000000e+00, float* %ref.tmp120, align 4, !tbaa !8
  %165 = bitcast float* %ref.tmp121 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %165) #6
  store float 0.000000e+00, float* %ref.tmp121, align 4, !tbaa !8
  %call122 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp119, float* nonnull align 4 dereferenceable(4) %ref.tmp120, float* nonnull align 4 dereferenceable(4) %scl, float* nonnull align 4 dereferenceable(4) %ref.tmp121)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp117, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x118, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp119)
  %166 = bitcast %class.btVector3* %ref.tmp123 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %166) #6
  %167 = bitcast float* %ref.tmp124 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %167) #6
  store float 0.000000e+00, float* %ref.tmp124, align 4, !tbaa !8
  %168 = bitcast float* %ref.tmp125 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %168) #6
  store float 1.000000e+00, float* %ref.tmp125, align 4, !tbaa !8
  %169 = bitcast float* %ref.tmp126 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %169) #6
  store float 0.000000e+00, float* %ref.tmp126, align 4, !tbaa !8
  %call127 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp123, float* nonnull align 4 dereferenceable(4) %ref.tmp124, float* nonnull align 4 dereferenceable(4) %ref.tmp125, float* nonnull align 4 dereferenceable(4) %ref.tmp126)
  %170 = bitcast %class.btIDebugDraw* %155 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable128 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %170, align 4, !tbaa !25
  %vfn129 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable128, i64 2
  %171 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn129, align 4
  call void %171(%class.btIDebugDraw* %155, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp111, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp117, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp123)
  %172 = bitcast float* %ref.tmp126 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %172) #6
  %173 = bitcast float* %ref.tmp125 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %173) #6
  %174 = bitcast float* %ref.tmp124 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %174) #6
  %175 = bitcast %class.btVector3* %ref.tmp123 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %175) #6
  %176 = bitcast float* %ref.tmp121 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %176) #6
  %177 = bitcast float* %ref.tmp120 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %177) #6
  %178 = bitcast %class.btVector3* %ref.tmp119 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %178) #6
  %179 = bitcast %class.btVector3* %ref.tmp117 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %179) #6
  %180 = bitcast float* %ref.tmp115 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %180) #6
  %181 = bitcast float* %ref.tmp114 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %181) #6
  %182 = bitcast %class.btVector3* %ref.tmp113 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %182) #6
  %183 = bitcast %class.btVector3* %ref.tmp111 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %183) #6
  %184 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %185 = bitcast %class.btVector3* %ref.tmp130 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %185) #6
  %186 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n, align 4, !tbaa !2
  %m_x131 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %186, i32 0, i32 1
  %187 = bitcast %class.btVector3* %ref.tmp132 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %187) #6
  %188 = bitcast float* %ref.tmp133 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %188) #6
  store float 0.000000e+00, float* %ref.tmp133, align 4, !tbaa !8
  %189 = bitcast float* %ref.tmp134 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %189) #6
  store float 0.000000e+00, float* %ref.tmp134, align 4, !tbaa !8
  %call135 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp132, float* nonnull align 4 dereferenceable(4) %ref.tmp133, float* nonnull align 4 dereferenceable(4) %ref.tmp134, float* nonnull align 4 dereferenceable(4) %scl)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp130, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x131, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp132)
  %190 = bitcast %class.btVector3* %ref.tmp136 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %190) #6
  %191 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n, align 4, !tbaa !2
  %m_x137 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %191, i32 0, i32 1
  %192 = bitcast %class.btVector3* %ref.tmp138 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %192) #6
  %193 = bitcast float* %ref.tmp139 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %193) #6
  store float 0.000000e+00, float* %ref.tmp139, align 4, !tbaa !8
  %194 = bitcast float* %ref.tmp140 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %194) #6
  store float 0.000000e+00, float* %ref.tmp140, align 4, !tbaa !8
  %call141 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp138, float* nonnull align 4 dereferenceable(4) %ref.tmp139, float* nonnull align 4 dereferenceable(4) %ref.tmp140, float* nonnull align 4 dereferenceable(4) %scl)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp136, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x137, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp138)
  %195 = bitcast %class.btVector3* %ref.tmp142 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %195) #6
  %196 = bitcast float* %ref.tmp143 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %196) #6
  store float 0.000000e+00, float* %ref.tmp143, align 4, !tbaa !8
  %197 = bitcast float* %ref.tmp144 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %197) #6
  store float 0.000000e+00, float* %ref.tmp144, align 4, !tbaa !8
  %198 = bitcast float* %ref.tmp145 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %198) #6
  store float 1.000000e+00, float* %ref.tmp145, align 4, !tbaa !8
  %call146 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp142, float* nonnull align 4 dereferenceable(4) %ref.tmp143, float* nonnull align 4 dereferenceable(4) %ref.tmp144, float* nonnull align 4 dereferenceable(4) %ref.tmp145)
  %199 = bitcast %class.btIDebugDraw* %184 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable147 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %199, align 4, !tbaa !25
  %vfn148 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable147, i64 2
  %200 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn148, align 4
  call void %200(%class.btIDebugDraw* %184, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp130, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp136, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp142)
  %201 = bitcast float* %ref.tmp145 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %201) #6
  %202 = bitcast float* %ref.tmp144 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %202) #6
  %203 = bitcast float* %ref.tmp143 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %203) #6
  %204 = bitcast %class.btVector3* %ref.tmp142 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %204) #6
  %205 = bitcast float* %ref.tmp140 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %205) #6
  %206 = bitcast float* %ref.tmp139 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %206) #6
  %207 = bitcast %class.btVector3* %ref.tmp138 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %207) #6
  %208 = bitcast %class.btVector3* %ref.tmp136 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %208) #6
  %209 = bitcast float* %ref.tmp134 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %209) #6
  %210 = bitcast float* %ref.tmp133 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %210) #6
  %211 = bitcast %class.btVector3* %ref.tmp132 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %211) #6
  %212 = bitcast %class.btVector3* %ref.tmp130 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %212) #6
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end91, %if.then90
  %213 = bitcast %"struct.btSoftBody::Node"** %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %213) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 15, label %for.inc149
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc149

for.inc149:                                       ; preds = %cleanup.cont, %cleanup
  %214 = load i32, i32* %i, align 4, !tbaa !6
  %inc150 = add nsw i32 %214, 1
  store i32 %inc150, i32* %i, align 4, !tbaa !6
  br label %for.cond81

for.end151:                                       ; preds = %for.cond81
  br label %if.end152

if.end152:                                        ; preds = %for.end151, %if.else
  %215 = load i32, i32* %drawflags.addr, align 4, !tbaa !6
  %and153 = and i32 %215, 2
  %cmp154 = icmp ne i32 0, %and153
  br i1 %cmp154, label %if.then155, label %if.end180

if.then155:                                       ; preds = %if.end152
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond156

for.cond156:                                      ; preds = %for.inc177, %if.then155
  %216 = load i32, i32* %i, align 4, !tbaa !6
  %217 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_links = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %217, i32 0, i32 10
  %call157 = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4LinkEE4sizeEv(%class.btAlignedObjectArray.29* %m_links)
  %cmp158 = icmp slt i32 %216, %call157
  br i1 %cmp158, label %for.body159, label %for.end179

for.body159:                                      ; preds = %for.cond156
  %218 = bitcast %"struct.btSoftBody::Link"** %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %218) #6
  %219 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_links160 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %219, i32 0, i32 10
  %220 = load i32, i32* %i, align 4, !tbaa !6
  %call161 = call nonnull align 4 dereferenceable(52) %"struct.btSoftBody::Link"* @_ZN20btAlignedObjectArrayIN10btSoftBody4LinkEEixEi(%class.btAlignedObjectArray.29* %m_links160, i32 %220)
  store %"struct.btSoftBody::Link"* %call161, %"struct.btSoftBody::Link"** %l, align 4, !tbaa !2
  %221 = load %"struct.btSoftBody::Link"*, %"struct.btSoftBody::Link"** %l, align 4, !tbaa !2
  %222 = bitcast %"struct.btSoftBody::Link"* %221 to %"struct.btSoftBody::Feature"*
  %m_material162 = getelementptr inbounds %"struct.btSoftBody::Feature", %"struct.btSoftBody::Feature"* %222, i32 0, i32 1
  %223 = load %"struct.btSoftBody::Material"*, %"struct.btSoftBody::Material"** %m_material162, align 4, !tbaa !27
  %m_flags163 = getelementptr inbounds %"struct.btSoftBody::Material", %"struct.btSoftBody::Material"* %223, i32 0, i32 4
  %224 = load i32, i32* %m_flags163, align 4, !tbaa !29
  %and164 = and i32 %224, 1
  %cmp165 = icmp eq i32 0, %and164
  br i1 %cmp165, label %if.then166, label %if.end167

if.then166:                                       ; preds = %for.body159
  store i32 18, i32* %cleanup.dest.slot, align 4
  br label %cleanup174

if.end167:                                        ; preds = %for.body159
  %225 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %226 = load %"struct.btSoftBody::Link"*, %"struct.btSoftBody::Link"** %l, align 4, !tbaa !2
  %m_n = getelementptr inbounds %"struct.btSoftBody::Link", %"struct.btSoftBody::Link"* %226, i32 0, i32 1
  %arrayidx = getelementptr inbounds [2 x %"struct.btSoftBody::Node"*], [2 x %"struct.btSoftBody::Node"*]* %m_n, i32 0, i32 0
  %227 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %arrayidx, align 4, !tbaa !2
  %m_x168 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %227, i32 0, i32 1
  %228 = load %"struct.btSoftBody::Link"*, %"struct.btSoftBody::Link"** %l, align 4, !tbaa !2
  %m_n169 = getelementptr inbounds %"struct.btSoftBody::Link", %"struct.btSoftBody::Link"* %228, i32 0, i32 1
  %arrayidx170 = getelementptr inbounds [2 x %"struct.btSoftBody::Node"*], [2 x %"struct.btSoftBody::Node"*]* %m_n169, i32 0, i32 1
  %229 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %arrayidx170, align 4, !tbaa !2
  %m_x171 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %229, i32 0, i32 1
  %230 = bitcast %class.btIDebugDraw* %225 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable172 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %230, align 4, !tbaa !25
  %vfn173 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable172, i64 2
  %231 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn173, align 4
  call void %231(%class.btIDebugDraw* %225, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x168, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x171, %class.btVector3* nonnull align 4 dereferenceable(16) %lcolor)
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup174

cleanup174:                                       ; preds = %if.end167, %if.then166
  %232 = bitcast %"struct.btSoftBody::Link"** %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %232) #6
  %cleanup.dest175 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest175, label %unreachable [
    i32 0, label %cleanup.cont176
    i32 18, label %for.inc177
  ]

cleanup.cont176:                                  ; preds = %cleanup174
  br label %for.inc177

for.inc177:                                       ; preds = %cleanup.cont176, %cleanup174
  %233 = load i32, i32* %i, align 4, !tbaa !6
  %inc178 = add nsw i32 %233, 1
  store i32 %inc178, i32* %i, align 4, !tbaa !6
  br label %for.cond156

for.end179:                                       ; preds = %for.cond156
  br label %if.end180

if.end180:                                        ; preds = %for.end179, %if.end152
  %234 = load i32, i32* %drawflags.addr, align 4, !tbaa !6
  %and181 = and i32 %234, 16
  %cmp182 = icmp ne i32 0, %and181
  br i1 %cmp182, label %if.then183, label %if.end217

if.then183:                                       ; preds = %if.end180
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond184

for.cond184:                                      ; preds = %for.inc214, %if.then183
  %235 = load i32, i32* %i, align 4, !tbaa !6
  %236 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_nodes185 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %236, i32 0, i32 9
  %call186 = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4NodeEE4sizeEv(%class.btAlignedObjectArray.25* %m_nodes185)
  %cmp187 = icmp slt i32 %235, %call186
  br i1 %cmp187, label %for.body188, label %for.end216

for.body188:                                      ; preds = %for.cond184
  %237 = bitcast %"struct.btSoftBody::Node"** %n189 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %237) #6
  %238 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_nodes190 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %238, i32 0, i32 9
  %239 = load i32, i32* %i, align 4, !tbaa !6
  %call191 = call nonnull align 4 dereferenceable(101) %"struct.btSoftBody::Node"* @_ZN20btAlignedObjectArrayIN10btSoftBody4NodeEEixEi(%class.btAlignedObjectArray.25* %m_nodes190, i32 %239)
  store %"struct.btSoftBody::Node"* %call191, %"struct.btSoftBody::Node"** %n189, align 4, !tbaa !2
  %240 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n189, align 4, !tbaa !2
  %241 = bitcast %"struct.btSoftBody::Node"* %240 to %"struct.btSoftBody::Feature"*
  %m_material192 = getelementptr inbounds %"struct.btSoftBody::Feature", %"struct.btSoftBody::Feature"* %241, i32 0, i32 1
  %242 = load %"struct.btSoftBody::Material"*, %"struct.btSoftBody::Material"** %m_material192, align 4, !tbaa !27
  %m_flags193 = getelementptr inbounds %"struct.btSoftBody::Material", %"struct.btSoftBody::Material"* %242, i32 0, i32 4
  %243 = load i32, i32* %m_flags193, align 4, !tbaa !29
  %and194 = and i32 %243, 1
  %cmp195 = icmp eq i32 0, %and194
  br i1 %cmp195, label %if.then196, label %if.end197

if.then196:                                       ; preds = %for.body188
  store i32 21, i32* %cleanup.dest.slot, align 4
  br label %cleanup211

if.end197:                                        ; preds = %for.body188
  %244 = bitcast %class.btVector3* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %244) #6
  %245 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n189, align 4, !tbaa !2
  %m_n198 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %245, i32 0, i32 5
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %d, %class.btVector3* nonnull align 4 dereferenceable(16) %m_n198, float* nonnull align 4 dereferenceable(4) %nscl)
  %246 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %247 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n189, align 4, !tbaa !2
  %m_x199 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %247, i32 0, i32 1
  %248 = bitcast %class.btVector3* %ref.tmp200 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %248) #6
  %249 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n189, align 4, !tbaa !2
  %m_x201 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %249, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp200, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x201, %class.btVector3* nonnull align 4 dereferenceable(16) %d)
  %250 = bitcast %class.btIDebugDraw* %246 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable202 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %250, align 4, !tbaa !25
  %vfn203 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable202, i64 2
  %251 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn203, align 4
  call void %251(%class.btIDebugDraw* %246, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x199, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp200, %class.btVector3* nonnull align 4 dereferenceable(16) %ncolor)
  %252 = bitcast %class.btVector3* %ref.tmp200 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %252) #6
  %253 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %254 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n189, align 4, !tbaa !2
  %m_x204 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %254, i32 0, i32 1
  %255 = bitcast %class.btVector3* %ref.tmp205 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %255) #6
  %256 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n189, align 4, !tbaa !2
  %m_x206 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %256, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp205, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x206, %class.btVector3* nonnull align 4 dereferenceable(16) %d)
  %257 = bitcast %class.btVector3* %ref.tmp207 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %257) #6
  %258 = bitcast float* %ref.tmp208 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %258) #6
  store float 5.000000e-01, float* %ref.tmp208, align 4, !tbaa !8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp207, %class.btVector3* nonnull align 4 dereferenceable(16) %ncolor, float* nonnull align 4 dereferenceable(4) %ref.tmp208)
  %259 = bitcast %class.btIDebugDraw* %253 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable209 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %259, align 4, !tbaa !25
  %vfn210 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable209, i64 2
  %260 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn210, align 4
  call void %260(%class.btIDebugDraw* %253, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x204, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp205, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp207)
  %261 = bitcast float* %ref.tmp208 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %261) #6
  %262 = bitcast %class.btVector3* %ref.tmp207 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %262) #6
  %263 = bitcast %class.btVector3* %ref.tmp205 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %263) #6
  %264 = bitcast %class.btVector3* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %264) #6
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup211

cleanup211:                                       ; preds = %if.end197, %if.then196
  %265 = bitcast %"struct.btSoftBody::Node"** %n189 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %265) #6
  %cleanup.dest212 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest212, label %unreachable [
    i32 0, label %cleanup.cont213
    i32 21, label %for.inc214
  ]

cleanup.cont213:                                  ; preds = %cleanup211
  br label %for.inc214

for.inc214:                                       ; preds = %cleanup.cont213, %cleanup211
  %266 = load i32, i32* %i, align 4, !tbaa !6
  %inc215 = add nsw i32 %266, 1
  store i32 %inc215, i32* %i, align 4, !tbaa !6
  br label %for.cond184

for.end216:                                       ; preds = %for.cond184
  br label %if.end217

if.end217:                                        ; preds = %for.end216, %if.end180
  %267 = load i32, i32* %drawflags.addr, align 4, !tbaa !6
  %and218 = and i32 %267, 32
  %cmp219 = icmp ne i32 0, %and218
  br i1 %cmp219, label %if.then220, label %if.end287

if.then220:                                       ; preds = %if.end217
  %268 = load atomic i8, i8* bitcast (i32* @_ZGVZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis to i8*) acquire, align 4
  %269 = and i8 %268, 1
  %guard.uninitialized = icmp eq i8 %269, 0
  br i1 %guard.uninitialized, label %init.check, label %init.end, !prof !31

init.check:                                       ; preds = %if.then220
  %270 = call i32 @__cxa_guard_acquire(i32* @_ZGVZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis) #6
  %tobool221 = icmp ne i32 %270, 0
  br i1 %tobool221, label %init, label %init.end

init:                                             ; preds = %init.check
  %271 = bitcast float* %ref.tmp222 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %271) #6
  store float 1.000000e+00, float* %ref.tmp222, align 4, !tbaa !8
  %272 = bitcast float* %ref.tmp223 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %272) #6
  store float 0.000000e+00, float* %ref.tmp223, align 4, !tbaa !8
  %273 = bitcast float* %ref.tmp224 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %273) #6
  store float 0.000000e+00, float* %ref.tmp224, align 4, !tbaa !8
  %call225 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([3 x %class.btVector3], [3 x %class.btVector3]* @_ZZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis, i32 0, i32 0), float* nonnull align 4 dereferenceable(4) %ref.tmp222, float* nonnull align 4 dereferenceable(4) %ref.tmp223, float* nonnull align 4 dereferenceable(4) %ref.tmp224)
  %274 = bitcast float* %ref.tmp226 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %274) #6
  store float 0.000000e+00, float* %ref.tmp226, align 4, !tbaa !8
  %275 = bitcast float* %ref.tmp227 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %275) #6
  store float 1.000000e+00, float* %ref.tmp227, align 4, !tbaa !8
  %276 = bitcast float* %ref.tmp228 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %276) #6
  store float 0.000000e+00, float* %ref.tmp228, align 4, !tbaa !8
  %call229 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([3 x %class.btVector3], [3 x %class.btVector3]* @_ZZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis, i32 0, i32 1), float* nonnull align 4 dereferenceable(4) %ref.tmp226, float* nonnull align 4 dereferenceable(4) %ref.tmp227, float* nonnull align 4 dereferenceable(4) %ref.tmp228)
  %277 = bitcast float* %ref.tmp230 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %277) #6
  store float 0.000000e+00, float* %ref.tmp230, align 4, !tbaa !8
  %278 = bitcast float* %ref.tmp231 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %278) #6
  store float 0.000000e+00, float* %ref.tmp231, align 4, !tbaa !8
  %279 = bitcast float* %ref.tmp232 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %279) #6
  store float 1.000000e+00, float* %ref.tmp232, align 4, !tbaa !8
  %call233 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([3 x %class.btVector3], [3 x %class.btVector3]* @_ZZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis, i32 0, i32 2), float* nonnull align 4 dereferenceable(4) %ref.tmp230, float* nonnull align 4 dereferenceable(4) %ref.tmp231, float* nonnull align 4 dereferenceable(4) %ref.tmp232)
  %280 = bitcast float* %ref.tmp232 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %280) #6
  %281 = bitcast float* %ref.tmp231 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %281) #6
  %282 = bitcast float* %ref.tmp230 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %282) #6
  %283 = bitcast float* %ref.tmp228 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %283) #6
  %284 = bitcast float* %ref.tmp227 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %284) #6
  %285 = bitcast float* %ref.tmp226 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %285) #6
  %286 = bitcast float* %ref.tmp224 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %286) #6
  %287 = bitcast float* %ref.tmp223 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %287) #6
  %288 = bitcast float* %ref.tmp222 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %288) #6
  %289 = call {}* @llvm.invariant.start.p0i8(i64 48, i8* bitcast ([3 x %class.btVector3]* @_ZZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis to i8*))
  call void @__cxa_guard_release(i32* @_ZGVZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis) #6
  br label %init.end

init.end:                                         ; preds = %init, %init.check, %if.then220
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond234

for.cond234:                                      ; preds = %for.inc284, %init.end
  %290 = load i32, i32* %i, align 4, !tbaa !6
  %291 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_rcontacts = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %291, i32 0, i32 14
  %call235 = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody8RContactEE4sizeEv(%class.btAlignedObjectArray.49* %m_rcontacts)
  %cmp236 = icmp slt i32 %290, %call235
  br i1 %cmp236, label %for.body237, label %for.end286

for.body237:                                      ; preds = %for.cond234
  %292 = bitcast %"struct.btSoftBody::RContact"** %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %292) #6
  %293 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_rcontacts238 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %293, i32 0, i32 14
  %294 = load i32, i32* %i, align 4, !tbaa !6
  %call239 = call nonnull align 4 dereferenceable(104) %"struct.btSoftBody::RContact"* @_ZN20btAlignedObjectArrayIN10btSoftBody8RContactEEixEi(%class.btAlignedObjectArray.49* %m_rcontacts238, i32 %294)
  store %"struct.btSoftBody::RContact"* %call239, %"struct.btSoftBody::RContact"** %c, align 4, !tbaa !2
  %295 = bitcast %class.btVector3* %o to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %295) #6
  %296 = load %"struct.btSoftBody::RContact"*, %"struct.btSoftBody::RContact"** %c, align 4, !tbaa !2
  %m_node = getelementptr inbounds %"struct.btSoftBody::RContact", %"struct.btSoftBody::RContact"* %296, i32 0, i32 1
  %297 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %m_node, align 4, !tbaa !32
  %m_x240 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %297, i32 0, i32 1
  %298 = bitcast %class.btVector3* %ref.tmp241 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %298) #6
  %299 = load %"struct.btSoftBody::RContact"*, %"struct.btSoftBody::RContact"** %c, align 4, !tbaa !2
  %m_cti = getelementptr inbounds %"struct.btSoftBody::RContact", %"struct.btSoftBody::RContact"* %299, i32 0, i32 0
  %m_normal = getelementptr inbounds %"struct.btSoftBody::sCti", %"struct.btSoftBody::sCti"* %m_cti, i32 0, i32 1
  %300 = bitcast float* %ref.tmp242 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %300) #6
  %301 = load %"struct.btSoftBody::RContact"*, %"struct.btSoftBody::RContact"** %c, align 4, !tbaa !2
  %m_node243 = getelementptr inbounds %"struct.btSoftBody::RContact", %"struct.btSoftBody::RContact"* %301, i32 0, i32 1
  %302 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %m_node243, align 4, !tbaa !32
  %m_x244 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %302, i32 0, i32 1
  %303 = load %"struct.btSoftBody::RContact"*, %"struct.btSoftBody::RContact"** %c, align 4, !tbaa !2
  %m_cti245 = getelementptr inbounds %"struct.btSoftBody::RContact", %"struct.btSoftBody::RContact"* %303, i32 0, i32 0
  %m_normal246 = getelementptr inbounds %"struct.btSoftBody::sCti", %"struct.btSoftBody::sCti"* %m_cti245, i32 0, i32 1
  %call247 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_x244, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normal246)
  %304 = load %"struct.btSoftBody::RContact"*, %"struct.btSoftBody::RContact"** %c, align 4, !tbaa !2
  %m_cti248 = getelementptr inbounds %"struct.btSoftBody::RContact", %"struct.btSoftBody::RContact"* %304, i32 0, i32 0
  %m_offset = getelementptr inbounds %"struct.btSoftBody::sCti", %"struct.btSoftBody::sCti"* %m_cti248, i32 0, i32 2
  %305 = load float, float* %m_offset, align 4, !tbaa !35
  %add = fadd float %call247, %305
  store float %add, float* %ref.tmp242, align 4, !tbaa !8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp241, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normal, float* nonnull align 4 dereferenceable(4) %ref.tmp242)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %o, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x240, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp241)
  %306 = bitcast float* %ref.tmp242 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %306) #6
  %307 = bitcast %class.btVector3* %ref.tmp241 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %307) #6
  %308 = bitcast %class.btVector3* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %308) #6
  %309 = bitcast %class.btVector3* %ref.tmp249 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %309) #6
  %310 = load %"struct.btSoftBody::RContact"*, %"struct.btSoftBody::RContact"** %c, align 4, !tbaa !2
  %m_cti250 = getelementptr inbounds %"struct.btSoftBody::RContact", %"struct.btSoftBody::RContact"* %310, i32 0, i32 0
  %m_normal251 = getelementptr inbounds %"struct.btSoftBody::sCti", %"struct.btSoftBody::sCti"* %m_cti250, i32 0, i32 1
  %311 = load %"struct.btSoftBody::RContact"*, %"struct.btSoftBody::RContact"** %c, align 4, !tbaa !2
  %m_cti252 = getelementptr inbounds %"struct.btSoftBody::RContact", %"struct.btSoftBody::RContact"* %311, i32 0, i32 0
  %m_normal253 = getelementptr inbounds %"struct.btSoftBody::sCti", %"struct.btSoftBody::sCti"* %m_cti252, i32 0, i32 1
  %call254 = call i32 @_ZNK9btVector37minAxisEv(%class.btVector3* %m_normal253)
  %arrayidx255 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* @_ZZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis, i32 0, i32 %call254
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp249, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normal251, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx255)
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %x, %class.btVector3* %ref.tmp249)
  %312 = bitcast %class.btVector3* %ref.tmp249 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %312) #6
  %313 = bitcast %class.btVector3* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %313) #6
  %314 = bitcast %class.btVector3* %ref.tmp256 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %314) #6
  %315 = load %"struct.btSoftBody::RContact"*, %"struct.btSoftBody::RContact"** %c, align 4, !tbaa !2
  %m_cti257 = getelementptr inbounds %"struct.btSoftBody::RContact", %"struct.btSoftBody::RContact"* %315, i32 0, i32 0
  %m_normal258 = getelementptr inbounds %"struct.btSoftBody::sCti", %"struct.btSoftBody::sCti"* %m_cti257, i32 0, i32 1
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp256, %class.btVector3* nonnull align 4 dereferenceable(16) %x, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normal258)
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %y, %class.btVector3* %ref.tmp256)
  %316 = bitcast %class.btVector3* %ref.tmp256 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %316) #6
  %317 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %318 = bitcast %class.btVector3* %ref.tmp259 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %318) #6
  %319 = bitcast %class.btVector3* %ref.tmp260 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %319) #6
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp260, %class.btVector3* nonnull align 4 dereferenceable(16) %x, float* nonnull align 4 dereferenceable(4) %nscl)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp259, %class.btVector3* nonnull align 4 dereferenceable(16) %o, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp260)
  %320 = bitcast %class.btVector3* %ref.tmp261 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %320) #6
  %321 = bitcast %class.btVector3* %ref.tmp262 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %321) #6
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp262, %class.btVector3* nonnull align 4 dereferenceable(16) %x, float* nonnull align 4 dereferenceable(4) %nscl)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp261, %class.btVector3* nonnull align 4 dereferenceable(16) %o, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp262)
  %322 = bitcast %class.btIDebugDraw* %317 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable263 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %322, align 4, !tbaa !25
  %vfn264 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable263, i64 2
  %323 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn264, align 4
  call void %323(%class.btIDebugDraw* %317, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp259, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp261, %class.btVector3* nonnull align 4 dereferenceable(16) %ccolor)
  %324 = bitcast %class.btVector3* %ref.tmp262 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %324) #6
  %325 = bitcast %class.btVector3* %ref.tmp261 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %325) #6
  %326 = bitcast %class.btVector3* %ref.tmp260 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %326) #6
  %327 = bitcast %class.btVector3* %ref.tmp259 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %327) #6
  %328 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %329 = bitcast %class.btVector3* %ref.tmp265 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %329) #6
  %330 = bitcast %class.btVector3* %ref.tmp266 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %330) #6
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp266, %class.btVector3* nonnull align 4 dereferenceable(16) %y, float* nonnull align 4 dereferenceable(4) %nscl)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp265, %class.btVector3* nonnull align 4 dereferenceable(16) %o, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp266)
  %331 = bitcast %class.btVector3* %ref.tmp267 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %331) #6
  %332 = bitcast %class.btVector3* %ref.tmp268 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %332) #6
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp268, %class.btVector3* nonnull align 4 dereferenceable(16) %y, float* nonnull align 4 dereferenceable(4) %nscl)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp267, %class.btVector3* nonnull align 4 dereferenceable(16) %o, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp268)
  %333 = bitcast %class.btIDebugDraw* %328 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable269 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %333, align 4, !tbaa !25
  %vfn270 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable269, i64 2
  %334 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn270, align 4
  call void %334(%class.btIDebugDraw* %328, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp265, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp267, %class.btVector3* nonnull align 4 dereferenceable(16) %ccolor)
  %335 = bitcast %class.btVector3* %ref.tmp268 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %335) #6
  %336 = bitcast %class.btVector3* %ref.tmp267 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %336) #6
  %337 = bitcast %class.btVector3* %ref.tmp266 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %337) #6
  %338 = bitcast %class.btVector3* %ref.tmp265 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %338) #6
  %339 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %340 = bitcast %class.btVector3* %ref.tmp271 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %340) #6
  %341 = bitcast %class.btVector3* %ref.tmp272 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %341) #6
  %342 = bitcast %class.btVector3* %ref.tmp273 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %342) #6
  %343 = load %"struct.btSoftBody::RContact"*, %"struct.btSoftBody::RContact"** %c, align 4, !tbaa !2
  %m_cti274 = getelementptr inbounds %"struct.btSoftBody::RContact", %"struct.btSoftBody::RContact"* %343, i32 0, i32 0
  %m_normal275 = getelementptr inbounds %"struct.btSoftBody::sCti", %"struct.btSoftBody::sCti"* %m_cti274, i32 0, i32 1
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp273, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normal275, float* nonnull align 4 dereferenceable(4) %nscl)
  %344 = bitcast float* %ref.tmp276 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %344) #6
  store float 3.000000e+00, float* %ref.tmp276, align 4, !tbaa !8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp272, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp273, float* nonnull align 4 dereferenceable(4) %ref.tmp276)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp271, %class.btVector3* nonnull align 4 dereferenceable(16) %o, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp272)
  %345 = bitcast %class.btVector3* %ref.tmp277 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %345) #6
  %346 = bitcast float* %ref.tmp278 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %346) #6
  store float 1.000000e+00, float* %ref.tmp278, align 4, !tbaa !8
  %347 = bitcast float* %ref.tmp279 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %347) #6
  store float 1.000000e+00, float* %ref.tmp279, align 4, !tbaa !8
  %348 = bitcast float* %ref.tmp280 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %348) #6
  store float 0.000000e+00, float* %ref.tmp280, align 4, !tbaa !8
  %call281 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp277, float* nonnull align 4 dereferenceable(4) %ref.tmp278, float* nonnull align 4 dereferenceable(4) %ref.tmp279, float* nonnull align 4 dereferenceable(4) %ref.tmp280)
  %349 = bitcast %class.btIDebugDraw* %339 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable282 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %349, align 4, !tbaa !25
  %vfn283 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable282, i64 2
  %350 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn283, align 4
  call void %350(%class.btIDebugDraw* %339, %class.btVector3* nonnull align 4 dereferenceable(16) %o, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp271, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp277)
  %351 = bitcast float* %ref.tmp280 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %351) #6
  %352 = bitcast float* %ref.tmp279 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %352) #6
  %353 = bitcast float* %ref.tmp278 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %353) #6
  %354 = bitcast %class.btVector3* %ref.tmp277 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %354) #6
  %355 = bitcast float* %ref.tmp276 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %355) #6
  %356 = bitcast %class.btVector3* %ref.tmp273 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %356) #6
  %357 = bitcast %class.btVector3* %ref.tmp272 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %357) #6
  %358 = bitcast %class.btVector3* %ref.tmp271 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %358) #6
  %359 = bitcast %class.btVector3* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %359) #6
  %360 = bitcast %class.btVector3* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %360) #6
  %361 = bitcast %class.btVector3* %o to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %361) #6
  %362 = bitcast %"struct.btSoftBody::RContact"** %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %362) #6
  br label %for.inc284

for.inc284:                                       ; preds = %for.body237
  %363 = load i32, i32* %i, align 4, !tbaa !6
  %inc285 = add nsw i32 %363, 1
  store i32 %inc285, i32* %i, align 4, !tbaa !6
  br label %for.cond234

for.end286:                                       ; preds = %for.cond234
  br label %if.end287

if.end287:                                        ; preds = %for.end286, %if.end217
  %364 = load i32, i32* %drawflags.addr, align 4, !tbaa !6
  %and288 = and i32 %364, 4
  %cmp289 = icmp ne i32 0, %and288
  br i1 %cmp289, label %if.then290, label %if.end346

if.then290:                                       ; preds = %if.end287
  %365 = bitcast float* %scl291 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %365) #6
  store float 0x3FE99999A0000000, float* %scl291, align 4, !tbaa !8
  %366 = bitcast float* %alp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %366) #6
  store float 1.000000e+00, float* %alp, align 4, !tbaa !8
  %367 = bitcast %class.btVector3* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %367) #6
  %368 = bitcast float* %ref.tmp292 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %368) #6
  store float 0.000000e+00, float* %ref.tmp292, align 4, !tbaa !8
  %369 = bitcast float* %ref.tmp293 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %369) #6
  store float 0x3FE6666660000000, float* %ref.tmp293, align 4, !tbaa !8
  %370 = bitcast float* %ref.tmp294 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %370) #6
  store float 0.000000e+00, float* %ref.tmp294, align 4, !tbaa !8
  %call295 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %col, float* nonnull align 4 dereferenceable(4) %ref.tmp292, float* nonnull align 4 dereferenceable(4) %ref.tmp293, float* nonnull align 4 dereferenceable(4) %ref.tmp294)
  %371 = bitcast float* %ref.tmp294 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %371) #6
  %372 = bitcast float* %ref.tmp293 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %372) #6
  %373 = bitcast float* %ref.tmp292 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %373) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond296

for.cond296:                                      ; preds = %for.inc343, %if.then290
  %374 = load i32, i32* %i, align 4, !tbaa !6
  %375 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_faces = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %375, i32 0, i32 11
  %call297 = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4FaceEE4sizeEv(%class.btAlignedObjectArray.33* %m_faces)
  %cmp298 = icmp slt i32 %374, %call297
  br i1 %cmp298, label %for.body299, label %for.end345

for.body299:                                      ; preds = %for.cond296
  %376 = bitcast %"struct.btSoftBody::Face"** %f to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %376) #6
  %377 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_faces300 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %377, i32 0, i32 11
  %378 = load i32, i32* %i, align 4, !tbaa !6
  %call301 = call nonnull align 4 dereferenceable(44) %"struct.btSoftBody::Face"* @_ZN20btAlignedObjectArrayIN10btSoftBody4FaceEEixEi(%class.btAlignedObjectArray.33* %m_faces300, i32 %378)
  store %"struct.btSoftBody::Face"* %call301, %"struct.btSoftBody::Face"** %f, align 4, !tbaa !2
  %379 = load %"struct.btSoftBody::Face"*, %"struct.btSoftBody::Face"** %f, align 4, !tbaa !2
  %380 = bitcast %"struct.btSoftBody::Face"* %379 to %"struct.btSoftBody::Feature"*
  %m_material302 = getelementptr inbounds %"struct.btSoftBody::Feature", %"struct.btSoftBody::Feature"* %380, i32 0, i32 1
  %381 = load %"struct.btSoftBody::Material"*, %"struct.btSoftBody::Material"** %m_material302, align 4, !tbaa !27
  %m_flags303 = getelementptr inbounds %"struct.btSoftBody::Material", %"struct.btSoftBody::Material"* %381, i32 0, i32 4
  %382 = load i32, i32* %m_flags303, align 4, !tbaa !29
  %and304 = and i32 %382, 1
  %cmp305 = icmp eq i32 0, %and304
  br i1 %cmp305, label %if.then306, label %if.end307

if.then306:                                       ; preds = %for.body299
  store i32 27, i32* %cleanup.dest.slot, align 4
  br label %cleanup340

if.end307:                                        ; preds = %for.body299
  %383 = bitcast [3 x %class.btVector3]* %x308 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %383) #6
  %arrayinit.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %x308, i32 0, i32 0
  %384 = load %"struct.btSoftBody::Face"*, %"struct.btSoftBody::Face"** %f, align 4, !tbaa !2
  %m_n309 = getelementptr inbounds %"struct.btSoftBody::Face", %"struct.btSoftBody::Face"* %384, i32 0, i32 1
  %arrayidx310 = getelementptr inbounds [3 x %"struct.btSoftBody::Node"*], [3 x %"struct.btSoftBody::Node"*]* %m_n309, i32 0, i32 0
  %385 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %arrayidx310, align 4, !tbaa !2
  %m_x311 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %385, i32 0, i32 1
  %386 = bitcast %class.btVector3* %arrayinit.begin to i8*
  %387 = bitcast %class.btVector3* %m_x311 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %386, i8* align 4 %387, i32 16, i1 false), !tbaa.struct !23
  %arrayinit.element = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.begin, i32 1
  %388 = load %"struct.btSoftBody::Face"*, %"struct.btSoftBody::Face"** %f, align 4, !tbaa !2
  %m_n312 = getelementptr inbounds %"struct.btSoftBody::Face", %"struct.btSoftBody::Face"* %388, i32 0, i32 1
  %arrayidx313 = getelementptr inbounds [3 x %"struct.btSoftBody::Node"*], [3 x %"struct.btSoftBody::Node"*]* %m_n312, i32 0, i32 1
  %389 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %arrayidx313, align 4, !tbaa !2
  %m_x314 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %389, i32 0, i32 1
  %390 = bitcast %class.btVector3* %arrayinit.element to i8*
  %391 = bitcast %class.btVector3* %m_x314 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %390, i8* align 4 %391, i32 16, i1 false), !tbaa.struct !23
  %arrayinit.element315 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element, i32 1
  %392 = load %"struct.btSoftBody::Face"*, %"struct.btSoftBody::Face"** %f, align 4, !tbaa !2
  %m_n316 = getelementptr inbounds %"struct.btSoftBody::Face", %"struct.btSoftBody::Face"* %392, i32 0, i32 1
  %arrayidx317 = getelementptr inbounds [3 x %"struct.btSoftBody::Node"*], [3 x %"struct.btSoftBody::Node"*]* %m_n316, i32 0, i32 2
  %393 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %arrayidx317, align 4, !tbaa !2
  %m_x318 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %393, i32 0, i32 1
  %394 = bitcast %class.btVector3* %arrayinit.element315 to i8*
  %395 = bitcast %class.btVector3* %m_x318 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %394, i8* align 4 %395, i32 16, i1 false), !tbaa.struct !23
  %396 = bitcast %class.btVector3* %c319 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %396) #6
  %397 = bitcast %class.btVector3* %ref.tmp320 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %397) #6
  %398 = bitcast %class.btVector3* %ref.tmp321 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %398) #6
  %arrayidx322 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %x308, i32 0, i32 0
  %arrayidx323 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %x308, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp321, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx322, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx323)
  %arrayidx324 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %x308, i32 0, i32 2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp320, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp321, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx324)
  %399 = bitcast float* %ref.tmp325 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %399) #6
  store float 3.000000e+00, float* %ref.tmp325, align 4, !tbaa !8
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %c319, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp320, float* nonnull align 4 dereferenceable(4) %ref.tmp325)
  %400 = bitcast float* %ref.tmp325 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %400) #6
  %401 = bitcast %class.btVector3* %ref.tmp321 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %401) #6
  %402 = bitcast %class.btVector3* %ref.tmp320 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %402) #6
  %403 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %404 = bitcast %class.btVector3* %ref.tmp326 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %404) #6
  %405 = bitcast %class.btVector3* %ref.tmp327 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %405) #6
  %406 = bitcast %class.btVector3* %ref.tmp328 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %406) #6
  %arrayidx329 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %x308, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp328, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx329, %class.btVector3* nonnull align 4 dereferenceable(16) %c319)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp327, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp328, float* nonnull align 4 dereferenceable(4) %scl291)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp326, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp327, %class.btVector3* nonnull align 4 dereferenceable(16) %c319)
  %407 = bitcast %class.btVector3* %ref.tmp330 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %407) #6
  %408 = bitcast %class.btVector3* %ref.tmp331 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %408) #6
  %409 = bitcast %class.btVector3* %ref.tmp332 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %409) #6
  %arrayidx333 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %x308, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp332, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx333, %class.btVector3* nonnull align 4 dereferenceable(16) %c319)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp331, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp332, float* nonnull align 4 dereferenceable(4) %scl291)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp330, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp331, %class.btVector3* nonnull align 4 dereferenceable(16) %c319)
  %410 = bitcast %class.btVector3* %ref.tmp334 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %410) #6
  %411 = bitcast %class.btVector3* %ref.tmp335 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %411) #6
  %412 = bitcast %class.btVector3* %ref.tmp336 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %412) #6
  %arrayidx337 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %x308, i32 0, i32 2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp336, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx337, %class.btVector3* nonnull align 4 dereferenceable(16) %c319)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp335, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp336, float* nonnull align 4 dereferenceable(4) %scl291)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp334, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp335, %class.btVector3* nonnull align 4 dereferenceable(16) %c319)
  %413 = bitcast %class.btIDebugDraw* %403 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)***
  %vtable338 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*** %413, align 4, !tbaa !25
  %vfn339 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)** %vtable338, i64 7
  %414 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)** %vfn339, align 4
  call void %414(%class.btIDebugDraw* %403, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp326, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp330, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp334, %class.btVector3* nonnull align 4 dereferenceable(16) %col, float 1.000000e+00)
  %415 = bitcast %class.btVector3* %ref.tmp336 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %415) #6
  %416 = bitcast %class.btVector3* %ref.tmp335 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %416) #6
  %417 = bitcast %class.btVector3* %ref.tmp334 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %417) #6
  %418 = bitcast %class.btVector3* %ref.tmp332 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %418) #6
  %419 = bitcast %class.btVector3* %ref.tmp331 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %419) #6
  %420 = bitcast %class.btVector3* %ref.tmp330 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %420) #6
  %421 = bitcast %class.btVector3* %ref.tmp328 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %421) #6
  %422 = bitcast %class.btVector3* %ref.tmp327 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %422) #6
  %423 = bitcast %class.btVector3* %ref.tmp326 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %423) #6
  %424 = bitcast %class.btVector3* %c319 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %424) #6
  %425 = bitcast [3 x %class.btVector3]* %x308 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %425) #6
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup340

cleanup340:                                       ; preds = %if.end307, %if.then306
  %426 = bitcast %"struct.btSoftBody::Face"** %f to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %426) #6
  %cleanup.dest341 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest341, label %unreachable [
    i32 0, label %cleanup.cont342
    i32 27, label %for.inc343
  ]

cleanup.cont342:                                  ; preds = %cleanup340
  br label %for.inc343

for.inc343:                                       ; preds = %cleanup.cont342, %cleanup340
  %427 = load i32, i32* %i, align 4, !tbaa !6
  %inc344 = add nsw i32 %427, 1
  store i32 %inc344, i32* %i, align 4, !tbaa !6
  br label %for.cond296

for.end345:                                       ; preds = %for.cond296
  %428 = bitcast %class.btVector3* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %428) #6
  %429 = bitcast float* %alp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %429) #6
  %430 = bitcast float* %scl291 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %430) #6
  br label %if.end346

if.end346:                                        ; preds = %for.end345, %if.end287
  %431 = load i32, i32* %drawflags.addr, align 4, !tbaa !6
  %and347 = and i32 %431, 8
  %cmp348 = icmp ne i32 0, %and347
  br i1 %cmp348, label %if.then349, label %if.end460

if.then349:                                       ; preds = %if.end346
  %432 = bitcast float* %scl350 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %432) #6
  store float 0x3FE99999A0000000, float* %scl350, align 4, !tbaa !8
  %433 = bitcast float* %alp351 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %433) #6
  store float 1.000000e+00, float* %alp351, align 4, !tbaa !8
  %434 = bitcast %class.btVector3* %col352 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %434) #6
  %435 = bitcast float* %ref.tmp353 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %435) #6
  store float 0x3FD3333340000000, float* %ref.tmp353, align 4, !tbaa !8
  %436 = bitcast float* %ref.tmp354 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %436) #6
  store float 0x3FD3333340000000, float* %ref.tmp354, align 4, !tbaa !8
  %437 = bitcast float* %ref.tmp355 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %437) #6
  store float 0x3FE6666660000000, float* %ref.tmp355, align 4, !tbaa !8
  %call356 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %col352, float* nonnull align 4 dereferenceable(4) %ref.tmp353, float* nonnull align 4 dereferenceable(4) %ref.tmp354, float* nonnull align 4 dereferenceable(4) %ref.tmp355)
  %438 = bitcast float* %ref.tmp355 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %438) #6
  %439 = bitcast float* %ref.tmp354 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %439) #6
  %440 = bitcast float* %ref.tmp353 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %440) #6
  %441 = bitcast i32* %i357 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %441) #6
  store i32 0, i32* %i357, align 4, !tbaa !6
  br label %for.cond358

for.cond358:                                      ; preds = %for.inc456, %if.then349
  %442 = load i32, i32* %i357, align 4, !tbaa !6
  %443 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_tetras = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %443, i32 0, i32 12
  %call359 = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody5TetraEE4sizeEv(%class.btAlignedObjectArray.37* %m_tetras)
  %cmp360 = icmp slt i32 %442, %call359
  br i1 %cmp360, label %for.body362, label %for.cond.cleanup361

for.cond.cleanup361:                              ; preds = %for.cond358
  store i32 28, i32* %cleanup.dest.slot, align 4
  %444 = bitcast i32* %i357 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %444) #6
  br label %for.end459

for.body362:                                      ; preds = %for.cond358
  %445 = bitcast %"struct.btSoftBody::Tetra"** %t to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %445) #6
  %446 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_tetras363 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %446, i32 0, i32 12
  %447 = load i32, i32* %i357, align 4, !tbaa !6
  %call364 = call nonnull align 4 dereferenceable(104) %"struct.btSoftBody::Tetra"* @_ZN20btAlignedObjectArrayIN10btSoftBody5TetraEEixEi(%class.btAlignedObjectArray.37* %m_tetras363, i32 %447)
  store %"struct.btSoftBody::Tetra"* %call364, %"struct.btSoftBody::Tetra"** %t, align 4, !tbaa !2
  %448 = load %"struct.btSoftBody::Tetra"*, %"struct.btSoftBody::Tetra"** %t, align 4, !tbaa !2
  %449 = bitcast %"struct.btSoftBody::Tetra"* %448 to %"struct.btSoftBody::Feature"*
  %m_material365 = getelementptr inbounds %"struct.btSoftBody::Feature", %"struct.btSoftBody::Feature"* %449, i32 0, i32 1
  %450 = load %"struct.btSoftBody::Material"*, %"struct.btSoftBody::Material"** %m_material365, align 4, !tbaa !27
  %m_flags366 = getelementptr inbounds %"struct.btSoftBody::Material", %"struct.btSoftBody::Material"* %450, i32 0, i32 4
  %451 = load i32, i32* %m_flags366, align 4, !tbaa !29
  %and367 = and i32 %451, 1
  %cmp368 = icmp eq i32 0, %and367
  br i1 %cmp368, label %if.then369, label %if.end370

if.then369:                                       ; preds = %for.body362
  store i32 30, i32* %cleanup.dest.slot, align 4
  br label %cleanup453

if.end370:                                        ; preds = %for.body362
  %452 = bitcast [4 x %class.btVector3]* %x371 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %452) #6
  %arrayinit.begin372 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x371, i32 0, i32 0
  %453 = load %"struct.btSoftBody::Tetra"*, %"struct.btSoftBody::Tetra"** %t, align 4, !tbaa !2
  %m_n373 = getelementptr inbounds %"struct.btSoftBody::Tetra", %"struct.btSoftBody::Tetra"* %453, i32 0, i32 1
  %arrayidx374 = getelementptr inbounds [4 x %"struct.btSoftBody::Node"*], [4 x %"struct.btSoftBody::Node"*]* %m_n373, i32 0, i32 0
  %454 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %arrayidx374, align 4, !tbaa !2
  %m_x375 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %454, i32 0, i32 1
  %455 = bitcast %class.btVector3* %arrayinit.begin372 to i8*
  %456 = bitcast %class.btVector3* %m_x375 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %455, i8* align 4 %456, i32 16, i1 false), !tbaa.struct !23
  %arrayinit.element376 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.begin372, i32 1
  %457 = load %"struct.btSoftBody::Tetra"*, %"struct.btSoftBody::Tetra"** %t, align 4, !tbaa !2
  %m_n377 = getelementptr inbounds %"struct.btSoftBody::Tetra", %"struct.btSoftBody::Tetra"* %457, i32 0, i32 1
  %arrayidx378 = getelementptr inbounds [4 x %"struct.btSoftBody::Node"*], [4 x %"struct.btSoftBody::Node"*]* %m_n377, i32 0, i32 1
  %458 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %arrayidx378, align 4, !tbaa !2
  %m_x379 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %458, i32 0, i32 1
  %459 = bitcast %class.btVector3* %arrayinit.element376 to i8*
  %460 = bitcast %class.btVector3* %m_x379 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %459, i8* align 4 %460, i32 16, i1 false), !tbaa.struct !23
  %arrayinit.element380 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element376, i32 1
  %461 = load %"struct.btSoftBody::Tetra"*, %"struct.btSoftBody::Tetra"** %t, align 4, !tbaa !2
  %m_n381 = getelementptr inbounds %"struct.btSoftBody::Tetra", %"struct.btSoftBody::Tetra"* %461, i32 0, i32 1
  %arrayidx382 = getelementptr inbounds [4 x %"struct.btSoftBody::Node"*], [4 x %"struct.btSoftBody::Node"*]* %m_n381, i32 0, i32 2
  %462 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %arrayidx382, align 4, !tbaa !2
  %m_x383 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %462, i32 0, i32 1
  %463 = bitcast %class.btVector3* %arrayinit.element380 to i8*
  %464 = bitcast %class.btVector3* %m_x383 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %463, i8* align 4 %464, i32 16, i1 false), !tbaa.struct !23
  %arrayinit.element384 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element380, i32 1
  %465 = load %"struct.btSoftBody::Tetra"*, %"struct.btSoftBody::Tetra"** %t, align 4, !tbaa !2
  %m_n385 = getelementptr inbounds %"struct.btSoftBody::Tetra", %"struct.btSoftBody::Tetra"* %465, i32 0, i32 1
  %arrayidx386 = getelementptr inbounds [4 x %"struct.btSoftBody::Node"*], [4 x %"struct.btSoftBody::Node"*]* %m_n385, i32 0, i32 3
  %466 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %arrayidx386, align 4, !tbaa !2
  %m_x387 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %466, i32 0, i32 1
  %467 = bitcast %class.btVector3* %arrayinit.element384 to i8*
  %468 = bitcast %class.btVector3* %m_x387 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %467, i8* align 4 %468, i32 16, i1 false), !tbaa.struct !23
  %469 = bitcast %class.btVector3* %c388 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %469) #6
  %470 = bitcast %class.btVector3* %ref.tmp389 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %470) #6
  %471 = bitcast %class.btVector3* %ref.tmp390 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %471) #6
  %472 = bitcast %class.btVector3* %ref.tmp391 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %472) #6
  %arrayidx392 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x371, i32 0, i32 0
  %arrayidx393 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x371, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp391, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx392, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx393)
  %arrayidx394 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x371, i32 0, i32 2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp390, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp391, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx394)
  %arrayidx395 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x371, i32 0, i32 3
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp389, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp390, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx395)
  %473 = bitcast float* %ref.tmp396 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %473) #6
  store float 4.000000e+00, float* %ref.tmp396, align 4, !tbaa !8
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %c388, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp389, float* nonnull align 4 dereferenceable(4) %ref.tmp396)
  %474 = bitcast float* %ref.tmp396 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %474) #6
  %475 = bitcast %class.btVector3* %ref.tmp391 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %475) #6
  %476 = bitcast %class.btVector3* %ref.tmp390 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %476) #6
  %477 = bitcast %class.btVector3* %ref.tmp389 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %477) #6
  %478 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %479 = bitcast %class.btVector3* %ref.tmp397 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %479) #6
  %480 = bitcast %class.btVector3* %ref.tmp398 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %480) #6
  %481 = bitcast %class.btVector3* %ref.tmp399 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %481) #6
  %arrayidx400 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x371, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp399, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx400, %class.btVector3* nonnull align 4 dereferenceable(16) %c388)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp398, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp399, float* nonnull align 4 dereferenceable(4) %scl350)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp397, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp398, %class.btVector3* nonnull align 4 dereferenceable(16) %c388)
  %482 = bitcast %class.btVector3* %ref.tmp401 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %482) #6
  %483 = bitcast %class.btVector3* %ref.tmp402 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %483) #6
  %484 = bitcast %class.btVector3* %ref.tmp403 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %484) #6
  %arrayidx404 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x371, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp403, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx404, %class.btVector3* nonnull align 4 dereferenceable(16) %c388)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp402, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp403, float* nonnull align 4 dereferenceable(4) %scl350)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp401, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp402, %class.btVector3* nonnull align 4 dereferenceable(16) %c388)
  %485 = bitcast %class.btVector3* %ref.tmp405 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %485) #6
  %486 = bitcast %class.btVector3* %ref.tmp406 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %486) #6
  %487 = bitcast %class.btVector3* %ref.tmp407 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %487) #6
  %arrayidx408 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x371, i32 0, i32 2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp407, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx408, %class.btVector3* nonnull align 4 dereferenceable(16) %c388)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp406, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp407, float* nonnull align 4 dereferenceable(4) %scl350)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp405, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp406, %class.btVector3* nonnull align 4 dereferenceable(16) %c388)
  %488 = bitcast %class.btIDebugDraw* %478 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)***
  %vtable409 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*** %488, align 4, !tbaa !25
  %vfn410 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)** %vtable409, i64 7
  %489 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)** %vfn410, align 4
  call void %489(%class.btIDebugDraw* %478, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp397, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp401, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp405, %class.btVector3* nonnull align 4 dereferenceable(16) %col352, float 1.000000e+00)
  %490 = bitcast %class.btVector3* %ref.tmp407 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %490) #6
  %491 = bitcast %class.btVector3* %ref.tmp406 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %491) #6
  %492 = bitcast %class.btVector3* %ref.tmp405 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %492) #6
  %493 = bitcast %class.btVector3* %ref.tmp403 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %493) #6
  %494 = bitcast %class.btVector3* %ref.tmp402 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %494) #6
  %495 = bitcast %class.btVector3* %ref.tmp401 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %495) #6
  %496 = bitcast %class.btVector3* %ref.tmp399 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %496) #6
  %497 = bitcast %class.btVector3* %ref.tmp398 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %497) #6
  %498 = bitcast %class.btVector3* %ref.tmp397 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %498) #6
  %499 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %500 = bitcast %class.btVector3* %ref.tmp411 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %500) #6
  %501 = bitcast %class.btVector3* %ref.tmp412 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %501) #6
  %502 = bitcast %class.btVector3* %ref.tmp413 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %502) #6
  %arrayidx414 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x371, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp413, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx414, %class.btVector3* nonnull align 4 dereferenceable(16) %c388)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp412, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp413, float* nonnull align 4 dereferenceable(4) %scl350)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp411, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp412, %class.btVector3* nonnull align 4 dereferenceable(16) %c388)
  %503 = bitcast %class.btVector3* %ref.tmp415 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %503) #6
  %504 = bitcast %class.btVector3* %ref.tmp416 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %504) #6
  %505 = bitcast %class.btVector3* %ref.tmp417 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %505) #6
  %arrayidx418 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x371, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp417, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx418, %class.btVector3* nonnull align 4 dereferenceable(16) %c388)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp416, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp417, float* nonnull align 4 dereferenceable(4) %scl350)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp415, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp416, %class.btVector3* nonnull align 4 dereferenceable(16) %c388)
  %506 = bitcast %class.btVector3* %ref.tmp419 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %506) #6
  %507 = bitcast %class.btVector3* %ref.tmp420 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %507) #6
  %508 = bitcast %class.btVector3* %ref.tmp421 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %508) #6
  %arrayidx422 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x371, i32 0, i32 3
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp421, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx422, %class.btVector3* nonnull align 4 dereferenceable(16) %c388)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp420, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp421, float* nonnull align 4 dereferenceable(4) %scl350)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp419, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp420, %class.btVector3* nonnull align 4 dereferenceable(16) %c388)
  %509 = bitcast %class.btIDebugDraw* %499 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)***
  %vtable423 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*** %509, align 4, !tbaa !25
  %vfn424 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)** %vtable423, i64 7
  %510 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)** %vfn424, align 4
  call void %510(%class.btIDebugDraw* %499, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp411, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp415, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp419, %class.btVector3* nonnull align 4 dereferenceable(16) %col352, float 1.000000e+00)
  %511 = bitcast %class.btVector3* %ref.tmp421 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %511) #6
  %512 = bitcast %class.btVector3* %ref.tmp420 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %512) #6
  %513 = bitcast %class.btVector3* %ref.tmp419 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %513) #6
  %514 = bitcast %class.btVector3* %ref.tmp417 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %514) #6
  %515 = bitcast %class.btVector3* %ref.tmp416 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %515) #6
  %516 = bitcast %class.btVector3* %ref.tmp415 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %516) #6
  %517 = bitcast %class.btVector3* %ref.tmp413 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %517) #6
  %518 = bitcast %class.btVector3* %ref.tmp412 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %518) #6
  %519 = bitcast %class.btVector3* %ref.tmp411 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %519) #6
  %520 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %521 = bitcast %class.btVector3* %ref.tmp425 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %521) #6
  %522 = bitcast %class.btVector3* %ref.tmp426 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %522) #6
  %523 = bitcast %class.btVector3* %ref.tmp427 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %523) #6
  %arrayidx428 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x371, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp427, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx428, %class.btVector3* nonnull align 4 dereferenceable(16) %c388)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp426, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp427, float* nonnull align 4 dereferenceable(4) %scl350)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp425, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp426, %class.btVector3* nonnull align 4 dereferenceable(16) %c388)
  %524 = bitcast %class.btVector3* %ref.tmp429 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %524) #6
  %525 = bitcast %class.btVector3* %ref.tmp430 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %525) #6
  %526 = bitcast %class.btVector3* %ref.tmp431 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %526) #6
  %arrayidx432 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x371, i32 0, i32 2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp431, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx432, %class.btVector3* nonnull align 4 dereferenceable(16) %c388)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp430, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp431, float* nonnull align 4 dereferenceable(4) %scl350)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp429, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp430, %class.btVector3* nonnull align 4 dereferenceable(16) %c388)
  %527 = bitcast %class.btVector3* %ref.tmp433 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %527) #6
  %528 = bitcast %class.btVector3* %ref.tmp434 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %528) #6
  %529 = bitcast %class.btVector3* %ref.tmp435 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %529) #6
  %arrayidx436 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x371, i32 0, i32 3
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp435, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx436, %class.btVector3* nonnull align 4 dereferenceable(16) %c388)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp434, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp435, float* nonnull align 4 dereferenceable(4) %scl350)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp433, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp434, %class.btVector3* nonnull align 4 dereferenceable(16) %c388)
  %530 = bitcast %class.btIDebugDraw* %520 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)***
  %vtable437 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*** %530, align 4, !tbaa !25
  %vfn438 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)** %vtable437, i64 7
  %531 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)** %vfn438, align 4
  call void %531(%class.btIDebugDraw* %520, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp425, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp429, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp433, %class.btVector3* nonnull align 4 dereferenceable(16) %col352, float 1.000000e+00)
  %532 = bitcast %class.btVector3* %ref.tmp435 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %532) #6
  %533 = bitcast %class.btVector3* %ref.tmp434 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %533) #6
  %534 = bitcast %class.btVector3* %ref.tmp433 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %534) #6
  %535 = bitcast %class.btVector3* %ref.tmp431 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %535) #6
  %536 = bitcast %class.btVector3* %ref.tmp430 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %536) #6
  %537 = bitcast %class.btVector3* %ref.tmp429 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %537) #6
  %538 = bitcast %class.btVector3* %ref.tmp427 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %538) #6
  %539 = bitcast %class.btVector3* %ref.tmp426 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %539) #6
  %540 = bitcast %class.btVector3* %ref.tmp425 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %540) #6
  %541 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %542 = bitcast %class.btVector3* %ref.tmp439 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %542) #6
  %543 = bitcast %class.btVector3* %ref.tmp440 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %543) #6
  %544 = bitcast %class.btVector3* %ref.tmp441 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %544) #6
  %arrayidx442 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x371, i32 0, i32 2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp441, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx442, %class.btVector3* nonnull align 4 dereferenceable(16) %c388)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp440, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp441, float* nonnull align 4 dereferenceable(4) %scl350)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp439, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp440, %class.btVector3* nonnull align 4 dereferenceable(16) %c388)
  %545 = bitcast %class.btVector3* %ref.tmp443 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %545) #6
  %546 = bitcast %class.btVector3* %ref.tmp444 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %546) #6
  %547 = bitcast %class.btVector3* %ref.tmp445 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %547) #6
  %arrayidx446 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x371, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp445, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx446, %class.btVector3* nonnull align 4 dereferenceable(16) %c388)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp444, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp445, float* nonnull align 4 dereferenceable(4) %scl350)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp443, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp444, %class.btVector3* nonnull align 4 dereferenceable(16) %c388)
  %548 = bitcast %class.btVector3* %ref.tmp447 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %548) #6
  %549 = bitcast %class.btVector3* %ref.tmp448 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %549) #6
  %550 = bitcast %class.btVector3* %ref.tmp449 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %550) #6
  %arrayidx450 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x371, i32 0, i32 3
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp449, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx450, %class.btVector3* nonnull align 4 dereferenceable(16) %c388)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp448, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp449, float* nonnull align 4 dereferenceable(4) %scl350)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp447, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp448, %class.btVector3* nonnull align 4 dereferenceable(16) %c388)
  %551 = bitcast %class.btIDebugDraw* %541 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)***
  %vtable451 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*** %551, align 4, !tbaa !25
  %vfn452 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)** %vtable451, i64 7
  %552 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)** %vfn452, align 4
  call void %552(%class.btIDebugDraw* %541, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp439, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp443, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp447, %class.btVector3* nonnull align 4 dereferenceable(16) %col352, float 1.000000e+00)
  %553 = bitcast %class.btVector3* %ref.tmp449 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %553) #6
  %554 = bitcast %class.btVector3* %ref.tmp448 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %554) #6
  %555 = bitcast %class.btVector3* %ref.tmp447 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %555) #6
  %556 = bitcast %class.btVector3* %ref.tmp445 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %556) #6
  %557 = bitcast %class.btVector3* %ref.tmp444 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %557) #6
  %558 = bitcast %class.btVector3* %ref.tmp443 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %558) #6
  %559 = bitcast %class.btVector3* %ref.tmp441 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %559) #6
  %560 = bitcast %class.btVector3* %ref.tmp440 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %560) #6
  %561 = bitcast %class.btVector3* %ref.tmp439 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %561) #6
  %562 = bitcast %class.btVector3* %c388 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %562) #6
  %563 = bitcast [4 x %class.btVector3]* %x371 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %563) #6
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup453

cleanup453:                                       ; preds = %if.end370, %if.then369
  %564 = bitcast %"struct.btSoftBody::Tetra"** %t to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %564) #6
  %cleanup.dest454 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest454, label %unreachable [
    i32 0, label %cleanup.cont455
    i32 30, label %for.inc456
  ]

cleanup.cont455:                                  ; preds = %cleanup453
  br label %for.inc456

for.inc456:                                       ; preds = %cleanup.cont455, %cleanup453
  %565 = load i32, i32* %i357, align 4, !tbaa !6
  %inc457 = add nsw i32 %565, 1
  store i32 %inc457, i32* %i357, align 4, !tbaa !6
  br label %for.cond358

for.end459:                                       ; preds = %for.cond.cleanup361
  %566 = bitcast %class.btVector3* %col352 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %566) #6
  %567 = bitcast float* %alp351 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %567) #6
  %568 = bitcast float* %scl350 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %568) #6
  br label %if.end460

if.end460:                                        ; preds = %for.end459, %if.end346
  br label %if.end461

if.end461:                                        ; preds = %if.end460, %for.end77
  %569 = load i32, i32* %drawflags.addr, align 4, !tbaa !6
  %and462 = and i32 %569, 64
  %cmp463 = icmp ne i32 0, %and462
  br i1 %cmp463, label %if.then464, label %if.end525

if.then464:                                       ; preds = %if.end461
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond465

for.cond465:                                      ; preds = %for.inc493, %if.then464
  %570 = load i32, i32* %i, align 4, !tbaa !6
  %571 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_anchors = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %571, i32 0, i32 13
  %call466 = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody6AnchorEE4sizeEv(%class.btAlignedObjectArray.41* %m_anchors)
  %cmp467 = icmp slt i32 %570, %call466
  br i1 %cmp467, label %for.body468, label %for.end495

for.body468:                                      ; preds = %for.cond465
  %572 = bitcast %"struct.btSoftBody::Anchor"** %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %572) #6
  %573 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_anchors469 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %573, i32 0, i32 13
  %574 = load i32, i32* %i, align 4, !tbaa !6
  %call470 = call nonnull align 4 dereferenceable(96) %"struct.btSoftBody::Anchor"* @_ZN20btAlignedObjectArrayIN10btSoftBody6AnchorEEixEi(%class.btAlignedObjectArray.41* %m_anchors469, i32 %574)
  store %"struct.btSoftBody::Anchor"* %call470, %"struct.btSoftBody::Anchor"** %a, align 4, !tbaa !2
  %575 = bitcast %class.btVector3* %q to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %575) #6
  %576 = load %"struct.btSoftBody::Anchor"*, %"struct.btSoftBody::Anchor"** %a, align 4, !tbaa !2
  %m_body = getelementptr inbounds %"struct.btSoftBody::Anchor", %"struct.btSoftBody::Anchor"* %576, i32 0, i32 2
  %577 = load %class.btRigidBody*, %class.btRigidBody** %m_body, align 4, !tbaa !36
  %578 = bitcast %class.btRigidBody* %577 to %class.btCollisionObject*
  %call471 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %578)
  %579 = load %"struct.btSoftBody::Anchor"*, %"struct.btSoftBody::Anchor"** %a, align 4, !tbaa !2
  %m_local = getelementptr inbounds %"struct.btSoftBody::Anchor", %"struct.btSoftBody::Anchor"* %579, i32 0, i32 1
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %q, %class.btTransform* %call471, %class.btVector3* nonnull align 4 dereferenceable(16) %m_local)
  %580 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %581 = load %"struct.btSoftBody::Anchor"*, %"struct.btSoftBody::Anchor"** %a, align 4, !tbaa !2
  %m_node472 = getelementptr inbounds %"struct.btSoftBody::Anchor", %"struct.btSoftBody::Anchor"* %581, i32 0, i32 0
  %582 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %m_node472, align 4, !tbaa !38
  %m_x473 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %582, i32 0, i32 1
  %583 = bitcast %class.btVector3* %ref.tmp474 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %583) #6
  %584 = bitcast float* %ref.tmp475 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %584) #6
  store float 1.000000e+00, float* %ref.tmp475, align 4, !tbaa !8
  %585 = bitcast float* %ref.tmp476 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %585) #6
  store float 0.000000e+00, float* %ref.tmp476, align 4, !tbaa !8
  %586 = bitcast float* %ref.tmp477 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %586) #6
  store float 0.000000e+00, float* %ref.tmp477, align 4, !tbaa !8
  %call478 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp474, float* nonnull align 4 dereferenceable(4) %ref.tmp475, float* nonnull align 4 dereferenceable(4) %ref.tmp476, float* nonnull align 4 dereferenceable(4) %ref.tmp477)
  call void @_ZL10drawVertexP12btIDebugDrawRK9btVector3fS3_(%class.btIDebugDraw* %580, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x473, float 2.500000e-01, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp474)
  %587 = bitcast float* %ref.tmp477 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %587) #6
  %588 = bitcast float* %ref.tmp476 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %588) #6
  %589 = bitcast float* %ref.tmp475 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %589) #6
  %590 = bitcast %class.btVector3* %ref.tmp474 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %590) #6
  %591 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %592 = bitcast %class.btVector3* %ref.tmp479 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %592) #6
  %593 = bitcast float* %ref.tmp480 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %593) #6
  store float 0.000000e+00, float* %ref.tmp480, align 4, !tbaa !8
  %594 = bitcast float* %ref.tmp481 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %594) #6
  store float 1.000000e+00, float* %ref.tmp481, align 4, !tbaa !8
  %595 = bitcast float* %ref.tmp482 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %595) #6
  store float 0.000000e+00, float* %ref.tmp482, align 4, !tbaa !8
  %call483 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp479, float* nonnull align 4 dereferenceable(4) %ref.tmp480, float* nonnull align 4 dereferenceable(4) %ref.tmp481, float* nonnull align 4 dereferenceable(4) %ref.tmp482)
  call void @_ZL10drawVertexP12btIDebugDrawRK9btVector3fS3_(%class.btIDebugDraw* %591, %class.btVector3* nonnull align 4 dereferenceable(16) %q, float 2.500000e-01, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp479)
  %596 = bitcast float* %ref.tmp482 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %596) #6
  %597 = bitcast float* %ref.tmp481 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %597) #6
  %598 = bitcast float* %ref.tmp480 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %598) #6
  %599 = bitcast %class.btVector3* %ref.tmp479 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %599) #6
  %600 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %601 = load %"struct.btSoftBody::Anchor"*, %"struct.btSoftBody::Anchor"** %a, align 4, !tbaa !2
  %m_node484 = getelementptr inbounds %"struct.btSoftBody::Anchor", %"struct.btSoftBody::Anchor"* %601, i32 0, i32 0
  %602 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %m_node484, align 4, !tbaa !38
  %m_x485 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %602, i32 0, i32 1
  %603 = bitcast %class.btVector3* %ref.tmp486 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %603) #6
  %604 = bitcast float* %ref.tmp487 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %604) #6
  store float 1.000000e+00, float* %ref.tmp487, align 4, !tbaa !8
  %605 = bitcast float* %ref.tmp488 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %605) #6
  store float 1.000000e+00, float* %ref.tmp488, align 4, !tbaa !8
  %606 = bitcast float* %ref.tmp489 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %606) #6
  store float 1.000000e+00, float* %ref.tmp489, align 4, !tbaa !8
  %call490 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp486, float* nonnull align 4 dereferenceable(4) %ref.tmp487, float* nonnull align 4 dereferenceable(4) %ref.tmp488, float* nonnull align 4 dereferenceable(4) %ref.tmp489)
  %607 = bitcast %class.btIDebugDraw* %600 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable491 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %607, align 4, !tbaa !25
  %vfn492 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable491, i64 2
  %608 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn492, align 4
  call void %608(%class.btIDebugDraw* %600, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x485, %class.btVector3* nonnull align 4 dereferenceable(16) %q, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp486)
  %609 = bitcast float* %ref.tmp489 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %609) #6
  %610 = bitcast float* %ref.tmp488 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %610) #6
  %611 = bitcast float* %ref.tmp487 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %611) #6
  %612 = bitcast %class.btVector3* %ref.tmp486 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %612) #6
  %613 = bitcast %class.btVector3* %q to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %613) #6
  %614 = bitcast %"struct.btSoftBody::Anchor"** %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %614) #6
  br label %for.inc493

for.inc493:                                       ; preds = %for.body468
  %615 = load i32, i32* %i, align 4, !tbaa !6
  %inc494 = add nsw i32 %615, 1
  store i32 %inc494, i32* %i, align 4, !tbaa !6
  br label %for.cond465

for.end495:                                       ; preds = %for.cond465
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond496

for.cond496:                                      ; preds = %for.inc522, %for.end495
  %616 = load i32, i32* %i, align 4, !tbaa !6
  %617 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_nodes497 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %617, i32 0, i32 9
  %call498 = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4NodeEE4sizeEv(%class.btAlignedObjectArray.25* %m_nodes497)
  %cmp499 = icmp slt i32 %616, %call498
  br i1 %cmp499, label %for.body500, label %for.end524

for.body500:                                      ; preds = %for.cond496
  %618 = bitcast %"struct.btSoftBody::Node"** %n501 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %618) #6
  %619 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_nodes502 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %619, i32 0, i32 9
  %620 = load i32, i32* %i, align 4, !tbaa !6
  %call503 = call nonnull align 4 dereferenceable(101) %"struct.btSoftBody::Node"* @_ZN20btAlignedObjectArrayIN10btSoftBody4NodeEEixEi(%class.btAlignedObjectArray.25* %m_nodes502, i32 %620)
  store %"struct.btSoftBody::Node"* %call503, %"struct.btSoftBody::Node"** %n501, align 4, !tbaa !2
  %621 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n501, align 4, !tbaa !2
  %622 = bitcast %"struct.btSoftBody::Node"* %621 to %"struct.btSoftBody::Feature"*
  %m_material504 = getelementptr inbounds %"struct.btSoftBody::Feature", %"struct.btSoftBody::Feature"* %622, i32 0, i32 1
  %623 = load %"struct.btSoftBody::Material"*, %"struct.btSoftBody::Material"** %m_material504, align 4, !tbaa !27
  %m_flags505 = getelementptr inbounds %"struct.btSoftBody::Material", %"struct.btSoftBody::Material"* %623, i32 0, i32 4
  %624 = load i32, i32* %m_flags505, align 4, !tbaa !29
  %and506 = and i32 %624, 1
  %cmp507 = icmp eq i32 0, %and506
  br i1 %cmp507, label %if.then508, label %if.end509

if.then508:                                       ; preds = %for.body500
  store i32 36, i32* %cleanup.dest.slot, align 4
  br label %cleanup519

if.end509:                                        ; preds = %for.body500
  %625 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n501, align 4, !tbaa !2
  %m_im = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %625, i32 0, i32 6
  %626 = load float, float* %m_im, align 4, !tbaa !39
  %cmp510 = fcmp ole float %626, 0.000000e+00
  br i1 %cmp510, label %if.then511, label %if.end518

if.then511:                                       ; preds = %if.end509
  %627 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %628 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n501, align 4, !tbaa !2
  %m_x512 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %628, i32 0, i32 1
  %629 = bitcast %class.btVector3* %ref.tmp513 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %629) #6
  %630 = bitcast float* %ref.tmp514 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %630) #6
  store float 1.000000e+00, float* %ref.tmp514, align 4, !tbaa !8
  %631 = bitcast float* %ref.tmp515 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %631) #6
  store float 0.000000e+00, float* %ref.tmp515, align 4, !tbaa !8
  %632 = bitcast float* %ref.tmp516 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %632) #6
  store float 0.000000e+00, float* %ref.tmp516, align 4, !tbaa !8
  %call517 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp513, float* nonnull align 4 dereferenceable(4) %ref.tmp514, float* nonnull align 4 dereferenceable(4) %ref.tmp515, float* nonnull align 4 dereferenceable(4) %ref.tmp516)
  call void @_ZL10drawVertexP12btIDebugDrawRK9btVector3fS3_(%class.btIDebugDraw* %627, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x512, float 2.500000e-01, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp513)
  %633 = bitcast float* %ref.tmp516 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %633) #6
  %634 = bitcast float* %ref.tmp515 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %634) #6
  %635 = bitcast float* %ref.tmp514 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %635) #6
  %636 = bitcast %class.btVector3* %ref.tmp513 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %636) #6
  br label %if.end518

if.end518:                                        ; preds = %if.then511, %if.end509
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup519

cleanup519:                                       ; preds = %if.end518, %if.then508
  %637 = bitcast %"struct.btSoftBody::Node"** %n501 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %637) #6
  %cleanup.dest520 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest520, label %unreachable [
    i32 0, label %cleanup.cont521
    i32 36, label %for.inc522
  ]

cleanup.cont521:                                  ; preds = %cleanup519
  br label %for.inc522

for.inc522:                                       ; preds = %cleanup.cont521, %cleanup519
  %638 = load i32, i32* %i, align 4, !tbaa !6
  %inc523 = add nsw i32 %638, 1
  store i32 %inc523, i32* %i, align 4, !tbaa !6
  br label %for.cond496

for.end524:                                       ; preds = %for.cond496
  br label %if.end525

if.end525:                                        ; preds = %for.end524, %if.end461
  %639 = load i32, i32* %drawflags.addr, align 4, !tbaa !6
  %and526 = and i32 %639, 128
  %cmp527 = icmp ne i32 0, %and526
  br i1 %cmp527, label %if.then528, label %if.end557

if.then528:                                       ; preds = %if.end525
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond529

for.cond529:                                      ; preds = %for.inc554, %if.then528
  %640 = load i32, i32* %i, align 4, !tbaa !6
  %641 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_notes = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %641, i32 0, i32 8
  %call530 = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4NoteEE4sizeEv(%class.btAlignedObjectArray.20* %m_notes)
  %cmp531 = icmp slt i32 %640, %call530
  br i1 %cmp531, label %for.body532, label %for.end556

for.body532:                                      ; preds = %for.cond529
  %642 = bitcast %"struct.btSoftBody::Note"** %n533 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %642) #6
  %643 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_notes534 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %643, i32 0, i32 8
  %644 = load i32, i32* %i, align 4, !tbaa !6
  %call535 = call nonnull align 4 dereferenceable(60) %"struct.btSoftBody::Note"* @_ZN20btAlignedObjectArrayIN10btSoftBody4NoteEEixEi(%class.btAlignedObjectArray.20* %m_notes534, i32 %644)
  store %"struct.btSoftBody::Note"* %call535, %"struct.btSoftBody::Note"** %n533, align 4, !tbaa !2
  %645 = bitcast %class.btVector3* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %645) #6
  %646 = load %"struct.btSoftBody::Note"*, %"struct.btSoftBody::Note"** %n533, align 4, !tbaa !2
  %m_offset536 = getelementptr inbounds %"struct.btSoftBody::Note", %"struct.btSoftBody::Note"* %646, i32 0, i32 2
  %647 = bitcast %class.btVector3* %p to i8*
  %648 = bitcast %class.btVector3* %m_offset536 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %647, i8* align 4 %648, i32 16, i1 false), !tbaa.struct !23
  %649 = bitcast i32* %j537 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %649) #6
  store i32 0, i32* %j537, align 4, !tbaa !6
  br label %for.cond538

for.cond538:                                      ; preds = %for.inc548, %for.body532
  %650 = load i32, i32* %j537, align 4, !tbaa !6
  %651 = load %"struct.btSoftBody::Note"*, %"struct.btSoftBody::Note"** %n533, align 4, !tbaa !2
  %m_rank = getelementptr inbounds %"struct.btSoftBody::Note", %"struct.btSoftBody::Note"* %651, i32 0, i32 3
  %652 = load i32, i32* %m_rank, align 4, !tbaa !41
  %cmp539 = icmp slt i32 %650, %652
  br i1 %cmp539, label %for.body541, label %for.cond.cleanup540

for.cond.cleanup540:                              ; preds = %for.cond538
  store i32 40, i32* %cleanup.dest.slot, align 4
  %653 = bitcast i32* %j537 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %653) #6
  br label %for.end551

for.body541:                                      ; preds = %for.cond538
  %654 = bitcast %class.btVector3* %ref.tmp542 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %654) #6
  %655 = load %"struct.btSoftBody::Note"*, %"struct.btSoftBody::Note"** %n533, align 4, !tbaa !2
  %m_nodes543 = getelementptr inbounds %"struct.btSoftBody::Note", %"struct.btSoftBody::Note"* %655, i32 0, i32 4
  %656 = load i32, i32* %j537, align 4, !tbaa !6
  %arrayidx544 = getelementptr inbounds [4 x %"struct.btSoftBody::Node"*], [4 x %"struct.btSoftBody::Node"*]* %m_nodes543, i32 0, i32 %656
  %657 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %arrayidx544, align 4, !tbaa !2
  %m_x545 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %657, i32 0, i32 1
  %658 = load %"struct.btSoftBody::Note"*, %"struct.btSoftBody::Note"** %n533, align 4, !tbaa !2
  %m_coords = getelementptr inbounds %"struct.btSoftBody::Note", %"struct.btSoftBody::Note"* %658, i32 0, i32 5
  %659 = load i32, i32* %j537, align 4, !tbaa !6
  %arrayidx546 = getelementptr inbounds [4 x float], [4 x float]* %m_coords, i32 0, i32 %659
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp542, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x545, float* nonnull align 4 dereferenceable(4) %arrayidx546)
  %call547 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %p, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp542)
  %660 = bitcast %class.btVector3* %ref.tmp542 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %660) #6
  br label %for.inc548

for.inc548:                                       ; preds = %for.body541
  %661 = load i32, i32* %j537, align 4, !tbaa !6
  %inc549 = add nsw i32 %661, 1
  store i32 %inc549, i32* %j537, align 4, !tbaa !6
  br label %for.cond538

for.end551:                                       ; preds = %for.cond.cleanup540
  %662 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %663 = load %"struct.btSoftBody::Note"*, %"struct.btSoftBody::Note"** %n533, align 4, !tbaa !2
  %m_text = getelementptr inbounds %"struct.btSoftBody::Note", %"struct.btSoftBody::Note"* %663, i32 0, i32 1
  %664 = load i8*, i8** %m_text, align 4, !tbaa !43
  %665 = bitcast %class.btIDebugDraw* %662 to void (%class.btIDebugDraw*, %class.btVector3*, i8*)***
  %vtable552 = load void (%class.btIDebugDraw*, %class.btVector3*, i8*)**, void (%class.btIDebugDraw*, %class.btVector3*, i8*)*** %665, align 4, !tbaa !25
  %vfn553 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, i8*)*, void (%class.btIDebugDraw*, %class.btVector3*, i8*)** %vtable552, i64 10
  %666 = load void (%class.btIDebugDraw*, %class.btVector3*, i8*)*, void (%class.btIDebugDraw*, %class.btVector3*, i8*)** %vfn553, align 4
  call void %666(%class.btIDebugDraw* %662, %class.btVector3* nonnull align 4 dereferenceable(16) %p, i8* %664)
  %667 = bitcast %class.btVector3* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %667) #6
  %668 = bitcast %"struct.btSoftBody::Note"** %n533 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %668) #6
  br label %for.inc554

for.inc554:                                       ; preds = %for.end551
  %669 = load i32, i32* %i, align 4, !tbaa !6
  %inc555 = add nsw i32 %669, 1
  store i32 %inc555, i32* %i, align 4, !tbaa !6
  br label %for.cond529

for.end556:                                       ; preds = %for.cond529
  br label %if.end557

if.end557:                                        ; preds = %for.end556, %if.end525
  %670 = load i32, i32* %drawflags.addr, align 4, !tbaa !6
  %and558 = and i32 %670, 512
  %cmp559 = icmp ne i32 0, %and558
  br i1 %cmp559, label %if.then560, label %if.end561

if.then560:                                       ; preds = %if.end557
  %671 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %672 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  call void @_ZN17btSoftBodyHelpers12DrawNodeTreeEP10btSoftBodyP12btIDebugDrawii(%class.btSoftBody* %671, %class.btIDebugDraw* %672, i32 0, i32 -1)
  br label %if.end561

if.end561:                                        ; preds = %if.then560, %if.end557
  %673 = load i32, i32* %drawflags.addr, align 4, !tbaa !6
  %and562 = and i32 %673, 1024
  %cmp563 = icmp ne i32 0, %and562
  br i1 %cmp563, label %if.then564, label %if.end565

if.then564:                                       ; preds = %if.end561
  %674 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %675 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  call void @_ZN17btSoftBodyHelpers12DrawFaceTreeEP10btSoftBodyP12btIDebugDrawii(%class.btSoftBody* %674, %class.btIDebugDraw* %675, i32 0, i32 -1)
  br label %if.end565

if.end565:                                        ; preds = %if.then564, %if.end561
  %676 = load i32, i32* %drawflags.addr, align 4, !tbaa !6
  %and566 = and i32 %676, 2048
  %cmp567 = icmp ne i32 0, %and566
  br i1 %cmp567, label %if.then568, label %if.end569

if.then568:                                       ; preds = %if.end565
  %677 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %678 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  call void @_ZN17btSoftBodyHelpers15DrawClusterTreeEP10btSoftBodyP12btIDebugDrawii(%class.btSoftBody* %677, %class.btIDebugDraw* %678, i32 0, i32 -1)
  br label %if.end569

if.end569:                                        ; preds = %if.then568, %if.end565
  %679 = load i32, i32* %drawflags.addr, align 4, !tbaa !6
  %and570 = and i32 %679, 4096
  %cmp571 = icmp ne i32 0, %and570
  br i1 %cmp571, label %if.then572, label %if.end692

if.then572:                                       ; preds = %if.end569
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond573

for.cond573:                                      ; preds = %for.inc689, %if.then572
  %680 = load i32, i32* %i, align 4, !tbaa !6
  %681 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_joints = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %681, i32 0, i32 16
  %call574 = call i32 @_ZNK20btAlignedObjectArrayIPN10btSoftBody5JointEE4sizeEv(%class.btAlignedObjectArray.57* %m_joints)
  %cmp575 = icmp slt i32 %680, %call574
  br i1 %cmp575, label %for.body576, label %for.end691

for.body576:                                      ; preds = %for.cond573
  %682 = bitcast %"struct.btSoftBody::Joint"** %pj to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %682) #6
  %683 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_joints577 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %683, i32 0, i32 16
  %684 = load i32, i32* %i, align 4, !tbaa !6
  %call578 = call nonnull align 4 dereferenceable(4) %"struct.btSoftBody::Joint"** @_ZN20btAlignedObjectArrayIPN10btSoftBody5JointEEixEi(%class.btAlignedObjectArray.57* %m_joints577, i32 %684)
  %685 = load %"struct.btSoftBody::Joint"*, %"struct.btSoftBody::Joint"** %call578, align 4, !tbaa !2
  store %"struct.btSoftBody::Joint"* %685, %"struct.btSoftBody::Joint"** %pj, align 4, !tbaa !2
  %686 = load %"struct.btSoftBody::Joint"*, %"struct.btSoftBody::Joint"** %pj, align 4, !tbaa !2
  %687 = bitcast %"struct.btSoftBody::Joint"* %686 to i32 (%"struct.btSoftBody::Joint"*)***
  %vtable579 = load i32 (%"struct.btSoftBody::Joint"*)**, i32 (%"struct.btSoftBody::Joint"*)*** %687, align 4, !tbaa !25
  %vfn580 = getelementptr inbounds i32 (%"struct.btSoftBody::Joint"*)*, i32 (%"struct.btSoftBody::Joint"*)** %vtable579, i64 5
  %688 = load i32 (%"struct.btSoftBody::Joint"*)*, i32 (%"struct.btSoftBody::Joint"*)** %vfn580, align 4
  %call581 = call i32 %688(%"struct.btSoftBody::Joint"* %686)
  switch i32 %call581, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb622
  ]

sw.bb:                                            ; preds = %for.body576
  %689 = bitcast %"struct.btSoftBody::LJoint"** %pjl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %689) #6
  %690 = load %"struct.btSoftBody::Joint"*, %"struct.btSoftBody::Joint"** %pj, align 4, !tbaa !2
  %691 = bitcast %"struct.btSoftBody::Joint"* %690 to %"struct.btSoftBody::LJoint"*
  store %"struct.btSoftBody::LJoint"* %691, %"struct.btSoftBody::LJoint"** %pjl, align 4, !tbaa !2
  %692 = bitcast %class.btVector3* %a0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %692) #6
  %693 = load %"struct.btSoftBody::Joint"*, %"struct.btSoftBody::Joint"** %pj, align 4, !tbaa !2
  %m_bodies = getelementptr inbounds %"struct.btSoftBody::Joint", %"struct.btSoftBody::Joint"* %693, i32 0, i32 1
  %arrayidx582 = getelementptr inbounds [2 x %"struct.btSoftBody::Body"], [2 x %"struct.btSoftBody::Body"]* %m_bodies, i32 0, i32 0
  %call583 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK10btSoftBody4Body5xformEv(%"struct.btSoftBody::Body"* %arrayidx582)
  %694 = load %"struct.btSoftBody::LJoint"*, %"struct.btSoftBody::LJoint"** %pjl, align 4, !tbaa !2
  %695 = bitcast %"struct.btSoftBody::LJoint"* %694 to %"struct.btSoftBody::Joint"*
  %m_refs = getelementptr inbounds %"struct.btSoftBody::Joint", %"struct.btSoftBody::Joint"* %695, i32 0, i32 2
  %arrayidx584 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %m_refs, i32 0, i32 0
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %a0, %class.btTransform* %call583, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx584)
  %696 = bitcast %class.btVector3* %a1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %696) #6
  %697 = load %"struct.btSoftBody::Joint"*, %"struct.btSoftBody::Joint"** %pj, align 4, !tbaa !2
  %m_bodies585 = getelementptr inbounds %"struct.btSoftBody::Joint", %"struct.btSoftBody::Joint"* %697, i32 0, i32 1
  %arrayidx586 = getelementptr inbounds [2 x %"struct.btSoftBody::Body"], [2 x %"struct.btSoftBody::Body"]* %m_bodies585, i32 0, i32 1
  %call587 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK10btSoftBody4Body5xformEv(%"struct.btSoftBody::Body"* %arrayidx586)
  %698 = load %"struct.btSoftBody::LJoint"*, %"struct.btSoftBody::LJoint"** %pjl, align 4, !tbaa !2
  %699 = bitcast %"struct.btSoftBody::LJoint"* %698 to %"struct.btSoftBody::Joint"*
  %m_refs588 = getelementptr inbounds %"struct.btSoftBody::Joint", %"struct.btSoftBody::Joint"* %699, i32 0, i32 2
  %arrayidx589 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %m_refs588, i32 0, i32 1
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %a1, %class.btTransform* %call587, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx589)
  %700 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %701 = load %"struct.btSoftBody::Joint"*, %"struct.btSoftBody::Joint"** %pj, align 4, !tbaa !2
  %m_bodies590 = getelementptr inbounds %"struct.btSoftBody::Joint", %"struct.btSoftBody::Joint"* %701, i32 0, i32 1
  %arrayidx591 = getelementptr inbounds [2 x %"struct.btSoftBody::Body"], [2 x %"struct.btSoftBody::Body"]* %m_bodies590, i32 0, i32 0
  %call592 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK10btSoftBody4Body5xformEv(%"struct.btSoftBody::Body"* %arrayidx591)
  %call593 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %call592)
  %702 = bitcast %class.btVector3* %ref.tmp594 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %702) #6
  %703 = bitcast float* %ref.tmp595 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %703) #6
  store float 1.000000e+00, float* %ref.tmp595, align 4, !tbaa !8
  %704 = bitcast float* %ref.tmp596 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %704) #6
  store float 1.000000e+00, float* %ref.tmp596, align 4, !tbaa !8
  %705 = bitcast float* %ref.tmp597 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %705) #6
  store float 0.000000e+00, float* %ref.tmp597, align 4, !tbaa !8
  %call598 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp594, float* nonnull align 4 dereferenceable(4) %ref.tmp595, float* nonnull align 4 dereferenceable(4) %ref.tmp596, float* nonnull align 4 dereferenceable(4) %ref.tmp597)
  %706 = bitcast %class.btIDebugDraw* %700 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable599 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %706, align 4, !tbaa !25
  %vfn600 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable599, i64 2
  %707 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn600, align 4
  call void %707(%class.btIDebugDraw* %700, %class.btVector3* nonnull align 4 dereferenceable(16) %call593, %class.btVector3* nonnull align 4 dereferenceable(16) %a0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp594)
  %708 = bitcast float* %ref.tmp597 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %708) #6
  %709 = bitcast float* %ref.tmp596 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %709) #6
  %710 = bitcast float* %ref.tmp595 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %710) #6
  %711 = bitcast %class.btVector3* %ref.tmp594 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %711) #6
  %712 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %713 = load %"struct.btSoftBody::Joint"*, %"struct.btSoftBody::Joint"** %pj, align 4, !tbaa !2
  %m_bodies601 = getelementptr inbounds %"struct.btSoftBody::Joint", %"struct.btSoftBody::Joint"* %713, i32 0, i32 1
  %arrayidx602 = getelementptr inbounds [2 x %"struct.btSoftBody::Body"], [2 x %"struct.btSoftBody::Body"]* %m_bodies601, i32 0, i32 1
  %call603 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK10btSoftBody4Body5xformEv(%"struct.btSoftBody::Body"* %arrayidx602)
  %call604 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %call603)
  %714 = bitcast %class.btVector3* %ref.tmp605 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %714) #6
  %715 = bitcast float* %ref.tmp606 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %715) #6
  store float 0.000000e+00, float* %ref.tmp606, align 4, !tbaa !8
  %716 = bitcast float* %ref.tmp607 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %716) #6
  store float 1.000000e+00, float* %ref.tmp607, align 4, !tbaa !8
  %717 = bitcast float* %ref.tmp608 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %717) #6
  store float 1.000000e+00, float* %ref.tmp608, align 4, !tbaa !8
  %call609 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp605, float* nonnull align 4 dereferenceable(4) %ref.tmp606, float* nonnull align 4 dereferenceable(4) %ref.tmp607, float* nonnull align 4 dereferenceable(4) %ref.tmp608)
  %718 = bitcast %class.btIDebugDraw* %712 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable610 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %718, align 4, !tbaa !25
  %vfn611 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable610, i64 2
  %719 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn611, align 4
  call void %719(%class.btIDebugDraw* %712, %class.btVector3* nonnull align 4 dereferenceable(16) %call604, %class.btVector3* nonnull align 4 dereferenceable(16) %a1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp605)
  %720 = bitcast float* %ref.tmp608 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %720) #6
  %721 = bitcast float* %ref.tmp607 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %721) #6
  %722 = bitcast float* %ref.tmp606 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %722) #6
  %723 = bitcast %class.btVector3* %ref.tmp605 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %723) #6
  %724 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %725 = bitcast %class.btVector3* %ref.tmp612 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %725) #6
  %726 = bitcast float* %ref.tmp613 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %726) #6
  store float 1.000000e+00, float* %ref.tmp613, align 4, !tbaa !8
  %727 = bitcast float* %ref.tmp614 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %727) #6
  store float 1.000000e+00, float* %ref.tmp614, align 4, !tbaa !8
  %728 = bitcast float* %ref.tmp615 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %728) #6
  store float 0.000000e+00, float* %ref.tmp615, align 4, !tbaa !8
  %call616 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp612, float* nonnull align 4 dereferenceable(4) %ref.tmp613, float* nonnull align 4 dereferenceable(4) %ref.tmp614, float* nonnull align 4 dereferenceable(4) %ref.tmp615)
  call void @_ZL10drawVertexP12btIDebugDrawRK9btVector3fS3_(%class.btIDebugDraw* %724, %class.btVector3* nonnull align 4 dereferenceable(16) %a0, float 2.500000e-01, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp612)
  %729 = bitcast float* %ref.tmp615 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %729) #6
  %730 = bitcast float* %ref.tmp614 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %730) #6
  %731 = bitcast float* %ref.tmp613 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %731) #6
  %732 = bitcast %class.btVector3* %ref.tmp612 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %732) #6
  %733 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %734 = bitcast %class.btVector3* %ref.tmp617 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %734) #6
  %735 = bitcast float* %ref.tmp618 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %735) #6
  store float 0.000000e+00, float* %ref.tmp618, align 4, !tbaa !8
  %736 = bitcast float* %ref.tmp619 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %736) #6
  store float 1.000000e+00, float* %ref.tmp619, align 4, !tbaa !8
  %737 = bitcast float* %ref.tmp620 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %737) #6
  store float 1.000000e+00, float* %ref.tmp620, align 4, !tbaa !8
  %call621 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp617, float* nonnull align 4 dereferenceable(4) %ref.tmp618, float* nonnull align 4 dereferenceable(4) %ref.tmp619, float* nonnull align 4 dereferenceable(4) %ref.tmp620)
  call void @_ZL10drawVertexP12btIDebugDrawRK9btVector3fS3_(%class.btIDebugDraw* %733, %class.btVector3* nonnull align 4 dereferenceable(16) %a1, float 2.500000e-01, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp617)
  %738 = bitcast float* %ref.tmp620 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %738) #6
  %739 = bitcast float* %ref.tmp619 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %739) #6
  %740 = bitcast float* %ref.tmp618 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %740) #6
  %741 = bitcast %class.btVector3* %ref.tmp617 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %741) #6
  %742 = bitcast %class.btVector3* %a1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %742) #6
  %743 = bitcast %class.btVector3* %a0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %743) #6
  %744 = bitcast %"struct.btSoftBody::LJoint"** %pjl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %744) #6
  br label %sw.epilog

sw.bb622:                                         ; preds = %for.body576
  %745 = bitcast %class.btVector3* %o0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %745) #6
  %746 = load %"struct.btSoftBody::Joint"*, %"struct.btSoftBody::Joint"** %pj, align 4, !tbaa !2
  %m_bodies623 = getelementptr inbounds %"struct.btSoftBody::Joint", %"struct.btSoftBody::Joint"* %746, i32 0, i32 1
  %arrayidx624 = getelementptr inbounds [2 x %"struct.btSoftBody::Body"], [2 x %"struct.btSoftBody::Body"]* %m_bodies623, i32 0, i32 0
  %call625 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK10btSoftBody4Body5xformEv(%"struct.btSoftBody::Body"* %arrayidx624)
  %call626 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %call625)
  %747 = bitcast %class.btVector3* %o0 to i8*
  %748 = bitcast %class.btVector3* %call626 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %747, i8* align 4 %748, i32 16, i1 false), !tbaa.struct !23
  %749 = bitcast %class.btVector3* %o1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %749) #6
  %750 = load %"struct.btSoftBody::Joint"*, %"struct.btSoftBody::Joint"** %pj, align 4, !tbaa !2
  %m_bodies627 = getelementptr inbounds %"struct.btSoftBody::Joint", %"struct.btSoftBody::Joint"* %750, i32 0, i32 1
  %arrayidx628 = getelementptr inbounds [2 x %"struct.btSoftBody::Body"], [2 x %"struct.btSoftBody::Body"]* %m_bodies627, i32 0, i32 1
  %call629 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK10btSoftBody4Body5xformEv(%"struct.btSoftBody::Body"* %arrayidx628)
  %call630 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %call629)
  %751 = bitcast %class.btVector3* %o1 to i8*
  %752 = bitcast %class.btVector3* %call630 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %751, i8* align 4 %752, i32 16, i1 false), !tbaa.struct !23
  %753 = bitcast %class.btVector3* %a0631 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %753) #6
  %754 = load %"struct.btSoftBody::Joint"*, %"struct.btSoftBody::Joint"** %pj, align 4, !tbaa !2
  %m_bodies632 = getelementptr inbounds %"struct.btSoftBody::Joint", %"struct.btSoftBody::Joint"* %754, i32 0, i32 1
  %arrayidx633 = getelementptr inbounds [2 x %"struct.btSoftBody::Body"], [2 x %"struct.btSoftBody::Body"]* %m_bodies632, i32 0, i32 0
  %call634 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK10btSoftBody4Body5xformEv(%"struct.btSoftBody::Body"* %arrayidx633)
  %call635 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call634)
  %755 = load %"struct.btSoftBody::Joint"*, %"struct.btSoftBody::Joint"** %pj, align 4, !tbaa !2
  %m_refs636 = getelementptr inbounds %"struct.btSoftBody::Joint", %"struct.btSoftBody::Joint"* %755, i32 0, i32 2
  %arrayidx637 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %m_refs636, i32 0, i32 0
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %a0631, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call635, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx637)
  %756 = bitcast %class.btVector3* %a1638 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %756) #6
  %757 = load %"struct.btSoftBody::Joint"*, %"struct.btSoftBody::Joint"** %pj, align 4, !tbaa !2
  %m_bodies639 = getelementptr inbounds %"struct.btSoftBody::Joint", %"struct.btSoftBody::Joint"* %757, i32 0, i32 1
  %arrayidx640 = getelementptr inbounds [2 x %"struct.btSoftBody::Body"], [2 x %"struct.btSoftBody::Body"]* %m_bodies639, i32 0, i32 1
  %call641 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK10btSoftBody4Body5xformEv(%"struct.btSoftBody::Body"* %arrayidx640)
  %call642 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call641)
  %758 = load %"struct.btSoftBody::Joint"*, %"struct.btSoftBody::Joint"** %pj, align 4, !tbaa !2
  %m_refs643 = getelementptr inbounds %"struct.btSoftBody::Joint", %"struct.btSoftBody::Joint"* %758, i32 0, i32 2
  %arrayidx644 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %m_refs643, i32 0, i32 1
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %a1638, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call642, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx644)
  %759 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %760 = bitcast %class.btVector3* %ref.tmp645 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %760) #6
  %761 = bitcast %class.btVector3* %ref.tmp646 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %761) #6
  %762 = bitcast float* %ref.tmp647 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %762) #6
  store float 1.000000e+01, float* %ref.tmp647, align 4, !tbaa !8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp646, %class.btVector3* nonnull align 4 dereferenceable(16) %a0631, float* nonnull align 4 dereferenceable(4) %ref.tmp647)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp645, %class.btVector3* nonnull align 4 dereferenceable(16) %o0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp646)
  %763 = bitcast %class.btVector3* %ref.tmp648 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %763) #6
  %764 = bitcast float* %ref.tmp649 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %764) #6
  store float 1.000000e+00, float* %ref.tmp649, align 4, !tbaa !8
  %765 = bitcast float* %ref.tmp650 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %765) #6
  store float 1.000000e+00, float* %ref.tmp650, align 4, !tbaa !8
  %766 = bitcast float* %ref.tmp651 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %766) #6
  store float 0.000000e+00, float* %ref.tmp651, align 4, !tbaa !8
  %call652 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp648, float* nonnull align 4 dereferenceable(4) %ref.tmp649, float* nonnull align 4 dereferenceable(4) %ref.tmp650, float* nonnull align 4 dereferenceable(4) %ref.tmp651)
  %767 = bitcast %class.btIDebugDraw* %759 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable653 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %767, align 4, !tbaa !25
  %vfn654 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable653, i64 2
  %768 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn654, align 4
  call void %768(%class.btIDebugDraw* %759, %class.btVector3* nonnull align 4 dereferenceable(16) %o0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp645, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp648)
  %769 = bitcast float* %ref.tmp651 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %769) #6
  %770 = bitcast float* %ref.tmp650 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %770) #6
  %771 = bitcast float* %ref.tmp649 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %771) #6
  %772 = bitcast %class.btVector3* %ref.tmp648 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %772) #6
  %773 = bitcast float* %ref.tmp647 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %773) #6
  %774 = bitcast %class.btVector3* %ref.tmp646 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %774) #6
  %775 = bitcast %class.btVector3* %ref.tmp645 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %775) #6
  %776 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %777 = bitcast %class.btVector3* %ref.tmp655 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %777) #6
  %778 = bitcast %class.btVector3* %ref.tmp656 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %778) #6
  %779 = bitcast float* %ref.tmp657 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %779) #6
  store float 1.000000e+01, float* %ref.tmp657, align 4, !tbaa !8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp656, %class.btVector3* nonnull align 4 dereferenceable(16) %a1638, float* nonnull align 4 dereferenceable(4) %ref.tmp657)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp655, %class.btVector3* nonnull align 4 dereferenceable(16) %o0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp656)
  %780 = bitcast %class.btVector3* %ref.tmp658 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %780) #6
  %781 = bitcast float* %ref.tmp659 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %781) #6
  store float 1.000000e+00, float* %ref.tmp659, align 4, !tbaa !8
  %782 = bitcast float* %ref.tmp660 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %782) #6
  store float 1.000000e+00, float* %ref.tmp660, align 4, !tbaa !8
  %783 = bitcast float* %ref.tmp661 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %783) #6
  store float 0.000000e+00, float* %ref.tmp661, align 4, !tbaa !8
  %call662 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp658, float* nonnull align 4 dereferenceable(4) %ref.tmp659, float* nonnull align 4 dereferenceable(4) %ref.tmp660, float* nonnull align 4 dereferenceable(4) %ref.tmp661)
  %784 = bitcast %class.btIDebugDraw* %776 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable663 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %784, align 4, !tbaa !25
  %vfn664 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable663, i64 2
  %785 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn664, align 4
  call void %785(%class.btIDebugDraw* %776, %class.btVector3* nonnull align 4 dereferenceable(16) %o0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp655, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp658)
  %786 = bitcast float* %ref.tmp661 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %786) #6
  %787 = bitcast float* %ref.tmp660 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %787) #6
  %788 = bitcast float* %ref.tmp659 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %788) #6
  %789 = bitcast %class.btVector3* %ref.tmp658 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %789) #6
  %790 = bitcast float* %ref.tmp657 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %790) #6
  %791 = bitcast %class.btVector3* %ref.tmp656 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %791) #6
  %792 = bitcast %class.btVector3* %ref.tmp655 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %792) #6
  %793 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %794 = bitcast %class.btVector3* %ref.tmp665 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %794) #6
  %795 = bitcast %class.btVector3* %ref.tmp666 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %795) #6
  %796 = bitcast float* %ref.tmp667 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %796) #6
  store float 1.000000e+01, float* %ref.tmp667, align 4, !tbaa !8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp666, %class.btVector3* nonnull align 4 dereferenceable(16) %a0631, float* nonnull align 4 dereferenceable(4) %ref.tmp667)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp665, %class.btVector3* nonnull align 4 dereferenceable(16) %o1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp666)
  %797 = bitcast %class.btVector3* %ref.tmp668 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %797) #6
  %798 = bitcast float* %ref.tmp669 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %798) #6
  store float 0.000000e+00, float* %ref.tmp669, align 4, !tbaa !8
  %799 = bitcast float* %ref.tmp670 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %799) #6
  store float 1.000000e+00, float* %ref.tmp670, align 4, !tbaa !8
  %800 = bitcast float* %ref.tmp671 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %800) #6
  store float 1.000000e+00, float* %ref.tmp671, align 4, !tbaa !8
  %call672 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp668, float* nonnull align 4 dereferenceable(4) %ref.tmp669, float* nonnull align 4 dereferenceable(4) %ref.tmp670, float* nonnull align 4 dereferenceable(4) %ref.tmp671)
  %801 = bitcast %class.btIDebugDraw* %793 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable673 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %801, align 4, !tbaa !25
  %vfn674 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable673, i64 2
  %802 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn674, align 4
  call void %802(%class.btIDebugDraw* %793, %class.btVector3* nonnull align 4 dereferenceable(16) %o1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp665, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp668)
  %803 = bitcast float* %ref.tmp671 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %803) #6
  %804 = bitcast float* %ref.tmp670 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %804) #6
  %805 = bitcast float* %ref.tmp669 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %805) #6
  %806 = bitcast %class.btVector3* %ref.tmp668 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %806) #6
  %807 = bitcast float* %ref.tmp667 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %807) #6
  %808 = bitcast %class.btVector3* %ref.tmp666 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %808) #6
  %809 = bitcast %class.btVector3* %ref.tmp665 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %809) #6
  %810 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %811 = bitcast %class.btVector3* %ref.tmp675 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %811) #6
  %812 = bitcast %class.btVector3* %ref.tmp676 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %812) #6
  %813 = bitcast float* %ref.tmp677 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %813) #6
  store float 1.000000e+01, float* %ref.tmp677, align 4, !tbaa !8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp676, %class.btVector3* nonnull align 4 dereferenceable(16) %a1638, float* nonnull align 4 dereferenceable(4) %ref.tmp677)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp675, %class.btVector3* nonnull align 4 dereferenceable(16) %o1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp676)
  %814 = bitcast %class.btVector3* %ref.tmp678 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %814) #6
  %815 = bitcast float* %ref.tmp679 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %815) #6
  store float 0.000000e+00, float* %ref.tmp679, align 4, !tbaa !8
  %816 = bitcast float* %ref.tmp680 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %816) #6
  store float 1.000000e+00, float* %ref.tmp680, align 4, !tbaa !8
  %817 = bitcast float* %ref.tmp681 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %817) #6
  store float 1.000000e+00, float* %ref.tmp681, align 4, !tbaa !8
  %call682 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp678, float* nonnull align 4 dereferenceable(4) %ref.tmp679, float* nonnull align 4 dereferenceable(4) %ref.tmp680, float* nonnull align 4 dereferenceable(4) %ref.tmp681)
  %818 = bitcast %class.btIDebugDraw* %810 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable683 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %818, align 4, !tbaa !25
  %vfn684 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable683, i64 2
  %819 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn684, align 4
  call void %819(%class.btIDebugDraw* %810, %class.btVector3* nonnull align 4 dereferenceable(16) %o1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp675, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp678)
  %820 = bitcast float* %ref.tmp681 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %820) #6
  %821 = bitcast float* %ref.tmp680 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %821) #6
  %822 = bitcast float* %ref.tmp679 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %822) #6
  %823 = bitcast %class.btVector3* %ref.tmp678 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %823) #6
  %824 = bitcast float* %ref.tmp677 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %824) #6
  %825 = bitcast %class.btVector3* %ref.tmp676 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %825) #6
  %826 = bitcast %class.btVector3* %ref.tmp675 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %826) #6
  store i32 46, i32* %cleanup.dest.slot, align 4
  %827 = bitcast %class.btVector3* %a1638 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %827) #6
  %828 = bitcast %class.btVector3* %a0631 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %828) #6
  %829 = bitcast %class.btVector3* %o1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %829) #6
  %830 = bitcast %class.btVector3* %o0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %830) #6
  br label %sw.epilog

sw.default:                                       ; preds = %for.body576
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb622, %sw.bb
  %831 = bitcast %"struct.btSoftBody::Joint"** %pj to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %831) #6
  br label %for.inc689

for.inc689:                                       ; preds = %sw.epilog
  %832 = load i32, i32* %i, align 4, !tbaa !6
  %inc690 = add nsw i32 %832, 1
  store i32 %inc690, i32* %i, align 4, !tbaa !6
  br label %for.cond573

for.end691:                                       ; preds = %for.cond573
  br label %if.end692

if.end692:                                        ; preds = %for.end691, %if.end569
  %833 = bitcast i32* %nj to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %833) #6
  %834 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %834) #6
  %835 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %835) #6
  %836 = bitcast %class.btVector3* %ccolor to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %836) #6
  %837 = bitcast %class.btVector3* %ncolor to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %837) #6
  %838 = bitcast %class.btVector3* %lcolor to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %838) #6
  %839 = bitcast float* %nscl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %839) #6
  %840 = bitcast float* %scl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %840) #6
  ret void

unreachable:                                      ; preds = %cleanup519, %cleanup453, %cleanup340, %cleanup211, %cleanup174, %cleanup
  unreachable
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !8
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !8
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !8
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !8
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

declare void @srand(i32) #3

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPN10btSoftBody7ClusterEE4sizeEv(%class.btAlignedObjectArray.77* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.77*, align 4
  store %class.btAlignedObjectArray.77* %this, %class.btAlignedObjectArray.77** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.77*, %class.btAlignedObjectArray.77** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !44
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.btSoftBody::Cluster"** @_ZN20btAlignedObjectArrayIPN10btSoftBody7ClusterEEixEi(%class.btAlignedObjectArray.77* %this, i32 %n) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.77*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.77* %this, %class.btAlignedObjectArray.77** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.77*, %class.btAlignedObjectArray.77** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 4
  %0 = load %"struct.btSoftBody::Cluster"**, %"struct.btSoftBody::Cluster"*** %m_data, align 4, !tbaa !47
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %"struct.btSoftBody::Cluster"*, %"struct.btSoftBody::Cluster"** %0, i32 %1
  ret %"struct.btSoftBody::Cluster"** %arrayidx
}

declare i32 @rand() #3

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #4 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !8
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !8
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !8
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !8
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !8
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector310normalizedEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %norm = alloca %class.btVector3, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast %class.btVector3* %norm to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #6
  %1 = bitcast %class.btVector3* %norm to i8*
  %2 = bitcast %class.btVector3* %this1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !23
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %norm)
  %3 = bitcast %class.btVector3* %agg.result to i8*
  %4 = bitcast %class.btVector3* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false), !tbaa.struct !23
  %5 = bitcast %class.btVector3* %norm to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #6
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

define linkonce_odr hidden %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.8* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.9* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator.9* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray.8* %this1)
  ret %class.btAlignedObjectArray.8* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.8* %this, i32 %newsize, %class.btVector3* nonnull align 4 dereferenceable(16) %fillData) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btVector3*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !6
  store %class.btVector3* %fillData, %class.btVector3** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !6
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %2 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  store i32 %4, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %8 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !48
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end17

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray.8* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  %14 = load i32, i32* %curSize, align 4, !tbaa !6
  store i32 %14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc14, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !6
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  br label %for.end16

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %18 = load %class.btVector3*, %class.btVector3** %m_data11, align 4, !tbaa !48
  %19 = load i32, i32* %i6, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds %class.btVector3, %class.btVector3* %18, i32 %19
  %20 = bitcast %class.btVector3* %arrayidx12 to i8*
  %call13 = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %20)
  %21 = bitcast i8* %call13 to %class.btVector3*
  %22 = load %class.btVector3*, %class.btVector3** %fillData.addr, align 4, !tbaa !2
  %23 = bitcast %class.btVector3* %21 to i8*
  %24 = bitcast %class.btVector3* %22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false), !tbaa.struct !23
  br label %for.inc14

for.inc14:                                        ; preds = %for.body10
  %25 = load i32, i32* %i6, align 4, !tbaa !6
  %inc15 = add nsw i32 %25, 1
  store i32 %inc15, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.end16:                                        ; preds = %for.cond.cleanup9
  br label %if.end17

if.end17:                                         ; preds = %for.end16, %for.end
  %26 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  store i32 %26, i32* %m_size, align 4, !tbaa !49
  %27 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPN10btSoftBody4NodeEE4sizeEv(%class.btAlignedObjectArray.60* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.60*, align 4
  store %class.btAlignedObjectArray.60* %this, %class.btAlignedObjectArray.60** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.60*, %class.btAlignedObjectArray.60** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.60, %class.btAlignedObjectArray.60* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !50
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !49
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.btSoftBody::Node"** @_ZN20btAlignedObjectArrayIPN10btSoftBody4NodeEEixEi(%class.btAlignedObjectArray.60* %this, i32 %n) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.60*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.60* %this, %class.btAlignedObjectArray.60** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.60*, %class.btAlignedObjectArray.60** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.60, %class.btAlignedObjectArray.60* %this1, i32 0, i32 4
  %0 = load %"struct.btSoftBody::Node"**, %"struct.btSoftBody::Node"*** %m_data, align 4, !tbaa !51
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %0, i32 %1
  ret %"struct.btSoftBody::Node"** %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %this, i32 %n) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !48
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %1
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btConvexHullComputer* @_ZN20btConvexHullComputerC2Ev(%class.btConvexHullComputer* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btConvexHullComputer*, align 4
  store %class.btConvexHullComputer* %this, %class.btConvexHullComputer** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexHullComputer*, %class.btConvexHullComputer** %this.addr, align 4
  %vertices = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.8* %vertices)
  %edges = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.89* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEEC2Ev(%class.btAlignedObjectArray.89* %edges)
  %faces = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 2
  %call3 = call %class.btAlignedObjectArray.85* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.85* %faces)
  ret %class.btConvexHullComputer* %this1
}

define linkonce_odr hidden float @_ZN20btConvexHullComputer7computeEPKfiiff(%class.btConvexHullComputer* %this, float* %coords, i32 %stride, i32 %count, float %shrink, float %shrinkClamp) #0 comdat {
entry:
  %this.addr = alloca %class.btConvexHullComputer*, align 4
  %coords.addr = alloca float*, align 4
  %stride.addr = alloca i32, align 4
  %count.addr = alloca i32, align 4
  %shrink.addr = alloca float, align 4
  %shrinkClamp.addr = alloca float, align 4
  store %class.btConvexHullComputer* %this, %class.btConvexHullComputer** %this.addr, align 4, !tbaa !2
  store float* %coords, float** %coords.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %count, i32* %count.addr, align 4, !tbaa !6
  store float %shrink, float* %shrink.addr, align 4, !tbaa !8
  store float %shrinkClamp, float* %shrinkClamp.addr, align 4, !tbaa !8
  %this1 = load %class.btConvexHullComputer*, %class.btConvexHullComputer** %this.addr, align 4
  %0 = load float*, float** %coords.addr, align 4, !tbaa !2
  %1 = bitcast float* %0 to i8*
  %2 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %3 = load i32, i32* %count.addr, align 4, !tbaa !6
  %4 = load float, float* %shrink.addr, align 4, !tbaa !8
  %5 = load float, float* %shrinkClamp.addr, align 4, !tbaa !8
  %call = call float @_ZN20btConvexHullComputer7computeEPKvbiiff(%class.btConvexHullComputer* %this1, i8* %1, i1 zeroext false, i32 %2, i32 %3, float %4, float %5)
  ret float %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.85* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.85*, align 4
  store %class.btAlignedObjectArray.85* %this, %class.btAlignedObjectArray.85** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.85*, %class.btAlignedObjectArray.85** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !52
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.85* %this, i32 %n) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.85*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.85* %this, %class.btAlignedObjectArray.85** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.85*, %class.btAlignedObjectArray.85** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !55
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"class.btConvexHullComputer::Edge"* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEEixEi(%class.btAlignedObjectArray.89* %this, i32 %n) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 4
  %0 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %m_data, align 4, !tbaa !56
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %0, i32 %1
  ret %"class.btConvexHullComputer::Edge"* %arrayidx
}

define linkonce_odr hidden %"class.btConvexHullComputer::Edge"* @_ZNK20btConvexHullComputer4Edge17getNextEdgeOfFaceEv(%"class.btConvexHullComputer::Edge"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullComputer::Edge"*, align 4
  store %"class.btConvexHullComputer::Edge"* %this, %"class.btConvexHullComputer::Edge"** %this.addr, align 4, !tbaa !2
  %this1 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %this.addr, align 4
  %reverse = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %this1, i32 0, i32 1
  %0 = load i32, i32* %reverse, align 4, !tbaa !59
  %add.ptr = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %this1, i32 %0
  %call = call %"class.btConvexHullComputer::Edge"* @_ZNK20btConvexHullComputer4Edge19getNextEdgeOfVertexEv(%"class.btConvexHullComputer::Edge"* %add.ptr)
  ret %"class.btConvexHullComputer::Edge"* %call
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK20btConvexHullComputer4Edge15getSourceVertexEv(%"class.btConvexHullComputer::Edge"* %this) #5 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullComputer::Edge"*, align 4
  store %"class.btConvexHullComputer::Edge"* %this, %"class.btConvexHullComputer::Edge"** %this.addr, align 4, !tbaa !2
  %this1 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %this.addr, align 4
  %reverse = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %this1, i32 0, i32 1
  %0 = load i32, i32* %reverse, align 4, !tbaa !59
  %add.ptr = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %this1, i32 %0
  %targetVertex = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %add.ptr, i32 0, i32 2
  %1 = load i32, i32* %targetVertex, align 4, !tbaa !61
  ret i32 %1
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK20btConvexHullComputer4Edge15getTargetVertexEv(%"class.btConvexHullComputer::Edge"* %this) #5 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullComputer::Edge"*, align 4
  store %"class.btConvexHullComputer::Edge"* %this, %"class.btConvexHullComputer::Edge"** %this.addr, align 4, !tbaa !2
  %this1 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %this.addr, align 4
  %targetVertex = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %this1, i32 0, i32 2
  %0 = load i32, i32* %targetVertex, align 4, !tbaa !61
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btConvexHullComputer* @_ZN20btConvexHullComputerD2Ev(%class.btConvexHullComputer* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btConvexHullComputer*, align 4
  store %class.btConvexHullComputer* %this, %class.btConvexHullComputer** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexHullComputer*, %class.btConvexHullComputer** %this.addr, align 4
  %faces = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 2
  %call = call %class.btAlignedObjectArray.85* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.85* %faces) #6
  %edges = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.89* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEED2Ev(%class.btAlignedObjectArray.89* %edges) #6
  %vertices = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 0
  %call3 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.8* %vertices) #6
  ret %class.btConvexHullComputer* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.8* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray.8* %this1)
  ret %class.btAlignedObjectArray.8* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4NodeEE4sizeEv(%class.btAlignedObjectArray.25* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.25*, align 4
  store %class.btAlignedObjectArray.25* %this, %class.btAlignedObjectArray.25** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.25*, %class.btAlignedObjectArray.25** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.25, %class.btAlignedObjectArray.25* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !62
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(101) %"struct.btSoftBody::Node"* @_ZN20btAlignedObjectArrayIN10btSoftBody4NodeEEixEi(%class.btAlignedObjectArray.25* %this, i32 %n) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.25*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.25* %this, %class.btAlignedObjectArray.25** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.25*, %class.btAlignedObjectArray.25** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.25, %class.btAlignedObjectArray.25* %this1, i32 0, i32 4
  %0 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %m_data, align 4, !tbaa !65
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %0, i32 %1
  ret %"struct.btSoftBody::Node"* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !8
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !8
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !8
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !8
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !8
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !8
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !8
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !8
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !8
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !8
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4LinkEE4sizeEv(%class.btAlignedObjectArray.29* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.29*, align 4
  store %class.btAlignedObjectArray.29* %this, %class.btAlignedObjectArray.29** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.29*, %class.btAlignedObjectArray.29** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.29, %class.btAlignedObjectArray.29* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !66
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(52) %"struct.btSoftBody::Link"* @_ZN20btAlignedObjectArrayIN10btSoftBody4LinkEEixEi(%class.btAlignedObjectArray.29* %this, i32 %n) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.29*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.29* %this, %class.btAlignedObjectArray.29** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.29*, %class.btAlignedObjectArray.29** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.29, %class.btAlignedObjectArray.29* %this1, i32 0, i32 4
  %0 = load %"struct.btSoftBody::Link"*, %"struct.btSoftBody::Link"** %m_data, align 4, !tbaa !69
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %"struct.btSoftBody::Link", %"struct.btSoftBody::Link"* %0, i32 %1
  ret %"struct.btSoftBody::Link"* %arrayidx
}

; Function Attrs: nounwind
declare i32 @__cxa_guard_acquire(i32*) #6

; Function Attrs: argmemonly nounwind willreturn
declare {}* @llvm.invariant.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
declare void @__cxa_guard_release(i32*) #6

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody8RContactEE4sizeEv(%class.btAlignedObjectArray.49* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.49*, align 4
  store %class.btAlignedObjectArray.49* %this, %class.btAlignedObjectArray.49** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.49*, %class.btAlignedObjectArray.49** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.49, %class.btAlignedObjectArray.49* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !70
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(104) %"struct.btSoftBody::RContact"* @_ZN20btAlignedObjectArrayIN10btSoftBody8RContactEEixEi(%class.btAlignedObjectArray.49* %this, i32 %n) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.49*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.49* %this, %class.btAlignedObjectArray.49** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.49*, %class.btAlignedObjectArray.49** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.49, %class.btAlignedObjectArray.49* %this1, i32 0, i32 4
  %0 = load %"struct.btSoftBody::RContact"*, %"struct.btSoftBody::RContact"** %m_data, align 4, !tbaa !73
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %"struct.btSoftBody::RContact", %"struct.btSoftBody::RContact"* %0, i32 %1
  ret %"struct.btSoftBody::RContact"* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %1 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  ret float %call
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_Z7btCrossRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %1 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK9btVector37minAxisEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %1 = load float, float* %arrayidx3, align 4, !tbaa !8
  %cmp = fcmp olt float %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 0
  %2 = load float, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %3 = load float, float* %arrayidx7, align 4, !tbaa !8
  %cmp8 = fcmp olt float %2, %3
  %4 = zext i1 %cmp8 to i64
  %cond = select i1 %cmp8, i32 0, i32 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 1
  %5 = load float, float* %arrayidx10, align 4, !tbaa !8
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %6 = load float, float* %arrayidx12, align 4, !tbaa !8
  %cmp13 = fcmp olt float %5, %6
  %7 = zext i1 %cmp13 to i64
  %cond14 = select i1 %cmp13, i32 1, i32 2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond15 = phi i32 [ %cond, %cond.true ], [ %cond14, %cond.false ]
  ret i32 %cond15
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4FaceEE4sizeEv(%class.btAlignedObjectArray.33* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.33*, align 4
  store %class.btAlignedObjectArray.33* %this, %class.btAlignedObjectArray.33** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.33*, %class.btAlignedObjectArray.33** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.33, %class.btAlignedObjectArray.33* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !74
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(44) %"struct.btSoftBody::Face"* @_ZN20btAlignedObjectArrayIN10btSoftBody4FaceEEixEi(%class.btAlignedObjectArray.33* %this, i32 %n) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.33*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.33* %this, %class.btAlignedObjectArray.33** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.33*, %class.btAlignedObjectArray.33** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.33, %class.btAlignedObjectArray.33* %this1, i32 0, i32 4
  %0 = load %"struct.btSoftBody::Face"*, %"struct.btSoftBody::Face"** %m_data, align 4, !tbaa !77
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %"struct.btSoftBody::Face", %"struct.btSoftBody::Face"* %0, i32 %1
  ret %"struct.btSoftBody::Face"* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZdvRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #4 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = load float*, float** %s.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !8
  %div = fdiv float 1.000000e+00, %3
  store float %div, float* %ref.tmp, align 4, !tbaa !8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %4 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody5TetraEE4sizeEv(%class.btAlignedObjectArray.37* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.37*, align 4
  store %class.btAlignedObjectArray.37* %this, %class.btAlignedObjectArray.37** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.37*, %class.btAlignedObjectArray.37** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.37, %class.btAlignedObjectArray.37* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !78
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(104) %"struct.btSoftBody::Tetra"* @_ZN20btAlignedObjectArrayIN10btSoftBody5TetraEEixEi(%class.btAlignedObjectArray.37* %this, i32 %n) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.37*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.37* %this, %class.btAlignedObjectArray.37** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.37*, %class.btAlignedObjectArray.37** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.37, %class.btAlignedObjectArray.37* %this1, i32 0, i32 4
  %0 = load %"struct.btSoftBody::Tetra"*, %"struct.btSoftBody::Tetra"** %m_data, align 4, !tbaa !81
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %"struct.btSoftBody::Tetra", %"struct.btSoftBody::Tetra"* %0, i32 %1
  ret %"struct.btSoftBody::Tetra"* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody6AnchorEE4sizeEv(%class.btAlignedObjectArray.41* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.41*, align 4
  store %class.btAlignedObjectArray.41* %this, %class.btAlignedObjectArray.41** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.41*, %class.btAlignedObjectArray.41** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.41* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !82
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(96) %"struct.btSoftBody::Anchor"* @_ZN20btAlignedObjectArrayIN10btSoftBody6AnchorEEixEi(%class.btAlignedObjectArray.41* %this, i32 %n) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.41*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.41* %this, %class.btAlignedObjectArray.41** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.41*, %class.btAlignedObjectArray.41** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.41* %this1, i32 0, i32 4
  %0 = load %"struct.btSoftBody::Anchor"*, %"struct.btSoftBody::Anchor"** %m_data, align 4, !tbaa !85
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %"struct.btSoftBody::Anchor", %"struct.btSoftBody::Anchor"* %0, i32 %1
  ret %"struct.btSoftBody::Anchor"* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

define internal void @_ZL10drawVertexP12btIDebugDrawRK9btVector3fS3_(%class.btIDebugDraw* %idraw, %class.btVector3* nonnull align 4 dereferenceable(16) %x, float %s, %class.btVector3* nonnull align 4 dereferenceable(16) %c) #0 {
entry:
  %idraw.addr = alloca %class.btIDebugDraw*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float, align 4
  %c.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp14 = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp21 = alloca %class.btVector3, align 4
  %ref.tmp22 = alloca %class.btVector3, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp24 = alloca float, align 4
  %ref.tmp26 = alloca %class.btVector3, align 4
  %ref.tmp27 = alloca %class.btVector3, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  store %class.btIDebugDraw* %idraw, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  store float %s, float* %s.addr, align 4, !tbaa !8
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4, !tbaa !2
  %0 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #6
  %2 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  %3 = bitcast %class.btVector3* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #6
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp1, float* nonnull align 4 dereferenceable(4) %s.addr, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1)
  %6 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #6
  %7 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  %8 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #6
  %9 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !8
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp5, float* nonnull align 4 dereferenceable(4) %s.addr, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %7, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %11 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %12 = bitcast %class.btIDebugDraw* %0 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %12, align 4, !tbaa !25
  %vfn = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %13 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %13(%class.btIDebugDraw* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %11)
  %14 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #6
  %15 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %16) #6
  %17 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %17) #6
  %18 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #6
  %19 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #6
  %20 = bitcast %class.btVector3* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %20) #6
  %21 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %21) #6
  %22 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %23 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %23) #6
  %24 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  %25 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %25) #6
  %26 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #6
  store float 0.000000e+00, float* %ref.tmp11, align 4, !tbaa !8
  %27 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #6
  store float 0.000000e+00, float* %ref.tmp12, align 4, !tbaa !8
  %call13 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %s.addr, float* nonnull align 4 dereferenceable(4) %ref.tmp12)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %24, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp10)
  %28 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %28) #6
  %29 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  %30 = bitcast %class.btVector3* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %30) #6
  %31 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #6
  store float 0.000000e+00, float* %ref.tmp16, align 4, !tbaa !8
  %32 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #6
  store float 0.000000e+00, float* %ref.tmp17, align 4, !tbaa !8
  %call18 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %s.addr, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp14, %class.btVector3* nonnull align 4 dereferenceable(16) %29, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp15)
  %33 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %34 = bitcast %class.btIDebugDraw* %22 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable19 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %34, align 4, !tbaa !25
  %vfn20 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable19, i64 2
  %35 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn20, align 4
  call void %35(%class.btIDebugDraw* %22, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp14, %class.btVector3* nonnull align 4 dereferenceable(16) %33)
  %36 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #6
  %37 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #6
  %38 = bitcast %class.btVector3* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %38) #6
  %39 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %39) #6
  %40 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #6
  %41 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #6
  %42 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %42) #6
  %43 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %43) #6
  %44 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %45 = bitcast %class.btVector3* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %45) #6
  %46 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  %47 = bitcast %class.btVector3* %ref.tmp22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %47) #6
  %48 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #6
  store float 0.000000e+00, float* %ref.tmp23, align 4, !tbaa !8
  %49 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #6
  store float 0.000000e+00, float* %ref.tmp24, align 4, !tbaa !8
  %call25 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp22, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp24, float* nonnull align 4 dereferenceable(4) %s.addr)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp21, %class.btVector3* nonnull align 4 dereferenceable(16) %46, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp22)
  %50 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %50) #6
  %51 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  %52 = bitcast %class.btVector3* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %52) #6
  %53 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #6
  store float 0.000000e+00, float* %ref.tmp28, align 4, !tbaa !8
  %54 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #6
  store float 0.000000e+00, float* %ref.tmp29, align 4, !tbaa !8
  %call30 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %s.addr)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp26, %class.btVector3* nonnull align 4 dereferenceable(16) %51, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp27)
  %55 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %56 = bitcast %class.btIDebugDraw* %44 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable31 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %56, align 4, !tbaa !25
  %vfn32 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable31, i64 2
  %57 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn32, align 4
  call void %57(%class.btIDebugDraw* %44, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp21, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp26, %class.btVector3* nonnull align 4 dereferenceable(16) %55)
  %58 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #6
  %59 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #6
  %60 = bitcast %class.btVector3* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %60) #6
  %61 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %61) #6
  %62 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #6
  %63 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #6
  %64 = bitcast %class.btVector3* %ref.tmp22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %64) #6
  %65 = bitcast %class.btVector3* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %65) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4NoteEE4sizeEv(%class.btAlignedObjectArray.20* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !86
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(60) %"struct.btSoftBody::Note"* @_ZN20btAlignedObjectArrayIN10btSoftBody4NoteEEixEi(%class.btAlignedObjectArray.20* %this, i32 %n) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %0 = load %"struct.btSoftBody::Note"*, %"struct.btSoftBody::Note"** %m_data, align 4, !tbaa !89
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %"struct.btSoftBody::Note", %"struct.btSoftBody::Note"* %0, i32 %1
  ret %"struct.btSoftBody::Note"* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !8
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !8
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !8
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !8
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !8
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !8
  ret %class.btVector3* %this1
}

define hidden void @_ZN17btSoftBodyHelpers12DrawNodeTreeEP10btSoftBodyP12btIDebugDrawii(%class.btSoftBody* %psb, %class.btIDebugDraw* %idraw, i32 %mindepth, i32 %maxdepth) #0 {
entry:
  %psb.addr = alloca %class.btSoftBody*, align 4
  %idraw.addr = alloca %class.btIDebugDraw*, align 4
  %mindepth.addr = alloca i32, align 4
  %maxdepth.addr = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  store %class.btSoftBody* %psb, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  store %class.btIDebugDraw* %idraw, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  store i32 %mindepth, i32* %mindepth.addr, align 4, !tbaa !6
  store i32 %maxdepth, i32* %maxdepth.addr, align 4, !tbaa !6
  %0 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %1 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_ndbvt = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %1, i32 0, i32 21
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %m_ndbvt, i32 0, i32 0
  %2 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root, align 4, !tbaa !90
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #6
  %4 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  store float 1.000000e+00, float* %ref.tmp1, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !8
  %6 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  store float 1.000000e+00, float* %ref.tmp3, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %7 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #6
  %8 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  store float 1.000000e+00, float* %ref.tmp5, align 4, !tbaa !8
  %9 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  store float 1.000000e+00, float* %ref.tmp6, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  store float 1.000000e+00, float* %ref.tmp7, align 4, !tbaa !8
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %11 = load i32, i32* %mindepth.addr, align 4, !tbaa !6
  %12 = load i32, i32* %maxdepth.addr, align 4, !tbaa !6
  call void @_ZL8drawTreeP12btIDebugDrawPK10btDbvtNodeiRK9btVector3S6_ii(%class.btIDebugDraw* %0, %struct.btDbvtNode* %2, i32 0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4, i32 %11, i32 %12)
  %13 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #6
  %14 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #6
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %16) #6
  %17 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  %18 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #6
  %19 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #6
  %20 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %20) #6
  ret void
}

define hidden void @_ZN17btSoftBodyHelpers12DrawFaceTreeEP10btSoftBodyP12btIDebugDrawii(%class.btSoftBody* %psb, %class.btIDebugDraw* %idraw, i32 %mindepth, i32 %maxdepth) #0 {
entry:
  %psb.addr = alloca %class.btSoftBody*, align 4
  %idraw.addr = alloca %class.btIDebugDraw*, align 4
  %mindepth.addr = alloca i32, align 4
  %maxdepth.addr = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  store %class.btSoftBody* %psb, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  store %class.btIDebugDraw* %idraw, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  store i32 %mindepth, i32* %mindepth.addr, align 4, !tbaa !6
  store i32 %maxdepth, i32* %maxdepth.addr, align 4, !tbaa !6
  %0 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %1 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_fdbvt = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %1, i32 0, i32 22
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %m_fdbvt, i32 0, i32 0
  %2 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root, align 4, !tbaa !115
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #6
  %4 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  store float 0.000000e+00, float* %ref.tmp1, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  store float 1.000000e+00, float* %ref.tmp2, align 4, !tbaa !8
  %6 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %7 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #6
  %8 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  store float 1.000000e+00, float* %ref.tmp5, align 4, !tbaa !8
  %9 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !8
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %11 = load i32, i32* %mindepth.addr, align 4, !tbaa !6
  %12 = load i32, i32* %maxdepth.addr, align 4, !tbaa !6
  call void @_ZL8drawTreeP12btIDebugDrawPK10btDbvtNodeiRK9btVector3S6_ii(%class.btIDebugDraw* %0, %struct.btDbvtNode* %2, i32 0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4, i32 %11, i32 %12)
  %13 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #6
  %14 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #6
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %16) #6
  %17 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  %18 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #6
  %19 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #6
  %20 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %20) #6
  ret void
}

define hidden void @_ZN17btSoftBodyHelpers15DrawClusterTreeEP10btSoftBodyP12btIDebugDrawii(%class.btSoftBody* %psb, %class.btIDebugDraw* %idraw, i32 %mindepth, i32 %maxdepth) #0 {
entry:
  %psb.addr = alloca %class.btSoftBody*, align 4
  %idraw.addr = alloca %class.btIDebugDraw*, align 4
  %mindepth.addr = alloca i32, align 4
  %maxdepth.addr = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  store %class.btSoftBody* %psb, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  store %class.btIDebugDraw* %idraw, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  store i32 %mindepth, i32* %mindepth.addr, align 4, !tbaa !6
  store i32 %maxdepth, i32* %maxdepth.addr, align 4, !tbaa !6
  %0 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %1 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_cdbvt = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %1, i32 0, i32 23
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %m_cdbvt, i32 0, i32 0
  %2 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root, align 4, !tbaa !116
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #6
  %4 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  store float 0.000000e+00, float* %ref.tmp1, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  store float 1.000000e+00, float* %ref.tmp2, align 4, !tbaa !8
  %6 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  store float 1.000000e+00, float* %ref.tmp3, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %7 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #6
  %8 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  store float 1.000000e+00, float* %ref.tmp5, align 4, !tbaa !8
  %9 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !8
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %11 = load i32, i32* %mindepth.addr, align 4, !tbaa !6
  %12 = load i32, i32* %maxdepth.addr, align 4, !tbaa !6
  call void @_ZL8drawTreeP12btIDebugDrawPK10btDbvtNodeiRK9btVector3S6_ii(%class.btIDebugDraw* %0, %struct.btDbvtNode* %2, i32 0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4, i32 %11, i32 %12)
  %13 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #6
  %14 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #6
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %16) #6
  %17 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  %18 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #6
  %19 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #6
  %20 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %20) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPN10btSoftBody5JointEE4sizeEv(%class.btAlignedObjectArray.57* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.57*, align 4
  store %class.btAlignedObjectArray.57* %this, %class.btAlignedObjectArray.57** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.57*, %class.btAlignedObjectArray.57** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.57, %class.btAlignedObjectArray.57* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !117
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.btSoftBody::Joint"** @_ZN20btAlignedObjectArrayIPN10btSoftBody5JointEEixEi(%class.btAlignedObjectArray.57* %this, i32 %n) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.57*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.57* %this, %class.btAlignedObjectArray.57** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.57*, %class.btAlignedObjectArray.57** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.57, %class.btAlignedObjectArray.57* %this1, i32 0, i32 4
  %0 = load %"struct.btSoftBody::Joint"**, %"struct.btSoftBody::Joint"*** %m_data, align 4, !tbaa !118
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %"struct.btSoftBody::Joint"*, %"struct.btSoftBody::Joint"** %0, i32 %1
  ret %"struct.btSoftBody::Joint"** %arrayidx
}

define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK10btSoftBody4Body5xformEv(%"struct.btSoftBody::Body"* %this) #0 comdat {
entry:
  %retval = alloca %class.btTransform*, align 4
  %this.addr = alloca %"struct.btSoftBody::Body"*, align 4
  store %"struct.btSoftBody::Body"* %this, %"struct.btSoftBody::Body"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btSoftBody::Body"*, %"struct.btSoftBody::Body"** %this.addr, align 4
  %0 = load atomic i8, i8* bitcast (i32* @_ZGVZNK10btSoftBody4Body5xformEvE8identity to i8*) acquire, align 4
  %1 = and i8 %0, 1
  %guard.uninitialized = icmp eq i8 %1, 0
  br i1 %guard.uninitialized, label %init.check, label %init.end, !prof !31

init.check:                                       ; preds = %entry
  %2 = call i32 @__cxa_guard_acquire(i32* @_ZGVZNK10btSoftBody4Body5xformEvE8identity) #6
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %init, label %init.end

init:                                             ; preds = %init.check
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransform11getIdentityEv()
  %call2 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* @_ZZNK10btSoftBody4Body5xformEvE8identity, %class.btTransform* nonnull align 4 dereferenceable(64) %call)
  %3 = call {}* @llvm.invariant.start.p0i8(i64 64, i8* bitcast (%class.btTransform* @_ZZNK10btSoftBody4Body5xformEvE8identity to i8*))
  call void @__cxa_guard_release(i32* @_ZGVZNK10btSoftBody4Body5xformEvE8identity) #6
  br label %init.end

init.end:                                         ; preds = %init, %init.check, %entry
  %m_collisionObject = getelementptr inbounds %"struct.btSoftBody::Body", %"struct.btSoftBody::Body"* %this1, i32 0, i32 2
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %m_collisionObject, align 4, !tbaa !119
  %tobool3 = icmp ne %class.btCollisionObject* %4, null
  br i1 %tobool3, label %if.then, label %if.end

if.then:                                          ; preds = %init.end
  %m_collisionObject4 = getelementptr inbounds %"struct.btSoftBody::Body", %"struct.btSoftBody::Body"* %this1, i32 0, i32 2
  %5 = load %class.btCollisionObject*, %class.btCollisionObject** %m_collisionObject4, align 4, !tbaa !119
  %call5 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %5)
  store %class.btTransform* %call5, %class.btTransform** %retval, align 4
  br label %return

if.end:                                           ; preds = %init.end
  %m_soft = getelementptr inbounds %"struct.btSoftBody::Body", %"struct.btSoftBody::Body"* %this1, i32 0, i32 0
  %6 = load %"struct.btSoftBody::Cluster"*, %"struct.btSoftBody::Cluster"** %m_soft, align 4, !tbaa !121
  %tobool6 = icmp ne %"struct.btSoftBody::Cluster"* %6, null
  br i1 %tobool6, label %if.then7, label %if.end9

if.then7:                                         ; preds = %if.end
  %m_soft8 = getelementptr inbounds %"struct.btSoftBody::Body", %"struct.btSoftBody::Body"* %this1, i32 0, i32 0
  %7 = load %"struct.btSoftBody::Cluster"*, %"struct.btSoftBody::Cluster"** %m_soft8, align 4, !tbaa !121
  %m_framexform = getelementptr inbounds %"struct.btSoftBody::Cluster", %"struct.btSoftBody::Cluster"* %7, i32 0, i32 3
  store %class.btTransform* %m_framexform, %class.btTransform** %retval, align 4
  br label %return

if.end9:                                          ; preds = %if.end
  store %class.btTransform* @_ZZNK10btSoftBody4Body5xformEvE8identity, %class.btTransform** %retval, align 4
  br label %return

return:                                           ; preds = %if.end9, %if.then7, %if.then
  %8 = load %class.btTransform*, %class.btTransform** %retval, align 4
  ret %class.btTransform* %8
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %1, i32 0)
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call1, float* %ref.tmp, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 1)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !8
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 2)
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !8
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #6
  %10 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

define hidden void @_ZN17btSoftBodyHelpers9DrawInfosEP10btSoftBodyP12btIDebugDrawbbb(%class.btSoftBody* %psb, %class.btIDebugDraw* %idraw, i1 zeroext %masses, i1 zeroext %areas, i1 zeroext %0) #0 {
entry:
  %psb.addr = alloca %class.btSoftBody*, align 4
  %idraw.addr = alloca %class.btIDebugDraw*, align 4
  %masses.addr = alloca i8, align 1
  %areas.addr = alloca i8, align 1
  %.addr = alloca i8, align 1
  %i = alloca i32, align 4
  %n = alloca %"struct.btSoftBody::Node"*, align 4
  %text = alloca [2048 x i8], align 16
  %buff = alloca [1024 x i8], align 16
  store %class.btSoftBody* %psb, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  store %class.btIDebugDraw* %idraw, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %frombool = zext i1 %masses to i8
  store i8 %frombool, i8* %masses.addr, align 1, !tbaa !122
  %frombool1 = zext i1 %areas to i8
  store i8 %frombool1, i8* %areas.addr, align 1, !tbaa !122
  %frombool2 = zext i1 %0 to i8
  store i8 %frombool2, i8* %.addr, align 1, !tbaa !122
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_nodes = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %3, i32 0, i32 9
  %call = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4NodeEE4sizeEv(%class.btAlignedObjectArray.25* %m_nodes)
  %cmp = icmp slt i32 %2, %call
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %5 = bitcast %"struct.btSoftBody::Node"** %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_nodes3 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %6, i32 0, i32 9
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %call4 = call nonnull align 4 dereferenceable(101) %"struct.btSoftBody::Node"* @_ZN20btAlignedObjectArrayIN10btSoftBody4NodeEEixEi(%class.btAlignedObjectArray.25* %m_nodes3, i32 %7)
  store %"struct.btSoftBody::Node"* %call4, %"struct.btSoftBody::Node"** %n, align 4, !tbaa !2
  %8 = bitcast [2048 x i8]* %text to i8*
  call void @llvm.lifetime.start.p0i8(i64 2048, i8* %8) #6
  %9 = bitcast [2048 x i8]* %text to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %9, i8 0, i32 2048, i1 false)
  %10 = bitcast [1024 x i8]* %buff to i8*
  call void @llvm.lifetime.start.p0i8(i64 1024, i8* %10) #6
  %11 = load i8, i8* %masses.addr, align 1, !tbaa !122, !range !22
  %tobool = trunc i8 %11 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %arraydecay = getelementptr inbounds [1024 x i8], [1024 x i8]* %buff, i32 0, i32 0
  %12 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n, align 4, !tbaa !2
  %m_im = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %12, i32 0, i32 6
  %13 = load float, float* %m_im, align 4, !tbaa !39
  %div = fdiv float 1.000000e+00, %13
  %conv = fpext float %div to double
  %call5 = call i32 (i8*, i8*, ...) @sprintf(i8* %arraydecay, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str, i32 0, i32 0), double %conv)
  %arraydecay6 = getelementptr inbounds [2048 x i8], [2048 x i8]* %text, i32 0, i32 0
  %arraydecay7 = getelementptr inbounds [1024 x i8], [1024 x i8]* %buff, i32 0, i32 0
  %call8 = call i8* @strcat(i8* %arraydecay6, i8* %arraydecay7)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %14 = load i8, i8* %areas.addr, align 1, !tbaa !122, !range !22
  %tobool9 = trunc i8 %14 to i1
  br i1 %tobool9, label %if.then10, label %if.end17

if.then10:                                        ; preds = %if.end
  %arraydecay11 = getelementptr inbounds [1024 x i8], [1024 x i8]* %buff, i32 0, i32 0
  %15 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n, align 4, !tbaa !2
  %m_area = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %15, i32 0, i32 7
  %16 = load float, float* %m_area, align 4, !tbaa !123
  %conv12 = fpext float %16 to double
  %call13 = call i32 (i8*, i8*, ...) @sprintf(i8* %arraydecay11, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.1, i32 0, i32 0), double %conv12)
  %arraydecay14 = getelementptr inbounds [2048 x i8], [2048 x i8]* %text, i32 0, i32 0
  %arraydecay15 = getelementptr inbounds [1024 x i8], [1024 x i8]* %buff, i32 0, i32 0
  %call16 = call i8* @strcat(i8* %arraydecay14, i8* %arraydecay15)
  br label %if.end17

if.end17:                                         ; preds = %if.then10, %if.end
  %arrayidx = getelementptr inbounds [2048 x i8], [2048 x i8]* %text, i32 0, i32 0
  %17 = load i8, i8* %arrayidx, align 16, !tbaa !24
  %tobool18 = icmp ne i8 %17, 0
  br i1 %tobool18, label %if.then19, label %if.end21

if.then19:                                        ; preds = %if.end17
  %18 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %19 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n, align 4, !tbaa !2
  %m_x = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %19, i32 0, i32 1
  %arraydecay20 = getelementptr inbounds [2048 x i8], [2048 x i8]* %text, i32 0, i32 0
  %20 = bitcast %class.btIDebugDraw* %18 to void (%class.btIDebugDraw*, %class.btVector3*, i8*)***
  %vtable = load void (%class.btIDebugDraw*, %class.btVector3*, i8*)**, void (%class.btIDebugDraw*, %class.btVector3*, i8*)*** %20, align 4, !tbaa !25
  %vfn = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, i8*)*, void (%class.btIDebugDraw*, %class.btVector3*, i8*)** %vtable, i64 10
  %21 = load void (%class.btIDebugDraw*, %class.btVector3*, i8*)*, void (%class.btIDebugDraw*, %class.btVector3*, i8*)** %vfn, align 4
  call void %21(%class.btIDebugDraw* %18, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x, i8* %arraydecay20)
  br label %if.end21

if.end21:                                         ; preds = %if.then19, %if.end17
  %22 = bitcast [1024 x i8]* %buff to i8*
  call void @llvm.lifetime.end.p0i8(i64 1024, i8* %22) #6
  %23 = bitcast [2048 x i8]* %text to i8*
  call void @llvm.lifetime.end.p0i8(i64 2048, i8* %23) #6
  %24 = bitcast %"struct.btSoftBody::Node"** %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #6
  br label %for.inc

for.inc:                                          ; preds = %if.end21
  %25 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %25, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #7

declare i32 @sprintf(i8*, i8*, ...) #3

declare i8* @strcat(i8*, i8*) #3

define internal void @_ZL8drawTreeP12btIDebugDrawPK10btDbvtNodeiRK9btVector3S6_ii(%class.btIDebugDraw* %idraw, %struct.btDbvtNode* %node, i32 %depth, %class.btVector3* nonnull align 4 dereferenceable(16) %ncolor, %class.btVector3* nonnull align 4 dereferenceable(16) %lcolor, i32 %mindepth, i32 %maxdepth) #0 {
entry:
  %idraw.addr = alloca %class.btIDebugDraw*, align 4
  %node.addr = alloca %struct.btDbvtNode*, align 4
  %depth.addr = alloca i32, align 4
  %ncolor.addr = alloca %class.btVector3*, align 4
  %lcolor.addr = alloca %class.btVector3*, align 4
  %mindepth.addr = alloca i32, align 4
  %maxdepth.addr = alloca i32, align 4
  %scl = alloca float, align 4
  %mi = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %mx = alloca %class.btVector3, align 4
  %ref.tmp12 = alloca %class.btVector3, align 4
  %ref.tmp14 = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca %class.btVector3, align 4
  store %class.btIDebugDraw* %idraw, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  store %struct.btDbvtNode* %node, %struct.btDbvtNode** %node.addr, align 4, !tbaa !2
  store i32 %depth, i32* %depth.addr, align 4, !tbaa !6
  store %class.btVector3* %ncolor, %class.btVector3** %ncolor.addr, align 4, !tbaa !2
  store %class.btVector3* %lcolor, %class.btVector3** %lcolor.addr, align 4, !tbaa !2
  store i32 %mindepth, i32* %mindepth.addr, align 4, !tbaa !6
  store i32 %maxdepth, i32* %maxdepth.addr, align 4, !tbaa !6
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.btDbvtNode* %0, null
  br i1 %tobool, label %if.then, label %if.end19

if.then:                                          ; preds = %entry
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4, !tbaa !2
  %call = call zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %1)
  br i1 %call, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then
  %2 = load i32, i32* %depth.addr, align 4, !tbaa !6
  %3 = load i32, i32* %maxdepth.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %if.then2, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.lhs.true
  %4 = load i32, i32* %maxdepth.addr, align 4, !tbaa !6
  %cmp1 = icmp slt i32 %4, 0
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %lor.lhs.false, %land.lhs.true
  %5 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %6 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4, !tbaa !2
  %7 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %6, i32 0, i32 2
  %childs = bitcast %union.anon.23* %7 to [2 x %struct.btDbvtNode*]*
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 0
  %8 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx, align 4, !tbaa !24
  %9 = load i32, i32* %depth.addr, align 4, !tbaa !6
  %add = add nsw i32 %9, 1
  %10 = load %class.btVector3*, %class.btVector3** %ncolor.addr, align 4, !tbaa !2
  %11 = load %class.btVector3*, %class.btVector3** %lcolor.addr, align 4, !tbaa !2
  %12 = load i32, i32* %mindepth.addr, align 4, !tbaa !6
  %13 = load i32, i32* %maxdepth.addr, align 4, !tbaa !6
  call void @_ZL8drawTreeP12btIDebugDrawPK10btDbvtNodeiRK9btVector3S6_ii(%class.btIDebugDraw* %5, %struct.btDbvtNode* %8, i32 %add, %class.btVector3* nonnull align 4 dereferenceable(16) %10, %class.btVector3* nonnull align 4 dereferenceable(16) %11, i32 %12, i32 %13)
  %14 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %15 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4, !tbaa !2
  %16 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %15, i32 0, i32 2
  %childs3 = bitcast %union.anon.23* %16 to [2 x %struct.btDbvtNode*]*
  %arrayidx4 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs3, i32 0, i32 1
  %17 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx4, align 4, !tbaa !24
  %18 = load i32, i32* %depth.addr, align 4, !tbaa !6
  %add5 = add nsw i32 %18, 1
  %19 = load %class.btVector3*, %class.btVector3** %ncolor.addr, align 4, !tbaa !2
  %20 = load %class.btVector3*, %class.btVector3** %lcolor.addr, align 4, !tbaa !2
  %21 = load i32, i32* %mindepth.addr, align 4, !tbaa !6
  %22 = load i32, i32* %maxdepth.addr, align 4, !tbaa !6
  call void @_ZL8drawTreeP12btIDebugDrawPK10btDbvtNodeiRK9btVector3S6_ii(%class.btIDebugDraw* %14, %struct.btDbvtNode* %17, i32 %add5, %class.btVector3* nonnull align 4 dereferenceable(16) %19, %class.btVector3* nonnull align 4 dereferenceable(16) %20, i32 %21, i32 %22)
  br label %if.end

if.end:                                           ; preds = %if.then2, %lor.lhs.false, %if.then
  %23 = load i32, i32* %depth.addr, align 4, !tbaa !6
  %24 = load i32, i32* %mindepth.addr, align 4, !tbaa !6
  %cmp6 = icmp sge i32 %23, %24
  br i1 %cmp6, label %if.then7, label %if.end18

if.then7:                                         ; preds = %if.end
  %25 = bitcast float* %scl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #6
  %26 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4, !tbaa !2
  %call8 = call zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %26)
  %27 = zext i1 %call8 to i64
  %cond = select i1 %call8, i32 1, i32 1
  %conv = sitofp i32 %cond to float
  store float %conv, float* %scl, align 4, !tbaa !8
  %28 = bitcast %class.btVector3* %mi to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %28) #6
  %29 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %29) #6
  %30 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4, !tbaa !2
  %volume = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %30, i32 0, i32 0
  call void @_ZNK12btDbvtAabbMm6CenterEv(%class.btVector3* sret align 4 %ref.tmp, %struct.btDbvtAabbMm* %volume)
  %31 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %31) #6
  %32 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %32) #6
  %33 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4, !tbaa !2
  %volume11 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %33, i32 0, i32 0
  call void @_ZNK12btDbvtAabbMm7ExtentsEv(%class.btVector3* sret align 4 %ref.tmp10, %struct.btDbvtAabbMm* %volume11)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %scl)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %mi, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9)
  %34 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %34) #6
  %35 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %35) #6
  %36 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %36) #6
  %37 = bitcast %class.btVector3* %mx to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %37) #6
  %38 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %38) #6
  %39 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4, !tbaa !2
  %volume13 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %39, i32 0, i32 0
  call void @_ZNK12btDbvtAabbMm6CenterEv(%class.btVector3* sret align 4 %ref.tmp12, %struct.btDbvtAabbMm* %volume13)
  %40 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %40) #6
  %41 = bitcast %class.btVector3* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %41) #6
  %42 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4, !tbaa !2
  %volume16 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %42, i32 0, i32 0
  call void @_ZNK12btDbvtAabbMm7ExtentsEv(%class.btVector3* sret align 4 %ref.tmp15, %struct.btDbvtAabbMm* %volume16)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp14, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp15, float* nonnull align 4 dereferenceable(4) %scl)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %mx, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp12, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp14)
  %43 = bitcast %class.btVector3* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %43) #6
  %44 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %44) #6
  %45 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %45) #6
  %46 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %47 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4, !tbaa !2
  %call17 = call zeroext i1 @_ZNK10btDbvtNode6isleafEv(%struct.btDbvtNode* %47)
  br i1 %call17, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then7
  %48 = load %class.btVector3*, %class.btVector3** %lcolor.addr, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %if.then7
  %49 = load %class.btVector3*, %class.btVector3** %ncolor.addr, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi %class.btVector3* [ %48, %cond.true ], [ %49, %cond.false ]
  call void @_ZL7drawBoxP12btIDebugDrawRK9btVector3S3_S3_(%class.btIDebugDraw* %46, %class.btVector3* nonnull align 4 dereferenceable(16) %mi, %class.btVector3* nonnull align 4 dereferenceable(16) %mx, %class.btVector3* nonnull align 4 dereferenceable(16) %cond-lvalue)
  %50 = bitcast %class.btVector3* %mx to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %50) #6
  %51 = bitcast %class.btVector3* %mi to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %51) #6
  %52 = bitcast float* %scl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #6
  br label %if.end18

if.end18:                                         ; preds = %cond.end, %if.end
  br label %if.end19

if.end19:                                         ; preds = %if.end18, %entry
  ret void
}

define hidden void @_ZN17btSoftBodyHelpers9DrawFrameEP10btSoftBodyP12btIDebugDraw(%class.btSoftBody* %psb, %class.btIDebugDraw* %idraw) #0 {
entry:
  %psb.addr = alloca %class.btSoftBody*, align 4
  %idraw.addr = alloca %class.btIDebugDraw*, align 4
  %com = alloca %class.btVector3, align 4
  %trs = alloca %class.btMatrix3x3, align 4
  %Xaxis = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %Yaxis = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %Zaxis = alloca %class.btVector3, align 4
  %ref.tmp14 = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp20 = alloca %class.btVector3, align 4
  %ref.tmp21 = alloca %class.btVector3, align 4
  %ref.tmp22 = alloca %class.btVector3, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp24 = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp27 = alloca %class.btVector3, align 4
  %ref.tmp28 = alloca %class.btVector3, align 4
  %ref.tmp29 = alloca %class.btVector3, align 4
  %ref.tmp30 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  %ref.tmp36 = alloca %class.btVector3, align 4
  %ref.tmp37 = alloca %class.btVector3, align 4
  %ref.tmp38 = alloca %class.btVector3, align 4
  %ref.tmp39 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  %ref.tmp41 = alloca float, align 4
  %i = alloca i32, align 4
  %x = alloca %class.btVector3, align 4
  %ref.tmp47 = alloca %class.btVector3, align 4
  %ref.tmp51 = alloca %class.btVector3, align 4
  %ref.tmp52 = alloca float, align 4
  %ref.tmp53 = alloca float, align 4
  %ref.tmp54 = alloca float, align 4
  store %class.btSoftBody* %psb, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  store %class.btIDebugDraw* %idraw, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %0 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_pose = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %0, i32 0, i32 5
  %m_bframe = getelementptr inbounds %"struct.btSoftBody::Pose", %"struct.btSoftBody::Pose"* %m_pose, i32 0, i32 1
  %1 = load i8, i8* %m_bframe, align 1, !tbaa !124, !range !22
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %class.btVector3* %com to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #6
  %3 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_pose1 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %3, i32 0, i32 5
  %m_com = getelementptr inbounds %"struct.btSoftBody::Pose", %"struct.btSoftBody::Pose"* %m_pose1, i32 0, i32 5
  %4 = bitcast %class.btVector3* %com to i8*
  %5 = bitcast %class.btVector3* %m_com to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !23
  %6 = bitcast %class.btMatrix3x3* %trs to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %6) #6
  %7 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_pose2 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %7, i32 0, i32 5
  %m_rot = getelementptr inbounds %"struct.btSoftBody::Pose", %"struct.btSoftBody::Pose"* %m_pose2, i32 0, i32 6
  %8 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_pose3 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %8, i32 0, i32 5
  %m_scl = getelementptr inbounds %"struct.btSoftBody::Pose", %"struct.btSoftBody::Pose"* %m_pose3, i32 0, i32 7
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %trs, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_rot, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_scl)
  %9 = bitcast %class.btVector3* %Xaxis to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #6
  %10 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #6
  %11 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #6
  %12 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  store float 1.000000e+00, float* %ref.tmp5, align 4, !tbaa !8
  %13 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !8
  %14 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %trs, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %Xaxis, %class.btVector3* %ref.tmp)
  %15 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  %18 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #6
  %19 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %19) #6
  %20 = bitcast %class.btVector3* %Yaxis to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %20) #6
  %21 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %21) #6
  %22 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %22) #6
  %23 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #6
  store float 0.000000e+00, float* %ref.tmp10, align 4, !tbaa !8
  %24 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #6
  store float 1.000000e+00, float* %ref.tmp11, align 4, !tbaa !8
  %25 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #6
  store float 0.000000e+00, float* %ref.tmp12, align 4, !tbaa !8
  %call13 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp12)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp8, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %trs, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9)
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %Yaxis, %class.btVector3* %ref.tmp8)
  %26 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #6
  %27 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #6
  %28 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #6
  %29 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #6
  %30 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %30) #6
  %31 = bitcast %class.btVector3* %Zaxis to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %31) #6
  %32 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %32) #6
  %33 = bitcast %class.btVector3* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %33) #6
  %34 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #6
  store float 0.000000e+00, float* %ref.tmp16, align 4, !tbaa !8
  %35 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #6
  store float 0.000000e+00, float* %ref.tmp17, align 4, !tbaa !8
  %36 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #6
  store float 1.000000e+00, float* %ref.tmp18, align 4, !tbaa !8
  %call19 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp18)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp14, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %trs, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp15)
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %Zaxis, %class.btVector3* %ref.tmp14)
  %37 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #6
  %38 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #6
  %39 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #6
  %40 = bitcast %class.btVector3* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %40) #6
  %41 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %41) #6
  %42 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %43 = bitcast %class.btVector3* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %43) #6
  %44 = bitcast %class.btVector3* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %44) #6
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp21, %class.btVector3* nonnull align 4 dereferenceable(16) %Xaxis, float* nonnull align 4 dereferenceable(4) @_ZZN17btSoftBodyHelpers9DrawFrameEP10btSoftBodyP12btIDebugDrawE4ascl)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp20, %class.btVector3* nonnull align 4 dereferenceable(16) %com, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp21)
  %45 = bitcast %class.btVector3* %ref.tmp22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %45) #6
  %46 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #6
  store float 1.000000e+00, float* %ref.tmp23, align 4, !tbaa !8
  %47 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #6
  store float 0.000000e+00, float* %ref.tmp24, align 4, !tbaa !8
  %48 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #6
  store float 0.000000e+00, float* %ref.tmp25, align 4, !tbaa !8
  %call26 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp22, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp24, float* nonnull align 4 dereferenceable(4) %ref.tmp25)
  %49 = bitcast %class.btIDebugDraw* %42 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %49, align 4, !tbaa !25
  %vfn = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %50 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %50(%class.btIDebugDraw* %42, %class.btVector3* nonnull align 4 dereferenceable(16) %com, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp20, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp22)
  %51 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #6
  %52 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #6
  %53 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #6
  %54 = bitcast %class.btVector3* %ref.tmp22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %54) #6
  %55 = bitcast %class.btVector3* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %55) #6
  %56 = bitcast %class.btVector3* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %56) #6
  %57 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %58 = bitcast %class.btVector3* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %58) #6
  %59 = bitcast %class.btVector3* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %59) #6
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp28, %class.btVector3* nonnull align 4 dereferenceable(16) %Yaxis, float* nonnull align 4 dereferenceable(4) @_ZZN17btSoftBodyHelpers9DrawFrameEP10btSoftBodyP12btIDebugDrawE4ascl)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp27, %class.btVector3* nonnull align 4 dereferenceable(16) %com, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp28)
  %60 = bitcast %class.btVector3* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %60) #6
  %61 = bitcast float* %ref.tmp30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %61) #6
  store float 0.000000e+00, float* %ref.tmp30, align 4, !tbaa !8
  %62 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #6
  store float 1.000000e+00, float* %ref.tmp31, align 4, !tbaa !8
  %63 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #6
  store float 0.000000e+00, float* %ref.tmp32, align 4, !tbaa !8
  %call33 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp30, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  %64 = bitcast %class.btIDebugDraw* %57 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable34 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %64, align 4, !tbaa !25
  %vfn35 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable34, i64 2
  %65 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn35, align 4
  call void %65(%class.btIDebugDraw* %57, %class.btVector3* nonnull align 4 dereferenceable(16) %com, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp27, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp29)
  %66 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #6
  %67 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #6
  %68 = bitcast float* %ref.tmp30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #6
  %69 = bitcast %class.btVector3* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %69) #6
  %70 = bitcast %class.btVector3* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %70) #6
  %71 = bitcast %class.btVector3* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %71) #6
  %72 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %73 = bitcast %class.btVector3* %ref.tmp36 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %73) #6
  %74 = bitcast %class.btVector3* %ref.tmp37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %74) #6
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp37, %class.btVector3* nonnull align 4 dereferenceable(16) %Zaxis, float* nonnull align 4 dereferenceable(4) @_ZZN17btSoftBodyHelpers9DrawFrameEP10btSoftBodyP12btIDebugDrawE4ascl)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp36, %class.btVector3* nonnull align 4 dereferenceable(16) %com, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp37)
  %75 = bitcast %class.btVector3* %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %75) #6
  %76 = bitcast float* %ref.tmp39 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #6
  store float 0.000000e+00, float* %ref.tmp39, align 4, !tbaa !8
  %77 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %77) #6
  store float 0.000000e+00, float* %ref.tmp40, align 4, !tbaa !8
  %78 = bitcast float* %ref.tmp41 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %78) #6
  store float 1.000000e+00, float* %ref.tmp41, align 4, !tbaa !8
  %call42 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp39, float* nonnull align 4 dereferenceable(4) %ref.tmp40, float* nonnull align 4 dereferenceable(4) %ref.tmp41)
  %79 = bitcast %class.btIDebugDraw* %72 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable43 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %79, align 4, !tbaa !25
  %vfn44 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable43, i64 2
  %80 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn44, align 4
  call void %80(%class.btIDebugDraw* %72, %class.btVector3* nonnull align 4 dereferenceable(16) %com, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp36, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp38)
  %81 = bitcast float* %ref.tmp41 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #6
  %82 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #6
  %83 = bitcast float* %ref.tmp39 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #6
  %84 = bitcast %class.btVector3* %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %84) #6
  %85 = bitcast %class.btVector3* %ref.tmp37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %85) #6
  %86 = bitcast %class.btVector3* %ref.tmp36 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %86) #6
  %87 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %87) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %88 = load i32, i32* %i, align 4, !tbaa !6
  %89 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_pose45 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %89, i32 0, i32 5
  %m_pos = getelementptr inbounds %"struct.btSoftBody::Pose", %"struct.btSoftBody::Pose"* %m_pose45, i32 0, i32 3
  %call46 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %m_pos)
  %cmp = icmp slt i32 %88, %call46
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %90 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %91 = bitcast %class.btVector3* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %91) #6
  %92 = bitcast %class.btVector3* %ref.tmp47 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %92) #6
  %93 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4, !tbaa !2
  %m_pose48 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %93, i32 0, i32 5
  %m_pos49 = getelementptr inbounds %"struct.btSoftBody::Pose", %"struct.btSoftBody::Pose"* %m_pose48, i32 0, i32 3
  %94 = load i32, i32* %i, align 4, !tbaa !6
  %call50 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %m_pos49, i32 %94)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp47, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %trs, %class.btVector3* nonnull align 4 dereferenceable(16) %call50)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %x, %class.btVector3* nonnull align 4 dereferenceable(16) %com, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp47)
  %95 = bitcast %class.btVector3* %ref.tmp47 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %95) #6
  %96 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %97 = bitcast %class.btVector3* %ref.tmp51 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %97) #6
  %98 = bitcast float* %ref.tmp52 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %98) #6
  store float 1.000000e+00, float* %ref.tmp52, align 4, !tbaa !8
  %99 = bitcast float* %ref.tmp53 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %99) #6
  store float 0.000000e+00, float* %ref.tmp53, align 4, !tbaa !8
  %100 = bitcast float* %ref.tmp54 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %100) #6
  store float 1.000000e+00, float* %ref.tmp54, align 4, !tbaa !8
  %call55 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp51, float* nonnull align 4 dereferenceable(4) %ref.tmp52, float* nonnull align 4 dereferenceable(4) %ref.tmp53, float* nonnull align 4 dereferenceable(4) %ref.tmp54)
  call void @_ZL10drawVertexP12btIDebugDrawRK9btVector3fS3_(%class.btIDebugDraw* %96, %class.btVector3* nonnull align 4 dereferenceable(16) %x, float 0x3FB99999A0000000, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp51)
  %101 = bitcast float* %ref.tmp54 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #6
  %102 = bitcast float* %ref.tmp53 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #6
  %103 = bitcast float* %ref.tmp52 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #6
  %104 = bitcast %class.btVector3* %ref.tmp51 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %104) #6
  %105 = bitcast %class.btVector3* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %105) #6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %106 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %106, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %107 = bitcast %class.btVector3* %Zaxis to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %107) #6
  %108 = bitcast %class.btVector3* %Yaxis to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %108) #6
  %109 = bitcast %class.btVector3* %Xaxis to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %109) #6
  %110 = bitcast %class.btMatrix3x3* %trs to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %110) #6
  %111 = bitcast %class.btVector3* %com to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %111) #6
  br label %if.end

if.end:                                           ; preds = %for.end, %entry
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #4 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %3, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4, !tbaa !8
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !8
  %7 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %9, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %8, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4, !tbaa !8
  %13 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %15, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %14, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4, !tbaa !8
  %16 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4, !tbaa !8
  %19 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #6
  %20 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %21 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %21, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %20, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4, !tbaa !8
  %22 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #6
  %23 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %24 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %24, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %23, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4, !tbaa !8
  %25 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #6
  %26 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %27 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %27, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %26, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4, !tbaa !8
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  %28 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #6
  %29 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #6
  %30 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #6
  %31 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #6
  %32 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #6
  %33 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #6
  %34 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #6
  %35 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #6
  %36 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #6
  ret void
}

define hidden %class.btSoftBody* @_ZN17btSoftBodyHelpers10CreateRopeER19btSoftBodyWorldInfoRK9btVector3S4_ii(%struct.btSoftBodyWorldInfo* nonnull align 4 dereferenceable(100) %worldInfo, %class.btVector3* nonnull align 4 dereferenceable(16) %from, %class.btVector3* nonnull align 4 dereferenceable(16) %to, i32 %res, i32 %fixeds) #0 {
entry:
  %worldInfo.addr = alloca %struct.btSoftBodyWorldInfo*, align 4
  %from.addr = alloca %class.btVector3*, align 4
  %to.addr = alloca %class.btVector3*, align 4
  %res.addr = alloca i32, align 4
  %fixeds.addr = alloca i32, align 4
  %r = alloca i32, align 4
  %x = alloca %class.btVector3*, align 4
  %m = alloca float*, align 4
  %i = alloca i32, align 4
  %t = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %psb = alloca %class.btSoftBody*, align 4
  store %struct.btSoftBodyWorldInfo* %worldInfo, %struct.btSoftBodyWorldInfo** %worldInfo.addr, align 4, !tbaa !2
  store %class.btVector3* %from, %class.btVector3** %from.addr, align 4, !tbaa !2
  store %class.btVector3* %to, %class.btVector3** %to.addr, align 4, !tbaa !2
  store i32 %res, i32* %res.addr, align 4, !tbaa !6
  store i32 %fixeds, i32* %fixeds.addr, align 4, !tbaa !6
  %0 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %res.addr, align 4, !tbaa !6
  %add = add nsw i32 %1, 2
  store i32 %add, i32* %r, align 4, !tbaa !6
  %2 = bitcast %class.btVector3** %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i32, i32* %r, align 4, !tbaa !6
  %4 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %3, i32 16)
  %5 = extractvalue { i32, i1 } %4, 1
  %6 = extractvalue { i32, i1 } %4, 0
  %7 = select i1 %5, i32 -1, i32 %6
  %call = call i8* @_ZN9btVector3naEm(i32 %7)
  %8 = bitcast i8* %call to %class.btVector3*
  %isempty = icmp eq i32 %3, 0
  br i1 %isempty, label %arrayctor.cont, label %new.ctorloop

new.ctorloop:                                     ; preds = %entry
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 %3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %new.ctorloop
  %arrayctor.cur = phi %class.btVector3* [ %8, %new.ctorloop ], [ %arrayctor.next, %arrayctor.loop ]
  %call1 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %entry, %arrayctor.loop
  store %class.btVector3* %8, %class.btVector3** %x, align 4, !tbaa !2
  %9 = bitcast float** %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %10 = load i32, i32* %r, align 4, !tbaa !6
  %11 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %10, i32 4)
  %12 = extractvalue { i32, i1 } %11, 1
  %13 = extractvalue { i32, i1 } %11, 0
  %14 = select i1 %12, i32 -1, i32 %13
  %call2 = call noalias nonnull i8* @_Znam(i32 %14) #11
  %15 = bitcast i8* %call2 to float*
  store float* %15, float** %m, align 4, !tbaa !2
  %16 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %arrayctor.cont
  %17 = load i32, i32* %i, align 4, !tbaa !6
  %18 = load i32, i32* %r, align 4, !tbaa !6
  %cmp = icmp slt i32 %17, %18
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %19 = bitcast float* %t to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #6
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %conv = sitofp i32 %20 to float
  %21 = load i32, i32* %r, align 4, !tbaa !6
  %sub = sub nsw i32 %21, 1
  %conv3 = sitofp i32 %sub to float
  %div = fdiv float %conv, %conv3
  store float %div, float* %t, align 4, !tbaa !8
  %22 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %22) #6
  %23 = load %class.btVector3*, %class.btVector3** %from.addr, align 4, !tbaa !2
  %24 = load %class.btVector3*, %class.btVector3** %to.addr, align 4, !tbaa !2
  call void @_Z4lerpRK9btVector3S1_RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %23, %class.btVector3* nonnull align 4 dereferenceable(16) %24, float* nonnull align 4 dereferenceable(4) %t)
  %25 = load %class.btVector3*, %class.btVector3** %x, align 4, !tbaa !2
  %26 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %25, i32 %26
  %27 = bitcast %class.btVector3* %arrayidx to i8*
  %28 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %27, i8* align 4 %28, i32 16, i1 false), !tbaa.struct !23
  %29 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #6
  %30 = load float*, float** %m, align 4, !tbaa !2
  %31 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds float, float* %30, i32 %31
  store float 1.000000e+00, float* %arrayidx4, align 4, !tbaa !8
  %32 = bitcast float* %t to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %33 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %33, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %34 = bitcast %class.btSoftBody** %psb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #6
  %call5 = call i8* @_ZN17btCollisionObjectnwEm(i32 1252)
  %35 = bitcast i8* %call5 to %class.btSoftBody*
  %36 = load %struct.btSoftBodyWorldInfo*, %struct.btSoftBodyWorldInfo** %worldInfo.addr, align 4, !tbaa !2
  %37 = load i32, i32* %r, align 4, !tbaa !6
  %38 = load %class.btVector3*, %class.btVector3** %x, align 4, !tbaa !2
  %39 = load float*, float** %m, align 4, !tbaa !2
  %call6 = call %class.btSoftBody* @_ZN10btSoftBodyC1EP19btSoftBodyWorldInfoiPK9btVector3PKf(%class.btSoftBody* %35, %struct.btSoftBodyWorldInfo* %36, i32 %37, %class.btVector3* %38, float* %39)
  store %class.btSoftBody* %35, %class.btSoftBody** %psb, align 4, !tbaa !2
  %40 = load i32, i32* %fixeds.addr, align 4, !tbaa !6
  %and = and i32 %40, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %41 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  call void @_ZN10btSoftBody7setMassEif(%class.btSoftBody* %41, i32 0, float 0.000000e+00)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %42 = load i32, i32* %fixeds.addr, align 4, !tbaa !6
  %and7 = and i32 %42, 2
  %tobool8 = icmp ne i32 %and7, 0
  br i1 %tobool8, label %if.then9, label %if.end11

if.then9:                                         ; preds = %if.end
  %43 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %44 = load i32, i32* %r, align 4, !tbaa !6
  %sub10 = sub nsw i32 %44, 1
  call void @_ZN10btSoftBody7setMassEif(%class.btSoftBody* %43, i32 %sub10, float 0.000000e+00)
  br label %if.end11

if.end11:                                         ; preds = %if.then9, %if.end
  %45 = load %class.btVector3*, %class.btVector3** %x, align 4, !tbaa !2
  %isnull = icmp eq %class.btVector3* %45, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %if.end11
  %46 = bitcast %class.btVector3* %45 to i8*
  call void @_ZN9btVector3daEPv(i8* %46) #6
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %if.end11
  %47 = load float*, float** %m, align 4, !tbaa !2
  %isnull12 = icmp eq float* %47, null
  br i1 %isnull12, label %delete.end14, label %delete.notnull13

delete.notnull13:                                 ; preds = %delete.end
  %48 = bitcast float* %47 to i8*
  call void @_ZdaPv(i8* %48) #12
  br label %delete.end14

delete.end14:                                     ; preds = %delete.notnull13, %delete.end
  store i32 1, i32* %i, align 4, !tbaa !6
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc19, %delete.end14
  %49 = load i32, i32* %i, align 4, !tbaa !6
  %50 = load i32, i32* %r, align 4, !tbaa !6
  %cmp16 = icmp slt i32 %49, %50
  br i1 %cmp16, label %for.body17, label %for.end21

for.body17:                                       ; preds = %for.cond15
  %51 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %52 = load i32, i32* %i, align 4, !tbaa !6
  %sub18 = sub nsw i32 %52, 1
  %53 = load i32, i32* %i, align 4, !tbaa !6
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %51, i32 %sub18, i32 %53, %"struct.btSoftBody::Material"* null, i1 zeroext false)
  br label %for.inc19

for.inc19:                                        ; preds = %for.body17
  %54 = load i32, i32* %i, align 4, !tbaa !6
  %inc20 = add nsw i32 %54, 1
  store i32 %inc20, i32* %i, align 4, !tbaa !6
  br label %for.cond15

for.end21:                                        ; preds = %for.cond15
  %55 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %56 = bitcast %class.btSoftBody** %psb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #6
  %57 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #6
  %58 = bitcast float** %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #6
  %59 = bitcast %class.btVector3** %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #6
  %60 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #6
  ret %class.btSoftBody* %55
}

; Function Attrs: nounwind readnone speculatable willreturn
declare { i32, i1 } @llvm.umul.with.overflow.i32(i32, i32) #8

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN9btVector3naEm(i32 %sizeInBytes) #4 comdat {
entry:
  %sizeInBytes.addr = alloca i32, align 4
  store i32 %sizeInBytes, i32* %sizeInBytes.addr, align 4, !tbaa !125
  %0 = load i32, i32* %sizeInBytes.addr, align 4, !tbaa !125
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %0, i32 16)
  ret i8* %call
}

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znam(i32) #9

; Function Attrs: inlinehint
define linkonce_odr hidden void @_Z4lerpRK9btVector3S1_RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2, float* nonnull align 4 dereferenceable(4) %t) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %t.addr = alloca float*, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  store float* %t, float** %t.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %1 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %2 = load float*, float** %t.addr, align 4, !tbaa !2
  call void @_ZNK9btVector34lerpERKS_RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, float* nonnull align 4 dereferenceable(4) %2)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN17btCollisionObjectnwEm(i32 %sizeInBytes) #4 comdat {
entry:
  %sizeInBytes.addr = alloca i32, align 4
  store i32 %sizeInBytes, i32* %sizeInBytes.addr, align 4, !tbaa !125
  %0 = load i32, i32* %sizeInBytes.addr, align 4, !tbaa !125
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %0, i32 16)
  ret i8* %call
}

declare %class.btSoftBody* @_ZN10btSoftBodyC1EP19btSoftBodyWorldInfoiPK9btVector3PKf(%class.btSoftBody* returned, %struct.btSoftBodyWorldInfo*, i32, %class.btVector3*, float*) unnamed_addr #3

declare void @_ZN10btSoftBody7setMassEif(%class.btSoftBody*, i32, float) #3

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector3daEPv(i8* %ptr) #2 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdaPv(i8*) #10

declare void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody*, i32, i32, %"struct.btSoftBody::Material"*, i1 zeroext) #3

define hidden %class.btSoftBody* @_ZN17btSoftBodyHelpers11CreatePatchER19btSoftBodyWorldInfoRK9btVector3S4_S4_S4_iiib(%struct.btSoftBodyWorldInfo* nonnull align 4 dereferenceable(100) %worldInfo, %class.btVector3* nonnull align 4 dereferenceable(16) %corner00, %class.btVector3* nonnull align 4 dereferenceable(16) %corner10, %class.btVector3* nonnull align 4 dereferenceable(16) %corner01, %class.btVector3* nonnull align 4 dereferenceable(16) %corner11, i32 %resx, i32 %resy, i32 %fixeds, i1 zeroext %gendiags) #0 {
entry:
  %retval = alloca %class.btSoftBody*, align 4
  %worldInfo.addr = alloca %struct.btSoftBodyWorldInfo*, align 4
  %corner00.addr = alloca %class.btVector3*, align 4
  %corner10.addr = alloca %class.btVector3*, align 4
  %corner01.addr = alloca %class.btVector3*, align 4
  %corner11.addr = alloca %class.btVector3*, align 4
  %resx.addr = alloca i32, align 4
  %resy.addr = alloca i32, align 4
  %fixeds.addr = alloca i32, align 4
  %gendiags.addr = alloca i8, align 1
  %rx = alloca i32, align 4
  %ry = alloca i32, align 4
  %tot = alloca i32, align 4
  %x = alloca %class.btVector3*, align 4
  %m = alloca float*, align 4
  %iy = alloca i32, align 4
  %ty = alloca float, align 4
  %py0 = alloca %class.btVector3, align 4
  %py1 = alloca %class.btVector3, align 4
  %ix = alloca i32, align 4
  %tx = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %psb = alloca %class.btSoftBody*, align 4
  %ix54 = alloca i32, align 4
  %idx = alloca i32, align 4
  %mdx = alloca i8, align 1
  %mdy = alloca i8, align 1
  store %struct.btSoftBodyWorldInfo* %worldInfo, %struct.btSoftBodyWorldInfo** %worldInfo.addr, align 4, !tbaa !2
  store %class.btVector3* %corner00, %class.btVector3** %corner00.addr, align 4, !tbaa !2
  store %class.btVector3* %corner10, %class.btVector3** %corner10.addr, align 4, !tbaa !2
  store %class.btVector3* %corner01, %class.btVector3** %corner01.addr, align 4, !tbaa !2
  store %class.btVector3* %corner11, %class.btVector3** %corner11.addr, align 4, !tbaa !2
  store i32 %resx, i32* %resx.addr, align 4, !tbaa !6
  store i32 %resy, i32* %resy.addr, align 4, !tbaa !6
  store i32 %fixeds, i32* %fixeds.addr, align 4, !tbaa !6
  %frombool = zext i1 %gendiags to i8
  store i8 %frombool, i8* %gendiags.addr, align 1, !tbaa !122
  %0 = load i32, i32* %resx.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %0, 2
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %resy.addr, align 4, !tbaa !6
  %cmp1 = icmp slt i32 %1, 2
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store %class.btSoftBody* null, %class.btSoftBody** %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %2 = bitcast i32* %rx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i32, i32* %resx.addr, align 4, !tbaa !6
  store i32 %3, i32* %rx, align 4, !tbaa !6
  %4 = bitcast i32* %ry to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load i32, i32* %resy.addr, align 4, !tbaa !6
  store i32 %5, i32* %ry, align 4, !tbaa !6
  %6 = bitcast i32* %tot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load i32, i32* %rx, align 4, !tbaa !6
  %8 = load i32, i32* %ry, align 4, !tbaa !6
  %mul = mul nsw i32 %7, %8
  store i32 %mul, i32* %tot, align 4, !tbaa !6
  %9 = bitcast %class.btVector3** %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %10 = load i32, i32* %tot, align 4, !tbaa !6
  %11 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %10, i32 16)
  %12 = extractvalue { i32, i1 } %11, 1
  %13 = extractvalue { i32, i1 } %11, 0
  %14 = select i1 %12, i32 -1, i32 %13
  %call = call i8* @_ZN9btVector3naEm(i32 %14)
  %15 = bitcast i8* %call to %class.btVector3*
  %isempty = icmp eq i32 %10, 0
  br i1 %isempty, label %arrayctor.cont, label %new.ctorloop

new.ctorloop:                                     ; preds = %if.end
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %15, i32 %10
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %new.ctorloop
  %arrayctor.cur = phi %class.btVector3* [ %15, %new.ctorloop ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %if.end, %arrayctor.loop
  store %class.btVector3* %15, %class.btVector3** %x, align 4, !tbaa !2
  %16 = bitcast float** %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  %17 = load i32, i32* %tot, align 4, !tbaa !6
  %18 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %17, i32 4)
  %19 = extractvalue { i32, i1 } %18, 1
  %20 = extractvalue { i32, i1 } %18, 0
  %21 = select i1 %19, i32 -1, i32 %20
  %call3 = call noalias nonnull i8* @_Znam(i32 %21) #11
  %22 = bitcast i8* %call3 to float*
  store float* %22, float** %m, align 4, !tbaa !2
  %23 = bitcast i32* %iy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #6
  store i32 0, i32* %iy, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc17, %arrayctor.cont
  %24 = load i32, i32* %iy, align 4, !tbaa !6
  %25 = load i32, i32* %ry, align 4, !tbaa !6
  %cmp4 = icmp slt i32 %24, %25
  br i1 %cmp4, label %for.body, label %for.end19

for.body:                                         ; preds = %for.cond
  %26 = bitcast float* %ty to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #6
  %27 = load i32, i32* %iy, align 4, !tbaa !6
  %conv = sitofp i32 %27 to float
  %28 = load i32, i32* %ry, align 4, !tbaa !6
  %sub = sub nsw i32 %28, 1
  %conv5 = sitofp i32 %sub to float
  %div = fdiv float %conv, %conv5
  store float %div, float* %ty, align 4, !tbaa !8
  %29 = bitcast %class.btVector3* %py0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %29) #6
  %30 = load %class.btVector3*, %class.btVector3** %corner00.addr, align 4, !tbaa !2
  %31 = load %class.btVector3*, %class.btVector3** %corner01.addr, align 4, !tbaa !2
  call void @_Z4lerpRK9btVector3S1_RKf(%class.btVector3* sret align 4 %py0, %class.btVector3* nonnull align 4 dereferenceable(16) %30, %class.btVector3* nonnull align 4 dereferenceable(16) %31, float* nonnull align 4 dereferenceable(4) %ty)
  %32 = bitcast %class.btVector3* %py1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %32) #6
  %33 = load %class.btVector3*, %class.btVector3** %corner10.addr, align 4, !tbaa !2
  %34 = load %class.btVector3*, %class.btVector3** %corner11.addr, align 4, !tbaa !2
  call void @_Z4lerpRK9btVector3S1_RKf(%class.btVector3* sret align 4 %py1, %class.btVector3* nonnull align 4 dereferenceable(16) %33, %class.btVector3* nonnull align 4 dereferenceable(16) %34, float* nonnull align 4 dereferenceable(4) %ty)
  %35 = bitcast i32* %ix to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #6
  store i32 0, i32* %ix, align 4, !tbaa !6
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc, %for.body
  %36 = load i32, i32* %ix, align 4, !tbaa !6
  %37 = load i32, i32* %rx, align 4, !tbaa !6
  %cmp7 = icmp slt i32 %36, %37
  br i1 %cmp7, label %for.body8, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond6
  %38 = bitcast i32* %ix to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #6
  br label %for.end

for.body8:                                        ; preds = %for.cond6
  %39 = bitcast float* %tx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #6
  %40 = load i32, i32* %ix, align 4, !tbaa !6
  %conv9 = sitofp i32 %40 to float
  %41 = load i32, i32* %rx, align 4, !tbaa !6
  %sub10 = sub nsw i32 %41, 1
  %conv11 = sitofp i32 %sub10 to float
  %div12 = fdiv float %conv9, %conv11
  store float %div12, float* %tx, align 4, !tbaa !8
  %42 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %42) #6
  call void @_Z4lerpRK9btVector3S1_RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %py0, %class.btVector3* nonnull align 4 dereferenceable(16) %py1, float* nonnull align 4 dereferenceable(4) %tx)
  %43 = load %class.btVector3*, %class.btVector3** %x, align 4, !tbaa !2
  %44 = load i32, i32* %iy, align 4, !tbaa !6
  %45 = load i32, i32* %rx, align 4, !tbaa !6
  %mul13 = mul nsw i32 %44, %45
  %46 = load i32, i32* %ix, align 4, !tbaa !6
  %add = add nsw i32 %mul13, %46
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %43, i32 %add
  %47 = bitcast %class.btVector3* %arrayidx to i8*
  %48 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %47, i8* align 4 %48, i32 16, i1 false), !tbaa.struct !23
  %49 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %49) #6
  %50 = load float*, float** %m, align 4, !tbaa !2
  %51 = load i32, i32* %iy, align 4, !tbaa !6
  %52 = load i32, i32* %rx, align 4, !tbaa !6
  %mul14 = mul nsw i32 %51, %52
  %53 = load i32, i32* %ix, align 4, !tbaa !6
  %add15 = add nsw i32 %mul14, %53
  %arrayidx16 = getelementptr inbounds float, float* %50, i32 %add15
  store float 1.000000e+00, float* %arrayidx16, align 4, !tbaa !8
  %54 = bitcast float* %tx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #6
  br label %for.inc

for.inc:                                          ; preds = %for.body8
  %55 = load i32, i32* %ix, align 4, !tbaa !6
  %inc = add nsw i32 %55, 1
  store i32 %inc, i32* %ix, align 4, !tbaa !6
  br label %for.cond6

for.end:                                          ; preds = %for.cond.cleanup
  %56 = bitcast %class.btVector3* %py1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %56) #6
  %57 = bitcast %class.btVector3* %py0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %57) #6
  %58 = bitcast float* %ty to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #6
  br label %for.inc17

for.inc17:                                        ; preds = %for.end
  %59 = load i32, i32* %iy, align 4, !tbaa !6
  %inc18 = add nsw i32 %59, 1
  store i32 %inc18, i32* %iy, align 4, !tbaa !6
  br label %for.cond

for.end19:                                        ; preds = %for.cond
  %60 = bitcast %class.btSoftBody** %psb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #6
  %call20 = call i8* @_ZN17btCollisionObjectnwEm(i32 1252)
  %61 = bitcast i8* %call20 to %class.btSoftBody*
  %62 = load %struct.btSoftBodyWorldInfo*, %struct.btSoftBodyWorldInfo** %worldInfo.addr, align 4, !tbaa !2
  %63 = load i32, i32* %tot, align 4, !tbaa !6
  %64 = load %class.btVector3*, %class.btVector3** %x, align 4, !tbaa !2
  %65 = load float*, float** %m, align 4, !tbaa !2
  %call21 = call %class.btSoftBody* @_ZN10btSoftBodyC1EP19btSoftBodyWorldInfoiPK9btVector3PKf(%class.btSoftBody* %61, %struct.btSoftBodyWorldInfo* %62, i32 %63, %class.btVector3* %64, float* %65)
  store %class.btSoftBody* %61, %class.btSoftBody** %psb, align 4, !tbaa !2
  %66 = load i32, i32* %fixeds.addr, align 4, !tbaa !6
  %and = and i32 %66, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then22, label %if.end25

if.then22:                                        ; preds = %for.end19
  %67 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %68 = load i32, i32* %rx, align 4, !tbaa !6
  %mul23 = mul nsw i32 0, %68
  %add24 = add nsw i32 %mul23, 0
  call void @_ZN10btSoftBody7setMassEif(%class.btSoftBody* %67, i32 %add24, float 0.000000e+00)
  br label %if.end25

if.end25:                                         ; preds = %if.then22, %for.end19
  %69 = load i32, i32* %fixeds.addr, align 4, !tbaa !6
  %and26 = and i32 %69, 2
  %tobool27 = icmp ne i32 %and26, 0
  br i1 %tobool27, label %if.then28, label %if.end32

if.then28:                                        ; preds = %if.end25
  %70 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %71 = load i32, i32* %rx, align 4, !tbaa !6
  %mul29 = mul nsw i32 0, %71
  %72 = load i32, i32* %rx, align 4, !tbaa !6
  %sub30 = sub nsw i32 %72, 1
  %add31 = add nsw i32 %mul29, %sub30
  call void @_ZN10btSoftBody7setMassEif(%class.btSoftBody* %70, i32 %add31, float 0.000000e+00)
  br label %if.end32

if.end32:                                         ; preds = %if.then28, %if.end25
  %73 = load i32, i32* %fixeds.addr, align 4, !tbaa !6
  %and33 = and i32 %73, 4
  %tobool34 = icmp ne i32 %and33, 0
  br i1 %tobool34, label %if.then35, label %if.end39

if.then35:                                        ; preds = %if.end32
  %74 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %75 = load i32, i32* %ry, align 4, !tbaa !6
  %sub36 = sub nsw i32 %75, 1
  %76 = load i32, i32* %rx, align 4, !tbaa !6
  %mul37 = mul nsw i32 %sub36, %76
  %add38 = add nsw i32 %mul37, 0
  call void @_ZN10btSoftBody7setMassEif(%class.btSoftBody* %74, i32 %add38, float 0.000000e+00)
  br label %if.end39

if.end39:                                         ; preds = %if.then35, %if.end32
  %77 = load i32, i32* %fixeds.addr, align 4, !tbaa !6
  %and40 = and i32 %77, 8
  %tobool41 = icmp ne i32 %and40, 0
  br i1 %tobool41, label %if.then42, label %if.end47

if.then42:                                        ; preds = %if.end39
  %78 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %79 = load i32, i32* %ry, align 4, !tbaa !6
  %sub43 = sub nsw i32 %79, 1
  %80 = load i32, i32* %rx, align 4, !tbaa !6
  %mul44 = mul nsw i32 %sub43, %80
  %81 = load i32, i32* %rx, align 4, !tbaa !6
  %sub45 = sub nsw i32 %81, 1
  %add46 = add nsw i32 %mul44, %sub45
  call void @_ZN10btSoftBody7setMassEif(%class.btSoftBody* %78, i32 %add46, float 0.000000e+00)
  br label %if.end47

if.end47:                                         ; preds = %if.then42, %if.end39
  %82 = load %class.btVector3*, %class.btVector3** %x, align 4, !tbaa !2
  %isnull = icmp eq %class.btVector3* %82, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %if.end47
  %83 = bitcast %class.btVector3* %82 to i8*
  call void @_ZN9btVector3daEPv(i8* %83) #6
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %if.end47
  %84 = load float*, float** %m, align 4, !tbaa !2
  %isnull48 = icmp eq float* %84, null
  br i1 %isnull48, label %delete.end50, label %delete.notnull49

delete.notnull49:                                 ; preds = %delete.end
  %85 = bitcast float* %84 to i8*
  call void @_ZdaPv(i8* %85) #12
  br label %delete.end50

delete.end50:                                     ; preds = %delete.notnull49, %delete.end
  store i32 0, i32* %iy, align 4, !tbaa !6
  br label %for.cond51

for.cond51:                                       ; preds = %for.inc145, %delete.end50
  %86 = load i32, i32* %iy, align 4, !tbaa !6
  %87 = load i32, i32* %ry, align 4, !tbaa !6
  %cmp52 = icmp slt i32 %86, %87
  br i1 %cmp52, label %for.body53, label %for.end147

for.body53:                                       ; preds = %for.cond51
  %88 = bitcast i32* %ix54 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #6
  store i32 0, i32* %ix54, align 4, !tbaa !6
  br label %for.cond55

for.cond55:                                       ; preds = %for.inc142, %for.body53
  %89 = load i32, i32* %ix54, align 4, !tbaa !6
  %90 = load i32, i32* %rx, align 4, !tbaa !6
  %cmp56 = icmp slt i32 %89, %90
  br i1 %cmp56, label %for.body58, label %for.cond.cleanup57

for.cond.cleanup57:                               ; preds = %for.cond55
  %91 = bitcast i32* %ix54 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #6
  br label %for.end144

for.body58:                                       ; preds = %for.cond55
  %92 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %92) #6
  %93 = load i32, i32* %iy, align 4, !tbaa !6
  %94 = load i32, i32* %rx, align 4, !tbaa !6
  %mul59 = mul nsw i32 %93, %94
  %95 = load i32, i32* %ix54, align 4, !tbaa !6
  %add60 = add nsw i32 %mul59, %95
  store i32 %add60, i32* %idx, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mdx) #6
  %96 = load i32, i32* %ix54, align 4, !tbaa !6
  %add61 = add nsw i32 %96, 1
  %97 = load i32, i32* %rx, align 4, !tbaa !6
  %cmp62 = icmp slt i32 %add61, %97
  %frombool63 = zext i1 %cmp62 to i8
  store i8 %frombool63, i8* %mdx, align 1, !tbaa !122
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mdy) #6
  %98 = load i32, i32* %iy, align 4, !tbaa !6
  %add64 = add nsw i32 %98, 1
  %99 = load i32, i32* %ry, align 4, !tbaa !6
  %cmp65 = icmp slt i32 %add64, %99
  %frombool66 = zext i1 %cmp65 to i8
  store i8 %frombool66, i8* %mdy, align 1, !tbaa !122
  %100 = load i8, i8* %mdx, align 1, !tbaa !122, !range !22
  %tobool67 = trunc i8 %100 to i1
  br i1 %tobool67, label %if.then68, label %if.end72

if.then68:                                        ; preds = %for.body58
  %101 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %102 = load i32, i32* %idx, align 4, !tbaa !6
  %103 = load i32, i32* %iy, align 4, !tbaa !6
  %104 = load i32, i32* %rx, align 4, !tbaa !6
  %mul69 = mul nsw i32 %103, %104
  %105 = load i32, i32* %ix54, align 4, !tbaa !6
  %add70 = add nsw i32 %105, 1
  %add71 = add nsw i32 %mul69, %add70
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %101, i32 %102, i32 %add71, %"struct.btSoftBody::Material"* null, i1 zeroext false)
  br label %if.end72

if.end72:                                         ; preds = %if.then68, %for.body58
  %106 = load i8, i8* %mdy, align 1, !tbaa !122, !range !22
  %tobool73 = trunc i8 %106 to i1
  br i1 %tobool73, label %if.then74, label %if.end78

if.then74:                                        ; preds = %if.end72
  %107 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %108 = load i32, i32* %idx, align 4, !tbaa !6
  %109 = load i32, i32* %iy, align 4, !tbaa !6
  %add75 = add nsw i32 %109, 1
  %110 = load i32, i32* %rx, align 4, !tbaa !6
  %mul76 = mul nsw i32 %add75, %110
  %111 = load i32, i32* %ix54, align 4, !tbaa !6
  %add77 = add nsw i32 %mul76, %111
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %107, i32 %108, i32 %add77, %"struct.btSoftBody::Material"* null, i1 zeroext false)
  br label %if.end78

if.end78:                                         ; preds = %if.then74, %if.end72
  %112 = load i8, i8* %mdx, align 1, !tbaa !122, !range !22
  %tobool79 = trunc i8 %112 to i1
  br i1 %tobool79, label %land.lhs.true, label %if.end141

land.lhs.true:                                    ; preds = %if.end78
  %113 = load i8, i8* %mdy, align 1, !tbaa !122, !range !22
  %tobool80 = trunc i8 %113 to i1
  br i1 %tobool80, label %if.then81, label %if.end141

if.then81:                                        ; preds = %land.lhs.true
  %114 = load i32, i32* %ix54, align 4, !tbaa !6
  %115 = load i32, i32* %iy, align 4, !tbaa !6
  %add82 = add nsw i32 %114, %115
  %and83 = and i32 %add82, 1
  %tobool84 = icmp ne i32 %and83, 0
  br i1 %tobool84, label %if.then85, label %if.else

if.then85:                                        ; preds = %if.then81
  %116 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %117 = load i32, i32* %iy, align 4, !tbaa !6
  %118 = load i32, i32* %rx, align 4, !tbaa !6
  %mul86 = mul nsw i32 %117, %118
  %119 = load i32, i32* %ix54, align 4, !tbaa !6
  %add87 = add nsw i32 %mul86, %119
  %120 = load i32, i32* %iy, align 4, !tbaa !6
  %121 = load i32, i32* %rx, align 4, !tbaa !6
  %mul88 = mul nsw i32 %120, %121
  %122 = load i32, i32* %ix54, align 4, !tbaa !6
  %add89 = add nsw i32 %122, 1
  %add90 = add nsw i32 %mul88, %add89
  %123 = load i32, i32* %iy, align 4, !tbaa !6
  %add91 = add nsw i32 %123, 1
  %124 = load i32, i32* %rx, align 4, !tbaa !6
  %mul92 = mul nsw i32 %add91, %124
  %125 = load i32, i32* %ix54, align 4, !tbaa !6
  %add93 = add nsw i32 %125, 1
  %add94 = add nsw i32 %mul92, %add93
  call void @_ZN10btSoftBody10appendFaceEiiiPNS_8MaterialE(%class.btSoftBody* %116, i32 %add87, i32 %add90, i32 %add94, %"struct.btSoftBody::Material"* null)
  %126 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %127 = load i32, i32* %iy, align 4, !tbaa !6
  %128 = load i32, i32* %rx, align 4, !tbaa !6
  %mul95 = mul nsw i32 %127, %128
  %129 = load i32, i32* %ix54, align 4, !tbaa !6
  %add96 = add nsw i32 %mul95, %129
  %130 = load i32, i32* %iy, align 4, !tbaa !6
  %add97 = add nsw i32 %130, 1
  %131 = load i32, i32* %rx, align 4, !tbaa !6
  %mul98 = mul nsw i32 %add97, %131
  %132 = load i32, i32* %ix54, align 4, !tbaa !6
  %add99 = add nsw i32 %132, 1
  %add100 = add nsw i32 %mul98, %add99
  %133 = load i32, i32* %iy, align 4, !tbaa !6
  %add101 = add nsw i32 %133, 1
  %134 = load i32, i32* %rx, align 4, !tbaa !6
  %mul102 = mul nsw i32 %add101, %134
  %135 = load i32, i32* %ix54, align 4, !tbaa !6
  %add103 = add nsw i32 %mul102, %135
  call void @_ZN10btSoftBody10appendFaceEiiiPNS_8MaterialE(%class.btSoftBody* %126, i32 %add96, i32 %add100, i32 %add103, %"struct.btSoftBody::Material"* null)
  %136 = load i8, i8* %gendiags.addr, align 1, !tbaa !122, !range !22
  %tobool104 = trunc i8 %136 to i1
  br i1 %tobool104, label %if.then105, label %if.end112

if.then105:                                       ; preds = %if.then85
  %137 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %138 = load i32, i32* %iy, align 4, !tbaa !6
  %139 = load i32, i32* %rx, align 4, !tbaa !6
  %mul106 = mul nsw i32 %138, %139
  %140 = load i32, i32* %ix54, align 4, !tbaa !6
  %add107 = add nsw i32 %mul106, %140
  %141 = load i32, i32* %iy, align 4, !tbaa !6
  %add108 = add nsw i32 %141, 1
  %142 = load i32, i32* %rx, align 4, !tbaa !6
  %mul109 = mul nsw i32 %add108, %142
  %143 = load i32, i32* %ix54, align 4, !tbaa !6
  %add110 = add nsw i32 %143, 1
  %add111 = add nsw i32 %mul109, %add110
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %137, i32 %add107, i32 %add111, %"struct.btSoftBody::Material"* null, i1 zeroext false)
  br label %if.end112

if.end112:                                        ; preds = %if.then105, %if.then85
  br label %if.end140

if.else:                                          ; preds = %if.then81
  %144 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %145 = load i32, i32* %iy, align 4, !tbaa !6
  %add113 = add nsw i32 %145, 1
  %146 = load i32, i32* %rx, align 4, !tbaa !6
  %mul114 = mul nsw i32 %add113, %146
  %147 = load i32, i32* %ix54, align 4, !tbaa !6
  %add115 = add nsw i32 %mul114, %147
  %148 = load i32, i32* %iy, align 4, !tbaa !6
  %149 = load i32, i32* %rx, align 4, !tbaa !6
  %mul116 = mul nsw i32 %148, %149
  %150 = load i32, i32* %ix54, align 4, !tbaa !6
  %add117 = add nsw i32 %mul116, %150
  %151 = load i32, i32* %iy, align 4, !tbaa !6
  %152 = load i32, i32* %rx, align 4, !tbaa !6
  %mul118 = mul nsw i32 %151, %152
  %153 = load i32, i32* %ix54, align 4, !tbaa !6
  %add119 = add nsw i32 %153, 1
  %add120 = add nsw i32 %mul118, %add119
  call void @_ZN10btSoftBody10appendFaceEiiiPNS_8MaterialE(%class.btSoftBody* %144, i32 %add115, i32 %add117, i32 %add120, %"struct.btSoftBody::Material"* null)
  %154 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %155 = load i32, i32* %iy, align 4, !tbaa !6
  %add121 = add nsw i32 %155, 1
  %156 = load i32, i32* %rx, align 4, !tbaa !6
  %mul122 = mul nsw i32 %add121, %156
  %157 = load i32, i32* %ix54, align 4, !tbaa !6
  %add123 = add nsw i32 %mul122, %157
  %158 = load i32, i32* %iy, align 4, !tbaa !6
  %159 = load i32, i32* %rx, align 4, !tbaa !6
  %mul124 = mul nsw i32 %158, %159
  %160 = load i32, i32* %ix54, align 4, !tbaa !6
  %add125 = add nsw i32 %160, 1
  %add126 = add nsw i32 %mul124, %add125
  %161 = load i32, i32* %iy, align 4, !tbaa !6
  %add127 = add nsw i32 %161, 1
  %162 = load i32, i32* %rx, align 4, !tbaa !6
  %mul128 = mul nsw i32 %add127, %162
  %163 = load i32, i32* %ix54, align 4, !tbaa !6
  %add129 = add nsw i32 %163, 1
  %add130 = add nsw i32 %mul128, %add129
  call void @_ZN10btSoftBody10appendFaceEiiiPNS_8MaterialE(%class.btSoftBody* %154, i32 %add123, i32 %add126, i32 %add130, %"struct.btSoftBody::Material"* null)
  %164 = load i8, i8* %gendiags.addr, align 1, !tbaa !122, !range !22
  %tobool131 = trunc i8 %164 to i1
  br i1 %tobool131, label %if.then132, label %if.end139

if.then132:                                       ; preds = %if.else
  %165 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %166 = load i32, i32* %iy, align 4, !tbaa !6
  %167 = load i32, i32* %rx, align 4, !tbaa !6
  %mul133 = mul nsw i32 %166, %167
  %168 = load i32, i32* %ix54, align 4, !tbaa !6
  %add134 = add nsw i32 %168, 1
  %add135 = add nsw i32 %mul133, %add134
  %169 = load i32, i32* %iy, align 4, !tbaa !6
  %add136 = add nsw i32 %169, 1
  %170 = load i32, i32* %rx, align 4, !tbaa !6
  %mul137 = mul nsw i32 %add136, %170
  %171 = load i32, i32* %ix54, align 4, !tbaa !6
  %add138 = add nsw i32 %mul137, %171
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %165, i32 %add135, i32 %add138, %"struct.btSoftBody::Material"* null, i1 zeroext false)
  br label %if.end139

if.end139:                                        ; preds = %if.then132, %if.else
  br label %if.end140

if.end140:                                        ; preds = %if.end139, %if.end112
  br label %if.end141

if.end141:                                        ; preds = %if.end140, %land.lhs.true, %if.end78
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mdy) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mdx) #6
  %172 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %172) #6
  br label %for.inc142

for.inc142:                                       ; preds = %if.end141
  %173 = load i32, i32* %ix54, align 4, !tbaa !6
  %inc143 = add nsw i32 %173, 1
  store i32 %inc143, i32* %ix54, align 4, !tbaa !6
  br label %for.cond55

for.end144:                                       ; preds = %for.cond.cleanup57
  br label %for.inc145

for.inc145:                                       ; preds = %for.end144
  %174 = load i32, i32* %iy, align 4, !tbaa !6
  %inc146 = add nsw i32 %174, 1
  store i32 %inc146, i32* %iy, align 4, !tbaa !6
  br label %for.cond51

for.end147:                                       ; preds = %for.cond51
  %175 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  store %class.btSoftBody* %175, %class.btSoftBody** %retval, align 4
  %176 = bitcast %class.btSoftBody** %psb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %176) #6
  %177 = bitcast i32* %iy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %177) #6
  %178 = bitcast float** %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %178) #6
  %179 = bitcast %class.btVector3** %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %179) #6
  %180 = bitcast i32* %tot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %180) #6
  %181 = bitcast i32* %ry to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %181) #6
  %182 = bitcast i32* %rx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %182) #6
  br label %return

return:                                           ; preds = %for.end147, %if.then
  %183 = load %class.btSoftBody*, %class.btSoftBody** %retval, align 4
  ret %class.btSoftBody* %183
}

declare void @_ZN10btSoftBody10appendFaceEiiiPNS_8MaterialE(%class.btSoftBody*, i32, i32, i32, %"struct.btSoftBody::Material"*) #3

define hidden %class.btSoftBody* @_ZN17btSoftBodyHelpers13CreatePatchUVER19btSoftBodyWorldInfoRK9btVector3S4_S4_S4_iiibPf(%struct.btSoftBodyWorldInfo* nonnull align 4 dereferenceable(100) %worldInfo, %class.btVector3* nonnull align 4 dereferenceable(16) %corner00, %class.btVector3* nonnull align 4 dereferenceable(16) %corner10, %class.btVector3* nonnull align 4 dereferenceable(16) %corner01, %class.btVector3* nonnull align 4 dereferenceable(16) %corner11, i32 %resx, i32 %resy, i32 %fixeds, i1 zeroext %gendiags, float* %tex_coords) #0 {
entry:
  %retval = alloca %class.btSoftBody*, align 4
  %worldInfo.addr = alloca %struct.btSoftBodyWorldInfo*, align 4
  %corner00.addr = alloca %class.btVector3*, align 4
  %corner10.addr = alloca %class.btVector3*, align 4
  %corner01.addr = alloca %class.btVector3*, align 4
  %corner11.addr = alloca %class.btVector3*, align 4
  %resx.addr = alloca i32, align 4
  %resy.addr = alloca i32, align 4
  %fixeds.addr = alloca i32, align 4
  %gendiags.addr = alloca i8, align 1
  %tex_coords.addr = alloca float*, align 4
  %rx = alloca i32, align 4
  %ry = alloca i32, align 4
  %tot = alloca i32, align 4
  %x = alloca %class.btVector3*, align 4
  %m = alloca float*, align 4
  %iy = alloca i32, align 4
  %ty = alloca float, align 4
  %py0 = alloca %class.btVector3, align 4
  %py1 = alloca %class.btVector3, align 4
  %ix = alloca i32, align 4
  %tx = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %psb = alloca %class.btSoftBody*, align 4
  %z = alloca i32, align 4
  %ix98 = alloca i32, align 4
  %mdx = alloca i8, align 1
  %mdy = alloca i8, align 1
  %node00 = alloca i32, align 4
  %node01 = alloca i32, align 4
  %node10 = alloca i32, align 4
  %node11 = alloca i32, align 4
  store %struct.btSoftBodyWorldInfo* %worldInfo, %struct.btSoftBodyWorldInfo** %worldInfo.addr, align 4, !tbaa !2
  store %class.btVector3* %corner00, %class.btVector3** %corner00.addr, align 4, !tbaa !2
  store %class.btVector3* %corner10, %class.btVector3** %corner10.addr, align 4, !tbaa !2
  store %class.btVector3* %corner01, %class.btVector3** %corner01.addr, align 4, !tbaa !2
  store %class.btVector3* %corner11, %class.btVector3** %corner11.addr, align 4, !tbaa !2
  store i32 %resx, i32* %resx.addr, align 4, !tbaa !6
  store i32 %resy, i32* %resy.addr, align 4, !tbaa !6
  store i32 %fixeds, i32* %fixeds.addr, align 4, !tbaa !6
  %frombool = zext i1 %gendiags to i8
  store i8 %frombool, i8* %gendiags.addr, align 1, !tbaa !122
  store float* %tex_coords, float** %tex_coords.addr, align 4, !tbaa !2
  %0 = load i32, i32* %resx.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %0, 2
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %resy.addr, align 4, !tbaa !6
  %cmp1 = icmp slt i32 %1, 2
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store %class.btSoftBody* null, %class.btSoftBody** %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %2 = bitcast i32* %rx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i32, i32* %resx.addr, align 4, !tbaa !6
  store i32 %3, i32* %rx, align 4, !tbaa !6
  %4 = bitcast i32* %ry to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load i32, i32* %resy.addr, align 4, !tbaa !6
  store i32 %5, i32* %ry, align 4, !tbaa !6
  %6 = bitcast i32* %tot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load i32, i32* %rx, align 4, !tbaa !6
  %8 = load i32, i32* %ry, align 4, !tbaa !6
  %mul = mul nsw i32 %7, %8
  store i32 %mul, i32* %tot, align 4, !tbaa !6
  %9 = bitcast %class.btVector3** %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %10 = load i32, i32* %tot, align 4, !tbaa !6
  %11 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %10, i32 16)
  %12 = extractvalue { i32, i1 } %11, 1
  %13 = extractvalue { i32, i1 } %11, 0
  %14 = select i1 %12, i32 -1, i32 %13
  %call = call i8* @_ZN9btVector3naEm(i32 %14)
  %15 = bitcast i8* %call to %class.btVector3*
  %isempty = icmp eq i32 %10, 0
  br i1 %isempty, label %arrayctor.cont, label %new.ctorloop

new.ctorloop:                                     ; preds = %if.end
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %15, i32 %10
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %new.ctorloop
  %arrayctor.cur = phi %class.btVector3* [ %15, %new.ctorloop ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %if.end, %arrayctor.loop
  store %class.btVector3* %15, %class.btVector3** %x, align 4, !tbaa !2
  %16 = bitcast float** %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  %17 = load i32, i32* %tot, align 4, !tbaa !6
  %18 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %17, i32 4)
  %19 = extractvalue { i32, i1 } %18, 1
  %20 = extractvalue { i32, i1 } %18, 0
  %21 = select i1 %19, i32 -1, i32 %20
  %call3 = call noalias nonnull i8* @_Znam(i32 %21) #11
  %22 = bitcast i8* %call3 to float*
  store float* %22, float** %m, align 4, !tbaa !2
  %23 = bitcast i32* %iy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #6
  store i32 0, i32* %iy, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc17, %arrayctor.cont
  %24 = load i32, i32* %iy, align 4, !tbaa !6
  %25 = load i32, i32* %ry, align 4, !tbaa !6
  %cmp4 = icmp slt i32 %24, %25
  br i1 %cmp4, label %for.body, label %for.end19

for.body:                                         ; preds = %for.cond
  %26 = bitcast float* %ty to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #6
  %27 = load i32, i32* %iy, align 4, !tbaa !6
  %conv = sitofp i32 %27 to float
  %28 = load i32, i32* %ry, align 4, !tbaa !6
  %sub = sub nsw i32 %28, 1
  %conv5 = sitofp i32 %sub to float
  %div = fdiv float %conv, %conv5
  store float %div, float* %ty, align 4, !tbaa !8
  %29 = bitcast %class.btVector3* %py0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %29) #6
  %30 = load %class.btVector3*, %class.btVector3** %corner00.addr, align 4, !tbaa !2
  %31 = load %class.btVector3*, %class.btVector3** %corner01.addr, align 4, !tbaa !2
  call void @_Z4lerpRK9btVector3S1_RKf(%class.btVector3* sret align 4 %py0, %class.btVector3* nonnull align 4 dereferenceable(16) %30, %class.btVector3* nonnull align 4 dereferenceable(16) %31, float* nonnull align 4 dereferenceable(4) %ty)
  %32 = bitcast %class.btVector3* %py1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %32) #6
  %33 = load %class.btVector3*, %class.btVector3** %corner10.addr, align 4, !tbaa !2
  %34 = load %class.btVector3*, %class.btVector3** %corner11.addr, align 4, !tbaa !2
  call void @_Z4lerpRK9btVector3S1_RKf(%class.btVector3* sret align 4 %py1, %class.btVector3* nonnull align 4 dereferenceable(16) %33, %class.btVector3* nonnull align 4 dereferenceable(16) %34, float* nonnull align 4 dereferenceable(4) %ty)
  %35 = bitcast i32* %ix to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #6
  store i32 0, i32* %ix, align 4, !tbaa !6
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc, %for.body
  %36 = load i32, i32* %ix, align 4, !tbaa !6
  %37 = load i32, i32* %rx, align 4, !tbaa !6
  %cmp7 = icmp slt i32 %36, %37
  br i1 %cmp7, label %for.body8, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond6
  %38 = bitcast i32* %ix to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #6
  br label %for.end

for.body8:                                        ; preds = %for.cond6
  %39 = bitcast float* %tx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #6
  %40 = load i32, i32* %ix, align 4, !tbaa !6
  %conv9 = sitofp i32 %40 to float
  %41 = load i32, i32* %rx, align 4, !tbaa !6
  %sub10 = sub nsw i32 %41, 1
  %conv11 = sitofp i32 %sub10 to float
  %div12 = fdiv float %conv9, %conv11
  store float %div12, float* %tx, align 4, !tbaa !8
  %42 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %42) #6
  call void @_Z4lerpRK9btVector3S1_RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %py0, %class.btVector3* nonnull align 4 dereferenceable(16) %py1, float* nonnull align 4 dereferenceable(4) %tx)
  %43 = load %class.btVector3*, %class.btVector3** %x, align 4, !tbaa !2
  %44 = load i32, i32* %iy, align 4, !tbaa !6
  %45 = load i32, i32* %rx, align 4, !tbaa !6
  %mul13 = mul nsw i32 %44, %45
  %46 = load i32, i32* %ix, align 4, !tbaa !6
  %add = add nsw i32 %mul13, %46
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %43, i32 %add
  %47 = bitcast %class.btVector3* %arrayidx to i8*
  %48 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %47, i8* align 4 %48, i32 16, i1 false), !tbaa.struct !23
  %49 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %49) #6
  %50 = load float*, float** %m, align 4, !tbaa !2
  %51 = load i32, i32* %iy, align 4, !tbaa !6
  %52 = load i32, i32* %rx, align 4, !tbaa !6
  %mul14 = mul nsw i32 %51, %52
  %53 = load i32, i32* %ix, align 4, !tbaa !6
  %add15 = add nsw i32 %mul14, %53
  %arrayidx16 = getelementptr inbounds float, float* %50, i32 %add15
  store float 1.000000e+00, float* %arrayidx16, align 4, !tbaa !8
  %54 = bitcast float* %tx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #6
  br label %for.inc

for.inc:                                          ; preds = %for.body8
  %55 = load i32, i32* %ix, align 4, !tbaa !6
  %inc = add nsw i32 %55, 1
  store i32 %inc, i32* %ix, align 4, !tbaa !6
  br label %for.cond6

for.end:                                          ; preds = %for.cond.cleanup
  %56 = bitcast %class.btVector3* %py1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %56) #6
  %57 = bitcast %class.btVector3* %py0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %57) #6
  %58 = bitcast float* %ty to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #6
  br label %for.inc17

for.inc17:                                        ; preds = %for.end
  %59 = load i32, i32* %iy, align 4, !tbaa !6
  %inc18 = add nsw i32 %59, 1
  store i32 %inc18, i32* %iy, align 4, !tbaa !6
  br label %for.cond

for.end19:                                        ; preds = %for.cond
  %60 = bitcast %class.btSoftBody** %psb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #6
  %call20 = call i8* @_ZN17btCollisionObjectnwEm(i32 1252)
  %61 = bitcast i8* %call20 to %class.btSoftBody*
  %62 = load %struct.btSoftBodyWorldInfo*, %struct.btSoftBodyWorldInfo** %worldInfo.addr, align 4, !tbaa !2
  %63 = load i32, i32* %tot, align 4, !tbaa !6
  %64 = load %class.btVector3*, %class.btVector3** %x, align 4, !tbaa !2
  %65 = load float*, float** %m, align 4, !tbaa !2
  %call21 = call %class.btSoftBody* @_ZN10btSoftBodyC1EP19btSoftBodyWorldInfoiPK9btVector3PKf(%class.btSoftBody* %61, %struct.btSoftBodyWorldInfo* %62, i32 %63, %class.btVector3* %64, float* %65)
  store %class.btSoftBody* %61, %class.btSoftBody** %psb, align 4, !tbaa !2
  %66 = load i32, i32* %fixeds.addr, align 4, !tbaa !6
  %and = and i32 %66, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then22, label %if.end25

if.then22:                                        ; preds = %for.end19
  %67 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %68 = load i32, i32* %rx, align 4, !tbaa !6
  %mul23 = mul nsw i32 0, %68
  %add24 = add nsw i32 %mul23, 0
  call void @_ZN10btSoftBody7setMassEif(%class.btSoftBody* %67, i32 %add24, float 0.000000e+00)
  br label %if.end25

if.end25:                                         ; preds = %if.then22, %for.end19
  %69 = load i32, i32* %fixeds.addr, align 4, !tbaa !6
  %and26 = and i32 %69, 2
  %tobool27 = icmp ne i32 %and26, 0
  br i1 %tobool27, label %if.then28, label %if.end32

if.then28:                                        ; preds = %if.end25
  %70 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %71 = load i32, i32* %rx, align 4, !tbaa !6
  %mul29 = mul nsw i32 0, %71
  %72 = load i32, i32* %rx, align 4, !tbaa !6
  %sub30 = sub nsw i32 %72, 1
  %add31 = add nsw i32 %mul29, %sub30
  call void @_ZN10btSoftBody7setMassEif(%class.btSoftBody* %70, i32 %add31, float 0.000000e+00)
  br label %if.end32

if.end32:                                         ; preds = %if.then28, %if.end25
  %73 = load i32, i32* %fixeds.addr, align 4, !tbaa !6
  %and33 = and i32 %73, 4
  %tobool34 = icmp ne i32 %and33, 0
  br i1 %tobool34, label %if.then35, label %if.end39

if.then35:                                        ; preds = %if.end32
  %74 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %75 = load i32, i32* %ry, align 4, !tbaa !6
  %sub36 = sub nsw i32 %75, 1
  %76 = load i32, i32* %rx, align 4, !tbaa !6
  %mul37 = mul nsw i32 %sub36, %76
  %add38 = add nsw i32 %mul37, 0
  call void @_ZN10btSoftBody7setMassEif(%class.btSoftBody* %74, i32 %add38, float 0.000000e+00)
  br label %if.end39

if.end39:                                         ; preds = %if.then35, %if.end32
  %77 = load i32, i32* %fixeds.addr, align 4, !tbaa !6
  %and40 = and i32 %77, 8
  %tobool41 = icmp ne i32 %and40, 0
  br i1 %tobool41, label %if.then42, label %if.end47

if.then42:                                        ; preds = %if.end39
  %78 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %79 = load i32, i32* %ry, align 4, !tbaa !6
  %sub43 = sub nsw i32 %79, 1
  %80 = load i32, i32* %rx, align 4, !tbaa !6
  %mul44 = mul nsw i32 %sub43, %80
  %81 = load i32, i32* %rx, align 4, !tbaa !6
  %sub45 = sub nsw i32 %81, 1
  %add46 = add nsw i32 %mul44, %sub45
  call void @_ZN10btSoftBody7setMassEif(%class.btSoftBody* %78, i32 %add46, float 0.000000e+00)
  br label %if.end47

if.end47:                                         ; preds = %if.then42, %if.end39
  %82 = load i32, i32* %fixeds.addr, align 4, !tbaa !6
  %and48 = and i32 %82, 16
  %tobool49 = icmp ne i32 %and48, 0
  br i1 %tobool49, label %if.then50, label %if.end55

if.then50:                                        ; preds = %if.end47
  %83 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %84 = load i32, i32* %rx, align 4, !tbaa !6
  %mul51 = mul nsw i32 0, %84
  %85 = load i32, i32* %rx, align 4, !tbaa !6
  %sub52 = sub nsw i32 %85, 1
  %div53 = sdiv i32 %sub52, 2
  %add54 = add nsw i32 %mul51, %div53
  call void @_ZN10btSoftBody7setMassEif(%class.btSoftBody* %83, i32 %add54, float 0.000000e+00)
  br label %if.end55

if.end55:                                         ; preds = %if.then50, %if.end47
  %86 = load i32, i32* %fixeds.addr, align 4, !tbaa !6
  %and56 = and i32 %86, 32
  %tobool57 = icmp ne i32 %and56, 0
  br i1 %tobool57, label %if.then58, label %if.end63

if.then58:                                        ; preds = %if.end55
  %87 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %88 = load i32, i32* %ry, align 4, !tbaa !6
  %sub59 = sub nsw i32 %88, 1
  %div60 = sdiv i32 %sub59, 2
  %89 = load i32, i32* %rx, align 4, !tbaa !6
  %mul61 = mul nsw i32 %div60, %89
  %add62 = add nsw i32 %mul61, 0
  call void @_ZN10btSoftBody7setMassEif(%class.btSoftBody* %87, i32 %add62, float 0.000000e+00)
  br label %if.end63

if.end63:                                         ; preds = %if.then58, %if.end55
  %90 = load i32, i32* %fixeds.addr, align 4, !tbaa !6
  %and64 = and i32 %90, 64
  %tobool65 = icmp ne i32 %and64, 0
  br i1 %tobool65, label %if.then66, label %if.end72

if.then66:                                        ; preds = %if.end63
  %91 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %92 = load i32, i32* %ry, align 4, !tbaa !6
  %sub67 = sub nsw i32 %92, 1
  %div68 = sdiv i32 %sub67, 2
  %93 = load i32, i32* %rx, align 4, !tbaa !6
  %mul69 = mul nsw i32 %div68, %93
  %94 = load i32, i32* %rx, align 4, !tbaa !6
  %sub70 = sub nsw i32 %94, 1
  %add71 = add nsw i32 %mul69, %sub70
  call void @_ZN10btSoftBody7setMassEif(%class.btSoftBody* %91, i32 %add71, float 0.000000e+00)
  br label %if.end72

if.end72:                                         ; preds = %if.then66, %if.end63
  %95 = load i32, i32* %fixeds.addr, align 4, !tbaa !6
  %and73 = and i32 %95, 128
  %tobool74 = icmp ne i32 %and73, 0
  br i1 %tobool74, label %if.then75, label %if.end81

if.then75:                                        ; preds = %if.end72
  %96 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %97 = load i32, i32* %ry, align 4, !tbaa !6
  %sub76 = sub nsw i32 %97, 1
  %98 = load i32, i32* %rx, align 4, !tbaa !6
  %mul77 = mul nsw i32 %sub76, %98
  %99 = load i32, i32* %rx, align 4, !tbaa !6
  %sub78 = sub nsw i32 %99, 1
  %div79 = sdiv i32 %sub78, 2
  %add80 = add nsw i32 %mul77, %div79
  call void @_ZN10btSoftBody7setMassEif(%class.btSoftBody* %96, i32 %add80, float 0.000000e+00)
  br label %if.end81

if.end81:                                         ; preds = %if.then75, %if.end72
  %100 = load i32, i32* %fixeds.addr, align 4, !tbaa !6
  %and82 = and i32 %100, 256
  %tobool83 = icmp ne i32 %and82, 0
  br i1 %tobool83, label %if.then84, label %if.end91

if.then84:                                        ; preds = %if.end81
  %101 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %102 = load i32, i32* %ry, align 4, !tbaa !6
  %sub85 = sub nsw i32 %102, 1
  %div86 = sdiv i32 %sub85, 2
  %103 = load i32, i32* %rx, align 4, !tbaa !6
  %mul87 = mul nsw i32 %div86, %103
  %104 = load i32, i32* %rx, align 4, !tbaa !6
  %sub88 = sub nsw i32 %104, 1
  %div89 = sdiv i32 %sub88, 2
  %add90 = add nsw i32 %mul87, %div89
  call void @_ZN10btSoftBody7setMassEif(%class.btSoftBody* %101, i32 %add90, float 0.000000e+00)
  br label %if.end91

if.end91:                                         ; preds = %if.then84, %if.end81
  %105 = load %class.btVector3*, %class.btVector3** %x, align 4, !tbaa !2
  %isnull = icmp eq %class.btVector3* %105, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %if.end91
  %106 = bitcast %class.btVector3* %105 to i8*
  call void @_ZN9btVector3daEPv(i8* %106) #6
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %if.end91
  %107 = load float*, float** %m, align 4, !tbaa !2
  %isnull92 = icmp eq float* %107, null
  br i1 %isnull92, label %delete.end94, label %delete.notnull93

delete.notnull93:                                 ; preds = %delete.end
  %108 = bitcast float* %107 to i8*
  call void @_ZdaPv(i8* %108) #12
  br label %delete.end94

delete.end94:                                     ; preds = %delete.notnull93, %delete.end
  %109 = bitcast i32* %z to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %109) #6
  store i32 0, i32* %z, align 4, !tbaa !6
  store i32 0, i32* %iy, align 4, !tbaa !6
  br label %for.cond95

for.cond95:                                       ; preds = %for.inc180, %delete.end94
  %110 = load i32, i32* %iy, align 4, !tbaa !6
  %111 = load i32, i32* %ry, align 4, !tbaa !6
  %cmp96 = icmp slt i32 %110, %111
  br i1 %cmp96, label %for.body97, label %for.end182

for.body97:                                       ; preds = %for.cond95
  %112 = bitcast i32* %ix98 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %112) #6
  store i32 0, i32* %ix98, align 4, !tbaa !6
  br label %for.cond99

for.cond99:                                       ; preds = %for.inc177, %for.body97
  %113 = load i32, i32* %ix98, align 4, !tbaa !6
  %114 = load i32, i32* %rx, align 4, !tbaa !6
  %cmp100 = icmp slt i32 %113, %114
  br i1 %cmp100, label %for.body102, label %for.cond.cleanup101

for.cond.cleanup101:                              ; preds = %for.cond99
  %115 = bitcast i32* %ix98 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #6
  br label %for.end179

for.body102:                                      ; preds = %for.cond99
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mdx) #6
  %116 = load i32, i32* %ix98, align 4, !tbaa !6
  %add103 = add nsw i32 %116, 1
  %117 = load i32, i32* %rx, align 4, !tbaa !6
  %cmp104 = icmp slt i32 %add103, %117
  %frombool105 = zext i1 %cmp104 to i8
  store i8 %frombool105, i8* %mdx, align 1, !tbaa !122
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mdy) #6
  %118 = load i32, i32* %iy, align 4, !tbaa !6
  %add106 = add nsw i32 %118, 1
  %119 = load i32, i32* %ry, align 4, !tbaa !6
  %cmp107 = icmp slt i32 %add106, %119
  %frombool108 = zext i1 %cmp107 to i8
  store i8 %frombool108, i8* %mdy, align 1, !tbaa !122
  %120 = bitcast i32* %node00 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %120) #6
  %121 = load i32, i32* %iy, align 4, !tbaa !6
  %122 = load i32, i32* %rx, align 4, !tbaa !6
  %mul109 = mul nsw i32 %121, %122
  %123 = load i32, i32* %ix98, align 4, !tbaa !6
  %add110 = add nsw i32 %mul109, %123
  store i32 %add110, i32* %node00, align 4, !tbaa !6
  %124 = bitcast i32* %node01 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %124) #6
  %125 = load i32, i32* %iy, align 4, !tbaa !6
  %126 = load i32, i32* %rx, align 4, !tbaa !6
  %mul111 = mul nsw i32 %125, %126
  %127 = load i32, i32* %ix98, align 4, !tbaa !6
  %add112 = add nsw i32 %127, 1
  %add113 = add nsw i32 %mul111, %add112
  store i32 %add113, i32* %node01, align 4, !tbaa !6
  %128 = bitcast i32* %node10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %128) #6
  %129 = load i32, i32* %iy, align 4, !tbaa !6
  %add114 = add nsw i32 %129, 1
  %130 = load i32, i32* %rx, align 4, !tbaa !6
  %mul115 = mul nsw i32 %add114, %130
  %131 = load i32, i32* %ix98, align 4, !tbaa !6
  %add116 = add nsw i32 %mul115, %131
  store i32 %add116, i32* %node10, align 4, !tbaa !6
  %132 = bitcast i32* %node11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %132) #6
  %133 = load i32, i32* %iy, align 4, !tbaa !6
  %add117 = add nsw i32 %133, 1
  %134 = load i32, i32* %rx, align 4, !tbaa !6
  %mul118 = mul nsw i32 %add117, %134
  %135 = load i32, i32* %ix98, align 4, !tbaa !6
  %add119 = add nsw i32 %135, 1
  %add120 = add nsw i32 %mul118, %add119
  store i32 %add120, i32* %node11, align 4, !tbaa !6
  %136 = load i8, i8* %mdx, align 1, !tbaa !122, !range !22
  %tobool121 = trunc i8 %136 to i1
  br i1 %tobool121, label %if.then122, label %if.end123

if.then122:                                       ; preds = %for.body102
  %137 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %138 = load i32, i32* %node00, align 4, !tbaa !6
  %139 = load i32, i32* %node01, align 4, !tbaa !6
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %137, i32 %138, i32 %139, %"struct.btSoftBody::Material"* null, i1 zeroext false)
  br label %if.end123

if.end123:                                        ; preds = %if.then122, %for.body102
  %140 = load i8, i8* %mdy, align 1, !tbaa !122, !range !22
  %tobool124 = trunc i8 %140 to i1
  br i1 %tobool124, label %if.then125, label %if.end126

if.then125:                                       ; preds = %if.end123
  %141 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %142 = load i32, i32* %node00, align 4, !tbaa !6
  %143 = load i32, i32* %node10, align 4, !tbaa !6
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %141, i32 %142, i32 %143, %"struct.btSoftBody::Material"* null, i1 zeroext false)
  br label %if.end126

if.end126:                                        ; preds = %if.then125, %if.end123
  %144 = load i8, i8* %mdx, align 1, !tbaa !122, !range !22
  %tobool127 = trunc i8 %144 to i1
  br i1 %tobool127, label %land.lhs.true, label %if.end176

land.lhs.true:                                    ; preds = %if.end126
  %145 = load i8, i8* %mdy, align 1, !tbaa !122, !range !22
  %tobool128 = trunc i8 %145 to i1
  br i1 %tobool128, label %if.then129, label %if.end176

if.then129:                                       ; preds = %land.lhs.true
  %146 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %147 = load i32, i32* %node00, align 4, !tbaa !6
  %148 = load i32, i32* %node10, align 4, !tbaa !6
  %149 = load i32, i32* %node11, align 4, !tbaa !6
  call void @_ZN10btSoftBody10appendFaceEiiiPNS_8MaterialE(%class.btSoftBody* %146, i32 %147, i32 %148, i32 %149, %"struct.btSoftBody::Material"* null)
  %150 = load float*, float** %tex_coords.addr, align 4, !tbaa !2
  %tobool130 = icmp ne float* %150, null
  br i1 %tobool130, label %if.then131, label %if.end150

if.then131:                                       ; preds = %if.then129
  %151 = load i32, i32* %resx.addr, align 4, !tbaa !6
  %152 = load i32, i32* %resy.addr, align 4, !tbaa !6
  %153 = load i32, i32* %ix98, align 4, !tbaa !6
  %154 = load i32, i32* %iy, align 4, !tbaa !6
  %call132 = call float @_ZN17btSoftBodyHelpers11CalculateUVEiiiii(i32 %151, i32 %152, i32 %153, i32 %154, i32 0)
  %155 = load float*, float** %tex_coords.addr, align 4, !tbaa !2
  %156 = load i32, i32* %z, align 4, !tbaa !6
  %add133 = add nsw i32 %156, 0
  %arrayidx134 = getelementptr inbounds float, float* %155, i32 %add133
  store float %call132, float* %arrayidx134, align 4, !tbaa !8
  %157 = load i32, i32* %resx.addr, align 4, !tbaa !6
  %158 = load i32, i32* %resy.addr, align 4, !tbaa !6
  %159 = load i32, i32* %ix98, align 4, !tbaa !6
  %160 = load i32, i32* %iy, align 4, !tbaa !6
  %call135 = call float @_ZN17btSoftBodyHelpers11CalculateUVEiiiii(i32 %157, i32 %158, i32 %159, i32 %160, i32 1)
  %161 = load float*, float** %tex_coords.addr, align 4, !tbaa !2
  %162 = load i32, i32* %z, align 4, !tbaa !6
  %add136 = add nsw i32 %162, 1
  %arrayidx137 = getelementptr inbounds float, float* %161, i32 %add136
  store float %call135, float* %arrayidx137, align 4, !tbaa !8
  %163 = load i32, i32* %resx.addr, align 4, !tbaa !6
  %164 = load i32, i32* %resy.addr, align 4, !tbaa !6
  %165 = load i32, i32* %ix98, align 4, !tbaa !6
  %166 = load i32, i32* %iy, align 4, !tbaa !6
  %call138 = call float @_ZN17btSoftBodyHelpers11CalculateUVEiiiii(i32 %163, i32 %164, i32 %165, i32 %166, i32 0)
  %167 = load float*, float** %tex_coords.addr, align 4, !tbaa !2
  %168 = load i32, i32* %z, align 4, !tbaa !6
  %add139 = add nsw i32 %168, 2
  %arrayidx140 = getelementptr inbounds float, float* %167, i32 %add139
  store float %call138, float* %arrayidx140, align 4, !tbaa !8
  %169 = load i32, i32* %resx.addr, align 4, !tbaa !6
  %170 = load i32, i32* %resy.addr, align 4, !tbaa !6
  %171 = load i32, i32* %ix98, align 4, !tbaa !6
  %172 = load i32, i32* %iy, align 4, !tbaa !6
  %call141 = call float @_ZN17btSoftBodyHelpers11CalculateUVEiiiii(i32 %169, i32 %170, i32 %171, i32 %172, i32 2)
  %173 = load float*, float** %tex_coords.addr, align 4, !tbaa !2
  %174 = load i32, i32* %z, align 4, !tbaa !6
  %add142 = add nsw i32 %174, 3
  %arrayidx143 = getelementptr inbounds float, float* %173, i32 %add142
  store float %call141, float* %arrayidx143, align 4, !tbaa !8
  %175 = load i32, i32* %resx.addr, align 4, !tbaa !6
  %176 = load i32, i32* %resy.addr, align 4, !tbaa !6
  %177 = load i32, i32* %ix98, align 4, !tbaa !6
  %178 = load i32, i32* %iy, align 4, !tbaa !6
  %call144 = call float @_ZN17btSoftBodyHelpers11CalculateUVEiiiii(i32 %175, i32 %176, i32 %177, i32 %178, i32 3)
  %179 = load float*, float** %tex_coords.addr, align 4, !tbaa !2
  %180 = load i32, i32* %z, align 4, !tbaa !6
  %add145 = add nsw i32 %180, 4
  %arrayidx146 = getelementptr inbounds float, float* %179, i32 %add145
  store float %call144, float* %arrayidx146, align 4, !tbaa !8
  %181 = load i32, i32* %resx.addr, align 4, !tbaa !6
  %182 = load i32, i32* %resy.addr, align 4, !tbaa !6
  %183 = load i32, i32* %ix98, align 4, !tbaa !6
  %184 = load i32, i32* %iy, align 4, !tbaa !6
  %call147 = call float @_ZN17btSoftBodyHelpers11CalculateUVEiiiii(i32 %181, i32 %182, i32 %183, i32 %184, i32 2)
  %185 = load float*, float** %tex_coords.addr, align 4, !tbaa !2
  %186 = load i32, i32* %z, align 4, !tbaa !6
  %add148 = add nsw i32 %186, 5
  %arrayidx149 = getelementptr inbounds float, float* %185, i32 %add148
  store float %call147, float* %arrayidx149, align 4, !tbaa !8
  br label %if.end150

if.end150:                                        ; preds = %if.then131, %if.then129
  %187 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %188 = load i32, i32* %node11, align 4, !tbaa !6
  %189 = load i32, i32* %node01, align 4, !tbaa !6
  %190 = load i32, i32* %node00, align 4, !tbaa !6
  call void @_ZN10btSoftBody10appendFaceEiiiPNS_8MaterialE(%class.btSoftBody* %187, i32 %188, i32 %189, i32 %190, %"struct.btSoftBody::Material"* null)
  %191 = load float*, float** %tex_coords.addr, align 4, !tbaa !2
  %tobool151 = icmp ne float* %191, null
  br i1 %tobool151, label %if.then152, label %if.end171

if.then152:                                       ; preds = %if.end150
  %192 = load i32, i32* %resx.addr, align 4, !tbaa !6
  %193 = load i32, i32* %resy.addr, align 4, !tbaa !6
  %194 = load i32, i32* %ix98, align 4, !tbaa !6
  %195 = load i32, i32* %iy, align 4, !tbaa !6
  %call153 = call float @_ZN17btSoftBodyHelpers11CalculateUVEiiiii(i32 %192, i32 %193, i32 %194, i32 %195, i32 3)
  %196 = load float*, float** %tex_coords.addr, align 4, !tbaa !2
  %197 = load i32, i32* %z, align 4, !tbaa !6
  %add154 = add nsw i32 %197, 6
  %arrayidx155 = getelementptr inbounds float, float* %196, i32 %add154
  store float %call153, float* %arrayidx155, align 4, !tbaa !8
  %198 = load i32, i32* %resx.addr, align 4, !tbaa !6
  %199 = load i32, i32* %resy.addr, align 4, !tbaa !6
  %200 = load i32, i32* %ix98, align 4, !tbaa !6
  %201 = load i32, i32* %iy, align 4, !tbaa !6
  %call156 = call float @_ZN17btSoftBodyHelpers11CalculateUVEiiiii(i32 %198, i32 %199, i32 %200, i32 %201, i32 2)
  %202 = load float*, float** %tex_coords.addr, align 4, !tbaa !2
  %203 = load i32, i32* %z, align 4, !tbaa !6
  %add157 = add nsw i32 %203, 7
  %arrayidx158 = getelementptr inbounds float, float* %202, i32 %add157
  store float %call156, float* %arrayidx158, align 4, !tbaa !8
  %204 = load i32, i32* %resx.addr, align 4, !tbaa !6
  %205 = load i32, i32* %resy.addr, align 4, !tbaa !6
  %206 = load i32, i32* %ix98, align 4, !tbaa !6
  %207 = load i32, i32* %iy, align 4, !tbaa !6
  %call159 = call float @_ZN17btSoftBodyHelpers11CalculateUVEiiiii(i32 %204, i32 %205, i32 %206, i32 %207, i32 3)
  %208 = load float*, float** %tex_coords.addr, align 4, !tbaa !2
  %209 = load i32, i32* %z, align 4, !tbaa !6
  %add160 = add nsw i32 %209, 8
  %arrayidx161 = getelementptr inbounds float, float* %208, i32 %add160
  store float %call159, float* %arrayidx161, align 4, !tbaa !8
  %210 = load i32, i32* %resx.addr, align 4, !tbaa !6
  %211 = load i32, i32* %resy.addr, align 4, !tbaa !6
  %212 = load i32, i32* %ix98, align 4, !tbaa !6
  %213 = load i32, i32* %iy, align 4, !tbaa !6
  %call162 = call float @_ZN17btSoftBodyHelpers11CalculateUVEiiiii(i32 %210, i32 %211, i32 %212, i32 %213, i32 1)
  %214 = load float*, float** %tex_coords.addr, align 4, !tbaa !2
  %215 = load i32, i32* %z, align 4, !tbaa !6
  %add163 = add nsw i32 %215, 9
  %arrayidx164 = getelementptr inbounds float, float* %214, i32 %add163
  store float %call162, float* %arrayidx164, align 4, !tbaa !8
  %216 = load i32, i32* %resx.addr, align 4, !tbaa !6
  %217 = load i32, i32* %resy.addr, align 4, !tbaa !6
  %218 = load i32, i32* %ix98, align 4, !tbaa !6
  %219 = load i32, i32* %iy, align 4, !tbaa !6
  %call165 = call float @_ZN17btSoftBodyHelpers11CalculateUVEiiiii(i32 %216, i32 %217, i32 %218, i32 %219, i32 0)
  %220 = load float*, float** %tex_coords.addr, align 4, !tbaa !2
  %221 = load i32, i32* %z, align 4, !tbaa !6
  %add166 = add nsw i32 %221, 10
  %arrayidx167 = getelementptr inbounds float, float* %220, i32 %add166
  store float %call165, float* %arrayidx167, align 4, !tbaa !8
  %222 = load i32, i32* %resx.addr, align 4, !tbaa !6
  %223 = load i32, i32* %resy.addr, align 4, !tbaa !6
  %224 = load i32, i32* %ix98, align 4, !tbaa !6
  %225 = load i32, i32* %iy, align 4, !tbaa !6
  %call168 = call float @_ZN17btSoftBodyHelpers11CalculateUVEiiiii(i32 %222, i32 %223, i32 %224, i32 %225, i32 1)
  %226 = load float*, float** %tex_coords.addr, align 4, !tbaa !2
  %227 = load i32, i32* %z, align 4, !tbaa !6
  %add169 = add nsw i32 %227, 11
  %arrayidx170 = getelementptr inbounds float, float* %226, i32 %add169
  store float %call168, float* %arrayidx170, align 4, !tbaa !8
  br label %if.end171

if.end171:                                        ; preds = %if.then152, %if.end150
  %228 = load i8, i8* %gendiags.addr, align 1, !tbaa !122, !range !22
  %tobool172 = trunc i8 %228 to i1
  br i1 %tobool172, label %if.then173, label %if.end174

if.then173:                                       ; preds = %if.end171
  %229 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %230 = load i32, i32* %node00, align 4, !tbaa !6
  %231 = load i32, i32* %node11, align 4, !tbaa !6
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %229, i32 %230, i32 %231, %"struct.btSoftBody::Material"* null, i1 zeroext false)
  br label %if.end174

if.end174:                                        ; preds = %if.then173, %if.end171
  %232 = load i32, i32* %z, align 4, !tbaa !6
  %add175 = add nsw i32 %232, 12
  store i32 %add175, i32* %z, align 4, !tbaa !6
  br label %if.end176

if.end176:                                        ; preds = %if.end174, %land.lhs.true, %if.end126
  %233 = bitcast i32* %node11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %233) #6
  %234 = bitcast i32* %node10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %234) #6
  %235 = bitcast i32* %node01 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %235) #6
  %236 = bitcast i32* %node00 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %236) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mdy) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mdx) #6
  br label %for.inc177

for.inc177:                                       ; preds = %if.end176
  %237 = load i32, i32* %ix98, align 4, !tbaa !6
  %inc178 = add nsw i32 %237, 1
  store i32 %inc178, i32* %ix98, align 4, !tbaa !6
  br label %for.cond99

for.end179:                                       ; preds = %for.cond.cleanup101
  br label %for.inc180

for.inc180:                                       ; preds = %for.end179
  %238 = load i32, i32* %iy, align 4, !tbaa !6
  %inc181 = add nsw i32 %238, 1
  store i32 %inc181, i32* %iy, align 4, !tbaa !6
  br label %for.cond95

for.end182:                                       ; preds = %for.cond95
  %239 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  store %class.btSoftBody* %239, %class.btSoftBody** %retval, align 4
  %240 = bitcast i32* %z to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %240) #6
  %241 = bitcast %class.btSoftBody** %psb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %241) #6
  %242 = bitcast i32* %iy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %242) #6
  %243 = bitcast float** %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %243) #6
  %244 = bitcast %class.btVector3** %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %244) #6
  %245 = bitcast i32* %tot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %245) #6
  %246 = bitcast i32* %ry to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %246) #6
  %247 = bitcast i32* %rx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %247) #6
  br label %return

return:                                           ; preds = %for.end182, %if.then
  %248 = load %class.btSoftBody*, %class.btSoftBody** %retval, align 4
  ret %class.btSoftBody* %248
}

; Function Attrs: nounwind
define hidden float @_ZN17btSoftBodyHelpers11CalculateUVEiiiii(i32 %resx, i32 %resy, i32 %ix, i32 %iy, i32 %id) #5 {
entry:
  %resx.addr = alloca i32, align 4
  %resy.addr = alloca i32, align 4
  %ix.addr = alloca i32, align 4
  %iy.addr = alloca i32, align 4
  %id.addr = alloca i32, align 4
  %tc = alloca float, align 4
  store i32 %resx, i32* %resx.addr, align 4, !tbaa !6
  store i32 %resy, i32* %resy.addr, align 4, !tbaa !6
  store i32 %ix, i32* %ix.addr, align 4, !tbaa !6
  store i32 %iy, i32* %iy.addr, align 4, !tbaa !6
  store i32 %id, i32* %id.addr, align 4, !tbaa !6
  %0 = bitcast float* %tc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store float 0.000000e+00, float* %tc, align 4, !tbaa !8
  %1 = load i32, i32* %id.addr, align 4, !tbaa !6
  %cmp = icmp eq i32 %1, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %resx.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %2, 1
  %conv = sitofp i32 %sub to float
  %div = fdiv float 1.000000e+00, %conv
  %3 = load i32, i32* %ix.addr, align 4, !tbaa !6
  %conv1 = sitofp i32 %3 to float
  %mul = fmul float %div, %conv1
  store float %mul, float* %tc, align 4, !tbaa !8
  br label %if.end32

if.else:                                          ; preds = %entry
  %4 = load i32, i32* %id.addr, align 4, !tbaa !6
  %cmp2 = icmp eq i32 %4, 1
  br i1 %cmp2, label %if.then3, label %if.else11

if.then3:                                         ; preds = %if.else
  %5 = load i32, i32* %resy.addr, align 4, !tbaa !6
  %sub4 = sub nsw i32 %5, 1
  %conv5 = sitofp i32 %sub4 to float
  %div6 = fdiv float 1.000000e+00, %conv5
  %6 = load i32, i32* %resy.addr, align 4, !tbaa !6
  %sub7 = sub nsw i32 %6, 1
  %7 = load i32, i32* %iy.addr, align 4, !tbaa !6
  %sub8 = sub nsw i32 %sub7, %7
  %conv9 = sitofp i32 %sub8 to float
  %mul10 = fmul float %div6, %conv9
  store float %mul10, float* %tc, align 4, !tbaa !8
  br label %if.end31

if.else11:                                        ; preds = %if.else
  %8 = load i32, i32* %id.addr, align 4, !tbaa !6
  %cmp12 = icmp eq i32 %8, 2
  br i1 %cmp12, label %if.then13, label %if.else22

if.then13:                                        ; preds = %if.else11
  %9 = load i32, i32* %resy.addr, align 4, !tbaa !6
  %sub14 = sub nsw i32 %9, 1
  %conv15 = sitofp i32 %sub14 to float
  %div16 = fdiv float 1.000000e+00, %conv15
  %10 = load i32, i32* %resy.addr, align 4, !tbaa !6
  %sub17 = sub nsw i32 %10, 1
  %11 = load i32, i32* %iy.addr, align 4, !tbaa !6
  %sub18 = sub nsw i32 %sub17, %11
  %sub19 = sub nsw i32 %sub18, 1
  %conv20 = sitofp i32 %sub19 to float
  %mul21 = fmul float %div16, %conv20
  store float %mul21, float* %tc, align 4, !tbaa !8
  br label %if.end30

if.else22:                                        ; preds = %if.else11
  %12 = load i32, i32* %id.addr, align 4, !tbaa !6
  %cmp23 = icmp eq i32 %12, 3
  br i1 %cmp23, label %if.then24, label %if.end

if.then24:                                        ; preds = %if.else22
  %13 = load i32, i32* %resx.addr, align 4, !tbaa !6
  %sub25 = sub nsw i32 %13, 1
  %conv26 = sitofp i32 %sub25 to float
  %div27 = fdiv float 1.000000e+00, %conv26
  %14 = load i32, i32* %ix.addr, align 4, !tbaa !6
  %add = add nsw i32 %14, 1
  %conv28 = sitofp i32 %add to float
  %mul29 = fmul float %div27, %conv28
  store float %mul29, float* %tc, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then24, %if.else22
  br label %if.end30

if.end30:                                         ; preds = %if.end, %if.then13
  br label %if.end31

if.end31:                                         ; preds = %if.end30, %if.then3
  br label %if.end32

if.end32:                                         ; preds = %if.end31, %if.then
  %15 = load float, float* %tc, align 4, !tbaa !8
  %16 = bitcast float* %tc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  ret float %15
}

define hidden %class.btSoftBody* @_ZN17btSoftBodyHelpers15CreateEllipsoidER19btSoftBodyWorldInfoRK9btVector3S4_i(%struct.btSoftBodyWorldInfo* nonnull align 4 dereferenceable(100) %worldInfo, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %radius, i32 %res) #0 {
entry:
  %worldInfo.addr = alloca %struct.btSoftBodyWorldInfo*, align 4
  %center.addr = alloca %class.btVector3*, align 4
  %radius.addr = alloca %class.btVector3*, align 4
  %res.addr = alloca i32, align 4
  %vtx = alloca %class.btAlignedObjectArray.8, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  store %struct.btSoftBodyWorldInfo* %worldInfo, %struct.btSoftBodyWorldInfo** %worldInfo.addr, align 4, !tbaa !2
  store %class.btVector3* %center, %class.btVector3** %center.addr, align 4, !tbaa !2
  store %class.btVector3* %radius, %class.btVector3** %radius.addr, align 4, !tbaa !2
  store i32 %res, i32* %res.addr, align 4, !tbaa !6
  %0 = bitcast %class.btAlignedObjectArray.8* %vtx to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %0) #6
  %call = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.8* %vtx)
  %1 = load i32, i32* %res.addr, align 4, !tbaa !6
  %add = add nsw i32 3, %1
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #6
  %call1 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.8* %vtx, i32 %add, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %3) #6
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vtx, i32 0)
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %vtx)
  call void @_ZZN17btSoftBodyHelpers15CreateEllipsoidER19btSoftBodyWorldInfoRK9btVector3S4_iEN10Hammersley8GenerateEPS2_i(%class.btVector3* %call2, i32 %call3)
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %vtx)
  %cmp = icmp slt i32 %5, %call4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %6 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %7 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #6
  %8 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #6
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vtx, i32 %9)
  %10 = load %class.btVector3*, %class.btVector3** %radius.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %call7, %class.btVector3* nonnull align 4 dereferenceable(16) %10)
  %11 = load %class.btVector3*, %class.btVector3** %center.addr, align 4, !tbaa !2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %11)
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vtx, i32 %12)
  %13 = bitcast %class.btVector3* %call8 to i8*
  %14 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 16, i1 false), !tbaa.struct !23
  %15 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %15) #6
  %16 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %16) #6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %17 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %18 = load %struct.btSoftBodyWorldInfo*, %struct.btSoftBodyWorldInfo** %worldInfo.addr, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vtx, i32 0)
  %call10 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %vtx)
  %call11 = call %class.btSoftBody* @_ZN17btSoftBodyHelpers20CreateFromConvexHullER19btSoftBodyWorldInfoPK9btVector3ib(%struct.btSoftBodyWorldInfo* nonnull align 4 dereferenceable(100) %18, %class.btVector3* %call9, i32 %call10, i1 zeroext true)
  %call12 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.8* %vtx) #6
  %19 = bitcast %class.btAlignedObjectArray.8* %vtx to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %19) #6
  ret %class.btSoftBody* %call11
}

define internal void @_ZZN17btSoftBodyHelpers15CreateEllipsoidER19btSoftBodyWorldInfoRK9btVector3S4_iEN10Hammersley8GenerateEPS2_i(%class.btVector3* %x, i32 %n) #0 {
entry:
  %x.addr = alloca %class.btVector3*, align 4
  %n.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %p = alloca float, align 4
  %t = alloca float, align 4
  %j = alloca i32, align 4
  %w = alloca float, align 4
  %a = alloca float, align 4
  %s = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc21, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %2 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #6
  br label %for.end22

for.body:                                         ; preds = %for.cond
  %4 = bitcast float* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  store float 5.000000e-01, float* %p, align 4, !tbaa !8
  %5 = bitcast float* %t to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  store float 0.000000e+00, float* %t, align 4, !tbaa !8
  %6 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load i32, i32* %i, align 4, !tbaa !6
  store i32 %7, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %8 = load i32, i32* %j, align 4, !tbaa !6
  %tobool = icmp ne i32 %8, 0
  br i1 %tobool, label %for.body3, label %for.cond.cleanup2

for.cond.cleanup2:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %9 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #6
  br label %for.end

for.body3:                                        ; preds = %for.cond1
  %10 = load i32, i32* %j, align 4, !tbaa !6
  %and = and i32 %10, 1
  %tobool4 = icmp ne i32 %and, 0
  br i1 %tobool4, label %if.then, label %if.end

if.then:                                          ; preds = %for.body3
  %11 = load float, float* %p, align 4, !tbaa !8
  %12 = load float, float* %t, align 4, !tbaa !8
  %add = fadd float %12, %11
  store float %add, float* %t, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body3
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %13 = load float, float* %p, align 4, !tbaa !8
  %conv = fpext float %13 to double
  %mul = fmul double %conv, 5.000000e-01
  %conv5 = fptrunc double %mul to float
  store float %conv5, float* %p, align 4, !tbaa !8
  %14 = load i32, i32* %j, align 4, !tbaa !6
  %shr = ashr i32 %14, 1
  store i32 %shr, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup2
  %15 = bitcast float* %w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #6
  %16 = load float, float* %t, align 4, !tbaa !8
  %mul6 = fmul float 2.000000e+00, %16
  %sub = fsub float %mul6, 1.000000e+00
  store float %sub, float* %w, align 4, !tbaa !8
  %17 = bitcast float* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #6
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %mul7 = mul nsw i32 2, %18
  %conv8 = sitofp i32 %mul7 to float
  %mul9 = fmul float %conv8, 0x400921FB60000000
  %add10 = fadd float 0x400921FB60000000, %mul9
  %19 = load i32, i32* %n.addr, align 4, !tbaa !6
  %conv11 = sitofp i32 %19 to float
  %div = fdiv float %add10, %conv11
  store float %div, float* %a, align 4, !tbaa !8
  %20 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #6
  %21 = load float, float* %w, align 4, !tbaa !8
  %22 = load float, float* %w, align 4, !tbaa !8
  %mul12 = fmul float %21, %22
  %sub13 = fsub float 1.000000e+00, %mul12
  %call = call float @_Z6btSqrtf(float %sub13)
  store float %call, float* %s, align 4, !tbaa !8
  %23 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %23) #6
  %24 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #6
  %25 = load float, float* %s, align 4, !tbaa !8
  %26 = load float, float* %a, align 4, !tbaa !8
  %call15 = call float @_Z5btCosf(float %26)
  %mul16 = fmul float %25, %call15
  store float %mul16, float* %ref.tmp14, align 4, !tbaa !8
  %27 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #6
  %28 = load float, float* %s, align 4, !tbaa !8
  %29 = load float, float* %a, align 4, !tbaa !8
  %call18 = call float @_Z5btSinf(float %29)
  %mul19 = fmul float %28, %call18
  store float %mul19, float* %ref.tmp17, align 4, !tbaa !8
  %call20 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %w)
  %30 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %class.btVector3, %class.btVector3* %30, i32 1
  store %class.btVector3* %incdec.ptr, %class.btVector3** %x.addr, align 4, !tbaa !2
  %31 = bitcast %class.btVector3* %30 to i8*
  %32 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %31, i8* align 4 %32, i32 16, i1 false), !tbaa.struct !23
  %33 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #6
  %34 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #6
  %35 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %35) #6
  %36 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #6
  %37 = bitcast float* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #6
  %38 = bitcast float* %w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #6
  %39 = bitcast float* %t to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #6
  %40 = bitcast float* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #6
  br label %for.inc21

for.inc21:                                        ; preds = %for.end
  %41 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %41, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end22:                                        ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !8
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !8
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !8
  %mul8 = fmul float %7, %9
  store float %mul8, float* %ref.tmp3, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !8
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !8
  %mul14 = fmul float %12, %14
  store float %mul14, float* %ref.tmp9, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  ret void
}

define hidden %class.btSoftBody* @_ZN17btSoftBodyHelpers20CreateFromConvexHullER19btSoftBodyWorldInfoPK9btVector3ib(%struct.btSoftBodyWorldInfo* nonnull align 4 dereferenceable(100) %worldInfo, %class.btVector3* %vertices, i32 %nvertices, i1 zeroext %randomizeConstraints) #0 {
entry:
  %worldInfo.addr = alloca %struct.btSoftBodyWorldInfo*, align 4
  %vertices.addr = alloca %class.btVector3*, align 4
  %nvertices.addr = alloca i32, align 4
  %randomizeConstraints.addr = alloca i8, align 1
  %hdsc = alloca %class.HullDesc, align 4
  %hres = alloca %class.HullResult, align 4
  %hlib = alloca %class.HullLibrary, align 4
  %psb = alloca %class.btSoftBody*, align 4
  %i = alloca i32, align 4
  %idx = alloca [3 x i32], align 4
  store %struct.btSoftBodyWorldInfo* %worldInfo, %struct.btSoftBodyWorldInfo** %worldInfo.addr, align 4, !tbaa !2
  store %class.btVector3* %vertices, %class.btVector3** %vertices.addr, align 4, !tbaa !2
  store i32 %nvertices, i32* %nvertices.addr, align 4, !tbaa !6
  %frombool = zext i1 %randomizeConstraints to i8
  store i8 %frombool, i8* %randomizeConstraints.addr, align 1, !tbaa !122
  %0 = bitcast %class.HullDesc* %hdsc to i8*
  call void @llvm.lifetime.start.p0i8(i64 28, i8* %0) #6
  %1 = load i32, i32* %nvertices.addr, align 4, !tbaa !6
  %2 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4, !tbaa !2
  %call = call %class.HullDesc* @_ZN8HullDescC2E8HullFlagjPK9btVector3j(%class.HullDesc* %hdsc, i32 1, i32 %1, %class.btVector3* %2, i32 16)
  %3 = bitcast %class.HullResult* %hres to i8*
  call void @llvm.lifetime.start.p0i8(i64 56, i8* %3) #6
  %call1 = call %class.HullResult* @_ZN10HullResultC2Ev(%class.HullResult* %hres)
  %4 = bitcast %class.HullLibrary* %hlib to i8*
  call void @llvm.lifetime.start.p0i8(i64 40, i8* %4) #6
  %call2 = call %class.HullLibrary* @_ZN11HullLibraryC2Ev(%class.HullLibrary* %hlib)
  %5 = load i32, i32* %nvertices.addr, align 4, !tbaa !6
  %mMaxVertices = getelementptr inbounds %class.HullDesc, %class.HullDesc* %hdsc, i32 0, i32 5
  store i32 %5, i32* %mMaxVertices, align 4, !tbaa !127
  %call3 = call i32 @_ZN11HullLibrary16CreateConvexHullERK8HullDescR10HullResult(%class.HullLibrary* %hlib, %class.HullDesc* nonnull align 4 dereferenceable(28) %hdsc, %class.HullResult* nonnull align 4 dereferenceable(56) %hres)
  %6 = bitcast %class.btSoftBody** %psb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %call4 = call i8* @_ZN17btCollisionObjectnwEm(i32 1252)
  %7 = bitcast i8* %call4 to %class.btSoftBody*
  %8 = load %struct.btSoftBodyWorldInfo*, %struct.btSoftBodyWorldInfo** %worldInfo.addr, align 4, !tbaa !2
  %mNumOutputVertices = getelementptr inbounds %class.HullResult, %class.HullResult* %hres, i32 0, i32 1
  %9 = load i32, i32* %mNumOutputVertices, align 4, !tbaa !129
  %m_OutputVertices = getelementptr inbounds %class.HullResult, %class.HullResult* %hres, i32 0, i32 2
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %m_OutputVertices, i32 0)
  %call6 = call %class.btSoftBody* @_ZN10btSoftBodyC1EP19btSoftBodyWorldInfoiPK9btVector3PKf(%class.btSoftBody* %7, %struct.btSoftBodyWorldInfo* %8, i32 %9, %class.btVector3* %call5, float* null)
  store %class.btSoftBody* %7, %class.btSoftBody** %psb, align 4, !tbaa !2
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %mNumFaces = getelementptr inbounds %class.HullResult, %class.HullResult* %hres, i32 0, i32 3
  %12 = load i32, i32* %mNumFaces, align 4, !tbaa !133
  %cmp = icmp slt i32 %11, %12
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %14 = bitcast [3 x i32]* %idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %14) #6
  %arrayinit.begin = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 0
  %m_Indices = getelementptr inbounds %class.HullResult, %class.HullResult* %hres, i32 0, i32 5
  %15 = load i32, i32* %i, align 4, !tbaa !6
  %mul = mul nsw i32 %15, 3
  %add = add nsw i32 %mul, 0
  %call7 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIjEixEi(%class.btAlignedObjectArray.93* %m_Indices, i32 %add)
  %16 = load i32, i32* %call7, align 4, !tbaa !6
  store i32 %16, i32* %arrayinit.begin, align 4, !tbaa !6
  %arrayinit.element = getelementptr inbounds i32, i32* %arrayinit.begin, i32 1
  %m_Indices8 = getelementptr inbounds %class.HullResult, %class.HullResult* %hres, i32 0, i32 5
  %17 = load i32, i32* %i, align 4, !tbaa !6
  %mul9 = mul nsw i32 %17, 3
  %add10 = add nsw i32 %mul9, 1
  %call11 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIjEixEi(%class.btAlignedObjectArray.93* %m_Indices8, i32 %add10)
  %18 = load i32, i32* %call11, align 4, !tbaa !6
  store i32 %18, i32* %arrayinit.element, align 4, !tbaa !6
  %arrayinit.element12 = getelementptr inbounds i32, i32* %arrayinit.element, i32 1
  %m_Indices13 = getelementptr inbounds %class.HullResult, %class.HullResult* %hres, i32 0, i32 5
  %19 = load i32, i32* %i, align 4, !tbaa !6
  %mul14 = mul nsw i32 %19, 3
  %add15 = add nsw i32 %mul14, 2
  %call16 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIjEixEi(%class.btAlignedObjectArray.93* %m_Indices13, i32 %add15)
  %20 = load i32, i32* %call16, align 4, !tbaa !6
  store i32 %20, i32* %arrayinit.element12, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 0
  %21 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %arrayidx17 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 1
  %22 = load i32, i32* %arrayidx17, align 4, !tbaa !6
  %cmp18 = icmp slt i32 %21, %22
  br i1 %cmp18, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %23 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 0
  %24 = load i32, i32* %arrayidx19, align 4, !tbaa !6
  %arrayidx20 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 1
  %25 = load i32, i32* %arrayidx20, align 4, !tbaa !6
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %23, i32 %24, i32 %25, %"struct.btSoftBody::Material"* null, i1 zeroext false)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %arrayidx21 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 1
  %26 = load i32, i32* %arrayidx21, align 4, !tbaa !6
  %arrayidx22 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 2
  %27 = load i32, i32* %arrayidx22, align 4, !tbaa !6
  %cmp23 = icmp slt i32 %26, %27
  br i1 %cmp23, label %if.then24, label %if.end27

if.then24:                                        ; preds = %if.end
  %28 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 1
  %29 = load i32, i32* %arrayidx25, align 4, !tbaa !6
  %arrayidx26 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 2
  %30 = load i32, i32* %arrayidx26, align 4, !tbaa !6
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %28, i32 %29, i32 %30, %"struct.btSoftBody::Material"* null, i1 zeroext false)
  br label %if.end27

if.end27:                                         ; preds = %if.then24, %if.end
  %arrayidx28 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 2
  %31 = load i32, i32* %arrayidx28, align 4, !tbaa !6
  %arrayidx29 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 0
  %32 = load i32, i32* %arrayidx29, align 4, !tbaa !6
  %cmp30 = icmp slt i32 %31, %32
  br i1 %cmp30, label %if.then31, label %if.end34

if.then31:                                        ; preds = %if.end27
  %33 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %arrayidx32 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 2
  %34 = load i32, i32* %arrayidx32, align 4, !tbaa !6
  %arrayidx33 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 0
  %35 = load i32, i32* %arrayidx33, align 4, !tbaa !6
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %33, i32 %34, i32 %35, %"struct.btSoftBody::Material"* null, i1 zeroext false)
  br label %if.end34

if.end34:                                         ; preds = %if.then31, %if.end27
  %36 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %arrayidx35 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 0
  %37 = load i32, i32* %arrayidx35, align 4, !tbaa !6
  %arrayidx36 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 1
  %38 = load i32, i32* %arrayidx36, align 4, !tbaa !6
  %arrayidx37 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 2
  %39 = load i32, i32* %arrayidx37, align 4, !tbaa !6
  call void @_ZN10btSoftBody10appendFaceEiiiPNS_8MaterialE(%class.btSoftBody* %36, i32 %37, i32 %38, i32 %39, %"struct.btSoftBody::Material"* null)
  %40 = bitcast [3 x i32]* %idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %40) #6
  br label %for.inc

for.inc:                                          ; preds = %if.end34
  %41 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %41, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %call38 = call i32 @_ZN11HullLibrary13ReleaseResultER10HullResult(%class.HullLibrary* %hlib, %class.HullResult* nonnull align 4 dereferenceable(56) %hres)
  %42 = load i8, i8* %randomizeConstraints.addr, align 1, !tbaa !122, !range !22
  %tobool = trunc i8 %42 to i1
  br i1 %tobool, label %if.then39, label %if.end40

if.then39:                                        ; preds = %for.end
  %43 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  call void @_ZN10btSoftBody20randomizeConstraintsEv(%class.btSoftBody* %43)
  br label %if.end40

if.end40:                                         ; preds = %if.then39, %for.end
  %44 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %45 = bitcast %class.btSoftBody** %psb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #6
  %call41 = call %class.HullLibrary* @_ZN11HullLibraryD2Ev(%class.HullLibrary* %hlib) #6
  %46 = bitcast %class.HullLibrary* %hlib to i8*
  call void @llvm.lifetime.end.p0i8(i64 40, i8* %46) #6
  %call42 = call %class.HullResult* @_ZN10HullResultD2Ev(%class.HullResult* %hres) #6
  %47 = bitcast %class.HullResult* %hres to i8*
  call void @llvm.lifetime.end.p0i8(i64 56, i8* %47) #6
  %48 = bitcast %class.HullDesc* %hdsc to i8*
  call void @llvm.lifetime.end.p0i8(i64 28, i8* %48) #6
  ret %class.btSoftBody* %44
}

define hidden %class.btSoftBody* @_ZN17btSoftBodyHelpers17CreateFromTriMeshER19btSoftBodyWorldInfoPKfPKiib(%struct.btSoftBodyWorldInfo* nonnull align 4 dereferenceable(100) %worldInfo, float* %vertices, i32* %triangles, i32 %ntriangles, i1 zeroext %randomizeConstraints) #0 {
entry:
  %worldInfo.addr = alloca %struct.btSoftBodyWorldInfo*, align 4
  %vertices.addr = alloca float*, align 4
  %triangles.addr = alloca i32*, align 4
  %ntriangles.addr = alloca i32, align 4
  %randomizeConstraints.addr = alloca i8, align 1
  %maxidx = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %ni = alloca i32, align 4
  %chks = alloca %class.btAlignedObjectArray.81, align 4
  %vtx = alloca %class.btAlignedObjectArray.8, align 4
  %ref.tmp = alloca i8, align 1
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  %psb = alloca %class.btSoftBody*, align 4
  %idx = alloca [3 x i32], align 4
  %j36 = alloca i32, align 4
  %k = alloca i32, align 4
  store %struct.btSoftBodyWorldInfo* %worldInfo, %struct.btSoftBodyWorldInfo** %worldInfo.addr, align 4, !tbaa !2
  store float* %vertices, float** %vertices.addr, align 4, !tbaa !2
  store i32* %triangles, i32** %triangles.addr, align 4, !tbaa !2
  store i32 %ntriangles, i32* %ntriangles.addr, align 4, !tbaa !6
  %frombool = zext i1 %randomizeConstraints to i8
  store i8 %frombool, i8* %randomizeConstraints.addr, align 1, !tbaa !122
  %0 = bitcast i32* %maxidx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %maxidx, align 4, !tbaa !6
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = bitcast i32* %ni to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  %4 = load i32, i32* %ntriangles.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %4, 3
  store i32 %mul, i32* %ni, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %ni, align 4, !tbaa !6
  %cmp = icmp slt i32 %5, %6
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load i32*, i32** %triangles.addr, align 4, !tbaa !2
  %8 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32, i32* %7, i32 %8
  %call = call nonnull align 4 dereferenceable(4) i32* @_Z5btMaxIiERKT_S2_S2_(i32* nonnull align 4 dereferenceable(4) %arrayidx, i32* nonnull align 4 dereferenceable(4) %maxidx)
  %9 = load i32, i32* %call, align 4, !tbaa !6
  store i32 %9, i32* %maxidx, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %11 = load i32, i32* %maxidx, align 4, !tbaa !6
  %inc1 = add nsw i32 %11, 1
  store i32 %inc1, i32* %maxidx, align 4, !tbaa !6
  %12 = bitcast %class.btAlignedObjectArray.81* %chks to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %12) #6
  %call2 = call %class.btAlignedObjectArray.81* @_ZN20btAlignedObjectArrayIbEC2Ev(%class.btAlignedObjectArray.81* %chks)
  %13 = bitcast %class.btAlignedObjectArray.8* %vtx to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %13) #6
  %call3 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.8* %vtx)
  %14 = load i32, i32* %maxidx, align 4, !tbaa !6
  %15 = load i32, i32* %maxidx, align 4, !tbaa !6
  %mul4 = mul nsw i32 %14, %15
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ref.tmp) #6
  store i8 0, i8* %ref.tmp, align 1, !tbaa !122
  call void @_ZN20btAlignedObjectArrayIbE6resizeEiRKb(%class.btAlignedObjectArray.81* %chks, i32 %mul4, i8* nonnull align 1 dereferenceable(1) %ref.tmp)
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ref.tmp) #6
  %16 = load i32, i32* %maxidx, align 4, !tbaa !6
  %17 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %17) #6
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp5)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.8* %vtx, i32 %16, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %18 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  store i32 0, i32* %j, align 4, !tbaa !6
  %19 = load i32, i32* %maxidx, align 4, !tbaa !6
  %mul7 = mul nsw i32 %19, 3
  store i32 %mul7, i32* %ni, align 4, !tbaa !6
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc18, %for.end
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %21 = load i32, i32* %ni, align 4, !tbaa !6
  %cmp9 = icmp slt i32 %20, %21
  br i1 %cmp9, label %for.body10, label %for.end21

for.body10:                                       ; preds = %for.cond8
  %22 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %22) #6
  %23 = load float*, float** %vertices.addr, align 4, !tbaa !2
  %24 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds float, float* %23, i32 %24
  %25 = load float*, float** %vertices.addr, align 4, !tbaa !2
  %26 = load i32, i32* %i, align 4, !tbaa !6
  %add = add nsw i32 %26, 1
  %arrayidx13 = getelementptr inbounds float, float* %25, i32 %add
  %27 = load float*, float** %vertices.addr, align 4, !tbaa !2
  %28 = load i32, i32* %i, align 4, !tbaa !6
  %add14 = add nsw i32 %28, 2
  %arrayidx15 = getelementptr inbounds float, float* %27, i32 %add14
  %call16 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp11, float* nonnull align 4 dereferenceable(4) %arrayidx12, float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %arrayidx15)
  %29 = load i32, i32* %j, align 4, !tbaa !6
  %call17 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vtx, i32 %29)
  %30 = bitcast %class.btVector3* %call17 to i8*
  %31 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %30, i8* align 4 %31, i32 16, i1 false), !tbaa.struct !23
  %32 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %32) #6
  br label %for.inc18

for.inc18:                                        ; preds = %for.body10
  %33 = load i32, i32* %j, align 4, !tbaa !6
  %inc19 = add nsw i32 %33, 1
  store i32 %inc19, i32* %j, align 4, !tbaa !6
  %34 = load i32, i32* %i, align 4, !tbaa !6
  %add20 = add nsw i32 %34, 3
  store i32 %add20, i32* %i, align 4, !tbaa !6
  br label %for.cond8

for.end21:                                        ; preds = %for.cond8
  %35 = bitcast %class.btSoftBody** %psb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #6
  %call22 = call i8* @_ZN17btCollisionObjectnwEm(i32 1252)
  %36 = bitcast i8* %call22 to %class.btSoftBody*
  %37 = load %struct.btSoftBodyWorldInfo*, %struct.btSoftBodyWorldInfo** %worldInfo.addr, align 4, !tbaa !2
  %call23 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %vtx)
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vtx, i32 0)
  %call25 = call %class.btSoftBody* @_ZN10btSoftBodyC1EP19btSoftBodyWorldInfoiPK9btVector3PKf(%class.btSoftBody* %36, %struct.btSoftBodyWorldInfo* %37, i32 %call23, %class.btVector3* %call24, float* null)
  store %class.btSoftBody* %36, %class.btSoftBody** %psb, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !6
  %38 = load i32, i32* %ntriangles.addr, align 4, !tbaa !6
  %mul26 = mul nsw i32 %38, 3
  store i32 %mul26, i32* %ni, align 4, !tbaa !6
  br label %for.cond27

for.cond27:                                       ; preds = %for.inc63, %for.end21
  %39 = load i32, i32* %i, align 4, !tbaa !6
  %40 = load i32, i32* %ni, align 4, !tbaa !6
  %cmp28 = icmp slt i32 %39, %40
  br i1 %cmp28, label %for.body29, label %for.end65

for.body29:                                       ; preds = %for.cond27
  %41 = bitcast [3 x i32]* %idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %41) #6
  %arrayinit.begin = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 0
  %42 = load i32*, i32** %triangles.addr, align 4, !tbaa !2
  %43 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx30 = getelementptr inbounds i32, i32* %42, i32 %43
  %44 = load i32, i32* %arrayidx30, align 4, !tbaa !6
  store i32 %44, i32* %arrayinit.begin, align 4, !tbaa !6
  %arrayinit.element = getelementptr inbounds i32, i32* %arrayinit.begin, i32 1
  %45 = load i32*, i32** %triangles.addr, align 4, !tbaa !2
  %46 = load i32, i32* %i, align 4, !tbaa !6
  %add31 = add nsw i32 %46, 1
  %arrayidx32 = getelementptr inbounds i32, i32* %45, i32 %add31
  %47 = load i32, i32* %arrayidx32, align 4, !tbaa !6
  store i32 %47, i32* %arrayinit.element, align 4, !tbaa !6
  %arrayinit.element33 = getelementptr inbounds i32, i32* %arrayinit.element, i32 1
  %48 = load i32*, i32** %triangles.addr, align 4, !tbaa !2
  %49 = load i32, i32* %i, align 4, !tbaa !6
  %add34 = add nsw i32 %49, 2
  %arrayidx35 = getelementptr inbounds i32, i32* %48, i32 %add34
  %50 = load i32, i32* %arrayidx35, align 4, !tbaa !6
  store i32 %50, i32* %arrayinit.element33, align 4, !tbaa !6
  %51 = bitcast i32* %j36 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #6
  store i32 2, i32* %j36, align 4, !tbaa !6
  %52 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #6
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %for.cond37

for.cond37:                                       ; preds = %for.inc57, %for.body29
  %53 = load i32, i32* %k, align 4, !tbaa !6
  %cmp38 = icmp slt i32 %53, 3
  br i1 %cmp38, label %for.body39, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond37
  %54 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #6
  %55 = bitcast i32* %j36 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #6
  br label %for.end59

for.body39:                                       ; preds = %for.cond37
  %56 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx40 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 %56
  %57 = load i32, i32* %arrayidx40, align 4, !tbaa !6
  %58 = load i32, i32* %maxidx, align 4, !tbaa !6
  %mul41 = mul nsw i32 %57, %58
  %59 = load i32, i32* %j36, align 4, !tbaa !6
  %arrayidx42 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 %59
  %60 = load i32, i32* %arrayidx42, align 4, !tbaa !6
  %add43 = add nsw i32 %mul41, %60
  %call44 = call nonnull align 1 dereferenceable(1) i8* @_ZN20btAlignedObjectArrayIbEixEi(%class.btAlignedObjectArray.81* %chks, i32 %add43)
  %61 = load i8, i8* %call44, align 1, !tbaa !122, !range !22
  %tobool = trunc i8 %61 to i1
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %for.body39
  %62 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx45 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 %62
  %63 = load i32, i32* %arrayidx45, align 4, !tbaa !6
  %64 = load i32, i32* %maxidx, align 4, !tbaa !6
  %mul46 = mul nsw i32 %63, %64
  %65 = load i32, i32* %j36, align 4, !tbaa !6
  %arrayidx47 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 %65
  %66 = load i32, i32* %arrayidx47, align 4, !tbaa !6
  %add48 = add nsw i32 %mul46, %66
  %call49 = call nonnull align 1 dereferenceable(1) i8* @_ZN20btAlignedObjectArrayIbEixEi(%class.btAlignedObjectArray.81* %chks, i32 %add48)
  store i8 1, i8* %call49, align 1, !tbaa !122
  %67 = load i32, i32* %j36, align 4, !tbaa !6
  %arrayidx50 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 %67
  %68 = load i32, i32* %arrayidx50, align 4, !tbaa !6
  %69 = load i32, i32* %maxidx, align 4, !tbaa !6
  %mul51 = mul nsw i32 %68, %69
  %70 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx52 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 %70
  %71 = load i32, i32* %arrayidx52, align 4, !tbaa !6
  %add53 = add nsw i32 %mul51, %71
  %call54 = call nonnull align 1 dereferenceable(1) i8* @_ZN20btAlignedObjectArrayIbEixEi(%class.btAlignedObjectArray.81* %chks, i32 %add53)
  store i8 1, i8* %call54, align 1, !tbaa !122
  %72 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %73 = load i32, i32* %j36, align 4, !tbaa !6
  %arrayidx55 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 %73
  %74 = load i32, i32* %arrayidx55, align 4, !tbaa !6
  %75 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx56 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 %75
  %76 = load i32, i32* %arrayidx56, align 4, !tbaa !6
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %72, i32 %74, i32 %76, %"struct.btSoftBody::Material"* null, i1 zeroext false)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body39
  br label %for.inc57

for.inc57:                                        ; preds = %if.end
  %77 = load i32, i32* %k, align 4, !tbaa !6
  %inc58 = add nsw i32 %77, 1
  store i32 %inc58, i32* %k, align 4, !tbaa !6
  store i32 %77, i32* %j36, align 4, !tbaa !6
  br label %for.cond37

for.end59:                                        ; preds = %for.cond.cleanup
  %78 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %arrayidx60 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 0
  %79 = load i32, i32* %arrayidx60, align 4, !tbaa !6
  %arrayidx61 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 1
  %80 = load i32, i32* %arrayidx61, align 4, !tbaa !6
  %arrayidx62 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 2
  %81 = load i32, i32* %arrayidx62, align 4, !tbaa !6
  call void @_ZN10btSoftBody10appendFaceEiiiPNS_8MaterialE(%class.btSoftBody* %78, i32 %79, i32 %80, i32 %81, %"struct.btSoftBody::Material"* null)
  %82 = bitcast [3 x i32]* %idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %82) #6
  br label %for.inc63

for.inc63:                                        ; preds = %for.end59
  %83 = load i32, i32* %i, align 4, !tbaa !6
  %add64 = add nsw i32 %83, 3
  store i32 %add64, i32* %i, align 4, !tbaa !6
  br label %for.cond27

for.end65:                                        ; preds = %for.cond27
  %84 = load i8, i8* %randomizeConstraints.addr, align 1, !tbaa !122, !range !22
  %tobool66 = trunc i8 %84 to i1
  br i1 %tobool66, label %if.then67, label %if.end68

if.then67:                                        ; preds = %for.end65
  %85 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  call void @_ZN10btSoftBody20randomizeConstraintsEv(%class.btSoftBody* %85)
  br label %if.end68

if.end68:                                         ; preds = %if.then67, %for.end65
  %86 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %87 = bitcast %class.btSoftBody** %psb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #6
  %call69 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.8* %vtx) #6
  %88 = bitcast %class.btAlignedObjectArray.8* %vtx to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %88) #6
  %call70 = call %class.btAlignedObjectArray.81* @_ZN20btAlignedObjectArrayIbED2Ev(%class.btAlignedObjectArray.81* %chks) #6
  %89 = bitcast %class.btAlignedObjectArray.81* %chks to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %89) #6
  %90 = bitcast i32* %ni to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #6
  %91 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #6
  %92 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #6
  %93 = bitcast i32* %maxidx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #6
  ret %class.btSoftBody* %86
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_Z5btMaxIiERKT_S2_S2_(i32* nonnull align 4 dereferenceable(4) %a, i32* nonnull align 4 dereferenceable(4) %b) #2 comdat {
entry:
  %a.addr = alloca i32*, align 4
  %b.addr = alloca i32*, align 4
  store i32* %a, i32** %a.addr, align 4, !tbaa !2
  store i32* %b, i32** %b.addr, align 4, !tbaa !2
  %0 = load i32*, i32** %a.addr, align 4, !tbaa !2
  %1 = load i32, i32* %0, align 4, !tbaa !6
  %2 = load i32*, i32** %b.addr, align 4, !tbaa !2
  %3 = load i32, i32* %2, align 4, !tbaa !6
  %cmp = icmp sgt i32 %1, %3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load i32*, i32** %a.addr, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %entry
  %5 = load i32*, i32** %b.addr, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %4, %cond.true ], [ %5, %cond.false ]
  ret i32* %cond-lvalue
}

define linkonce_odr hidden %class.btAlignedObjectArray.81* @_ZN20btAlignedObjectArrayIbEC2Ev(%class.btAlignedObjectArray.81* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.81*, align 4
  store %class.btAlignedObjectArray.81* %this, %class.btAlignedObjectArray.81** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.81*, %class.btAlignedObjectArray.81** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.82* @_ZN18btAlignedAllocatorIbLj16EEC2Ev(%class.btAlignedAllocator.82* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIbE4initEv(%class.btAlignedObjectArray.81* %this1)
  ret %class.btAlignedObjectArray.81* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIbE6resizeEiRKb(%class.btAlignedObjectArray.81* %this, i32 %newsize, i8* nonnull align 1 dereferenceable(1) %fillData) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.81*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca i8*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.81* %this, %class.btAlignedObjectArray.81** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !6
  store i8* %fillData, i8** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.81*, %class.btAlignedObjectArray.81** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %call = call i32 @_ZNK20btAlignedObjectArrayIbE4sizeEv(%class.btAlignedObjectArray.81* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !6
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %2 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  store i32 %4, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 4
  %8 = load i8*, i8** %m_data, align 4, !tbaa !134
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8, i8* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIbE4sizeEv(%class.btAlignedObjectArray.81* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayIbE7reserveEi(%class.btAlignedObjectArray.81* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  %14 = load i32, i32* %curSize, align 4, !tbaa !6
  store i32 %14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !6
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 4
  %18 = load i8*, i8** %m_data11, align 4, !tbaa !134
  %19 = load i32, i32* %i6, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds i8, i8* %18, i32 %19
  %20 = load i8*, i8** %fillData.addr, align 4, !tbaa !2
  %21 = load i8, i8* %20, align 1, !tbaa !122, !range !22
  %tobool = trunc i8 %21 to i1
  %frombool = zext i1 %tobool to i8
  store i8 %frombool, i8* %arrayidx12, align 1, !tbaa !122
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %22 = load i32, i32* %i6, align 4, !tbaa !6
  %inc14 = add nsw i32 %22, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %23 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 2
  store i32 %23, i32* %m_size, align 4, !tbaa !135
  %24 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 1 dereferenceable(1) i8* @_ZN20btAlignedObjectArrayIbEixEi(%class.btAlignedObjectArray.81* %this, i32 %n) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.81*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.81* %this, %class.btAlignedObjectArray.81** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.81*, %class.btAlignedObjectArray.81** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 4
  %0 = load i8*, i8** %m_data, align 4, !tbaa !134
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8, i8* %0, i32 %1
  ret i8* %arrayidx
}

declare void @_ZN10btSoftBody20randomizeConstraintsEv(%class.btSoftBody*) #3

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.81* @_ZN20btAlignedObjectArrayIbED2Ev(%class.btAlignedObjectArray.81* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.81*, align 4
  store %class.btAlignedObjectArray.81* %this, %class.btAlignedObjectArray.81** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.81*, %class.btAlignedObjectArray.81** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIbE5clearEv(%class.btAlignedObjectArray.81* %this1)
  ret %class.btAlignedObjectArray.81* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.HullDesc* @_ZN8HullDescC2E8HullFlagjPK9btVector3j(%class.HullDesc* returned %this, i32 %flag, i32 %vcount, %class.btVector3* %vertices, i32 %stride) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.HullDesc*, align 4
  %flag.addr = alloca i32, align 4
  %vcount.addr = alloca i32, align 4
  %vertices.addr = alloca %class.btVector3*, align 4
  %stride.addr = alloca i32, align 4
  store %class.HullDesc* %this, %class.HullDesc** %this.addr, align 4, !tbaa !2
  store i32 %flag, i32* %flag.addr, align 4, !tbaa !136
  store i32 %vcount, i32* %vcount.addr, align 4, !tbaa !6
  store %class.btVector3* %vertices, %class.btVector3** %vertices.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  %this1 = load %class.HullDesc*, %class.HullDesc** %this.addr, align 4
  %0 = load i32, i32* %flag.addr, align 4, !tbaa !136
  %mFlags = getelementptr inbounds %class.HullDesc, %class.HullDesc* %this1, i32 0, i32 0
  store i32 %0, i32* %mFlags, align 4, !tbaa !138
  %1 = load i32, i32* %vcount.addr, align 4, !tbaa !6
  %mVcount = getelementptr inbounds %class.HullDesc, %class.HullDesc* %this1, i32 0, i32 1
  store i32 %1, i32* %mVcount, align 4, !tbaa !139
  %2 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4, !tbaa !2
  %mVertices = getelementptr inbounds %class.HullDesc, %class.HullDesc* %this1, i32 0, i32 2
  store %class.btVector3* %2, %class.btVector3** %mVertices, align 4, !tbaa !140
  %3 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mVertexStride = getelementptr inbounds %class.HullDesc, %class.HullDesc* %this1, i32 0, i32 3
  store i32 %3, i32* %mVertexStride, align 4, !tbaa !141
  %mNormalEpsilon = getelementptr inbounds %class.HullDesc, %class.HullDesc* %this1, i32 0, i32 4
  store float 0x3F50624DE0000000, float* %mNormalEpsilon, align 4, !tbaa !142
  %mMaxVertices = getelementptr inbounds %class.HullDesc, %class.HullDesc* %this1, i32 0, i32 5
  store i32 4096, i32* %mMaxVertices, align 4, !tbaa !127
  ret %class.HullDesc* %this1
}

define linkonce_odr hidden %class.HullResult* @_ZN10HullResultC2Ev(%class.HullResult* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.HullResult*, align 4
  store %class.HullResult* %this, %class.HullResult** %this.addr, align 4, !tbaa !2
  %this1 = load %class.HullResult*, %class.HullResult** %this.addr, align 4
  %m_OutputVertices = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 2
  %call = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.8* %m_OutputVertices)
  %m_Indices = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 5
  %call2 = call %class.btAlignedObjectArray.93* @_ZN20btAlignedObjectArrayIjEC2Ev(%class.btAlignedObjectArray.93* %m_Indices)
  %mPolygons = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 0
  store i8 1, i8* %mPolygons, align 4, !tbaa !143
  %mNumOutputVertices = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 1
  store i32 0, i32* %mNumOutputVertices, align 4, !tbaa !129
  %mNumFaces = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 3
  store i32 0, i32* %mNumFaces, align 4, !tbaa !133
  %mNumIndices = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 4
  store i32 0, i32* %mNumIndices, align 4, !tbaa !144
  ret %class.HullResult* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.HullLibrary* @_ZN11HullLibraryC2Ev(%class.HullLibrary* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.HullLibrary*, align 4
  store %class.HullLibrary* %this, %class.HullLibrary** %this.addr, align 4, !tbaa !2
  %this1 = load %class.HullLibrary*, %class.HullLibrary** %this.addr, align 4
  %m_tris = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray.97* @_ZN20btAlignedObjectArrayIP14btHullTriangleEC2Ev(%class.btAlignedObjectArray.97* %m_tris)
  %m_vertexIndexMapping = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.85* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.85* %m_vertexIndexMapping)
  ret %class.HullLibrary* %this1
}

declare i32 @_ZN11HullLibrary16CreateConvexHullERK8HullDescR10HullResult(%class.HullLibrary*, %class.HullDesc* nonnull align 4 dereferenceable(28), %class.HullResult* nonnull align 4 dereferenceable(56)) #3

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIjEixEi(%class.btAlignedObjectArray.93* %this, i32 %n) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.93*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.93* %this, %class.btAlignedObjectArray.93** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.93*, %class.btAlignedObjectArray.93** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !145
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

declare i32 @_ZN11HullLibrary13ReleaseResultER10HullResult(%class.HullLibrary*, %class.HullResult* nonnull align 4 dereferenceable(56)) #3

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.HullLibrary* @_ZN11HullLibraryD2Ev(%class.HullLibrary* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.HullLibrary*, align 4
  store %class.HullLibrary* %this, %class.HullLibrary** %this.addr, align 4, !tbaa !2
  %this1 = load %class.HullLibrary*, %class.HullLibrary** %this.addr, align 4
  %m_vertexIndexMapping = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 1
  %call = call %class.btAlignedObjectArray.85* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.85* %m_vertexIndexMapping) #6
  %m_tris = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %call2 = call %class.btAlignedObjectArray.97* @_ZN20btAlignedObjectArrayIP14btHullTriangleED2Ev(%class.btAlignedObjectArray.97* %m_tris) #6
  ret %class.HullLibrary* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.HullResult* @_ZN10HullResultD2Ev(%class.HullResult* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.HullResult*, align 4
  store %class.HullResult* %this, %class.HullResult** %this.addr, align 4, !tbaa !2
  %this1 = load %class.HullResult*, %class.HullResult** %this.addr, align 4
  %m_Indices = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 5
  %call = call %class.btAlignedObjectArray.93* @_ZN20btAlignedObjectArrayIjED2Ev(%class.btAlignedObjectArray.93* %m_Indices) #6
  %m_OutputVertices = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 2
  %call2 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.8* %m_OutputVertices) #6
  ret %class.HullResult* %this1
}

define hidden %class.btSoftBody* @_ZN17btSoftBodyHelpers20CreateFromTetGenDataER19btSoftBodyWorldInfoPKcS3_S3_bbb(%struct.btSoftBodyWorldInfo* nonnull align 4 dereferenceable(100) %worldInfo, i8* %ele, i8* %face, i8* %node, i1 zeroext %bfacelinks, i1 zeroext %btetralinks, i1 zeroext %bfacesfromtetras) #0 {
entry:
  %worldInfo.addr = alloca %struct.btSoftBodyWorldInfo*, align 4
  %ele.addr = alloca i8*, align 4
  %face.addr = alloca i8*, align 4
  %node.addr = alloca i8*, align 4
  %bfacelinks.addr = alloca i8, align 1
  %btetralinks.addr = alloca i8, align 1
  %bfacesfromtetras.addr = alloca i8, align 1
  %pos = alloca %class.btAlignedObjectArray.8, align 4
  %nnode = alloca i32, align 4
  %ndims = alloca i32, align 4
  %nattrb = alloca i32, align 4
  %hasbounds = alloca i32, align 4
  %result = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  %index = alloca i32, align 4
  %x = alloca float, align 4
  %y = alloca float, align 4
  %z = alloca float, align 4
  %psb = alloca %class.btSoftBody*, align 4
  %ntetra = alloca i32, align 4
  %ncorner = alloca i32, align 4
  %neattrb = alloca i32, align 4
  %i21 = alloca i32, align 4
  %index26 = alloca i32, align 4
  %ni = alloca [4 x i32], align 16
  store %struct.btSoftBodyWorldInfo* %worldInfo, %struct.btSoftBodyWorldInfo** %worldInfo.addr, align 4, !tbaa !2
  store i8* %ele, i8** %ele.addr, align 4, !tbaa !2
  store i8* %face, i8** %face.addr, align 4, !tbaa !2
  store i8* %node, i8** %node.addr, align 4, !tbaa !2
  %frombool = zext i1 %bfacelinks to i8
  store i8 %frombool, i8* %bfacelinks.addr, align 1, !tbaa !122
  %frombool1 = zext i1 %btetralinks to i8
  store i8 %frombool1, i8* %btetralinks.addr, align 1, !tbaa !122
  %frombool2 = zext i1 %bfacesfromtetras to i8
  store i8 %frombool2, i8* %bfacesfromtetras.addr, align 1, !tbaa !122
  %0 = bitcast %class.btAlignedObjectArray.8* %pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %0) #6
  %call = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.8* %pos)
  %1 = bitcast i32* %nnode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store i32 0, i32* %nnode, align 4, !tbaa !6
  %2 = bitcast i32* %ndims to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  store i32 0, i32* %ndims, align 4, !tbaa !6
  %3 = bitcast i32* %nattrb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  store i32 0, i32* %nattrb, align 4, !tbaa !6
  %4 = bitcast i32* %hasbounds to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  store i32 0, i32* %hasbounds, align 4, !tbaa !6
  %5 = bitcast i32* %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load i8*, i8** %node.addr, align 4, !tbaa !2
  %call3 = call i32 (i8*, i8*, ...) @sscanf(i8* %6, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.2, i32 0, i32 0), i32* %nnode, i32* %ndims, i32* %nattrb, i32* %hasbounds)
  store i32 %call3, i32* %result, align 4, !tbaa !6
  %7 = load i8*, i8** %node.addr, align 4, !tbaa !2
  %call4 = call i32 (i8*, i8*, ...) @sscanf(i8* %7, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.2, i32 0, i32 0), i32* %nnode, i32* %ndims, i32* %nattrb, i32* %hasbounds)
  store i32 %call4, i32* %result, align 4, !tbaa !6
  %8 = load i8*, i8** %node.addr, align 4, !tbaa !2
  %call5 = call i32 @_ZL8nextLinePKc(i8* %8)
  %9 = load i8*, i8** %node.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %9, i32 %call5
  store i8* %add.ptr, i8** %node.addr, align 4, !tbaa !2
  %10 = load i32, i32* %nnode, align 4, !tbaa !6
  %11 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #6
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.8* %pos, i32 %10, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %12 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %12) #6
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %14 = load i32, i32* %i, align 4, !tbaa !6
  %call7 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %pos)
  %cmp = icmp slt i32 %14, %call7
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %15 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %16 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  store i32 0, i32* %index, align 4, !tbaa !6
  %17 = bitcast float* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #6
  %18 = bitcast float* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #6
  %19 = bitcast float* %z to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #6
  %20 = load i8*, i8** %node.addr, align 4, !tbaa !2
  %call8 = call i32 (i8*, i8*, ...) @sscanf(i8* %20, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.3, i32 0, i32 0), i32* %index, float* %x, float* %y, float* %z)
  %21 = load i8*, i8** %node.addr, align 4, !tbaa !2
  %call9 = call i32 @_ZL8nextLinePKc(i8* %21)
  %22 = load i8*, i8** %node.addr, align 4, !tbaa !2
  %add.ptr10 = getelementptr inbounds i8, i8* %22, i32 %call9
  store i8* %add.ptr10, i8** %node.addr, align 4, !tbaa !2
  %23 = load i32, i32* %index, align 4, !tbaa !6
  %call11 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %pos, i32 %23)
  %24 = load float, float* %x, align 4, !tbaa !8
  call void @_ZN9btVector34setXEf(%class.btVector3* %call11, float %24)
  %25 = load i32, i32* %index, align 4, !tbaa !6
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %pos, i32 %25)
  %26 = load float, float* %y, align 4, !tbaa !8
  call void @_ZN9btVector34setYEf(%class.btVector3* %call12, float %26)
  %27 = load i32, i32* %index, align 4, !tbaa !6
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %pos, i32 %27)
  %28 = load float, float* %z, align 4, !tbaa !8
  call void @_ZN9btVector34setZEf(%class.btVector3* %call13, float %28)
  %29 = bitcast float* %z to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #6
  %30 = bitcast float* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #6
  %31 = bitcast float* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #6
  %32 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %33 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %33, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %34 = bitcast %class.btSoftBody** %psb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #6
  %call14 = call i8* @_ZN17btCollisionObjectnwEm(i32 1252)
  %35 = bitcast i8* %call14 to %class.btSoftBody*
  %36 = load %struct.btSoftBodyWorldInfo*, %struct.btSoftBodyWorldInfo** %worldInfo.addr, align 4, !tbaa !2
  %37 = load i32, i32* %nnode, align 4, !tbaa !6
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %pos, i32 0)
  %call16 = call %class.btSoftBody* @_ZN10btSoftBodyC1EP19btSoftBodyWorldInfoiPK9btVector3PKf(%class.btSoftBody* %35, %struct.btSoftBodyWorldInfo* %36, i32 %37, %class.btVector3* %call15, float* null)
  store %class.btSoftBody* %35, %class.btSoftBody** %psb, align 4, !tbaa !2
  %38 = load i8*, i8** %ele.addr, align 4, !tbaa !2
  %tobool = icmp ne i8* %38, null
  br i1 %tobool, label %land.lhs.true, label %if.end55

land.lhs.true:                                    ; preds = %for.end
  %39 = load i8*, i8** %ele.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %39, i32 0
  %40 = load i8, i8* %arrayidx, align 1, !tbaa !24
  %tobool17 = icmp ne i8 %40, 0
  br i1 %tobool17, label %if.then, label %if.end55

if.then:                                          ; preds = %land.lhs.true
  %41 = bitcast i32* %ntetra to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #6
  store i32 0, i32* %ntetra, align 4, !tbaa !6
  %42 = bitcast i32* %ncorner to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #6
  store i32 0, i32* %ncorner, align 4, !tbaa !6
  %43 = bitcast i32* %neattrb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #6
  store i32 0, i32* %neattrb, align 4, !tbaa !6
  %44 = load i8*, i8** %ele.addr, align 4, !tbaa !2
  %call18 = call i32 (i8*, i8*, ...) @sscanf(i8* %44, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.4, i32 0, i32 0), i32* %ntetra, i32* %ncorner, i32* %neattrb)
  %45 = load i8*, i8** %ele.addr, align 4, !tbaa !2
  %call19 = call i32 @_ZL8nextLinePKc(i8* %45)
  %46 = load i8*, i8** %ele.addr, align 4, !tbaa !2
  %add.ptr20 = getelementptr inbounds i8, i8* %46, i32 %call19
  store i8* %add.ptr20, i8** %ele.addr, align 4, !tbaa !2
  %47 = bitcast i32* %i21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #6
  store i32 0, i32* %i21, align 4, !tbaa !6
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc52, %if.then
  %48 = load i32, i32* %i21, align 4, !tbaa !6
  %49 = load i32, i32* %ntetra, align 4, !tbaa !6
  %cmp23 = icmp slt i32 %48, %49
  br i1 %cmp23, label %for.body25, label %for.cond.cleanup24

for.cond.cleanup24:                               ; preds = %for.cond22
  %50 = bitcast i32* %i21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #6
  br label %for.end54

for.body25:                                       ; preds = %for.cond22
  %51 = bitcast i32* %index26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #6
  store i32 0, i32* %index26, align 4, !tbaa !6
  %52 = bitcast [4 x i32]* %ni to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %52) #6
  %53 = load i8*, i8** %ele.addr, align 4, !tbaa !2
  %arrayidx27 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 1
  %arrayidx29 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 2
  %arrayidx30 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 3
  %call31 = call i32 (i8*, i8*, ...) @sscanf(i8* %53, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.5, i32 0, i32 0), i32* %index26, i32* %arrayidx27, i32* %arrayidx28, i32* %arrayidx29, i32* %arrayidx30)
  %54 = load i8*, i8** %ele.addr, align 4, !tbaa !2
  %call32 = call i32 @_ZL8nextLinePKc(i8* %54)
  %55 = load i8*, i8** %ele.addr, align 4, !tbaa !2
  %add.ptr33 = getelementptr inbounds i8, i8* %55, i32 %call32
  store i8* %add.ptr33, i8** %ele.addr, align 4, !tbaa !2
  %56 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %arrayidx34 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 0
  %57 = load i32, i32* %arrayidx34, align 16, !tbaa !6
  %arrayidx35 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 1
  %58 = load i32, i32* %arrayidx35, align 4, !tbaa !6
  %arrayidx36 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 2
  %59 = load i32, i32* %arrayidx36, align 8, !tbaa !6
  %arrayidx37 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 3
  %60 = load i32, i32* %arrayidx37, align 4, !tbaa !6
  call void @_ZN10btSoftBody11appendTetraEiiiiPNS_8MaterialE(%class.btSoftBody* %56, i32 %57, i32 %58, i32 %59, i32 %60, %"struct.btSoftBody::Material"* null)
  %61 = load i8, i8* %btetralinks.addr, align 1, !tbaa !122, !range !22
  %tobool38 = trunc i8 %61 to i1
  br i1 %tobool38, label %if.then39, label %if.end

if.then39:                                        ; preds = %for.body25
  %62 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %arrayidx40 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 0
  %63 = load i32, i32* %arrayidx40, align 16, !tbaa !6
  %arrayidx41 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 1
  %64 = load i32, i32* %arrayidx41, align 4, !tbaa !6
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %62, i32 %63, i32 %64, %"struct.btSoftBody::Material"* null, i1 zeroext true)
  %65 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %arrayidx42 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 1
  %66 = load i32, i32* %arrayidx42, align 4, !tbaa !6
  %arrayidx43 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 2
  %67 = load i32, i32* %arrayidx43, align 8, !tbaa !6
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %65, i32 %66, i32 %67, %"struct.btSoftBody::Material"* null, i1 zeroext true)
  %68 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %arrayidx44 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 2
  %69 = load i32, i32* %arrayidx44, align 8, !tbaa !6
  %arrayidx45 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 0
  %70 = load i32, i32* %arrayidx45, align 16, !tbaa !6
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %68, i32 %69, i32 %70, %"struct.btSoftBody::Material"* null, i1 zeroext true)
  %71 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %arrayidx46 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 0
  %72 = load i32, i32* %arrayidx46, align 16, !tbaa !6
  %arrayidx47 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 3
  %73 = load i32, i32* %arrayidx47, align 4, !tbaa !6
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %71, i32 %72, i32 %73, %"struct.btSoftBody::Material"* null, i1 zeroext true)
  %74 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %arrayidx48 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 1
  %75 = load i32, i32* %arrayidx48, align 4, !tbaa !6
  %arrayidx49 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 3
  %76 = load i32, i32* %arrayidx49, align 4, !tbaa !6
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %74, i32 %75, i32 %76, %"struct.btSoftBody::Material"* null, i1 zeroext true)
  %77 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %arrayidx50 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 2
  %78 = load i32, i32* %arrayidx50, align 8, !tbaa !6
  %arrayidx51 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 3
  %79 = load i32, i32* %arrayidx51, align 4, !tbaa !6
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %77, i32 %78, i32 %79, %"struct.btSoftBody::Material"* null, i1 zeroext true)
  br label %if.end

if.end:                                           ; preds = %if.then39, %for.body25
  %80 = bitcast [4 x i32]* %ni to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %80) #6
  %81 = bitcast i32* %index26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #6
  br label %for.inc52

for.inc52:                                        ; preds = %if.end
  %82 = load i32, i32* %i21, align 4, !tbaa !6
  %inc53 = add nsw i32 %82, 1
  store i32 %inc53, i32* %i21, align 4, !tbaa !6
  br label %for.cond22

for.end54:                                        ; preds = %for.cond.cleanup24
  %83 = bitcast i32* %neattrb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #6
  %84 = bitcast i32* %ncorner to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #6
  %85 = bitcast i32* %ntetra to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #6
  br label %if.end55

if.end55:                                         ; preds = %for.end54, %land.lhs.true, %for.end
  %86 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %m_nodes = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %86, i32 0, i32 9
  %call56 = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4NodeEE4sizeEv(%class.btAlignedObjectArray.25* %m_nodes)
  %call57 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.6, i32 0, i32 0), i32 %call56)
  %87 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %m_links = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %87, i32 0, i32 10
  %call58 = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4LinkEE4sizeEv(%class.btAlignedObjectArray.29* %m_links)
  %call59 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.7, i32 0, i32 0), i32 %call58)
  %88 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %m_faces = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %88, i32 0, i32 11
  %call60 = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4FaceEE4sizeEv(%class.btAlignedObjectArray.33* %m_faces)
  %call61 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.8, i32 0, i32 0), i32 %call60)
  %89 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %m_tetras = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %89, i32 0, i32 12
  %call62 = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody5TetraEE4sizeEv(%class.btAlignedObjectArray.37* %m_tetras)
  %call63 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.9, i32 0, i32 0), i32 %call62)
  %90 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %91 = bitcast %class.btSoftBody** %psb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #6
  %92 = bitcast i32* %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #6
  %93 = bitcast i32* %hasbounds to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #6
  %94 = bitcast i32* %nattrb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #6
  %95 = bitcast i32* %ndims to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #6
  %96 = bitcast i32* %nnode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #6
  %call64 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.8* %pos) #6
  %97 = bitcast %class.btAlignedObjectArray.8* %pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %97) #6
  ret %class.btSoftBody* %90
}

declare i32 @sscanf(i8*, i8*, ...) #3

; Function Attrs: nounwind
define internal i32 @_ZL8nextLinePKc(i8* %buffer) #5 {
entry:
  %buffer.addr = alloca i8*, align 4
  %numBytesRead = alloca i32, align 4
  store i8* %buffer, i8** %buffer.addr, align 4, !tbaa !2
  %0 = bitcast i32* %numBytesRead to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %numBytesRead, align 4, !tbaa !6
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i8*, i8** %buffer.addr, align 4, !tbaa !2
  %2 = load i8, i8* %1, align 1, !tbaa !24
  %conv = sext i8 %2 to i32
  %cmp = icmp ne i32 %conv, 10
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %3 = load i8*, i8** %buffer.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %3, i32 1
  store i8* %incdec.ptr, i8** %buffer.addr, align 4, !tbaa !2
  %4 = load i32, i32* %numBytesRead, align 4, !tbaa !6
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %numBytesRead, align 4, !tbaa !6
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %5 = load i8*, i8** %buffer.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %5, i32 0
  %6 = load i8, i8* %arrayidx, align 1, !tbaa !24
  %conv1 = sext i8 %6 to i32
  %cmp2 = icmp eq i32 %conv1, 10
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %while.end
  %7 = load i8*, i8** %buffer.addr, align 4, !tbaa !2
  %incdec.ptr3 = getelementptr inbounds i8, i8* %7, i32 1
  store i8* %incdec.ptr3, i8** %buffer.addr, align 4, !tbaa !2
  %8 = load i32, i32* %numBytesRead, align 4, !tbaa !6
  %inc4 = add nsw i32 %8, 1
  store i32 %inc4, i32* %numBytesRead, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end
  %9 = load i32, i32* %numBytesRead, align 4, !tbaa !6
  %10 = bitcast i32* %numBytesRead to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  ret i32 %9
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector34setXEf(%class.btVector3* %this, float %_x) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float %_x, float* %_x.addr, align 4, !tbaa !8
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float, float* %_x.addr, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %0, float* %arrayidx, align 4, !tbaa !8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector34setYEf(%class.btVector3* %this, float %_y) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_y.addr = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float %_y, float* %_y.addr, align 4, !tbaa !8
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float, float* %_y.addr, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  store float %0, float* %arrayidx, align 4, !tbaa !8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector34setZEf(%class.btVector3* %this, float %_z) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_z.addr = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float %_z, float* %_z.addr, align 4, !tbaa !8
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float, float* %_z.addr, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  store float %0, float* %arrayidx, align 4, !tbaa !8
  ret void
}

declare void @_ZN10btSoftBody11appendTetraEiiiiPNS_8MaterialE(%class.btSoftBody*, i32, i32, i32, i32, %"struct.btSoftBody::Material"*) #3

declare i32 @printf(i8*, ...) #3

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4, !tbaa !8
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1) #6
  ret %class.btVector3* %call2
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !8
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !8
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #6
  ret %class.btVector3* %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #2 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !8
  %0 = load float, float* %y.addr, align 4, !tbaa !8
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #8

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !8
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !8
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !8
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !8
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !8
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !8
  %6 = load float*, float** %s.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !8
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !8
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !8
  ret %class.btVector3* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.89* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEEC2Ev(%class.btAlignedObjectArray.89* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.90* @_ZN18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EEC2Ev(%class.btAlignedAllocator.90* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4initEv(%class.btAlignedObjectArray.89* %this1)
  ret %class.btAlignedObjectArray.89* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.85* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.85* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.85*, align 4
  store %class.btAlignedObjectArray.85* %this, %class.btAlignedObjectArray.85** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.85*, %class.btAlignedObjectArray.85** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.86* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.86* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.85* %this1)
  ret %class.btAlignedObjectArray.85* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.90* @_ZN18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EEC2Ev(%class.btAlignedAllocator.90* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.90*, align 4
  store %class.btAlignedAllocator.90* %this, %class.btAlignedAllocator.90** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.90*, %class.btAlignedAllocator.90** %this.addr, align 4
  ret %class.btAlignedAllocator.90* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4initEv(%class.btAlignedObjectArray.89* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !146
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 4
  store %"class.btConvexHullComputer::Edge"* null, %"class.btConvexHullComputer::Edge"** %m_data, align 4, !tbaa !56
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !147
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !148
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.86* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.86* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.86*, align 4
  store %class.btAlignedAllocator.86* %this, %class.btAlignedAllocator.86** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.86*, %class.btAlignedAllocator.86** %this.addr, align 4
  ret %class.btAlignedAllocator.86* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.85* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.85*, align 4
  store %class.btAlignedObjectArray.85* %this, %class.btAlignedObjectArray.85** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.85*, %class.btAlignedObjectArray.85** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !149
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 4
  store i32* null, i32** %m_data, align 4, !tbaa !55
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !52
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !150
  ret void
}

declare float @_ZN20btConvexHullComputer7computeEPKvbiiff(%class.btConvexHullComputer*, i8*, i1 zeroext, i32, i32, float, float) #3

; Function Attrs: nounwind
define linkonce_odr hidden %"class.btConvexHullComputer::Edge"* @_ZNK20btConvexHullComputer4Edge19getNextEdgeOfVertexEv(%"class.btConvexHullComputer::Edge"* %this) #5 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullComputer::Edge"*, align 4
  store %"class.btConvexHullComputer::Edge"* %this, %"class.btConvexHullComputer::Edge"** %this.addr, align 4, !tbaa !2
  %this1 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %this.addr, align 4
  %next = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %this1, i32 0, i32 0
  %0 = load i32, i32* %next, align 4, !tbaa !151
  %add.ptr = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %this1, i32 %0
  ret %"class.btConvexHullComputer::Edge"* %add.ptr
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.85* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.85* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.85*, align 4
  store %class.btAlignedObjectArray.85* %this, %class.btAlignedObjectArray.85** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.85*, %class.btAlignedObjectArray.85** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.85* %this1)
  ret %class.btAlignedObjectArray.85* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.89* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEED2Ev(%class.btAlignedObjectArray.89* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE5clearEv(%class.btAlignedObjectArray.89* %this1)
  ret %class.btAlignedObjectArray.89* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.85* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.85*, align 4
  store %class.btAlignedObjectArray.85* %this, %class.btAlignedObjectArray.85** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.85*, %class.btAlignedObjectArray.85** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.85* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.85* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.85* %this1)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.85* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.85* %this, i32 %first, i32 %last) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.85*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.85* %this, %class.btAlignedObjectArray.85** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.85*, %class.btAlignedObjectArray.85** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 4
  %4 = load i32*, i32** %m_data, align 4, !tbaa !55
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.85* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.85*, align 4
  store %class.btAlignedObjectArray.85* %this, %class.btAlignedObjectArray.85** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.85*, %class.btAlignedObjectArray.85** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !55
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !149, !range !22
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4, !tbaa !55
  call void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.86* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4, !tbaa !55
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.86* %this, i32* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.86*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.86* %this, %class.btAlignedAllocator.86** %this.addr, align 4, !tbaa !2
  store i32* %ptr, i32** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.86*, %class.btAlignedAllocator.86** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE5clearEv(%class.btAlignedObjectArray.89* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4sizeEv(%class.btAlignedObjectArray.89* %this1)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE7destroyEii(%class.btAlignedObjectArray.89* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE10deallocateEv(%class.btAlignedObjectArray.89* %this1)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4initEv(%class.btAlignedObjectArray.89* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE7destroyEii(%class.btAlignedObjectArray.89* %this, i32 %first, i32 %last) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 4
  %4 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %m_data, align 4, !tbaa !56
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4sizeEv(%class.btAlignedObjectArray.89* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !147
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE10deallocateEv(%class.btAlignedObjectArray.89* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 4
  %0 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %m_data, align 4, !tbaa !56
  %tobool = icmp ne %"class.btConvexHullComputer::Edge"* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !146, !range !22
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 4
  %2 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %m_data4, align 4, !tbaa !56
  call void @_ZN18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EE10deallocateEPS1_(%class.btAlignedAllocator.90* %m_allocator, %"class.btConvexHullComputer::Edge"* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 4
  store %"class.btConvexHullComputer::Edge"* null, %"class.btConvexHullComputer::Edge"** %m_data5, align 4, !tbaa !56
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EE10deallocateEPS1_(%class.btAlignedAllocator.90* %this, %"class.btConvexHullComputer::Edge"* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.90*, align 4
  %ptr.addr = alloca %"class.btConvexHullComputer::Edge"*, align 4
  store %class.btAlignedAllocator.90* %this, %class.btAlignedAllocator.90** %this.addr, align 4, !tbaa !2
  store %"class.btConvexHullComputer::Edge"* %ptr, %"class.btConvexHullComputer::Edge"** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.90*, %class.btAlignedAllocator.90** %this.addr, align 4
  %0 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %"class.btConvexHullComputer::Edge"* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %1 = load float, float* %arrayidx, align 4, !tbaa !8
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %3 = load float, float* %arrayidx3, align 4, !tbaa !8
  %mul = fmul float %1, %3
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %4 = load float, float* %arrayidx5, align 4, !tbaa !8
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %6 = load float, float* %arrayidx7, align 4, !tbaa !8
  %mul8 = fmul float %4, %6
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4, !tbaa !8
  %7 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %8 = load float, float* %arrayidx11, align 4, !tbaa !8
  %9 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %10 = load float, float* %arrayidx13, align 4, !tbaa !8
  %mul14 = fmul float %8, %10
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %11 = load float, float* %arrayidx16, align 4, !tbaa !8
  %12 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %13 = load float, float* %arrayidx18, align 4, !tbaa !8
  %mul19 = fmul float %11, %13
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4, !tbaa !8
  %14 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %15 = load float, float* %arrayidx23, align 4, !tbaa !8
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %17 = load float, float* %arrayidx25, align 4, !tbaa !8
  %mul26 = fmul float %15, %17
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %18 = load float, float* %arrayidx28, align 4, !tbaa !8
  %19 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %19, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %20 = load float, float* %arrayidx30, align 4, !tbaa !8
  %mul31 = fmul float %18, %20
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %21 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #6
  %22 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #6
  %23 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #6
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #6
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4, !tbaa !8
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !8
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !8
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #6
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransform11getIdentityEv() #0 comdat {
entry:
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %0 = load atomic i8, i8* bitcast (i32* @_ZGVZN11btTransform11getIdentityEvE17identityTransform to i8*) acquire, align 4
  %1 = and i8 %0, 1
  %guard.uninitialized = icmp eq i8 %1, 0
  br i1 %guard.uninitialized, label %init.check, label %init.end, !prof !31

init.check:                                       ; preds = %entry
  %2 = call i32 @__cxa_guard_acquire(i32* @_ZGVZN11btTransform11getIdentityEvE17identityTransform) #6
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %init, label %init.end

init:                                             ; preds = %init.check
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x311getIdentityEv()
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #6
  %4 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  store float 0.000000e+00, float* %ref.tmp1, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !8
  %6 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !8
  %call4 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %call5 = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* @_ZZN11btTransform11getIdentityEvE17identityTransform, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %7 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  %8 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #6
  %9 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #6
  %10 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %10) #6
  %11 = call {}* @llvm.invariant.start.p0i8(i64 64, i8* bitcast (%class.btTransform* @_ZZN11btTransform11getIdentityEvE17identityTransform to i8*))
  call void @__cxa_guard_release(i32* @_ZGVZN11btTransform11getIdentityEvE17identityTransform) #6
  br label %init.end

init.end:                                         ; preds = %init, %init.check, %entry
  ret %class.btTransform* @_ZZN11btTransform11getIdentityEvE17identityTransform
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !23
  ret %class.btTransform* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x311getIdentityEv() #0 comdat {
entry:
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %0 = load atomic i8, i8* bitcast (i32* @_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix to i8*) acquire, align 4
  %1 = and i8 %0, 1
  %guard.uninitialized = icmp eq i8 %1, 0
  br i1 %guard.uninitialized, label %init.check, label %init.end, !prof !31

init.check:                                       ; preds = %entry
  %2 = call i32 @__cxa_guard_acquire(i32* @_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix) #6
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %init, label %init.end

init:                                             ; preds = %init.check
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !8
  %4 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  store float 0.000000e+00, float* %ref.tmp1, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !8
  %6 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !8
  %7 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  store float 1.000000e+00, float* %ref.tmp4, align 4, !tbaa !8
  %8 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  store float 0.000000e+00, float* %ref.tmp5, align 4, !tbaa !8
  %9 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !8
  %11 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #6
  store float 1.000000e+00, float* %ref.tmp8, align 4, !tbaa !8
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* @_ZZN11btMatrix3x311getIdentityEvE14identityMatrix, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  %12 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #6
  %13 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #6
  %14 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #6
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  %18 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #6
  %19 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #6
  %20 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #6
  %21 = call {}* @llvm.invariant.start.p0i8(i64 48, i8* bitcast (%class.btMatrix3x3* @_ZZN11btMatrix3x311getIdentityEvE14identityMatrix to i8*))
  call void @__cxa_guard_release(i32* @_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix) #6
  br label %init.end

init.end:                                         ; preds = %init, %init.check, %entry
  ret %class.btMatrix3x3* @_ZZN11btMatrix3x311getIdentityEvE14identityMatrix
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %b.addr = alloca %class.btMatrix3x3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %b, %class.btMatrix3x3** %b.addr, align 4, !tbaa !2
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %b.addr, align 4, !tbaa !2
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !23
  ret %class.btTransform* %this1
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !8
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !8
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !8
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #4 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !23
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !23
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !23
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %this) #4 comdat {
entry:
  %this.addr = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvtNode* %this, %struct.btDbvtNode** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %this.addr, align 4
  %call = call zeroext i1 @_ZNK10btDbvtNode6isleafEv(%struct.btDbvtNode* %this1)
  %lnot = xor i1 %call, true
  ret i1 %lnot
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK12btDbvtAabbMm6CenterEv(%class.btVector3* noalias sret align 4 %agg.result, %struct.btDbvtAabbMm* %this) #4 comdat {
entry:
  %this.addr = alloca %struct.btDbvtAabbMm*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  store %struct.btDbvtAabbMm* %this, %struct.btDbvtAabbMm** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #6
  %mi = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 0
  %mx = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %mi, %class.btVector3* nonnull align 4 dereferenceable(16) %mx)
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store float 2.000000e+00, float* %ref.tmp2, align 4, !tbaa !8
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #6
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %3) #6
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK12btDbvtAabbMm7ExtentsEv(%class.btVector3* noalias sret align 4 %agg.result, %struct.btDbvtAabbMm* %this) #4 comdat {
entry:
  %this.addr = alloca %struct.btDbvtAabbMm*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  store %struct.btDbvtAabbMm* %this, %struct.btDbvtAabbMm** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #6
  %mx = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 1
  %mi = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %mx, %class.btVector3* nonnull align 4 dereferenceable(16) %mi)
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store float 2.000000e+00, float* %ref.tmp2, align 4, !tbaa !8
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #6
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %3) #6
  ret void
}

define internal void @_ZL7drawBoxP12btIDebugDrawRK9btVector3S3_S3_(%class.btIDebugDraw* %idraw, %class.btVector3* nonnull align 4 dereferenceable(16) %mins, %class.btVector3* nonnull align 4 dereferenceable(16) %maxs, %class.btVector3* nonnull align 4 dereferenceable(16) %color) #0 {
entry:
  %idraw.addr = alloca %class.btIDebugDraw*, align 4
  %mins.addr = alloca %class.btVector3*, align 4
  %maxs.addr = alloca %class.btVector3*, align 4
  %color.addr = alloca %class.btVector3*, align 4
  %c = alloca [8 x %class.btVector3], align 16
  store %class.btIDebugDraw* %idraw, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  store %class.btVector3* %mins, %class.btVector3** %mins.addr, align 4, !tbaa !2
  store %class.btVector3* %maxs, %class.btVector3** %maxs.addr, align 4, !tbaa !2
  store %class.btVector3* %color, %class.btVector3** %color.addr, align 4, !tbaa !2
  %0 = bitcast [8 x %class.btVector3]* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 128, i8* %0) #6
  %arrayinit.begin = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 0
  %1 = load %class.btVector3*, %class.btVector3** %mins.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load %class.btVector3*, %class.btVector3** %mins.addr, align 4, !tbaa !2
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %2)
  %3 = load %class.btVector3*, %class.btVector3** %mins.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %3)
  %call3 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.begin, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call1, float* nonnull align 4 dereferenceable(4) %call2)
  %arrayinit.element = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.begin, i32 1
  %4 = load %class.btVector3*, %class.btVector3** %maxs.addr, align 4, !tbaa !2
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %4)
  %5 = load %class.btVector3*, %class.btVector3** %mins.addr, align 4, !tbaa !2
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %5)
  %6 = load %class.btVector3*, %class.btVector3** %mins.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %6)
  %call7 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call5, float* nonnull align 4 dereferenceable(4) %call6)
  %arrayinit.element8 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element, i32 1
  %7 = load %class.btVector3*, %class.btVector3** %maxs.addr, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %7)
  %8 = load %class.btVector3*, %class.btVector3** %maxs.addr, align 4, !tbaa !2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %8)
  %9 = load %class.btVector3*, %class.btVector3** %mins.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %9)
  %call12 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element8, float* nonnull align 4 dereferenceable(4) %call9, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call11)
  %arrayinit.element13 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element8, i32 1
  %10 = load %class.btVector3*, %class.btVector3** %mins.addr, align 4, !tbaa !2
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %10)
  %11 = load %class.btVector3*, %class.btVector3** %maxs.addr, align 4, !tbaa !2
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %11)
  %12 = load %class.btVector3*, %class.btVector3** %mins.addr, align 4, !tbaa !2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %12)
  %call17 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element13, float* nonnull align 4 dereferenceable(4) %call14, float* nonnull align 4 dereferenceable(4) %call15, float* nonnull align 4 dereferenceable(4) %call16)
  %arrayinit.element18 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element13, i32 1
  %13 = load %class.btVector3*, %class.btVector3** %mins.addr, align 4, !tbaa !2
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %13)
  %14 = load %class.btVector3*, %class.btVector3** %mins.addr, align 4, !tbaa !2
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %14)
  %15 = load %class.btVector3*, %class.btVector3** %maxs.addr, align 4, !tbaa !2
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %15)
  %call22 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element18, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call20, float* nonnull align 4 dereferenceable(4) %call21)
  %arrayinit.element23 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element18, i32 1
  %16 = load %class.btVector3*, %class.btVector3** %maxs.addr, align 4, !tbaa !2
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %16)
  %17 = load %class.btVector3*, %class.btVector3** %mins.addr, align 4, !tbaa !2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %17)
  %18 = load %class.btVector3*, %class.btVector3** %maxs.addr, align 4, !tbaa !2
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %18)
  %call27 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element23, float* nonnull align 4 dereferenceable(4) %call24, float* nonnull align 4 dereferenceable(4) %call25, float* nonnull align 4 dereferenceable(4) %call26)
  %arrayinit.element28 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element23, i32 1
  %19 = load %class.btVector3*, %class.btVector3** %maxs.addr, align 4, !tbaa !2
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %19)
  %20 = load %class.btVector3*, %class.btVector3** %maxs.addr, align 4, !tbaa !2
  %call30 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %20)
  %21 = load %class.btVector3*, %class.btVector3** %maxs.addr, align 4, !tbaa !2
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %21)
  %call32 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element28, float* nonnull align 4 dereferenceable(4) %call29, float* nonnull align 4 dereferenceable(4) %call30, float* nonnull align 4 dereferenceable(4) %call31)
  %arrayinit.element33 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element28, i32 1
  %22 = load %class.btVector3*, %class.btVector3** %mins.addr, align 4, !tbaa !2
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %22)
  %23 = load %class.btVector3*, %class.btVector3** %maxs.addr, align 4, !tbaa !2
  %call35 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %23)
  %24 = load %class.btVector3*, %class.btVector3** %maxs.addr, align 4, !tbaa !2
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %24)
  %call37 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element33, float* nonnull align 4 dereferenceable(4) %call34, float* nonnull align 4 dereferenceable(4) %call35, float* nonnull align 4 dereferenceable(4) %call36)
  %25 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 0
  %arrayidx38 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 1
  %26 = load %class.btVector3*, %class.btVector3** %color.addr, align 4, !tbaa !2
  %27 = bitcast %class.btIDebugDraw* %25 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %27, align 4, !tbaa !25
  %vfn = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %28 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %28(%class.btIDebugDraw* %25, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx38, %class.btVector3* nonnull align 4 dereferenceable(16) %26)
  %29 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %arrayidx39 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 1
  %arrayidx40 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 2
  %30 = load %class.btVector3*, %class.btVector3** %color.addr, align 4, !tbaa !2
  %31 = bitcast %class.btIDebugDraw* %29 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable41 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %31, align 4, !tbaa !25
  %vfn42 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable41, i64 2
  %32 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn42, align 4
  call void %32(%class.btIDebugDraw* %29, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx39, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx40, %class.btVector3* nonnull align 4 dereferenceable(16) %30)
  %33 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %arrayidx43 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 2
  %arrayidx44 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 3
  %34 = load %class.btVector3*, %class.btVector3** %color.addr, align 4, !tbaa !2
  %35 = bitcast %class.btIDebugDraw* %33 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable45 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %35, align 4, !tbaa !25
  %vfn46 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable45, i64 2
  %36 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn46, align 4
  call void %36(%class.btIDebugDraw* %33, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx43, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx44, %class.btVector3* nonnull align 4 dereferenceable(16) %34)
  %37 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %arrayidx47 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 3
  %arrayidx48 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 0
  %38 = load %class.btVector3*, %class.btVector3** %color.addr, align 4, !tbaa !2
  %39 = bitcast %class.btIDebugDraw* %37 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable49 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %39, align 4, !tbaa !25
  %vfn50 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable49, i64 2
  %40 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn50, align 4
  call void %40(%class.btIDebugDraw* %37, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx47, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx48, %class.btVector3* nonnull align 4 dereferenceable(16) %38)
  %41 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %arrayidx51 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 4
  %arrayidx52 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 5
  %42 = load %class.btVector3*, %class.btVector3** %color.addr, align 4, !tbaa !2
  %43 = bitcast %class.btIDebugDraw* %41 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable53 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %43, align 4, !tbaa !25
  %vfn54 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable53, i64 2
  %44 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn54, align 4
  call void %44(%class.btIDebugDraw* %41, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx51, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx52, %class.btVector3* nonnull align 4 dereferenceable(16) %42)
  %45 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 5
  %arrayidx56 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 6
  %46 = load %class.btVector3*, %class.btVector3** %color.addr, align 4, !tbaa !2
  %47 = bitcast %class.btIDebugDraw* %45 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable57 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %47, align 4, !tbaa !25
  %vfn58 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable57, i64 2
  %48 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn58, align 4
  call void %48(%class.btIDebugDraw* %45, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx55, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx56, %class.btVector3* nonnull align 4 dereferenceable(16) %46)
  %49 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %arrayidx59 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 6
  %arrayidx60 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 7
  %50 = load %class.btVector3*, %class.btVector3** %color.addr, align 4, !tbaa !2
  %51 = bitcast %class.btIDebugDraw* %49 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable61 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %51, align 4, !tbaa !25
  %vfn62 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable61, i64 2
  %52 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn62, align 4
  call void %52(%class.btIDebugDraw* %49, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx59, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx60, %class.btVector3* nonnull align 4 dereferenceable(16) %50)
  %53 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %arrayidx63 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 7
  %arrayidx64 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 4
  %54 = load %class.btVector3*, %class.btVector3** %color.addr, align 4, !tbaa !2
  %55 = bitcast %class.btIDebugDraw* %53 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable65 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %55, align 4, !tbaa !25
  %vfn66 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable65, i64 2
  %56 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn66, align 4
  call void %56(%class.btIDebugDraw* %53, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx63, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx64, %class.btVector3* nonnull align 4 dereferenceable(16) %54)
  %57 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %arrayidx67 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 0
  %arrayidx68 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 4
  %58 = load %class.btVector3*, %class.btVector3** %color.addr, align 4, !tbaa !2
  %59 = bitcast %class.btIDebugDraw* %57 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable69 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %59, align 4, !tbaa !25
  %vfn70 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable69, i64 2
  %60 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn70, align 4
  call void %60(%class.btIDebugDraw* %57, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx67, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx68, %class.btVector3* nonnull align 4 dereferenceable(16) %58)
  %61 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %arrayidx71 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 1
  %arrayidx72 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 5
  %62 = load %class.btVector3*, %class.btVector3** %color.addr, align 4, !tbaa !2
  %63 = bitcast %class.btIDebugDraw* %61 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable73 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %63, align 4, !tbaa !25
  %vfn74 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable73, i64 2
  %64 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn74, align 4
  call void %64(%class.btIDebugDraw* %61, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx71, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx72, %class.btVector3* nonnull align 4 dereferenceable(16) %62)
  %65 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %arrayidx75 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 2
  %arrayidx76 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 6
  %66 = load %class.btVector3*, %class.btVector3** %color.addr, align 4, !tbaa !2
  %67 = bitcast %class.btIDebugDraw* %65 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable77 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %67, align 4, !tbaa !25
  %vfn78 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable77, i64 2
  %68 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn78, align 4
  call void %68(%class.btIDebugDraw* %65, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx75, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx76, %class.btVector3* nonnull align 4 dereferenceable(16) %66)
  %69 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4, !tbaa !2
  %arrayidx79 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 3
  %arrayidx80 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 7
  %70 = load %class.btVector3*, %class.btVector3** %color.addr, align 4, !tbaa !2
  %71 = bitcast %class.btIDebugDraw* %69 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable81 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %71, align 4, !tbaa !25
  %vfn82 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable81, i64 2
  %72 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn82, align 4
  call void %72(%class.btIDebugDraw* %69, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx79, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx80, %class.btVector3* nonnull align 4 dereferenceable(16) %70)
  %73 = bitcast [8 x %class.btVector3]* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 128, i8* %73) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZNK10btDbvtNode6isleafEv(%struct.btDbvtNode* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvtNode* %this, %struct.btDbvtNode** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %this1, i32 0, i32 2
  %childs = bitcast %union.anon.23* %0 to [2 x %struct.btDbvtNode*]*
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 1
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx, align 4, !tbaa !24
  %cmp = icmp eq %struct.btDbvtNode* %1, null
  ret i1 %cmp
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !8
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !8
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !8
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !8
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !8
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !8
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !8
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !8
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !8
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector34lerpERKS_RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %t) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %t.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %t, float** %t.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !8
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %3 = load float, float* %arrayidx3, align 4, !tbaa !8
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 0
  %4 = load float, float* %arrayidx5, align 4, !tbaa !8
  %sub = fsub float %3, %4
  %5 = load float*, float** %t.addr, align 4, !tbaa !2
  %6 = load float, float* %5, align 4, !tbaa !8
  %mul = fmul float %sub, %6
  %add = fadd float %1, %mul
  store float %add, float* %ref.tmp, align 4, !tbaa !8
  %7 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %m_floats7 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 1
  %8 = load float, float* %arrayidx8, align 4, !tbaa !8
  %9 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 1
  %10 = load float, float* %arrayidx10, align 4, !tbaa !8
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 1
  %11 = load float, float* %arrayidx12, align 4, !tbaa !8
  %sub13 = fsub float %10, %11
  %12 = load float*, float** %t.addr, align 4, !tbaa !2
  %13 = load float, float* %12, align 4, !tbaa !8
  %mul14 = fmul float %sub13, %13
  %add15 = fadd float %8, %mul14
  store float %add15, float* %ref.tmp6, align 4, !tbaa !8
  %14 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %15 = load float, float* %arrayidx18, align 4, !tbaa !8
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats19 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [4 x float], [4 x float]* %m_floats19, i32 0, i32 2
  %17 = load float, float* %arrayidx20, align 4, !tbaa !8
  %m_floats21 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx22 = getelementptr inbounds [4 x float], [4 x float]* %m_floats21, i32 0, i32 2
  %18 = load float, float* %arrayidx22, align 4, !tbaa !8
  %sub23 = fsub float %17, %18
  %19 = load float*, float** %t.addr, align 4, !tbaa !2
  %20 = load float, float* %19, align 4, !tbaa !8
  %mul24 = fmul float %sub23, %20
  %add25 = fadd float %15, %mul24
  store float %add25, float* %ref.tmp16, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp16)
  %21 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #6
  %22 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #6
  %23 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z5btCosf(float %x) #2 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !8
  %0 = load float, float* %x.addr, align 4, !tbaa !8
  %1 = call float @llvm.cos.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z5btSinf(float %x) #2 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !8
  %0 = load float, float* %x.addr, align 4, !tbaa !8
  %1 = call float @llvm.sin.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.cos.f32(float) #8

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sin.f32(float) #8

define linkonce_odr hidden %class.btAlignedObjectArray.93* @_ZN20btAlignedObjectArrayIjEC2Ev(%class.btAlignedObjectArray.93* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.93*, align 4
  store %class.btAlignedObjectArray.93* %this, %class.btAlignedObjectArray.93** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.93*, %class.btAlignedObjectArray.93** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.94* @_ZN18btAlignedAllocatorIjLj16EEC2Ev(%class.btAlignedAllocator.94* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIjE4initEv(%class.btAlignedObjectArray.93* %this1)
  ret %class.btAlignedObjectArray.93* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.94* @_ZN18btAlignedAllocatorIjLj16EEC2Ev(%class.btAlignedAllocator.94* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.94*, align 4
  store %class.btAlignedAllocator.94* %this, %class.btAlignedAllocator.94** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.94*, %class.btAlignedAllocator.94** %this.addr, align 4
  ret %class.btAlignedAllocator.94* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE4initEv(%class.btAlignedObjectArray.93* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.93*, align 4
  store %class.btAlignedObjectArray.93* %this, %class.btAlignedObjectArray.93** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.93*, %class.btAlignedObjectArray.93** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !152
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 4
  store i32* null, i32** %m_data, align 4, !tbaa !145
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !153
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !154
  ret void
}

define linkonce_odr hidden %class.btAlignedObjectArray.97* @_ZN20btAlignedObjectArrayIP14btHullTriangleEC2Ev(%class.btAlignedObjectArray.97* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.97*, align 4
  store %class.btAlignedObjectArray.97* %this, %class.btAlignedObjectArray.97** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.97*, %class.btAlignedObjectArray.97** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.97, %class.btAlignedObjectArray.97* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.98* @_ZN18btAlignedAllocatorIP14btHullTriangleLj16EEC2Ev(%class.btAlignedAllocator.98* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP14btHullTriangleE4initEv(%class.btAlignedObjectArray.97* %this1)
  ret %class.btAlignedObjectArray.97* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.98* @_ZN18btAlignedAllocatorIP14btHullTriangleLj16EEC2Ev(%class.btAlignedAllocator.98* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.98*, align 4
  store %class.btAlignedAllocator.98* %this, %class.btAlignedAllocator.98** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.98*, %class.btAlignedAllocator.98** %this.addr, align 4
  ret %class.btAlignedAllocator.98* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP14btHullTriangleE4initEv(%class.btAlignedObjectArray.97* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.97*, align 4
  store %class.btAlignedObjectArray.97* %this, %class.btAlignedObjectArray.97** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.97*, %class.btAlignedObjectArray.97** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.97, %class.btAlignedObjectArray.97* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !155
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.97, %class.btAlignedObjectArray.97* %this1, i32 0, i32 4
  store %class.btHullTriangle** null, %class.btHullTriangle*** %m_data, align 4, !tbaa !158
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.97, %class.btAlignedObjectArray.97* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !159
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.97, %class.btAlignedObjectArray.97* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !160
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.97* @_ZN20btAlignedObjectArrayIP14btHullTriangleED2Ev(%class.btAlignedObjectArray.97* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.97*, align 4
  store %class.btAlignedObjectArray.97* %this, %class.btAlignedObjectArray.97** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.97*, %class.btAlignedObjectArray.97** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP14btHullTriangleE5clearEv(%class.btAlignedObjectArray.97* %this1)
  ret %class.btAlignedObjectArray.97* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP14btHullTriangleE5clearEv(%class.btAlignedObjectArray.97* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.97*, align 4
  store %class.btAlignedObjectArray.97* %this, %class.btAlignedObjectArray.97** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.97*, %class.btAlignedObjectArray.97** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP14btHullTriangleE4sizeEv(%class.btAlignedObjectArray.97* %this1)
  call void @_ZN20btAlignedObjectArrayIP14btHullTriangleE7destroyEii(%class.btAlignedObjectArray.97* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP14btHullTriangleE10deallocateEv(%class.btAlignedObjectArray.97* %this1)
  call void @_ZN20btAlignedObjectArrayIP14btHullTriangleE4initEv(%class.btAlignedObjectArray.97* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP14btHullTriangleE7destroyEii(%class.btAlignedObjectArray.97* %this, i32 %first, i32 %last) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.97*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.97* %this, %class.btAlignedObjectArray.97** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.97*, %class.btAlignedObjectArray.97** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.97, %class.btAlignedObjectArray.97* %this1, i32 0, i32 4
  %4 = load %class.btHullTriangle**, %class.btHullTriangle*** %m_data, align 4, !tbaa !158
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btHullTriangle*, %class.btHullTriangle** %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP14btHullTriangleE4sizeEv(%class.btAlignedObjectArray.97* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.97*, align 4
  store %class.btAlignedObjectArray.97* %this, %class.btAlignedObjectArray.97** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.97*, %class.btAlignedObjectArray.97** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.97, %class.btAlignedObjectArray.97* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !159
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP14btHullTriangleE10deallocateEv(%class.btAlignedObjectArray.97* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.97*, align 4
  store %class.btAlignedObjectArray.97* %this, %class.btAlignedObjectArray.97** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.97*, %class.btAlignedObjectArray.97** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.97, %class.btAlignedObjectArray.97* %this1, i32 0, i32 4
  %0 = load %class.btHullTriangle**, %class.btHullTriangle*** %m_data, align 4, !tbaa !158
  %tobool = icmp ne %class.btHullTriangle** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.97, %class.btAlignedObjectArray.97* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !155, !range !22
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.97, %class.btAlignedObjectArray.97* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.97, %class.btAlignedObjectArray.97* %this1, i32 0, i32 4
  %2 = load %class.btHullTriangle**, %class.btHullTriangle*** %m_data4, align 4, !tbaa !158
  call void @_ZN18btAlignedAllocatorIP14btHullTriangleLj16EE10deallocateEPS1_(%class.btAlignedAllocator.98* %m_allocator, %class.btHullTriangle** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.97, %class.btAlignedObjectArray.97* %this1, i32 0, i32 4
  store %class.btHullTriangle** null, %class.btHullTriangle*** %m_data5, align 4, !tbaa !158
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP14btHullTriangleLj16EE10deallocateEPS1_(%class.btAlignedAllocator.98* %this, %class.btHullTriangle** %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.98*, align 4
  %ptr.addr = alloca %class.btHullTriangle**, align 4
  store %class.btAlignedAllocator.98* %this, %class.btAlignedAllocator.98** %this.addr, align 4, !tbaa !2
  store %class.btHullTriangle** %ptr, %class.btHullTriangle*** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.98*, %class.btAlignedAllocator.98** %this.addr, align 4
  %0 = load %class.btHullTriangle**, %class.btHullTriangle*** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btHullTriangle** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.93* @_ZN20btAlignedObjectArrayIjED2Ev(%class.btAlignedObjectArray.93* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.93*, align 4
  store %class.btAlignedObjectArray.93* %this, %class.btAlignedObjectArray.93** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.93*, %class.btAlignedObjectArray.93** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIjE5clearEv(%class.btAlignedObjectArray.93* %this1)
  ret %class.btAlignedObjectArray.93* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE5clearEv(%class.btAlignedObjectArray.93* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.93*, align 4
  store %class.btAlignedObjectArray.93* %this, %class.btAlignedObjectArray.93** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.93*, %class.btAlignedObjectArray.93** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIjE4sizeEv(%class.btAlignedObjectArray.93* %this1)
  call void @_ZN20btAlignedObjectArrayIjE7destroyEii(%class.btAlignedObjectArray.93* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIjE10deallocateEv(%class.btAlignedObjectArray.93* %this1)
  call void @_ZN20btAlignedObjectArrayIjE4initEv(%class.btAlignedObjectArray.93* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE7destroyEii(%class.btAlignedObjectArray.93* %this, i32 %first, i32 %last) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.93*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.93* %this, %class.btAlignedObjectArray.93** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.93*, %class.btAlignedObjectArray.93** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 4
  %4 = load i32*, i32** %m_data, align 4, !tbaa !145
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIjE4sizeEv(%class.btAlignedObjectArray.93* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.93*, align 4
  store %class.btAlignedObjectArray.93* %this, %class.btAlignedObjectArray.93** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.93*, %class.btAlignedObjectArray.93** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !153
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE10deallocateEv(%class.btAlignedObjectArray.93* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.93*, align 4
  store %class.btAlignedObjectArray.93* %this, %class.btAlignedObjectArray.93** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.93*, %class.btAlignedObjectArray.93** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !145
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !152, !range !22
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4, !tbaa !145
  call void @_ZN18btAlignedAllocatorIjLj16EE10deallocateEPj(%class.btAlignedAllocator.94* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4, !tbaa !145
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIjLj16EE10deallocateEPj(%class.btAlignedAllocator.94* %this, i32* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.94*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.94* %this, %class.btAlignedAllocator.94** %this.addr, align 4, !tbaa !2
  store i32* %ptr, i32** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.94*, %class.btAlignedAllocator.94** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.9* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator.9* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  ret %class.btAlignedAllocator.9* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray.8* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !161
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data, align 4, !tbaa !48
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !49
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !162
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray.8* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray.8* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.8* %this, i32 %first, i32 %last) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %4 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !48
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.8* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !48
  %tobool = icmp ne %class.btVector3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !161, !range !22
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %2 = load %class.btVector3*, %class.btVector3** %m_data4, align 4, !tbaa !48
  call void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.9* %m_allocator, %class.btVector3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data5, align 4, !tbaa !48
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.9* %this, %class.btVector3* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %ptr.addr = alloca %class.btVector3*, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %ptr, %class.btVector3** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVector3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray.8* %this, i32 %_Count) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btVector3*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray.8* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btVector3** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray.8* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btVector3*
  store %class.btVector3* %3, %class.btVector3** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %this1)
  %4 = load %class.btVector3*, %class.btVector3** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call3, %class.btVector3* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.8* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !161
  %5 = load %class.btVector3*, %class.btVector3** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %class.btVector3* %5, %class.btVector3** %m_data, align 4, !tbaa !48
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !162
  %7 = bitcast %class.btVector3** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN9btVector3nwEmPv(i32 %0, i8* %ptr) #2 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !125
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray.8* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !162
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray.8* %this, i32 %size) #4 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.9* %m_allocator, i32 %1, %class.btVector3** null)
  %2 = bitcast %class.btVector3* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray.8* %this, i32 %start, i32 %end, %class.btVector3* %dest) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store %class.btVector3* %dest, %class.btVector3** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btVector3*, %class.btVector3** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 %5
  %6 = bitcast %class.btVector3* %arrayidx to i8*
  %call = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %6)
  %7 = bitcast i8* %call to %class.btVector3*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %8 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !48
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 %9
  %10 = bitcast %class.btVector3* %7 to i8*
  %11 = bitcast %class.btVector3* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !23
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #6
  ret void
}

define linkonce_odr hidden %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.9* %this, i32 %n, %class.btVector3** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btVector3**, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store %class.btVector3** %hint, %class.btVector3*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btVector3*
  ret %class.btVector3* %1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.82* @_ZN18btAlignedAllocatorIbLj16EEC2Ev(%class.btAlignedAllocator.82* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.82*, align 4
  store %class.btAlignedAllocator.82* %this, %class.btAlignedAllocator.82** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.82*, %class.btAlignedAllocator.82** %this.addr, align 4
  ret %class.btAlignedAllocator.82* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIbE4initEv(%class.btAlignedObjectArray.81* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.81*, align 4
  store %class.btAlignedObjectArray.81* %this, %class.btAlignedObjectArray.81** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.81*, %class.btAlignedObjectArray.81** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !163
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 4
  store i8* null, i8** %m_data, align 4, !tbaa !134
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !135
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !164
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIbE5clearEv(%class.btAlignedObjectArray.81* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.81*, align 4
  store %class.btAlignedObjectArray.81* %this, %class.btAlignedObjectArray.81** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.81*, %class.btAlignedObjectArray.81** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIbE4sizeEv(%class.btAlignedObjectArray.81* %this1)
  call void @_ZN20btAlignedObjectArrayIbE7destroyEii(%class.btAlignedObjectArray.81* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIbE10deallocateEv(%class.btAlignedObjectArray.81* %this1)
  call void @_ZN20btAlignedObjectArrayIbE4initEv(%class.btAlignedObjectArray.81* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIbE7destroyEii(%class.btAlignedObjectArray.81* %this, i32 %first, i32 %last) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.81*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.81* %this, %class.btAlignedObjectArray.81** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.81*, %class.btAlignedObjectArray.81** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 4
  %4 = load i8*, i8** %m_data, align 4, !tbaa !134
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIbE4sizeEv(%class.btAlignedObjectArray.81* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.81*, align 4
  store %class.btAlignedObjectArray.81* %this, %class.btAlignedObjectArray.81** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.81*, %class.btAlignedObjectArray.81** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !135
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIbE10deallocateEv(%class.btAlignedObjectArray.81* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.81*, align 4
  store %class.btAlignedObjectArray.81* %this, %class.btAlignedObjectArray.81** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.81*, %class.btAlignedObjectArray.81** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 4
  %0 = load i8*, i8** %m_data, align 4, !tbaa !134
  %tobool = icmp ne i8* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !163, !range !22
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 4
  %2 = load i8*, i8** %m_data4, align 4, !tbaa !134
  call void @_ZN18btAlignedAllocatorIbLj16EE10deallocateEPb(%class.btAlignedAllocator.82* %m_allocator, i8* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 4
  store i8* null, i8** %m_data5, align 4, !tbaa !134
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIbLj16EE10deallocateEPb(%class.btAlignedAllocator.82* %this, i8* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.82*, align 4
  %ptr.addr = alloca i8*, align 4
  store %class.btAlignedAllocator.82* %this, %class.btAlignedAllocator.82** %this.addr, align 4, !tbaa !2
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.82*, %class.btAlignedAllocator.82** %this.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIbE7reserveEi(%class.btAlignedObjectArray.81* %this, i32 %_Count) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.81*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca i8*, align 4
  store %class.btAlignedObjectArray.81* %this, %class.btAlignedObjectArray.81** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.81*, %class.btAlignedObjectArray.81** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIbE8capacityEv(%class.btAlignedObjectArray.81* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast i8** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayIbE8allocateEi(%class.btAlignedObjectArray.81* %this1, i32 %2)
  store i8* %call2, i8** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIbE4sizeEv(%class.btAlignedObjectArray.81* %this1)
  %3 = load i8*, i8** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIbE4copyEiiPb(%class.btAlignedObjectArray.81* %this1, i32 0, i32 %call3, i8* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIbE4sizeEv(%class.btAlignedObjectArray.81* %this1)
  call void @_ZN20btAlignedObjectArrayIbE7destroyEii(%class.btAlignedObjectArray.81* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIbE10deallocateEv(%class.btAlignedObjectArray.81* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !163
  %4 = load i8*, i8** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 4
  store i8* %4, i8** %m_data, align 4, !tbaa !134
  %5 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4, !tbaa !164
  %6 = bitcast i8** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIbE8capacityEv(%class.btAlignedObjectArray.81* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.81*, align 4
  store %class.btAlignedObjectArray.81* %this, %class.btAlignedObjectArray.81** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.81*, %class.btAlignedObjectArray.81** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !164
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIbE8allocateEi(%class.btAlignedObjectArray.81* %this, i32 %size) #4 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.81*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.81* %this, %class.btAlignedObjectArray.81** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.81*, %class.btAlignedObjectArray.81** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call i8* @_ZN18btAlignedAllocatorIbLj16EE8allocateEiPPKb(%class.btAlignedAllocator.82* %m_allocator, i32 %1, i8** null)
  store i8* %call, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i8*, i8** %retval, align 4
  ret i8* %2
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIbE4copyEiiPb(%class.btAlignedObjectArray.81* %this, i32 %start, i32 %end, i8* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.81*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.81* %this, %class.btAlignedObjectArray.81** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.81*, %class.btAlignedObjectArray.81** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 %5
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 4
  %6 = load i8*, i8** %m_data, align 4, !tbaa !134
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds i8, i8* %6, i32 %7
  %8 = load i8, i8* %arrayidx2, align 1, !tbaa !122, !range !22
  %tobool = trunc i8 %8 to i1
  %frombool = zext i1 %tobool to i8
  store i8 %frombool, i8* %arrayidx, align 1, !tbaa !122
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  ret void
}

define linkonce_odr hidden i8* @_ZN18btAlignedAllocatorIbLj16EE8allocateEiPPKb(%class.btAlignedAllocator.82* %this, i32 %n, i8** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.82*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca i8**, align 4
  store %class.btAlignedAllocator.82* %this, %class.btAlignedAllocator.82** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store i8** %hint, i8*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.82*, %class.btAlignedAllocator.82** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 1, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  ret i8* %call
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }
attributes #7 = { argmemonly nounwind willreturn writeonly }
attributes #8 = { nounwind readnone speculatable willreturn }
attributes #9 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #10 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #11 = { builtin allocsize(0) }
attributes #12 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"float", !4, i64 0}
!10 = !{!11, !14, i64 377}
!11 = !{!"_ZTSN10btSoftBody7ClusterE", !12, i64 0, !15, i64 20, !17, i64 40, !19, i64 60, !9, i64 124, !9, i64 128, !20, i64 132, !20, i64 180, !21, i64 228, !4, i64 244, !4, i64 276, !7, i64 308, !7, i64 312, !21, i64 316, !21, i64 332, !3, i64 348, !9, i64 352, !9, i64 356, !9, i64 360, !9, i64 364, !9, i64 368, !9, i64 372, !14, i64 376, !14, i64 377, !7, i64 380}
!12 = !{!"_ZTS20btAlignedObjectArrayIfE", !13, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !14, i64 16}
!13 = !{!"_ZTS18btAlignedAllocatorIfLj16EE"}
!14 = !{!"bool", !4, i64 0}
!15 = !{!"_ZTS20btAlignedObjectArrayIPN10btSoftBody4NodeEE", !16, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !14, i64 16}
!16 = !{!"_ZTS18btAlignedAllocatorIPN10btSoftBody4NodeELj16EE"}
!17 = !{!"_ZTS20btAlignedObjectArrayI9btVector3E", !18, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !14, i64 16}
!18 = !{!"_ZTS18btAlignedAllocatorI9btVector3Lj16EE"}
!19 = !{!"_ZTS11btTransform", !20, i64 0, !21, i64 48}
!20 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!21 = !{!"_ZTS9btVector3", !4, i64 0}
!22 = !{i8 0, i8 2}
!23 = !{i64 0, i64 16, !24}
!24 = !{!4, !4, i64 0}
!25 = !{!26, !26, i64 0}
!26 = !{!"vtable pointer", !5, i64 0}
!27 = !{!28, !3, i64 4}
!28 = !{!"_ZTSN10btSoftBody7FeatureE", !3, i64 4}
!29 = !{!30, !7, i64 16}
!30 = !{!"_ZTSN10btSoftBody8MaterialE", !9, i64 4, !9, i64 8, !9, i64 12, !7, i64 16}
!31 = !{!"branch_weights", i32 1, i32 1048575}
!32 = !{!33, !3, i64 24}
!33 = !{!"_ZTSN10btSoftBody8RContactE", !34, i64 0, !3, i64 24, !20, i64 28, !21, i64 76, !9, i64 92, !9, i64 96, !9, i64 100}
!34 = !{!"_ZTSN10btSoftBody4sCtiE", !3, i64 0, !21, i64 4, !9, i64 20}
!35 = !{!33, !9, i64 20}
!36 = !{!37, !3, i64 20}
!37 = !{!"_ZTSN10btSoftBody6AnchorE", !3, i64 0, !21, i64 4, !3, i64 20, !9, i64 24, !20, i64 28, !21, i64 76, !9, i64 92}
!38 = !{!37, !3, i64 0}
!39 = !{!40, !9, i64 88}
!40 = !{!"_ZTSN10btSoftBody4NodeE", !21, i64 8, !21, i64 24, !21, i64 40, !21, i64 56, !21, i64 72, !9, i64 88, !9, i64 92, !3, i64 96, !7, i64 100}
!41 = !{!42, !7, i64 24}
!42 = !{!"_ZTSN10btSoftBody4NoteE", !3, i64 4, !21, i64 8, !7, i64 24, !4, i64 28, !4, i64 44}
!43 = !{!42, !3, i64 4}
!44 = !{!45, !7, i64 4}
!45 = !{!"_ZTS20btAlignedObjectArrayIPN10btSoftBody7ClusterEE", !46, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !14, i64 16}
!46 = !{!"_ZTS18btAlignedAllocatorIPN10btSoftBody7ClusterELj16EE"}
!47 = !{!45, !3, i64 12}
!48 = !{!17, !3, i64 12}
!49 = !{!17, !7, i64 4}
!50 = !{!15, !7, i64 4}
!51 = !{!15, !3, i64 12}
!52 = !{!53, !7, i64 4}
!53 = !{!"_ZTS20btAlignedObjectArrayIiE", !54, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !14, i64 16}
!54 = !{!"_ZTS18btAlignedAllocatorIiLj16EE"}
!55 = !{!53, !3, i64 12}
!56 = !{!57, !3, i64 12}
!57 = !{!"_ZTS20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE", !58, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !14, i64 16}
!58 = !{!"_ZTS18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EE"}
!59 = !{!60, !7, i64 4}
!60 = !{!"_ZTSN20btConvexHullComputer4EdgeE", !7, i64 0, !7, i64 4, !7, i64 8}
!61 = !{!60, !7, i64 8}
!62 = !{!63, !7, i64 4}
!63 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody4NodeEE", !64, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !14, i64 16}
!64 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody4NodeELj16EE"}
!65 = !{!63, !3, i64 12}
!66 = !{!67, !7, i64 4}
!67 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody4LinkEE", !68, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !14, i64 16}
!68 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody4LinkELj16EE"}
!69 = !{!67, !3, i64 12}
!70 = !{!71, !7, i64 4}
!71 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody8RContactEE", !72, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !14, i64 16}
!72 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody8RContactELj16EE"}
!73 = !{!71, !3, i64 12}
!74 = !{!75, !7, i64 4}
!75 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody4FaceEE", !76, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !14, i64 16}
!76 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody4FaceELj16EE"}
!77 = !{!75, !3, i64 12}
!78 = !{!79, !7, i64 4}
!79 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody5TetraEE", !80, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !14, i64 16}
!80 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody5TetraELj16EE"}
!81 = !{!79, !3, i64 12}
!82 = !{!83, !7, i64 4}
!83 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody6AnchorEE", !84, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !14, i64 16}
!84 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody6AnchorELj16EE"}
!85 = !{!83, !3, i64 12}
!86 = !{!87, !7, i64 4}
!87 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody4NoteEE", !88, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !14, i64 16}
!88 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody4NoteELj16EE"}
!89 = !{!87, !3, i64 12}
!90 = !{!91, !3, i64 928}
!91 = !{!"_ZTS10btSoftBody", !92, i64 264, !3, i64 284, !94, i64 288, !100, i64 452, !101, i64 472, !3, i64 680, !3, i64 684, !87, i64 688, !63, i64 708, !67, i64 728, !75, i64 748, !79, i64 768, !83, i64 788, !71, i64 808, !102, i64 828, !104, i64 848, !106, i64 868, !9, i64 888, !4, i64 892, !14, i64 924, !108, i64 928, !108, i64 988, !108, i64 1048, !45, i64 1108, !113, i64 1128, !19, i64 1148, !21, i64 1212, !9, i64 1228, !53, i64 1232}
!92 = !{!"_ZTS20btAlignedObjectArrayIPK17btCollisionObjectE", !93, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !14, i64 16}
!93 = !{!"_ZTS18btAlignedAllocatorIPK17btCollisionObjectLj16EE"}
!94 = !{!"_ZTSN10btSoftBody6ConfigE", !95, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !9, i64 16, !9, i64 20, !9, i64 24, !9, i64 28, !9, i64 32, !9, i64 36, !9, i64 40, !9, i64 44, !9, i64 48, !9, i64 52, !9, i64 56, !9, i64 60, !9, i64 64, !9, i64 68, !9, i64 72, !9, i64 76, !9, i64 80, !7, i64 84, !7, i64 88, !7, i64 92, !7, i64 96, !7, i64 100, !96, i64 104, !98, i64 124, !98, i64 144}
!95 = !{!"_ZTSN10btSoftBody10eAeroModel1_E", !4, i64 0}
!96 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody8eVSolver1_EE", !97, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !14, i64 16}
!97 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody8eVSolver1_ELj16EE"}
!98 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody8ePSolver1_EE", !99, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !14, i64 16}
!99 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody8ePSolver1_ELj16EE"}
!100 = !{!"_ZTSN10btSoftBody11SolverStateE", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !9, i64 16}
!101 = !{!"_ZTSN10btSoftBody4PoseE", !14, i64 0, !14, i64 1, !9, i64 4, !17, i64 8, !12, i64 28, !21, i64 48, !20, i64 64, !20, i64 112, !20, i64 160}
!102 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody8SContactEE", !103, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !14, i64 16}
!103 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody8SContactELj16EE"}
!104 = !{!"_ZTS20btAlignedObjectArrayIPN10btSoftBody5JointEE", !105, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !14, i64 16}
!105 = !{!"_ZTS18btAlignedAllocatorIPN10btSoftBody5JointELj16EE"}
!106 = !{!"_ZTS20btAlignedObjectArrayIPN10btSoftBody8MaterialEE", !107, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !14, i64 16}
!107 = !{!"_ZTS18btAlignedAllocatorIPN10btSoftBody8MaterialELj16EE"}
!108 = !{!"_ZTS6btDbvt", !3, i64 0, !3, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !109, i64 20, !111, i64 40}
!109 = !{!"_ZTS20btAlignedObjectArrayIN6btDbvt6sStkNNEE", !110, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !14, i64 16}
!110 = !{!"_ZTS18btAlignedAllocatorIN6btDbvt6sStkNNELj16EE"}
!111 = !{!"_ZTS20btAlignedObjectArrayIPK10btDbvtNodeE", !112, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !14, i64 16}
!112 = !{!"_ZTS18btAlignedAllocatorIPK10btDbvtNodeLj16EE"}
!113 = !{!"_ZTS20btAlignedObjectArrayIbE", !114, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !14, i64 16}
!114 = !{!"_ZTS18btAlignedAllocatorIbLj16EE"}
!115 = !{!91, !3, i64 988}
!116 = !{!91, !3, i64 1048}
!117 = !{!104, !7, i64 4}
!118 = !{!104, !3, i64 12}
!119 = !{!120, !3, i64 8}
!120 = !{!"_ZTSN10btSoftBody4BodyE", !3, i64 0, !3, i64 4, !3, i64 8}
!121 = !{!120, !3, i64 0}
!122 = !{!14, !14, i64 0}
!123 = !{!40, !9, i64 92}
!124 = !{!91, !14, i64 473}
!125 = !{!126, !126, i64 0}
!126 = !{!"long", !4, i64 0}
!127 = !{!128, !7, i64 20}
!128 = !{!"_ZTS8HullDesc", !7, i64 0, !7, i64 4, !3, i64 8, !7, i64 12, !9, i64 16, !7, i64 20, !7, i64 24}
!129 = !{!130, !7, i64 4}
!130 = !{!"_ZTS10HullResult", !14, i64 0, !7, i64 4, !17, i64 8, !7, i64 28, !7, i64 32, !131, i64 36}
!131 = !{!"_ZTS20btAlignedObjectArrayIjE", !132, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !14, i64 16}
!132 = !{!"_ZTS18btAlignedAllocatorIjLj16EE"}
!133 = !{!130, !7, i64 28}
!134 = !{!113, !3, i64 12}
!135 = !{!113, !7, i64 4}
!136 = !{!137, !137, i64 0}
!137 = !{!"_ZTS8HullFlag", !4, i64 0}
!138 = !{!128, !7, i64 0}
!139 = !{!128, !7, i64 4}
!140 = !{!128, !3, i64 8}
!141 = !{!128, !7, i64 12}
!142 = !{!128, !9, i64 16}
!143 = !{!130, !14, i64 0}
!144 = !{!130, !7, i64 32}
!145 = !{!131, !3, i64 12}
!146 = !{!57, !14, i64 16}
!147 = !{!57, !7, i64 4}
!148 = !{!57, !7, i64 8}
!149 = !{!53, !14, i64 16}
!150 = !{!53, !7, i64 8}
!151 = !{!60, !7, i64 0}
!152 = !{!131, !14, i64 16}
!153 = !{!131, !7, i64 4}
!154 = !{!131, !7, i64 8}
!155 = !{!156, !14, i64 16}
!156 = !{!"_ZTS20btAlignedObjectArrayIP14btHullTriangleE", !157, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !14, i64 16}
!157 = !{!"_ZTS18btAlignedAllocatorIP14btHullTriangleLj16EE"}
!158 = !{!156, !3, i64 12}
!159 = !{!156, !7, i64 4}
!160 = !{!156, !7, i64 8}
!161 = !{!17, !14, i64 16}
!162 = !{!17, !7, i64 8}
!163 = !{!113, !14, i64 16}
!164 = !{!113, !7, i64 8}
